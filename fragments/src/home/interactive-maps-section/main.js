const editMode = document.body.classList.contains('has-edit-mode-menu');

/*******************************************************/
const desktopContentId = 36556;
const mobileContentId = 36593;
const DOMAIN = Liferay.ThemeDisplay.getPortalURL();
const siteId = Liferay.ThemeDisplay.getScopeGroupId();
const getQuery = (contentId, siteId) => `{
  structuredContentByKey(key: "${contentId}", siteKey: "${siteId}") {
    title
    key
    contentFields {
      label
      name
      value {
        data
        image {
          contentUrl
          description
        }
      }
    }
  }
}`;
const renderContent = ({data}) => {
  if (! data.structuredContentByKey) return false;
  const content = data.structuredContentByKey;

  if(content.key === desktopContentId.toString()){
    const images = data.structuredContentByKey.contentFields;
    const section = fragmentElement.querySelector('section.interactive-maps');
    if(!section) return false;
    images.forEach((item, index) => {
      const element = section.querySelector('.'+item.name);
      if (element && element.tagName === 'A'){
        element.setAttribute('href', item.value.image.contentUrl);
      }
    });
  }else if(content.key === mobileContentId.toString()){
    const fields = data.structuredContentByKey.contentFields;
    const section = fragmentElement.querySelector('section.mobile-interactive-maps');
    if(!section) return false;
    fields.forEach((item, index) => {
      const element = section.querySelector(`[data-field-id="${item.name}"]`);
      if (element && element.tagName === 'IMG'){
        element.setAttribute('src', item.value.image.contentUrl);
        element.setAttribute('alt', item.value.image.description);
      }else if (element && element.tagName === 'A'){
        element.setAttribute('href', item.value.image.contentUrl);
      }else if (element && (element.tagName === 'H4' || element.tagName === 'P' || element.tagName === 'SPAN')){
        element.textContent = item.value.data;
      }else if (element && element.tagName === 'OPTION'){
        element.textContent = item.value.data;
        element.value = item.value.data;
      }
    });
  }
}

const getContent = (contentId) => {
  const options = {
    method: "post",
    headers: {
      "Content-Type": "application/json",
      authorization: `Basic aW5mb0BtaXNrLm9yZy5zYTpUcmFmZmljMTIzCg==`
    },
    body: JSON.stringify({
      query: getQuery(contentId, siteId)
    })
  };

  fetch(`${DOMAIN}/o/graphql`, options)
    .then(res => res.json())
    .then(renderContent);
}
getContent(desktopContentId);
getContent(mobileContentId);
/*******************************************************/

$(window).on('load', function (){

const districtsSlider = $('.districts-slider');
const $districtsPinClick = $('.districts-pin-click[data-id]');
const assetsSlider = $('.assets-slider');
const $assetsPinClick = $('.assets-pin-click[data-id]');
const mobileDistrictsImageSlider = $('.m-districts-image-slider');
const mobileDistrictsTextSlider = $('.m-districts-text-slider');
const districtsSelect = $('.districts-select');
const mobileAssetsImageSlider = $('.m-assets-image-slider');
const mobileAssetsTextSlider = $('.m-assets-text-slider');
const assetsSelect = $('.assets-select');

let pin_icon = '.pin-icon';

if(editMode){
  pin_icon = '.pin-icon img';
  const fancyboxImages = fragmentElement.querySelectorAll('a[data-fancybox]');
  fancyboxImages.forEach((item, index) => {
    item.style.display = 'none';
  });
}

$(document).ready(function () {
  $('.map-tab-btns a').on('click', function () {
    $('.pin-click').removeClass("active");
    $(".map-close").hide();
    $(".map-info-window").slideUp();
    $(".pin-click").removeClass("bounceInDown animated");

    $(".pin-click").each(function () {
      var animate = $(this).attr('data-animation');
      var speedDuration = $(this).attr('data-duration');
      var $t = $(this);
      setTimeout(function () {
        $t.addClass(animate + ' animated');
      }, speedDuration);
    });

    assetsSlider.slick("reinit");
    districtsSlider.slick("reinit");

    $('[data-map-name]').hide();
    $('[data-map-name="'+$(this).data("target")+'"]').show();
    $('.map-tab-btns a').removeClass("active");
    $(this).addClass('active');
  });
  if (districtsSlider.length > 0) {
    districtsSlider
      .slick({
        dots: false,
        infinite: false,
        fade: true,
        arrows: false,
        cssEase: 'ease-out',
        speed: 500,
        swipe: false,
        pauseOnFocus: false,
        lazyLoad: 'ondemand',
      })
  }
  if ($districtsPinClick.length > 0) {
    $districtsPinClick.on("click", pin_icon, function () {
      var thiss = $(this).closest(".pin-click");
      var slideno = thiss.data('id');
      if(!thiss.hasClass("active")){
        $districtsPinClick.removeClass("active");
        thiss.addClass("active");
        $(".map-close").hide();
        $(".map-info-window").slideUp();
        thiss.find(".map-info-window").slideDown(250);
        setTimeout(function () {
          thiss.find(".map-close").show();
        }, 250);
        districtsSlider.slick('slickGoTo', slideno);
      }
    });
    $districtsPinClick.on("click", ".map-close", function () {
      $(this).hide();
      $districtsPinClick.removeClass("active");
      $(".map-info-window").slideUp();
      districtsSlider.slick('slickGoTo', 0);
    })
  }
  if (assetsSlider.length > 0) {
    assetsSlider
      .slick({
        dots: false,
        infinite: false,
        cssEase: 'ease-out',
        speed: 500,
        fade: true,
        arrows: false,
        swipe: false,
        pauseOnFocus: false,
        lazyLoad: 'ondemand',
      })
  }
  if ($assetsPinClick.length > 0) {
    $assetsPinClick.on("click", pin_icon, function () {
      var thiss = $(this).closest(".pin-click");
      var slideno = thiss.data('id');
      if(!thiss.hasClass("active")){
        $assetsPinClick.removeClass("active");
        thiss.addClass("active");
        $(".map-close").hide();
        $(".map-info-window").slideUp();
        thiss.find(".map-info-window").slideDown(250);
        setTimeout(function () {
          thiss.find(".map-close").show();
        }, 250);
        thiss.find(".map-info-window").slideDown();
        assetsSlider.slick('slickGoTo', slideno);
      }
    });
    $assetsPinClick.on("click", ".map-close", function () {
      $(this).hide();
      $assetsPinClick.removeClass("active");
      $(".map-close").hide();
      $(".map-info-window").slideUp();
      assetsSlider.slick('slickGoTo', 0);
    })
  }
  $('.mobile-map-btns a').on('click', function () {
    $('.mobile-interactive-maps').addClass('loading').append('<div class="loader"></div>');
    setTimeout(function () {
      $('.mobile-interactive-maps').removeClass('loading');
      $('.mobile-interactive-maps').find('.loader').remove();
    }, 350)
    setTimeout(function () {
      mobileDistrictsImageSlider.slick("reinit");
      mobileDistrictsTextSlider.slick("reinit");
      mobileAssetsImageSlider.slick("reinit");
      mobileAssetsTextSlider.slick("reinit");
    }, 300)
    $('[data-mobile-map-name]').hide();
    $('[data-mobile-map-name="'+$(this).data("target")+'"]').show();
    $('.mobile-map-btns a').removeClass("active");
    $(this).addClass('active');
  });
  if (mobileDistrictsImageSlider.length > 0) {
    mobileDistrictsImageSlider
      .slick({
        dots: false,
        infinite: false,
        fade: true,
        arrows: false,
        cssEase: 'ease-out',
        speed: 500,
        swipe: false,
        pauseOnFocus: false,
        lazyLoad: 'ondemand',
        focusOnSelect: true,
        asNavFor: mobileDistrictsTextSlider
      })
  }
  if (mobileDistrictsTextSlider.length > 0) {
    mobileDistrictsTextSlider
      .slick({
        dots: false,
        infinite: false,
        fade: true,
        arrows: false,
        speed: 500,
        swipe: false,
        pauseOnFocus: false,
        lazyLoad: 'ondemand',
        focusOnSelect: true,
        asNavFor: mobileDistrictsImageSlider
      })
  }
  if (districtsSelect.length > 0) {
    districtsSelect.on('change', 'select.selectpicker', function () {
      var slideno = $(this).find('option:selected').data('id');
      mobileDistrictsTextSlider.slick('slickGoTo', slideno - 0);
    });
  }
  if (mobileAssetsImageSlider.length > 0) {
    mobileAssetsImageSlider
      .slick({
        dots: false,
        infinite: false,
        fade: true,
        arrows: false,
        cssEase: 'ease-out',
        speed: 500,
        swipe: false,
        pauseOnFocus: false,
        lazyLoad: 'ondemand',
        focusOnSelect: true,
        asNavFor: mobileAssetsTextSlider
      })
  }
  if (mobileAssetsTextSlider.length > 0) {
    mobileAssetsTextSlider
      .slick({
        dots: false,
        infinite: false,
        fade: true,
        arrows: false,
        speed: 500,
        swipe: false,
        pauseOnFocus: false,
        lazyLoad: 'ondemand',
        focusOnSelect: true,
        asNavFor: mobileAssetsImageSlider
      })
  }
  if (assetsSelect.length > 0) {
    assetsSelect.on('change', 'select.selectpicker', function () {
      var slideno = $(this).find('option:selected').data('id');
      mobileAssetsTextSlider.slick('slickGoTo', slideno - 0);
    });
  }
  if($('[data-fancybox]').length> 0){
    $('[data-fancybox]').fancybox({
      //toolbar  : false,
      //smallBtn : true,
      loop: false,
      keyboard: true,
      //infobar: false,
      clickSlide: false,
      clickOutside: false,
      touch: false,
      buttons: [
        //"zoom",
        //"share",
        //"slideShow",
        //"fullScreen",
        //"download",
        //"thumbs",
        "close"
      ],
    })
  }
});


  $('.selectpicker22').selectpicker();

  var $interactivemap = $('.mobile-interactive-maps');
  $interactivemap.find('.filter-option-inner-inner').click(function(){
    console.log("asd");
    $(".selectpicker22").selectpicker('toggle');
  });


});
