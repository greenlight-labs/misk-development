console.group('Form Section');
console.log('fragmentElement', fragmentElement);
console.groupEnd();

var
  $contactUs = $('#contactUs'),
  emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  letters = /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/;

$(window).on('load', function (){
  if ($contactUs.length > 0) {
    $contactUs
      .find('[name="property"]').selectpicker()
      .change(function (e) {
        $('#contactUs').formValidation('revalidateField', 'property');
      })
      .end()
      .formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        fields: {
          name: {
            validators: {
              notEmpty: {
                message: ' '
              }
            }
          },
          email: {
            validators: {
              notEmpty: {
                message: ' '
              }, regexp: {
                regexp: emailRegex,
                message: ' '
              }
            }
          },
          textarea: {
            validators: {
              notEmpty: {
                message: ' '
              }
            }
          },
          property: {
            validators: {
              notEmpty: {
                message: ' '
              }
            }
          }
        },
        onSuccess: function (e) {
          e.preventDefault();
          var $form = $(e.target),
            formId = '#' + $form[0].id;
          $(formId).addClass('loading').append('<div class="loader"></div>');
          /***************************/
          const DOMAIN = Liferay.ThemeDisplay.getPortalURL();
          const lformId = "41600";
          let formRecord = {
            "draft": false,
            "formFieldValues": [
              {
                "name": "FullName",
                "value": $(formId).find('input[name=name]').val()
              },
              {
                "name": "Email",
                "value": $(formId).find('input[name=email]').val()
              },
              {
                "name": "Phone",
                "value": $(formId).find('#phone').val()
              },
              {
                "name": "Property",
                "value": $(formId).find('select[name=property]').val()
              },
              {
                "name": "Comments",
                "value": $(formId).find('textarea[name=textarea]').val()
              }
            ]
          };
          const query = `
        mutation createFormRecord($formId:Long!, $formRecord:InputFormRecord!) {
          createFormFormRecord(formId: $formId, formRecord: $formRecord){
            id
            draft
          }
        }
        `;
          const variables = {
            formId: lformId,
            formRecord: formRecord

          }
          const options = {
            method: "post",
            headers: {
              "Content-Type": "application/json",
              authorization: `Basic aW5mb0BtaXNrLm9yZy5zYTpUcmFmZmljMTIzCg==`
            },
            body: JSON.stringify({query, variables})
          };

          fetch(`${DOMAIN}/o/graphql`, options)
            .then(res => res.json())
            .then(function(){
              var fields = $(formId).data('formValidation').getOptions().fields,
                $parent, $icon;
              for (var field in fields) {
                $parent = $('[name="' + field + '"]').parents('.form-group');
              }
              // Then reset the form
              $('.alert-success').addClass('in');
              $(formId).data('formValidation').resetForm(true);
              $("input[type=text], textarea").val("");
              $('.file-input-name').hide();
              $('.thanks').show();
              $(formId).removeClass('loading');
              $(formId).find('.loader').remove();
              setTimeout(function () {
                $('.thanks').hide();
                $(formId).data('formValidation').resetForm(true);
              }, 5000);
            });
          /***************************/
          /*$.ajax({
            type: 'post',
            url: ' ',
            data: $(formId).serialize(),
            success: function () {
              setTimeout(function () {
                var fields = $(formId).data('formValidation').getOptions().fields,
                  $parent, $icon;
                for (var field in fields) {
                  $parent = $('[name="' + field + '"]').parents('.form-group');
                }
                // Then reset the form
                $('.alert-success').addClass('in');
                $(formId).data('formValidation').resetForm(true);
                $("input[type=text], textarea").val("");
                $('.file-input-name').hide();
                $('.thanks').show();
                $(formId).removeClass('loading');
                $(formId).find('.loader').remove();
                setTimeout(function () {
                  $('.thanks').hide();
                  $(formId).data('formValidation').resetForm(true);
                }, 5000);
              }, 1500);
            }
          });*/
          /***************************/
        }
      });
    $contactUs
      .find('.filter-option-inner-inner').click(function(){
      console.log("asd");
      $(".selectpicker-2").selectpicker('toggle')
    })
  }

})

if($('#phone').length > 0){
  var input = document.querySelector("#phone");
  var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
  var errorMsg = document.querySelector("#error-msg");
  var validMsg = document.querySelector("#valid-msg");

  var iti = window.intlTelInput(input, {
    nationalMode: false,
    separateDialCode: true,
    formatOnDisplay: true,
    initialCountry: "sa",
    utilsScript: "/assets/scripts/utils.js",
    autoPlaceholder: "aggressive",
    container: false
  });

  var reset = function() {
    input.classList.remove("error");
    //errorMsg.innerHTML = " ";
    //errorMsg.classList.add("hide");
  };

// on blur: validate
  input.addEventListener('blur', function() {
    reset();
    if (input.value.trim()) {
      if (!iti.isValidNumber()) {
        input.classList.add("error");
        var errorCode = iti.getValidationError();
        //errorMsg.innerHTML = errorMap[errorCode];
        //errorMsg.classList.remove("hide");
      }
    }
  });

// on keyup / change flag: reset
  input.addEventListener('change', reset);
  input.addEventListener('keyup', reset);
}
