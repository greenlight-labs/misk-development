﻿if (document.body.contains(document.getElementById('contact_map'))) {
    var marker;
    var map;
    var image = '/assets/images/map-pin.png';
    function initialize() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap',
            disableDefaultUI: true,
            gestureHandling: 'cooperative',
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("contact_map"), mapOptions);
        map.setTilt(45);
        // Multiple Markers
        var markers = [
            ['Misk City', 24.700312, 46.556772]
        ];
        var marker, i;
        for (i = 0; i < markers.length; i++) {
            var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                url: 'https://www.google.com/maps/dir//Exit+37%2c+Western+Ring+Rd%2c+Riyadh+Saudi+Arabia/%4024.6965295%2c46.5689786%2c16.08z/data=%214m9%214m8%211m0%211m5%211m1%211s0x3e2f1e247bd85343:0x2649472410525275%212m2%211d46.5689287%212d24.699549%213e0',
                map: map,
                icon: image,
                draggable: false,
                animation: google.maps.Animation.DROP,
                title: markers[i][0]
            });
            google.maps.event.addListener(marker, 'click', function() {
                window.open(
                    marker.url,
                    '_blank' // <- This is what makes it open in a new window.
                );
            });
            map.fitBounds(bounds);
        }
        var boundsListener = google.maps.event.addListener((map), 'idle', function(event) {
            this.setZoom(15);
            google.maps.event.removeListener(boundsListener);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
}





