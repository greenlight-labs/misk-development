var
    scroll,
    heroTextSlider = $('.homeBanner'),
    //heroImageSlider = $('.homeImgBanner'),
    circleInstances = {},
    innovationSlider = $('.innovation-slider'),
    iconSlider = $('.icon-slider'),
    socialMediaImgSlider = $('.social-media-img-slider-section'),
    downloadPdfSlider = $('.download-pdf-slider-section'),
    latestEventsSlider = $('.latest-events-slider'),
    latestNewsSlider = $('.latest-news-slider'),
    innovationTextSlider = $('.innovation-text-slider'),
    creativityTextSlider = $('.creativity-text-slider'),
    districtsSlider = $('.districts-slider'),
    $districtsPinClick = $('.districts-pin-click[data-id]'),
    assetsSlider = $('.assets-slider'),
    $assetsPinClick = $('.assets-pin-click[data-id]'),
    visionImgSlider = $('.vision-img-slider'),
    visionTextSlider = $('.vision-text-slider'),
    marketTextSlider = $('.marketplace-text-slider'),
    LiveIntegratedSlider = $('.live-integrated-slider'),
    mobileDistrictsImageSlider = $('.m-districts-image-slider'),
    mobileDistrictsTextSlider = $('.m-districts-text-slider'),
    districtsSelect = $('.districts-select'),
    mobileAssetsImageSlider = $('.m-assets-image-slider'),
    mobileAssetsTextSlider = $('.m-assets-text-slider'),
    assetsSelect = $('.assets-select'),
    $contactUs = $('#contactUs'),
    emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    letters = /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/,
    slideWrapper = $(".main-slider"),
    iframes = slideWrapper.find('.embed-player'),
    lazyImages = slideWrapper.find('.slide-image'),
    lazyCounter = 0,
    storiesLongImg = $('.stories-long-image-slider'),
    storiesThumbnail = $('.stories-thumbnail-slider'),
    storiesContentSlider = $('.stories-content-slider'),
    innovationExploreSlider = $(".innovation-explore-slider"),
    relatedNewsSlider = $(".related-news-slider"),
    marketplaceSlider = $(".marketplace-slider"),
    searchBtnClick = $('.search-btn'),
    filterBtnClick = $('.filter-buttons'),
    albumSliderMain = $('.album-slider-main'),
    albumSliderThumb = $('.album-slider-thmb'),
    residencySliderMain = $('.residence-slider-main'),
    residenceySliderThumb = $('.residence-slider-thumb'),
    companyIconSliderDine = $('.company-icon-slider-dine'),
    mainNavigationTl = gsap.timeline({paused: true});


function animateElements() {
    if ($(window).width() > 1199.98){
        if ($('.animate').length > 0) {
            $('.animate').bind('inview', function (event, isInView) {
                if (isInView) {
                    var animate = $(this).attr('data-animation');
                    var speedDuration = $(this).attr('data-duration');
                    var $t = $(this);
                    setTimeout(function () {
                        $t.addClass(animate + ' animated');
                    }, speedDuration);
                }
            });
        }
    }
}

function homeTextSlider() {
    if (heroTextSlider.length > 0) {
        var $slick_carousel = heroTextSlider;
        var sliderAnimationSpeed = 10000;
        $slick_carousel.on('init', function (event, slick) {
            if ($("#circle-border-container0").find("svg").length > 0) {
                $("#circle-border-container0").html("");
            }
            var bar = new ProgressBar.Circle('#circle-border-container0', {
                strokeWidth: 6,
                easing: 'easeInOut',
                duration: sliderAnimationSpeed,
                color: $(slick.$slides[0]).find(".pbar-color").css("background-color"),
                trailColor: '#ffffff',
                trailWidth: 6,
                svgStyle: null
            });
            circleInstances['circle-border-container0'] = bar;
            bar.animate(0);
            circleInstances['circle-border-container0'].animate(1.0);
        });
        $slick_carousel.slick({
            autoplay: true,
            autoplaySpeed: sliderAnimationSpeed,
            fade: true,
            infinite: true,
            dots: true,
            swipe: false,
            rtl: true,
            pauseOnFocus: false,
            lazyLoad: 'ondemand',
            focusOnSelect: true,
            asNavFor: slideWrapper,
            customPaging: function (slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                var circleElem = '<span id="circle-border-container' + i + '" class="cirle-border"></span><a><img src="' + thumb + '" alt=""></a>';
                return circleElem;
            },
            arrows: true,
            prevArrow: '<a href="javascript:" class="right-arrow-slider"><i class="icon-right-arrow"></i></a>',
            nextArrow: '<a href="javascript:" class="left-arrow-slider"><i class="icon-left-arrow"></i></a>',
            pauseOnHover: false,
            responsive: [{
                breakpoint: 767.98,
                settings: {
                    fade: false,
                    swipe: true,
                    arrows: true,
                    prevArrow: '<a href="javascript:" class="left-arrow-slider"><i class="icon-right-arrow"></i></a>',
                    nextArrow: '<a href="javascript:" class="right-arrow-slider"><i class="icon-left-arrow"></i></a>'
                }
            }]
        })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if ($("#circle-border-container" + nextSlide).find("svg").length > 0) {
                    $("#circle-border-container" + nextSlide).html("");
                }
                var bar = new ProgressBar.Circle('#circle-border-container' + nextSlide, {
                    strokeWidth: 6,
                    easing: 'easeInOut',
                    duration: sliderAnimationSpeed,
                    color: $(slick.$slides[nextSlide]).find(".pbar-color").css("background-color"),
                    trailColor: '#ffffff',
                    trailWidth: 6,
                    svgStyle: null
                });
                circleInstances['circle-border-container' + nextSlide] = bar;
                bar.animate(nextSlide);
                circleInstances['circle-border-container' + nextSlide].animate(1.0);
                $('.homeBanner .animate').off('inview');
                var currentElem = slick.$slides[nextSlide], $currentSlideElem = $(currentElem);
                var $elem = $currentSlideElem.find('.banner-animate').children();
                $($elem).each(function (i, v) {
                    if ($(v).hasClass('animated')) {
                        $(v).removeClass('fadeInUp animated');
                        $(v).removeClass('fadeInLeft animated');
                        $(v).removeClass('fadeInRight animated');
                        $(v).removeClass('fadeInDown animated');
                        $(v).removeClass('maskTextUp animated');
                    }
                    $(v).addClass('animate');
                });
                $currentSlideElem.find('.animate').each(function () {
                    $(this).bind('inview', function (event, isInView) {
                        if (isInView) {
                            var animate = $(this).attr('data-animation');
                            var speedDuration = $(this).attr('data-duration');
                            var $t = $(this);
                            setTimeout(function () {
                                $t.addClass(animate + ' animated');
                            }, speedDuration);
                        }
                    });
                });
                $('.homeBanner .animate').trigger('inview', [true]);
            });
    }
}

function homeImageBanner() {
    /*if (heroImageSlider.length > 0) {
        heroImageSlider.slick({
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            arrows: false,
            swipe: false,
            pauseOnFocus: false,
            lazyLoad: 'ondemand',
            asNavFor: heroTextSlider
        });
    }*/
// POST commands to YouTube or Vimeo API
    function postMessageToPlayer(player, command) {
        if (player == null || command == null) return;
        player.contentWindow.postMessage(JSON.stringify(command), "*");
    }
// When the slide is changing
    function playPauseVideo(slick, control) {
        var currentSlide, slideType, startTime, player, video;

        currentSlide = slick.find(".slick-current");
        slideType = currentSlide.attr("class").split(" ")[1];
        player = currentSlide.find("iframe").get(0);
        startTime = currentSlide.data("video-start");

        if (slideType === "vimeo") {
            switch (control) {
                case "play":
                    if ((startTime != null && startTime > 0) && !currentSlide.hasClass('started')) {
                        currentSlide.addClass('started');
                        postMessageToPlayer(player, {
                            "method": "setCurrentTime",
                            "value": startTime
                        });
                    }
                    postMessageToPlayer(player, {
                        "method": "play",
                        "value": 1
                    });
                    break;
                case "pause":
                    postMessageToPlayer(player, {
                        "method": "pause",
                        "value": 1
                    });
                    break;
            }
        } else if (slideType === "youtube") {
            switch (control) {
                case "play":
                    postMessageToPlayer(player, {
                        "event": "command",
                        "func": "mute"
                    });
                    postMessageToPlayer(player, {
                        "event": "command",
                        "func": "playVideo"
                    });
                    break;
                case "pause":
                    postMessageToPlayer(player, {
                        "event": "command",
                        "func": "pauseVideo"
                    });
                    break;
            }
        } else if (slideType === "video") {
            video = currentSlide.children("video").get(0);
            if (video != null) {
                if (control === "play") {
                    video.play();
                } else {
                    video.pause();
                }
            }
        }
    }
// Resize player
    function resizePlayer(iframes, ratio) {
        if (!iframes[0]) return;
        var win = $(".main-slider"),
            width = win.width(),
            playerWidth,
            height = win.height(),
            playerHeight,
            ratio = ratio || 16 / 9;

        iframes.each(function () {
            var current = $(this);
            if (width / ratio < height) {
                playerWidth = Math.ceil(height * ratio);
                current.width(playerWidth).height(height).css({
                    left: (width - playerWidth) / 2,
                    top: 0
                });
            } else {
                playerHeight = Math.ceil(width / ratio);
                current.width(width).height(playerHeight).css({
                    left: 0,
                    top: (height - playerHeight) / 2
                });
            }
        });
    }
// DOM Ready
    slideWrapper.on("init", function (slick) {
        slick = $(slick.currentTarget);
        setTimeout(function () {
            playPauseVideo(slick, "play");
        }, 1000);
        resizePlayer(iframes, 16 / 9);
    });
    slideWrapper.on("beforeChange", function (event, slick) {
        slick = $(slick.$slider);
        playPauseVideo(slick, "pause");
    });
    slideWrapper.on("afterChange", function (event, slick) {
        slick = $(slick.$slider);
        playPauseVideo(slick, "play");
    });
    slideWrapper.on("lazyLoaded", function (event, slick, image, imageSource) {
        lazyCounter++;
        if (lazyCounter === lazyImages.length) {
            lazyImages.addClass('show');
            // slideWrapper.slick("slickPlay");
        }
    });
    //start the slider
    slideWrapper.slick({
        // fade:true,
        autoplaySpeed: 4000,
        cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)",
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        arrows: false,
        swipe: false,
        rtl: true,
        pauseOnFocus: false,
        lazyLoad: 'ondemand',
        asNavFor: heroTextSlider
    });
    var videoData = $('.video-source');
    var videoMobileData = $('.video-source-mobile');
    var videoSource = videoData.data("video-src");
    var posterSource = videoData.data("poster-src");
    var videoMobileSource = videoMobileData.data("video-src-mobile");
    var posterMobileSource = videoMobileData.data("poster-src-mobile");
    if ($(window).width() > 767.98) {
        $('.banner-slide-video source').attr('src', videoSource)
        $('.banner-slide-video').attr('poster', posterSource)
    } else {
        $('.banner-slide-video source').attr('src', videoMobileSource)
        $('.banner-slide-video').attr('poster', posterMobileSource)
    }
// Resize event
    $(window).on("resize.slickVideoPlayer", function () {
        resizePlayer(iframes, 16 / 9);
    });
}

/* Vision Image Slider */
function visionImgSliderFunc(){
    if (visionImgSlider.length > 0) {
        var $status = $('.pagingInfo');
        visionImgSlider.on('init reInit', function (event, slick) {
            $status.text('1' + ' / ' + slick.slideCount);
            $('.VisionTextArrow .portLeftArrow').removeClass('slick-disabled');
        });
        visionImgSlider
            .slick({
                dots: false,
                arrows: false,
                pauseOnHover: false,
                slidesToShow: 1,
                variableWidth: true,
                cssEase: 'ease-out',
                rtl: true,
                speed: 500,
                focusOnSelect: true,
                asNavFor: visionTextSlider,
                responsive: [
                    {
                        breakpoint: 1199.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 991.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 767.98,
                        settings: {
                            swipe: true,
                            centerMode: false,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })

        visionTextSlider
            .slick({
                dots: false,
                arrows: true,
                nextArrow: $(".portRightArrow"),
                prevArrow: $(".portLeftArrow"),
                pauseOnHover: false,
                slidesToShow: 1,
                cssEase: 'ease-out',
                rtl: true,
                fade: true,
                speed: 500,
                asNavFor: visionImgSlider,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 1199.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 991.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 767.98,
                        settings: {
                            swipe: true,
                            centerMode: false,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                var i = (nextSlide ? nextSlide : 0) + 1;
                $status.text(i + ' / ' + slick.slideCount);
                if (currentSlide < nextSlide) {
                    $('.next-slide').trigger('click');
                } else {
                    $('.prev-slide').trigger('click');
                }
            })
            .on('afterChange', function (event, slick, currentSlide, nextSlide) {
                setTimeout(function () {
                }, 750);
            });
    }
}
/* Vision Image Slider */

/* Live Integrated Slider */
function LiveIntegratedSliderFunc(){
    if (LiveIntegratedSlider.length > 0) {
        LiveIntegratedSlider
            .slick({
                dots: false,
                slidesToShow: 2,
                infinite: false,
                rtl: true,
                nextArrow: '<a href="javascript:" class="arrow-prev" aria-label="Left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
                prevArrow: '<a href="javascript:" class="arrow-next" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
                responsive: [
                    {breakpoint: 767.98, settings: {swipe: true, slidesToShow: 1}}
                ]
            });
    }
}
/* Live Integrated Slider */


function iePopup() {
    setTimeout(function () {
        if ($("html").hasClass("ie")) {
            $('#iePopup').modal('show');
        }
    }, 2000);
}


/* Page Active Start */
function isURL(str) {
    var path = window.location.pathname;
    if (path.match(str)) {
        return true;
    } else {
        return false;
    }
}

/* Page Active End */


/* Main Navigation Start */
function mainNavigation() {
    mainNavigationTl.to("#mainNavigation", {
        opacity: 1,
        visibility: 'visible',
        duration: 0,
        ease: "linear"
    })
    mainNavigationTl.fromTo(".m-primary-bg", {
        height: '0%',
        ease: "power1.inOut"
    }, {
        height: '100%',
        duration: 0.5,
        ease: "power1.inOut"
    })
    mainNavigationTl.fromTo(".m-secondary-bg", {
        height: '0%',
        ease: "power1.inOut"
    }, {
        height: '100%',
        duration: 0.5,
        ease: "power1.inOut"
    }, '-=0.15')
    mainNavigationTl.fromTo(".m-navigation-body", {
        visibility: 'hidden',
        ease: "power1.inOut"
    }, {
        visibility: 'visible',
        duration: 0.005,
        ease: "power1.inOut"
    })
    mainNavigationTl.fromTo(".navHead", {
        y: "-100%",
        ease: "power1.inOut"
    }, {
        y: "0%",
        duration: 1,
        ease: "power1.inOut"
    }, '-=0.5')

    $(".first-level-menu > .first-child > .firstLink").each(function (index, el) {
        mainNavigationTl.fromTo($(el), {
            y: "100%",
            ease: "power1.inOut"
        }, {
            y: "0%",
            duration: .1,
            ease: "power1.inOut"
        })
    })
    mainNavigationTl.fromTo(".headHeadingAnimate span", {
        y: "100%",
        ease: "power1.inOut"
    }, {
        y: "0%",
        duration: 1,
        ease: "power1.inOut"
    }, '-=1');

    mainNavigationTl.fromTo(".menu-open-click", {
        y: "110%",
        ease: "power1.inOut"
    }, {
        y: "0%",
        duration: 1,
        ease: "power1.inOut"
    }, '-=1')
    mainNavigationTl.fromTo(".left-line", {
        height: "0%",
        ease: "power1.inOut"
    }, {
        height: "100%",
        duration: 1,
        ease: "power1.inOut"
    }, '-=1')
    $(".headBtns li .btn").each(function (index, el) {
        mainNavigationTl.fromTo($(el), {
            y: "102%",
            ease: "power1.inOut"
        }, {
            y: "0%",
            duration: .1,
            ease: "power1.inOut",
        }, '-=.5')
    })
    $(".headFtNav li").each(function (index, el) {
        mainNavigationTl.fromTo($(el), {
            y: "102%",
            ease: "power1.inOut"
        }, {
            y: "0%",
            duration: .15,
            ease: "power1.inOut"
        }, '-=.1')
    })
    $(".headSocialIcons li").each(function (index, el) {
        mainNavigationTl.fromTo($(el), {
            y: "130%",
            ease: "power1.inOut"
        }, {
            y: "0%",
            duration: .15,
            ease: "power1.inOut"
        }, '-=.1')
    })
    $('.mainNavOpen').on('click', function () {
        mainNavigationTl.play();
        $('html').addClass("overflow-hidden")
    })
    $('.mainNavClose').on('click', function () {
        $('.menu-open-click').removeClass('active');
        $('.subMenu').slideUp();
        mainNavigationTl.reverse();
        $('html').removeClass("overflow-hidden")
    })
}

/* Main Navigation End */


/* Play Pause Video */
function playPauseVideo() {
    if ($('#pageVideo').length > 0) {
        var video = document.getElementById('pageVideo');
        var playButton = $('.video-play-btn');
        var pauseButton = $('.video-pause-btn');
        // Event listener for the play/pause button
        playButton.on("click", function (e) {
            e.preventDefault();
            if (video.paused == true) {
                // Play the video
                playButton.css('display', 'none');
                pauseButton.css('display', 'block');
                video.play();

                // Update the button text to 'Pause'
                // playButton.innerHTML = "Pause";
            }
        });

        pauseButton.on("click", function (e) {
            e.preventDefault();
            if (video.paused == false) {
                // Pause the video
                pauseButton.css('display', 'none');
                playButton.css('display', 'block');
                video.pause();
            }
        });
    }
}

/* Play Pause Video */

/* Search Popup */
function searchPopup() {
    if (searchBtnClick.length) {
        searchBtnClick.on('click', function () {
            searchBtnClick.addClass('active');
            $('.search-popup').addClass('active');
        });
        $('.search-close').on('click', function () {
            $('.search-animate').removeClass('translateUp animated');
            setTimeout(function () {
                searchBtnClick.removeClass('active');
                $('.search-popup').removeClass('active');
            }, 500);
        })
    }
    $(".search-popup-btn").on("click", function (){
        $("#searchBarInput").submit();
    });
}

/* Search Popup */

/* Search Filter Buttons*/
function searchFilterBtn() {
    filterBtnClick.on('click', function() {
        if(filterBtnClick.hasClass("active")){
            filterBtnClick.removeClass("active");
            $(this).addClass('active').siblings().removeClass('active');
        }else{

        }
    });
}
/* Search Filter Buttons*/

/* Icon Slider */
function iconSliderFunc() {
    if (iconSlider.length > 0) {
        iconSlider
            .slick({
                infinite: true,
                dots: false,
                arrows: true,
                prevArrow: '<a href="javascript:" class="arrow-prev" aria-label="Left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
                nextArrow: '<a href="javascript:" class="arrow-next" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
                slidesToShow: 3,
                slidesToScroll: 1,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
    }
}

/* Icon Slider */

/* Social Media Slider */
function socialMediaSlider() {
    if (socialMediaImgSlider.length > 0) {
        socialMediaImgSlider
            .slick({
                infinite: true,
                dots: false,
                arrows: false,
                centerMode: true,
                // variableWidth: true,
                centerPadding: '10%',
                slidesToShow: 3,
                slidesToScroll: 1,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 1025,
                        settings: {
                            swipe: true,
                            centerMode: true,
                            variableWidth: true,
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
    }
}

/* Social Media Slider */

/* Download Pdf */
function downloadSlider() {
    if (downloadPdfSlider.length > 0) {
        downloadPdfSlider
            .slick({
                infinite: true,
                dots: false,
                arrows: false,
                variableWidth: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 1367,
                        settings: {
                            swipe: true,
                            centerMode: true,
                            variableWidth: false,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 1025,
                        settings: {
                            swipe: true,
                            centerMode: true,
                            variableWidth: false,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 575.98,
                        settings: {
                            swipe: true,
                            centerMode: true,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
    }
}

/* Download Pdf */

/* Latest Event Slider */
function eventSlider() {
    if (latestEventsSlider.length > 0) {
        latestEventsSlider
            .slick({
                dots: false,
                arrows: true,
                prevArrow: '<a href="javascript:" class="latest-events-arrow-prev" aria-label="Left Arrow"><i class="icon-left-arrow"></i></a>',
                nextArrow: '<a href="javascript:" class="latest-events-arrow-next" aria-label="right Arrow"><i class="icon-right-arrow"></i></a>',
                pauseOnHover: false,
                centerMode: true,
                centerPadding: 0,
                slidesToShow: 1,
                variableWidth: true,
                cssEase: 'ease-out',
                speed: 500,
                focusOnSelect: true,
                swipe: false,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 1199.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 991.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 767.98,
                        settings: {
                            swipe: true,
                            centerMode: true,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 575.98,
                        settings: {
                            swipe: true,
                            centerMode: true,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })
    }
}

/* Latest Event Slider */

/* Latest News Slider */
function latestNews() {
    if (latestNewsSlider.length > 0) {
        latestNewsSlider
            .slick({
                dots: false,
                arrows: true,
                nextArrow: '<a href="javascript:" class="latest-news-arrow-prev" aria-label="Left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
                prevArrow: '<a href="javascript:" class="latest-news-arrow-next" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
                pauseOnHover: false,
                centerMode: true,
                centerPadding: 0,
                slidesToShow: 1,
                variableWidth: true,
                cssEase: 'ease-out',
                speed: 500,
                focusOnSelect: true,
                swipe: false,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 1199.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 991.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 767.98,
                        settings: {
                            swipe: true,
                            centerMode: true,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 575.98,
                        settings: {
                            swipe: true,
                            centerMode: false,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })
    }
}

/* Latest News Slider */

/* Innovation Slider */
function innovationSliderFunc() {
    if (innovationSlider.length > 0) {
        innovationSlider
            .slick({
                dots: false,
                arrows: true,
                nextArrow: '<a href="javascript:" class="arrow-next" aria-label="left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
                prevArrow: '<a href="javascript:" class="arrow-prev" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
                pauseOnHover: false,
                centerMode: true,
                centerPadding: 0,
                slidesToShow: 1,
                variableWidth: true,
                cssEase: 'ease-out',
                speed: 500,
                focusOnSelect: true,
                swipe: false,
                rtl: true,
                asNavFor: innovationTextSlider,
                responsive: [
                    {
                        breakpoint: 1199.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 991.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 767.98,
                        settings: {
                            swipe: true,
                            centerMode: false,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })
    }
}

/* Innovation Slider */

/* Innovation Text Slider */
function innovationTextSliderFunc() {
    if (innovationTextSlider.length > 0) {
        innovationTextSlider
            .slick({
                dots: false,
                arrows: false,
                pauseOnHover: false,
                centerMode: true,
                centerPadding: 0,
                slidesToShow: 1,
                cssEase: 'ease-out',
                speed: 500,
                variableWidth: true,
                swipe: false,
                focusOnSelect: true,
                asNavFor: innovationSlider,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 767.98,
                        settings: {
                            centerMode: false,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })
    }
}

/* Innovation Text Slider */

/* Creativity Text Slider */
function creativityTextSliderFunc() {
    if (creativityTextSlider.length > 0) {
        var $status = $('.pagingInfo');
        creativityTextSlider
            .on('init reInit', function (event, slick, currentSlide, nextSlide) {
                var i = (currentSlide ? currentSlide : 0) + 1;
                $status.text(i + '/' + slick.slideCount);
                var port_title = $('.creative-box').eq(0).attr('data-color');
                $('.portTextArrow').addClass(port_title);
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {

                creativityTextSlider.find('.animate').off('inview');
                var currentElem = slick.$slides[nextSlide], $currentSlideElem = $(currentElem);
                var $elem = $currentSlideElem.find('.text-animate').children();
                $($elem).each(function (i, v) {
                    if ($(v).hasClass('animated')) {
                        $(v).removeClass('fadeInUp animated');
                        $(v).removeClass('fadeInLeft animated');
                        $(v).removeClass('fadeInRight animated');
                        $(v).removeClass('fadeInDown animated');
                        $(v).removeClass('maskTextUp animated');
                    }
                    $(v).addClass('animate');
                });
                $currentSlideElem.find('.animate').each(function () {
                    $(this).bind('inview', function (event, isInView) {
                        if (isInView) {
                            var animate = $(this).attr('data-animation');
                            var speedDuration = $(this).attr('data-duration');
                            var $t = $(this);
                            setTimeout(function () {
                                $t.addClass(animate + ' animated');
                            }, speedDuration);
                        }
                    });
                });
                creativityTextSlider.find('.animate').trigger('inview', [true]);
            })
        creativityTextSlider
            .slick({
                dots: false,
                fade: true,
                swipe: false,
                infinite: false,
                rtl: true,
                nextArrow: $(".portRightArrow"),
                prevArrow: $(".portLeftArrow"),
                responsive: [
                    {
                        breakpoint: 1199.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 991.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 767.98,
                        settings: {
                            swipe: true
                        }
                    }
                ]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.portTextArrow').addClass("disabled");
                var i = (nextSlide ? nextSlide : 0) + 1;
                $status.text(i + '/' + slick.slideCount);
                var port_title = $('.creative-box').eq(nextSlide).attr('data-color');
                var prev_title = $('.creative-box').eq(currentSlide).attr('data-color');
                $('.portTextArrow').removeClass(prev_title);
                $('.portTextArrow').addClass(port_title);
                if (currentSlide < nextSlide) {
                    $('.next-slide').trigger('click');
                } else {
                    $('.prev-slide').trigger('click');
                }
            })
            .on('afterChange', function (event, slick, currentSlide, nextSlide) {
                setTimeout(function () {
                    $('.portTextArrow').removeClass("disabled");
                }, 750);
            });
    }
}

/* Creativity Text Slider */

/* Contact Us Form */
function contactUsForm() {
    if ($contactUs.length > 0) {
        $contactUs
            .find('[name="organisation"]').selectpicker()
            .change(function (e) {
                $('#contactUs').formValidation('revalidateField', 'property');
            })
            .end()
            .formValidation({
                framework: 'bootstrap',
                excluded: ':disabled',
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: ' '
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: ' '
                            }, regexp: {
                                regexp: emailRegex,
                                message: ' '
                            }
                        }
                    },
                    textarea: {
                        validators: {
                            notEmpty: {
                                message: ' '
                            }
                        }
                    },
                    captcha: {
                        validators: {
                            notEmpty: {
                                message: ' '
                            },
                        }
                    },
                    property: {
                        validators: {
                            notEmpty: {
                                message: ' '
                            }
                        }
                    }
                },
                onSuccess: function (e) {
                    e.preventDefault();
                    var $form = $(e.target),
                        formId = '#' + $form[0].id;
                    $(formId).addClass('loading').append('<div class="loader"></div>');

                    $.ajax({
                        type: 'post',
                        url: '/en/post.aspx',
                        data: $(formId).serialize(),
                        success: function (data) {
                            if (data == "success") {
                                setTimeout(function () {
                                    var fields = $(formId).data('formValidation').getOptions().fields,
                                        $parent, $icon;
                                    for (var field in fields) {
                                        $parent = $('[name="' + field + '"]').parents('.form-group');
                                    }
                                    // Then reset the form
                                    $('.alert-success').addClass('in');
                                    $(formId).data('formValidation').resetForm(true);
                                    $("input[type=text], textarea").val("");
                                    $('.file-input-name').hide();
                                    $('.thanks').show();
                                    $(formId).removeClass('loading');
                                    $(formId).find('.loader').remove();
                                    setTimeout(function () {
                                        $('.thanks').hide();

                                        $(formId).data('formValidation').resetForm(true);

                                        refresh();
                                    }, 2000);
                                }, 1500);
                            } else if (data == "invalid") {
                                $(formId).find('.loader').remove();
                                $(formId).removeClass('loading');
                                debugger;
                                $("input[name=captcha]").val("");
                                $("input[name=captcha]").parent().removeClass('has-success').addClass('has-error');
                                $("input[name=captcha]").parent().parent().removeClass('has-success').addClass('has-error');
                                setTimeout(function () {

                                    refresh();
                                }, 4000);
                            } else if (data == "fail") {
                                $(formId).find('.loader').remove();
                                $(formId).removeClass('loading');
                                $("input[name=captcha]").val("");
                                $("input[name=captcha]").parent().removeClass('has-success').addClass('has-error');
                                setTimeout(function () {
                                    $(".errorCaptcha").hide();
                                    $(".error").hide();
                                    refresh();
                                }, 4000);
                            }
                        },
                        error: function (e) {
                            $(formId).find('.loader').remove();
                            $(formId).data('formValidation').resetForm(true);
                            refresh();
                        }
                    });
                    //$.ajax({
                    //    type: 'post',
                    //    url: ' ',
                    //    data: $(formId).serialize(),
                    //    success: function () {
                    //        setTimeout(function () {
                    //            var fields = $(formId).data('formValidation').getOptions().fields,
                    //                $parent, $icon;
                    //            for (var field in fields) {
                    //                $parent = $('[name="' + field + '"]').parents('.form-group');
                    //            }
                    //            // Then reset the form
                    //            $('.alert-success').addClass('in');
                    //            $(formId).data('formValidation').resetForm(true);
                    //            $("input[type=text], textarea").val("");
                    //            $('.file-input-name').hide();
                    //            $('.thanks').show();
                    //            $(formId).removeClass('loading');
                    //            $(formId).find('.loader').remove();
                    //            setTimeout(function () {
                    //                $('.thanks').hide();
                    //                $(formId).data('formValidation').resetForm(true);
                    //            }, 5000);
                    //        }, 1500);
                    //    }
                    //});
                }
            });

        function refresh() {
            $("#imgCaptcha").attr("src", "/en/FormCaptcha.aspx?" + new Date().getMilliseconds());
        }

        $('.captcha').click(function () {
            refresh();
        });
        refresh();
    }
}

/* Contact Us Form */

/* SVG Swiper Slider */
function svgSwiperSlider() {
    var duration = 550, epsilon = (1000 / 60 / duration) / 4,
        firstCustomMinaAnimation = bezier(.42, .03, .77, .63, epsilon),
        secondCustomMinaAnimation = bezier(.27, .5, .6, .99, epsilon), animating = false;
    $('.cd-slider-wrapper').each(function () {
        initSlider($(this));
    });

    function initSlider(sliderWrapper) {
        var slider = sliderWrapper.find('.cd-slider'), sliderNav = sliderWrapper.find('.cd-slider-navigation'),
            sliderControls = sliderWrapper.find('.cd-slider-controls').find('li');
        var pathArray = [];
        pathArray[0] = slider.data('step1');
        pathArray[1] = slider.data('step4');
        pathArray[2] = slider.data('step2');
        pathArray[3] = slider.data('step5');
        pathArray[4] = slider.data('step3');
        pathArray[5] = slider.data('step6');
        sliderNav.on('click', '.next-slide', function (event) {
            event.preventDefault();
            nextSlide(slider, sliderControls, pathArray);
        });
        sliderNav.on('click', '.prev-slide', function (event) {
            event.preventDefault();
            prevSlide(slider, sliderControls, pathArray);
        });
        slider.on('swipeleft', function (event) {
            nextSlide(slider, sliderControls, pathArray);
        });
        slider.on('swiperight', function (event) {
            prevSlide(slider, sliderControls, pathArray);
        });
        sliderControls.on('click', function (event) {
            event.preventDefault();
            var selectedItem = $(this);
            if (!selectedItem.hasClass('selected')) {
                var selectedSlidePosition = selectedItem.index(),
                    selectedSlide = slider.children('li').eq(selectedSlidePosition),
                    visibleSlide = retrieveVisibleSlide(slider), visibleSlidePosition = visibleSlide.index(),
                    direction = '';
                direction = (visibleSlidePosition < selectedSlidePosition) ? 'next' : 'prev';
                updateSlide(visibleSlide, selectedSlide, direction, sliderControls, pathArray);
            }
        });
    }

    function retrieveVisibleSlide(slider, sliderControls, pathArray) {
        return slider.find('.visible');
    }

    function nextSlide(slider, sliderControls, pathArray) {
        var visibleSlide = retrieveVisibleSlide(slider),
            nextSlide = (visibleSlide.next('li').length > 0) ? visibleSlide.next('li') : slider.find('li').eq(0);
        updateSlide(visibleSlide, nextSlide, 'next', sliderControls, pathArray);
    }

    function prevSlide(slider, sliderControls, pathArray) {
        var visibleSlide = retrieveVisibleSlide(slider),
            prevSlide = (visibleSlide.prev('li').length > 0) ? visibleSlide.prev('li') : slider.find('li').last();
        updateSlide(visibleSlide, prevSlide, 'prev', sliderControls, pathArray);
    }

    function updateSlide(oldSlide, newSlide, direction, controls, paths) {
        if (!animating) {
            animating = true;
            var clipPathId = newSlide.find('path').attr('id'), clipPath = Snap('#' + clipPathId);
            if (direction == 'next') {
                var path1 = paths[0], path2 = paths[2], path3 = paths[4];
            } else {
                var path1 = paths[1], path2 = paths[3], path3 = paths[5];
            }
            updateNavSlide(newSlide, controls);
            newSlide.addClass('is-animating');
            clipPath.attr('d', path1).animate({'d': path2}, duration, firstCustomMinaAnimation, function () {
                clipPath.animate({'d': path3}, duration, secondCustomMinaAnimation, function () {
                    oldSlide.removeClass('visible');
                    newSlide.addClass('visible').removeClass('is-animating');
                    animating = false;
                });
            });
        }
    }

    function updateNavSlide(actualSlide, controls) {
        var position = actualSlide.index();
        controls.removeClass('selected').eq(position).addClass('selected');
    }

    function bezier(x1, y1, x2, y2, epsilon) {
        var curveX = function (t) {
            var v = 1 - t;
            return 3 * v * v * t * x1 + 3 * v * t * t * x2 + t * t * t;
        };
        var curveY = function (t) {
            var v = 1 - t;
            return 3 * v * v * t * y1 + 3 * v * t * t * y2 + t * t * t;
        };
        var derivativeCurveX = function (t) {
            var v = 1 - t;
            return 3 * (2 * (t - 1) * t + v * v) * x1 + 3 * (-t * t * t + 2 * v * t) * x2;
        };
        return function (t) {
            var x = t, t0, t1, t2, x2, d2, i;
            for (t2 = x, i = 0; i < 8; i++) {
                x2 = curveX(t2) - x;
                if (Math.abs(x2) < epsilon) return curveY(t2);
                d2 = derivativeCurveX(t2);
                if (Math.abs(d2) < 1e-6) break;
                t2 = t2 - x2 / d2;
            }
            t0 = 0, t1 = 1, t2 = x;
            if (t2 < t0) return curveY(t0);
            if (t2 > t1) return curveY(t1);
            while (t0 < t1) {
                x2 = curveX(t2);
                if (Math.abs(x2 - x) < epsilon) return curveY(t2);
                if (x > x2) t0 = t2; else t1 = t2;
                t2 = (t1 - t0) * .5 + t0;
            }
            return curveY(t2);
        };
    }

    function elementInViewport(el) {
        var top = el.offsetTop;
        var left = el.offsetLeft;
        var width = el.offsetWidth;
        var height = el.offsetHeight;
        while (el.offsetParent) {
            el = el.offsetParent;
            top += el.offsetTop;
            left += el.offsetLeft;
        }
        return (top < (window.pageYOffset + window.innerHeight) && left < (window.pageXOffset + window.innerWidth) && (top + height) > window.pageYOffset && (left + width) > window.pageXOffset);
    }
}

/* SVG Swiper Slider */

/* Districts Slider */
function districtsSliderFunc() {
    if (districtsSlider.length > 0) {
        districtsSlider
            .slick({
                dots: false,
                infinite: false,
                fade: true,
                arrows: false,
                cssEase: 'ease-out',
                speed: 500,
                swipe: false,
                pauseOnFocus: false,
                rtl: true,
                lazyLoad: 'ondemand',
            })
    }
}

/* Districts Slider */

/* Districts Pin Click */
function districtsPinClickFunc() {
    if ($districtsPinClick.length > 0) {
        $districtsPinClick.on("click", ".pin-icon", function () {
            var thiss = $(this).closest(".pin-click");
            var slideno = thiss.data('id');
            if (!thiss.hasClass("active")) {
                $districtsPinClick.removeClass("active");
                thiss.addClass("active");
                $(".map-close").hide();
                $(".map-info-window").slideUp();
                thiss.find(".map-info-window").slideDown(250);
                setTimeout(function () {
                    thiss.find(".map-close").show();
                }, 250);
                districtsSlider.slick('slickGoTo', slideno);
            }
        });
        $districtsPinClick.on("click", ".map-close", function () {
            $(this).hide();
            $districtsPinClick.removeClass("active");
            $(".map-info-window").slideUp();
            districtsSlider.slick('slickGoTo', 0);
        })
    }
}

/* Districts Pin Click */

/* Assets Slider */
function assetsSliderFunc() {
    if (assetsSlider.length > 0) {
        assetsSlider
            .slick({
                dots: false,
                infinite: false,
                cssEase: 'ease-out',
                speed: 500,
                fade: true,
                arrows: false,
                swipe: false,
                pauseOnFocus: false,
                rtl: true,
                lazyLoad: 'ondemand',
            })
    }
}

/* Assets Slider */

/* Assets Pin */
function assetsPinFunc() {
    if ($assetsPinClick.length > 0) {
        $assetsPinClick.on("click", ".pin-icon", function () {
            var thiss = $(this).closest(".pin-click");
            var slideno = thiss.data('id');
            if (!thiss.hasClass("active")) {
                $assetsPinClick.removeClass("active");
                thiss.addClass("active");
                $(".map-close").hide();
                $(".map-info-window").slideUp();
                thiss.find(".map-info-window").slideDown(250);
                setTimeout(function () {
                    thiss.find(".map-close").show();
                }, 250);
                thiss.find(".map-info-window").slideDown();
                assetsSlider.slick('slickGoTo', slideno);
            }
        });
        $assetsPinClick.on("click", ".map-close", function () {
            $(this).hide();
            $assetsPinClick.removeClass("active");
            $(".map-close").hide();
            $(".map-info-window").slideUp();
            assetsSlider.slick('slickGoTo', 0);
        })
    }
}

/* Assets Pin */

/* Mobile Districts Image Slider */
function mDistrictsImgSlider() {
    if (mobileDistrictsImageSlider.length > 0) {
        mobileDistrictsImageSlider
            .slick({
                dots: false,
                infinite: false,
                fade: true,
                arrows: false,
                cssEase: 'ease-out',
                speed: 500,
                swipe: false,
                pauseOnFocus: false,
                lazyLoad: 'ondemand',
                focusOnSelect: true,
                rtl: true,
                asNavFor: mobileDistrictsTextSlider
            })
    }
}

/* Mobile Districts Image Slider */


/* Mobile Districts Text Slider */
function moDistrictsTxtSlider() {
    if (mobileDistrictsTextSlider.length > 0) {
        mobileDistrictsTextSlider
            .slick({
                dots: false,
                infinite: false,
                fade: true,
                arrows: false,
                speed: 500,
                swipe: false,
                pauseOnFocus: false,
                lazyLoad: 'ondemand',
                focusOnSelect: true,
                rtl: true,
                asNavFor: mobileDistrictsImageSlider
            })
    }
}

/* Mobile Districts Text Slider */

/* Districts Select */
function districtsSelectFunc() {
    if (districtsSelect.length > 0) {
        districtsSelect.on('change', 'select.selectpicker', function () {
            var slideno = $(this).find('option:selected').data('id');
            mobileDistrictsTextSlider.slick('slickGoTo', slideno - 0);
        });
    }
}

/* Districts Select */

/* Mobile Assets Image Slider */
function mAssetsImageSlider() {
    if (mobileAssetsImageSlider.length > 0) {
        mobileAssetsImageSlider
            .slick({
                dots: false,
                infinite: false,
                fade: true,
                arrows: false,
                cssEase: 'ease-out',
                speed: 500,
                swipe: false,
                pauseOnFocus: false,
                lazyLoad: 'ondemand',
                focusOnSelect: true,
                rtl: true,
                asNavFor: mobileAssetsTextSlider
            })
    }
}

/* Mobile Assets Image Slider */


/* Mobile Assets Text Slider */
function mAssetsTxtSlider() {
    if (mobileAssetsTextSlider.length > 0) {
        mobileAssetsTextSlider
            .slick({
                dots: false,
                infinite: false,
                fade: true,
                arrows: false,
                speed: 500,
                swipe: false,
                pauseOnFocus: false,
                lazyLoad: 'ondemand',
                focusOnSelect: true,
                rtl: true,
                asNavFor: mobileAssetsImageSlider
            })
    }
}

/* Mobile Assets Text Slider */

/* Assets Select */
function assetsSelectFunc() {
    if (assetsSelect.length > 0) {
        assetsSelect.on('change', 'select.selectpicker', function () {
            var slideno = $(this).find('option:selected').data('id');
            mobileAssetsTextSlider.slick('slickGoTo', slideno - 0);
        });
    }
}

/* Assets Select */

/* Marketplace Slider */
function marketPlaceSliderFunc() {
    if (marketplaceSlider.length > 0) {
        var $status = $('.pagingInfo');
        marketplaceSlider.slick({
            dots: false,
            arrows: false,
            pauseOnHover: false,
            centerMode: true,
            centerPadding: 0,
            slidesToShow: 1,
            slidesToScroll: 1,
            cssEase: 'ease-out',
            speed: 500,
            swipe: false,
            rtl: true,
            responsive: [
                {
                    breakpoint: 1199.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 991.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 767.98,
                    settings: {
                        swipe: true,
                    }
                },
                {
                    breakpoint: 575.98,
                    settings: {
                        swipe: true,
                        centerMode: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        marketTextSlider
            .slick({
                dots: false,
                arrows: true,
                nextArrow: $(".portRightArrow"),
                prevArrow: $(".portLeftArrow"),
                pauseOnHover: false,
                slidesToShow: 1,
                cssEase: 'ease-out',
                fade: true,
                speed: 500,
                asNavFor: marketplaceSlider,
                focusOnSelect: true,
                rtl: true,
                responsive: [
                    {
                        breakpoint: 1199.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 991.98,
                        settings: {
                            swipe: true
                        }
                    },
                    {
                        breakpoint: 767.98,
                        settings: {
                            swipe: true,
                            centerMode: false,
                            variableWidth: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })
            .on('init reInit', function (event, slick, currentSlide, nextSlide) {
                var i = (currentSlide ? currentSlide : 0) + 1;
                $status.text(i + ' / ' + slick.slideCount);
                $('.portTextArrow').addClass(port_title);
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $('.portTextArrow').addClass("disabled");
                var i = (nextSlide ? nextSlide : 0) + 1;
                $status.text(i + ' / ' + slick.slideCount);
                if (currentSlide < nextSlide) {
                    $('.next-slide').trigger('click');
                } else {
                    $('.prev-slide').trigger('click');
                }
            })
            .on('afterChange', function (event, slick, currentSlide, nextSlide) {
                setTimeout(function () {
                    $('.portTextArrow').removeClass("disabled");
                }, 750);
            });
    }
}

/* Marketplace Slider */

/* Innovation Explore Slider */
function innovationExploreSliderFunc() {
    if (innovationExploreSlider.length > 0) {
        innovationExploreSlider.slick({
            dots: false,
            arrows: true,
            nextArrow: '<a href="javascript:" class="arrow-prev" aria-label="Left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
            prevArrow: '<a href="javascript:" class="arrow-next" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
            pauseOnHover: false,
            centerMode: true,
            centerPadding: 0,
            slidesToShow: 1,
            variableWidth: true,
            cssEase: 'ease-out',
            speed: 500,
            focusOnSelect: true,
            swipe: false,
            rtl: true,
            responsive: [
                {
                    breakpoint: 1199.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 991.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 767.98,
                    settings: {
                        swipe: true,
                    }
                },
                {
                    breakpoint: 575.98,
                    settings: {
                        swipe: true,
                        centerMode: false,
                        variableWidth: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}

/* Innovation Explore Slider */

/* Related News Slider */
function relatedNewsSliderFunc() {
    if (relatedNewsSlider.length > 0) {
        relatedNewsSlider.slick({
            dots: false,
            arrows: true,
            prevArrow: '<a href="javascript:" class="arrow-next" aria-label="Left Arrow"><i class="icon-arrow-arrow-right"></i></a>',
            nextArrow: '<a href="javascript:" class="arrow-prev" aria-label="right Arrow"><i class="icon-arrow-arrow-left"></i></a>',
            pauseOnHover: false,
            centerMode: true,
            centerPadding: 0,
            slidesToShow: 1,
            variableWidth: true,
            cssEase: 'ease-out',
            speed: 500,
            focusOnSelect: true,
            swipe: false,
            rtl: true,
            responsive: [
                {
                    breakpoint: 1199.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 991.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 767.98,
                    settings: {
                        swipe: true,
                    }
                },
                {
                    breakpoint: 575.98,
                    settings: {
                        swipe: true,
                        centerMode: false,
                        variableWidth: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}

/* Related News Slider */

/* Stories Long Image */
function storiesLongImgFunc() {
    if (storiesLongImg.length > 0) {
        storiesLongImg.slick({
            dots: false,
            arrows: false,
            fade: true,
            slidesToShow: 1,
            cssEase: 'ease-out',
            speed: 500,
            swipe: false,
            rtl: true,
            asNavFor: $('.stories-slider'),
            responsive: [
                {
                    breakpoint: 1199.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 991.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 767.98,
                    settings: {
                        swipe: true,
                    }
                },
                {
                    breakpoint: 575.98,
                    settings: {
                        swipe: true,
                        centerMode: false,
                        variableWidth: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        storiesContentSlider.slick({
            dots: false,
            arrows: false,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            cssEase: 'ease-out',
            speed: 500,
            swipe: false,
            asNavFor: $('.stories-slider'),
            focusOnSelect: true,
            rtl: true,
            responsive: [
                {
                    breakpoint: 1199.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 991.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 767.98,
                    settings: {
                        swipe: true,
                    }
                },
                {
                    breakpoint: 575.98,
                    settings: {
                        swipe: true,
                        centerMode: false,
                        variableWidth: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        storiesThumbnail.slick({
            dots: false,
            arrows: true,
            prevArrow: '<a href="javascript:" class="arrow-prev" aria-label="Left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
            nextArrow: '<a href="javascript:" class="arrow-next" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
            slidesToShow: 3,
            slidesToScroll: 1,
            cssEase: 'ease-out',
            speed: 500,
            swipe: false,
            rtl: true,
            asNavFor: $('.stories-slider'),
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 1199.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 991.98,
                    settings: {
                        swipe: true
                    }
                },
                {
                    breakpoint: 767.98,
                    settings: {
                        swipe: true,
                    }
                },
                {
                    breakpoint: 575.98,
                    settings: {
                        swipe: true,
                        centerMode: false,
                        variableWidth: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}

/* Stories Long Image */

/* Album Slider Main */
function albumSliderMainFunc() {
    if (albumSliderMain.length > 0) {

        albumSliderMain.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '<a href="javascript:" class="arr-prev" aria-label="Left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
            nextArrow: '<a href="javascript:" class="arr-next" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
            infinite: false,
            rtl: true,
            asNavFor: albumSliderThumb
        });

        albumSliderThumb.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: albumSliderMain,
            dots: false,
            rtl: true,
            infinite: false,
            focusOnSelect: true
        });
    }
}

/* Album Slider Main */

/* Residency Slider Main */
var bar = [];
function residencySliderMainFunc() {
    if (residencySliderMain.length > 0) {
        var slideLen = 0;
        var showSlidesLen = 3;
        $(".residence-slider-main .slide").each(function (e){
            slideLen++;
        });
        if(slideLen === 3){
            showSlidesLen = 2;
        }

        residencySliderMain.slick({
            autoplay: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            arrows: true,
            rtl: true,
            prevArrow: '<a href="javascript:" class="res-arrow-prev" aria-label="Left Arrow"><i class="icon-arrow-arrow-left"></i></a>',
            nextArrow: '<a href="javascript:" class="res-arrow-next" aria-label="right Arrow"><i class="icon-arrow-arrow-right"></i></a>',
            asNavFor: residenceySliderThumb
        })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {

                residencySliderMain.find('.animate').off('inview');
                var currentElem = slick.$slides[nextSlide], $currentSlideElem = $(currentElem);
                var $elem = $currentSlideElem.find('.banner-text-animate').children();
                $($elem).each(function (i, v) {
                    if ($(v).hasClass('animated')) {
                        $(v).removeClass('fadeInUp animated');
                        $(v).removeClass('fadeInLeft animated');
                        $(v).removeClass('fadeInRight animated');
                        $(v).removeClass('fadeInDown animated');
                        $(v).removeClass('maskTextUp animated');
                    }
                    $(v).addClass('animate');
                });
                $currentSlideElem.find('.animate').each(function () {
                    $(this).bind('inview', function (event, isInView) {
                        if (isInView) {
                            var animate = $(this).attr('data-animation');
                            var speedDuration = $(this).attr('data-duration');
                            var $t = $(this);
                            setTimeout(function () {
                                $t.addClass(animate + ' animated');
                            }, speedDuration);
                        }
                    });
                });
                residencySliderMain.find('.animate').trigger('inview', [true]);
            });


        var sliderAnimationSpeed2 = 5000;
        residenceySliderThumb.on('init', function (event, slick) {
            var currentElem = slick.$slides[0], $currentSlideElem = $(currentElem);
            var index = 0;

            slick.$slides.each(function(index){
                var progress = "<div id='definitively-progress" + index + "' class='definitively-progress definitively-progress" + index + "'></div>";
                $(this).find(".progress-bar").append(progress);
                var barElem = new ProgressBar.Circle('#definitively-progress'+ index, {
                    strokeWidth: 6,
                    easing: 'easeInOut',
                    duration: sliderAnimationSpeed2,
                    color: "#FF6B63",
                    trailColor: '#ffffff',
                    trailWidth: 6,
                    svgStyle: null
                });
                bar.push(barElem);
            });
            bar[0].animate(1);
        })
            .slick({
                slidesToShow: showSlidesLen,
                slidesToScroll: 1,
                arrows: false,
                rtl: true,
                asNavFor: residencySliderMain,
                dots: false,
                focusOnSelect: true,
                autoplay: true,
                autoplaySpeed: sliderAnimationSpeed2,
                pauseOnHover: false,
                centerMode: true,
                responsive: [
                    {
                        breakpoint: 575.98,
                        settings: {
                            slidesToShow: showSlidesLen - 1,
                        }
                    },
                ]
            })
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                // $(".definitively-progress").remove();
                bar[currentSlide].animate(0);
            })
            .on('afterChange', function (event, slick, currentSlide, nextSlide) {

                index = currentSlide;
                bar[index].animate(1.0);

            });
    }
}

/* Residency Slider Main */


/* Album Listing Filter Dropdown */
function dropDownFilter() {
    var ddlYears = $("#albumYears");
    var currentYear = (new Date()).getFullYear();
    for (var i = 2000; i <= currentYear; i++) {
        var option = $("<option />");
        option.html(i);
        option.val(i);
        ddlYears.append(option);
    }
}

/* Album Listing Filter Dropdown */

/* Company Icon Slider Dine */
function companyIconSliderDineFunc() {
    var shopbtn = $('.shop-btn');
    var shopContent = $('#shop1');
    var dinebtn = $('.dine-btn');
    var dineContent = $('#dine1');
    $(shopbtn).click(function () {
        $(dineContent).removeClass('in active show');
        $(shopContent).addClass('in active show');
        companyIconSliderDine.slick('setPosition');
    })
    $(dinebtn).click(function () {
        $(shopContent).removeClass('in active show');
        $(dineContent).addClass('in active show');
        companyIconSliderDine.slick('setPosition');
    })

    if (companyIconSliderDine.length > 0) {
        companyIconSliderDine.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            infinite: false,
            rtl: true
        });
    }
}

/* Company Icon Slider Dine */

/* Plan a visit page */
function planAVisit(){
    $('.datepicker-input').datepicker({
        format: 'mm/dd/yyyy',
        rtl: true
    });
    $('.timepicker-input').timepicker({
        icons: {
            up: 'icon-up-arrow',
            down: 'icon-down-arrow'
        },
        appendWidgetTo: '.bfh-timepicker'
    });

    var children = 0;
    var adult = 0;
    $('#children-plus-btn').on('click',function(){
        if(children >= 0){
            children = children + 1;
            document.getElementById("childrens").value = children + " Children";
        }
    });
    $('#children-minus-btn').on('click',function(){
        if(children >= 1){
            children = children - 1;
            document.getElementById("childrens").value = children + " Children";
        }
    });
    $('#adult-plus-btn').on('click',function(){
        if(adult >= 0){
            adult = adult + 1;
            document.getElementById("adults").value = adult + " Adult";
        }
    });
    $('#adult-minus-btn').on('click',function(){
        if(adult >= 1){
            adult = adult - 1;
            document.getElementById("adults").value = adult + " Adult";
        }
    });
}
/* Plan a visit page */

/* Getting To Miskcity Page */
function gettingToMiskcity(){
    $('.route-info-div').hide();
    $('.filter-icon-box').on('click',function (){
        $('.direction-filter-sec .filter-icon-box').parent().removeClass('active');
        $(this).parent().addClass('active');

        $map_img_url = $(this).attr('data-type');
        $('.direction-images-sec img').attr('src',$map_img_url);

        $('.route-info-div').hide();
        $route_info = $(this).attr('data-route');

        if($route_info === 'bus'){
            $('.bus-station-route').show();
        }
        if($route_info === 'metro'){
            $('.metro-station-route').show();
        }
        if($route_info === 'bicycle'){
            $('.bicycle-route').show();
        }
        if($route_info === 'footway'){
            $('.foot-way-route').show();
        }

        $('.take-car-route').on('click',function (){
            $('.direction-images-sec img').attr('src','/assets/images/map_car.jpg');
            $('.route-info-div').hide();
            $('.direction-filter-sec .filter-icon-box').parent().removeClass('active');
            $('#car_link').parent().addClass('active');
            $('#car_link_mob').parent().addClass('active');
        });
    });
}
/* Getting To Miskcity Page */

/* Share Buttons */
function shareButtons(){
    if($(".all-share-btn").length > 0){
        $(".all-share-btn").on("click", function(){
            $(this).toggleClass("share-btn-open");
            $(this).next(".share-icons").stop().slideToggle();
        });
    }
}
/* Share Buttons */

/*Add Event To Calender */
function addEventToCalender(){
    window.addeventasync = function(){
        addeventatc.settings({
            appleical  : {show:true, text:"Apple Calendar"},
            google     : {show:true, text:"Google <em>(online)</em>"},
            office365  : {show:true, text:"Office 365 <em>(online)</em>"},
            outlook    : {show:true, text:"Outlook"},
            outlookcom : {show:true, text:"Outlook.com <em>(online)</em>"},
            yahoo      : {show:true, text:"Yahoo <em>(online)</em>"}
        });
    };
}
/*Add Event To Calender */


$(document).ready(function () {

    var pathnamefull = window.location.pathname;
    var hrefdocument = String(window.location.pathname).replace("/ar/", "/en/");
    var pageurl = pathnamefull.replace(pathnamefull, hrefdocument);
    $("a.eng-lang").attr("href", pageurl);


    $('.first-level-menu li, .second-level-menu li, .third-level-menu li').each(function () {
        if (isURL($(this).find('a').attr('href'))) {
            var parent = $(this).find('a').attr('href');
            if (parent.length > 1) {
                $("a[href='" + parent + "']").parent('li').addClass('active');
                if ($("a[href='" + parent + "']").parent('li').parents('ul').hasClass('second-level-menu')) {
                    var parentHref = $("a[href='" + parent + "']").parent('li').parent('ul').parent('li').find('a').attr('href');

                    $(".ft-listing a[href='" + parentHref + "']").parent('li').addClass('active');
                    $("a[href='" + parent + "']").parent('li').parents('ul').parent('.subMenuLinks').parent('.subMenu').parent('li').addClass('active');
                }
            } else {
                if (window.location.pathname == parent) {
                    $("a[href='" + parent + "']").parent('li').addClass('active');

                }

            }
        }
    });
    $('.ftNav ul li').each(function () {
        if (isURL($(this).find('a').attr('href'))) {
            var parent = $(this).find('a').attr('href');
            $(".ftNav li a[href='" + parent + "']").parent('li').addClass('active');
        }
    });
    $('footer ul li').each(function () {
        if (isURL($(this).find('a').attr('href'))) {
            var parent = $(this).find('a').attr('href');
            $("footer ul li a[href='" + parent + "']").parent('li').addClass('active');
        }
    });

    /*if ($('#phone').length > 0) {
        var input = document.querySelector("#phone");
        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
        var errorMsg = document.querySelector("#error-msg");
        var validMsg = document.querySelector("#valid-msg");
        var iti = window.intlTelInput(input, {
            nationalMode: false,
            separateDialCode: true,
            formatOnDisplay: true,
            initialCountry: "sa",
            utilsScript: "/assets/scripts/utils.js",
            autoPlaceholder: "aggressive",
            container: false
        });
        var reset = function () {
            input.classList.remove("error");
        };
        input.addEventListener('blur', function () {
            reset();
            if (input.value.trim()) {
                if (!iti.isValidNumber()) {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                }
            }
        });
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    }*/
    $('.map-tab-btns a').on('click', function () {
        // var dataTarget = $(this).attr("data-target");
        // var dataDescription = $(this).attr("data-description");
        // $(".map-nav-description h4").text(dataTarget);
        // $(".map-nav-description p").text(dataDescription);

        $('.pin-click').removeClass("active");
        $(".map-close").hide();
        $(".map-info-window").slideUp();
        $(".pin-click").removeClass("bounceInDown animated");
        $(".pin-click").each(function () {
            var animate = $(this).attr('data-animation');
            var speedDuration = $(this).attr('data-duration');
            var $t = $(this);
            setTimeout(function () {
                $t.addClass(animate + ' animated');
            }, speedDuration);
        });
        assetsSlider.slick("reinit");
        districtsSlider.slick("reinit");
        $('[data-map-name]').hide();
        $('[data-map-name="' + $(this).data("target") + '"]').show();
        $('.map-tab-btns a').removeClass("active");
        $(this).addClass('active');
    });
    $('.mobile-map-btns a').on('click', function () {
        // var dataTarget = $(this).attr("data-target");
        // var dataDescription = $(this).attr("data-description");
        // $(".map-nav-description-mobile h4").text(dataTarget);
        // $(".map-nav-description-mobile p").text(dataDescription);

        $('.mobile-interactive-maps').addClass('loading').append('<div class="loader"></div>');
        setTimeout(function () {
            $('.mobile-interactive-maps').removeClass('loading');
            $('.mobile-interactive-maps').find('.loader').remove();
        }, 350)
        setTimeout(function () {
            mobileDistrictsImageSlider.slick("reinit");
            mobileDistrictsTextSlider.slick("reinit");
            mobileAssetsImageSlider.slick("reinit");
            mobileAssetsTextSlider.slick("reinit");
        }, 300)
        $('[data-mobile-map-name]').hide();
        $('[data-mobile-map-name="' + $(this).data("target") + '"]').show();
        $('.mobile-map-btns a').removeClass("active");
        $(this).addClass('active');
    });

    if ($(window).width() > 1199.98 && $("body").hasClass("smoothscroll-enabled")) {
        if ($('.rotating-left').length > 0) {
            var roundSvg = gsap.timeline({repeat: -1});
            roundSvg.to(".rotating-left", {rotation: -360, ease: "none", duration: 100})
            //roundSvg.to(".rotating-right", {rotation: -360, ease: "none", duration: 50}, '-=50')
            roundSvg.play();
        }
        if ($('.rotating-right').length > 0) {
            var roundSvg2 = gsap.timeline({repeat: -1});
            roundSvg2.to(".rotating-right", {rotation: 360, ease: "none", duration: 100})
            //roundSvg.to(".rotating-right", {rotation: -360, ease: "none", duration: 50}, '-=50')
            roundSvg2.play();
        }
        if($('.moveUp').length > 0) {
            var moveUp = new TimelineMax({repeat:-1, yoyo:true, repeatDelay:0.5});
            moveUp.to(".moveUp", 3, {y:-20});
            moveUp.to(".moveUp", 3, {y:0});
        }
        var secOff = [];
        $(".big-text .text-b").each(function (i) {
            secOff[i] = $(this).offset().top - $(window).height();
            console.log(secOff[i]);
        });

        scroll = new LocomotiveScroll({
            el: document.querySelector('[data-scroll-container]'),

            smooth: $("html").hasClass("ie") ? false : true,
        });
        $("body").on("mouseenter", ".bootstrap-select .dropdown-menu", function(){
            scroll.stop();
        });
        $("body").on("mouseleave", ".bootstrap-select .dropdown-menu", function(){
            scroll.start();
        });

        $("body").on("mouseenter", ".iti__flag-container", function(){
            scroll.stop();
        });
        $("body").on("mouseleave", ".iti__flag-container", function(){
            scroll.start();
        });

        $("body").on("mouseenter", "#contact_map", function(){
            scroll.stop();
        });
        $("body").on("mouseleave", "#contact_map", function(){
            scroll.start();
        });

        scroll.on('scroll', function (obj) {
            obj.scroll.y > 800 ? $('.sticky-header').addClass('show') : $('.sticky-header').removeClass('show');
            $(".big-text .text-b").each(function (i) {
                var thiss = $(this);
                // console.log(obj.scroll.y);
                if ((obj.scroll.y + 5000) > secOff[i]) {
                    thiss.addClass("is-inview");
                } else {
                    thiss.removeClass("is-inview");
                }
            })
        });
    }
    else {
        $('.menu-open-click').on('click', function () {
            $(this).toggleClass('active');
            $(this).next().slideToggle()
        })
    }

    if ($('[data-fancybox]').length > 0) {
        $('[data-fancybox]').fancybox({
            //toolbar  : false,
            //smallBtn : true,
            loop: false,
            keyboard: true,
            //infobar: false,
            clickSlide: false,
            clickOutside: false,
            touch: false,
            buttons: [
                //"zoom",
                //"share",
                //"slideShow",
                //"fullScreen",
                //"download",
                //"thumbs",
                "close"
            ],
        })
    }
    if ($('[data-fancybox="gallery"]').length > 0) {
        $('[data-fancybox="gallery"]').fancybox({
            //toolbar  : false,
            //smallBtn : true,
            loop: false,
            keyboard: true,
            //infobar: false,
            clickSlide: false,
            clickOutside: false,
            touch: false,
            wheel: false,
            buttons: [
                //"zoom",
                //"share",
                //"slideShow",
                //"fullScreen",
                //"download",
                //"thumbs",
                "close"
            ],
            transitionEffect: "slide",
            afterClose: function (){
                setTimeout(function (){
                    if($(".album-slider-main").length && $(".album-slider-main").hasClass("slick-initiated")){
                        $(".album-slider-main").slick("refresh")
                    }
                },500);
            }
        })
    }
    if ($('[data-fancybox="teamsInfo"]').length > 0) {
        $('[data-fancybox="teamsInfo"]').fancybox({
            toolbar  : false,
            smallBtn : true,
            loop: false,
            keyboard: true,
            infobar: false,
            clickSlide: false,
            clickOutside: false,
            touch: false,
            wheel: false,
            buttons: [
                //"zoom",
                //"share",
                //"slideShow",
                //"fullScreen",
                //"download",
                //"thumbs",
                //"close"
            ],
            transitionEffect: "slide",
        })
    }
    mainNavigation();
    playPauseVideo();
    searchPopup();
    searchFilterBtn();
    iconSliderFunc();
    socialMediaSlider();
    downloadSlider();
    eventSlider();
    latestNews();
    innovationSliderFunc();
    innovationTextSliderFunc();
    creativityTextSliderFunc();
    contactUsForm();
    svgSwiperSlider();
    districtsSliderFunc();
    districtsPinClickFunc();
    assetsSliderFunc();
    assetsPinFunc();
    mDistrictsImgSlider();
    moDistrictsTxtSlider();
    districtsSelectFunc();
    mAssetsImageSlider();
    mAssetsTxtSlider();
    assetsSelectFunc();
    marketPlaceSliderFunc();
    innovationExploreSliderFunc();
    relatedNewsSliderFunc();
    storiesLongImgFunc();
    albumSliderMainFunc();
    residencySliderMainFunc();
    dropDownFilter();
    companyIconSliderDineFunc();
    visionImgSliderFunc();
    LiveIntegratedSliderFunc();
    shareButtons();
    planAVisit();
    gettingToMiskcity();
    addEventToCalender();
});

$(window).on('load', function () {

    var tl = gsap.timeline({paused: true})
    tl.fromTo(".loader-img", {opacity: 1}, {opacity: 0, ease: "expo.out", duration: 1.5})
    tl.fromTo(".loading-bg-primary", {height: '100%'}, {height: '0%', duration: 1})
    tl.fromTo(".head-white-bg", {height: '0'}, {height: '100%', ease: "expo.in"});
    tl.play().then(function () {$("#loader-wrapper").hide();}).then(animateElements).then(homeTextSlider).then(function () {
        $('body').addClass('beforeLoaded');
        setTimeout(function () {
            $('.homeBanner .slick-dots').addClass('fadeInUp animated');
        }, 200)
        $('#container').removeAttr("style");
    }).then(iePopup);

    homeImageBanner();
    setTimeout(function (){
        window.dispatchEvent(new Event('resize'));
    },2000);
    setTimeout(function () {
        scrollTo(0, -1);
    }, 0);
});

$(document).keydown(function (event) {
    if (event.ctrlKey == true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109' || event.which == '187' || event.which == '189')) {
        event.preventDefault();
    }
});

$(window).bind('mousewheel DOMMouseScroll', function (event) {
    if (event.ctrlKey == true) {
        event.preventDefault();
    }
});