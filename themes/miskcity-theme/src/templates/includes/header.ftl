<div class="container">
    <div class="row align-content-center align-items-center">
        <div class="col-6 col-md-4 animate" data-animation="fadeInLeft">
            <#if language == "ar_SA">
                <a href="/ar/web/guest/home" class="logo" aria-label="Logo"><img src="${main_logo_ar}" alt="" class="img-fluid"></a>
            <#else>
                <a href="/en/web/guest/home" class="logo" aria-label="Logo"><img src="${main_logo_en}" alt="" class="img-fluid"></a>
            </#if>
        </div>
        <div class="col-6 col-md-8 animate" data-animation="fadeInRight">
            <nav class="head-nav">
                <ul>
                    <li><@liferay_ui["language"] ddmTemplateKey="71502" /></li>
                    <li>
                        <a href="javascript:" class="search-btn">
                            <img src="/assets/svgs/search.svg" alt="search" class="normal">
                            <img src="/assets/svgs/search-white.svg" alt="search" class="showHover">
                        </a>
                    </li>
                    <li>
                        <a href="javascript:" class="menu-btn mainNavOpen">
                            <img src="/assets/svgs/menu.svg" alt="menu" class="normal">
                            <img src="/assets/svgs/menu-hover.svg" alt="menu" class="showHover">
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>