<div class="mobileText">
    <#if language == "ar_SA">
        <a href="/ar/web/guest/home" class="logo"><img src="${orientation_logo}" alt="" class="img-fluid"></a>
    <#else>
        <a href="/en/web/guest/home" class="logo"><img src="${orientation_logo}" alt="" class="img-fluid"></a>
    </#if>
    <p>
        <i><img src="/assets/images/rotate.gif" class="img-fluid" alt="Rotate"></i>
        For an optimal experience, please<br> rotate your device to portrait mode.
    </p>
</div>
