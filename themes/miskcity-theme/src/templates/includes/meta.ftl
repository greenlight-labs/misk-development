<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<#if language == "ar_SA">
    <meta name="description" content="مرحباًبكم في مدينة محمد بن سلمان غير الربحية, موطن الإبداع وحاضنة المواهب الشابة.">
    <meta name="keywords" content="مدينة محمد بن سلمان غير الربحية">
    <meta property="og:title" content="مدينة محمد بن سلمان غير الربحية" />
    <meta property="og:type" content="Content" />
    <meta property="og:url" content="https://nonprofitcity.sa/ar/" />
    <meta property="og:image" content="https://nonprofitcity.sa/assets/images/nonprofitcity_social_img.jpg" />

    <meta name="twitter:title" content="مدينة محمد بن سلمان غير الربحية">
    <meta name="twitter:description" content="مرحباًبكم في مدينة محمد بن سلمان غير الربحية, موطن الإبداع وحاضنة المواهب الشابة.">
    <meta name="twitter:image" content="https://nonprofitcity.sa/assets/images/nonprofitcity_social_img.jpg">
    <meta name="twitter:card" content="summary">
<#else>
    <meta name="description" content="Welcome to Mohammed Bin Salman Nonprofit City. A visionary home for young, dynamic, creative people.">
    <meta name="keywords" content="Mohammed Bin Salman Nonprofit City">
    <meta property="og:title" content="Mohammed Bin Salman Nonprofit City" />
    <meta property="og:type" content="Content" />
    <meta property="og:url" content="https://nonprofitcity.sa/en/" />
    <meta property="og:image" content="https://nonprofitcity.sa/assets/images/nonprofitcity_social_img.jpg" />

    <meta name="twitter:title" content="Mohammed Bin Salman Nonprofit City">
    <meta name="twitter:description" content="Welcome to Mohammed Bin Salman Nonprofit City. A visionary home for young, dynamic, creative people.">
    <meta name="twitter:image" content="https://nonprofitcity.sa/assets/images/nonprofitcity_social_img.jpg">
    <meta name="twitter:card" content="summary">
</#if>
<meta name="author" content="Traffic Digital"/>
<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
<!--<link rel="manifest" href="/site.webmanifest">-->
<meta name="msapplication-TileColor" content="#DA1884">
<meta name="theme-color" content="#DA1884">
<meta name="HandheldFriendly" content="True">
<#if language == "ar_SA">
    <link rel="stylesheet" href="/o/miskcity-theme/assets/css/style-ar.css?t=${theme_timestamp}">
<#else>
    <link rel="stylesheet" href="/o/miskcity-theme/assets/css/style.css?t=${theme_timestamp}">
</#if>
<!--[if IE]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->