<div class="mainNavigation" id="mainNavigation">
    <div class="m-primary-bg"></div>
    <div class="m-secondary-bg"></div>
    <div class="m-navigation-body">
        <div class="navHead">
            <div class="container">
                <div class="row align-content-center align-items-center">
                    <div class="col-6 col-md-4">
                        <#if language == "ar_SA">
                            <a href="/ar/web/guest/home" class="logo" aria-label="Logo"><img src="${main_logo_ar}" alt="" class="img-fluid"></a>
                        <#else>
                            <a href="/en/web/guest/home" class="logo" aria-label="Logo"><img src="${main_logo_en}" alt="" class="img-fluid"></a>
                        </#if>
                    </div>
                    <div class="col-6 col-md-8">
                        <div class="menuCloseWrap">
                            <a href="javascript:" class="close-menu-btn mainNavClose">
                                <span class="closeText"><@liferay.language key="misk-close-menu" /></span>
                                <span class="closeCircle">
                                    <i class="icon-close"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="menuWrapper">
                        <nav class="mainMenu">
                            <#assign primaryNavigationPreferencesMap =
                            {
                            "displayDepth": "3",
                            "displayStyle": "ddmTemplate_73504",
                            "portletSetupPortletDecoratorId": "barebone",
                            "rootLayoutType": "relative",
                            "siteNavigationMenuId": "59313",
                            "siteNavigationMenuType": "1"
                            }
                            />

                            <@liferay.navigation_menu
                            default_preferences=freeMarkerPortletPreferences.getPreferences(primaryNavigationPreferencesMap)
                            instance_id="header_navigation_menu"
                            />
                            <span class="left-line"></span>
                        </nav>
                        <nav class="headBtns">
                            <ul>
                                <#if language == "ar_SA">
                                    <li><a href="/ar/contact-us/" class="btn btn-outline-primary"><span>تواصل معنا</span><i class="dot-line"></i></a></li>
                                <#else>
                                    <li><a href="/en/contact-us/" class="btn btn-outline-primary"><span>Contact us</span><i class="dot-line"></i></a></li>
                                </#if>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container headBtmCon">
            <div class="row">
                <div class="col-10">
                    <nav class="headFtNav">
                        <ul>
                            <li>&copy; ${the_year} <@liferay.language key="misk-website-name" /></li>
                            <li><@liferay.language key="misk-copyright-text" /></li>
                            <#if language == "ar_SA">
                                <li><a href="/ar/privacy-policy/">سياسة الخصوصية</a></li>
                                <li><a href="/ar/terms-of-use/">شروط الاستخدام</a></li>
                                <li><a href="/ar/disclaimer/">	إخلاء المسؤولية</a></li>
                            <#else>
                                <li><a href="/en/privacy-policy/">Privacy Policy</a></li>
                                <li><a href="/en/terms-of-use">Terms of Use</a></li>
                                <li><a href="/en/disclaimer/">Disclaimer</a></li>
                            </#if>
                        </ul>
                    </nav>
                </div>
                <div class="col-2">
                    <nav class="headSocialIcons">
                        <ul>
                            <#--<li><a href="https://www.facebook.com/miskksa/"><i class="icon-facebook"></i></a></li>
                            <li><a href="https://twitter.com/MiskKSA"><i class="icon-twitter"></i></a></li>
                            <li><a href="https://www.linkedin.com/company/misk-foundation/"><i class="icon-linkedin"></i></a></li>-->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="misk-big-text">
            <#if language == "ar_SA">
                <img src="${main_logo_ar}" alt="" class="img-fluid">
            <#else>
                <img src="${main_logo_en}" alt="" class="img-fluid">
            </#if>
        </div>
    </div>
</div>