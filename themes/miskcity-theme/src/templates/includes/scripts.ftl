<script src="https://polyfill.io/v3/polyfill.min.js?features=Array.prototype.forEach%2CNodeList.prototype.forEach%2CCustomEvent%2CEvent%2CObject.assign"></script>
<script src="/assets/js/app.min.js"></script>
<#if language == "ar_SA">
    <script src="/o/miskcity-theme/assets/js/main-ar.js?t=${theme_timestamp}"></script>
<#else>
    <script src="/o/miskcity-theme/assets/js/main.js?t=${theme_timestamp}"></script>
</#if>
<script type="text/javascript">
    function sanitizeInput(form, input) {
        var q = form[input].value;
        var alphaRegex = /[^a-zA-Z\u0621-\u064A\u066E-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFF\s]/g;
        // replce all non arabic and english characters with empty string
        q = q.replace(alphaRegex, '');
        // replace all spaces with +
        //q = q.replace(/\s+/g, '+');
        form[input].value = q;
        return true;
    }
</script>