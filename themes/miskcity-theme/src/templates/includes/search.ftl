<#assign
    <#--
    function sanitizeInput(form, input) {
        var q = form[input].value;
        var alphaRegex = /[^a-zA-Z\u0621-\u064A\u066E-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFF\s]/g;
        // replce all non arabic and english characters with empty string
        q = q.replace(alphaRegex, '');
        // replace all spaces with +
        //q = q.replace(/\s+/g, '+');
        form[input].value = q;
        return true;
    }
    -->
    type = (request.getParameter("type")?? && request.getParameter("type") != "")?then(request.getParameter("type"), "all")
    query = (request.getParameter("q")?? && request.getParameter("q") != "")?then(request.getParameter("q"), "")
    <#--sanitize query-->
    query = query?replace("[^a-zA-Z\\u0621-\\u064A\\u066E-\\u06D3\\u06D5\\u06E5\\u06E6\\u06EE\\u06EF\\u06FA-\\u06FC\\u06FF\\u0750-\\u077F\\u08A0-\\u08FF\\uFB50-\\uFDFF\\uFE70-\\uFEFF\\s]", "", "r")
/>

<div class="search-popup">
    <div class="search-footer-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                    <#if language == "ar_SA">
                        <a href="/ar/web/guest/home" class="logo" aria-label="Logo"><img src="${main_logo_ar}" alt="" class="img-fluid"></a>
                    <#else>
                        <a href="/en/web/guest/home" class="logo" aria-label="Logo"><img src="${main_logo_en}" alt="" class="img-fluid"></a>
                    </#if>
                </div>
                <div class="col-6">
                    <a href="javascript:" class="search-close">
                        <span class="closeText"><@liferay.language key="misk-close" /></span>
                        <span class="closeCircle">
                            <i class="icon-close"></i>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-11 overflow-hidden">
                <div class="search-animate animate" data-animation="translateUp" data-duration="200">
                    <div class="search-filter-btn">
                        <ul>
                            <li>
                                <span><@liferay.language key="misk-search-within" /></span>
                            </li>
                            <li>
                                <a class="${(type == "all")?then("active","")}" href="javascript:" data-field-id="all"><@liferay.language key="misk-all" /></a>
                            </li>
                            <li>
                                <a class="${(type == "stories")?then("active","")}" href="javascript:" data-field-id="stories"><@liferay.language key="misk-stories" /></a>
                            </li>
                            <li>
                                <a class="${(type == "events")?then("active","")}" href="javascript:" data-field-id="events"><@liferay.language key="misk-events" /></a>
                            </li>
                            <li>
                                <a class="${(type == "residences")?then("active","")}" href="javascript:" data-field-id="residences"><@liferay.language key="misk-residences" /></a>
                            </li>
                        </ul>
                    </div>
                    <div class="searchbar-input">
                        <form action="${(language == "ar_SA")?then("/ar/search-results/", "/en/search-results/")}" method="get" id="searchBarInput" onsubmit="sanitizeInput(this, 'q')">
                            <div class="form-group">
                                <input type="text" name="q" class="form-control" placeholder="<@liferay.language key="misk-search" />" value="${query}">
                                <input type="hidden" name="type" value="${type}">
                                <button type="submit" class="search-popup-btn search-btn">
                                    <img src="/assets/svgs/search.svg" alt="search" class="normal">
                                    <img src="/assets/svgs/search-white.svg" alt="search" class="showHover">
                                </button>
                            </div>
                            <p><@liferay.language key="misk-search-for-misk-city" /></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function () {
        $('.search-filter-btn li a').on('click', function () {
            var filters = $('.search-filter-btn li a');
            var field_id = $(this).data('field-id');
            if(field_id){
                $('div.searchbar-input form input[name="type"]').val(field_id);
                filters.each(function (index, element) {
                    $(element).removeClass('active');
                });
                $(this).addClass('active');
            }
        });
    }
</script>