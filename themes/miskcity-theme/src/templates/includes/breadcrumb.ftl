<section class="breadcrumb-section" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <#assign preferences = freeMarkerPortletPreferences.getPreferences({
                    "portletSetupPortletDecoratorId":"barebone",
                    "showCurrentGroup":true,
                    "displayStyle":"ddmTemplate_40204",
                    "displayStyleGroupId": "20124",
                    "showLayout":true,
                    "showPortletBreadcrumb":true,
                    "showGuestGroup":false
                }) />

                <@liferay.breadcrumbs default_preferences="${preferences}" />


                <#--<@liferay_portlet["runtime"]
                defaultPreferences="${preferences}"
                persistSettings=false
                portletName="com_liferay_site_navigation_breadcrumb_web_portlet_SiteNavigationBreadcrumbPortlet"
                settingsScope="group"
                />-->
                <#--instanceId="main_breadcrumb"-->
                <#--<nav aria-label="breadcrumb">
                    <ol class="breadcrumb animate" data-animation="fadeInUp" data-duration="100">
                        <li class="breadcrumb-item"><a href="/en/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                    </ol>
                </nav>-->
            </div>
        </div>
    </div>
</section>
