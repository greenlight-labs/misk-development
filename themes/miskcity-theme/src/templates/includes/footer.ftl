<footer data-scroll-section>
    <div class="container" data-scroll data-scroll-speed="0">
        <div class="row">
            <div class="col-12 order-last col-md-6 order-md-0 animate" data-animation="fadeInLeft" data-duration="300">
                <p>&copy; ${the_year} <@liferay.language key="misk-website-name" />. <@liferay.language key="misk-copyright-text" /></p>
            </div>
            <div class="col-12 order-first col-md-6 order-md-0 animate" data-animation="fadeInRight" data-duration="300">
                <nav>
                    <#--<ul class="sitemap-links">
                        <li><a href="/en/privacy-policy/">Privacy Policy</a></li>
                        <li><a href="/en/terms-and-conditions/">Terms &amp; Conditions</a></li>
                        <li><a href="/en/disclaimer/">Disclaimer</a></li>
                    </ul>-->
                    <#assign secondaryNavigationPreferencesMap =
                    {
                    "displayDepth": "1",
                    "displayStyle": "ddmTemplate_NAVBAR-FOOTER-MENU-FTL",
                    "portletSetupPortletDecoratorId": "barebone",
                    "rootLayoutType": "relative",
                    "siteNavigationMenuId": "63616",
                    "siteNavigationMenuType": "2"
                    }
                    />

                    <@liferay.navigation_menu
                    default_preferences=freeMarkerPortletPreferences.getPreferences(secondaryNavigationPreferencesMap)
                    instance_id="footer_navigation_menu"
                    />
                </nav>
            </div>
        </div>
    </div>
</footer>


<!-- Modal -->
<div class="modal fade popup-style ie-popup" id="iePopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" id="exampleModalLabel">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <i class="icon-close"></i> </button>
            <div class="modal-body">
                <h3>Notice</h3>
                <p>Your web browser is not fully supported by MISK CITY and misk.trafficdemos.com. For optimal experience and full features, please upgrade to a modern browser.</p>
                <p>You can get the new Chrome at <a href="https://www.google.com/chrome/?brand=CHBD&gclid=CjwKCAjw5cL2BRASEiwAENqAPs0dwkGR3SpWmdbQGBberk2ORcpEt-x6m_hXO8l7KAovJ1K-QzLAcxoCfp8QAvD_BwE&gclsrc=aw.ds"><span>Download Chrome</span></a>.</p>
            </div>
        </div>
    </div>
</div>
