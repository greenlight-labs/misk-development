<!DOCTYPE html>

<#include init />

<!--[if lte IE 9]>
<html class="${root_css_class} ie9" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">
<![endif]-->
<!--[if gt IE 9]><!-->
<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">
<head>
	<@liferay_util["include"] page=top_head_include />
	<#include "${full_templates_path}/includes/meta.ftl" />
	<title>${the_title} - ${site_name}</title>
</head>
<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />
<@liferay_util["include"] page=body_top_include />
<@liferay.control_menu />

<div class="container-fluid" id="wrapper">
	<#include "${full_templates_path}/includes/loading.ftl" />

	<a class="sr-only sr-only-focusable" href="#container">Skip to main content</a>
	<#include "${full_templates_path}/includes/main-navigation.ftl" />
	<header class="sticky-header">
		<#include "${full_templates_path}/includes/header.ftl" />
	</header>
	<main id="container" data-scroll-container style="overflow: hidden">

		<div class="no-banner-space" data-scroll-section>
			<div class="static-header">
				<div class="head-white-bg"></div>
				<header>
					<#include "${full_templates_path}/includes/header.ftl" />
				</header>
			</div>
		</div>

		<#if show_breadcrumbs>
			<#include "${full_templates_path}/includes/breadcrumb.ftl" />
		</#if>

		<div id="content">
			<!-- Page Content -->
			<#include "${full_templates_path}/content.ftl" />
		</div>

		<#include "${full_templates_path}/includes/footer.ftl" />
	</main>

	<#include "${full_templates_path}/includes/search.ftl" />
	<#include "${full_templates_path}/includes/orientation-lock.ftl" />
</div>

<@liferay_util["include"] page=body_bottom_include />
<@liferay_util["include"] page=bottom_include />

<#include "${full_templates_path}/includes/scripts.ftl" />

</body>
</html>
