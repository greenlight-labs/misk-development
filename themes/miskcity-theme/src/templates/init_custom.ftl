<#--
This file allows you to override and define new FreeMarker variables.
-->

<#assign
home_page_urls = ["/", "/en", "/en/", "/web/guest", "/web/guest/", "/web/guest/home", "/web/guest/home/", "/ar", "/ar/", "/ar/web/guest", "/ar/web/guest/", "/ar/web/guest/home", "/ar/web/guest/home/"]
current_url = htmlUtil.escape(theme_display.getURLCurrent())
current_url = current_url?split("?")[0]
is_home_page = home_page_urls?seq_contains(current_url)
/>
<#--
<#if is_home_page>
    <#assign css_class = css_class + " smoothscroll-enabled" />
</#if>
-->

<#assign css_class = css_class + " smoothscroll-enabled" />
<#-- website logos -->
<#assign
main_logo_en = "/assets/svgs/logo.svg"
main_logo_ar = "/assets/svgs/logo.svg"
website_loader = "/assets/svgs/loader.gif"
orientation_logo = "/assets/svgs/white-logo.svg"
/>

<#assign home_page_id = themeDisplay.getLayout().getLayoutId()/>

<#assign show_breadcrumbs = true />

<#if home_page_id == 59>
    <#assign show_breadcrumbs = false />
</#if>