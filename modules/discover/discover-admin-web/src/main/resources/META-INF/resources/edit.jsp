<%@page import="discover.service.CategoryServiceUtil"%>
<%@include file = "init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    Discover entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = DiscoverLocalServiceUtil.getDiscover(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    List<Category> categories = CategoryServiceUtil.getCategories(scopeGroupId);
%>
 
<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editEntryURL %>" name="fm">

    <aui:model-context bean="<%= entry %>" model="<%= Discover.class %>" />

    <aui:input type="hidden" name="resourcePrimKey"
               value='<%= entry == null ? "" : entry.getDiscoverId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:input name="title" label="Title" autoSize="true" helpMessage="Max 30 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="listingImage" label="Listing Image" helpMessage="Image Dimensions: W x H pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="listingImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="description" label="Description" helpMessage="Max 160 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
             <aui:input name="orderNo" label="Order Number" helpMessage="Order Number default value 9999" placeholder="9999">                
            </aui:input>
            <aui:select required="true" label="Category" name="categoryId">
                <% for (Category curCategory : categories) { %>
                <aui:option label="<%= curCategory.getName(locale) %>" value="<%= curCategory.getCategoryId() %>" />
                <% } %>
            </aui:select>
        </aui:fieldset>
    </aui:fieldset-group>

    <%@include file="/includes/gallery.jsp" %>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>