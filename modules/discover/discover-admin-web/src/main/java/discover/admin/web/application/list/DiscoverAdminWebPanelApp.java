package discover.admin.web.application.list;

import discover.admin.web.constants.DiscoverAdminWebPanelCategoryKeys;
import discover.admin.web.constants.DiscoverAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + DiscoverAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class DiscoverAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return DiscoverAdminWebPortletKeys.DISCOVERADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + DiscoverAdminWebPortletKeys.DISCOVERADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}