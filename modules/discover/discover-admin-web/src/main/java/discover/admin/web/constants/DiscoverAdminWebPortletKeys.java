package discover.admin.web.constants;

/**
 * @author tz
 */
public class DiscoverAdminWebPortletKeys {

	public static final String DISCOVERADMINWEB =
		"discover_admin_web_DiscoverAdminWebPortlet";
	public static final String CATEGORIESADMINWEB =
			"discover_admin_web_CategoriesAdminWebPortlet";
}