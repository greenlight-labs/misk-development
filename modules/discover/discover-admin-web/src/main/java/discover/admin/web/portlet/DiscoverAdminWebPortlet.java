package discover.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import discover.admin.web.constants.DiscoverAdminWebPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.*;

import discover.exception.DiscoverValidateException;
import discover.model.Discover;
import discover.service.DiscoverLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=DiscoverAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DiscoverAdminWebPortletKeys.DISCOVERADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class DiscoverAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addEntry(ActionRequest request, ActionResponse response) throws Exception, DiscoverValidateException {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
		long orderNo = ParamUtil.getLong(request, "orderNo", 9999);
		Discover entry = _discoverLocalService.getDiscoverFromRequest(primaryKey, request);
		if (orderNo != 0) {
			entry.setOrderNo(orderNo);
		} else {
			entry.setOrderNo(9999);
		}

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Discover.class.getName(), request);

		// Add entry

		_discoverLocalService.addEntry(entry, serviceContext);

		// Add gallery images
		updateGallery(entry, request, serviceContext);

		SessionMessages.add(request, "discoverAddedSuccessfully");
	}

	public void updateEntry(ActionRequest request, ActionResponse response)
			throws Exception {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
		long orderNo = ParamUtil.getLong(request, "orderNo",9999);
		Discover entry = _discoverLocalService.getDiscoverFromRequest(
				primaryKey, request);
		if (orderNo != 0) {
			entry.setOrderNo(orderNo);
		} else {
			entry.setOrderNo(9999);
		}
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Discover.class.getName(), request);

		//Update entry
		_discoverLocalService.updateEntry(entry, serviceContext);

		// Add gallery images
		updateGallery(entry, request, serviceContext);

		SessionMessages.add(request, "discoverUpdatedSuccessfully");
	}

	public void deleteEntry(ActionRequest request, ActionResponse response)
			throws PortalException {

		long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

		try {
			_discoverLocalService.deleteEntry(entryId);
		} catch (PortalException pe) {
			ReflectionUtil.throwException(pe);
		}
	}

	public void updateGallery(Discover entry, ActionRequest actionRequest, ServiceContext serviceContext)
			throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
				WebKeys.THEME_DISPLAY);

		_discoverLocalService.updateGalleries(
				entry,
				_discoverLocalService.getGalleries(actionRequest),
				serviceContext
		);
	}

	@Reference
	private DiscoverLocalService _discoverLocalService;

	@Reference
	private ItemSelector _itemSelector;
}