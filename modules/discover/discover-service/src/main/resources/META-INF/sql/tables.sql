create table misk_discover (
	uuid_ VARCHAR(75) null,
	discoverId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title STRING null,
	description STRING null,
	listingImage STRING null,
	categoryId LONG,
	orderNo LONG
);

create table misk_discover_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug STRING null
);

create table misk_discover_galleries (
	uuid_ VARCHAR(75) null,
	slideId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	discoverId LONG,
	image STRING null,
	thumbnail STRING null
);