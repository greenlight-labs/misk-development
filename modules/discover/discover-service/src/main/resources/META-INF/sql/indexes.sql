create index IX_BB5D4587 on misk_discover (categoryId);
create index IX_D8FEAC50 on misk_discover (groupId);
create index IX_310E850E on misk_discover (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_62F0F710 on misk_discover (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_8B6E0DD on misk_discover_categories (groupId);
create index IX_A7C41F21 on misk_discover_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_885BF9E3 on misk_discover_categories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_4078D623 on misk_discover_galleries (discoverId);
create index IX_3B409EBF on misk_discover_galleries (groupId);
create index IX_20B4DF7F on misk_discover_galleries (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_C9D39C1 on misk_discover_galleries (uuid_[$COLUMN_LENGTH:75$], groupId);