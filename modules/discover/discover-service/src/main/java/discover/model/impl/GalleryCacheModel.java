/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import discover.model.Gallery;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Gallery in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class GalleryCacheModel implements CacheModel<Gallery>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GalleryCacheModel)) {
			return false;
		}

		GalleryCacheModel galleryCacheModel = (GalleryCacheModel)object;

		if (slideId == galleryCacheModel.slideId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, slideId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", slideId=");
		sb.append(slideId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", discoverId=");
		sb.append(discoverId);
		sb.append(", image=");
		sb.append(image);
		sb.append(", thumbnail=");
		sb.append(thumbnail);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Gallery toEntityModel() {
		GalleryImpl galleryImpl = new GalleryImpl();

		if (uuid == null) {
			galleryImpl.setUuid("");
		}
		else {
			galleryImpl.setUuid(uuid);
		}

		galleryImpl.setSlideId(slideId);
		galleryImpl.setGroupId(groupId);
		galleryImpl.setCompanyId(companyId);
		galleryImpl.setUserId(userId);

		if (userName == null) {
			galleryImpl.setUserName("");
		}
		else {
			galleryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			galleryImpl.setCreateDate(null);
		}
		else {
			galleryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			galleryImpl.setModifiedDate(null);
		}
		else {
			galleryImpl.setModifiedDate(new Date(modifiedDate));
		}

		galleryImpl.setDiscoverId(discoverId);

		if (image == null) {
			galleryImpl.setImage("");
		}
		else {
			galleryImpl.setImage(image);
		}

		if (thumbnail == null) {
			galleryImpl.setThumbnail("");
		}
		else {
			galleryImpl.setThumbnail(thumbnail);
		}

		galleryImpl.resetOriginalValues();

		return galleryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		slideId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		discoverId = objectInput.readLong();
		image = objectInput.readUTF();
		thumbnail = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(slideId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(discoverId);

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (thumbnail == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(thumbnail);
		}
	}

	public String uuid;
	public long slideId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long discoverId;
	public String image;
	public String thumbnail;

}