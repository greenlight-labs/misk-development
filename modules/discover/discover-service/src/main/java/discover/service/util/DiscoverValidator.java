package discover.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import discover.model.Discover;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Discover Validator 
 * 
 * @author tz
 *
 */
public class DiscoverValidator implements ModelValidator<Discover> {

	@Override
	public void validate(Discover entry) throws PortalException {
/*   */
        // Field discoverId
        validateDiscoverId(entry.getDiscoverId());

        // Field title
        validateTitle(entry.getTitle());

        // Field description
        validateDescription(entry.getDescription());

        // Field listingImage
        validateListingImage(entry.getListingImage());
		
	}

/*   */
    /**
    * discoverId field Validation
    *
    * @param field discoverId
    */
    protected void validateDiscoverId(long field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
    }

    /**
    * title field Validation
    *
    * @param field title
    */
    protected void validateTitle(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("discover-title-required");
        }

    }

    /**
    * description field Validation
    *
    * @param field description
    */
    protected void validateDescription(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("discover-description-required");
        }

    }

    /**
    * listingImage field Validation
    *
    * @param field listingImage
    */
    protected void validateListingImage(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

/*  */ 	
	

	protected List<String> _errors = new ArrayList<>();

}
