/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import discover.exception.DiscoverValidateException;
import discover.model.Discover;
import discover.model.Gallery;
import discover.service.DiscoverLocalServiceUtil;
import discover.service.GalleryLocalServiceUtil;
import discover.service.base.DiscoverLocalServiceBaseImpl;
import discover.service.indexer.DiscoverIndexer;
import discover.service.util.DiscoverValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * The implementation of the discover local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>discover.service.DiscoverLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DiscoverLocalServiceBaseImpl
 */
@Component(property = "model.class.name=discover.model.Discover", service = AopService.class)
public class DiscoverLocalServiceImpl extends DiscoverLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>discover.service.DiscoverLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>discover.service.DiscoverLocalServiceUtil</code>.
	 */

	public Discover addEntry(Discover orgEntry, ServiceContext serviceContext)
			throws PortalException, DiscoverValidateException {

		// Validation

		ModelValidator<Discover> modelValidator = new DiscoverValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Discover entry = _addEntry(orgEntry, serviceContext);

		Discover addedEntry = discoverPersistence.update(entry);

		discoverPersistence.clearCache();

		return addedEntry;
	}

	public Discover updateEntry(Discover orgEntry, ServiceContext serviceContext)
			throws PortalException, DiscoverValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Discover> modelValidator = new DiscoverValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Discover entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Discover updatedEntry = discoverPersistence.update(entry);
		discoverPersistence.clearCache();

		return updatedEntry;
	}

	protected Discover _addEntry(Discover entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Discover.class.getName());

		Discover newEntry = discoverPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setCategoryId(entry.getCategoryId());
		newEntry.setUuid(serviceContext.getUuid());
		newEntry.setOrderNo(entry.getOrderNo()!=0?entry.getOrderNo():9999);
		newEntry.setTitle(entry.getTitle());
		newEntry.setDescription(entry.getDescription());
		newEntry.setListingImage(entry.getListingImage());

		// return discoverPersistence.update(newEntry);
		return newEntry;
	}

	protected Discover _updateEntry(long primaryKey, Discover entry, ServiceContext serviceContext)
			throws PortalException {

		Discover updateEntry = fetchDiscover(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setCategoryId(entry.getCategoryId());
		updateEntry.setUuid(entry.getUuid());
		updateEntry.setOrderNo(entry.getOrderNo()!=0?entry.getOrderNo():9999);
		updateEntry.setDiscoverId(entry.getDiscoverId());
		updateEntry.setTitle(entry.getTitle());
		updateEntry.setDescription(entry.getDescription());
		updateEntry.setListingImage(entry.getListingImage());

		return updateEntry;
	}

	public Discover deleteEntry(long primaryKey) throws PortalException {
		Discover entry = getDiscover(primaryKey);
		discoverPersistence.remove(entry);

		return entry;
	}

	public List<Discover> findAllInGroup(long groupId) {

		return discoverPersistence.findByGroupId(groupId);
	}

	public List<Discover> findAllInGroup(long groupId, int start, int end, OrderByComparator<Discover> obc) {

		return discoverPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Discover> findAllInGroup(long groupId, int start, int end) {

		return discoverPersistence.findByGroupId(groupId, start, end);
	}

	public int countAllInGroup(long groupId) {

		return discoverPersistence.countByGroupId(groupId);
	}

	public Discover getDiscoverFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, DiscoverValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Discover entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchDiscover(primaryKey);
		}

		try {
			entry.setDiscoverId(primaryKey);

			entry.setTitleMap(LocalizationUtil.getLocalizationMap(request, "title"));
			entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));
			entry.setListingImageMap(LocalizationUtil.getLocalizationMap(request, "listingImage"));
			entry.setCategoryId(ParamUtil.getLong(request, "categoryId"));
			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new DiscoverValidateException(error);
		}

		return entry;
	}

	public Discover getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Discover.class.getName());

		return createDiscover(primaryKey);
	}

	/*
	 * **********************************- START: Discover Gallery Code
	 * -*****************************
	 */
	public void addEntryGallery(Discover orgEntry, PortletRequest request) throws PortletException {

		String galleryIndexesString = ParamUtil.getString(request, "galleryIndexes");

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);
	}

	@Override
	public void updateGalleries(Discover entry, List<Gallery> galleries, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> galleryIds = new HashSet<>();

		for (Gallery gallery : galleries) {
			long galleryId = gallery.getSlideId();

			// additional step to set Ids - initially zero
			gallery.setDiscoverId(entry.getDiscoverId());
			gallery.setUserId(entry.getUserId());
			gallery.setCompanyId(entry.getCompanyId());
			gallery.setGroupId(entry.getGroupId());

			if (galleryId <= 0) {
				gallery = GalleryLocalServiceUtil.addEntry(gallery, serviceContext);

				galleryId = gallery.getSlideId();
			} else {
				GalleryLocalServiceUtil.updateEntry(gallery, serviceContext);
			}

			galleryIds.add(galleryId);
		}

		galleries = GalleryLocalServiceUtil.findAllInDiscover(entry.getDiscoverId());

		for (Gallery gallery : galleries) {
			if (!galleryIds.contains(gallery.getSlideId())) {
				GalleryLocalServiceUtil.deleteGallery(gallery.getSlideId());
			}
		}
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest) {
		return getGalleries(actionRequest, Collections.<Gallery>emptyList());
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest, List<Gallery> defaultGalleries) {

		String galleryIndexesString = ParamUtil.getString(actionRequest, "galleryIndexes");

		if (galleryIndexesString == null) {
			return defaultGalleries;
		}

		List<Gallery> galleries = new ArrayList<>();

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);

		for (int galleryIndex : galleryIndexes) {
			String image = ParamUtil.getString(actionRequest, "galleryImage" + galleryIndex);

			if (Validator.isNull(image)) {
				continue;
			}

			String thumbnail = ParamUtil.getString(actionRequest, "galleryThumbnail" + galleryIndex);

			long galleryId = ParamUtil.getLong(actionRequest, "galleryId" + galleryIndex);

			Gallery gallery = GalleryLocalServiceUtil.createGallery(galleryId);

			gallery.setDiscoverId(0);
			gallery.setImage(image);
			gallery.setThumbnail(thumbnail);

			galleries.add(gallery);
		}

		return galleries;
	}

	/**
	 * search
	 */
	@Override
	public List<Discover> searchDiscovers(SearchContext searchContext) {

		Hits hits;
		_log.info("search discover");
		List<Discover> discoverList = new ArrayList<>();
		DiscoverIndexer indexer = (DiscoverIndexer) IndexerRegistryUtil.getIndexer(Discover.class);
		try {

			hits = indexer.search(searchContext);

			_log.info("indexer " + indexer.search(searchContext));
			for (int i = 0; i < hits.getDocs().length; i++) {
				Document doc = hits.doc(i);

				long discoverId = GetterUtil.getLong(doc.get(Field.ENTRY_CLASS_PK));
				Discover discover = null;
				discover = DiscoverLocalServiceUtil.getDiscover(discoverId);
				discoverList.add(discover);
			}
		} catch (PortalException | SystemException e) {
			_log.error(e);
		}

		return discoverList;
	}
	/*
	 * **********************************- END: Discover Gallery Code
	 * -*****************************
	 */

	private static Log _log = LogFactoryUtil.getLog(DiscoverLocalServiceImpl.class);
}