package discover.service.indexer;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.GetterUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import discover.model.Discover;
import discover.service.DiscoverLocalService;
import discover.service.DiscoverLocalServiceUtil;

@Component(immediate = true, service = Indexer.class)
public class DiscoverIndexer extends BaseIndexer<Discover> {
	public static final String CLASS_NAME = DiscoverIndexer.class.getName();
	public static final String DESCRIPTION = "description";

	public DiscoverIndexer() {
		setDefaultSelectedFieldNames(Field.ASSET_TAG_NAMES, Field.COMPANY_ID, Field.CONTENT, Field.ENTRY_CLASS_NAME,
				Field.ENTRY_CLASS_PK, Field.GROUP_ID, Field.MODIFIED_DATE, Field.SCOPE_GROUP_ID, Field.TITLE, Field.UID,
				DESCRIPTION);
		setPermissionAware(false);
		setFilterSearch(true);
	}

	@Override
	public String getClassName() {
		return Discover.class.getName();
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest,
			PortletResponse portletResponse) throws Exception {
		Summary summary = createSummary(document);
		summary.setMaxContentLength(200);
		return summary;
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		Discover event = discoverLocalService.getDiscover(classPK);
		doReindex(event);

	}

	@Override
	protected void doReindex(String[] ids) throws Exception {
		long companyId = GetterUtil.getLong(ids[0]);
		reindexEmployees(companyId);

	}

	@Override
	protected void doReindex(Discover object) throws Exception {
		Document document = getDocument(object);
		indexWriterHelper.updateDocument(getSearchEngineId(), object.getCompanyId(), document, isCommitImmediately());

	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter,
			SearchContext searchContext) throws Exception {
		addSearchLocalizedTerm(searchQuery, searchContext, Field.TITLE, false);
		addSearchLocalizedTerm(searchQuery, searchContext, DESCRIPTION, false);

	}

	@Override
	protected void doDelete(Discover object) throws Exception {
		deleteDocument(object.getCompanyId(), object.getDiscoverId());

	}

	@Override
	protected Document doGetDocument(Discover object) throws Exception {

		Document document = getBaseModelDocument(Discover.class.getName(), object);

		document.addLocalizedKeyword(Field.TITLE, object.getTitleMap());
		document.addLocalizedKeyword(DESCRIPTION, object.getDescriptionMap());

		return document;
	}

	protected void reindexEmployees(long companyId) throws PortalException {
		final IndexableActionableDynamicQuery indexableActionableDynamicQuery = discoverLocalService
				.getIndexableActionableDynamicQuery();
		indexableActionableDynamicQuery.setCompanyId(companyId);
		indexableActionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod() {
			@Override
			public void performAction(Object event) {
				try {
					Document document = getDocument((Discover) event);
					indexableActionableDynamicQuery.addDocuments(document);
				} catch (PortalException pe) {
					pe.printStackTrace();
				}
			}
		});
		indexableActionableDynamicQuery.setSearchEngineId(getSearchEngineId());
		indexableActionableDynamicQuery.performActions();
	}

	private static final Log log = LogFactoryUtil.getLog(DiscoverIndexer.class);

	@Reference
	DiscoverLocalService discoverLocalService;

	@Reference
	protected IndexWriterHelper indexWriterHelper;
}