package discover.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import discover.model.Gallery;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class GalleryValidator implements ModelValidator<Gallery> {

	@Override
	public void validate(Gallery entry) throws PortalException {
/*   */
        // Field slideId
        validateSlideId(entry.getSlideId());

        // Field discoverId
        validateDiscoverId(entry.getDiscoverId());

        // Field image
        validateImage(entry.getImage());

        // Field thumbnail
        validateThumbnail(entry.getThumbnail());
	}

/*   */
    /**
    * slideId field Validation
    *
    * @param field slideId
    */
    protected void validateSlideId(long field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
    }

    /**
    * discoverId field Validation
    *
    * @param field discoverId
    */
    protected void validateDiscoverId(long field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * image field Validation
    *
    * @param field image
    */
    protected void validateImage(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("gallery-image-required");
        }

    }

    /**
    * thumbnail field Validation
    *
    * @param field thumbnail
    */
    protected void validateThumbnail(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("gallery-thumbnail-required");
        }

    }

/*  */ 	
	

	protected List<String> _errors = new ArrayList<>();

}
