/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service.impl;

import com.liferay.asset.kernel.exception.CategoryNameException;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import discover.exception.CategorySlugException;
import discover.model.Category;
import discover.service.base.CategoryLocalServiceBaseImpl;


/**
 * The implementation of the category local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>events.service.CategoryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CategoryLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=events.model.Category",
	service = AopService.class
)
public class CategoryLocalServiceImpl extends CategoryLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>events.service.CategoryLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>events.service.CategoryLocalServiceUtil</code>.
	 */

	public Category addCategory(long userId,
								Map<Locale, String> nameMap, String slug,
								ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(nameMap, slug);

		long categoryId = counterLocalService.increment();

		Category category = categoryPersistence.create(categoryId);

		category.setUuid(serviceContext.getUuid());
		category.setUserId(userId);
		category.setGroupId(groupId);
		category.setCompanyId(user.getCompanyId());
		category.setUserName(user.getFullName());
		category.setCreateDate(serviceContext.getCreateDate(now));
		category.setModifiedDate(serviceContext.getModifiedDate(now));

		category.setNameMap(nameMap);
		category.setSlug(slug);

		category.setExpandoBridgeAttributes(serviceContext);

		categoryPersistence.update(category);
		categoryPersistence.clearCache();

		return category;
	}

	public Category updateCategory(long userId, long categoryId,
								   Map<Locale, String> nameMap, String slug,
								   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(nameMap, slug);

		Category category = getCategory(categoryId);

		User user = userLocalService.getUser(userId);

		category.setUserId(userId);
		category.setUserName(user.getFullName());
		category.setModifiedDate(serviceContext.getModifiedDate(now));

		category.setNameMap(nameMap);
		category.setSlug(slug);

		category.setExpandoBridgeAttributes(serviceContext);

		categoryPersistence.update(category);
		categoryPersistence.clearCache();

		return category;
	}

	public Category deleteCategory(long categoryId,
								   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Category category = getCategory(categoryId);
		category = deleteCategory(category);

		return category;
	}

	public List<Category> getCategories(long groupId) {

		return categoryPersistence.findByGroupId(groupId);
	}

	public List<Category> getCategories(long groupId, int start, int end,
										OrderByComparator<Category> obc) {

		return categoryPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Category> getCategories(long groupId, int start, int end) {

		return categoryPersistence.findByGroupId(groupId, start, end);
	}

	public int getCategoriesCount(long groupId) {

		return categoryPersistence.countByGroupId(groupId);
	}

	protected void validate(
			Map<Locale, String> nameMap, String slug
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String name = nameMap.get(locale);

		if (Validator.isNull(name)) {
			throw new CategoryNameException();
		}

		if (Validator.isNull(slug)) {
			throw new CategorySlugException();
		}

	}
}