/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Discover}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Discover
 * @generated
 */
public class DiscoverWrapper
	extends BaseModelWrapper<Discover>
	implements Discover, ModelWrapper<Discover> {

	public DiscoverWrapper(Discover discover) {
		super(discover);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("discoverId", getDiscoverId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("listingImage", getListingImage());
		attributes.put("categoryId", getCategoryId());
		attributes.put("orderNo", getOrderNo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long discoverId = (Long)attributes.get("discoverId");

		if (discoverId != null) {
			setDiscoverId(discoverId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String listingImage = (String)attributes.get("listingImage");

		if (listingImage != null) {
			setListingImage(listingImage);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long orderNo = (Long)attributes.get("orderNo");

		if (orderNo != null) {
			setOrderNo(orderNo);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the category ID of this discover.
	 *
	 * @return the category ID of this discover
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this discover.
	 *
	 * @return the company ID of this discover
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this discover.
	 *
	 * @return the create date of this discover
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this discover.
	 *
	 * @return the description of this discover
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this discover in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this discover
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this discover in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this discover. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this discover in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this discover
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this discover in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this discover
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this discover.
	 *
	 * @return the locales and localized descriptions of this discover
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the discover ID of this discover.
	 *
	 * @return the discover ID of this discover
	 */
	@Override
	public long getDiscoverId() {
		return model.getDiscoverId();
	}

	/**
	 * Returns the group ID of this discover.
	 *
	 * @return the group ID of this discover
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the listing image of this discover.
	 *
	 * @return the listing image of this discover
	 */
	@Override
	public String getListingImage() {
		return model.getListingImage();
	}

	/**
	 * Returns the localized listing image of this discover in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing image of this discover
	 */
	@Override
	public String getListingImage(java.util.Locale locale) {
		return model.getListingImage(locale);
	}

	/**
	 * Returns the localized listing image of this discover in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this discover. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingImage(java.util.Locale locale, boolean useDefault) {
		return model.getListingImage(locale, useDefault);
	}

	/**
	 * Returns the localized listing image of this discover in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing image of this discover
	 */
	@Override
	public String getListingImage(String languageId) {
		return model.getListingImage(languageId);
	}

	/**
	 * Returns the localized listing image of this discover in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this discover
	 */
	@Override
	public String getListingImage(String languageId, boolean useDefault) {
		return model.getListingImage(languageId, useDefault);
	}

	@Override
	public String getListingImageCurrentLanguageId() {
		return model.getListingImageCurrentLanguageId();
	}

	@Override
	public String getListingImageCurrentValue() {
		return model.getListingImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing images of this discover.
	 *
	 * @return the locales and localized listing images of this discover
	 */
	@Override
	public Map<java.util.Locale, String> getListingImageMap() {
		return model.getListingImageMap();
	}

	/**
	 * Returns the modified date of this discover.
	 *
	 * @return the modified date of this discover
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the order no of this discover.
	 *
	 * @return the order no of this discover
	 */
	@Override
	public long getOrderNo() {
		return model.getOrderNo();
	}

	/**
	 * Returns the primary key of this discover.
	 *
	 * @return the primary key of this discover
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the title of this discover.
	 *
	 * @return the title of this discover
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this discover in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this discover
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this discover in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this discover. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this discover in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this discover
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this discover in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this discover
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this discover.
	 *
	 * @return the locales and localized titles of this discover
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the user ID of this discover.
	 *
	 * @return the user ID of this discover
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this discover.
	 *
	 * @return the user name of this discover
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this discover.
	 *
	 * @return the user uuid of this discover
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this discover.
	 *
	 * @return the uuid of this discover
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the category ID of this discover.
	 *
	 * @param categoryId the category ID of this discover
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this discover.
	 *
	 * @param companyId the company ID of this discover
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this discover.
	 *
	 * @param createDate the create date of this discover
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this discover.
	 *
	 * @param description the description of this discover
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this discover in the language.
	 *
	 * @param description the localized description of this discover
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this discover in the language, and sets the default locale.
	 *
	 * @param description the localized description of this discover
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this discover from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this discover
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this discover from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this discover
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the discover ID of this discover.
	 *
	 * @param discoverId the discover ID of this discover
	 */
	@Override
	public void setDiscoverId(long discoverId) {
		model.setDiscoverId(discoverId);
	}

	/**
	 * Sets the group ID of this discover.
	 *
	 * @param groupId the group ID of this discover
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the listing image of this discover.
	 *
	 * @param listingImage the listing image of this discover
	 */
	@Override
	public void setListingImage(String listingImage) {
		model.setListingImage(listingImage);
	}

	/**
	 * Sets the localized listing image of this discover in the language.
	 *
	 * @param listingImage the localized listing image of this discover
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingImage(String listingImage, java.util.Locale locale) {
		model.setListingImage(listingImage, locale);
	}

	/**
	 * Sets the localized listing image of this discover in the language, and sets the default locale.
	 *
	 * @param listingImage the localized listing image of this discover
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImage(
		String listingImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingImage(listingImage, locale, defaultLocale);
	}

	@Override
	public void setListingImageCurrentLanguageId(String languageId) {
		model.setListingImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing images of this discover from the map of locales and localized listing images.
	 *
	 * @param listingImageMap the locales and localized listing images of this discover
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap) {

		model.setListingImageMap(listingImageMap);
	}

	/**
	 * Sets the localized listing images of this discover from the map of locales and localized listing images, and sets the default locale.
	 *
	 * @param listingImageMap the locales and localized listing images of this discover
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap,
		java.util.Locale defaultLocale) {

		model.setListingImageMap(listingImageMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this discover.
	 *
	 * @param modifiedDate the modified date of this discover
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the order no of this discover.
	 *
	 * @param orderNo the order no of this discover
	 */
	@Override
	public void setOrderNo(long orderNo) {
		model.setOrderNo(orderNo);
	}

	/**
	 * Sets the primary key of this discover.
	 *
	 * @param primaryKey the primary key of this discover
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the title of this discover.
	 *
	 * @param title the title of this discover
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this discover in the language.
	 *
	 * @param title the localized title of this discover
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this discover in the language, and sets the default locale.
	 *
	 * @param title the localized title of this discover
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this discover from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this discover
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this discover from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this discover
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this discover.
	 *
	 * @param userId the user ID of this discover
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this discover.
	 *
	 * @param userName the user name of this discover
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this discover.
	 *
	 * @param userUuid the user uuid of this discover
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this discover.
	 *
	 * @param uuid the uuid of this discover
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected DiscoverWrapper wrap(Discover discover) {
		return new DiscoverWrapper(discover);
	}

}