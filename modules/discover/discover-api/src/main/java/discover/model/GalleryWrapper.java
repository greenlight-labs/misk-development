/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Gallery}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Gallery
 * @generated
 */
public class GalleryWrapper
	extends BaseModelWrapper<Gallery>
	implements Gallery, ModelWrapper<Gallery> {

	public GalleryWrapper(Gallery gallery) {
		super(gallery);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("slideId", getSlideId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("discoverId", getDiscoverId());
		attributes.put("image", getImage());
		attributes.put("thumbnail", getThumbnail());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long slideId = (Long)attributes.get("slideId");

		if (slideId != null) {
			setSlideId(slideId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long discoverId = (Long)attributes.get("discoverId");

		if (discoverId != null) {
			setDiscoverId(discoverId);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String thumbnail = (String)attributes.get("thumbnail");

		if (thumbnail != null) {
			setThumbnail(thumbnail);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the company ID of this gallery.
	 *
	 * @return the company ID of this gallery
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this gallery.
	 *
	 * @return the create date of this gallery
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the discover ID of this gallery.
	 *
	 * @return the discover ID of this gallery
	 */
	@Override
	public long getDiscoverId() {
		return model.getDiscoverId();
	}

	/**
	 * Returns the group ID of this gallery.
	 *
	 * @return the group ID of this gallery
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the image of this gallery.
	 *
	 * @return the image of this gallery
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the localized image of this gallery in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this gallery
	 */
	@Override
	public String getImage(java.util.Locale locale) {
		return model.getImage(locale);
	}

	/**
	 * Returns the localized image of this gallery in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this gallery. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getImage(java.util.Locale locale, boolean useDefault) {
		return model.getImage(locale, useDefault);
	}

	/**
	 * Returns the localized image of this gallery in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this gallery
	 */
	@Override
	public String getImage(String languageId) {
		return model.getImage(languageId);
	}

	/**
	 * Returns the localized image of this gallery in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this gallery
	 */
	@Override
	public String getImage(String languageId, boolean useDefault) {
		return model.getImage(languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return model.getImageCurrentLanguageId();
	}

	@Override
	public String getImageCurrentValue() {
		return model.getImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized images of this gallery.
	 *
	 * @return the locales and localized images of this gallery
	 */
	@Override
	public Map<java.util.Locale, String> getImageMap() {
		return model.getImageMap();
	}

	/**
	 * Returns the modified date of this gallery.
	 *
	 * @return the modified date of this gallery
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this gallery.
	 *
	 * @return the primary key of this gallery
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the slide ID of this gallery.
	 *
	 * @return the slide ID of this gallery
	 */
	@Override
	public long getSlideId() {
		return model.getSlideId();
	}

	/**
	 * Returns the thumbnail of this gallery.
	 *
	 * @return the thumbnail of this gallery
	 */
	@Override
	public String getThumbnail() {
		return model.getThumbnail();
	}

	/**
	 * Returns the localized thumbnail of this gallery in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized thumbnail of this gallery
	 */
	@Override
	public String getThumbnail(java.util.Locale locale) {
		return model.getThumbnail(locale);
	}

	/**
	 * Returns the localized thumbnail of this gallery in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized thumbnail of this gallery. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getThumbnail(java.util.Locale locale, boolean useDefault) {
		return model.getThumbnail(locale, useDefault);
	}

	/**
	 * Returns the localized thumbnail of this gallery in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized thumbnail of this gallery
	 */
	@Override
	public String getThumbnail(String languageId) {
		return model.getThumbnail(languageId);
	}

	/**
	 * Returns the localized thumbnail of this gallery in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized thumbnail of this gallery
	 */
	@Override
	public String getThumbnail(String languageId, boolean useDefault) {
		return model.getThumbnail(languageId, useDefault);
	}

	@Override
	public String getThumbnailCurrentLanguageId() {
		return model.getThumbnailCurrentLanguageId();
	}

	@Override
	public String getThumbnailCurrentValue() {
		return model.getThumbnailCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized thumbnails of this gallery.
	 *
	 * @return the locales and localized thumbnails of this gallery
	 */
	@Override
	public Map<java.util.Locale, String> getThumbnailMap() {
		return model.getThumbnailMap();
	}

	/**
	 * Returns the user ID of this gallery.
	 *
	 * @return the user ID of this gallery
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this gallery.
	 *
	 * @return the user name of this gallery
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this gallery.
	 *
	 * @return the user uuid of this gallery
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this gallery.
	 *
	 * @return the uuid of this gallery
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the company ID of this gallery.
	 *
	 * @param companyId the company ID of this gallery
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this gallery.
	 *
	 * @param createDate the create date of this gallery
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the discover ID of this gallery.
	 *
	 * @param discoverId the discover ID of this gallery
	 */
	@Override
	public void setDiscoverId(long discoverId) {
		model.setDiscoverId(discoverId);
	}

	/**
	 * Sets the group ID of this gallery.
	 *
	 * @param groupId the group ID of this gallery
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the image of this gallery.
	 *
	 * @param image the image of this gallery
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the localized image of this gallery in the language.
	 *
	 * @param image the localized image of this gallery
	 * @param locale the locale of the language
	 */
	@Override
	public void setImage(String image, java.util.Locale locale) {
		model.setImage(image, locale);
	}

	/**
	 * Sets the localized image of this gallery in the language, and sets the default locale.
	 *
	 * @param image the localized image of this gallery
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImage(
		String image, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setImage(image, locale, defaultLocale);
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		model.setImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized images of this gallery from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this gallery
	 */
	@Override
	public void setImageMap(Map<java.util.Locale, String> imageMap) {
		model.setImageMap(imageMap);
	}

	/**
	 * Sets the localized images of this gallery from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this gallery
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImageMap(
		Map<java.util.Locale, String> imageMap,
		java.util.Locale defaultLocale) {

		model.setImageMap(imageMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this gallery.
	 *
	 * @param modifiedDate the modified date of this gallery
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this gallery.
	 *
	 * @param primaryKey the primary key of this gallery
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the slide ID of this gallery.
	 *
	 * @param slideId the slide ID of this gallery
	 */
	@Override
	public void setSlideId(long slideId) {
		model.setSlideId(slideId);
	}

	/**
	 * Sets the thumbnail of this gallery.
	 *
	 * @param thumbnail the thumbnail of this gallery
	 */
	@Override
	public void setThumbnail(String thumbnail) {
		model.setThumbnail(thumbnail);
	}

	/**
	 * Sets the localized thumbnail of this gallery in the language.
	 *
	 * @param thumbnail the localized thumbnail of this gallery
	 * @param locale the locale of the language
	 */
	@Override
	public void setThumbnail(String thumbnail, java.util.Locale locale) {
		model.setThumbnail(thumbnail, locale);
	}

	/**
	 * Sets the localized thumbnail of this gallery in the language, and sets the default locale.
	 *
	 * @param thumbnail the localized thumbnail of this gallery
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setThumbnail(
		String thumbnail, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setThumbnail(thumbnail, locale, defaultLocale);
	}

	@Override
	public void setThumbnailCurrentLanguageId(String languageId) {
		model.setThumbnailCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized thumbnails of this gallery from the map of locales and localized thumbnails.
	 *
	 * @param thumbnailMap the locales and localized thumbnails of this gallery
	 */
	@Override
	public void setThumbnailMap(Map<java.util.Locale, String> thumbnailMap) {
		model.setThumbnailMap(thumbnailMap);
	}

	/**
	 * Sets the localized thumbnails of this gallery from the map of locales and localized thumbnails, and sets the default locale.
	 *
	 * @param thumbnailMap the locales and localized thumbnails of this gallery
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setThumbnailMap(
		Map<java.util.Locale, String> thumbnailMap,
		java.util.Locale defaultLocale) {

		model.setThumbnailMap(thumbnailMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this gallery.
	 *
	 * @param userId the user ID of this gallery
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this gallery.
	 *
	 * @param userName the user name of this gallery
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this gallery.
	 *
	 * @param userUuid the user uuid of this gallery
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this gallery.
	 *
	 * @param uuid the uuid of this gallery
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected GalleryWrapper wrap(Gallery gallery) {
		return new GalleryWrapper(gallery);
	}

}