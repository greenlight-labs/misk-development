/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import discover.model.Discover;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Discover. This utility wraps
 * <code>discover.service.impl.DiscoverLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DiscoverLocalService
 * @generated
 */
public class DiscoverLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>discover.service.impl.DiscoverLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the discover to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was added
	 */
	public static Discover addDiscover(Discover discover) {
		return getService().addDiscover(discover);
	}

	public static Discover addEntry(
			Discover orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws discover.exception.DiscoverValidateException, PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static void addEntryGallery(
			Discover orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryGallery(orgEntry, request);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * Creates a new discover with the primary key. Does not add the discover to the database.
	 *
	 * @param discoverId the primary key for the new discover
	 * @return the new discover
	 */
	public static Discover createDiscover(long discoverId) {
		return getService().createDiscover(discoverId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the discover from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was removed
	 */
	public static Discover deleteDiscover(Discover discover) {
		return getService().deleteDiscover(discover);
	}

	/**
	 * Deletes the discover with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover that was removed
	 * @throws PortalException if a discover with the primary key could not be found
	 */
	public static Discover deleteDiscover(long discoverId)
		throws PortalException {

		return getService().deleteDiscover(discoverId);
	}

	public static Discover deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Discover fetchDiscover(long discoverId) {
		return getService().fetchDiscover(discoverId);
	}

	/**
	 * Returns the discover matching the UUID and group.
	 *
	 * @param uuid the discover's UUID
	 * @param groupId the primary key of the group
	 * @return the matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchDiscoverByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchDiscoverByUuidAndGroupId(uuid, groupId);
	}

	public static List<Discover> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<Discover> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<Discover> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Discover> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the discover with the primary key.
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover
	 * @throws PortalException if a discover with the primary key could not be found
	 */
	public static Discover getDiscover(long discoverId) throws PortalException {
		return getService().getDiscover(discoverId);
	}

	/**
	 * Returns the discover matching the UUID and group.
	 *
	 * @param uuid the discover's UUID
	 * @param groupId the primary key of the group
	 * @return the matching discover
	 * @throws PortalException if a matching discover could not be found
	 */
	public static Discover getDiscoverByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getDiscoverByUuidAndGroupId(uuid, groupId);
	}

	public static Discover getDiscoverFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws discover.exception.DiscoverValidateException,
			   javax.portlet.PortletException {

		return getService().getDiscoverFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the discovers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @return the range of discovers
	 */
	public static List<Discover> getDiscovers(int start, int end) {
		return getService().getDiscovers(start, end);
	}

	/**
	 * Returns all the discovers matching the UUID and company.
	 *
	 * @param uuid the UUID of the discovers
	 * @param companyId the primary key of the company
	 * @return the matching discovers, or an empty list if no matches were found
	 */
	public static List<Discover> getDiscoversByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getDiscoversByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of discovers matching the UUID and company.
	 *
	 * @param uuid the UUID of the discovers
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching discovers, or an empty list if no matches were found
	 */
	public static List<Discover> getDiscoversByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Discover> orderByComparator) {

		return getService().getDiscoversByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of discovers.
	 *
	 * @return the number of discovers
	 */
	public static int getDiscoversCount() {
		return getService().getDiscoversCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static List<discover.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getGalleries(actionRequest);
	}

	public static List<discover.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		List<discover.model.Gallery> defaultGalleries) {

		return getService().getGalleries(actionRequest, defaultGalleries);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Discover getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * search
	 */
	public static List<Discover> searchDiscovers(
		com.liferay.portal.kernel.search.SearchContext searchContext) {

		return getService().searchDiscovers(searchContext);
	}

	/**
	 * Updates the discover in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was updated
	 */
	public static Discover updateDiscover(Discover discover) {
		return getService().updateDiscover(discover);
	}

	public static Discover updateEntry(
			Discover orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws discover.exception.DiscoverValidateException, PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static void updateGalleries(
			Discover entry, List<discover.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateGalleries(entry, galleries, serviceContext);
	}

	public static DiscoverLocalService getService() {
		return _service;
	}

	private static volatile DiscoverLocalService _service;

}