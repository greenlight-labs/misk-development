/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import discover.exception.DiscoverValidateException;

import discover.model.Discover;
import discover.model.Gallery;

import java.io.Serializable;

import java.util.*;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Discover. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see DiscoverLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface DiscoverLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>discover.service.impl.DiscoverLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the discover local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link DiscoverLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the discover to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Discover addDiscover(Discover discover);

	public Discover addEntry(Discover orgEntry, ServiceContext serviceContext)
		throws DiscoverValidateException, PortalException;

	public void addEntryGallery(Discover orgEntry, PortletRequest request)
		throws PortletException;

	public int countAllInGroup(long groupId);

	/**
	 * Creates a new discover with the primary key. Does not add the discover to the database.
	 *
	 * @param discoverId the primary key for the new discover
	 * @return the new discover
	 */
	@Transactional(enabled = false)
	public Discover createDiscover(long discoverId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the discover from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Discover deleteDiscover(Discover discover);

	/**
	 * Deletes the discover with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover that was removed
	 * @throws PortalException if a discover with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Discover deleteDiscover(long discoverId) throws PortalException;

	public Discover deleteEntry(long primaryKey) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Discover fetchDiscover(long discoverId);

	/**
	 * Returns the discover matching the UUID and group.
	 *
	 * @param uuid the discover's UUID
	 * @param groupId the primary key of the group
	 * @return the matching discover, or <code>null</code> if a matching discover could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Discover fetchDiscoverByUuidAndGroupId(String uuid, long groupId);

	public List<Discover> findAllInGroup(long groupId);

	public List<Discover> findAllInGroup(long groupId, int start, int end);

	public List<Discover> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Discover> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the discover with the primary key.
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover
	 * @throws PortalException if a discover with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Discover getDiscover(long discoverId) throws PortalException;

	/**
	 * Returns the discover matching the UUID and group.
	 *
	 * @param uuid the discover's UUID
	 * @param groupId the primary key of the group
	 * @return the matching discover
	 * @throws PortalException if a matching discover could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Discover getDiscoverByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Discover getDiscoverFromRequest(
			long primaryKey, PortletRequest request)
		throws DiscoverValidateException, PortletException;

	/**
	 * Returns a range of all the discovers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @return the range of discovers
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Discover> getDiscovers(int start, int end);

	/**
	 * Returns all the discovers matching the UUID and company.
	 *
	 * @param uuid the UUID of the discovers
	 * @param companyId the primary key of the company
	 * @return the matching discovers, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Discover> getDiscoversByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of discovers matching the UUID and company.
	 *
	 * @param uuid the UUID of the discovers
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching discovers, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Discover> getDiscoversByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Discover> orderByComparator);

	/**
	 * Returns the number of discovers.
	 *
	 * @return the number of discovers
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getDiscoversCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallery> getGalleries(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallery> getGalleries(
		ActionRequest actionRequest, List<Gallery> defaultGalleries);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Discover getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * search
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Discover> searchDiscovers(SearchContext searchContext);

	/**
	 * Updates the discover in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Discover updateDiscover(Discover discover);

	public Discover updateEntry(
			Discover orgEntry, ServiceContext serviceContext)
		throws DiscoverValidateException, PortalException;

	public void updateGalleries(
			Discover entry, List<Gallery> galleries,
			ServiceContext serviceContext)
		throws PortalException;

}