/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import discover.model.Discover;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the discover service. This utility wraps <code>discover.service.persistence.impl.DiscoverPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DiscoverPersistence
 * @generated
 */
public class DiscoverUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Discover discover) {
		getPersistence().clearCache(discover);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Discover> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Discover> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Discover> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Discover> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Discover> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Discover update(Discover discover) {
		return getPersistence().update(discover);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Discover update(
		Discover discover, ServiceContext serviceContext) {

		return getPersistence().update(discover, serviceContext);
	}

	/**
	 * Returns all the discovers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching discovers
	 */
	public static List<Discover> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the discovers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @return the range of matching discovers
	 */
	public static List<Discover> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the discovers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching discovers
	 */
	public static List<Discover> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Discover> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the discovers where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching discovers
	 */
	public static List<Discover> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Discover> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first discover in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching discover
	 * @throws NoSuchDiscoverException if a matching discover could not be found
	 */
	public static Discover findByUuid_First(
			String uuid, OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first discover in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByUuid_First(
		String uuid, OrderByComparator<Discover> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last discover in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching discover
	 * @throws NoSuchDiscoverException if a matching discover could not be found
	 */
	public static Discover findByUuid_Last(
			String uuid, OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last discover in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByUuid_Last(
		String uuid, OrderByComparator<Discover> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the discovers before and after the current discover in the ordered set where uuid = &#63;.
	 *
	 * @param discoverId the primary key of the current discover
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next discover
	 * @throws NoSuchDiscoverException if a discover with the primary key could not be found
	 */
	public static Discover[] findByUuid_PrevAndNext(
			long discoverId, String uuid,
			OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByUuid_PrevAndNext(
			discoverId, uuid, orderByComparator);
	}

	/**
	 * Removes all the discovers where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of discovers where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching discovers
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the discover where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDiscoverException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching discover
	 * @throws NoSuchDiscoverException if a matching discover could not be found
	 */
	public static Discover findByUUID_G(String uuid, long groupId)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the discover where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the discover where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the discover where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the discover that was removed
	 */
	public static Discover removeByUUID_G(String uuid, long groupId)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of discovers where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching discovers
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the discovers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching discovers
	 */
	public static List<Discover> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the discovers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @return the range of matching discovers
	 */
	public static List<Discover> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the discovers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching discovers
	 */
	public static List<Discover> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Discover> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the discovers where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching discovers
	 */
	public static List<Discover> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Discover> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first discover in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching discover
	 * @throws NoSuchDiscoverException if a matching discover could not be found
	 */
	public static Discover findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first discover in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Discover> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last discover in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching discover
	 * @throws NoSuchDiscoverException if a matching discover could not be found
	 */
	public static Discover findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last discover in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Discover> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the discovers before and after the current discover in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param discoverId the primary key of the current discover
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next discover
	 * @throws NoSuchDiscoverException if a discover with the primary key could not be found
	 */
	public static Discover[] findByUuid_C_PrevAndNext(
			long discoverId, String uuid, long companyId,
			OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByUuid_C_PrevAndNext(
			discoverId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the discovers where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of discovers where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching discovers
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the discovers where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching discovers
	 */
	public static List<Discover> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the discovers where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @return the range of matching discovers
	 */
	public static List<Discover> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the discovers where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching discovers
	 */
	public static List<Discover> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Discover> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the discovers where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching discovers
	 */
	public static List<Discover> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Discover> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first discover in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching discover
	 * @throws NoSuchDiscoverException if a matching discover could not be found
	 */
	public static Discover findByGroupId_First(
			long groupId, OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first discover in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByGroupId_First(
		long groupId, OrderByComparator<Discover> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last discover in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching discover
	 * @throws NoSuchDiscoverException if a matching discover could not be found
	 */
	public static Discover findByGroupId_Last(
			long groupId, OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last discover in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching discover, or <code>null</code> if a matching discover could not be found
	 */
	public static Discover fetchByGroupId_Last(
		long groupId, OrderByComparator<Discover> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the discovers before and after the current discover in the ordered set where groupId = &#63;.
	 *
	 * @param discoverId the primary key of the current discover
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next discover
	 * @throws NoSuchDiscoverException if a discover with the primary key could not be found
	 */
	public static Discover[] findByGroupId_PrevAndNext(
			long discoverId, long groupId,
			OrderByComparator<Discover> orderByComparator)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByGroupId_PrevAndNext(
			discoverId, groupId, orderByComparator);
	}

	/**
	 * Removes all the discovers where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of discovers where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching discovers
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Caches the discover in the entity cache if it is enabled.
	 *
	 * @param discover the discover
	 */
	public static void cacheResult(Discover discover) {
		getPersistence().cacheResult(discover);
	}

	/**
	 * Caches the discovers in the entity cache if it is enabled.
	 *
	 * @param discovers the discovers
	 */
	public static void cacheResult(List<Discover> discovers) {
		getPersistence().cacheResult(discovers);
	}

	/**
	 * Creates a new discover with the primary key. Does not add the discover to the database.
	 *
	 * @param discoverId the primary key for the new discover
	 * @return the new discover
	 */
	public static Discover create(long discoverId) {
		return getPersistence().create(discoverId);
	}

	/**
	 * Removes the discover with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover that was removed
	 * @throws NoSuchDiscoverException if a discover with the primary key could not be found
	 */
	public static Discover remove(long discoverId)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().remove(discoverId);
	}

	public static Discover updateImpl(Discover discover) {
		return getPersistence().updateImpl(discover);
	}

	/**
	 * Returns the discover with the primary key or throws a <code>NoSuchDiscoverException</code> if it could not be found.
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover
	 * @throws NoSuchDiscoverException if a discover with the primary key could not be found
	 */
	public static Discover findByPrimaryKey(long discoverId)
		throws discover.exception.NoSuchDiscoverException {

		return getPersistence().findByPrimaryKey(discoverId);
	}

	/**
	 * Returns the discover with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover, or <code>null</code> if a discover with the primary key could not be found
	 */
	public static Discover fetchByPrimaryKey(long discoverId) {
		return getPersistence().fetchByPrimaryKey(discoverId);
	}

	/**
	 * Returns all the discovers.
	 *
	 * @return the discovers
	 */
	public static List<Discover> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the discovers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @return the range of discovers
	 */
	public static List<Discover> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the discovers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of discovers
	 */
	public static List<Discover> findAll(
		int start, int end, OrderByComparator<Discover> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the discovers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of discovers
	 */
	public static List<Discover> findAll(
		int start, int end, OrderByComparator<Discover> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the discovers from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of discovers.
	 *
	 * @return the number of discovers
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static DiscoverPersistence getPersistence() {
		return _persistence;
	}

	private static volatile DiscoverPersistence _persistence;

}