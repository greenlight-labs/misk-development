/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DiscoverService}.
 *
 * @author Brian Wing Shun Chan
 * @see DiscoverService
 * @generated
 */
public class DiscoverServiceWrapper
	implements DiscoverService, ServiceWrapper<DiscoverService> {

	public DiscoverServiceWrapper(DiscoverService discoverService) {
		_discoverService = discoverService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _discoverService.getOSGiServiceIdentifier();
	}

	@Override
	public DiscoverService getWrappedService() {
		return _discoverService;
	}

	@Override
	public void setWrappedService(DiscoverService discoverService) {
		_discoverService = discoverService;
	}

	private DiscoverService _discoverService;

}