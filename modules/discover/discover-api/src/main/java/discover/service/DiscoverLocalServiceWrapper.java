/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DiscoverLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DiscoverLocalService
 * @generated
 */
public class DiscoverLocalServiceWrapper
	implements DiscoverLocalService, ServiceWrapper<DiscoverLocalService> {

	public DiscoverLocalServiceWrapper(
		DiscoverLocalService discoverLocalService) {

		_discoverLocalService = discoverLocalService;
	}

	/**
	 * Adds the discover to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was added
	 */
	@Override
	public discover.model.Discover addDiscover(
		discover.model.Discover discover) {

		return _discoverLocalService.addDiscover(discover);
	}

	@Override
	public discover.model.Discover addEntry(
			discover.model.Discover orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   discover.exception.DiscoverValidateException {

		return _discoverLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public void addEntryGallery(
			discover.model.Discover orgEntry,
			javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		_discoverLocalService.addEntryGallery(orgEntry, request);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _discoverLocalService.countAllInGroup(groupId);
	}

	/**
	 * Creates a new discover with the primary key. Does not add the discover to the database.
	 *
	 * @param discoverId the primary key for the new discover
	 * @return the new discover
	 */
	@Override
	public discover.model.Discover createDiscover(long discoverId) {
		return _discoverLocalService.createDiscover(discoverId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _discoverLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the discover from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was removed
	 */
	@Override
	public discover.model.Discover deleteDiscover(
		discover.model.Discover discover) {

		return _discoverLocalService.deleteDiscover(discover);
	}

	/**
	 * Deletes the discover with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover that was removed
	 * @throws PortalException if a discover with the primary key could not be found
	 */
	@Override
	public discover.model.Discover deleteDiscover(long discoverId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _discoverLocalService.deleteDiscover(discoverId);
	}

	@Override
	public discover.model.Discover deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _discoverLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _discoverLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _discoverLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _discoverLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _discoverLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _discoverLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _discoverLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _discoverLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public discover.model.Discover fetchDiscover(long discoverId) {
		return _discoverLocalService.fetchDiscover(discoverId);
	}

	/**
	 * Returns the discover matching the UUID and group.
	 *
	 * @param uuid the discover's UUID
	 * @param groupId the primary key of the group
	 * @return the matching discover, or <code>null</code> if a matching discover could not be found
	 */
	@Override
	public discover.model.Discover fetchDiscoverByUuidAndGroupId(
		String uuid, long groupId) {

		return _discoverLocalService.fetchDiscoverByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<discover.model.Discover> findAllInGroup(
		long groupId) {

		return _discoverLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<discover.model.Discover> findAllInGroup(
		long groupId, int start, int end) {

		return _discoverLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<discover.model.Discover> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<discover.model.Discover> obc) {

		return _discoverLocalService.findAllInGroup(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _discoverLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the discover with the primary key.
	 *
	 * @param discoverId the primary key of the discover
	 * @return the discover
	 * @throws PortalException if a discover with the primary key could not be found
	 */
	@Override
	public discover.model.Discover getDiscover(long discoverId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _discoverLocalService.getDiscover(discoverId);
	}

	/**
	 * Returns the discover matching the UUID and group.
	 *
	 * @param uuid the discover's UUID
	 * @param groupId the primary key of the group
	 * @return the matching discover
	 * @throws PortalException if a matching discover could not be found
	 */
	@Override
	public discover.model.Discover getDiscoverByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _discoverLocalService.getDiscoverByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public discover.model.Discover getDiscoverFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws discover.exception.DiscoverValidateException,
			   javax.portlet.PortletException {

		return _discoverLocalService.getDiscoverFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the discovers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>discover.model.impl.DiscoverModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @return the range of discovers
	 */
	@Override
	public java.util.List<discover.model.Discover> getDiscovers(
		int start, int end) {

		return _discoverLocalService.getDiscovers(start, end);
	}

	/**
	 * Returns all the discovers matching the UUID and company.
	 *
	 * @param uuid the UUID of the discovers
	 * @param companyId the primary key of the company
	 * @return the matching discovers, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<discover.model.Discover>
		getDiscoversByUuidAndCompanyId(String uuid, long companyId) {

		return _discoverLocalService.getDiscoversByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of discovers matching the UUID and company.
	 *
	 * @param uuid the UUID of the discovers
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of discovers
	 * @param end the upper bound of the range of discovers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching discovers, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<discover.model.Discover>
		getDiscoversByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<discover.model.Discover> orderByComparator) {

		return _discoverLocalService.getDiscoversByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of discovers.
	 *
	 * @return the number of discovers
	 */
	@Override
	public int getDiscoversCount() {
		return _discoverLocalService.getDiscoversCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _discoverLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public java.util.List<discover.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return _discoverLocalService.getGalleries(actionRequest);
	}

	@Override
	public java.util.List<discover.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		java.util.List<discover.model.Gallery> defaultGalleries) {

		return _discoverLocalService.getGalleries(
			actionRequest, defaultGalleries);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _discoverLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public discover.model.Discover getNewObject(long primaryKey) {
		return _discoverLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _discoverLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _discoverLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * search
	 */
	@Override
	public java.util.List<discover.model.Discover> searchDiscovers(
		com.liferay.portal.kernel.search.SearchContext searchContext) {

		return _discoverLocalService.searchDiscovers(searchContext);
	}

	/**
	 * Updates the discover in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DiscoverLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param discover the discover
	 * @return the discover that was updated
	 */
	@Override
	public discover.model.Discover updateDiscover(
		discover.model.Discover discover) {

		return _discoverLocalService.updateDiscover(discover);
	}

	@Override
	public discover.model.Discover updateEntry(
			discover.model.Discover orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   discover.exception.DiscoverValidateException {

		return _discoverLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public void updateGalleries(
			discover.model.Discover entry,
			java.util.List<discover.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_discoverLocalService.updateGalleries(entry, galleries, serviceContext);
	}

	@Override
	public DiscoverLocalService getWrappedService() {
		return _discoverLocalService;
	}

	@Override
	public void setWrappedService(DiscoverLocalService discoverLocalService) {
		_discoverLocalService = discoverLocalService;
	}

	private DiscoverLocalService _discoverLocalService;

}