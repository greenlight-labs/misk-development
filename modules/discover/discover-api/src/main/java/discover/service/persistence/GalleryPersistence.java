/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package discover.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import discover.exception.NoSuchGalleryException;

import discover.model.Gallery;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the gallery service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GalleryUtil
 * @generated
 */
@ProviderType
public interface GalleryPersistence extends BasePersistence<Gallery> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link GalleryUtil} to access the gallery persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching galleries
	 */
	public java.util.List<Gallery> findByUuid(String uuid);

	/**
	 * Returns a range of all the galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public java.util.List<Gallery> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public Gallery[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Removes all the galleries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching galleries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the gallery where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchGalleryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByUUID_G(String uuid, long groupId)
		throws NoSuchGalleryException;

	/**
	 * Returns the gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the gallery where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the gallery that was removed
	 */
	public Gallery removeByUUID_G(String uuid, long groupId)
		throws NoSuchGalleryException;

	/**
	 * Returns the number of galleries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching galleries
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching galleries
	 */
	public java.util.List<Gallery> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public java.util.List<Gallery> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public Gallery[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Removes all the galleries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching galleries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching galleries
	 */
	public java.util.List<Gallery> findByGroupId(long groupId);

	/**
	 * Returns a range of all the galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public java.util.List<Gallery> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the first gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the last gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the last gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where groupId = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public Gallery[] findByGroupId_PrevAndNext(
			long slideId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Removes all the galleries where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching galleries
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the galleries where discoverId = &#63;.
	 *
	 * @param discoverId the discover ID
	 * @return the matching galleries
	 */
	public java.util.List<Gallery> findByDiscoverId(long discoverId);

	/**
	 * Returns a range of all the galleries where discoverId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param discoverId the discover ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public java.util.List<Gallery> findByDiscoverId(
		long discoverId, int start, int end);

	/**
	 * Returns an ordered range of all the galleries where discoverId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param discoverId the discover ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByDiscoverId(
		long discoverId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the galleries where discoverId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param discoverId the discover ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public java.util.List<Gallery> findByDiscoverId(
		long discoverId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first gallery in the ordered set where discoverId = &#63;.
	 *
	 * @param discoverId the discover ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByDiscoverId_First(
			long discoverId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the first gallery in the ordered set where discoverId = &#63;.
	 *
	 * @param discoverId the discover ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByDiscoverId_First(
		long discoverId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the last gallery in the ordered set where discoverId = &#63;.
	 *
	 * @param discoverId the discover ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public Gallery findByDiscoverId_Last(
			long discoverId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Returns the last gallery in the ordered set where discoverId = &#63;.
	 *
	 * @param discoverId the discover ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public Gallery fetchByDiscoverId_Last(
		long discoverId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where discoverId = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param discoverId the discover ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public Gallery[] findByDiscoverId_PrevAndNext(
			long slideId, long discoverId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallery>
				orderByComparator)
		throws NoSuchGalleryException;

	/**
	 * Removes all the galleries where discoverId = &#63; from the database.
	 *
	 * @param discoverId the discover ID
	 */
	public void removeByDiscoverId(long discoverId);

	/**
	 * Returns the number of galleries where discoverId = &#63;.
	 *
	 * @param discoverId the discover ID
	 * @return the number of matching galleries
	 */
	public int countByDiscoverId(long discoverId);

	/**
	 * Caches the gallery in the entity cache if it is enabled.
	 *
	 * @param gallery the gallery
	 */
	public void cacheResult(Gallery gallery);

	/**
	 * Caches the galleries in the entity cache if it is enabled.
	 *
	 * @param galleries the galleries
	 */
	public void cacheResult(java.util.List<Gallery> galleries);

	/**
	 * Creates a new gallery with the primary key. Does not add the gallery to the database.
	 *
	 * @param slideId the primary key for the new gallery
	 * @return the new gallery
	 */
	public Gallery create(long slideId);

	/**
	 * Removes the gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery that was removed
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public Gallery remove(long slideId) throws NoSuchGalleryException;

	public Gallery updateImpl(Gallery gallery);

	/**
	 * Returns the gallery with the primary key or throws a <code>NoSuchGalleryException</code> if it could not be found.
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public Gallery findByPrimaryKey(long slideId) throws NoSuchGalleryException;

	/**
	 * Returns the gallery with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery, or <code>null</code> if a gallery with the primary key could not be found
	 */
	public Gallery fetchByPrimaryKey(long slideId);

	/**
	 * Returns all the galleries.
	 *
	 * @return the galleries
	 */
	public java.util.List<Gallery> findAll();

	/**
	 * Returns a range of all the galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of galleries
	 */
	public java.util.List<Gallery> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of galleries
	 */
	public java.util.List<Gallery> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of galleries
	 */
	public java.util.List<Gallery> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the galleries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of galleries.
	 *
	 * @return the number of galleries
	 */
	public int countAll();

}