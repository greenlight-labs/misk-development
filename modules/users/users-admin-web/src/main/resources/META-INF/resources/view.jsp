<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="misk.app.users.model.AppUser"%>
<%@page import="misk.app.users.service.AppUserLocalServiceUtil"%>
<%@ include file="/init.jsp"%>


<liferay-portlet:resourceURL id="exportAppUser" var="exportURL">	
</liferay-portlet:resourceURL>


<div class="container-fluid w-75">


	<div class="row pt-md-3 pb-md-3">
		<div class="col">
			<h1>App Users List</h1>
		</div>
		<div class="col text-right">
			<aui:button onClick="<%=exportURL.toString() %>" value="Export all App Users">
			</aui:button>
		</div>
	</div>
	<liferay-ui:search-container
		total="<%=AppUserLocalServiceUtil.countAllInGroup(scopeGroupId)%>">
		<liferay-ui:search-container-results
			results="<%=AppUserLocalServiceUtil.findAllInGroup(scopeGroupId, searchContainer.getStart(),
						searchContainer.getEnd())%>" />

		<liferay-ui:search-container-row
			className="misk.app.users.model.AppUser" modelVar="appUser"
			keyProperty="appUserId">
			<portlet:renderURL var="viewAppUserDetailURL"
				windowState="<%=LiferayWindowState.POP_UP.toString()%>">
				<portlet:param name="mvcRenderCommandName"
					value="viewAppUserDetails" />
				<portlet:param name="appUserId"
					value="<%=String.valueOf(appUser.getAppUserId())%>" />
			</portlet:renderURL>


			<liferay-ui:search-container-column-text name="App User Id">
				<a href="javascript:void(0)"
					onclick="showPopup('<%=viewAppUserDetailURL.toString()%>')";"><%=appUser.getAppUserId()%></a>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="Full Name">

				<a href="javascript:void(0)"
					onclick="showPopup('<%=viewAppUserDetailURL.toString()%>')";"><%=appUser.getFullName()%></a>
			</liferay-ui:search-container-column-text>
			
			<liferay-ui:search-container-column-text name="Email Address">
			<a href="javascript:void(0)"
					onclick="showPopup('<%=viewAppUserDetailURL.toString()%>')";">	<%=appUser.getEmailAddress()%></a>
			</liferay-ui:search-container-column-text>
			<liferay-ui:search-container-column-text name="Phone Number">
				<a href="javascript:void(0)"
					onclick="showPopup('<%=viewAppUserDetailURL.toString()%>')";"><%=appUser.getPhoneNumber()%></a>
			</liferay-ui:search-container-column-text>
			<liferay-ui:search-container-column-text name="Deactivate Reason"
				value="<%=String.valueOf(appUser.getDeactivateReason())%>" />
			<liferay-ui:search-container-column-text name="Status"
				value="<%=appUser.getStatus() == true ? "Active" : "In-Active"%>" />
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator />
	</liferay-ui:search-container>
</div>

<aui:script>

 function showPopup(url)
{
console.log("App User id --> "+ url);
    Liferay.Util.openWindow(
            {
                dialog: {
                    cache: false,
                    width:550,
                    height:600,
                    centered: true,                 
                    resizable: true,                   
                    modal: true,
                    destroyOnClose: true,
                },
                id: 'AppUser',
                 title: "App User Details",            
                uri: url
            }
    );    
}
</aui:script>