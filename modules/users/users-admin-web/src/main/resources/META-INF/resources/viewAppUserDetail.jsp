<%@page import="misk.app.users.model.AppUser" %>
<%@ include file="/init.jsp" %>
<%
    AppUser appUserObj = (AppUser) request.getAttribute("appUser");
%>
<table class="table table-striped">
    <tbody>
    <%
		String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
		String baseUrlWithContextPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    %>
    <tr>
        <td colspan="2" class="text-center">
			<%
				if(appUserObj.getProfileImage() != null && !appUserObj.getProfileImage().isEmpty()) {
			%>
            	<img src="<%=baseUrl+"/"+appUserObj.getProfileImage()%>" alt="Profile Image" class="img-circle" width="150" height="150">
			<%
				}else{
			%>
				<img src="<%=baseUrlWithContextPath%>/images/default-profile.jpg" alt="Profile Image" class="img-circle" width="150" height="150">
			<%
				}
			%>
        </td>
    </tr>
    <tr>
        <td>Full Name</td>
        <td><%=appUserObj.getFullName()%>
        </td>
    </tr>
    <tr>
        <td>App User Id</td>
        <td><%=appUserObj.getAppUserId()%>
        </td>
    </tr>
    <tr>
        <td>Email Address</td>
        <td><%=appUserObj.getEmailAddress()%>
        </td>
    </tr>
    <tr>
        <td>Phone Number</td>
        <td><%=appUserObj.getPhoneNumber()%>
        </td>
    </tr>
    <tr>
        <td>Facebook</td>
        <td><%=appUserObj.getFacebookId()%>
        </td>
    </tr>
    <tr>
        <td>Google User Id</td>
        <td><%=appUserObj.getGoogleUserId()%>
        </td>
    </tr>
    <tr>
        <td>Apple User Id</td>
        <td><%=appUserObj.getAppleUserId()%>
        </td>
    </tr>
    <tr>
        <td>status</td>
        <td><%=appUserObj.getStatus() == true ? "Active" : "In-Active"%>
        </td>
    </tr>
    <tr>
        <td>Deactivate Reason</td>
        <td><%=appUserObj.getDeactivateReason()%>
        </td>
    </tr>
    </tbody>
</table>