package users.admin.web.constants;

/**
 * @author manis
 */
public class UsersAdminWebPortletKeys {

	public static final String USERSADMINWEB =
		"users_admin_web_UsersAdminWebPortlet";
	public static final String VIEW_APP_USER =
			"viewAppUserDetails";
	public static final String EXPORT_APP_USER ="exportAppUser";
}