package users.admin.web.portlet.action;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CSVUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.List;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalServiceUtil;
import users.admin.web.constants.UsersAdminWebPortletKeys;

@Component(immediate = true, property = { "javax.portlet.name=" + UsersAdminWebPortletKeys.USERSADMINWEB,
		"mvc.command.name=" + UsersAdminWebPortletKeys.EXPORT_APP_USER }, service = MVCResourceCommand.class)
public class ExportAppUserMVCResourceCommand extends BaseMVCResourceCommand {

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception {

		try {
			SessionMessages.add(resourceRequest,
					_portal.getPortletId(resourceRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

			String csv = getAppUserListCSV(resourceRequest, resourceResponse);

			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, "app_user_list.csv", csv.getBytes(),
					ContentTypes.TEXT_CSV_UTF8);
		} catch (Exception exception) {
			SessionErrors.add(resourceRequest, exception.getClass());

			_log.error(exception, exception);
		}
	}

	public String getAppUserListCSV(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {
		StringBuffer sb = new StringBuffer();
		int count = 1;
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<AppUser> appUserList = AppUserLocalServiceUtil.getAppUsers(-1, -1);
		sb = generateCSVHeaders(sb);
		for (AppUser appUser : appUserList) {

			sb.append(CSVUtil.encode(count++));
			sb.append(StringPool.COMMA);
			sb.append(CSVUtil.encode(appUser.getAppUserId()));
			sb.append(StringPool.COMMA);
			sb.append(CSVUtil.encode(appUser.getFullName()));
			sb.append(StringPool.COMMA);
			sb.append(CSVUtil.encode(appUser.getEmailAddress()));
			sb.append(StringPool.COMMA);

			sb.append(CSVUtil.encode(appUser.getPhoneNumber()));
			sb.append(StringPool.COMMA);

			sb.append(CSVUtil.encode(appUser.getFacebookId()));
			sb.append(StringPool.COMMA);

			sb.append(CSVUtil.encode(appUser.getGoogleUserId()));
			sb.append(StringPool.COMMA);

			sb.append(CSVUtil.encode(appUser.getAppleUserId()));
			sb.append(StringPool.COMMA);

			sb.append(CSVUtil.encode(appUser.getStatus() == true ? "Active" : "In-Active"));
			sb.append(StringPool.COMMA);

			sb.append(CSVUtil.encode(appUser.getDeactivateReason()));
			sb.append(StringPool.COMMA);

			sb.append(StringPool.NEW_LINE);

		}
		return sb.toString();

	}

	public AppUser getAppUserName(long appUserId, ThemeDisplay themeDisplay) {
		AppUser appUserObj = null;
		try {
			appUserObj = AppUserLocalServiceUtil.getAppUser(appUserId);
		} catch (PortalException e) {
		}
		return appUserObj;
	}

	public StringBuffer generateCSVHeaders(StringBuffer sb) {
		sb.append(CSVUtil.encode("S.NO"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("App User Id"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Full Name"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Email Address"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Phone Number"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Facebook Id"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Google User Id"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Apple  User Id"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Status"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Deactivate Reason"));
		sb.append(StringPool.COMMA);
		sb.append(StringPool.NEW_LINE);
		return sb;

	}

	private static final Log _log = LogFactoryUtil.getLog(ExportAppUserMVCResourceCommand.class);

	@Reference
	private Portal _portal;
}
