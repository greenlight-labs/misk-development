package users.admin.web.portlet.action;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalServiceUtil;
import users.admin.web.constants.UsersAdminWebPortletKeys;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + UsersAdminWebPortletKeys.USERSADMINWEB,
                "mvc.command.name=" + UsersAdminWebPortletKeys.VIEW_APP_USER
        },
        service = MVCRenderCommand.class
)
public class ViewAppUserMVCRenderCommand  implements MVCRenderCommand {
	 @Override
	    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		
		 String appUserId = ParamUtil.getString(renderRequest, "appUserId");
		 AppUser appUser=null;
		 try {
			appUser=AppUserLocalServiceUtil.getAppUser(Long.valueOf(appUserId));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 renderRequest.setAttribute("appUser", appUser);
		 return "/viewAppUserDetail.jsp";
	 }
}
