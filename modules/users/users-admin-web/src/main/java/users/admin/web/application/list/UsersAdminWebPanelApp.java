package users.admin.web.application.list;

import users.admin.web.constants.UsersAdminWebPanelCategoryKeys;
import users.admin.web.constants.UsersAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author manis
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + UsersAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class UsersAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return UsersAdminWebPortletKeys.USERSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + UsersAdminWebPortletKeys.USERSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}