/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AppUserService}.
 *
 * @author Brian Wing Shun Chan
 * @see AppUserService
 * @generated
 */
public class AppUserServiceWrapper
	implements AppUserService, ServiceWrapper<AppUserService> {

	public AppUserServiceWrapper(AppUserService appUserService) {
		_appUserService = appUserService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _appUserService.getOSGiServiceIdentifier();
	}

	@Override
	public AppUserService getWrappedService() {
		return _appUserService;
	}

	@Override
	public void setWrappedService(AppUserService appUserService) {
		_appUserService = appUserService;
	}

	private AppUserService _appUserService;

}