/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

import misk.app.users.model.AppUser;

/**
 * Provides the local service utility for AppUser. This utility wraps
 * <code>misk.app.users.service.impl.AppUserLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AppUserLocalService
 * @generated
 */
public class AppUserLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>misk.app.users.service.impl.AppUserLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the app user to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was added
	 */
	public static AppUser addAppUser(AppUser appUser) {
		return getService().addAppUser(appUser);
	}

	public static AppUser addEntry(
			AppUser orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws misk.app.users.exception.AppUserValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * create function to compare plain password and hash password
	 */
	public static boolean comparePassword(
		String password, String passwordHash) {

		return getService().comparePassword(password, passwordHash);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * Creates a new app user with the primary key. Does not add the app user to the database.
	 *
	 * @param appUserId the primary key for the new app user
	 * @return the new app user
	 */
	public static AppUser createAppUser(long appUserId) {
		return getService().createAppUser(appUserId);
	}

	/**
	 * create function to convert plain password to hash password
	 */
	public static String createPassword(String password) {
		return getService().createPassword(password);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the app user from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was removed
	 */
	public static AppUser deleteAppUser(AppUser appUser) {
		return getService().deleteAppUser(appUser);
	}

	/**
	 * Deletes the app user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user that was removed
	 * @throws PortalException if a app user with the primary key could not be found
	 */
	public static AppUser deleteAppUser(long appUserId) throws PortalException {
		return getService().deleteAppUser(appUserId);
	}

	public static AppUser deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static AppUser fetchAppUser(long appUserId) {
		return getService().fetchAppUser(appUserId);
	}

	/**
	 * Returns the app user matching the UUID and group.
	 *
	 * @param uuid the app user's UUID
	 * @param groupId the primary key of the group
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchAppUserByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchAppUserByUuidAndGroupId(uuid, groupId);
	}

	public static AppUser fetchByAppleUserId(String appleUserId) {
		return getService().fetchByAppleUserId(appleUserId);
	}

	public static AppUser fetchByEmailAddress(String emailAddress) {
		return getService().fetchByEmailAddress(emailAddress);
	}

	public static AppUser fetchByFacebookId(String facebookId) {
		return getService().fetchByFacebookId(facebookId);
	}

	public static AppUser fetchByGoogleUserId(String googleUserId) {
		return getService().fetchByGoogleUserId(googleUserId);
	}

	public static AppUser fetchByPhoneNumber(String phonNumber) {
		return getService().fetchByPhoneNumber(phonNumber);
	}

	public static List<AppUser> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<AppUser> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<AppUser> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<AppUser> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static AppUser findByEmailAddress(String emailAddress)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getService().findByEmailAddress(emailAddress);
	}

	public static List<AppUser> findByUserIds(long[] userIds) {
		return getService().findByUserIds(userIds);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the app user with the primary key.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user
	 * @throws PortalException if a app user with the primary key could not be found
	 */
	public static AppUser getAppUser(long appUserId) throws PortalException {
		return getService().getAppUser(appUserId);
	}

	/**
	 * Returns the app user matching the UUID and group.
	 *
	 * @param uuid the app user's UUID
	 * @param groupId the primary key of the group
	 * @return the matching app user
	 * @throws PortalException if a matching app user could not be found
	 */
	public static AppUser getAppUserByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getAppUserByUuidAndGroupId(uuid, groupId);
	}

	public static AppUser getAppUserFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException,
			   misk.app.users.exception.AppUserValidateException {

		return getService().getAppUserFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of app users
	 */
	public static List<AppUser> getAppUsers(int start, int end) {
		return getService().getAppUsers(start, end);
	}

	/**
	 * Returns all the app users matching the UUID and company.
	 *
	 * @param uuid the UUID of the app users
	 * @param companyId the primary key of the company
	 * @return the matching app users, or an empty list if no matches were found
	 */
	public static List<AppUser> getAppUsersByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getAppUsersByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of app users matching the UUID and company.
	 *
	 * @param uuid the UUID of the app users
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching app users, or an empty list if no matches were found
	 */
	public static List<AppUser> getAppUsersByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return getService().getAppUsersByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of app users.
	 *
	 * @return the number of app users
	 */
	public static int getAppUsersCount() {
		return getService().getAppUsersCount();
	}

	public static java.util.Date getDateTimeFromRequest(
		javax.portlet.PortletRequest request, String prefix) {

		return getService().getDateTimeFromRequest(request, prefix);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static AppUser getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static boolean isEmailUsedByAnotherUser(
		String emailAddress, long appUserId) {

		return getService().isEmailUsedByAnotherUser(emailAddress, appUserId);
	}

	public static boolean isPhoneUsedByAnotherUser(
		String phoneNumber, long appUserId) {

		return getService().isPhoneUsedByAnotherUser(phoneNumber, appUserId);
	}

	/**
	 * Updates the app user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was updated
	 */
	public static AppUser updateAppUser(AppUser appUser) {
		return getService().updateAppUser(appUser);
	}

	public static AppUser updateEntry(
			AppUser orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws misk.app.users.exception.AppUserValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static AppUserLocalService getService() {
		return _service;
	}

	private static volatile AppUserLocalService _service;

}