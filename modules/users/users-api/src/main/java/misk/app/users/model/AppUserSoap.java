/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link misk.app.users.service.http.AppUserServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class AppUserSoap implements Serializable {

	public static AppUserSoap toSoapModel(AppUser model) {
		AppUserSoap soapModel = new AppUserSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setAppUserId(model.getAppUserId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setFullName(model.getFullName());
		soapModel.setEmailAddress(model.getEmailAddress());
		soapModel.setPhoneNumber(model.getPhoneNumber());
		soapModel.setLanguageId(model.getLanguageId());
		soapModel.setPassword(model.getPassword());
		soapModel.setPasswordReset(model.isPasswordReset());
		soapModel.setPasswordResetToken(model.getPasswordResetToken());
		soapModel.setPasswordModifiedDate(model.getPasswordModifiedDate());
		soapModel.setPasscode(model.getPasscode());
		soapModel.setFacebookId(model.getFacebookId());
		soapModel.setGoogleUserId(model.getGoogleUserId());
		soapModel.setAppleUserId(model.getAppleUserId());
		soapModel.setEmailAddressVerified(model.isEmailAddressVerified());
		soapModel.setPhoneNumberVerified(model.isPhoneNumberVerified());
		soapModel.setAgreedToTermsOfUse(model.isAgreedToTermsOfUse());
		soapModel.setStatus(model.isStatus());
		soapModel.setProfileBannerImage(model.getProfileBannerImage());
		soapModel.setProfileImage(model.getProfileImage());
		soapModel.setGcpToken(model.getGcpToken());
		soapModel.setAndroidDeviceToken(model.getAndroidDeviceToken());
		soapModel.setIosDeviceToken(model.getIosDeviceToken());
		soapModel.setDeactivateReason(model.getDeactivateReason());

		return soapModel;
	}

	public static AppUserSoap[] toSoapModels(AppUser[] models) {
		AppUserSoap[] soapModels = new AppUserSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AppUserSoap[][] toSoapModels(AppUser[][] models) {
		AppUserSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AppUserSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AppUserSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AppUserSoap[] toSoapModels(List<AppUser> models) {
		List<AppUserSoap> soapModels = new ArrayList<AppUserSoap>(
			models.size());

		for (AppUser model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AppUserSoap[soapModels.size()]);
	}

	public AppUserSoap() {
	}

	public long getPrimaryKey() {
		return _appUserId;
	}

	public void setPrimaryKey(long pk) {
		setAppUserId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getAppUserId() {
		return _appUserId;
	}

	public void setAppUserId(long appUserId) {
		_appUserId = appUserId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getFullName() {
		return _fullName;
	}

	public void setFullName(String fullName) {
		_fullName = fullName;
	}

	public String getEmailAddress() {
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		_emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return _phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		_phoneNumber = phoneNumber;
	}

	public String getLanguageId() {
		return _languageId;
	}

	public void setLanguageId(String languageId) {
		_languageId = languageId;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		_password = password;
	}

	public boolean getPasswordReset() {
		return _passwordReset;
	}

	public boolean isPasswordReset() {
		return _passwordReset;
	}

	public void setPasswordReset(boolean passwordReset) {
		_passwordReset = passwordReset;
	}

	public String getPasswordResetToken() {
		return _passwordResetToken;
	}

	public void setPasswordResetToken(String passwordResetToken) {
		_passwordResetToken = passwordResetToken;
	}

	public Date getPasswordModifiedDate() {
		return _passwordModifiedDate;
	}

	public void setPasswordModifiedDate(Date passwordModifiedDate) {
		_passwordModifiedDate = passwordModifiedDate;
	}

	public String getPasscode() {
		return _passcode;
	}

	public void setPasscode(String passcode) {
		_passcode = passcode;
	}

	public String getFacebookId() {
		return _facebookId;
	}

	public void setFacebookId(String facebookId) {
		_facebookId = facebookId;
	}

	public String getGoogleUserId() {
		return _googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		_googleUserId = googleUserId;
	}

	public String getAppleUserId() {
		return _appleUserId;
	}

	public void setAppleUserId(String appleUserId) {
		_appleUserId = appleUserId;
	}

	public boolean getEmailAddressVerified() {
		return _emailAddressVerified;
	}

	public boolean isEmailAddressVerified() {
		return _emailAddressVerified;
	}

	public void setEmailAddressVerified(boolean emailAddressVerified) {
		_emailAddressVerified = emailAddressVerified;
	}

	public boolean getPhoneNumberVerified() {
		return _phoneNumberVerified;
	}

	public boolean isPhoneNumberVerified() {
		return _phoneNumberVerified;
	}

	public void setPhoneNumberVerified(boolean phoneNumberVerified) {
		_phoneNumberVerified = phoneNumberVerified;
	}

	public boolean getAgreedToTermsOfUse() {
		return _agreedToTermsOfUse;
	}

	public boolean isAgreedToTermsOfUse() {
		return _agreedToTermsOfUse;
	}

	public void setAgreedToTermsOfUse(boolean agreedToTermsOfUse) {
		_agreedToTermsOfUse = agreedToTermsOfUse;
	}

	public boolean getStatus() {
		return _status;
	}

	public boolean isStatus() {
		return _status;
	}

	public void setStatus(boolean status) {
		_status = status;
	}

	public String getProfileBannerImage() {
		return _profileBannerImage;
	}

	public void setProfileBannerImage(String profileBannerImage) {
		_profileBannerImage = profileBannerImage;
	}

	public String getProfileImage() {
		return _profileImage;
	}

	public void setProfileImage(String profileImage) {
		_profileImage = profileImage;
	}

	public String getGcpToken() {
		return _gcpToken;
	}

	public void setGcpToken(String gcpToken) {
		_gcpToken = gcpToken;
	}

	public String getAndroidDeviceToken() {
		return _androidDeviceToken;
	}

	public void setAndroidDeviceToken(String androidDeviceToken) {
		_androidDeviceToken = androidDeviceToken;
	}

	public String getIosDeviceToken() {
		return _iosDeviceToken;
	}

	public void setIosDeviceToken(String iosDeviceToken) {
		_iosDeviceToken = iosDeviceToken;
	}

	public String getDeactivateReason() {
		return _deactivateReason;
	}

	public void setDeactivateReason(String deactivateReason) {
		_deactivateReason = deactivateReason;
	}

	private String _uuid;
	private long _appUserId;
	private long _groupId;
	private long _companyId;
	private Date _createDate;
	private Date _modifiedDate;
	private String _fullName;
	private String _emailAddress;
	private String _phoneNumber;
	private String _languageId;
	private String _password;
	private boolean _passwordReset;
	private String _passwordResetToken;
	private Date _passwordModifiedDate;
	private String _passcode;
	private String _facebookId;
	private String _googleUserId;
	private String _appleUserId;
	private boolean _emailAddressVerified;
	private boolean _phoneNumberVerified;
	private boolean _agreedToTermsOfUse;
	private boolean _status;
	private String _profileBannerImage;
	private String _profileImage;
	private String _gcpToken;
	private String _androidDeviceToken;
	private String _iosDeviceToken;
	private String _deactivateReason;

}