/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import misk.app.users.model.AppUser;

/**
 * The persistence utility for the app user service. This utility wraps <code>misk.app.users.service.persistence.impl.AppUserPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppUserPersistence
 * @generated
 */
public class AppUserUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(AppUser appUser) {
		getPersistence().clearCache(appUser);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, AppUser> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<AppUser> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<AppUser> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<AppUser> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static AppUser update(AppUser appUser) {
		return getPersistence().update(appUser);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static AppUser update(
		AppUser appUser, ServiceContext serviceContext) {

		return getPersistence().update(appUser, serviceContext);
	}

	/**
	 * Returns all the app users where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching app users
	 */
	public static List<AppUser> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public static List<AppUser> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByUuid_First(
			String uuid, OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByUuid_First(
		String uuid, OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByUuid_Last(
			String uuid, OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByUuid_Last(
		String uuid, OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where uuid = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public static AppUser[] findByUuid_PrevAndNext(
			long appUserId, String uuid,
			OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByUuid_PrevAndNext(
			appUserId, uuid, orderByComparator);
	}

	/**
	 * Removes all the app users where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of app users where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching app users
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByUUID_G(String uuid, long groupId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the app user where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the app user that was removed
	 */
	public static AppUser removeByUUID_G(String uuid, long groupId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of app users where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching app users
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching app users
	 */
	public static List<AppUser> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public static List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public static AppUser[] findByUuid_C_PrevAndNext(
			long appUserId, String uuid, long companyId,
			OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByUuid_C_PrevAndNext(
			appUserId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the app users where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of app users where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching app users
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the app users where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching app users
	 */
	public static List<AppUser> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public static List<AppUser> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByGroupId_First(
			long groupId, OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByGroupId_First(
		long groupId, OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByGroupId_Last(
			long groupId, OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByGroupId_Last(
		long groupId, OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where groupId = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public static AppUser[] findByGroupId_PrevAndNext(
			long appUserId, long groupId,
			OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByGroupId_PrevAndNext(
			appUserId, groupId, orderByComparator);
	}

	/**
	 * Removes all the app users where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of app users where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching app users
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app users
	 */
	public static List<AppUser> findByPhone(String phoneNumber) {
		return getPersistence().findByPhone(phoneNumber);
	}

	/**
	 * Returns a range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public static List<AppUser> findByPhone(
		String phoneNumber, int start, int end) {

		return getPersistence().findByPhone(phoneNumber, start, end);
	}

	/**
	 * Returns an ordered range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByPhone(
		String phoneNumber, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().findByPhone(
			phoneNumber, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public static List<AppUser> findByPhone(
		String phoneNumber, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByPhone(
			phoneNumber, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByPhone_First(
			String phoneNumber, OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByPhone_First(
			phoneNumber, orderByComparator);
	}

	/**
	 * Returns the first app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByPhone_First(
		String phoneNumber, OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByPhone_First(
			phoneNumber, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByPhone_Last(
			String phoneNumber, OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByPhone_Last(
			phoneNumber, orderByComparator);
	}

	/**
	 * Returns the last app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByPhone_Last(
		String phoneNumber, OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().fetchByPhone_Last(
			phoneNumber, orderByComparator);
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public static AppUser[] findByPhone_PrevAndNext(
			long appUserId, String phoneNumber,
			OrderByComparator<AppUser> orderByComparator)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByPhone_PrevAndNext(
			appUserId, phoneNumber, orderByComparator);
	}

	/**
	 * Removes all the app users where phoneNumber = &#63; from the database.
	 *
	 * @param phoneNumber the phone number
	 */
	public static void removeByPhone(String phoneNumber) {
		getPersistence().removeByPhone(phoneNumber);
	}

	/**
	 * Returns the number of app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the number of matching app users
	 */
	public static int countByPhone(String phoneNumber) {
		return getPersistence().countByPhone(phoneNumber);
	}

	/**
	 * Returns the app user where emailAddress = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param emailAddress the email address
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByEmailAddress(String emailAddress)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByEmailAddress(emailAddress);
	}

	/**
	 * Returns the app user where emailAddress = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param emailAddress the email address
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByEmailAddress(String emailAddress) {
		return getPersistence().fetchByEmailAddress(emailAddress);
	}

	/**
	 * Returns the app user where emailAddress = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param emailAddress the email address
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByEmailAddress(
		String emailAddress, boolean useFinderCache) {

		return getPersistence().fetchByEmailAddress(
			emailAddress, useFinderCache);
	}

	/**
	 * Removes the app user where emailAddress = &#63; from the database.
	 *
	 * @param emailAddress the email address
	 * @return the app user that was removed
	 */
	public static AppUser removeByEmailAddress(String emailAddress)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().removeByEmailAddress(emailAddress);
	}

	/**
	 * Returns the number of app users where emailAddress = &#63;.
	 *
	 * @param emailAddress the email address
	 * @return the number of matching app users
	 */
	public static int countByEmailAddress(String emailAddress) {
		return getPersistence().countByEmailAddress(emailAddress);
	}

	/**
	 * Returns the app user where phoneNumber = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByPhoneNumber(String phoneNumber)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByPhoneNumber(phoneNumber);
	}

	/**
	 * Returns the app user where phoneNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByPhoneNumber(String phoneNumber) {
		return getPersistence().fetchByPhoneNumber(phoneNumber);
	}

	/**
	 * Returns the app user where phoneNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param phoneNumber the phone number
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByPhoneNumber(
		String phoneNumber, boolean useFinderCache) {

		return getPersistence().fetchByPhoneNumber(phoneNumber, useFinderCache);
	}

	/**
	 * Removes the app user where phoneNumber = &#63; from the database.
	 *
	 * @param phoneNumber the phone number
	 * @return the app user that was removed
	 */
	public static AppUser removeByPhoneNumber(String phoneNumber)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().removeByPhoneNumber(phoneNumber);
	}

	/**
	 * Returns the number of app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the number of matching app users
	 */
	public static int countByPhoneNumber(String phoneNumber) {
		return getPersistence().countByPhoneNumber(phoneNumber);
	}

	/**
	 * Returns the app user where facebookId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param facebookId the facebook ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByFacebookId(String facebookId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByFacebookId(facebookId);
	}

	/**
	 * Returns the app user where facebookId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param facebookId the facebook ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByFacebookId(String facebookId) {
		return getPersistence().fetchByFacebookId(facebookId);
	}

	/**
	 * Returns the app user where facebookId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param facebookId the facebook ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByFacebookId(
		String facebookId, boolean useFinderCache) {

		return getPersistence().fetchByFacebookId(facebookId, useFinderCache);
	}

	/**
	 * Removes the app user where facebookId = &#63; from the database.
	 *
	 * @param facebookId the facebook ID
	 * @return the app user that was removed
	 */
	public static AppUser removeByFacebookId(String facebookId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().removeByFacebookId(facebookId);
	}

	/**
	 * Returns the number of app users where facebookId = &#63;.
	 *
	 * @param facebookId the facebook ID
	 * @return the number of matching app users
	 */
	public static int countByFacebookId(String facebookId) {
		return getPersistence().countByFacebookId(facebookId);
	}

	/**
	 * Returns the app user where googleUserId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param googleUserId the google user ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByGoogleUserId(String googleUserId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByGoogleUserId(googleUserId);
	}

	/**
	 * Returns the app user where googleUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param googleUserId the google user ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByGoogleUserId(String googleUserId) {
		return getPersistence().fetchByGoogleUserId(googleUserId);
	}

	/**
	 * Returns the app user where googleUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param googleUserId the google user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByGoogleUserId(
		String googleUserId, boolean useFinderCache) {

		return getPersistence().fetchByGoogleUserId(
			googleUserId, useFinderCache);
	}

	/**
	 * Removes the app user where googleUserId = &#63; from the database.
	 *
	 * @param googleUserId the google user ID
	 * @return the app user that was removed
	 */
	public static AppUser removeByGoogleUserId(String googleUserId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().removeByGoogleUserId(googleUserId);
	}

	/**
	 * Returns the number of app users where googleUserId = &#63;.
	 *
	 * @param googleUserId the google user ID
	 * @return the number of matching app users
	 */
	public static int countByGoogleUserId(String googleUserId) {
		return getPersistence().countByGoogleUserId(googleUserId);
	}

	/**
	 * Returns the app user where appleUserId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param appleUserId the apple user ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public static AppUser findByAppleUserId(String appleUserId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByAppleUserId(appleUserId);
	}

	/**
	 * Returns the app user where appleUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appleUserId the apple user ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByAppleUserId(String appleUserId) {
		return getPersistence().fetchByAppleUserId(appleUserId);
	}

	/**
	 * Returns the app user where appleUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appleUserId the apple user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public static AppUser fetchByAppleUserId(
		String appleUserId, boolean useFinderCache) {

		return getPersistence().fetchByAppleUserId(appleUserId, useFinderCache);
	}

	/**
	 * Removes the app user where appleUserId = &#63; from the database.
	 *
	 * @param appleUserId the apple user ID
	 * @return the app user that was removed
	 */
	public static AppUser removeByAppleUserId(String appleUserId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().removeByAppleUserId(appleUserId);
	}

	/**
	 * Returns the number of app users where appleUserId = &#63;.
	 *
	 * @param appleUserId the apple user ID
	 * @return the number of matching app users
	 */
	public static int countByAppleUserId(String appleUserId) {
		return getPersistence().countByAppleUserId(appleUserId);
	}

	/**
	 * Caches the app user in the entity cache if it is enabled.
	 *
	 * @param appUser the app user
	 */
	public static void cacheResult(AppUser appUser) {
		getPersistence().cacheResult(appUser);
	}

	/**
	 * Caches the app users in the entity cache if it is enabled.
	 *
	 * @param appUsers the app users
	 */
	public static void cacheResult(List<AppUser> appUsers) {
		getPersistence().cacheResult(appUsers);
	}

	/**
	 * Creates a new app user with the primary key. Does not add the app user to the database.
	 *
	 * @param appUserId the primary key for the new app user
	 * @return the new app user
	 */
	public static AppUser create(long appUserId) {
		return getPersistence().create(appUserId);
	}

	/**
	 * Removes the app user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user that was removed
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public static AppUser remove(long appUserId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().remove(appUserId);
	}

	public static AppUser updateImpl(AppUser appUser) {
		return getPersistence().updateImpl(appUser);
	}

	/**
	 * Returns the app user with the primary key or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public static AppUser findByPrimaryKey(long appUserId)
		throws misk.app.users.exception.NoSuchAppUserException {

		return getPersistence().findByPrimaryKey(appUserId);
	}

	/**
	 * Returns the app user with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user, or <code>null</code> if a app user with the primary key could not be found
	 */
	public static AppUser fetchByPrimaryKey(long appUserId) {
		return getPersistence().fetchByPrimaryKey(appUserId);
	}

	/**
	 * Returns all the app users.
	 *
	 * @return the app users
	 */
	public static List<AppUser> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of app users
	 */
	public static List<AppUser> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of app users
	 */
	public static List<AppUser> findAll(
		int start, int end, OrderByComparator<AppUser> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of app users
	 */
	public static List<AppUser> findAll(
		int start, int end, OrderByComparator<AppUser> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the app users from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of app users.
	 *
	 * @return the number of app users
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AppUserPersistence getPersistence() {
		return _persistence;
	}

	private static volatile AppUserPersistence _persistence;

}