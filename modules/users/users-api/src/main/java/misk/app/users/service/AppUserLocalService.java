/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import misk.app.users.exception.AppUserValidateException;
import misk.app.users.exception.NoSuchAppUserException;
import misk.app.users.model.AppUser;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for AppUser. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see AppUserLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface AppUserLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>misk.app.users.service.impl.AppUserLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the app user local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link AppUserLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the app user to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public AppUser addAppUser(AppUser appUser);

	public AppUser addEntry(AppUser orgEntry, ServiceContext serviceContext)
		throws AppUserValidateException, PortalException;

	/**
	 * create function to compare plain password and hash password
	 */
	public boolean comparePassword(String password, String passwordHash);

	public int countAllInGroup(long groupId);

	/**
	 * Creates a new app user with the primary key. Does not add the app user to the database.
	 *
	 * @param appUserId the primary key for the new app user
	 * @return the new app user
	 */
	@Transactional(enabled = false)
	public AppUser createAppUser(long appUserId);

	/**
	 * create function to convert plain password to hash password
	 */
	public String createPassword(String password);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the app user from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public AppUser deleteAppUser(AppUser appUser);

	/**
	 * Deletes the app user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user that was removed
	 * @throws PortalException if a app user with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public AppUser deleteAppUser(long appUserId) throws PortalException;

	public AppUser deleteEntry(long primaryKey) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser fetchAppUser(long appUserId);

	/**
	 * Returns the app user matching the UUID and group.
	 *
	 * @param uuid the app user's UUID
	 * @param groupId the primary key of the group
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser fetchAppUserByUuidAndGroupId(String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser fetchByAppleUserId(String appleUserId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser fetchByEmailAddress(String emailAddress);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser fetchByFacebookId(String facebookId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser fetchByGoogleUserId(String googleUserId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser fetchByPhoneNumber(String phonNumber);

	public List<AppUser> findAllInGroup(long groupId);

	public List<AppUser> findAllInGroup(long groupId, int start, int end);

	public List<AppUser> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<AppUser> obc);

	public AppUser findByEmailAddress(String emailAddress)
		throws NoSuchAppUserException;

	public List<AppUser> findByUserIds(long[] userIds);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the app user with the primary key.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user
	 * @throws PortalException if a app user with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser getAppUser(long appUserId) throws PortalException;

	/**
	 * Returns the app user matching the UUID and group.
	 *
	 * @param uuid the app user's UUID
	 * @param groupId the primary key of the group
	 * @return the matching app user
	 * @throws PortalException if a matching app user could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser getAppUserByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser getAppUserFromRequest(
			long primaryKey, PortletRequest request)
		throws AppUserValidateException, PortletException;

	/**
	 * Returns a range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of app users
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<AppUser> getAppUsers(int start, int end);

	/**
	 * Returns all the app users matching the UUID and company.
	 *
	 * @param uuid the UUID of the app users
	 * @param companyId the primary key of the company
	 * @return the matching app users, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<AppUser> getAppUsersByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of app users matching the UUID and company.
	 *
	 * @param uuid the UUID of the app users
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching app users, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<AppUser> getAppUsersByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppUser> orderByComparator);

	/**
	 * Returns the number of app users.
	 *
	 * @return the number of app users
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getAppUsersCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Date getDateTimeFromRequest(PortletRequest request, String prefix);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public AppUser getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isEmailUsedByAnotherUser(
		String emailAddress, long appUserId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public boolean isPhoneUsedByAnotherUser(String phoneNumber, long appUserId);

	/**
	 * Updates the app user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public AppUser updateAppUser(AppUser appUser);

	public AppUser updateEntry(AppUser orgEntry, ServiceContext serviceContext)
		throws AppUserValidateException, PortalException;

}