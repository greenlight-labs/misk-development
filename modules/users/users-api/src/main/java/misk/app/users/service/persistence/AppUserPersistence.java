/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import misk.app.users.exception.NoSuchAppUserException;
import misk.app.users.model.AppUser;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the app user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppUserUtil
 * @generated
 */
@ProviderType
public interface AppUserPersistence extends BasePersistence<AppUser> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AppUserUtil} to access the app user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the app users where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching app users
	 */
	public java.util.List<AppUser> findByUuid(String uuid);

	/**
	 * Returns a range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public java.util.List<AppUser> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns an ordered range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the first app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the last app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the last app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the app users before and after the current app user in the ordered set where uuid = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public AppUser[] findByUuid_PrevAndNext(
			long appUserId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Removes all the app users where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of app users where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching app users
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByUUID_G(String uuid, long groupId)
		throws NoSuchAppUserException;

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the app user where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the app user that was removed
	 */
	public AppUser removeByUUID_G(String uuid, long groupId)
		throws NoSuchAppUserException;

	/**
	 * Returns the number of app users where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching app users
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching app users
	 */
	public java.util.List<AppUser> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public java.util.List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns an ordered range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the first app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the last app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the last app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the app users before and after the current app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public AppUser[] findByUuid_C_PrevAndNext(
			long appUserId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Removes all the app users where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of app users where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching app users
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the app users where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching app users
	 */
	public java.util.List<AppUser> findByGroupId(long groupId);

	/**
	 * Returns a range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public java.util.List<AppUser> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns an ordered range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the first app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the last app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the last app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the app users before and after the current app user in the ordered set where groupId = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public AppUser[] findByGroupId_PrevAndNext(
			long appUserId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Removes all the app users where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of app users where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching app users
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app users
	 */
	public java.util.List<AppUser> findByPhone(String phoneNumber);

	/**
	 * Returns a range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	public java.util.List<AppUser> findByPhone(
		String phoneNumber, int start, int end);

	/**
	 * Returns an ordered range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByPhone(
		String phoneNumber, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns an ordered range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	public java.util.List<AppUser> findByPhone(
		String phoneNumber, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByPhone_First(
			String phoneNumber,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the first app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByPhone_First(
		String phoneNumber,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the last app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByPhone_Last(
			String phoneNumber,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Returns the last app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByPhone_Last(
		String phoneNumber,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns the app users before and after the current app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public AppUser[] findByPhone_PrevAndNext(
			long appUserId, String phoneNumber,
			com.liferay.portal.kernel.util.OrderByComparator<AppUser>
				orderByComparator)
		throws NoSuchAppUserException;

	/**
	 * Removes all the app users where phoneNumber = &#63; from the database.
	 *
	 * @param phoneNumber the phone number
	 */
	public void removeByPhone(String phoneNumber);

	/**
	 * Returns the number of app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the number of matching app users
	 */
	public int countByPhone(String phoneNumber);

	/**
	 * Returns the app user where emailAddress = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param emailAddress the email address
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByEmailAddress(String emailAddress)
		throws NoSuchAppUserException;

	/**
	 * Returns the app user where emailAddress = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param emailAddress the email address
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByEmailAddress(String emailAddress);

	/**
	 * Returns the app user where emailAddress = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param emailAddress the email address
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByEmailAddress(
		String emailAddress, boolean useFinderCache);

	/**
	 * Removes the app user where emailAddress = &#63; from the database.
	 *
	 * @param emailAddress the email address
	 * @return the app user that was removed
	 */
	public AppUser removeByEmailAddress(String emailAddress)
		throws NoSuchAppUserException;

	/**
	 * Returns the number of app users where emailAddress = &#63;.
	 *
	 * @param emailAddress the email address
	 * @return the number of matching app users
	 */
	public int countByEmailAddress(String emailAddress);

	/**
	 * Returns the app user where phoneNumber = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByPhoneNumber(String phoneNumber)
		throws NoSuchAppUserException;

	/**
	 * Returns the app user where phoneNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByPhoneNumber(String phoneNumber);

	/**
	 * Returns the app user where phoneNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param phoneNumber the phone number
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByPhoneNumber(
		String phoneNumber, boolean useFinderCache);

	/**
	 * Removes the app user where phoneNumber = &#63; from the database.
	 *
	 * @param phoneNumber the phone number
	 * @return the app user that was removed
	 */
	public AppUser removeByPhoneNumber(String phoneNumber)
		throws NoSuchAppUserException;

	/**
	 * Returns the number of app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the number of matching app users
	 */
	public int countByPhoneNumber(String phoneNumber);

	/**
	 * Returns the app user where facebookId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param facebookId the facebook ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByFacebookId(String facebookId)
		throws NoSuchAppUserException;

	/**
	 * Returns the app user where facebookId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param facebookId the facebook ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByFacebookId(String facebookId);

	/**
	 * Returns the app user where facebookId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param facebookId the facebook ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByFacebookId(String facebookId, boolean useFinderCache);

	/**
	 * Removes the app user where facebookId = &#63; from the database.
	 *
	 * @param facebookId the facebook ID
	 * @return the app user that was removed
	 */
	public AppUser removeByFacebookId(String facebookId)
		throws NoSuchAppUserException;

	/**
	 * Returns the number of app users where facebookId = &#63;.
	 *
	 * @param facebookId the facebook ID
	 * @return the number of matching app users
	 */
	public int countByFacebookId(String facebookId);

	/**
	 * Returns the app user where googleUserId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param googleUserId the google user ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByGoogleUserId(String googleUserId)
		throws NoSuchAppUserException;

	/**
	 * Returns the app user where googleUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param googleUserId the google user ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByGoogleUserId(String googleUserId);

	/**
	 * Returns the app user where googleUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param googleUserId the google user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByGoogleUserId(
		String googleUserId, boolean useFinderCache);

	/**
	 * Removes the app user where googleUserId = &#63; from the database.
	 *
	 * @param googleUserId the google user ID
	 * @return the app user that was removed
	 */
	public AppUser removeByGoogleUserId(String googleUserId)
		throws NoSuchAppUserException;

	/**
	 * Returns the number of app users where googleUserId = &#63;.
	 *
	 * @param googleUserId the google user ID
	 * @return the number of matching app users
	 */
	public int countByGoogleUserId(String googleUserId);

	/**
	 * Returns the app user where appleUserId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param appleUserId the apple user ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	public AppUser findByAppleUserId(String appleUserId)
		throws NoSuchAppUserException;

	/**
	 * Returns the app user where appleUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appleUserId the apple user ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByAppleUserId(String appleUserId);

	/**
	 * Returns the app user where appleUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appleUserId the apple user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	public AppUser fetchByAppleUserId(
		String appleUserId, boolean useFinderCache);

	/**
	 * Removes the app user where appleUserId = &#63; from the database.
	 *
	 * @param appleUserId the apple user ID
	 * @return the app user that was removed
	 */
	public AppUser removeByAppleUserId(String appleUserId)
		throws NoSuchAppUserException;

	/**
	 * Returns the number of app users where appleUserId = &#63;.
	 *
	 * @param appleUserId the apple user ID
	 * @return the number of matching app users
	 */
	public int countByAppleUserId(String appleUserId);

	/**
	 * Caches the app user in the entity cache if it is enabled.
	 *
	 * @param appUser the app user
	 */
	public void cacheResult(AppUser appUser);

	/**
	 * Caches the app users in the entity cache if it is enabled.
	 *
	 * @param appUsers the app users
	 */
	public void cacheResult(java.util.List<AppUser> appUsers);

	/**
	 * Creates a new app user with the primary key. Does not add the app user to the database.
	 *
	 * @param appUserId the primary key for the new app user
	 * @return the new app user
	 */
	public AppUser create(long appUserId);

	/**
	 * Removes the app user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user that was removed
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public AppUser remove(long appUserId) throws NoSuchAppUserException;

	public AppUser updateImpl(AppUser appUser);

	/**
	 * Returns the app user with the primary key or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	public AppUser findByPrimaryKey(long appUserId)
		throws NoSuchAppUserException;

	/**
	 * Returns the app user with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user, or <code>null</code> if a app user with the primary key could not be found
	 */
	public AppUser fetchByPrimaryKey(long appUserId);

	/**
	 * Returns all the app users.
	 *
	 * @return the app users
	 */
	public java.util.List<AppUser> findAll();

	/**
	 * Returns a range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of app users
	 */
	public java.util.List<AppUser> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of app users
	 */
	public java.util.List<AppUser> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator);

	/**
	 * Returns an ordered range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of app users
	 */
	public java.util.List<AppUser> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<AppUser>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the app users from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of app users.
	 *
	 * @return the number of app users
	 */
	public int countAll();

}