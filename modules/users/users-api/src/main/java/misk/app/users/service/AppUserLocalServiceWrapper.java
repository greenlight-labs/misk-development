/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AppUserLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AppUserLocalService
 * @generated
 */
public class AppUserLocalServiceWrapper
	implements AppUserLocalService, ServiceWrapper<AppUserLocalService> {

	public AppUserLocalServiceWrapper(AppUserLocalService appUserLocalService) {
		_appUserLocalService = appUserLocalService;
	}

	/**
	 * Adds the app user to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was added
	 */
	@Override
	public misk.app.users.model.AppUser addAppUser(
		misk.app.users.model.AppUser appUser) {

		return _appUserLocalService.addAppUser(appUser);
	}

	@Override
	public misk.app.users.model.AppUser addEntry(
			misk.app.users.model.AppUser orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   misk.app.users.exception.AppUserValidateException {

		return _appUserLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * create function to compare plain password and hash password
	 */
	@Override
	public boolean comparePassword(String password, String passwordHash) {
		return _appUserLocalService.comparePassword(password, passwordHash);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _appUserLocalService.countAllInGroup(groupId);
	}

	/**
	 * Creates a new app user with the primary key. Does not add the app user to the database.
	 *
	 * @param appUserId the primary key for the new app user
	 * @return the new app user
	 */
	@Override
	public misk.app.users.model.AppUser createAppUser(long appUserId) {
		return _appUserLocalService.createAppUser(appUserId);
	}

	/**
	 * create function to convert plain password to hash password
	 */
	@Override
	public String createPassword(String password) {
		return _appUserLocalService.createPassword(password);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appUserLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the app user from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was removed
	 */
	@Override
	public misk.app.users.model.AppUser deleteAppUser(
		misk.app.users.model.AppUser appUser) {

		return _appUserLocalService.deleteAppUser(appUser);
	}

	/**
	 * Deletes the app user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user that was removed
	 * @throws PortalException if a app user with the primary key could not be found
	 */
	@Override
	public misk.app.users.model.AppUser deleteAppUser(long appUserId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appUserLocalService.deleteAppUser(appUserId);
	}

	@Override
	public misk.app.users.model.AppUser deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appUserLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appUserLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _appUserLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _appUserLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _appUserLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _appUserLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _appUserLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _appUserLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public misk.app.users.model.AppUser fetchAppUser(long appUserId) {
		return _appUserLocalService.fetchAppUser(appUserId);
	}

	/**
	 * Returns the app user matching the UUID and group.
	 *
	 * @param uuid the app user's UUID
	 * @param groupId the primary key of the group
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public misk.app.users.model.AppUser fetchAppUserByUuidAndGroupId(
		String uuid, long groupId) {

		return _appUserLocalService.fetchAppUserByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public misk.app.users.model.AppUser fetchByAppleUserId(String appleUserId) {
		return _appUserLocalService.fetchByAppleUserId(appleUserId);
	}

	@Override
	public misk.app.users.model.AppUser fetchByEmailAddress(
		String emailAddress) {

		return _appUserLocalService.fetchByEmailAddress(emailAddress);
	}

	@Override
	public misk.app.users.model.AppUser fetchByFacebookId(String facebookId) {
		return _appUserLocalService.fetchByFacebookId(facebookId);
	}

	@Override
	public misk.app.users.model.AppUser fetchByGoogleUserId(
		String googleUserId) {

		return _appUserLocalService.fetchByGoogleUserId(googleUserId);
	}

	@Override
	public misk.app.users.model.AppUser fetchByPhoneNumber(String phonNumber) {
		return _appUserLocalService.fetchByPhoneNumber(phonNumber);
	}

	@Override
	public java.util.List<misk.app.users.model.AppUser> findAllInGroup(
		long groupId) {

		return _appUserLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<misk.app.users.model.AppUser> findAllInGroup(
		long groupId, int start, int end) {

		return _appUserLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<misk.app.users.model.AppUser> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<misk.app.users.model.AppUser> obc) {

		return _appUserLocalService.findAllInGroup(groupId, start, end, obc);
	}

	@Override
	public misk.app.users.model.AppUser findByEmailAddress(String emailAddress)
		throws misk.app.users.exception.NoSuchAppUserException {

		return _appUserLocalService.findByEmailAddress(emailAddress);
	}

	@Override
	public java.util.List<misk.app.users.model.AppUser> findByUserIds(
		long[] userIds) {

		return _appUserLocalService.findByUserIds(userIds);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _appUserLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the app user with the primary key.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user
	 * @throws PortalException if a app user with the primary key could not be found
	 */
	@Override
	public misk.app.users.model.AppUser getAppUser(long appUserId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appUserLocalService.getAppUser(appUserId);
	}

	/**
	 * Returns the app user matching the UUID and group.
	 *
	 * @param uuid the app user's UUID
	 * @param groupId the primary key of the group
	 * @return the matching app user
	 * @throws PortalException if a matching app user could not be found
	 */
	@Override
	public misk.app.users.model.AppUser getAppUserByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appUserLocalService.getAppUserByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public misk.app.users.model.AppUser getAppUserFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException,
			   misk.app.users.exception.AppUserValidateException {

		return _appUserLocalService.getAppUserFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>misk.app.users.model.impl.AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of app users
	 */
	@Override
	public java.util.List<misk.app.users.model.AppUser> getAppUsers(
		int start, int end) {

		return _appUserLocalService.getAppUsers(start, end);
	}

	/**
	 * Returns all the app users matching the UUID and company.
	 *
	 * @param uuid the UUID of the app users
	 * @param companyId the primary key of the company
	 * @return the matching app users, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<misk.app.users.model.AppUser>
		getAppUsersByUuidAndCompanyId(String uuid, long companyId) {

		return _appUserLocalService.getAppUsersByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of app users matching the UUID and company.
	 *
	 * @param uuid the UUID of the app users
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching app users, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<misk.app.users.model.AppUser>
		getAppUsersByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<misk.app.users.model.AppUser> orderByComparator) {

		return _appUserLocalService.getAppUsersByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of app users.
	 *
	 * @return the number of app users
	 */
	@Override
	public int getAppUsersCount() {
		return _appUserLocalService.getAppUsersCount();
	}

	@Override
	public java.util.Date getDateTimeFromRequest(
		javax.portlet.PortletRequest request, String prefix) {

		return _appUserLocalService.getDateTimeFromRequest(request, prefix);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _appUserLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _appUserLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public misk.app.users.model.AppUser getNewObject(long primaryKey) {
		return _appUserLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _appUserLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _appUserLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public boolean isEmailUsedByAnotherUser(
		String emailAddress, long appUserId) {

		return _appUserLocalService.isEmailUsedByAnotherUser(
			emailAddress, appUserId);
	}

	@Override
	public boolean isPhoneUsedByAnotherUser(
		String phoneNumber, long appUserId) {

		return _appUserLocalService.isPhoneUsedByAnotherUser(
			phoneNumber, appUserId);
	}

	/**
	 * Updates the app user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AppUserLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param appUser the app user
	 * @return the app user that was updated
	 */
	@Override
	public misk.app.users.model.AppUser updateAppUser(
		misk.app.users.model.AppUser appUser) {

		return _appUserLocalService.updateAppUser(appUser);
	}

	@Override
	public misk.app.users.model.AppUser updateEntry(
			misk.app.users.model.AppUser orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   misk.app.users.exception.AppUserValidateException {

		return _appUserLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public AppUserLocalService getWrappedService() {
		return _appUserLocalService;
	}

	@Override
	public void setWrappedService(AppUserLocalService appUserLocalService) {
		_appUserLocalService = appUserLocalService;
	}

	private AppUserLocalService _appUserLocalService;

}