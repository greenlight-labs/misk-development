/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AppUser}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppUser
 * @generated
 */
public class AppUserWrapper
	extends BaseModelWrapper<AppUser>
	implements AppUser, ModelWrapper<AppUser> {

	public AppUserWrapper(AppUser appUser) {
		super(appUser);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("appUserId", getAppUserId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("fullName", getFullName());
		attributes.put("emailAddress", getEmailAddress());
		attributes.put("phoneNumber", getPhoneNumber());
		attributes.put("languageId", getLanguageId());
		attributes.put("password", getPassword());
		attributes.put("passwordReset", isPasswordReset());
		attributes.put("passwordResetToken", getPasswordResetToken());
		attributes.put("passwordModifiedDate", getPasswordModifiedDate());
		attributes.put("passcode", getPasscode());
		attributes.put("facebookId", getFacebookId());
		attributes.put("googleUserId", getGoogleUserId());
		attributes.put("appleUserId", getAppleUserId());
		attributes.put("emailAddressVerified", isEmailAddressVerified());
		attributes.put("phoneNumberVerified", isPhoneNumberVerified());
		attributes.put("agreedToTermsOfUse", isAgreedToTermsOfUse());
		attributes.put("status", isStatus());
		attributes.put("profileBannerImage", getProfileBannerImage());
		attributes.put("profileImage", getProfileImage());
		attributes.put("gcpToken", getGcpToken());
		attributes.put("androidDeviceToken", getAndroidDeviceToken());
		attributes.put("iosDeviceToken", getIosDeviceToken());
		attributes.put("deactivateReason", getDeactivateReason());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long appUserId = (Long)attributes.get("appUserId");

		if (appUserId != null) {
			setAppUserId(appUserId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String fullName = (String)attributes.get("fullName");

		if (fullName != null) {
			setFullName(fullName);
		}

		String emailAddress = (String)attributes.get("emailAddress");

		if (emailAddress != null) {
			setEmailAddress(emailAddress);
		}

		String phoneNumber = (String)attributes.get("phoneNumber");

		if (phoneNumber != null) {
			setPhoneNumber(phoneNumber);
		}

		String languageId = (String)attributes.get("languageId");

		if (languageId != null) {
			setLanguageId(languageId);
		}

		String password = (String)attributes.get("password");

		if (password != null) {
			setPassword(password);
		}

		Boolean passwordReset = (Boolean)attributes.get("passwordReset");

		if (passwordReset != null) {
			setPasswordReset(passwordReset);
		}

		String passwordResetToken = (String)attributes.get(
			"passwordResetToken");

		if (passwordResetToken != null) {
			setPasswordResetToken(passwordResetToken);
		}

		Date passwordModifiedDate = (Date)attributes.get(
			"passwordModifiedDate");

		if (passwordModifiedDate != null) {
			setPasswordModifiedDate(passwordModifiedDate);
		}

		String passcode = (String)attributes.get("passcode");

		if (passcode != null) {
			setPasscode(passcode);
		}

		String facebookId = (String)attributes.get("facebookId");

		if (facebookId != null) {
			setFacebookId(facebookId);
		}

		String googleUserId = (String)attributes.get("googleUserId");

		if (googleUserId != null) {
			setGoogleUserId(googleUserId);
		}

		String appleUserId = (String)attributes.get("appleUserId");

		if (appleUserId != null) {
			setAppleUserId(appleUserId);
		}

		Boolean emailAddressVerified = (Boolean)attributes.get(
			"emailAddressVerified");

		if (emailAddressVerified != null) {
			setEmailAddressVerified(emailAddressVerified);
		}

		Boolean phoneNumberVerified = (Boolean)attributes.get(
			"phoneNumberVerified");

		if (phoneNumberVerified != null) {
			setPhoneNumberVerified(phoneNumberVerified);
		}

		Boolean agreedToTermsOfUse = (Boolean)attributes.get(
			"agreedToTermsOfUse");

		if (agreedToTermsOfUse != null) {
			setAgreedToTermsOfUse(agreedToTermsOfUse);
		}

		Boolean status = (Boolean)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		String profileBannerImage = (String)attributes.get(
			"profileBannerImage");

		if (profileBannerImage != null) {
			setProfileBannerImage(profileBannerImage);
		}

		String profileImage = (String)attributes.get("profileImage");

		if (profileImage != null) {
			setProfileImage(profileImage);
		}

		String gcpToken = (String)attributes.get("gcpToken");

		if (gcpToken != null) {
			setGcpToken(gcpToken);
		}

		String androidDeviceToken = (String)attributes.get(
			"androidDeviceToken");

		if (androidDeviceToken != null) {
			setAndroidDeviceToken(androidDeviceToken);
		}

		String iosDeviceToken = (String)attributes.get("iosDeviceToken");

		if (iosDeviceToken != null) {
			setIosDeviceToken(iosDeviceToken);
		}

		String deactivateReason = (String)attributes.get("deactivateReason");

		if (deactivateReason != null) {
			setDeactivateReason(deactivateReason);
		}
	}

	/**
	 * Returns the agreed to terms of use of this app user.
	 *
	 * @return the agreed to terms of use of this app user
	 */
	@Override
	public boolean getAgreedToTermsOfUse() {
		return model.getAgreedToTermsOfUse();
	}

	/**
	 * Returns the android device token of this app user.
	 *
	 * @return the android device token of this app user
	 */
	@Override
	public String getAndroidDeviceToken() {
		return model.getAndroidDeviceToken();
	}

	/**
	 * Returns the apple user ID of this app user.
	 *
	 * @return the apple user ID of this app user
	 */
	@Override
	public String getAppleUserId() {
		return model.getAppleUserId();
	}

	/**
	 * Returns the app user ID of this app user.
	 *
	 * @return the app user ID of this app user
	 */
	@Override
	public long getAppUserId() {
		return model.getAppUserId();
	}

	/**
	 * Returns the app user uuid of this app user.
	 *
	 * @return the app user uuid of this app user
	 */
	@Override
	public String getAppUserUuid() {
		return model.getAppUserUuid();
	}

	/**
	 * Returns the company ID of this app user.
	 *
	 * @return the company ID of this app user
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this app user.
	 *
	 * @return the create date of this app user
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the deactivate reason of this app user.
	 *
	 * @return the deactivate reason of this app user
	 */
	@Override
	public String getDeactivateReason() {
		return model.getDeactivateReason();
	}

	/**
	 * Returns the email address of this app user.
	 *
	 * @return the email address of this app user
	 */
	@Override
	public String getEmailAddress() {
		return model.getEmailAddress();
	}

	/**
	 * Returns the email address verified of this app user.
	 *
	 * @return the email address verified of this app user
	 */
	@Override
	public boolean getEmailAddressVerified() {
		return model.getEmailAddressVerified();
	}

	/**
	 * Returns the facebook ID of this app user.
	 *
	 * @return the facebook ID of this app user
	 */
	@Override
	public String getFacebookId() {
		return model.getFacebookId();
	}

	/**
	 * Returns the full name of this app user.
	 *
	 * @return the full name of this app user
	 */
	@Override
	public String getFullName() {
		return model.getFullName();
	}

	/**
	 * Returns the gcp token of this app user.
	 *
	 * @return the gcp token of this app user
	 */
	@Override
	public String getGcpToken() {
		return model.getGcpToken();
	}

	/**
	 * Returns the google user ID of this app user.
	 *
	 * @return the google user ID of this app user
	 */
	@Override
	public String getGoogleUserId() {
		return model.getGoogleUserId();
	}

	/**
	 * Returns the group ID of this app user.
	 *
	 * @return the group ID of this app user
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the ios device token of this app user.
	 *
	 * @return the ios device token of this app user
	 */
	@Override
	public String getIosDeviceToken() {
		return model.getIosDeviceToken();
	}

	/**
	 * Returns the language ID of this app user.
	 *
	 * @return the language ID of this app user
	 */
	@Override
	public String getLanguageId() {
		return model.getLanguageId();
	}

	/**
	 * Returns the modified date of this app user.
	 *
	 * @return the modified date of this app user
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the passcode of this app user.
	 *
	 * @return the passcode of this app user
	 */
	@Override
	public String getPasscode() {
		return model.getPasscode();
	}

	/**
	 * Returns the password of this app user.
	 *
	 * @return the password of this app user
	 */
	@Override
	public String getPassword() {
		return model.getPassword();
	}

	/**
	 * Returns the password modified date of this app user.
	 *
	 * @return the password modified date of this app user
	 */
	@Override
	public Date getPasswordModifiedDate() {
		return model.getPasswordModifiedDate();
	}

	/**
	 * Returns the password reset of this app user.
	 *
	 * @return the password reset of this app user
	 */
	@Override
	public boolean getPasswordReset() {
		return model.getPasswordReset();
	}

	/**
	 * Returns the password reset token of this app user.
	 *
	 * @return the password reset token of this app user
	 */
	@Override
	public String getPasswordResetToken() {
		return model.getPasswordResetToken();
	}

	/**
	 * Returns the phone number of this app user.
	 *
	 * @return the phone number of this app user
	 */
	@Override
	public String getPhoneNumber() {
		return model.getPhoneNumber();
	}

	/**
	 * Returns the phone number verified of this app user.
	 *
	 * @return the phone number verified of this app user
	 */
	@Override
	public boolean getPhoneNumberVerified() {
		return model.getPhoneNumberVerified();
	}

	/**
	 * Returns the primary key of this app user.
	 *
	 * @return the primary key of this app user
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the profile banner image of this app user.
	 *
	 * @return the profile banner image of this app user
	 */
	@Override
	public String getProfileBannerImage() {
		return model.getProfileBannerImage();
	}

	/**
	 * Returns the profile image of this app user.
	 *
	 * @return the profile image of this app user
	 */
	@Override
	public String getProfileImage() {
		return model.getProfileImage();
	}

	/**
	 * Returns the status of this app user.
	 *
	 * @return the status of this app user
	 */
	@Override
	public boolean getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the uuid of this app user.
	 *
	 * @return the uuid of this app user
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this app user is agreed to terms of use.
	 *
	 * @return <code>true</code> if this app user is agreed to terms of use; <code>false</code> otherwise
	 */
	@Override
	public boolean isAgreedToTermsOfUse() {
		return model.isAgreedToTermsOfUse();
	}

	/**
	 * Returns <code>true</code> if this app user is email address verified.
	 *
	 * @return <code>true</code> if this app user is email address verified; <code>false</code> otherwise
	 */
	@Override
	public boolean isEmailAddressVerified() {
		return model.isEmailAddressVerified();
	}

	/**
	 * Returns <code>true</code> if this app user is password reset.
	 *
	 * @return <code>true</code> if this app user is password reset; <code>false</code> otherwise
	 */
	@Override
	public boolean isPasswordReset() {
		return model.isPasswordReset();
	}

	/**
	 * Returns <code>true</code> if this app user is phone number verified.
	 *
	 * @return <code>true</code> if this app user is phone number verified; <code>false</code> otherwise
	 */
	@Override
	public boolean isPhoneNumberVerified() {
		return model.isPhoneNumberVerified();
	}

	/**
	 * Returns <code>true</code> if this app user is status.
	 *
	 * @return <code>true</code> if this app user is status; <code>false</code> otherwise
	 */
	@Override
	public boolean isStatus() {
		return model.isStatus();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets whether this app user is agreed to terms of use.
	 *
	 * @param agreedToTermsOfUse the agreed to terms of use of this app user
	 */
	@Override
	public void setAgreedToTermsOfUse(boolean agreedToTermsOfUse) {
		model.setAgreedToTermsOfUse(agreedToTermsOfUse);
	}

	/**
	 * Sets the android device token of this app user.
	 *
	 * @param androidDeviceToken the android device token of this app user
	 */
	@Override
	public void setAndroidDeviceToken(String androidDeviceToken) {
		model.setAndroidDeviceToken(androidDeviceToken);
	}

	/**
	 * Sets the apple user ID of this app user.
	 *
	 * @param appleUserId the apple user ID of this app user
	 */
	@Override
	public void setAppleUserId(String appleUserId) {
		model.setAppleUserId(appleUserId);
	}

	/**
	 * Sets the app user ID of this app user.
	 *
	 * @param appUserId the app user ID of this app user
	 */
	@Override
	public void setAppUserId(long appUserId) {
		model.setAppUserId(appUserId);
	}

	/**
	 * Sets the app user uuid of this app user.
	 *
	 * @param appUserUuid the app user uuid of this app user
	 */
	@Override
	public void setAppUserUuid(String appUserUuid) {
		model.setAppUserUuid(appUserUuid);
	}

	/**
	 * Sets the company ID of this app user.
	 *
	 * @param companyId the company ID of this app user
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this app user.
	 *
	 * @param createDate the create date of this app user
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the deactivate reason of this app user.
	 *
	 * @param deactivateReason the deactivate reason of this app user
	 */
	@Override
	public void setDeactivateReason(String deactivateReason) {
		model.setDeactivateReason(deactivateReason);
	}

	/**
	 * Sets the email address of this app user.
	 *
	 * @param emailAddress the email address of this app user
	 */
	@Override
	public void setEmailAddress(String emailAddress) {
		model.setEmailAddress(emailAddress);
	}

	/**
	 * Sets whether this app user is email address verified.
	 *
	 * @param emailAddressVerified the email address verified of this app user
	 */
	@Override
	public void setEmailAddressVerified(boolean emailAddressVerified) {
		model.setEmailAddressVerified(emailAddressVerified);
	}

	/**
	 * Sets the facebook ID of this app user.
	 *
	 * @param facebookId the facebook ID of this app user
	 */
	@Override
	public void setFacebookId(String facebookId) {
		model.setFacebookId(facebookId);
	}

	/**
	 * Sets the full name of this app user.
	 *
	 * @param fullName the full name of this app user
	 */
	@Override
	public void setFullName(String fullName) {
		model.setFullName(fullName);
	}

	/**
	 * Sets the gcp token of this app user.
	 *
	 * @param gcpToken the gcp token of this app user
	 */
	@Override
	public void setGcpToken(String gcpToken) {
		model.setGcpToken(gcpToken);
	}

	/**
	 * Sets the google user ID of this app user.
	 *
	 * @param googleUserId the google user ID of this app user
	 */
	@Override
	public void setGoogleUserId(String googleUserId) {
		model.setGoogleUserId(googleUserId);
	}

	/**
	 * Sets the group ID of this app user.
	 *
	 * @param groupId the group ID of this app user
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the ios device token of this app user.
	 *
	 * @param iosDeviceToken the ios device token of this app user
	 */
	@Override
	public void setIosDeviceToken(String iosDeviceToken) {
		model.setIosDeviceToken(iosDeviceToken);
	}

	/**
	 * Sets the language ID of this app user.
	 *
	 * @param languageId the language ID of this app user
	 */
	@Override
	public void setLanguageId(String languageId) {
		model.setLanguageId(languageId);
	}

	/**
	 * Sets the modified date of this app user.
	 *
	 * @param modifiedDate the modified date of this app user
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the passcode of this app user.
	 *
	 * @param passcode the passcode of this app user
	 */
	@Override
	public void setPasscode(String passcode) {
		model.setPasscode(passcode);
	}

	/**
	 * Sets the password of this app user.
	 *
	 * @param password the password of this app user
	 */
	@Override
	public void setPassword(String password) {
		model.setPassword(password);
	}

	/**
	 * Sets the password modified date of this app user.
	 *
	 * @param passwordModifiedDate the password modified date of this app user
	 */
	@Override
	public void setPasswordModifiedDate(Date passwordModifiedDate) {
		model.setPasswordModifiedDate(passwordModifiedDate);
	}

	/**
	 * Sets whether this app user is password reset.
	 *
	 * @param passwordReset the password reset of this app user
	 */
	@Override
	public void setPasswordReset(boolean passwordReset) {
		model.setPasswordReset(passwordReset);
	}

	/**
	 * Sets the password reset token of this app user.
	 *
	 * @param passwordResetToken the password reset token of this app user
	 */
	@Override
	public void setPasswordResetToken(String passwordResetToken) {
		model.setPasswordResetToken(passwordResetToken);
	}

	/**
	 * Sets the phone number of this app user.
	 *
	 * @param phoneNumber the phone number of this app user
	 */
	@Override
	public void setPhoneNumber(String phoneNumber) {
		model.setPhoneNumber(phoneNumber);
	}

	/**
	 * Sets whether this app user is phone number verified.
	 *
	 * @param phoneNumberVerified the phone number verified of this app user
	 */
	@Override
	public void setPhoneNumberVerified(boolean phoneNumberVerified) {
		model.setPhoneNumberVerified(phoneNumberVerified);
	}

	/**
	 * Sets the primary key of this app user.
	 *
	 * @param primaryKey the primary key of this app user
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the profile banner image of this app user.
	 *
	 * @param profileBannerImage the profile banner image of this app user
	 */
	@Override
	public void setProfileBannerImage(String profileBannerImage) {
		model.setProfileBannerImage(profileBannerImage);
	}

	/**
	 * Sets the profile image of this app user.
	 *
	 * @param profileImage the profile image of this app user
	 */
	@Override
	public void setProfileImage(String profileImage) {
		model.setProfileImage(profileImage);
	}

	/**
	 * Sets whether this app user is status.
	 *
	 * @param status the status of this app user
	 */
	@Override
	public void setStatus(boolean status) {
		model.setStatus(status);
	}

	/**
	 * Sets the uuid of this app user.
	 *
	 * @param uuid the uuid of this app user
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected AppUserWrapper wrap(AppUser appUser) {
		return new AppUserWrapper(appUser);
	}

}