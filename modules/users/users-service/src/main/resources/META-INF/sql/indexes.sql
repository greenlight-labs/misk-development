create unique index IX_612629AB on misk_app_users (appleUserId[$COLUMN_LENGTH:75$]);
create unique index IX_1C9A5CD1 on misk_app_users (emailAddress[$COLUMN_LENGTH:75$]);
create unique index IX_6E5C461A on misk_app_users (facebookId[$COLUMN_LENGTH:75$]);
create unique index IX_C6A9F838 on misk_app_users (googleUserId[$COLUMN_LENGTH:75$]);
create index IX_3A65D165 on misk_app_users (groupId);
create unique index IX_106B0802 on misk_app_users (phoneNumber[$COLUMN_LENGTH:75$]);
create index IX_1AE74799 on misk_app_users (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_9EE05B on misk_app_users (uuid_[$COLUMN_LENGTH:75$], groupId);