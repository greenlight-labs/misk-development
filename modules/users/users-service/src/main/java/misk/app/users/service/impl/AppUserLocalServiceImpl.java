/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import misk.app.users.exception.AppUserValidateException;
import misk.app.users.exception.NoSuchAppUserException;
import misk.app.users.model.AppUser;
import misk.app.users.service.base.AppUserLocalServiceBaseImpl;
import misk.app.users.service.util.AppUserValidator;
import org.osgi.service.component.annotations.Component;
import org.mindrot.jbcrypt.BCrypt;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the app user local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>misk.app.users.service.AppUserLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppUserLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=misk.app.users.model.AppUser",
	service = AopService.class
)
public class AppUserLocalServiceImpl extends AppUserLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>misk.app.users.service.AppUserLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>misk.app.users.service.AppUserLocalServiceUtil</code>.
	 */
	public AppUser addEntry(AppUser orgEntry, ServiceContext serviceContext)
			throws PortalException, AppUserValidateException {

		// Validation

		ModelValidator<AppUser> modelValidator = new AppUserValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		AppUser entry = _addEntry(orgEntry, serviceContext);

		AppUser addedEntry = appUserPersistence.update(entry);
		appUserPersistence.clearCache();

		return addedEntry;
	}

	public AppUser updateEntry(
			AppUser orgEntry, ServiceContext serviceContext)
			throws PortalException, AppUserValidateException {

		AppUser user = appUserLocalService.getAppUser(orgEntry.getAppUserId());

		// Validation

		ModelValidator<AppUser> modelValidator = new AppUserValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		AppUser entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		AppUser updatedEntry = appUserPersistence.update(entry);
		appUserPersistence.clearCache();

		return updatedEntry;
	}

	protected AppUser _addEntry(AppUser entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(AppUser.class.getName());

		AppUser newEntry = appUserPersistence.create(id);

		//AppUser user = appUserLocalService.getAppUser(serviceContext.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);

		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setFullName(entry.getFullName());
		newEntry.setEmailAddress(entry.getEmailAddress());
		newEntry.setPhoneNumber(entry.getPhoneNumber());
		newEntry.setLanguageId(entry.getLanguageId());
		newEntry.setPassword(entry.getPassword());
		newEntry.setPasswordReset(entry.getPasswordReset());
		newEntry.setPasswordResetToken(entry.getPasswordResetToken());
		newEntry.setPasswordModifiedDate(now);
		newEntry.setPasscode(entry.getPasscode());
		newEntry.setFacebookId(entry.getFacebookId());
		newEntry.setGoogleUserId(entry.getGoogleUserId());
		newEntry.setEmailAddressVerified(entry.getEmailAddressVerified());
		newEntry.setPhoneNumberVerified(entry.getPhoneNumberVerified());
		newEntry.setAgreedToTermsOfUse(entry.getAgreedToTermsOfUse());
		newEntry.setStatus(entry.getStatus());
		newEntry.setProfileImage(entry.getProfileImage());
		newEntry.setProfileBannerImage(entry.getProfileBannerImage());
		newEntry.setGcpToken(entry.getGcpToken());
		newEntry.setAndroidDeviceToken(entry.getAndroidDeviceToken());
		newEntry.setIosDeviceToken(entry.getIosDeviceToken());
		newEntry.setDeactivateReason(entry.getDeactivateReason());
		return newEntry;
	}

	protected AppUser _updateEntry(
			long primaryKey, AppUser entry, ServiceContext serviceContext)
			throws PortalException {

		AppUser updateEntry = fetchAppUser(primaryKey);

		//AppUser user = appUserLocalService.getAppUser(entry.getAppUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);

		updateEntry.setUuid(entry.getUuid());

		updateEntry.setAppUserId(entry.getAppUserId());
		updateEntry.setFullName(entry.getFullName());
		updateEntry.setEmailAddress(entry.getEmailAddress());
		updateEntry.setPhoneNumber(entry.getPhoneNumber());
		updateEntry.setLanguageId(entry.getLanguageId());
		updateEntry.setPassword(entry.getPassword());
		updateEntry.setPasswordReset(entry.getPasswordReset());
		updateEntry.setPasswordResetToken(entry.getPasswordResetToken());
		updateEntry.setPasswordModifiedDate(entry.getPasswordModifiedDate());
		updateEntry.setPasscode(entry.getPasscode());
		updateEntry.setFacebookId(entry.getFacebookId());
		updateEntry.setGoogleUserId(entry.getGoogleUserId());
		updateEntry.setEmailAddressVerified(entry.getEmailAddressVerified());
		updateEntry.setPhoneNumberVerified(entry.getPhoneNumberVerified());
		updateEntry.setAgreedToTermsOfUse(entry.getAgreedToTermsOfUse());
		updateEntry.setStatus(entry.getStatus());
		updateEntry.setProfileBannerImage(entry.getProfileBannerImage());
		updateEntry.setProfileImage(entry.getProfileImage());
		updateEntry.setGcpToken(entry.getGcpToken());
		updateEntry.setAndroidDeviceToken(entry.getAndroidDeviceToken());
		updateEntry.setIosDeviceToken(entry.getIosDeviceToken());
		updateEntry.setDeactivateReason(entry.getDeactivateReason());
		return updateEntry;
	}

	/**
	 * create function to convert plain password to hash password
	 * */
	public String createPassword(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}

	/**
	 * create function to compare plain password and hash password
	 * */
	public boolean comparePassword(String password, String passwordHash) {
		return BCrypt.checkpw(password, passwordHash);
	}

	public AppUser deleteEntry(long primaryKey) throws PortalException {
		AppUser entry = getAppUser(primaryKey);
		appUserPersistence.remove(entry);

		return entry;
	}

	public List<AppUser> findAllInGroup(long groupId) {

		return appUserPersistence.findByGroupId(groupId);
	}

	public List<AppUser> findAllInGroup(long groupId, int start, int end,
									 OrderByComparator<AppUser> obc) {

		return appUserPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<AppUser> findAllInGroup(long groupId, int start, int end) {

		return appUserPersistence.findByGroupId(groupId, start, end);
	}

	public int countAllInGroup(long groupId) {

		return appUserPersistence.countByGroupId(groupId);
	}

	public AppUser findByEmailAddress(String emailAddress)
			throws misk.app.users.exception.NoSuchAppUserException {

		return appUserPersistence.findByEmailAddress(emailAddress);
	}

	public AppUser fetchByEmailAddress(String emailAddress)  {

		return appUserPersistence.fetchByEmailAddress(emailAddress);
	}
	public AppUser fetchByPhoneNumber(String phonNumber) {

		return appUserPersistence.fetchByPhoneNumber(phonNumber);
	}
	public AppUser fetchByFacebookId(String facebookId) {
		return appUserPersistence.fetchByFacebookId(facebookId);
	}
	public AppUser fetchByGoogleUserId(String googleUserId) {
		return appUserPersistence.fetchByGoogleUserId(googleUserId);
	}
	public AppUser fetchByAppleUserId(String appleUserId) {
		return appUserPersistence.fetchByAppleUserId(appleUserId);
	}

	public boolean isEmailUsedByAnotherUser(String emailAddress, long appUserId) {
		int count = appUserFinder.isEmailUsedByAnotherUser(emailAddress, appUserId);
		return count > 0;
	}

	public boolean isPhoneUsedByAnotherUser(String phoneNumber, long appUserId) {
		int count = appUserFinder.isPhoneUsedByAnotherUser(phoneNumber, appUserId);
		return count > 0;
	}

	public List<AppUser> findByUserIds(long[] userIds) {
		return appUserFinder.findByUserIds(userIds);
	}

	public AppUser getAppUserFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, AppUserValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		AppUser entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchAppUser(primaryKey);
		}

		try {
			Date now = new Date();

			entry.setAppUserId(primaryKey);
			//entry.setHeadingMap(LocalizationUtil.getLocalizationMap(request, "heading"));
			entry.setFullName(ParamUtil.getString(request, "fullName"));
			entry.setEmailAddress(ParamUtil.getString(request, "emailAddress"));
			entry.setLanguageId(ParamUtil.getString(request, "languageId"));
			entry.setPassword(ParamUtil.getString(request, "password"));
			entry.setPasswordReset(ParamUtil.getBoolean(request, "passwordReset"));
			entry.setPasswordResetToken(ParamUtil.getString(request, "passwordResetToken"));
			//entry.setPasswordModifiedDate(getDateTimeFromRequest(request, "passwordModifiedDate"));
			entry.setPasswordModifiedDate(now);
			entry.setPasscode(ParamUtil.getString(request, "passcode"));
			entry.setFacebookId(ParamUtil.getString(request, "facebookId"));
			entry.setGoogleUserId(ParamUtil.getString(request, "googleUserId"));
			entry.setEmailAddressVerified(ParamUtil.getBoolean(request, "emailAddressVerified"));
			entry.setPhoneNumberVerified(ParamUtil.getBoolean(request, "phoneNumberVerified"));
			entry.setAgreedToTermsOfUse(ParamUtil.getBoolean(request, "agreedToTermsOfUse"));
			entry.setStatus(ParamUtil.getBoolean(request, "status"));
			entry.setProfileImage(ParamUtil.getString(request,"profileImage"));
			entry.setProfileBannerImage(ParamUtil.getString(request,"profileBannerImage"));
			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new AppUserValidateException(error);
		}

		return entry;
	}

	public AppUser getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(AppUser.class.getName());

		return createAppUser(primaryKey);
	}

	public Date getDateTimeFromRequest(PortletRequest request, String prefix) {
		int Year = ParamUtil.getInteger(request, prefix + "Year");
		int Month = ParamUtil.getInteger(request, prefix + "Month") + 1;
		int Day = ParamUtil.getInteger(request, prefix + "Day");
		int Hour = ParamUtil.getInteger(request, prefix + "Hour");
		int Minute = ParamUtil.getInteger(request, prefix + "Minute");
		int AmPm = ParamUtil.getInteger(request, prefix + "AmPm");

		if (AmPm == Calendar.PM) {
			Hour += 12;
		}

		LocalDateTime ldt;

		try {
			ldt = LocalDateTime.of(Year, Month, Day, Hour, Minute, 0);
		}
		catch (Exception e) {
			_log.error(
					"Unnable get date data. Initialize with current date", e);
			Date in = new Date();

			Instant instant = in.toInstant();

			return Date.from(instant);
		}

		return Date.from(
				ldt.atZone(
						ZoneId.systemDefault()
				).toInstant());
	}

	private static Log _log = LogFactoryUtil.getLog(
			AppUserLocalServiceImpl.class);
}