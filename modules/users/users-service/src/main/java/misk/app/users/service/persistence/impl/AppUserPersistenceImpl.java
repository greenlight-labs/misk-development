/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import misk.app.users.exception.NoSuchAppUserException;
import misk.app.users.model.AppUser;
import misk.app.users.model.impl.AppUserImpl;
import misk.app.users.model.impl.AppUserModelImpl;
import misk.app.users.service.persistence.AppUserPersistence;
import misk.app.users.service.persistence.AppUserUtil;
import misk.app.users.service.persistence.impl.constants.AppUserPersistenceConstants;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the app user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = AppUserPersistence.class)
public class AppUserPersistenceImpl
	extends BasePersistenceImpl<AppUser> implements AppUserPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>AppUserUtil</code> to access the app user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		AppUserImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the app users where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching app users
	 */
	@Override
	public List<AppUser> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	@Override
	public List<AppUser> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<AppUser> list = null;

		if (useFinderCache) {
			list = (List<AppUser>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppUser appUser : list) {
					if (!uuid.equals(appUser.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<AppUser>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByUuid_First(
			String uuid, OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByUuid_First(uuid, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByUuid_First(
		String uuid, OrderByComparator<AppUser> orderByComparator) {

		List<AppUser> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByUuid_Last(
			String uuid, OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByUuid_Last(uuid, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByUuid_Last(
		String uuid, OrderByComparator<AppUser> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<AppUser> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where uuid = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser[] findByUuid_PrevAndNext(
			long appUserId, String uuid,
			OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		uuid = Objects.toString(uuid, "");

		AppUser appUser = findByPrimaryKey(appUserId);

		Session session = null;

		try {
			session = openSession();

			AppUser[] array = new AppUserImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, appUser, uuid, orderByComparator, true);

			array[1] = appUser;

			array[2] = getByUuid_PrevAndNext(
				session, appUser, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppUser getByUuid_PrevAndNext(
		Session session, AppUser appUser, String uuid,
		OrderByComparator<AppUser> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_APPUSER_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(appUser)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppUser> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the app users where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (AppUser appUser :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(appUser);
		}
	}

	/**
	 * Returns the number of app users where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching app users
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 = "appUser.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(appUser.uuid IS NULL OR appUser.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByUUID_G(String uuid, long groupId)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByUUID_G(uuid, groupId);

		if (appUser == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchAppUserException(sb.toString());
		}

		return appUser;
	}

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the app user where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof AppUser) {
			AppUser appUser = (AppUser)result;

			if (!Objects.equals(uuid, appUser.getUuid()) ||
				(groupId != appUser.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<AppUser> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					AppUser appUser = list.get(0);

					result = appUser;

					cacheResult(appUser);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (AppUser)result;
		}
	}

	/**
	 * Removes the app user where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the app user that was removed
	 */
	@Override
	public AppUser removeByUUID_G(String uuid, long groupId)
		throws NoSuchAppUserException {

		AppUser appUser = findByUUID_G(uuid, groupId);

		return remove(appUser);
	}

	/**
	 * Returns the number of app users where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching app users
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"appUser.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(appUser.uuid IS NULL OR appUser.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"appUser.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching app users
	 */
	@Override
	public List<AppUser> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	@Override
	public List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the app users where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<AppUser> list = null;

		if (useFinderCache) {
			list = (List<AppUser>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppUser appUser : list) {
					if (!uuid.equals(appUser.getUuid()) ||
						(companyId != appUser.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<AppUser>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the first app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<AppUser> orderByComparator) {

		List<AppUser> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the last app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<AppUser> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<AppUser> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser[] findByUuid_C_PrevAndNext(
			long appUserId, String uuid, long companyId,
			OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		uuid = Objects.toString(uuid, "");

		AppUser appUser = findByPrimaryKey(appUserId);

		Session session = null;

		try {
			session = openSession();

			AppUser[] array = new AppUserImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, appUser, uuid, companyId, orderByComparator, true);

			array[1] = appUser;

			array[2] = getByUuid_C_PrevAndNext(
				session, appUser, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppUser getByUuid_C_PrevAndNext(
		Session session, AppUser appUser, String uuid, long companyId,
		OrderByComparator<AppUser> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_APPUSER_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(appUser)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppUser> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the app users where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (AppUser appUser :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(appUser);
		}
	}

	/**
	 * Returns the number of app users where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching app users
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"appUser.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(appUser.uuid IS NULL OR appUser.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"appUser.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the app users where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching app users
	 */
	@Override
	public List<AppUser> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	@Override
	public List<AppUser> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the app users where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<AppUser> list = null;

		if (useFinderCache) {
			list = (List<AppUser>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppUser appUser : list) {
					if (groupId != appUser.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<AppUser>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByGroupId_First(
			long groupId, OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByGroupId_First(groupId, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the first app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByGroupId_First(
		long groupId, OrderByComparator<AppUser> orderByComparator) {

		List<AppUser> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByGroupId_Last(
			long groupId, OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByGroupId_Last(groupId, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the last app user in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByGroupId_Last(
		long groupId, OrderByComparator<AppUser> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<AppUser> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where groupId = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser[] findByGroupId_PrevAndNext(
			long appUserId, long groupId,
			OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = findByPrimaryKey(appUserId);

		Session session = null;

		try {
			session = openSession();

			AppUser[] array = new AppUserImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, appUser, groupId, orderByComparator, true);

			array[1] = appUser;

			array[2] = getByGroupId_PrevAndNext(
				session, appUser, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppUser getByGroupId_PrevAndNext(
		Session session, AppUser appUser, long groupId,
		OrderByComparator<AppUser> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_APPUSER_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(appUser)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppUser> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the app users where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (AppUser appUser :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(appUser);
		}
	}

	/**
	 * Returns the number of app users where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching app users
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"appUser.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByPhone;
	private FinderPath _finderPathWithoutPaginationFindByPhone;
	private FinderPath _finderPathCountByPhone;

	/**
	 * Returns all the app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app users
	 */
	@Override
	public List<AppUser> findByPhone(String phoneNumber) {
		return findByPhone(
			phoneNumber, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of matching app users
	 */
	@Override
	public List<AppUser> findByPhone(String phoneNumber, int start, int end) {
		return findByPhone(phoneNumber, start, end, null);
	}

	/**
	 * Returns an ordered range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByPhone(
		String phoneNumber, int start, int end,
		OrderByComparator<AppUser> orderByComparator) {

		return findByPhone(phoneNumber, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the app users where phoneNumber = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param phoneNumber the phone number
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching app users
	 */
	@Override
	public List<AppUser> findByPhone(
		String phoneNumber, int start, int end,
		OrderByComparator<AppUser> orderByComparator, boolean useFinderCache) {

		phoneNumber = Objects.toString(phoneNumber, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByPhone;
				finderArgs = new Object[] {phoneNumber};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByPhone;
			finderArgs = new Object[] {
				phoneNumber, start, end, orderByComparator
			};
		}

		List<AppUser> list = null;

		if (useFinderCache) {
			list = (List<AppUser>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (AppUser appUser : list) {
					if (!phoneNumber.equals(appUser.getPhoneNumber())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindPhoneNumber = false;

			if (phoneNumber.isEmpty()) {
				sb.append(_FINDER_COLUMN_PHONE_PHONENUMBER_3);
			}
			else {
				bindPhoneNumber = true;

				sb.append(_FINDER_COLUMN_PHONE_PHONENUMBER_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AppUserModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindPhoneNumber) {
					queryPos.add(phoneNumber);
				}

				list = (List<AppUser>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByPhone_First(
			String phoneNumber, OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByPhone_First(phoneNumber, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("phoneNumber=");
		sb.append(phoneNumber);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the first app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByPhone_First(
		String phoneNumber, OrderByComparator<AppUser> orderByComparator) {

		List<AppUser> list = findByPhone(phoneNumber, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByPhone_Last(
			String phoneNumber, OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByPhone_Last(phoneNumber, orderByComparator);

		if (appUser != null) {
			return appUser;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("phoneNumber=");
		sb.append(phoneNumber);

		sb.append("}");

		throw new NoSuchAppUserException(sb.toString());
	}

	/**
	 * Returns the last app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByPhone_Last(
		String phoneNumber, OrderByComparator<AppUser> orderByComparator) {

		int count = countByPhone(phoneNumber);

		if (count == 0) {
			return null;
		}

		List<AppUser> list = findByPhone(
			phoneNumber, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the app users before and after the current app user in the ordered set where phoneNumber = &#63;.
	 *
	 * @param appUserId the primary key of the current app user
	 * @param phoneNumber the phone number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser[] findByPhone_PrevAndNext(
			long appUserId, String phoneNumber,
			OrderByComparator<AppUser> orderByComparator)
		throws NoSuchAppUserException {

		phoneNumber = Objects.toString(phoneNumber, "");

		AppUser appUser = findByPrimaryKey(appUserId);

		Session session = null;

		try {
			session = openSession();

			AppUser[] array = new AppUserImpl[3];

			array[0] = getByPhone_PrevAndNext(
				session, appUser, phoneNumber, orderByComparator, true);

			array[1] = appUser;

			array[2] = getByPhone_PrevAndNext(
				session, appUser, phoneNumber, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected AppUser getByPhone_PrevAndNext(
		Session session, AppUser appUser, String phoneNumber,
		OrderByComparator<AppUser> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_APPUSER_WHERE);

		boolean bindPhoneNumber = false;

		if (phoneNumber.isEmpty()) {
			sb.append(_FINDER_COLUMN_PHONE_PHONENUMBER_3);
		}
		else {
			bindPhoneNumber = true;

			sb.append(_FINDER_COLUMN_PHONE_PHONENUMBER_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AppUserModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindPhoneNumber) {
			queryPos.add(phoneNumber);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(appUser)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<AppUser> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the app users where phoneNumber = &#63; from the database.
	 *
	 * @param phoneNumber the phone number
	 */
	@Override
	public void removeByPhone(String phoneNumber) {
		for (AppUser appUser :
				findByPhone(
					phoneNumber, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(appUser);
		}
	}

	/**
	 * Returns the number of app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the number of matching app users
	 */
	@Override
	public int countByPhone(String phoneNumber) {
		phoneNumber = Objects.toString(phoneNumber, "");

		FinderPath finderPath = _finderPathCountByPhone;

		Object[] finderArgs = new Object[] {phoneNumber};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindPhoneNumber = false;

			if (phoneNumber.isEmpty()) {
				sb.append(_FINDER_COLUMN_PHONE_PHONENUMBER_3);
			}
			else {
				bindPhoneNumber = true;

				sb.append(_FINDER_COLUMN_PHONE_PHONENUMBER_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindPhoneNumber) {
					queryPos.add(phoneNumber);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PHONE_PHONENUMBER_2 =
		"appUser.phoneNumber = ?";

	private static final String _FINDER_COLUMN_PHONE_PHONENUMBER_3 =
		"(appUser.phoneNumber IS NULL OR appUser.phoneNumber = '')";

	private FinderPath _finderPathFetchByEmailAddress;
	private FinderPath _finderPathCountByEmailAddress;

	/**
	 * Returns the app user where emailAddress = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param emailAddress the email address
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByEmailAddress(String emailAddress)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByEmailAddress(emailAddress);

		if (appUser == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("emailAddress=");
			sb.append(emailAddress);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchAppUserException(sb.toString());
		}

		return appUser;
	}

	/**
	 * Returns the app user where emailAddress = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param emailAddress the email address
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByEmailAddress(String emailAddress) {
		return fetchByEmailAddress(emailAddress, true);
	}

	/**
	 * Returns the app user where emailAddress = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param emailAddress the email address
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByEmailAddress(
		String emailAddress, boolean useFinderCache) {

		emailAddress = Objects.toString(emailAddress, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {emailAddress};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByEmailAddress, finderArgs, this);
		}

		if (result instanceof AppUser) {
			AppUser appUser = (AppUser)result;

			if (!Objects.equals(emailAddress, appUser.getEmailAddress())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindEmailAddress = false;

			if (emailAddress.isEmpty()) {
				sb.append(_FINDER_COLUMN_EMAILADDRESS_EMAILADDRESS_3);
			}
			else {
				bindEmailAddress = true;

				sb.append(_FINDER_COLUMN_EMAILADDRESS_EMAILADDRESS_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindEmailAddress) {
					queryPos.add(emailAddress);
				}

				List<AppUser> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByEmailAddress, finderArgs, list);
					}
				}
				else {
					AppUser appUser = list.get(0);

					result = appUser;

					cacheResult(appUser);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (AppUser)result;
		}
	}

	/**
	 * Removes the app user where emailAddress = &#63; from the database.
	 *
	 * @param emailAddress the email address
	 * @return the app user that was removed
	 */
	@Override
	public AppUser removeByEmailAddress(String emailAddress)
		throws NoSuchAppUserException {

		AppUser appUser = findByEmailAddress(emailAddress);

		return remove(appUser);
	}

	/**
	 * Returns the number of app users where emailAddress = &#63;.
	 *
	 * @param emailAddress the email address
	 * @return the number of matching app users
	 */
	@Override
	public int countByEmailAddress(String emailAddress) {
		emailAddress = Objects.toString(emailAddress, "");

		FinderPath finderPath = _finderPathCountByEmailAddress;

		Object[] finderArgs = new Object[] {emailAddress};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindEmailAddress = false;

			if (emailAddress.isEmpty()) {
				sb.append(_FINDER_COLUMN_EMAILADDRESS_EMAILADDRESS_3);
			}
			else {
				bindEmailAddress = true;

				sb.append(_FINDER_COLUMN_EMAILADDRESS_EMAILADDRESS_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindEmailAddress) {
					queryPos.add(emailAddress);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EMAILADDRESS_EMAILADDRESS_2 =
		"appUser.emailAddress = ?";

	private static final String _FINDER_COLUMN_EMAILADDRESS_EMAILADDRESS_3 =
		"(appUser.emailAddress IS NULL OR appUser.emailAddress = '')";

	private FinderPath _finderPathFetchByPhoneNumber;
	private FinderPath _finderPathCountByPhoneNumber;

	/**
	 * Returns the app user where phoneNumber = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByPhoneNumber(String phoneNumber)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByPhoneNumber(phoneNumber);

		if (appUser == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("phoneNumber=");
			sb.append(phoneNumber);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchAppUserException(sb.toString());
		}

		return appUser;
	}

	/**
	 * Returns the app user where phoneNumber = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param phoneNumber the phone number
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByPhoneNumber(String phoneNumber) {
		return fetchByPhoneNumber(phoneNumber, true);
	}

	/**
	 * Returns the app user where phoneNumber = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param phoneNumber the phone number
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByPhoneNumber(
		String phoneNumber, boolean useFinderCache) {

		phoneNumber = Objects.toString(phoneNumber, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {phoneNumber};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByPhoneNumber, finderArgs, this);
		}

		if (result instanceof AppUser) {
			AppUser appUser = (AppUser)result;

			if (!Objects.equals(phoneNumber, appUser.getPhoneNumber())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindPhoneNumber = false;

			if (phoneNumber.isEmpty()) {
				sb.append(_FINDER_COLUMN_PHONENUMBER_PHONENUMBER_3);
			}
			else {
				bindPhoneNumber = true;

				sb.append(_FINDER_COLUMN_PHONENUMBER_PHONENUMBER_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindPhoneNumber) {
					queryPos.add(phoneNumber);
				}

				List<AppUser> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByPhoneNumber, finderArgs, list);
					}
				}
				else {
					AppUser appUser = list.get(0);

					result = appUser;

					cacheResult(appUser);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (AppUser)result;
		}
	}

	/**
	 * Removes the app user where phoneNumber = &#63; from the database.
	 *
	 * @param phoneNumber the phone number
	 * @return the app user that was removed
	 */
	@Override
	public AppUser removeByPhoneNumber(String phoneNumber)
		throws NoSuchAppUserException {

		AppUser appUser = findByPhoneNumber(phoneNumber);

		return remove(appUser);
	}

	/**
	 * Returns the number of app users where phoneNumber = &#63;.
	 *
	 * @param phoneNumber the phone number
	 * @return the number of matching app users
	 */
	@Override
	public int countByPhoneNumber(String phoneNumber) {
		phoneNumber = Objects.toString(phoneNumber, "");

		FinderPath finderPath = _finderPathCountByPhoneNumber;

		Object[] finderArgs = new Object[] {phoneNumber};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindPhoneNumber = false;

			if (phoneNumber.isEmpty()) {
				sb.append(_FINDER_COLUMN_PHONENUMBER_PHONENUMBER_3);
			}
			else {
				bindPhoneNumber = true;

				sb.append(_FINDER_COLUMN_PHONENUMBER_PHONENUMBER_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindPhoneNumber) {
					queryPos.add(phoneNumber);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PHONENUMBER_PHONENUMBER_2 =
		"appUser.phoneNumber = ?";

	private static final String _FINDER_COLUMN_PHONENUMBER_PHONENUMBER_3 =
		"(appUser.phoneNumber IS NULL OR appUser.phoneNumber = '')";

	private FinderPath _finderPathFetchByFacebookId;
	private FinderPath _finderPathCountByFacebookId;

	/**
	 * Returns the app user where facebookId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param facebookId the facebook ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByFacebookId(String facebookId)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByFacebookId(facebookId);

		if (appUser == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("facebookId=");
			sb.append(facebookId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchAppUserException(sb.toString());
		}

		return appUser;
	}

	/**
	 * Returns the app user where facebookId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param facebookId the facebook ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByFacebookId(String facebookId) {
		return fetchByFacebookId(facebookId, true);
	}

	/**
	 * Returns the app user where facebookId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param facebookId the facebook ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByFacebookId(
		String facebookId, boolean useFinderCache) {

		facebookId = Objects.toString(facebookId, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {facebookId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByFacebookId, finderArgs, this);
		}

		if (result instanceof AppUser) {
			AppUser appUser = (AppUser)result;

			if (!Objects.equals(facebookId, appUser.getFacebookId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindFacebookId = false;

			if (facebookId.isEmpty()) {
				sb.append(_FINDER_COLUMN_FACEBOOKID_FACEBOOKID_3);
			}
			else {
				bindFacebookId = true;

				sb.append(_FINDER_COLUMN_FACEBOOKID_FACEBOOKID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindFacebookId) {
					queryPos.add(facebookId);
				}

				List<AppUser> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByFacebookId, finderArgs, list);
					}
				}
				else {
					AppUser appUser = list.get(0);

					result = appUser;

					cacheResult(appUser);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (AppUser)result;
		}
	}

	/**
	 * Removes the app user where facebookId = &#63; from the database.
	 *
	 * @param facebookId the facebook ID
	 * @return the app user that was removed
	 */
	@Override
	public AppUser removeByFacebookId(String facebookId)
		throws NoSuchAppUserException {

		AppUser appUser = findByFacebookId(facebookId);

		return remove(appUser);
	}

	/**
	 * Returns the number of app users where facebookId = &#63;.
	 *
	 * @param facebookId the facebook ID
	 * @return the number of matching app users
	 */
	@Override
	public int countByFacebookId(String facebookId) {
		facebookId = Objects.toString(facebookId, "");

		FinderPath finderPath = _finderPathCountByFacebookId;

		Object[] finderArgs = new Object[] {facebookId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindFacebookId = false;

			if (facebookId.isEmpty()) {
				sb.append(_FINDER_COLUMN_FACEBOOKID_FACEBOOKID_3);
			}
			else {
				bindFacebookId = true;

				sb.append(_FINDER_COLUMN_FACEBOOKID_FACEBOOKID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindFacebookId) {
					queryPos.add(facebookId);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FACEBOOKID_FACEBOOKID_2 =
		"appUser.facebookId = ?";

	private static final String _FINDER_COLUMN_FACEBOOKID_FACEBOOKID_3 =
		"(appUser.facebookId IS NULL OR appUser.facebookId = '')";

	private FinderPath _finderPathFetchByGoogleUserId;
	private FinderPath _finderPathCountByGoogleUserId;

	/**
	 * Returns the app user where googleUserId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param googleUserId the google user ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByGoogleUserId(String googleUserId)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByGoogleUserId(googleUserId);

		if (appUser == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("googleUserId=");
			sb.append(googleUserId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchAppUserException(sb.toString());
		}

		return appUser;
	}

	/**
	 * Returns the app user where googleUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param googleUserId the google user ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByGoogleUserId(String googleUserId) {
		return fetchByGoogleUserId(googleUserId, true);
	}

	/**
	 * Returns the app user where googleUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param googleUserId the google user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByGoogleUserId(
		String googleUserId, boolean useFinderCache) {

		googleUserId = Objects.toString(googleUserId, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {googleUserId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByGoogleUserId, finderArgs, this);
		}

		if (result instanceof AppUser) {
			AppUser appUser = (AppUser)result;

			if (!Objects.equals(googleUserId, appUser.getGoogleUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindGoogleUserId = false;

			if (googleUserId.isEmpty()) {
				sb.append(_FINDER_COLUMN_GOOGLEUSERID_GOOGLEUSERID_3);
			}
			else {
				bindGoogleUserId = true;

				sb.append(_FINDER_COLUMN_GOOGLEUSERID_GOOGLEUSERID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindGoogleUserId) {
					queryPos.add(googleUserId);
				}

				List<AppUser> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByGoogleUserId, finderArgs, list);
					}
				}
				else {
					AppUser appUser = list.get(0);

					result = appUser;

					cacheResult(appUser);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (AppUser)result;
		}
	}

	/**
	 * Removes the app user where googleUserId = &#63; from the database.
	 *
	 * @param googleUserId the google user ID
	 * @return the app user that was removed
	 */
	@Override
	public AppUser removeByGoogleUserId(String googleUserId)
		throws NoSuchAppUserException {

		AppUser appUser = findByGoogleUserId(googleUserId);

		return remove(appUser);
	}

	/**
	 * Returns the number of app users where googleUserId = &#63;.
	 *
	 * @param googleUserId the google user ID
	 * @return the number of matching app users
	 */
	@Override
	public int countByGoogleUserId(String googleUserId) {
		googleUserId = Objects.toString(googleUserId, "");

		FinderPath finderPath = _finderPathCountByGoogleUserId;

		Object[] finderArgs = new Object[] {googleUserId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindGoogleUserId = false;

			if (googleUserId.isEmpty()) {
				sb.append(_FINDER_COLUMN_GOOGLEUSERID_GOOGLEUSERID_3);
			}
			else {
				bindGoogleUserId = true;

				sb.append(_FINDER_COLUMN_GOOGLEUSERID_GOOGLEUSERID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindGoogleUserId) {
					queryPos.add(googleUserId);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GOOGLEUSERID_GOOGLEUSERID_2 =
		"appUser.googleUserId = ?";

	private static final String _FINDER_COLUMN_GOOGLEUSERID_GOOGLEUSERID_3 =
		"(appUser.googleUserId IS NULL OR appUser.googleUserId = '')";

	private FinderPath _finderPathFetchByAppleUserId;
	private FinderPath _finderPathCountByAppleUserId;

	/**
	 * Returns the app user where appleUserId = &#63; or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param appleUserId the apple user ID
	 * @return the matching app user
	 * @throws NoSuchAppUserException if a matching app user could not be found
	 */
	@Override
	public AppUser findByAppleUserId(String appleUserId)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByAppleUserId(appleUserId);

		if (appUser == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("appleUserId=");
			sb.append(appleUserId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchAppUserException(sb.toString());
		}

		return appUser;
	}

	/**
	 * Returns the app user where appleUserId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appleUserId the apple user ID
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByAppleUserId(String appleUserId) {
		return fetchByAppleUserId(appleUserId, true);
	}

	/**
	 * Returns the app user where appleUserId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appleUserId the apple user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching app user, or <code>null</code> if a matching app user could not be found
	 */
	@Override
	public AppUser fetchByAppleUserId(
		String appleUserId, boolean useFinderCache) {

		appleUserId = Objects.toString(appleUserId, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {appleUserId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByAppleUserId, finderArgs, this);
		}

		if (result instanceof AppUser) {
			AppUser appUser = (AppUser)result;

			if (!Objects.equals(appleUserId, appUser.getAppleUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_SELECT_APPUSER_WHERE);

			boolean bindAppleUserId = false;

			if (appleUserId.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPLEUSERID_APPLEUSERID_3);
			}
			else {
				bindAppleUserId = true;

				sb.append(_FINDER_COLUMN_APPLEUSERID_APPLEUSERID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindAppleUserId) {
					queryPos.add(appleUserId);
				}

				List<AppUser> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByAppleUserId, finderArgs, list);
					}
				}
				else {
					AppUser appUser = list.get(0);

					result = appUser;

					cacheResult(appUser);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (AppUser)result;
		}
	}

	/**
	 * Removes the app user where appleUserId = &#63; from the database.
	 *
	 * @param appleUserId the apple user ID
	 * @return the app user that was removed
	 */
	@Override
	public AppUser removeByAppleUserId(String appleUserId)
		throws NoSuchAppUserException {

		AppUser appUser = findByAppleUserId(appleUserId);

		return remove(appUser);
	}

	/**
	 * Returns the number of app users where appleUserId = &#63;.
	 *
	 * @param appleUserId the apple user ID
	 * @return the number of matching app users
	 */
	@Override
	public int countByAppleUserId(String appleUserId) {
		appleUserId = Objects.toString(appleUserId, "");

		FinderPath finderPath = _finderPathCountByAppleUserId;

		Object[] finderArgs = new Object[] {appleUserId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_APPUSER_WHERE);

			boolean bindAppleUserId = false;

			if (appleUserId.isEmpty()) {
				sb.append(_FINDER_COLUMN_APPLEUSERID_APPLEUSERID_3);
			}
			else {
				bindAppleUserId = true;

				sb.append(_FINDER_COLUMN_APPLEUSERID_APPLEUSERID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindAppleUserId) {
					queryPos.add(appleUserId);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPLEUSERID_APPLEUSERID_2 =
		"appUser.appleUserId = ?";

	private static final String _FINDER_COLUMN_APPLEUSERID_APPLEUSERID_3 =
		"(appUser.appleUserId IS NULL OR appUser.appleUserId = '')";

	public AppUserPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("password", "password_");

		setDBColumnNames(dbColumnNames);

		setModelClass(AppUser.class);

		setModelImplClass(AppUserImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the app user in the entity cache if it is enabled.
	 *
	 * @param appUser the app user
	 */
	@Override
	public void cacheResult(AppUser appUser) {
		entityCache.putResult(
			AppUserImpl.class, appUser.getPrimaryKey(), appUser);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {appUser.getUuid(), appUser.getGroupId()}, appUser);

		finderCache.putResult(
			_finderPathFetchByEmailAddress,
			new Object[] {appUser.getEmailAddress()}, appUser);

		finderCache.putResult(
			_finderPathFetchByPhoneNumber,
			new Object[] {appUser.getPhoneNumber()}, appUser);

		finderCache.putResult(
			_finderPathFetchByFacebookId,
			new Object[] {appUser.getFacebookId()}, appUser);

		finderCache.putResult(
			_finderPathFetchByGoogleUserId,
			new Object[] {appUser.getGoogleUserId()}, appUser);

		finderCache.putResult(
			_finderPathFetchByAppleUserId,
			new Object[] {appUser.getAppleUserId()}, appUser);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the app users in the entity cache if it is enabled.
	 *
	 * @param appUsers the app users
	 */
	@Override
	public void cacheResult(List<AppUser> appUsers) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (appUsers.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (AppUser appUser : appUsers) {
			if (entityCache.getResult(
					AppUserImpl.class, appUser.getPrimaryKey()) == null) {

				cacheResult(appUser);
			}
		}
	}

	/**
	 * Clears the cache for all app users.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AppUserImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the app user.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(AppUser appUser) {
		entityCache.removeResult(AppUserImpl.class, appUser);
	}

	@Override
	public void clearCache(List<AppUser> appUsers) {
		for (AppUser appUser : appUsers) {
			entityCache.removeResult(AppUserImpl.class, appUser);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(AppUserImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(AppUserModelImpl appUserModelImpl) {
		Object[] args = new Object[] {
			appUserModelImpl.getUuid(), appUserModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, appUserModelImpl, false);

		args = new Object[] {appUserModelImpl.getEmailAddress()};

		finderCache.putResult(
			_finderPathCountByEmailAddress, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByEmailAddress, args, appUserModelImpl, false);

		args = new Object[] {appUserModelImpl.getPhoneNumber()};

		finderCache.putResult(
			_finderPathCountByPhoneNumber, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByPhoneNumber, args, appUserModelImpl, false);

		args = new Object[] {appUserModelImpl.getFacebookId()};

		finderCache.putResult(
			_finderPathCountByFacebookId, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByFacebookId, args, appUserModelImpl, false);

		args = new Object[] {appUserModelImpl.getGoogleUserId()};

		finderCache.putResult(
			_finderPathCountByGoogleUserId, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByGoogleUserId, args, appUserModelImpl, false);

		args = new Object[] {appUserModelImpl.getAppleUserId()};

		finderCache.putResult(
			_finderPathCountByAppleUserId, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByAppleUserId, args, appUserModelImpl, false);
	}

	/**
	 * Creates a new app user with the primary key. Does not add the app user to the database.
	 *
	 * @param appUserId the primary key for the new app user
	 * @return the new app user
	 */
	@Override
	public AppUser create(long appUserId) {
		AppUser appUser = new AppUserImpl();

		appUser.setNew(true);
		appUser.setPrimaryKey(appUserId);

		String uuid = PortalUUIDUtil.generate();

		appUser.setUuid(uuid);

		appUser.setCompanyId(CompanyThreadLocal.getCompanyId());

		return appUser;
	}

	/**
	 * Removes the app user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user that was removed
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser remove(long appUserId) throws NoSuchAppUserException {
		return remove((Serializable)appUserId);
	}

	/**
	 * Removes the app user with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the app user
	 * @return the app user that was removed
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser remove(Serializable primaryKey)
		throws NoSuchAppUserException {

		Session session = null;

		try {
			session = openSession();

			AppUser appUser = (AppUser)session.get(
				AppUserImpl.class, primaryKey);

			if (appUser == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAppUserException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(appUser);
		}
		catch (NoSuchAppUserException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected AppUser removeImpl(AppUser appUser) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(appUser)) {
				appUser = (AppUser)session.get(
					AppUserImpl.class, appUser.getPrimaryKeyObj());
			}

			if (appUser != null) {
				session.delete(appUser);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (appUser != null) {
			clearCache(appUser);
		}

		return appUser;
	}

	@Override
	public AppUser updateImpl(AppUser appUser) {
		boolean isNew = appUser.isNew();

		if (!(appUser instanceof AppUserModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(appUser.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(appUser);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in appUser proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom AppUser implementation " +
					appUser.getClass());
		}

		AppUserModelImpl appUserModelImpl = (AppUserModelImpl)appUser;

		if (Validator.isNull(appUser.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			appUser.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (appUser.getCreateDate() == null)) {
			if (serviceContext == null) {
				appUser.setCreateDate(date);
			}
			else {
				appUser.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!appUserModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				appUser.setModifiedDate(date);
			}
			else {
				appUser.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(appUser);
			}
			else {
				appUser = (AppUser)session.merge(appUser);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(AppUserImpl.class, appUserModelImpl, false, true);

		cacheUniqueFindersCache(appUserModelImpl);

		if (isNew) {
			appUser.setNew(false);
		}

		appUser.resetOriginalValues();

		return appUser;
	}

	/**
	 * Returns the app user with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the app user
	 * @return the app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAppUserException {

		AppUser appUser = fetchByPrimaryKey(primaryKey);

		if (appUser == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAppUserException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return appUser;
	}

	/**
	 * Returns the app user with the primary key or throws a <code>NoSuchAppUserException</code> if it could not be found.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user
	 * @throws NoSuchAppUserException if a app user with the primary key could not be found
	 */
	@Override
	public AppUser findByPrimaryKey(long appUserId)
		throws NoSuchAppUserException {

		return findByPrimaryKey((Serializable)appUserId);
	}

	/**
	 * Returns the app user with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param appUserId the primary key of the app user
	 * @return the app user, or <code>null</code> if a app user with the primary key could not be found
	 */
	@Override
	public AppUser fetchByPrimaryKey(long appUserId) {
		return fetchByPrimaryKey((Serializable)appUserId);
	}

	/**
	 * Returns all the app users.
	 *
	 * @return the app users
	 */
	@Override
	public List<AppUser> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @return the range of app users
	 */
	@Override
	public List<AppUser> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of app users
	 */
	@Override
	public List<AppUser> findAll(
		int start, int end, OrderByComparator<AppUser> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the app users.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AppUserModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of app users
	 * @param end the upper bound of the range of app users (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of app users
	 */
	@Override
	public List<AppUser> findAll(
		int start, int end, OrderByComparator<AppUser> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<AppUser> list = null;

		if (useFinderCache) {
			list = (List<AppUser>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_APPUSER);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_APPUSER;

				sql = sql.concat(AppUserModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<AppUser>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the app users from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (AppUser appUser : findAll()) {
			remove(appUser);
		}
	}

	/**
	 * Returns the number of app users.
	 *
	 * @return the number of app users
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_APPUSER);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "appUserId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_APPUSER;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AppUserModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the app user persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class, new AppUserModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", AppUser.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathWithPaginationFindByPhone = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPhone",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"phoneNumber"}, true);

		_finderPathWithoutPaginationFindByPhone = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPhone",
			new String[] {String.class.getName()}, new String[] {"phoneNumber"},
			true);

		_finderPathCountByPhone = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPhone",
			new String[] {String.class.getName()}, new String[] {"phoneNumber"},
			false);

		_finderPathFetchByEmailAddress = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByEmailAddress",
			new String[] {String.class.getName()},
			new String[] {"emailAddress"}, true);

		_finderPathCountByEmailAddress = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEmailAddress",
			new String[] {String.class.getName()},
			new String[] {"emailAddress"}, false);

		_finderPathFetchByPhoneNumber = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByPhoneNumber",
			new String[] {String.class.getName()}, new String[] {"phoneNumber"},
			true);

		_finderPathCountByPhoneNumber = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPhoneNumber",
			new String[] {String.class.getName()}, new String[] {"phoneNumber"},
			false);

		_finderPathFetchByFacebookId = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByFacebookId",
			new String[] {String.class.getName()}, new String[] {"facebookId"},
			true);

		_finderPathCountByFacebookId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFacebookId",
			new String[] {String.class.getName()}, new String[] {"facebookId"},
			false);

		_finderPathFetchByGoogleUserId = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByGoogleUserId",
			new String[] {String.class.getName()},
			new String[] {"googleUserId"}, true);

		_finderPathCountByGoogleUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGoogleUserId",
			new String[] {String.class.getName()},
			new String[] {"googleUserId"}, false);

		_finderPathFetchByAppleUserId = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByAppleUserId",
			new String[] {String.class.getName()}, new String[] {"appleUserId"},
			true);

		_finderPathCountByAppleUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAppleUserId",
			new String[] {String.class.getName()}, new String[] {"appleUserId"},
			false);

		_setAppUserUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setAppUserUtilPersistence(null);

		entityCache.removeCache(AppUserImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setAppUserUtilPersistence(
		AppUserPersistence appUserPersistence) {

		try {
			Field field = AppUserUtil.class.getDeclaredField("_persistence");

			field.setAccessible(true);

			field.set(null, appUserPersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = AppUserPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = AppUserPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = AppUserPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_APPUSER =
		"SELECT appUser FROM AppUser appUser";

	private static final String _SQL_SELECT_APPUSER_WHERE =
		"SELECT appUser FROM AppUser appUser WHERE ";

	private static final String _SQL_COUNT_APPUSER =
		"SELECT COUNT(appUser) FROM AppUser appUser";

	private static final String _SQL_COUNT_APPUSER_WHERE =
		"SELECT COUNT(appUser) FROM AppUser appUser WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "appUser.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No AppUser exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No AppUser exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		AppUserPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "password"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class AppUserModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			AppUserModelImpl appUserModelImpl = (AppUserModelImpl)baseModel;

			long columnBitmask = appUserModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(appUserModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						appUserModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(AppUserPersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(appUserModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			AppUserModelImpl appUserModelImpl, String[] columnNames,
			boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] = appUserModelImpl.getColumnOriginalValue(
						columnName);
				}
				else {
					arguments[i] = appUserModelImpl.getColumnValue(columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= AppUserModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}