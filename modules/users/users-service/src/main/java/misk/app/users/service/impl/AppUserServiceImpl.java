/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.service.impl;

import com.liferay.portal.aop.AopService;

import misk.app.users.service.base.AppUserServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the app user remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>misk.app.users.service.AppUserService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AppUserServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=appuser",
		"json.web.service.context.path=AppUser"
	},
	service = AopService.class
)
public class AppUserServiceImpl extends AppUserServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>misk.app.users.service.AppUserServiceUtil</code> to access the app user remote service.
	 */
}