package misk.app.users.service.persistence.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import misk.app.users.model.AppUser;
import misk.app.users.model.impl.AppUserImpl;
import misk.app.users.service.impl.AppUserLocalServiceImpl;
import misk.app.users.service.persistence.AppUserFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


@Component(service = AppUserFinder.class)
public class AppUserFinderImpl
        extends AppUserFinderBaseImpl implements AppUserFinder {

    public static final String IS_EMAIL_USED_BY_ANOTHER_USER =
            AppUserFinder.class.getName() + ".isEmailUsedByAnotherUser";

    public static final String IS_PHONE_USED_BY_ANOTHER_USER =
            AppUserFinder.class.getName() + ".isPhoneUsedByAnotherUser";
    private static final String FIND_BY_USER_IDS = AppUserFinder.class.getName() + ".findByUserIds";

    public int isEmailUsedByAnotherUser(String emailAddress, long appUserId) {
        Session session = null;

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), IS_EMAIL_USED_BY_ANOTHER_USER);

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.addScalar(COUNT_COLUMN_NAME, Type.LONG);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add(emailAddress);
            queryPos.add(appUserId);

            Iterator<Long> iterator = sqlQuery.iterate();

            if (iterator.hasNext()) {
                Long count = iterator.next();

                if (count != null) {
                    return count.intValue();
                }
            }

            return 0;
        } catch (Exception exception) {
            throw new SystemException(exception);
        } finally {
            closeSession(session);
        }
    }

    public int isPhoneUsedByAnotherUser(String phoneNumber, long appUserId) {
        Session session = null;

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), IS_PHONE_USED_BY_ANOTHER_USER);

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.addScalar(COUNT_COLUMN_NAME, Type.LONG);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add(phoneNumber);
            queryPos.add(appUserId);

            Iterator<Long> iterator = sqlQuery.iterate();

            if (iterator.hasNext()) {
                Long count = iterator.next();

                if (count != null) {
                    return count.intValue();
                }
            }

            return 0;
        } catch (Exception exception) {
            throw new SystemException(exception);
        } finally {
            closeSession(session);
        }
    }

    public List<AppUser> findByUserIds(long[] userIds) {
        Session session = null;
        List<AppUser> appUsers = Collections.emptyList();

        try {
            if (userIds.length > 0) {
                session = openSession();
                String sql = _customSQL.get(getClass(), FIND_BY_USER_IDS);
                sql = StringUtil.replace(sql, "[$USER_IDS$]", StringUtil.merge(userIds, StringPool.COMMA));
                //sql = StringUtil.replaceLast(sql, "AND", StringPool.BLANK);

                SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

                sqlQuery.setCacheable(false);
                sqlQuery.addEntity("AppUser", AppUserImpl.class);

                /*QueryPos queryPos = QueryPos.getInstance(sqlQuery);
                queryPos.add(userIds);*/
                appUsers = (List<AppUser>) sqlQuery.list();
            }
            return appUsers;
        } catch (Exception exception) {
            throw new SystemException(exception);
        } finally {
            closeSession(session);
        }
    }

    @Reference
    private CustomSQL _customSQL;

    private static Log _log = LogFactoryUtil.getLog(AppUserFinderImpl.class);
}
