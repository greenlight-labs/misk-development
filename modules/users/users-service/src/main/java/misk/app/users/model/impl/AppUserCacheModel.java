/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package misk.app.users.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import misk.app.users.model.AppUser;

/**
 * The cache model class for representing AppUser in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AppUserCacheModel implements CacheModel<AppUser>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AppUserCacheModel)) {
			return false;
		}

		AppUserCacheModel appUserCacheModel = (AppUserCacheModel)object;

		if (appUserId == appUserCacheModel.appUserId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, appUserId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(57);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", appUserId=");
		sb.append(appUserId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", fullName=");
		sb.append(fullName);
		sb.append(", emailAddress=");
		sb.append(emailAddress);
		sb.append(", phoneNumber=");
		sb.append(phoneNumber);
		sb.append(", languageId=");
		sb.append(languageId);
		sb.append(", password=");
		sb.append(password);
		sb.append(", passwordReset=");
		sb.append(passwordReset);
		sb.append(", passwordResetToken=");
		sb.append(passwordResetToken);
		sb.append(", passwordModifiedDate=");
		sb.append(passwordModifiedDate);
		sb.append(", passcode=");
		sb.append(passcode);
		sb.append(", facebookId=");
		sb.append(facebookId);
		sb.append(", googleUserId=");
		sb.append(googleUserId);
		sb.append(", appleUserId=");
		sb.append(appleUserId);
		sb.append(", emailAddressVerified=");
		sb.append(emailAddressVerified);
		sb.append(", phoneNumberVerified=");
		sb.append(phoneNumberVerified);
		sb.append(", agreedToTermsOfUse=");
		sb.append(agreedToTermsOfUse);
		sb.append(", status=");
		sb.append(status);
		sb.append(", profileBannerImage=");
		sb.append(profileBannerImage);
		sb.append(", profileImage=");
		sb.append(profileImage);
		sb.append(", gcpToken=");
		sb.append(gcpToken);
		sb.append(", androidDeviceToken=");
		sb.append(androidDeviceToken);
		sb.append(", iosDeviceToken=");
		sb.append(iosDeviceToken);
		sb.append(", deactivateReason=");
		sb.append(deactivateReason);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public AppUser toEntityModel() {
		AppUserImpl appUserImpl = new AppUserImpl();

		if (uuid == null) {
			appUserImpl.setUuid("");
		}
		else {
			appUserImpl.setUuid(uuid);
		}

		appUserImpl.setAppUserId(appUserId);
		appUserImpl.setGroupId(groupId);
		appUserImpl.setCompanyId(companyId);

		if (createDate == Long.MIN_VALUE) {
			appUserImpl.setCreateDate(null);
		}
		else {
			appUserImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			appUserImpl.setModifiedDate(null);
		}
		else {
			appUserImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (fullName == null) {
			appUserImpl.setFullName("");
		}
		else {
			appUserImpl.setFullName(fullName);
		}

		if (emailAddress == null) {
			appUserImpl.setEmailAddress("");
		}
		else {
			appUserImpl.setEmailAddress(emailAddress);
		}

		if (phoneNumber == null) {
			appUserImpl.setPhoneNumber("");
		}
		else {
			appUserImpl.setPhoneNumber(phoneNumber);
		}

		if (languageId == null) {
			appUserImpl.setLanguageId("");
		}
		else {
			appUserImpl.setLanguageId(languageId);
		}

		if (password == null) {
			appUserImpl.setPassword("");
		}
		else {
			appUserImpl.setPassword(password);
		}

		appUserImpl.setPasswordReset(passwordReset);

		if (passwordResetToken == null) {
			appUserImpl.setPasswordResetToken("");
		}
		else {
			appUserImpl.setPasswordResetToken(passwordResetToken);
		}

		if (passwordModifiedDate == Long.MIN_VALUE) {
			appUserImpl.setPasswordModifiedDate(null);
		}
		else {
			appUserImpl.setPasswordModifiedDate(new Date(passwordModifiedDate));
		}

		if (passcode == null) {
			appUserImpl.setPasscode("");
		}
		else {
			appUserImpl.setPasscode(passcode);
		}

		if (facebookId == null) {
			appUserImpl.setFacebookId("");
		}
		else {
			appUserImpl.setFacebookId(facebookId);
		}

		if (googleUserId == null) {
			appUserImpl.setGoogleUserId("");
		}
		else {
			appUserImpl.setGoogleUserId(googleUserId);
		}

		if (appleUserId == null) {
			appUserImpl.setAppleUserId("");
		}
		else {
			appUserImpl.setAppleUserId(appleUserId);
		}

		appUserImpl.setEmailAddressVerified(emailAddressVerified);
		appUserImpl.setPhoneNumberVerified(phoneNumberVerified);
		appUserImpl.setAgreedToTermsOfUse(agreedToTermsOfUse);
		appUserImpl.setStatus(status);

		if (profileBannerImage == null) {
			appUserImpl.setProfileBannerImage("");
		}
		else {
			appUserImpl.setProfileBannerImage(profileBannerImage);
		}

		if (profileImage == null) {
			appUserImpl.setProfileImage("");
		}
		else {
			appUserImpl.setProfileImage(profileImage);
		}

		if (gcpToken == null) {
			appUserImpl.setGcpToken("");
		}
		else {
			appUserImpl.setGcpToken(gcpToken);
		}

		if (androidDeviceToken == null) {
			appUserImpl.setAndroidDeviceToken("");
		}
		else {
			appUserImpl.setAndroidDeviceToken(androidDeviceToken);
		}

		if (iosDeviceToken == null) {
			appUserImpl.setIosDeviceToken("");
		}
		else {
			appUserImpl.setIosDeviceToken(iosDeviceToken);
		}

		if (deactivateReason == null) {
			appUserImpl.setDeactivateReason("");
		}
		else {
			appUserImpl.setDeactivateReason(deactivateReason);
		}

		appUserImpl.resetOriginalValues();

		return appUserImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		appUserId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		fullName = objectInput.readUTF();
		emailAddress = objectInput.readUTF();
		phoneNumber = objectInput.readUTF();
		languageId = objectInput.readUTF();
		password = objectInput.readUTF();

		passwordReset = objectInput.readBoolean();
		passwordResetToken = objectInput.readUTF();
		passwordModifiedDate = objectInput.readLong();
		passcode = objectInput.readUTF();
		facebookId = objectInput.readUTF();
		googleUserId = objectInput.readUTF();
		appleUserId = objectInput.readUTF();

		emailAddressVerified = objectInput.readBoolean();

		phoneNumberVerified = objectInput.readBoolean();

		agreedToTermsOfUse = objectInput.readBoolean();

		status = objectInput.readBoolean();
		profileBannerImage = objectInput.readUTF();
		profileImage = objectInput.readUTF();
		gcpToken = objectInput.readUTF();
		androidDeviceToken = objectInput.readUTF();
		iosDeviceToken = objectInput.readUTF();
		deactivateReason = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(appUserId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (fullName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(fullName);
		}

		if (emailAddress == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(emailAddress);
		}

		if (phoneNumber == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(phoneNumber);
		}

		if (languageId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(languageId);
		}

		if (password == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(password);
		}

		objectOutput.writeBoolean(passwordReset);

		if (passwordResetToken == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(passwordResetToken);
		}

		objectOutput.writeLong(passwordModifiedDate);

		if (passcode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(passcode);
		}

		if (facebookId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(facebookId);
		}

		if (googleUserId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(googleUserId);
		}

		if (appleUserId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(appleUserId);
		}

		objectOutput.writeBoolean(emailAddressVerified);

		objectOutput.writeBoolean(phoneNumberVerified);

		objectOutput.writeBoolean(agreedToTermsOfUse);

		objectOutput.writeBoolean(status);

		if (profileBannerImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(profileBannerImage);
		}

		if (profileImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(profileImage);
		}

		if (gcpToken == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(gcpToken);
		}

		if (androidDeviceToken == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(androidDeviceToken);
		}

		if (iosDeviceToken == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(iosDeviceToken);
		}

		if (deactivateReason == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(deactivateReason);
		}
	}

	public String uuid;
	public long appUserId;
	public long groupId;
	public long companyId;
	public long createDate;
	public long modifiedDate;
	public String fullName;
	public String emailAddress;
	public String phoneNumber;
	public String languageId;
	public String password;
	public boolean passwordReset;
	public String passwordResetToken;
	public long passwordModifiedDate;
	public String passcode;
	public String facebookId;
	public String googleUserId;
	public String appleUserId;
	public boolean emailAddressVerified;
	public boolean phoneNumberVerified;
	public boolean agreedToTermsOfUse;
	public boolean status;
	public String profileBannerImage;
	public String profileImage;
	public String gcpToken;
	public String androidDeviceToken;
	public String iosDeviceToken;
	public String deactivateReason;

}