package misk.app.users.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import misk.app.users.exception.AppUserValidateException;
import misk.app.users.model.AppUser;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AppUser Validator
 * 
 * @author tz
 *
 */
public class AppUserValidator implements ModelValidator<AppUser> {

	@Override
	public void validate(AppUser entry) throws PortalException {
/*   */
        // Field userId
        validateAppUserId(entry.getAppUserId());

        // Field fullName
        validateFullName(entry.getFullName());

        // Field emailAddress
        validateEmailAddress(entry.getEmailAddress());

        // Field languageId
        validateLanguageId(entry.getLanguageId());

        // Field password
        validatePassword(entry.getPassword());

        // Field passwordReset
        validatePasswordReset(entry.getPasswordReset());

        // Field passwordResetToken
        validatePasswordResetToken(entry.getPasswordResetToken());

        // Field passwordModifiedDate
        validatePasswordModifiedDate(entry.getPasswordModifiedDate());

        // Field facebookId
        validateFacebookId(entry.getFacebookId());

        // Field googleUserId
        validateGoogleUserId(entry.getGoogleUserId());

        // Field appleUserId
        validateAppleUserId(entry.getAppleUserId());

        // Field emailAddressVerified
        validateEmailAddressVerified(entry.getEmailAddressVerified());

        // Field agreedToTermsOfUse
        validateAgreedToTermsOfUse(entry.getAgreedToTermsOfUse());

        // Field status
        validateStatus(entry.getStatus());

		if (0 < _errors.size()) {
			throw new AppUserValidateException(_errors);
		}
		
	}

/*   */
    /**
    * userId field Validation
    *
    * @param field userId
    */
    protected void validateAppUserId(long field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
    }

    /**
    * fullName field Validation
    *
    * @param field fullName
    */
    protected void validateFullName(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("user-fullname-required");
        }

    }

    /**
    * emailAddress field Validation
    *
    * @param field emailAddress
    */
    protected void validateEmailAddress(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("user-emailaddress-required");
        }

    }

    /**
    * languageId field Validation
    *
    * @param field languageId
    */
    protected void validateLanguageId(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("user-languageid-required");
        }

    }

    /**
    * password field Validation
    *
    * @param field password
    */
    protected void validatePassword(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * passwordReset field Validation
    *
    * @param field passwordReset
    */
    protected void validatePasswordReset(boolean field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * passwordResetToken field Validation
    *
    * @param field passwordResetToken
    */
    protected void validatePasswordResetToken(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * passwordModifiedDate field Validation
    *
    * @param field passwordModifiedDate
    */
    protected void validatePasswordModifiedDate(Date field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * facebookId field Validation
    *
    * @param field facebookId
    */
    protected void validateFacebookId(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * googleUserId field Validation
    *
    * @param field googleUserId
    */
    protected void validateGoogleUserId(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * appleUserId field Validation
    *
    * @param field appleUserId
    */
    protected void validateAppleUserId(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * emailAddressVerified field Validation
    *
    * @param field emailAddressVerified
    */
    protected void validateEmailAddressVerified(boolean field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * agreedToTermsOfUse field Validation
    *
    * @param field agreedToTermsOfUse
    */
    protected void validateAgreedToTermsOfUse(boolean field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * status field Validation
    *
    * @param field status
    */
    protected void validateStatus(boolean field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

/*  */ 	
	

	protected List<String> _errors = new ArrayList<>();

}
