/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import explore.wadic.model.Explore;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Explore. This utility wraps
 * <code>explore.wadic.service.impl.ExploreLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ExploreLocalService
 * @generated
 */
public class ExploreLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>explore.wadic.service.impl.ExploreLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Explore addEntry(
			Explore orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws explore.wadic.exception.ExploreValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static void addEntryGallery(
			Explore orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryGallery(orgEntry, request);
	}

	/**
	 * Adds the explore to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was added
	 */
	public static Explore addExplore(Explore explore) {
		return getService().addExplore(explore);
	}

	public static int countByGroupId(long groupId) {
		return getService().countByGroupId(groupId);
	}

	/**
	 * Creates a new explore with the primary key. Does not add the explore to the database.
	 *
	 * @param exploreId the primary key for the new explore
	 * @return the new explore
	 */
	public static Explore createExplore(long exploreId) {
		return getService().createExplore(exploreId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static Explore deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * Deletes the explore from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was removed
	 */
	public static Explore deleteExplore(Explore explore) {
		return getService().deleteExplore(explore);
	}

	/**
	 * Deletes the explore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore that was removed
	 * @throws PortalException if a explore with the primary key could not be found
	 */
	public static Explore deleteExplore(long exploreId) throws PortalException {
		return getService().deleteExplore(exploreId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Explore fetchExplore(long exploreId) {
		return getService().fetchExplore(exploreId);
	}

	/**
	 * Returns the explore matching the UUID and group.
	 *
	 * @param uuid the explore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchExploreByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchExploreByUuidAndGroupId(uuid, groupId);
	}

	public static List<Explore> findByGroupId(long groupId) {
		return getService().findByGroupId(groupId);
	}

	public static List<Explore> findByGroupId(
		long groupId, int start, int end) {

		return getService().findByGroupId(groupId, start, end);
	}

	public static List<Explore> findByGroupId(
		long groupId, int start, int end, OrderByComparator<Explore> obc) {

		return getService().findByGroupId(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the explore with the primary key.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore
	 * @throws PortalException if a explore with the primary key could not be found
	 */
	public static Explore getExplore(long exploreId) throws PortalException {
		return getService().getExplore(exploreId);
	}

	/**
	 * Returns the explore matching the UUID and group.
	 *
	 * @param uuid the explore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching explore
	 * @throws PortalException if a matching explore could not be found
	 */
	public static Explore getExploreByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getExploreByUuidAndGroupId(uuid, groupId);
	}

	public static Explore getExploreFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws explore.wadic.exception.ExploreValidateException,
			   javax.portlet.PortletException {

		return getService().getExploreFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of explores
	 */
	public static List<Explore> getExplores(int start, int end) {
		return getService().getExplores(start, end);
	}

	/**
	 * Returns all the explores matching the UUID and company.
	 *
	 * @param uuid the UUID of the explores
	 * @param companyId the primary key of the company
	 * @return the matching explores, or an empty list if no matches were found
	 */
	public static List<Explore> getExploresByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getExploresByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of explores matching the UUID and company.
	 *
	 * @param uuid the UUID of the explores
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching explores, or an empty list if no matches were found
	 */
	public static List<Explore> getExploresByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Explore> orderByComparator) {

		return getService().getExploresByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of explores.
	 *
	 * @return the number of explores
	 */
	public static int getExploresCount() {
		return getService().getExploresCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static List<explore.wadic.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getGalleries(actionRequest);
	}

	public static List<explore.wadic.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		List<explore.wadic.model.Gallery> defaultGalleries) {

		return getService().getGalleries(actionRequest, defaultGalleries);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Explore getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static Explore updateEntry(
			Explore orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws explore.wadic.exception.ExploreValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the explore in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was updated
	 */
	public static Explore updateExplore(Explore explore) {
		return getService().updateExplore(explore);
	}

	public static void updateGalleries(
			Explore entry, List<explore.wadic.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateGalleries(entry, galleries, serviceContext);
	}

	public static ExploreLocalService getService() {
		return _service;
	}

	private static volatile ExploreLocalService _service;

}