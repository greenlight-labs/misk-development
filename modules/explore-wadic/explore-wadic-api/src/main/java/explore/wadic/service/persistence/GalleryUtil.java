/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import explore.wadic.model.Gallery;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the gallery service. This utility wraps <code>explore.wadic.service.persistence.impl.GalleryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GalleryPersistence
 * @generated
 */
public class GalleryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Gallery gallery) {
		getPersistence().clearCache(gallery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Gallery> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Gallery> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Gallery> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Gallery> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Gallery update(Gallery gallery) {
		return getPersistence().update(gallery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Gallery update(
		Gallery gallery, ServiceContext serviceContext) {

		return getPersistence().update(gallery, serviceContext);
	}

	/**
	 * Returns all the galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching galleries
	 */
	public static List<Gallery> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public static List<Gallery> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Gallery> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByUuid_First(
			String uuid, OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByUuid_First(
		String uuid, OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByUuid_Last(
			String uuid, OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByUuid_Last(
		String uuid, OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public static Gallery[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByUuid_PrevAndNext(
			slideId, uuid, orderByComparator);
	}

	/**
	 * Removes all the galleries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching galleries
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the gallery where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchGalleryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByUUID_G(String uuid, long groupId)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the gallery where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the gallery that was removed
	 */
	public static Gallery removeByUUID_G(String uuid, long groupId)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of galleries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching galleries
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching galleries
	 */
	public static List<Gallery> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public static List<Gallery> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Gallery> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public static Gallery[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			slideId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the galleries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching galleries
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching galleries
	 */
	public static List<Gallery> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public static List<Gallery> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Gallery> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByGroupId_First(
			long groupId, OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByGroupId_First(
		long groupId, OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByGroupId_Last(
			long groupId, OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByGroupId_Last(
		long groupId, OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where groupId = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public static Gallery[] findByGroupId_PrevAndNext(
			long slideId, long groupId,
			OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByGroupId_PrevAndNext(
			slideId, groupId, orderByComparator);
	}

	/**
	 * Removes all the galleries where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching galleries
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the galleries where exploreId = &#63;.
	 *
	 * @param exploreId the explore ID
	 * @return the matching galleries
	 */
	public static List<Gallery> findByExploreId(long exploreId) {
		return getPersistence().findByExploreId(exploreId);
	}

	/**
	 * Returns a range of all the galleries where exploreId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param exploreId the explore ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of matching galleries
	 */
	public static List<Gallery> findByExploreId(
		long exploreId, int start, int end) {

		return getPersistence().findByExploreId(exploreId, start, end);
	}

	/**
	 * Returns an ordered range of all the galleries where exploreId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param exploreId the explore ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByExploreId(
		long exploreId, int start, int end,
		OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().findByExploreId(
			exploreId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the galleries where exploreId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param exploreId the explore ID
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching galleries
	 */
	public static List<Gallery> findByExploreId(
		long exploreId, int start, int end,
		OrderByComparator<Gallery> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByExploreId(
			exploreId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first gallery in the ordered set where exploreId = &#63;.
	 *
	 * @param exploreId the explore ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByExploreId_First(
			long exploreId, OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByExploreId_First(
			exploreId, orderByComparator);
	}

	/**
	 * Returns the first gallery in the ordered set where exploreId = &#63;.
	 *
	 * @param exploreId the explore ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByExploreId_First(
		long exploreId, OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByExploreId_First(
			exploreId, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where exploreId = &#63;.
	 *
	 * @param exploreId the explore ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery
	 * @throws NoSuchGalleryException if a matching gallery could not be found
	 */
	public static Gallery findByExploreId_Last(
			long exploreId, OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByExploreId_Last(
			exploreId, orderByComparator);
	}

	/**
	 * Returns the last gallery in the ordered set where exploreId = &#63;.
	 *
	 * @param exploreId the explore ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	public static Gallery fetchByExploreId_Last(
		long exploreId, OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().fetchByExploreId_Last(
			exploreId, orderByComparator);
	}

	/**
	 * Returns the galleries before and after the current gallery in the ordered set where exploreId = &#63;.
	 *
	 * @param slideId the primary key of the current gallery
	 * @param exploreId the explore ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public static Gallery[] findByExploreId_PrevAndNext(
			long slideId, long exploreId,
			OrderByComparator<Gallery> orderByComparator)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByExploreId_PrevAndNext(
			slideId, exploreId, orderByComparator);
	}

	/**
	 * Removes all the galleries where exploreId = &#63; from the database.
	 *
	 * @param exploreId the explore ID
	 */
	public static void removeByExploreId(long exploreId) {
		getPersistence().removeByExploreId(exploreId);
	}

	/**
	 * Returns the number of galleries where exploreId = &#63;.
	 *
	 * @param exploreId the explore ID
	 * @return the number of matching galleries
	 */
	public static int countByExploreId(long exploreId) {
		return getPersistence().countByExploreId(exploreId);
	}

	/**
	 * Caches the gallery in the entity cache if it is enabled.
	 *
	 * @param gallery the gallery
	 */
	public static void cacheResult(Gallery gallery) {
		getPersistence().cacheResult(gallery);
	}

	/**
	 * Caches the galleries in the entity cache if it is enabled.
	 *
	 * @param galleries the galleries
	 */
	public static void cacheResult(List<Gallery> galleries) {
		getPersistence().cacheResult(galleries);
	}

	/**
	 * Creates a new gallery with the primary key. Does not add the gallery to the database.
	 *
	 * @param slideId the primary key for the new gallery
	 * @return the new gallery
	 */
	public static Gallery create(long slideId) {
		return getPersistence().create(slideId);
	}

	/**
	 * Removes the gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery that was removed
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public static Gallery remove(long slideId)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().remove(slideId);
	}

	public static Gallery updateImpl(Gallery gallery) {
		return getPersistence().updateImpl(gallery);
	}

	/**
	 * Returns the gallery with the primary key or throws a <code>NoSuchGalleryException</code> if it could not be found.
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery
	 * @throws NoSuchGalleryException if a gallery with the primary key could not be found
	 */
	public static Gallery findByPrimaryKey(long slideId)
		throws explore.wadic.exception.NoSuchGalleryException {

		return getPersistence().findByPrimaryKey(slideId);
	}

	/**
	 * Returns the gallery with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery, or <code>null</code> if a gallery with the primary key could not be found
	 */
	public static Gallery fetchByPrimaryKey(long slideId) {
		return getPersistence().fetchByPrimaryKey(slideId);
	}

	/**
	 * Returns all the galleries.
	 *
	 * @return the galleries
	 */
	public static List<Gallery> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of galleries
	 */
	public static List<Gallery> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of galleries
	 */
	public static List<Gallery> findAll(
		int start, int end, OrderByComparator<Gallery> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of galleries
	 */
	public static List<Gallery> findAll(
		int start, int end, OrderByComparator<Gallery> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the galleries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of galleries.
	 *
	 * @return the number of galleries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static GalleryPersistence getPersistence() {
		return _persistence;
	}

	private static volatile GalleryPersistence _persistence;

}