/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Explore}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Explore
 * @generated
 */
public class ExploreWrapper
	extends BaseModelWrapper<Explore>
	implements Explore, ModelWrapper<Explore> {

	public ExploreWrapper(Explore explore) {
		super(explore);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("exploreId", getExploreId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("type", getType());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("timeLabel", getTimeLabel());
		attributes.put("tourTime", getTourTime());
		attributes.put("image", getImage());
		attributes.put("video", getVideo());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long exploreId = (Long)attributes.get("exploreId");

		if (exploreId != null) {
			setExploreId(exploreId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String timeLabel = (String)attributes.get("timeLabel");

		if (timeLabel != null) {
			setTimeLabel(timeLabel);
		}

		String tourTime = (String)attributes.get("tourTime");

		if (tourTime != null) {
			setTourTime(tourTime);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String video = (String)attributes.get("video");

		if (video != null) {
			setVideo(video);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the company ID of this explore.
	 *
	 * @return the company ID of this explore
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this explore.
	 *
	 * @return the create date of this explore
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this explore.
	 *
	 * @return the description of this explore
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this explore
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this explore. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this explore
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this explore
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this explore.
	 *
	 * @return the locales and localized descriptions of this explore
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the explore ID of this explore.
	 *
	 * @return the explore ID of this explore
	 */
	@Override
	public long getExploreId() {
		return model.getExploreId();
	}

	/**
	 * Returns the group ID of this explore.
	 *
	 * @return the group ID of this explore
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the image of this explore.
	 *
	 * @return the image of this explore
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the modified date of this explore.
	 *
	 * @return the modified date of this explore
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this explore.
	 *
	 * @return the name of this explore
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the localized name of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized name of this explore
	 */
	@Override
	public String getName(java.util.Locale locale) {
		return model.getName(locale);
	}

	/**
	 * Returns the localized name of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this explore. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getName(java.util.Locale locale, boolean useDefault) {
		return model.getName(locale, useDefault);
	}

	/**
	 * Returns the localized name of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized name of this explore
	 */
	@Override
	public String getName(String languageId) {
		return model.getName(languageId);
	}

	/**
	 * Returns the localized name of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this explore
	 */
	@Override
	public String getName(String languageId, boolean useDefault) {
		return model.getName(languageId, useDefault);
	}

	@Override
	public String getNameCurrentLanguageId() {
		return model.getNameCurrentLanguageId();
	}

	@Override
	public String getNameCurrentValue() {
		return model.getNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized names of this explore.
	 *
	 * @return the locales and localized names of this explore
	 */
	@Override
	public Map<java.util.Locale, String> getNameMap() {
		return model.getNameMap();
	}

	/**
	 * Returns the primary key of this explore.
	 *
	 * @return the primary key of this explore
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the time label of this explore.
	 *
	 * @return the time label of this explore
	 */
	@Override
	public String getTimeLabel() {
		return model.getTimeLabel();
	}

	/**
	 * Returns the localized time label of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized time label of this explore
	 */
	@Override
	public String getTimeLabel(java.util.Locale locale) {
		return model.getTimeLabel(locale);
	}

	/**
	 * Returns the localized time label of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized time label of this explore. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTimeLabel(java.util.Locale locale, boolean useDefault) {
		return model.getTimeLabel(locale, useDefault);
	}

	/**
	 * Returns the localized time label of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized time label of this explore
	 */
	@Override
	public String getTimeLabel(String languageId) {
		return model.getTimeLabel(languageId);
	}

	/**
	 * Returns the localized time label of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized time label of this explore
	 */
	@Override
	public String getTimeLabel(String languageId, boolean useDefault) {
		return model.getTimeLabel(languageId, useDefault);
	}

	@Override
	public String getTimeLabelCurrentLanguageId() {
		return model.getTimeLabelCurrentLanguageId();
	}

	@Override
	public String getTimeLabelCurrentValue() {
		return model.getTimeLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized time labels of this explore.
	 *
	 * @return the locales and localized time labels of this explore
	 */
	@Override
	public Map<java.util.Locale, String> getTimeLabelMap() {
		return model.getTimeLabelMap();
	}

	/**
	 * Returns the tour time of this explore.
	 *
	 * @return the tour time of this explore
	 */
	@Override
	public String getTourTime() {
		return model.getTourTime();
	}

	/**
	 * Returns the localized tour time of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized tour time of this explore
	 */
	@Override
	public String getTourTime(java.util.Locale locale) {
		return model.getTourTime(locale);
	}

	/**
	 * Returns the localized tour time of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized tour time of this explore. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTourTime(java.util.Locale locale, boolean useDefault) {
		return model.getTourTime(locale, useDefault);
	}

	/**
	 * Returns the localized tour time of this explore in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized tour time of this explore
	 */
	@Override
	public String getTourTime(String languageId) {
		return model.getTourTime(languageId);
	}

	/**
	 * Returns the localized tour time of this explore in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized tour time of this explore
	 */
	@Override
	public String getTourTime(String languageId, boolean useDefault) {
		return model.getTourTime(languageId, useDefault);
	}

	@Override
	public String getTourTimeCurrentLanguageId() {
		return model.getTourTimeCurrentLanguageId();
	}

	@Override
	public String getTourTimeCurrentValue() {
		return model.getTourTimeCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized tour times of this explore.
	 *
	 * @return the locales and localized tour times of this explore
	 */
	@Override
	public Map<java.util.Locale, String> getTourTimeMap() {
		return model.getTourTimeMap();
	}

	/**
	 * Returns the type of this explore.
	 *
	 * @return the type of this explore
	 */
	@Override
	public String getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this explore.
	 *
	 * @return the user ID of this explore
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this explore.
	 *
	 * @return the user name of this explore
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this explore.
	 *
	 * @return the user uuid of this explore
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this explore.
	 *
	 * @return the uuid of this explore
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the video of this explore.
	 *
	 * @return the video of this explore
	 */
	@Override
	public String getVideo() {
		return model.getVideo();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the company ID of this explore.
	 *
	 * @param companyId the company ID of this explore
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this explore.
	 *
	 * @param createDate the create date of this explore
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this explore.
	 *
	 * @param description the description of this explore
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this explore in the language.
	 *
	 * @param description the localized description of this explore
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this explore in the language, and sets the default locale.
	 *
	 * @param description the localized description of this explore
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this explore from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this explore
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this explore from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this explore
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the explore ID of this explore.
	 *
	 * @param exploreId the explore ID of this explore
	 */
	@Override
	public void setExploreId(long exploreId) {
		model.setExploreId(exploreId);
	}

	/**
	 * Sets the group ID of this explore.
	 *
	 * @param groupId the group ID of this explore
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the image of this explore.
	 *
	 * @param image the image of this explore
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the modified date of this explore.
	 *
	 * @param modifiedDate the modified date of this explore
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this explore.
	 *
	 * @param name the name of this explore
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the localized name of this explore in the language.
	 *
	 * @param name the localized name of this explore
	 * @param locale the locale of the language
	 */
	@Override
	public void setName(String name, java.util.Locale locale) {
		model.setName(name, locale);
	}

	/**
	 * Sets the localized name of this explore in the language, and sets the default locale.
	 *
	 * @param name the localized name of this explore
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setName(
		String name, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setName(name, locale, defaultLocale);
	}

	@Override
	public void setNameCurrentLanguageId(String languageId) {
		model.setNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized names of this explore from the map of locales and localized names.
	 *
	 * @param nameMap the locales and localized names of this explore
	 */
	@Override
	public void setNameMap(Map<java.util.Locale, String> nameMap) {
		model.setNameMap(nameMap);
	}

	/**
	 * Sets the localized names of this explore from the map of locales and localized names, and sets the default locale.
	 *
	 * @param nameMap the locales and localized names of this explore
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setNameMap(
		Map<java.util.Locale, String> nameMap, java.util.Locale defaultLocale) {

		model.setNameMap(nameMap, defaultLocale);
	}

	/**
	 * Sets the primary key of this explore.
	 *
	 * @param primaryKey the primary key of this explore
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the time label of this explore.
	 *
	 * @param timeLabel the time label of this explore
	 */
	@Override
	public void setTimeLabel(String timeLabel) {
		model.setTimeLabel(timeLabel);
	}

	/**
	 * Sets the localized time label of this explore in the language.
	 *
	 * @param timeLabel the localized time label of this explore
	 * @param locale the locale of the language
	 */
	@Override
	public void setTimeLabel(String timeLabel, java.util.Locale locale) {
		model.setTimeLabel(timeLabel, locale);
	}

	/**
	 * Sets the localized time label of this explore in the language, and sets the default locale.
	 *
	 * @param timeLabel the localized time label of this explore
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTimeLabel(
		String timeLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setTimeLabel(timeLabel, locale, defaultLocale);
	}

	@Override
	public void setTimeLabelCurrentLanguageId(String languageId) {
		model.setTimeLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized time labels of this explore from the map of locales and localized time labels.
	 *
	 * @param timeLabelMap the locales and localized time labels of this explore
	 */
	@Override
	public void setTimeLabelMap(Map<java.util.Locale, String> timeLabelMap) {
		model.setTimeLabelMap(timeLabelMap);
	}

	/**
	 * Sets the localized time labels of this explore from the map of locales and localized time labels, and sets the default locale.
	 *
	 * @param timeLabelMap the locales and localized time labels of this explore
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTimeLabelMap(
		Map<java.util.Locale, String> timeLabelMap,
		java.util.Locale defaultLocale) {

		model.setTimeLabelMap(timeLabelMap, defaultLocale);
	}

	/**
	 * Sets the tour time of this explore.
	 *
	 * @param tourTime the tour time of this explore
	 */
	@Override
	public void setTourTime(String tourTime) {
		model.setTourTime(tourTime);
	}

	/**
	 * Sets the localized tour time of this explore in the language.
	 *
	 * @param tourTime the localized tour time of this explore
	 * @param locale the locale of the language
	 */
	@Override
	public void setTourTime(String tourTime, java.util.Locale locale) {
		model.setTourTime(tourTime, locale);
	}

	/**
	 * Sets the localized tour time of this explore in the language, and sets the default locale.
	 *
	 * @param tourTime the localized tour time of this explore
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTourTime(
		String tourTime, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setTourTime(tourTime, locale, defaultLocale);
	}

	@Override
	public void setTourTimeCurrentLanguageId(String languageId) {
		model.setTourTimeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized tour times of this explore from the map of locales and localized tour times.
	 *
	 * @param tourTimeMap the locales and localized tour times of this explore
	 */
	@Override
	public void setTourTimeMap(Map<java.util.Locale, String> tourTimeMap) {
		model.setTourTimeMap(tourTimeMap);
	}

	/**
	 * Sets the localized tour times of this explore from the map of locales and localized tour times, and sets the default locale.
	 *
	 * @param tourTimeMap the locales and localized tour times of this explore
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTourTimeMap(
		Map<java.util.Locale, String> tourTimeMap,
		java.util.Locale defaultLocale) {

		model.setTourTimeMap(tourTimeMap, defaultLocale);
	}

	/**
	 * Sets the type of this explore.
	 *
	 * @param type the type of this explore
	 */
	@Override
	public void setType(String type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this explore.
	 *
	 * @param userId the user ID of this explore
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this explore.
	 *
	 * @param userName the user name of this explore
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this explore.
	 *
	 * @param userUuid the user uuid of this explore
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this explore.
	 *
	 * @param uuid the uuid of this explore
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the video of this explore.
	 *
	 * @param video the video of this explore
	 */
	@Override
	public void setVideo(String video) {
		model.setVideo(video);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected ExploreWrapper wrap(Explore explore) {
		return new ExploreWrapper(explore);
	}

}