/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import explore.wadic.exception.NoSuchExploreException;
import explore.wadic.model.Explore;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the explore service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ExploreUtil
 * @generated
 */
@ProviderType
public interface ExplorePersistence extends BasePersistence<Explore> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ExploreUtil} to access the explore persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the explores where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching explores
	 */
	public java.util.List<Explore> findByUuid(String uuid);

	/**
	 * Returns a range of all the explores where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of matching explores
	 */
	public java.util.List<Explore> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the explores where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching explores
	 */
	public java.util.List<Explore> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns an ordered range of all the explores where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching explores
	 */
	public java.util.List<Explore> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public Explore findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Returns the first explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns the last explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public Explore findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Returns the last explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns the explores before and after the current explore in the ordered set where uuid = &#63;.
	 *
	 * @param exploreId the primary key of the current explore
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public Explore[] findByUuid_PrevAndNext(
			long exploreId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Removes all the explores where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of explores where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching explores
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the explore where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchExploreException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public Explore findByUUID_G(String uuid, long groupId)
		throws NoSuchExploreException;

	/**
	 * Returns the explore where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the explore where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the explore where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the explore that was removed
	 */
	public Explore removeByUUID_G(String uuid, long groupId)
		throws NoSuchExploreException;

	/**
	 * Returns the number of explores where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching explores
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching explores
	 */
	public java.util.List<Explore> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of matching explores
	 */
	public java.util.List<Explore> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching explores
	 */
	public java.util.List<Explore> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns an ordered range of all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching explores
	 */
	public java.util.List<Explore> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public Explore findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Returns the first explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns the last explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public Explore findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Returns the last explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns the explores before and after the current explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param exploreId the primary key of the current explore
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public Explore[] findByUuid_C_PrevAndNext(
			long exploreId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Removes all the explores where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of explores where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching explores
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the explores where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching explores
	 */
	public java.util.List<Explore> findByGroupId(long groupId);

	/**
	 * Returns a range of all the explores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of matching explores
	 */
	public java.util.List<Explore> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the explores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching explores
	 */
	public java.util.List<Explore> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns an ordered range of all the explores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching explores
	 */
	public java.util.List<Explore> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public Explore findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Returns the first explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns the last explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public Explore findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Returns the last explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public Explore fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns the explores before and after the current explore in the ordered set where groupId = &#63;.
	 *
	 * @param exploreId the primary key of the current explore
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public Explore[] findByGroupId_PrevAndNext(
			long exploreId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Explore>
				orderByComparator)
		throws NoSuchExploreException;

	/**
	 * Removes all the explores where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of explores where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching explores
	 */
	public int countByGroupId(long groupId);

	/**
	 * Caches the explore in the entity cache if it is enabled.
	 *
	 * @param explore the explore
	 */
	public void cacheResult(Explore explore);

	/**
	 * Caches the explores in the entity cache if it is enabled.
	 *
	 * @param explores the explores
	 */
	public void cacheResult(java.util.List<Explore> explores);

	/**
	 * Creates a new explore with the primary key. Does not add the explore to the database.
	 *
	 * @param exploreId the primary key for the new explore
	 * @return the new explore
	 */
	public Explore create(long exploreId);

	/**
	 * Removes the explore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore that was removed
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public Explore remove(long exploreId) throws NoSuchExploreException;

	public Explore updateImpl(Explore explore);

	/**
	 * Returns the explore with the primary key or throws a <code>NoSuchExploreException</code> if it could not be found.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public Explore findByPrimaryKey(long exploreId)
		throws NoSuchExploreException;

	/**
	 * Returns the explore with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore, or <code>null</code> if a explore with the primary key could not be found
	 */
	public Explore fetchByPrimaryKey(long exploreId);

	/**
	 * Returns all the explores.
	 *
	 * @return the explores
	 */
	public java.util.List<Explore> findAll();

	/**
	 * Returns a range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of explores
	 */
	public java.util.List<Explore> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of explores
	 */
	public java.util.List<Explore> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator);

	/**
	 * Returns an ordered range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of explores
	 */
	public java.util.List<Explore> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Explore>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the explores from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of explores.
	 *
	 * @return the number of explores
	 */
	public int countAll();

}