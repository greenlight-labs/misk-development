/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ExploreLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ExploreLocalService
 * @generated
 */
public class ExploreLocalServiceWrapper
	implements ExploreLocalService, ServiceWrapper<ExploreLocalService> {

	public ExploreLocalServiceWrapper(ExploreLocalService exploreLocalService) {
		_exploreLocalService = exploreLocalService;
	}

	@Override
	public explore.wadic.model.Explore addEntry(
			explore.wadic.model.Explore orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   explore.wadic.exception.ExploreValidateException {

		return _exploreLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public void addEntryGallery(
			explore.wadic.model.Explore orgEntry,
			javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		_exploreLocalService.addEntryGallery(orgEntry, request);
	}

	/**
	 * Adds the explore to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was added
	 */
	@Override
	public explore.wadic.model.Explore addExplore(
		explore.wadic.model.Explore explore) {

		return _exploreLocalService.addExplore(explore);
	}

	@Override
	public int countByGroupId(long groupId) {
		return _exploreLocalService.countByGroupId(groupId);
	}

	/**
	 * Creates a new explore with the primary key. Does not add the explore to the database.
	 *
	 * @param exploreId the primary key for the new explore
	 * @return the new explore
	 */
	@Override
	public explore.wadic.model.Explore createExplore(long exploreId) {
		return _exploreLocalService.createExplore(exploreId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _exploreLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public explore.wadic.model.Explore deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _exploreLocalService.deleteEntry(primaryKey);
	}

	/**
	 * Deletes the explore from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was removed
	 */
	@Override
	public explore.wadic.model.Explore deleteExplore(
		explore.wadic.model.Explore explore) {

		return _exploreLocalService.deleteExplore(explore);
	}

	/**
	 * Deletes the explore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore that was removed
	 * @throws PortalException if a explore with the primary key could not be found
	 */
	@Override
	public explore.wadic.model.Explore deleteExplore(long exploreId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _exploreLocalService.deleteExplore(exploreId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _exploreLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _exploreLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _exploreLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _exploreLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _exploreLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _exploreLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _exploreLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public explore.wadic.model.Explore fetchExplore(long exploreId) {
		return _exploreLocalService.fetchExplore(exploreId);
	}

	/**
	 * Returns the explore matching the UUID and group.
	 *
	 * @param uuid the explore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching explore, or <code>null</code> if a matching explore could not be found
	 */
	@Override
	public explore.wadic.model.Explore fetchExploreByUuidAndGroupId(
		String uuid, long groupId) {

		return _exploreLocalService.fetchExploreByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<explore.wadic.model.Explore> findByGroupId(
		long groupId) {

		return _exploreLocalService.findByGroupId(groupId);
	}

	@Override
	public java.util.List<explore.wadic.model.Explore> findByGroupId(
		long groupId, int start, int end) {

		return _exploreLocalService.findByGroupId(groupId, start, end);
	}

	@Override
	public java.util.List<explore.wadic.model.Explore> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<explore.wadic.model.Explore> obc) {

		return _exploreLocalService.findByGroupId(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _exploreLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the explore with the primary key.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore
	 * @throws PortalException if a explore with the primary key could not be found
	 */
	@Override
	public explore.wadic.model.Explore getExplore(long exploreId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _exploreLocalService.getExplore(exploreId);
	}

	/**
	 * Returns the explore matching the UUID and group.
	 *
	 * @param uuid the explore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching explore
	 * @throws PortalException if a matching explore could not be found
	 */
	@Override
	public explore.wadic.model.Explore getExploreByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _exploreLocalService.getExploreByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public explore.wadic.model.Explore getExploreFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws explore.wadic.exception.ExploreValidateException,
			   javax.portlet.PortletException {

		return _exploreLocalService.getExploreFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of explores
	 */
	@Override
	public java.util.List<explore.wadic.model.Explore> getExplores(
		int start, int end) {

		return _exploreLocalService.getExplores(start, end);
	}

	/**
	 * Returns all the explores matching the UUID and company.
	 *
	 * @param uuid the UUID of the explores
	 * @param companyId the primary key of the company
	 * @return the matching explores, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<explore.wadic.model.Explore>
		getExploresByUuidAndCompanyId(String uuid, long companyId) {

		return _exploreLocalService.getExploresByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of explores matching the UUID and company.
	 *
	 * @param uuid the UUID of the explores
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching explores, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<explore.wadic.model.Explore>
		getExploresByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<explore.wadic.model.Explore> orderByComparator) {

		return _exploreLocalService.getExploresByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of explores.
	 *
	 * @return the number of explores
	 */
	@Override
	public int getExploresCount() {
		return _exploreLocalService.getExploresCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _exploreLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public java.util.List<explore.wadic.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return _exploreLocalService.getGalleries(actionRequest);
	}

	@Override
	public java.util.List<explore.wadic.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		java.util.List<explore.wadic.model.Gallery> defaultGalleries) {

		return _exploreLocalService.getGalleries(
			actionRequest, defaultGalleries);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _exploreLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public explore.wadic.model.Explore getNewObject(long primaryKey) {
		return _exploreLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _exploreLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _exploreLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public explore.wadic.model.Explore updateEntry(
			explore.wadic.model.Explore orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   explore.wadic.exception.ExploreValidateException {

		return _exploreLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the explore in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was updated
	 */
	@Override
	public explore.wadic.model.Explore updateExplore(
		explore.wadic.model.Explore explore) {

		return _exploreLocalService.updateExplore(explore);
	}

	@Override
	public void updateGalleries(
			explore.wadic.model.Explore entry,
			java.util.List<explore.wadic.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_exploreLocalService.updateGalleries(entry, galleries, serviceContext);
	}

	@Override
	public ExploreLocalService getWrappedService() {
		return _exploreLocalService;
	}

	@Override
	public void setWrappedService(ExploreLocalService exploreLocalService) {
		_exploreLocalService = exploreLocalService;
	}

	private ExploreLocalService _exploreLocalService;

}