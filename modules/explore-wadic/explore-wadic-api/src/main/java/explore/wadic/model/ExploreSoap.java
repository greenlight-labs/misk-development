/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link explore.wadic.service.http.ExploreServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class ExploreSoap implements Serializable {

	public static ExploreSoap toSoapModel(Explore model) {
		ExploreSoap soapModel = new ExploreSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setExploreId(model.getExploreId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setType(model.getType());
		soapModel.setName(model.getName());
		soapModel.setDescription(model.getDescription());
		soapModel.setTimeLabel(model.getTimeLabel());
		soapModel.setTourTime(model.getTourTime());
		soapModel.setImage(model.getImage());
		soapModel.setVideo(model.getVideo());

		return soapModel;
	}

	public static ExploreSoap[] toSoapModels(Explore[] models) {
		ExploreSoap[] soapModels = new ExploreSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ExploreSoap[][] toSoapModels(Explore[][] models) {
		ExploreSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ExploreSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ExploreSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ExploreSoap[] toSoapModels(List<Explore> models) {
		List<ExploreSoap> soapModels = new ArrayList<ExploreSoap>(
			models.size());

		for (Explore model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ExploreSoap[soapModels.size()]);
	}

	public ExploreSoap() {
	}

	public long getPrimaryKey() {
		return _exploreId;
	}

	public void setPrimaryKey(long pk) {
		setExploreId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getExploreId() {
		return _exploreId;
	}

	public void setExploreId(long exploreId) {
		_exploreId = exploreId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getTimeLabel() {
		return _timeLabel;
	}

	public void setTimeLabel(String timeLabel) {
		_timeLabel = timeLabel;
	}

	public String getTourTime() {
		return _tourTime;
	}

	public void setTourTime(String tourTime) {
		_tourTime = tourTime;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getVideo() {
		return _video;
	}

	public void setVideo(String video) {
		_video = video;
	}

	private String _uuid;
	private long _exploreId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _type;
	private String _name;
	private String _description;
	private String _timeLabel;
	private String _tourTime;
	private String _image;
	private String _video;

}