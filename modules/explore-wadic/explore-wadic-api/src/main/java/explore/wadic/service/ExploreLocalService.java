/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import explore.wadic.exception.ExploreValidateException;
import explore.wadic.model.Explore;
import explore.wadic.model.Gallery;

import java.io.Serializable;

import java.util.*;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Explore. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ExploreLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface ExploreLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>explore.wadic.service.impl.ExploreLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the explore local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link ExploreLocalServiceUtil} if injection and service tracking are not available.
	 */
	public Explore addEntry(Explore orgEntry, ServiceContext serviceContext)
		throws ExploreValidateException, PortalException;

	public void addEntryGallery(Explore orgEntry, PortletRequest request)
		throws PortletException;

	/**
	 * Adds the explore to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Explore addExplore(Explore explore);

	public int countByGroupId(long groupId);

	/**
	 * Creates a new explore with the primary key. Does not add the explore to the database.
	 *
	 * @param exploreId the primary key for the new explore
	 * @return the new explore
	 */
	@Transactional(enabled = false)
	public Explore createExplore(long exploreId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public Explore deleteEntry(long primaryKey) throws PortalException;

	/**
	 * Deletes the explore from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Explore deleteExplore(Explore explore);

	/**
	 * Deletes the explore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore that was removed
	 * @throws PortalException if a explore with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Explore deleteExplore(long exploreId) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Explore fetchExplore(long exploreId);

	/**
	 * Returns the explore matching the UUID and group.
	 *
	 * @param uuid the explore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching explore, or <code>null</code> if a matching explore could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Explore fetchExploreByUuidAndGroupId(String uuid, long groupId);

	public List<Explore> findByGroupId(long groupId);

	public List<Explore> findByGroupId(long groupId, int start, int end);

	public List<Explore> findByGroupId(
		long groupId, int start, int end, OrderByComparator<Explore> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the explore with the primary key.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore
	 * @throws PortalException if a explore with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Explore getExplore(long exploreId) throws PortalException;

	/**
	 * Returns the explore matching the UUID and group.
	 *
	 * @param uuid the explore's UUID
	 * @param groupId the primary key of the group
	 * @return the matching explore
	 * @throws PortalException if a matching explore could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Explore getExploreByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Explore getExploreFromRequest(
			long primaryKey, PortletRequest request)
		throws ExploreValidateException, PortletException;

	/**
	 * Returns a range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.wadic.model.impl.ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of explores
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Explore> getExplores(int start, int end);

	/**
	 * Returns all the explores matching the UUID and company.
	 *
	 * @param uuid the UUID of the explores
	 * @param companyId the primary key of the company
	 * @return the matching explores, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Explore> getExploresByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of explores matching the UUID and company.
	 *
	 * @param uuid the UUID of the explores
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching explores, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Explore> getExploresByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Explore> orderByComparator);

	/**
	 * Returns the number of explores.
	 *
	 * @return the number of explores
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getExploresCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallery> getGalleries(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallery> getGalleries(
		ActionRequest actionRequest, List<Gallery> defaultGalleries);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Explore getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public Explore updateEntry(Explore orgEntry, ServiceContext serviceContext)
		throws ExploreValidateException, PortalException;

	/**
	 * Updates the explore in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ExploreLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param explore the explore
	 * @return the explore that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Explore updateExplore(Explore explore);

	public void updateGalleries(
			Explore entry, List<Gallery> galleries,
			ServiceContext serviceContext)
		throws PortalException;

}