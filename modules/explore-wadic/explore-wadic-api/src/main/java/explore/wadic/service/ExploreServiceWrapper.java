/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ExploreService}.
 *
 * @author Brian Wing Shun Chan
 * @see ExploreService
 * @generated
 */
public class ExploreServiceWrapper
	implements ExploreService, ServiceWrapper<ExploreService> {

	public ExploreServiceWrapper(ExploreService exploreService) {
		_exploreService = exploreService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _exploreService.getOSGiServiceIdentifier();
	}

	@Override
	public ExploreService getWrappedService() {
		return _exploreService;
	}

	@Override
	public void setWrappedService(ExploreService exploreService) {
		_exploreService = exploreService;
	}

	private ExploreService _exploreService;

}