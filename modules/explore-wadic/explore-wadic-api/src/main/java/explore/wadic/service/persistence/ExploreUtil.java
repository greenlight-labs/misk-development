/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import explore.wadic.model.Explore;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the explore service. This utility wraps <code>explore.wadic.service.persistence.impl.ExplorePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ExplorePersistence
 * @generated
 */
public class ExploreUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Explore explore) {
		getPersistence().clearCache(explore);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Explore> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Explore> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Explore> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Explore> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Explore> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Explore update(Explore explore) {
		return getPersistence().update(explore);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Explore update(
		Explore explore, ServiceContext serviceContext) {

		return getPersistence().update(explore, serviceContext);
	}

	/**
	 * Returns all the explores where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching explores
	 */
	public static List<Explore> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the explores where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of matching explores
	 */
	public static List<Explore> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the explores where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching explores
	 */
	public static List<Explore> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Explore> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the explores where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching explores
	 */
	public static List<Explore> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Explore> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public static Explore findByUuid_First(
			String uuid, OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByUuid_First(
		String uuid, OrderByComparator<Explore> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public static Explore findByUuid_Last(
			String uuid, OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last explore in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByUuid_Last(
		String uuid, OrderByComparator<Explore> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the explores before and after the current explore in the ordered set where uuid = &#63;.
	 *
	 * @param exploreId the primary key of the current explore
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public static Explore[] findByUuid_PrevAndNext(
			long exploreId, String uuid,
			OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByUuid_PrevAndNext(
			exploreId, uuid, orderByComparator);
	}

	/**
	 * Removes all the explores where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of explores where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching explores
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the explore where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchExploreException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public static Explore findByUUID_G(String uuid, long groupId)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the explore where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the explore where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the explore where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the explore that was removed
	 */
	public static Explore removeByUUID_G(String uuid, long groupId)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of explores where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching explores
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching explores
	 */
	public static List<Explore> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of matching explores
	 */
	public static List<Explore> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching explores
	 */
	public static List<Explore> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Explore> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the explores where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching explores
	 */
	public static List<Explore> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Explore> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public static Explore findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Explore> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public static Explore findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Explore> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the explores before and after the current explore in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param exploreId the primary key of the current explore
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public static Explore[] findByUuid_C_PrevAndNext(
			long exploreId, String uuid, long companyId,
			OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByUuid_C_PrevAndNext(
			exploreId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the explores where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of explores where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching explores
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the explores where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching explores
	 */
	public static List<Explore> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the explores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of matching explores
	 */
	public static List<Explore> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the explores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching explores
	 */
	public static List<Explore> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Explore> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the explores where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching explores
	 */
	public static List<Explore> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Explore> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public static Explore findByGroupId_First(
			long groupId, OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByGroupId_First(
		long groupId, OrderByComparator<Explore> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore
	 * @throws NoSuchExploreException if a matching explore could not be found
	 */
	public static Explore findByGroupId_Last(
			long groupId, OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last explore in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching explore, or <code>null</code> if a matching explore could not be found
	 */
	public static Explore fetchByGroupId_Last(
		long groupId, OrderByComparator<Explore> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the explores before and after the current explore in the ordered set where groupId = &#63;.
	 *
	 * @param exploreId the primary key of the current explore
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public static Explore[] findByGroupId_PrevAndNext(
			long exploreId, long groupId,
			OrderByComparator<Explore> orderByComparator)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByGroupId_PrevAndNext(
			exploreId, groupId, orderByComparator);
	}

	/**
	 * Removes all the explores where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of explores where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching explores
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Caches the explore in the entity cache if it is enabled.
	 *
	 * @param explore the explore
	 */
	public static void cacheResult(Explore explore) {
		getPersistence().cacheResult(explore);
	}

	/**
	 * Caches the explores in the entity cache if it is enabled.
	 *
	 * @param explores the explores
	 */
	public static void cacheResult(List<Explore> explores) {
		getPersistence().cacheResult(explores);
	}

	/**
	 * Creates a new explore with the primary key. Does not add the explore to the database.
	 *
	 * @param exploreId the primary key for the new explore
	 * @return the new explore
	 */
	public static Explore create(long exploreId) {
		return getPersistence().create(exploreId);
	}

	/**
	 * Removes the explore with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore that was removed
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public static Explore remove(long exploreId)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().remove(exploreId);
	}

	public static Explore updateImpl(Explore explore) {
		return getPersistence().updateImpl(explore);
	}

	/**
	 * Returns the explore with the primary key or throws a <code>NoSuchExploreException</code> if it could not be found.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore
	 * @throws NoSuchExploreException if a explore with the primary key could not be found
	 */
	public static Explore findByPrimaryKey(long exploreId)
		throws explore.wadic.exception.NoSuchExploreException {

		return getPersistence().findByPrimaryKey(exploreId);
	}

	/**
	 * Returns the explore with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param exploreId the primary key of the explore
	 * @return the explore, or <code>null</code> if a explore with the primary key could not be found
	 */
	public static Explore fetchByPrimaryKey(long exploreId) {
		return getPersistence().fetchByPrimaryKey(exploreId);
	}

	/**
	 * Returns all the explores.
	 *
	 * @return the explores
	 */
	public static List<Explore> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @return the range of explores
	 */
	public static List<Explore> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of explores
	 */
	public static List<Explore> findAll(
		int start, int end, OrderByComparator<Explore> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the explores.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ExploreModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of explores
	 * @param end the upper bound of the range of explores (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of explores
	 */
	public static List<Explore> findAll(
		int start, int end, OrderByComparator<Explore> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the explores from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of explores.
	 *
	 * @return the number of explores
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ExplorePersistence getPersistence() {
		return _persistence;
	}

	private static volatile ExplorePersistence _persistence;

}