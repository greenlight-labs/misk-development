/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import explore.wadic.exception.ExploreValidateException;
import explore.wadic.model.Explore;
import explore.wadic.model.Gallery;
import explore.wadic.service.GalleryLocalServiceUtil;
import explore.wadic.service.base.ExploreLocalServiceBaseImpl;

import explore.wadic.service.util.ExploreValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * The implementation of the explore local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>explore.wadic.service.ExploreLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ExploreLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=explore.wadic.model.Explore",
	service = AopService.class
)
public class ExploreLocalServiceImpl extends ExploreLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>explore.wadic.service.ExploreLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>explore.wadic.service.ExploreLocalServiceUtil</code>.
	 */

	public Explore addEntry(Explore orgEntry, ServiceContext serviceContext)
			throws PortalException, ExploreValidateException {

		// Validation

		ModelValidator<Explore> modelValidator = new ExploreValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Explore entry = _addEntry(orgEntry, serviceContext);

		Explore addedEntry = explorePersistence.update(entry);

		explorePersistence.clearCache();

		return addedEntry;
	}

	public Explore updateEntry(Explore orgEntry, ServiceContext serviceContext)
			throws PortalException, ExploreValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Explore> modelValidator = new ExploreValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Explore entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Explore updatedEntry = explorePersistence.update(entry);
		explorePersistence.clearCache();

		return updatedEntry;
	}

	protected Explore _addEntry(Explore entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Explore.class.getName());

		Explore newEntry = explorePersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setType(entry.getType());
		newEntry.setName(entry.getName());
		newEntry.setDescription(entry.getDescription());
		newEntry.setTimeLabel(entry.getTimeLabel());
		newEntry.setTourTime(entry.getTourTime());
		newEntry.setImage(entry.getImage());
		newEntry.setVideo(entry.getVideo());

		return newEntry;
	}

	protected Explore _updateEntry(long primaryKey, Explore entry, ServiceContext serviceContext)
			throws PortalException {

		Explore updateEntry = fetchExplore(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setType(entry.getType());
		updateEntry.setName(entry.getName());
		updateEntry.setDescription(entry.getDescription());
		updateEntry.setTimeLabel(entry.getTimeLabel());
		updateEntry.setTourTime(entry.getTourTime());
		updateEntry.setImage(entry.getImage());
		updateEntry.setVideo(entry.getVideo());

		return updateEntry;
	}

	public Explore deleteEntry(long primaryKey) throws PortalException {
		Explore entry = getExplore(primaryKey);
		explorePersistence.remove(entry);

		return entry;
	}

	public List<Explore> findByGroupId(long groupId) {

		return explorePersistence.findByGroupId(groupId);
	}

	public List<Explore> findByGroupId(long groupId, int start, int end, OrderByComparator<Explore> obc) {

		return explorePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Explore> findByGroupId(long groupId, int start, int end) {

		return explorePersistence.findByGroupId(groupId, start, end);
	}

	public int countByGroupId(long groupId) {

		return explorePersistence.countByGroupId(groupId);
	}

	public Explore getExploreFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, ExploreValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Explore entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchExplore(primaryKey);
		}

		try {
			entry.setExploreId(primaryKey);

			entry.setType(ParamUtil.getString(request, "type"));
			entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
			entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));
			entry.setTimeLabelMap(LocalizationUtil.getLocalizationMap(request, "timeLabel"));
			entry.setTourTimeMap(LocalizationUtil.getLocalizationMap(request, "tourTime"));
			entry.setImage(ParamUtil.getString(request, "image"));
			entry.setVideo(ParamUtil.getString(request, "video"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new ExploreValidateException(error);
		}

		return entry;
	}

	public Explore getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Explore.class.getName());

		return createExplore(primaryKey);
	}
	
	/*
	 * **********************************- START: Explore Gallery Code
	 * -*****************************
	 */
	public void addEntryGallery(Explore orgEntry, PortletRequest request) throws PortletException {

		String galleryIndexesString = ParamUtil.getString(request, "galleryIndexes");

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);
	}

	@Override
	public void updateGalleries(Explore entry, List<Gallery> galleries, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> galleryIds = new HashSet<>();

		for (Gallery gallery : galleries) {
			long galleryId = gallery.getSlideId();

			// additional step to set Ids - initially zero
			gallery.setExploreId(entry.getExploreId());
			gallery.setUserId(entry.getUserId());
			gallery.setCompanyId(entry.getCompanyId());
			gallery.setGroupId(entry.getGroupId());

			if (galleryId <= 0) {
				gallery = GalleryLocalServiceUtil.addEntry(gallery, serviceContext);

				galleryId = gallery.getSlideId();
			} else {
				GalleryLocalServiceUtil.updateEntry(gallery, serviceContext);
			}

			galleryIds.add(galleryId);
		}

		galleries = GalleryLocalServiceUtil.findByExploreId(entry.getExploreId());

		for (Gallery gallery : galleries) {
			if (!galleryIds.contains(gallery.getSlideId())) {
				GalleryLocalServiceUtil.deleteGallery(gallery.getSlideId());
			}
		}
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest) {
		return getGalleries(actionRequest, Collections.<Gallery>emptyList());
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest, List<Gallery> defaultGalleries) {

		String galleryIndexesString = ParamUtil.getString(actionRequest, "galleryIndexes");

		if (galleryIndexesString == null) {
			return defaultGalleries;
		}

		List<Gallery> galleries = new ArrayList<>();

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);

		for (int galleryIndex : galleryIndexes) {
			String image = ParamUtil.getString(actionRequest, "galleryImage" + galleryIndex);

			if (Validator.isNull(image)) {
				continue;
			}

			String thumbnail = ParamUtil.getString(actionRequest, "galleryThumbnail" + galleryIndex);

			long galleryId = ParamUtil.getLong(actionRequest, "galleryId" + galleryIndex);

			Gallery gallery = GalleryLocalServiceUtil.createGallery(galleryId);

			gallery.setExploreId(0);
			gallery.setImage(image);
			gallery.setThumbnail(thumbnail);

			galleries.add(gallery);
		}

		return galleries;
	}

	/*
	 * **********************************- END: Explore Gallery Code
	 * -*****************************
	 */

	private static Log _log = LogFactoryUtil.getLog(ExploreLocalServiceImpl.class);
}