/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.wadic.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import explore.wadic.model.Explore;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Explore in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ExploreCacheModel implements CacheModel<Explore>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExploreCacheModel)) {
			return false;
		}

		ExploreCacheModel exploreCacheModel = (ExploreCacheModel)object;

		if (exploreId == exploreCacheModel.exploreId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, exploreId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", exploreId=");
		sb.append(exploreId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", type=");
		sb.append(type);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", timeLabel=");
		sb.append(timeLabel);
		sb.append(", tourTime=");
		sb.append(tourTime);
		sb.append(", image=");
		sb.append(image);
		sb.append(", video=");
		sb.append(video);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Explore toEntityModel() {
		ExploreImpl exploreImpl = new ExploreImpl();

		if (uuid == null) {
			exploreImpl.setUuid("");
		}
		else {
			exploreImpl.setUuid(uuid);
		}

		exploreImpl.setExploreId(exploreId);
		exploreImpl.setGroupId(groupId);
		exploreImpl.setCompanyId(companyId);
		exploreImpl.setUserId(userId);

		if (userName == null) {
			exploreImpl.setUserName("");
		}
		else {
			exploreImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			exploreImpl.setCreateDate(null);
		}
		else {
			exploreImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			exploreImpl.setModifiedDate(null);
		}
		else {
			exploreImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (type == null) {
			exploreImpl.setType("");
		}
		else {
			exploreImpl.setType(type);
		}

		if (name == null) {
			exploreImpl.setName("");
		}
		else {
			exploreImpl.setName(name);
		}

		if (description == null) {
			exploreImpl.setDescription("");
		}
		else {
			exploreImpl.setDescription(description);
		}

		if (timeLabel == null) {
			exploreImpl.setTimeLabel("");
		}
		else {
			exploreImpl.setTimeLabel(timeLabel);
		}

		if (tourTime == null) {
			exploreImpl.setTourTime("");
		}
		else {
			exploreImpl.setTourTime(tourTime);
		}

		if (image == null) {
			exploreImpl.setImage("");
		}
		else {
			exploreImpl.setImage(image);
		}

		if (video == null) {
			exploreImpl.setVideo("");
		}
		else {
			exploreImpl.setVideo(video);
		}

		exploreImpl.resetOriginalValues();

		return exploreImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		exploreId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		type = objectInput.readUTF();
		name = objectInput.readUTF();
		description = objectInput.readUTF();
		timeLabel = objectInput.readUTF();
		tourTime = objectInput.readUTF();
		image = objectInput.readUTF();
		video = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(exploreId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (timeLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(timeLabel);
		}

		if (tourTime == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(tourTime);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (video == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(video);
		}
	}

	public String uuid;
	public long exploreId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String type;
	public String name;
	public String description;
	public String timeLabel;
	public String tourTime;
	public String image;
	public String video;

}