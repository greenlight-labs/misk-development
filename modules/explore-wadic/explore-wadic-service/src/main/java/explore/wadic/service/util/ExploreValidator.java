package explore.wadic.service.util;

import explore.wadic.model.Explore;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class ExploreValidator implements ModelValidator<Explore> {

	@Override
	public void validate(Explore entry) throws PortalException {
/*   */
        // Validate fields
        validateType(entry.getType());
        validateName(entry.getName());
        validateDescription(entry.getDescription());
        validateTimeLabel(entry.getTimeLabel());
        validateTourTime(entry.getTourTime());
        validateImage(entry.getImage());
        validateVideo(entry.getVideo());
	}

    protected void validateType(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("explore-type-required");
        }
    }

    protected void validateName(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("explore-name-required");
        }
    }

    protected void validateDescription(String description) {}

    protected void validateTimeLabel(String field) {}
    protected void validateTourTime(String field) {}
    protected void validateImage(String field) {}
    protected void validateVideo(String field) {}

	protected List<String> _errors = new ArrayList<>();

}
