package explore.wadic.service.util;

import explore.wadic.model.Gallery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class GalleryValidator implements ModelValidator<Gallery> {

	@Override
	public void validate(Gallery entry) throws PortalException {

        // validate fields
        validateExploreId(entry.getExploreId());
        validateImage(entry.getImage());
        validateThumbnail(entry.getThumbnail());
	}

    protected void validateExploreId(long field) {
        if(Validator.isNull(field)){
            _errors.add("gallery-explore-id-required");
        }
    }

    /**
    * image field Validation
    *
    * @param field image
    */
    protected void validateImage(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("gallery-image-required");
        }

    }

    /**
    * thumbnail field Validation
    *
    * @param field thumbnail
    */
    protected void validateThumbnail(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("gallery-thumbnail-required");
        }
    }

/*  */

	protected List<String> _errors = new ArrayList<>();

}
