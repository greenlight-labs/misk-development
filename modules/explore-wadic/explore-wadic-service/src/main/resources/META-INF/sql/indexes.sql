create index IX_2F21BEC7 on misk_explore_wadic (groupId);
create index IX_CA634877 on misk_explore_wadic (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_8E4180B9 on misk_explore_wadic (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_4B0AD80A on misk_explore_wadic_galleries (exploreId);
create index IX_CC844876 on misk_explore_wadic_galleries (groupId);
create index IX_66FF33A8 on misk_explore_wadic_galleries (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_290842A on misk_explore_wadic_galleries (uuid_[$COLUMN_LENGTH:75$], groupId);