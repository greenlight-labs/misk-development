create table misk_explore_wadic (
	uuid_ VARCHAR(75) null,
	exploreId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	type_ VARCHAR(75) null,
	name STRING null,
	description STRING null,
	timeLabel STRING null,
	tourTime STRING null,
	image VARCHAR(75) null,
	video VARCHAR(75) null
);

create table misk_explore_wadic_galleries (
	uuid_ VARCHAR(75) null,
	slideId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	exploreId LONG,
	image VARCHAR(75) null,
	thumbnail VARCHAR(75) null
);