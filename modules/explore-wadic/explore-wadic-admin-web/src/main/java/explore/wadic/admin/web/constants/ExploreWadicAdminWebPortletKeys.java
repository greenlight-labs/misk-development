package explore.wadic.admin.web.constants;

/**
 * @author tz
 */
public class ExploreWadicAdminWebPortletKeys {

	public static final String EXPLOREWADICADMINWEB =
		"explore_wadic_admin_web_ExploreWadicAdminWebPortlet";

}