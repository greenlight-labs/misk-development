package explore.wadic.admin.web.application.list;

import explore.wadic.admin.web.constants.ExploreWadicAdminWebPanelCategoryKeys;
import explore.wadic.admin.web.constants.ExploreWadicAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + ExploreWadicAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class ExploreWadicAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return ExploreWadicAdminWebPortletKeys.EXPLOREWADICADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + ExploreWadicAdminWebPortletKeys.EXPLOREWADICADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}