/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import explore.model.Amenities;
import explore.model.Attractions;
import explore.model.BookSpace;
import explore.model.Gallary;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Attractions. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see AttractionsLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface AttractionsLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>explore.service.impl.AttractionsLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the attractions local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link AttractionsLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the attractions to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Attractions addAttractions(Attractions attractions);

	public Attractions addAttractions(
			long userId, Map<Locale, String> locationLabel,
			Map<Locale, String> locationLatitude,
			Map<Locale, String> locationLongitude,
			Map<Locale, String> amenitiesLabel,
			Map<Locale, String> bookSpaceLabel,
			Map<Locale, String> gallaryLabel, String eventId,
			String headerArticleId, ServiceContext serviceContext)
		throws PortalException;

	public void addEntryAmenities(Attractions orgEntry, PortletRequest request)
		throws PortletException;

	public void addEntryBookSpace(Attractions orgEntry, PortletRequest request)
		throws PortletException;

	public void addEntryGallery(Attractions orgEntry, PortletRequest request)
		throws PortletException;

	/**
	 * Creates a new attractions with the primary key. Does not add the attractions to the database.
	 *
	 * @param attractionsId the primary key for the new attractions
	 * @return the new attractions
	 */
	@Transactional(enabled = false)
	public Attractions createAttractions(long attractionsId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the attractions from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Attractions deleteAttractions(Attractions attractions);

	/**
	 * Deletes the attractions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions that was removed
	 * @throws PortalException if a attractions with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Attractions deleteAttractions(long attractionsId)
		throws PortalException;

	public Attractions deleteAttractions(
			long attractionsId, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Attractions fetchAttractions(long attractionsId);

	/**
	 * Returns the attractions matching the UUID and group.
	 *
	 * @param uuid the attractions's UUID
	 * @param groupId the primary key of the group
	 * @return the matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Attractions fetchAttractionsByUuidAndGroupId(
		String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Amenities> getAmenities(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Amenities> getAmenities(
		ActionRequest actionRequest, List<Amenities> defaultAmenities);

	/**
	 * Returns the attractions with the primary key.
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions
	 * @throws PortalException if a attractions with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Attractions getAttractions(long attractionsId)
		throws PortalException;

	/**
	 * Returns the attractions matching the UUID and group.
	 *
	 * @param uuid the attractions's UUID
	 * @param groupId the primary key of the group
	 * @return the matching attractions
	 * @throws PortalException if a matching attractions could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Attractions getAttractionsByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getAttractionsCount(long groupId);

	/**
	 * Returns a range of all the attractionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @return the range of attractionses
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Attractions> getAttractionses(int start, int end);

	/**
	 * Returns all the attractionses matching the UUID and company.
	 *
	 * @param uuid the UUID of the attractionses
	 * @param companyId the primary key of the company
	 * @return the matching attractionses, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Attractions> getAttractionsesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of attractionses matching the UUID and company.
	 *
	 * @param uuid the UUID of the attractionses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching attractionses, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Attractions> getAttractionsesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Attractions> orderByComparator);

	/**
	 * Returns the number of attractionses.
	 *
	 * @return the number of attractionses
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getAttractionsesCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Attractions> getAttractionsList(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Attractions> getAttractionsList(
		long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Attractions> getAttractionsList(
		long groupId, int start, int end, OrderByComparator<Attractions> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<BookSpace> getBookSpace(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<BookSpace> getBookSpace(
		ActionRequest actionRequest, List<BookSpace> defaultSchedules);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallary> getGalleries(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallary> getGalleries(
		ActionRequest actionRequest, List<Gallary> defaultGalleries);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public void updateAmenities(
			Attractions entry, List<Amenities> AmenitiesList,
			ServiceContext serviceContext)
		throws PortalException;

	/**
	 * Updates the attractions in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Attractions updateAttractions(Attractions attractions);

	public Attractions updateAttractions(
			long userId, long attractionsId, Map<Locale, String> locationLabel,
			Map<Locale, String> locationLatitude,
			Map<Locale, String> locationLongitude,
			Map<Locale, String> amenitiesLabel,
			Map<Locale, String> bookSpaceLabel,
			Map<Locale, String> gallaryLabel, String eventId,
			String headerArticleId, ServiceContext serviceContext)
		throws PortalException;

	public void updateAttractionssGalleries(
			Attractions entry, List<Gallary> galleries,
			ServiceContext serviceContext)
		throws PortalException;

	public void updateBookSpace(
			Attractions entry, List<BookSpace> bookspaceList,
			ServiceContext serviceContext)
		throws PortalException;

}