/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookSpaceLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookSpaceLocalService
 * @generated
 */
public class BookSpaceLocalServiceWrapper
	implements BookSpaceLocalService, ServiceWrapper<BookSpaceLocalService> {

	public BookSpaceLocalServiceWrapper(
		BookSpaceLocalService bookSpaceLocalService) {

		_bookSpaceLocalService = bookSpaceLocalService;
	}

	/**
	 * Adds the book space to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookSpace the book space
	 * @return the book space that was added
	 */
	@Override
	public explore.model.BookSpace addBookSpace(
		explore.model.BookSpace bookSpace) {

		return _bookSpaceLocalService.addBookSpace(bookSpace);
	}

	@Override
	public explore.model.BookSpace addEntry(
			explore.model.BookSpace orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Creates a new book space with the primary key. Does not add the book space to the database.
	 *
	 * @param bookspaceId the primary key for the new book space
	 * @return the new book space
	 */
	@Override
	public explore.model.BookSpace createBookSpace(long bookspaceId) {
		return _bookSpaceLocalService.createBookSpace(bookspaceId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the book space from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookSpace the book space
	 * @return the book space that was removed
	 */
	@Override
	public explore.model.BookSpace deleteBookSpace(
		explore.model.BookSpace bookSpace) {

		return _bookSpaceLocalService.deleteBookSpace(bookSpace);
	}

	/**
	 * Deletes the book space with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space that was removed
	 * @throws PortalException if a book space with the primary key could not be found
	 */
	@Override
	public explore.model.BookSpace deleteBookSpace(long bookspaceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.deleteBookSpace(bookspaceId);
	}

	@Override
	public explore.model.BookSpace deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _bookSpaceLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _bookSpaceLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.model.impl.BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _bookSpaceLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.model.impl.BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _bookSpaceLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _bookSpaceLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _bookSpaceLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public explore.model.BookSpace fetchBookSpace(long bookspaceId) {
		return _bookSpaceLocalService.fetchBookSpace(bookspaceId);
	}

	/**
	 * Returns the book space with the matching UUID and company.
	 *
	 * @param uuid the book space's UUID
	 * @param companyId the primary key of the company
	 * @return the matching book space, or <code>null</code> if a matching book space could not be found
	 */
	@Override
	public explore.model.BookSpace fetchBookSpaceByUuidAndCompanyId(
		String uuid, long companyId) {

		return _bookSpaceLocalService.fetchBookSpaceByUuidAndCompanyId(
			uuid, companyId);
	}

	@Override
	public java.util.List<explore.model.BookSpace> findAllInAttractions(
		long attractionId) {

		return _bookSpaceLocalService.findAllInAttractions(attractionId);
	}

	@Override
	public java.util.List<explore.model.BookSpace> findAllInAttractions(
		long attractionId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<explore.model.BookSpace> obc) {

		return _bookSpaceLocalService.findAllInAttractions(
			attractionId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _bookSpaceLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the book space with the primary key.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space
	 * @throws PortalException if a book space with the primary key could not be found
	 */
	@Override
	public explore.model.BookSpace getBookSpace(long bookspaceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.getBookSpace(bookspaceId);
	}

	/**
	 * Returns the book space with the matching UUID and company.
	 *
	 * @param uuid the book space's UUID
	 * @param companyId the primary key of the company
	 * @return the matching book space
	 * @throws PortalException if a matching book space could not be found
	 */
	@Override
	public explore.model.BookSpace getBookSpaceByUuidAndCompanyId(
			String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.getBookSpaceByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.model.impl.BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of book spaces
	 */
	@Override
	public java.util.List<explore.model.BookSpace> getBookSpaces(
		int start, int end) {

		return _bookSpaceLocalService.getBookSpaces(start, end);
	}

	/**
	 * Returns the number of book spaces.
	 *
	 * @return the number of book spaces
	 */
	@Override
	public int getBookSpacesCount() {
		return _bookSpaceLocalService.getBookSpacesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _bookSpaceLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _bookSpaceLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _bookSpaceLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the book space in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookSpace the book space
	 * @return the book space that was updated
	 */
	@Override
	public explore.model.BookSpace updateBookSpace(
		explore.model.BookSpace bookSpace) {

		return _bookSpaceLocalService.updateBookSpace(bookSpace);
	}

	@Override
	public explore.model.BookSpace updateEntry(
			explore.model.BookSpace orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookSpaceLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public BookSpaceLocalService getWrappedService() {
		return _bookSpaceLocalService;
	}

	@Override
	public void setWrappedService(BookSpaceLocalService bookSpaceLocalService) {
		_bookSpaceLocalService = bookSpaceLocalService;
	}

	private BookSpaceLocalService _bookSpaceLocalService;

}