/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import explore.exception.NoSuchAmenitiesException;

import explore.model.Amenities;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the amenities service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AmenitiesUtil
 * @generated
 */
@ProviderType
public interface AmenitiesPersistence extends BasePersistence<Amenities> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AmenitiesUtil} to access the amenities persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the amenitieses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid(String uuid);

	/**
	 * Returns a range of all the amenitieses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @return the range of matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the amenitieses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns an ordered range of all the amenitieses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first amenities in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenities
	 * @throws NoSuchAmenitiesException if a matching amenities could not be found
	 */
	public Amenities findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Returns the first amenities in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenities, or <code>null</code> if a matching amenities could not be found
	 */
	public Amenities fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns the last amenities in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenities
	 * @throws NoSuchAmenitiesException if a matching amenities could not be found
	 */
	public Amenities findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Returns the last amenities in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenities, or <code>null</code> if a matching amenities could not be found
	 */
	public Amenities fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns the amenitieses before and after the current amenities in the ordered set where uuid = &#63;.
	 *
	 * @param amenitiesId the primary key of the current amenities
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next amenities
	 * @throws NoSuchAmenitiesException if a amenities with the primary key could not be found
	 */
	public Amenities[] findByUuid_PrevAndNext(
			long amenitiesId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Removes all the amenitieses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of amenitieses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching amenitieses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the amenitieses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the amenitieses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @return the range of matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the amenitieses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns an ordered range of all the amenitieses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching amenitieses
	 */
	public java.util.List<Amenities> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first amenities in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenities
	 * @throws NoSuchAmenitiesException if a matching amenities could not be found
	 */
	public Amenities findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Returns the first amenities in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenities, or <code>null</code> if a matching amenities could not be found
	 */
	public Amenities fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns the last amenities in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenities
	 * @throws NoSuchAmenitiesException if a matching amenities could not be found
	 */
	public Amenities findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Returns the last amenities in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenities, or <code>null</code> if a matching amenities could not be found
	 */
	public Amenities fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns the amenitieses before and after the current amenities in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param amenitiesId the primary key of the current amenities
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next amenities
	 * @throws NoSuchAmenitiesException if a amenities with the primary key could not be found
	 */
	public Amenities[] findByUuid_C_PrevAndNext(
			long amenitiesId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Removes all the amenitieses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of amenitieses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching amenitieses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the amenitieses where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the matching amenitieses
	 */
	public java.util.List<Amenities> findByAttractionsId(long attractionsId);

	/**
	 * Returns a range of all the amenitieses where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @return the range of matching amenitieses
	 */
	public java.util.List<Amenities> findByAttractionsId(
		long attractionsId, int start, int end);

	/**
	 * Returns an ordered range of all the amenitieses where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching amenitieses
	 */
	public java.util.List<Amenities> findByAttractionsId(
		long attractionsId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns an ordered range of all the amenitieses where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching amenitieses
	 */
	public java.util.List<Amenities> findByAttractionsId(
		long attractionsId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first amenities in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenities
	 * @throws NoSuchAmenitiesException if a matching amenities could not be found
	 */
	public Amenities findByAttractionsId_First(
			long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Returns the first amenities in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenities, or <code>null</code> if a matching amenities could not be found
	 */
	public Amenities fetchByAttractionsId_First(
		long attractionsId,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns the last amenities in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenities
	 * @throws NoSuchAmenitiesException if a matching amenities could not be found
	 */
	public Amenities findByAttractionsId_Last(
			long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Returns the last amenities in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenities, or <code>null</code> if a matching amenities could not be found
	 */
	public Amenities fetchByAttractionsId_Last(
		long attractionsId,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns the amenitieses before and after the current amenities in the ordered set where attractionsId = &#63;.
	 *
	 * @param amenitiesId the primary key of the current amenities
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next amenities
	 * @throws NoSuchAmenitiesException if a amenities with the primary key could not be found
	 */
	public Amenities[] findByAttractionsId_PrevAndNext(
			long amenitiesId, long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<Amenities>
				orderByComparator)
		throws NoSuchAmenitiesException;

	/**
	 * Removes all the amenitieses where attractionsId = &#63; from the database.
	 *
	 * @param attractionsId the attractions ID
	 */
	public void removeByAttractionsId(long attractionsId);

	/**
	 * Returns the number of amenitieses where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the number of matching amenitieses
	 */
	public int countByAttractionsId(long attractionsId);

	/**
	 * Caches the amenities in the entity cache if it is enabled.
	 *
	 * @param amenities the amenities
	 */
	public void cacheResult(Amenities amenities);

	/**
	 * Caches the amenitieses in the entity cache if it is enabled.
	 *
	 * @param amenitieses the amenitieses
	 */
	public void cacheResult(java.util.List<Amenities> amenitieses);

	/**
	 * Creates a new amenities with the primary key. Does not add the amenities to the database.
	 *
	 * @param amenitiesId the primary key for the new amenities
	 * @return the new amenities
	 */
	public Amenities create(long amenitiesId);

	/**
	 * Removes the amenities with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param amenitiesId the primary key of the amenities
	 * @return the amenities that was removed
	 * @throws NoSuchAmenitiesException if a amenities with the primary key could not be found
	 */
	public Amenities remove(long amenitiesId) throws NoSuchAmenitiesException;

	public Amenities updateImpl(Amenities amenities);

	/**
	 * Returns the amenities with the primary key or throws a <code>NoSuchAmenitiesException</code> if it could not be found.
	 *
	 * @param amenitiesId the primary key of the amenities
	 * @return the amenities
	 * @throws NoSuchAmenitiesException if a amenities with the primary key could not be found
	 */
	public Amenities findByPrimaryKey(long amenitiesId)
		throws NoSuchAmenitiesException;

	/**
	 * Returns the amenities with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param amenitiesId the primary key of the amenities
	 * @return the amenities, or <code>null</code> if a amenities with the primary key could not be found
	 */
	public Amenities fetchByPrimaryKey(long amenitiesId);

	/**
	 * Returns all the amenitieses.
	 *
	 * @return the amenitieses
	 */
	public java.util.List<Amenities> findAll();

	/**
	 * Returns a range of all the amenitieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @return the range of amenitieses
	 */
	public java.util.List<Amenities> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the amenitieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of amenitieses
	 */
	public java.util.List<Amenities> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator);

	/**
	 * Returns an ordered range of all the amenitieses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenitiesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenitieses
	 * @param end the upper bound of the range of amenitieses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of amenitieses
	 */
	public java.util.List<Amenities> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Amenities>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the amenitieses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of amenitieses.
	 *
	 * @return the number of amenitieses
	 */
	public int countAll();

}