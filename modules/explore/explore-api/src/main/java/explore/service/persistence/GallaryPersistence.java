/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import explore.exception.NoSuchGallaryException;

import explore.model.Gallary;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the gallary service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GallaryUtil
 * @generated
 */
@ProviderType
public interface GallaryPersistence extends BasePersistence<Gallary> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link GallaryUtil} to access the gallary persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the gallaries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching gallaries
	 */
	public java.util.List<Gallary> findByUuid(String uuid);

	/**
	 * Returns a range of all the gallaries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of matching gallaries
	 */
	public java.util.List<Gallary> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching gallaries
	 */
	public java.util.List<Gallary> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching gallaries
	 */
	public java.util.List<Gallary> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	public Gallary findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	public Gallary fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	public Gallary findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	public Gallary fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns the gallaries before and after the current gallary in the ordered set where uuid = &#63;.
	 *
	 * @param gallaryId the primary key of the current gallary
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	public Gallary[] findByUuid_PrevAndNext(
			long gallaryId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Removes all the gallaries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of gallaries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching gallaries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching gallaries
	 */
	public java.util.List<Gallary> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of matching gallaries
	 */
	public java.util.List<Gallary> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching gallaries
	 */
	public java.util.List<Gallary> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching gallaries
	 */
	public java.util.List<Gallary> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	public Gallary findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	public Gallary fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	public Gallary findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	public Gallary fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns the gallaries before and after the current gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param gallaryId the primary key of the current gallary
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	public Gallary[] findByUuid_C_PrevAndNext(
			long gallaryId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Removes all the gallaries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching gallaries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the gallaries where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the matching gallaries
	 */
	public java.util.List<Gallary> findByAttractionsId(long attractionsId);

	/**
	 * Returns a range of all the gallaries where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of matching gallaries
	 */
	public java.util.List<Gallary> findByAttractionsId(
		long attractionsId, int start, int end);

	/**
	 * Returns an ordered range of all the gallaries where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching gallaries
	 */
	public java.util.List<Gallary> findByAttractionsId(
		long attractionsId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns an ordered range of all the gallaries where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching gallaries
	 */
	public java.util.List<Gallary> findByAttractionsId(
		long attractionsId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	public Gallary findByAttractionsId_First(
			long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Returns the first gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	public Gallary fetchByAttractionsId_First(
		long attractionsId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns the last gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	public Gallary findByAttractionsId_Last(
			long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Returns the last gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	public Gallary fetchByAttractionsId_Last(
		long attractionsId,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns the gallaries before and after the current gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param gallaryId the primary key of the current gallary
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	public Gallary[] findByAttractionsId_PrevAndNext(
			long gallaryId, long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<Gallary>
				orderByComparator)
		throws NoSuchGallaryException;

	/**
	 * Removes all the gallaries where attractionsId = &#63; from the database.
	 *
	 * @param attractionsId the attractions ID
	 */
	public void removeByAttractionsId(long attractionsId);

	/**
	 * Returns the number of gallaries where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the number of matching gallaries
	 */
	public int countByAttractionsId(long attractionsId);

	/**
	 * Caches the gallary in the entity cache if it is enabled.
	 *
	 * @param gallary the gallary
	 */
	public void cacheResult(Gallary gallary);

	/**
	 * Caches the gallaries in the entity cache if it is enabled.
	 *
	 * @param gallaries the gallaries
	 */
	public void cacheResult(java.util.List<Gallary> gallaries);

	/**
	 * Creates a new gallary with the primary key. Does not add the gallary to the database.
	 *
	 * @param gallaryId the primary key for the new gallary
	 * @return the new gallary
	 */
	public Gallary create(long gallaryId);

	/**
	 * Removes the gallary with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary that was removed
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	public Gallary remove(long gallaryId) throws NoSuchGallaryException;

	public Gallary updateImpl(Gallary gallary);

	/**
	 * Returns the gallary with the primary key or throws a <code>NoSuchGallaryException</code> if it could not be found.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	public Gallary findByPrimaryKey(long gallaryId)
		throws NoSuchGallaryException;

	/**
	 * Returns the gallary with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary, or <code>null</code> if a gallary with the primary key could not be found
	 */
	public Gallary fetchByPrimaryKey(long gallaryId);

	/**
	 * Returns all the gallaries.
	 *
	 * @return the gallaries
	 */
	public java.util.List<Gallary> findAll();

	/**
	 * Returns a range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of gallaries
	 */
	public java.util.List<Gallary> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of gallaries
	 */
	public java.util.List<Gallary> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator);

	/**
	 * Returns an ordered range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of gallaries
	 */
	public java.util.List<Gallary> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Gallary>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the gallaries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of gallaries.
	 *
	 * @return the number of gallaries
	 */
	public int countAll();

}