/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import explore.exception.NoSuchAttractionsException;

import explore.model.Attractions;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the attractions service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AttractionsUtil
 * @generated
 */
@ProviderType
public interface AttractionsPersistence extends BasePersistence<Attractions> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AttractionsUtil} to access the attractions persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the attractionses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching attractionses
	 */
	public java.util.List<Attractions> findByUuid(String uuid);

	/**
	 * Returns a range of all the attractionses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @return the range of matching attractionses
	 */
	public java.util.List<Attractions> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the attractionses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attractionses
	 */
	public java.util.List<Attractions> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns an ordered range of all the attractionses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching attractionses
	 */
	public java.util.List<Attractions> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first attractions in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attractions
	 * @throws NoSuchAttractionsException if a matching attractions could not be found
	 */
	public Attractions findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Returns the first attractions in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns the last attractions in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attractions
	 * @throws NoSuchAttractionsException if a matching attractions could not be found
	 */
	public Attractions findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Returns the last attractions in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns the attractionses before and after the current attractions in the ordered set where uuid = &#63;.
	 *
	 * @param attractionsId the primary key of the current attractions
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attractions
	 * @throws NoSuchAttractionsException if a attractions with the primary key could not be found
	 */
	public Attractions[] findByUuid_PrevAndNext(
			long attractionsId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Removes all the attractionses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of attractionses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching attractionses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the attractions where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAttractionsException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching attractions
	 * @throws NoSuchAttractionsException if a matching attractions could not be found
	 */
	public Attractions findByUUID_G(String uuid, long groupId)
		throws NoSuchAttractionsException;

	/**
	 * Returns the attractions where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the attractions where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the attractions where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the attractions that was removed
	 */
	public Attractions removeByUUID_G(String uuid, long groupId)
		throws NoSuchAttractionsException;

	/**
	 * Returns the number of attractionses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching attractionses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the attractionses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching attractionses
	 */
	public java.util.List<Attractions> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the attractionses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @return the range of matching attractionses
	 */
	public java.util.List<Attractions> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the attractionses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attractionses
	 */
	public java.util.List<Attractions> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns an ordered range of all the attractionses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching attractionses
	 */
	public java.util.List<Attractions> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first attractions in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attractions
	 * @throws NoSuchAttractionsException if a matching attractions could not be found
	 */
	public Attractions findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Returns the first attractions in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns the last attractions in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attractions
	 * @throws NoSuchAttractionsException if a matching attractions could not be found
	 */
	public Attractions findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Returns the last attractions in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns the attractionses before and after the current attractions in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param attractionsId the primary key of the current attractions
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attractions
	 * @throws NoSuchAttractionsException if a attractions with the primary key could not be found
	 */
	public Attractions[] findByUuid_C_PrevAndNext(
			long attractionsId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Removes all the attractionses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of attractionses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching attractionses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the attractionses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching attractionses
	 */
	public java.util.List<Attractions> findByGroupId(long groupId);

	/**
	 * Returns a range of all the attractionses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @return the range of matching attractionses
	 */
	public java.util.List<Attractions> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the attractionses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching attractionses
	 */
	public java.util.List<Attractions> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns an ordered range of all the attractionses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching attractionses
	 */
	public java.util.List<Attractions> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first attractions in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attractions
	 * @throws NoSuchAttractionsException if a matching attractions could not be found
	 */
	public Attractions findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Returns the first attractions in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns the last attractions in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attractions
	 * @throws NoSuchAttractionsException if a matching attractions could not be found
	 */
	public Attractions findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Returns the last attractions in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public Attractions fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns the attractionses before and after the current attractions in the ordered set where groupId = &#63;.
	 *
	 * @param attractionsId the primary key of the current attractions
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next attractions
	 * @throws NoSuchAttractionsException if a attractions with the primary key could not be found
	 */
	public Attractions[] findByGroupId_PrevAndNext(
			long attractionsId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Attractions>
				orderByComparator)
		throws NoSuchAttractionsException;

	/**
	 * Removes all the attractionses where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of attractionses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching attractionses
	 */
	public int countByGroupId(long groupId);

	/**
	 * Caches the attractions in the entity cache if it is enabled.
	 *
	 * @param attractions the attractions
	 */
	public void cacheResult(Attractions attractions);

	/**
	 * Caches the attractionses in the entity cache if it is enabled.
	 *
	 * @param attractionses the attractionses
	 */
	public void cacheResult(java.util.List<Attractions> attractionses);

	/**
	 * Creates a new attractions with the primary key. Does not add the attractions to the database.
	 *
	 * @param attractionsId the primary key for the new attractions
	 * @return the new attractions
	 */
	public Attractions create(long attractionsId);

	/**
	 * Removes the attractions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions that was removed
	 * @throws NoSuchAttractionsException if a attractions with the primary key could not be found
	 */
	public Attractions remove(long attractionsId)
		throws NoSuchAttractionsException;

	public Attractions updateImpl(Attractions attractions);

	/**
	 * Returns the attractions with the primary key or throws a <code>NoSuchAttractionsException</code> if it could not be found.
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions
	 * @throws NoSuchAttractionsException if a attractions with the primary key could not be found
	 */
	public Attractions findByPrimaryKey(long attractionsId)
		throws NoSuchAttractionsException;

	/**
	 * Returns the attractions with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions, or <code>null</code> if a attractions with the primary key could not be found
	 */
	public Attractions fetchByPrimaryKey(long attractionsId);

	/**
	 * Returns all the attractionses.
	 *
	 * @return the attractionses
	 */
	public java.util.List<Attractions> findAll();

	/**
	 * Returns a range of all the attractionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @return the range of attractionses
	 */
	public java.util.List<Attractions> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the attractionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of attractionses
	 */
	public java.util.List<Attractions> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator);

	/**
	 * Returns an ordered range of all the attractionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of attractionses
	 */
	public java.util.List<Attractions> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Attractions>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the attractionses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of attractionses.
	 *
	 * @return the number of attractionses
	 */
	public int countAll();

}