/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import explore.exception.NoSuchBookSpaceException;

import explore.model.BookSpace;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the book space service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookSpaceUtil
 * @generated
 */
@ProviderType
public interface BookSpacePersistence extends BasePersistence<BookSpace> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BookSpaceUtil} to access the book space persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the book spaces where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid(String uuid);

	/**
	 * Returns a range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public BookSpace findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Returns the first book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public BookSpace fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns the last book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public BookSpace findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Returns the last book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public BookSpace fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where uuid = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public BookSpace[] findByUuid_PrevAndNext(
			long bookspaceId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Removes all the book spaces where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of book spaces where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching book spaces
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	public java.util.List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public BookSpace findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Returns the first book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public BookSpace fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns the last book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public BookSpace findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Returns the last book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public BookSpace fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public BookSpace[] findByUuid_C_PrevAndNext(
			long bookspaceId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Removes all the book spaces where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching book spaces
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the book spaces where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the matching book spaces
	 */
	public java.util.List<BookSpace> findByAttractionsId(long attractionsId);

	/**
	 * Returns a range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	public java.util.List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end);

	/**
	 * Returns an ordered range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	public java.util.List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns an ordered range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	public java.util.List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public BookSpace findByAttractionsId_First(
			long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Returns the first book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public BookSpace fetchByAttractionsId_First(
		long attractionsId,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns the last book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public BookSpace findByAttractionsId_Last(
			long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Returns the last book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public BookSpace fetchByAttractionsId_Last(
		long attractionsId,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public BookSpace[] findByAttractionsId_PrevAndNext(
			long bookspaceId, long attractionsId,
			com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
				orderByComparator)
		throws NoSuchBookSpaceException;

	/**
	 * Removes all the book spaces where attractionsId = &#63; from the database.
	 *
	 * @param attractionsId the attractions ID
	 */
	public void removeByAttractionsId(long attractionsId);

	/**
	 * Returns the number of book spaces where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the number of matching book spaces
	 */
	public int countByAttractionsId(long attractionsId);

	/**
	 * Caches the book space in the entity cache if it is enabled.
	 *
	 * @param bookSpace the book space
	 */
	public void cacheResult(BookSpace bookSpace);

	/**
	 * Caches the book spaces in the entity cache if it is enabled.
	 *
	 * @param bookSpaces the book spaces
	 */
	public void cacheResult(java.util.List<BookSpace> bookSpaces);

	/**
	 * Creates a new book space with the primary key. Does not add the book space to the database.
	 *
	 * @param bookspaceId the primary key for the new book space
	 * @return the new book space
	 */
	public BookSpace create(long bookspaceId);

	/**
	 * Removes the book space with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space that was removed
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public BookSpace remove(long bookspaceId) throws NoSuchBookSpaceException;

	public BookSpace updateImpl(BookSpace bookSpace);

	/**
	 * Returns the book space with the primary key or throws a <code>NoSuchBookSpaceException</code> if it could not be found.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public BookSpace findByPrimaryKey(long bookspaceId)
		throws NoSuchBookSpaceException;

	/**
	 * Returns the book space with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space, or <code>null</code> if a book space with the primary key could not be found
	 */
	public BookSpace fetchByPrimaryKey(long bookspaceId);

	/**
	 * Returns all the book spaces.
	 *
	 * @return the book spaces
	 */
	public java.util.List<BookSpace> findAll();

	/**
	 * Returns a range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of book spaces
	 */
	public java.util.List<BookSpace> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of book spaces
	 */
	public java.util.List<BookSpace> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator);

	/**
	 * Returns an ordered range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of book spaces
	 */
	public java.util.List<BookSpace> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<BookSpace>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the book spaces from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of book spaces.
	 *
	 * @return the number of book spaces
	 */
	public int countAll();

}