/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Attractions}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Attractions
 * @generated
 */
public class AttractionsWrapper
	extends BaseModelWrapper<Attractions>
	implements Attractions, ModelWrapper<Attractions> {

	public AttractionsWrapper(Attractions attractions) {
		super(attractions);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("attractionsId", getAttractionsId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("headerArticleId", getHeaderArticleId());
		attributes.put("locationLabel", getLocationLabel());
		attributes.put("locationLatitude", getLocationLatitude());
		attributes.put("locationLongitude", getLocationLongitude());
		attributes.put("amenitiesLabel", getAmenitiesLabel());
		attributes.put("bookSpaceLabel", getBookSpaceLabel());
		attributes.put("gallaryLabel", getGallaryLabel());
		attributes.put("eventId", getEventId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long attractionsId = (Long)attributes.get("attractionsId");

		if (attractionsId != null) {
			setAttractionsId(attractionsId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String headerArticleId = (String)attributes.get("headerArticleId");

		if (headerArticleId != null) {
			setHeaderArticleId(headerArticleId);
		}

		String locationLabel = (String)attributes.get("locationLabel");

		if (locationLabel != null) {
			setLocationLabel(locationLabel);
		}

		String locationLatitude = (String)attributes.get("locationLatitude");

		if (locationLatitude != null) {
			setLocationLatitude(locationLatitude);
		}

		String locationLongitude = (String)attributes.get("locationLongitude");

		if (locationLongitude != null) {
			setLocationLongitude(locationLongitude);
		}

		String amenitiesLabel = (String)attributes.get("amenitiesLabel");

		if (amenitiesLabel != null) {
			setAmenitiesLabel(amenitiesLabel);
		}

		String bookSpaceLabel = (String)attributes.get("bookSpaceLabel");

		if (bookSpaceLabel != null) {
			setBookSpaceLabel(bookSpaceLabel);
		}

		String gallaryLabel = (String)attributes.get("gallaryLabel");

		if (gallaryLabel != null) {
			setGallaryLabel(gallaryLabel);
		}

		String eventId = (String)attributes.get("eventId");

		if (eventId != null) {
			setEventId(eventId);
		}
	}

	/**
	 * Returns the amenities label of this attractions.
	 *
	 * @return the amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel() {
		return model.getAmenitiesLabel();
	}

	/**
	 * Returns the localized amenities label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel(java.util.Locale locale) {
		return model.getAmenitiesLabel(locale);
	}

	/**
	 * Returns the localized amenities label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized amenities label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getAmenitiesLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getAmenitiesLabel(locale, useDefault);
	}

	/**
	 * Returns the localized amenities label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel(String languageId) {
		return model.getAmenitiesLabel(languageId);
	}

	/**
	 * Returns the localized amenities label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel(String languageId, boolean useDefault) {
		return model.getAmenitiesLabel(languageId, useDefault);
	}

	@Override
	public String getAmenitiesLabelCurrentLanguageId() {
		return model.getAmenitiesLabelCurrentLanguageId();
	}

	@Override
	public String getAmenitiesLabelCurrentValue() {
		return model.getAmenitiesLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized amenities labels of this attractions.
	 *
	 * @return the locales and localized amenities labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getAmenitiesLabelMap() {
		return model.getAmenitiesLabelMap();
	}

	/**
	 * Returns the attractions ID of this attractions.
	 *
	 * @return the attractions ID of this attractions
	 */
	@Override
	public long getAttractionsId() {
		return model.getAttractionsId();
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the book space label of this attractions.
	 *
	 * @return the book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel() {
		return model.getBookSpaceLabel();
	}

	/**
	 * Returns the localized book space label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel(java.util.Locale locale) {
		return model.getBookSpaceLabel(locale);
	}

	/**
	 * Returns the localized book space label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBookSpaceLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getBookSpaceLabel(locale, useDefault);
	}

	/**
	 * Returns the localized book space label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel(String languageId) {
		return model.getBookSpaceLabel(languageId);
	}

	/**
	 * Returns the localized book space label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel(String languageId, boolean useDefault) {
		return model.getBookSpaceLabel(languageId, useDefault);
	}

	@Override
	public String getBookSpaceLabelCurrentLanguageId() {
		return model.getBookSpaceLabelCurrentLanguageId();
	}

	@Override
	public String getBookSpaceLabelCurrentValue() {
		return model.getBookSpaceLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized book space labels of this attractions.
	 *
	 * @return the locales and localized book space labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getBookSpaceLabelMap() {
		return model.getBookSpaceLabelMap();
	}

	/**
	 * Returns the company ID of this attractions.
	 *
	 * @return the company ID of this attractions
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this attractions.
	 *
	 * @return the create date of this attractions
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the event ID of this attractions.
	 *
	 * @return the event ID of this attractions
	 */
	@Override
	public String getEventId() {
		return model.getEventId();
	}

	/**
	 * Returns the gallary label of this attractions.
	 *
	 * @return the gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel() {
		return model.getGallaryLabel();
	}

	/**
	 * Returns the localized gallary label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel(java.util.Locale locale) {
		return model.getGallaryLabel(locale);
	}

	/**
	 * Returns the localized gallary label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized gallary label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getGallaryLabel(java.util.Locale locale, boolean useDefault) {
		return model.getGallaryLabel(locale, useDefault);
	}

	/**
	 * Returns the localized gallary label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel(String languageId) {
		return model.getGallaryLabel(languageId);
	}

	/**
	 * Returns the localized gallary label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel(String languageId, boolean useDefault) {
		return model.getGallaryLabel(languageId, useDefault);
	}

	@Override
	public String getGallaryLabelCurrentLanguageId() {
		return model.getGallaryLabelCurrentLanguageId();
	}

	@Override
	public String getGallaryLabelCurrentValue() {
		return model.getGallaryLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized gallary labels of this attractions.
	 *
	 * @return the locales and localized gallary labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getGallaryLabelMap() {
		return model.getGallaryLabelMap();
	}

	/**
	 * Returns the group ID of this attractions.
	 *
	 * @return the group ID of this attractions
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the header article ID of this attractions.
	 *
	 * @return the header article ID of this attractions
	 */
	@Override
	public String getHeaderArticleId() {
		return model.getHeaderArticleId();
	}

	/**
	 * Returns the location label of this attractions.
	 *
	 * @return the location label of this attractions
	 */
	@Override
	public String getLocationLabel() {
		return model.getLocationLabel();
	}

	/**
	 * Returns the localized location label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized location label of this attractions
	 */
	@Override
	public String getLocationLabel(java.util.Locale locale) {
		return model.getLocationLabel(locale);
	}

	/**
	 * Returns the localized location label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLocationLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getLocationLabel(locale, useDefault);
	}

	/**
	 * Returns the localized location label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized location label of this attractions
	 */
	@Override
	public String getLocationLabel(String languageId) {
		return model.getLocationLabel(languageId);
	}

	/**
	 * Returns the localized location label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location label of this attractions
	 */
	@Override
	public String getLocationLabel(String languageId, boolean useDefault) {
		return model.getLocationLabel(languageId, useDefault);
	}

	@Override
	public String getLocationLabelCurrentLanguageId() {
		return model.getLocationLabelCurrentLanguageId();
	}

	@Override
	public String getLocationLabelCurrentValue() {
		return model.getLocationLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized location labels of this attractions.
	 *
	 * @return the locales and localized location labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getLocationLabelMap() {
		return model.getLocationLabelMap();
	}

	/**
	 * Returns the location latitude of this attractions.
	 *
	 * @return the location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude() {
		return model.getLocationLatitude();
	}

	/**
	 * Returns the localized location latitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude(java.util.Locale locale) {
		return model.getLocationLatitude(locale);
	}

	/**
	 * Returns the localized location latitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location latitude of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLocationLatitude(
		java.util.Locale locale, boolean useDefault) {

		return model.getLocationLatitude(locale, useDefault);
	}

	/**
	 * Returns the localized location latitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude(String languageId) {
		return model.getLocationLatitude(languageId);
	}

	/**
	 * Returns the localized location latitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude(String languageId, boolean useDefault) {
		return model.getLocationLatitude(languageId, useDefault);
	}

	@Override
	public String getLocationLatitudeCurrentLanguageId() {
		return model.getLocationLatitudeCurrentLanguageId();
	}

	@Override
	public String getLocationLatitudeCurrentValue() {
		return model.getLocationLatitudeCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized location latitudes of this attractions.
	 *
	 * @return the locales and localized location latitudes of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getLocationLatitudeMap() {
		return model.getLocationLatitudeMap();
	}

	/**
	 * Returns the location longitude of this attractions.
	 *
	 * @return the location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude() {
		return model.getLocationLongitude();
	}

	/**
	 * Returns the localized location longitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude(java.util.Locale locale) {
		return model.getLocationLongitude(locale);
	}

	/**
	 * Returns the localized location longitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location longitude of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLocationLongitude(
		java.util.Locale locale, boolean useDefault) {

		return model.getLocationLongitude(locale, useDefault);
	}

	/**
	 * Returns the localized location longitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude(String languageId) {
		return model.getLocationLongitude(languageId);
	}

	/**
	 * Returns the localized location longitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude(String languageId, boolean useDefault) {
		return model.getLocationLongitude(languageId, useDefault);
	}

	@Override
	public String getLocationLongitudeCurrentLanguageId() {
		return model.getLocationLongitudeCurrentLanguageId();
	}

	@Override
	public String getLocationLongitudeCurrentValue() {
		return model.getLocationLongitudeCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized location longitudes of this attractions.
	 *
	 * @return the locales and localized location longitudes of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getLocationLongitudeMap() {
		return model.getLocationLongitudeMap();
	}

	/**
	 * Returns the modified date of this attractions.
	 *
	 * @return the modified date of this attractions
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this attractions.
	 *
	 * @return the primary key of this attractions
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this attractions.
	 *
	 * @return the user ID of this attractions
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this attractions.
	 *
	 * @return the user name of this attractions
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this attractions.
	 *
	 * @return the user uuid of this attractions
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this attractions.
	 *
	 * @return the uuid of this attractions
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the amenities label of this attractions.
	 *
	 * @param amenitiesLabel the amenities label of this attractions
	 */
	@Override
	public void setAmenitiesLabel(String amenitiesLabel) {
		model.setAmenitiesLabel(amenitiesLabel);
	}

	/**
	 * Sets the localized amenities label of this attractions in the language.
	 *
	 * @param amenitiesLabel the localized amenities label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setAmenitiesLabel(
		String amenitiesLabel, java.util.Locale locale) {

		model.setAmenitiesLabel(amenitiesLabel, locale);
	}

	/**
	 * Sets the localized amenities label of this attractions in the language, and sets the default locale.
	 *
	 * @param amenitiesLabel the localized amenities label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAmenitiesLabel(
		String amenitiesLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setAmenitiesLabel(amenitiesLabel, locale, defaultLocale);
	}

	@Override
	public void setAmenitiesLabelCurrentLanguageId(String languageId) {
		model.setAmenitiesLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized amenities labels of this attractions from the map of locales and localized amenities labels.
	 *
	 * @param amenitiesLabelMap the locales and localized amenities labels of this attractions
	 */
	@Override
	public void setAmenitiesLabelMap(
		Map<java.util.Locale, String> amenitiesLabelMap) {

		model.setAmenitiesLabelMap(amenitiesLabelMap);
	}

	/**
	 * Sets the localized amenities labels of this attractions from the map of locales and localized amenities labels, and sets the default locale.
	 *
	 * @param amenitiesLabelMap the locales and localized amenities labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAmenitiesLabelMap(
		Map<java.util.Locale, String> amenitiesLabelMap,
		java.util.Locale defaultLocale) {

		model.setAmenitiesLabelMap(amenitiesLabelMap, defaultLocale);
	}

	/**
	 * Sets the attractions ID of this attractions.
	 *
	 * @param attractionsId the attractions ID of this attractions
	 */
	@Override
	public void setAttractionsId(long attractionsId) {
		model.setAttractionsId(attractionsId);
	}

	/**
	 * Sets the book space label of this attractions.
	 *
	 * @param bookSpaceLabel the book space label of this attractions
	 */
	@Override
	public void setBookSpaceLabel(String bookSpaceLabel) {
		model.setBookSpaceLabel(bookSpaceLabel);
	}

	/**
	 * Sets the localized book space label of this attractions in the language.
	 *
	 * @param bookSpaceLabel the localized book space label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setBookSpaceLabel(
		String bookSpaceLabel, java.util.Locale locale) {

		model.setBookSpaceLabel(bookSpaceLabel, locale);
	}

	/**
	 * Sets the localized book space label of this attractions in the language, and sets the default locale.
	 *
	 * @param bookSpaceLabel the localized book space label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceLabel(
		String bookSpaceLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBookSpaceLabel(bookSpaceLabel, locale, defaultLocale);
	}

	@Override
	public void setBookSpaceLabelCurrentLanguageId(String languageId) {
		model.setBookSpaceLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized book space labels of this attractions from the map of locales and localized book space labels.
	 *
	 * @param bookSpaceLabelMap the locales and localized book space labels of this attractions
	 */
	@Override
	public void setBookSpaceLabelMap(
		Map<java.util.Locale, String> bookSpaceLabelMap) {

		model.setBookSpaceLabelMap(bookSpaceLabelMap);
	}

	/**
	 * Sets the localized book space labels of this attractions from the map of locales and localized book space labels, and sets the default locale.
	 *
	 * @param bookSpaceLabelMap the locales and localized book space labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceLabelMap(
		Map<java.util.Locale, String> bookSpaceLabelMap,
		java.util.Locale defaultLocale) {

		model.setBookSpaceLabelMap(bookSpaceLabelMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this attractions.
	 *
	 * @param companyId the company ID of this attractions
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this attractions.
	 *
	 * @param createDate the create date of this attractions
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the event ID of this attractions.
	 *
	 * @param eventId the event ID of this attractions
	 */
	@Override
	public void setEventId(String eventId) {
		model.setEventId(eventId);
	}

	/**
	 * Sets the gallary label of this attractions.
	 *
	 * @param gallaryLabel the gallary label of this attractions
	 */
	@Override
	public void setGallaryLabel(String gallaryLabel) {
		model.setGallaryLabel(gallaryLabel);
	}

	/**
	 * Sets the localized gallary label of this attractions in the language.
	 *
	 * @param gallaryLabel the localized gallary label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setGallaryLabel(String gallaryLabel, java.util.Locale locale) {
		model.setGallaryLabel(gallaryLabel, locale);
	}

	/**
	 * Sets the localized gallary label of this attractions in the language, and sets the default locale.
	 *
	 * @param gallaryLabel the localized gallary label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setGallaryLabel(
		String gallaryLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setGallaryLabel(gallaryLabel, locale, defaultLocale);
	}

	@Override
	public void setGallaryLabelCurrentLanguageId(String languageId) {
		model.setGallaryLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized gallary labels of this attractions from the map of locales and localized gallary labels.
	 *
	 * @param gallaryLabelMap the locales and localized gallary labels of this attractions
	 */
	@Override
	public void setGallaryLabelMap(
		Map<java.util.Locale, String> gallaryLabelMap) {

		model.setGallaryLabelMap(gallaryLabelMap);
	}

	/**
	 * Sets the localized gallary labels of this attractions from the map of locales and localized gallary labels, and sets the default locale.
	 *
	 * @param gallaryLabelMap the locales and localized gallary labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setGallaryLabelMap(
		Map<java.util.Locale, String> gallaryLabelMap,
		java.util.Locale defaultLocale) {

		model.setGallaryLabelMap(gallaryLabelMap, defaultLocale);
	}

	/**
	 * Sets the group ID of this attractions.
	 *
	 * @param groupId the group ID of this attractions
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the header article ID of this attractions.
	 *
	 * @param headerArticleId the header article ID of this attractions
	 */
	@Override
	public void setHeaderArticleId(String headerArticleId) {
		model.setHeaderArticleId(headerArticleId);
	}

	/**
	 * Sets the location label of this attractions.
	 *
	 * @param locationLabel the location label of this attractions
	 */
	@Override
	public void setLocationLabel(String locationLabel) {
		model.setLocationLabel(locationLabel);
	}

	/**
	 * Sets the localized location label of this attractions in the language.
	 *
	 * @param locationLabel the localized location label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setLocationLabel(
		String locationLabel, java.util.Locale locale) {

		model.setLocationLabel(locationLabel, locale);
	}

	/**
	 * Sets the localized location label of this attractions in the language, and sets the default locale.
	 *
	 * @param locationLabel the localized location label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLabel(
		String locationLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLocationLabel(locationLabel, locale, defaultLocale);
	}

	@Override
	public void setLocationLabelCurrentLanguageId(String languageId) {
		model.setLocationLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized location labels of this attractions from the map of locales and localized location labels.
	 *
	 * @param locationLabelMap the locales and localized location labels of this attractions
	 */
	@Override
	public void setLocationLabelMap(
		Map<java.util.Locale, String> locationLabelMap) {

		model.setLocationLabelMap(locationLabelMap);
	}

	/**
	 * Sets the localized location labels of this attractions from the map of locales and localized location labels, and sets the default locale.
	 *
	 * @param locationLabelMap the locales and localized location labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLabelMap(
		Map<java.util.Locale, String> locationLabelMap,
		java.util.Locale defaultLocale) {

		model.setLocationLabelMap(locationLabelMap, defaultLocale);
	}

	/**
	 * Sets the location latitude of this attractions.
	 *
	 * @param locationLatitude the location latitude of this attractions
	 */
	@Override
	public void setLocationLatitude(String locationLatitude) {
		model.setLocationLatitude(locationLatitude);
	}

	/**
	 * Sets the localized location latitude of this attractions in the language.
	 *
	 * @param locationLatitude the localized location latitude of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setLocationLatitude(
		String locationLatitude, java.util.Locale locale) {

		model.setLocationLatitude(locationLatitude, locale);
	}

	/**
	 * Sets the localized location latitude of this attractions in the language, and sets the default locale.
	 *
	 * @param locationLatitude the localized location latitude of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLatitude(
		String locationLatitude, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLocationLatitude(locationLatitude, locale, defaultLocale);
	}

	@Override
	public void setLocationLatitudeCurrentLanguageId(String languageId) {
		model.setLocationLatitudeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized location latitudes of this attractions from the map of locales and localized location latitudes.
	 *
	 * @param locationLatitudeMap the locales and localized location latitudes of this attractions
	 */
	@Override
	public void setLocationLatitudeMap(
		Map<java.util.Locale, String> locationLatitudeMap) {

		model.setLocationLatitudeMap(locationLatitudeMap);
	}

	/**
	 * Sets the localized location latitudes of this attractions from the map of locales and localized location latitudes, and sets the default locale.
	 *
	 * @param locationLatitudeMap the locales and localized location latitudes of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLatitudeMap(
		Map<java.util.Locale, String> locationLatitudeMap,
		java.util.Locale defaultLocale) {

		model.setLocationLatitudeMap(locationLatitudeMap, defaultLocale);
	}

	/**
	 * Sets the location longitude of this attractions.
	 *
	 * @param locationLongitude the location longitude of this attractions
	 */
	@Override
	public void setLocationLongitude(String locationLongitude) {
		model.setLocationLongitude(locationLongitude);
	}

	/**
	 * Sets the localized location longitude of this attractions in the language.
	 *
	 * @param locationLongitude the localized location longitude of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setLocationLongitude(
		String locationLongitude, java.util.Locale locale) {

		model.setLocationLongitude(locationLongitude, locale);
	}

	/**
	 * Sets the localized location longitude of this attractions in the language, and sets the default locale.
	 *
	 * @param locationLongitude the localized location longitude of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLongitude(
		String locationLongitude, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLocationLongitude(locationLongitude, locale, defaultLocale);
	}

	@Override
	public void setLocationLongitudeCurrentLanguageId(String languageId) {
		model.setLocationLongitudeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized location longitudes of this attractions from the map of locales and localized location longitudes.
	 *
	 * @param locationLongitudeMap the locales and localized location longitudes of this attractions
	 */
	@Override
	public void setLocationLongitudeMap(
		Map<java.util.Locale, String> locationLongitudeMap) {

		model.setLocationLongitudeMap(locationLongitudeMap);
	}

	/**
	 * Sets the localized location longitudes of this attractions from the map of locales and localized location longitudes, and sets the default locale.
	 *
	 * @param locationLongitudeMap the locales and localized location longitudes of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLongitudeMap(
		Map<java.util.Locale, String> locationLongitudeMap,
		java.util.Locale defaultLocale) {

		model.setLocationLongitudeMap(locationLongitudeMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this attractions.
	 *
	 * @param modifiedDate the modified date of this attractions
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this attractions.
	 *
	 * @param primaryKey the primary key of this attractions
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this attractions.
	 *
	 * @param userId the user ID of this attractions
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this attractions.
	 *
	 * @param userName the user name of this attractions
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this attractions.
	 *
	 * @param userUuid the user uuid of this attractions
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this attractions.
	 *
	 * @param uuid the uuid of this attractions
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected AttractionsWrapper wrap(Attractions attractions) {
		return new AttractionsWrapper(attractions);
	}

}