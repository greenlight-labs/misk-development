/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Gallary}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Gallary
 * @generated
 */
public class GallaryWrapper
	extends BaseModelWrapper<Gallary>
	implements Gallary, ModelWrapper<Gallary> {

	public GallaryWrapper(Gallary gallary) {
		super(gallary);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("gallaryId", getGallaryId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("attractionsId", getAttractionsId());
		attributes.put("image", getImage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long gallaryId = (Long)attributes.get("gallaryId");

		if (gallaryId != null) {
			setGallaryId(gallaryId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long attractionsId = (Long)attributes.get("attractionsId");

		if (attractionsId != null) {
			setAttractionsId(attractionsId);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}
	}

	/**
	 * Returns the attractions ID of this gallary.
	 *
	 * @return the attractions ID of this gallary
	 */
	@Override
	public long getAttractionsId() {
		return model.getAttractionsId();
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the company ID of this gallary.
	 *
	 * @return the company ID of this gallary
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this gallary.
	 *
	 * @return the create date of this gallary
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the gallary ID of this gallary.
	 *
	 * @return the gallary ID of this gallary
	 */
	@Override
	public long getGallaryId() {
		return model.getGallaryId();
	}

	/**
	 * Returns the image of this gallary.
	 *
	 * @return the image of this gallary
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the localized image of this gallary in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this gallary
	 */
	@Override
	public String getImage(java.util.Locale locale) {
		return model.getImage(locale);
	}

	/**
	 * Returns the localized image of this gallary in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this gallary. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getImage(java.util.Locale locale, boolean useDefault) {
		return model.getImage(locale, useDefault);
	}

	/**
	 * Returns the localized image of this gallary in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this gallary
	 */
	@Override
	public String getImage(String languageId) {
		return model.getImage(languageId);
	}

	/**
	 * Returns the localized image of this gallary in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this gallary
	 */
	@Override
	public String getImage(String languageId, boolean useDefault) {
		return model.getImage(languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return model.getImageCurrentLanguageId();
	}

	@Override
	public String getImageCurrentValue() {
		return model.getImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized images of this gallary.
	 *
	 * @return the locales and localized images of this gallary
	 */
	@Override
	public Map<java.util.Locale, String> getImageMap() {
		return model.getImageMap();
	}

	/**
	 * Returns the modified date of this gallary.
	 *
	 * @return the modified date of this gallary
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this gallary.
	 *
	 * @return the primary key of this gallary
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this gallary.
	 *
	 * @return the user ID of this gallary
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this gallary.
	 *
	 * @return the user name of this gallary
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this gallary.
	 *
	 * @return the user uuid of this gallary
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this gallary.
	 *
	 * @return the uuid of this gallary
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the attractions ID of this gallary.
	 *
	 * @param attractionsId the attractions ID of this gallary
	 */
	@Override
	public void setAttractionsId(long attractionsId) {
		model.setAttractionsId(attractionsId);
	}

	/**
	 * Sets the company ID of this gallary.
	 *
	 * @param companyId the company ID of this gallary
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this gallary.
	 *
	 * @param createDate the create date of this gallary
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the gallary ID of this gallary.
	 *
	 * @param gallaryId the gallary ID of this gallary
	 */
	@Override
	public void setGallaryId(long gallaryId) {
		model.setGallaryId(gallaryId);
	}

	/**
	 * Sets the image of this gallary.
	 *
	 * @param image the image of this gallary
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the localized image of this gallary in the language.
	 *
	 * @param image the localized image of this gallary
	 * @param locale the locale of the language
	 */
	@Override
	public void setImage(String image, java.util.Locale locale) {
		model.setImage(image, locale);
	}

	/**
	 * Sets the localized image of this gallary in the language, and sets the default locale.
	 *
	 * @param image the localized image of this gallary
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImage(
		String image, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setImage(image, locale, defaultLocale);
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		model.setImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized images of this gallary from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this gallary
	 */
	@Override
	public void setImageMap(Map<java.util.Locale, String> imageMap) {
		model.setImageMap(imageMap);
	}

	/**
	 * Sets the localized images of this gallary from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this gallary
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImageMap(
		Map<java.util.Locale, String> imageMap,
		java.util.Locale defaultLocale) {

		model.setImageMap(imageMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this gallary.
	 *
	 * @param modifiedDate the modified date of this gallary
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this gallary.
	 *
	 * @param primaryKey the primary key of this gallary
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this gallary.
	 *
	 * @param userId the user ID of this gallary
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this gallary.
	 *
	 * @param userName the user name of this gallary
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this gallary.
	 *
	 * @param userUuid the user uuid of this gallary
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this gallary.
	 *
	 * @param uuid the uuid of this gallary
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected GallaryWrapper wrap(Gallary gallary) {
		return new GallaryWrapper(gallary);
	}

}