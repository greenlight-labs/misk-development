/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link explore.service.http.BookSpaceServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class BookSpaceSoap implements Serializable {

	public static BookSpaceSoap toSoapModel(BookSpace model) {
		BookSpaceSoap soapModel = new BookSpaceSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setBookspaceId(model.getBookspaceId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAttractionsId(model.getAttractionsId());
		soapModel.setBookSpaceImage(model.getBookSpaceImage());
		soapModel.setBookSpaceTitle(model.getBookSpaceTitle());
		soapModel.setIcon(model.getIcon());
		soapModel.setCapacity(model.getCapacity());
		soapModel.setArea(model.getArea());
		soapModel.setDuration(model.getDuration());

		return soapModel;
	}

	public static BookSpaceSoap[] toSoapModels(BookSpace[] models) {
		BookSpaceSoap[] soapModels = new BookSpaceSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static BookSpaceSoap[][] toSoapModels(BookSpace[][] models) {
		BookSpaceSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new BookSpaceSoap[models.length][models[0].length];
		}
		else {
			soapModels = new BookSpaceSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static BookSpaceSoap[] toSoapModels(List<BookSpace> models) {
		List<BookSpaceSoap> soapModels = new ArrayList<BookSpaceSoap>(
			models.size());

		for (BookSpace model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new BookSpaceSoap[soapModels.size()]);
	}

	public BookSpaceSoap() {
	}

	public long getPrimaryKey() {
		return _bookspaceId;
	}

	public void setPrimaryKey(long pk) {
		setBookspaceId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getBookspaceId() {
		return _bookspaceId;
	}

	public void setBookspaceId(long bookspaceId) {
		_bookspaceId = bookspaceId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getAttractionsId() {
		return _attractionsId;
	}

	public void setAttractionsId(long attractionsId) {
		_attractionsId = attractionsId;
	}

	public String getBookSpaceImage() {
		return _bookSpaceImage;
	}

	public void setBookSpaceImage(String bookSpaceImage) {
		_bookSpaceImage = bookSpaceImage;
	}

	public String getBookSpaceTitle() {
		return _bookSpaceTitle;
	}

	public void setBookSpaceTitle(String bookSpaceTitle) {
		_bookSpaceTitle = bookSpaceTitle;
	}

	public String getIcon() {
		return _icon;
	}

	public void setIcon(String icon) {
		_icon = icon;
	}

	public String getCapacity() {
		return _capacity;
	}

	public void setCapacity(String capacity) {
		_capacity = capacity;
	}

	public String getArea() {
		return _area;
	}

	public void setArea(String area) {
		_area = area;
	}

	public String getDuration() {
		return _duration;
	}

	public void setDuration(String duration) {
		_duration = duration;
	}

	private String _uuid;
	private long _bookspaceId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _attractionsId;
	private String _bookSpaceImage;
	private String _bookSpaceTitle;
	private String _icon;
	private String _capacity;
	private String _area;
	private String _duration;

}