/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BookSpace}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookSpace
 * @generated
 */
public class BookSpaceWrapper
	extends BaseModelWrapper<BookSpace>
	implements BookSpace, ModelWrapper<BookSpace> {

	public BookSpaceWrapper(BookSpace bookSpace) {
		super(bookSpace);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("bookspaceId", getBookspaceId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("attractionsId", getAttractionsId());
		attributes.put("bookSpaceImage", getBookSpaceImage());
		attributes.put("bookSpaceTitle", getBookSpaceTitle());
		attributes.put("icon", getIcon());
		attributes.put("capacity", getCapacity());
		attributes.put("area", getArea());
		attributes.put("duration", getDuration());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long bookspaceId = (Long)attributes.get("bookspaceId");

		if (bookspaceId != null) {
			setBookspaceId(bookspaceId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long attractionsId = (Long)attributes.get("attractionsId");

		if (attractionsId != null) {
			setAttractionsId(attractionsId);
		}

		String bookSpaceImage = (String)attributes.get("bookSpaceImage");

		if (bookSpaceImage != null) {
			setBookSpaceImage(bookSpaceImage);
		}

		String bookSpaceTitle = (String)attributes.get("bookSpaceTitle");

		if (bookSpaceTitle != null) {
			setBookSpaceTitle(bookSpaceTitle);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		String capacity = (String)attributes.get("capacity");

		if (capacity != null) {
			setCapacity(capacity);
		}

		String area = (String)attributes.get("area");

		if (area != null) {
			setArea(area);
		}

		String duration = (String)attributes.get("duration");

		if (duration != null) {
			setDuration(duration);
		}
	}

	/**
	 * Returns the area of this book space.
	 *
	 * @return the area of this book space
	 */
	@Override
	public String getArea() {
		return model.getArea();
	}

	/**
	 * Returns the localized area of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized area of this book space
	 */
	@Override
	public String getArea(java.util.Locale locale) {
		return model.getArea(locale);
	}

	/**
	 * Returns the localized area of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized area of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getArea(java.util.Locale locale, boolean useDefault) {
		return model.getArea(locale, useDefault);
	}

	/**
	 * Returns the localized area of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized area of this book space
	 */
	@Override
	public String getArea(String languageId) {
		return model.getArea(languageId);
	}

	/**
	 * Returns the localized area of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized area of this book space
	 */
	@Override
	public String getArea(String languageId, boolean useDefault) {
		return model.getArea(languageId, useDefault);
	}

	@Override
	public String getAreaCurrentLanguageId() {
		return model.getAreaCurrentLanguageId();
	}

	@Override
	public String getAreaCurrentValue() {
		return model.getAreaCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized areas of this book space.
	 *
	 * @return the locales and localized areas of this book space
	 */
	@Override
	public Map<java.util.Locale, String> getAreaMap() {
		return model.getAreaMap();
	}

	/**
	 * Returns the attractions ID of this book space.
	 *
	 * @return the attractions ID of this book space
	 */
	@Override
	public long getAttractionsId() {
		return model.getAttractionsId();
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the bookspace ID of this book space.
	 *
	 * @return the bookspace ID of this book space
	 */
	@Override
	public long getBookspaceId() {
		return model.getBookspaceId();
	}

	/**
	 * Returns the book space image of this book space.
	 *
	 * @return the book space image of this book space
	 */
	@Override
	public String getBookSpaceImage() {
		return model.getBookSpaceImage();
	}

	/**
	 * Returns the localized book space image of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized book space image of this book space
	 */
	@Override
	public String getBookSpaceImage(java.util.Locale locale) {
		return model.getBookSpaceImage(locale);
	}

	/**
	 * Returns the localized book space image of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space image of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBookSpaceImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getBookSpaceImage(locale, useDefault);
	}

	/**
	 * Returns the localized book space image of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized book space image of this book space
	 */
	@Override
	public String getBookSpaceImage(String languageId) {
		return model.getBookSpaceImage(languageId);
	}

	/**
	 * Returns the localized book space image of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space image of this book space
	 */
	@Override
	public String getBookSpaceImage(String languageId, boolean useDefault) {
		return model.getBookSpaceImage(languageId, useDefault);
	}

	@Override
	public String getBookSpaceImageCurrentLanguageId() {
		return model.getBookSpaceImageCurrentLanguageId();
	}

	@Override
	public String getBookSpaceImageCurrentValue() {
		return model.getBookSpaceImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized book space images of this book space.
	 *
	 * @return the locales and localized book space images of this book space
	 */
	@Override
	public Map<java.util.Locale, String> getBookSpaceImageMap() {
		return model.getBookSpaceImageMap();
	}

	/**
	 * Returns the book space title of this book space.
	 *
	 * @return the book space title of this book space
	 */
	@Override
	public String getBookSpaceTitle() {
		return model.getBookSpaceTitle();
	}

	/**
	 * Returns the localized book space title of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized book space title of this book space
	 */
	@Override
	public String getBookSpaceTitle(java.util.Locale locale) {
		return model.getBookSpaceTitle(locale);
	}

	/**
	 * Returns the localized book space title of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space title of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBookSpaceTitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getBookSpaceTitle(locale, useDefault);
	}

	/**
	 * Returns the localized book space title of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized book space title of this book space
	 */
	@Override
	public String getBookSpaceTitle(String languageId) {
		return model.getBookSpaceTitle(languageId);
	}

	/**
	 * Returns the localized book space title of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space title of this book space
	 */
	@Override
	public String getBookSpaceTitle(String languageId, boolean useDefault) {
		return model.getBookSpaceTitle(languageId, useDefault);
	}

	@Override
	public String getBookSpaceTitleCurrentLanguageId() {
		return model.getBookSpaceTitleCurrentLanguageId();
	}

	@Override
	public String getBookSpaceTitleCurrentValue() {
		return model.getBookSpaceTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized book space titles of this book space.
	 *
	 * @return the locales and localized book space titles of this book space
	 */
	@Override
	public Map<java.util.Locale, String> getBookSpaceTitleMap() {
		return model.getBookSpaceTitleMap();
	}

	/**
	 * Returns the capacity of this book space.
	 *
	 * @return the capacity of this book space
	 */
	@Override
	public String getCapacity() {
		return model.getCapacity();
	}

	/**
	 * Returns the localized capacity of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized capacity of this book space
	 */
	@Override
	public String getCapacity(java.util.Locale locale) {
		return model.getCapacity(locale);
	}

	/**
	 * Returns the localized capacity of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized capacity of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getCapacity(java.util.Locale locale, boolean useDefault) {
		return model.getCapacity(locale, useDefault);
	}

	/**
	 * Returns the localized capacity of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized capacity of this book space
	 */
	@Override
	public String getCapacity(String languageId) {
		return model.getCapacity(languageId);
	}

	/**
	 * Returns the localized capacity of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized capacity of this book space
	 */
	@Override
	public String getCapacity(String languageId, boolean useDefault) {
		return model.getCapacity(languageId, useDefault);
	}

	@Override
	public String getCapacityCurrentLanguageId() {
		return model.getCapacityCurrentLanguageId();
	}

	@Override
	public String getCapacityCurrentValue() {
		return model.getCapacityCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized capacities of this book space.
	 *
	 * @return the locales and localized capacities of this book space
	 */
	@Override
	public Map<java.util.Locale, String> getCapacityMap() {
		return model.getCapacityMap();
	}

	/**
	 * Returns the company ID of this book space.
	 *
	 * @return the company ID of this book space
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this book space.
	 *
	 * @return the create date of this book space
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the duration of this book space.
	 *
	 * @return the duration of this book space
	 */
	@Override
	public String getDuration() {
		return model.getDuration();
	}

	/**
	 * Returns the localized duration of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized duration of this book space
	 */
	@Override
	public String getDuration(java.util.Locale locale) {
		return model.getDuration(locale);
	}

	/**
	 * Returns the localized duration of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized duration of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDuration(java.util.Locale locale, boolean useDefault) {
		return model.getDuration(locale, useDefault);
	}

	/**
	 * Returns the localized duration of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized duration of this book space
	 */
	@Override
	public String getDuration(String languageId) {
		return model.getDuration(languageId);
	}

	/**
	 * Returns the localized duration of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized duration of this book space
	 */
	@Override
	public String getDuration(String languageId, boolean useDefault) {
		return model.getDuration(languageId, useDefault);
	}

	@Override
	public String getDurationCurrentLanguageId() {
		return model.getDurationCurrentLanguageId();
	}

	@Override
	public String getDurationCurrentValue() {
		return model.getDurationCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized durations of this book space.
	 *
	 * @return the locales and localized durations of this book space
	 */
	@Override
	public Map<java.util.Locale, String> getDurationMap() {
		return model.getDurationMap();
	}

	/**
	 * Returns the icon of this book space.
	 *
	 * @return the icon of this book space
	 */
	@Override
	public String getIcon() {
		return model.getIcon();
	}

	/**
	 * Returns the localized icon of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized icon of this book space
	 */
	@Override
	public String getIcon(java.util.Locale locale) {
		return model.getIcon(locale);
	}

	/**
	 * Returns the localized icon of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized icon of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getIcon(java.util.Locale locale, boolean useDefault) {
		return model.getIcon(locale, useDefault);
	}

	/**
	 * Returns the localized icon of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized icon of this book space
	 */
	@Override
	public String getIcon(String languageId) {
		return model.getIcon(languageId);
	}

	/**
	 * Returns the localized icon of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized icon of this book space
	 */
	@Override
	public String getIcon(String languageId, boolean useDefault) {
		return model.getIcon(languageId, useDefault);
	}

	@Override
	public String getIconCurrentLanguageId() {
		return model.getIconCurrentLanguageId();
	}

	@Override
	public String getIconCurrentValue() {
		return model.getIconCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized icons of this book space.
	 *
	 * @return the locales and localized icons of this book space
	 */
	@Override
	public Map<java.util.Locale, String> getIconMap() {
		return model.getIconMap();
	}

	/**
	 * Returns the modified date of this book space.
	 *
	 * @return the modified date of this book space
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this book space.
	 *
	 * @return the primary key of this book space
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this book space.
	 *
	 * @return the user ID of this book space
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this book space.
	 *
	 * @return the user name of this book space
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this book space.
	 *
	 * @return the user uuid of this book space
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this book space.
	 *
	 * @return the uuid of this book space
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the area of this book space.
	 *
	 * @param area the area of this book space
	 */
	@Override
	public void setArea(String area) {
		model.setArea(area);
	}

	/**
	 * Sets the localized area of this book space in the language.
	 *
	 * @param area the localized area of this book space
	 * @param locale the locale of the language
	 */
	@Override
	public void setArea(String area, java.util.Locale locale) {
		model.setArea(area, locale);
	}

	/**
	 * Sets the localized area of this book space in the language, and sets the default locale.
	 *
	 * @param area the localized area of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setArea(
		String area, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setArea(area, locale, defaultLocale);
	}

	@Override
	public void setAreaCurrentLanguageId(String languageId) {
		model.setAreaCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized areas of this book space from the map of locales and localized areas.
	 *
	 * @param areaMap the locales and localized areas of this book space
	 */
	@Override
	public void setAreaMap(Map<java.util.Locale, String> areaMap) {
		model.setAreaMap(areaMap);
	}

	/**
	 * Sets the localized areas of this book space from the map of locales and localized areas, and sets the default locale.
	 *
	 * @param areaMap the locales and localized areas of this book space
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAreaMap(
		Map<java.util.Locale, String> areaMap, java.util.Locale defaultLocale) {

		model.setAreaMap(areaMap, defaultLocale);
	}

	/**
	 * Sets the attractions ID of this book space.
	 *
	 * @param attractionsId the attractions ID of this book space
	 */
	@Override
	public void setAttractionsId(long attractionsId) {
		model.setAttractionsId(attractionsId);
	}

	/**
	 * Sets the bookspace ID of this book space.
	 *
	 * @param bookspaceId the bookspace ID of this book space
	 */
	@Override
	public void setBookspaceId(long bookspaceId) {
		model.setBookspaceId(bookspaceId);
	}

	/**
	 * Sets the book space image of this book space.
	 *
	 * @param bookSpaceImage the book space image of this book space
	 */
	@Override
	public void setBookSpaceImage(String bookSpaceImage) {
		model.setBookSpaceImage(bookSpaceImage);
	}

	/**
	 * Sets the localized book space image of this book space in the language.
	 *
	 * @param bookSpaceImage the localized book space image of this book space
	 * @param locale the locale of the language
	 */
	@Override
	public void setBookSpaceImage(
		String bookSpaceImage, java.util.Locale locale) {

		model.setBookSpaceImage(bookSpaceImage, locale);
	}

	/**
	 * Sets the localized book space image of this book space in the language, and sets the default locale.
	 *
	 * @param bookSpaceImage the localized book space image of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceImage(
		String bookSpaceImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBookSpaceImage(bookSpaceImage, locale, defaultLocale);
	}

	@Override
	public void setBookSpaceImageCurrentLanguageId(String languageId) {
		model.setBookSpaceImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized book space images of this book space from the map of locales and localized book space images.
	 *
	 * @param bookSpaceImageMap the locales and localized book space images of this book space
	 */
	@Override
	public void setBookSpaceImageMap(
		Map<java.util.Locale, String> bookSpaceImageMap) {

		model.setBookSpaceImageMap(bookSpaceImageMap);
	}

	/**
	 * Sets the localized book space images of this book space from the map of locales and localized book space images, and sets the default locale.
	 *
	 * @param bookSpaceImageMap the locales and localized book space images of this book space
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceImageMap(
		Map<java.util.Locale, String> bookSpaceImageMap,
		java.util.Locale defaultLocale) {

		model.setBookSpaceImageMap(bookSpaceImageMap, defaultLocale);
	}

	/**
	 * Sets the book space title of this book space.
	 *
	 * @param bookSpaceTitle the book space title of this book space
	 */
	@Override
	public void setBookSpaceTitle(String bookSpaceTitle) {
		model.setBookSpaceTitle(bookSpaceTitle);
	}

	/**
	 * Sets the localized book space title of this book space in the language.
	 *
	 * @param bookSpaceTitle the localized book space title of this book space
	 * @param locale the locale of the language
	 */
	@Override
	public void setBookSpaceTitle(
		String bookSpaceTitle, java.util.Locale locale) {

		model.setBookSpaceTitle(bookSpaceTitle, locale);
	}

	/**
	 * Sets the localized book space title of this book space in the language, and sets the default locale.
	 *
	 * @param bookSpaceTitle the localized book space title of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceTitle(
		String bookSpaceTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBookSpaceTitle(bookSpaceTitle, locale, defaultLocale);
	}

	@Override
	public void setBookSpaceTitleCurrentLanguageId(String languageId) {
		model.setBookSpaceTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized book space titles of this book space from the map of locales and localized book space titles.
	 *
	 * @param bookSpaceTitleMap the locales and localized book space titles of this book space
	 */
	@Override
	public void setBookSpaceTitleMap(
		Map<java.util.Locale, String> bookSpaceTitleMap) {

		model.setBookSpaceTitleMap(bookSpaceTitleMap);
	}

	/**
	 * Sets the localized book space titles of this book space from the map of locales and localized book space titles, and sets the default locale.
	 *
	 * @param bookSpaceTitleMap the locales and localized book space titles of this book space
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceTitleMap(
		Map<java.util.Locale, String> bookSpaceTitleMap,
		java.util.Locale defaultLocale) {

		model.setBookSpaceTitleMap(bookSpaceTitleMap, defaultLocale);
	}

	/**
	 * Sets the capacity of this book space.
	 *
	 * @param capacity the capacity of this book space
	 */
	@Override
	public void setCapacity(String capacity) {
		model.setCapacity(capacity);
	}

	/**
	 * Sets the localized capacity of this book space in the language.
	 *
	 * @param capacity the localized capacity of this book space
	 * @param locale the locale of the language
	 */
	@Override
	public void setCapacity(String capacity, java.util.Locale locale) {
		model.setCapacity(capacity, locale);
	}

	/**
	 * Sets the localized capacity of this book space in the language, and sets the default locale.
	 *
	 * @param capacity the localized capacity of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setCapacity(
		String capacity, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setCapacity(capacity, locale, defaultLocale);
	}

	@Override
	public void setCapacityCurrentLanguageId(String languageId) {
		model.setCapacityCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized capacities of this book space from the map of locales and localized capacities.
	 *
	 * @param capacityMap the locales and localized capacities of this book space
	 */
	@Override
	public void setCapacityMap(Map<java.util.Locale, String> capacityMap) {
		model.setCapacityMap(capacityMap);
	}

	/**
	 * Sets the localized capacities of this book space from the map of locales and localized capacities, and sets the default locale.
	 *
	 * @param capacityMap the locales and localized capacities of this book space
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setCapacityMap(
		Map<java.util.Locale, String> capacityMap,
		java.util.Locale defaultLocale) {

		model.setCapacityMap(capacityMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this book space.
	 *
	 * @param companyId the company ID of this book space
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this book space.
	 *
	 * @param createDate the create date of this book space
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the duration of this book space.
	 *
	 * @param duration the duration of this book space
	 */
	@Override
	public void setDuration(String duration) {
		model.setDuration(duration);
	}

	/**
	 * Sets the localized duration of this book space in the language.
	 *
	 * @param duration the localized duration of this book space
	 * @param locale the locale of the language
	 */
	@Override
	public void setDuration(String duration, java.util.Locale locale) {
		model.setDuration(duration, locale);
	}

	/**
	 * Sets the localized duration of this book space in the language, and sets the default locale.
	 *
	 * @param duration the localized duration of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDuration(
		String duration, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDuration(duration, locale, defaultLocale);
	}

	@Override
	public void setDurationCurrentLanguageId(String languageId) {
		model.setDurationCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized durations of this book space from the map of locales and localized durations.
	 *
	 * @param durationMap the locales and localized durations of this book space
	 */
	@Override
	public void setDurationMap(Map<java.util.Locale, String> durationMap) {
		model.setDurationMap(durationMap);
	}

	/**
	 * Sets the localized durations of this book space from the map of locales and localized durations, and sets the default locale.
	 *
	 * @param durationMap the locales and localized durations of this book space
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDurationMap(
		Map<java.util.Locale, String> durationMap,
		java.util.Locale defaultLocale) {

		model.setDurationMap(durationMap, defaultLocale);
	}

	/**
	 * Sets the icon of this book space.
	 *
	 * @param icon the icon of this book space
	 */
	@Override
	public void setIcon(String icon) {
		model.setIcon(icon);
	}

	/**
	 * Sets the localized icon of this book space in the language.
	 *
	 * @param icon the localized icon of this book space
	 * @param locale the locale of the language
	 */
	@Override
	public void setIcon(String icon, java.util.Locale locale) {
		model.setIcon(icon, locale);
	}

	/**
	 * Sets the localized icon of this book space in the language, and sets the default locale.
	 *
	 * @param icon the localized icon of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setIcon(
		String icon, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setIcon(icon, locale, defaultLocale);
	}

	@Override
	public void setIconCurrentLanguageId(String languageId) {
		model.setIconCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized icons of this book space from the map of locales and localized icons.
	 *
	 * @param iconMap the locales and localized icons of this book space
	 */
	@Override
	public void setIconMap(Map<java.util.Locale, String> iconMap) {
		model.setIconMap(iconMap);
	}

	/**
	 * Sets the localized icons of this book space from the map of locales and localized icons, and sets the default locale.
	 *
	 * @param iconMap the locales and localized icons of this book space
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setIconMap(
		Map<java.util.Locale, String> iconMap, java.util.Locale defaultLocale) {

		model.setIconMap(iconMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this book space.
	 *
	 * @param modifiedDate the modified date of this book space
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this book space.
	 *
	 * @param primaryKey the primary key of this book space
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this book space.
	 *
	 * @param userId the user ID of this book space
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this book space.
	 *
	 * @param userName the user name of this book space
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this book space.
	 *
	 * @param userUuid the user uuid of this book space
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this book space.
	 *
	 * @param uuid the uuid of this book space
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected BookSpaceWrapper wrap(BookSpace bookSpace) {
		return new BookSpaceWrapper(bookSpace);
	}

}