/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link explore.service.http.GallaryServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class GallarySoap implements Serializable {

	public static GallarySoap toSoapModel(Gallary model) {
		GallarySoap soapModel = new GallarySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setGallaryId(model.getGallaryId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAttractionsId(model.getAttractionsId());
		soapModel.setImage(model.getImage());

		return soapModel;
	}

	public static GallarySoap[] toSoapModels(Gallary[] models) {
		GallarySoap[] soapModels = new GallarySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static GallarySoap[][] toSoapModels(Gallary[][] models) {
		GallarySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new GallarySoap[models.length][models[0].length];
		}
		else {
			soapModels = new GallarySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static GallarySoap[] toSoapModels(List<Gallary> models) {
		List<GallarySoap> soapModels = new ArrayList<GallarySoap>(
			models.size());

		for (Gallary model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new GallarySoap[soapModels.size()]);
	}

	public GallarySoap() {
	}

	public long getPrimaryKey() {
		return _gallaryId;
	}

	public void setPrimaryKey(long pk) {
		setGallaryId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getGallaryId() {
		return _gallaryId;
	}

	public void setGallaryId(long gallaryId) {
		_gallaryId = gallaryId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getAttractionsId() {
		return _attractionsId;
	}

	public void setAttractionsId(long attractionsId) {
		_attractionsId = attractionsId;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	private String _uuid;
	private long _gallaryId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _attractionsId;
	private String _image;

}