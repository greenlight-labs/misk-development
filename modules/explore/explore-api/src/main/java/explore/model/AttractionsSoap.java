/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link explore.service.http.AttractionsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class AttractionsSoap implements Serializable {

	public static AttractionsSoap toSoapModel(Attractions model) {
		AttractionsSoap soapModel = new AttractionsSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setAttractionsId(model.getAttractionsId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setHeaderArticleId(model.getHeaderArticleId());
		soapModel.setLocationLabel(model.getLocationLabel());
		soapModel.setLocationLatitude(model.getLocationLatitude());
		soapModel.setLocationLongitude(model.getLocationLongitude());
		soapModel.setAmenitiesLabel(model.getAmenitiesLabel());
		soapModel.setBookSpaceLabel(model.getBookSpaceLabel());
		soapModel.setGallaryLabel(model.getGallaryLabel());
		soapModel.setEventId(model.getEventId());

		return soapModel;
	}

	public static AttractionsSoap[] toSoapModels(Attractions[] models) {
		AttractionsSoap[] soapModels = new AttractionsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AttractionsSoap[][] toSoapModels(Attractions[][] models) {
		AttractionsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AttractionsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AttractionsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AttractionsSoap[] toSoapModels(List<Attractions> models) {
		List<AttractionsSoap> soapModels = new ArrayList<AttractionsSoap>(
			models.size());

		for (Attractions model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AttractionsSoap[soapModels.size()]);
	}

	public AttractionsSoap() {
	}

	public long getPrimaryKey() {
		return _attractionsId;
	}

	public void setPrimaryKey(long pk) {
		setAttractionsId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getAttractionsId() {
		return _attractionsId;
	}

	public void setAttractionsId(long attractionsId) {
		_attractionsId = attractionsId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getHeaderArticleId() {
		return _headerArticleId;
	}

	public void setHeaderArticleId(String headerArticleId) {
		_headerArticleId = headerArticleId;
	}

	public String getLocationLabel() {
		return _locationLabel;
	}

	public void setLocationLabel(String locationLabel) {
		_locationLabel = locationLabel;
	}

	public String getLocationLatitude() {
		return _locationLatitude;
	}

	public void setLocationLatitude(String locationLatitude) {
		_locationLatitude = locationLatitude;
	}

	public String getLocationLongitude() {
		return _locationLongitude;
	}

	public void setLocationLongitude(String locationLongitude) {
		_locationLongitude = locationLongitude;
	}

	public String getAmenitiesLabel() {
		return _amenitiesLabel;
	}

	public void setAmenitiesLabel(String amenitiesLabel) {
		_amenitiesLabel = amenitiesLabel;
	}

	public String getBookSpaceLabel() {
		return _bookSpaceLabel;
	}

	public void setBookSpaceLabel(String bookSpaceLabel) {
		_bookSpaceLabel = bookSpaceLabel;
	}

	public String getGallaryLabel() {
		return _gallaryLabel;
	}

	public void setGallaryLabel(String gallaryLabel) {
		_gallaryLabel = gallaryLabel;
	}

	public String getEventId() {
		return _eventId;
	}

	public void setEventId(String eventId) {
		_eventId = eventId;
	}

	private String _uuid;
	private long _attractionsId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _headerArticleId;
	private String _locationLabel;
	private String _locationLatitude;
	private String _locationLongitude;
	private String _amenitiesLabel;
	private String _bookSpaceLabel;
	private String _gallaryLabel;
	private String _eventId;

}