package explore.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.*;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import explore.admin.web.constants.ExploreAdminWebPortletKeys;
import explore.model.Attractions;
import explore.service.AttractionsLocalService;

/**
 * @author manis
 */
@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true", "com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false", "com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true", "javax.portlet.display-name=ExploreAdminWeb",
		"javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ExploreAdminWebPortletKeys.EXPLOREADMINWEB,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",

}, service = Portlet.class)
public class ExploreAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		// set attribute to render request
		renderRequest.setAttribute("attractionsId", "112351");

		super.render(renderRequest, renderResponse);
	}

	public void addAttraction(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Attractions.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		Map<Locale, String> locationLabel = LocalizationUtil.getLocalizationMap(request, "locationLabel");
		Map<Locale, String> locationLatitude = LocalizationUtil.getLocalizationMap(request, "locationLatitude");
		Map<Locale, String> locationLongitude = LocalizationUtil.getLocalizationMap(request, "locationLongitude");
		Map<Locale, String> amenitiesLabel = LocalizationUtil.getLocalizationMap(request, "amenitiesLabel");
		Map<Locale, String> bookSpaceLabel = LocalizationUtil.getLocalizationMap(request, "bookSpaceLabel");
		Map<Locale, String> gallaryLabel = LocalizationUtil.getLocalizationMap(request, "gallaryLabel");
		String eventId = ParamUtil.getString(request, "eventId");
		String headerArticleId = ParamUtil.getString(request, "headerArticleId");

		// _attractionsService.
		Attractions attractions = _attractionsLocalService.addAttractions(themeDisplay.getUserId(), locationLabel,
				locationLatitude, locationLongitude, amenitiesLabel, bookSpaceLabel, gallaryLabel, eventId,
				headerArticleId, serviceContext);

		// Add gallery images
		_attractionsLocalService.updateAttractionssGalleries(attractions,
				_attractionsLocalService.getGalleries(request), serviceContext);

		// Add aminities

		_attractionsLocalService.updateAmenities(attractions, _attractionsLocalService.getAmenities(request),
				serviceContext);

		// Add BookSpace
		_attractionsLocalService.updateBookSpace(attractions, _attractionsLocalService.getBookSpace(request),
				serviceContext);
	}

	public void updateAttraction(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Attractions.class.getName(), request);
		long attractionsId = ParamUtil.getLong(request, "attractionsId");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Map<Locale, String> locationLabel = LocalizationUtil.getLocalizationMap(request, "locationLabel");
		Map<Locale, String> locationLatitude = LocalizationUtil.getLocalizationMap(request, "locationLatitude");
		Map<Locale, String> locationLongitude = LocalizationUtil.getLocalizationMap(request, "locationLongitude");
		Map<Locale, String> amenitiesLabel = LocalizationUtil.getLocalizationMap(request, "amenitiesLabel");
		Map<Locale, String> bookSpaceLabel = LocalizationUtil.getLocalizationMap(request, "bookSpaceLabel");
		Map<Locale, String> gallaryLabel = LocalizationUtil.getLocalizationMap(request, "gallaryLabel");
		String headerArticleId = ParamUtil.getString(request, "headerArticleId");
		String eventId = ParamUtil.getString(request, "eventId");
		// _attractionsService.
		Attractions attractions = _attractionsLocalService.updateAttractions(themeDisplay.getUserId(), attractionsId,
				locationLabel, locationLatitude, locationLongitude, amenitiesLabel, bookSpaceLabel, gallaryLabel,
				eventId, headerArticleId, serviceContext);

		// Add gallery images
		_attractionsLocalService.updateAttractionssGalleries(attractions,
				_attractionsLocalService.getGalleries(request), serviceContext);
		// Add aminities

		_attractionsLocalService.updateAmenities(attractions, _attractionsLocalService.getAmenities(request),
				serviceContext);

		// Add BookSpace
		_attractionsLocalService.updateBookSpace(attractions, _attractionsLocalService.getBookSpace(request),
				serviceContext);
	}

	public void deleteAttractions(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Attractions.class.getName(), request);

		long attractionsId = ParamUtil.getLong(request, "attractionsId");

		try {

			_attractionsLocalService.deleteAttractions(attractionsId, serviceContext);
		} catch (PortalException pe) {

		}
	}

	@Reference
	private AttractionsLocalService _attractionsLocalService;

	@Reference
	private ItemSelector _itemSelector;

}