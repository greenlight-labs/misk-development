package explore.admin.web.application.list;

import explore.admin.web.constants.ExploreAdminWebPanelCategoryKeys;
import explore.admin.web.constants.ExploreAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author manis
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + ExploreAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class ExploreAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return ExploreAdminWebPortletKeys.EXPLOREADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + ExploreAdminWebPortletKeys.EXPLOREADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}