<%@page import="explore.model.Attractions"%>
<%@include file="/init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Attractions attractions = (Attractions) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="attractionsId"
                       value="<%=String.valueOf(attractions.getAttractionsId()) %>" />
        <portlet:param name="mvcPath"
                       value="/edit_attractions.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteAttractions" var="deleteURL">
        <portlet:param name="attractionsId"
                       value="<%= String.valueOf(attractions.getAttractionsId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>