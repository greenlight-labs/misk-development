<%@page import="explore.service.AttractionsLocalServiceUtil"%>
<%@page import="explore.model.Attractions"%>
<%@include file = "/init.jsp" %>

<%
    String attractionId = request.getAttribute("attractionsId").toString();
    long attractionsId = Long.parseLong(attractionId);

Attractions attraction = null;

    if (attractionsId > 0) {
        try {
        	attraction = AttractionsLocalServiceUtil.getAttractions(attractionsId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }


%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= attraction == null ? "addAttraction" : "updateAttraction" %>' var="editAttractionURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editAttractionURL %>" name="fm">
    <aui:model-context bean="<%= attraction %>" model="<%= Attractions.class %>" />
    <aui:input type="hidden" name="attractionsId"
               value='<%= attraction == null ? "" : attraction.getAttractionsId() %>' />
    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
             <aui:input name="headerArticleId" label="Header Article Id" helpMessage="Header Article Id" />
            <aui:input name="locationLabel" label="Location Label" helpMessage="Location Label" >
             <aui:validator name="required"/>
             </aui:input>
            <aui:input name="locationLatitude" label="Location Latitude" helpMessage="Location Latitude" />
            <aui:input name="locationLongitude" label="Location Longitude" helpMessage="Location Longitude" />


            <aui:input name="amenitiesLabel" label="Facilities Label" helpMessage="Facilities Label" />
            <aui:input name="bookSpaceLabel" label="Explore Label" helpMessage="Explore Label" />
            <aui:input name="gallaryLabel" label="Gallary Label" helpMessage="Gallary Label" />
            <aui:input name="eventId" label="Event Id" helpMessage="Event Id" />
        </aui:fieldset>
    </aui:fieldset-group>
   		<%@include file="/includes/gallery.jsp" %>
	  <%@include file="/includes/amenities.jsp" %>
	  <%@include file="/includes/bookSpace.jsp" %>
        <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>


<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>