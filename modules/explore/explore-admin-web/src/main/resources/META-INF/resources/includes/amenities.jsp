<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="explore.model.Amenities" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="explore.service.AmenitiesLocalServiceUtil" %>
<%@ page import="explore.model.impl.AmenitiesImpl" %><%
    /* *****************************************************/
    List<Amenities> amenitiesSlides = Collections.emptyList();
    int[] amenitiesIndexes = null;

    String amenitiesIndexesParam = ParamUtil.getString(request, "amenitiesIndexes");
    if (Validator.isNotNull(amenitiesIndexesParam)) {
        amenitiesSlides = new ArrayList<Amenities>();

        amenitiesIndexes = StringUtil.split(amenitiesIndexesParam, 0);

        for (int scheduleIndex : amenitiesIndexes) {
            amenitiesSlides.add(new AmenitiesImpl());
        }
    } else {
    	if (attraction != null) {
            amenitiesSlides = AmenitiesLocalServiceUtil.findAllInAttractions(attraction.getAttractionsId());

            amenitiesIndexes = new int[amenitiesSlides.size()];

            for (int i = 0; i < amenitiesSlides.size(); i++) {
                amenitiesIndexes[i] = i;
            }
        }

        if (amenitiesSlides.isEmpty()) {
            amenitiesSlides = new ArrayList<Amenities>();

            amenitiesSlides.add(new AmenitiesImpl());

            amenitiesIndexes = new int[] {0};
        }

        if (amenitiesIndexes == null) {
            amenitiesIndexes = new int[0];
        }
    }
    /* *****************************************************/
%>

<aui:fieldset-group markupView="lexicon">
    <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Facilites Section" id='<%= renderResponse.getNamespace() + "amenitiesSlides" %>'>
        <%
            for (int i = 0; i < amenitiesIndexes.length; i++) {
                int amenitiesSlideIndex = amenitiesIndexes[i];
                Amenities amenitiesSlide = amenitiesSlides.get(i);
        %>
        <aui:model-context bean="<%= amenitiesSlide %>" model="<%= Amenities.class %>"/>
        <div class="field-row lfr-form-row lfr-form-row-inline">
            <div class="row-fields">
                <aui:field-wrapper>
                    <div style="color:#bc1a1ab3" name='<%= "slide" + amenitiesSlideIndex %>' class="slide-heading"><h3>Slide <%= amenitiesSlideIndex + 1 %></h3></div>
                    <aui:input name='<%= "amenitiesId" + amenitiesSlideIndex %>' type="hidden" value="<%= amenitiesSlide.getAmenitiesId() %>" />
                    
                    <aui:input label="Title" fieldParam='<%= "title" + amenitiesSlideIndex %>' id='<%= "title" + amenitiesSlideIndex %>' name="title" helpMessage="Max 15 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                    <aui:input label="Image" fieldParam='<%= "image" + amenitiesSlideIndex %>' id='<%= "image" + amenitiesSlideIndex %>' name="image" helpMessage="Image Dimensions: 1229 x 931 pixels">
                        <aui:validator name="required"/>
                        <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                            function(val) {
                                var ext = val.substring(val.lastIndexOf('.') + 1);
                                return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                            }
                        </aui:validator>
                    </aui:input>
                    <div class="button-holder">
                        <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "image" + amenitiesSlideIndex %>">
                            <span class="lfr-btn-label">Select</span>
                        </button>
                    </div>
                </aui:field-wrapper>
            </div>
        </div>
        <%
            }
        %>
        <aui:input name="amenitiesIndexes" type="hidden" value="<%= StringUtil.merge(amenitiesIndexes) %>"/>
    </aui:fieldset>
</aui:fieldset-group>

<aui:script use="liferay-auto-fields">
    new Liferay.AutoFields({
    contentBox: '#<portlet:namespace />amenitiesSlides',
    fieldIndexes: '<portlet:namespace />amenitiesIndexes',
    namespace: '<portlet:namespace />',
    sortable: true,
    sortableHandle: '.lfr-form-row'
    }).render();
</aui:script>
<aui:script use="liferay-item-selector-dialog">
    /*Repeater fields image selection*/
    var amenitiesSlidesContainer = $('#<portlet:namespace />amenitiesSlides');
    amenitiesSlidesContainer.on('click','.select-button-repeater',function(event){
        event.preventDefault();
		var id=this.name.match(/(\d+)/)[0];
        var element = event.currentTarget;
        var fieldId = element.name;
		var itemSelectorDialog = new A.LiferayItemSelectorDialog
		(
			{
				eventName: 'selectDocumentLibrary',
				on: {
						selectedItemChange: function(event) {
							var selectedItem = event.newVal;
							if(selectedItem)
							{
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
						   }
						}
				},
				title: '<liferay-ui:message key="Select File" />',
				url: '${itemSelectorURL }'
			}
		);
		itemSelectorDialog.open();

	});
</aui:script>
<script type="text/javascript">
    var scheduleContainer = $('#<portlet:namespace />amenitiesSlides');
    /*change slide number on add/delete row*/
    scheduleContainer.on('click', '.add-row', function(event){
        event.preventDefault();
        var panelBody = $(this).parents('div.panel-body');
        var fieldRows = panelBody.find('div.field-row');
        fieldRows.each(function(index, item) {
            var slideHeading = $(item).find('div.slide-heading');
            var id = index+1;
            slideHeading.find('h3').text('Slide '+id);
        });
    });
</script>