create index IX_24EC520F on misk_explore_attractions (groupId);
create index IX_9875E62F on misk_explore_attractions (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E5502C71 on misk_explore_attractions (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_4160F8CB on misk_explore_bookspace (attractionsId);
create index IX_7E2853C0 on misk_explore_bookspace (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_11C95600 on misk_explore_faclities (attractionsId);
create index IX_1F1EEE6B on misk_explore_faclities (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_E7F6EE1A on misk_explore_gallary (attractionsId);
create index IX_42F62811 on misk_explore_gallary (uuid_[$COLUMN_LENGTH:75$], companyId);