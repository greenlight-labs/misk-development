create table misk_explore_attractions (
	uuid_ VARCHAR(75) null,
	attractionsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	headerArticleId VARCHAR(75) null,
	locationLabel STRING null,
	locationLatitude STRING null,
	locationLongitude STRING null,
	amenitiesLabel STRING null,
	bookSpaceLabel STRING null,
	gallaryLabel STRING null,
	eventId VARCHAR(75) null
);

create table misk_explore_bookspace (
	uuid_ VARCHAR(75) null,
	bookspaceId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attractionsId LONG,
	bookSpaceImage STRING null,
	bookSpaceTitle STRING null,
	icon STRING null,
	capacity STRING null,
	area STRING null,
	duration STRING null
);

create table misk_explore_faclities (
	uuid_ VARCHAR(75) null,
	amenitiesId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attractionsId LONG,
	title STRING null,
	image STRING null
);

create table misk_explore_gallary (
	uuid_ VARCHAR(75) null,
	gallaryId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attractionsId LONG,
	image STRING null
);