/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import explore.exception.NoSuchBookSpaceException;

import explore.model.BookSpace;
import explore.model.impl.BookSpaceImpl;
import explore.model.impl.BookSpaceModelImpl;

import explore.service.persistence.BookSpacePersistence;
import explore.service.persistence.BookSpaceUtil;
import explore.service.persistence.impl.constants.ExplorePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the book space service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = BookSpacePersistence.class)
public class BookSpacePersistenceImpl
	extends BasePersistenceImpl<BookSpace> implements BookSpacePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>BookSpaceUtil</code> to access the book space persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		BookSpaceImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the book spaces where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<BookSpace> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<BookSpace> list = null;

		if (useFinderCache) {
			list = (List<BookSpace>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (BookSpace bookSpace : list) {
					if (!uuid.equals(bookSpace.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_BOOKSPACE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(BookSpaceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<BookSpace>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	@Override
	public BookSpace findByUuid_First(
			String uuid, OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = fetchByUuid_First(uuid, orderByComparator);

		if (bookSpace != null) {
			return bookSpace;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchBookSpaceException(sb.toString());
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	@Override
	public BookSpace fetchByUuid_First(
		String uuid, OrderByComparator<BookSpace> orderByComparator) {

		List<BookSpace> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	@Override
	public BookSpace findByUuid_Last(
			String uuid, OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = fetchByUuid_Last(uuid, orderByComparator);

		if (bookSpace != null) {
			return bookSpace;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchBookSpaceException(sb.toString());
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	@Override
	public BookSpace fetchByUuid_Last(
		String uuid, OrderByComparator<BookSpace> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<BookSpace> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where uuid = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace[] findByUuid_PrevAndNext(
			long bookspaceId, String uuid,
			OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		uuid = Objects.toString(uuid, "");

		BookSpace bookSpace = findByPrimaryKey(bookspaceId);

		Session session = null;

		try {
			session = openSession();

			BookSpace[] array = new BookSpaceImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, bookSpace, uuid, orderByComparator, true);

			array[1] = bookSpace;

			array[2] = getByUuid_PrevAndNext(
				session, bookSpace, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookSpace getByUuid_PrevAndNext(
		Session session, BookSpace bookSpace, String uuid,
		OrderByComparator<BookSpace> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_BOOKSPACE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(BookSpaceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(bookSpace)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<BookSpace> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book spaces where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (BookSpace bookSpace :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(bookSpace);
		}
	}

	/**
	 * Returns the number of book spaces where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching book spaces
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_BOOKSPACE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"bookSpace.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(bookSpace.uuid IS NULL OR bookSpace.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<BookSpace> list = null;

		if (useFinderCache) {
			list = (List<BookSpace>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (BookSpace bookSpace : list) {
					if (!uuid.equals(bookSpace.getUuid()) ||
						(companyId != bookSpace.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_BOOKSPACE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(BookSpaceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<BookSpace>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	@Override
	public BookSpace findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (bookSpace != null) {
			return bookSpace;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchBookSpaceException(sb.toString());
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	@Override
	public BookSpace fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<BookSpace> orderByComparator) {

		List<BookSpace> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	@Override
	public BookSpace findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (bookSpace != null) {
			return bookSpace;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchBookSpaceException(sb.toString());
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	@Override
	public BookSpace fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<BookSpace> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<BookSpace> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace[] findByUuid_C_PrevAndNext(
			long bookspaceId, String uuid, long companyId,
			OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		uuid = Objects.toString(uuid, "");

		BookSpace bookSpace = findByPrimaryKey(bookspaceId);

		Session session = null;

		try {
			session = openSession();

			BookSpace[] array = new BookSpaceImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, bookSpace, uuid, companyId, orderByComparator, true);

			array[1] = bookSpace;

			array[2] = getByUuid_C_PrevAndNext(
				session, bookSpace, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookSpace getByUuid_C_PrevAndNext(
		Session session, BookSpace bookSpace, String uuid, long companyId,
		OrderByComparator<BookSpace> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_BOOKSPACE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(BookSpaceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(bookSpace)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<BookSpace> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book spaces where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (BookSpace bookSpace :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(bookSpace);
		}
	}

	/**
	 * Returns the number of book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching book spaces
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_BOOKSPACE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"bookSpace.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(bookSpace.uuid IS NULL OR bookSpace.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"bookSpace.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByAttractionsId;
	private FinderPath _finderPathWithoutPaginationFindByAttractionsId;
	private FinderPath _finderPathCountByAttractionsId;

	/**
	 * Returns all the book spaces where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the matching book spaces
	 */
	@Override
	public List<BookSpace> findByAttractionsId(long attractionsId) {
		return findByAttractionsId(
			attractionsId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end) {

		return findByAttractionsId(attractionsId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator) {

		return findByAttractionsId(
			attractionsId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	@Override
	public List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByAttractionsId;
				finderArgs = new Object[] {attractionsId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByAttractionsId;
			finderArgs = new Object[] {
				attractionsId, start, end, orderByComparator
			};
		}

		List<BookSpace> list = null;

		if (useFinderCache) {
			list = (List<BookSpace>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (BookSpace bookSpace : list) {
					if (attractionsId != bookSpace.getAttractionsId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_BOOKSPACE_WHERE);

			sb.append(_FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(BookSpaceModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(attractionsId);

				list = (List<BookSpace>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	@Override
	public BookSpace findByAttractionsId_First(
			long attractionsId, OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = fetchByAttractionsId_First(
			attractionsId, orderByComparator);

		if (bookSpace != null) {
			return bookSpace;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("attractionsId=");
		sb.append(attractionsId);

		sb.append("}");

		throw new NoSuchBookSpaceException(sb.toString());
	}

	/**
	 * Returns the first book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	@Override
	public BookSpace fetchByAttractionsId_First(
		long attractionsId, OrderByComparator<BookSpace> orderByComparator) {

		List<BookSpace> list = findByAttractionsId(
			attractionsId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	@Override
	public BookSpace findByAttractionsId_Last(
			long attractionsId, OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = fetchByAttractionsId_Last(
			attractionsId, orderByComparator);

		if (bookSpace != null) {
			return bookSpace;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("attractionsId=");
		sb.append(attractionsId);

		sb.append("}");

		throw new NoSuchBookSpaceException(sb.toString());
	}

	/**
	 * Returns the last book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	@Override
	public BookSpace fetchByAttractionsId_Last(
		long attractionsId, OrderByComparator<BookSpace> orderByComparator) {

		int count = countByAttractionsId(attractionsId);

		if (count == 0) {
			return null;
		}

		List<BookSpace> list = findByAttractionsId(
			attractionsId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace[] findByAttractionsId_PrevAndNext(
			long bookspaceId, long attractionsId,
			OrderByComparator<BookSpace> orderByComparator)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = findByPrimaryKey(bookspaceId);

		Session session = null;

		try {
			session = openSession();

			BookSpace[] array = new BookSpaceImpl[3];

			array[0] = getByAttractionsId_PrevAndNext(
				session, bookSpace, attractionsId, orderByComparator, true);

			array[1] = bookSpace;

			array[2] = getByAttractionsId_PrevAndNext(
				session, bookSpace, attractionsId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected BookSpace getByAttractionsId_PrevAndNext(
		Session session, BookSpace bookSpace, long attractionsId,
		OrderByComparator<BookSpace> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_BOOKSPACE_WHERE);

		sb.append(_FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(BookSpaceModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(attractionsId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(bookSpace)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<BookSpace> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the book spaces where attractionsId = &#63; from the database.
	 *
	 * @param attractionsId the attractions ID
	 */
	@Override
	public void removeByAttractionsId(long attractionsId) {
		for (BookSpace bookSpace :
				findByAttractionsId(
					attractionsId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(bookSpace);
		}
	}

	/**
	 * Returns the number of book spaces where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the number of matching book spaces
	 */
	@Override
	public int countByAttractionsId(long attractionsId) {
		FinderPath finderPath = _finderPathCountByAttractionsId;

		Object[] finderArgs = new Object[] {attractionsId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_BOOKSPACE_WHERE);

			sb.append(_FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(attractionsId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2 =
		"bookSpace.attractionsId = ?";

	public BookSpacePersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(BookSpace.class);

		setModelImplClass(BookSpaceImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the book space in the entity cache if it is enabled.
	 *
	 * @param bookSpace the book space
	 */
	@Override
	public void cacheResult(BookSpace bookSpace) {
		entityCache.putResult(
			BookSpaceImpl.class, bookSpace.getPrimaryKey(), bookSpace);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the book spaces in the entity cache if it is enabled.
	 *
	 * @param bookSpaces the book spaces
	 */
	@Override
	public void cacheResult(List<BookSpace> bookSpaces) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (bookSpaces.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (BookSpace bookSpace : bookSpaces) {
			if (entityCache.getResult(
					BookSpaceImpl.class, bookSpace.getPrimaryKey()) == null) {

				cacheResult(bookSpace);
			}
		}
	}

	/**
	 * Clears the cache for all book spaces.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(BookSpaceImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the book space.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(BookSpace bookSpace) {
		entityCache.removeResult(BookSpaceImpl.class, bookSpace);
	}

	@Override
	public void clearCache(List<BookSpace> bookSpaces) {
		for (BookSpace bookSpace : bookSpaces) {
			entityCache.removeResult(BookSpaceImpl.class, bookSpace);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(BookSpaceImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new book space with the primary key. Does not add the book space to the database.
	 *
	 * @param bookspaceId the primary key for the new book space
	 * @return the new book space
	 */
	@Override
	public BookSpace create(long bookspaceId) {
		BookSpace bookSpace = new BookSpaceImpl();

		bookSpace.setNew(true);
		bookSpace.setPrimaryKey(bookspaceId);

		String uuid = PortalUUIDUtil.generate();

		bookSpace.setUuid(uuid);

		bookSpace.setCompanyId(CompanyThreadLocal.getCompanyId());

		return bookSpace;
	}

	/**
	 * Removes the book space with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space that was removed
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace remove(long bookspaceId) throws NoSuchBookSpaceException {
		return remove((Serializable)bookspaceId);
	}

	/**
	 * Removes the book space with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the book space
	 * @return the book space that was removed
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace remove(Serializable primaryKey)
		throws NoSuchBookSpaceException {

		Session session = null;

		try {
			session = openSession();

			BookSpace bookSpace = (BookSpace)session.get(
				BookSpaceImpl.class, primaryKey);

			if (bookSpace == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchBookSpaceException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(bookSpace);
		}
		catch (NoSuchBookSpaceException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected BookSpace removeImpl(BookSpace bookSpace) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(bookSpace)) {
				bookSpace = (BookSpace)session.get(
					BookSpaceImpl.class, bookSpace.getPrimaryKeyObj());
			}

			if (bookSpace != null) {
				session.delete(bookSpace);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (bookSpace != null) {
			clearCache(bookSpace);
		}

		return bookSpace;
	}

	@Override
	public BookSpace updateImpl(BookSpace bookSpace) {
		boolean isNew = bookSpace.isNew();

		if (!(bookSpace instanceof BookSpaceModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(bookSpace.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(bookSpace);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in bookSpace proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom BookSpace implementation " +
					bookSpace.getClass());
		}

		BookSpaceModelImpl bookSpaceModelImpl = (BookSpaceModelImpl)bookSpace;

		if (Validator.isNull(bookSpace.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			bookSpace.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (bookSpace.getCreateDate() == null)) {
			if (serviceContext == null) {
				bookSpace.setCreateDate(date);
			}
			else {
				bookSpace.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!bookSpaceModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				bookSpace.setModifiedDate(date);
			}
			else {
				bookSpace.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(bookSpace);
			}
			else {
				bookSpace = (BookSpace)session.merge(bookSpace);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			BookSpaceImpl.class, bookSpaceModelImpl, false, true);

		if (isNew) {
			bookSpace.setNew(false);
		}

		bookSpace.resetOriginalValues();

		return bookSpace;
	}

	/**
	 * Returns the book space with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the book space
	 * @return the book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace findByPrimaryKey(Serializable primaryKey)
		throws NoSuchBookSpaceException {

		BookSpace bookSpace = fetchByPrimaryKey(primaryKey);

		if (bookSpace == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchBookSpaceException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return bookSpace;
	}

	/**
	 * Returns the book space with the primary key or throws a <code>NoSuchBookSpaceException</code> if it could not be found.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace findByPrimaryKey(long bookspaceId)
		throws NoSuchBookSpaceException {

		return findByPrimaryKey((Serializable)bookspaceId);
	}

	/**
	 * Returns the book space with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space, or <code>null</code> if a book space with the primary key could not be found
	 */
	@Override
	public BookSpace fetchByPrimaryKey(long bookspaceId) {
		return fetchByPrimaryKey((Serializable)bookspaceId);
	}

	/**
	 * Returns all the book spaces.
	 *
	 * @return the book spaces
	 */
	@Override
	public List<BookSpace> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of book spaces
	 */
	@Override
	public List<BookSpace> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of book spaces
	 */
	@Override
	public List<BookSpace> findAll(
		int start, int end, OrderByComparator<BookSpace> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of book spaces
	 */
	@Override
	public List<BookSpace> findAll(
		int start, int end, OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<BookSpace> list = null;

		if (useFinderCache) {
			list = (List<BookSpace>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_BOOKSPACE);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_BOOKSPACE;

				sql = sql.concat(BookSpaceModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<BookSpace>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the book spaces from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (BookSpace bookSpace : findAll()) {
			remove(bookSpace);
		}
	}

	/**
	 * Returns the number of book spaces.
	 *
	 * @return the number of book spaces
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_BOOKSPACE);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "bookspaceId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_BOOKSPACE;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return BookSpaceModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the book space persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class, new BookSpaceModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", BookSpace.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByAttractionsId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAttractionsId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"attractionsId"}, true);

		_finderPathWithoutPaginationFindByAttractionsId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAttractionsId",
			new String[] {Long.class.getName()}, new String[] {"attractionsId"},
			true);

		_finderPathCountByAttractionsId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAttractionsId",
			new String[] {Long.class.getName()}, new String[] {"attractionsId"},
			false);

		_setBookSpaceUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setBookSpaceUtilPersistence(null);

		entityCache.removeCache(BookSpaceImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setBookSpaceUtilPersistence(
		BookSpacePersistence bookSpacePersistence) {

		try {
			Field field = BookSpaceUtil.class.getDeclaredField("_persistence");

			field.setAccessible(true);

			field.set(null, bookSpacePersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = ExplorePersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = ExplorePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = ExplorePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_BOOKSPACE =
		"SELECT bookSpace FROM BookSpace bookSpace";

	private static final String _SQL_SELECT_BOOKSPACE_WHERE =
		"SELECT bookSpace FROM BookSpace bookSpace WHERE ";

	private static final String _SQL_COUNT_BOOKSPACE =
		"SELECT COUNT(bookSpace) FROM BookSpace bookSpace";

	private static final String _SQL_COUNT_BOOKSPACE_WHERE =
		"SELECT COUNT(bookSpace) FROM BookSpace bookSpace WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "bookSpace.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No BookSpace exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No BookSpace exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		BookSpacePersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class BookSpaceModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			BookSpaceModelImpl bookSpaceModelImpl =
				(BookSpaceModelImpl)baseModel;

			long columnBitmask = bookSpaceModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(bookSpaceModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						bookSpaceModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(BookSpacePersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(bookSpaceModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			BookSpaceModelImpl bookSpaceModelImpl, String[] columnNames,
			boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] = bookSpaceModelImpl.getColumnOriginalValue(
						columnName);
				}
				else {
					arguments[i] = bookSpaceModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= BookSpaceModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}