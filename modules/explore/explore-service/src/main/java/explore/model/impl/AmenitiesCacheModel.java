/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import explore.model.Amenities;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Amenities in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AmenitiesCacheModel
	implements CacheModel<Amenities>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AmenitiesCacheModel)) {
			return false;
		}

		AmenitiesCacheModel amenitiesCacheModel = (AmenitiesCacheModel)object;

		if (amenitiesId == amenitiesCacheModel.amenitiesId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, amenitiesId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", amenitiesId=");
		sb.append(amenitiesId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", attractionsId=");
		sb.append(attractionsId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", image=");
		sb.append(image);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Amenities toEntityModel() {
		AmenitiesImpl amenitiesImpl = new AmenitiesImpl();

		if (uuid == null) {
			amenitiesImpl.setUuid("");
		}
		else {
			amenitiesImpl.setUuid(uuid);
		}

		amenitiesImpl.setAmenitiesId(amenitiesId);
		amenitiesImpl.setCompanyId(companyId);
		amenitiesImpl.setUserId(userId);

		if (userName == null) {
			amenitiesImpl.setUserName("");
		}
		else {
			amenitiesImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			amenitiesImpl.setCreateDate(null);
		}
		else {
			amenitiesImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			amenitiesImpl.setModifiedDate(null);
		}
		else {
			amenitiesImpl.setModifiedDate(new Date(modifiedDate));
		}

		amenitiesImpl.setAttractionsId(attractionsId);

		if (title == null) {
			amenitiesImpl.setTitle("");
		}
		else {
			amenitiesImpl.setTitle(title);
		}

		if (image == null) {
			amenitiesImpl.setImage("");
		}
		else {
			amenitiesImpl.setImage(image);
		}

		amenitiesImpl.resetOriginalValues();

		return amenitiesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		amenitiesId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		attractionsId = objectInput.readLong();
		title = objectInput.readUTF();
		image = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(amenitiesId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(attractionsId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}
	}

	public String uuid;
	public long amenitiesId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long attractionsId;
	public String title;
	public String image;

}