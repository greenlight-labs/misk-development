/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import explore.model.Attractions;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Attractions in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AttractionsCacheModel
	implements CacheModel<Attractions>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AttractionsCacheModel)) {
			return false;
		}

		AttractionsCacheModel attractionsCacheModel =
			(AttractionsCacheModel)object;

		if (attractionsId == attractionsCacheModel.attractionsId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, attractionsId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(33);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", attractionsId=");
		sb.append(attractionsId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", headerArticleId=");
		sb.append(headerArticleId);
		sb.append(", locationLabel=");
		sb.append(locationLabel);
		sb.append(", locationLatitude=");
		sb.append(locationLatitude);
		sb.append(", locationLongitude=");
		sb.append(locationLongitude);
		sb.append(", amenitiesLabel=");
		sb.append(amenitiesLabel);
		sb.append(", bookSpaceLabel=");
		sb.append(bookSpaceLabel);
		sb.append(", gallaryLabel=");
		sb.append(gallaryLabel);
		sb.append(", eventId=");
		sb.append(eventId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Attractions toEntityModel() {
		AttractionsImpl attractionsImpl = new AttractionsImpl();

		if (uuid == null) {
			attractionsImpl.setUuid("");
		}
		else {
			attractionsImpl.setUuid(uuid);
		}

		attractionsImpl.setAttractionsId(attractionsId);
		attractionsImpl.setGroupId(groupId);
		attractionsImpl.setCompanyId(companyId);
		attractionsImpl.setUserId(userId);

		if (userName == null) {
			attractionsImpl.setUserName("");
		}
		else {
			attractionsImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			attractionsImpl.setCreateDate(null);
		}
		else {
			attractionsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			attractionsImpl.setModifiedDate(null);
		}
		else {
			attractionsImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (headerArticleId == null) {
			attractionsImpl.setHeaderArticleId("");
		}
		else {
			attractionsImpl.setHeaderArticleId(headerArticleId);
		}

		if (locationLabel == null) {
			attractionsImpl.setLocationLabel("");
		}
		else {
			attractionsImpl.setLocationLabel(locationLabel);
		}

		if (locationLatitude == null) {
			attractionsImpl.setLocationLatitude("");
		}
		else {
			attractionsImpl.setLocationLatitude(locationLatitude);
		}

		if (locationLongitude == null) {
			attractionsImpl.setLocationLongitude("");
		}
		else {
			attractionsImpl.setLocationLongitude(locationLongitude);
		}

		if (amenitiesLabel == null) {
			attractionsImpl.setAmenitiesLabel("");
		}
		else {
			attractionsImpl.setAmenitiesLabel(amenitiesLabel);
		}

		if (bookSpaceLabel == null) {
			attractionsImpl.setBookSpaceLabel("");
		}
		else {
			attractionsImpl.setBookSpaceLabel(bookSpaceLabel);
		}

		if (gallaryLabel == null) {
			attractionsImpl.setGallaryLabel("");
		}
		else {
			attractionsImpl.setGallaryLabel(gallaryLabel);
		}

		if (eventId == null) {
			attractionsImpl.setEventId("");
		}
		else {
			attractionsImpl.setEventId(eventId);
		}

		attractionsImpl.resetOriginalValues();

		return attractionsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		attractionsId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		headerArticleId = objectInput.readUTF();
		locationLabel = objectInput.readUTF();
		locationLatitude = objectInput.readUTF();
		locationLongitude = objectInput.readUTF();
		amenitiesLabel = objectInput.readUTF();
		bookSpaceLabel = objectInput.readUTF();
		gallaryLabel = objectInput.readUTF();
		eventId = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(attractionsId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (headerArticleId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(headerArticleId);
		}

		if (locationLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(locationLabel);
		}

		if (locationLatitude == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(locationLatitude);
		}

		if (locationLongitude == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(locationLongitude);
		}

		if (amenitiesLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(amenitiesLabel);
		}

		if (bookSpaceLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bookSpaceLabel);
		}

		if (gallaryLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(gallaryLabel);
		}

		if (eventId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(eventId);
		}
	}

	public String uuid;
	public long attractionsId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String headerArticleId;
	public String locationLabel;
	public String locationLatitude;
	public String locationLongitude;
	public String amenitiesLabel;
	public String bookSpaceLabel;
	public String gallaryLabel;
	public String eventId;

}