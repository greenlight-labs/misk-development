/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import explore.model.Gallary;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Gallary in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class GallaryCacheModel implements CacheModel<Gallary>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GallaryCacheModel)) {
			return false;
		}

		GallaryCacheModel gallaryCacheModel = (GallaryCacheModel)object;

		if (gallaryId == gallaryCacheModel.gallaryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, gallaryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", gallaryId=");
		sb.append(gallaryId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", attractionsId=");
		sb.append(attractionsId);
		sb.append(", image=");
		sb.append(image);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Gallary toEntityModel() {
		GallaryImpl gallaryImpl = new GallaryImpl();

		if (uuid == null) {
			gallaryImpl.setUuid("");
		}
		else {
			gallaryImpl.setUuid(uuid);
		}

		gallaryImpl.setGallaryId(gallaryId);
		gallaryImpl.setCompanyId(companyId);
		gallaryImpl.setUserId(userId);

		if (userName == null) {
			gallaryImpl.setUserName("");
		}
		else {
			gallaryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			gallaryImpl.setCreateDate(null);
		}
		else {
			gallaryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			gallaryImpl.setModifiedDate(null);
		}
		else {
			gallaryImpl.setModifiedDate(new Date(modifiedDate));
		}

		gallaryImpl.setAttractionsId(attractionsId);

		if (image == null) {
			gallaryImpl.setImage("");
		}
		else {
			gallaryImpl.setImage(image);
		}

		gallaryImpl.resetOriginalValues();

		return gallaryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		gallaryId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		attractionsId = objectInput.readLong();
		image = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(gallaryId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(attractionsId);

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}
	}

	public String uuid;
	public long gallaryId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long attractionsId;
	public String image;

}