create table misk_events (
	uuid_ VARCHAR(75) null,
	eventId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	eventStartDateTime DATE null,
	eventEndDateTime DATE null,
	tagId LONG,
	categoryId LONG,
	orderNo LONG,
	type_ INTEGER,
	paytype INTEGER,
	title STRING null,
	smallImage STRING null,
	bigImage STRING null,
	displayDate DATE null,
	venue STRING null,
	timing STRING null,
	duration STRING null,
	descriptionField1 TEXT null,
	videoLink STRING null,
	descriptionField2 TEXT null,
	subtitle STRING null,
	descriptionField3 TEXT null,
	detailImage STRING null,
	quoteText STRING null,
	quoteAuthor STRING null,
	descriptionField4 TEXT null,
	eventdescription TEXT null,
	eventfeaturedImage STRING null,
	buyTicketLink STRING null,
	lat VARCHAR(75) null,
	lan VARCHAR(75) null,
	eventPrice VARCHAR(75) null,
	currency_ VARCHAR(75) null,
	email VARCHAR(75) null,
	phone VARCHAR(75) null
);

create table misk_events_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug VARCHAR(75) null,
	catColor VARCHAR(75) null
);

create table misk_events_galleries (
	uuid_ VARCHAR(75) null,
	slideId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	eventId LONG,
	image STRING null,
	thumbnail STRING null
);

create table misk_events_pages_tags (
	uuid_ VARCHAR(75) null,
	pageId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	pageName VARCHAR(75) null,
	pageSlug VARCHAR(75) null,
	tagId LONG
);

create table misk_events_schedule (
	uuid_ VARCHAR(75) null,
	scheduleId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	eventId LONG,
	title STRING null,
	description STRING null,
	duration STRING null,
	image VARCHAR(75) null
);

create table misk_events_tags (
	uuid_ VARCHAR(75) null,
	tagId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug VARCHAR(75) null
);

create table misk_events_visitors (
	uuid_ VARCHAR(75) null,
	visitor_id LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	eventvisitorId LONG,
	createDate DATE null,
	modifiedDate DATE null
);