create index IX_7350C977 on misk_events (categoryId);
create index IX_ACAB2E60 on misk_events (groupId);
create index IX_78675F35 on misk_events (paytype, categoryId);
create index IX_48494EFB on misk_events (tagId);
create index IX_56B7323E on misk_events (title[$COLUMN_LENGTH:255$]);
create index IX_71C9190B on misk_events (type_);
create index IX_4EEB54FE on misk_events (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_2CA10300 on misk_events (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_229F74CD on misk_events_categories (groupId);
create index IX_63AAF931 on misk_events_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_A95217F3 on misk_events_categories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_3341C36A on misk_events_galleries (eventId);
create index IX_D8FDCCCF on misk_events_galleries (groupId);
create index IX_1E82836F on misk_events_galleries (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E46319B1 on misk_events_galleries (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_85641435 on misk_events_pages_tags (groupId);
create unique index IX_28344023 on misk_events_pages_tags (pageSlug[$COLUMN_LENGTH:75$]);
create index IX_B87F2EC9 on misk_events_pages_tags (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_7066D38B on misk_events_pages_tags (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_246F814D on misk_events_schedule (eventId);
create index IX_CA2B8AB2 on misk_events_schedule (groupId);
create index IX_598B4EC on misk_events_schedule (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_1161866E on misk_events_schedule (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_36E70E70 on misk_events_tags (groupId);
create unique index IX_67DD63D9 on misk_events_tags (slug[$COLUMN_LENGTH:75$]);
create index IX_9EA286EE on misk_events_tags (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_3F5BF0F0 on misk_events_tags (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_A0762729 on misk_events_visitors (eventvisitorId);
create index IX_21771AC4 on misk_events_visitors (groupId);
create index IX_1D5186E0 on misk_events_visitors (userId);
create index IX_2F79191A on misk_events_visitors (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_F6C91E1C on misk_events_visitors (uuid_[$COLUMN_LENGTH:75$], groupId);