/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import events.model.Schedule;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Schedule in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ScheduleCacheModel
	implements CacheModel<Schedule>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ScheduleCacheModel)) {
			return false;
		}

		ScheduleCacheModel scheduleCacheModel = (ScheduleCacheModel)object;

		if (scheduleId == scheduleCacheModel.scheduleId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, scheduleId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", scheduleId=");
		sb.append(scheduleId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", eventId=");
		sb.append(eventId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", duration=");
		sb.append(duration);
		sb.append(", image=");
		sb.append(image);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Schedule toEntityModel() {
		ScheduleImpl scheduleImpl = new ScheduleImpl();

		if (uuid == null) {
			scheduleImpl.setUuid("");
		}
		else {
			scheduleImpl.setUuid(uuid);
		}

		scheduleImpl.setScheduleId(scheduleId);
		scheduleImpl.setGroupId(groupId);
		scheduleImpl.setCompanyId(companyId);
		scheduleImpl.setUserId(userId);

		if (userName == null) {
			scheduleImpl.setUserName("");
		}
		else {
			scheduleImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			scheduleImpl.setCreateDate(null);
		}
		else {
			scheduleImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			scheduleImpl.setModifiedDate(null);
		}
		else {
			scheduleImpl.setModifiedDate(new Date(modifiedDate));
		}

		scheduleImpl.setEventId(eventId);

		if (title == null) {
			scheduleImpl.setTitle("");
		}
		else {
			scheduleImpl.setTitle(title);
		}

		if (description == null) {
			scheduleImpl.setDescription("");
		}
		else {
			scheduleImpl.setDescription(description);
		}

		if (duration == null) {
			scheduleImpl.setDuration("");
		}
		else {
			scheduleImpl.setDuration(duration);
		}

		if (image == null) {
			scheduleImpl.setImage("");
		}
		else {
			scheduleImpl.setImage(image);
		}

		scheduleImpl.resetOriginalValues();

		return scheduleImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		scheduleId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		eventId = objectInput.readLong();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		duration = objectInput.readUTF();
		image = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(scheduleId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(eventId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (duration == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(duration);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}
	}

	public String uuid;
	public long scheduleId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long eventId;
	public String title;
	public String description;
	public String duration;
	public String image;

}