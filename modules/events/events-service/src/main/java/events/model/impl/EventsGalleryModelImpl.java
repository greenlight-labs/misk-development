/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model.impl;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.LocaleException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import events.model.EventsGallery;
import events.model.EventsGalleryModel;
import events.model.EventsGallerySoap;

import java.io.Serializable;

import java.lang.reflect.InvocationHandler;

import java.sql.Blob;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * The base model implementation for the EventsGallery service. Represents a row in the &quot;misk_events_galleries&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface <code>EventsGalleryModel</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link EventsGalleryImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventsGalleryImpl
 * @generated
 */
@JSON(strict = true)
public class EventsGalleryModelImpl
	extends BaseModelImpl<EventsGallery> implements EventsGalleryModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a events gallery model instance should use the <code>EventsGallery</code> interface instead.
	 */
	public static final String TABLE_NAME = "misk_events_galleries";

	public static final Object[][] TABLE_COLUMNS = {
		{"uuid_", Types.VARCHAR}, {"slideId", Types.BIGINT},
		{"groupId", Types.BIGINT}, {"companyId", Types.BIGINT},
		{"userId", Types.BIGINT}, {"userName", Types.VARCHAR},
		{"createDate", Types.TIMESTAMP}, {"modifiedDate", Types.TIMESTAMP},
		{"eventId", Types.BIGINT}, {"image", Types.VARCHAR},
		{"thumbnail", Types.VARCHAR}
	};

	public static final Map<String, Integer> TABLE_COLUMNS_MAP =
		new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("uuid_", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("slideId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("groupId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("companyId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("userName", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("createDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("modifiedDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("eventId", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("image", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("thumbnail", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE =
		"create table misk_events_galleries (uuid_ VARCHAR(75) null,slideId LONG not null primary key,groupId LONG,companyId LONG,userId LONG,userName VARCHAR(75) null,createDate DATE null,modifiedDate DATE null,eventId LONG,image STRING null,thumbnail STRING null)";

	public static final String TABLE_SQL_DROP =
		"drop table misk_events_galleries";

	public static final String ORDER_BY_JPQL =
		" ORDER BY eventsGallery.createDate ASC";

	public static final String ORDER_BY_SQL =
		" ORDER BY misk_events_galleries.createDate ASC";

	public static final String DATA_SOURCE = "liferayDataSource";

	public static final String SESSION_FACTORY = "liferaySessionFactory";

	public static final String TX_MANAGER = "liferayTransactionManager";

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long COMPANYID_COLUMN_BITMASK = 1L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long EVENTID_COLUMN_BITMASK = 2L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long GROUPID_COLUMN_BITMASK = 4L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link #getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long UUID_COLUMN_BITMASK = 8L;

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *		#getColumnBitmask(String)}
	 */
	@Deprecated
	public static final long CREATEDATE_COLUMN_BITMASK = 16L;

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setEntityCacheEnabled(boolean entityCacheEnabled) {
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static void setFinderCacheEnabled(boolean finderCacheEnabled) {
	}

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static EventsGallery toModel(EventsGallerySoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		EventsGallery model = new EventsGalleryImpl();

		model.setUuid(soapModel.getUuid());
		model.setSlideId(soapModel.getSlideId());
		model.setGroupId(soapModel.getGroupId());
		model.setCompanyId(soapModel.getCompanyId());
		model.setUserId(soapModel.getUserId());
		model.setUserName(soapModel.getUserName());
		model.setCreateDate(soapModel.getCreateDate());
		model.setModifiedDate(soapModel.getModifiedDate());
		model.setEventId(soapModel.getEventId());
		model.setImage(soapModel.getImage());
		model.setThumbnail(soapModel.getThumbnail());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	public static List<EventsGallery> toModels(EventsGallerySoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<EventsGallery> models = new ArrayList<EventsGallery>(
			soapModels.length);

		for (EventsGallerySoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public EventsGalleryModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _slideId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setSlideId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _slideId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return EventsGallery.class;
	}

	@Override
	public String getModelClassName() {
		return EventsGallery.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		Map<String, Function<EventsGallery, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		for (Map.Entry<String, Function<EventsGallery, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<EventsGallery, Object> attributeGetterFunction =
				entry.getValue();

			attributes.put(
				attributeName,
				attributeGetterFunction.apply((EventsGallery)this));
		}

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Map<String, BiConsumer<EventsGallery, Object>>
			attributeSetterBiConsumers = getAttributeSetterBiConsumers();

		for (Map.Entry<String, Object> entry : attributes.entrySet()) {
			String attributeName = entry.getKey();

			BiConsumer<EventsGallery, Object> attributeSetterBiConsumer =
				attributeSetterBiConsumers.get(attributeName);

			if (attributeSetterBiConsumer != null) {
				attributeSetterBiConsumer.accept(
					(EventsGallery)this, entry.getValue());
			}
		}
	}

	public Map<String, Function<EventsGallery, Object>>
		getAttributeGetterFunctions() {

		return _attributeGetterFunctions;
	}

	public Map<String, BiConsumer<EventsGallery, Object>>
		getAttributeSetterBiConsumers() {

		return _attributeSetterBiConsumers;
	}

	private static final Map<String, Function<EventsGallery, Object>>
		_attributeGetterFunctions;
	private static final Map<String, BiConsumer<EventsGallery, Object>>
		_attributeSetterBiConsumers;

	static {
		Map<String, Function<EventsGallery, Object>> attributeGetterFunctions =
			new LinkedHashMap<String, Function<EventsGallery, Object>>();
		Map<String, BiConsumer<EventsGallery, ?>> attributeSetterBiConsumers =
			new LinkedHashMap<String, BiConsumer<EventsGallery, ?>>();

		attributeGetterFunctions.put("uuid", EventsGallery::getUuid);
		attributeSetterBiConsumers.put(
			"uuid", (BiConsumer<EventsGallery, String>)EventsGallery::setUuid);
		attributeGetterFunctions.put("slideId", EventsGallery::getSlideId);
		attributeSetterBiConsumers.put(
			"slideId",
			(BiConsumer<EventsGallery, Long>)EventsGallery::setSlideId);
		attributeGetterFunctions.put("groupId", EventsGallery::getGroupId);
		attributeSetterBiConsumers.put(
			"groupId",
			(BiConsumer<EventsGallery, Long>)EventsGallery::setGroupId);
		attributeGetterFunctions.put("companyId", EventsGallery::getCompanyId);
		attributeSetterBiConsumers.put(
			"companyId",
			(BiConsumer<EventsGallery, Long>)EventsGallery::setCompanyId);
		attributeGetterFunctions.put("userId", EventsGallery::getUserId);
		attributeSetterBiConsumers.put(
			"userId",
			(BiConsumer<EventsGallery, Long>)EventsGallery::setUserId);
		attributeGetterFunctions.put("userName", EventsGallery::getUserName);
		attributeSetterBiConsumers.put(
			"userName",
			(BiConsumer<EventsGallery, String>)EventsGallery::setUserName);
		attributeGetterFunctions.put(
			"createDate", EventsGallery::getCreateDate);
		attributeSetterBiConsumers.put(
			"createDate",
			(BiConsumer<EventsGallery, Date>)EventsGallery::setCreateDate);
		attributeGetterFunctions.put(
			"modifiedDate", EventsGallery::getModifiedDate);
		attributeSetterBiConsumers.put(
			"modifiedDate",
			(BiConsumer<EventsGallery, Date>)EventsGallery::setModifiedDate);
		attributeGetterFunctions.put("eventId", EventsGallery::getEventId);
		attributeSetterBiConsumers.put(
			"eventId",
			(BiConsumer<EventsGallery, Long>)EventsGallery::setEventId);
		attributeGetterFunctions.put("image", EventsGallery::getImage);
		attributeSetterBiConsumers.put(
			"image",
			(BiConsumer<EventsGallery, String>)EventsGallery::setImage);
		attributeGetterFunctions.put("thumbnail", EventsGallery::getThumbnail);
		attributeSetterBiConsumers.put(
			"thumbnail",
			(BiConsumer<EventsGallery, String>)EventsGallery::setThumbnail);

		_attributeGetterFunctions = Collections.unmodifiableMap(
			attributeGetterFunctions);
		_attributeSetterBiConsumers = Collections.unmodifiableMap(
			(Map)attributeSetterBiConsumers);
	}

	@JSON
	@Override
	public String getUuid() {
		if (_uuid == null) {
			return "";
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_uuid = uuid;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public String getOriginalUuid() {
		return getColumnOriginalValue("uuid_");
	}

	@JSON
	@Override
	public long getSlideId() {
		return _slideId;
	}

	@Override
	public void setSlideId(long slideId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_slideId = slideId;
	}

	@JSON
	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_groupId = groupId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalGroupId() {
		return GetterUtil.getLong(this.<Long>getColumnOriginalValue("groupId"));
	}

	@JSON
	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_companyId = companyId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalCompanyId() {
		return GetterUtil.getLong(
			this.<Long>getColumnOriginalValue("companyId"));
	}

	@JSON
	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() {
		try {
			User user = UserLocalServiceUtil.getUserById(getUserId());

			return user.getUuid();
		}
		catch (PortalException portalException) {
			return "";
		}
	}

	@Override
	public void setUserUuid(String userUuid) {
	}

	@JSON
	@Override
	public String getUserName() {
		if (_userName == null) {
			return "";
		}
		else {
			return _userName;
		}
	}

	@Override
	public void setUserName(String userName) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_userName = userName;
	}

	@JSON
	@Override
	public Date getCreateDate() {
		return _createDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_createDate = createDate;
	}

	@JSON
	@Override
	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public boolean hasSetModifiedDate() {
		return _setModifiedDate;
	}

	@Override
	public void setModifiedDate(Date modifiedDate) {
		_setModifiedDate = true;

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_modifiedDate = modifiedDate;
	}

	@JSON
	@Override
	public long getEventId() {
		return _eventId;
	}

	@Override
	public void setEventId(long eventId) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_eventId = eventId;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), replaced by {@link
	 *             #getColumnOriginalValue(String)}
	 */
	@Deprecated
	public long getOriginalEventId() {
		return GetterUtil.getLong(this.<Long>getColumnOriginalValue("eventId"));
	}

	@JSON
	@Override
	public String getImage() {
		if (_image == null) {
			return "";
		}
		else {
			return _image;
		}
	}

	@Override
	public String getImage(Locale locale) {
		String languageId = LocaleUtil.toLanguageId(locale);

		return getImage(languageId);
	}

	@Override
	public String getImage(Locale locale, boolean useDefault) {
		String languageId = LocaleUtil.toLanguageId(locale);

		return getImage(languageId, useDefault);
	}

	@Override
	public String getImage(String languageId) {
		return LocalizationUtil.getLocalization(getImage(), languageId);
	}

	@Override
	public String getImage(String languageId, boolean useDefault) {
		return LocalizationUtil.getLocalization(
			getImage(), languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return _imageCurrentLanguageId;
	}

	@JSON
	@Override
	public String getImageCurrentValue() {
		Locale locale = getLocale(_imageCurrentLanguageId);

		return getImage(locale);
	}

	@Override
	public Map<Locale, String> getImageMap() {
		return LocalizationUtil.getLocalizationMap(getImage());
	}

	@Override
	public void setImage(String image) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_image = image;
	}

	@Override
	public void setImage(String image, Locale locale) {
		setImage(image, locale, LocaleUtil.getSiteDefault());
	}

	@Override
	public void setImage(String image, Locale locale, Locale defaultLocale) {
		String languageId = LocaleUtil.toLanguageId(locale);
		String defaultLanguageId = LocaleUtil.toLanguageId(defaultLocale);

		if (Validator.isNotNull(image)) {
			setImage(
				LocalizationUtil.updateLocalization(
					getImage(), "Image", image, languageId, defaultLanguageId));
		}
		else {
			setImage(
				LocalizationUtil.removeLocalization(
					getImage(), "Image", languageId));
		}
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		_imageCurrentLanguageId = languageId;
	}

	@Override
	public void setImageMap(Map<Locale, String> imageMap) {
		setImageMap(imageMap, LocaleUtil.getSiteDefault());
	}

	@Override
	public void setImageMap(
		Map<Locale, String> imageMap, Locale defaultLocale) {

		if (imageMap == null) {
			return;
		}

		setImage(
			LocalizationUtil.updateLocalization(
				imageMap, getImage(), "Image",
				LocaleUtil.toLanguageId(defaultLocale)));
	}

	@JSON
	@Override
	public String getThumbnail() {
		if (_thumbnail == null) {
			return "";
		}
		else {
			return _thumbnail;
		}
	}

	@Override
	public String getThumbnail(Locale locale) {
		String languageId = LocaleUtil.toLanguageId(locale);

		return getThumbnail(languageId);
	}

	@Override
	public String getThumbnail(Locale locale, boolean useDefault) {
		String languageId = LocaleUtil.toLanguageId(locale);

		return getThumbnail(languageId, useDefault);
	}

	@Override
	public String getThumbnail(String languageId) {
		return LocalizationUtil.getLocalization(getThumbnail(), languageId);
	}

	@Override
	public String getThumbnail(String languageId, boolean useDefault) {
		return LocalizationUtil.getLocalization(
			getThumbnail(), languageId, useDefault);
	}

	@Override
	public String getThumbnailCurrentLanguageId() {
		return _thumbnailCurrentLanguageId;
	}

	@JSON
	@Override
	public String getThumbnailCurrentValue() {
		Locale locale = getLocale(_thumbnailCurrentLanguageId);

		return getThumbnail(locale);
	}

	@Override
	public Map<Locale, String> getThumbnailMap() {
		return LocalizationUtil.getLocalizationMap(getThumbnail());
	}

	@Override
	public void setThumbnail(String thumbnail) {
		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		_thumbnail = thumbnail;
	}

	@Override
	public void setThumbnail(String thumbnail, Locale locale) {
		setThumbnail(thumbnail, locale, LocaleUtil.getSiteDefault());
	}

	@Override
	public void setThumbnail(
		String thumbnail, Locale locale, Locale defaultLocale) {

		String languageId = LocaleUtil.toLanguageId(locale);
		String defaultLanguageId = LocaleUtil.toLanguageId(defaultLocale);

		if (Validator.isNotNull(thumbnail)) {
			setThumbnail(
				LocalizationUtil.updateLocalization(
					getThumbnail(), "Thumbnail", thumbnail, languageId,
					defaultLanguageId));
		}
		else {
			setThumbnail(
				LocalizationUtil.removeLocalization(
					getThumbnail(), "Thumbnail", languageId));
		}
	}

	@Override
	public void setThumbnailCurrentLanguageId(String languageId) {
		_thumbnailCurrentLanguageId = languageId;
	}

	@Override
	public void setThumbnailMap(Map<Locale, String> thumbnailMap) {
		setThumbnailMap(thumbnailMap, LocaleUtil.getSiteDefault());
	}

	@Override
	public void setThumbnailMap(
		Map<Locale, String> thumbnailMap, Locale defaultLocale) {

		if (thumbnailMap == null) {
			return;
		}

		setThumbnail(
			LocalizationUtil.updateLocalization(
				thumbnailMap, getThumbnail(), "Thumbnail",
				LocaleUtil.toLanguageId(defaultLocale)));
	}

	@Override
	public StagedModelType getStagedModelType() {
		return new StagedModelType(
			PortalUtil.getClassNameId(EventsGallery.class.getName()));
	}

	public long getColumnBitmask() {
		if (_columnBitmask > 0) {
			return _columnBitmask;
		}

		if ((_columnOriginalValues == null) ||
			(_columnOriginalValues == Collections.EMPTY_MAP)) {

			return 0;
		}

		for (Map.Entry<String, Object> entry :
				_columnOriginalValues.entrySet()) {

			if (!Objects.equals(
					entry.getValue(), getColumnValue(entry.getKey()))) {

				_columnBitmask |= _columnBitmasks.get(entry.getKey());
			}
		}

		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(
			getCompanyId(), EventsGallery.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public String[] getAvailableLanguageIds() {
		Set<String> availableLanguageIds = new TreeSet<String>();

		Map<Locale, String> imageMap = getImageMap();

		for (Map.Entry<Locale, String> entry : imageMap.entrySet()) {
			Locale locale = entry.getKey();
			String value = entry.getValue();

			if (Validator.isNotNull(value)) {
				availableLanguageIds.add(LocaleUtil.toLanguageId(locale));
			}
		}

		Map<Locale, String> thumbnailMap = getThumbnailMap();

		for (Map.Entry<Locale, String> entry : thumbnailMap.entrySet()) {
			Locale locale = entry.getKey();
			String value = entry.getValue();

			if (Validator.isNotNull(value)) {
				availableLanguageIds.add(LocaleUtil.toLanguageId(locale));
			}
		}

		return availableLanguageIds.toArray(
			new String[availableLanguageIds.size()]);
	}

	@Override
	public String getDefaultLanguageId() {
		String xml = getImage();

		if (xml == null) {
			return "";
		}

		Locale defaultLocale = LocaleUtil.getSiteDefault();

		return LocalizationUtil.getDefaultLanguageId(xml, defaultLocale);
	}

	@Override
	public void prepareLocalizedFieldsForImport() throws LocaleException {
		Locale defaultLocale = LocaleUtil.fromLanguageId(
			getDefaultLanguageId());

		Locale[] availableLocales = LocaleUtil.fromLanguageIds(
			getAvailableLanguageIds());

		Locale defaultImportLocale = LocalizationUtil.getDefaultImportLocale(
			EventsGallery.class.getName(), getPrimaryKey(), defaultLocale,
			availableLocales);

		prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	@Override
	@SuppressWarnings("unused")
	public void prepareLocalizedFieldsForImport(Locale defaultImportLocale)
		throws LocaleException {

		Locale defaultLocale = LocaleUtil.getSiteDefault();

		String modelDefaultLanguageId = getDefaultLanguageId();

		String image = getImage(defaultLocale);

		if (Validator.isNull(image)) {
			setImage(getImage(modelDefaultLanguageId), defaultLocale);
		}
		else {
			setImage(getImage(defaultLocale), defaultLocale, defaultLocale);
		}

		String thumbnail = getThumbnail(defaultLocale);

		if (Validator.isNull(thumbnail)) {
			setThumbnail(getThumbnail(modelDefaultLanguageId), defaultLocale);
		}
		else {
			setThumbnail(
				getThumbnail(defaultLocale), defaultLocale, defaultLocale);
		}
	}

	@Override
	public EventsGallery toEscapedModel() {
		if (_escapedModel == null) {
			Function<InvocationHandler, EventsGallery>
				escapedModelProxyProviderFunction =
					EscapedModelProxyProviderFunctionHolder.
						_escapedModelProxyProviderFunction;

			_escapedModel = escapedModelProxyProviderFunction.apply(
				new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		EventsGalleryImpl eventsGalleryImpl = new EventsGalleryImpl();

		eventsGalleryImpl.setUuid(getUuid());
		eventsGalleryImpl.setSlideId(getSlideId());
		eventsGalleryImpl.setGroupId(getGroupId());
		eventsGalleryImpl.setCompanyId(getCompanyId());
		eventsGalleryImpl.setUserId(getUserId());
		eventsGalleryImpl.setUserName(getUserName());
		eventsGalleryImpl.setCreateDate(getCreateDate());
		eventsGalleryImpl.setModifiedDate(getModifiedDate());
		eventsGalleryImpl.setEventId(getEventId());
		eventsGalleryImpl.setImage(getImage());
		eventsGalleryImpl.setThumbnail(getThumbnail());

		eventsGalleryImpl.resetOriginalValues();

		return eventsGalleryImpl;
	}

	@Override
	public int compareTo(EventsGallery eventsGallery) {
		int value = 0;

		value = DateUtil.compareTo(
			getCreateDate(), eventsGallery.getCreateDate());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EventsGallery)) {
			return false;
		}

		EventsGallery eventsGallery = (EventsGallery)object;

		long primaryKey = eventsGallery.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isEntityCacheEnabled() {
		return true;
	}

	/**
	 * @deprecated As of Athanasius (7.3.x), with no direct replacement
	 */
	@Deprecated
	@Override
	public boolean isFinderCacheEnabled() {
		return true;
	}

	@Override
	public void resetOriginalValues() {
		_columnOriginalValues = Collections.emptyMap();

		_setModifiedDate = false;

		_columnBitmask = 0;
	}

	@Override
	public CacheModel<EventsGallery> toCacheModel() {
		EventsGalleryCacheModel eventsGalleryCacheModel =
			new EventsGalleryCacheModel();

		eventsGalleryCacheModel.uuid = getUuid();

		String uuid = eventsGalleryCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			eventsGalleryCacheModel.uuid = null;
		}

		eventsGalleryCacheModel.slideId = getSlideId();

		eventsGalleryCacheModel.groupId = getGroupId();

		eventsGalleryCacheModel.companyId = getCompanyId();

		eventsGalleryCacheModel.userId = getUserId();

		eventsGalleryCacheModel.userName = getUserName();

		String userName = eventsGalleryCacheModel.userName;

		if ((userName != null) && (userName.length() == 0)) {
			eventsGalleryCacheModel.userName = null;
		}

		Date createDate = getCreateDate();

		if (createDate != null) {
			eventsGalleryCacheModel.createDate = createDate.getTime();
		}
		else {
			eventsGalleryCacheModel.createDate = Long.MIN_VALUE;
		}

		Date modifiedDate = getModifiedDate();

		if (modifiedDate != null) {
			eventsGalleryCacheModel.modifiedDate = modifiedDate.getTime();
		}
		else {
			eventsGalleryCacheModel.modifiedDate = Long.MIN_VALUE;
		}

		eventsGalleryCacheModel.eventId = getEventId();

		eventsGalleryCacheModel.image = getImage();

		String image = eventsGalleryCacheModel.image;

		if ((image != null) && (image.length() == 0)) {
			eventsGalleryCacheModel.image = null;
		}

		eventsGalleryCacheModel.thumbnail = getThumbnail();

		String thumbnail = eventsGalleryCacheModel.thumbnail;

		if ((thumbnail != null) && (thumbnail.length() == 0)) {
			eventsGalleryCacheModel.thumbnail = null;
		}

		return eventsGalleryCacheModel;
	}

	@Override
	public String toString() {
		Map<String, Function<EventsGallery, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			(5 * attributeGetterFunctions.size()) + 2);

		sb.append("{");

		for (Map.Entry<String, Function<EventsGallery, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<EventsGallery, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("\"");
			sb.append(attributeName);
			sb.append("\": ");

			Object value = attributeGetterFunction.apply((EventsGallery)this);

			if (value == null) {
				sb.append("null");
			}
			else if (value instanceof Blob || value instanceof Date ||
					 value instanceof Map || value instanceof String) {

				sb.append(
					"\"" + StringUtil.replace(value.toString(), "\"", "'") +
						"\"");
			}
			else {
				sb.append(value);
			}

			sb.append(", ");
		}

		if (sb.index() > 1) {
			sb.setIndex(sb.index() - 1);
		}

		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		Map<String, Function<EventsGallery, Object>> attributeGetterFunctions =
			getAttributeGetterFunctions();

		StringBundler sb = new StringBundler(
			(5 * attributeGetterFunctions.size()) + 4);

		sb.append("<model><model-name>");
		sb.append(getModelClassName());
		sb.append("</model-name>");

		for (Map.Entry<String, Function<EventsGallery, Object>> entry :
				attributeGetterFunctions.entrySet()) {

			String attributeName = entry.getKey();
			Function<EventsGallery, Object> attributeGetterFunction =
				entry.getValue();

			sb.append("<column><column-name>");
			sb.append(attributeName);
			sb.append("</column-name><column-value><![CDATA[");
			sb.append(attributeGetterFunction.apply((EventsGallery)this));
			sb.append("]]></column-value></column>");
		}

		sb.append("</model>");

		return sb.toString();
	}

	private static class EscapedModelProxyProviderFunctionHolder {

		private static final Function<InvocationHandler, EventsGallery>
			_escapedModelProxyProviderFunction =
				ProxyUtil.getProxyProviderFunction(
					EventsGallery.class, ModelWrapper.class);

	}

	private String _uuid;
	private long _slideId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private boolean _setModifiedDate;
	private long _eventId;
	private String _image;
	private String _imageCurrentLanguageId;
	private String _thumbnail;
	private String _thumbnailCurrentLanguageId;

	public <T> T getColumnValue(String columnName) {
		columnName = _attributeNames.getOrDefault(columnName, columnName);

		Function<EventsGallery, Object> function =
			_attributeGetterFunctions.get(columnName);

		if (function == null) {
			throw new IllegalArgumentException(
				"No attribute getter function found for " + columnName);
		}

		return (T)function.apply((EventsGallery)this);
	}

	public <T> T getColumnOriginalValue(String columnName) {
		if (_columnOriginalValues == null) {
			return null;
		}

		if (_columnOriginalValues == Collections.EMPTY_MAP) {
			_setColumnOriginalValues();
		}

		return (T)_columnOriginalValues.get(columnName);
	}

	private void _setColumnOriginalValues() {
		_columnOriginalValues = new HashMap<String, Object>();

		_columnOriginalValues.put("uuid_", _uuid);
		_columnOriginalValues.put("slideId", _slideId);
		_columnOriginalValues.put("groupId", _groupId);
		_columnOriginalValues.put("companyId", _companyId);
		_columnOriginalValues.put("userId", _userId);
		_columnOriginalValues.put("userName", _userName);
		_columnOriginalValues.put("createDate", _createDate);
		_columnOriginalValues.put("modifiedDate", _modifiedDate);
		_columnOriginalValues.put("eventId", _eventId);
		_columnOriginalValues.put("image", _image);
		_columnOriginalValues.put("thumbnail", _thumbnail);
	}

	private static final Map<String, String> _attributeNames;

	static {
		Map<String, String> attributeNames = new HashMap<>();

		attributeNames.put("uuid_", "uuid");

		_attributeNames = Collections.unmodifiableMap(attributeNames);
	}

	private transient Map<String, Object> _columnOriginalValues;

	public static long getColumnBitmask(String columnName) {
		return _columnBitmasks.get(columnName);
	}

	private static final Map<String, Long> _columnBitmasks;

	static {
		Map<String, Long> columnBitmasks = new HashMap<>();

		columnBitmasks.put("uuid_", 1L);

		columnBitmasks.put("slideId", 2L);

		columnBitmasks.put("groupId", 4L);

		columnBitmasks.put("companyId", 8L);

		columnBitmasks.put("userId", 16L);

		columnBitmasks.put("userName", 32L);

		columnBitmasks.put("createDate", 64L);

		columnBitmasks.put("modifiedDate", 128L);

		columnBitmasks.put("eventId", 256L);

		columnBitmasks.put("image", 512L);

		columnBitmasks.put("thumbnail", 1024L);

		_columnBitmasks = Collections.unmodifiableMap(columnBitmasks);
	}

	private long _columnBitmask;
	private EventsGallery _escapedModel;

}