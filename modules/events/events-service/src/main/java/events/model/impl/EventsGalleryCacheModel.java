/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import events.model.EventsGallery;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EventsGallery in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EventsGalleryCacheModel
	implements CacheModel<EventsGallery>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EventsGalleryCacheModel)) {
			return false;
		}

		EventsGalleryCacheModel eventsGalleryCacheModel =
			(EventsGalleryCacheModel)object;

		if (slideId == eventsGalleryCacheModel.slideId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, slideId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", slideId=");
		sb.append(slideId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", eventId=");
		sb.append(eventId);
		sb.append(", image=");
		sb.append(image);
		sb.append(", thumbnail=");
		sb.append(thumbnail);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EventsGallery toEntityModel() {
		EventsGalleryImpl eventsGalleryImpl = new EventsGalleryImpl();

		if (uuid == null) {
			eventsGalleryImpl.setUuid("");
		}
		else {
			eventsGalleryImpl.setUuid(uuid);
		}

		eventsGalleryImpl.setSlideId(slideId);
		eventsGalleryImpl.setGroupId(groupId);
		eventsGalleryImpl.setCompanyId(companyId);
		eventsGalleryImpl.setUserId(userId);

		if (userName == null) {
			eventsGalleryImpl.setUserName("");
		}
		else {
			eventsGalleryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			eventsGalleryImpl.setCreateDate(null);
		}
		else {
			eventsGalleryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			eventsGalleryImpl.setModifiedDate(null);
		}
		else {
			eventsGalleryImpl.setModifiedDate(new Date(modifiedDate));
		}

		eventsGalleryImpl.setEventId(eventId);

		if (image == null) {
			eventsGalleryImpl.setImage("");
		}
		else {
			eventsGalleryImpl.setImage(image);
		}

		if (thumbnail == null) {
			eventsGalleryImpl.setThumbnail("");
		}
		else {
			eventsGalleryImpl.setThumbnail(thumbnail);
		}

		eventsGalleryImpl.resetOriginalValues();

		return eventsGalleryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		slideId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		eventId = objectInput.readLong();
		image = objectInput.readUTF();
		thumbnail = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(slideId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(eventId);

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (thumbnail == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(thumbnail);
		}
	}

	public String uuid;
	public long slideId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long eventId;
	public String image;
	public String thumbnail;

}