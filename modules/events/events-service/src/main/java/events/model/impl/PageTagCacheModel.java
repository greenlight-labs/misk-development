/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import events.model.PageTag;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing PageTag in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class PageTagCacheModel implements CacheModel<PageTag>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof PageTagCacheModel)) {
			return false;
		}

		PageTagCacheModel pageTagCacheModel = (PageTagCacheModel)object;

		if (pageId == pageTagCacheModel.pageId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, pageId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", pageId=");
		sb.append(pageId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", pageName=");
		sb.append(pageName);
		sb.append(", pageSlug=");
		sb.append(pageSlug);
		sb.append(", tagId=");
		sb.append(tagId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PageTag toEntityModel() {
		PageTagImpl pageTagImpl = new PageTagImpl();

		if (uuid == null) {
			pageTagImpl.setUuid("");
		}
		else {
			pageTagImpl.setUuid(uuid);
		}

		pageTagImpl.setPageId(pageId);
		pageTagImpl.setGroupId(groupId);
		pageTagImpl.setCompanyId(companyId);
		pageTagImpl.setUserId(userId);

		if (userName == null) {
			pageTagImpl.setUserName("");
		}
		else {
			pageTagImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			pageTagImpl.setCreateDate(null);
		}
		else {
			pageTagImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			pageTagImpl.setModifiedDate(null);
		}
		else {
			pageTagImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (pageName == null) {
			pageTagImpl.setPageName("");
		}
		else {
			pageTagImpl.setPageName(pageName);
		}

		if (pageSlug == null) {
			pageTagImpl.setPageSlug("");
		}
		else {
			pageTagImpl.setPageSlug(pageSlug);
		}

		pageTagImpl.setTagId(tagId);

		pageTagImpl.resetOriginalValues();

		return pageTagImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		pageId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		pageName = objectInput.readUTF();
		pageSlug = objectInput.readUTF();

		tagId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(pageId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (pageName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(pageName);
		}

		if (pageSlug == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(pageSlug);
		}

		objectOutput.writeLong(tagId);
	}

	public String uuid;
	public long pageId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String pageName;
	public String pageSlug;
	public long tagId;

}