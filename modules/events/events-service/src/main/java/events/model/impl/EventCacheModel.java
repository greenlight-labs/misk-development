/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import events.model.Event;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Event in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EventCacheModel implements CacheModel<Event>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EventCacheModel)) {
			return false;
		}

		EventCacheModel eventCacheModel = (EventCacheModel)object;

		if (eventId == eventCacheModel.eventId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, eventId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(81);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", eventId=");
		sb.append(eventId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", eventStartDateTime=");
		sb.append(eventStartDateTime);
		sb.append(", eventEndDateTime=");
		sb.append(eventEndDateTime);
		sb.append(", tagId=");
		sb.append(tagId);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", orderNo=");
		sb.append(orderNo);
		sb.append(", type=");
		sb.append(type);
		sb.append(", paytype=");
		sb.append(paytype);
		sb.append(", title=");
		sb.append(title);
		sb.append(", smallImage=");
		sb.append(smallImage);
		sb.append(", bigImage=");
		sb.append(bigImage);
		sb.append(", displayDate=");
		sb.append(displayDate);
		sb.append(", venue=");
		sb.append(venue);
		sb.append(", timing=");
		sb.append(timing);
		sb.append(", duration=");
		sb.append(duration);
		sb.append(", descriptionField1=");
		sb.append(descriptionField1);
		sb.append(", videoLink=");
		sb.append(videoLink);
		sb.append(", descriptionField2=");
		sb.append(descriptionField2);
		sb.append(", subtitle=");
		sb.append(subtitle);
		sb.append(", descriptionField3=");
		sb.append(descriptionField3);
		sb.append(", detailImage=");
		sb.append(detailImage);
		sb.append(", quoteText=");
		sb.append(quoteText);
		sb.append(", quoteAuthor=");
		sb.append(quoteAuthor);
		sb.append(", descriptionField4=");
		sb.append(descriptionField4);
		sb.append(", eventdescription=");
		sb.append(eventdescription);
		sb.append(", eventfeaturedImage=");
		sb.append(eventfeaturedImage);
		sb.append(", buyTicketLink=");
		sb.append(buyTicketLink);
		sb.append(", lat=");
		sb.append(lat);
		sb.append(", lan=");
		sb.append(lan);
		sb.append(", eventPrice=");
		sb.append(eventPrice);
		sb.append(", currency=");
		sb.append(currency);
		sb.append(", email=");
		sb.append(email);
		sb.append(", phone=");
		sb.append(phone);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Event toEntityModel() {
		EventImpl eventImpl = new EventImpl();

		if (uuid == null) {
			eventImpl.setUuid("");
		}
		else {
			eventImpl.setUuid(uuid);
		}

		eventImpl.setEventId(eventId);
		eventImpl.setGroupId(groupId);
		eventImpl.setCompanyId(companyId);
		eventImpl.setUserId(userId);

		if (userName == null) {
			eventImpl.setUserName("");
		}
		else {
			eventImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			eventImpl.setCreateDate(null);
		}
		else {
			eventImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			eventImpl.setModifiedDate(null);
		}
		else {
			eventImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (eventStartDateTime == Long.MIN_VALUE) {
			eventImpl.setEventStartDateTime(null);
		}
		else {
			eventImpl.setEventStartDateTime(new Date(eventStartDateTime));
		}

		if (eventEndDateTime == Long.MIN_VALUE) {
			eventImpl.setEventEndDateTime(null);
		}
		else {
			eventImpl.setEventEndDateTime(new Date(eventEndDateTime));
		}

		eventImpl.setTagId(tagId);
		eventImpl.setCategoryId(categoryId);
		eventImpl.setOrderNo(orderNo);
		eventImpl.setType(type);
		eventImpl.setPaytype(paytype);

		if (title == null) {
			eventImpl.setTitle("");
		}
		else {
			eventImpl.setTitle(title);
		}

		if (smallImage == null) {
			eventImpl.setSmallImage("");
		}
		else {
			eventImpl.setSmallImage(smallImage);
		}

		if (bigImage == null) {
			eventImpl.setBigImage("");
		}
		else {
			eventImpl.setBigImage(bigImage);
		}

		if (displayDate == Long.MIN_VALUE) {
			eventImpl.setDisplayDate(null);
		}
		else {
			eventImpl.setDisplayDate(new Date(displayDate));
		}

		if (venue == null) {
			eventImpl.setVenue("");
		}
		else {
			eventImpl.setVenue(venue);
		}

		if (timing == null) {
			eventImpl.setTiming("");
		}
		else {
			eventImpl.setTiming(timing);
		}

		if (duration == null) {
			eventImpl.setDuration("");
		}
		else {
			eventImpl.setDuration(duration);
		}

		if (descriptionField1 == null) {
			eventImpl.setDescriptionField1("");
		}
		else {
			eventImpl.setDescriptionField1(descriptionField1);
		}

		if (videoLink == null) {
			eventImpl.setVideoLink("");
		}
		else {
			eventImpl.setVideoLink(videoLink);
		}

		if (descriptionField2 == null) {
			eventImpl.setDescriptionField2("");
		}
		else {
			eventImpl.setDescriptionField2(descriptionField2);
		}

		if (subtitle == null) {
			eventImpl.setSubtitle("");
		}
		else {
			eventImpl.setSubtitle(subtitle);
		}

		if (descriptionField3 == null) {
			eventImpl.setDescriptionField3("");
		}
		else {
			eventImpl.setDescriptionField3(descriptionField3);
		}

		if (detailImage == null) {
			eventImpl.setDetailImage("");
		}
		else {
			eventImpl.setDetailImage(detailImage);
		}

		if (quoteText == null) {
			eventImpl.setQuoteText("");
		}
		else {
			eventImpl.setQuoteText(quoteText);
		}

		if (quoteAuthor == null) {
			eventImpl.setQuoteAuthor("");
		}
		else {
			eventImpl.setQuoteAuthor(quoteAuthor);
		}

		if (descriptionField4 == null) {
			eventImpl.setDescriptionField4("");
		}
		else {
			eventImpl.setDescriptionField4(descriptionField4);
		}

		if (eventdescription == null) {
			eventImpl.setEventdescription("");
		}
		else {
			eventImpl.setEventdescription(eventdescription);
		}

		if (eventfeaturedImage == null) {
			eventImpl.setEventfeaturedImage("");
		}
		else {
			eventImpl.setEventfeaturedImage(eventfeaturedImage);
		}

		if (buyTicketLink == null) {
			eventImpl.setBuyTicketLink("");
		}
		else {
			eventImpl.setBuyTicketLink(buyTicketLink);
		}

		if (lat == null) {
			eventImpl.setLat("");
		}
		else {
			eventImpl.setLat(lat);
		}

		if (lan == null) {
			eventImpl.setLan("");
		}
		else {
			eventImpl.setLan(lan);
		}

		if (eventPrice == null) {
			eventImpl.setEventPrice("");
		}
		else {
			eventImpl.setEventPrice(eventPrice);
		}

		if (currency == null) {
			eventImpl.setCurrency("");
		}
		else {
			eventImpl.setCurrency(currency);
		}

		if (email == null) {
			eventImpl.setEmail("");
		}
		else {
			eventImpl.setEmail(email);
		}

		if (phone == null) {
			eventImpl.setPhone("");
		}
		else {
			eventImpl.setPhone(phone);
		}

		eventImpl.resetOriginalValues();

		return eventImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		eventId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		eventStartDateTime = objectInput.readLong();
		eventEndDateTime = objectInput.readLong();

		tagId = objectInput.readLong();

		categoryId = objectInput.readLong();

		orderNo = objectInput.readLong();

		type = objectInput.readInt();

		paytype = objectInput.readInt();
		title = objectInput.readUTF();
		smallImage = objectInput.readUTF();
		bigImage = objectInput.readUTF();
		displayDate = objectInput.readLong();
		venue = objectInput.readUTF();
		timing = objectInput.readUTF();
		duration = objectInput.readUTF();
		descriptionField1 = (String)objectInput.readObject();
		videoLink = objectInput.readUTF();
		descriptionField2 = (String)objectInput.readObject();
		subtitle = objectInput.readUTF();
		descriptionField3 = (String)objectInput.readObject();
		detailImage = objectInput.readUTF();
		quoteText = objectInput.readUTF();
		quoteAuthor = objectInput.readUTF();
		descriptionField4 = (String)objectInput.readObject();
		eventdescription = (String)objectInput.readObject();
		eventfeaturedImage = objectInput.readUTF();
		buyTicketLink = objectInput.readUTF();
		lat = objectInput.readUTF();
		lan = objectInput.readUTF();
		eventPrice = objectInput.readUTF();
		currency = objectInput.readUTF();
		email = objectInput.readUTF();
		phone = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(eventId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(eventStartDateTime);
		objectOutput.writeLong(eventEndDateTime);

		objectOutput.writeLong(tagId);

		objectOutput.writeLong(categoryId);

		objectOutput.writeLong(orderNo);

		objectOutput.writeInt(type);

		objectOutput.writeInt(paytype);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (smallImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(smallImage);
		}

		if (bigImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bigImage);
		}

		objectOutput.writeLong(displayDate);

		if (venue == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(venue);
		}

		if (timing == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(timing);
		}

		if (duration == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(duration);
		}

		if (descriptionField1 == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(descriptionField1);
		}

		if (videoLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(videoLink);
		}

		if (descriptionField2 == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(descriptionField2);
		}

		if (subtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(subtitle);
		}

		if (descriptionField3 == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(descriptionField3);
		}

		if (detailImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(detailImage);
		}

		if (quoteText == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(quoteText);
		}

		if (quoteAuthor == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(quoteAuthor);
		}

		if (descriptionField4 == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(descriptionField4);
		}

		if (eventdescription == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(eventdescription);
		}

		if (eventfeaturedImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(eventfeaturedImage);
		}

		if (buyTicketLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buyTicketLink);
		}

		if (lat == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lat);
		}

		if (lan == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lan);
		}

		if (eventPrice == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(eventPrice);
		}

		if (currency == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(currency);
		}

		if (email == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (phone == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(phone);
		}
	}

	public String uuid;
	public long eventId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long eventStartDateTime;
	public long eventEndDateTime;
	public long tagId;
	public long categoryId;
	public long orderNo;
	public int type;
	public int paytype;
	public String title;
	public String smallImage;
	public String bigImage;
	public long displayDate;
	public String venue;
	public String timing;
	public String duration;
	public String descriptionField1;
	public String videoLink;
	public String descriptionField2;
	public String subtitle;
	public String descriptionField3;
	public String detailImage;
	public String quoteText;
	public String quoteAuthor;
	public String descriptionField4;
	public String eventdescription;
	public String eventfeaturedImage;
	public String buyTicketLink;
	public String lat;
	public String lan;
	public String eventPrice;
	public String currency;
	public String email;
	public String phone;

}