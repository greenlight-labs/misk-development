package events.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import events.model.Schedule;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class ScheduleValidator implements ModelValidator<Schedule> {

	@Override
	public void validate(Schedule entry) throws PortalException {
/*   */
        // Field slideId
        validateScheduleId(entry.getScheduleId());

        // Field eventId
        validateEventId(entry.getEventId());

        // Field Title
        validateTitle(entry.getTitle());

        // Field Description
        validateDescription(entry.getDescription());

        // Field Duration
        validateDuration(entry.getDuration());

        // Field image
        validateImage(entry.getImage());
	}

/*   */
    /**
    * scheduleId field Validation
    *
    * @param field scheduleId
    */
    protected void validateScheduleId(long field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
    }

    /**
    * eventId field Validation
    *
    * @param field eventId
    */
    protected void validateEventId(long field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * title field Validation
    *
    * @param field title
    */
    protected void validateTitle(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * description field Validation
    *
    * @param field description
    */
    protected void validateDescription(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * duration field Validation
    *
    * @param field duration
    */
    protected void validateDuration(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }

    /**
    * image field Validation
    *
    * @param field image
    */
    protected void validateImage(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("gallery-image-required");
        }

    }

/*  */ 	
	

	protected List<String> _errors = new ArrayList<>();

}
