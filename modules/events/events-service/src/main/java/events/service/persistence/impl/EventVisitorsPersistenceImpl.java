/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import events.exception.NoSuchVisitorsException;

import events.model.EventVisitors;
import events.model.impl.EventVisitorsImpl;
import events.model.impl.EventVisitorsModelImpl;

import events.service.persistence.EventVisitorsPersistence;
import events.service.persistence.EventVisitorsUtil;
import events.service.persistence.impl.constants.EventPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the event visitors service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = EventVisitorsPersistence.class)
public class EventVisitorsPersistenceImpl
	extends BasePersistenceImpl<EventVisitors>
	implements EventVisitorsPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>EventVisitorsUtil</code> to access the event visitors persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		EventVisitorsImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the event visitorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<EventVisitors> list = null;

		if (useFinderCache) {
			list = (List<EventVisitors>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EventVisitors eventVisitors : list) {
					if (!uuid.equals(eventVisitors.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<EventVisitors>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByUuid_First(
			String uuid, OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByUuid_First(
			uuid, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByUuid_First(
		String uuid, OrderByComparator<EventVisitors> orderByComparator) {

		List<EventVisitors> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByUuid_Last(
			String uuid, OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByUuid_Last(uuid, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByUuid_Last(
		String uuid, OrderByComparator<EventVisitors> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<EventVisitors> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors[] findByUuid_PrevAndNext(
			long visitor_id, String uuid,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		uuid = Objects.toString(uuid, "");

		EventVisitors eventVisitors = findByPrimaryKey(visitor_id);

		Session session = null;

		try {
			session = openSession();

			EventVisitors[] array = new EventVisitorsImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, eventVisitors, uuid, orderByComparator, true);

			array[1] = eventVisitors;

			array[2] = getByUuid_PrevAndNext(
				session, eventVisitors, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EventVisitors getByUuid_PrevAndNext(
		Session session, EventVisitors eventVisitors, String uuid,
		OrderByComparator<EventVisitors> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						eventVisitors)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EventVisitors> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the event visitorses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (EventVisitors eventVisitors :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(eventVisitors);
		}
	}

	/**
	 * Returns the number of event visitorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching event visitorses
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EVENTVISITORS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"eventVisitors.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(eventVisitors.uuid IS NULL OR eventVisitors.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchVisitorsException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByUUID_G(String uuid, long groupId)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByUUID_G(uuid, groupId);

		if (eventVisitors == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchVisitorsException(sb.toString());
		}

		return eventVisitors;
	}

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof EventVisitors) {
			EventVisitors eventVisitors = (EventVisitors)result;

			if (!Objects.equals(uuid, eventVisitors.getUuid()) ||
				(groupId != eventVisitors.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<EventVisitors> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					EventVisitors eventVisitors = list.get(0);

					result = eventVisitors;

					cacheResult(eventVisitors);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (EventVisitors)result;
		}
	}

	/**
	 * Removes the event visitors where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the event visitors that was removed
	 */
	@Override
	public EventVisitors removeByUUID_G(String uuid, long groupId)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = findByUUID_G(uuid, groupId);

		return remove(eventVisitors);
	}

	/**
	 * Returns the number of event visitorses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching event visitorses
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EVENTVISITORS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"eventVisitors.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(eventVisitors.uuid IS NULL OR eventVisitors.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"eventVisitors.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<EventVisitors> list = null;

		if (useFinderCache) {
			list = (List<EventVisitors>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EventVisitors eventVisitors : list) {
					if (!uuid.equals(eventVisitors.getUuid()) ||
						(companyId != eventVisitors.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<EventVisitors>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<EventVisitors> orderByComparator) {

		List<EventVisitors> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<EventVisitors> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<EventVisitors> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors[] findByUuid_C_PrevAndNext(
			long visitor_id, String uuid, long companyId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		uuid = Objects.toString(uuid, "");

		EventVisitors eventVisitors = findByPrimaryKey(visitor_id);

		Session session = null;

		try {
			session = openSession();

			EventVisitors[] array = new EventVisitorsImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, eventVisitors, uuid, companyId, orderByComparator,
				true);

			array[1] = eventVisitors;

			array[2] = getByUuid_C_PrevAndNext(
				session, eventVisitors, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EventVisitors getByUuid_C_PrevAndNext(
		Session session, EventVisitors eventVisitors, String uuid,
		long companyId, OrderByComparator<EventVisitors> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						eventVisitors)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EventVisitors> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the event visitorses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (EventVisitors eventVisitors :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(eventVisitors);
		}
	}

	/**
	 * Returns the number of event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching event visitorses
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_EVENTVISITORS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"eventVisitors.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(eventVisitors.uuid IS NULL OR eventVisitors.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"eventVisitors.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the event visitorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<EventVisitors> list = null;

		if (useFinderCache) {
			list = (List<EventVisitors>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EventVisitors eventVisitors : list) {
					if (groupId != eventVisitors.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<EventVisitors>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByGroupId_First(
			long groupId, OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByGroupId_First(
			groupId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the first event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByGroupId_First(
		long groupId, OrderByComparator<EventVisitors> orderByComparator) {

		List<EventVisitors> list = findByGroupId(
			groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByGroupId_Last(
			long groupId, OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByGroupId_Last(
			groupId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the last event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByGroupId_Last(
		long groupId, OrderByComparator<EventVisitors> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<EventVisitors> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors[] findByGroupId_PrevAndNext(
			long visitor_id, long groupId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = findByPrimaryKey(visitor_id);

		Session session = null;

		try {
			session = openSession();

			EventVisitors[] array = new EventVisitorsImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, eventVisitors, groupId, orderByComparator, true);

			array[1] = eventVisitors;

			array[2] = getByGroupId_PrevAndNext(
				session, eventVisitors, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EventVisitors getByGroupId_PrevAndNext(
		Session session, EventVisitors eventVisitors, long groupId,
		OrderByComparator<EventVisitors> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						eventVisitors)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EventVisitors> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the event visitorses where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (EventVisitors eventVisitors :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(eventVisitors);
		}
	}

	/**
	 * Returns the number of event visitorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching event visitorses
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EVENTVISITORS_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"eventVisitors.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByEventVisitorId;
	private FinderPath _finderPathWithoutPaginationFindByEventVisitorId;
	private FinderPath _finderPathCountByEventVisitorId;

	/**
	 * Returns all the event visitorses where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @return the matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByEventVisitorId(long eventvisitorId) {
		return findByEventVisitorId(
			eventvisitorId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end) {

		return findByEventVisitorId(eventvisitorId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return findByEventVisitorId(
			eventvisitorId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByEventVisitorId;
				finderArgs = new Object[] {eventvisitorId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByEventVisitorId;
			finderArgs = new Object[] {
				eventvisitorId, start, end, orderByComparator
			};
		}

		List<EventVisitors> list = null;

		if (useFinderCache) {
			list = (List<EventVisitors>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EventVisitors eventVisitors : list) {
					if (eventvisitorId != eventVisitors.getEventvisitorId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

			sb.append(_FINDER_COLUMN_EVENTVISITORID_EVENTVISITORID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(eventvisitorId);

				list = (List<EventVisitors>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByEventVisitorId_First(
			long eventvisitorId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByEventVisitorId_First(
			eventvisitorId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("eventvisitorId=");
		sb.append(eventvisitorId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the first event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByEventVisitorId_First(
		long eventvisitorId,
		OrderByComparator<EventVisitors> orderByComparator) {

		List<EventVisitors> list = findByEventVisitorId(
			eventvisitorId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByEventVisitorId_Last(
			long eventvisitorId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByEventVisitorId_Last(
			eventvisitorId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("eventvisitorId=");
		sb.append(eventvisitorId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the last event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByEventVisitorId_Last(
		long eventvisitorId,
		OrderByComparator<EventVisitors> orderByComparator) {

		int count = countByEventVisitorId(eventvisitorId);

		if (count == 0) {
			return null;
		}

		List<EventVisitors> list = findByEventVisitorId(
			eventvisitorId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors[] findByEventVisitorId_PrevAndNext(
			long visitor_id, long eventvisitorId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = findByPrimaryKey(visitor_id);

		Session session = null;

		try {
			session = openSession();

			EventVisitors[] array = new EventVisitorsImpl[3];

			array[0] = getByEventVisitorId_PrevAndNext(
				session, eventVisitors, eventvisitorId, orderByComparator,
				true);

			array[1] = eventVisitors;

			array[2] = getByEventVisitorId_PrevAndNext(
				session, eventVisitors, eventvisitorId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EventVisitors getByEventVisitorId_PrevAndNext(
		Session session, EventVisitors eventVisitors, long eventvisitorId,
		OrderByComparator<EventVisitors> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

		sb.append(_FINDER_COLUMN_EVENTVISITORID_EVENTVISITORID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(eventvisitorId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						eventVisitors)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EventVisitors> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the event visitorses where eventvisitorId = &#63; from the database.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 */
	@Override
	public void removeByEventVisitorId(long eventvisitorId) {
		for (EventVisitors eventVisitors :
				findByEventVisitorId(
					eventvisitorId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(eventVisitors);
		}
	}

	/**
	 * Returns the number of event visitorses where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @return the number of matching event visitorses
	 */
	@Override
	public int countByEventVisitorId(long eventvisitorId) {
		FinderPath finderPath = _finderPathCountByEventVisitorId;

		Object[] finderArgs = new Object[] {eventvisitorId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EVENTVISITORS_WHERE);

			sb.append(_FINDER_COLUMN_EVENTVISITORID_EVENTVISITORID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(eventvisitorId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EVENTVISITORID_EVENTVISITORID_2 =
		"eventVisitors.eventvisitorId = ?";

	private FinderPath _finderPathWithPaginationFindByeventuserId;
	private FinderPath _finderPathWithoutPaginationFindByeventuserId;
	private FinderPath _finderPathCountByeventuserId;

	/**
	 * Returns all the event visitorses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByeventuserId(long userId) {
		return findByeventuserId(
			userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByeventuserId(
		long userId, int start, int end) {

		return findByeventuserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByeventuserId(
		long userId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return findByeventuserId(userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	@Override
	public List<EventVisitors> findByeventuserId(
		long userId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByeventuserId;
				finderArgs = new Object[] {userId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByeventuserId;
			finderArgs = new Object[] {userId, start, end, orderByComparator};
		}

		List<EventVisitors> list = null;

		if (useFinderCache) {
			list = (List<EventVisitors>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EventVisitors eventVisitors : list) {
					if (userId != eventVisitors.getUserId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

			sb.append(_FINDER_COLUMN_EVENTUSERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				list = (List<EventVisitors>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByeventuserId_First(
			long userId, OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByeventuserId_First(
			userId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the first event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByeventuserId_First(
		long userId, OrderByComparator<EventVisitors> orderByComparator) {

		List<EventVisitors> list = findByeventuserId(
			userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors findByeventuserId_Last(
			long userId, OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByeventuserId_Last(
			userId, orderByComparator);

		if (eventVisitors != null) {
			return eventVisitors;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchVisitorsException(sb.toString());
	}

	/**
	 * Returns the last event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public EventVisitors fetchByeventuserId_Last(
		long userId, OrderByComparator<EventVisitors> orderByComparator) {

		int count = countByeventuserId(userId);

		if (count == 0) {
			return null;
		}

		List<EventVisitors> list = findByeventuserId(
			userId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where userId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors[] findByeventuserId_PrevAndNext(
			long visitor_id, long userId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = findByPrimaryKey(visitor_id);

		Session session = null;

		try {
			session = openSession();

			EventVisitors[] array = new EventVisitorsImpl[3];

			array[0] = getByeventuserId_PrevAndNext(
				session, eventVisitors, userId, orderByComparator, true);

			array[1] = eventVisitors;

			array[2] = getByeventuserId_PrevAndNext(
				session, eventVisitors, userId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected EventVisitors getByeventuserId_PrevAndNext(
		Session session, EventVisitors eventVisitors, long userId,
		OrderByComparator<EventVisitors> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_EVENTVISITORS_WHERE);

		sb.append(_FINDER_COLUMN_EVENTUSERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(EventVisitorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(userId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						eventVisitors)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<EventVisitors> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the event visitorses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByeventuserId(long userId) {
		for (EventVisitors eventVisitors :
				findByeventuserId(
					userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(eventVisitors);
		}
	}

	/**
	 * Returns the number of event visitorses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching event visitorses
	 */
	@Override
	public int countByeventuserId(long userId) {
		FinderPath finderPath = _finderPathCountByeventuserId;

		Object[] finderArgs = new Object[] {userId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_EVENTVISITORS_WHERE);

			sb.append(_FINDER_COLUMN_EVENTUSERID_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_EVENTUSERID_USERID_2 =
		"eventVisitors.userId = ?";

	public EventVisitorsPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(EventVisitors.class);

		setModelImplClass(EventVisitorsImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the event visitors in the entity cache if it is enabled.
	 *
	 * @param eventVisitors the event visitors
	 */
	@Override
	public void cacheResult(EventVisitors eventVisitors) {
		entityCache.putResult(
			EventVisitorsImpl.class, eventVisitors.getPrimaryKey(),
			eventVisitors);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {eventVisitors.getUuid(), eventVisitors.getGroupId()},
			eventVisitors);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the event visitorses in the entity cache if it is enabled.
	 *
	 * @param eventVisitorses the event visitorses
	 */
	@Override
	public void cacheResult(List<EventVisitors> eventVisitorses) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (eventVisitorses.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (EventVisitors eventVisitors : eventVisitorses) {
			if (entityCache.getResult(
					EventVisitorsImpl.class, eventVisitors.getPrimaryKey()) ==
						null) {

				cacheResult(eventVisitors);
			}
		}
	}

	/**
	 * Clears the cache for all event visitorses.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(EventVisitorsImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the event visitors.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EventVisitors eventVisitors) {
		entityCache.removeResult(EventVisitorsImpl.class, eventVisitors);
	}

	@Override
	public void clearCache(List<EventVisitors> eventVisitorses) {
		for (EventVisitors eventVisitors : eventVisitorses) {
			entityCache.removeResult(EventVisitorsImpl.class, eventVisitors);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(EventVisitorsImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		EventVisitorsModelImpl eventVisitorsModelImpl) {

		Object[] args = new Object[] {
			eventVisitorsModelImpl.getUuid(),
			eventVisitorsModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, eventVisitorsModelImpl, false);
	}

	/**
	 * Creates a new event visitors with the primary key. Does not add the event visitors to the database.
	 *
	 * @param visitor_id the primary key for the new event visitors
	 * @return the new event visitors
	 */
	@Override
	public EventVisitors create(long visitor_id) {
		EventVisitors eventVisitors = new EventVisitorsImpl();

		eventVisitors.setNew(true);
		eventVisitors.setPrimaryKey(visitor_id);

		String uuid = PortalUUIDUtil.generate();

		eventVisitors.setUuid(uuid);

		eventVisitors.setCompanyId(CompanyThreadLocal.getCompanyId());

		return eventVisitors;
	}

	/**
	 * Removes the event visitors with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors that was removed
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors remove(long visitor_id)
		throws NoSuchVisitorsException {

		return remove((Serializable)visitor_id);
	}

	/**
	 * Removes the event visitors with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the event visitors
	 * @return the event visitors that was removed
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors remove(Serializable primaryKey)
		throws NoSuchVisitorsException {

		Session session = null;

		try {
			session = openSession();

			EventVisitors eventVisitors = (EventVisitors)session.get(
				EventVisitorsImpl.class, primaryKey);

			if (eventVisitors == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVisitorsException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(eventVisitors);
		}
		catch (NoSuchVisitorsException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EventVisitors removeImpl(EventVisitors eventVisitors) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(eventVisitors)) {
				eventVisitors = (EventVisitors)session.get(
					EventVisitorsImpl.class, eventVisitors.getPrimaryKeyObj());
			}

			if (eventVisitors != null) {
				session.delete(eventVisitors);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (eventVisitors != null) {
			clearCache(eventVisitors);
		}

		return eventVisitors;
	}

	@Override
	public EventVisitors updateImpl(EventVisitors eventVisitors) {
		boolean isNew = eventVisitors.isNew();

		if (!(eventVisitors instanceof EventVisitorsModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(eventVisitors.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					eventVisitors);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in eventVisitors proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom EventVisitors implementation " +
					eventVisitors.getClass());
		}

		EventVisitorsModelImpl eventVisitorsModelImpl =
			(EventVisitorsModelImpl)eventVisitors;

		if (Validator.isNull(eventVisitors.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			eventVisitors.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (eventVisitors.getCreateDate() == null)) {
			if (serviceContext == null) {
				eventVisitors.setCreateDate(date);
			}
			else {
				eventVisitors.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!eventVisitorsModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				eventVisitors.setModifiedDate(date);
			}
			else {
				eventVisitors.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(eventVisitors);
			}
			else {
				eventVisitors = (EventVisitors)session.merge(eventVisitors);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			EventVisitorsImpl.class, eventVisitorsModelImpl, false, true);

		cacheUniqueFindersCache(eventVisitorsModelImpl);

		if (isNew) {
			eventVisitors.setNew(false);
		}

		eventVisitors.resetOriginalValues();

		return eventVisitors;
	}

	/**
	 * Returns the event visitors with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the event visitors
	 * @return the event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors findByPrimaryKey(Serializable primaryKey)
		throws NoSuchVisitorsException {

		EventVisitors eventVisitors = fetchByPrimaryKey(primaryKey);

		if (eventVisitors == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchVisitorsException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return eventVisitors;
	}

	/**
	 * Returns the event visitors with the primary key or throws a <code>NoSuchVisitorsException</code> if it could not be found.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors findByPrimaryKey(long visitor_id)
		throws NoSuchVisitorsException {

		return findByPrimaryKey((Serializable)visitor_id);
	}

	/**
	 * Returns the event visitors with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors, or <code>null</code> if a event visitors with the primary key could not be found
	 */
	@Override
	public EventVisitors fetchByPrimaryKey(long visitor_id) {
		return fetchByPrimaryKey((Serializable)visitor_id);
	}

	/**
	 * Returns all the event visitorses.
	 *
	 * @return the event visitorses
	 */
	@Override
	public List<EventVisitors> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of event visitorses
	 */
	@Override
	public List<EventVisitors> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of event visitorses
	 */
	@Override
	public List<EventVisitors> findAll(
		int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of event visitorses
	 */
	@Override
	public List<EventVisitors> findAll(
		int start, int end, OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<EventVisitors> list = null;

		if (useFinderCache) {
			list = (List<EventVisitors>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_EVENTVISITORS);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_EVENTVISITORS;

				sql = sql.concat(EventVisitorsModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<EventVisitors>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the event visitorses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (EventVisitors eventVisitors : findAll()) {
			remove(eventVisitors);
		}
	}

	/**
	 * Returns the number of event visitorses.
	 *
	 * @return the number of event visitorses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_EVENTVISITORS);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "visitor_id";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_EVENTVISITORS;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return EventVisitorsModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the event visitors persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class, new EventVisitorsModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", EventVisitors.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathWithPaginationFindByEventVisitorId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByEventVisitorId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"eventvisitorId"}, true);

		_finderPathWithoutPaginationFindByEventVisitorId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByEventVisitorId",
			new String[] {Long.class.getName()},
			new String[] {"eventvisitorId"}, true);

		_finderPathCountByEventVisitorId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByEventVisitorId",
			new String[] {Long.class.getName()},
			new String[] {"eventvisitorId"}, false);

		_finderPathWithPaginationFindByeventuserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByeventuserId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"userId"}, true);

		_finderPathWithoutPaginationFindByeventuserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByeventuserId",
			new String[] {Long.class.getName()}, new String[] {"userId"}, true);

		_finderPathCountByeventuserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByeventuserId",
			new String[] {Long.class.getName()}, new String[] {"userId"},
			false);

		_setEventVisitorsUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setEventVisitorsUtilPersistence(null);

		entityCache.removeCache(EventVisitorsImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setEventVisitorsUtilPersistence(
		EventVisitorsPersistence eventVisitorsPersistence) {

		try {
			Field field = EventVisitorsUtil.class.getDeclaredField(
				"_persistence");

			field.setAccessible(true);

			field.set(null, eventVisitorsPersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = EventPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = EventPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = EventPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_EVENTVISITORS =
		"SELECT eventVisitors FROM EventVisitors eventVisitors";

	private static final String _SQL_SELECT_EVENTVISITORS_WHERE =
		"SELECT eventVisitors FROM EventVisitors eventVisitors WHERE ";

	private static final String _SQL_COUNT_EVENTVISITORS =
		"SELECT COUNT(eventVisitors) FROM EventVisitors eventVisitors";

	private static final String _SQL_COUNT_EVENTVISITORS_WHERE =
		"SELECT COUNT(eventVisitors) FROM EventVisitors eventVisitors WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "eventVisitors.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No EventVisitors exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No EventVisitors exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		EventVisitorsPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class EventVisitorsModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			EventVisitorsModelImpl eventVisitorsModelImpl =
				(EventVisitorsModelImpl)baseModel;

			long columnBitmask = eventVisitorsModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(eventVisitorsModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						eventVisitorsModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(EventVisitorsPersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(eventVisitorsModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			EventVisitorsModelImpl eventVisitorsModelImpl, String[] columnNames,
			boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] =
						eventVisitorsModelImpl.getColumnOriginalValue(
							columnName);
				}
				else {
					arguments[i] = eventVisitorsModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= EventVisitorsModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}