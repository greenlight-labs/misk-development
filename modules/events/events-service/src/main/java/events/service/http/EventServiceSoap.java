/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import events.service.EventServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * <code>EventServiceUtil</code> service
 * utility. The static methods of this class call the same methods of the
 * service utility. However, the signatures are different because it is
 * difficult for SOAP to support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a <code>java.util.List</code>,
 * that is translated to an array of
 * <code>events.model.EventSoap</code>. If the method in the
 * service utility returns a
 * <code>events.model.Event</code>, that is translated to a
 * <code>events.model.EventSoap</code>. Methods that SOAP
 * cannot safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventServiceHttp
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class EventServiceSoap {

	public static events.model.EventSoap getEventById(long eventId)
		throws RemoteException {

		try {
			events.model.Event returnValue = EventServiceUtil.getEventById(
				eventId);

			return events.model.EventSoap.toSoapModel(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEvents(long groupId)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEvents(groupId);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEvents(
			long groupId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
				obc)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEvents(groupId, start, end, obc);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEvents(
			long groupId, int start, int end)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEvents(groupId, start, end);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static int getEventsCount(long groupId) throws RemoteException {
		try {
			int returnValue = EventServiceUtil.getEventsCount(groupId);

			return returnValue;
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEventsByType(int type)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEventsByType(type);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEventsByType(
			int type, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
				obc)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEventsByType(type, start, end, obc);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEventsByType(
			int type, int start, int end)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEventsByType(type, start, end);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static int getEventsByTypeCount(int type) throws RemoteException {
		try {
			int returnValue = EventServiceUtil.getEventsByTypeCount(type);

			return returnValue;
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] findByTitle(String title)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.findByTitle(title);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] findByTitle(
			String title, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
				obc)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.findByTitle(title, start, end, obc);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] findByTitle(
			String title, int start, int end)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.findByTitle(title, start, end);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static int findByTitleCount(String title) throws RemoteException {
		try {
			int returnValue = EventServiceUtil.findByTitleCount(title);

			return returnValue;
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEventsByCategory(long categoryId)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEventsByCategory(categoryId);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEventsByCategory(
			long categoryId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
				obc)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEventsByCategory(
					categoryId, start, end, obc);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] getEventsByCategory(
			long categoryId, int start, int end)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.getEventsByCategory(categoryId, start, end);

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static int getEventsByCategoryCount(long categoryId)
		throws RemoteException {

		try {
			int returnValue = EventServiceUtil.getEventsByCategoryCount(
				categoryId);

			return returnValue;
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static events.model.EventSoap[] removeExpiredEvent(
			events.model.EventSoap[] eventList)
		throws RemoteException {

		try {
			java.util.List<events.model.Event> returnValue =
				EventServiceUtil.removeExpiredEvent(
					events.model.impl.EventModelImpl.toModels(eventList));

			return events.model.EventSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(EventServiceSoap.class);

}