/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import events.exception.NoSuchEventException;
import events.model.Event;
import events.service.base.EventServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The implementation of the event remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>events.service.EventService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=event",
		"json.web.service.context.path=Event"
	},
	service = AopService.class
)
public class EventServiceImpl extends EventServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>events.service.EventServiceUtil</code> to access the event remote service.
	 */

	public Event getEventById(long eventId) throws NoSuchEventException {

		return eventPersistence.findByPrimaryKey(eventId);
	}

	/* *
	 * get events by group
	 * */
	public List<Event> getEvents(long groupId) {

		return removeExpiredEvent(eventPersistence.findByGroupId(groupId));
	}

	public List<Event> getEvents(long groupId, int start, int end,
								  OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByGroupId(groupId, start, end, obc));
	}

	public List<Event> getEvents(long groupId, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByGroupId(groupId, start, end));
	}

	public int getEventsCount(long groupId) {
		List<Event> events = getEvents(groupId);
		return events.size();
		//return eventPersistence.countByGroupId(groupId);
	}

	/* *
	 * get events by type
	 * */
	public List<Event> getEventsByType(int type) {

		return removeExpiredEvent(eventPersistence.findByType(type));
	}

	public List<Event> getEventsByType(int type, int start, int end,
										OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByType(type, start, end, obc));
	}

	public List<Event> getEventsByType(int type, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByType(type, start, end));
	}

	public int getEventsByTypeCount(int type) {
		List<Event> events = getEventsByType(type);
		return events.size();
		//return eventPersistence.countByType(type);
	}

	/* *
	 * find events by title
	 * */
	public List<Event> findByTitle(String title) {

		return removeExpiredEvent(eventPersistence.findByTitle('%'+title+'%'));
	}

	public List<Event> findByTitle(String title, int start, int end,
								  OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByTitle('%'+title+'%', start, end, obc));
	}

	public List<Event> findByTitle(String title, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByTitle('%'+title+'%', start, end));
	}

	public int findByTitleCount(String title) {
		List<Event> events = findByTitle(title);
		return events.size();
		//return eventPersistence.countByTitle('%'+title+'%');
	}

	/* *
	 * get events by category
	 * */
	public List<Event> getEventsByCategory(long categoryId) {

		return removeExpiredEvent(eventPersistence.findByCategoryId(categoryId));
	}

	public List<Event> getEventsByCategory(long categoryId, int start, int end,
										 OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByCategoryId(categoryId, start, end, obc));
	}

	public List<Event> getEventsByCategory(long categoryId, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByCategoryId(categoryId, start, end));
	}

	public int getEventsByCategoryCount(long categoryId) {
		List<Event> events = getEventsByCategory(categoryId);
		return events.size();
		//return eventPersistence.countByCategoryId(categoryId);
	}
	
	public List<Event> removeExpiredEvent(List<Event> eventList) {
		List<Event> excludeExpireEventList = new ArrayList<Event>();
		if (Validator.isNotNull(eventList) && eventList.size() > 0) {
			excludeExpireEventList = eventList.stream().filter(p -> p.getEventEndDateTime().after(new Date()))
					.collect(Collectors.toList());
		}
		return excludeExpireEventList;
	}
}