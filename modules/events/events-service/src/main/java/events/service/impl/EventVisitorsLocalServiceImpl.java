/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import events.model.Event;
import events.model.EventVisitors;
import events.service.base.EventVisitorsLocalServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the event visitors local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>events.service.EventVisitorsLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventVisitorsLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=events.model.EventVisitors",
	service = AopService.class
)
public class EventVisitorsLocalServiceImpl
	extends EventVisitorsLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>events.service.EventVisitorsLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>events.service.EventVisitorsLocalServiceUtil</code>.
	 */
	public EventVisitors addEventVisitors(long userId,long eventId, ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		EventVisitors event = eventVisitorsPersistence.create(eventId);

		event.setUuid(serviceContext.getUuid());
		event.setUserId(userId);
		event.setGroupId(groupId);
		event.setCompanyId(user.getCompanyId());
		event.setCreateDate(serviceContext.getCreateDate(now));
		event.setModifiedDate(serviceContext.getModifiedDate(now));

		event.setExpandoBridgeAttributes(serviceContext);
		eventVisitorsPersistence.update(event);
		eventVisitorsPersistence.clearCache();

		return event;
	}

	@Override
	public List<EventVisitors> findEventByUser(long userId) {
		return eventVisitorsPersistence.findByeventuserId(userId);
	}
	@Override
	public List<EventVisitors> findEventByvisitor(long eventid) {
		return eventVisitorsPersistence.findByEventVisitorId(eventid);
	}

}