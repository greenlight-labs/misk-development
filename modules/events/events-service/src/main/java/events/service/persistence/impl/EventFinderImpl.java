package events.service.persistence.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import events.model.Event;
import events.model.impl.EventImpl;
import events.service.persistence.EventFinder;


@Component(service = EventFinder.class)
public class EventFinderImpl
        extends EventFinderBaseImpl implements EventFinder {

    public static final String FIND_BY_FILTER =
            EventFinder.class.getName() + ".findByFilter";
    public static final String COUNT_BY_FILTER =
            EventFinder.class.getName() + ".countByFilter";
    Log _log = LogFactoryUtil.getLog(EventFinderImpl.class);
    public List<Event> findByFilter(int payType, String priceFrom, String priceTo, long categoryId, String dateFrom, String dateTo, int offset, int limit) {
        Session session = null;
        List<Event> events = Collections.emptyList();

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), FIND_BY_FILTER);

            sql = StringUtil.replace(sql, "[$PAY_TYPE$]", getPayType(payType));           
        	if (payType > 0) {
        		 sql = StringUtil.replace(sql, "[$EVENT_PRICE_BETWEEN$]", getEventPrice(priceFrom, priceTo));
			} else {
				sql = StringUtil.replace(sql, "[$EVENT_PRICE_BETWEEN$]", getEventPriceAndFreePayType(priceFrom, priceTo));
			}
            sql = StringUtil.replace(sql, "[$CATEGORY_ID$]", getCategoryId(categoryId));
            sql = StringUtil.replace(sql, "[$DATE_RANGE_BETWEEN$]", getDateRange(dateFrom, dateTo));

            sql = StringUtil.replaceLast(sql, "AND", StringPool.BLANK);

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);
            sqlQuery.setCacheable(false);
            sqlQuery.addEntity("Event", EventImpl.class);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            if (payType > 0) {
                queryPos.add(payType);
            }

            if (Validator.isNotNull(priceFrom) && Validator.isNotNull(priceTo)) {
                queryPos.add(priceFrom);
                queryPos.add(priceTo);
            }

            if (categoryId > 0) {
                queryPos.add(categoryId);
            }

            if (Validator.isNotNull(dateFrom) && Validator.isNotNull(dateTo)) {
                queryPos.add(dateFrom);
                queryPos.add(dateTo);
                queryPos.add(dateFrom);
                queryPos.add(dateTo);
            }

            queryPos.add(offset);
            queryPos.add(limit);

            events = (List<Event>) sqlQuery.list();

            return events;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    public int countByFilter(int payType, String priceFrom, String priceTo, long categoryId, String dateFrom, String dateTo) {
        Session session = null;

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), COUNT_BY_FILTER);

            sql = StringUtil.replace(sql, "[$PAY_TYPE$]", getPayType(payType));
            
            if (payType > 0) {
       		 sql = StringUtil.replace(sql, "[$EVENT_PRICE_BETWEEN$]", getEventPrice(priceFrom, priceTo));
			} else {
				sql = StringUtil.replace(sql, "[$EVENT_PRICE_BETWEEN$]", getEventPriceAndFreePayType(priceFrom, priceTo));
			}            
			//sql = StringUtil.replace(sql, "[$EVENT_PRICE_BETWEEN$]", getEventPrice(priceFrom, priceTo));			
            sql = StringUtil.replace(sql, "[$CATEGORY_ID$]", getCategoryId(categoryId));
            sql = StringUtil.replace(sql, "[$DATE_RANGE_BETWEEN$]", getDateRange(dateFrom, dateTo));

            sql = StringUtil.replaceLast(sql, "AND", StringPool.BLANK);

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.addScalar(COUNT_COLUMN_NAME, Type.LONG);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            if (payType > 0) {
                queryPos.add(payType);
            }

            if (Validator.isNotNull(priceFrom) && Validator.isNotNull(priceTo)) {
                queryPos.add(priceFrom);
                queryPos.add(priceTo);
            }

            if (categoryId > 0) {
                queryPos.add(categoryId);
            }

            if (Validator.isNotNull(dateFrom) && Validator.isNotNull(dateTo)) {
                queryPos.add(dateFrom);
                queryPos.add(dateTo);
                queryPos.add(dateFrom);
                queryPos.add(dateTo);
            }

            Iterator<Long> iterator = sqlQuery.iterate();

            if (iterator.hasNext()) {
                Long count = iterator.next();

                if (count != null) {
                    return count.intValue();
                }
            }

            return 0;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    protected String getPayType(int payType) {
        if (payType > 0) {
            return "e.paytype = ? AND";
        }

        return StringPool.BLANK;
    }

    protected String getEventPrice(String priceFrom, String priceTo) {
        if (Validator.isNotNull(priceFrom) && Validator.isNotNull(priceTo)) {
            return "e.eventPrice BETWEEN ? AND ? AND";
        }

        return StringPool.BLANK;
    }
    
    protected String getEventPriceAndFreePayType(String priceFrom, String priceTo) {
        if (Validator.isNotNull(priceFrom) && Validator.isNotNull(priceTo)) {
        	
            return "(e.eventPrice BETWEEN ? AND ?  OR e.paytype = 1) AND";
        }

        return StringPool.BLANK;
    }

    protected String getCategoryId(long categoryId) {
        if (categoryId > 0) {
            return "e.categoryId = ? AND";
        }

        return StringPool.BLANK;
    }

    protected String getDateRange(String dateFrom, String dateTo) {
        if (Validator.isNotNull(dateFrom) && Validator.isNotNull(dateTo)) {
            return "(DATE(e.eventStartDateTime) BETWEEN ? AND ? OR DATE(e.eventEndDateTime) BETWEEN ? AND ?) AND";
        }

        return StringPool.BLANK;
    }

    @Reference
    private CustomSQL _customSQL;
}
