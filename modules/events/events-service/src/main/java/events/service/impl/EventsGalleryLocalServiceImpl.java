/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import events.exception.EventsGalleryValidateException;
import events.model.EventsGallery;
import events.service.base.EventsGalleryLocalServiceBaseImpl;

import events.service.util.EventsGalleryValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the events gallery local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>events.service.EventsGalleryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventsGalleryLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=events.model.EventsGallery",
	service = AopService.class
)
public class EventsGalleryLocalServiceImpl
	extends EventsGalleryLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>events.service.EventsGalleryLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>events.service.EventsGalleryLocalServiceUtil</code>.
	 */
	public EventsGallery addEntry(EventsGallery orgEntry, ServiceContext serviceContext)
			throws PortalException, EventsGalleryValidateException {

		// Validation

		ModelValidator<EventsGallery> modelValidator = new EventsGalleryValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		EventsGallery entry = _addEntry(orgEntry, serviceContext);

		EventsGallery addedEntry = eventsGalleryPersistence.update(entry);
		eventsGalleryPersistence.clearCache();

		return addedEntry;
	}

	public EventsGallery updateEntry(
			EventsGallery orgEntry, ServiceContext serviceContext)
			throws PortalException, EventsGalleryValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<EventsGallery> modelValidator = new EventsGalleryValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		EventsGallery entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		EventsGallery updatedEntry = eventsGalleryPersistence.update(entry);
		eventsGalleryPersistence.clearCache();

		return updatedEntry;
	}

	protected EventsGallery _addEntry(EventsGallery entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(EventsGallery.class.getName());

		EventsGallery newEntry = eventsGalleryPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);

		newEntry.setUuid(serviceContext.getUuid());
		newEntry.setEventId(entry.getEventId());
		newEntry.setImage(entry.getImage());
		newEntry.setThumbnail(entry.getThumbnail());

		return newEntry;
	}

	protected EventsGallery _updateEntry(
			long primaryKey, EventsGallery entry, ServiceContext serviceContext)
			throws PortalException {

		EventsGallery updateEntry = fetchEventsGallery(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);

		updateEntry.setUuid(entry.getUuid());

		updateEntry.setSlideId(entry.getSlideId());
		updateEntry.setEventId(entry.getEventId());
		updateEntry.setImage(entry.getImage());
		updateEntry.setThumbnail(entry.getThumbnail());

		return updateEntry;
	}

	public EventsGallery deleteEntry(long primaryKey) throws PortalException {
		EventsGallery entry = getEventsGallery(primaryKey);
		eventsGalleryPersistence.remove(entry);

		return entry;
	}

	public List<EventsGallery> findAllInGroup(long groupId) {

		return eventsGalleryPersistence.findByGroupId(groupId);
	}

	public List<EventsGallery> findAllInGroup(long groupId, int start, int end,
										OrderByComparator<EventsGallery> obc) {

		return eventsGalleryPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<EventsGallery> findAllInGroup(long groupId, int start, int end) {

		return eventsGalleryPersistence.findByGroupId(groupId, start, end);
	}

	public int countAllInGroup(long groupId) {

		return eventsGalleryPersistence.countByGroupId(groupId);
	}

	/* *********************- gallery by discover id -*********************** */
	public List<EventsGallery> findAllInDiscover(long discoverId) {

		return eventsGalleryPersistence.findByEventId(discoverId);
	}

	public List<EventsGallery> findAllInDiscover(long discoverId, int start, int end,
										   OrderByComparator<EventsGallery> obc) {

		return eventsGalleryPersistence.findByEventId(discoverId, start, end, obc);
	}

	public List<EventsGallery> findAllInDiscover(long discoverId, int start, int end) {

		return eventsGalleryPersistence.findByEventId(discoverId, start, end);
	}

	public int countAllInDiscover(long discoverId) {

		return eventsGalleryPersistence.countByEventId(discoverId);
	}
	/* ********************************************************************** */

	public EventsGallery getGalleryFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, EventsGalleryValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		EventsGallery entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchEventsGallery(primaryKey);
		}

		try {
			entry.setSlideId(primaryKey);
			entry.setEventId(ParamUtil.getLong(request, "eventId"));
			entry.setImageMap(LocalizationUtil.getLocalizationMap(request, "image"));
			entry.setThumbnailMap(LocalizationUtil.getLocalizationMap(request, "thumbnail"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new EventsGalleryValidateException(error);
		}

		return entry;
	}

	public EventsGallery getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(EventsGallery.class.getName());

		return createEventsGallery(primaryKey);
	}

	private static Log _log = LogFactoryUtil.getLog(
			EventsGalleryLocalServiceImpl.class);
}