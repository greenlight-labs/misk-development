package events.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import events.model.Tag;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Category Validator
 * 
 * @author tz
 *
 */
public class TagValidator implements ModelValidator<Tag> {

	@Override
	public void validate(Tag entry) throws PortalException {

        // Fields: tagId, name, color, status
        validateTagId(entry.getTagId());
        validateName(entry.getName());

	}

    protected void validateTagId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("highlight-tag-required");
        }
    }

    protected void validateName(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("highlight-name-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
