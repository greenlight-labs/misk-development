package events.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import events.model.PageTag;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator
 *
 * @author tz
 */
public class PageTagValidator implements ModelValidator<PageTag> {

    @Override
    public void validate(PageTag entry) throws PortalException {
        /*   */
        // Validate fields
        validatePageName(entry.getPageName());
        validatePageSlug(entry.getPageSlug());
        validateTagId(entry.getTagId());
    }

    /**
     * categoryId field Validation
     *
     * @param field categoryId
     */
    protected void validatePageName(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("page-tag-page-name-required");
        }
    }

    protected void validatePageSlug(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("page-tag-page-slug-required");
        }
    }

    protected void validateTagId(long field) {
        if (Validator.isNull(field)) {
            _errors.add("page-tag-tag-id-required");
        }
    }

    protected List<String> _errors = new ArrayList<>();

}
