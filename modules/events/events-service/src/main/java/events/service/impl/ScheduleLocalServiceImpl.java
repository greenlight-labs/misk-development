/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import events.exception.ScheduleValidateException;
import events.model.Schedule;
import events.service.base.ScheduleLocalServiceBaseImpl;

import events.service.util.ScheduleValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the schedule local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>events.service.ScheduleLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ScheduleLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=events.model.Schedule",
	service = AopService.class
)
public class ScheduleLocalServiceImpl extends ScheduleLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>events.service.ScheduleLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>events.service.ScheduleLocalServiceUtil</code>.
	 */

	public Schedule addEntry(Schedule orgEntry, ServiceContext serviceContext)
			throws PortalException, ScheduleValidateException {

		// Validation

		ModelValidator<Schedule> modelValidator = new ScheduleValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Schedule entry = _addEntry(orgEntry, serviceContext);

		Schedule addedEntry = schedulePersistence.update(entry);
		schedulePersistence.clearCache();

		return addedEntry;
	}

	public Schedule updateEntry(
			Schedule orgEntry, ServiceContext serviceContext)
			throws PortalException, ScheduleValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Schedule> modelValidator = new ScheduleValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Schedule entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Schedule updatedEntry = schedulePersistence.update(entry);
		schedulePersistence.clearCache();

		return updatedEntry;
	}

	protected Schedule _addEntry(Schedule entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(Schedule.class.getName());

		Schedule newEntry = schedulePersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);

		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setEventId(entry.getEventId());
		newEntry.setTitle(entry.getTitle());
		newEntry.setDescription(entry.getDescription());
		newEntry.setDuration(entry.getDuration());
		newEntry.setImage(entry.getImage());

		return newEntry;
	}

	protected Schedule _updateEntry(
			long primaryKey, Schedule entry, ServiceContext serviceContext)
			throws PortalException {

		Schedule updateEntry = fetchSchedule(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);

		updateEntry.setUuid(entry.getUuid());

		updateEntry.setEventId(entry.getEventId());
		updateEntry.setTitle(entry.getTitle());
		updateEntry.setDescription(entry.getDescription());
		updateEntry.setDuration(entry.getDuration());
		updateEntry.setImage(entry.getImage());

		return updateEntry;
	}

	public Schedule deleteEntry(long primaryKey) throws PortalException {
		Schedule entry = getSchedule(primaryKey);
		schedulePersistence.remove(entry);

		return entry;
	}

	public List<Schedule> findAllInGroup(long groupId) {

		return schedulePersistence.findByGroupId(groupId);
	}

	public List<Schedule> findAllInGroup(long groupId, int start, int end,
										OrderByComparator<Schedule> obc) {

		return schedulePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Schedule> findAllInGroup(long groupId, int start, int end) {

		return schedulePersistence.findByGroupId(groupId, start, end);
	}

	public int countAllInGroup(long groupId) {

		return schedulePersistence.countByGroupId(groupId);
	}

	/* *********************- schedule by event id -*********************** */
	public List<Schedule> findAllInEvent(long eventId) {

		return schedulePersistence.findByEventId(eventId);
	}

	public List<Schedule> findAllInEvent(long eventId, int start, int end,
										   OrderByComparator<Schedule> obc) {

		return schedulePersistence.findByEventId(eventId, start, end, obc);
	}

	public List<Schedule> findAllInEvent(long eventId, int start, int end) {

		return schedulePersistence.findByEventId(eventId, start, end);
	}

	public int countAllInEvent(long eventId) {

		return schedulePersistence.countByEventId(eventId);
	}
	/* ********************************************************************** */

	public Schedule getScheduleFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, ScheduleValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Schedule entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchSchedule(primaryKey);
		}

		try {
			entry.setScheduleId(primaryKey);
			entry.setEventId(ParamUtil.getLong(request, "eventId"));
			entry.setTitleMap(LocalizationUtil.getLocalizationMap(request, "title"));
			entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));
			entry.setDurationMap(LocalizationUtil.getLocalizationMap(request, "duration"));
			entry.setImage(ParamUtil.getString(request, "image"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new ScheduleValidateException(error);
		}

		return entry;
	}

	public Schedule getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(Schedule.class.getName());

		return createSchedule(primaryKey);
	}

	private static Log _log = LogFactoryUtil.getLog(
			ScheduleLocalServiceImpl.class);
}