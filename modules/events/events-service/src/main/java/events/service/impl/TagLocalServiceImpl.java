/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.impl;

import com.github.slugify.Slugify;
import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import events.exception.TagValidateException;
import events.model.Tag;
import events.service.base.TagLocalServiceBaseImpl;

import events.service.util.TagValidator;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * @author Brian Wing Shun Chan
 */
@Component(
	property = "model.class.name=events.model.Tag", service = AopService.class
)
public class TagLocalServiceImpl extends TagLocalServiceBaseImpl {

	public Tag addEntry(Tag orgEntry, ServiceContext serviceContext)
			throws PortalException, TagValidateException {

		// Validation

		ModelValidator<Tag> modelValidator = new TagValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Tag entry = _addEntry(orgEntry, serviceContext);

		Tag addedEntry = tagPersistence.update(entry);

		tagPersistence.clearCache();

		return addedEntry;
	}

	public Tag updateEntry(
			Tag orgEntry, ServiceContext serviceContext)
			throws PortalException, TagValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Tag> modelValidator = new TagValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Tag entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Tag updatedEntry = tagPersistence.update(entry);
		tagPersistence.clearCache();

		return updatedEntry;
	}

	protected Tag _addEntry(Tag entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(Tag.class.getName());

		Tag newEntry = tagPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setNameMap(entry.getNameMap());
		newEntry.setSlug(entry.getSlug());

		//return tagPersistence.update(newEntry);
		return newEntry;
	}

	protected Tag _updateEntry(
			long primaryKey, Tag entry, ServiceContext serviceContext)
			throws PortalException {

		Tag updateEntry = fetchTag(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setNameMap(entry.getNameMap());
		updateEntry.setSlug(entry.getSlug());

		return updateEntry;
	}

	public Tag deleteEntry(long primaryKey) throws PortalException {
		Tag entry = getTag(primaryKey);
		tagPersistence.remove(entry);

		return entry;
	}

	public List<Tag> findAllInGroup(long groupId) {

		return tagPersistence.findByGroupId(groupId);
	}

	public List<Tag> findAllInGroup(long groupId, int start, int end,
									OrderByComparator<Tag> obc) {

		return tagPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Tag> findAllInGroup(long groupId, int start, int end) {

		return tagPersistence.findByGroupId(groupId, start, end);
	}


	public int countAllInGroup(long groupId) {

		return tagPersistence.countByGroupId(groupId);
	}

	public Tag fetchBySlug(String slug) {

		return tagPersistence.fetchBySlug(slug);
	}

	public Tag getTagFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, TagValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Tag entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchTag(primaryKey);
		}

		try {
			Map<Locale, String> nameMap = LocalizationUtil.getLocalizationMap(request, "name");
			String name = nameMap.get(LocaleUtils.toLocale("en_US"));
			String slug = ParamUtil.getString(request, "slug");
			if(StringUtils.isEmpty(slug)){
				Slugify slg = new Slugify();
				slug = slg.slugify(name);
			}

			entry.setNameMap(nameMap);
			entry.setSlug(slug);

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new TagValidateException(error);
		}

		return entry;
	}

	public Tag getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(Tag.class.getName());

		return createTag(primaryKey);
	}

	private static Log _log = LogFactoryUtil.getLog(
			TagLocalServiceImpl.class);
}