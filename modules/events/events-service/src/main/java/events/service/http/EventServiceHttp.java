/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import events.service.EventServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>EventServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventServiceSoap
 * @generated
 */
public class EventServiceHttp {

	public static events.model.Event getEventById(
			HttpPrincipal httpPrincipal, long eventId)
		throws events.exception.NoSuchEventException {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventById",
				_getEventByIdParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey, eventId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						events.exception.NoSuchEventException) {

					throw (events.exception.NoSuchEventException)exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (events.model.Event)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEvents(
		HttpPrincipal httpPrincipal, long groupId) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEvents", _getEventsParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey, groupId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEvents(
		HttpPrincipal httpPrincipal, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEvents", _getEventsParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, groupId, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEvents(
		HttpPrincipal httpPrincipal, long groupId, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEvents", _getEventsParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, groupId, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getEventsCount(
		HttpPrincipal httpPrincipal, long groupId) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsCount",
				_getEventsCountParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey, groupId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEventsByType(
		HttpPrincipal httpPrincipal, int type) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByType",
				_getEventsByTypeParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey, type);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEventsByType(
		HttpPrincipal httpPrincipal, int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByType",
				_getEventsByTypeParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, type, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEventsByType(
		HttpPrincipal httpPrincipal, int type, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByType",
				_getEventsByTypeParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, type, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getEventsByTypeCount(
		HttpPrincipal httpPrincipal, int type) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByTypeCount",
				_getEventsByTypeCountParameterTypes8);

			MethodHandler methodHandler = new MethodHandler(methodKey, type);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> findByTitle(
		HttpPrincipal httpPrincipal, String title) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "findByTitle",
				_findByTitleParameterTypes9);

			MethodHandler methodHandler = new MethodHandler(methodKey, title);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> findByTitle(
		HttpPrincipal httpPrincipal, String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "findByTitle",
				_findByTitleParameterTypes10);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> findByTitle(
		HttpPrincipal httpPrincipal, String title, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "findByTitle",
				_findByTitleParameterTypes11);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int findByTitleCount(
		HttpPrincipal httpPrincipal, String title) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "findByTitleCount",
				_findByTitleCountParameterTypes12);

			MethodHandler methodHandler = new MethodHandler(methodKey, title);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEventsByCategory(
		HttpPrincipal httpPrincipal, long categoryId) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByCategory",
				_getEventsByCategoryParameterTypes13);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEventsByCategory(
		HttpPrincipal httpPrincipal, long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByCategory",
				_getEventsByCategoryParameterTypes14);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> getEventsByCategory(
		HttpPrincipal httpPrincipal, long categoryId, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByCategory",
				_getEventsByCategoryParameterTypes15);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getEventsByCategoryCount(
		HttpPrincipal httpPrincipal, long categoryId) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "getEventsByCategoryCount",
				_getEventsByCategoryCountParameterTypes16);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<events.model.Event> removeExpiredEvent(
		HttpPrincipal httpPrincipal,
		java.util.List<events.model.Event> eventList) {

		try {
			MethodKey methodKey = new MethodKey(
				EventServiceUtil.class, "removeExpiredEvent",
				_removeExpiredEventParameterTypes17);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, eventList);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<events.model.Event>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(EventServiceHttp.class);

	private static final Class<?>[] _getEventByIdParameterTypes0 = new Class[] {
		long.class
	};
	private static final Class<?>[] _getEventsParameterTypes1 = new Class[] {
		long.class
	};
	private static final Class<?>[] _getEventsParameterTypes2 = new Class[] {
		long.class, int.class, int.class,
		com.liferay.portal.kernel.util.OrderByComparator.class
	};
	private static final Class<?>[] _getEventsParameterTypes3 = new Class[] {
		long.class, int.class, int.class
	};
	private static final Class<?>[] _getEventsCountParameterTypes4 =
		new Class[] {long.class};
	private static final Class<?>[] _getEventsByTypeParameterTypes5 =
		new Class[] {int.class};
	private static final Class<?>[] _getEventsByTypeParameterTypes6 =
		new Class[] {
			int.class, int.class, int.class,
			com.liferay.portal.kernel.util.OrderByComparator.class
		};
	private static final Class<?>[] _getEventsByTypeParameterTypes7 =
		new Class[] {int.class, int.class, int.class};
	private static final Class<?>[] _getEventsByTypeCountParameterTypes8 =
		new Class[] {int.class};
	private static final Class<?>[] _findByTitleParameterTypes9 = new Class[] {
		String.class
	};
	private static final Class<?>[] _findByTitleParameterTypes10 = new Class[] {
		String.class, int.class, int.class,
		com.liferay.portal.kernel.util.OrderByComparator.class
	};
	private static final Class<?>[] _findByTitleParameterTypes11 = new Class[] {
		String.class, int.class, int.class
	};
	private static final Class<?>[] _findByTitleCountParameterTypes12 =
		new Class[] {String.class};
	private static final Class<?>[] _getEventsByCategoryParameterTypes13 =
		new Class[] {long.class};
	private static final Class<?>[] _getEventsByCategoryParameterTypes14 =
		new Class[] {
			long.class, int.class, int.class,
			com.liferay.portal.kernel.util.OrderByComparator.class
		};
	private static final Class<?>[] _getEventsByCategoryParameterTypes15 =
		new Class[] {long.class, int.class, int.class};
	private static final Class<?>[] _getEventsByCategoryCountParameterTypes16 =
		new Class[] {long.class};
	private static final Class<?>[] _removeExpiredEventParameterTypes17 =
		new Class[] {java.util.List.class};

}