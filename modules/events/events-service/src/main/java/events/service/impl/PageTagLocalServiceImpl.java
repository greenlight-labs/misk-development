package events.service.impl;

import com.github.slugify.Slugify;
import events.exception.PageTagValidateException;
import events.model.PageTag;
import events.model.Tag;
import events.service.base.PageTagLocalServiceBaseImpl;
import events.service.util.PageTagValidator;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

@Component(
        property = "model.class.name=events.model.PageTag", service = AopService.class
)
public class PageTagLocalServiceImpl extends PageTagLocalServiceBaseImpl {

    private static Log _log = LogFactoryUtil.getLog(PageTagLocalServiceImpl.class);

    public PageTag addEntry(PageTag orgEntry, ServiceContext serviceContext)
            throws PortalException, PageTagValidateException {

        // Validation

        ModelValidator<PageTag> modelValidator = new PageTagValidator();
        modelValidator.validate(orgEntry);

        // Add entry

        PageTag entry = _addEntry(orgEntry, serviceContext);

        PageTag addedEntry = pageTagPersistence.update(entry);

        pageTagPersistence.clearCache();

        return addedEntry;
    }

    public PageTag updateEntry(PageTag orgEntry, ServiceContext serviceContext)
            throws PortalException, PageTagValidateException {

        User user = userLocalService.getUser(orgEntry.getUserId());

        // Validation

        ModelValidator<PageTag> modelValidator = new PageTagValidator();
        modelValidator.validate(orgEntry);

        // Update entry

        PageTag entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

        PageTag updatedEntry = pageTagPersistence.update(entry);
        pageTagPersistence.clearCache();

        return updatedEntry;
    }

    protected PageTag _addEntry(PageTag entry, ServiceContext serviceContext) throws PortalException {

        long id = counterLocalService.increment(PageTag.class.getName());

        PageTag newEntry = pageTagPersistence.create(id);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        newEntry.setCompanyId(entry.getCompanyId());
        newEntry.setGroupId(entry.getGroupId());
        newEntry.setUserId(user.getUserId());
        newEntry.setUserName(user.getFullName());
        newEntry.setCreateDate(now);
        newEntry.setModifiedDate(now);
        newEntry.setUuid(serviceContext.getUuid());

        newEntry.setPageName(entry.getPageName());
        newEntry.setPageSlug(entry.getPageSlug());
        newEntry.setTagId(entry.getTagId());

        return newEntry;
    }

    protected PageTag _updateEntry(long primaryKey, PageTag entry, ServiceContext serviceContext)
            throws PortalException {

        PageTag updateEntry = fetchPageTag(primaryKey);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        updateEntry.setCompanyId(entry.getCompanyId());
        updateEntry.setGroupId(entry.getGroupId());
        updateEntry.setUserId(user.getUserId());
        updateEntry.setUserName(user.getFullName());
        updateEntry.setCreateDate(entry.getCreateDate());
        updateEntry.setModifiedDate(now);
        updateEntry.setUuid(entry.getUuid());

        updateEntry.setPageName(entry.getPageName());
        updateEntry.setPageSlug(entry.getPageSlug());
        updateEntry.setTagId(entry.getTagId());

        return updateEntry;
    }

    public PageTag deleteEntry(long primaryKey) throws PortalException {
        PageTag entry = getPageTag(primaryKey);
        pageTagPersistence.remove(entry);

        return entry;
    }

    public List<PageTag> findByGroupId(long groupId) {

        return pageTagPersistence.findByGroupId(groupId);
    }

    public List<PageTag> findByGroupId(long groupId, int start, int end, OrderByComparator<PageTag> obc) {

        return pageTagPersistence.findByGroupId(groupId, start, end, obc);
    }

    public List<PageTag> findByGroupId(long groupId, int start, int end) {

        return pageTagPersistence.findByGroupId(groupId, start, end);
    }

    public int countByGroupId(long groupId) {

        return pageTagPersistence.countByGroupId(groupId);
    }

    public PageTag getPageTagFromRequest(long primaryKey, PortletRequest request)
            throws PortletException, PageTagValidateException {

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

        // Create or fetch existing data

        PageTag entry;

        if (primaryKey <= 0) {
            entry = getNewObject(primaryKey);
        } else {
            entry = fetchPageTag(primaryKey);
        }

        try {
            String pageName = ParamUtil.getString(request, "pageName");
            String pageSlug = ParamUtil.getString(request, "pageSlug");
            if(StringUtils.isEmpty(pageSlug)){
                Slugify slg = new Slugify();
                pageSlug = slg.slugify(pageName);
            }

            entry.setPageName(pageName);
            entry.setPageSlug(pageSlug);
            entry.setTagId(ParamUtil.getLong(request, "tagId"));

            entry.setCompanyId(themeDisplay.getCompanyId());
            entry.setGroupId(themeDisplay.getScopeGroupId());
            entry.setUserId(themeDisplay.getUserId());
        } catch (Exception e) {
            _log.error("Errors occur while populating the model", e);
            List<String> error = new ArrayList<>();
            error.add("value-convert-error");

            throw new PageTagValidateException(error);
        }

        return entry;
    }

    public PageTag getNewObject(long primaryKey) {
        primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(PageTag.class.getName());

        return createPageTag(primaryKey);
    }

    public PageTag fetchByPageSlug(String pageSlug) {

        return pageTagPersistence.fetchByPageSlug(pageSlug);
    }

    /* **********************************- Additional Code Here -***************************** */
}