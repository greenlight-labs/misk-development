package events.service.indexer;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.GetterUtil;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import events.model.Event;
import events.service.EventLocalService;

@Component(immediate = true, service = Indexer.class)
public class EventIndexer extends BaseIndexer<Event> {
	public static final String CLASS_NAME = EventIndexer.class.getName();

	private static final String SUBTITLE = "subtitle";

	private static final String VENUE = "venue";

	private static final String DESCRIPTIONFIELD1 = "descriptionField1";

	private static final String QUOTETEXT = "quoteText";

	public EventIndexer() {
		setDefaultSelectedFieldNames(Field.ASSET_TAG_NAMES, Field.COMPANY_ID, Field.CONTENT, Field.ENTRY_CLASS_NAME,
				Field.ENTRY_CLASS_PK, Field.GROUP_ID, Field.MODIFIED_DATE, Field.SCOPE_GROUP_ID, Field.UID, SUBTITLE,
				DESCRIPTIONFIELD1, VENUE, QUOTETEXT, Field.TITLE);
		setPermissionAware(false);
		setFilterSearch(true);
	}

	@Override
	public String getClassName() {
		return Event.class.getName();
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest,
			PortletResponse portletResponse) throws Exception {
		Summary summary = createSummary(document);
		summary.setMaxContentLength(200);
		return summary;
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		Event event = eventLocalService.getEvent(classPK);
		doReindex(event);

	}

	@Override
	protected void doReindex(String[] ids) throws Exception {
		long companyId = GetterUtil.getLong(ids[0]);
		reindexEmployees(companyId);

	}

	@Override
	protected void doReindex(Event object) throws Exception {
		Document document = getDocument(object);
		indexWriterHelper.updateDocument(getSearchEngineId(), object.getCompanyId(), document, isCommitImmediately());

	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter,
			SearchContext searchContext) throws Exception {
		addSearchLocalizedTerm(searchQuery, searchContext, Field.TITLE, false);
		addSearchLocalizedTerm(searchQuery, searchContext, SUBTITLE, false);
		addSearchLocalizedTerm(searchQuery, searchContext, VENUE, false);
		addSearchLocalizedTerm(searchQuery, searchContext, QUOTETEXT, false);
		addSearchLocalizedTerm(searchQuery, searchContext, DESCRIPTIONFIELD1, false);

	}

	@Override
	protected void doDelete(Event object) throws Exception {
		deleteDocument(object.getCompanyId(), object.getEventId());

	}

	@Override
	protected Document doGetDocument(Event object) throws Exception {

		Document document = getBaseModelDocument(Event.class.getName(), object);

		document.addLocalizedKeyword(Field.TITLE, object.getTitleMap());
		document.addLocalizedKeyword(SUBTITLE, object.getSubtitleMap());
		document.addLocalizedKeyword(VENUE, object.getVenueMap());
		document.addLocalizedKeyword(QUOTETEXT, object.getQuoteTextMap());
		document.addLocalizedKeyword(DESCRIPTIONFIELD1, object.getDescriptionField1Map());

		return document;
	}

	protected void reindexEmployees(long companyId) throws PortalException {
		final IndexableActionableDynamicQuery indexableActionableDynamicQuery = eventLocalService
				.getIndexableActionableDynamicQuery();
		indexableActionableDynamicQuery.setCompanyId(companyId);
		indexableActionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod() {
			@Override
			public void performAction(Object event) {
				try {
					Document document = getDocument((Event) event);
					indexableActionableDynamicQuery.addDocuments(document);
				} catch (PortalException pe) {
					pe.printStackTrace();
				}
			}
		});
		indexableActionableDynamicQuery.setSearchEngineId(getSearchEngineId());
		indexableActionableDynamicQuery.performActions();
	}

	private static final Log log = LogFactoryUtil.getLog(EventIndexer.class);

	@Reference
	EventLocalService eventLocalService;

	@Reference
	protected IndexWriterHelper indexWriterHelper;
}