/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.impl;

import com.liferay.portal.aop.AopService;

import events.exception.NoSuchVisitorsException;
import events.model.EventVisitors;
import events.service.base.EventVisitorsServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the event visitors remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>events.service.EventVisitorsService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventVisitorsServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=event",
		"json.web.service.context.path=EventVisitors"
	},
	service = AopService.class
)
public class EventVisitorsServiceImpl extends EventVisitorsServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>events.service.EventVisitorsServiceUtil</code> to access the event visitors remote service.
	 */
	public EventVisitors geteventVisitors(long visitorid) throws NoSuchVisitorsException {

		return eventVisitorsPersistence.findByPrimaryKey(visitorid);

	}

}