/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexSearcherHelperUtil;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.*;
import com.push.notification.constants.PushNotificationKeys;
import com.push.notification.service.PushNotificationLocalServiceUtil;
import events.exception.*;
import events.model.Event;
import events.model.EventsGallery;
import events.model.Schedule;
import events.service.EventLocalServiceUtil;
import events.service.EventsGalleryLocalServiceUtil;
import events.service.ScheduleLocalServiceUtil;
import events.service.base.EventLocalServiceBaseImpl;
import events.service.indexer.EventIndexer;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The implementation of the event local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>events.service.EventLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventLocalServiceBaseImpl
 */
@Component(property = "model.class.name=events.model.Event", service = AopService.class)
public class EventLocalServiceImpl extends EventLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>events.service.EventLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>events.service.EventLocalServiceUtil</code>.
	 */

	public Event addEvent(long userId, Date eventStartDateTime, Date eventEndDateTime, long tagId, long categoryId, int type, int paytype,
			Map<Locale, String> titleMap, Map<Locale, String> smallImageMap, Map<Locale, String> bigImageMap,
			Date displayDate, Map<Locale, String> venueMap, Map<Locale, String> timingMap,
			Map<Locale, String> durationMap, Map<Locale, String> descriptionField1Map, Map<Locale, String> videoLinkMap,
			Map<Locale, String> descriptionField2Map, Map<Locale, String> subtitleMap,
			Map<Locale, String> descriptionField3Map, Map<Locale, String> detailImageMap,
			Map<Locale, String> quoteTextMap, Map<Locale, String> quoteAuthorMap,
			Map<Locale, String> descriptionField4Map, Map<Locale, String> eventdescription,
			Map<Locale, String> eventfeaturedImage, Map<Locale, String> buyTicketLink,
			String lat, String lan, String eventPrice, String email,
			String phone, String currency,long  orderNo, boolean sendPushNotification, ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(type, titleMap, displayDate, venueMap, timingMap, durationMap);

		long eventId = counterLocalService.increment();

		Event event = eventPersistence.create(eventId);
		// default Value of OrderNo to 9999

		if (orderNo == 0) {
			orderNo = 9999;
		}

		event.setUuid(serviceContext.getUuid());
		event.setUserId(userId);
		event.setGroupId(groupId);
		event.setCompanyId(user.getCompanyId());
		event.setUserName(user.getFullName());
		event.setCreateDate(serviceContext.getCreateDate(now));
		event.setModifiedDate(serviceContext.getModifiedDate(now));
		event.setEventStartDateTime(eventStartDateTime);
		event.setEventEndDateTime(eventEndDateTime);
		event.setTagId(tagId);
		event.setCategoryId(categoryId);
		event.setType(type);
		event.setPaytype(paytype);
		event.setTitleMap(titleMap);
		event.setSmallImageMap(smallImageMap);
		event.setBigImageMap(bigImageMap);
		event.setDisplayDate(displayDate);
		event.setVenueMap(venueMap);
		event.setTimingMap(timingMap);
		event.setDurationMap(durationMap);
		event.setDescriptionField1Map(descriptionField1Map);
		event.setVideoLinkMap(videoLinkMap);
		event.setDescriptionField2Map(descriptionField2Map);
		event.setSubtitleMap(subtitleMap);
		event.setDescriptionField3Map(descriptionField3Map);
		event.setDetailImageMap(detailImageMap);
		event.setQuoteTextMap(quoteTextMap);
		event.setQuoteAuthorMap(quoteAuthorMap);
		event.setDescriptionField4Map(descriptionField4Map);
		event.setExpandoBridgeAttributes(serviceContext);
		event.setEventdescriptionMap(eventdescription);
		event.setEventfeaturedImageMap(eventfeaturedImage);
		event.setBuyTicketLinkMap(buyTicketLink);
		event.setLat(lat);
		event.setLan(lan);
		event.setEventPrice(eventPrice);
		event.setEmail(email);
		event.setPhone(phone);
		event.setCurrency(currency);
		event.setOrderNo(orderNo);
		Event addedEntry = eventPersistence.update(event);
		eventPersistence.clearCache();
		if(sendPushNotification){
			// send item added push notification
			if(Validator.isNotNull(addedEntry)) {
				String notificationBody = "New Event: \""+addedEntry.getTitle("en_US")+"\".";
				PushNotificationLocalServiceUtil.saveNotification(PushNotificationKeys.NOTIFICATION_TYPE_EVENT, addedEntry.getEventId(), addedEntry.getTitle(), notificationBody, null);
			}
		}
		return event;
	}

	public Event updateEvent(long userId, long eventId, Date eventStartDateTime, Date eventEndDateTime, long tagId, long categoryId,
			int type, int paytype, Map<Locale, String> titleMap, Map<Locale, String> smallImageMap, Map<Locale, String> bigImageMap,
			Date displayDate, Map<Locale, String> venueMap, Map<Locale, String> timingMap,
			Map<Locale, String> durationMap, Map<Locale, String> descriptionField1Map, Map<Locale, String> videoLinkMap,
			Map<Locale, String> descriptionField2Map, Map<Locale, String> subtitleMap,
			Map<Locale, String> descriptionField3Map, Map<Locale, String> detailImageMap,
			Map<Locale, String> quoteTextMap, Map<Locale, String> quoteAuthorMap,
			Map<Locale, String> descriptionField4Map, Map<Locale, String> eventdescription,
			Map<Locale, String> eventfeaturedImage, Map<Locale, String> buyTicketLink,
			String lat, String lan, String eventPrice, String email,
			String phone, String currency, long  orderNo, boolean sendPushNotification, ServiceContext serviceContext) throws PortalException, SystemException {
		long groupId = serviceContext.getScopeGroupId();

		Date now = new Date();

		validate(type, titleMap, displayDate, venueMap, timingMap, durationMap);

		Event event = getEvent(eventId);

		User user = userLocalService.getUser(userId);
		// default Value of OrderNo to 9999

		if (orderNo == 0) {
			orderNo = 9999;
		}
		event.setUserId(userId);
		event.setUserName(user.getFullName());
		event.setModifiedDate(serviceContext.getModifiedDate(now));
		event.setEventStartDateTime(eventStartDateTime);
		event.setEventEndDateTime(eventEndDateTime);
		event.setTagId(tagId);
		event.setCategoryId(categoryId);
		event.setType(type);
		event.setPaytype(paytype);
		event.setTitleMap(titleMap);
		event.setSmallImageMap(smallImageMap);
		event.setBigImageMap(bigImageMap);
		event.setDisplayDate(displayDate);
		event.setVenueMap(venueMap);
		event.setTimingMap(timingMap);
		event.setDurationMap(durationMap);
		event.setDescriptionField1Map(descriptionField1Map);
		event.setVideoLinkMap(videoLinkMap);
		event.setDescriptionField2Map(descriptionField2Map);
		event.setSubtitleMap(subtitleMap);
		event.setDescriptionField3Map(descriptionField3Map);
		event.setBuyTicketLinkMap(buyTicketLink);
		event.setDetailImageMap(detailImageMap);
		event.setQuoteTextMap(quoteTextMap);
		event.setQuoteAuthorMap(quoteAuthorMap);
		event.setDescriptionField4Map(descriptionField4Map);
		event.setExpandoBridgeAttributes(serviceContext);
		event.setEventdescriptionMap(eventdescription);
		event.setEventfeaturedImageMap(eventfeaturedImage);
		event.setOrderNo(orderNo);
		event.setLat(lat);
		event.setLan(lan);
		event.setEmail(email);
		event.setPhone(phone);
		event.setEventPrice(eventPrice);
		event.setCurrency(currency);
		Event updatedEntry = eventPersistence.update(event);
		eventPersistence.clearCache();
		if(sendPushNotification){
			// send item updated push notification
			if(Validator.isNotNull(updatedEntry)) {
				String notificationBody = "Event update: \""+updatedEntry.getTitle("en_US")+"\".";
				PushNotificationLocalServiceUtil.saveNotification(PushNotificationKeys.NOTIFICATION_TYPE_EVENT_UPDATE, updatedEntry.getEventId(), updatedEntry.getTitle(), notificationBody, null);
			}
		}

		return event;
	}

	public Event deleteEvent(long eventId, ServiceContext serviceContext) throws PortalException, SystemException {

		Event event = getEvent(eventId);
		Event deletedEntry = null;
		if(Validator.isNotNull(event)){
			deletedEntry = deleteEvent(event);
		}

		return deletedEntry;
	}

	public List<Event> getEvents(long groupId) {

		return removeExpiredEvent(eventPersistence.findByGroupId(groupId));
	}

	public List<Event> getEvents(long groupId, int start, int end, OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByGroupId(groupId, start, end, obc));
	}

	public List<Event> getEvents(long groupId, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByGroupId(groupId, start, end));
	}
	
	public List<Event> getEventsIncludeExpiredEvents(long groupId, int start, int end) {

		return eventPersistence.findByGroupId(groupId, start, end);
	}

	public int getEventsCount(long groupId) {
		List<Event> events = getEvents(groupId);
		return events.size();
		//return eventPersistence.countByGroupId(groupId);
	}

	protected void validate(int type, Map<Locale, String> titleMap, Date displayDate, Map<Locale, String> venueMap,
			Map<Locale, String> timingMap, Map<Locale, String> durationMap) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		if (Validator.isNull(type)) {
			throw new EventTypeException();
		}

		String title = titleMap.get(locale);

		if (Validator.isNull(title)) {
			throw new EventTitleException();
		}
		if (Validator.isNull(displayDate)) {
			throw new EventDisplayDateException();
		}

		String venue = venueMap.get(locale);

		if (Validator.isNull(venue)) {
			throw new EventVenueException();
		}

		String timing = timingMap.get(locale);

		if (Validator.isNull(timing)) {
			throw new EventTimingException();
		}

		String duration = durationMap.get(locale);

		if (Validator.isNull(duration)) {
			throw new EventDurationException();
		}

	}

	/*
	 * * find events by title
	 */
	public List<Event> findByTitle(String title) {

		return removeExpiredEvent(eventPersistence.findByTitle('%' + title + '%'));
	}

	public List<Event> findByTitle(String title, int start, int end, OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByTitle('%' + title + '%', start, end, obc));
	}

	public List<Event> findByTitle(String title, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByTitle('%' + title + '%', start, end));
	}

	public int findByTitleCount(String title) {
		List<Event> events = findByTitle(title);
		return events.size();
		//return eventPersistence.countByTitle('%' + title + '%');
	}

	public List<Event> findByTagId(long tagId) {

		return removeExpiredEvent(eventPersistence.findByTagId(tagId));
	}

	public List<Event> findByTagId(long tagId, int start, int end, OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByTagId(tagId, start, end, obc));
	}

	public List<Event> findByTagId(long tagId, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByTagId(tagId, start, end));
	}

	public int countByTagId(long tagId) {
		List<Event> events = findByTagId(tagId);
		return events.size();
		//return eventPersistence.countByTagId(tagId);
	}
	/*
	 * **********************************- START: Events Gallery Code
	 * -*****************************
	 */

	public void addEntryGallery(Event orgEntry, PortletRequest request) throws PortletException {

		String galleryIndexesString = ParamUtil.getString(request, "galleryIndexes");

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);
	}

	@Override
	public void updateEventsGalleries(Event entry, List<EventsGallery> galleries, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> galleryIds = new HashSet<>();

		for (EventsGallery gallery : galleries) {
			long galleryId = gallery.getSlideId();

			// additional step to set Ids - initially zero
			gallery.setEventId(entry.getEventId());
			gallery.setUserId(entry.getUserId());
			gallery.setCompanyId(entry.getCompanyId());
			gallery.setGroupId(entry.getGroupId());

			if (galleryId <= 0) {
				gallery = EventsGalleryLocalServiceUtil.addEntry(gallery, serviceContext);

				galleryId = gallery.getSlideId();
			} else {
				EventsGalleryLocalServiceUtil.updateEntry(gallery, serviceContext);
			}

			galleryIds.add(galleryId);
		}

		galleries = EventsGalleryLocalServiceUtil.findAllInDiscover(entry.getEventId());

		for (EventsGallery gallery : galleries) {
			if (!galleryIds.contains(gallery.getSlideId())) {
				EventsGalleryLocalServiceUtil.deleteEventsGallery(gallery.getSlideId());
			}
		}
	}

	@Override
	public List<EventsGallery> getGalleries(ActionRequest actionRequest) {
		return getGalleries(actionRequest, Collections.<EventsGallery>emptyList());
	}

	@Override
	public List<EventsGallery> getGalleries(ActionRequest actionRequest, List<EventsGallery> defaultGalleries) {

		String galleryIndexesString = ParamUtil.getString(actionRequest, "galleryIndexes");

		if (galleryIndexesString == null) {
			return defaultGalleries;
		}

		List<EventsGallery> galleries = new ArrayList<>();

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);

		for (int galleryIndex : galleryIndexes) {
			String image = ParamUtil.getString(actionRequest, "galleryImage" + galleryIndex);

			if (Validator.isNull(image)) {
				continue;
			}

			String thumbnail = ParamUtil.getString(actionRequest, "galleryThumbnail" + galleryIndex);

			long galleryId = ParamUtil.getLong(actionRequest, "galleryId" + galleryIndex);

			EventsGallery gallery = EventsGalleryLocalServiceUtil.createEventsGallery(galleryId);

			gallery.setEventId(0);
			gallery.setImage(image);
			gallery.setThumbnail(thumbnail);

			galleries.add(gallery);
		}

		return galleries;
	}

	/*
	 * **********************************- END: Events Gallery Code
	 * -*****************************
	 */
	/*
	 * **********************************- START: Event Schedule Code
	 * -*****************************
	 */
	public void addEntrySchedule(Event orgEntry, PortletRequest request) throws PortletException {

		String scheduleIndexesString = ParamUtil.getString(request, "scheduleIndexes");

		int[] scheduleIndexes = StringUtil.split(scheduleIndexesString, 0);
	}

	@Override
	public void updateSchedules(Event entry, List<Schedule> schedules, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> scheduleIds = new HashSet<>();

		for (Schedule schedule : schedules) {
			long scheduleId = schedule.getScheduleId();

			// additional step to set Ids - initially zero
			schedule.setEventId(entry.getEventId());
			schedule.setUserId(entry.getUserId());
			schedule.setCompanyId(entry.getCompanyId());
			schedule.setGroupId(entry.getGroupId());

			if (scheduleId <= 0) {
				schedule = ScheduleLocalServiceUtil.addEntry(schedule, serviceContext);

				scheduleId = schedule.getScheduleId();
			} else {
				ScheduleLocalServiceUtil.updateEntry(schedule, serviceContext);
			}

			scheduleIds.add(scheduleId);
		}

		schedules = ScheduleLocalServiceUtil.findAllInEvent(entry.getEventId());

		for (Schedule schedule : schedules) {
			if (!scheduleIds.contains(schedule.getScheduleId())) {
				ScheduleLocalServiceUtil.deleteSchedule(schedule.getScheduleId());
			}
		}
	}

	@Override
	public List<Schedule> getSchedules(ActionRequest actionRequest) {
		return getSchedules(actionRequest, Collections.<Schedule>emptyList());
	}

	@Override
	public List<Schedule> getSchedules(ActionRequest actionRequest, List<Schedule> defaultSchedules) {

		String scheduleIndexesString = ParamUtil.getString(actionRequest, "scheduleIndexes");

		if (scheduleIndexesString == null) {
			return defaultSchedules;
		}

		List<Schedule> schedules = new ArrayList<>();

		int[] scheduleIndexes = StringUtil.split(scheduleIndexesString, 0);

		for (int scheduleIndex : scheduleIndexes) {
			String title = ParamUtil.getString(actionRequest, "scheduleTitle" + scheduleIndex);
			String description = ParamUtil.getString(actionRequest, "scheduleDescription" + scheduleIndex);

			if (Validator.isNull(title) || Validator.isNull(description)) {
				continue;
			}

			String duration = ParamUtil.getString(actionRequest, "scheduleDuration" + scheduleIndex);
			String image = ParamUtil.getString(actionRequest, "scheduleImage" + scheduleIndex);

			long scheduleId = ParamUtil.getLong(actionRequest, "scheduleId" + scheduleIndex);

			Schedule schedule = ScheduleLocalServiceUtil.createSchedule(scheduleId);

			schedule.setEventId(0);
			schedule.setTitle(title);
			schedule.setDescription(description);
			schedule.setDuration(duration);
			schedule.setImage(image);

			schedules.add(schedule);
		}

		return schedules;
	}
	/*
	 * **********************************- END: Event Schedule Code
	 * -*****************************
	 */

	/*
	 * * get events by category
	 */
	public List<Event> getEventsByCategory(long categoryId) {

		return removeExpiredEvent(eventPersistence.findByCategoryId(categoryId));
	}

	public List<Event> getEventsByCategory(long categoryId, int start, int end, OrderByComparator<Event> obc) {

		return removeExpiredEvent(eventPersistence.findByCategoryId(categoryId, start, end, obc));
	}

	public List<Event> getEventsByCategory(long categoryId, int start, int end) {

		return removeExpiredEvent(eventPersistence.findByCategoryId(categoryId, start, end));
	}

	public int getEventsByCategoryCount(long categoryId) {
		List<Event> events = eventPersistence.findByCategoryId(categoryId);
		return events.size();
		//return eventPersistence.countByCategoryId(categoryId);
	}

	// search
	@Override
	public List<Event> searchEvents(SearchContext searchContext) {

		Hits hits;

		List<Event> eventList = new ArrayList<>();
		EventIndexer indexer = (EventIndexer) IndexerRegistryUtil.getIndexer(Event.class);
		try {

			hits = indexer.search(searchContext);

			for (int i = 0; i < hits.getDocs().length; i++) {
				Document doc = hits.doc(i);
				long eventId = GetterUtil.getLong(doc.get(Field.ENTRY_CLASS_PK));
				Event event = null;
				event = EventLocalServiceUtil.getEvent(eventId);

				eventList.add(event);
			}
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}

		return removeExpiredEvent(eventList);
	}

	/*
	 * @Reference private PushNotificationLocalService
	 * pushNotificationLocalServices;
	 */

	public List<Event> findByFilter(int payType, String priceFrom, String priceTo, long categoryId, String dateFrom, String dateTo, int offset, int limit) {

		return eventFinder.findByFilter(payType, priceFrom, priceTo, categoryId, dateFrom, dateTo, offset, limit);
	}

	public int countByFilter(int payType, String priceFrom, String priceTo, long categoryId, String dateFrom, String dateTo) {

		return eventFinder.countByFilter(payType, priceFrom, priceTo, categoryId, dateFrom, dateTo);
	}

	public List<Event> removeExpiredEvent(List<Event> eventList) {
		List<Event> excludeExpireEventList = new ArrayList<Event>();
		if (Validator.isNotNull(eventList) && eventList.size() > 0) {
			excludeExpireEventList = eventList.stream().filter(p -> p.getEventEndDateTime().after(new Date()))
					.collect(Collectors.toList());
		}
		return excludeExpireEventList;
	}

}