/*var eventData = {
    "title": "ebay live auction test",
    "desc": "this is a live auction test \n testing new line.",
    "location": "my house",
    "url": "http://www.ebay.com",
    "time": {
        "start": "March 12, 2014 14:00:00",
        "end": "march 13, 2014 15:30:00",
        "zone": "+03:00",
        "allday": false
    },
};*/

$(function() {
    function adjustToUTC(dateObj, zone) {
        var dateOut = new Date(dateObj),
            hours, mins;

        if (isNaN(dateOut.getTime())) {
            return new Date();
        }

        // adjust to UTC
        hours = zone.substring(1, 3);
        mins = zone.substring(4, 6);
        if (zone.substring(0, 1) === '-') {
            dateOut.setHours(dateOut.getHours() + (hours - 0));
            dateOut.setMinutes(dateOut.getMinutes() + (mins - 0));
        } else {
            dateOut.setHours(dateOut.getHours() - hours);
            dateOut.setMinutes(dateOut.getMinutes() - mins);
        }
        return dateOut;
    }

    function getDatePart(part, digits) {
        part = part.toString();
        while (part.length < digits) {
            part = '0' + part;
        }
        return part;
    }

    function getUTCTime(dateObj) {
        var newDateObj = adjustToUTC(dateObj, eventData.time.zone);
        return getDatePart(newDateObj.getFullYear(), 4) + getDatePart(newDateObj.getMonth() + 1, 2) + getDatePart(newDateObj.getDate(), 2) + 'T' + getDatePart(newDateObj.getHours(), 2) + getDatePart(newDateObj.getMinutes(), 2) + getDatePart(newDateObj.getSeconds(), 2) + 'Z';
    }

    function prepareEvent(data) {
        var tData = "";
        tData += "BEGIN:VCALENDAR\r\n";
        tData += "VERSION:2.0\r\n";
        tData += "PRODID:-//Miskcity//NONSGML v1.0//EN\r\n";
        tData += "METHOD:PUBLISH\r\n";
        tData += "BEGIN:VEVENT\r\n";
        tData += "SUMMARY:" + data.title + "\r\n";
        tData += "DESCRIPTION:" + data.desc + "\r\n";
        tData += "LOCATION:" + data.location + "\r\n";
        tData += "URL:" + data.url + "\r\n";
        tData += "UID:00" + Math.floor(Math.random() * 10000000) + "-Custom@test\r\n";
        tData += "SEQUENCE:0\r\n";
        tData += "DTSTAMP:" + getUTCTime(data.time.start) + "\r\n";
        tData += "DTSTART:" + getUTCTime(data.time.start) + "\r\n";
        tData += "DTEND:" + getUTCTime(data.time.end) + "\r\n";
        tData += "END:VEVENT\r\n";
        tData += "END:VCALENDAR\r\n";
        return tData;
    }

    $('.add-to-calendar-btn').click(function () {
        var icsMSG = prepareEvent(eventData);
        $(this).attr("href", "data:text/calendar;charset=utf8," + encodeURIComponent(icsMSG));
        //window.open("data:text/calendar;charset=utf8," + escape(icsMSG));
    });
});