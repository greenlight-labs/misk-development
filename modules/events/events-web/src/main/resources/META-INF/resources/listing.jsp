<%
    List<Event> events;
    int type = 1;
    boolean isQuerySearch = false;
    if(Validator.isNotNull(query)){
        //@MY_TODO: create findByTitle_Type and query posts with it as done in stories
        events = EventServiceUtil.findByTitle(query, start, end);
        isQuerySearch = true;
    }else{
        events = EventServiceUtil.getEventsByType(type, start, end);
    }
%>
<%
    if(events.size() > 0 || isQuerySearch){
%>
    <%@include file="/includes/search.jsp" %>
<%
    }
%>
<div class="row">
    <%
        if(events.size() > 0){
            for (Event curEvent : events) {
    %>
    <portlet:renderURL var="detailPageURL2">
        <portlet:param name="eventId" value="<%= String.valueOf(curEvent.getEventId()) %>" />
        <portlet:param name="mvcPath" value="/detail.jsp" />
    </portlet:renderURL>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="list-box animate" data-animation="fadeInUp" data-duration="300">
            <div class="image-container">
                <a class="image-overlay" href="<%= detailPageURL2.toString() %>">
                    <% if( Validator.isNotNull(curEvent.getSmallImage(locale)) ){ %>
                        <img src="<%= curEvent.getSmallImage(locale) %>" class="img-fluid" alt="news1">
                    <% }else{ %>
                        <img src="/assets/images/no-image-available.png" class="img-fluid" alt="">
                    <% } %>
                </a>
                <div class="date-box">
                    <h3><%= day.format(curEvent.getDisplayDate()) %></h3>
                    <p><%= shortMonthYear.format(curEvent.getDisplayDate()) %></p>
                </div>
            </div>
            <div class="news-info">
                <p class="venue-time-text"><span class="text-info">Venue and Time:</span> <%= curEvent.getVenue(locale) %> <span class="venue-time-separator">|</span> <%= curEvent.getTiming(locale) %></p>
                <h4>
                    <a href="<%= detailPageURL2.toString() %>"><%= curEvent.getTitle(locale) %></a>
                </h4>
            </div>
        </div>
    </div>
    <%
        }
    } else {
            if( isQuerySearch ) {
    %>
                <div class="text-center">
                    <p><liferay-ui:message key="misk-no-record-found" /></p>
                </div>
    <%
            } else {
    %>
                <div class="text-center">
                    <p><liferay-ui:message key="misk-events-coming-check-back-later" /></p>
                </div>
    <%
            }
        }
    %>
</div>