<%@ include file="/init.jsp" %>
<%
    int latestHappeningType = 2;
    List<Event> latestEvents = EventServiceUtil.getEventsByType(latestHappeningType, 0, 2);
    int count = latestEvents.size();
%>

<%
    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String editMode = originalRequest.getParameter("p_l_mode");
%>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
    <div class="row">
        <div class="col-12">
            <div class="page-editor__collection__block">
                <div class="page-editor__collection-item page-editor__topper">
                    <div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title">Latest Events Module</p></div>
                    <br><br>
                    <div class="alert alert-info portlet-configuration">
                        <aui:a href="<%= portletDisplay.getURLConfiguration() %>" label="please-configure-this-portlet-to-make-it-visible-to-all-users" onClick="<%= portletDisplay.getURLConfigurationJS() %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } else { %>

<% if(count > 0 ) { %>

<section class="happening-section" data-scroll-section>
    <div class="bg-gray-wrap animate" data-animation="fadeInUp" data-duration="500"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="related-news-heading">
                    <h3><%= sectionTitlePortlet1.replaceAll("\n", "<br/>") %></h3>
                    <% if(buttonLabelPortlet1 != null && buttonLinkPortlet1 != null) { %>
                        <a href="<%= buttonLinkPortlet1 %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><%= buttonLabelPortlet1 %></span><i class="dot-line"></i></a>
                    <% } %>
                </div>
            </div>
            <div class="col-md-5">
                <div class="happening-box">
                    <portlet:renderURL var="detailPageURL">
                        <portlet:param name="eventId" value="<%= String.valueOf(latestEvents.get(0).getEventId()) %>" />
                        <portlet:param name="mvcPath" value="/detail.jsp" />
                    </portlet:renderURL>
                    <a href="<%= detailPageURL.toString() %>" class="happening-img-inno image-overlay">
                        <img src="<%= latestEvents.get(0).getBigImage(locale) %>" alt="" class="img-fluid">
                        <div class="date-box date-box-big">
                            <h3><%= day.format(latestEvents.get(0).getDisplayDate()) %></h3>
                            <p><%= shortMonthYear.format(latestEvents.get(0).getDisplayDate()) %></p>
                        </div>
                    </a>
                    <div class="happening-img-content">
                        <p class="text-info heading">Venue and Time:</p>
                        <p class="venue-time-text"> <%= latestEvents.get(0).getVenue(locale) %> <span class="venue-time-separator">|</span><%= latestEvents.get(0).getTiming(locale) %></p>
                        <h4 class="mb-0"><a href="<%= detailPageURL.toString() %>"><%= latestEvents.get(0).getTitle(locale) %></a></h4>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <% if(count > 1 && latestEvents.get(1) != null ) { %>
                        <portlet:renderURL var="detailPageURL2">
                            <portlet:param name="eventId" value="<%= String.valueOf(latestEvents.get(1).getEventId()) %>" />
                            <portlet:param name="mvcPath" value="/detail.jsp" />
                        </portlet:renderURL>
                        <div class="col-md-6">
                            <div class="happening-box small-happen-box">
                                <a href="<%= detailPageURL2.toString() %>" class="happening-img-inno image-overlay">
                                    <img src="<%= latestEvents.get(1).getSmallImage(locale) %>" alt="" class="img-fluid">
                                    <div class="date-box">
                                        <h3><%= day.format(latestEvents.get(1).getDisplayDate()) %></h3>
                                        <p><%= shortMonthYear.format(latestEvents.get(1).getDisplayDate()) %></p>
                                    </div>
                                </a>
                                <div class="happening-img-content">
                                    <p class="text-info heading">Venue and Time:</p>
                                    <p class="venue-time-text">
                                        <%= latestEvents.get(1).getVenue(locale) %>
                                        <span class="venue-time-separator">|</span>
                                        <%= latestEvents.get(1).getVenue(locale).length() > 17 ? "<br class=\"d-none d-lg-block\">" : "" %>
                                        <%= latestEvents.get(1).getTiming(locale) %>
                                    </p>
                                    <h4 class="mb-0"><a href="<%= detailPageURL2.toString() %>"><%= latestEvents.get(1).getTitle(locale) %></a></h4>
                                </div>
                            </div>
                        </div>
                    <% } %>
                    <% if(count > 2 && latestEvents.get(2) != null ) { %>
                        <portlet:renderURL var="detailPageURL3">
                            <portlet:param name="eventId" value="<%= String.valueOf(latestEvents.get(2).getEventId()) %>" />
                            <portlet:param name="mvcPath" value="/detail.jsp" />
                        </portlet:renderURL>
                        <div class="col-md-6">
                            <div class="happening-box small-happen-box">
                                <a href="<%= detailPageURL3.toString() %>" class="happening-img-inno image-overlay">
                                    <img src="<%= latestEvents.get(2).getSmallImage(locale) %>" alt="" class="img-fluid">
                                    <div class="date-box">
                                        <h3><%= day.format(latestEvents.get(2).getDisplayDate()) %></h3>
                                        <p><%= shortMonthYear.format(latestEvents.get(2).getDisplayDate()) %></p>
                                    </div>
                                </a>
                                <div class="happening-img-content">
                                    <p class="text-info heading">Venue and Time:</p>
                                    <p class="venue-time-text">
                                        <%= latestEvents.get(2).getVenue(locale) %>
                                        <span class="venue-time-separator">|</span>
                                        <%= latestEvents.get(2).getVenue(locale).length() > 17 ? "<br class=\"d-none d-lg-block\">" : "" %>
                                        <%= latestEvents.get(2).getTiming(locale) %>
                                    </p>
                                    <h4 class="mb-0"><a href="<%= detailPageURL3.toString() %>"><%= latestEvents.get(2).getTitle(locale) %></a></h4>
                                </div>
                            </div>
                        </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</section>

<% } %>
<% } %>