<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.*" %>
<%@ page import="events.model.Event" %>
<%@ page import="events.service.EventLocalServiceUtil" %>
<%@ page import="events.service.EventServiceUtil" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.text.Format" %>
<%@ page import="java.util.List" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM dd, yyyy", locale, timeZone);
    Format day = FastDateFormatFactoryUtil.getSimpleDateFormat("dd", locale, timeZone);
    Format monthYear = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM, yyyy", locale, timeZone);
    Format shortMonthYear = FastDateFormatFactoryUtil.getSimpleDateFormat("MMM, yyyy", locale, timeZone);
    Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM dd, yyyy HH:mm", locale, timeZone);

    // Latest Happening Section Preference - Events Page
    String sectionTitlePortlet1Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "sectionTitle", LanguageUtil.get(locale, "events_web_portlet1_config_section_title")));
    String buttonLabelPortlet1Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLabel", LanguageUtil.get(locale, "events_web_portlet1_config_button_label")));
    String buttonLinkPortlet1Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLink", LanguageUtil.get(locale, "events_web_portlet1_config_button_link")));

    String sectionTitlePortlet1 = LocalizationUtil.getLocalization(sectionTitlePortlet1Xml, themeDisplay.getLanguageId());
    String buttonLabelPortlet1 = LocalizationUtil.getLocalization(buttonLabelPortlet1Xml, themeDisplay.getLanguageId());
    String buttonLinkPortlet1 = LocalizationUtil.getLocalization(buttonLinkPortlet1Xml, themeDisplay.getLanguageId());
    // Latest Events Slider Preference - Media Center and Other Pages
    String sectionTitlePortlet2Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "sectionTitle", LanguageUtil.get(locale, "events_web_portlet2_config_section_title")));
    String buttonLabelPortlet2Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLabel", LanguageUtil.get(locale, "events_web_portlet2_config_button_label")));
    String buttonLinkPortlet2Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLink", LanguageUtil.get(locale, "events_web_portlet2_config_button_link")));

    String sectionTitlePortlet2 = LocalizationUtil.getLocalization(sectionTitlePortlet2Xml, themeDisplay.getLanguageId());
    String buttonLabelPortlet2 = LocalizationUtil.getLocalization(buttonLabelPortlet2Xml, themeDisplay.getLanguageId());
    String buttonLinkPortlet2 = LocalizationUtil.getLocalization(buttonLinkPortlet2Xml, themeDisplay.getLanguageId());

    int limit = 9;
    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String query = originalRequest.getParameter("q");
    int start = ParamUtil.getInteger(request, "start");
    int end = ParamUtil.getInteger(request, "end");
    end = end == 0 ? limit : end;
%>