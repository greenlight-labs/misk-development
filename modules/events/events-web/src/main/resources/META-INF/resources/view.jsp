<%@ include file="/init.jsp" %>

<%--<%@include file="includes/top_section.jsp" %>--%>

<%--<%@include file="/latest_events.jsp" %>--%>

<%
    /*HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String query = originalRequest.getParameter("q");*/
%>

<section class="latest-news-section" data-scroll-section>
    <div class="container">

        <%@include file="/listing.jsp" %>

        <%@include file="/includes/pagination.jsp" %>
    </div>
</section>
