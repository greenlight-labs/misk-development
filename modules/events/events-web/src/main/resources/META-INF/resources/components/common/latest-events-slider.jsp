<%@ include file="/init.jsp" %>
<%
    List<Event> latestEvents = EventServiceUtil.getEvents(scopeGroupId, 0, 9);
    int count = latestEvents.size();
%>

<%
    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String editMode = originalRequest.getParameter("p_l_mode");
%>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
    <div class="row">
        <div class="col-12">
            <div class="page-editor__collection__block">
                <div class="page-editor__collection-item page-editor__topper">
                    <div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title">Latest Events Slider Module</p></div>
                    <br><br>
                    <div class="alert alert-info portlet-configuration">
                        <aui:a href="<%= portletDisplay.getURLConfiguration() %>" label="please-configure-this-portlet-to-make-it-visible-to-all-users" onClick="<%= portletDisplay.getURLConfigurationJS() %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } else { %>

<% if(count > 0 ) { %>

<section class="latest-news-slider-section lns-media-center" data-scroll-section>
    <div class="slider-right-shape">
        <img src="/assets/svgs/yellow-shape.svg" class="img-fluid" alt="triangle">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="related-news-heading">
                    <% if(themeDisplay.getLanguageId().equals("ar_SA")) { %>
                        <h3><%= sectionTitlePortlet2.replaceAll("\n", "<br/>") %></h3>
                        <% if(buttonLabelPortlet2 != null && buttonLinkPortlet2 != null) { %>
                            <a href="<%= buttonLinkPortlet2 %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><liferay-ui:message key="events_web_portlet2_config_button_label"/></span><i class="dot-line"></i></a>
                        <% } %>
                    <% }else{ %>
                        <h3><%= sectionTitlePortlet2.replaceAll("\n", "<br/>") %></h3>
                        <% if(buttonLabelPortlet2 != null && buttonLinkPortlet2 != null) { %>
                            <a href="<%= buttonLinkPortlet2 %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><%= buttonLabelPortlet2 %></span><i class="dot-line"></i></a>
                        <% } %>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="latest-news-slider">
        <% for (Event curEvent : latestEvents){ %>
        <portlet:renderURL var="detailPageURL4">
            <portlet:param name="eventId" value="<%= String.valueOf(curEvent.getEventId()) %>" />
            <portlet:param name="mvcPath" value="/detail.jsp" />
        </portlet:renderURL>
            <div class="latest-news-body">
            <div class="latest-news-image-box">
                <a href="<%= detailPageURL4.toString() %>" class="image-overlay"><img src="<%= curEvent.getBigImage(locale) %>" class="img-fluid" alt=""></a>
                <div class="date-box">
                    <h3><%= day.format(curEvent.getDisplayDate()) %></h3>
                    <p><%= shortMonthYear.format(curEvent.getDisplayDate()) %></p>
                </div>
            </div>
            <div class="latest-news-info-box">
                <p class="venue-time-text"><span class="text-info"><liferay-ui:message key="events_web_venue_and_time" /></span> <%= curEvent.getVenue(locale) %> <span class="venue-time-separator">|</span> <%= curEvent.getTiming(locale) %></p>
                <p class="info-box-title mb-4"><a href="<%= detailPageURL4.toString() %>"><%= curEvent.getTitle(locale) %></a></p>
            </div>
        </div>
        <% } %>
    </div>
</section>

<% } %>
<% } %>