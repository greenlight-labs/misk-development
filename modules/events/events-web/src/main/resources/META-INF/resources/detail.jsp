<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@include file="/init.jsp" %>

<%
    long eventId = ParamUtil.getLong(renderRequest, "eventId");

    Event event = null;
    if (eventId > 0) {
        try {
            event = EventLocalServiceUtil.getEvent(eventId);
        } catch (PortalException e) {
            //e.printStackTrace();
        }
    }
%>

<%
    if (event == null) {
%>
<h4>No Record Found.</h4>
<%
} else {
%>
<section class="news-detail-content" data-scroll-section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="offset-md-1"></div>
            <div class="col-md-10">
                <div class="news-detail-header">
                    <div class="news-detail-date">
                        <h2><%= day.format(event.getDisplayDate()) %></h2>
                        <p><%= shortMonthYear.format(event.getDisplayDate()) %></p>
                    </div>
                    <div class="col-7 d-md-none">
                        <div class="news-detail-header-buttons event-page-mobile-header-buttons">
                            <div class="sharethis-inline-share-buttons share-button"></div>
                        </div>
                        <div class="news-detail-header-buttons mt-3">
                            <a href="javascript:" class="share-button add-to-calendar-btn">
                                <img src="/assets/svgs/calender-icon.svg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="news-detail-header-content">
                        <p class="news-sub-head text-primary">Event</p>
                        <h3><%= event.getTitle(locale) %></h3>
                        <div class="event-detail-time">
                            <p class="venue-time-text"><span class="text-info">Venue and Time:</span> <%= event.getVenue(locale) %> <span class="venue-time-separator">|</span> <%= event.getTiming(locale) %></p>
                            <p class="venue-time-text"><span class="text-info">Duration:</span> <%= event.getDuration(locale) %></p>
                            <input type="hidden" name="txtTimeFrom">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1 d-md-block d-none">
                <div class="news-detail-header-buttons">
                    <div class="sharethis-inline-share-buttons share-button"></div>
                </div>
                <div class="news-detail-header-buttons mt-3">
                    <a href="javascript:" class="share-button add-to-calendar-btn">
                        <img src="/assets/svgs/calender-icon.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="news-detail-text">
                    <%= event.getDescriptionField1(locale) %>
                </div>
            </div>
        </div>
    </div>
</section>

<%--show section if videoLink is not empty--%>
<% if (Validator.isNotNull(event.getVideoLink(locale))) { %>
<section class="video-section d-flex align-items-center" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="hs-responsive-embed">
                    <%--check if videoLink contains youtube or vimeo link then display appropriate iframe--%>
                    <% if (event.getVideoLink(locale).contains("youtube")) { %>
                        <iframe class="video-responsive" src="<%= event.getVideoLink(locale) %>?controls=0?rel=0&amp;showinfo=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <% } else if (event.getVideoLink(locale).contains("vimeo")) { %>
                        <iframe class="video-responsive" src="<%= event.getVideoLink(locale) %>?h=95564dbfbf&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                    <% } else{ %>
                        <iframe class="video-responsive" src="<%= event.getVideoLink(locale) %>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</section>
<% } %>

<section class="news-detail-content" data-scroll-section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <%= event.getDescriptionField2(locale) %>
                <h4><%= event.getSubtitle(locale) %></h4>
                <%= event.getDescriptionField3(locale) %>

                <div class="image-wrap">
                    <img class="img-fluid" src="<%= event.getDetailImage(locale) %>" alt="">
                </div>
                <div class="author-content">
                    <div class="blue-box"><img src="/assets/svgs/quote.svg" alt=""></div>
                    <div class="author-data">
                        <h4><%= event.getQuoteText(locale) %></h4>
                        <p><%= event.getQuoteAuthor(locale) %></p>
                    </div>
                </div>
                <%= event.getDescriptionField4(locale) %>
                <div class="share-wrap">
                    <div class="share-box">
                        <a href="javascript:" class="share-link">Share</a>
                        <div class="news-detail-header-buttons bottom-btn">
                            <div class="sharethis-inline-share-buttons all-share-btn share-button"></div>
                        </div>
                    </div>
                    <div class="line-separator ls-event"></div>
                    <a href="javascript:" class="add-link add-to-calendar-btn">Add to Calender</a>
                    <a href="javascript:" class="share-button add-to-calendar-btn">
                        <img src="/assets/svgs/calender-icon.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<%@ include file="includes/related_posts_section.jsp" %>

<portlet:renderURL var="detailPageURL3">
    <portlet:param name="eventId" value="<%= String.valueOf(event.getEventId()) %>" />
    <portlet:param name="mvcPath" value="/detail.jsp" />
</portlet:renderURL>

<script type="text/javascript">
    var eventData = {
        "title": "<%= event.getTitle(locale) %>",
        "desc": "Location: <%= event.getVenue(locale) %> \n Timing: <%= event.getTiming(locale) %> \n Duration: <%= event.getDuration(locale) %>",
        "location": "<%= event.getVenue(locale) %>",
        "url": "<%= detailPageURL3.toString() %>",
        "time": {
            //@MY_TODO: Calendar dates need to fix - check once maybe wrong timezone
            "start": "<%= dateTimeFormat.format(event.getEventStartDateTime()) %>",
            "end": "<%= dateTimeFormat.format(event.getEventEndDateTime()) %>",
            "zone": "+03:00",
            "allday": false
        },
    };
</script>
<%
    }
%>
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5fb4b2505447720012bb52b3&product=sop' async='async'></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/addToCalendar.js"></script>