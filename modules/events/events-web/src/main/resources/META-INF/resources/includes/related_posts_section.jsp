<%
    long categoryId = event.getCategoryId();
    List<Event> events = EventServiceUtil.getEventsByCategory(categoryId);

    if(events.size() > 0){
%>

<section class="related-news-section" data-scroll-section>

    <%--<%
        JournalArticle articleRelatedStoriesSection = JournalArticleLocalServiceUtil.fetchLatestArticle(scopeGroupId, "76010", 0);
        JournalArticleDisplay articleRelatedStoriesSectionDisplay = JournalArticleLocalServiceUtil.getArticleDisplay(scopeGroupId, articleRelatedStoriesSection.getArticleId(), articleRelatedStoriesSection.getDDMTemplateKey(), null, themeDisplay.getLanguageId(), themeDisplay);
        String relatedStoriesSection = articleRelatedStoriesSectionDisplay.getContent();
    %>

    <%=relatedStoriesSection.toString() %>--%>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="related-news-heading">
                    <h3>Related Events</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="innovation-image-slider">
                    <div class="slider related-news-slider">
                        <%
                            for (Event curEvent : events) {
                                if(eventId == curEvent.getEventId()){
                                    continue;
                                }
                        %>
                        <portlet:renderURL var="detailPageURL">
                            <portlet:param name="eventId" value="<%= String.valueOf(curEvent.getEventId()) %>" />
                            <portlet:param name="mvcPath" value="/detail.jsp" />
                        </portlet:renderURL>
                        <div>
                            <div class="innovate-img-box">
                                <a href="<%= detailPageURL.toString() %>" class="img-inno image-overlay">
                                    <img src="<%= curEvent.getSmallImage(locale) %>" alt="" class="img-fluid">
                                    <div class="date-box">
                                        <h3><%= day.format(curEvent.getDisplayDate()) %></h3>
                                        <p><%= shortMonthYear.format(curEvent.getDisplayDate()) %></p>
                                    </div>
                                </a>
                                <div class="img-content">
                                    <p class="venue-time-text"><span class="text-info">Venue and Time:</span> <%= curEvent.getVenue(locale) %> <span class="venue-time-separator">|</span> <%= curEvent.getTiming(locale) %></p>
                                    <h4 class="animate" data-animation="fadeInUp" data-duration="500">
                                        <a href="<%= detailPageURL.toString() %>"><%= curEvent.getTitle(locale) %></a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<%
    }
%>