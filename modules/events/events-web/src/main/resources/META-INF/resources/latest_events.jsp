<%
    int latestHappeningType = 2;
    List<Event> latestEvents = EventServiceUtil.getEventsByType(latestHappeningType);
%>

<section class="latest-news-slider-section" data-scroll-section>
    <div class="slider-right-shape">
        <img src="/assets/svgs/yellow-shadow-stories.svg" class="img-fluid" alt="triangle">
    </div>
    <div class="latest-news-slider">
        <%
            for (Event curEvent : latestEvents) {
        %>
        <portlet:renderURL var="detailPageURL">
            <portlet:param name="eventId" value="<%= String.valueOf(curEvent.getEventId()) %>" />
            <portlet:param name="mvcPath" value="/detail.jsp" />
        </portlet:renderURL>

        <div class="latest-news-body">
            <div class="latest-news-image-box">
                <a href="<%= detailPageURL.toString() %>" class="image-overlay">
                    <img src="<%= curEvent.getBigImage(locale) %>" alt="" class="img-fluid">
                </a>
                <div class="date-box">
                    <h3><%= day.format(curEvent.getDisplayDate()) %></h3>
                    <p><%= shortMonthYear.format(curEvent.getDisplayDate()) %></p>
                </div>
            </div>
            <div class="latest-news-info-box">
                <p class="venue-time-text"><span class="text-info">Venue and Time:</span> <%= curEvent.getVenue(locale) %> <span class="venue-time-separator">|</span> <%= curEvent.getTiming(locale) %></p>
                <p class="info-box-title mb-4">
                    <a href="<%= detailPageURL.toString() %>">
                        <%= curEvent.getTitle(locale) %>
                    </a>
                </p>
            </div>
        </div>
        <%
            }
        %>
    </div>
</section>