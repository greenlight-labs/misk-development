package events.web.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import events.model.Event;
import events.service.EventLocalService;
import events.web.constants.EventsWebPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.misk",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=EventsWeb",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + EventsWebPortletKeys.EVENTSWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class EventsWebPortlet extends MVCPortlet {
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					Event.class.getName(), renderRequest);

			long groupId = serviceContext.getScopeGroupId();

			List<Event> events = _eventLocalService.getEvents(groupId);
		} catch (Exception e) {
			throw new PortletException(e);
		}

		super.render(renderRequest, renderResponse);
	}

	public void detail(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		super.render(renderRequest, renderResponse);
	}

	/*public void search(ActionRequest request, ActionResponse response)
			throws PortalException {

		String searchQuery = ParamUtil.getString(request, "q");

		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					Event.class.getName(), request);

			long groupId = serviceContext.getScopeGroupId();

			List<Event> events = _eventLocalService.getEvents(groupId);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}*/

	@Reference
	private EventLocalService _eventLocalService;
}