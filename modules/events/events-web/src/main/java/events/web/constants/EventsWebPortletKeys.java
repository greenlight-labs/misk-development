package events.web.constants;

/**
 * @author tz
 */
public class EventsWebPortletKeys {

	public static final String EVENTSWEB =
		"events_web_EventsWebPortlet";

	public static final String LATESTEVENTS =
		"events_web_LatestEventsPortlet";

	public static final String LATESTEVENTSSLIDER =
		"events_web_LatestEventsSliderPortlet";

}