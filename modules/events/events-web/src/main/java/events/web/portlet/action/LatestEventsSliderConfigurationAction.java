package events.web.portlet.action;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import events.web.constants.EventsWebPortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Map;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + EventsWebPortletKeys.LATESTEVENTSSLIDER
        },
        service = ConfigurationAction.class
)
public class LatestEventsSliderConfigurationAction extends DefaultConfigurationAction {
        @Override
        public String getJspPath(HttpServletRequest httpServletRequest) {
                return "/components/latest-events-slider-configuration.jsp";
        }

        @Override
        public void processAction(
                PortletConfig portletConfig, ActionRequest actionRequest,
                ActionResponse actionResponse)
                throws Exception {

                Locale defaultLocale = LocaleUtil.getSiteDefault();

                PortletPreferences preferences = actionRequest.getPreferences();

                // Save Localized Values
                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "sectionTitle");
                Map<Locale, String> sectionTitleMap = LocalizationUtil.getLocalizationMap(actionRequest, "sectionTitle");
                preferences.setValue("sectionTitle", sectionTitleMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "buttonLabel");
                Map<Locale, String> buttonLabelMap = LocalizationUtil.getLocalizationMap(actionRequest, "buttonLabel");
                preferences.setValue("buttonLabel", buttonLabelMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "buttonLink");
                Map<Locale, String> buttonLinkMap = LocalizationUtil.getLocalizationMap(actionRequest, "buttonLink");
                preferences.setValue("buttonLink", buttonLinkMap.get(defaultLocale));

                // Store these 'added' preferences
                preferences.store();

                super.processAction(portletConfig, actionRequest, actionResponse);
        }
}
