/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PageTag}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PageTag
 * @generated
 */
public class PageTagWrapper
	extends BaseModelWrapper<PageTag>
	implements ModelWrapper<PageTag>, PageTag {

	public PageTagWrapper(PageTag pageTag) {
		super(pageTag);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("pageId", getPageId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("pageName", getPageName());
		attributes.put("pageSlug", getPageSlug());
		attributes.put("tagId", getTagId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long pageId = (Long)attributes.get("pageId");

		if (pageId != null) {
			setPageId(pageId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String pageName = (String)attributes.get("pageName");

		if (pageName != null) {
			setPageName(pageName);
		}

		String pageSlug = (String)attributes.get("pageSlug");

		if (pageSlug != null) {
			setPageSlug(pageSlug);
		}

		Long tagId = (Long)attributes.get("tagId");

		if (tagId != null) {
			setTagId(tagId);
		}
	}

	/**
	 * Returns the company ID of this page tag.
	 *
	 * @return the company ID of this page tag
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this page tag.
	 *
	 * @return the create date of this page tag
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the group ID of this page tag.
	 *
	 * @return the group ID of this page tag
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this page tag.
	 *
	 * @return the modified date of this page tag
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the page ID of this page tag.
	 *
	 * @return the page ID of this page tag
	 */
	@Override
	public long getPageId() {
		return model.getPageId();
	}

	/**
	 * Returns the page name of this page tag.
	 *
	 * @return the page name of this page tag
	 */
	@Override
	public String getPageName() {
		return model.getPageName();
	}

	/**
	 * Returns the page slug of this page tag.
	 *
	 * @return the page slug of this page tag
	 */
	@Override
	public String getPageSlug() {
		return model.getPageSlug();
	}

	/**
	 * Returns the primary key of this page tag.
	 *
	 * @return the primary key of this page tag
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the tag ID of this page tag.
	 *
	 * @return the tag ID of this page tag
	 */
	@Override
	public long getTagId() {
		return model.getTagId();
	}

	/**
	 * Returns the user ID of this page tag.
	 *
	 * @return the user ID of this page tag
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this page tag.
	 *
	 * @return the user name of this page tag
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this page tag.
	 *
	 * @return the user uuid of this page tag
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this page tag.
	 *
	 * @return the uuid of this page tag
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this page tag.
	 *
	 * @param companyId the company ID of this page tag
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this page tag.
	 *
	 * @param createDate the create date of this page tag
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this page tag.
	 *
	 * @param groupId the group ID of this page tag
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this page tag.
	 *
	 * @param modifiedDate the modified date of this page tag
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the page ID of this page tag.
	 *
	 * @param pageId the page ID of this page tag
	 */
	@Override
	public void setPageId(long pageId) {
		model.setPageId(pageId);
	}

	/**
	 * Sets the page name of this page tag.
	 *
	 * @param pageName the page name of this page tag
	 */
	@Override
	public void setPageName(String pageName) {
		model.setPageName(pageName);
	}

	/**
	 * Sets the page slug of this page tag.
	 *
	 * @param pageSlug the page slug of this page tag
	 */
	@Override
	public void setPageSlug(String pageSlug) {
		model.setPageSlug(pageSlug);
	}

	/**
	 * Sets the primary key of this page tag.
	 *
	 * @param primaryKey the primary key of this page tag
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the tag ID of this page tag.
	 *
	 * @param tagId the tag ID of this page tag
	 */
	@Override
	public void setTagId(long tagId) {
		model.setTagId(tagId);
	}

	/**
	 * Sets the user ID of this page tag.
	 *
	 * @param userId the user ID of this page tag
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this page tag.
	 *
	 * @param userName the user name of this page tag
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this page tag.
	 *
	 * @param userUuid the user uuid of this page tag
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this page tag.
	 *
	 * @param uuid the uuid of this page tag
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected PageTagWrapper wrap(PageTag pageTag) {
		return new PageTagWrapper(pageTag);
	}

}