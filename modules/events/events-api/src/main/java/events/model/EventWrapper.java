/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Event}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Event
 * @generated
 */
public class EventWrapper
	extends BaseModelWrapper<Event> implements Event, ModelWrapper<Event> {

	public EventWrapper(Event event) {
		super(event);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("eventId", getEventId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("eventStartDateTime", getEventStartDateTime());
		attributes.put("eventEndDateTime", getEventEndDateTime());
		attributes.put("tagId", getTagId());
		attributes.put("categoryId", getCategoryId());
		attributes.put("orderNo", getOrderNo());
		attributes.put("type", getType());
		attributes.put("paytype", getPaytype());
		attributes.put("title", getTitle());
		attributes.put("smallImage", getSmallImage());
		attributes.put("bigImage", getBigImage());
		attributes.put("displayDate", getDisplayDate());
		attributes.put("venue", getVenue());
		attributes.put("timing", getTiming());
		attributes.put("duration", getDuration());
		attributes.put("descriptionField1", getDescriptionField1());
		attributes.put("videoLink", getVideoLink());
		attributes.put("descriptionField2", getDescriptionField2());
		attributes.put("subtitle", getSubtitle());
		attributes.put("descriptionField3", getDescriptionField3());
		attributes.put("detailImage", getDetailImage());
		attributes.put("quoteText", getQuoteText());
		attributes.put("quoteAuthor", getQuoteAuthor());
		attributes.put("descriptionField4", getDescriptionField4());
		attributes.put("eventdescription", getEventdescription());
		attributes.put("eventfeaturedImage", getEventfeaturedImage());
		attributes.put("buyTicketLink", getBuyTicketLink());
		attributes.put("lat", getLat());
		attributes.put("lan", getLan());
		attributes.put("eventPrice", getEventPrice());
		attributes.put("currency", getCurrency());
		attributes.put("email", getEmail());
		attributes.put("phone", getPhone());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long eventId = (Long)attributes.get("eventId");

		if (eventId != null) {
			setEventId(eventId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date eventStartDateTime = (Date)attributes.get("eventStartDateTime");

		if (eventStartDateTime != null) {
			setEventStartDateTime(eventStartDateTime);
		}

		Date eventEndDateTime = (Date)attributes.get("eventEndDateTime");

		if (eventEndDateTime != null) {
			setEventEndDateTime(eventEndDateTime);
		}

		Long tagId = (Long)attributes.get("tagId");

		if (tagId != null) {
			setTagId(tagId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long orderNo = (Long)attributes.get("orderNo");

		if (orderNo != null) {
			setOrderNo(orderNo);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Integer paytype = (Integer)attributes.get("paytype");

		if (paytype != null) {
			setPaytype(paytype);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String smallImage = (String)attributes.get("smallImage");

		if (smallImage != null) {
			setSmallImage(smallImage);
		}

		String bigImage = (String)attributes.get("bigImage");

		if (bigImage != null) {
			setBigImage(bigImage);
		}

		Date displayDate = (Date)attributes.get("displayDate");

		if (displayDate != null) {
			setDisplayDate(displayDate);
		}

		String venue = (String)attributes.get("venue");

		if (venue != null) {
			setVenue(venue);
		}

		String timing = (String)attributes.get("timing");

		if (timing != null) {
			setTiming(timing);
		}

		String duration = (String)attributes.get("duration");

		if (duration != null) {
			setDuration(duration);
		}

		String descriptionField1 = (String)attributes.get("descriptionField1");

		if (descriptionField1 != null) {
			setDescriptionField1(descriptionField1);
		}

		String videoLink = (String)attributes.get("videoLink");

		if (videoLink != null) {
			setVideoLink(videoLink);
		}

		String descriptionField2 = (String)attributes.get("descriptionField2");

		if (descriptionField2 != null) {
			setDescriptionField2(descriptionField2);
		}

		String subtitle = (String)attributes.get("subtitle");

		if (subtitle != null) {
			setSubtitle(subtitle);
		}

		String descriptionField3 = (String)attributes.get("descriptionField3");

		if (descriptionField3 != null) {
			setDescriptionField3(descriptionField3);
		}

		String detailImage = (String)attributes.get("detailImage");

		if (detailImage != null) {
			setDetailImage(detailImage);
		}

		String quoteText = (String)attributes.get("quoteText");

		if (quoteText != null) {
			setQuoteText(quoteText);
		}

		String quoteAuthor = (String)attributes.get("quoteAuthor");

		if (quoteAuthor != null) {
			setQuoteAuthor(quoteAuthor);
		}

		String descriptionField4 = (String)attributes.get("descriptionField4");

		if (descriptionField4 != null) {
			setDescriptionField4(descriptionField4);
		}

		String eventdescription = (String)attributes.get("eventdescription");

		if (eventdescription != null) {
			setEventdescription(eventdescription);
		}

		String eventfeaturedImage = (String)attributes.get(
			"eventfeaturedImage");

		if (eventfeaturedImage != null) {
			setEventfeaturedImage(eventfeaturedImage);
		}

		String buyTicketLink = (String)attributes.get("buyTicketLink");

		if (buyTicketLink != null) {
			setBuyTicketLink(buyTicketLink);
		}

		String lat = (String)attributes.get("lat");

		if (lat != null) {
			setLat(lat);
		}

		String lan = (String)attributes.get("lan");

		if (lan != null) {
			setLan(lan);
		}

		String eventPrice = (String)attributes.get("eventPrice");

		if (eventPrice != null) {
			setEventPrice(eventPrice);
		}

		String currency = (String)attributes.get("currency");

		if (currency != null) {
			setCurrency(currency);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String phone = (String)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the big image of this event.
	 *
	 * @return the big image of this event
	 */
	@Override
	public String getBigImage() {
		return model.getBigImage();
	}

	/**
	 * Returns the localized big image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized big image of this event
	 */
	@Override
	public String getBigImage(java.util.Locale locale) {
		return model.getBigImage(locale);
	}

	/**
	 * Returns the localized big image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized big image of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBigImage(java.util.Locale locale, boolean useDefault) {
		return model.getBigImage(locale, useDefault);
	}

	/**
	 * Returns the localized big image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized big image of this event
	 */
	@Override
	public String getBigImage(String languageId) {
		return model.getBigImage(languageId);
	}

	/**
	 * Returns the localized big image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized big image of this event
	 */
	@Override
	public String getBigImage(String languageId, boolean useDefault) {
		return model.getBigImage(languageId, useDefault);
	}

	@Override
	public String getBigImageCurrentLanguageId() {
		return model.getBigImageCurrentLanguageId();
	}

	@Override
	public String getBigImageCurrentValue() {
		return model.getBigImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized big images of this event.
	 *
	 * @return the locales and localized big images of this event
	 */
	@Override
	public Map<java.util.Locale, String> getBigImageMap() {
		return model.getBigImageMap();
	}

	/**
	 * Returns the buy ticket link of this event.
	 *
	 * @return the buy ticket link of this event
	 */
	@Override
	public String getBuyTicketLink() {
		return model.getBuyTicketLink();
	}

	/**
	 * Returns the localized buy ticket link of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized buy ticket link of this event
	 */
	@Override
	public String getBuyTicketLink(java.util.Locale locale) {
		return model.getBuyTicketLink(locale);
	}

	/**
	 * Returns the localized buy ticket link of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized buy ticket link of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBuyTicketLink(
		java.util.Locale locale, boolean useDefault) {

		return model.getBuyTicketLink(locale, useDefault);
	}

	/**
	 * Returns the localized buy ticket link of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized buy ticket link of this event
	 */
	@Override
	public String getBuyTicketLink(String languageId) {
		return model.getBuyTicketLink(languageId);
	}

	/**
	 * Returns the localized buy ticket link of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized buy ticket link of this event
	 */
	@Override
	public String getBuyTicketLink(String languageId, boolean useDefault) {
		return model.getBuyTicketLink(languageId, useDefault);
	}

	@Override
	public String getBuyTicketLinkCurrentLanguageId() {
		return model.getBuyTicketLinkCurrentLanguageId();
	}

	@Override
	public String getBuyTicketLinkCurrentValue() {
		return model.getBuyTicketLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized buy ticket links of this event.
	 *
	 * @return the locales and localized buy ticket links of this event
	 */
	@Override
	public Map<java.util.Locale, String> getBuyTicketLinkMap() {
		return model.getBuyTicketLinkMap();
	}

	/**
	 * Returns the category ID of this event.
	 *
	 * @return the category ID of this event
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this event.
	 *
	 * @return the company ID of this event
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this event.
	 *
	 * @return the create date of this event
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the currency of this event.
	 *
	 * @return the currency of this event
	 */
	@Override
	public String getCurrency() {
		return model.getCurrency();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description field1 of this event.
	 *
	 * @return the description field1 of this event
	 */
	@Override
	public String getDescriptionField1() {
		return model.getDescriptionField1();
	}

	/**
	 * Returns the localized description field1 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description field1 of this event
	 */
	@Override
	public String getDescriptionField1(java.util.Locale locale) {
		return model.getDescriptionField1(locale);
	}

	/**
	 * Returns the localized description field1 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field1 of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescriptionField1(
		java.util.Locale locale, boolean useDefault) {

		return model.getDescriptionField1(locale, useDefault);
	}

	/**
	 * Returns the localized description field1 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description field1 of this event
	 */
	@Override
	public String getDescriptionField1(String languageId) {
		return model.getDescriptionField1(languageId);
	}

	/**
	 * Returns the localized description field1 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field1 of this event
	 */
	@Override
	public String getDescriptionField1(String languageId, boolean useDefault) {
		return model.getDescriptionField1(languageId, useDefault);
	}

	@Override
	public String getDescriptionField1CurrentLanguageId() {
		return model.getDescriptionField1CurrentLanguageId();
	}

	@Override
	public String getDescriptionField1CurrentValue() {
		return model.getDescriptionField1CurrentValue();
	}

	/**
	 * Returns a map of the locales and localized description field1s of this event.
	 *
	 * @return the locales and localized description field1s of this event
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionField1Map() {
		return model.getDescriptionField1Map();
	}

	/**
	 * Returns the description field2 of this event.
	 *
	 * @return the description field2 of this event
	 */
	@Override
	public String getDescriptionField2() {
		return model.getDescriptionField2();
	}

	/**
	 * Returns the localized description field2 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description field2 of this event
	 */
	@Override
	public String getDescriptionField2(java.util.Locale locale) {
		return model.getDescriptionField2(locale);
	}

	/**
	 * Returns the localized description field2 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field2 of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescriptionField2(
		java.util.Locale locale, boolean useDefault) {

		return model.getDescriptionField2(locale, useDefault);
	}

	/**
	 * Returns the localized description field2 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description field2 of this event
	 */
	@Override
	public String getDescriptionField2(String languageId) {
		return model.getDescriptionField2(languageId);
	}

	/**
	 * Returns the localized description field2 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field2 of this event
	 */
	@Override
	public String getDescriptionField2(String languageId, boolean useDefault) {
		return model.getDescriptionField2(languageId, useDefault);
	}

	@Override
	public String getDescriptionField2CurrentLanguageId() {
		return model.getDescriptionField2CurrentLanguageId();
	}

	@Override
	public String getDescriptionField2CurrentValue() {
		return model.getDescriptionField2CurrentValue();
	}

	/**
	 * Returns a map of the locales and localized description field2s of this event.
	 *
	 * @return the locales and localized description field2s of this event
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionField2Map() {
		return model.getDescriptionField2Map();
	}

	/**
	 * Returns the description field3 of this event.
	 *
	 * @return the description field3 of this event
	 */
	@Override
	public String getDescriptionField3() {
		return model.getDescriptionField3();
	}

	/**
	 * Returns the localized description field3 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description field3 of this event
	 */
	@Override
	public String getDescriptionField3(java.util.Locale locale) {
		return model.getDescriptionField3(locale);
	}

	/**
	 * Returns the localized description field3 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field3 of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescriptionField3(
		java.util.Locale locale, boolean useDefault) {

		return model.getDescriptionField3(locale, useDefault);
	}

	/**
	 * Returns the localized description field3 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description field3 of this event
	 */
	@Override
	public String getDescriptionField3(String languageId) {
		return model.getDescriptionField3(languageId);
	}

	/**
	 * Returns the localized description field3 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field3 of this event
	 */
	@Override
	public String getDescriptionField3(String languageId, boolean useDefault) {
		return model.getDescriptionField3(languageId, useDefault);
	}

	@Override
	public String getDescriptionField3CurrentLanguageId() {
		return model.getDescriptionField3CurrentLanguageId();
	}

	@Override
	public String getDescriptionField3CurrentValue() {
		return model.getDescriptionField3CurrentValue();
	}

	/**
	 * Returns a map of the locales and localized description field3s of this event.
	 *
	 * @return the locales and localized description field3s of this event
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionField3Map() {
		return model.getDescriptionField3Map();
	}

	/**
	 * Returns the description field4 of this event.
	 *
	 * @return the description field4 of this event
	 */
	@Override
	public String getDescriptionField4() {
		return model.getDescriptionField4();
	}

	/**
	 * Returns the localized description field4 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description field4 of this event
	 */
	@Override
	public String getDescriptionField4(java.util.Locale locale) {
		return model.getDescriptionField4(locale);
	}

	/**
	 * Returns the localized description field4 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field4 of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescriptionField4(
		java.util.Locale locale, boolean useDefault) {

		return model.getDescriptionField4(locale, useDefault);
	}

	/**
	 * Returns the localized description field4 of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description field4 of this event
	 */
	@Override
	public String getDescriptionField4(String languageId) {
		return model.getDescriptionField4(languageId);
	}

	/**
	 * Returns the localized description field4 of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field4 of this event
	 */
	@Override
	public String getDescriptionField4(String languageId, boolean useDefault) {
		return model.getDescriptionField4(languageId, useDefault);
	}

	@Override
	public String getDescriptionField4CurrentLanguageId() {
		return model.getDescriptionField4CurrentLanguageId();
	}

	@Override
	public String getDescriptionField4CurrentValue() {
		return model.getDescriptionField4CurrentValue();
	}

	/**
	 * Returns a map of the locales and localized description field4s of this event.
	 *
	 * @return the locales and localized description field4s of this event
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionField4Map() {
		return model.getDescriptionField4Map();
	}

	/**
	 * Returns the detail image of this event.
	 *
	 * @return the detail image of this event
	 */
	@Override
	public String getDetailImage() {
		return model.getDetailImage();
	}

	/**
	 * Returns the localized detail image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized detail image of this event
	 */
	@Override
	public String getDetailImage(java.util.Locale locale) {
		return model.getDetailImage(locale);
	}

	/**
	 * Returns the localized detail image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized detail image of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDetailImage(java.util.Locale locale, boolean useDefault) {
		return model.getDetailImage(locale, useDefault);
	}

	/**
	 * Returns the localized detail image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized detail image of this event
	 */
	@Override
	public String getDetailImage(String languageId) {
		return model.getDetailImage(languageId);
	}

	/**
	 * Returns the localized detail image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized detail image of this event
	 */
	@Override
	public String getDetailImage(String languageId, boolean useDefault) {
		return model.getDetailImage(languageId, useDefault);
	}

	@Override
	public String getDetailImageCurrentLanguageId() {
		return model.getDetailImageCurrentLanguageId();
	}

	@Override
	public String getDetailImageCurrentValue() {
		return model.getDetailImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized detail images of this event.
	 *
	 * @return the locales and localized detail images of this event
	 */
	@Override
	public Map<java.util.Locale, String> getDetailImageMap() {
		return model.getDetailImageMap();
	}

	/**
	 * Returns the display date of this event.
	 *
	 * @return the display date of this event
	 */
	@Override
	public Date getDisplayDate() {
		return model.getDisplayDate();
	}

	/**
	 * Returns the duration of this event.
	 *
	 * @return the duration of this event
	 */
	@Override
	public String getDuration() {
		return model.getDuration();
	}

	/**
	 * Returns the localized duration of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized duration of this event
	 */
	@Override
	public String getDuration(java.util.Locale locale) {
		return model.getDuration(locale);
	}

	/**
	 * Returns the localized duration of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized duration of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDuration(java.util.Locale locale, boolean useDefault) {
		return model.getDuration(locale, useDefault);
	}

	/**
	 * Returns the localized duration of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized duration of this event
	 */
	@Override
	public String getDuration(String languageId) {
		return model.getDuration(languageId);
	}

	/**
	 * Returns the localized duration of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized duration of this event
	 */
	@Override
	public String getDuration(String languageId, boolean useDefault) {
		return model.getDuration(languageId, useDefault);
	}

	@Override
	public String getDurationCurrentLanguageId() {
		return model.getDurationCurrentLanguageId();
	}

	@Override
	public String getDurationCurrentValue() {
		return model.getDurationCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized durations of this event.
	 *
	 * @return the locales and localized durations of this event
	 */
	@Override
	public Map<java.util.Locale, String> getDurationMap() {
		return model.getDurationMap();
	}

	/**
	 * Returns the email of this event.
	 *
	 * @return the email of this event
	 */
	@Override
	public String getEmail() {
		return model.getEmail();
	}

	/**
	 * Returns the eventdescription of this event.
	 *
	 * @return the eventdescription of this event
	 */
	@Override
	public String getEventdescription() {
		return model.getEventdescription();
	}

	/**
	 * Returns the localized eventdescription of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized eventdescription of this event
	 */
	@Override
	public String getEventdescription(java.util.Locale locale) {
		return model.getEventdescription(locale);
	}

	/**
	 * Returns the localized eventdescription of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized eventdescription of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getEventdescription(
		java.util.Locale locale, boolean useDefault) {

		return model.getEventdescription(locale, useDefault);
	}

	/**
	 * Returns the localized eventdescription of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized eventdescription of this event
	 */
	@Override
	public String getEventdescription(String languageId) {
		return model.getEventdescription(languageId);
	}

	/**
	 * Returns the localized eventdescription of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized eventdescription of this event
	 */
	@Override
	public String getEventdescription(String languageId, boolean useDefault) {
		return model.getEventdescription(languageId, useDefault);
	}

	@Override
	public String getEventdescriptionCurrentLanguageId() {
		return model.getEventdescriptionCurrentLanguageId();
	}

	@Override
	public String getEventdescriptionCurrentValue() {
		return model.getEventdescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized eventdescriptions of this event.
	 *
	 * @return the locales and localized eventdescriptions of this event
	 */
	@Override
	public Map<java.util.Locale, String> getEventdescriptionMap() {
		return model.getEventdescriptionMap();
	}

	/**
	 * Returns the event end date time of this event.
	 *
	 * @return the event end date time of this event
	 */
	@Override
	public Date getEventEndDateTime() {
		return model.getEventEndDateTime();
	}

	/**
	 * Returns the eventfeatured image of this event.
	 *
	 * @return the eventfeatured image of this event
	 */
	@Override
	public String getEventfeaturedImage() {
		return model.getEventfeaturedImage();
	}

	/**
	 * Returns the localized eventfeatured image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized eventfeatured image of this event
	 */
	@Override
	public String getEventfeaturedImage(java.util.Locale locale) {
		return model.getEventfeaturedImage(locale);
	}

	/**
	 * Returns the localized eventfeatured image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized eventfeatured image of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getEventfeaturedImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getEventfeaturedImage(locale, useDefault);
	}

	/**
	 * Returns the localized eventfeatured image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized eventfeatured image of this event
	 */
	@Override
	public String getEventfeaturedImage(String languageId) {
		return model.getEventfeaturedImage(languageId);
	}

	/**
	 * Returns the localized eventfeatured image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized eventfeatured image of this event
	 */
	@Override
	public String getEventfeaturedImage(String languageId, boolean useDefault) {
		return model.getEventfeaturedImage(languageId, useDefault);
	}

	@Override
	public String getEventfeaturedImageCurrentLanguageId() {
		return model.getEventfeaturedImageCurrentLanguageId();
	}

	@Override
	public String getEventfeaturedImageCurrentValue() {
		return model.getEventfeaturedImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized eventfeatured images of this event.
	 *
	 * @return the locales and localized eventfeatured images of this event
	 */
	@Override
	public Map<java.util.Locale, String> getEventfeaturedImageMap() {
		return model.getEventfeaturedImageMap();
	}

	/**
	 * Returns the event ID of this event.
	 *
	 * @return the event ID of this event
	 */
	@Override
	public long getEventId() {
		return model.getEventId();
	}

	/**
	 * Returns the event price of this event.
	 *
	 * @return the event price of this event
	 */
	@Override
	public String getEventPrice() {
		return model.getEventPrice();
	}

	/**
	 * Returns the event start date time of this event.
	 *
	 * @return the event start date time of this event
	 */
	@Override
	public Date getEventStartDateTime() {
		return model.getEventStartDateTime();
	}

	/**
	 * Returns the group ID of this event.
	 *
	 * @return the group ID of this event
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the lan of this event.
	 *
	 * @return the lan of this event
	 */
	@Override
	public String getLan() {
		return model.getLan();
	}

	/**
	 * Returns the lat of this event.
	 *
	 * @return the lat of this event
	 */
	@Override
	public String getLat() {
		return model.getLat();
	}

	/**
	 * Returns the modified date of this event.
	 *
	 * @return the modified date of this event
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the order no of this event.
	 *
	 * @return the order no of this event
	 */
	@Override
	public long getOrderNo() {
		return model.getOrderNo();
	}

	/**
	 * Returns the paytype of this event.
	 *
	 * @return the paytype of this event
	 */
	@Override
	public int getPaytype() {
		return model.getPaytype();
	}

	/**
	 * Returns the phone of this event.
	 *
	 * @return the phone of this event
	 */
	@Override
	public String getPhone() {
		return model.getPhone();
	}

	/**
	 * Returns the primary key of this event.
	 *
	 * @return the primary key of this event
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the quote author of this event.
	 *
	 * @return the quote author of this event
	 */
	@Override
	public String getQuoteAuthor() {
		return model.getQuoteAuthor();
	}

	/**
	 * Returns the localized quote author of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized quote author of this event
	 */
	@Override
	public String getQuoteAuthor(java.util.Locale locale) {
		return model.getQuoteAuthor(locale);
	}

	/**
	 * Returns the localized quote author of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized quote author of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getQuoteAuthor(java.util.Locale locale, boolean useDefault) {
		return model.getQuoteAuthor(locale, useDefault);
	}

	/**
	 * Returns the localized quote author of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized quote author of this event
	 */
	@Override
	public String getQuoteAuthor(String languageId) {
		return model.getQuoteAuthor(languageId);
	}

	/**
	 * Returns the localized quote author of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized quote author of this event
	 */
	@Override
	public String getQuoteAuthor(String languageId, boolean useDefault) {
		return model.getQuoteAuthor(languageId, useDefault);
	}

	@Override
	public String getQuoteAuthorCurrentLanguageId() {
		return model.getQuoteAuthorCurrentLanguageId();
	}

	@Override
	public String getQuoteAuthorCurrentValue() {
		return model.getQuoteAuthorCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized quote authors of this event.
	 *
	 * @return the locales and localized quote authors of this event
	 */
	@Override
	public Map<java.util.Locale, String> getQuoteAuthorMap() {
		return model.getQuoteAuthorMap();
	}

	/**
	 * Returns the quote text of this event.
	 *
	 * @return the quote text of this event
	 */
	@Override
	public String getQuoteText() {
		return model.getQuoteText();
	}

	/**
	 * Returns the localized quote text of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized quote text of this event
	 */
	@Override
	public String getQuoteText(java.util.Locale locale) {
		return model.getQuoteText(locale);
	}

	/**
	 * Returns the localized quote text of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized quote text of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getQuoteText(java.util.Locale locale, boolean useDefault) {
		return model.getQuoteText(locale, useDefault);
	}

	/**
	 * Returns the localized quote text of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized quote text of this event
	 */
	@Override
	public String getQuoteText(String languageId) {
		return model.getQuoteText(languageId);
	}

	/**
	 * Returns the localized quote text of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized quote text of this event
	 */
	@Override
	public String getQuoteText(String languageId, boolean useDefault) {
		return model.getQuoteText(languageId, useDefault);
	}

	@Override
	public String getQuoteTextCurrentLanguageId() {
		return model.getQuoteTextCurrentLanguageId();
	}

	@Override
	public String getQuoteTextCurrentValue() {
		return model.getQuoteTextCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized quote texts of this event.
	 *
	 * @return the locales and localized quote texts of this event
	 */
	@Override
	public Map<java.util.Locale, String> getQuoteTextMap() {
		return model.getQuoteTextMap();
	}

	/**
	 * Returns the small image of this event.
	 *
	 * @return the small image of this event
	 */
	@Override
	public String getSmallImage() {
		return model.getSmallImage();
	}

	/**
	 * Returns the localized small image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized small image of this event
	 */
	@Override
	public String getSmallImage(java.util.Locale locale) {
		return model.getSmallImage(locale);
	}

	/**
	 * Returns the localized small image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized small image of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSmallImage(java.util.Locale locale, boolean useDefault) {
		return model.getSmallImage(locale, useDefault);
	}

	/**
	 * Returns the localized small image of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized small image of this event
	 */
	@Override
	public String getSmallImage(String languageId) {
		return model.getSmallImage(languageId);
	}

	/**
	 * Returns the localized small image of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized small image of this event
	 */
	@Override
	public String getSmallImage(String languageId, boolean useDefault) {
		return model.getSmallImage(languageId, useDefault);
	}

	@Override
	public String getSmallImageCurrentLanguageId() {
		return model.getSmallImageCurrentLanguageId();
	}

	@Override
	public String getSmallImageCurrentValue() {
		return model.getSmallImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized small images of this event.
	 *
	 * @return the locales and localized small images of this event
	 */
	@Override
	public Map<java.util.Locale, String> getSmallImageMap() {
		return model.getSmallImageMap();
	}

	/**
	 * Returns the subtitle of this event.
	 *
	 * @return the subtitle of this event
	 */
	@Override
	public String getSubtitle() {
		return model.getSubtitle();
	}

	/**
	 * Returns the localized subtitle of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized subtitle of this event
	 */
	@Override
	public String getSubtitle(java.util.Locale locale) {
		return model.getSubtitle(locale);
	}

	/**
	 * Returns the localized subtitle of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized subtitle of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSubtitle(java.util.Locale locale, boolean useDefault) {
		return model.getSubtitle(locale, useDefault);
	}

	/**
	 * Returns the localized subtitle of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized subtitle of this event
	 */
	@Override
	public String getSubtitle(String languageId) {
		return model.getSubtitle(languageId);
	}

	/**
	 * Returns the localized subtitle of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized subtitle of this event
	 */
	@Override
	public String getSubtitle(String languageId, boolean useDefault) {
		return model.getSubtitle(languageId, useDefault);
	}

	@Override
	public String getSubtitleCurrentLanguageId() {
		return model.getSubtitleCurrentLanguageId();
	}

	@Override
	public String getSubtitleCurrentValue() {
		return model.getSubtitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized subtitles of this event.
	 *
	 * @return the locales and localized subtitles of this event
	 */
	@Override
	public Map<java.util.Locale, String> getSubtitleMap() {
		return model.getSubtitleMap();
	}

	/**
	 * Returns the tag ID of this event.
	 *
	 * @return the tag ID of this event
	 */
	@Override
	public long getTagId() {
		return model.getTagId();
	}

	/**
	 * Returns the timing of this event.
	 *
	 * @return the timing of this event
	 */
	@Override
	public String getTiming() {
		return model.getTiming();
	}

	/**
	 * Returns the localized timing of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized timing of this event
	 */
	@Override
	public String getTiming(java.util.Locale locale) {
		return model.getTiming(locale);
	}

	/**
	 * Returns the localized timing of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized timing of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTiming(java.util.Locale locale, boolean useDefault) {
		return model.getTiming(locale, useDefault);
	}

	/**
	 * Returns the localized timing of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized timing of this event
	 */
	@Override
	public String getTiming(String languageId) {
		return model.getTiming(languageId);
	}

	/**
	 * Returns the localized timing of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized timing of this event
	 */
	@Override
	public String getTiming(String languageId, boolean useDefault) {
		return model.getTiming(languageId, useDefault);
	}

	@Override
	public String getTimingCurrentLanguageId() {
		return model.getTimingCurrentLanguageId();
	}

	@Override
	public String getTimingCurrentValue() {
		return model.getTimingCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized timings of this event.
	 *
	 * @return the locales and localized timings of this event
	 */
	@Override
	public Map<java.util.Locale, String> getTimingMap() {
		return model.getTimingMap();
	}

	/**
	 * Returns the title of this event.
	 *
	 * @return the title of this event
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this event
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this event
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this event
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this event.
	 *
	 * @return the locales and localized titles of this event
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the type of this event.
	 *
	 * @return the type of this event
	 */
	@Override
	public int getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this event.
	 *
	 * @return the user ID of this event
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this event.
	 *
	 * @return the user name of this event
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this event.
	 *
	 * @return the user uuid of this event
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this event.
	 *
	 * @return the uuid of this event
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the venue of this event.
	 *
	 * @return the venue of this event
	 */
	@Override
	public String getVenue() {
		return model.getVenue();
	}

	/**
	 * Returns the localized venue of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized venue of this event
	 */
	@Override
	public String getVenue(java.util.Locale locale) {
		return model.getVenue(locale);
	}

	/**
	 * Returns the localized venue of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized venue of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getVenue(java.util.Locale locale, boolean useDefault) {
		return model.getVenue(locale, useDefault);
	}

	/**
	 * Returns the localized venue of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized venue of this event
	 */
	@Override
	public String getVenue(String languageId) {
		return model.getVenue(languageId);
	}

	/**
	 * Returns the localized venue of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized venue of this event
	 */
	@Override
	public String getVenue(String languageId, boolean useDefault) {
		return model.getVenue(languageId, useDefault);
	}

	@Override
	public String getVenueCurrentLanguageId() {
		return model.getVenueCurrentLanguageId();
	}

	@Override
	public String getVenueCurrentValue() {
		return model.getVenueCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized venues of this event.
	 *
	 * @return the locales and localized venues of this event
	 */
	@Override
	public Map<java.util.Locale, String> getVenueMap() {
		return model.getVenueMap();
	}

	/**
	 * Returns the video link of this event.
	 *
	 * @return the video link of this event
	 */
	@Override
	public String getVideoLink() {
		return model.getVideoLink();
	}

	/**
	 * Returns the localized video link of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized video link of this event
	 */
	@Override
	public String getVideoLink(java.util.Locale locale) {
		return model.getVideoLink(locale);
	}

	/**
	 * Returns the localized video link of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized video link of this event. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getVideoLink(java.util.Locale locale, boolean useDefault) {
		return model.getVideoLink(locale, useDefault);
	}

	/**
	 * Returns the localized video link of this event in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized video link of this event
	 */
	@Override
	public String getVideoLink(String languageId) {
		return model.getVideoLink(languageId);
	}

	/**
	 * Returns the localized video link of this event in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized video link of this event
	 */
	@Override
	public String getVideoLink(String languageId, boolean useDefault) {
		return model.getVideoLink(languageId, useDefault);
	}

	@Override
	public String getVideoLinkCurrentLanguageId() {
		return model.getVideoLinkCurrentLanguageId();
	}

	@Override
	public String getVideoLinkCurrentValue() {
		return model.getVideoLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized video links of this event.
	 *
	 * @return the locales and localized video links of this event
	 */
	@Override
	public Map<java.util.Locale, String> getVideoLinkMap() {
		return model.getVideoLinkMap();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the big image of this event.
	 *
	 * @param bigImage the big image of this event
	 */
	@Override
	public void setBigImage(String bigImage) {
		model.setBigImage(bigImage);
	}

	/**
	 * Sets the localized big image of this event in the language.
	 *
	 * @param bigImage the localized big image of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setBigImage(String bigImage, java.util.Locale locale) {
		model.setBigImage(bigImage, locale);
	}

	/**
	 * Sets the localized big image of this event in the language, and sets the default locale.
	 *
	 * @param bigImage the localized big image of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBigImage(
		String bigImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBigImage(bigImage, locale, defaultLocale);
	}

	@Override
	public void setBigImageCurrentLanguageId(String languageId) {
		model.setBigImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized big images of this event from the map of locales and localized big images.
	 *
	 * @param bigImageMap the locales and localized big images of this event
	 */
	@Override
	public void setBigImageMap(Map<java.util.Locale, String> bigImageMap) {
		model.setBigImageMap(bigImageMap);
	}

	/**
	 * Sets the localized big images of this event from the map of locales and localized big images, and sets the default locale.
	 *
	 * @param bigImageMap the locales and localized big images of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBigImageMap(
		Map<java.util.Locale, String> bigImageMap,
		java.util.Locale defaultLocale) {

		model.setBigImageMap(bigImageMap, defaultLocale);
	}

	/**
	 * Sets the buy ticket link of this event.
	 *
	 * @param buyTicketLink the buy ticket link of this event
	 */
	@Override
	public void setBuyTicketLink(String buyTicketLink) {
		model.setBuyTicketLink(buyTicketLink);
	}

	/**
	 * Sets the localized buy ticket link of this event in the language.
	 *
	 * @param buyTicketLink the localized buy ticket link of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setBuyTicketLink(
		String buyTicketLink, java.util.Locale locale) {

		model.setBuyTicketLink(buyTicketLink, locale);
	}

	/**
	 * Sets the localized buy ticket link of this event in the language, and sets the default locale.
	 *
	 * @param buyTicketLink the localized buy ticket link of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBuyTicketLink(
		String buyTicketLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBuyTicketLink(buyTicketLink, locale, defaultLocale);
	}

	@Override
	public void setBuyTicketLinkCurrentLanguageId(String languageId) {
		model.setBuyTicketLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized buy ticket links of this event from the map of locales and localized buy ticket links.
	 *
	 * @param buyTicketLinkMap the locales and localized buy ticket links of this event
	 */
	@Override
	public void setBuyTicketLinkMap(
		Map<java.util.Locale, String> buyTicketLinkMap) {

		model.setBuyTicketLinkMap(buyTicketLinkMap);
	}

	/**
	 * Sets the localized buy ticket links of this event from the map of locales and localized buy ticket links, and sets the default locale.
	 *
	 * @param buyTicketLinkMap the locales and localized buy ticket links of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBuyTicketLinkMap(
		Map<java.util.Locale, String> buyTicketLinkMap,
		java.util.Locale defaultLocale) {

		model.setBuyTicketLinkMap(buyTicketLinkMap, defaultLocale);
	}

	/**
	 * Sets the category ID of this event.
	 *
	 * @param categoryId the category ID of this event
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this event.
	 *
	 * @param companyId the company ID of this event
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this event.
	 *
	 * @param createDate the create date of this event
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the currency of this event.
	 *
	 * @param currency the currency of this event
	 */
	@Override
	public void setCurrency(String currency) {
		model.setCurrency(currency);
	}

	/**
	 * Sets the description field1 of this event.
	 *
	 * @param descriptionField1 the description field1 of this event
	 */
	@Override
	public void setDescriptionField1(String descriptionField1) {
		model.setDescriptionField1(descriptionField1);
	}

	/**
	 * Sets the localized description field1 of this event in the language.
	 *
	 * @param descriptionField1 the localized description field1 of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescriptionField1(
		String descriptionField1, java.util.Locale locale) {

		model.setDescriptionField1(descriptionField1, locale);
	}

	/**
	 * Sets the localized description field1 of this event in the language, and sets the default locale.
	 *
	 * @param descriptionField1 the localized description field1 of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField1(
		String descriptionField1, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescriptionField1(descriptionField1, locale, defaultLocale);
	}

	@Override
	public void setDescriptionField1CurrentLanguageId(String languageId) {
		model.setDescriptionField1CurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized description field1s of this event from the map of locales and localized description field1s.
	 *
	 * @param descriptionField1Map the locales and localized description field1s of this event
	 */
	@Override
	public void setDescriptionField1Map(
		Map<java.util.Locale, String> descriptionField1Map) {

		model.setDescriptionField1Map(descriptionField1Map);
	}

	/**
	 * Sets the localized description field1s of this event from the map of locales and localized description field1s, and sets the default locale.
	 *
	 * @param descriptionField1Map the locales and localized description field1s of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField1Map(
		Map<java.util.Locale, String> descriptionField1Map,
		java.util.Locale defaultLocale) {

		model.setDescriptionField1Map(descriptionField1Map, defaultLocale);
	}

	/**
	 * Sets the description field2 of this event.
	 *
	 * @param descriptionField2 the description field2 of this event
	 */
	@Override
	public void setDescriptionField2(String descriptionField2) {
		model.setDescriptionField2(descriptionField2);
	}

	/**
	 * Sets the localized description field2 of this event in the language.
	 *
	 * @param descriptionField2 the localized description field2 of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescriptionField2(
		String descriptionField2, java.util.Locale locale) {

		model.setDescriptionField2(descriptionField2, locale);
	}

	/**
	 * Sets the localized description field2 of this event in the language, and sets the default locale.
	 *
	 * @param descriptionField2 the localized description field2 of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField2(
		String descriptionField2, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescriptionField2(descriptionField2, locale, defaultLocale);
	}

	@Override
	public void setDescriptionField2CurrentLanguageId(String languageId) {
		model.setDescriptionField2CurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized description field2s of this event from the map of locales and localized description field2s.
	 *
	 * @param descriptionField2Map the locales and localized description field2s of this event
	 */
	@Override
	public void setDescriptionField2Map(
		Map<java.util.Locale, String> descriptionField2Map) {

		model.setDescriptionField2Map(descriptionField2Map);
	}

	/**
	 * Sets the localized description field2s of this event from the map of locales and localized description field2s, and sets the default locale.
	 *
	 * @param descriptionField2Map the locales and localized description field2s of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField2Map(
		Map<java.util.Locale, String> descriptionField2Map,
		java.util.Locale defaultLocale) {

		model.setDescriptionField2Map(descriptionField2Map, defaultLocale);
	}

	/**
	 * Sets the description field3 of this event.
	 *
	 * @param descriptionField3 the description field3 of this event
	 */
	@Override
	public void setDescriptionField3(String descriptionField3) {
		model.setDescriptionField3(descriptionField3);
	}

	/**
	 * Sets the localized description field3 of this event in the language.
	 *
	 * @param descriptionField3 the localized description field3 of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescriptionField3(
		String descriptionField3, java.util.Locale locale) {

		model.setDescriptionField3(descriptionField3, locale);
	}

	/**
	 * Sets the localized description field3 of this event in the language, and sets the default locale.
	 *
	 * @param descriptionField3 the localized description field3 of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField3(
		String descriptionField3, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescriptionField3(descriptionField3, locale, defaultLocale);
	}

	@Override
	public void setDescriptionField3CurrentLanguageId(String languageId) {
		model.setDescriptionField3CurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized description field3s of this event from the map of locales and localized description field3s.
	 *
	 * @param descriptionField3Map the locales and localized description field3s of this event
	 */
	@Override
	public void setDescriptionField3Map(
		Map<java.util.Locale, String> descriptionField3Map) {

		model.setDescriptionField3Map(descriptionField3Map);
	}

	/**
	 * Sets the localized description field3s of this event from the map of locales and localized description field3s, and sets the default locale.
	 *
	 * @param descriptionField3Map the locales and localized description field3s of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField3Map(
		Map<java.util.Locale, String> descriptionField3Map,
		java.util.Locale defaultLocale) {

		model.setDescriptionField3Map(descriptionField3Map, defaultLocale);
	}

	/**
	 * Sets the description field4 of this event.
	 *
	 * @param descriptionField4 the description field4 of this event
	 */
	@Override
	public void setDescriptionField4(String descriptionField4) {
		model.setDescriptionField4(descriptionField4);
	}

	/**
	 * Sets the localized description field4 of this event in the language.
	 *
	 * @param descriptionField4 the localized description field4 of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescriptionField4(
		String descriptionField4, java.util.Locale locale) {

		model.setDescriptionField4(descriptionField4, locale);
	}

	/**
	 * Sets the localized description field4 of this event in the language, and sets the default locale.
	 *
	 * @param descriptionField4 the localized description field4 of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField4(
		String descriptionField4, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescriptionField4(descriptionField4, locale, defaultLocale);
	}

	@Override
	public void setDescriptionField4CurrentLanguageId(String languageId) {
		model.setDescriptionField4CurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized description field4s of this event from the map of locales and localized description field4s.
	 *
	 * @param descriptionField4Map the locales and localized description field4s of this event
	 */
	@Override
	public void setDescriptionField4Map(
		Map<java.util.Locale, String> descriptionField4Map) {

		model.setDescriptionField4Map(descriptionField4Map);
	}

	/**
	 * Sets the localized description field4s of this event from the map of locales and localized description field4s, and sets the default locale.
	 *
	 * @param descriptionField4Map the locales and localized description field4s of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField4Map(
		Map<java.util.Locale, String> descriptionField4Map,
		java.util.Locale defaultLocale) {

		model.setDescriptionField4Map(descriptionField4Map, defaultLocale);
	}

	/**
	 * Sets the detail image of this event.
	 *
	 * @param detailImage the detail image of this event
	 */
	@Override
	public void setDetailImage(String detailImage) {
		model.setDetailImage(detailImage);
	}

	/**
	 * Sets the localized detail image of this event in the language.
	 *
	 * @param detailImage the localized detail image of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setDetailImage(String detailImage, java.util.Locale locale) {
		model.setDetailImage(detailImage, locale);
	}

	/**
	 * Sets the localized detail image of this event in the language, and sets the default locale.
	 *
	 * @param detailImage the localized detail image of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDetailImage(
		String detailImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDetailImage(detailImage, locale, defaultLocale);
	}

	@Override
	public void setDetailImageCurrentLanguageId(String languageId) {
		model.setDetailImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized detail images of this event from the map of locales and localized detail images.
	 *
	 * @param detailImageMap the locales and localized detail images of this event
	 */
	@Override
	public void setDetailImageMap(
		Map<java.util.Locale, String> detailImageMap) {

		model.setDetailImageMap(detailImageMap);
	}

	/**
	 * Sets the localized detail images of this event from the map of locales and localized detail images, and sets the default locale.
	 *
	 * @param detailImageMap the locales and localized detail images of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDetailImageMap(
		Map<java.util.Locale, String> detailImageMap,
		java.util.Locale defaultLocale) {

		model.setDetailImageMap(detailImageMap, defaultLocale);
	}

	/**
	 * Sets the display date of this event.
	 *
	 * @param displayDate the display date of this event
	 */
	@Override
	public void setDisplayDate(Date displayDate) {
		model.setDisplayDate(displayDate);
	}

	/**
	 * Sets the duration of this event.
	 *
	 * @param duration the duration of this event
	 */
	@Override
	public void setDuration(String duration) {
		model.setDuration(duration);
	}

	/**
	 * Sets the localized duration of this event in the language.
	 *
	 * @param duration the localized duration of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setDuration(String duration, java.util.Locale locale) {
		model.setDuration(duration, locale);
	}

	/**
	 * Sets the localized duration of this event in the language, and sets the default locale.
	 *
	 * @param duration the localized duration of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDuration(
		String duration, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDuration(duration, locale, defaultLocale);
	}

	@Override
	public void setDurationCurrentLanguageId(String languageId) {
		model.setDurationCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized durations of this event from the map of locales and localized durations.
	 *
	 * @param durationMap the locales and localized durations of this event
	 */
	@Override
	public void setDurationMap(Map<java.util.Locale, String> durationMap) {
		model.setDurationMap(durationMap);
	}

	/**
	 * Sets the localized durations of this event from the map of locales and localized durations, and sets the default locale.
	 *
	 * @param durationMap the locales and localized durations of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDurationMap(
		Map<java.util.Locale, String> durationMap,
		java.util.Locale defaultLocale) {

		model.setDurationMap(durationMap, defaultLocale);
	}

	/**
	 * Sets the email of this event.
	 *
	 * @param email the email of this event
	 */
	@Override
	public void setEmail(String email) {
		model.setEmail(email);
	}

	/**
	 * Sets the eventdescription of this event.
	 *
	 * @param eventdescription the eventdescription of this event
	 */
	@Override
	public void setEventdescription(String eventdescription) {
		model.setEventdescription(eventdescription);
	}

	/**
	 * Sets the localized eventdescription of this event in the language.
	 *
	 * @param eventdescription the localized eventdescription of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setEventdescription(
		String eventdescription, java.util.Locale locale) {

		model.setEventdescription(eventdescription, locale);
	}

	/**
	 * Sets the localized eventdescription of this event in the language, and sets the default locale.
	 *
	 * @param eventdescription the localized eventdescription of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEventdescription(
		String eventdescription, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setEventdescription(eventdescription, locale, defaultLocale);
	}

	@Override
	public void setEventdescriptionCurrentLanguageId(String languageId) {
		model.setEventdescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized eventdescriptions of this event from the map of locales and localized eventdescriptions.
	 *
	 * @param eventdescriptionMap the locales and localized eventdescriptions of this event
	 */
	@Override
	public void setEventdescriptionMap(
		Map<java.util.Locale, String> eventdescriptionMap) {

		model.setEventdescriptionMap(eventdescriptionMap);
	}

	/**
	 * Sets the localized eventdescriptions of this event from the map of locales and localized eventdescriptions, and sets the default locale.
	 *
	 * @param eventdescriptionMap the locales and localized eventdescriptions of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEventdescriptionMap(
		Map<java.util.Locale, String> eventdescriptionMap,
		java.util.Locale defaultLocale) {

		model.setEventdescriptionMap(eventdescriptionMap, defaultLocale);
	}

	/**
	 * Sets the event end date time of this event.
	 *
	 * @param eventEndDateTime the event end date time of this event
	 */
	@Override
	public void setEventEndDateTime(Date eventEndDateTime) {
		model.setEventEndDateTime(eventEndDateTime);
	}

	/**
	 * Sets the eventfeatured image of this event.
	 *
	 * @param eventfeaturedImage the eventfeatured image of this event
	 */
	@Override
	public void setEventfeaturedImage(String eventfeaturedImage) {
		model.setEventfeaturedImage(eventfeaturedImage);
	}

	/**
	 * Sets the localized eventfeatured image of this event in the language.
	 *
	 * @param eventfeaturedImage the localized eventfeatured image of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setEventfeaturedImage(
		String eventfeaturedImage, java.util.Locale locale) {

		model.setEventfeaturedImage(eventfeaturedImage, locale);
	}

	/**
	 * Sets the localized eventfeatured image of this event in the language, and sets the default locale.
	 *
	 * @param eventfeaturedImage the localized eventfeatured image of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEventfeaturedImage(
		String eventfeaturedImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setEventfeaturedImage(eventfeaturedImage, locale, defaultLocale);
	}

	@Override
	public void setEventfeaturedImageCurrentLanguageId(String languageId) {
		model.setEventfeaturedImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized eventfeatured images of this event from the map of locales and localized eventfeatured images.
	 *
	 * @param eventfeaturedImageMap the locales and localized eventfeatured images of this event
	 */
	@Override
	public void setEventfeaturedImageMap(
		Map<java.util.Locale, String> eventfeaturedImageMap) {

		model.setEventfeaturedImageMap(eventfeaturedImageMap);
	}

	/**
	 * Sets the localized eventfeatured images of this event from the map of locales and localized eventfeatured images, and sets the default locale.
	 *
	 * @param eventfeaturedImageMap the locales and localized eventfeatured images of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEventfeaturedImageMap(
		Map<java.util.Locale, String> eventfeaturedImageMap,
		java.util.Locale defaultLocale) {

		model.setEventfeaturedImageMap(eventfeaturedImageMap, defaultLocale);
	}

	/**
	 * Sets the event ID of this event.
	 *
	 * @param eventId the event ID of this event
	 */
	@Override
	public void setEventId(long eventId) {
		model.setEventId(eventId);
	}

	/**
	 * Sets the event price of this event.
	 *
	 * @param eventPrice the event price of this event
	 */
	@Override
	public void setEventPrice(String eventPrice) {
		model.setEventPrice(eventPrice);
	}

	/**
	 * Sets the event start date time of this event.
	 *
	 * @param eventStartDateTime the event start date time of this event
	 */
	@Override
	public void setEventStartDateTime(Date eventStartDateTime) {
		model.setEventStartDateTime(eventStartDateTime);
	}

	/**
	 * Sets the group ID of this event.
	 *
	 * @param groupId the group ID of this event
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the lan of this event.
	 *
	 * @param lan the lan of this event
	 */
	@Override
	public void setLan(String lan) {
		model.setLan(lan);
	}

	/**
	 * Sets the lat of this event.
	 *
	 * @param lat the lat of this event
	 */
	@Override
	public void setLat(String lat) {
		model.setLat(lat);
	}

	/**
	 * Sets the modified date of this event.
	 *
	 * @param modifiedDate the modified date of this event
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the order no of this event.
	 *
	 * @param orderNo the order no of this event
	 */
	@Override
	public void setOrderNo(long orderNo) {
		model.setOrderNo(orderNo);
	}

	/**
	 * Sets the paytype of this event.
	 *
	 * @param paytype the paytype of this event
	 */
	@Override
	public void setPaytype(int paytype) {
		model.setPaytype(paytype);
	}

	/**
	 * Sets the phone of this event.
	 *
	 * @param phone the phone of this event
	 */
	@Override
	public void setPhone(String phone) {
		model.setPhone(phone);
	}

	/**
	 * Sets the primary key of this event.
	 *
	 * @param primaryKey the primary key of this event
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the quote author of this event.
	 *
	 * @param quoteAuthor the quote author of this event
	 */
	@Override
	public void setQuoteAuthor(String quoteAuthor) {
		model.setQuoteAuthor(quoteAuthor);
	}

	/**
	 * Sets the localized quote author of this event in the language.
	 *
	 * @param quoteAuthor the localized quote author of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setQuoteAuthor(String quoteAuthor, java.util.Locale locale) {
		model.setQuoteAuthor(quoteAuthor, locale);
	}

	/**
	 * Sets the localized quote author of this event in the language, and sets the default locale.
	 *
	 * @param quoteAuthor the localized quote author of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setQuoteAuthor(
		String quoteAuthor, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setQuoteAuthor(quoteAuthor, locale, defaultLocale);
	}

	@Override
	public void setQuoteAuthorCurrentLanguageId(String languageId) {
		model.setQuoteAuthorCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized quote authors of this event from the map of locales and localized quote authors.
	 *
	 * @param quoteAuthorMap the locales and localized quote authors of this event
	 */
	@Override
	public void setQuoteAuthorMap(
		Map<java.util.Locale, String> quoteAuthorMap) {

		model.setQuoteAuthorMap(quoteAuthorMap);
	}

	/**
	 * Sets the localized quote authors of this event from the map of locales and localized quote authors, and sets the default locale.
	 *
	 * @param quoteAuthorMap the locales and localized quote authors of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setQuoteAuthorMap(
		Map<java.util.Locale, String> quoteAuthorMap,
		java.util.Locale defaultLocale) {

		model.setQuoteAuthorMap(quoteAuthorMap, defaultLocale);
	}

	/**
	 * Sets the quote text of this event.
	 *
	 * @param quoteText the quote text of this event
	 */
	@Override
	public void setQuoteText(String quoteText) {
		model.setQuoteText(quoteText);
	}

	/**
	 * Sets the localized quote text of this event in the language.
	 *
	 * @param quoteText the localized quote text of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setQuoteText(String quoteText, java.util.Locale locale) {
		model.setQuoteText(quoteText, locale);
	}

	/**
	 * Sets the localized quote text of this event in the language, and sets the default locale.
	 *
	 * @param quoteText the localized quote text of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setQuoteText(
		String quoteText, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setQuoteText(quoteText, locale, defaultLocale);
	}

	@Override
	public void setQuoteTextCurrentLanguageId(String languageId) {
		model.setQuoteTextCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized quote texts of this event from the map of locales and localized quote texts.
	 *
	 * @param quoteTextMap the locales and localized quote texts of this event
	 */
	@Override
	public void setQuoteTextMap(Map<java.util.Locale, String> quoteTextMap) {
		model.setQuoteTextMap(quoteTextMap);
	}

	/**
	 * Sets the localized quote texts of this event from the map of locales and localized quote texts, and sets the default locale.
	 *
	 * @param quoteTextMap the locales and localized quote texts of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setQuoteTextMap(
		Map<java.util.Locale, String> quoteTextMap,
		java.util.Locale defaultLocale) {

		model.setQuoteTextMap(quoteTextMap, defaultLocale);
	}

	/**
	 * Sets the small image of this event.
	 *
	 * @param smallImage the small image of this event
	 */
	@Override
	public void setSmallImage(String smallImage) {
		model.setSmallImage(smallImage);
	}

	/**
	 * Sets the localized small image of this event in the language.
	 *
	 * @param smallImage the localized small image of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setSmallImage(String smallImage, java.util.Locale locale) {
		model.setSmallImage(smallImage, locale);
	}

	/**
	 * Sets the localized small image of this event in the language, and sets the default locale.
	 *
	 * @param smallImage the localized small image of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSmallImage(
		String smallImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSmallImage(smallImage, locale, defaultLocale);
	}

	@Override
	public void setSmallImageCurrentLanguageId(String languageId) {
		model.setSmallImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized small images of this event from the map of locales and localized small images.
	 *
	 * @param smallImageMap the locales and localized small images of this event
	 */
	@Override
	public void setSmallImageMap(Map<java.util.Locale, String> smallImageMap) {
		model.setSmallImageMap(smallImageMap);
	}

	/**
	 * Sets the localized small images of this event from the map of locales and localized small images, and sets the default locale.
	 *
	 * @param smallImageMap the locales and localized small images of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSmallImageMap(
		Map<java.util.Locale, String> smallImageMap,
		java.util.Locale defaultLocale) {

		model.setSmallImageMap(smallImageMap, defaultLocale);
	}

	/**
	 * Sets the subtitle of this event.
	 *
	 * @param subtitle the subtitle of this event
	 */
	@Override
	public void setSubtitle(String subtitle) {
		model.setSubtitle(subtitle);
	}

	/**
	 * Sets the localized subtitle of this event in the language.
	 *
	 * @param subtitle the localized subtitle of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setSubtitle(String subtitle, java.util.Locale locale) {
		model.setSubtitle(subtitle, locale);
	}

	/**
	 * Sets the localized subtitle of this event in the language, and sets the default locale.
	 *
	 * @param subtitle the localized subtitle of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSubtitle(
		String subtitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSubtitle(subtitle, locale, defaultLocale);
	}

	@Override
	public void setSubtitleCurrentLanguageId(String languageId) {
		model.setSubtitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized subtitles of this event from the map of locales and localized subtitles.
	 *
	 * @param subtitleMap the locales and localized subtitles of this event
	 */
	@Override
	public void setSubtitleMap(Map<java.util.Locale, String> subtitleMap) {
		model.setSubtitleMap(subtitleMap);
	}

	/**
	 * Sets the localized subtitles of this event from the map of locales and localized subtitles, and sets the default locale.
	 *
	 * @param subtitleMap the locales and localized subtitles of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSubtitleMap(
		Map<java.util.Locale, String> subtitleMap,
		java.util.Locale defaultLocale) {

		model.setSubtitleMap(subtitleMap, defaultLocale);
	}

	/**
	 * Sets the tag ID of this event.
	 *
	 * @param tagId the tag ID of this event
	 */
	@Override
	public void setTagId(long tagId) {
		model.setTagId(tagId);
	}

	/**
	 * Sets the timing of this event.
	 *
	 * @param timing the timing of this event
	 */
	@Override
	public void setTiming(String timing) {
		model.setTiming(timing);
	}

	/**
	 * Sets the localized timing of this event in the language.
	 *
	 * @param timing the localized timing of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setTiming(String timing, java.util.Locale locale) {
		model.setTiming(timing, locale);
	}

	/**
	 * Sets the localized timing of this event in the language, and sets the default locale.
	 *
	 * @param timing the localized timing of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTiming(
		String timing, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setTiming(timing, locale, defaultLocale);
	}

	@Override
	public void setTimingCurrentLanguageId(String languageId) {
		model.setTimingCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized timings of this event from the map of locales and localized timings.
	 *
	 * @param timingMap the locales and localized timings of this event
	 */
	@Override
	public void setTimingMap(Map<java.util.Locale, String> timingMap) {
		model.setTimingMap(timingMap);
	}

	/**
	 * Sets the localized timings of this event from the map of locales and localized timings, and sets the default locale.
	 *
	 * @param timingMap the locales and localized timings of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTimingMap(
		Map<java.util.Locale, String> timingMap,
		java.util.Locale defaultLocale) {

		model.setTimingMap(timingMap, defaultLocale);
	}

	/**
	 * Sets the title of this event.
	 *
	 * @param title the title of this event
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this event in the language.
	 *
	 * @param title the localized title of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this event in the language, and sets the default locale.
	 *
	 * @param title the localized title of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this event from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this event
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this event from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the type of this event.
	 *
	 * @param type the type of this event
	 */
	@Override
	public void setType(int type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this event.
	 *
	 * @param userId the user ID of this event
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this event.
	 *
	 * @param userName the user name of this event
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this event.
	 *
	 * @param userUuid the user uuid of this event
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this event.
	 *
	 * @param uuid the uuid of this event
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the venue of this event.
	 *
	 * @param venue the venue of this event
	 */
	@Override
	public void setVenue(String venue) {
		model.setVenue(venue);
	}

	/**
	 * Sets the localized venue of this event in the language.
	 *
	 * @param venue the localized venue of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setVenue(String venue, java.util.Locale locale) {
		model.setVenue(venue, locale);
	}

	/**
	 * Sets the localized venue of this event in the language, and sets the default locale.
	 *
	 * @param venue the localized venue of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVenue(
		String venue, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setVenue(venue, locale, defaultLocale);
	}

	@Override
	public void setVenueCurrentLanguageId(String languageId) {
		model.setVenueCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized venues of this event from the map of locales and localized venues.
	 *
	 * @param venueMap the locales and localized venues of this event
	 */
	@Override
	public void setVenueMap(Map<java.util.Locale, String> venueMap) {
		model.setVenueMap(venueMap);
	}

	/**
	 * Sets the localized venues of this event from the map of locales and localized venues, and sets the default locale.
	 *
	 * @param venueMap the locales and localized venues of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVenueMap(
		Map<java.util.Locale, String> venueMap,
		java.util.Locale defaultLocale) {

		model.setVenueMap(venueMap, defaultLocale);
	}

	/**
	 * Sets the video link of this event.
	 *
	 * @param videoLink the video link of this event
	 */
	@Override
	public void setVideoLink(String videoLink) {
		model.setVideoLink(videoLink);
	}

	/**
	 * Sets the localized video link of this event in the language.
	 *
	 * @param videoLink the localized video link of this event
	 * @param locale the locale of the language
	 */
	@Override
	public void setVideoLink(String videoLink, java.util.Locale locale) {
		model.setVideoLink(videoLink, locale);
	}

	/**
	 * Sets the localized video link of this event in the language, and sets the default locale.
	 *
	 * @param videoLink the localized video link of this event
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVideoLink(
		String videoLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setVideoLink(videoLink, locale, defaultLocale);
	}

	@Override
	public void setVideoLinkCurrentLanguageId(String languageId) {
		model.setVideoLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized video links of this event from the map of locales and localized video links.
	 *
	 * @param videoLinkMap the locales and localized video links of this event
	 */
	@Override
	public void setVideoLinkMap(Map<java.util.Locale, String> videoLinkMap) {
		model.setVideoLinkMap(videoLinkMap);
	}

	/**
	 * Sets the localized video links of this event from the map of locales and localized video links, and sets the default locale.
	 *
	 * @param videoLinkMap the locales and localized video links of this event
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVideoLinkMap(
		Map<java.util.Locale, String> videoLinkMap,
		java.util.Locale defaultLocale) {

		model.setVideoLinkMap(videoLinkMap, defaultLocale);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected EventWrapper wrap(Event event) {
		return new EventWrapper(event);
	}

}