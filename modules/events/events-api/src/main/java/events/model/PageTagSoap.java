/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link events.service.http.PageTagServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class PageTagSoap implements Serializable {

	public static PageTagSoap toSoapModel(PageTag model) {
		PageTagSoap soapModel = new PageTagSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setPageId(model.getPageId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setPageName(model.getPageName());
		soapModel.setPageSlug(model.getPageSlug());
		soapModel.setTagId(model.getTagId());

		return soapModel;
	}

	public static PageTagSoap[] toSoapModels(PageTag[] models) {
		PageTagSoap[] soapModels = new PageTagSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PageTagSoap[][] toSoapModels(PageTag[][] models) {
		PageTagSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PageTagSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PageTagSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PageTagSoap[] toSoapModels(List<PageTag> models) {
		List<PageTagSoap> soapModels = new ArrayList<PageTagSoap>(
			models.size());

		for (PageTag model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PageTagSoap[soapModels.size()]);
	}

	public PageTagSoap() {
	}

	public long getPrimaryKey() {
		return _pageId;
	}

	public void setPrimaryKey(long pk) {
		setPageId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getPageId() {
		return _pageId;
	}

	public void setPageId(long pageId) {
		_pageId = pageId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getPageName() {
		return _pageName;
	}

	public void setPageName(String pageName) {
		_pageName = pageName;
	}

	public String getPageSlug() {
		return _pageSlug;
	}

	public void setPageSlug(String pageSlug) {
		_pageSlug = pageSlug;
	}

	public long getTagId() {
		return _tagId;
	}

	public void setTagId(long tagId) {
		_tagId = tagId;
	}

	private String _uuid;
	private long _pageId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _pageName;
	private String _pageSlug;
	private long _tagId;

}