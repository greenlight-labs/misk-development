/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EventVisitors}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventVisitors
 * @generated
 */
public class EventVisitorsWrapper
	extends BaseModelWrapper<EventVisitors>
	implements EventVisitors, ModelWrapper<EventVisitors> {

	public EventVisitorsWrapper(EventVisitors eventVisitors) {
		super(eventVisitors);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("visitor_id", getVisitor_id());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("eventvisitorId", getEventvisitorId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long visitor_id = (Long)attributes.get("visitor_id");

		if (visitor_id != null) {
			setVisitor_id(visitor_id);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long eventvisitorId = (Long)attributes.get("eventvisitorId");

		if (eventvisitorId != null) {
			setEventvisitorId(eventvisitorId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}
	}

	/**
	 * Returns the company ID of this event visitors.
	 *
	 * @return the company ID of this event visitors
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this event visitors.
	 *
	 * @return the create date of this event visitors
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the eventvisitor ID of this event visitors.
	 *
	 * @return the eventvisitor ID of this event visitors
	 */
	@Override
	public long getEventvisitorId() {
		return model.getEventvisitorId();
	}

	/**
	 * Returns the group ID of this event visitors.
	 *
	 * @return the group ID of this event visitors
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this event visitors.
	 *
	 * @return the modified date of this event visitors
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this event visitors.
	 *
	 * @return the primary key of this event visitors
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this event visitors.
	 *
	 * @return the user ID of this event visitors
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user uuid of this event visitors.
	 *
	 * @return the user uuid of this event visitors
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this event visitors.
	 *
	 * @return the uuid of this event visitors
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the visitor_id of this event visitors.
	 *
	 * @return the visitor_id of this event visitors
	 */
	@Override
	public long getVisitor_id() {
		return model.getVisitor_id();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the company ID of this event visitors.
	 *
	 * @param companyId the company ID of this event visitors
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this event visitors.
	 *
	 * @param createDate the create date of this event visitors
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the eventvisitor ID of this event visitors.
	 *
	 * @param eventvisitorId the eventvisitor ID of this event visitors
	 */
	@Override
	public void setEventvisitorId(long eventvisitorId) {
		model.setEventvisitorId(eventvisitorId);
	}

	/**
	 * Sets the group ID of this event visitors.
	 *
	 * @param groupId the group ID of this event visitors
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this event visitors.
	 *
	 * @param modifiedDate the modified date of this event visitors
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this event visitors.
	 *
	 * @param primaryKey the primary key of this event visitors
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this event visitors.
	 *
	 * @param userId the user ID of this event visitors
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user uuid of this event visitors.
	 *
	 * @param userUuid the user uuid of this event visitors
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this event visitors.
	 *
	 * @param uuid the uuid of this event visitors
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the visitor_id of this event visitors.
	 *
	 * @param visitor_id the visitor_id of this event visitors
	 */
	@Override
	public void setVisitor_id(long visitor_id) {
		model.setVisitor_id(visitor_id);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected EventVisitorsWrapper wrap(EventVisitors eventVisitors) {
		return new EventVisitorsWrapper(eventVisitors);
	}

}