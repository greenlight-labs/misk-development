/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link events.service.http.EventServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class EventSoap implements Serializable {

	public static EventSoap toSoapModel(Event model) {
		EventSoap soapModel = new EventSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setEventId(model.getEventId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setEventStartDateTime(model.getEventStartDateTime());
		soapModel.setEventEndDateTime(model.getEventEndDateTime());
		soapModel.setTagId(model.getTagId());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setOrderNo(model.getOrderNo());
		soapModel.setType(model.getType());
		soapModel.setPaytype(model.getPaytype());
		soapModel.setTitle(model.getTitle());
		soapModel.setSmallImage(model.getSmallImage());
		soapModel.setBigImage(model.getBigImage());
		soapModel.setDisplayDate(model.getDisplayDate());
		soapModel.setVenue(model.getVenue());
		soapModel.setTiming(model.getTiming());
		soapModel.setDuration(model.getDuration());
		soapModel.setDescriptionField1(model.getDescriptionField1());
		soapModel.setVideoLink(model.getVideoLink());
		soapModel.setDescriptionField2(model.getDescriptionField2());
		soapModel.setSubtitle(model.getSubtitle());
		soapModel.setDescriptionField3(model.getDescriptionField3());
		soapModel.setDetailImage(model.getDetailImage());
		soapModel.setQuoteText(model.getQuoteText());
		soapModel.setQuoteAuthor(model.getQuoteAuthor());
		soapModel.setDescriptionField4(model.getDescriptionField4());
		soapModel.setEventdescription(model.getEventdescription());
		soapModel.setEventfeaturedImage(model.getEventfeaturedImage());
		soapModel.setBuyTicketLink(model.getBuyTicketLink());
		soapModel.setLat(model.getLat());
		soapModel.setLan(model.getLan());
		soapModel.setEventPrice(model.getEventPrice());
		soapModel.setCurrency(model.getCurrency());
		soapModel.setEmail(model.getEmail());
		soapModel.setPhone(model.getPhone());

		return soapModel;
	}

	public static EventSoap[] toSoapModels(Event[] models) {
		EventSoap[] soapModels = new EventSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EventSoap[][] toSoapModels(Event[][] models) {
		EventSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EventSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EventSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EventSoap[] toSoapModels(List<Event> models) {
		List<EventSoap> soapModels = new ArrayList<EventSoap>(models.size());

		for (Event model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EventSoap[soapModels.size()]);
	}

	public EventSoap() {
	}

	public long getPrimaryKey() {
		return _eventId;
	}

	public void setPrimaryKey(long pk) {
		setEventId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getEventId() {
		return _eventId;
	}

	public void setEventId(long eventId) {
		_eventId = eventId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getEventStartDateTime() {
		return _eventStartDateTime;
	}

	public void setEventStartDateTime(Date eventStartDateTime) {
		_eventStartDateTime = eventStartDateTime;
	}

	public Date getEventEndDateTime() {
		return _eventEndDateTime;
	}

	public void setEventEndDateTime(Date eventEndDateTime) {
		_eventEndDateTime = eventEndDateTime;
	}

	public long getTagId() {
		return _tagId;
	}

	public void setTagId(long tagId) {
		_tagId = tagId;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public long getOrderNo() {
		return _orderNo;
	}

	public void setOrderNo(long orderNo) {
		_orderNo = orderNo;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public int getPaytype() {
		return _paytype;
	}

	public void setPaytype(int paytype) {
		_paytype = paytype;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getSmallImage() {
		return _smallImage;
	}

	public void setSmallImage(String smallImage) {
		_smallImage = smallImage;
	}

	public String getBigImage() {
		return _bigImage;
	}

	public void setBigImage(String bigImage) {
		_bigImage = bigImage;
	}

	public Date getDisplayDate() {
		return _displayDate;
	}

	public void setDisplayDate(Date displayDate) {
		_displayDate = displayDate;
	}

	public String getVenue() {
		return _venue;
	}

	public void setVenue(String venue) {
		_venue = venue;
	}

	public String getTiming() {
		return _timing;
	}

	public void setTiming(String timing) {
		_timing = timing;
	}

	public String getDuration() {
		return _duration;
	}

	public void setDuration(String duration) {
		_duration = duration;
	}

	public String getDescriptionField1() {
		return _descriptionField1;
	}

	public void setDescriptionField1(String descriptionField1) {
		_descriptionField1 = descriptionField1;
	}

	public String getVideoLink() {
		return _videoLink;
	}

	public void setVideoLink(String videoLink) {
		_videoLink = videoLink;
	}

	public String getDescriptionField2() {
		return _descriptionField2;
	}

	public void setDescriptionField2(String descriptionField2) {
		_descriptionField2 = descriptionField2;
	}

	public String getSubtitle() {
		return _subtitle;
	}

	public void setSubtitle(String subtitle) {
		_subtitle = subtitle;
	}

	public String getDescriptionField3() {
		return _descriptionField3;
	}

	public void setDescriptionField3(String descriptionField3) {
		_descriptionField3 = descriptionField3;
	}

	public String getDetailImage() {
		return _detailImage;
	}

	public void setDetailImage(String detailImage) {
		_detailImage = detailImage;
	}

	public String getQuoteText() {
		return _quoteText;
	}

	public void setQuoteText(String quoteText) {
		_quoteText = quoteText;
	}

	public String getQuoteAuthor() {
		return _quoteAuthor;
	}

	public void setQuoteAuthor(String quoteAuthor) {
		_quoteAuthor = quoteAuthor;
	}

	public String getDescriptionField4() {
		return _descriptionField4;
	}

	public void setDescriptionField4(String descriptionField4) {
		_descriptionField4 = descriptionField4;
	}

	public String getEventdescription() {
		return _eventdescription;
	}

	public void setEventdescription(String eventdescription) {
		_eventdescription = eventdescription;
	}

	public String getEventfeaturedImage() {
		return _eventfeaturedImage;
	}

	public void setEventfeaturedImage(String eventfeaturedImage) {
		_eventfeaturedImage = eventfeaturedImage;
	}

	public String getBuyTicketLink() {
		return _buyTicketLink;
	}

	public void setBuyTicketLink(String buyTicketLink) {
		_buyTicketLink = buyTicketLink;
	}

	public String getLat() {
		return _lat;
	}

	public void setLat(String lat) {
		_lat = lat;
	}

	public String getLan() {
		return _lan;
	}

	public void setLan(String lan) {
		_lan = lan;
	}

	public String getEventPrice() {
		return _eventPrice;
	}

	public void setEventPrice(String eventPrice) {
		_eventPrice = eventPrice;
	}

	public String getCurrency() {
		return _currency;
	}

	public void setCurrency(String currency) {
		_currency = currency;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getPhone() {
		return _phone;
	}

	public void setPhone(String phone) {
		_phone = phone;
	}

	private String _uuid;
	private long _eventId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _eventStartDateTime;
	private Date _eventEndDateTime;
	private long _tagId;
	private long _categoryId;
	private long _orderNo;
	private int _type;
	private int _paytype;
	private String _title;
	private String _smallImage;
	private String _bigImage;
	private Date _displayDate;
	private String _venue;
	private String _timing;
	private String _duration;
	private String _descriptionField1;
	private String _videoLink;
	private String _descriptionField2;
	private String _subtitle;
	private String _descriptionField3;
	private String _detailImage;
	private String _quoteText;
	private String _quoteAuthor;
	private String _descriptionField4;
	private String _eventdescription;
	private String _eventfeaturedImage;
	private String _buyTicketLink;
	private String _lat;
	private String _lan;
	private String _eventPrice;
	private String _currency;
	private String _email;
	private String _phone;

}