/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link events.service.http.EventVisitorsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class EventVisitorsSoap implements Serializable {

	public static EventVisitorsSoap toSoapModel(EventVisitors model) {
		EventVisitorsSoap soapModel = new EventVisitorsSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setVisitor_id(model.getVisitor_id());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setEventvisitorId(model.getEventvisitorId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());

		return soapModel;
	}

	public static EventVisitorsSoap[] toSoapModels(EventVisitors[] models) {
		EventVisitorsSoap[] soapModels = new EventVisitorsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EventVisitorsSoap[][] toSoapModels(EventVisitors[][] models) {
		EventVisitorsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EventVisitorsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EventVisitorsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EventVisitorsSoap[] toSoapModels(List<EventVisitors> models) {
		List<EventVisitorsSoap> soapModels = new ArrayList<EventVisitorsSoap>(
			models.size());

		for (EventVisitors model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EventVisitorsSoap[soapModels.size()]);
	}

	public EventVisitorsSoap() {
	}

	public long getPrimaryKey() {
		return _visitor_id;
	}

	public void setPrimaryKey(long pk) {
		setVisitor_id(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getVisitor_id() {
		return _visitor_id;
	}

	public void setVisitor_id(long visitor_id) {
		_visitor_id = visitor_id;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getEventvisitorId() {
		return _eventvisitorId;
	}

	public void setEventvisitorId(long eventvisitorId) {
		_eventvisitorId = eventvisitorId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	private String _uuid;
	private long _visitor_id;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private long _eventvisitorId;
	private Date _createDate;
	private Date _modifiedDate;

}