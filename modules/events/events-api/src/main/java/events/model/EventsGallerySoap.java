/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link events.service.http.EventsGalleryServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class EventsGallerySoap implements Serializable {

	public static EventsGallerySoap toSoapModel(EventsGallery model) {
		EventsGallerySoap soapModel = new EventsGallerySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setSlideId(model.getSlideId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setEventId(model.getEventId());
		soapModel.setImage(model.getImage());
		soapModel.setThumbnail(model.getThumbnail());

		return soapModel;
	}

	public static EventsGallerySoap[] toSoapModels(EventsGallery[] models) {
		EventsGallerySoap[] soapModels = new EventsGallerySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EventsGallerySoap[][] toSoapModels(EventsGallery[][] models) {
		EventsGallerySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EventsGallerySoap[models.length][models[0].length];
		}
		else {
			soapModels = new EventsGallerySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EventsGallerySoap[] toSoapModels(List<EventsGallery> models) {
		List<EventsGallerySoap> soapModels = new ArrayList<EventsGallerySoap>(
			models.size());

		for (EventsGallery model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EventsGallerySoap[soapModels.size()]);
	}

	public EventsGallerySoap() {
	}

	public long getPrimaryKey() {
		return _slideId;
	}

	public void setPrimaryKey(long pk) {
		setSlideId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getSlideId() {
		return _slideId;
	}

	public void setSlideId(long slideId) {
		_slideId = slideId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getEventId() {
		return _eventId;
	}

	public void setEventId(long eventId) {
		_eventId = eventId;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getThumbnail() {
		return _thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		_thumbnail = thumbnail;
	}

	private String _uuid;
	private long _slideId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _eventId;
	private String _image;
	private String _thumbnail;

}