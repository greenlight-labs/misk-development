/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EventsGalleryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EventsGalleryLocalService
 * @generated
 */
public class EventsGalleryLocalServiceWrapper
	implements EventsGalleryLocalService,
			   ServiceWrapper<EventsGalleryLocalService> {

	public EventsGalleryLocalServiceWrapper(
		EventsGalleryLocalService eventsGalleryLocalService) {

		_eventsGalleryLocalService = eventsGalleryLocalService;
	}

	@Override
	public events.model.EventsGallery addEntry(
			events.model.EventsGallery orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   events.exception.EventsGalleryValidateException {

		return _eventsGalleryLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the events gallery to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was added
	 */
	@Override
	public events.model.EventsGallery addEventsGallery(
		events.model.EventsGallery eventsGallery) {

		return _eventsGalleryLocalService.addEventsGallery(eventsGallery);
	}

	@Override
	public int countAllInDiscover(long discoverId) {
		return _eventsGalleryLocalService.countAllInDiscover(discoverId);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _eventsGalleryLocalService.countAllInGroup(groupId);
	}

	/**
	 * Creates a new events gallery with the primary key. Does not add the events gallery to the database.
	 *
	 * @param slideId the primary key for the new events gallery
	 * @return the new events gallery
	 */
	@Override
	public events.model.EventsGallery createEventsGallery(long slideId) {
		return _eventsGalleryLocalService.createEventsGallery(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventsGalleryLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public events.model.EventsGallery deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventsGalleryLocalService.deleteEntry(primaryKey);
	}

	/**
	 * Deletes the events gallery from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was removed
	 */
	@Override
	public events.model.EventsGallery deleteEventsGallery(
		events.model.EventsGallery eventsGallery) {

		return _eventsGalleryLocalService.deleteEventsGallery(eventsGallery);
	}

	/**
	 * Deletes the events gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery that was removed
	 * @throws PortalException if a events gallery with the primary key could not be found
	 */
	@Override
	public events.model.EventsGallery deleteEventsGallery(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventsGalleryLocalService.deleteEventsGallery(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventsGalleryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _eventsGalleryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _eventsGalleryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _eventsGalleryLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _eventsGalleryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _eventsGalleryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _eventsGalleryLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public events.model.EventsGallery fetchEventsGallery(long slideId) {
		return _eventsGalleryLocalService.fetchEventsGallery(slideId);
	}

	/**
	 * Returns the events gallery matching the UUID and group.
	 *
	 * @param uuid the events gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	@Override
	public events.model.EventsGallery fetchEventsGalleryByUuidAndGroupId(
		String uuid, long groupId) {

		return _eventsGalleryLocalService.fetchEventsGalleryByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<events.model.EventsGallery> findAllInDiscover(
		long discoverId) {

		return _eventsGalleryLocalService.findAllInDiscover(discoverId);
	}

	@Override
	public java.util.List<events.model.EventsGallery> findAllInDiscover(
		long discoverId, int start, int end) {

		return _eventsGalleryLocalService.findAllInDiscover(
			discoverId, start, end);
	}

	@Override
	public java.util.List<events.model.EventsGallery> findAllInDiscover(
		long discoverId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<events.model.EventsGallery> obc) {

		return _eventsGalleryLocalService.findAllInDiscover(
			discoverId, start, end, obc);
	}

	@Override
	public java.util.List<events.model.EventsGallery> findAllInGroup(
		long groupId) {

		return _eventsGalleryLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<events.model.EventsGallery> findAllInGroup(
		long groupId, int start, int end) {

		return _eventsGalleryLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<events.model.EventsGallery> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<events.model.EventsGallery> obc) {

		return _eventsGalleryLocalService.findAllInGroup(
			groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _eventsGalleryLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of events galleries
	 */
	@Override
	public java.util.List<events.model.EventsGallery> getEventsGalleries(
		int start, int end) {

		return _eventsGalleryLocalService.getEventsGalleries(start, end);
	}

	/**
	 * Returns all the events galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the events galleries
	 * @param companyId the primary key of the company
	 * @return the matching events galleries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.EventsGallery>
		getEventsGalleriesByUuidAndCompanyId(String uuid, long companyId) {

		return _eventsGalleryLocalService.getEventsGalleriesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of events galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the events galleries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching events galleries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.EventsGallery>
		getEventsGalleriesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<events.model.EventsGallery> orderByComparator) {

		return _eventsGalleryLocalService.getEventsGalleriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of events galleries.
	 *
	 * @return the number of events galleries
	 */
	@Override
	public int getEventsGalleriesCount() {
		return _eventsGalleryLocalService.getEventsGalleriesCount();
	}

	/**
	 * Returns the events gallery with the primary key.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery
	 * @throws PortalException if a events gallery with the primary key could not be found
	 */
	@Override
	public events.model.EventsGallery getEventsGallery(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventsGalleryLocalService.getEventsGallery(slideId);
	}

	/**
	 * Returns the events gallery matching the UUID and group.
	 *
	 * @param uuid the events gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching events gallery
	 * @throws PortalException if a matching events gallery could not be found
	 */
	@Override
	public events.model.EventsGallery getEventsGalleryByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventsGalleryLocalService.getEventsGalleryByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _eventsGalleryLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public events.model.EventsGallery getGalleryFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws events.exception.EventsGalleryValidateException,
			   javax.portlet.PortletException {

		return _eventsGalleryLocalService.getGalleryFromRequest(
			primaryKey, request);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _eventsGalleryLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public events.model.EventsGallery getNewObject(long primaryKey) {
		return _eventsGalleryLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _eventsGalleryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventsGalleryLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public events.model.EventsGallery updateEntry(
			events.model.EventsGallery orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   events.exception.EventsGalleryValidateException {

		return _eventsGalleryLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the events gallery in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was updated
	 */
	@Override
	public events.model.EventsGallery updateEventsGallery(
		events.model.EventsGallery eventsGallery) {

		return _eventsGalleryLocalService.updateEventsGallery(eventsGallery);
	}

	@Override
	public EventsGalleryLocalService getWrappedService() {
		return _eventsGalleryLocalService;
	}

	@Override
	public void setWrappedService(
		EventsGalleryLocalService eventsGalleryLocalService) {

		_eventsGalleryLocalService = eventsGalleryLocalService;
	}

	private EventsGalleryLocalService _eventsGalleryLocalService;

}