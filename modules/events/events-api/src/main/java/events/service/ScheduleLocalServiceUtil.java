/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.Schedule;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Schedule. This utility wraps
 * <code>events.service.impl.ScheduleLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ScheduleLocalService
 * @generated
 */
public class ScheduleLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>events.service.impl.ScheduleLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Schedule addEntry(
			Schedule orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws events.exception.ScheduleValidateException, PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the schedule to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param schedule the schedule
	 * @return the schedule that was added
	 */
	public static Schedule addSchedule(Schedule schedule) {
		return getService().addSchedule(schedule);
	}

	public static int countAllInEvent(long eventId) {
		return getService().countAllInEvent(eventId);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new schedule with the primary key. Does not add the schedule to the database.
	 *
	 * @param scheduleId the primary key for the new schedule
	 * @return the new schedule
	 */
	public static Schedule createSchedule(long scheduleId) {
		return getService().createSchedule(scheduleId);
	}

	public static Schedule deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param scheduleId the primary key of the schedule
	 * @return the schedule that was removed
	 * @throws PortalException if a schedule with the primary key could not be found
	 */
	public static Schedule deleteSchedule(long scheduleId)
		throws PortalException {

		return getService().deleteSchedule(scheduleId);
	}

	/**
	 * Deletes the schedule from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param schedule the schedule
	 * @return the schedule that was removed
	 */
	public static Schedule deleteSchedule(Schedule schedule) {
		return getService().deleteSchedule(schedule);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Schedule fetchSchedule(long scheduleId) {
		return getService().fetchSchedule(scheduleId);
	}

	/**
	 * Returns the schedule matching the UUID and group.
	 *
	 * @param uuid the schedule's UUID
	 * @param groupId the primary key of the group
	 * @return the matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public static Schedule fetchScheduleByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchScheduleByUuidAndGroupId(uuid, groupId);
	}

	public static List<Schedule> findAllInEvent(long eventId) {
		return getService().findAllInEvent(eventId);
	}

	public static List<Schedule> findAllInEvent(
		long eventId, int start, int end) {

		return getService().findAllInEvent(eventId, start, end);
	}

	public static List<Schedule> findAllInEvent(
		long eventId, int start, int end, OrderByComparator<Schedule> obc) {

		return getService().findAllInEvent(eventId, start, end, obc);
	}

	public static List<Schedule> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<Schedule> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<Schedule> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Schedule> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Schedule getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the schedule with the primary key.
	 *
	 * @param scheduleId the primary key of the schedule
	 * @return the schedule
	 * @throws PortalException if a schedule with the primary key could not be found
	 */
	public static Schedule getSchedule(long scheduleId) throws PortalException {
		return getService().getSchedule(scheduleId);
	}

	/**
	 * Returns the schedule matching the UUID and group.
	 *
	 * @param uuid the schedule's UUID
	 * @param groupId the primary key of the group
	 * @return the matching schedule
	 * @throws PortalException if a matching schedule could not be found
	 */
	public static Schedule getScheduleByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getScheduleByUuidAndGroupId(uuid, groupId);
	}

	public static Schedule getScheduleFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws events.exception.ScheduleValidateException,
			   javax.portlet.PortletException {

		return getService().getScheduleFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @return the range of schedules
	 */
	public static List<Schedule> getSchedules(int start, int end) {
		return getService().getSchedules(start, end);
	}

	/**
	 * Returns all the schedules matching the UUID and company.
	 *
	 * @param uuid the UUID of the schedules
	 * @param companyId the primary key of the company
	 * @return the matching schedules, or an empty list if no matches were found
	 */
	public static List<Schedule> getSchedulesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getSchedulesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of schedules matching the UUID and company.
	 *
	 * @param uuid the UUID of the schedules
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching schedules, or an empty list if no matches were found
	 */
	public static List<Schedule> getSchedulesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Schedule> orderByComparator) {

		return getService().getSchedulesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of schedules.
	 *
	 * @return the number of schedules
	 */
	public static int getSchedulesCount() {
		return getService().getSchedulesCount();
	}

	public static Schedule updateEntry(
			Schedule orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws events.exception.ScheduleValidateException, PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the schedule in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param schedule the schedule
	 * @return the schedule that was updated
	 */
	public static Schedule updateSchedule(Schedule schedule) {
		return getService().updateSchedule(schedule);
	}

	public static ScheduleLocalService getService() {
		return _service;
	}

	private static volatile ScheduleLocalService _service;

}