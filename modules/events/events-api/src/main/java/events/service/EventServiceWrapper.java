/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EventService}.
 *
 * @author Brian Wing Shun Chan
 * @see EventService
 * @generated
 */
public class EventServiceWrapper
	implements EventService, ServiceWrapper<EventService> {

	public EventServiceWrapper(EventService eventService) {
		_eventService = eventService;
	}

	@Override
	public java.util.List<events.model.Event> findByTitle(String title) {
		return _eventService.findByTitle(title);
	}

	@Override
	public java.util.List<events.model.Event> findByTitle(
		String title, int start, int end) {

		return _eventService.findByTitle(title, start, end);
	}

	@Override
	public java.util.List<events.model.Event> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		return _eventService.findByTitle(title, start, end, obc);
	}

	@Override
	public int findByTitleCount(String title) {
		return _eventService.findByTitleCount(title);
	}

	@Override
	public events.model.Event getEventById(long eventId)
		throws events.exception.NoSuchEventException {

		return _eventService.getEventById(eventId);
	}

	@Override
	public java.util.List<events.model.Event> getEvents(long groupId) {
		return _eventService.getEvents(groupId);
	}

	@Override
	public java.util.List<events.model.Event> getEvents(
		long groupId, int start, int end) {

		return _eventService.getEvents(groupId, start, end);
	}

	@Override
	public java.util.List<events.model.Event> getEvents(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		return _eventService.getEvents(groupId, start, end, obc);
	}

	@Override
	public java.util.List<events.model.Event> getEventsByCategory(
		long categoryId) {

		return _eventService.getEventsByCategory(categoryId);
	}

	@Override
	public java.util.List<events.model.Event> getEventsByCategory(
		long categoryId, int start, int end) {

		return _eventService.getEventsByCategory(categoryId, start, end);
	}

	@Override
	public java.util.List<events.model.Event> getEventsByCategory(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		return _eventService.getEventsByCategory(categoryId, start, end, obc);
	}

	@Override
	public int getEventsByCategoryCount(long categoryId) {
		return _eventService.getEventsByCategoryCount(categoryId);
	}

	@Override
	public java.util.List<events.model.Event> getEventsByType(int type) {
		return _eventService.getEventsByType(type);
	}

	@Override
	public java.util.List<events.model.Event> getEventsByType(
		int type, int start, int end) {

		return _eventService.getEventsByType(type, start, end);
	}

	@Override
	public java.util.List<events.model.Event> getEventsByType(
		int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Event>
			obc) {

		return _eventService.getEventsByType(type, start, end, obc);
	}

	@Override
	public int getEventsByTypeCount(int type) {
		return _eventService.getEventsByTypeCount(type);
	}

	@Override
	public int getEventsCount(long groupId) {
		return _eventService.getEventsCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _eventService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<events.model.Event> removeExpiredEvent(
		java.util.List<events.model.Event> eventList) {

		return _eventService.removeExpiredEvent(eventList);
	}

	@Override
	public EventService getWrappedService() {
		return _eventService;
	}

	@Override
	public void setWrappedService(EventService eventService) {
		_eventService = eventService;
	}

	private EventService _eventService;

}