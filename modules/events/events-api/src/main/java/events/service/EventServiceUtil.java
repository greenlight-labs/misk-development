/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.Event;

import java.util.List;

/**
 * Provides the remote service utility for Event. This utility wraps
 * <code>events.service.impl.EventServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see EventService
 * @generated
 */
public class EventServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>events.service.impl.EventServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static List<Event> findByTitle(String title) {
		return getService().findByTitle(title);
	}

	public static List<Event> findByTitle(String title, int start, int end) {
		return getService().findByTitle(title, start, end);
	}

	public static List<Event> findByTitle(
		String title, int start, int end, OrderByComparator<Event> obc) {

		return getService().findByTitle(title, start, end, obc);
	}

	public static int findByTitleCount(String title) {
		return getService().findByTitleCount(title);
	}

	public static Event getEventById(long eventId)
		throws events.exception.NoSuchEventException {

		return getService().getEventById(eventId);
	}

	public static List<Event> getEvents(long groupId) {
		return getService().getEvents(groupId);
	}

	public static List<Event> getEvents(long groupId, int start, int end) {
		return getService().getEvents(groupId, start, end);
	}

	public static List<Event> getEvents(
		long groupId, int start, int end, OrderByComparator<Event> obc) {

		return getService().getEvents(groupId, start, end, obc);
	}

	public static List<Event> getEventsByCategory(long categoryId) {
		return getService().getEventsByCategory(categoryId);
	}

	public static List<Event> getEventsByCategory(
		long categoryId, int start, int end) {

		return getService().getEventsByCategory(categoryId, start, end);
	}

	public static List<Event> getEventsByCategory(
		long categoryId, int start, int end, OrderByComparator<Event> obc) {

		return getService().getEventsByCategory(categoryId, start, end, obc);
	}

	public static int getEventsByCategoryCount(long categoryId) {
		return getService().getEventsByCategoryCount(categoryId);
	}

	public static List<Event> getEventsByType(int type) {
		return getService().getEventsByType(type);
	}

	public static List<Event> getEventsByType(int type, int start, int end) {
		return getService().getEventsByType(type, start, end);
	}

	public static List<Event> getEventsByType(
		int type, int start, int end, OrderByComparator<Event> obc) {

		return getService().getEventsByType(type, start, end, obc);
	}

	public static int getEventsByTypeCount(int type) {
		return getService().getEventsByTypeCount(type);
	}

	public static int getEventsCount(long groupId) {
		return getService().getEventsCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static List<Event> removeExpiredEvent(List<Event> eventList) {
		return getService().removeExpiredEvent(eventList);
	}

	public static EventService getService() {
		return _service;
	}

	private static volatile EventService _service;

}