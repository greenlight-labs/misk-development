/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import events.exception.NoSuchScheduleException;

import events.model.Schedule;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the schedule service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ScheduleUtil
 * @generated
 */
@ProviderType
public interface SchedulePersistence extends BasePersistence<Schedule> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ScheduleUtil} to access the schedule persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the schedules where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching schedules
	 */
	public java.util.List<Schedule> findByUuid(String uuid);

	/**
	 * Returns a range of all the schedules where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @return the range of matching schedules
	 */
	public java.util.List<Schedule> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the schedules where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns an ordered range of all the schedules where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first schedule in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the first schedule in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the last schedule in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the last schedule in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the schedules before and after the current schedule in the ordered set where uuid = &#63;.
	 *
	 * @param scheduleId the primary key of the current schedule
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next schedule
	 * @throws NoSuchScheduleException if a schedule with the primary key could not be found
	 */
	public Schedule[] findByUuid_PrevAndNext(
			long scheduleId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Removes all the schedules where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of schedules where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching schedules
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the schedule where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchScheduleException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByUUID_G(String uuid, long groupId)
		throws NoSuchScheduleException;

	/**
	 * Returns the schedule where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the schedule where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the schedule where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the schedule that was removed
	 */
	public Schedule removeByUUID_G(String uuid, long groupId)
		throws NoSuchScheduleException;

	/**
	 * Returns the number of schedules where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching schedules
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the schedules where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching schedules
	 */
	public java.util.List<Schedule> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the schedules where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @return the range of matching schedules
	 */
	public java.util.List<Schedule> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the schedules where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns an ordered range of all the schedules where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first schedule in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the first schedule in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the last schedule in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the last schedule in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the schedules before and after the current schedule in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param scheduleId the primary key of the current schedule
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next schedule
	 * @throws NoSuchScheduleException if a schedule with the primary key could not be found
	 */
	public Schedule[] findByUuid_C_PrevAndNext(
			long scheduleId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Removes all the schedules where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of schedules where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching schedules
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the schedules where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching schedules
	 */
	public java.util.List<Schedule> findByGroupId(long groupId);

	/**
	 * Returns a range of all the schedules where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @return the range of matching schedules
	 */
	public java.util.List<Schedule> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the schedules where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns an ordered range of all the schedules where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first schedule in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the first schedule in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the last schedule in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the last schedule in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the schedules before and after the current schedule in the ordered set where groupId = &#63;.
	 *
	 * @param scheduleId the primary key of the current schedule
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next schedule
	 * @throws NoSuchScheduleException if a schedule with the primary key could not be found
	 */
	public Schedule[] findByGroupId_PrevAndNext(
			long scheduleId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Removes all the schedules where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of schedules where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching schedules
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the schedules where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @return the matching schedules
	 */
	public java.util.List<Schedule> findByEventId(long eventId);

	/**
	 * Returns a range of all the schedules where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @return the range of matching schedules
	 */
	public java.util.List<Schedule> findByEventId(
		long eventId, int start, int end);

	/**
	 * Returns an ordered range of all the schedules where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByEventId(
		long eventId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns an ordered range of all the schedules where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching schedules
	 */
	public java.util.List<Schedule> findByEventId(
		long eventId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first schedule in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByEventId_First(
			long eventId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the first schedule in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByEventId_First(
		long eventId,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the last schedule in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule
	 * @throws NoSuchScheduleException if a matching schedule could not be found
	 */
	public Schedule findByEventId_Last(
			long eventId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Returns the last schedule in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	public Schedule fetchByEventId_Last(
		long eventId,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns the schedules before and after the current schedule in the ordered set where eventId = &#63;.
	 *
	 * @param scheduleId the primary key of the current schedule
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next schedule
	 * @throws NoSuchScheduleException if a schedule with the primary key could not be found
	 */
	public Schedule[] findByEventId_PrevAndNext(
			long scheduleId, long eventId,
			com.liferay.portal.kernel.util.OrderByComparator<Schedule>
				orderByComparator)
		throws NoSuchScheduleException;

	/**
	 * Removes all the schedules where eventId = &#63; from the database.
	 *
	 * @param eventId the event ID
	 */
	public void removeByEventId(long eventId);

	/**
	 * Returns the number of schedules where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @return the number of matching schedules
	 */
	public int countByEventId(long eventId);

	/**
	 * Caches the schedule in the entity cache if it is enabled.
	 *
	 * @param schedule the schedule
	 */
	public void cacheResult(Schedule schedule);

	/**
	 * Caches the schedules in the entity cache if it is enabled.
	 *
	 * @param schedules the schedules
	 */
	public void cacheResult(java.util.List<Schedule> schedules);

	/**
	 * Creates a new schedule with the primary key. Does not add the schedule to the database.
	 *
	 * @param scheduleId the primary key for the new schedule
	 * @return the new schedule
	 */
	public Schedule create(long scheduleId);

	/**
	 * Removes the schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param scheduleId the primary key of the schedule
	 * @return the schedule that was removed
	 * @throws NoSuchScheduleException if a schedule with the primary key could not be found
	 */
	public Schedule remove(long scheduleId) throws NoSuchScheduleException;

	public Schedule updateImpl(Schedule schedule);

	/**
	 * Returns the schedule with the primary key or throws a <code>NoSuchScheduleException</code> if it could not be found.
	 *
	 * @param scheduleId the primary key of the schedule
	 * @return the schedule
	 * @throws NoSuchScheduleException if a schedule with the primary key could not be found
	 */
	public Schedule findByPrimaryKey(long scheduleId)
		throws NoSuchScheduleException;

	/**
	 * Returns the schedule with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param scheduleId the primary key of the schedule
	 * @return the schedule, or <code>null</code> if a schedule with the primary key could not be found
	 */
	public Schedule fetchByPrimaryKey(long scheduleId);

	/**
	 * Returns all the schedules.
	 *
	 * @return the schedules
	 */
	public java.util.List<Schedule> findAll();

	/**
	 * Returns a range of all the schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @return the range of schedules
	 */
	public java.util.List<Schedule> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of schedules
	 */
	public java.util.List<Schedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator);

	/**
	 * Returns an ordered range of all the schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of schedules
	 */
	public java.util.List<Schedule> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Schedule>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the schedules from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of schedules.
	 *
	 * @return the number of schedules
	 */
	public int countAll();

}