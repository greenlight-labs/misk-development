/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.PageTag;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the page tag service. This utility wraps <code>events.service.persistence.impl.PageTagPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PageTagPersistence
 * @generated
 */
public class PageTagUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(PageTag pageTag) {
		getPersistence().clearCache(pageTag);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, PageTag> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PageTag> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PageTag> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PageTag> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static PageTag update(PageTag pageTag) {
		return getPersistence().update(pageTag);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static PageTag update(
		PageTag pageTag, ServiceContext serviceContext) {

		return getPersistence().update(pageTag, serviceContext);
	}

	/**
	 * Returns all the page tags where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching page tags
	 */
	public static List<PageTag> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the page tags where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of matching page tags
	 */
	public static List<PageTag> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching page tags
	 */
	public static List<PageTag> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching page tags
	 */
	public static List<PageTag> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PageTag> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByUuid_First(
			String uuid, OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByUuid_First(
		String uuid, OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByUuid_Last(
			String uuid, OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByUuid_Last(
		String uuid, OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the page tags before and after the current page tag in the ordered set where uuid = &#63;.
	 *
	 * @param pageId the primary key of the current page tag
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public static PageTag[] findByUuid_PrevAndNext(
			long pageId, String uuid,
			OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByUuid_PrevAndNext(
			pageId, uuid, orderByComparator);
	}

	/**
	 * Removes all the page tags where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of page tags where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching page tags
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the page tag where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchPageTagException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByUUID_G(String uuid, long groupId)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the page tag where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the page tag where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the page tag where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the page tag that was removed
	 */
	public static PageTag removeByUUID_G(String uuid, long groupId)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of page tags where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching page tags
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching page tags
	 */
	public static List<PageTag> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of matching page tags
	 */
	public static List<PageTag> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching page tags
	 */
	public static List<PageTag> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching page tags
	 */
	public static List<PageTag> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PageTag> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the page tags before and after the current page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param pageId the primary key of the current page tag
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public static PageTag[] findByUuid_C_PrevAndNext(
			long pageId, String uuid, long companyId,
			OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByUuid_C_PrevAndNext(
			pageId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the page tags where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching page tags
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the page tags where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching page tags
	 */
	public static List<PageTag> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the page tags where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of matching page tags
	 */
	public static List<PageTag> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the page tags where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching page tags
	 */
	public static List<PageTag> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the page tags where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching page tags
	 */
	public static List<PageTag> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<PageTag> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByGroupId_First(
			long groupId, OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByGroupId_First(
		long groupId, OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByGroupId_Last(
			long groupId, OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByGroupId_Last(
		long groupId, OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the page tags before and after the current page tag in the ordered set where groupId = &#63;.
	 *
	 * @param pageId the primary key of the current page tag
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public static PageTag[] findByGroupId_PrevAndNext(
			long pageId, long groupId,
			OrderByComparator<PageTag> orderByComparator)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByGroupId_PrevAndNext(
			pageId, groupId, orderByComparator);
	}

	/**
	 * Removes all the page tags where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of page tags where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching page tags
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns the page tag where pageSlug = &#63; or throws a <code>NoSuchPageTagException</code> if it could not be found.
	 *
	 * @param pageSlug the page slug
	 * @return the matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public static PageTag findByPageSlug(String pageSlug)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByPageSlug(pageSlug);
	}

	/**
	 * Returns the page tag where pageSlug = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param pageSlug the page slug
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByPageSlug(String pageSlug) {
		return getPersistence().fetchByPageSlug(pageSlug);
	}

	/**
	 * Returns the page tag where pageSlug = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param pageSlug the page slug
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchByPageSlug(
		String pageSlug, boolean useFinderCache) {

		return getPersistence().fetchByPageSlug(pageSlug, useFinderCache);
	}

	/**
	 * Removes the page tag where pageSlug = &#63; from the database.
	 *
	 * @param pageSlug the page slug
	 * @return the page tag that was removed
	 */
	public static PageTag removeByPageSlug(String pageSlug)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().removeByPageSlug(pageSlug);
	}

	/**
	 * Returns the number of page tags where pageSlug = &#63;.
	 *
	 * @param pageSlug the page slug
	 * @return the number of matching page tags
	 */
	public static int countByPageSlug(String pageSlug) {
		return getPersistence().countByPageSlug(pageSlug);
	}

	/**
	 * Caches the page tag in the entity cache if it is enabled.
	 *
	 * @param pageTag the page tag
	 */
	public static void cacheResult(PageTag pageTag) {
		getPersistence().cacheResult(pageTag);
	}

	/**
	 * Caches the page tags in the entity cache if it is enabled.
	 *
	 * @param pageTags the page tags
	 */
	public static void cacheResult(List<PageTag> pageTags) {
		getPersistence().cacheResult(pageTags);
	}

	/**
	 * Creates a new page tag with the primary key. Does not add the page tag to the database.
	 *
	 * @param pageId the primary key for the new page tag
	 * @return the new page tag
	 */
	public static PageTag create(long pageId) {
		return getPersistence().create(pageId);
	}

	/**
	 * Removes the page tag with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag that was removed
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public static PageTag remove(long pageId)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().remove(pageId);
	}

	public static PageTag updateImpl(PageTag pageTag) {
		return getPersistence().updateImpl(pageTag);
	}

	/**
	 * Returns the page tag with the primary key or throws a <code>NoSuchPageTagException</code> if it could not be found.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public static PageTag findByPrimaryKey(long pageId)
		throws events.exception.NoSuchPageTagException {

		return getPersistence().findByPrimaryKey(pageId);
	}

	/**
	 * Returns the page tag with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag, or <code>null</code> if a page tag with the primary key could not be found
	 */
	public static PageTag fetchByPrimaryKey(long pageId) {
		return getPersistence().fetchByPrimaryKey(pageId);
	}

	/**
	 * Returns all the page tags.
	 *
	 * @return the page tags
	 */
	public static List<PageTag> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of page tags
	 */
	public static List<PageTag> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of page tags
	 */
	public static List<PageTag> findAll(
		int start, int end, OrderByComparator<PageTag> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of page tags
	 */
	public static List<PageTag> findAll(
		int start, int end, OrderByComparator<PageTag> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the page tags from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of page tags.
	 *
	 * @return the number of page tags
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static PageTagPersistence getPersistence() {
		return _persistence;
	}

	private static volatile PageTagPersistence _persistence;

}