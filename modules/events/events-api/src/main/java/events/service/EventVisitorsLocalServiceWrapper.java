/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EventVisitorsLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see EventVisitorsLocalService
 * @generated
 */
public class EventVisitorsLocalServiceWrapper
	implements EventVisitorsLocalService,
			   ServiceWrapper<EventVisitorsLocalService> {

	public EventVisitorsLocalServiceWrapper(
		EventVisitorsLocalService eventVisitorsLocalService) {

		_eventVisitorsLocalService = eventVisitorsLocalService;
	}

	/**
	 * Adds the event visitors to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventVisitors the event visitors
	 * @return the event visitors that was added
	 */
	@Override
	public events.model.EventVisitors addEventVisitors(
		events.model.EventVisitors eventVisitors) {

		return _eventVisitorsLocalService.addEventVisitors(eventVisitors);
	}

	@Override
	public events.model.EventVisitors addEventVisitors(
			long userId, long eventId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventVisitorsLocalService.addEventVisitors(
			userId, eventId, serviceContext);
	}

	/**
	 * Creates a new event visitors with the primary key. Does not add the event visitors to the database.
	 *
	 * @param visitor_id the primary key for the new event visitors
	 * @return the new event visitors
	 */
	@Override
	public events.model.EventVisitors createEventVisitors(long visitor_id) {
		return _eventVisitorsLocalService.createEventVisitors(visitor_id);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventVisitorsLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the event visitors from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventVisitors the event visitors
	 * @return the event visitors that was removed
	 */
	@Override
	public events.model.EventVisitors deleteEventVisitors(
		events.model.EventVisitors eventVisitors) {

		return _eventVisitorsLocalService.deleteEventVisitors(eventVisitors);
	}

	/**
	 * Deletes the event visitors with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors that was removed
	 * @throws PortalException if a event visitors with the primary key could not be found
	 */
	@Override
	public events.model.EventVisitors deleteEventVisitors(long visitor_id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventVisitorsLocalService.deleteEventVisitors(visitor_id);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventVisitorsLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _eventVisitorsLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _eventVisitorsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _eventVisitorsLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _eventVisitorsLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _eventVisitorsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _eventVisitorsLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public events.model.EventVisitors fetchEventVisitors(long visitor_id) {
		return _eventVisitorsLocalService.fetchEventVisitors(visitor_id);
	}

	/**
	 * Returns the event visitors matching the UUID and group.
	 *
	 * @param uuid the event visitors's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	@Override
	public events.model.EventVisitors fetchEventVisitorsByUuidAndGroupId(
		String uuid, long groupId) {

		return _eventVisitorsLocalService.fetchEventVisitorsByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<events.model.EventVisitors> findEventByUser(
		long userId) {

		return _eventVisitorsLocalService.findEventByUser(userId);
	}

	@Override
	public java.util.List<events.model.EventVisitors> findEventByvisitor(
		long eventid) {

		return _eventVisitorsLocalService.findEventByvisitor(eventid);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _eventVisitorsLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the event visitors with the primary key.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors
	 * @throws PortalException if a event visitors with the primary key could not be found
	 */
	@Override
	public events.model.EventVisitors getEventVisitors(long visitor_id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventVisitorsLocalService.getEventVisitors(visitor_id);
	}

	/**
	 * Returns the event visitors matching the UUID and group.
	 *
	 * @param uuid the event visitors's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event visitors
	 * @throws PortalException if a matching event visitors could not be found
	 */
	@Override
	public events.model.EventVisitors getEventVisitorsByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventVisitorsLocalService.getEventVisitorsByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of event visitorses
	 */
	@Override
	public java.util.List<events.model.EventVisitors> getEventVisitorses(
		int start, int end) {

		return _eventVisitorsLocalService.getEventVisitorses(start, end);
	}

	/**
	 * Returns all the event visitorses matching the UUID and company.
	 *
	 * @param uuid the UUID of the event visitorses
	 * @param companyId the primary key of the company
	 * @return the matching event visitorses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.EventVisitors>
		getEventVisitorsesByUuidAndCompanyId(String uuid, long companyId) {

		return _eventVisitorsLocalService.getEventVisitorsesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of event visitorses matching the UUID and company.
	 *
	 * @param uuid the UUID of the event visitorses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching event visitorses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.EventVisitors>
		getEventVisitorsesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<events.model.EventVisitors> orderByComparator) {

		return _eventVisitorsLocalService.getEventVisitorsesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of event visitorses.
	 *
	 * @return the number of event visitorses
	 */
	@Override
	public int getEventVisitorsesCount() {
		return _eventVisitorsLocalService.getEventVisitorsesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _eventVisitorsLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _eventVisitorsLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _eventVisitorsLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _eventVisitorsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the event visitors in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventVisitors the event visitors
	 * @return the event visitors that was updated
	 */
	@Override
	public events.model.EventVisitors updateEventVisitors(
		events.model.EventVisitors eventVisitors) {

		return _eventVisitorsLocalService.updateEventVisitors(eventVisitors);
	}

	@Override
	public EventVisitorsLocalService getWrappedService() {
		return _eventVisitorsLocalService;
	}

	@Override
	public void setWrappedService(
		EventVisitorsLocalService eventVisitorsLocalService) {

		_eventVisitorsLocalService = eventVisitorsLocalService;
	}

	private EventVisitorsLocalService _eventVisitorsLocalService;

}