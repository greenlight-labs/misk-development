/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.PageTag;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for PageTag. This utility wraps
 * <code>events.service.impl.PageTagLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PageTagLocalService
 * @generated
 */
public class PageTagLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>events.service.impl.PageTagLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static PageTag addEntry(
			PageTag orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws events.exception.PageTagValidateException, PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the page tag to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was added
	 */
	public static PageTag addPageTag(PageTag pageTag) {
		return getService().addPageTag(pageTag);
	}

	public static int countByGroupId(long groupId) {
		return getService().countByGroupId(groupId);
	}

	/**
	 * Creates a new page tag with the primary key. Does not add the page tag to the database.
	 *
	 * @param pageId the primary key for the new page tag
	 * @return the new page tag
	 */
	public static PageTag createPageTag(long pageId) {
		return getService().createPageTag(pageId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static PageTag deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * Deletes the page tag with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag that was removed
	 * @throws PortalException if a page tag with the primary key could not be found
	 */
	public static PageTag deletePageTag(long pageId) throws PortalException {
		return getService().deletePageTag(pageId);
	}

	/**
	 * Deletes the page tag from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was removed
	 */
	public static PageTag deletePageTag(PageTag pageTag) {
		return getService().deletePageTag(pageTag);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static PageTag fetchByPageSlug(String pageSlug) {
		return getService().fetchByPageSlug(pageSlug);
	}

	public static PageTag fetchPageTag(long pageId) {
		return getService().fetchPageTag(pageId);
	}

	/**
	 * Returns the page tag matching the UUID and group.
	 *
	 * @param uuid the page tag's UUID
	 * @param groupId the primary key of the group
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public static PageTag fetchPageTagByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchPageTagByUuidAndGroupId(uuid, groupId);
	}

	public static List<PageTag> findByGroupId(long groupId) {
		return getService().findByGroupId(groupId);
	}

	public static List<PageTag> findByGroupId(
		long groupId, int start, int end) {

		return getService().findByGroupId(groupId, start, end);
	}

	public static List<PageTag> findByGroupId(
		long groupId, int start, int end, OrderByComparator<PageTag> obc) {

		return getService().findByGroupId(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static PageTag getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * Returns the page tag with the primary key.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag
	 * @throws PortalException if a page tag with the primary key could not be found
	 */
	public static PageTag getPageTag(long pageId) throws PortalException {
		return getService().getPageTag(pageId);
	}

	/**
	 * Returns the page tag matching the UUID and group.
	 *
	 * @param uuid the page tag's UUID
	 * @param groupId the primary key of the group
	 * @return the matching page tag
	 * @throws PortalException if a matching page tag could not be found
	 */
	public static PageTag getPageTagByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getPageTagByUuidAndGroupId(uuid, groupId);
	}

	public static PageTag getPageTagFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws events.exception.PageTagValidateException,
			   javax.portlet.PortletException {

		return getService().getPageTagFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of page tags
	 */
	public static List<PageTag> getPageTags(int start, int end) {
		return getService().getPageTags(start, end);
	}

	/**
	 * Returns all the page tags matching the UUID and company.
	 *
	 * @param uuid the UUID of the page tags
	 * @param companyId the primary key of the company
	 * @return the matching page tags, or an empty list if no matches were found
	 */
	public static List<PageTag> getPageTagsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getPageTagsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of page tags matching the UUID and company.
	 *
	 * @param uuid the UUID of the page tags
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching page tags, or an empty list if no matches were found
	 */
	public static List<PageTag> getPageTagsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PageTag> orderByComparator) {

		return getService().getPageTagsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of page tags.
	 *
	 * @return the number of page tags
	 */
	public static int getPageTagsCount() {
		return getService().getPageTagsCount();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static PageTag updateEntry(
			PageTag orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws events.exception.PageTagValidateException, PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the page tag in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was updated
	 */
	public static PageTag updatePageTag(PageTag pageTag) {
		return getService().updatePageTag(pageTag);
	}

	public static PageTagLocalService getService() {
		return _service;
	}

	private static volatile PageTagLocalService _service;

}