/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.exception.PageTagValidateException;

import events.model.PageTag;

import java.io.Serializable;

import java.util.*;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for PageTag. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see PageTagLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface PageTagLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>events.service.impl.PageTagLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the page tag local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link PageTagLocalServiceUtil} if injection and service tracking are not available.
	 */
	public PageTag addEntry(PageTag orgEntry, ServiceContext serviceContext)
		throws PageTagValidateException, PortalException;

	/**
	 * Adds the page tag to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public PageTag addPageTag(PageTag pageTag);

	public int countByGroupId(long groupId);

	/**
	 * Creates a new page tag with the primary key. Does not add the page tag to the database.
	 *
	 * @param pageId the primary key for the new page tag
	 * @return the new page tag
	 */
	@Transactional(enabled = false)
	public PageTag createPageTag(long pageId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public PageTag deleteEntry(long primaryKey) throws PortalException;

	/**
	 * Deletes the page tag with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag that was removed
	 * @throws PortalException if a page tag with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public PageTag deletePageTag(long pageId) throws PortalException;

	/**
	 * Deletes the page tag from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public PageTag deletePageTag(PageTag pageTag);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PageTag fetchByPageSlug(String pageSlug);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PageTag fetchPageTag(long pageId);

	/**
	 * Returns the page tag matching the UUID and group.
	 *
	 * @param uuid the page tag's UUID
	 * @param groupId the primary key of the group
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PageTag fetchPageTagByUuidAndGroupId(String uuid, long groupId);

	public List<PageTag> findByGroupId(long groupId);

	public List<PageTag> findByGroupId(long groupId, int start, int end);

	public List<PageTag> findByGroupId(
		long groupId, int start, int end, OrderByComparator<PageTag> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PageTag getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * Returns the page tag with the primary key.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag
	 * @throws PortalException if a page tag with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PageTag getPageTag(long pageId) throws PortalException;

	/**
	 * Returns the page tag matching the UUID and group.
	 *
	 * @param uuid the page tag's UUID
	 * @param groupId the primary key of the group
	 * @return the matching page tag
	 * @throws PortalException if a matching page tag could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PageTag getPageTagByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PageTag getPageTagFromRequest(
			long primaryKey, PortletRequest request)
		throws PageTagValidateException, PortletException;

	/**
	 * Returns a range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of page tags
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<PageTag> getPageTags(int start, int end);

	/**
	 * Returns all the page tags matching the UUID and company.
	 *
	 * @param uuid the UUID of the page tags
	 * @param companyId the primary key of the company
	 * @return the matching page tags, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<PageTag> getPageTagsByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of page tags matching the UUID and company.
	 *
	 * @param uuid the UUID of the page tags
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching page tags, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<PageTag> getPageTagsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PageTag> orderByComparator);

	/**
	 * Returns the number of page tags.
	 *
	 * @return the number of page tags
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getPageTagsCount();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public PageTag updateEntry(PageTag orgEntry, ServiceContext serviceContext)
		throws PageTagValidateException, PortalException;

	/**
	 * Updates the page tag in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public PageTag updatePageTag(PageTag pageTag);

}