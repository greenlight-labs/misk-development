/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import events.exception.NoSuchVisitorsException;

import events.model.EventVisitors;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the event visitors service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventVisitorsUtil
 * @generated
 */
@ProviderType
public interface EventVisitorsPersistence
	extends BasePersistence<EventVisitors> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EventVisitorsUtil} to access the event visitors persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the event visitorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid(String uuid);

	/**
	 * Returns a range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public EventVisitors[] findByUuid_PrevAndNext(
			long visitor_id, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Removes all the event visitorses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of event visitorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching event visitorses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchVisitorsException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByUUID_G(String uuid, long groupId)
		throws NoSuchVisitorsException;

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the event visitors where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the event visitors that was removed
	 */
	public EventVisitors removeByUUID_G(String uuid, long groupId)
		throws NoSuchVisitorsException;

	/**
	 * Returns the number of event visitorses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching event visitorses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public EventVisitors[] findByUuid_C_PrevAndNext(
			long visitor_id, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Removes all the event visitorses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching event visitorses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the event visitorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching event visitorses
	 */
	public java.util.List<EventVisitors> findByGroupId(long groupId);

	/**
	 * Returns a range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns an ordered range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the first event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the last event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the last event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public EventVisitors[] findByGroupId_PrevAndNext(
			long visitor_id, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Removes all the event visitorses where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of event visitorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching event visitorses
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the event visitorses where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @return the matching event visitorses
	 */
	public java.util.List<EventVisitors> findByEventVisitorId(
		long eventvisitorId);

	/**
	 * Returns a range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end);

	/**
	 * Returns an ordered range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns an ordered range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByEventVisitorId_First(
			long eventvisitorId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the first event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByEventVisitorId_First(
		long eventvisitorId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the last event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByEventVisitorId_Last(
			long eventvisitorId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the last event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByEventVisitorId_Last(
		long eventvisitorId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public EventVisitors[] findByEventVisitorId_PrevAndNext(
			long visitor_id, long eventvisitorId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Removes all the event visitorses where eventvisitorId = &#63; from the database.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 */
	public void removeByEventVisitorId(long eventvisitorId);

	/**
	 * Returns the number of event visitorses where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @return the number of matching event visitorses
	 */
	public int countByEventVisitorId(long eventvisitorId);

	/**
	 * Returns all the event visitorses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching event visitorses
	 */
	public java.util.List<EventVisitors> findByeventuserId(long userId);

	/**
	 * Returns a range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByeventuserId(
		long userId, int start, int end);

	/**
	 * Returns an ordered range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByeventuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns an ordered range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public java.util.List<EventVisitors> findByeventuserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByeventuserId_First(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the first event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByeventuserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the last event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public EventVisitors findByeventuserId_Last(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Returns the last event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public EventVisitors fetchByeventuserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where userId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public EventVisitors[] findByeventuserId_PrevAndNext(
			long visitor_id, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
				orderByComparator)
		throws NoSuchVisitorsException;

	/**
	 * Removes all the event visitorses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public void removeByeventuserId(long userId);

	/**
	 * Returns the number of event visitorses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching event visitorses
	 */
	public int countByeventuserId(long userId);

	/**
	 * Caches the event visitors in the entity cache if it is enabled.
	 *
	 * @param eventVisitors the event visitors
	 */
	public void cacheResult(EventVisitors eventVisitors);

	/**
	 * Caches the event visitorses in the entity cache if it is enabled.
	 *
	 * @param eventVisitorses the event visitorses
	 */
	public void cacheResult(java.util.List<EventVisitors> eventVisitorses);

	/**
	 * Creates a new event visitors with the primary key. Does not add the event visitors to the database.
	 *
	 * @param visitor_id the primary key for the new event visitors
	 * @return the new event visitors
	 */
	public EventVisitors create(long visitor_id);

	/**
	 * Removes the event visitors with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors that was removed
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public EventVisitors remove(long visitor_id) throws NoSuchVisitorsException;

	public EventVisitors updateImpl(EventVisitors eventVisitors);

	/**
	 * Returns the event visitors with the primary key or throws a <code>NoSuchVisitorsException</code> if it could not be found.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public EventVisitors findByPrimaryKey(long visitor_id)
		throws NoSuchVisitorsException;

	/**
	 * Returns the event visitors with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors, or <code>null</code> if a event visitors with the primary key could not be found
	 */
	public EventVisitors fetchByPrimaryKey(long visitor_id);

	/**
	 * Returns all the event visitorses.
	 *
	 * @return the event visitorses
	 */
	public java.util.List<EventVisitors> findAll();

	/**
	 * Returns a range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of event visitorses
	 */
	public java.util.List<EventVisitors> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of event visitorses
	 */
	public java.util.List<EventVisitors> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator);

	/**
	 * Returns an ordered range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of event visitorses
	 */
	public java.util.List<EventVisitors> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventVisitors>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the event visitorses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of event visitorses.
	 *
	 * @return the number of event visitorses
	 */
	public int countAll();

}