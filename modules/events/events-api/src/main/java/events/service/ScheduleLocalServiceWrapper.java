/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ScheduleLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ScheduleLocalService
 * @generated
 */
public class ScheduleLocalServiceWrapper
	implements ScheduleLocalService, ServiceWrapper<ScheduleLocalService> {

	public ScheduleLocalServiceWrapper(
		ScheduleLocalService scheduleLocalService) {

		_scheduleLocalService = scheduleLocalService;
	}

	@Override
	public events.model.Schedule addEntry(
			events.model.Schedule orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   events.exception.ScheduleValidateException {

		return _scheduleLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the schedule to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param schedule the schedule
	 * @return the schedule that was added
	 */
	@Override
	public events.model.Schedule addSchedule(events.model.Schedule schedule) {
		return _scheduleLocalService.addSchedule(schedule);
	}

	@Override
	public int countAllInEvent(long eventId) {
		return _scheduleLocalService.countAllInEvent(eventId);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _scheduleLocalService.countAllInGroup(groupId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _scheduleLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new schedule with the primary key. Does not add the schedule to the database.
	 *
	 * @param scheduleId the primary key for the new schedule
	 * @return the new schedule
	 */
	@Override
	public events.model.Schedule createSchedule(long scheduleId) {
		return _scheduleLocalService.createSchedule(scheduleId);
	}

	@Override
	public events.model.Schedule deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _scheduleLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _scheduleLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the schedule with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param scheduleId the primary key of the schedule
	 * @return the schedule that was removed
	 * @throws PortalException if a schedule with the primary key could not be found
	 */
	@Override
	public events.model.Schedule deleteSchedule(long scheduleId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _scheduleLocalService.deleteSchedule(scheduleId);
	}

	/**
	 * Deletes the schedule from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param schedule the schedule
	 * @return the schedule that was removed
	 */
	@Override
	public events.model.Schedule deleteSchedule(
		events.model.Schedule schedule) {

		return _scheduleLocalService.deleteSchedule(schedule);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _scheduleLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _scheduleLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _scheduleLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _scheduleLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _scheduleLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _scheduleLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public events.model.Schedule fetchSchedule(long scheduleId) {
		return _scheduleLocalService.fetchSchedule(scheduleId);
	}

	/**
	 * Returns the schedule matching the UUID and group.
	 *
	 * @param uuid the schedule's UUID
	 * @param groupId the primary key of the group
	 * @return the matching schedule, or <code>null</code> if a matching schedule could not be found
	 */
	@Override
	public events.model.Schedule fetchScheduleByUuidAndGroupId(
		String uuid, long groupId) {

		return _scheduleLocalService.fetchScheduleByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<events.model.Schedule> findAllInEvent(long eventId) {
		return _scheduleLocalService.findAllInEvent(eventId);
	}

	@Override
	public java.util.List<events.model.Schedule> findAllInEvent(
		long eventId, int start, int end) {

		return _scheduleLocalService.findAllInEvent(eventId, start, end);
	}

	@Override
	public java.util.List<events.model.Schedule> findAllInEvent(
		long eventId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Schedule>
			obc) {

		return _scheduleLocalService.findAllInEvent(eventId, start, end, obc);
	}

	@Override
	public java.util.List<events.model.Schedule> findAllInGroup(long groupId) {
		return _scheduleLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<events.model.Schedule> findAllInGroup(
		long groupId, int start, int end) {

		return _scheduleLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<events.model.Schedule> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Schedule>
			obc) {

		return _scheduleLocalService.findAllInGroup(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _scheduleLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _scheduleLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _scheduleLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public events.model.Schedule getNewObject(long primaryKey) {
		return _scheduleLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _scheduleLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _scheduleLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the schedule with the primary key.
	 *
	 * @param scheduleId the primary key of the schedule
	 * @return the schedule
	 * @throws PortalException if a schedule with the primary key could not be found
	 */
	@Override
	public events.model.Schedule getSchedule(long scheduleId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _scheduleLocalService.getSchedule(scheduleId);
	}

	/**
	 * Returns the schedule matching the UUID and group.
	 *
	 * @param uuid the schedule's UUID
	 * @param groupId the primary key of the group
	 * @return the matching schedule
	 * @throws PortalException if a matching schedule could not be found
	 */
	@Override
	public events.model.Schedule getScheduleByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _scheduleLocalService.getScheduleByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public events.model.Schedule getScheduleFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws events.exception.ScheduleValidateException,
			   javax.portlet.PortletException {

		return _scheduleLocalService.getScheduleFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the schedules.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.ScheduleModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @return the range of schedules
	 */
	@Override
	public java.util.List<events.model.Schedule> getSchedules(
		int start, int end) {

		return _scheduleLocalService.getSchedules(start, end);
	}

	/**
	 * Returns all the schedules matching the UUID and company.
	 *
	 * @param uuid the UUID of the schedules
	 * @param companyId the primary key of the company
	 * @return the matching schedules, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.Schedule> getSchedulesByUuidAndCompanyId(
		String uuid, long companyId) {

		return _scheduleLocalService.getSchedulesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of schedules matching the UUID and company.
	 *
	 * @param uuid the UUID of the schedules
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of schedules
	 * @param end the upper bound of the range of schedules (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching schedules, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.Schedule> getSchedulesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.Schedule>
			orderByComparator) {

		return _scheduleLocalService.getSchedulesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of schedules.
	 *
	 * @return the number of schedules
	 */
	@Override
	public int getSchedulesCount() {
		return _scheduleLocalService.getSchedulesCount();
	}

	@Override
	public events.model.Schedule updateEntry(
			events.model.Schedule orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   events.exception.ScheduleValidateException {

		return _scheduleLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the schedule in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ScheduleLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param schedule the schedule
	 * @return the schedule that was updated
	 */
	@Override
	public events.model.Schedule updateSchedule(
		events.model.Schedule schedule) {

		return _scheduleLocalService.updateSchedule(schedule);
	}

	@Override
	public ScheduleLocalService getWrappedService() {
		return _scheduleLocalService;
	}

	@Override
	public void setWrappedService(ScheduleLocalService scheduleLocalService) {
		_scheduleLocalService = scheduleLocalService;
	}

	private ScheduleLocalService _scheduleLocalService;

}