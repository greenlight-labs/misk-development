/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PageTagLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PageTagLocalService
 * @generated
 */
public class PageTagLocalServiceWrapper
	implements PageTagLocalService, ServiceWrapper<PageTagLocalService> {

	public PageTagLocalServiceWrapper(PageTagLocalService pageTagLocalService) {
		_pageTagLocalService = pageTagLocalService;
	}

	@Override
	public events.model.PageTag addEntry(
			events.model.PageTag orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   events.exception.PageTagValidateException {

		return _pageTagLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the page tag to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was added
	 */
	@Override
	public events.model.PageTag addPageTag(events.model.PageTag pageTag) {
		return _pageTagLocalService.addPageTag(pageTag);
	}

	@Override
	public int countByGroupId(long groupId) {
		return _pageTagLocalService.countByGroupId(groupId);
	}

	/**
	 * Creates a new page tag with the primary key. Does not add the page tag to the database.
	 *
	 * @param pageId the primary key for the new page tag
	 * @return the new page tag
	 */
	@Override
	public events.model.PageTag createPageTag(long pageId) {
		return _pageTagLocalService.createPageTag(pageId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pageTagLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public events.model.PageTag deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pageTagLocalService.deleteEntry(primaryKey);
	}

	/**
	 * Deletes the page tag with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag that was removed
	 * @throws PortalException if a page tag with the primary key could not be found
	 */
	@Override
	public events.model.PageTag deletePageTag(long pageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pageTagLocalService.deletePageTag(pageId);
	}

	/**
	 * Deletes the page tag from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was removed
	 */
	@Override
	public events.model.PageTag deletePageTag(events.model.PageTag pageTag) {
		return _pageTagLocalService.deletePageTag(pageTag);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pageTagLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _pageTagLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _pageTagLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _pageTagLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _pageTagLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _pageTagLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _pageTagLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public events.model.PageTag fetchByPageSlug(String pageSlug) {
		return _pageTagLocalService.fetchByPageSlug(pageSlug);
	}

	@Override
	public events.model.PageTag fetchPageTag(long pageId) {
		return _pageTagLocalService.fetchPageTag(pageId);
	}

	/**
	 * Returns the page tag matching the UUID and group.
	 *
	 * @param uuid the page tag's UUID
	 * @param groupId the primary key of the group
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	@Override
	public events.model.PageTag fetchPageTagByUuidAndGroupId(
		String uuid, long groupId) {

		return _pageTagLocalService.fetchPageTagByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<events.model.PageTag> findByGroupId(long groupId) {
		return _pageTagLocalService.findByGroupId(groupId);
	}

	@Override
	public java.util.List<events.model.PageTag> findByGroupId(
		long groupId, int start, int end) {

		return _pageTagLocalService.findByGroupId(groupId, start, end);
	}

	@Override
	public java.util.List<events.model.PageTag> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.PageTag>
			obc) {

		return _pageTagLocalService.findByGroupId(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _pageTagLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _pageTagLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _pageTagLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public events.model.PageTag getNewObject(long primaryKey) {
		return _pageTagLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _pageTagLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * Returns the page tag with the primary key.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag
	 * @throws PortalException if a page tag with the primary key could not be found
	 */
	@Override
	public events.model.PageTag getPageTag(long pageId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pageTagLocalService.getPageTag(pageId);
	}

	/**
	 * Returns the page tag matching the UUID and group.
	 *
	 * @param uuid the page tag's UUID
	 * @param groupId the primary key of the group
	 * @return the matching page tag
	 * @throws PortalException if a matching page tag could not be found
	 */
	@Override
	public events.model.PageTag getPageTagByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pageTagLocalService.getPageTagByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public events.model.PageTag getPageTagFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws events.exception.PageTagValidateException,
			   javax.portlet.PortletException {

		return _pageTagLocalService.getPageTagFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of page tags
	 */
	@Override
	public java.util.List<events.model.PageTag> getPageTags(
		int start, int end) {

		return _pageTagLocalService.getPageTags(start, end);
	}

	/**
	 * Returns all the page tags matching the UUID and company.
	 *
	 * @param uuid the UUID of the page tags
	 * @param companyId the primary key of the company
	 * @return the matching page tags, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.PageTag> getPageTagsByUuidAndCompanyId(
		String uuid, long companyId) {

		return _pageTagLocalService.getPageTagsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of page tags matching the UUID and company.
	 *
	 * @param uuid the UUID of the page tags
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching page tags, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<events.model.PageTag> getPageTagsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<events.model.PageTag>
			orderByComparator) {

		return _pageTagLocalService.getPageTagsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of page tags.
	 *
	 * @return the number of page tags
	 */
	@Override
	public int getPageTagsCount() {
		return _pageTagLocalService.getPageTagsCount();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pageTagLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public events.model.PageTag updateEntry(
			events.model.PageTag orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   events.exception.PageTagValidateException {

		return _pageTagLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the page tag in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PageTagLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pageTag the page tag
	 * @return the page tag that was updated
	 */
	@Override
	public events.model.PageTag updatePageTag(events.model.PageTag pageTag) {
		return _pageTagLocalService.updatePageTag(pageTag);
	}

	@Override
	public PageTagLocalService getWrappedService() {
		return _pageTagLocalService;
	}

	@Override
	public void setWrappedService(PageTagLocalService pageTagLocalService) {
		_pageTagLocalService = pageTagLocalService;
	}

	private PageTagLocalService _pageTagLocalService;

}