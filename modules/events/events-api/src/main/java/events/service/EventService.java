/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.exception.NoSuchEventException;

import events.model.Event;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the remote service interface for Event. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see EventServiceUtil
 * @generated
 */
@AccessControlled
@JSONWebService
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface EventService extends BaseService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>events.service.impl.EventServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the event remote service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link EventServiceUtil} if injection and service tracking are not available.
	 */
	public List<Event> findByTitle(String title);

	public List<Event> findByTitle(String title, int start, int end);

	public List<Event> findByTitle(
		String title, int start, int end, OrderByComparator<Event> obc);

	public int findByTitleCount(String title);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Event getEventById(long eventId) throws NoSuchEventException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEvents(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEvents(long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEvents(
		long groupId, int start, int end, OrderByComparator<Event> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByCategory(long categoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByCategory(long categoryId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByCategory(
		long categoryId, int start, int end, OrderByComparator<Event> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEventsByCategoryCount(long categoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByType(int type);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByType(int type, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByType(
		int type, int start, int end, OrderByComparator<Event> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEventsByTypeCount(int type);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEventsCount(long groupId);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	public List<Event> removeExpiredEvent(List<Event> eventList);

}