/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.EventVisitors;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for EventVisitors. This utility wraps
 * <code>events.service.impl.EventVisitorsLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EventVisitorsLocalService
 * @generated
 */
public class EventVisitorsLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>events.service.impl.EventVisitorsLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the event visitors to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventVisitors the event visitors
	 * @return the event visitors that was added
	 */
	public static EventVisitors addEventVisitors(EventVisitors eventVisitors) {
		return getService().addEventVisitors(eventVisitors);
	}

	public static EventVisitors addEventVisitors(
			long userId, long eventId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addEventVisitors(userId, eventId, serviceContext);
	}

	/**
	 * Creates a new event visitors with the primary key. Does not add the event visitors to the database.
	 *
	 * @param visitor_id the primary key for the new event visitors
	 * @return the new event visitors
	 */
	public static EventVisitors createEventVisitors(long visitor_id) {
		return getService().createEventVisitors(visitor_id);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the event visitors from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventVisitors the event visitors
	 * @return the event visitors that was removed
	 */
	public static EventVisitors deleteEventVisitors(
		EventVisitors eventVisitors) {

		return getService().deleteEventVisitors(eventVisitors);
	}

	/**
	 * Deletes the event visitors with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors that was removed
	 * @throws PortalException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors deleteEventVisitors(long visitor_id)
		throws PortalException {

		return getService().deleteEventVisitors(visitor_id);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static EventVisitors fetchEventVisitors(long visitor_id) {
		return getService().fetchEventVisitors(visitor_id);
	}

	/**
	 * Returns the event visitors matching the UUID and group.
	 *
	 * @param uuid the event visitors's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchEventVisitorsByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchEventVisitorsByUuidAndGroupId(uuid, groupId);
	}

	public static List<EventVisitors> findEventByUser(long userId) {
		return getService().findEventByUser(userId);
	}

	public static List<EventVisitors> findEventByvisitor(long eventid) {
		return getService().findEventByvisitor(eventid);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the event visitors with the primary key.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors
	 * @throws PortalException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors getEventVisitors(long visitor_id)
		throws PortalException {

		return getService().getEventVisitors(visitor_id);
	}

	/**
	 * Returns the event visitors matching the UUID and group.
	 *
	 * @param uuid the event visitors's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event visitors
	 * @throws PortalException if a matching event visitors could not be found
	 */
	public static EventVisitors getEventVisitorsByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getEventVisitorsByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of event visitorses
	 */
	public static List<EventVisitors> getEventVisitorses(int start, int end) {
		return getService().getEventVisitorses(start, end);
	}

	/**
	 * Returns all the event visitorses matching the UUID and company.
	 *
	 * @param uuid the UUID of the event visitorses
	 * @param companyId the primary key of the company
	 * @return the matching event visitorses, or an empty list if no matches were found
	 */
	public static List<EventVisitors> getEventVisitorsesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getEventVisitorsesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of event visitorses matching the UUID and company.
	 *
	 * @param uuid the UUID of the event visitorses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching event visitorses, or an empty list if no matches were found
	 */
	public static List<EventVisitors> getEventVisitorsesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getService().getEventVisitorsesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of event visitorses.
	 *
	 * @return the number of event visitorses
	 */
	public static int getEventVisitorsesCount() {
		return getService().getEventVisitorsesCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the event visitors in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventVisitorsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventVisitors the event visitors
	 * @return the event visitors that was updated
	 */
	public static EventVisitors updateEventVisitors(
		EventVisitors eventVisitors) {

		return getService().updateEventVisitors(eventVisitors);
	}

	public static EventVisitorsLocalService getService() {
		return _service;
	}

	private static volatile EventVisitorsLocalService _service;

}