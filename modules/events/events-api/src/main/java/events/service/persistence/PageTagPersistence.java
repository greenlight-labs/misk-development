/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import events.exception.NoSuchPageTagException;

import events.model.PageTag;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the page tag service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PageTagUtil
 * @generated
 */
@ProviderType
public interface PageTagPersistence extends BasePersistence<PageTag> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PageTagUtil} to access the page tag persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the page tags where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching page tags
	 */
	public java.util.List<PageTag> findByUuid(String uuid);

	/**
	 * Returns a range of all the page tags where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of matching page tags
	 */
	public java.util.List<PageTag> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching page tags
	 */
	public java.util.List<PageTag> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching page tags
	 */
	public java.util.List<PageTag> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns the page tags before and after the current page tag in the ordered set where uuid = &#63;.
	 *
	 * @param pageId the primary key of the current page tag
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public PageTag[] findByUuid_PrevAndNext(
			long pageId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Removes all the page tags where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of page tags where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching page tags
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the page tag where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchPageTagException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByUUID_G(String uuid, long groupId)
		throws NoSuchPageTagException;

	/**
	 * Returns the page tag where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the page tag where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the page tag where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the page tag that was removed
	 */
	public PageTag removeByUUID_G(String uuid, long groupId)
		throws NoSuchPageTagException;

	/**
	 * Returns the number of page tags where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching page tags
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching page tags
	 */
	public java.util.List<PageTag> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of matching page tags
	 */
	public java.util.List<PageTag> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching page tags
	 */
	public java.util.List<PageTag> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns an ordered range of all the page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching page tags
	 */
	public java.util.List<PageTag> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Returns the first page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Returns the last page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns the page tags before and after the current page tag in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param pageId the primary key of the current page tag
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public PageTag[] findByUuid_C_PrevAndNext(
			long pageId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Removes all the page tags where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of page tags where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching page tags
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the page tags where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching page tags
	 */
	public java.util.List<PageTag> findByGroupId(long groupId);

	/**
	 * Returns a range of all the page tags where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of matching page tags
	 */
	public java.util.List<PageTag> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the page tags where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching page tags
	 */
	public java.util.List<PageTag> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns an ordered range of all the page tags where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching page tags
	 */
	public java.util.List<PageTag> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Returns the first page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns the last page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Returns the last page tag in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns the page tags before and after the current page tag in the ordered set where groupId = &#63;.
	 *
	 * @param pageId the primary key of the current page tag
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public PageTag[] findByGroupId_PrevAndNext(
			long pageId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<PageTag>
				orderByComparator)
		throws NoSuchPageTagException;

	/**
	 * Removes all the page tags where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of page tags where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching page tags
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns the page tag where pageSlug = &#63; or throws a <code>NoSuchPageTagException</code> if it could not be found.
	 *
	 * @param pageSlug the page slug
	 * @return the matching page tag
	 * @throws NoSuchPageTagException if a matching page tag could not be found
	 */
	public PageTag findByPageSlug(String pageSlug)
		throws NoSuchPageTagException;

	/**
	 * Returns the page tag where pageSlug = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param pageSlug the page slug
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByPageSlug(String pageSlug);

	/**
	 * Returns the page tag where pageSlug = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param pageSlug the page slug
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching page tag, or <code>null</code> if a matching page tag could not be found
	 */
	public PageTag fetchByPageSlug(String pageSlug, boolean useFinderCache);

	/**
	 * Removes the page tag where pageSlug = &#63; from the database.
	 *
	 * @param pageSlug the page slug
	 * @return the page tag that was removed
	 */
	public PageTag removeByPageSlug(String pageSlug)
		throws NoSuchPageTagException;

	/**
	 * Returns the number of page tags where pageSlug = &#63;.
	 *
	 * @param pageSlug the page slug
	 * @return the number of matching page tags
	 */
	public int countByPageSlug(String pageSlug);

	/**
	 * Caches the page tag in the entity cache if it is enabled.
	 *
	 * @param pageTag the page tag
	 */
	public void cacheResult(PageTag pageTag);

	/**
	 * Caches the page tags in the entity cache if it is enabled.
	 *
	 * @param pageTags the page tags
	 */
	public void cacheResult(java.util.List<PageTag> pageTags);

	/**
	 * Creates a new page tag with the primary key. Does not add the page tag to the database.
	 *
	 * @param pageId the primary key for the new page tag
	 * @return the new page tag
	 */
	public PageTag create(long pageId);

	/**
	 * Removes the page tag with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag that was removed
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public PageTag remove(long pageId) throws NoSuchPageTagException;

	public PageTag updateImpl(PageTag pageTag);

	/**
	 * Returns the page tag with the primary key or throws a <code>NoSuchPageTagException</code> if it could not be found.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag
	 * @throws NoSuchPageTagException if a page tag with the primary key could not be found
	 */
	public PageTag findByPrimaryKey(long pageId) throws NoSuchPageTagException;

	/**
	 * Returns the page tag with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param pageId the primary key of the page tag
	 * @return the page tag, or <code>null</code> if a page tag with the primary key could not be found
	 */
	public PageTag fetchByPrimaryKey(long pageId);

	/**
	 * Returns all the page tags.
	 *
	 * @return the page tags
	 */
	public java.util.List<PageTag> findAll();

	/**
	 * Returns a range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @return the range of page tags
	 */
	public java.util.List<PageTag> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of page tags
	 */
	public java.util.List<PageTag> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator);

	/**
	 * Returns an ordered range of all the page tags.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PageTagModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of page tags
	 * @param end the upper bound of the range of page tags (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of page tags
	 */
	public java.util.List<PageTag> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PageTag>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the page tags from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of page tags.
	 *
	 * @return the number of page tags
	 */
	public int countAll();

}