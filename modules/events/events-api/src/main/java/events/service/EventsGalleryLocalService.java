/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.exception.EventsGalleryValidateException;

import events.model.EventsGallery;

import java.io.Serializable;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for EventsGallery. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see EventsGalleryLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface EventsGalleryLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>events.service.impl.EventsGalleryLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the events gallery local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link EventsGalleryLocalServiceUtil} if injection and service tracking are not available.
	 */
	public EventsGallery addEntry(
			EventsGallery orgEntry, ServiceContext serviceContext)
		throws EventsGalleryValidateException, PortalException;

	/**
	 * Adds the events gallery to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public EventsGallery addEventsGallery(EventsGallery eventsGallery);

	public int countAllInDiscover(long discoverId);

	public int countAllInGroup(long groupId);

	/**
	 * Creates a new events gallery with the primary key. Does not add the events gallery to the database.
	 *
	 * @param slideId the primary key for the new events gallery
	 * @return the new events gallery
	 */
	@Transactional(enabled = false)
	public EventsGallery createEventsGallery(long slideId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public EventsGallery deleteEntry(long primaryKey) throws PortalException;

	/**
	 * Deletes the events gallery from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public EventsGallery deleteEventsGallery(EventsGallery eventsGallery);

	/**
	 * Deletes the events gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery that was removed
	 * @throws PortalException if a events gallery with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public EventsGallery deleteEventsGallery(long slideId)
		throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public EventsGallery fetchEventsGallery(long slideId);

	/**
	 * Returns the events gallery matching the UUID and group.
	 *
	 * @param uuid the events gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public EventsGallery fetchEventsGalleryByUuidAndGroupId(
		String uuid, long groupId);

	public List<EventsGallery> findAllInDiscover(long discoverId);

	public List<EventsGallery> findAllInDiscover(
		long discoverId, int start, int end);

	public List<EventsGallery> findAllInDiscover(
		long discoverId, int start, int end,
		OrderByComparator<EventsGallery> obc);

	public List<EventsGallery> findAllInGroup(long groupId);

	public List<EventsGallery> findAllInGroup(long groupId, int start, int end);

	public List<EventsGallery> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<EventsGallery> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns a range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of events galleries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<EventsGallery> getEventsGalleries(int start, int end);

	/**
	 * Returns all the events galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the events galleries
	 * @param companyId the primary key of the company
	 * @return the matching events galleries, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<EventsGallery> getEventsGalleriesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of events galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the events galleries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching events galleries, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<EventsGallery> getEventsGalleriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator);

	/**
	 * Returns the number of events galleries.
	 *
	 * @return the number of events galleries
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEventsGalleriesCount();

	/**
	 * Returns the events gallery with the primary key.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery
	 * @throws PortalException if a events gallery with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public EventsGallery getEventsGallery(long slideId) throws PortalException;

	/**
	 * Returns the events gallery matching the UUID and group.
	 *
	 * @param uuid the events gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching events gallery
	 * @throws PortalException if a matching events gallery could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public EventsGallery getEventsGalleryByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public EventsGallery getGalleryFromRequest(
			long primaryKey, PortletRequest request)
		throws EventsGalleryValidateException, PortletException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public EventsGallery getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public EventsGallery updateEntry(
			EventsGallery orgEntry, ServiceContext serviceContext)
		throws EventsGalleryValidateException, PortalException;

	/**
	 * Updates the events gallery in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public EventsGallery updateEventsGallery(EventsGallery eventsGallery);

}