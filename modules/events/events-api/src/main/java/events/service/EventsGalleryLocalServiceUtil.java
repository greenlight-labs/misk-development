/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.EventsGallery;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for EventsGallery. This utility wraps
 * <code>events.service.impl.EventsGalleryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EventsGalleryLocalService
 * @generated
 */
public class EventsGalleryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>events.service.impl.EventsGalleryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static EventsGallery addEntry(
			EventsGallery orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws events.exception.EventsGalleryValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the events gallery to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was added
	 */
	public static EventsGallery addEventsGallery(EventsGallery eventsGallery) {
		return getService().addEventsGallery(eventsGallery);
	}

	public static int countAllInDiscover(long discoverId) {
		return getService().countAllInDiscover(discoverId);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * Creates a new events gallery with the primary key. Does not add the events gallery to the database.
	 *
	 * @param slideId the primary key for the new events gallery
	 * @return the new events gallery
	 */
	public static EventsGallery createEventsGallery(long slideId) {
		return getService().createEventsGallery(slideId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static EventsGallery deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * Deletes the events gallery from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was removed
	 */
	public static EventsGallery deleteEventsGallery(
		EventsGallery eventsGallery) {

		return getService().deleteEventsGallery(eventsGallery);
	}

	/**
	 * Deletes the events gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery that was removed
	 * @throws PortalException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery deleteEventsGallery(long slideId)
		throws PortalException {

		return getService().deleteEventsGallery(slideId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static EventsGallery fetchEventsGallery(long slideId) {
		return getService().fetchEventsGallery(slideId);
	}

	/**
	 * Returns the events gallery matching the UUID and group.
	 *
	 * @param uuid the events gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchEventsGalleryByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchEventsGalleryByUuidAndGroupId(uuid, groupId);
	}

	public static List<EventsGallery> findAllInDiscover(long discoverId) {
		return getService().findAllInDiscover(discoverId);
	}

	public static List<EventsGallery> findAllInDiscover(
		long discoverId, int start, int end) {

		return getService().findAllInDiscover(discoverId, start, end);
	}

	public static List<EventsGallery> findAllInDiscover(
		long discoverId, int start, int end,
		OrderByComparator<EventsGallery> obc) {

		return getService().findAllInDiscover(discoverId, start, end, obc);
	}

	public static List<EventsGallery> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<EventsGallery> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<EventsGallery> findAllInGroup(
		long groupId, int start, int end,
		OrderByComparator<EventsGallery> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of events galleries
	 */
	public static List<EventsGallery> getEventsGalleries(int start, int end) {
		return getService().getEventsGalleries(start, end);
	}

	/**
	 * Returns all the events galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the events galleries
	 * @param companyId the primary key of the company
	 * @return the matching events galleries, or an empty list if no matches were found
	 */
	public static List<EventsGallery> getEventsGalleriesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getEventsGalleriesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of events galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the events galleries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching events galleries, or an empty list if no matches were found
	 */
	public static List<EventsGallery> getEventsGalleriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getService().getEventsGalleriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of events galleries.
	 *
	 * @return the number of events galleries
	 */
	public static int getEventsGalleriesCount() {
		return getService().getEventsGalleriesCount();
	}

	/**
	 * Returns the events gallery with the primary key.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery
	 * @throws PortalException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery getEventsGallery(long slideId)
		throws PortalException {

		return getService().getEventsGallery(slideId);
	}

	/**
	 * Returns the events gallery matching the UUID and group.
	 *
	 * @param uuid the events gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching events gallery
	 * @throws PortalException if a matching events gallery could not be found
	 */
	public static EventsGallery getEventsGalleryByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getEventsGalleryByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static EventsGallery getGalleryFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws events.exception.EventsGalleryValidateException,
			   javax.portlet.PortletException {

		return getService().getGalleryFromRequest(primaryKey, request);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static EventsGallery getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static EventsGallery updateEntry(
			EventsGallery orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws events.exception.EventsGalleryValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the events gallery in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventsGalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventsGallery the events gallery
	 * @return the events gallery that was updated
	 */
	public static EventsGallery updateEventsGallery(
		EventsGallery eventsGallery) {

		return getService().updateEventsGallery(eventsGallery);
	}

	public static EventsGalleryLocalService getService() {
		return _service;
	}

	private static volatile EventsGalleryLocalService _service;

}