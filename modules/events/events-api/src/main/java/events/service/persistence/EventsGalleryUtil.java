/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.EventsGallery;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the events gallery service. This utility wraps <code>events.service.persistence.impl.EventsGalleryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventsGalleryPersistence
 * @generated
 */
public class EventsGalleryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(EventsGallery eventsGallery) {
		getPersistence().clearCache(eventsGallery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, EventsGallery> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EventsGallery> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EventsGallery> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EventsGallery> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static EventsGallery update(EventsGallery eventsGallery) {
		return getPersistence().update(eventsGallery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static EventsGallery update(
		EventsGallery eventsGallery, ServiceContext serviceContext) {

		return getPersistence().update(eventsGallery, serviceContext);
	}

	/**
	 * Returns all the events galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching events galleries
	 */
	public static List<EventsGallery> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the events galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public static List<EventsGallery> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByUuid_First(
			String uuid, OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByUuid_First(
		String uuid, OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByUuid_Last(
			String uuid, OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByUuid_Last(
		String uuid, OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByUuid_PrevAndNext(
			slideId, uuid, orderByComparator);
	}

	/**
	 * Removes all the events galleries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of events galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching events galleries
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the events gallery where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchsGalleryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByUUID_G(String uuid, long groupId)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the events gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the events gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the events gallery where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the events gallery that was removed
	 */
	public static EventsGallery removeByUUID_G(String uuid, long groupId)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of events galleries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching events galleries
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching events galleries
	 */
	public static List<EventsGallery> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public static List<EventsGallery> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			slideId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the events galleries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching events galleries
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the events galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching events galleries
	 */
	public static List<EventsGallery> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the events galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public static List<EventsGallery> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the events galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the events galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByGroupId_First(
			long groupId, OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByGroupId_First(
		long groupId, OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByGroupId_Last(
			long groupId, OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByGroupId_Last(
		long groupId, OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery[] findByGroupId_PrevAndNext(
			long slideId, long groupId,
			OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByGroupId_PrevAndNext(
			slideId, groupId, orderByComparator);
	}

	/**
	 * Removes all the events galleries where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of events galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching events galleries
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the events galleries where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @return the matching events galleries
	 */
	public static List<EventsGallery> findByEventId(long eventId) {
		return getPersistence().findByEventId(eventId);
	}

	/**
	 * Returns a range of all the events galleries where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public static List<EventsGallery> findByEventId(
		long eventId, int start, int end) {

		return getPersistence().findByEventId(eventId, start, end);
	}

	/**
	 * Returns an ordered range of all the events galleries where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByEventId(
		long eventId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().findByEventId(
			eventId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the events galleries where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public static List<EventsGallery> findByEventId(
		long eventId, int start, int end,
		OrderByComparator<EventsGallery> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByEventId(
			eventId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByEventId_First(
			long eventId, OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByEventId_First(eventId, orderByComparator);
	}

	/**
	 * Returns the first events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByEventId_First(
		long eventId, OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByEventId_First(
			eventId, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public static EventsGallery findByEventId_Last(
			long eventId, OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByEventId_Last(eventId, orderByComparator);
	}

	/**
	 * Returns the last events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public static EventsGallery fetchByEventId_Last(
		long eventId, OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().fetchByEventId_Last(eventId, orderByComparator);
	}

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery[] findByEventId_PrevAndNext(
			long slideId, long eventId,
			OrderByComparator<EventsGallery> orderByComparator)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByEventId_PrevAndNext(
			slideId, eventId, orderByComparator);
	}

	/**
	 * Removes all the events galleries where eventId = &#63; from the database.
	 *
	 * @param eventId the event ID
	 */
	public static void removeByEventId(long eventId) {
		getPersistence().removeByEventId(eventId);
	}

	/**
	 * Returns the number of events galleries where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @return the number of matching events galleries
	 */
	public static int countByEventId(long eventId) {
		return getPersistence().countByEventId(eventId);
	}

	/**
	 * Caches the events gallery in the entity cache if it is enabled.
	 *
	 * @param eventsGallery the events gallery
	 */
	public static void cacheResult(EventsGallery eventsGallery) {
		getPersistence().cacheResult(eventsGallery);
	}

	/**
	 * Caches the events galleries in the entity cache if it is enabled.
	 *
	 * @param eventsGalleries the events galleries
	 */
	public static void cacheResult(List<EventsGallery> eventsGalleries) {
		getPersistence().cacheResult(eventsGalleries);
	}

	/**
	 * Creates a new events gallery with the primary key. Does not add the events gallery to the database.
	 *
	 * @param slideId the primary key for the new events gallery
	 * @return the new events gallery
	 */
	public static EventsGallery create(long slideId) {
		return getPersistence().create(slideId);
	}

	/**
	 * Removes the events gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery that was removed
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery remove(long slideId)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().remove(slideId);
	}

	public static EventsGallery updateImpl(EventsGallery eventsGallery) {
		return getPersistence().updateImpl(eventsGallery);
	}

	/**
	 * Returns the events gallery with the primary key or throws a <code>NoSuchsGalleryException</code> if it could not be found.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public static EventsGallery findByPrimaryKey(long slideId)
		throws events.exception.NoSuchsGalleryException {

		return getPersistence().findByPrimaryKey(slideId);
	}

	/**
	 * Returns the events gallery with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery, or <code>null</code> if a events gallery with the primary key could not be found
	 */
	public static EventsGallery fetchByPrimaryKey(long slideId) {
		return getPersistence().fetchByPrimaryKey(slideId);
	}

	/**
	 * Returns all the events galleries.
	 *
	 * @return the events galleries
	 */
	public static List<EventsGallery> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of events galleries
	 */
	public static List<EventsGallery> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of events galleries
	 */
	public static List<EventsGallery> findAll(
		int start, int end,
		OrderByComparator<EventsGallery> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of events galleries
	 */
	public static List<EventsGallery> findAll(
		int start, int end, OrderByComparator<EventsGallery> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the events galleries from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of events galleries.
	 *
	 * @return the number of events galleries
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static EventsGalleryPersistence getPersistence() {
		return _persistence;
	}

	private static volatile EventsGalleryPersistence _persistence;

}