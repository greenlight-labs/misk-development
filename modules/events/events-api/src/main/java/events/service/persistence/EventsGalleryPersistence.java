/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import events.exception.NoSuchsGalleryException;

import events.model.EventsGallery;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the events gallery service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventsGalleryUtil
 * @generated
 */
@ProviderType
public interface EventsGalleryPersistence
	extends BasePersistence<EventsGallery> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EventsGalleryUtil} to access the events gallery persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the events galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid(String uuid);

	/**
	 * Returns a range of all the events galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public EventsGallery[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Removes all the events galleries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of events galleries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching events galleries
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the events gallery where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchsGalleryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByUUID_G(String uuid, long groupId)
		throws NoSuchsGalleryException;

	/**
	 * Returns the events gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the events gallery where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the events gallery where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the events gallery that was removed
	 */
	public EventsGallery removeByUUID_G(String uuid, long groupId)
		throws NoSuchsGalleryException;

	/**
	 * Returns the number of events galleries where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching events galleries
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the first events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the last events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public EventsGallery[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Removes all the events galleries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of events galleries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching events galleries
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the events galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching events galleries
	 */
	public java.util.List<EventsGallery> findByGroupId(long groupId);

	/**
	 * Returns a range of all the events galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the events galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the events galleries where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the first events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the last events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the last events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where groupId = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public EventsGallery[] findByGroupId_PrevAndNext(
			long slideId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Removes all the events galleries where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of events galleries where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching events galleries
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the events galleries where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @return the matching events galleries
	 */
	public java.util.List<EventsGallery> findByEventId(long eventId);

	/**
	 * Returns a range of all the events galleries where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByEventId(
		long eventId, int start, int end);

	/**
	 * Returns an ordered range of all the events galleries where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByEventId(
		long eventId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the events galleries where eventId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param eventId the event ID
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching events galleries
	 */
	public java.util.List<EventsGallery> findByEventId(
		long eventId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByEventId_First(
			long eventId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the first events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByEventId_First(
		long eventId,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the last events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery
	 * @throws NoSuchsGalleryException if a matching events gallery could not be found
	 */
	public EventsGallery findByEventId_Last(
			long eventId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Returns the last events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching events gallery, or <code>null</code> if a matching events gallery could not be found
	 */
	public EventsGallery fetchByEventId_Last(
		long eventId,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns the events galleries before and after the current events gallery in the ordered set where eventId = &#63;.
	 *
	 * @param slideId the primary key of the current events gallery
	 * @param eventId the event ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public EventsGallery[] findByEventId_PrevAndNext(
			long slideId, long eventId,
			com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
				orderByComparator)
		throws NoSuchsGalleryException;

	/**
	 * Removes all the events galleries where eventId = &#63; from the database.
	 *
	 * @param eventId the event ID
	 */
	public void removeByEventId(long eventId);

	/**
	 * Returns the number of events galleries where eventId = &#63;.
	 *
	 * @param eventId the event ID
	 * @return the number of matching events galleries
	 */
	public int countByEventId(long eventId);

	/**
	 * Caches the events gallery in the entity cache if it is enabled.
	 *
	 * @param eventsGallery the events gallery
	 */
	public void cacheResult(EventsGallery eventsGallery);

	/**
	 * Caches the events galleries in the entity cache if it is enabled.
	 *
	 * @param eventsGalleries the events galleries
	 */
	public void cacheResult(java.util.List<EventsGallery> eventsGalleries);

	/**
	 * Creates a new events gallery with the primary key. Does not add the events gallery to the database.
	 *
	 * @param slideId the primary key for the new events gallery
	 * @return the new events gallery
	 */
	public EventsGallery create(long slideId);

	/**
	 * Removes the events gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery that was removed
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public EventsGallery remove(long slideId) throws NoSuchsGalleryException;

	public EventsGallery updateImpl(EventsGallery eventsGallery);

	/**
	 * Returns the events gallery with the primary key or throws a <code>NoSuchsGalleryException</code> if it could not be found.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery
	 * @throws NoSuchsGalleryException if a events gallery with the primary key could not be found
	 */
	public EventsGallery findByPrimaryKey(long slideId)
		throws NoSuchsGalleryException;

	/**
	 * Returns the events gallery with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the events gallery
	 * @return the events gallery, or <code>null</code> if a events gallery with the primary key could not be found
	 */
	public EventsGallery fetchByPrimaryKey(long slideId);

	/**
	 * Returns all the events galleries.
	 *
	 * @return the events galleries
	 */
	public java.util.List<EventsGallery> findAll();

	/**
	 * Returns a range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @return the range of events galleries
	 */
	public java.util.List<EventsGallery> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of events galleries
	 */
	public java.util.List<EventsGallery> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator);

	/**
	 * Returns an ordered range of all the events galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventsGalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events galleries
	 * @param end the upper bound of the range of events galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of events galleries
	 */
	public java.util.List<EventsGallery> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EventsGallery>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the events galleries from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of events galleries.
	 *
	 * @return the number of events galleries
	 */
	public int countAll();

}