/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.EventVisitors;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the event visitors service. This utility wraps <code>events.service.persistence.impl.EventVisitorsPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EventVisitorsPersistence
 * @generated
 */
public class EventVisitorsUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(EventVisitors eventVisitors) {
		getPersistence().clearCache(eventVisitors);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, EventVisitors> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EventVisitors> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EventVisitors> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EventVisitors> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static EventVisitors update(EventVisitors eventVisitors) {
		return getPersistence().update(eventVisitors);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static EventVisitors update(
		EventVisitors eventVisitors, ServiceContext serviceContext) {

		return getPersistence().update(eventVisitors, serviceContext);
	}

	/**
	 * Returns all the event visitorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching event visitorses
	 */
	public static List<EventVisitors> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public static List<EventVisitors> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByUuid_First(
			String uuid, OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByUuid_First(
		String uuid, OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByUuid_Last(
			String uuid, OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByUuid_Last(
		String uuid, OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where uuid = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors[] findByUuid_PrevAndNext(
			long visitor_id, String uuid,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByUuid_PrevAndNext(
			visitor_id, uuid, orderByComparator);
	}

	/**
	 * Removes all the event visitorses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of event visitorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching event visitorses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchVisitorsException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByUUID_G(String uuid, long groupId)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the event visitors where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the event visitors where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the event visitors that was removed
	 */
	public static EventVisitors removeByUUID_G(String uuid, long groupId)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of event visitorses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching event visitorses
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching event visitorses
	 */
	public static List<EventVisitors> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public static List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors[] findByUuid_C_PrevAndNext(
			long visitor_id, String uuid, long companyId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByUuid_C_PrevAndNext(
			visitor_id, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the event visitorses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of event visitorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching event visitorses
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the event visitorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching event visitorses
	 */
	public static List<EventVisitors> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public static List<EventVisitors> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the event visitorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByGroupId_First(
			long groupId, OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByGroupId_First(
		long groupId, OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByGroupId_Last(
			long groupId, OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByGroupId_Last(
		long groupId, OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where groupId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors[] findByGroupId_PrevAndNext(
			long visitor_id, long groupId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByGroupId_PrevAndNext(
			visitor_id, groupId, orderByComparator);
	}

	/**
	 * Removes all the event visitorses where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of event visitorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching event visitorses
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the event visitorses where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @return the matching event visitorses
	 */
	public static List<EventVisitors> findByEventVisitorId(
		long eventvisitorId) {

		return getPersistence().findByEventVisitorId(eventvisitorId);
	}

	/**
	 * Returns a range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public static List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end) {

		return getPersistence().findByEventVisitorId(
			eventvisitorId, start, end);
	}

	/**
	 * Returns an ordered range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().findByEventVisitorId(
			eventvisitorId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the event visitorses where eventvisitorId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByEventVisitorId(
		long eventvisitorId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByEventVisitorId(
			eventvisitorId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByEventVisitorId_First(
			long eventvisitorId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByEventVisitorId_First(
			eventvisitorId, orderByComparator);
	}

	/**
	 * Returns the first event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByEventVisitorId_First(
		long eventvisitorId,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByEventVisitorId_First(
			eventvisitorId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByEventVisitorId_Last(
			long eventvisitorId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByEventVisitorId_Last(
			eventvisitorId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByEventVisitorId_Last(
		long eventvisitorId,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByEventVisitorId_Last(
			eventvisitorId, orderByComparator);
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where eventvisitorId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param eventvisitorId the eventvisitor ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors[] findByEventVisitorId_PrevAndNext(
			long visitor_id, long eventvisitorId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByEventVisitorId_PrevAndNext(
			visitor_id, eventvisitorId, orderByComparator);
	}

	/**
	 * Removes all the event visitorses where eventvisitorId = &#63; from the database.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 */
	public static void removeByEventVisitorId(long eventvisitorId) {
		getPersistence().removeByEventVisitorId(eventvisitorId);
	}

	/**
	 * Returns the number of event visitorses where eventvisitorId = &#63;.
	 *
	 * @param eventvisitorId the eventvisitor ID
	 * @return the number of matching event visitorses
	 */
	public static int countByEventVisitorId(long eventvisitorId) {
		return getPersistence().countByEventVisitorId(eventvisitorId);
	}

	/**
	 * Returns all the event visitorses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching event visitorses
	 */
	public static List<EventVisitors> findByeventuserId(long userId) {
		return getPersistence().findByeventuserId(userId);
	}

	/**
	 * Returns a range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of matching event visitorses
	 */
	public static List<EventVisitors> findByeventuserId(
		long userId, int start, int end) {

		return getPersistence().findByeventuserId(userId, start, end);
	}

	/**
	 * Returns an ordered range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByeventuserId(
		long userId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().findByeventuserId(
			userId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the event visitorses where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching event visitorses
	 */
	public static List<EventVisitors> findByeventuserId(
		long userId, int start, int end,
		OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByeventuserId(
			userId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByeventuserId_First(
			long userId, OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByeventuserId_First(
			userId, orderByComparator);
	}

	/**
	 * Returns the first event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByeventuserId_First(
		long userId, OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByeventuserId_First(
			userId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors
	 * @throws NoSuchVisitorsException if a matching event visitors could not be found
	 */
	public static EventVisitors findByeventuserId_Last(
			long userId, OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByeventuserId_Last(
			userId, orderByComparator);
	}

	/**
	 * Returns the last event visitors in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching event visitors, or <code>null</code> if a matching event visitors could not be found
	 */
	public static EventVisitors fetchByeventuserId_Last(
		long userId, OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().fetchByeventuserId_Last(
			userId, orderByComparator);
	}

	/**
	 * Returns the event visitorses before and after the current event visitors in the ordered set where userId = &#63;.
	 *
	 * @param visitor_id the primary key of the current event visitors
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors[] findByeventuserId_PrevAndNext(
			long visitor_id, long userId,
			OrderByComparator<EventVisitors> orderByComparator)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByeventuserId_PrevAndNext(
			visitor_id, userId, orderByComparator);
	}

	/**
	 * Removes all the event visitorses where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public static void removeByeventuserId(long userId) {
		getPersistence().removeByeventuserId(userId);
	}

	/**
	 * Returns the number of event visitorses where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching event visitorses
	 */
	public static int countByeventuserId(long userId) {
		return getPersistence().countByeventuserId(userId);
	}

	/**
	 * Caches the event visitors in the entity cache if it is enabled.
	 *
	 * @param eventVisitors the event visitors
	 */
	public static void cacheResult(EventVisitors eventVisitors) {
		getPersistence().cacheResult(eventVisitors);
	}

	/**
	 * Caches the event visitorses in the entity cache if it is enabled.
	 *
	 * @param eventVisitorses the event visitorses
	 */
	public static void cacheResult(List<EventVisitors> eventVisitorses) {
		getPersistence().cacheResult(eventVisitorses);
	}

	/**
	 * Creates a new event visitors with the primary key. Does not add the event visitors to the database.
	 *
	 * @param visitor_id the primary key for the new event visitors
	 * @return the new event visitors
	 */
	public static EventVisitors create(long visitor_id) {
		return getPersistence().create(visitor_id);
	}

	/**
	 * Removes the event visitors with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors that was removed
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors remove(long visitor_id)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().remove(visitor_id);
	}

	public static EventVisitors updateImpl(EventVisitors eventVisitors) {
		return getPersistence().updateImpl(eventVisitors);
	}

	/**
	 * Returns the event visitors with the primary key or throws a <code>NoSuchVisitorsException</code> if it could not be found.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors
	 * @throws NoSuchVisitorsException if a event visitors with the primary key could not be found
	 */
	public static EventVisitors findByPrimaryKey(long visitor_id)
		throws events.exception.NoSuchVisitorsException {

		return getPersistence().findByPrimaryKey(visitor_id);
	}

	/**
	 * Returns the event visitors with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param visitor_id the primary key of the event visitors
	 * @return the event visitors, or <code>null</code> if a event visitors with the primary key could not be found
	 */
	public static EventVisitors fetchByPrimaryKey(long visitor_id) {
		return getPersistence().fetchByPrimaryKey(visitor_id);
	}

	/**
	 * Returns all the event visitorses.
	 *
	 * @return the event visitorses
	 */
	public static List<EventVisitors> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @return the range of event visitorses
	 */
	public static List<EventVisitors> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of event visitorses
	 */
	public static List<EventVisitors> findAll(
		int start, int end,
		OrderByComparator<EventVisitors> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the event visitorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>EventVisitorsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of event visitorses
	 * @param end the upper bound of the range of event visitorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of event visitorses
	 */
	public static List<EventVisitors> findAll(
		int start, int end, OrderByComparator<EventVisitors> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the event visitorses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of event visitorses.
	 *
	 * @return the number of event visitorses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static EventVisitorsPersistence getPersistence() {
		return _persistence;
	}

	private static volatile EventVisitorsPersistence _persistence;

}