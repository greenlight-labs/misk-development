/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.model.Event;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for Event. This utility wraps
 * <code>events.service.impl.EventLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EventLocalService
 * @generated
 */
public class EventLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>events.service.impl.EventLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static void addEntryGallery(
			Event orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryGallery(orgEntry, request);
	}

	public static void addEntrySchedule(
			Event orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntrySchedule(orgEntry, request);
	}

	/**
	 * Adds the event to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param event the event
	 * @return the event that was added
	 */
	public static Event addEvent(Event event) {
		return getService().addEvent(event);
	}

	public static Event addEvent(
			long userId, java.util.Date eventStartDateTime,
			java.util.Date eventEndDateTime, long tagId, long categoryId,
			int type, int paytype, Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> smallImageMap,
			Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate, Map<java.util.Locale, String> venueMap,
			Map<java.util.Locale, String> timingMap,
			Map<java.util.Locale, String> durationMap,
			Map<java.util.Locale, String> descriptionField1Map,
			Map<java.util.Locale, String> videoLinkMap,
			Map<java.util.Locale, String> descriptionField2Map,
			Map<java.util.Locale, String> subtitleMap,
			Map<java.util.Locale, String> descriptionField3Map,
			Map<java.util.Locale, String> detailImageMap,
			Map<java.util.Locale, String> quoteTextMap,
			Map<java.util.Locale, String> quoteAuthorMap,
			Map<java.util.Locale, String> descriptionField4Map,
			Map<java.util.Locale, String> eventdescription,
			Map<java.util.Locale, String> eventfeaturedImage,
			Map<java.util.Locale, String> buyTicketLink, String lat, String lan,
			String eventPrice, String email, String phone, String currency,
			long orderNo, boolean sendPushNotification,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addEvent(
			userId, eventStartDateTime, eventEndDateTime, tagId, categoryId,
			type, paytype, titleMap, smallImageMap, bigImageMap, displayDate,
			venueMap, timingMap, durationMap, descriptionField1Map,
			videoLinkMap, descriptionField2Map, subtitleMap,
			descriptionField3Map, detailImageMap, quoteTextMap, quoteAuthorMap,
			descriptionField4Map, eventdescription, eventfeaturedImage,
			buyTicketLink, lat, lan, eventPrice, email, phone, currency,
			orderNo, sendPushNotification, serviceContext);
	}

	public static int countByFilter(
		int payType, String priceFrom, String priceTo, long categoryId,
		String dateFrom, String dateTo) {

		return getService().countByFilter(
			payType, priceFrom, priceTo, categoryId, dateFrom, dateTo);
	}

	public static int countByTagId(long tagId) {
		return getService().countByTagId(tagId);
	}

	/**
	 * Creates a new event with the primary key. Does not add the event to the database.
	 *
	 * @param eventId the primary key for the new event
	 * @return the new event
	 */
	public static Event createEvent(long eventId) {
		return getService().createEvent(eventId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the event from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param event the event
	 * @return the event that was removed
	 */
	public static Event deleteEvent(Event event) {
		return getService().deleteEvent(event);
	}

	/**
	 * Deletes the event with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventId the primary key of the event
	 * @return the event that was removed
	 * @throws PortalException if a event with the primary key could not be found
	 */
	public static Event deleteEvent(long eventId) throws PortalException {
		return getService().deleteEvent(eventId);
	}

	public static Event deleteEvent(
			long eventId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteEvent(eventId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Event fetchEvent(long eventId) {
		return getService().fetchEvent(eventId);
	}

	/**
	 * Returns the event matching the UUID and group.
	 *
	 * @param uuid the event's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event, or <code>null</code> if a matching event could not be found
	 */
	public static Event fetchEventByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchEventByUuidAndGroupId(uuid, groupId);
	}

	public static List<Event> findByFilter(
		int payType, String priceFrom, String priceTo, long categoryId,
		String dateFrom, String dateTo, int offset, int limit) {

		return getService().findByFilter(
			payType, priceFrom, priceTo, categoryId, dateFrom, dateTo, offset,
			limit);
	}

	public static List<Event> findByTagId(long tagId) {
		return getService().findByTagId(tagId);
	}

	public static List<Event> findByTagId(long tagId, int start, int end) {
		return getService().findByTagId(tagId, start, end);
	}

	public static List<Event> findByTagId(
		long tagId, int start, int end, OrderByComparator<Event> obc) {

		return getService().findByTagId(tagId, start, end, obc);
	}

	public static List<Event> findByTitle(String title) {
		return getService().findByTitle(title);
	}

	public static List<Event> findByTitle(String title, int start, int end) {
		return getService().findByTitle(title, start, end);
	}

	public static List<Event> findByTitle(
		String title, int start, int end, OrderByComparator<Event> obc) {

		return getService().findByTitle(title, start, end, obc);
	}

	public static int findByTitleCount(String title) {
		return getService().findByTitleCount(title);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the event with the primary key.
	 *
	 * @param eventId the primary key of the event
	 * @return the event
	 * @throws PortalException if a event with the primary key could not be found
	 */
	public static Event getEvent(long eventId) throws PortalException {
		return getService().getEvent(eventId);
	}

	/**
	 * Returns the event matching the UUID and group.
	 *
	 * @param uuid the event's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event
	 * @throws PortalException if a matching event could not be found
	 */
	public static Event getEventByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getEventByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the events.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events
	 * @param end the upper bound of the range of events (not inclusive)
	 * @return the range of events
	 */
	public static List<Event> getEvents(int start, int end) {
		return getService().getEvents(start, end);
	}

	public static List<Event> getEvents(long groupId) {
		return getService().getEvents(groupId);
	}

	public static List<Event> getEvents(long groupId, int start, int end) {
		return getService().getEvents(groupId, start, end);
	}

	public static List<Event> getEvents(
		long groupId, int start, int end, OrderByComparator<Event> obc) {

		return getService().getEvents(groupId, start, end, obc);
	}

	public static List<Event> getEventsByCategory(long categoryId) {
		return getService().getEventsByCategory(categoryId);
	}

	public static List<Event> getEventsByCategory(
		long categoryId, int start, int end) {

		return getService().getEventsByCategory(categoryId, start, end);
	}

	public static List<Event> getEventsByCategory(
		long categoryId, int start, int end, OrderByComparator<Event> obc) {

		return getService().getEventsByCategory(categoryId, start, end, obc);
	}

	public static int getEventsByCategoryCount(long categoryId) {
		return getService().getEventsByCategoryCount(categoryId);
	}

	/**
	 * Returns all the events matching the UUID and company.
	 *
	 * @param uuid the UUID of the events
	 * @param companyId the primary key of the company
	 * @return the matching events, or an empty list if no matches were found
	 */
	public static List<Event> getEventsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getEventsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of events matching the UUID and company.
	 *
	 * @param uuid the UUID of the events
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of events
	 * @param end the upper bound of the range of events (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching events, or an empty list if no matches were found
	 */
	public static List<Event> getEventsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Event> orderByComparator) {

		return getService().getEventsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of events.
	 *
	 * @return the number of events
	 */
	public static int getEventsCount() {
		return getService().getEventsCount();
	}

	public static int getEventsCount(long groupId) {
		return getService().getEventsCount(groupId);
	}

	public static List<Event> getEventsIncludeExpiredEvents(
		long groupId, int start, int end) {

		return getService().getEventsIncludeExpiredEvents(groupId, start, end);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static List<events.model.EventsGallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getGalleries(actionRequest);
	}

	public static List<events.model.EventsGallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		List<events.model.EventsGallery> defaultGalleries) {

		return getService().getGalleries(actionRequest, defaultGalleries);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static List<events.model.Schedule> getSchedules(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getSchedules(actionRequest);
	}

	public static List<events.model.Schedule> getSchedules(
		javax.portlet.ActionRequest actionRequest,
		List<events.model.Schedule> defaultSchedules) {

		return getService().getSchedules(actionRequest, defaultSchedules);
	}

	public static List<Event> removeExpiredEvent(List<Event> eventList) {
		return getService().removeExpiredEvent(eventList);
	}

	public static List<Event> searchEvents(
		com.liferay.portal.kernel.search.SearchContext searchContext) {

		return getService().searchEvents(searchContext);
	}

	/**
	 * Updates the event in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param event the event
	 * @return the event that was updated
	 */
	public static Event updateEvent(Event event) {
		return getService().updateEvent(event);
	}

	public static Event updateEvent(
			long userId, long eventId, java.util.Date eventStartDateTime,
			java.util.Date eventEndDateTime, long tagId, long categoryId,
			int type, int paytype, Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> smallImageMap,
			Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate, Map<java.util.Locale, String> venueMap,
			Map<java.util.Locale, String> timingMap,
			Map<java.util.Locale, String> durationMap,
			Map<java.util.Locale, String> descriptionField1Map,
			Map<java.util.Locale, String> videoLinkMap,
			Map<java.util.Locale, String> descriptionField2Map,
			Map<java.util.Locale, String> subtitleMap,
			Map<java.util.Locale, String> descriptionField3Map,
			Map<java.util.Locale, String> detailImageMap,
			Map<java.util.Locale, String> quoteTextMap,
			Map<java.util.Locale, String> quoteAuthorMap,
			Map<java.util.Locale, String> descriptionField4Map,
			Map<java.util.Locale, String> eventdescription,
			Map<java.util.Locale, String> eventfeaturedImage,
			Map<java.util.Locale, String> buyTicketLink, String lat, String lan,
			String eventPrice, String email, String phone, String currency,
			long orderNo, boolean sendPushNotification,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateEvent(
			userId, eventId, eventStartDateTime, eventEndDateTime, tagId,
			categoryId, type, paytype, titleMap, smallImageMap, bigImageMap,
			displayDate, venueMap, timingMap, durationMap, descriptionField1Map,
			videoLinkMap, descriptionField2Map, subtitleMap,
			descriptionField3Map, detailImageMap, quoteTextMap, quoteAuthorMap,
			descriptionField4Map, eventdescription, eventfeaturedImage,
			buyTicketLink, lat, lan, eventPrice, email, phone, currency,
			orderNo, sendPushNotification, serviceContext);
	}

	public static void updateEventsGalleries(
			Event entry, List<events.model.EventsGallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateEventsGalleries(entry, galleries, serviceContext);
	}

	public static void updateSchedules(
			Event entry, List<events.model.Schedule> schedules,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateSchedules(entry, schedules, serviceContext);
	}

	public static EventLocalService getService() {
		return _service;
	}

	private static volatile EventLocalService _service;

}