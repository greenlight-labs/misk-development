/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package events.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import events.exception.*;

import events.model.Event;
import events.model.EventsGallery;
import events.model.Schedule;

import java.io.Serializable;

import java.util.*;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Event. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see EventLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface EventLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>events.service.impl.EventLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the event local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link EventLocalServiceUtil} if injection and service tracking are not available.
	 */
	public void addEntryGallery(Event orgEntry, PortletRequest request)
		throws PortletException;

	public void addEntrySchedule(Event orgEntry, PortletRequest request)
		throws PortletException;

	/**
	 * Adds the event to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param event the event
	 * @return the event that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Event addEvent(Event event);

	public Event addEvent(
			long userId, java.util.Date eventStartDateTime,
			java.util.Date eventEndDateTime, long tagId, long categoryId,
			int type, int paytype,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> smallImageMap,
			java.util.Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate,
			java.util.Map<java.util.Locale, String> venueMap,
			java.util.Map<java.util.Locale, String> timingMap,
			java.util.Map<java.util.Locale, String> durationMap,
			java.util.Map<java.util.Locale, String> descriptionField1Map,
			java.util.Map<java.util.Locale, String> videoLinkMap,
			java.util.Map<java.util.Locale, String> descriptionField2Map,
			java.util.Map<java.util.Locale, String> subtitleMap,
			java.util.Map<java.util.Locale, String> descriptionField3Map,
			java.util.Map<java.util.Locale, String> detailImageMap,
			java.util.Map<java.util.Locale, String> quoteTextMap,
			java.util.Map<java.util.Locale, String> quoteAuthorMap,
			java.util.Map<java.util.Locale, String> descriptionField4Map,
			java.util.Map<java.util.Locale, String> eventdescription,
			java.util.Map<java.util.Locale, String> eventfeaturedImage,
			java.util.Map<java.util.Locale, String> buyTicketLink, String lat,
			String lan, String eventPrice, String email, String phone,
			String currency, long orderNo, boolean sendPushNotification,
			ServiceContext serviceContext)
		throws PortalException;

	public int countByFilter(
		int payType, String priceFrom, String priceTo, long categoryId,
		String dateFrom, String dateTo);

	public int countByTagId(long tagId);

	/**
	 * Creates a new event with the primary key. Does not add the event to the database.
	 *
	 * @param eventId the primary key for the new event
	 * @return the new event
	 */
	@Transactional(enabled = false)
	public Event createEvent(long eventId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the event from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param event the event
	 * @return the event that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Event deleteEvent(Event event);

	/**
	 * Deletes the event with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param eventId the primary key of the event
	 * @return the event that was removed
	 * @throws PortalException if a event with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Event deleteEvent(long eventId) throws PortalException;

	public Event deleteEvent(long eventId, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Event fetchEvent(long eventId);

	/**
	 * Returns the event matching the UUID and group.
	 *
	 * @param uuid the event's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event, or <code>null</code> if a matching event could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Event fetchEventByUuidAndGroupId(String uuid, long groupId);

	public List<Event> findByFilter(
		int payType, String priceFrom, String priceTo, long categoryId,
		String dateFrom, String dateTo, int offset, int limit);

	public List<Event> findByTagId(long tagId);

	public List<Event> findByTagId(long tagId, int start, int end);

	public List<Event> findByTagId(
		long tagId, int start, int end, OrderByComparator<Event> obc);

	public List<Event> findByTitle(String title);

	public List<Event> findByTitle(String title, int start, int end);

	public List<Event> findByTitle(
		String title, int start, int end, OrderByComparator<Event> obc);

	public int findByTitleCount(String title);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the event with the primary key.
	 *
	 * @param eventId the primary key of the event
	 * @return the event
	 * @throws PortalException if a event with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Event getEvent(long eventId) throws PortalException;

	/**
	 * Returns the event matching the UUID and group.
	 *
	 * @param uuid the event's UUID
	 * @param groupId the primary key of the group
	 * @return the matching event
	 * @throws PortalException if a matching event could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Event getEventByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the events.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>events.model.impl.EventModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of events
	 * @param end the upper bound of the range of events (not inclusive)
	 * @return the range of events
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEvents(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEvents(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEvents(long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEvents(
		long groupId, int start, int end, OrderByComparator<Event> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByCategory(long categoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByCategory(long categoryId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByCategory(
		long categoryId, int start, int end, OrderByComparator<Event> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEventsByCategoryCount(long categoryId);

	/**
	 * Returns all the events matching the UUID and company.
	 *
	 * @param uuid the UUID of the events
	 * @param companyId the primary key of the company
	 * @return the matching events, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByUuidAndCompanyId(String uuid, long companyId);

	/**
	 * Returns a range of events matching the UUID and company.
	 *
	 * @param uuid the UUID of the events
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of events
	 * @param end the upper bound of the range of events (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching events, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Event> orderByComparator);

	/**
	 * Returns the number of events.
	 *
	 * @return the number of events
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEventsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getEventsCount(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> getEventsIncludeExpiredEvents(
		long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<EventsGallery> getGalleries(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<EventsGallery> getGalleries(
		ActionRequest actionRequest, List<EventsGallery> defaultGalleries);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Schedule> getSchedules(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Schedule> getSchedules(
		ActionRequest actionRequest, List<Schedule> defaultSchedules);

	public List<Event> removeExpiredEvent(List<Event> eventList);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Event> searchEvents(SearchContext searchContext);

	/**
	 * Updates the event in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect EventLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param event the event
	 * @return the event that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Event updateEvent(Event event);

	public Event updateEvent(
			long userId, long eventId, java.util.Date eventStartDateTime,
			java.util.Date eventEndDateTime, long tagId, long categoryId,
			int type, int paytype,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> smallImageMap,
			java.util.Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate,
			java.util.Map<java.util.Locale, String> venueMap,
			java.util.Map<java.util.Locale, String> timingMap,
			java.util.Map<java.util.Locale, String> durationMap,
			java.util.Map<java.util.Locale, String> descriptionField1Map,
			java.util.Map<java.util.Locale, String> videoLinkMap,
			java.util.Map<java.util.Locale, String> descriptionField2Map,
			java.util.Map<java.util.Locale, String> subtitleMap,
			java.util.Map<java.util.Locale, String> descriptionField3Map,
			java.util.Map<java.util.Locale, String> detailImageMap,
			java.util.Map<java.util.Locale, String> quoteTextMap,
			java.util.Map<java.util.Locale, String> quoteAuthorMap,
			java.util.Map<java.util.Locale, String> descriptionField4Map,
			java.util.Map<java.util.Locale, String> eventdescription,
			java.util.Map<java.util.Locale, String> eventfeaturedImage,
			java.util.Map<java.util.Locale, String> buyTicketLink, String lat,
			String lan, String eventPrice, String email, String phone,
			String currency, long orderNo, boolean sendPushNotification,
			ServiceContext serviceContext)
		throws PortalException, SystemException;

	public void updateEventsGalleries(
			Event entry, List<EventsGallery> galleries,
			ServiceContext serviceContext)
		throws PortalException;

	public void updateSchedules(
			Event entry, List<Schedule> schedules,
			ServiceContext serviceContext)
		throws PortalException;

}