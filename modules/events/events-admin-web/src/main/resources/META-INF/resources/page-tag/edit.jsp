<%@include file = "../init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    PageTag entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = PageTagLocalServiceUtil.getPageTag(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    // get all tags
    List<Tag> tags = TagLocalServiceUtil.findAllInGroup(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= entry %>" model="<%= PageTag.class %>" />

        <aui:input type="hidden" name="resourcePrimKey"
                   value='<%= entry == null ? "" : entry.getPageId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:input name="pageName" label="Page Name" autoSize="true" helpMessage="Max 50 Characters (Recommended)">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="pageSlug" label="Page Slug" autoSize="true" helpMessage="Leave empty for auto generate" />
                <aui:select name="tagId" label="Tag">
                    <aui:option value="0" label="All" />
                    <% for (Tag tag : tags) {%>
                        <aui:option value="<%= tag.getTagId() %>" label="<%= tag.getName() %>" />
                    <% }%>
                </aui:select>
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
