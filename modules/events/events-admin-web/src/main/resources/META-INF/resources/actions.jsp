<%@include file="/init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Event event = (Event) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="eventId"
                       value="<%=String.valueOf(event.getEventId()) %>" />
        <portlet:param name="mvcPath"
                       value="/edit.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteEvent" var="deleteURL">
        <portlet:param name="eventId"
                       value="<%= String.valueOf(event.getEventId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>