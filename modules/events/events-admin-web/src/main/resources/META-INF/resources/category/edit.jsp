<%@include file = "/init.jsp" %>

<%
    long categoryId = ParamUtil.getLong(request, "categoryId");

    Category category = null;

    if (categoryId > 0) {
        try {
            category = CategoryLocalServiceUtil.getCategory(categoryId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= category == null ? "addCategory" : "updateCategory" %>' var="editCategoryURL" />

<div class="container-fluid-1280">

    <aui:form action="<%= editCategoryURL %>" name="fm">

        <aui:model-context bean="<%= category %>" model="<%= Category.class %>" />

        <aui:input type="hidden" name="categoryId"
                   value='<%= category == null ? "" : category.getCategoryId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:input name="name" label="Name" autoSize="true" helpMessage="Max 15 Characters (Recommended)">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="catColor" label="Category Color" autoSize="true" helpMessage="#ccccc">
                    <aui:validator name="required"/>
                </aui:input>
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>