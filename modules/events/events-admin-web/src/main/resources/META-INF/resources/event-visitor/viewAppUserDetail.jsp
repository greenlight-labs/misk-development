<%@page import="events.admin.web.model.EventVisitorAppUserDTO"%>
<%@page import="events.model.Event"%>
<%@page import="java.util.List"%>
<%@page import="misk.app.users.model.AppUser"%>
<%@ include file="/init.jsp"%>
<%
	List<EventVisitorAppUserDTO> appUserList = (List) request.getAttribute("appUserList");

	Event event = (Event) request.getAttribute("event");
%>


<div class="container-fluid w-90">
<div class="eventDetails">
	<div class="row">
		<div class="col-4">
			<b>Event Start Date : </b>
		</div>
		<div class="col-8"><%=dateFormat.format(event.getEventStartDateTime())%></div>
	</div>
	<div class="row stripColor">
		<div class="col-4">
			<b>Event End Date : </b>
		</div>
		<div class="col-8"><%=dateFormat.format(event.getEventEndDateTime())%></div>
	</div>
</div>
<table class="table table-striped">
	<thead>
		<th>S.No</th>
		<th>App User Id</th>
		<th>Full Name</th>
		<th>Email Address</th>
		<th>Phone Number</th>
		<th>Event Registration Date</th>
	</thead>
	<tbody>
		<%
			int count = 1;
			for (EventVisitorAppUserDTO obj : appUserList) {
		%>
		<tr>
			<td><%=count++%></td>
			<td><%=obj.getAppUser().getAppUserId()%></td>
			<td><%=obj.getAppUser().getFullName()%></td>
			<td><%=obj.getAppUser().getEmailAddress()%></td>
			<td><%=obj.getAppUser().getPhoneNumber()%></td>
			<td><%=dateFormat.format(obj.getEventVisitors().getCreateDate())%></td>
		</tr>
		<%
			}
		%>
	</tbody>
</table>
</div>

<style>

.eventDetails{
margin-bottom:10px;
margin-top:5px;
background-color:#E7E9EB;
}
.stripColor{
border:2px solid #FFFFFF;
}
</style>

