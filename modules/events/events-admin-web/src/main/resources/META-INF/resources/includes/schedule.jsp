<%@ page import="events.model.Schedule" %>
<%@ page import="events.service.ScheduleLocalServiceUtil" %>
<%@ page import="events.model.impl.ScheduleImpl" %><%
    /* *****************************************************/
    List<Schedule> scheduleSlides = Collections.emptyList();
    int[] scheduleIndexes = null;

    String scheduleIndexesParam = ParamUtil.getString(request, "scheduleIndexes");
    if (Validator.isNotNull(scheduleIndexesParam)) {
        scheduleSlides = new ArrayList<Schedule>();

        scheduleIndexes = StringUtil.split(scheduleIndexesParam, 0);

        for (int scheduleIndex : scheduleIndexes) {
            scheduleSlides.add(new ScheduleImpl());
        }
    } else {
        if (event != null) {
            scheduleSlides = ScheduleLocalServiceUtil.findAllInEvent(event.getEventId());

            scheduleIndexes = new int[scheduleSlides.size()];

            for (int i = 0; i < scheduleSlides.size(); i++) {
                scheduleIndexes[i] = i;
            }
        }

        if (scheduleSlides.isEmpty()) {
            scheduleSlides = new ArrayList<Schedule>();

            scheduleSlides.add(new ScheduleImpl());

            scheduleIndexes = new int[] {0};
        }

        if (scheduleIndexes == null) {
            scheduleIndexes = new int[0];
        }
    }
    /* *****************************************************/
%>

<aui:fieldset-group markupView="lexicon">
    <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Companion App - Schedule Section" id='<%= renderResponse.getNamespace() + "scheduleSlides" %>'>
        <%
            for (int i = 0; i < scheduleIndexes.length; i++) {
                int scheduleSlideIndex = scheduleIndexes[i];
                Schedule scheduleSlide = scheduleSlides.get(i);
        %>
        <aui:model-context bean="<%= scheduleSlide %>" model="<%= Schedule.class %>"/>
        <div class="field-row lfr-form-row lfr-form-row-inline">
            <div class="row-fields">
                <aui:field-wrapper>
                    <div style="color:#bc1a1ab3" name='<%= "slide" + scheduleSlideIndex %>' class="slide-heading"><h3>Slide <%= scheduleSlideIndex + 1 %></h3></div>
                    <aui:input name='<%= "scheduleId" + scheduleSlideIndex %>' type="hidden" value="<%= scheduleSlide.getScheduleId() %>" />
                    <aui:input label="Title" fieldParam='<%= "scheduleTitle" + scheduleSlideIndex %>' id='<%= "scheduleTitle" + scheduleSlideIndex %>' name="title" helpMessage="Max 30 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                    <aui:input label="Description" fieldParam='<%= "scheduleDescription" + scheduleSlideIndex %>' id='<%= "scheduleDescription" + scheduleSlideIndex %>' name="description" helpMessage="Max 170 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                    <aui:input label="Duration" fieldParam='<%= "scheduleDuration" + scheduleSlideIndex %>' id='<%= "scheduleDuration" + scheduleSlideIndex %>' name="duration" helpMessage="Max 15 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                    <aui:input label="Image" fieldParam='<%= "scheduleImage" + scheduleSlideIndex %>' id='<%= "scheduleImage" + scheduleSlideIndex %>' name="image" helpMessage="Image Dimensions: 1229 x 931 pixels">
                        <aui:validator name="required"/>
                        <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                            function(val) {
                                var ext = val.substring(val.lastIndexOf('.') + 1);
                                return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                            }
                        </aui:validator>
                    </aui:input>
                    <div class="button-holder">
                        <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "scheduleImage" + scheduleSlideIndex %>">
                            <span class="lfr-btn-label">Select</span>
                        </button>
                    </div>
                </aui:field-wrapper>
            </div>
        </div>
        <%
            }
        %>
        <aui:input name="scheduleIndexes" type="hidden" value="<%= StringUtil.merge(scheduleIndexes) %>"/>
    </aui:fieldset>
</aui:fieldset-group>

<aui:script use="liferay-auto-fields">
    new Liferay.AutoFields({
    contentBox: '#<portlet:namespace />scheduleSlides',
    fieldIndexes: '<portlet:namespace />scheduleIndexes',
    namespace: '<portlet:namespace />',
    sortable: true,
    sortableHandle: '.lfr-form-row'
    }).render();
</aui:script>
<aui:script use="liferay-item-selector-dialog">
    /*Repeater fields image selection*/
    var scheduleSlidesContainer = $('#<portlet:namespace />scheduleSlides');
    scheduleSlidesContainer.on('click','.select-button-repeater',function(event){
        event.preventDefault();
		var id=this.name.match(/(\d+)/)[0];
        var element = event.currentTarget;
        var fieldId = element.name;
		var itemSelectorDialog = new A.LiferayItemSelectorDialog
		(
			{
				eventName: 'selectDocumentLibrary',
				on: {
						selectedItemChange: function(event) {
							var selectedItem = event.newVal;
							if(selectedItem)
							{
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
						   }
						}
				},
				title: '<liferay-ui:message key="Select File" />',
				url: '${itemSelectorURL }'
			}
		);
		itemSelectorDialog.open();

	});
</aui:script>
<script type="text/javascript">
    var scheduleContainer = $('#<portlet:namespace />scheduleSlides');
    /*change slide number on add/delete row*/
    scheduleContainer.on('click', '.add-row', function(event){
        event.preventDefault();
        var panelBody = $(this).parents('div.panel-body');
        var fieldRows = panelBody.find('div.field-row');
        fieldRows.each(function(index, item) {
            var slideHeading = $(item).find('div.slide-heading');
            var id = index+1;
            slideHeading.find('h3').text('Slide '+id);
        });
    });
</script>