<%@include file="../init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    PageTag entry = (PageTag) row.getObject();
    String primKey = String.valueOf(entry.getPrimaryKey());
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="resourcePrimKey" value="<%= primKey %>" />
        <portlet:param name="mvcPath" value="/page-tag/edit.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit" url="<%=editURL.toString() %>" />

</liferay-ui:icon-menu>