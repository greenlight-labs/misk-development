<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="java.util.Map"%>
<%@page import="events.admin.web.model.EventVisitorsDTO"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="events.service.EventLocalServiceUtil"%>
<%@page import="events.model.EventVisitors"%>
<%@page import="events.service.EventVisitorsLocalServiceUtil"%>
<%@ include file="/init.jsp"%>

<portlet:renderURL var="exportEventVisitorsURL"
				windowState="<%=LiferayWindowState.POP_UP.toString()%>">
				<portlet:param name="mvcRenderCommandName"
					value="exportEventVisitors" />			
			</portlet:renderURL>
			
<liferay-portlet:resourceURL id="exportEventVisitors" var="exportURL">	
<liferay-portlet:param name="data" value="<%= "LIST_OF_EVENT_VISTOR" %>" />
</liferay-portlet:resourceURL>
<%
	HashMap<String, EventVisitorsDTO> map = (HashMap) request.getAttribute("eventVisitor");
%>

<div class="container-fluid w-75">


	<div class="row pt-md-3 pb-md-3">
		<div class="col">
			<h1>Event Registration Records (Free Events)</h1>
		</div>
		<div class="col text-right">
			<%-- <aui:button onClick="<%=exportURL.toString() %>" value="Export Event Visitors"> --%>
			
			<aui:button onclick="javascript:getListSelectedEventVisitor();" value="Export Event Visitors">
			
			</aui:button>
		</div>
	</div>

	<table class="table table-striped">
		<thead>
			<th>Select</th>
			<th>S.No</th>
			<th>Event Name</th>
			<th>Visitor Count</th>
		</thead>
		<tbody>
			<%
				int count = 1;
				for (Map.Entry<String, EventVisitorsDTO> entry : map.entrySet()) {
			%>
			<portlet:renderURL var="viewEventVisitorAppUserURL"
				windowState="<%=LiferayWindowState.POP_UP.toString()%>">
				<portlet:param name="mvcRenderCommandName"
					value="viewEventVisitorAppUser" />
				<portlet:param name="eventId"
					value="<%=String.valueOf(entry.getKey())%>" />
			</portlet:renderURL>
			<tr>

				<td>
				 <input type="checkbox" id="selectEventVisitorCheckBox" class="selectEventVisitorCheckBox" name="selectEventVisitorCheckBox" value="<%= entry.getKey() %>">		 </td>
				<td><a href="javascript:void(0)"
					onclick="showPopup('<%=viewEventVisitorAppUserURL.toString()%>','<%=entry.getValue().getEventName()%>')";"><%=count++%></a></td>
				<td><a href="javascript:void(0)"
					onclick="showPopup('<%=viewEventVisitorAppUserURL.toString()%>','<%=entry.getValue().getEventName()%>')";"><%=entry.getValue().getEventName()%></a></td>
				<td><a href="javascript:void(0)"
					onclick="showPopup('<%=viewEventVisitorAppUserURL.toString()%>','<%=entry.getValue().getEventName()%>')";"><%=entry.getValue().getVistorCount()%></a></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>

</div>


<aui:script>

 function showPopup(url, eventName)
{
console.log("App User id --> "+ url);
    Liferay.Util.openWindow(
            {
                dialog: {
                    cache: false,
                    width:850,
                    height:600,
                    centered: true,                 
                    resizable: true,                   
                    modal: true,
                    destroyOnClose: true,
                },
                id: 'EventVisitorUserList',
                 title: eventName,            
                uri: url
            }
    );    
}

function getListSelectedEventVisitor()
{
	
var selected = [];
$('.selectEventVisitorCheckBox:checkbox:checked').each(function() {
    selected.push($(this).attr('value'));
});
var Export_URL='<%= exportURL + "&compress=0&etag=0&strip=0" %>';
console.log("selected.toString()"+selected.toString())
Export_URL=Export_URL.replace('LIST_OF_EVENT_VISTOR',selected.toString());
console.log("Export_URL "+Export_URL);
submitForm(document.hrefFm, Export_URL);
}

</aui:script>
