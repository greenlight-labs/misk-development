<%@page import="com.liferay.portal.kernel.portlet.PortalPreferences"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="java.util.Collections"%>
<%@page import="org.apache.commons.beanutils.BeanComparator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="events.model.Event"%>
<%@page import="java.util.List"%>
<%@page import="events.service.EventLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ include file="/init.jsp" %>

<%
 
 PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);
 String orderByCol = ParamUtil.getString(request, "orderByCol");
 String orderByType = ParamUtil.getString(request, "orderByType");

 if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
 	portalPrefs.setValue("NAME_SPACE", "order-by-col", orderByCol);
 	portalPrefs.setValue("NAME_SPACE", "order-by-type", orderByType);

 } else {
 	orderByCol = portalPrefs.getValue("NAME_SPACE", "order-by-col", "orderNo");
 	orderByType = portalPrefs.getValue("NAME_SPACE", "order-by-type", "asc");
 }
 if(Validator.isNull(orderByCol)){
		orderByCol="orderNo";
	}
	if(Validator.isNull(orderByType)){
		orderByType="asc";
	}
 String sortingOrder = orderByType;
 
 %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="events-admin-buttons">
        <portlet:renderURL var="addEventURL">
            <portlet:param name="mvcPath"
                           value="/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEventURL.toString() %>"
                    value="Add Event"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= EventLocalServiceUtil.getEventsCount(scopeGroupId) %>" orderByType="<%=orderByType %>" orderByCol="<%=orderByCol %>">
    <liferay-ui:search-container-results>
    <%
    
    List<Event> eventList = EventLocalServiceUtil.getEventsIncludeExpiredEvents(scopeGroupId,-1, -1);
    
    List<Event> eventListPerPage = ListUtil.subList(eventList, searchContainer.getStart(),searchContainer.getEnd());
    int totalRecords =  eventList.size();
    
    List<Event> sortableEventList = new ArrayList<Event>(eventListPerPage);
    if(Validator.isNotNull(orderByCol)){
        //Pass the column name to BeanComparator to get comparator object
        BeanComparator comparator = new BeanComparator(orderByCol);
        //It will sort in ascending order
       	 Collections.sort(sortableEventList, comparator); 
        if(sortingOrder.equalsIgnoreCase("asc")){
        
        
        }else{
            //It will sort in descending order
            Collections.reverse(sortableEventList);
        }
 
    }
    pageContext.setAttribute("beginIndex",searchContainer.getStart());
	pageContext.setAttribute("endIndex",searchContainer.getEnd());
    pageContext.setAttribute("results", sortableEventList);
    pageContext.setAttribute("total", totalRecords);

    %>
    
    
    </liferay-ui:search-container-results>
  <%--       <liferay-ui:search-container-results
                results="<%= EventLocalServiceUtil.getEventsIncludeExpiredEvents(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/> --%>

        <liferay-ui:search-container-row
                className="events.model.Event" modelVar="event">
            <%
                // get category name from category id
                long categoryId = event.getCategoryId();
                String categoryName = "";
                if (categoryId > 0) {
                    Category category = CategoryLocalServiceUtil.getCategory(categoryId);
                    categoryName = category.getName(locale);
                }
                // get tag name from tag id
                long tagId = event.getTagId();
                String tagName = "";
                if (tagId > 0) {
                    Tag tag = TagLocalServiceUtil.getTag(tagId);
                    tagName = tag.getName(locale);
                }
            %>

            <liferay-ui:search-container-column-text name="title" value="<%= HtmlUtil.escape(event.getTitle(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Start Date" value="<%= dateFormat.format(event.getEventStartDateTime()) %>"/>
            <liferay-ui:search-container-column-text name="End Date" value="<%= dateFormat.format(event.getEventEndDateTime()) %>"/>
            <liferay-ui:search-container-column-text name="Category" value="<%= HtmlUtil.escape(categoryName) %>"/>
            <liferay-ui:search-container-column-text name="Tag" value="<%= HtmlUtil.escape(tagName) %>"/>
			<liferay-ui:search-container-column-text name="Order No" value="<%= String.valueOf(event.getOrderNo()) %>" orderable="<%= true %>" orderableProperty="orderNo" />
            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>