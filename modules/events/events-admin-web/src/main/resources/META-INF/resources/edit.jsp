<%@include file = "/init.jsp" %>

<%
    long eventId = ParamUtil.getLong(request, "eventId");

    Event event = null;

    if (eventId > 0) {
        try {
            event = EventLocalServiceUtil.getEvent(eventId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }

    List<Category> categories = CategoryServiceUtil.getCategories(scopeGroupId);
    List<Tag> tags = TagLocalServiceUtil.findAllInGroup(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= event == null ? "addEvent" : "updateEvent" %>' var="editEventURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editEventURL %>" name="fm" onSubmit="return false">

    <aui:model-context bean="<%= event %>" model="<%= Event.class %>" />

    <aui:input type="hidden" name="eventId"
               value='<%= event == null ? "" : event.getEventId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset collapsed="false" collapsible="true" label="Add to Calendar Fields">
            <aui:input name="eventStartDateTime" label="Event Start DateTime" helpMessage="Event Start Date Time should be less than OR equal to Event End Date Time field">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="eventEndDateTime" label="Event End DateTime" helpMessage="These times will be validated for events, so the validity or expiry of event should be based on these fields">
                <aui:validator name="required"/>
            </aui:input>
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset collapsed="false" collapsible="true" label="Website & Companion App - Fields">
            <aui:select required="true" label="Category" name="categoryId">
                <% for (Category curCategory : categories) { %>
                <aui:option label="<%= curCategory.getName(locale) %>" value="<%= curCategory.getCategoryId() %>" />
                <% } %>
            </aui:select>
            <aui:input name="title" helpMessage="Max 65 Characters (Recommended)" autoFocus="true">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="venue" label="Venue" helpMessage="Max 25 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="timing" label="Display Time" helpMessage="Max 15 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="duration" helpMessage="Max 35 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="orderNo" label="Order Number" helpMessage="Order Number default value 9999" placeholder="9999" />
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset collapsed="false" collapsible="true" label="Website - Fields">
            <aui:select label="type" name="type">
                <aui:option label="default" value="1" />
                <aui:option label="latest-events" value="2" />
                <aui:option label="wadi-c" value="3" />
            </aui:select>
            <aui:input name="displayDate">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="smallImage" label="Small Image" helpMessage="Image Dimensions: 460 x 560 pixels">
                <aui:validator name="required">
                    function() {
                    return AUI.$('#<portlet:namespace />type').val() === "1";
                    }
                </aui:validator>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
                <aui:validator name="custom" errorMessage="File does not exist">
                    function(fileUrl) {
                        var fileExist = false;
                        AUI.$.ajax({
                            url: fileUrl,
                            async: false,
                            success: function (data) {
                                fileExist = data;
                            }
                        });
                        return fileExist;
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="smallImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="bigImage" label="Big Image" helpMessage="Image Dimensions: 1390 x 828 pixels">
                <aui:validator name="required">
                    function() {
                    return AUI.$('#<portlet:namespace />type').val() === "2";
                    }
                </aui:validator>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="bigImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="descriptionField1" label="Description field 1" helpMessage="Max 470 Characters (Recommended)" />
            <aui:input name="videoLink" label="Video Link" helpMessage="Youtube or Vimeo link required &#013;&#010; https://www.youtube.com/embed/BXtNXIKaolk &#013;&#010; https://player.vimeo.com/video/757873622" />
            <aui:input name="descriptionField2" label="Description field 2" helpMessage="Max 1050 Characters (Recommended)" />
            <aui:input name="subtitle" helpMessage="Max 70 Characters (Recommended)" />
            <aui:input name="descriptionField3" label="Description field 3" helpMessage="Max 315 Characters (Recommended)" />
            <aui:input name="detailImage" label="Detail Image" helpMessage="Image Dimensions: 1103 x 426 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="detailImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="quoteText" label="Quote Text" helpMessage="Max 150 Characters (Recommended)" />
            <aui:input name="quoteAuthor" label="Quote Author" helpMessage="Max 15 Characters (Recommended)" />
            <aui:input name="descriptionField4" label="Description field 4" helpMessage="Max 1050 Characters (Recommended)" />
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset collapsed="true" collapsible="true" label="Companion App - Fields">
            <aui:select required="true" label="Tag" name="tagId">
                <% for (Tag curTag : tags) { %>
                <aui:option label="<%= curTag.getName() %>" value="<%= curTag.getTagId() %>" />
                <% } %>
            </aui:select>
            <aui:select label="paytype" name="paytype">
                <aui:option label="free" value="1" />
                <aui:option label="paid" value="2" />
            </aui:select>
            <aui:input name="lat" label="Lat" helpMessage="Latitude (Recommended)" />
            <aui:input name="lan" label="Lan" helpMessage="Longitude (Recommended)" />
            <aui:input name="email" label="Email" helpMessage="Email (Recommended)" />
            <aui:input name="phone" label="Phone" helpMessage="Phone (Recommended)" />
            <div class="row">
                <div class="col-md-6">
                    <aui:input name="eventPrice" label="Event Price" helpMessage="Event Price (Recommended)" />
                </div>
                <div class="col-md-6">
                    <aui:select label="currency" name="currency">
                        <aui:option label="SAR" value="SAR" />
                        <aui:option label="AED" value="AED" />
                    </aui:select>
                </div>
            </div>
            <aui:input name="buyTicketLink" label="Buy Ticket Link" />
            <aui:input name="eventfeaturedImage" label="Event Featured Image" helpMessage="Featured Image">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="eventfeaturedImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="eventdescription" label="Event Description" helpMessage="Max 1050 Characters (Recommended)" />
        </aui:fieldset>
    </aui:fieldset-group>

    <%@include file="/includes/gallery.jsp" %>

    <%@include file="/includes/schedule.jsp" %>

    <aui:button-row>
        <%--send push notification checkbox--%>
        <aui:input name="sendPushNotification" type="checkbox" label="Send Push Notification" />
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
    // show/hide conditional fields
    var type = window.document.querySelector('[name=<portlet:namespace />type]');
    var type_value = type.value;
    function toggleConditionalFields(type_value){
        var smallImage = $('[name="<portlet:namespace />smallImage"]').parents('.form-group');
        var smallImageButton = smallImage.next();
        var bigImage = $('[name="<portlet:namespace />bigImage"]').parents('.form-group');
        var bigImageButton = bigImage.next();
        if(type_value === 1 || type_value === "1"){
            smallImage.show();
            smallImageButton.show();
            bigImage.hide();
            bigImageButton.hide();
        }else if(type_value === 2 || type_value === "2" ){
            /*smallImage.hide();
            smallImageButton.hide();*/
            bigImage.show();
            bigImageButton.show();
        }
    }
    toggleConditionalFields(type_value);
    type.addEventListener('change', function (event) {
        event.preventDefault();
        var type = window.document.querySelector('[name=<portlet:namespace />type]');
        var type_value = type.value;
        toggleConditionalFields(type_value);
    });
    // add event listener in form submit
    var form = window.document.querySelector('#<portlet:namespace />fm');
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        var eventStartDate = window.document.querySelector('[name=<portlet:namespace />eventStartDateTime]');
        var eventStartTime = window.document.querySelector('[name=<portlet:namespace />eventStartDateTimeTime]');
        var eventEndDate = window.document.querySelector('[name=<portlet:namespace />eventEndDateTime]');
        var eventEndTime = window.document.querySelector('[name=<portlet:namespace />eventEndDateTimeTime]');
        var eventStartDate_value = eventStartDate.value;
        var eventStartTime_value = eventStartTime.value;
        var eventEndDate_value = eventEndDate.value;
        var eventEndTime_value = eventEndTime.value;
        var eventStartDateTime = eventStartDate_value + " " + eventStartTime_value;
        var eventEndDateTime = eventEndDate_value + " " + eventEndTime_value;
        var eventStartDateTime_date = new Date(eventStartDateTime);
        var eventEndDateTime_date = new Date(eventEndDateTime);

        if(eventStartDateTime_date > eventEndDateTime_date){
            // add error message in aui field
            //eventStartDateTime.setAttribute('error', 'Event Start Date Time should be less than Event End Date Time');
            alert('Event Start Date Time should be less than Event End Date Time');
            return false;
        }else{
            // submit only if form do not have any error
            setTimeout(function(){
                if (!form.querySelector('.has-error')) {
                    form.submit();
                }
            }, 1000);
            return false;
        }
    });
    // add readonly attribute to all image fields on page load
    window.onload = function() {
        var smallImage = $('[name="<portlet:namespace />smallImage"]');
        var bigImage = $('[name="<portlet:namespace />bigImage"]');
        var detailImage = $('[name="<portlet:namespace />detailImage"]');
        var eventfeaturedImage = $('[name="<portlet:namespace />eventfeaturedImage"]');
        smallImage.attr('readonly', true);
        bigImage.attr('readonly', true);
        detailImage.attr('readonly', true);
        eventfeaturedImage.attr('readonly', true);
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>