<%@ include file="../init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="tags-admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/tag/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= TagLocalServiceUtil.countAllInGroup(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= TagLocalServiceUtil.findAllInGroup(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="events.model.Tag" modelVar="tag">

            <liferay-ui:search-container-column-text
                    name="name"
                    value="<%= HtmlUtil.escape(tag.getName(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/tag/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>