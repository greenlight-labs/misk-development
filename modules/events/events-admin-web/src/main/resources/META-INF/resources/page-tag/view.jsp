<%@ include file="../init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath" value="/page-tag/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>
        <%--add entry button--%>
        <aui:button onClick="<%= addEntryURL.toString() %>" value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= PageTagLocalServiceUtil.countByGroupId(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= PageTagLocalServiceUtil.findByGroupId(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row className="events.model.PageTag" modelVar="entry">
            <%
                // check tagId and get tag name
                long tagId = entry.getTagId();
                String tagName = "";
                if (tagId > 0) {
                    Tag tag = TagLocalServiceUtil.fetchTag(tagId);
                    if(tag != null) {
                        tagName = tag.getName(locale);
                    }
                }else if (tagId == 0) {
                    tagName = "All";
                }
            %>

            <liferay-ui:search-container-column-text name="Page" value="<%= HtmlUtil.escape(entry.getPageName()) %>"/>
            <liferay-ui:search-container-column-text name="Page Slug" value="<%= HtmlUtil.escape(entry.getPageSlug()) %>"/>
            <liferay-ui:search-container-column-text name="Tag" value="<%= HtmlUtil.escape(tagName) %>"/>

            <liferay-ui:search-container-column-jsp align="center" path="/page-tag/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>