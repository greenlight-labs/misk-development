package events.admin.web.model;

public class EventVisitorsDTO {

	public String eventName;
	public String eventVisitorId;
	public int vistorCount;

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public int getVistorCount() {
		return vistorCount;
	}

	public void setVistorCount(int vistorCount) {
		this.vistorCount = vistorCount;
	}

	public String getEventVisitorId() {
		return eventVisitorId;
	}

	public void setEventVisitorId(String eventVisitorId) {
		this.eventVisitorId = eventVisitorId;
	}

}
