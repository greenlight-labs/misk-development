package events.admin.web.portlet.action;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CSVUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.text.Format;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import events.admin.web.constants.EventsAdminWebPortletKeys;
import events.model.Event;
import events.model.EventVisitors;
import events.service.EventLocalServiceUtil;
import events.service.EventVisitorsLocalServiceUtil;
import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalServiceUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + EventsAdminWebPortletKeys.EVENTVISITORADMINWEB,
		"mvc.command.name=" + EventsAdminWebPortletKeys.EXPORT_EVENT_VISITOR }, service = MVCResourceCommand.class)
public class ExportEventVisitorsMVCResourceCommand extends BaseMVCResourceCommand {

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception {

		try {
			SessionMessages.add(resourceRequest,
					_portal.getPortletId(resourceRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

			String selected = ParamUtil.getString(resourceRequest, "data");
			_log.info(selected);

			String csv = getEventVisitorListCSV(resourceRequest, resourceResponse, selected);

			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, "event_visitors.csv", csv.getBytes(),
					ContentTypes.TEXT_CSV_UTF8);
		} catch (Exception exception) {
			SessionErrors.add(resourceRequest, exception.getClass());

			_log.error(exception, exception);
		}
	}

	public String getEventVisitorListCSV(ResourceRequest resourceRequest, ResourceResponse resourceResponse,
			String selectedEvents) {
		StringBuffer sb = new StringBuffer();
		int count = 1;
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM dd, yyyy", themeDisplay.getLocale(),
				themeDisplay.getTimeZone());
		List<EventVisitors> eventVisitorsList = EventVisitorsLocalServiceUtil.getEventVisitorses(-1, -1);
		sb = generateCSVHeaders(sb);
		if (Validator.isNotNull(selectedEvents)) {
			for (EventVisitors eventVisitor : eventVisitorsList) {
				try {
					Event event = getEventName(eventVisitor.getEventvisitorId(), themeDisplay);
					if (Validator.isNotNull(event) && selectedEvents.contains(String.valueOf(event.getEventId()))) {
						sb.append(CSVUtil.encode(count++));
						sb.append(StringPool.COMMA);
						sb.append(CSVUtil.encode(event.getTitle(themeDisplay.getLocale())));
						sb.append(StringPool.COMMA);
						sb.append(CSVUtil.encode(dateFormat.format(event.getEventStartDateTime())));
						sb.append(StringPool.COMMA);
						sb.append(CSVUtil.encode(dateFormat.format(event.getEventEndDateTime())));
						sb.append(StringPool.COMMA);
						AppUser appUserObj = getAppUserName(eventVisitor.getUserId(), themeDisplay);
						if (Validator.isNotNull(appUserObj)) {
							sb.append(CSVUtil.encode(appUserObj.getFullName()));
							sb.append(StringPool.COMMA);
							sb.append(CSVUtil.encode(appUserObj.getEmailAddress()));
							sb.append(StringPool.COMMA);
							sb.append(CSVUtil.encode(appUserObj.getPhoneNumber()));
							sb.append(StringPool.COMMA);
							sb.append(CSVUtil.encode(dateFormat.format(eventVisitor.getCreateDate())));
							sb.append(StringPool.COMMA);
						} else {
							sb.append(StringPool.COMMA);
							sb.append(StringPool.COMMA);
							sb.append(StringPool.COMMA);
							sb.append(StringPool.COMMA);
						}
						sb.append(StringPool.NEW_LINE);
					}

				} catch (Exception e) {
				}
			}
		}

		return sb.toString();

	}

	public StringBuffer generateCSVHeaders(StringBuffer sb) {
		sb.append(CSVUtil.encode("S.NO"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Event Name"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Event Start Date"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Event End Date"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Full Name"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Email Address"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Phone Number"));
		sb.append(StringPool.COMMA);
		sb.append(CSVUtil.encode("Event Registration Date"));
		sb.append(StringPool.COMMA);
		sb.append(StringPool.NEW_LINE);
		return sb;

	}

	public AppUser getAppUserName(long appUserId, ThemeDisplay themeDisplay) {
		AppUser appUserObj = null;
		try {
			appUserObj = AppUserLocalServiceUtil.getAppUser(appUserId);
		} catch (PortalException e) {
		}
		return appUserObj;
	}

	public Event getEventName(long eventId, ThemeDisplay themeDisplay) {
		Event event = null;

		try {
			event = EventLocalServiceUtil.getEvent(eventId);
			/* eventName = event.getTitle(themeDisplay.getLocale()); */
		} catch (PortalException e) {
			/* eventName = StringPool.BLANK; */
		}
		return event;
	}

	private static final Log _log = LogFactoryUtil.getLog(ExportEventVisitorsMVCResourceCommand.class);

	@Reference
	private Portal _portal;
}
