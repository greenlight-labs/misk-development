package events.admin.web.portlet;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import events.admin.web.constants.EventsAdminWebPortletKeys;
import events.admin.web.model.EventVisitorsDTO;
import events.model.Event;
import events.model.EventVisitors;
import events.service.EventLocalServiceUtil;
import events.service.EventVisitorsLocalServiceUtil;

/**
 * @author manis
 */
@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true", "com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false", "com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true", "javax.portlet.display-name=EventVisitorAdminWeb",
		"javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/event-visitor/view.jsp",
		"javax.portlet.name=" + EventsAdminWebPortletKeys.EVENTVISITORADMINWEB,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",

}, service = Portlet.class)
public class EventVisitorAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		List<EventVisitors> eventVisitorsList = EventVisitorsLocalServiceUtil.getEventVisitorses(-1, -1);
		HashMap<String, EventVisitorsDTO> map = new HashMap<String, EventVisitorsDTO>();
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		if (Validator.isNotNull(eventVisitorsList)) {
			for (EventVisitors eventVisitor : eventVisitorsList) {
				String eventName = getEventName(eventVisitor.getEventvisitorId(), themeDisplay);
				if (!eventName.isEmpty()) {
					if (map.containsKey(String.valueOf(eventVisitor.getEventvisitorId()))) {
						EventVisitorsDTO eventObj = map.get(String.valueOf(eventVisitor.getEventvisitorId()));
						map.put(String.valueOf(eventVisitor.getEventvisitorId()), increaseCount(eventObj));
					} else {
						EventVisitorsDTO eventObj = new EventVisitorsDTO();
						eventObj.setEventName(eventName);
						eventObj.setVistorCount(1);				
						map.put(String.valueOf(eventVisitor.getEventvisitorId()), eventObj);
					}
				}
			}
		}
		renderRequest.setAttribute("eventVisitor", map);
		super.render(renderRequest, renderResponse);
	}

	public EventVisitorsDTO increaseCount(EventVisitorsDTO eventObj) {
		int count = 1;
		count = eventObj.getVistorCount() + 1;
		eventObj.setVistorCount(count);
		return eventObj;
	}

	public String getEventName(long eventId, ThemeDisplay themeDisplay) {
		String eventName = StringPool.BLANK;

		try {
			Event event = EventLocalServiceUtil.getEvent(eventId);
			eventName = event.getTitle(themeDisplay.getLocale());
		} catch (PortalException e) {
			eventName = StringPool.BLANK;
		}
		return eventName;
	}
}