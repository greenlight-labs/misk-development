package events.admin.web.portlet.action;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import events.admin.web.constants.EventsAdminWebPortletKeys;
import events.admin.web.model.EventVisitorAppUserDTO;
import events.model.Event;
import events.model.EventVisitors;
import events.service.EventLocalServiceUtil;
import events.service.EventVisitorsLocalServiceUtil;
import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalServiceUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + EventsAdminWebPortletKeys.EVENTVISITORADMINWEB,
		"mvc.command.name=" + EventsAdminWebPortletKeys.EVENT_VISITOR_APPUSER }, service = MVCRenderCommand.class)
public class ViewAppUserMVCRenderCommand implements MVCRenderCommand {
	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

		String eventId = ParamUtil.getString(renderRequest, "eventId");
		String eventVisitorId = ParamUtil.getString(renderRequest, "eventVisitorId");
		List<EventVisitorAppUserDTO> appUserList = new ArrayList<>();
		Event event = null;
		AppUser appUser = null;
		try {
			List<EventVisitors> EventVisitorsList = EventVisitorsLocalServiceUtil
					.findEventByvisitor(Long.valueOf(eventId));
			event = getEventObject(Long.parseLong(eventId));

			for (EventVisitors obj : EventVisitorsList) {
				EventVisitorAppUserDTO evAppUserobj = new EventVisitorAppUserDTO();
				try {
					AppUser appUserObj = AppUserLocalServiceUtil.getAppUser(obj.getUserId());
					evAppUserobj.setAppUser(appUserObj);
					evAppUserobj.setEventVisitors(obj);
					appUserList.add(evAppUserobj);
				} catch (PortalException e) {
				}
			}
		} catch (Exception e) {

		}
		renderRequest.setAttribute("appUserList", appUserList);
		renderRequest.setAttribute("event", event);
		return "/event-visitor/viewAppUserDetail.jsp";
	}

	public Event getEventObject(long eventId) {
		Event event = null;

		try {
			event = EventLocalServiceUtil.getEvent(eventId);

		} catch (PortalException e) {

		}
		return event;

	}
}
