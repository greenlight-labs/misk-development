package events.admin.web.model;

import events.model.EventVisitors;
import misk.app.users.model.AppUser;

public class EventVisitorAppUserDTO {
	
	public AppUser appUser;
	public EventVisitors eventVisitors;
	public AppUser getAppUser() {
		return appUser;
	}
	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}
	public EventVisitors getEventVisitors() {
		return eventVisitors;
	}
	public void setEventVisitors(EventVisitors eventVisitors) {
		this.eventVisitors = eventVisitors;
	}
	
	
	
}
