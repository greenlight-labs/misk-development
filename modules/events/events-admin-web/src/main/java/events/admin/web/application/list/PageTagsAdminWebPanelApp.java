package events.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import events.admin.web.constants.EventsAdminWebPanelTagKeys;
import events.admin.web.constants.EventsAdminWebPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=105",
		"panel.category.key=" + EventsAdminWebPanelTagKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class PageTagsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return EventsAdminWebPortletKeys.PAGETAGSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + EventsAdminWebPortletKeys.PAGETAGSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}