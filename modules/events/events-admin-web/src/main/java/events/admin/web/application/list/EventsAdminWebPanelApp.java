package events.admin.web.application.list;

import events.admin.web.constants.EventsAdminWebPanelCategoryKeys;
import events.admin.web.constants.EventsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + EventsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class EventsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return EventsAdminWebPortletKeys.EVENTSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + EventsAdminWebPortletKeys.EVENTSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}