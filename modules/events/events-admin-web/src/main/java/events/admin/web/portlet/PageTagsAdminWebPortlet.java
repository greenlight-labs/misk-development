package events.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import events.admin.web.constants.EventsAdminWebPortletKeys;
import events.exception.PageTagValidateException;
import events.model.PageTag;
import events.service.PageTagLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

/**
 * @author tz
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.add-default-resource=true",
                "com.liferay.portlet.display-tag=tag.hidden",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.layout-cacheable=true",
                "com.liferay.portlet.private-request-attributes=false",
                "com.liferay.portlet.private-session-attributes=false",
                "com.liferay.portlet.render-weight=50",
                "com.liferay.portlet.use-default-template=true",
                "javax.portlet.display-name=PageTagsAdminWeb",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/page-tag/view.jsp",
                "javax.portlet.name=" + EventsAdminWebPortletKeys.PAGETAGSADMINWEB,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",

        },
        service = Portlet.class
)
public class PageTagsAdminWebPortlet extends MVCPortlet {

    public void addEntry(ActionRequest request, ActionResponse response)
            throws Exception, PageTagValidateException {

        long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
        PageTag entry = _pageTagLocalService.getPageTagFromRequest(
                primaryKey, request);
        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                PageTag.class.getName(), request);

        // Add entry
        _pageTagLocalService.addEntry(entry, serviceContext);

        SessionMessages.add(request, "tagAddedSuccessfully");
    }

    public void updateEntry(ActionRequest request, ActionResponse response)
            throws Exception {

        long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

        PageTag entry = _pageTagLocalService.getPageTagFromRequest(
                primaryKey, request);

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                PageTag.class.getName(), request);

        //Update entry
        _pageTagLocalService.updateEntry(entry, serviceContext);

        SessionMessages.add(request, "tagUpdatedSuccessfully");
    }

    public void deleteEntry(ActionRequest request, ActionResponse response)
            throws PortalException {

        long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

        try {
            _pageTagLocalService.deleteEntry(entryId);
        } catch (PortalException pe) {
            ReflectionUtil.throwException(pe);
        }
    }

    @Reference
    private PageTagLocalService _pageTagLocalService;

    @Reference
    private ItemSelector _itemSelector;
}