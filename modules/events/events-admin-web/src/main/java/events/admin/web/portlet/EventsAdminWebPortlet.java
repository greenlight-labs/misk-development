package events.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import events.admin.web.constants.EventsAdminWebPortletKeys;
import events.exception.EventEndDateTimeException;
import events.exception.EventStartDateTimeException;
import events.model.Event;
import events.service.EventLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=EventsAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + EventsAdminWebPortletKeys.EVENTSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class EventsAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addEvent(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Event.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		Date eventStartDateTime = getDateTimeFromRequest(request, "eventStartDateTime");
		Date eventEndDateTime = getDateTimeFromRequest(request, "eventEndDateTime");

		long orderNo = ParamUtil.getLong(request, "orderNo",9999);

		long userId = serviceContext.getUserId();

		User user = userLocalService.getUser(userId);

		// default Value of OrderNo to 9999
		if (orderNo == 0) {
			orderNo = 9999;
		}
		long tagId = ParamUtil.getLong(request, "tagId");
		long categoryId = ParamUtil.getLong(request, "categoryId");
		int type = ParamUtil.getInteger(request, "type");
		int paytype = ParamUtil.getInteger(request, "paytype");
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
		Map<Locale, String> smallImageMap = LocalizationUtil.getLocalizationMap(request, "smallImage");
		Map<Locale, String> bigImageMap = LocalizationUtil.getLocalizationMap(request, "bigImage");
		Date displayDate = ParamUtil.getDate(request, "displayDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale()), null);
		Map<Locale, String> venueMap = LocalizationUtil.getLocalizationMap(request, "venue");
		Map<Locale, String> timingMap = LocalizationUtil.getLocalizationMap(request, "timing");
		Map<Locale, String> durationMap = LocalizationUtil.getLocalizationMap(request, "duration");
		Map<Locale, String> descriptionField1Map = LocalizationUtil.getLocalizationMap(request, "descriptionField1");
		Map<Locale, String> videoLinkMap = LocalizationUtil.getLocalizationMap(request, "videoLink");
		Map<Locale, String> descriptionField2Map = LocalizationUtil.getLocalizationMap(request, "descriptionField2");
		Map<Locale, String> subtitleMap = LocalizationUtil.getLocalizationMap(request, "subtitle");
		Map<Locale, String> descriptionField3Map = LocalizationUtil.getLocalizationMap(request, "descriptionField3");
		Map<Locale, String> detailImageMap = LocalizationUtil.getLocalizationMap(request, "detailImage");
		Map<Locale, String> quoteTextMap = LocalizationUtil.getLocalizationMap(request, "quoteText");
		Map<Locale, String> quoteAuthorMap = LocalizationUtil.getLocalizationMap(request, "quoteAuthor");
		Map<Locale, String> descriptionField4Map = LocalizationUtil.getLocalizationMap(request, "descriptionField4");
		Map<Locale, String> eventfeaturedImage = LocalizationUtil.getLocalizationMap(request, "eventfeaturedImage");
		Map<Locale, String> buyTicketLink = LocalizationUtil.getLocalizationMap(request, "buyTicketLink");
		String typecolor = ParamUtil.getString(request, "typeColor");
		String lat = ParamUtil.getString(request, "lat");
		String lan = ParamUtil.getString(request, "lan");
		String email = ParamUtil.getString(request, "email");
		String phone = ParamUtil.getString(request, "phone");
		String eventPrice = ParamUtil.getString(request, "eventPrice");
		String currency = ParamUtil.getString(request, "currency");
		Map<Locale, String> eventdescription = LocalizationUtil.getLocalizationMap(request, "eventdescription");
		boolean sendPushNotification = ParamUtil.getBoolean(request, "sendPushNotification");

		try {
			Event addedEntry = _eventLocalService.addEvent(serviceContext.getUserId(),
					eventStartDateTime, eventEndDateTime, tagId, categoryId,
					type, paytype, titleMap, smallImageMap, bigImageMap, displayDate,
					venueMap, timingMap, durationMap,
					descriptionField1Map, videoLinkMap, descriptionField2Map, subtitleMap,
					descriptionField3Map, detailImageMap, quoteTextMap, quoteAuthorMap,
					descriptionField4Map,eventdescription,eventfeaturedImage,buyTicketLink, lat,lan,eventPrice,
					email,phone,currency,orderNo,sendPushNotification,serviceContext);

			// Add gallery images
			_eventLocalService.updateEventsGalleries(addedEntry, _eventLocalService.getGalleries(request), serviceContext);
			// add schedule activities
			updateSchedule(addedEntry, request, serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(EventsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit.jsp");
		}
	}

	public void updateEvent(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Event.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long eventId = ParamUtil.getLong(request, "eventId");

		Date eventStartDateTime = getDateTimeFromRequest(request, "eventStartDateTime");
		Date eventEndDateTime = getDateTimeFromRequest(request, "eventEndDateTime");

		long orderNo = ParamUtil.getLong(request, "orderNo",9999);
		long userId = serviceContext.getUserId();
		// default Value of OrderNo to 9999
		if (orderNo == 0) {
			orderNo = 9999;
		}
		User user = userLocalService.getUser(userId);

		long tagId = ParamUtil.getLong(request, "tagId");
		long categoryId = ParamUtil.getLong(request, "categoryId");
		int type = ParamUtil.getInteger(request, "type");
		int paytype = ParamUtil.getInteger(request, "paytype");
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
		Map<Locale, String> smallImageMap = LocalizationUtil.getLocalizationMap(request, "smallImage");
		Map<Locale, String> bigImageMap = LocalizationUtil.getLocalizationMap(request, "bigImage");
		Date displayDate = ParamUtil.getDate(request, "displayDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale()), null);
		Map<Locale, String> venueMap = LocalizationUtil.getLocalizationMap(request, "venue");
		Map<Locale, String> timingMap = LocalizationUtil.getLocalizationMap(request, "timing");
		Map<Locale, String> durationMap = LocalizationUtil.getLocalizationMap(request, "duration");
		Map<Locale, String> descriptionField1Map = LocalizationUtil.getLocalizationMap(request, "descriptionField1");
		Map<Locale, String> videoLinkMap = LocalizationUtil.getLocalizationMap(request, "videoLink");
		Map<Locale, String> descriptionField2Map = LocalizationUtil.getLocalizationMap(request, "descriptionField2");
		Map<Locale, String> subtitleMap = LocalizationUtil.getLocalizationMap(request, "subtitle");
		Map<Locale, String> descriptionField3Map = LocalizationUtil.getLocalizationMap(request, "descriptionField3");
		Map<Locale, String> detailImageMap = LocalizationUtil.getLocalizationMap(request, "detailImage");
		Map<Locale, String> quoteTextMap = LocalizationUtil.getLocalizationMap(request, "quoteText");
		Map<Locale, String> quoteAuthorMap = LocalizationUtil.getLocalizationMap(request, "quoteAuthor");
		Map<Locale, String> descriptionField4Map = LocalizationUtil.getLocalizationMap(request, "descriptionField4");
		Map<Locale, String> eventfeaturedImage = LocalizationUtil.getLocalizationMap(request, "eventfeaturedImage");
		Map<Locale, String> eventdescription = LocalizationUtil.getLocalizationMap(request, "eventdescription");
		Map<Locale, String> buyTicketLink = LocalizationUtil.getLocalizationMap(request, "buyTicketLink");
		String typecolor = ParamUtil.getString(request, "typeColor");
		String lat = ParamUtil.getString(request, "lat");
		String lan = ParamUtil.getString(request, "lan");
		String email = ParamUtil.getString(request, "email");
		String phone = ParamUtil.getString(request, "phone");
		String eventPrice = ParamUtil.getString(request, "eventPrice");
		String currency = ParamUtil.getString(request, "currency");
		boolean sendPushNotification = ParamUtil.getBoolean(request, "sendPushNotification");
		try {
			Event updatedEntry = _eventLocalService.updateEvent(serviceContext.getUserId(), eventId,
					eventStartDateTime, eventEndDateTime, tagId, categoryId,
					type, paytype, titleMap, smallImageMap, bigImageMap, displayDate,
					venueMap, timingMap, durationMap,
					descriptionField1Map, videoLinkMap, descriptionField2Map, subtitleMap,
					descriptionField3Map, detailImageMap, quoteTextMap, quoteAuthorMap,
					descriptionField4Map,eventdescription,eventfeaturedImage, buyTicketLink, lat,lan,eventPrice,
					email,phone,currency,orderNo, sendPushNotification, serviceContext);
			
			// Add gallery images
			_eventLocalService.updateEventsGalleries(updatedEntry, _eventLocalService.getGalleries(request), serviceContext);
			// add schedule activities
			updateSchedule(updatedEntry, request, serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(EventsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit.jsp");
		}
	}

	public void deleteEvent(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Event.class.getName(), request);
		long userId = serviceContext.getUserId();
		User user = userLocalService.getUser(userId);
		long eventId = ParamUtil.getLong(request, "eventId");

		try {
			_eventLocalService.deleteEvent(eventId, serviceContext);
		} catch (PortalException pe) {
			Logger.getLogger(EventsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	public void updateGallery(Event entry, ActionRequest actionRequest, ServiceContext serviceContext)
			throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
				WebKeys.THEME_DISPLAY);


		_eventLocalService.updateEventsGalleries(
				entry,
				_eventLocalService.getGalleries(actionRequest),
				serviceContext
		);
	}

	public void updateSchedule(Event entry, ActionRequest actionRequest, ServiceContext serviceContext)
			throws PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
				WebKeys.THEME_DISPLAY);

		_eventLocalService.updateSchedules(
				entry,
				_eventLocalService.getSchedules(actionRequest),
				serviceContext
		);
	}

	public Date getDateTimeFromRequest(PortletRequest request, String prefix) {
		int Year = ParamUtil.getInteger(request, prefix + "Year");
		int Month = ParamUtil.getInteger(request, prefix + "Month") + 1;
		int Day = ParamUtil.getInteger(request, prefix + "Day");
		int Hour = ParamUtil.getInteger(request, prefix + "Hour");
		int Minute = ParamUtil.getInteger(request, prefix + "Minute");
		int AmPm = ParamUtil.getInteger(request, prefix + "AmPm");

		if (AmPm == Calendar.PM) {
			Hour += 12;
		}

		LocalDateTime ldt;

		try {
			// convert to Asia/Riyadh timezone
			ldt = LocalDateTime.of(Year, Month, Day, Hour, Minute, 0);
			ZonedDateTime zdt = ldt.atZone(ZoneId.of("Asia/Riyadh"));
			ZonedDateTime zdt2 = zdt.withZoneSameInstant(ZoneId.systemDefault());
			ldt = zdt2.toLocalDateTime();
			//ldt = LocalDateTime.of(Year, Month, Day, Hour, Minute, 0);
		} catch (Exception e) {
			Logger.getLogger(EventsAdminWebPortlet.class.getName()).log(Level.SEVERE, "Unable get date data. Initialize with current date", e);
			Date in = new Date();
			Instant instant = in.toInstant();
			return Date.from(instant);
		}

		return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
	}

	@Reference
	private EventLocalService _eventLocalService;
	
	@Reference
	private ItemSelector _itemSelector;

	@Reference
	private Portal _portal;
	
	@Reference
	protected com.liferay.portal.kernel.service.UserLocalService
			userLocalService;
}