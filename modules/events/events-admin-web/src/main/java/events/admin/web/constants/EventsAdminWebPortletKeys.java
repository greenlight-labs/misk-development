package events.admin.web.constants;

/**
 * @author tz
 */
public class EventsAdminWebPortletKeys {

	public static final String EVENTSADMINWEB =
		"events_admin_web_EventsAdminWebPortlet";

	public static final String CATEGORIESADMINWEB =
			"events_admin_web_CategoriesAdminWebPortlet";

	public static final String TAGSADMINWEB =
			"events_admin_web_TagsAdminWebPortlet";

	public static final String PAGETAGSADMINWEB =
			"events_admin_web_PageTagsAdminWebPortlet";
	
	public static final String EVENTVISITORADMINWEB =
			"events_admin_web_EventVisitorAdminWebPortlet";
	
	public static final String EVENT_VISITOR_APPUSER ="viewEventVisitorAppUser";
	public static final String EXPORT_EVENT_VISITOR ="exportEventVisitors";
}