package events.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import events.admin.web.constants.EventVisitorAdminWebPanelCategoryKeys;
import events.admin.web.constants.EventsAdminWebPortletKeys;

/**
 * @author manis
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + EventVisitorAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class EventVisitorAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return EventsAdminWebPortletKeys.EVENTVISITORADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + EventsAdminWebPortletKeys.EVENTVISITORADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}