/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package partners.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PartnerService}.
 *
 * @author Brian Wing Shun Chan
 * @see PartnerService
 * @generated
 */
public class PartnerServiceWrapper
	implements PartnerService, ServiceWrapper<PartnerService> {

	public PartnerServiceWrapper(PartnerService partnerService) {
		_partnerService = partnerService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _partnerService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<partners.model.Partner> getPartners(long groupId) {
		return _partnerService.getPartners(groupId);
	}

	@Override
	public java.util.List<partners.model.Partner> getPartners(
		long groupId, int start, int end) {

		return _partnerService.getPartners(groupId, start, end);
	}

	@Override
	public java.util.List<partners.model.Partner> getPartners(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<partners.model.Partner>
			obc) {

		return _partnerService.getPartners(groupId, start, end, obc);
	}

	@Override
	public int getPartnersCount(long groupId) {
		return _partnerService.getPartnersCount(groupId);
	}

	@Override
	public PartnerService getWrappedService() {
		return _partnerService;
	}

	@Override
	public void setWrappedService(PartnerService partnerService) {
		_partnerService = partnerService;
	}

	private PartnerService _partnerService;

}