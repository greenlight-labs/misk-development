/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package partners.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

import partners.model.Partner;

/**
 * Provides the remote service utility for Partner. This utility wraps
 * <code>partners.service.impl.PartnerServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see PartnerService
 * @generated
 */
public class PartnerServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>partners.service.impl.PartnerServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static List<Partner> getPartners(long groupId) {
		return getService().getPartners(groupId);
	}

	public static List<Partner> getPartners(long groupId, int start, int end) {
		return getService().getPartners(groupId, start, end);
	}

	public static List<Partner> getPartners(
		long groupId, int start, int end, OrderByComparator<Partner> obc) {

		return getService().getPartners(groupId, start, end, obc);
	}

	public static int getPartnersCount(long groupId) {
		return getService().getPartnersCount(groupId);
	}

	public static PartnerService getService() {
		return _service;
	}

	private static volatile PartnerService _service;

}