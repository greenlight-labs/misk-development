/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package partners.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import partners.model.Partner;

/**
 * Provides the local service utility for Partner. This utility wraps
 * <code>partners.service.impl.PartnerLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PartnerLocalService
 * @generated
 */
public class PartnerLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>partners.service.impl.PartnerLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Partner addPartner(
			long userId, String listingImage, String popupImage,
			Map<java.util.Locale, String> nameMap,
			Map<java.util.Locale, String> designationMap,
			Map<java.util.Locale, String> joiningDateMap,
			Map<java.util.Locale, String> descriptionMap, int position,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addPartner(
			userId, listingImage, popupImage, nameMap, designationMap,
			joiningDateMap, descriptionMap, position, serviceContext);
	}

	/**
	 * Adds the partner to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PartnerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param partner the partner
	 * @return the partner that was added
	 */
	public static Partner addPartner(Partner partner) {
		return getService().addPartner(partner);
	}

	/**
	 * Creates a new partner with the primary key. Does not add the partner to the database.
	 *
	 * @param partnerId the primary key for the new partner
	 * @return the new partner
	 */
	public static Partner createPartner(long partnerId) {
		return getService().createPartner(partnerId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the partner with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PartnerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param partnerId the primary key of the partner
	 * @return the partner that was removed
	 * @throws PortalException if a partner with the primary key could not be found
	 */
	public static Partner deletePartner(long partnerId) throws PortalException {
		return getService().deletePartner(partnerId);
	}

	public static Partner deletePartner(
			long partnerId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deletePartner(partnerId, serviceContext);
	}

	/**
	 * Deletes the partner from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PartnerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param partner the partner
	 * @return the partner that was removed
	 */
	public static Partner deletePartner(Partner partner) {
		return getService().deletePartner(partner);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>partners.model.impl.PartnerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>partners.model.impl.PartnerModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Partner fetchPartner(long partnerId) {
		return getService().fetchPartner(partnerId);
	}

	/**
	 * Returns the partner matching the UUID and group.
	 *
	 * @param uuid the partner's UUID
	 * @param groupId the primary key of the group
	 * @return the matching partner, or <code>null</code> if a matching partner could not be found
	 */
	public static Partner fetchPartnerByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchPartnerByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * Returns the partner with the primary key.
	 *
	 * @param partnerId the primary key of the partner
	 * @return the partner
	 * @throws PortalException if a partner with the primary key could not be found
	 */
	public static Partner getPartner(long partnerId) throws PortalException {
		return getService().getPartner(partnerId);
	}

	/**
	 * Returns the partner matching the UUID and group.
	 *
	 * @param uuid the partner's UUID
	 * @param groupId the primary key of the group
	 * @return the matching partner
	 * @throws PortalException if a matching partner could not be found
	 */
	public static Partner getPartnerByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getPartnerByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the partners.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>partners.model.impl.PartnerModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of partners
	 * @param end the upper bound of the range of partners (not inclusive)
	 * @return the range of partners
	 */
	public static List<Partner> getPartners(int start, int end) {
		return getService().getPartners(start, end);
	}

	public static List<Partner> getPartners(long groupId) {
		return getService().getPartners(groupId);
	}

	public static List<Partner> getPartners(long groupId, int start, int end) {
		return getService().getPartners(groupId, start, end);
	}

	public static List<Partner> getPartners(
		long groupId, int start, int end, OrderByComparator<Partner> obc) {

		return getService().getPartners(groupId, start, end, obc);
	}

	/**
	 * Returns all the partners matching the UUID and company.
	 *
	 * @param uuid the UUID of the partners
	 * @param companyId the primary key of the company
	 * @return the matching partners, or an empty list if no matches were found
	 */
	public static List<Partner> getPartnersByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getPartnersByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of partners matching the UUID and company.
	 *
	 * @param uuid the UUID of the partners
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of partners
	 * @param end the upper bound of the range of partners (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching partners, or an empty list if no matches were found
	 */
	public static List<Partner> getPartnersByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Partner> orderByComparator) {

		return getService().getPartnersByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of partners.
	 *
	 * @return the number of partners
	 */
	public static int getPartnersCount() {
		return getService().getPartnersCount();
	}

	public static int getPartnersCount(long groupId) {
		return getService().getPartnersCount(groupId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static Partner updatePartner(
			long userId, long partnerId, String listingImage, String popupImage,
			Map<java.util.Locale, String> nameMap,
			Map<java.util.Locale, String> designationMap,
			Map<java.util.Locale, String> joiningDateMap,
			Map<java.util.Locale, String> descriptionMap, int position,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updatePartner(
			userId, partnerId, listingImage, popupImage, nameMap,
			designationMap, joiningDateMap, descriptionMap, position,
			serviceContext);
	}

	/**
	 * Updates the partner in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PartnerLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param partner the partner
	 * @return the partner that was updated
	 */
	public static Partner updatePartner(Partner partner) {
		return getService().updatePartner(partner);
	}

	public static PartnerLocalService getService() {
		return _service;
	}

	private static volatile PartnerLocalService _service;

}