/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package partners.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link partners.service.http.PartnerServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class PartnerSoap implements Serializable {

	public static PartnerSoap toSoapModel(Partner model) {
		PartnerSoap soapModel = new PartnerSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setPartnerId(model.getPartnerId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setListingImage(model.getListingImage());
		soapModel.setPopupImage(model.getPopupImage());
		soapModel.setName(model.getName());
		soapModel.setDesignation(model.getDesignation());
		soapModel.setJoiningDate(model.getJoiningDate());
		soapModel.setDescription(model.getDescription());
		soapModel.setPosition(model.getPosition());

		return soapModel;
	}

	public static PartnerSoap[] toSoapModels(Partner[] models) {
		PartnerSoap[] soapModels = new PartnerSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PartnerSoap[][] toSoapModels(Partner[][] models) {
		PartnerSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PartnerSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PartnerSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PartnerSoap[] toSoapModels(List<Partner> models) {
		List<PartnerSoap> soapModels = new ArrayList<PartnerSoap>(
			models.size());

		for (Partner model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PartnerSoap[soapModels.size()]);
	}

	public PartnerSoap() {
	}

	public long getPrimaryKey() {
		return _partnerId;
	}

	public void setPrimaryKey(long pk) {
		setPartnerId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getPartnerId() {
		return _partnerId;
	}

	public void setPartnerId(long partnerId) {
		_partnerId = partnerId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getListingImage() {
		return _listingImage;
	}

	public void setListingImage(String listingImage) {
		_listingImage = listingImage;
	}

	public String getPopupImage() {
		return _popupImage;
	}

	public void setPopupImage(String popupImage) {
		_popupImage = popupImage;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDesignation() {
		return _designation;
	}

	public void setDesignation(String designation) {
		_designation = designation;
	}

	public String getJoiningDate() {
		return _joiningDate;
	}

	public void setJoiningDate(String joiningDate) {
		_joiningDate = joiningDate;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public int getPosition() {
		return _position;
	}

	public void setPosition(int position) {
		_position = position;
	}

	private String _uuid;
	private long _partnerId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _listingImage;
	private String _popupImage;
	private String _name;
	private String _designation;
	private String _joiningDate;
	private String _description;
	private int _position;

}