/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package partners.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import partners.model.Partner;

/**
 * The cache model class for representing Partner in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class PartnerCacheModel implements CacheModel<Partner>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof PartnerCacheModel)) {
			return false;
		}

		PartnerCacheModel partnerCacheModel = (PartnerCacheModel)object;

		if (partnerId == partnerCacheModel.partnerId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, partnerId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", partnerId=");
		sb.append(partnerId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", popupImage=");
		sb.append(popupImage);
		sb.append(", name=");
		sb.append(name);
		sb.append(", designation=");
		sb.append(designation);
		sb.append(", joiningDate=");
		sb.append(joiningDate);
		sb.append(", description=");
		sb.append(description);
		sb.append(", position=");
		sb.append(position);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Partner toEntityModel() {
		PartnerImpl partnerImpl = new PartnerImpl();

		if (uuid == null) {
			partnerImpl.setUuid("");
		}
		else {
			partnerImpl.setUuid(uuid);
		}

		partnerImpl.setPartnerId(partnerId);
		partnerImpl.setGroupId(groupId);
		partnerImpl.setCompanyId(companyId);
		partnerImpl.setUserId(userId);

		if (userName == null) {
			partnerImpl.setUserName("");
		}
		else {
			partnerImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			partnerImpl.setCreateDate(null);
		}
		else {
			partnerImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			partnerImpl.setModifiedDate(null);
		}
		else {
			partnerImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (listingImage == null) {
			partnerImpl.setListingImage("");
		}
		else {
			partnerImpl.setListingImage(listingImage);
		}

		if (popupImage == null) {
			partnerImpl.setPopupImage("");
		}
		else {
			partnerImpl.setPopupImage(popupImage);
		}

		if (name == null) {
			partnerImpl.setName("");
		}
		else {
			partnerImpl.setName(name);
		}

		if (designation == null) {
			partnerImpl.setDesignation("");
		}
		else {
			partnerImpl.setDesignation(designation);
		}

		if (joiningDate == null) {
			partnerImpl.setJoiningDate("");
		}
		else {
			partnerImpl.setJoiningDate(joiningDate);
		}

		if (description == null) {
			partnerImpl.setDescription("");
		}
		else {
			partnerImpl.setDescription(description);
		}

		partnerImpl.setPosition(position);

		partnerImpl.resetOriginalValues();

		return partnerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		partnerId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		listingImage = objectInput.readUTF();
		popupImage = objectInput.readUTF();
		name = objectInput.readUTF();
		designation = objectInput.readUTF();
		joiningDate = objectInput.readUTF();
		description = (String)objectInput.readObject();

		position = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(partnerId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (popupImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(popupImage);
		}

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (designation == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(designation);
		}

		if (joiningDate == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(joiningDate);
		}

		if (description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(description);
		}

		objectOutput.writeInt(position);
	}

	public String uuid;
	public long partnerId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String listingImage;
	public String popupImage;
	public String name;
	public String designation;
	public String joiningDate;
	public String description;
	public int position;

}