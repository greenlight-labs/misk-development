/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package partners.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import org.osgi.service.component.annotations.Component;
import partners.model.Partner;
import partners.service.base.PartnerServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the partner remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>partners.service.PartnerService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PartnerServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=partner",
		"json.web.service.context.path=Partner"
	},
	service = AopService.class
)
public class PartnerServiceImpl extends PartnerServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>partners.service.PartnerServiceUtil</code> to access the partner remote service.
	 */

	public List<Partner> getPartners(long groupId) {

		return partnerPersistence.findByGroupId(groupId);
	}

	public List<Partner> getPartners(long groupId, int start, int end,
							   OrderByComparator<Partner> obc) {

		return partnerPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Partner> getPartners(long groupId, int start, int end) {

		return partnerPersistence.findByGroupId(groupId, start, end);
	}

	public int getPartnersCount(long groupId) {

		return partnerPersistence.countByGroupId(groupId);
	}
}