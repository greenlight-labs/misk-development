/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package partners.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import partners.exception.*;
import partners.model.Partner;
import partners.service.base.PartnerLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the partner local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>partners.service.PartnerLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PartnerLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=partners.model.Partner",
	service = AopService.class
)
public class PartnerLocalServiceImpl extends PartnerLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>partners.service.PartnerLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>partners.service.PartnerLocalServiceUtil</code>.
	 */

	public Partner addPartner(long userId,
						String listingImage, String popupImage, Map<Locale, String> nameMap, Map<Locale, String> designationMap,
						Map<Locale, String> joiningDateMap, Map<Locale, String> descriptionMap, int position,
						ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(listingImage, popupImage, nameMap, designationMap, descriptionMap);

		long partnerId = counterLocalService.increment();

		Partner partner = partnerPersistence.create(partnerId);

		partner.setUuid(serviceContext.getUuid());
		partner.setUserId(userId);
		partner.setGroupId(groupId);
		partner.setCompanyId(user.getCompanyId());
		partner.setUserName(user.getFullName());
		partner.setCreateDate(serviceContext.getCreateDate(now));
		partner.setModifiedDate(serviceContext.getModifiedDate(now));

		partner.setListingImage(listingImage);
		partner.setPopupImage(popupImage);
		partner.setNameMap(nameMap);
		partner.setDesignationMap(designationMap);
		partner.setJoiningDateMap(joiningDateMap);
		partner.setDescriptionMap(descriptionMap);
		partner.setPosition(position);

		partner.setExpandoBridgeAttributes(serviceContext);

		partnerPersistence.update(partner);
		partnerPersistence.clearCache();

		return partner;
	}

	public Partner updatePartner(long userId, long partnerId,
						   String listingImage, String popupImage, Map<Locale, String> nameMap, Map<Locale, String> designationMap,
						   Map<Locale, String> joiningDateMap, Map<Locale, String> descriptionMap, int position,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(listingImage, popupImage, nameMap, designationMap, descriptionMap);

		Partner partner = getPartner(partnerId);

		User user = userLocalService.getUser(userId);

		partner.setUserId(userId);
		partner.setUserName(user.getFullName());
		partner.setModifiedDate(serviceContext.getModifiedDate(now));

		partner.setListingImage(listingImage);
		partner.setPopupImage(popupImage);
		partner.setNameMap(nameMap);
		partner.setDesignationMap(designationMap);
		partner.setJoiningDateMap(joiningDateMap);
		partner.setDescriptionMap(descriptionMap);
		partner.setPosition(position);

		partner.setExpandoBridgeAttributes(serviceContext);

		partnerPersistence.update(partner);
		partnerPersistence.clearCache();

		return partner;
	}

	public Partner deletePartner(long partnerId,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Partner partner = getPartner(partnerId);
		partner = deletePartner(partner);

		return partner;
	}

	public List<Partner> getPartners(long groupId) {

		return partnerPersistence.findByGroupId(groupId);
	}

	public List<Partner> getPartners(long groupId, int start, int end,
							   OrderByComparator<Partner> obc) {

		return partnerPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Partner> getPartners(long groupId, int start, int end) {

		return partnerPersistence.findByGroupId(groupId, start, end);
	}

	public int getPartnersCount(long groupId) {

		return partnerPersistence.countByGroupId(groupId);
	}

	protected void validate(
			String listingImage, String popupImage, Map<Locale, String> nameMap,
			Map<Locale, String> designationMap, Map<Locale, String> descriptionMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		if (Validator.isNull(listingImage)) {
			throw new PartnerListingImageException();
		}

		if (Validator.isNull(popupImage)) {
			throw new PartnerPopupImageException();
		}

		String name = nameMap.get(locale);

		if (Validator.isNull(name)) {
			throw new PartnerNameException();
		}

		String designation = designationMap.get(locale);

		if (Validator.isNull(designation)) {
			throw new PartnerDesignationException();
		}

		String description = descriptionMap.get(locale);

		if (Validator.isNull(description)) {
			throw new PartnerDescriptionException();
		}

	}
}