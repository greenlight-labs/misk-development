create index IX_CD353A4E on misk_partners (groupId);
create index IX_6DC4D4D0 on misk_partners (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_7D1C4F52 on misk_partners (uuid_[$COLUMN_LENGTH:75$], groupId);