create table misk_partners (
	uuid_ VARCHAR(75) null,
	partnerId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	listingImage VARCHAR(200) null,
	popupImage VARCHAR(200) null,
	name STRING null,
	designation STRING null,
	joiningDate STRING null,
	description TEXT null,
	position INTEGER
);