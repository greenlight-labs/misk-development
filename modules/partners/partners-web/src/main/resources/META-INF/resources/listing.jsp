<div class="row">
<%
    List<Partner> partners = PartnerServiceUtil.getPartners(scopeGroupId, start, end);
    int partners_size = partners.size();
    if(partners_size > 0){
        int counter = 1;
        for (Partner curPartner : partners) {
%>
    <div class="col-md-4 main-divs">
        <div class="employee-info-sec">
            <div class="emp-img">
                <img class="img-fluid" src="<%= curPartner.getListingImage() %>" alt="pic">
            </div>
            <h4><%= curPartner.getName(locale) %></h4>
            <%--<h5><%= curPartner.getDesignation(locale) %></h5>--%>
            <hr class="dashed-line">
            <button class="btn btn-outline-white teams-read-more-btn">
                <span><liferay-ui:message key="partners_web_read_more" /></span><i class="dot-line"></i>
            </button>
            <a href="#teamsModal<%=counter%>" data-fancybox="teamsInfo"></a>
        </div>
        <div id="teamsModal<%=counter%>" class="modal-main-box" >
            <div class="container-fluid">
                <div class="row" dir="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "rtl" : "ltr" %>">
                    <div class="col-md-5 pl-0">
                        <div class="modal-img">
                            <img class="img-fluid" src="<%= curPartner.getPopupImage() %>" alt="pic">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="modal-content">
                            <h3><%= curPartner.getName(locale) %></h3>
                            <%--<h4><%= curPartner.getDesignation(locale) %></h4>
                            <h5><%= curPartner.getJoiningDate(locale) %></h5>--%>
                            <hr>
                            <%= curPartner.getDescription(locale) %>
                        </div>
                    </div>
                </div>
                <% if(partners_size > 1){ %>
                <div class="row" dir="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "rtl" : "ltr" %>">
                    <div class="col-md-6 left-content-box">
                        <div class="modal-bottom-left-content">
                            <% if(counter > 1){ %>
                                <h4><%= partners.get(counter-2).getName(locale) %></h4>
                                <%--<h6><%= partners.get(counter-2).getDesignation(locale) %></h6>--%>
                                <a data-fancybox-prev href="javascript:" class="btn btn-outline-white" style="color: #da1884">
                                    <span><liferay-ui:message key="partners_web_previous" /></span><i class="dot-line"></i>
                                </a>
                            <% } %>
                        </div>
                    </div>
                    <div class="col-md-6 left-content-box">
                        <div class="modal-bottom-right-content">
                            <% if(counter < limit && counter < partners_size){ %>
                                <h4><%= partners.get(counter).getName(locale) %></h4>
                                <%--<h6><%= partners.get(counter).getDesignation(locale) %></h6>--%>
                                <a data-fancybox-next href="javascript:" class="btn btn-outline-white" style="color: #da1884">
                                    <span><liferay-ui:message key="partners_web_next" /></span><i class="dot-line"></i>
                                </a>
                            <% } %>
                        </div>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <% if(counter == 2){ %>
        </div>
        <div class="teams-blue-shadow-img animate" data-animation="fadeInRight" data-duration="700"><img src="/assets/svgs/blue-shape.svg" alt="" class="img-fluid"></div>
        <div class="row">
    <% } %>
    <% if(counter == 5 || counter == 7){ %>
        </div>
        <div class="row">
    <% } %>
<%
        counter++;
    }
%>
</div>
<%
} else {
%>
<div class="text-center pt-5">
    <h4>No Record Found.</h4>
</div>
<%
    }
%>

<script type="text/javascript">
    window.addEventListener('load', function() {
        // open fancybox on button click
        $('.teams-read-more-btn').on('click', function (event) {
            event.preventDefault();
            $(this).next().trigger('click');
            return false;
        });
    });
</script>