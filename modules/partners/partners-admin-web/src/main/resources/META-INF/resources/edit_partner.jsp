<%@include file = "init.jsp" %>

<%
    long partnerId = ParamUtil.getLong(request, "partnerId");

    Partner partner = null;

    if (partnerId > 0) {
        try {
            partner = PartnerLocalServiceUtil.getPartner(partnerId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= partner == null ? "addPartner" : "updatePartner" %>' var="editPartnerURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editPartnerURL %>" name="fm">

    <aui:model-context bean="<%= partner %>" model="<%= Partner.class %>" />

    <aui:input type="hidden" name="partnerId"
               value='<%= partner == null ? "" : partner.getPartnerId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:input name="name" label="Name" helpMessage="Max 20 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="designation" label="Designation" helpMessage="Max 30 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="joiningDate" label="Joining Date" helpMessage="Max 30 Characters (Recommended)" />
            <aui:input name="listingImage" label="Listing Image" helpMessage="Image Dimensions: 405 x 493 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="listingImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="popupImage" label="Popup Image" helpMessage="Image Dimensions: 690 x 690 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="popupImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="description" label="Description" helpMessage="Max 560 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="position" label="Position" helpMessage="Enter position to show members in sort order">
                <aui:validator name="required"/>
            </aui:input>
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>