<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="partners-admin-buttons">
        <portlet:renderURL var="addPartnerURL">
            <portlet:param name="mvcPath"
                           value="/edit_partner.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addPartnerURL.toString() %>"
                    value="Add Partner"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= PartnerLocalServiceUtil.getPartnersCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= PartnerLocalServiceUtil.getPartners(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="partners.model.Partner" modelVar="partner">

            <liferay-ui:search-container-column-text name="Name" value="<%= HtmlUtil.escape(partner.getName(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Designation" value="<%= HtmlUtil.escape(partner.getDesignation(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Joining Date" value="<%= HtmlUtil.escape(partner.getJoiningDate(locale)) %>"/>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>