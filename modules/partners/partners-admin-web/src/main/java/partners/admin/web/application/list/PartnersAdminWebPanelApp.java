package partners.admin.web.application.list;

import partners.admin.web.constants.PartnersAdminWebPanelCategoryKeys;
import partners.admin.web.constants.PartnersAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + PartnersAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class PartnersAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return PartnersAdminWebPortletKeys.PARTNERSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + PartnersAdminWebPortletKeys.PARTNERSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}