package partners.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import partners.admin.web.constants.PartnersAdminWebPortletKeys;
import partners.model.Partner;
import partners.service.PartnerLocalService;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=PartnersAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + PartnersAdminWebPortletKeys.PARTNERSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class PartnersAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addPartner(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Partner.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		String listingImage = ParamUtil.getString(request, "listingImage");
		String popupImage = ParamUtil.getString(request, "popupImage");
		Map<Locale, String> nameMap = LocalizationUtil.getLocalizationMap(request, "name");
		Map<Locale, String> designationMap = LocalizationUtil.getLocalizationMap(request, "designation");
		Map<Locale, String> joiningDateMap = LocalizationUtil.getLocalizationMap(request, "joiningDate");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(request, "description");
		int position = ParamUtil.getInteger(request, "position");

		try {
			_partnerLocalService.addPartner(serviceContext.getUserId(),
					listingImage, popupImage, nameMap, designationMap, joiningDateMap, descriptionMap, position,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(PartnersAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_partner.jsp");
		}
	}

	public void updatePartner(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Partner.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long partnerId = ParamUtil.getLong(request, "partnerId");

		String listingImage = ParamUtil.getString(request, "listingImage");
		String popupImage = ParamUtil.getString(request, "popupImage");
		Map<Locale, String> nameMap = LocalizationUtil.getLocalizationMap(request, "name");
		Map<Locale, String> designationMap = LocalizationUtil.getLocalizationMap(request, "designation");
		Map<Locale, String> joiningDateMap = LocalizationUtil.getLocalizationMap(request, "joiningDate");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(request, "description");
		int position = ParamUtil.getInteger(request, "position");

		try {
			_partnerLocalService.updatePartner(serviceContext.getUserId(), partnerId,
					listingImage, popupImage, nameMap, designationMap, joiningDateMap, descriptionMap, position,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(PartnersAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_partner.jsp");
		}
	}

	public void deletePartner(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Partner.class.getName(), request);

		long partnerId = ParamUtil.getLong(request, "partnerId");

		try {
			_partnerLocalService.deletePartner(partnerId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(PartnersAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private PartnerLocalService _partnerLocalService;

	@Reference
	private ItemSelector _itemSelector;
}