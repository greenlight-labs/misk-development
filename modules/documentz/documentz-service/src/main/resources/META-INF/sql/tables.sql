create table misk_documentz (
	uuid_ VARCHAR(75) null,
	documentId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title STRING null,
	image VARCHAR(200) null,
	publishedDate DATE null,
	pdfFile STRING null,
	pdfSize STRING null
);