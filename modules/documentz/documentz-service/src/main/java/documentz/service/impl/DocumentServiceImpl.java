/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package documentz.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import documentz.model.Document;
import documentz.service.base.DocumentServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the document remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>documentz.service.DocumentService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=document",
		"json.web.service.context.path=Document"
	},
	service = AopService.class
)
public class DocumentServiceImpl extends DocumentServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>documentz.service.DocumentServiceUtil</code> to access the document remote service.
	 */

	public List<Document> getDocumentz(long groupId) {

		return documentPersistence.findByGroupId(groupId);
	}

	public List<Document> getDocumentz(long groupId, int start, int end,
								 OrderByComparator<Document> obc) {

		return documentPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Document> getDocumentz(long groupId, int start, int end) {

		return documentPersistence.findByGroupId(groupId, start, end);
	}

	public int getDocumentzCount(long groupId) {

		return documentPersistence.countByGroupId(groupId);
	}
}