/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package documentz.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import documentz.exception.*;
import documentz.model.Document;
import documentz.service.base.DocumentLocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the document local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>documentz.service.DocumentLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=documentz.model.Document",
	service = AopService.class
)
public class DocumentLocalServiceImpl extends DocumentLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>documentz.service.DocumentLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>documentz.service.DocumentLocalServiceUtil</code>.
	 */

	public Document addDocument(long userId,
							Map<Locale, String> titleMap, String image, Date publishedDate, Map<Locale, String> pdfFileMap, Map<Locale, String> pdfSizeMap,
						  ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(titleMap, image, publishedDate, pdfFileMap, pdfSizeMap);

		long documentId = counterLocalService.increment();

		Document document = documentPersistence.create(documentId);

		document.setUuid(serviceContext.getUuid());
		document.setUserId(userId);
		document.setGroupId(groupId);
		document.setCompanyId(user.getCompanyId());
		document.setUserName(user.getFullName());
		document.setCreateDate(serviceContext.getCreateDate(now));
		document.setModifiedDate(serviceContext.getModifiedDate(now));

		document.setTitleMap(titleMap);
		document.setImage(image);
		document.setPublishedDate(publishedDate);
		document.setPdfFileMap(pdfFileMap);
		document.setPdfSizeMap(pdfSizeMap);

		document.setExpandoBridgeAttributes(serviceContext);

		documentPersistence.update(document);
		documentPersistence.clearCache();

		return document;
	}

	public Document updateDocument(long userId, long documentId,
								   Map<Locale, String> titleMap, String image, Date publishedDate, Map<Locale, String> pdfFileMap, Map<Locale, String> pdfSizeMap,
							 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(titleMap, image, publishedDate, pdfFileMap, pdfSizeMap);

		Document document = getDocument(documentId);

		User user = userLocalService.getUser(userId);

		document.setUserId(userId);
		document.setUserName(user.getFullName());
		document.setModifiedDate(serviceContext.getModifiedDate(now));

		document.setTitleMap(titleMap);
		document.setImage(image);
		document.setPublishedDate(publishedDate);
		document.setPdfFileMap(pdfFileMap);
		document.setPdfSizeMap(pdfSizeMap);

		document.setExpandoBridgeAttributes(serviceContext);

		documentPersistence.update(document);
		documentPersistence.clearCache();

		return document;
	}

	public Document deleteDocument(long documentId,
							 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Document document = getDocument(documentId);
		document = deleteDocument(document);

		return document;
	}

	public List<Document> getDocumentz(long groupId) {

		return documentPersistence.findByGroupId(groupId);
	}

	public List<Document> getDocumentz(long groupId, int start, int end,
								 OrderByComparator<Document> obc) {

		return documentPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Document> getDocumentz(long groupId, int start, int end) {

		return documentPersistence.findByGroupId(groupId, start, end);
	}

	public int getDocumentzCount(long groupId) {

		return documentPersistence.countByGroupId(groupId);
	}

	protected void validate(
			Map<Locale, String> titleMap, String image, Date publishedDate, Map<Locale, String> pdfFileMap, Map<Locale, String> pdfSizeMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String title = titleMap.get(locale);

		if (Validator.isNull(title)) {
			throw new DocumentTitleException();
		}

		if (Validator.isNull(image)) {
			throw new DocumentImageException();
		}

		if (Validator.isNull(publishedDate)) {
			throw new DocumentPublishedDateException();
		}

		String pdfFile = pdfFileMap.get(locale);

		if (Validator.isNull(pdfFile)) {
			throw new DocumentPdfFileException();
		}

		String pdfSize = pdfSizeMap.get(locale);

		if (Validator.isNull(pdfSize)) {
			throw new DocumentPdfSizeException();
		}

	}
}