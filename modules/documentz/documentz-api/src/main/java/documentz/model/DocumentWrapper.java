/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package documentz.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Document}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Document
 * @generated
 */
public class DocumentWrapper
	extends BaseModelWrapper<Document>
	implements Document, ModelWrapper<Document> {

	public DocumentWrapper(Document document) {
		super(document);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("documentId", getDocumentId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("image", getImage());
		attributes.put("publishedDate", getPublishedDate());
		attributes.put("pdfFile", getPdfFile());
		attributes.put("pdfSize", getPdfSize());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long documentId = (Long)attributes.get("documentId");

		if (documentId != null) {
			setDocumentId(documentId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		Date publishedDate = (Date)attributes.get("publishedDate");

		if (publishedDate != null) {
			setPublishedDate(publishedDate);
		}

		String pdfFile = (String)attributes.get("pdfFile");

		if (pdfFile != null) {
			setPdfFile(pdfFile);
		}

		String pdfSize = (String)attributes.get("pdfSize");

		if (pdfSize != null) {
			setPdfSize(pdfSize);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the company ID of this document.
	 *
	 * @return the company ID of this document
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this document.
	 *
	 * @return the create date of this document
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the document ID of this document.
	 *
	 * @return the document ID of this document
	 */
	@Override
	public long getDocumentId() {
		return model.getDocumentId();
	}

	/**
	 * Returns the group ID of this document.
	 *
	 * @return the group ID of this document
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the image of this document.
	 *
	 * @return the image of this document
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the modified date of this document.
	 *
	 * @return the modified date of this document
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the pdf file of this document.
	 *
	 * @return the pdf file of this document
	 */
	@Override
	public String getPdfFile() {
		return model.getPdfFile();
	}

	/**
	 * Returns the localized pdf file of this document in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized pdf file of this document
	 */
	@Override
	public String getPdfFile(java.util.Locale locale) {
		return model.getPdfFile(locale);
	}

	/**
	 * Returns the localized pdf file of this document in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized pdf file of this document. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getPdfFile(java.util.Locale locale, boolean useDefault) {
		return model.getPdfFile(locale, useDefault);
	}

	/**
	 * Returns the localized pdf file of this document in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized pdf file of this document
	 */
	@Override
	public String getPdfFile(String languageId) {
		return model.getPdfFile(languageId);
	}

	/**
	 * Returns the localized pdf file of this document in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized pdf file of this document
	 */
	@Override
	public String getPdfFile(String languageId, boolean useDefault) {
		return model.getPdfFile(languageId, useDefault);
	}

	@Override
	public String getPdfFileCurrentLanguageId() {
		return model.getPdfFileCurrentLanguageId();
	}

	@Override
	public String getPdfFileCurrentValue() {
		return model.getPdfFileCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized pdf files of this document.
	 *
	 * @return the locales and localized pdf files of this document
	 */
	@Override
	public Map<java.util.Locale, String> getPdfFileMap() {
		return model.getPdfFileMap();
	}

	/**
	 * Returns the pdf size of this document.
	 *
	 * @return the pdf size of this document
	 */
	@Override
	public String getPdfSize() {
		return model.getPdfSize();
	}

	/**
	 * Returns the localized pdf size of this document in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized pdf size of this document
	 */
	@Override
	public String getPdfSize(java.util.Locale locale) {
		return model.getPdfSize(locale);
	}

	/**
	 * Returns the localized pdf size of this document in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized pdf size of this document. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getPdfSize(java.util.Locale locale, boolean useDefault) {
		return model.getPdfSize(locale, useDefault);
	}

	/**
	 * Returns the localized pdf size of this document in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized pdf size of this document
	 */
	@Override
	public String getPdfSize(String languageId) {
		return model.getPdfSize(languageId);
	}

	/**
	 * Returns the localized pdf size of this document in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized pdf size of this document
	 */
	@Override
	public String getPdfSize(String languageId, boolean useDefault) {
		return model.getPdfSize(languageId, useDefault);
	}

	@Override
	public String getPdfSizeCurrentLanguageId() {
		return model.getPdfSizeCurrentLanguageId();
	}

	@Override
	public String getPdfSizeCurrentValue() {
		return model.getPdfSizeCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized pdf sizes of this document.
	 *
	 * @return the locales and localized pdf sizes of this document
	 */
	@Override
	public Map<java.util.Locale, String> getPdfSizeMap() {
		return model.getPdfSizeMap();
	}

	/**
	 * Returns the primary key of this document.
	 *
	 * @return the primary key of this document
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the published date of this document.
	 *
	 * @return the published date of this document
	 */
	@Override
	public Date getPublishedDate() {
		return model.getPublishedDate();
	}

	/**
	 * Returns the title of this document.
	 *
	 * @return the title of this document
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this document in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this document
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this document in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this document. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this document in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this document
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this document in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this document
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this document.
	 *
	 * @return the locales and localized titles of this document
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the user ID of this document.
	 *
	 * @return the user ID of this document
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this document.
	 *
	 * @return the user name of this document
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this document.
	 *
	 * @return the user uuid of this document
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this document.
	 *
	 * @return the uuid of this document
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the company ID of this document.
	 *
	 * @param companyId the company ID of this document
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this document.
	 *
	 * @param createDate the create date of this document
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the document ID of this document.
	 *
	 * @param documentId the document ID of this document
	 */
	@Override
	public void setDocumentId(long documentId) {
		model.setDocumentId(documentId);
	}

	/**
	 * Sets the group ID of this document.
	 *
	 * @param groupId the group ID of this document
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the image of this document.
	 *
	 * @param image the image of this document
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the modified date of this document.
	 *
	 * @param modifiedDate the modified date of this document
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the pdf file of this document.
	 *
	 * @param pdfFile the pdf file of this document
	 */
	@Override
	public void setPdfFile(String pdfFile) {
		model.setPdfFile(pdfFile);
	}

	/**
	 * Sets the localized pdf file of this document in the language.
	 *
	 * @param pdfFile the localized pdf file of this document
	 * @param locale the locale of the language
	 */
	@Override
	public void setPdfFile(String pdfFile, java.util.Locale locale) {
		model.setPdfFile(pdfFile, locale);
	}

	/**
	 * Sets the localized pdf file of this document in the language, and sets the default locale.
	 *
	 * @param pdfFile the localized pdf file of this document
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setPdfFile(
		String pdfFile, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setPdfFile(pdfFile, locale, defaultLocale);
	}

	@Override
	public void setPdfFileCurrentLanguageId(String languageId) {
		model.setPdfFileCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized pdf files of this document from the map of locales and localized pdf files.
	 *
	 * @param pdfFileMap the locales and localized pdf files of this document
	 */
	@Override
	public void setPdfFileMap(Map<java.util.Locale, String> pdfFileMap) {
		model.setPdfFileMap(pdfFileMap);
	}

	/**
	 * Sets the localized pdf files of this document from the map of locales and localized pdf files, and sets the default locale.
	 *
	 * @param pdfFileMap the locales and localized pdf files of this document
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setPdfFileMap(
		Map<java.util.Locale, String> pdfFileMap,
		java.util.Locale defaultLocale) {

		model.setPdfFileMap(pdfFileMap, defaultLocale);
	}

	/**
	 * Sets the pdf size of this document.
	 *
	 * @param pdfSize the pdf size of this document
	 */
	@Override
	public void setPdfSize(String pdfSize) {
		model.setPdfSize(pdfSize);
	}

	/**
	 * Sets the localized pdf size of this document in the language.
	 *
	 * @param pdfSize the localized pdf size of this document
	 * @param locale the locale of the language
	 */
	@Override
	public void setPdfSize(String pdfSize, java.util.Locale locale) {
		model.setPdfSize(pdfSize, locale);
	}

	/**
	 * Sets the localized pdf size of this document in the language, and sets the default locale.
	 *
	 * @param pdfSize the localized pdf size of this document
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setPdfSize(
		String pdfSize, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setPdfSize(pdfSize, locale, defaultLocale);
	}

	@Override
	public void setPdfSizeCurrentLanguageId(String languageId) {
		model.setPdfSizeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized pdf sizes of this document from the map of locales and localized pdf sizes.
	 *
	 * @param pdfSizeMap the locales and localized pdf sizes of this document
	 */
	@Override
	public void setPdfSizeMap(Map<java.util.Locale, String> pdfSizeMap) {
		model.setPdfSizeMap(pdfSizeMap);
	}

	/**
	 * Sets the localized pdf sizes of this document from the map of locales and localized pdf sizes, and sets the default locale.
	 *
	 * @param pdfSizeMap the locales and localized pdf sizes of this document
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setPdfSizeMap(
		Map<java.util.Locale, String> pdfSizeMap,
		java.util.Locale defaultLocale) {

		model.setPdfSizeMap(pdfSizeMap, defaultLocale);
	}

	/**
	 * Sets the primary key of this document.
	 *
	 * @param primaryKey the primary key of this document
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the published date of this document.
	 *
	 * @param publishedDate the published date of this document
	 */
	@Override
	public void setPublishedDate(Date publishedDate) {
		model.setPublishedDate(publishedDate);
	}

	/**
	 * Sets the title of this document.
	 *
	 * @param title the title of this document
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this document in the language.
	 *
	 * @param title the localized title of this document
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this document in the language, and sets the default locale.
	 *
	 * @param title the localized title of this document
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this document from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this document
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this document from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this document
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this document.
	 *
	 * @param userId the user ID of this document
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this document.
	 *
	 * @param userName the user name of this document
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this document.
	 *
	 * @param userUuid the user uuid of this document
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this document.
	 *
	 * @param uuid the uuid of this document
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected DocumentWrapper wrap(Document document) {
		return new DocumentWrapper(document);
	}

}