/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package documentz.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DocumentService}.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentService
 * @generated
 */
public class DocumentServiceWrapper
	implements DocumentService, ServiceWrapper<DocumentService> {

	public DocumentServiceWrapper(DocumentService documentService) {
		_documentService = documentService;
	}

	@Override
	public java.util.List<documentz.model.Document> getDocumentz(long groupId) {
		return _documentService.getDocumentz(groupId);
	}

	@Override
	public java.util.List<documentz.model.Document> getDocumentz(
		long groupId, int start, int end) {

		return _documentService.getDocumentz(groupId, start, end);
	}

	@Override
	public java.util.List<documentz.model.Document> getDocumentz(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<documentz.model.Document> obc) {

		return _documentService.getDocumentz(groupId, start, end, obc);
	}

	@Override
	public int getDocumentzCount(long groupId) {
		return _documentService.getDocumentzCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _documentService.getOSGiServiceIdentifier();
	}

	@Override
	public DocumentService getWrappedService() {
		return _documentService;
	}

	@Override
	public void setWrappedService(DocumentService documentService) {
		_documentService = documentService;
	}

	private DocumentService _documentService;

}