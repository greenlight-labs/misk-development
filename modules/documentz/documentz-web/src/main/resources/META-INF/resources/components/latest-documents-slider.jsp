<%@ page import="com.liferay.portal.kernel.util.PortalUtil" %>
<%@ page import="documentz.service.DocumentLocalServiceUtil" %>
<%@ include file="/init.jsp" %>
<%
    List<Document> latestDocumentz = DocumentLocalServiceUtil.getDocumentz(scopeGroupId, 0, 9);
    int count = latestDocumentz.size();
%>

<%
    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String editMode = originalRequest.getParameter("p_l_mode");
%>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
    <div class="row">
        <div class="col-12">
            <div class="page-editor__collection__block">
                <div class="page-editor__collection-item page-editor__topper">
                    <div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title">Latest Documents Slider Module</p></div>
                    <br><br>
                    <div class="alert alert-info portlet-configuration">
                        <aui:a href="<%= portletDisplay.getURLConfiguration() %>" label="please-configure-this-portlet-to-make-it-visible-to-all-users" onClick="<%= portletDisplay.getURLConfigurationJS() %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } else { %>

<% if(count > 0 ) { %>

<section class="document-section" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="related-news-heading">
                    <% if(themeDisplay.getLanguageId().equals("ar_SA")) { %>
                        <h3><liferay-ui:message key="documents_web_portlet_config_section_title" /></h3>
                        <% if(buttonLabel != null && buttonLink != null) { %>
                            <a href="<%= buttonLink %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><liferay-ui:message key="documents_web_portlet_config_button_label" /></span><i class="dot-line"></i></a>
                        <% } %>
                    <% }else{ %>
                        <h3><%= sectionTitle.replaceAll("\n", "<br/>") %></h3>
                        <% if(buttonLabel != null && buttonLink != null) { %>
                            <a href="<%= buttonLink %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><%= buttonLabel %></span><i class="dot-line"></i></a>
                        <% } %>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="download-pdf-slider-section">
                    <% for (Document curDocument : latestDocumentz){ %>
                        <portlet:renderURL var="detailPageURL4">
                            <portlet:param name="eventId" value="<%= String.valueOf(curDocument.getDocumentId()) %>" />
                            <portlet:param name="mvcPath" value="/detail.jsp" />
                        </portlet:renderURL>

                        <div class="download-pdf-slider-body">
                            <div class="download-pdf-image-box">
                                <img src="<%= curDocument.getImage() %>" alt="" class="img-fluid">
                            </div>
                            <p class="download-pdf-published-date"><%= dateFormat.format(curDocument.getCreateDate()) %></p>
                            <h4 class="download-pdf-title"><%= curDocument.getTitle(locale) %></h4>
                            <div class="download-pdf-link-box">
                                <a href="javascript:" class="pdf-icon-link"><i class="icon-pdf"></i></a>
                                <div class="download-pdf-link-body">
                                    <a href="<%= detailPageURL4.toString() %>" class="download-pdf-text"><liferay-ui:message key="documents_web_download_now" /></a>
                                    <span class="download-pdf-size"><%= curDocument.getPdfSize(locale) %></span>
                                </div>
                            </div>
                        </div>
                    <% } %>

                </div>
            </div>
        </div>
    </div>
</section>

<% } %>
<% } %>