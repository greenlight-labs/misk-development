<%
    List<Document> documents = DocumentServiceUtil.getDocumentz(scopeGroupId, start, end);
    int documents_size = documents.size();
    if(documents_size > 0){
        for (Document curDocument : documents) {
%>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="download-pdf-slider-body">
            <div class="download-pdf-image-box">
                <img src="<%= curDocument.getImage() %>" alt="" class="img-fluid">
            </div>
            <p class="download-pdf-published-date"><%= curDocument.getPublishedDate() %></p>
            <h4 class="download-pdf-title"><%= curDocument.getTitle(locale) %></h4>
            <div class="download-pdf-link-box">
                <a href="<%= curDocument.getPdfFile(locale) %>" class="pdf-icon-link"><i class="icon-pdf"></i></a>
                <div class="download-pdf-link-body">
                    <p class="download-pdf-text">Download Now</p>
                    <span class="download-pdf-size"><%= curDocument.getPdfSize(locale) %></span>
                </div>
            </div>
        </div>
    </div>
<%
    }
} else {
%>
<div class="text-center pt-5">
    <h4>No Record Found.</h4>
</div>
<%
    }
%>
