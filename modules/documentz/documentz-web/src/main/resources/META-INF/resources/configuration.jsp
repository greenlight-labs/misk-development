<%@ include file="/init.jsp"%>

<%
    String redirect = ParamUtil.getString(request, "redirect");
%>

<liferay-portlet:actionURL portletConfiguration="<%=true%>" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="<%=true%>" var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>" method="post" name="fm">
    <aui:input name="<%=Constants.CMD%>" type="hidden" value="<%=Constants.UPDATE%>" />

    <aui:input name="redirect" type="hidden" value="<%=configurationRenderURL%>" />

    <div class="portlet-configuration-body-content">
        <div class="container-fluid-1280">
            <aui:fieldset-group markupView="lexicon">
                <aui:fieldset>
                    <aui:input label="Section Title" name="preferences--sectionTitle--" value="<%= sectionTitle %>" type="textarea" />
                    <aui:input label="Button Label" name="preferences--buttonLabel--" value="<%= buttonLabel %>" type="text" />
                    <aui:input label="Button Link" name="preferences--buttonLink--" value="<%= buttonLink %>" type="text" />
                </aui:fieldset>
            </aui:fieldset-group>
        </div>
    </div>

    <aui:button-row>
        <aui:button cssClass="btn-lg" type="submit" />
        <aui:button cssClass="btn-lg" href="<%= redirect %>" type="cancel" />
    </aui:button-row>
</aui:form>