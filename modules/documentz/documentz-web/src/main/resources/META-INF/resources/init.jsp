<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="documentz.service.DocumentServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.util.List" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="documentz.model.Document" %>
<%@ page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil" %>
<%@ page import="java.text.Format" %>
<%@ page import="com.liferay.portal.kernel.util.Constants" %>
<%@ page import="javax.portlet.PortletPreferences" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    int limit = 9;
    int start = ParamUtil.getInteger(request, "start");
    int end = ParamUtil.getInteger(request, "end");
    end = end == 0 ? limit : end;

    Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM dd, yyyy", locale, timeZone);

    PortletPreferences preference = renderRequest.getPreferences();
    String sectionTitle = preference.getValue("sectionTitle", "Misk Documents");
    String buttonLabel = preference.getValue("buttonLabel", "VIEW ALL DOCS");
    String buttonLink = preference.getValue("buttonLink", "/en/documents");
%>