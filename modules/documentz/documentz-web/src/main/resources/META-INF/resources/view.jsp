<%@ include file="init.jsp" %>

<section class="latest-news-section documents-news-sec" data-scroll-section>
	<div class="container">
		<div class="row">

			<%@include file="listing.jsp" %>

			<div class="documents-pagination">
				<%@include file="includes/pagination.jsp" %>
			</div>
		</div>
	</div>
</section>