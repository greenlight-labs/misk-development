package documentz.web.portlet.action;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import org.osgi.service.component.annotations.Component;
import documentz.web.constants.DocumentzWebPortletKeys;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + DocumentzWebPortletKeys.LATESTDOCUMENTSSLIDER
        },
        service = ConfigurationAction.class
)
public class DocumentzConfigurationAction extends DefaultConfigurationAction {
}
