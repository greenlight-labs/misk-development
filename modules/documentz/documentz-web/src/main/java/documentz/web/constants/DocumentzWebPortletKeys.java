package documentz.web.constants;

/**
 * @author tz
 */
public class DocumentzWebPortletKeys {

	public static final String DOCUMENTZWEB =
		"documentz_web_DocumentzWebPortlet";

	public static final String LATESTDOCUMENTSSLIDER =
			"documentz_web_LatestDocumentsSliderPortlet";

}