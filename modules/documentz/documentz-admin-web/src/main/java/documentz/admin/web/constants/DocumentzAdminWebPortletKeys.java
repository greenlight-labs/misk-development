package documentz.admin.web.constants;

/**
 * @author tz
 */
public class DocumentzAdminWebPortletKeys {

	public static final String DOCUMENTZADMINWEB =
		"documentz_admin_web_DocumentzAdminWebPortlet";

}