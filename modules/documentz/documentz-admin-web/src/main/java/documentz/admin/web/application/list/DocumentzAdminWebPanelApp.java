package documentz.admin.web.application.list;

import documentz.admin.web.constants.DocumentzAdminWebPanelCategoryKeys;
import documentz.admin.web.constants.DocumentzAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + DocumentzAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class DocumentzAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return DocumentzAdminWebPortletKeys.DOCUMENTZADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + DocumentzAdminWebPortletKeys.DOCUMENTZADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}