package documentz.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.file.criterion.FileItemSelectorCriterion;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import documentz.admin.web.constants.DocumentzAdminWebPortletKeys;
import documentz.model.Document;
import documentz.service.DocumentLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=DocumentzAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + DocumentzAdminWebPortletKeys.DOCUMENTZADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class DocumentzAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());
		// image upload
		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();
		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);
		// pdf upload
		FileItemSelectorCriterion fileItemSelectorCriterion = new FileItemSelectorCriterion();
		fileItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		/*List<ItemSelectorCriterion> itemSelectorCriteria = new ArrayList<>();
		itemSelectorCriteria.add(imageItemSelectorCriterion);
		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", itemSelectorCriteria.toArray(new ItemSelectorCriterion[0]));*/

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		PortletURL pdfItemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectPDFDocumentLibrary", fileItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);
		renderRequest.setAttribute("pdfItemSelectorURL", pdfItemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addDocument(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Document.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
		String image = ParamUtil.getString(request, "image");
		Date publishedDate = ParamUtil.getDate(request, "publishedDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale()), null);
		Map<Locale, String> pdfFileMap = LocalizationUtil.getLocalizationMap(request, "pdfFile");
		Map<Locale, String> pdfSizeMap = LocalizationUtil.getLocalizationMap(request, "pdfSize");

		try {
			_documentLocalService.addDocument(serviceContext.getUserId(),
					titleMap, image, publishedDate, pdfFileMap, pdfSizeMap,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(DocumentzAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_document.jsp");
		}
	}

	public void updateDocument(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Document.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long documentId = ParamUtil.getLong(request, "documentId");

		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
		String image = ParamUtil.getString(request, "image");
		Date publishedDate = ParamUtil.getDate(request, "publishedDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale()), null);
		Map<Locale, String> pdfFileMap = LocalizationUtil.getLocalizationMap(request, "pdfFile");
		Map<Locale, String> pdfSizeMap = LocalizationUtil.getLocalizationMap(request, "pdfSize");

		try {
			_documentLocalService.updateDocument(serviceContext.getUserId(), documentId,
					titleMap, image, publishedDate, pdfFileMap, pdfSizeMap,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(DocumentzAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_document.jsp");
		}
	}

	public void deleteDocument(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Document.class.getName(), request);

		long documentId = ParamUtil.getLong(request, "documentId");

		try {
			_documentLocalService.deleteDocument(documentId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(DocumentzAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private DocumentLocalService _documentLocalService;

	@Reference
	private ItemSelector _itemSelector;
}