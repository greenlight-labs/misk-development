<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="documents-admin-buttons">
        <portlet:renderURL var="addDocumentURL">
            <portlet:param name="mvcPath"
                           value="/edit_document.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addDocumentURL.toString() %>"
                    value="Add Document"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= DocumentLocalServiceUtil.getDocumentzCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= DocumentLocalServiceUtil.getDocumentz(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="documentz.model.Document" modelVar="document">

            <liferay-ui:search-container-column-text name="Title" value="<%= HtmlUtil.escape(document.getTitle(locale)) %>"/>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>