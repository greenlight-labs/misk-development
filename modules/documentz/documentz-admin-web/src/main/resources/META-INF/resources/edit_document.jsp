<%@include file = "init.jsp" %>

<%
    long documentId = ParamUtil.getLong(request, "documentId");

    Document document = null;

    if (documentId > 0) {
        try {
            document = DocumentLocalServiceUtil.getDocument(documentId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= document == null ? "addDocument" : "updateDocument" %>' var="editDocumentURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editDocumentURL %>" name="fm">

    <aui:model-context bean="<%= document %>" model="<%= Document.class %>" />

    <aui:input type="hidden" name="documentId"
               value='<%= document == null ? "" : document.getDocumentId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:input name="title" label="Title" helpMessage="Max 20 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="image" label="Image" helpMessage="Image Dimensions: 329 x 377 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="image">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="publishedDate" label="Published Date">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="pdfFile" label="PDF File" helpMessage="Please upload PDF file.">
                <aui:validator name="required"/>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-pdf-button btn-secondary" type="button" data-field-id="pdfFile">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="pdfSize" label="PDF Size">
                <aui:validator name="required"/>
            </aui:input>
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }

    const pdfButton = window.document.querySelector( '.select-pdf-button' );
    pdfButton.addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectPDFDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select PDF" />',
                    url: '${pdfItemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
</aui:script>