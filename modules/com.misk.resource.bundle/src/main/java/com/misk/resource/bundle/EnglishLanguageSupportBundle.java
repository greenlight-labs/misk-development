package com.misk.resource.bundle;

import com.liferay.portal.kernel.language.UTF8Control;
import org.osgi.service.component.annotations.Component;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

@Component(
		immediate = true,
		property = { "language.id=en_US" },
		service = ResourceBundle.class
)
public class EnglishLanguageSupportBundle extends ResourceBundle {
	@Override
	public Locale getLocale() {
		return new Locale("en", "US");
	}

	@Override
	protected Object handleGetObject(String key) {
		return _resourceBundle.getObject(key);
	}

	@Override
	public Enumeration<String> getKeys() {
		return _resourceBundle.getKeys();
	}

	private final ResourceBundle _resourceBundle = ResourceBundle.getBundle(
            "content.Language", getLocale(), getClass().getClassLoader(), UTF8Control.INSTANCE);
}