create index IX_DFD12649 on misk_favorite (itemId, userId);
create index IX_92CE37F0 on misk_favorite (typeId, userId);
create index IX_4EEAA72 on misk_favorite (userId, typeId, itemId);
create unique index IX_C41A6203 on misk_favorite (uuid_[$COLUMN_LENGTH:75$], groupId);

create unique index IX_943209A1 on misk_type (uuid_[$COLUMN_LENGTH:75$], groupId);