create table misk_favorite (
	uuid_ VARCHAR(75) null,
	favoriteId LONG not null primary key,
	groupId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	itemId LONG,
	typeId LONG
);

create table misk_type (
	uuid_ VARCHAR(75) null,
	typeId LONG not null primary key,
	groupId LONG,
	name VARCHAR(75) null
);