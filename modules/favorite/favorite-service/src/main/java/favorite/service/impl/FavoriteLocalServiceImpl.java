/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package favorite.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import favorite.exception.NoSuchFavoriteException;
import favorite.model.Favorite;
import favorite.service.base.FavoriteLocalServiceBaseImpl;

/**
 * The implementation of the favorite local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>favorite.service.FavoriteLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FavoriteLocalServiceBaseImpl
 */
@Component(property = "model.class.name=favorite.model.Favorite", service = AopService.class)
public class FavoriteLocalServiceImpl extends FavoriteLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>favorite.service.FavoriteLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>favorite.service.FavoriteLocalServiceUtil</code>.
	 * 
	 */

	public List<Favorite> getFavorites(long typeId, long userId) throws PortalException {

		return favoritePersistence.findByTypeIdAndUserId(typeId, userId);
	}

	public List<Favorite> getUserfavorites(long userId) throws PortalException {

		return favoritePersistence.findByUserFavorites(userId);
	}

	public Favorite fetchByItemIdAndUserId(long itemId, long userId) {

		return favoritePersistence.fetchByItemIdAndUserId(itemId, userId);
	}

	public Favorite fetchByU_T_I(long userId, long typeId, long itemId) {

		return favoritePersistence.fetchByU_T_I(userId, typeId, itemId);
	}
}