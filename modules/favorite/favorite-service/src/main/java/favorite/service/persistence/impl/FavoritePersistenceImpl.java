/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package favorite.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import favorite.exception.NoSuchFavoriteException;

import favorite.model.Favorite;
import favorite.model.impl.FavoriteImpl;
import favorite.model.impl.FavoriteModelImpl;

import favorite.service.persistence.FavoritePersistence;
import favorite.service.persistence.FavoriteUtil;
import favorite.service.persistence.impl.constants.FavoritePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the favorite service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = FavoritePersistence.class)
public class FavoritePersistenceImpl
	extends BasePersistenceImpl<Favorite> implements FavoritePersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>FavoriteUtil</code> to access the favorite persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		FavoriteImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the favorites where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching favorites
	 */
	@Override
	public List<Favorite> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	@Override
	public List<Favorite> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	@Override
	public List<Favorite> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Favorite> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	@Override
	public List<Favorite> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Favorite> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Favorite> list = null;

		if (useFinderCache) {
			list = (List<Favorite>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Favorite favorite : list) {
					if (!uuid.equals(favorite.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_FAVORITE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FavoriteModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<Favorite>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByUuid_First(
			String uuid, OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByUuid_First(uuid, orderByComparator);

		if (favorite != null) {
			return favorite;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFavoriteException(sb.toString());
	}

	/**
	 * Returns the first favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByUuid_First(
		String uuid, OrderByComparator<Favorite> orderByComparator) {

		List<Favorite> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByUuid_Last(
			String uuid, OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByUuid_Last(uuid, orderByComparator);

		if (favorite != null) {
			return favorite;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchFavoriteException(sb.toString());
	}

	/**
	 * Returns the last favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByUuid_Last(
		String uuid, OrderByComparator<Favorite> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Favorite> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where uuid = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite[] findByUuid_PrevAndNext(
			long favoriteId, String uuid,
			OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		uuid = Objects.toString(uuid, "");

		Favorite favorite = findByPrimaryKey(favoriteId);

		Session session = null;

		try {
			session = openSession();

			Favorite[] array = new FavoriteImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, favorite, uuid, orderByComparator, true);

			array[1] = favorite;

			array[2] = getByUuid_PrevAndNext(
				session, favorite, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Favorite getByUuid_PrevAndNext(
		Session session, Favorite favorite, String uuid,
		OrderByComparator<Favorite> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_FAVORITE_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FavoriteModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(favorite)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Favorite> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the favorites where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Favorite favorite :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(favorite);
		}
	}

	/**
	 * Returns the number of favorites where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching favorites
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_FAVORITE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"favorite.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(favorite.uuid IS NULL OR favorite.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByUUID_G(String uuid, long groupId)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByUUID_G(uuid, groupId);

		if (favorite == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFavoriteException(sb.toString());
		}

		return favorite;
	}

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof Favorite) {
			Favorite favorite = (Favorite)result;

			if (!Objects.equals(uuid, favorite.getUuid()) ||
				(groupId != favorite.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_FAVORITE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<Favorite> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					Favorite favorite = list.get(0);

					result = favorite;

					cacheResult(favorite);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Favorite)result;
		}
	}

	/**
	 * Removes the favorite where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the favorite that was removed
	 */
	@Override
	public Favorite removeByUUID_G(String uuid, long groupId)
		throws NoSuchFavoriteException {

		Favorite favorite = findByUUID_G(uuid, groupId);

		return remove(favorite);
	}

	/**
	 * Returns the number of favorites where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching favorites
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FAVORITE_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"favorite.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(favorite.uuid IS NULL OR favorite.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"favorite.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByTypeIdAndUserId;
	private FinderPath _finderPathWithoutPaginationFindByTypeIdAndUserId;
	private FinderPath _finderPathCountByTypeIdAndUserId;

	/**
	 * Returns all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @return the matching favorites
	 */
	@Override
	public List<Favorite> findByTypeIdAndUserId(long typeId, long userId) {
		return findByTypeIdAndUserId(
			typeId, userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	@Override
	public List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end) {

		return findByTypeIdAndUserId(typeId, userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	@Override
	public List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator) {

		return findByTypeIdAndUserId(
			typeId, userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	@Override
	public List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByTypeIdAndUserId;
				finderArgs = new Object[] {typeId, userId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByTypeIdAndUserId;
			finderArgs = new Object[] {
				typeId, userId, start, end, orderByComparator
			};
		}

		List<Favorite> list = null;

		if (useFinderCache) {
			list = (List<Favorite>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Favorite favorite : list) {
					if ((typeId != favorite.getTypeId()) ||
						(userId != favorite.getUserId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_TYPEIDANDUSERID_TYPEID_2);

			sb.append(_FINDER_COLUMN_TYPEIDANDUSERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FavoriteModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(typeId);

				queryPos.add(userId);

				list = (List<Favorite>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByTypeIdAndUserId_First(
			long typeId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByTypeIdAndUserId_First(
			typeId, userId, orderByComparator);

		if (favorite != null) {
			return favorite;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("typeId=");
		sb.append(typeId);

		sb.append(", userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchFavoriteException(sb.toString());
	}

	/**
	 * Returns the first favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByTypeIdAndUserId_First(
		long typeId, long userId,
		OrderByComparator<Favorite> orderByComparator) {

		List<Favorite> list = findByTypeIdAndUserId(
			typeId, userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByTypeIdAndUserId_Last(
			long typeId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByTypeIdAndUserId_Last(
			typeId, userId, orderByComparator);

		if (favorite != null) {
			return favorite;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("typeId=");
		sb.append(typeId);

		sb.append(", userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchFavoriteException(sb.toString());
	}

	/**
	 * Returns the last favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByTypeIdAndUserId_Last(
		long typeId, long userId,
		OrderByComparator<Favorite> orderByComparator) {

		int count = countByTypeIdAndUserId(typeId, userId);

		if (count == 0) {
			return null;
		}

		List<Favorite> list = findByTypeIdAndUserId(
			typeId, userId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite[] findByTypeIdAndUserId_PrevAndNext(
			long favoriteId, long typeId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = findByPrimaryKey(favoriteId);

		Session session = null;

		try {
			session = openSession();

			Favorite[] array = new FavoriteImpl[3];

			array[0] = getByTypeIdAndUserId_PrevAndNext(
				session, favorite, typeId, userId, orderByComparator, true);

			array[1] = favorite;

			array[2] = getByTypeIdAndUserId_PrevAndNext(
				session, favorite, typeId, userId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Favorite getByTypeIdAndUserId_PrevAndNext(
		Session session, Favorite favorite, long typeId, long userId,
		OrderByComparator<Favorite> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_FAVORITE_WHERE);

		sb.append(_FINDER_COLUMN_TYPEIDANDUSERID_TYPEID_2);

		sb.append(_FINDER_COLUMN_TYPEIDANDUSERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FavoriteModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(typeId);

		queryPos.add(userId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(favorite)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Favorite> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the favorites where typeId = &#63; and userId = &#63; from the database.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 */
	@Override
	public void removeByTypeIdAndUserId(long typeId, long userId) {
		for (Favorite favorite :
				findByTypeIdAndUserId(
					typeId, userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(favorite);
		}
	}

	/**
	 * Returns the number of favorites where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	@Override
	public int countByTypeIdAndUserId(long typeId, long userId) {
		FinderPath finderPath = _finderPathCountByTypeIdAndUserId;

		Object[] finderArgs = new Object[] {typeId, userId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_TYPEIDANDUSERID_TYPEID_2);

			sb.append(_FINDER_COLUMN_TYPEIDANDUSERID_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(typeId);

				queryPos.add(userId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TYPEIDANDUSERID_TYPEID_2 =
		"favorite.typeId = ? AND ";

	private static final String _FINDER_COLUMN_TYPEIDANDUSERID_USERID_2 =
		"favorite.userId = ?";

	private FinderPath _finderPathFetchByItemIdAndUserId;
	private FinderPath _finderPathCountByItemIdAndUserId;

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByItemIdAndUserId(long itemId, long userId)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByItemIdAndUserId(itemId, userId);

		if (favorite == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("itemId=");
			sb.append(itemId);

			sb.append(", userId=");
			sb.append(userId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFavoriteException(sb.toString());
		}

		return favorite;
	}

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByItemIdAndUserId(long itemId, long userId) {
		return fetchByItemIdAndUserId(itemId, userId, true);
	}

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByItemIdAndUserId(
		long itemId, long userId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {itemId, userId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByItemIdAndUserId, finderArgs, this);
		}

		if (result instanceof Favorite) {
			Favorite favorite = (Favorite)result;

			if ((itemId != favorite.getItemId()) ||
				(userId != favorite.getUserId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_ITEMIDANDUSERID_ITEMID_2);

			sb.append(_FINDER_COLUMN_ITEMIDANDUSERID_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(itemId);

				queryPos.add(userId);

				List<Favorite> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByItemIdAndUserId, finderArgs,
							list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {itemId, userId};
							}

							_log.warn(
								"FavoritePersistenceImpl.fetchByItemIdAndUserId(long, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Favorite favorite = list.get(0);

					result = favorite;

					cacheResult(favorite);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Favorite)result;
		}
	}

	/**
	 * Removes the favorite where itemId = &#63; and userId = &#63; from the database.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the favorite that was removed
	 */
	@Override
	public Favorite removeByItemIdAndUserId(long itemId, long userId)
		throws NoSuchFavoriteException {

		Favorite favorite = findByItemIdAndUserId(itemId, userId);

		return remove(favorite);
	}

	/**
	 * Returns the number of favorites where itemId = &#63; and userId = &#63;.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	@Override
	public int countByItemIdAndUserId(long itemId, long userId) {
		FinderPath finderPath = _finderPathCountByItemIdAndUserId;

		Object[] finderArgs = new Object[] {itemId, userId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_ITEMIDANDUSERID_ITEMID_2);

			sb.append(_FINDER_COLUMN_ITEMIDANDUSERID_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(itemId);

				queryPos.add(userId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ITEMIDANDUSERID_ITEMID_2 =
		"favorite.itemId = ? AND ";

	private static final String _FINDER_COLUMN_ITEMIDANDUSERID_USERID_2 =
		"favorite.userId = ?";

	private FinderPath _finderPathFetchByU_T_I;
	private FinderPath _finderPathCountByU_T_I;

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByU_T_I(long userId, long typeId, long itemId)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByU_T_I(userId, typeId, itemId);

		if (favorite == null) {
			StringBundler sb = new StringBundler(8);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("userId=");
			sb.append(userId);

			sb.append(", typeId=");
			sb.append(typeId);

			sb.append(", itemId=");
			sb.append(itemId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchFavoriteException(sb.toString());
		}

		return favorite;
	}

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByU_T_I(long userId, long typeId, long itemId) {
		return fetchByU_T_I(userId, typeId, itemId, true);
	}

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByU_T_I(
		long userId, long typeId, long itemId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {userId, typeId, itemId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByU_T_I, finderArgs, this);
		}

		if (result instanceof Favorite) {
			Favorite favorite = (Favorite)result;

			if ((userId != favorite.getUserId()) ||
				(typeId != favorite.getTypeId()) ||
				(itemId != favorite.getItemId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(5);

			sb.append(_SQL_SELECT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_U_T_I_USERID_2);

			sb.append(_FINDER_COLUMN_U_T_I_TYPEID_2);

			sb.append(_FINDER_COLUMN_U_T_I_ITEMID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				queryPos.add(typeId);

				queryPos.add(itemId);

				List<Favorite> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByU_T_I, finderArgs, list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									userId, typeId, itemId
								};
							}

							_log.warn(
								"FavoritePersistenceImpl.fetchByU_T_I(long, long, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Favorite favorite = list.get(0);

					result = favorite;

					cacheResult(favorite);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Favorite)result;
		}
	}

	/**
	 * Removes the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the favorite that was removed
	 */
	@Override
	public Favorite removeByU_T_I(long userId, long typeId, long itemId)
		throws NoSuchFavoriteException {

		Favorite favorite = findByU_T_I(userId, typeId, itemId);

		return remove(favorite);
	}

	/**
	 * Returns the number of favorites where userId = &#63; and typeId = &#63; and itemId = &#63;.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the number of matching favorites
	 */
	@Override
	public int countByU_T_I(long userId, long typeId, long itemId) {
		FinderPath finderPath = _finderPathCountByU_T_I;

		Object[] finderArgs = new Object[] {userId, typeId, itemId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_COUNT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_U_T_I_USERID_2);

			sb.append(_FINDER_COLUMN_U_T_I_TYPEID_2);

			sb.append(_FINDER_COLUMN_U_T_I_ITEMID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				queryPos.add(typeId);

				queryPos.add(itemId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_U_T_I_USERID_2 =
		"favorite.userId = ? AND ";

	private static final String _FINDER_COLUMN_U_T_I_TYPEID_2 =
		"favorite.typeId = ? AND ";

	private static final String _FINDER_COLUMN_U_T_I_ITEMID_2 =
		"favorite.itemId = ?";

	private FinderPath _finderPathWithPaginationFindByUserFavorites;
	private FinderPath _finderPathWithoutPaginationFindByUserFavorites;
	private FinderPath _finderPathCountByUserFavorites;

	/**
	 * Returns all the favorites where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching favorites
	 */
	@Override
	public List<Favorite> findByUserFavorites(long userId) {
		return findByUserFavorites(
			userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	@Override
	public List<Favorite> findByUserFavorites(long userId, int start, int end) {
		return findByUserFavorites(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	@Override
	public List<Favorite> findByUserFavorites(
		long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator) {

		return findByUserFavorites(userId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	@Override
	public List<Favorite> findByUserFavorites(
		long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUserFavorites;
				finderArgs = new Object[] {userId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUserFavorites;
			finderArgs = new Object[] {userId, start, end, orderByComparator};
		}

		List<Favorite> list = null;

		if (useFinderCache) {
			list = (List<Favorite>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Favorite favorite : list) {
					if (userId != favorite.getUserId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_USERFAVORITES_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(FavoriteModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				list = (List<Favorite>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByUserFavorites_First(
			long userId, OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByUserFavorites_First(
			userId, orderByComparator);

		if (favorite != null) {
			return favorite;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchFavoriteException(sb.toString());
	}

	/**
	 * Returns the first favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByUserFavorites_First(
		long userId, OrderByComparator<Favorite> orderByComparator) {

		List<Favorite> list = findByUserFavorites(
			userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	@Override
	public Favorite findByUserFavorites_Last(
			long userId, OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByUserFavorites_Last(
			userId, orderByComparator);

		if (favorite != null) {
			return favorite;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("userId=");
		sb.append(userId);

		sb.append("}");

		throw new NoSuchFavoriteException(sb.toString());
	}

	/**
	 * Returns the last favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public Favorite fetchByUserFavorites_Last(
		long userId, OrderByComparator<Favorite> orderByComparator) {

		int count = countByUserFavorites(userId);

		if (count == 0) {
			return null;
		}

		List<Favorite> list = findByUserFavorites(
			userId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where userId = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite[] findByUserFavorites_PrevAndNext(
			long favoriteId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws NoSuchFavoriteException {

		Favorite favorite = findByPrimaryKey(favoriteId);

		Session session = null;

		try {
			session = openSession();

			Favorite[] array = new FavoriteImpl[3];

			array[0] = getByUserFavorites_PrevAndNext(
				session, favorite, userId, orderByComparator, true);

			array[1] = favorite;

			array[2] = getByUserFavorites_PrevAndNext(
				session, favorite, userId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Favorite getByUserFavorites_PrevAndNext(
		Session session, Favorite favorite, long userId,
		OrderByComparator<Favorite> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_FAVORITE_WHERE);

		sb.append(_FINDER_COLUMN_USERFAVORITES_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(FavoriteModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(userId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(favorite)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Favorite> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the favorites where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	@Override
	public void removeByUserFavorites(long userId) {
		for (Favorite favorite :
				findByUserFavorites(
					userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(favorite);
		}
	}

	/**
	 * Returns the number of favorites where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	@Override
	public int countByUserFavorites(long userId) {
		FinderPath finderPath = _finderPathCountByUserFavorites;

		Object[] finderArgs = new Object[] {userId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_FAVORITE_WHERE);

			sb.append(_FINDER_COLUMN_USERFAVORITES_USERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(userId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERFAVORITES_USERID_2 =
		"favorite.userId = ?";

	public FavoritePersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(Favorite.class);

		setModelImplClass(FavoriteImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the favorite in the entity cache if it is enabled.
	 *
	 * @param favorite the favorite
	 */
	@Override
	public void cacheResult(Favorite favorite) {
		entityCache.putResult(
			FavoriteImpl.class, favorite.getPrimaryKey(), favorite);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {favorite.getUuid(), favorite.getGroupId()}, favorite);

		finderCache.putResult(
			_finderPathFetchByItemIdAndUserId,
			new Object[] {favorite.getItemId(), favorite.getUserId()},
			favorite);

		finderCache.putResult(
			_finderPathFetchByU_T_I,
			new Object[] {
				favorite.getUserId(), favorite.getTypeId(), favorite.getItemId()
			},
			favorite);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the favorites in the entity cache if it is enabled.
	 *
	 * @param favorites the favorites
	 */
	@Override
	public void cacheResult(List<Favorite> favorites) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (favorites.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (Favorite favorite : favorites) {
			if (entityCache.getResult(
					FavoriteImpl.class, favorite.getPrimaryKey()) == null) {

				cacheResult(favorite);
			}
		}
	}

	/**
	 * Clears the cache for all favorites.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(FavoriteImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the favorite.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Favorite favorite) {
		entityCache.removeResult(FavoriteImpl.class, favorite);
	}

	@Override
	public void clearCache(List<Favorite> favorites) {
		for (Favorite favorite : favorites) {
			entityCache.removeResult(FavoriteImpl.class, favorite);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(FavoriteImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		FavoriteModelImpl favoriteModelImpl) {

		Object[] args = new Object[] {
			favoriteModelImpl.getUuid(), favoriteModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, favoriteModelImpl, false);

		args = new Object[] {
			favoriteModelImpl.getItemId(), favoriteModelImpl.getUserId()
		};

		finderCache.putResult(
			_finderPathCountByItemIdAndUserId, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByItemIdAndUserId, args, favoriteModelImpl, false);

		args = new Object[] {
			favoriteModelImpl.getUserId(), favoriteModelImpl.getTypeId(),
			favoriteModelImpl.getItemId()
		};

		finderCache.putResult(
			_finderPathCountByU_T_I, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByU_T_I, args, favoriteModelImpl, false);
	}

	/**
	 * Creates a new favorite with the primary key. Does not add the favorite to the database.
	 *
	 * @param favoriteId the primary key for the new favorite
	 * @return the new favorite
	 */
	@Override
	public Favorite create(long favoriteId) {
		Favorite favorite = new FavoriteImpl();

		favorite.setNew(true);
		favorite.setPrimaryKey(favoriteId);

		String uuid = PortalUUIDUtil.generate();

		favorite.setUuid(uuid);

		return favorite;
	}

	/**
	 * Removes the favorite with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite that was removed
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite remove(long favoriteId) throws NoSuchFavoriteException {
		return remove((Serializable)favoriteId);
	}

	/**
	 * Removes the favorite with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the favorite
	 * @return the favorite that was removed
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite remove(Serializable primaryKey)
		throws NoSuchFavoriteException {

		Session session = null;

		try {
			session = openSession();

			Favorite favorite = (Favorite)session.get(
				FavoriteImpl.class, primaryKey);

			if (favorite == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFavoriteException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(favorite);
		}
		catch (NoSuchFavoriteException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Favorite removeImpl(Favorite favorite) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(favorite)) {
				favorite = (Favorite)session.get(
					FavoriteImpl.class, favorite.getPrimaryKeyObj());
			}

			if (favorite != null) {
				session.delete(favorite);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (favorite != null) {
			clearCache(favorite);
		}

		return favorite;
	}

	@Override
	public Favorite updateImpl(Favorite favorite) {
		boolean isNew = favorite.isNew();

		if (!(favorite instanceof FavoriteModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(favorite.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(favorite);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in favorite proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Favorite implementation " +
					favorite.getClass());
		}

		FavoriteModelImpl favoriteModelImpl = (FavoriteModelImpl)favorite;

		if (Validator.isNull(favorite.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			favorite.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (favorite.getCreateDate() == null)) {
			if (serviceContext == null) {
				favorite.setCreateDate(date);
			}
			else {
				favorite.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!favoriteModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				favorite.setModifiedDate(date);
			}
			else {
				favorite.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(favorite);
			}
			else {
				favorite = (Favorite)session.merge(favorite);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			FavoriteImpl.class, favoriteModelImpl, false, true);

		cacheUniqueFindersCache(favoriteModelImpl);

		if (isNew) {
			favorite.setNew(false);
		}

		favorite.resetOriginalValues();

		return favorite;
	}

	/**
	 * Returns the favorite with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the favorite
	 * @return the favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite findByPrimaryKey(Serializable primaryKey)
		throws NoSuchFavoriteException {

		Favorite favorite = fetchByPrimaryKey(primaryKey);

		if (favorite == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchFavoriteException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return favorite;
	}

	/**
	 * Returns the favorite with the primary key or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite findByPrimaryKey(long favoriteId)
		throws NoSuchFavoriteException {

		return findByPrimaryKey((Serializable)favoriteId);
	}

	/**
	 * Returns the favorite with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite, or <code>null</code> if a favorite with the primary key could not be found
	 */
	@Override
	public Favorite fetchByPrimaryKey(long favoriteId) {
		return fetchByPrimaryKey((Serializable)favoriteId);
	}

	/**
	 * Returns all the favorites.
	 *
	 * @return the favorites
	 */
	@Override
	public List<Favorite> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of favorites
	 */
	@Override
	public List<Favorite> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of favorites
	 */
	@Override
	public List<Favorite> findAll(
		int start, int end, OrderByComparator<Favorite> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of favorites
	 */
	@Override
	public List<Favorite> findAll(
		int start, int end, OrderByComparator<Favorite> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Favorite> list = null;

		if (useFinderCache) {
			list = (List<Favorite>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_FAVORITE);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_FAVORITE;

				sql = sql.concat(FavoriteModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Favorite>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the favorites from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Favorite favorite : findAll()) {
			remove(favorite);
		}
	}

	/**
	 * Returns the number of favorites.
	 *
	 * @return the number of favorites
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_FAVORITE);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "favoriteId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_FAVORITE;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return FavoriteModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the favorite persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class, new FavoriteModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", Favorite.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByTypeIdAndUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByTypeIdAndUserId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"typeId", "userId"}, true);

		_finderPathWithoutPaginationFindByTypeIdAndUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByTypeIdAndUserId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"typeId", "userId"}, true);

		_finderPathCountByTypeIdAndUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTypeIdAndUserId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"typeId", "userId"}, false);

		_finderPathFetchByItemIdAndUserId = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByItemIdAndUserId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"itemId", "userId"}, true);

		_finderPathCountByItemIdAndUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByItemIdAndUserId",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"itemId", "userId"}, false);

		_finderPathFetchByU_T_I = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByU_T_I",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			new String[] {"userId", "typeId", "itemId"}, true);

		_finderPathCountByU_T_I = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByU_T_I",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			new String[] {"userId", "typeId", "itemId"}, false);

		_finderPathWithPaginationFindByUserFavorites = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserFavorites",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"userId"}, true);

		_finderPathWithoutPaginationFindByUserFavorites = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserFavorites",
			new String[] {Long.class.getName()}, new String[] {"userId"}, true);

		_finderPathCountByUserFavorites = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserFavorites",
			new String[] {Long.class.getName()}, new String[] {"userId"},
			false);

		_setFavoriteUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setFavoriteUtilPersistence(null);

		entityCache.removeCache(FavoriteImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setFavoriteUtilPersistence(
		FavoritePersistence favoritePersistence) {

		try {
			Field field = FavoriteUtil.class.getDeclaredField("_persistence");

			field.setAccessible(true);

			field.set(null, favoritePersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = FavoritePersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = FavoritePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = FavoritePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_FAVORITE =
		"SELECT favorite FROM Favorite favorite";

	private static final String _SQL_SELECT_FAVORITE_WHERE =
		"SELECT favorite FROM Favorite favorite WHERE ";

	private static final String _SQL_COUNT_FAVORITE =
		"SELECT COUNT(favorite) FROM Favorite favorite";

	private static final String _SQL_COUNT_FAVORITE_WHERE =
		"SELECT COUNT(favorite) FROM Favorite favorite WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "favorite.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Favorite exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Favorite exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		FavoritePersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class FavoriteModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			FavoriteModelImpl favoriteModelImpl = (FavoriteModelImpl)baseModel;

			long columnBitmask = favoriteModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(favoriteModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						favoriteModelImpl.getColumnBitmask(columnName);
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(favoriteModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			FavoriteModelImpl favoriteModelImpl, String[] columnNames,
			boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] = favoriteModelImpl.getColumnOriginalValue(
						columnName);
				}
				else {
					arguments[i] = favoriteModelImpl.getColumnValue(columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

	}

}