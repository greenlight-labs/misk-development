/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package favorite.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import favorite.model.Favorite;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Favorite in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FavoriteCacheModel
	implements CacheModel<Favorite>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FavoriteCacheModel)) {
			return false;
		}

		FavoriteCacheModel favoriteCacheModel = (FavoriteCacheModel)object;

		if (favoriteId == favoriteCacheModel.favoriteId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, favoriteId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", favoriteId=");
		sb.append(favoriteId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", itemId=");
		sb.append(itemId);
		sb.append(", typeId=");
		sb.append(typeId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Favorite toEntityModel() {
		FavoriteImpl favoriteImpl = new FavoriteImpl();

		if (uuid == null) {
			favoriteImpl.setUuid("");
		}
		else {
			favoriteImpl.setUuid(uuid);
		}

		favoriteImpl.setFavoriteId(favoriteId);
		favoriteImpl.setGroupId(groupId);
		favoriteImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			favoriteImpl.setCreateDate(null);
		}
		else {
			favoriteImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			favoriteImpl.setModifiedDate(null);
		}
		else {
			favoriteImpl.setModifiedDate(new Date(modifiedDate));
		}

		favoriteImpl.setItemId(itemId);
		favoriteImpl.setTypeId(typeId);

		favoriteImpl.resetOriginalValues();

		return favoriteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		favoriteId = objectInput.readLong();

		groupId = objectInput.readLong();

		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		itemId = objectInput.readLong();

		typeId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(favoriteId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(itemId);

		objectOutput.writeLong(typeId);
	}

	public String uuid;
	public long favoriteId;
	public long groupId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long itemId;
	public long typeId;

}