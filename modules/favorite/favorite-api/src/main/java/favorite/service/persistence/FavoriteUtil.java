/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package favorite.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import favorite.model.Favorite;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the favorite service. This utility wraps <code>favorite.service.persistence.impl.FavoritePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FavoritePersistence
 * @generated
 */
public class FavoriteUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Favorite favorite) {
		getPersistence().clearCache(favorite);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Favorite> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Favorite> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Favorite> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Favorite> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Favorite update(Favorite favorite) {
		return getPersistence().update(favorite);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Favorite update(
		Favorite favorite, ServiceContext serviceContext) {

		return getPersistence().update(favorite, serviceContext);
	}

	/**
	 * Returns all the favorites where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching favorites
	 */
	public static List<Favorite> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	public static List<Favorite> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	public static List<Favorite> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	public static List<Favorite> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Favorite> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByUuid_First(
			String uuid, OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByUuid_First(
		String uuid, OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByUuid_Last(
			String uuid, OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByUuid_Last(
		String uuid, OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where uuid = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public static Favorite[] findByUuid_PrevAndNext(
			long favoriteId, String uuid,
			OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByUuid_PrevAndNext(
			favoriteId, uuid, orderByComparator);
	}

	/**
	 * Removes all the favorites where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of favorites where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching favorites
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByUUID_G(String uuid, long groupId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the favorite where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the favorite that was removed
	 */
	public static Favorite removeByUUID_G(String uuid, long groupId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of favorites where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching favorites
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @return the matching favorites
	 */
	public static List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId) {

		return getPersistence().findByTypeIdAndUserId(typeId, userId);
	}

	/**
	 * Returns a range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	public static List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end) {

		return getPersistence().findByTypeIdAndUserId(
			typeId, userId, start, end);
	}

	/**
	 * Returns an ordered range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	public static List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().findByTypeIdAndUserId(
			typeId, userId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	public static List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByTypeIdAndUserId(
			typeId, userId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByTypeIdAndUserId_First(
			long typeId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByTypeIdAndUserId_First(
			typeId, userId, orderByComparator);
	}

	/**
	 * Returns the first favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByTypeIdAndUserId_First(
		long typeId, long userId,
		OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().fetchByTypeIdAndUserId_First(
			typeId, userId, orderByComparator);
	}

	/**
	 * Returns the last favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByTypeIdAndUserId_Last(
			long typeId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByTypeIdAndUserId_Last(
			typeId, userId, orderByComparator);
	}

	/**
	 * Returns the last favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByTypeIdAndUserId_Last(
		long typeId, long userId,
		OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().fetchByTypeIdAndUserId_Last(
			typeId, userId, orderByComparator);
	}

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public static Favorite[] findByTypeIdAndUserId_PrevAndNext(
			long favoriteId, long typeId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByTypeIdAndUserId_PrevAndNext(
			favoriteId, typeId, userId, orderByComparator);
	}

	/**
	 * Removes all the favorites where typeId = &#63; and userId = &#63; from the database.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 */
	public static void removeByTypeIdAndUserId(long typeId, long userId) {
		getPersistence().removeByTypeIdAndUserId(typeId, userId);
	}

	/**
	 * Returns the number of favorites where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	public static int countByTypeIdAndUserId(long typeId, long userId) {
		return getPersistence().countByTypeIdAndUserId(typeId, userId);
	}

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByItemIdAndUserId(long itemId, long userId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByItemIdAndUserId(itemId, userId);
	}

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByItemIdAndUserId(long itemId, long userId) {
		return getPersistence().fetchByItemIdAndUserId(itemId, userId);
	}

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByItemIdAndUserId(
		long itemId, long userId, boolean useFinderCache) {

		return getPersistence().fetchByItemIdAndUserId(
			itemId, userId, useFinderCache);
	}

	/**
	 * Removes the favorite where itemId = &#63; and userId = &#63; from the database.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the favorite that was removed
	 */
	public static Favorite removeByItemIdAndUserId(long itemId, long userId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().removeByItemIdAndUserId(itemId, userId);
	}

	/**
	 * Returns the number of favorites where itemId = &#63; and userId = &#63;.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	public static int countByItemIdAndUserId(long itemId, long userId) {
		return getPersistence().countByItemIdAndUserId(itemId, userId);
	}

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByU_T_I(long userId, long typeId, long itemId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByU_T_I(userId, typeId, itemId);
	}

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByU_T_I(long userId, long typeId, long itemId) {
		return getPersistence().fetchByU_T_I(userId, typeId, itemId);
	}

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByU_T_I(
		long userId, long typeId, long itemId, boolean useFinderCache) {

		return getPersistence().fetchByU_T_I(
			userId, typeId, itemId, useFinderCache);
	}

	/**
	 * Removes the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the favorite that was removed
	 */
	public static Favorite removeByU_T_I(long userId, long typeId, long itemId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().removeByU_T_I(userId, typeId, itemId);
	}

	/**
	 * Returns the number of favorites where userId = &#63; and typeId = &#63; and itemId = &#63;.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the number of matching favorites
	 */
	public static int countByU_T_I(long userId, long typeId, long itemId) {
		return getPersistence().countByU_T_I(userId, typeId, itemId);
	}

	/**
	 * Returns all the favorites where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching favorites
	 */
	public static List<Favorite> findByUserFavorites(long userId) {
		return getPersistence().findByUserFavorites(userId);
	}

	/**
	 * Returns a range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	public static List<Favorite> findByUserFavorites(
		long userId, int start, int end) {

		return getPersistence().findByUserFavorites(userId, start, end);
	}

	/**
	 * Returns an ordered range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	public static List<Favorite> findByUserFavorites(
		long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().findByUserFavorites(
			userId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	public static List<Favorite> findByUserFavorites(
		long userId, int start, int end,
		OrderByComparator<Favorite> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUserFavorites(
			userId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByUserFavorites_First(
			long userId, OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByUserFavorites_First(
			userId, orderByComparator);
	}

	/**
	 * Returns the first favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByUserFavorites_First(
		long userId, OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().fetchByUserFavorites_First(
			userId, orderByComparator);
	}

	/**
	 * Returns the last favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public static Favorite findByUserFavorites_Last(
			long userId, OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByUserFavorites_Last(
			userId, orderByComparator);
	}

	/**
	 * Returns the last favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public static Favorite fetchByUserFavorites_Last(
		long userId, OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().fetchByUserFavorites_Last(
			userId, orderByComparator);
	}

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where userId = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public static Favorite[] findByUserFavorites_PrevAndNext(
			long favoriteId, long userId,
			OrderByComparator<Favorite> orderByComparator)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByUserFavorites_PrevAndNext(
			favoriteId, userId, orderByComparator);
	}

	/**
	 * Removes all the favorites where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public static void removeByUserFavorites(long userId) {
		getPersistence().removeByUserFavorites(userId);
	}

	/**
	 * Returns the number of favorites where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	public static int countByUserFavorites(long userId) {
		return getPersistence().countByUserFavorites(userId);
	}

	/**
	 * Caches the favorite in the entity cache if it is enabled.
	 *
	 * @param favorite the favorite
	 */
	public static void cacheResult(Favorite favorite) {
		getPersistence().cacheResult(favorite);
	}

	/**
	 * Caches the favorites in the entity cache if it is enabled.
	 *
	 * @param favorites the favorites
	 */
	public static void cacheResult(List<Favorite> favorites) {
		getPersistence().cacheResult(favorites);
	}

	/**
	 * Creates a new favorite with the primary key. Does not add the favorite to the database.
	 *
	 * @param favoriteId the primary key for the new favorite
	 * @return the new favorite
	 */
	public static Favorite create(long favoriteId) {
		return getPersistence().create(favoriteId);
	}

	/**
	 * Removes the favorite with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite that was removed
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public static Favorite remove(long favoriteId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().remove(favoriteId);
	}

	public static Favorite updateImpl(Favorite favorite) {
		return getPersistence().updateImpl(favorite);
	}

	/**
	 * Returns the favorite with the primary key or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public static Favorite findByPrimaryKey(long favoriteId)
		throws favorite.exception.NoSuchFavoriteException {

		return getPersistence().findByPrimaryKey(favoriteId);
	}

	/**
	 * Returns the favorite with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite, or <code>null</code> if a favorite with the primary key could not be found
	 */
	public static Favorite fetchByPrimaryKey(long favoriteId) {
		return getPersistence().fetchByPrimaryKey(favoriteId);
	}

	/**
	 * Returns all the favorites.
	 *
	 * @return the favorites
	 */
	public static List<Favorite> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of favorites
	 */
	public static List<Favorite> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of favorites
	 */
	public static List<Favorite> findAll(
		int start, int end, OrderByComparator<Favorite> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of favorites
	 */
	public static List<Favorite> findAll(
		int start, int end, OrderByComparator<Favorite> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the favorites from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of favorites.
	 *
	 * @return the number of favorites
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static FavoritePersistence getPersistence() {
		return _persistence;
	}

	private static volatile FavoritePersistence _persistence;

}