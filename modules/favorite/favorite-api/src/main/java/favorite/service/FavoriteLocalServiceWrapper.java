/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package favorite.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FavoriteLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see FavoriteLocalService
 * @generated
 */
public class FavoriteLocalServiceWrapper
	implements FavoriteLocalService, ServiceWrapper<FavoriteLocalService> {

	public FavoriteLocalServiceWrapper(
		FavoriteLocalService favoriteLocalService) {

		_favoriteLocalService = favoriteLocalService;
	}

	/**
	 * Adds the favorite to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FavoriteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param favorite the favorite
	 * @return the favorite that was added
	 */
	@Override
	public favorite.model.Favorite addFavorite(
		favorite.model.Favorite favorite) {

		return _favoriteLocalService.addFavorite(favorite);
	}

	/**
	 * Creates a new favorite with the primary key. Does not add the favorite to the database.
	 *
	 * @param favoriteId the primary key for the new favorite
	 * @return the new favorite
	 */
	@Override
	public favorite.model.Favorite createFavorite(long favoriteId) {
		return _favoriteLocalService.createFavorite(favoriteId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the favorite from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FavoriteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param favorite the favorite
	 * @return the favorite that was removed
	 */
	@Override
	public favorite.model.Favorite deleteFavorite(
		favorite.model.Favorite favorite) {

		return _favoriteLocalService.deleteFavorite(favorite);
	}

	/**
	 * Deletes the favorite with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FavoriteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite that was removed
	 * @throws PortalException if a favorite with the primary key could not be found
	 */
	@Override
	public favorite.model.Favorite deleteFavorite(long favoriteId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.deleteFavorite(favoriteId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _favoriteLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _favoriteLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>favorite.model.impl.FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _favoriteLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>favorite.model.impl.FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _favoriteLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _favoriteLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _favoriteLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public favorite.model.Favorite fetchByItemIdAndUserId(
		long itemId, long userId) {

		return _favoriteLocalService.fetchByItemIdAndUserId(itemId, userId);
	}

	@Override
	public favorite.model.Favorite fetchByU_T_I(
		long userId, long typeId, long itemId) {

		return _favoriteLocalService.fetchByU_T_I(userId, typeId, itemId);
	}

	@Override
	public favorite.model.Favorite fetchFavorite(long favoriteId) {
		return _favoriteLocalService.fetchFavorite(favoriteId);
	}

	/**
	 * Returns the favorite matching the UUID and group.
	 *
	 * @param uuid the favorite's UUID
	 * @param groupId the primary key of the group
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	@Override
	public favorite.model.Favorite fetchFavoriteByUuidAndGroupId(
		String uuid, long groupId) {

		return _favoriteLocalService.fetchFavoriteByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _favoriteLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the favorite with the primary key.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite
	 * @throws PortalException if a favorite with the primary key could not be found
	 */
	@Override
	public favorite.model.Favorite getFavorite(long favoriteId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.getFavorite(favoriteId);
	}

	/**
	 * Returns the favorite matching the UUID and group.
	 *
	 * @param uuid the favorite's UUID
	 * @param groupId the primary key of the group
	 * @return the matching favorite
	 * @throws PortalException if a matching favorite could not be found
	 */
	@Override
	public favorite.model.Favorite getFavoriteByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.getFavoriteByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>favorite.model.impl.FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of favorites
	 */
	@Override
	public java.util.List<favorite.model.Favorite> getFavorites(
		int start, int end) {

		return _favoriteLocalService.getFavorites(start, end);
	}

	@Override
	public java.util.List<favorite.model.Favorite> getFavorites(
			long typeId, long userId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.getFavorites(typeId, userId);
	}

	/**
	 * Returns the number of favorites.
	 *
	 * @return the number of favorites
	 */
	@Override
	public int getFavoritesCount() {
		return _favoriteLocalService.getFavoritesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _favoriteLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _favoriteLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public java.util.List<favorite.model.Favorite> getUserfavorites(long userId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _favoriteLocalService.getUserfavorites(userId);
	}

	/**
	 * Updates the favorite in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FavoriteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param favorite the favorite
	 * @return the favorite that was updated
	 */
	@Override
	public favorite.model.Favorite updateFavorite(
		favorite.model.Favorite favorite) {

		return _favoriteLocalService.updateFavorite(favorite);
	}

	@Override
	public FavoriteLocalService getWrappedService() {
		return _favoriteLocalService;
	}

	@Override
	public void setWrappedService(FavoriteLocalService favoriteLocalService) {
		_favoriteLocalService = favoriteLocalService;
	}

	private FavoriteLocalService _favoriteLocalService;

}