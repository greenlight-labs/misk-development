/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package favorite.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import favorite.exception.NoSuchFavoriteException;

import favorite.model.Favorite;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the favorite service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FavoriteUtil
 * @generated
 */
@ProviderType
public interface FavoritePersistence extends BasePersistence<Favorite> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FavoriteUtil} to access the favorite persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the favorites where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching favorites
	 */
	public java.util.List<Favorite> findByUuid(String uuid);

	/**
	 * Returns a range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	public java.util.List<Favorite> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	public java.util.List<Favorite> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns an ordered range of all the favorites where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	public java.util.List<Favorite> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Returns the first favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns the last favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Returns the last favorite in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where uuid = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public Favorite[] findByUuid_PrevAndNext(
			long favoriteId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Removes all the favorites where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of favorites where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching favorites
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByUUID_G(String uuid, long groupId)
		throws NoSuchFavoriteException;

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the favorite where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the favorite where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the favorite that was removed
	 */
	public Favorite removeByUUID_G(String uuid, long groupId)
		throws NoSuchFavoriteException;

	/**
	 * Returns the number of favorites where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching favorites
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @return the matching favorites
	 */
	public java.util.List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId);

	/**
	 * Returns a range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	public java.util.List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end);

	/**
	 * Returns an ordered range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	public java.util.List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns an ordered range of all the favorites where typeId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	public java.util.List<Favorite> findByTypeIdAndUserId(
		long typeId, long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByTypeIdAndUserId_First(
			long typeId, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Returns the first favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByTypeIdAndUserId_First(
		long typeId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns the last favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByTypeIdAndUserId_Last(
			long typeId, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Returns the last favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByTypeIdAndUserId_Last(
		long typeId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where typeId = &#63; and userId = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public Favorite[] findByTypeIdAndUserId_PrevAndNext(
			long favoriteId, long typeId, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Removes all the favorites where typeId = &#63; and userId = &#63; from the database.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 */
	public void removeByTypeIdAndUserId(long typeId, long userId);

	/**
	 * Returns the number of favorites where typeId = &#63; and userId = &#63;.
	 *
	 * @param typeId the type ID
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	public int countByTypeIdAndUserId(long typeId, long userId);

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByItemIdAndUserId(long itemId, long userId)
		throws NoSuchFavoriteException;

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByItemIdAndUserId(long itemId, long userId);

	/**
	 * Returns the favorite where itemId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByItemIdAndUserId(
		long itemId, long userId, boolean useFinderCache);

	/**
	 * Removes the favorite where itemId = &#63; and userId = &#63; from the database.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the favorite that was removed
	 */
	public Favorite removeByItemIdAndUserId(long itemId, long userId)
		throws NoSuchFavoriteException;

	/**
	 * Returns the number of favorites where itemId = &#63; and userId = &#63;.
	 *
	 * @param itemId the item ID
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	public int countByItemIdAndUserId(long itemId, long userId);

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByU_T_I(long userId, long typeId, long itemId)
		throws NoSuchFavoriteException;

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByU_T_I(long userId, long typeId, long itemId);

	/**
	 * Returns the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByU_T_I(
		long userId, long typeId, long itemId, boolean useFinderCache);

	/**
	 * Removes the favorite where userId = &#63; and typeId = &#63; and itemId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the favorite that was removed
	 */
	public Favorite removeByU_T_I(long userId, long typeId, long itemId)
		throws NoSuchFavoriteException;

	/**
	 * Returns the number of favorites where userId = &#63; and typeId = &#63; and itemId = &#63;.
	 *
	 * @param userId the user ID
	 * @param typeId the type ID
	 * @param itemId the item ID
	 * @return the number of matching favorites
	 */
	public int countByU_T_I(long userId, long typeId, long itemId);

	/**
	 * Returns all the favorites where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching favorites
	 */
	public java.util.List<Favorite> findByUserFavorites(long userId);

	/**
	 * Returns a range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of matching favorites
	 */
	public java.util.List<Favorite> findByUserFavorites(
		long userId, int start, int end);

	/**
	 * Returns an ordered range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching favorites
	 */
	public java.util.List<Favorite> findByUserFavorites(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns an ordered range of all the favorites where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching favorites
	 */
	public java.util.List<Favorite> findByUserFavorites(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByUserFavorites_First(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Returns the first favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByUserFavorites_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns the last favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite
	 * @throws NoSuchFavoriteException if a matching favorite could not be found
	 */
	public Favorite findByUserFavorites_Last(
			long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Returns the last favorite in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching favorite, or <code>null</code> if a matching favorite could not be found
	 */
	public Favorite fetchByUserFavorites_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns the favorites before and after the current favorite in the ordered set where userId = &#63;.
	 *
	 * @param favoriteId the primary key of the current favorite
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public Favorite[] findByUserFavorites_PrevAndNext(
			long favoriteId, long userId,
			com.liferay.portal.kernel.util.OrderByComparator<Favorite>
				orderByComparator)
		throws NoSuchFavoriteException;

	/**
	 * Removes all the favorites where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 */
	public void removeByUserFavorites(long userId);

	/**
	 * Returns the number of favorites where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching favorites
	 */
	public int countByUserFavorites(long userId);

	/**
	 * Caches the favorite in the entity cache if it is enabled.
	 *
	 * @param favorite the favorite
	 */
	public void cacheResult(Favorite favorite);

	/**
	 * Caches the favorites in the entity cache if it is enabled.
	 *
	 * @param favorites the favorites
	 */
	public void cacheResult(java.util.List<Favorite> favorites);

	/**
	 * Creates a new favorite with the primary key. Does not add the favorite to the database.
	 *
	 * @param favoriteId the primary key for the new favorite
	 * @return the new favorite
	 */
	public Favorite create(long favoriteId);

	/**
	 * Removes the favorite with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite that was removed
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public Favorite remove(long favoriteId) throws NoSuchFavoriteException;

	public Favorite updateImpl(Favorite favorite);

	/**
	 * Returns the favorite with the primary key or throws a <code>NoSuchFavoriteException</code> if it could not be found.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite
	 * @throws NoSuchFavoriteException if a favorite with the primary key could not be found
	 */
	public Favorite findByPrimaryKey(long favoriteId)
		throws NoSuchFavoriteException;

	/**
	 * Returns the favorite with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param favoriteId the primary key of the favorite
	 * @return the favorite, or <code>null</code> if a favorite with the primary key could not be found
	 */
	public Favorite fetchByPrimaryKey(long favoriteId);

	/**
	 * Returns all the favorites.
	 *
	 * @return the favorites
	 */
	public java.util.List<Favorite> findAll();

	/**
	 * Returns a range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @return the range of favorites
	 */
	public java.util.List<Favorite> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of favorites
	 */
	public java.util.List<Favorite> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator);

	/**
	 * Returns an ordered range of all the favorites.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>FavoriteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of favorites
	 * @param end the upper bound of the range of favorites (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of favorites
	 */
	public java.util.List<Favorite> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Favorite>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the favorites from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of favorites.
	 *
	 * @return the number of favorites
	 */
	public int countAll();

}