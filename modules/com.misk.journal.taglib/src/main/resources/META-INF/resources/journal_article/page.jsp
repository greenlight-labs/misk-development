<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */
--%>

<%@ include file="/journal_article/init.jsp" %>

<liferay-util:dynamic-include key="com.liferay.journal.taglib#/journal_article/page.jsp#pre" />

<%
JournalArticleDisplay articleDisplay = (JournalArticleDisplay)request.getAttribute("liferay-journal:journal-article:articleDisplay");
String wrapperCssClass = (String)request.getAttribute("liferay-journal:journal-article:wrapperCssClass");
HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
//String editMode =  ParamUtil.getString(originalRequest, "p_l_mode");
String editMode = originalRequest.getParameter("p_l_mode");
%>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
	<div class="row">
		<div class="col-12">
			<div class="page-editor__collection__block">
				<div class="page-editor__collection-item page-editor__topper">
					<div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title"><%= HtmlUtil.escape(articleDisplay.getTitle()) %></p></div>
				</div>
			</div>
		</div>
	</div>
</div>
<% } else { %>
<div class="journal-content-article <%= Validator.isNotNull(wrapperCssClass) ? wrapperCssClass : StringPool.BLANK %>" data-analytics-asset-id="<%= articleDisplay.getArticleId() %>" data-analytics-asset-title="<%= HtmlUtil.escapeAttribute(articleDisplay.getTitle()) %>" data-analytics-asset-type="web-content">
	<c:if test='<%= GetterUtil.getBoolean((String)request.getAttribute("liferay-journal:journal-article:showTitle")) %>'>
		<%= HtmlUtil.escape(articleDisplay.getTitle()) %>
	</c:if>

	<%= articleDisplay.getContent() %>
</div>
<% } %>

<%
List<AssetTag> assetTags = AssetTagLocalServiceUtil.getTags(JournalArticleDisplay.class.getName(), articleDisplay.getResourcePrimKey());

PortalUtil.setPageKeywords(ListUtil.toString(assetTags, AssetTag.NAME_ACCESSOR), request);
%>

<liferay-util:dynamic-include key="com.liferay.journal.taglib#/journal_article/page.jsp#post" />