<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="teams.service.TeamServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.util.List" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="teams.model.Team" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    int limit = 10;
    int start = ParamUtil.getInteger(request, "start");
    int end = ParamUtil.getInteger(request, "end");
    end = end == 0 ? limit : end;
%>