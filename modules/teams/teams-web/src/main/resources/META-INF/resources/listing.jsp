<div class="row">
<%
    List<Team> teams = TeamServiceUtil.getTeams(scopeGroupId, start, end);
    int teams_size = teams.size();
    if(teams_size > 0){
        int counter = 1;
        for (Team curTeam : teams) {
%>
    <div class="col-md-4 main-divs">
        <div class="employee-info-sec">
            <div class="emp-img">
                <img class="img-fluid" src="<%= curTeam.getListingImage() %>" alt="pic">
            </div>
            <h4><%= curTeam.getName(locale) %></h4>
            <h5><%= curTeam.getDesignation(locale) %></h5>
            <hr class="dashed-line">
            <button class="btn btn-outline-white teams-read-more-btn">
                <span><liferay-ui:message key="teams_web_read_more" /></span><i class="dot-line"></i>
            </button>
            <a href="#teamsModal<%=counter%>" data-fancybox="teamsInfo"></a>
        </div>
        <div id="teamsModal<%=counter%>" class="modal-main-box" >
            <div class="container-fluid">
                <div class="row" <%=themeDisplay.getLanguageId().equals("ar_SA") ? "dir='rtl'" : "" %>>
                    <div class="col-md-5 pl-0">
                        <div class="modal-img">
                            <img class="img-fluid" src="<%= curTeam.getPopupImage() %>" alt="pic">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="modal-content">
                            <h3><%= curTeam.getName(locale) %></h3>
                            <h4><%= curTeam.getDesignation(locale) %></h4>
                            <h5><%= curTeam.getJoiningDate(locale) %></h5>
                            <hr>
                            <%= curTeam.getDescription(locale) %>
                        </div>
                    </div>
                </div>
                <% if(teams_size > 1){ %>
                <div class="row" <%=themeDisplay.getLanguageId().equals("ar_SA") ? "dir='rtl'" : "" %>>
                    <div class="col-md-6 left-content-box">
                        <div class="modal-bottom-left-content">
                            <% if(counter > 1){ %>
                                <h4><%= teams.get(counter-2).getName(locale) %></h4>
                                <h6><%= teams.get(counter-2).getDesignation(locale) %></h6>
                                <a data-fancybox-prev href="javascript:" class="btn btn-outline-white" style="color: #da1884">
                                    <span><liferay-ui:message key="teams_web_previous" /></span><i class="dot-line"></i>
                                </a>
                            <% } %>
                        </div>
                    </div>
                    <div class="col-md-6 left-content-box">
                        <div class="modal-bottom-right-content">
                            <% if(counter < limit && counter < teams_size){ %>
                                <h4><%= teams.get(counter).getName(locale) %></h4>
                                <h6><%= teams.get(counter).getDesignation(locale) %></h6>
                                <a data-fancybox-next href="javascript:" class="btn btn-outline-white" style="color: #da1884">
                                    <span><liferay-ui:message key="teams_web_next" /></span><i class="dot-line"></i>
                                </a>
                            <% } %>
                        </div>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
    <% if(counter == 2){ %>
        </div>
        <div class="teams-blue-shadow-img animate" data-animation="fadeInRight" data-duration="700"><img src="/assets/svgs/blue-shape.svg" alt="" class="img-fluid"></div>
        <div class="row">
    <% } %>
    <% if(counter == 5 || counter == 7){ %>
        </div>
        <div class="row">
    <% } %>
<%
        counter++;
    }
%>
</div>
<%
} else {
%>
<div class="text-center pt-5">
    <h4>No Record Found.</h4>
</div>
<%
    }
%>

<script type="text/javascript">
    window.addEventListener('load', function() {
        // open fancybox on button click
        $('.teams-read-more-btn').on('click', function (event) {
            event.preventDefault();
            $(this).next().trigger('click');
            return false;
        });
    });
</script>