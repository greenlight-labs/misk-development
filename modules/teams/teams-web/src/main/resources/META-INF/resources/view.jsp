<%@ include file="init.jsp" %>

<section class="teams-info-section" data-scroll-section>
	<div class="container-fluid">

		<%@include file="listing.jsp" %>

		<div class="teams-pagination">
			<%@include file="includes/pagination.jsp" %>
		</div>
	</div>
</section>
