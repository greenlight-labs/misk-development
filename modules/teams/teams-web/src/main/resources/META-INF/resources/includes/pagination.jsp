<%
    int totalRecords = TeamServiceUtil.getTeamsCount(scopeGroupId);
    if(totalRecords > limit){
%>
<div class="row">
    <div class="col-12 text-center" >
        <div class="pagination-container">
            <nav aria-label="Pagination">
                <ul class="pagination animate" data-animation="fadeInUp" data-duration="300">
                    <%
                        int pages = (totalRecords+limit-1)/limit;
                        for (int i = 0; i < pages; i++) {
                            int pStart = limit * i;
                            int pEnd = limit * (i+1);

                            PortletURL paginationPageURL = renderResponse.createRenderURL();

                            paginationPageURL.setParameter("mvcPath", "/view.jsp");
                            paginationPageURL.setParameter("start", String.valueOf(pStart));
                            paginationPageURL.setParameter("end", String.valueOf(pEnd));

                            int pageNo = i+1;
                            int currentPage = end/limit;
                            String className = (currentPage == pageNo ? "active" : "");
                            String leftArrowClass = (currentPage == 1 ? "disabled" : "");
                            String rightArrowClass = (currentPage == pages ? "disabled" : "");

                            if(i == 0){
                            %>
                            <li class="page-item left-arrow-list <%=leftArrowClass%>">
                                <a class="page-link" href="<%= paginationPageURL.toString() %>"><i class="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "icon-right-arrow" : "icon-left-arrow" %>"></i></a>
                            </li>
                            <%
                                }
                            %>
                            <li class="page-item <%=className%>"><a class="page-link" href="<%= paginationPageURL.toString() %>"><%=pageNo%></a></li>
                            <%
                                if(pageNo == pages){
                            %>
                            <li class="page-item right-arrow-list <%=rightArrowClass%>">
                                <a class="page-link" href="<%= paginationPageURL.toString() %>"><i class="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "icon-left-arrow" : "icon-right-arrow" %>"></i></a>
                            </li>
                            <%
                                }
                            %>
                    <%
                        }
                    %>
                </ul>
            </nav>
        </div>
    </div>
</div>
<%
    }
%>
