package teams.admin.web.constants;

/**
 * @author tz
 */
public class TeamsAdminWebPortletKeys {

	public static final String TEAMSADMINWEB =
		"teams_admin_web_TeamsAdminWebPortlet";

}