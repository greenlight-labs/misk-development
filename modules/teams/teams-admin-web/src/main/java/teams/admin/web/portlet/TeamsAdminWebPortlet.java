package teams.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import teams.admin.web.constants.TeamsAdminWebPortletKeys;
import teams.model.Team;
import teams.service.TeamLocalService;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=TeamsAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + TeamsAdminWebPortletKeys.TEAMSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class TeamsAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addTeam(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Team.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		String listingImage = ParamUtil.getString(request, "listingImage");
		String popupImage = ParamUtil.getString(request, "popupImage");
		Map<Locale, String> nameMap = LocalizationUtil.getLocalizationMap(request, "name");
		Map<Locale, String> designationMap = LocalizationUtil.getLocalizationMap(request, "designation");
		Map<Locale, String> joiningDateMap = LocalizationUtil.getLocalizationMap(request, "joiningDate");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(request, "description");
		int position = ParamUtil.getInteger(request, "position");

		try {
			_teamLocalService.addTeam(serviceContext.getUserId(),
					listingImage, popupImage, nameMap, designationMap, joiningDateMap, descriptionMap, position,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(TeamsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_team.jsp");
		}
	}

	public void updateTeam(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Team.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long teamId = ParamUtil.getLong(request, "teamId");

		String listingImage = ParamUtil.getString(request, "listingImage");
		String popupImage = ParamUtil.getString(request, "popupImage");
		Map<Locale, String> nameMap = LocalizationUtil.getLocalizationMap(request, "name");
		Map<Locale, String> designationMap = LocalizationUtil.getLocalizationMap(request, "designation");
		Map<Locale, String> joiningDateMap = LocalizationUtil.getLocalizationMap(request, "joiningDate");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(request, "description");
		int position = ParamUtil.getInteger(request, "position");

		try {
			_teamLocalService.updateTeam(serviceContext.getUserId(), teamId,
					listingImage, popupImage, nameMap, designationMap, joiningDateMap, descriptionMap, position,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(TeamsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_team.jsp");
		}
	}

	public void deleteTeam(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Team.class.getName(), request);

		long teamId = ParamUtil.getLong(request, "teamId");

		try {
			_teamLocalService.deleteTeam(teamId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(TeamsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private TeamLocalService _teamLocalService;

	@Reference
	private ItemSelector _itemSelector;
}