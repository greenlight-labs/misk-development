package teams.admin.web.application.list;

import teams.admin.web.constants.TeamsAdminWebPanelCategoryKeys;
import teams.admin.web.constants.TeamsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + TeamsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class TeamsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return TeamsAdminWebPortletKeys.TEAMSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + TeamsAdminWebPortletKeys.TEAMSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}