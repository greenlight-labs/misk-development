<%@include file="init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Team team = (Team) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="teamId"
                       value="<%=String.valueOf(team.getTeamId()) %>" />
        <portlet:param name="mvcPath"
                       value="/edit_team.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteTeam" var="deleteURL">
        <portlet:param name="teamId"
                       value="<%= String.valueOf(team.getTeamId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>