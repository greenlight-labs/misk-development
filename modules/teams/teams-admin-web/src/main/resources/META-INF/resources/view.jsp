<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="teams-admin-buttons">
        <portlet:renderURL var="addTeamURL">
            <portlet:param name="mvcPath"
                           value="/edit_team.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addTeamURL.toString() %>"
                    value="Add Team"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= TeamLocalServiceUtil.getTeamsCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= TeamLocalServiceUtil.getTeams(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="teams.model.Team" modelVar="team">

            <liferay-ui:search-container-column-text name="Name" value="<%= HtmlUtil.escape(team.getName(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Designation" value="<%= HtmlUtil.escape(team.getDesignation(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Joining Date" value="<%= HtmlUtil.escape(team.getJoiningDate(locale)) %>"/>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>