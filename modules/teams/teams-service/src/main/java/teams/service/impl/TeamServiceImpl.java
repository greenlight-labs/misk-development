/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package teams.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import org.osgi.service.component.annotations.Component;
import teams.model.Team;
import teams.service.base.TeamServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the team remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>teams.service.TeamService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TeamServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=team",
		"json.web.service.context.path=Team"
	},
	service = AopService.class
)
public class TeamServiceImpl extends TeamServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>teams.service.TeamServiceUtil</code> to access the team remote service.
	 */

	public List<Team> getTeams(long groupId) {

		return teamPersistence.findByGroupId(groupId);
	}

	public List<Team> getTeams(long groupId, int start, int end,
							   OrderByComparator<Team> obc) {

		return teamPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Team> getTeams(long groupId, int start, int end) {

		return teamPersistence.findByGroupId(groupId, start, end);
	}

	public int getTeamsCount(long groupId) {

		return teamPersistence.countByGroupId(groupId);
	}
}