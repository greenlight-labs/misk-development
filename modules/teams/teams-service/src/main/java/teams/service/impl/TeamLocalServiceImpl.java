/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package teams.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.exception.TeamNameException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import teams.exception.TeamDescriptionException;
import teams.exception.TeamDesignationException;
import teams.exception.TeamListingImageException;
import teams.exception.TeamPopupImageException;
import teams.model.Team;
import teams.service.base.TeamLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the team local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>teams.service.TeamLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TeamLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=teams.model.Team", service = AopService.class
)
public class TeamLocalServiceImpl extends TeamLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>teams.service.TeamLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>teams.service.TeamLocalServiceUtil</code>.
	 */

	public Team addTeam(long userId,
						String listingImage, String popupImage, Map<Locale, String> nameMap, Map<Locale, String> designationMap,
						Map<Locale, String> joiningDateMap, Map<Locale, String> descriptionMap, int position,
						ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(listingImage, popupImage, nameMap, designationMap, descriptionMap);

		long teamId = counterLocalService.increment();

		Team team = teamPersistence.create(teamId);

		team.setUuid(serviceContext.getUuid());
		team.setUserId(userId);
		team.setGroupId(groupId);
		team.setCompanyId(user.getCompanyId());
		team.setUserName(user.getFullName());
		team.setCreateDate(serviceContext.getCreateDate(now));
		team.setModifiedDate(serviceContext.getModifiedDate(now));

		team.setListingImage(listingImage);
		team.setPopupImage(popupImage);
		team.setNameMap(nameMap);
		team.setDesignationMap(designationMap);
		team.setJoiningDateMap(joiningDateMap);
		team.setDescriptionMap(descriptionMap);
		team.setPosition(position);

		team.setExpandoBridgeAttributes(serviceContext);

		teamPersistence.update(team);
		teamPersistence.clearCache();

		return team;
	}

	public Team updateTeam(long userId, long teamId,
						   String listingImage, String popupImage, Map<Locale, String> nameMap, Map<Locale, String> designationMap,
						   Map<Locale, String> joiningDateMap, Map<Locale, String> descriptionMap, int position,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(listingImage, popupImage, nameMap, designationMap, descriptionMap);

		Team team = getTeam(teamId);

		User user = userLocalService.getUser(userId);

		team.setUserId(userId);
		team.setUserName(user.getFullName());
		team.setModifiedDate(serviceContext.getModifiedDate(now));

		team.setListingImage(listingImage);
		team.setPopupImage(popupImage);
		team.setNameMap(nameMap);
		team.setDesignationMap(designationMap);
		team.setJoiningDateMap(joiningDateMap);
		team.setDescriptionMap(descriptionMap);
		team.setPosition(position);

		team.setExpandoBridgeAttributes(serviceContext);

		teamPersistence.update(team);
		teamPersistence.clearCache();

		return team;
	}

	public Team deleteTeam(long teamId,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Team team = getTeam(teamId);
		team = deleteTeam(team);

		return team;
	}

	public List<Team> getTeams(long groupId) {

		return teamPersistence.findByGroupId(groupId);
	}

	public List<Team> getTeams(long groupId, int start, int end,
							   OrderByComparator<Team> obc) {

		return teamPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Team> getTeams(long groupId, int start, int end) {

		return teamPersistence.findByGroupId(groupId, start, end);
	}

	public int getTeamsCount(long groupId) {

		return teamPersistence.countByGroupId(groupId);
	}

	protected void validate(
			String listingImage, String popupImage, Map<Locale, String> nameMap,
			Map<Locale, String> designationMap, Map<Locale, String> descriptionMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		if (Validator.isNull(listingImage)) {
			throw new TeamListingImageException();
		}

		if (Validator.isNull(popupImage)) {
			throw new TeamPopupImageException();
		}

		String name = nameMap.get(locale);

		if (Validator.isNull(name)) {
			throw new TeamNameException();
		}

		String designation = designationMap.get(locale);

		if (Validator.isNull(designation)) {
			throw new TeamDesignationException();
		}

		String description = descriptionMap.get(locale);

		if (Validator.isNull(description)) {
			throw new TeamDescriptionException();
		}

	}

}