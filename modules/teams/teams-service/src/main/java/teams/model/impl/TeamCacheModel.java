/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package teams.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import teams.model.Team;

/**
 * The cache model class for representing Team in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class TeamCacheModel implements CacheModel<Team>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof TeamCacheModel)) {
			return false;
		}

		TeamCacheModel teamCacheModel = (TeamCacheModel)object;

		if (teamId == teamCacheModel.teamId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, teamId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", teamId=");
		sb.append(teamId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", popupImage=");
		sb.append(popupImage);
		sb.append(", name=");
		sb.append(name);
		sb.append(", designation=");
		sb.append(designation);
		sb.append(", joiningDate=");
		sb.append(joiningDate);
		sb.append(", description=");
		sb.append(description);
		sb.append(", position=");
		sb.append(position);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Team toEntityModel() {
		TeamImpl teamImpl = new TeamImpl();

		if (uuid == null) {
			teamImpl.setUuid("");
		}
		else {
			teamImpl.setUuid(uuid);
		}

		teamImpl.setTeamId(teamId);
		teamImpl.setGroupId(groupId);
		teamImpl.setCompanyId(companyId);
		teamImpl.setUserId(userId);

		if (userName == null) {
			teamImpl.setUserName("");
		}
		else {
			teamImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			teamImpl.setCreateDate(null);
		}
		else {
			teamImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			teamImpl.setModifiedDate(null);
		}
		else {
			teamImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (listingImage == null) {
			teamImpl.setListingImage("");
		}
		else {
			teamImpl.setListingImage(listingImage);
		}

		if (popupImage == null) {
			teamImpl.setPopupImage("");
		}
		else {
			teamImpl.setPopupImage(popupImage);
		}

		if (name == null) {
			teamImpl.setName("");
		}
		else {
			teamImpl.setName(name);
		}

		if (designation == null) {
			teamImpl.setDesignation("");
		}
		else {
			teamImpl.setDesignation(designation);
		}

		if (joiningDate == null) {
			teamImpl.setJoiningDate("");
		}
		else {
			teamImpl.setJoiningDate(joiningDate);
		}

		if (description == null) {
			teamImpl.setDescription("");
		}
		else {
			teamImpl.setDescription(description);
		}

		teamImpl.setPosition(position);

		teamImpl.resetOriginalValues();

		return teamImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		teamId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		listingImage = objectInput.readUTF();
		popupImage = objectInput.readUTF();
		name = objectInput.readUTF();
		designation = objectInput.readUTF();
		joiningDate = objectInput.readUTF();
		description = (String)objectInput.readObject();

		position = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(teamId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (popupImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(popupImage);
		}

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (designation == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(designation);
		}

		if (joiningDate == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(joiningDate);
		}

		if (description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(description);
		}

		objectOutput.writeInt(position);
	}

	public String uuid;
	public long teamId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String listingImage;
	public String popupImage;
	public String name;
	public String designation;
	public String joiningDate;
	public String description;
	public int position;

}