create index IX_8AFE7C19 on misk_teams (groupId);
create index IX_7E116765 on misk_teams (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E7268327 on misk_teams (uuid_[$COLUMN_LENGTH:75$], groupId);