/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package teams.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Team}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Team
 * @generated
 */
public class TeamWrapper
	extends BaseModelWrapper<Team> implements ModelWrapper<Team>, Team {

	public TeamWrapper(Team team) {
		super(team);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("teamId", getTeamId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("listingImage", getListingImage());
		attributes.put("popupImage", getPopupImage());
		attributes.put("name", getName());
		attributes.put("designation", getDesignation());
		attributes.put("joiningDate", getJoiningDate());
		attributes.put("description", getDescription());
		attributes.put("position", getPosition());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long teamId = (Long)attributes.get("teamId");

		if (teamId != null) {
			setTeamId(teamId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String listingImage = (String)attributes.get("listingImage");

		if (listingImage != null) {
			setListingImage(listingImage);
		}

		String popupImage = (String)attributes.get("popupImage");

		if (popupImage != null) {
			setPopupImage(popupImage);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String designation = (String)attributes.get("designation");

		if (designation != null) {
			setDesignation(designation);
		}

		String joiningDate = (String)attributes.get("joiningDate");

		if (joiningDate != null) {
			setJoiningDate(joiningDate);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Integer position = (Integer)attributes.get("position");

		if (position != null) {
			setPosition(position);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the company ID of this team.
	 *
	 * @return the company ID of this team
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this team.
	 *
	 * @return the create date of this team
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this team.
	 *
	 * @return the description of this team
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this team
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this team. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this team
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this team
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this team.
	 *
	 * @return the locales and localized descriptions of this team
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the designation of this team.
	 *
	 * @return the designation of this team
	 */
	@Override
	public String getDesignation() {
		return model.getDesignation();
	}

	/**
	 * Returns the localized designation of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized designation of this team
	 */
	@Override
	public String getDesignation(java.util.Locale locale) {
		return model.getDesignation(locale);
	}

	/**
	 * Returns the localized designation of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized designation of this team. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDesignation(java.util.Locale locale, boolean useDefault) {
		return model.getDesignation(locale, useDefault);
	}

	/**
	 * Returns the localized designation of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized designation of this team
	 */
	@Override
	public String getDesignation(String languageId) {
		return model.getDesignation(languageId);
	}

	/**
	 * Returns the localized designation of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized designation of this team
	 */
	@Override
	public String getDesignation(String languageId, boolean useDefault) {
		return model.getDesignation(languageId, useDefault);
	}

	@Override
	public String getDesignationCurrentLanguageId() {
		return model.getDesignationCurrentLanguageId();
	}

	@Override
	public String getDesignationCurrentValue() {
		return model.getDesignationCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized designations of this team.
	 *
	 * @return the locales and localized designations of this team
	 */
	@Override
	public Map<java.util.Locale, String> getDesignationMap() {
		return model.getDesignationMap();
	}

	/**
	 * Returns the group ID of this team.
	 *
	 * @return the group ID of this team
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the joining date of this team.
	 *
	 * @return the joining date of this team
	 */
	@Override
	public String getJoiningDate() {
		return model.getJoiningDate();
	}

	/**
	 * Returns the localized joining date of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized joining date of this team
	 */
	@Override
	public String getJoiningDate(java.util.Locale locale) {
		return model.getJoiningDate(locale);
	}

	/**
	 * Returns the localized joining date of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized joining date of this team. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getJoiningDate(java.util.Locale locale, boolean useDefault) {
		return model.getJoiningDate(locale, useDefault);
	}

	/**
	 * Returns the localized joining date of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized joining date of this team
	 */
	@Override
	public String getJoiningDate(String languageId) {
		return model.getJoiningDate(languageId);
	}

	/**
	 * Returns the localized joining date of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized joining date of this team
	 */
	@Override
	public String getJoiningDate(String languageId, boolean useDefault) {
		return model.getJoiningDate(languageId, useDefault);
	}

	@Override
	public String getJoiningDateCurrentLanguageId() {
		return model.getJoiningDateCurrentLanguageId();
	}

	@Override
	public String getJoiningDateCurrentValue() {
		return model.getJoiningDateCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized joining dates of this team.
	 *
	 * @return the locales and localized joining dates of this team
	 */
	@Override
	public Map<java.util.Locale, String> getJoiningDateMap() {
		return model.getJoiningDateMap();
	}

	/**
	 * Returns the listing image of this team.
	 *
	 * @return the listing image of this team
	 */
	@Override
	public String getListingImage() {
		return model.getListingImage();
	}

	/**
	 * Returns the modified date of this team.
	 *
	 * @return the modified date of this team
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this team.
	 *
	 * @return the name of this team
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the localized name of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized name of this team
	 */
	@Override
	public String getName(java.util.Locale locale) {
		return model.getName(locale);
	}

	/**
	 * Returns the localized name of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this team. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getName(java.util.Locale locale, boolean useDefault) {
		return model.getName(locale, useDefault);
	}

	/**
	 * Returns the localized name of this team in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized name of this team
	 */
	@Override
	public String getName(String languageId) {
		return model.getName(languageId);
	}

	/**
	 * Returns the localized name of this team in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this team
	 */
	@Override
	public String getName(String languageId, boolean useDefault) {
		return model.getName(languageId, useDefault);
	}

	@Override
	public String getNameCurrentLanguageId() {
		return model.getNameCurrentLanguageId();
	}

	@Override
	public String getNameCurrentValue() {
		return model.getNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized names of this team.
	 *
	 * @return the locales and localized names of this team
	 */
	@Override
	public Map<java.util.Locale, String> getNameMap() {
		return model.getNameMap();
	}

	/**
	 * Returns the popup image of this team.
	 *
	 * @return the popup image of this team
	 */
	@Override
	public String getPopupImage() {
		return model.getPopupImage();
	}

	/**
	 * Returns the position of this team.
	 *
	 * @return the position of this team
	 */
	@Override
	public int getPosition() {
		return model.getPosition();
	}

	/**
	 * Returns the primary key of this team.
	 *
	 * @return the primary key of this team
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the team ID of this team.
	 *
	 * @return the team ID of this team
	 */
	@Override
	public long getTeamId() {
		return model.getTeamId();
	}

	/**
	 * Returns the user ID of this team.
	 *
	 * @return the user ID of this team
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this team.
	 *
	 * @return the user name of this team
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this team.
	 *
	 * @return the user uuid of this team
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this team.
	 *
	 * @return the uuid of this team
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the company ID of this team.
	 *
	 * @param companyId the company ID of this team
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this team.
	 *
	 * @param createDate the create date of this team
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this team.
	 *
	 * @param description the description of this team
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this team in the language.
	 *
	 * @param description the localized description of this team
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this team in the language, and sets the default locale.
	 *
	 * @param description the localized description of this team
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this team from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this team
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this team from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this team
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the designation of this team.
	 *
	 * @param designation the designation of this team
	 */
	@Override
	public void setDesignation(String designation) {
		model.setDesignation(designation);
	}

	/**
	 * Sets the localized designation of this team in the language.
	 *
	 * @param designation the localized designation of this team
	 * @param locale the locale of the language
	 */
	@Override
	public void setDesignation(String designation, java.util.Locale locale) {
		model.setDesignation(designation, locale);
	}

	/**
	 * Sets the localized designation of this team in the language, and sets the default locale.
	 *
	 * @param designation the localized designation of this team
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDesignation(
		String designation, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDesignation(designation, locale, defaultLocale);
	}

	@Override
	public void setDesignationCurrentLanguageId(String languageId) {
		model.setDesignationCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized designations of this team from the map of locales and localized designations.
	 *
	 * @param designationMap the locales and localized designations of this team
	 */
	@Override
	public void setDesignationMap(
		Map<java.util.Locale, String> designationMap) {

		model.setDesignationMap(designationMap);
	}

	/**
	 * Sets the localized designations of this team from the map of locales and localized designations, and sets the default locale.
	 *
	 * @param designationMap the locales and localized designations of this team
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDesignationMap(
		Map<java.util.Locale, String> designationMap,
		java.util.Locale defaultLocale) {

		model.setDesignationMap(designationMap, defaultLocale);
	}

	/**
	 * Sets the group ID of this team.
	 *
	 * @param groupId the group ID of this team
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the joining date of this team.
	 *
	 * @param joiningDate the joining date of this team
	 */
	@Override
	public void setJoiningDate(String joiningDate) {
		model.setJoiningDate(joiningDate);
	}

	/**
	 * Sets the localized joining date of this team in the language.
	 *
	 * @param joiningDate the localized joining date of this team
	 * @param locale the locale of the language
	 */
	@Override
	public void setJoiningDate(String joiningDate, java.util.Locale locale) {
		model.setJoiningDate(joiningDate, locale);
	}

	/**
	 * Sets the localized joining date of this team in the language, and sets the default locale.
	 *
	 * @param joiningDate the localized joining date of this team
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setJoiningDate(
		String joiningDate, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setJoiningDate(joiningDate, locale, defaultLocale);
	}

	@Override
	public void setJoiningDateCurrentLanguageId(String languageId) {
		model.setJoiningDateCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized joining dates of this team from the map of locales and localized joining dates.
	 *
	 * @param joiningDateMap the locales and localized joining dates of this team
	 */
	@Override
	public void setJoiningDateMap(
		Map<java.util.Locale, String> joiningDateMap) {

		model.setJoiningDateMap(joiningDateMap);
	}

	/**
	 * Sets the localized joining dates of this team from the map of locales and localized joining dates, and sets the default locale.
	 *
	 * @param joiningDateMap the locales and localized joining dates of this team
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setJoiningDateMap(
		Map<java.util.Locale, String> joiningDateMap,
		java.util.Locale defaultLocale) {

		model.setJoiningDateMap(joiningDateMap, defaultLocale);
	}

	/**
	 * Sets the listing image of this team.
	 *
	 * @param listingImage the listing image of this team
	 */
	@Override
	public void setListingImage(String listingImage) {
		model.setListingImage(listingImage);
	}

	/**
	 * Sets the modified date of this team.
	 *
	 * @param modifiedDate the modified date of this team
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this team.
	 *
	 * @param name the name of this team
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the localized name of this team in the language.
	 *
	 * @param name the localized name of this team
	 * @param locale the locale of the language
	 */
	@Override
	public void setName(String name, java.util.Locale locale) {
		model.setName(name, locale);
	}

	/**
	 * Sets the localized name of this team in the language, and sets the default locale.
	 *
	 * @param name the localized name of this team
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setName(
		String name, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setName(name, locale, defaultLocale);
	}

	@Override
	public void setNameCurrentLanguageId(String languageId) {
		model.setNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized names of this team from the map of locales and localized names.
	 *
	 * @param nameMap the locales and localized names of this team
	 */
	@Override
	public void setNameMap(Map<java.util.Locale, String> nameMap) {
		model.setNameMap(nameMap);
	}

	/**
	 * Sets the localized names of this team from the map of locales and localized names, and sets the default locale.
	 *
	 * @param nameMap the locales and localized names of this team
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setNameMap(
		Map<java.util.Locale, String> nameMap, java.util.Locale defaultLocale) {

		model.setNameMap(nameMap, defaultLocale);
	}

	/**
	 * Sets the popup image of this team.
	 *
	 * @param popupImage the popup image of this team
	 */
	@Override
	public void setPopupImage(String popupImage) {
		model.setPopupImage(popupImage);
	}

	/**
	 * Sets the position of this team.
	 *
	 * @param position the position of this team
	 */
	@Override
	public void setPosition(int position) {
		model.setPosition(position);
	}

	/**
	 * Sets the primary key of this team.
	 *
	 * @param primaryKey the primary key of this team
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the team ID of this team.
	 *
	 * @param teamId the team ID of this team
	 */
	@Override
	public void setTeamId(long teamId) {
		model.setTeamId(teamId);
	}

	/**
	 * Sets the user ID of this team.
	 *
	 * @param userId the user ID of this team
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this team.
	 *
	 * @param userName the user name of this team
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this team.
	 *
	 * @param userUuid the user uuid of this team
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this team.
	 *
	 * @param uuid the uuid of this team
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected TeamWrapper wrap(Team team) {
		return new TeamWrapper(team);
	}

}