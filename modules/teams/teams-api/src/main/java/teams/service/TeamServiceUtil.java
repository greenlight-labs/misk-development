/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package teams.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

import teams.model.Team;

/**
 * Provides the remote service utility for Team. This utility wraps
 * <code>teams.service.impl.TeamServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see TeamService
 * @generated
 */
public class TeamServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>teams.service.impl.TeamServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static List<Team> getTeams(long groupId) {
		return getService().getTeams(groupId);
	}

	public static List<Team> getTeams(long groupId, int start, int end) {
		return getService().getTeams(groupId, start, end);
	}

	public static List<Team> getTeams(
		long groupId, int start, int end, OrderByComparator<Team> obc) {

		return getService().getTeams(groupId, start, end, obc);
	}

	public static int getTeamsCount(long groupId) {
		return getService().getTeamsCount(groupId);
	}

	public static TeamService getService() {
		return _service;
	}

	private static volatile TeamService _service;

}