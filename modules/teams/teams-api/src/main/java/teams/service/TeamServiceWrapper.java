/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package teams.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TeamService}.
 *
 * @author Brian Wing Shun Chan
 * @see TeamService
 * @generated
 */
public class TeamServiceWrapper
	implements ServiceWrapper<TeamService>, TeamService {

	public TeamServiceWrapper(TeamService teamService) {
		_teamService = teamService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _teamService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<teams.model.Team> getTeams(long groupId) {
		return _teamService.getTeams(groupId);
	}

	@Override
	public java.util.List<teams.model.Team> getTeams(
		long groupId, int start, int end) {

		return _teamService.getTeams(groupId, start, end);
	}

	@Override
	public java.util.List<teams.model.Team> getTeams(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<teams.model.Team>
			obc) {

		return _teamService.getTeams(groupId, start, end, obc);
	}

	@Override
	public int getTeamsCount(long groupId) {
		return _teamService.getTeamsCount(groupId);
	}

	@Override
	public TeamService getWrappedService() {
		return _teamService;
	}

	@Override
	public void setWrappedService(TeamService teamService) {
		_teamService = teamService;
	}

	private TeamService _teamService;

}