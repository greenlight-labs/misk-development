package forms.admin.web.portlet;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import forms.admin.web.constants.FormsAdminWebPortletKeys;
import forms.exception.ContactUsFormTypeValidateException;
import forms.model.ContactUsFormType;
import forms.service.ContactUsFormTypeLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=ContactUsFormTypeAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/type/view.jsp",
		"javax.portlet.name=" + FormsAdminWebPortletKeys.CONTACTUSFORMTYPEADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class ContactUsFormTypeAdminWebPortlet extends MVCPortlet {

	public void addEntry(ActionRequest request, ActionResponse response)
			throws Exception, ContactUsFormTypeValidateException {

		try {
			long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
			ContactUsFormType entry = _contactUsFormTypeLocalService.getContactUsFormTypeFromRequest(
					primaryKey, request);
			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					ContactUsFormType.class.getName(), request);

			// Add entry
			_contactUsFormTypeLocalService.addEntry(entry, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(ContactUsFormTypeAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/type/edit.jsp");
		}
	}

	public void updateEntry(ActionRequest request, ActionResponse response)
			throws Exception {

		try {
			long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

			ContactUsFormType entry = _contactUsFormTypeLocalService.getContactUsFormTypeFromRequest(
					primaryKey, request);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					ContactUsFormType.class.getName(), request);

			//Update entry
			_contactUsFormTypeLocalService.updateEntry(entry, serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(ContactUsFormTypeAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/type/edit.jsp");
		}
	}

	public void deleteEntry(ActionRequest request, ActionResponse response)
			throws PortalException {

		long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

		try {
			_contactUsFormTypeLocalService.deleteEntry(entryId);
		} catch (PortalException pe) {
			ReflectionUtil.throwException(pe);
		}
	}

	@Reference
	private ContactUsFormTypeLocalService _contactUsFormTypeLocalService;
}