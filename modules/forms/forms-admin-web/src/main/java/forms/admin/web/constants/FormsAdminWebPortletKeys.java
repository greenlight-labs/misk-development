package forms.admin.web.constants;

/**
 * @author tz
 */
public class FormsAdminWebPortletKeys {

	public static final String FORMSADMINWEB =
		"forms_admin_web_FormsAdminWebPortlet";

	public static final String CONTACTUSFORMTYPEADMINWEB =
			"forms_admin_web_ContactUsFormTypeAdminWebPortlet";

}