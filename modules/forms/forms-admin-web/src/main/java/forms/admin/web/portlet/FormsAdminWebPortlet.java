package forms.admin.web.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import forms.admin.web.constants.FormsAdminWebPortletKeys;
import forms.model.Form;
import forms.service.FormLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=FormsAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + FormsAdminWebPortletKeys.FORMSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class FormsAdminWebPortlet extends MVCPortlet {

	public void addForm(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Form.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		String firstName = ParamUtil.getString(request, "firstName");
		String lastName = ParamUtil.getString(request, "lastName");
		String email = ParamUtil.getString(request, "email");
		String phone = ParamUtil.getString(request, "phone");
		String inquiryType = ParamUtil.getString(request, "inquiryType");
		String question = ParamUtil.getString(request, "question");
		String language = ParamUtil.getString(request, "language");

		try {
			_formLocalService.addForm(serviceContext.getUserId(),
					firstName, lastName, email, phone, inquiryType, question, language,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(FormsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_form.jsp");
		}
	}

	public void updateForm(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Form.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long formId = ParamUtil.getLong(request, "formId");

		String firstName = ParamUtil.getString(request, "firstName");
		String lastName = ParamUtil.getString(request, "lastName");
		String email = ParamUtil.getString(request, "email");
		String phone = ParamUtil.getString(request, "phone");
		String inquiryType = ParamUtil.getString(request, "inquiryType");
		String question = ParamUtil.getString(request, "question");
		String language = ParamUtil.getString(request, "language");

		try {
			_formLocalService.updateForm(serviceContext.getUserId(), formId,
					firstName, lastName, email, phone, inquiryType, question, language,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(FormsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_form.jsp");
		}
	}

	public void deleteForm(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Form.class.getName(), request);

		long formId = ParamUtil.getLong(request, "formId");

		try {
			_formLocalService.deleteForm(formId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(FormsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private FormLocalService _formLocalService;
}