package forms.admin.web.application.list;

import forms.admin.web.constants.FormsAdminWebPanelCategoryKeys;
import forms.admin.web.constants.FormsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + FormsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class FormsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return FormsAdminWebPortletKeys.FORMSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + FormsAdminWebPortletKeys.FORMSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}