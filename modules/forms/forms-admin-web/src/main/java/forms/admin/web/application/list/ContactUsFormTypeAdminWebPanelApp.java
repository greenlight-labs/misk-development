package forms.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import forms.admin.web.constants.FormsAdminWebPanelCategoryKeys;
import forms.admin.web.constants.FormsAdminWebPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + FormsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class ContactUsFormTypeAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return FormsAdminWebPortletKeys.CONTACTUSFORMTYPEADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + FormsAdminWebPortletKeys.CONTACTUSFORMTYPEADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}