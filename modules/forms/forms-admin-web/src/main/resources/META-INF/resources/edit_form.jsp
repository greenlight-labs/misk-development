<%@include file = "init.jsp" %>

<%
    long formId = ParamUtil.getLong(request, "formId");

    Form form = null;

    if (formId > 0) {
        try {
            form = FormLocalServiceUtil.getForm(formId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= form == null ? "addForm" : "updateForm" %>' var="editFormURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editFormURL %>" name="fm">

    <aui:model-context bean="<%= form %>" model="<%= Form.class %>" />

    <aui:input type="hidden" name="formId"
               value='<%= form == null ? "" : form.getFormId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:input name="firstName" label="First Name">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="lastName" label="Last Name">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="email" label="Email">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="phone" label="Phone" />
            <aui:input name="inquiryType" label="Inquiry Type">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="question" label="Question">
                <aui:validator name="required"/>
            </aui:input>
            <aui:select name="language" label="Language">
                <aui:option value="English" label="English" />
                <aui:option value="Arabic" label="Arabic" />
                <aui:validator name="required"/>
            </aui:select>
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>