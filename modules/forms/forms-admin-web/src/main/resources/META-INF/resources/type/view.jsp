<%@ page import="forms.service.ContactUsFormTypeLocalServiceUtil" %>
<%@ include file="../init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="forms-admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/type/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= ContactUsFormTypeLocalServiceUtil.countByGroupId(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= ContactUsFormTypeLocalServiceUtil.findByGroupId(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="forms.model.ContactUsFormType" modelVar="entry">

            <liferay-ui:search-container-column-text
                    name="name"
                    value="<%= HtmlUtil.escape(entry.getName(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/type/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>