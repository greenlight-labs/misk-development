<%@include file="init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Form form = (Form) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="formId"
                       value="<%=String.valueOf(form.getFormId()) %>" />
        <portlet:param name="mvcPath"
                       value="/edit_form.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteForm" var="deleteURL">
        <portlet:param name="formId"
                       value="<%= String.valueOf(form.getFormId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon image="delete" message="Delete" url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>