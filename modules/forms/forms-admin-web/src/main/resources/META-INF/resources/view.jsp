<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

	<%--<aui:button-row cssClass="forms-admin-buttons">
		<portlet:renderURL var="addFormURL">
			<portlet:param name="mvcPath"
						   value="/edit_form.jsp"/>
			<portlet:param name="redirect" value="<%= "currentURL" %>"/>
		</portlet:renderURL>

		<aui:button onClick="<%= addFormURL.toString() %>"
					value="Add Form"/>
	</aui:button-row>--%>

	<liferay-ui:search-container total="<%= FormLocalServiceUtil.getFormsCount(scopeGroupId) %>">
		<liferay-ui:search-container-results
				results="<%= FormLocalServiceUtil.getForms(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

		<liferay-ui:search-container-row
				className="forms.model.Form" modelVar="form">

			<liferay-ui:search-container-column-text name="First Name" value="<%= HtmlUtil.escape(form.getFirstName()) %>"/>
			<liferay-ui:search-container-column-text name="Last Name" value="<%= HtmlUtil.escape(form.getLastName()) %>"/>
			<liferay-ui:search-container-column-text name="Email" value="<%= HtmlUtil.escape(form.getEmail()) %>"/>
			<liferay-ui:search-container-column-text name="Phone" value="<%= HtmlUtil.escape(form.getPhone()) %>"/>
			<liferay-ui:search-container-column-text name="Inquiry Type" value="<%= HtmlUtil.escape(form.getInquiryType()) %>"/>
			<liferay-ui:search-container-column-text name="Question" value="<%= HtmlUtil.escape(form.getQuestion()) %>"/>
			<liferay-ui:search-container-column-text name="Submitted Date" value="<%= dateFormat.format(form.getCreateDate()) %>"/>
			<liferay-ui:search-container-column-text name="Language" value="<%= HtmlUtil.escape(form.getLanguage()) %>"/>

			<liferay-ui:search-container-column-jsp
					align="right"
					path="/actions.jsp"/>

		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator/>
	</liferay-ui:search-container>
</div>