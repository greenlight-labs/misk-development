<%@include file = "../init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    ContactUsFormType entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = ContactUsFormTypeLocalServiceUtil.getContactUsFormType(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= entry %>" model="<%= ContactUsFormType.class %>" />

        <aui:input type="hidden" name="resourcePrimKey"
                   value='<%= entry == null ? "" : entry.getTypeId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:input name="name" label="Name" autoSize="true">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="email" label="Email" autoSize="true">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="cc" label="CC" />
                <aui:input name="bcc" label="BCC" />
                <aui:input name="status" label="Status" />
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>