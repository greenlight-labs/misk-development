package feedback.admin.web.constants;

/**
 * @author manis
 */
public class FeedbackAdminWebPortletKeys {

	public static final String FEEDBACKADMINWEB =
		"feedback_admin_web_FeedbackAdminWebPortlet";

}