package feedback.admin.web.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import feedback.admin.web.constants.FeedbackAdminWebPortletKeys;
import forms.model.Feedback;
import forms.service.FeedbackLocalService;
import forms.service.FeedbackService;

/**
 * @author manis
 */
@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true", "com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false", "com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true", "javax.portlet.display-name=FeedbackAdminWeb",
		"javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + FeedbackAdminWebPortletKeys.FEEDBACKADMINWEB,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",

}, service = Portlet.class)
public class FeedbackAdminWebPortlet extends MVCPortlet {

	public void deleteFeedback(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Feedback.class.getName(), request);

		long feedbackId = ParamUtil.getLong(request, "feedbackId");

		try {
			_feedbackService.deleteFeedback(feedbackId);
		} catch (PortalException pe) {

			Logger.getLogger(FeedbackAdminWebPortlet.class.getName()).log(Level.SEVERE, null, pe);
		}
	}

	@Reference
	private FeedbackLocalService _feedbackService;
}