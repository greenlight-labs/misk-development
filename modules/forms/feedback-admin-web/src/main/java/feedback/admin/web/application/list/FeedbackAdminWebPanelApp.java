package feedback.admin.web.application.list;

import feedback.admin.web.constants.FeedbackAdminWebPanelCategoryKeys;
import feedback.admin.web.constants.FeedbackAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author manis
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + FeedbackAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class FeedbackAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return FeedbackAdminWebPortletKeys.FEEDBACKADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + FeedbackAdminWebPortletKeys.FEEDBACKADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}