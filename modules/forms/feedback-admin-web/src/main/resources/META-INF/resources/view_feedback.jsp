<%@include file="init.jsp"%>

<%
	long feedbackId = ParamUtil.getLong(request, "feedbackId");

	Feedback feedback = null;

	if (feedbackId > 0) {
		try {
			feedback = FeedbackLocalServiceUtil.getFeedback(feedbackId);
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
%>

<portlet:renderURL var="viewURL">
	<portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>


<div class="heading-text">
	<h1>Feedback Details</h1>
</div>

<table>
	<tr>
		<th>Full Name</th>
		<td><%=feedback.getFullName()%></td>
	</tr>
	<tr>
		<th>Email Address</th>
		<td><%=feedback.getEmailAddress()%></td>

	</tr>
	<tr>
		<th>Type</th>
		<td><%=feedback.getType()%></td>

	</tr>
	<tr>
		<th>Message</th>
		<td><%=feedback.getMessage()%></td>

	</tr>
	<tr>
		<th>Created Date</th>
		<td><%=dateFormat.format(feedback.getCreateDate())%></td>

	</tr>
</table>
<div class="button-back">
<aui:button-row>
	<aui:button onClick="<%=viewURL%>" type="cancel" value="Back" />
</aui:button-row>
</div>
<style>
table {
	border-collapse: collapse;
	width: 90%;
	margin-left: 2%;
	
}

th, td {
	border: 1px solid;
	padding: 10px;
	text-align: left;
}

.heading-text {
	text-align: center;
	margin-top: 3%;
}
.button-back{
width: 90%;
text-align: right;
}
</style>