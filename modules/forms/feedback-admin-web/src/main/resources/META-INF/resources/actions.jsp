<%@include file="init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Feedback feedback = (Feedback) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="viewURL">
        <portlet:param name="feedbackId"
                       value="<%=String.valueOf(feedback.getFeedbackId()) %>" />
        <portlet:param name="mvcPath"
                       value="/view_feedback.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="view" message="View"
                     url="<%=viewURL.toString() %>" />

    <portlet:actionURL name="deleteFeedback" var="deleteURL">
        <portlet:param name="feedbackId"
                       value="<%= String.valueOf(feedback.getFeedbackId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon image="delete" message="Delete" url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>