<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="forms.service.FeedbackLocalServiceUtil"%>
<%@ page import="forms.model.Feedback"%>
<%@ include file="/init.jsp"%>

<div class="container-fluid-1280">


	<liferay-ui:search-container
		total="<%=FeedbackLocalServiceUtil.getFeedbacksCount()%>">
		<liferay-ui:search-container-results
			results="<%=FeedbackLocalServiceUtil.getFeedbacks(scopeGroupId, searchContainer.getStart(),
						searchContainer.getEnd())%>" />
		<liferay-ui:search-container-row className="forms.model.Feedback"
			modelVar="feedback">
			<liferay-ui:search-container-column-text name="Full Name"
				value="<%=HtmlUtil.escape(feedback.getFullName())%>" />
			<liferay-ui:search-container-column-text name="Email Address"
				value="<%=HtmlUtil.escape(feedback.getEmailAddress())%>" />
			<liferay-ui:search-container-column-text name="Type"
				value="<%=HtmlUtil.escape(feedback.getType())%>" />
			<liferay-ui:search-container-column-text name="Message"
				value="<%=HtmlUtil.escape(feedback.getMessage().length() < 20 ? feedback.getMessage()
							: feedback.getMessage().substring(0, 20)) + "..."%>" />
			<liferay-ui:search-container-column-text name="Submitted Date"
				value="<%=dateFormat.format(feedback.getCreateDate())%>" />
			<liferay-ui:search-container-column-jsp align="right"
				path="/actions.jsp" />

		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator />
	</liferay-ui:search-container>
</div>