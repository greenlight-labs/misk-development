/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FormService}.
 *
 * @author Brian Wing Shun Chan
 * @see FormService
 * @generated
 */
public class FormServiceWrapper
	implements FormService, ServiceWrapper<FormService> {

	public FormServiceWrapper(FormService formService) {
		_formService = formService;
	}

	@Override
	public java.util.List<forms.model.Form> getForms(long groupId) {
		return _formService.getForms(groupId);
	}

	@Override
	public java.util.List<forms.model.Form> getForms(
		long groupId, int start, int end) {

		return _formService.getForms(groupId, start, end);
	}

	@Override
	public java.util.List<forms.model.Form> getForms(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<forms.model.Form>
			obc) {

		return _formService.getForms(groupId, start, end, obc);
	}

	@Override
	public int getFormsCount(long groupId) {
		return _formService.getFormsCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _formService.getOSGiServiceIdentifier();
	}

	@Override
	public FormService getWrappedService() {
		return _formService;
	}

	@Override
	public void setWrappedService(FormService formService) {
		_formService = formService;
	}

	private FormService _formService;

}