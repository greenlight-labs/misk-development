/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ContactUsFormTypeLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ContactUsFormTypeLocalService
 * @generated
 */
public class ContactUsFormTypeLocalServiceWrapper
	implements ContactUsFormTypeLocalService,
			   ServiceWrapper<ContactUsFormTypeLocalService> {

	public ContactUsFormTypeLocalServiceWrapper(
		ContactUsFormTypeLocalService contactUsFormTypeLocalService) {

		_contactUsFormTypeLocalService = contactUsFormTypeLocalService;
	}

	/**
	 * Adds the contact us form type to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param contactUsFormType the contact us form type
	 * @return the contact us form type that was added
	 */
	@Override
	public forms.model.ContactUsFormType addContactUsFormType(
		forms.model.ContactUsFormType contactUsFormType) {

		return _contactUsFormTypeLocalService.addContactUsFormType(
			contactUsFormType);
	}

	@Override
	public forms.model.ContactUsFormType addEntry(
			forms.model.ContactUsFormType orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   forms.exception.ContactUsFormTypeValidateException {

		return _contactUsFormTypeLocalService.addEntry(
			orgEntry, serviceContext);
	}

	@Override
	public int countByGroupId(long groupId) {
		return _contactUsFormTypeLocalService.countByGroupId(groupId);
	}

	/**
	 * Creates a new contact us form type with the primary key. Does not add the contact us form type to the database.
	 *
	 * @param typeId the primary key for the new contact us form type
	 * @return the new contact us form type
	 */
	@Override
	public forms.model.ContactUsFormType createContactUsFormType(long typeId) {
		return _contactUsFormTypeLocalService.createContactUsFormType(typeId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contactUsFormTypeLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the contact us form type from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param contactUsFormType the contact us form type
	 * @return the contact us form type that was removed
	 */
	@Override
	public forms.model.ContactUsFormType deleteContactUsFormType(
		forms.model.ContactUsFormType contactUsFormType) {

		return _contactUsFormTypeLocalService.deleteContactUsFormType(
			contactUsFormType);
	}

	/**
	 * Deletes the contact us form type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type that was removed
	 * @throws PortalException if a contact us form type with the primary key could not be found
	 */
	@Override
	public forms.model.ContactUsFormType deleteContactUsFormType(long typeId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contactUsFormTypeLocalService.deleteContactUsFormType(typeId);
	}

	@Override
	public forms.model.ContactUsFormType deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contactUsFormTypeLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contactUsFormTypeLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _contactUsFormTypeLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _contactUsFormTypeLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>forms.model.impl.ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _contactUsFormTypeLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>forms.model.impl.ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _contactUsFormTypeLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _contactUsFormTypeLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _contactUsFormTypeLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public forms.model.ContactUsFormType fetchContactUsFormType(long typeId) {
		return _contactUsFormTypeLocalService.fetchContactUsFormType(typeId);
	}

	/**
	 * Returns the contact us form type matching the UUID and group.
	 *
	 * @param uuid the contact us form type's UUID
	 * @param groupId the primary key of the group
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	@Override
	public forms.model.ContactUsFormType fetchContactUsFormTypeByUuidAndGroupId(
		String uuid, long groupId) {

		return _contactUsFormTypeLocalService.
			fetchContactUsFormTypeByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<forms.model.ContactUsFormType> findByGroupId(
		long groupId) {

		return _contactUsFormTypeLocalService.findByGroupId(groupId);
	}

	@Override
	public java.util.List<forms.model.ContactUsFormType> findByGroupId(
		long groupId, int start, int end) {

		return _contactUsFormTypeLocalService.findByGroupId(
			groupId, start, end);
	}

	@Override
	public java.util.List<forms.model.ContactUsFormType> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<forms.model.ContactUsFormType> obc) {

		return _contactUsFormTypeLocalService.findByGroupId(
			groupId, start, end, obc);
	}

	@Override
	public forms.model.ContactUsFormType findEmailByInquiryType(
		String inquiryType) {

		return _contactUsFormTypeLocalService.findEmailByInquiryType(
			inquiryType);
	}

	@Override
	public forms.model.ContactUsFormType findEmailByInquiryTypeLike(
		String inquiryType) {

		return _contactUsFormTypeLocalService.findEmailByInquiryTypeLike(
			inquiryType);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _contactUsFormTypeLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the contact us form type with the primary key.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type
	 * @throws PortalException if a contact us form type with the primary key could not be found
	 */
	@Override
	public forms.model.ContactUsFormType getContactUsFormType(long typeId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contactUsFormTypeLocalService.getContactUsFormType(typeId);
	}

	/**
	 * Returns the contact us form type matching the UUID and group.
	 *
	 * @param uuid the contact us form type's UUID
	 * @param groupId the primary key of the group
	 * @return the matching contact us form type
	 * @throws PortalException if a matching contact us form type could not be found
	 */
	@Override
	public forms.model.ContactUsFormType getContactUsFormTypeByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contactUsFormTypeLocalService.
			getContactUsFormTypeByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public forms.model.ContactUsFormType getContactUsFormTypeFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws forms.exception.ContactUsFormTypeValidateException,
			   javax.portlet.PortletException {

		return _contactUsFormTypeLocalService.getContactUsFormTypeFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>forms.model.impl.ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of contact us form types
	 */
	@Override
	public java.util.List<forms.model.ContactUsFormType> getContactUsFormTypes(
		int start, int end) {

		return _contactUsFormTypeLocalService.getContactUsFormTypes(start, end);
	}

	/**
	 * Returns all the contact us form types matching the UUID and company.
	 *
	 * @param uuid the UUID of the contact us form types
	 * @param companyId the primary key of the company
	 * @return the matching contact us form types, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<forms.model.ContactUsFormType>
		getContactUsFormTypesByUuidAndCompanyId(String uuid, long companyId) {

		return _contactUsFormTypeLocalService.
			getContactUsFormTypesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of contact us form types matching the UUID and company.
	 *
	 * @param uuid the UUID of the contact us form types
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching contact us form types, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<forms.model.ContactUsFormType>
		getContactUsFormTypesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<forms.model.ContactUsFormType> orderByComparator) {

		return _contactUsFormTypeLocalService.
			getContactUsFormTypesByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of contact us form types.
	 *
	 * @return the number of contact us form types
	 */
	@Override
	public int getContactUsFormTypesCount() {
		return _contactUsFormTypeLocalService.getContactUsFormTypesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _contactUsFormTypeLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _contactUsFormTypeLocalService.
			getIndexableActionableDynamicQuery();
	}

	@Override
	public forms.model.ContactUsFormType getNewObject(long primaryKey) {
		return _contactUsFormTypeLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _contactUsFormTypeLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _contactUsFormTypeLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the contact us form type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param contactUsFormType the contact us form type
	 * @return the contact us form type that was updated
	 */
	@Override
	public forms.model.ContactUsFormType updateContactUsFormType(
		forms.model.ContactUsFormType contactUsFormType) {

		return _contactUsFormTypeLocalService.updateContactUsFormType(
			contactUsFormType);
	}

	@Override
	public forms.model.ContactUsFormType updateEntry(
			forms.model.ContactUsFormType orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   forms.exception.ContactUsFormTypeValidateException {

		return _contactUsFormTypeLocalService.updateEntry(
			orgEntry, serviceContext);
	}

	@Override
	public ContactUsFormTypeLocalService getWrappedService() {
		return _contactUsFormTypeLocalService;
	}

	@Override
	public void setWrappedService(
		ContactUsFormTypeLocalService contactUsFormTypeLocalService) {

		_contactUsFormTypeLocalService = contactUsFormTypeLocalService;
	}

	private ContactUsFormTypeLocalService _contactUsFormTypeLocalService;

}