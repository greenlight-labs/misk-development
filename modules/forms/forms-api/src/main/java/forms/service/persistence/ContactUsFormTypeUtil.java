/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import forms.model.ContactUsFormType;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the contact us form type service. This utility wraps <code>forms.service.persistence.impl.ContactUsFormTypePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContactUsFormTypePersistence
 * @generated
 */
public class ContactUsFormTypeUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ContactUsFormType contactUsFormType) {
		getPersistence().clearCache(contactUsFormType);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, ContactUsFormType> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ContactUsFormType> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ContactUsFormType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ContactUsFormType> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ContactUsFormType update(
		ContactUsFormType contactUsFormType) {

		return getPersistence().update(contactUsFormType);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ContactUsFormType update(
		ContactUsFormType contactUsFormType, ServiceContext serviceContext) {

		return getPersistence().update(contactUsFormType, serviceContext);
	}

	/**
	 * Returns all the contact us form types where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the contact us form types where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByUuid_First(
			String uuid, OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByUuid_First(
		String uuid, OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByUuid_Last(
			String uuid, OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByUuid_Last(
		String uuid, OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the contact us form types before and after the current contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param typeId the primary key of the current contact us form type
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType[] findByUuid_PrevAndNext(
			long typeId, String uuid,
			OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByUuid_PrevAndNext(
			typeId, uuid, orderByComparator);
	}

	/**
	 * Removes all the contact us form types where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of contact us form types where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching contact us form types
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the contact us form type where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchContactUsFormTypeException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByUUID_G(String uuid, long groupId)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the contact us form type where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the contact us form type where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the contact us form type where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the contact us form type that was removed
	 */
	public static ContactUsFormType removeByUUID_G(String uuid, long groupId)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of contact us form types where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching contact us form types
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the contact us form types before and after the current contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param typeId the primary key of the current contact us form type
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType[] findByUuid_C_PrevAndNext(
			long typeId, String uuid, long companyId,
			OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByUuid_C_PrevAndNext(
			typeId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the contact us form types where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching contact us form types
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the contact us form types where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching contact us form types
	 */
	public static List<ContactUsFormType> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the contact us form types where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the contact us form types where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the contact us form types where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching contact us form types
	 */
	public static List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByGroupId_First(
			long groupId,
			OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByGroupId_First(
		long groupId, OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByGroupId_Last(
			long groupId,
			OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByGroupId_Last(
		long groupId, OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the contact us form types before and after the current contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param typeId the primary key of the current contact us form type
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType[] findByGroupId_PrevAndNext(
			long typeId, long groupId,
			OrderByComparator<ContactUsFormType> orderByComparator)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByGroupId_PrevAndNext(
			typeId, groupId, orderByComparator);
	}

	/**
	 * Removes all the contact us form types where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of contact us form types where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching contact us form types
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns the contact us form type where name = &#63; or throws a <code>NoSuchContactUsFormTypeException</code> if it could not be found.
	 *
	 * @param name the name
	 * @return the matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType findByName(String name)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByName(name);
	}

	/**
	 * Returns the contact us form type where name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param name the name
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByName(String name) {
		return getPersistence().fetchByName(name);
	}

	/**
	 * Returns the contact us form type where name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param name the name
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchByName(
		String name, boolean useFinderCache) {

		return getPersistence().fetchByName(name, useFinderCache);
	}

	/**
	 * Removes the contact us form type where name = &#63; from the database.
	 *
	 * @param name the name
	 * @return the contact us form type that was removed
	 */
	public static ContactUsFormType removeByName(String name)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().removeByName(name);
	}

	/**
	 * Returns the number of contact us form types where name = &#63;.
	 *
	 * @param name the name
	 * @return the number of matching contact us form types
	 */
	public static int countByName(String name) {
		return getPersistence().countByName(name);
	}

	/**
	 * Caches the contact us form type in the entity cache if it is enabled.
	 *
	 * @param contactUsFormType the contact us form type
	 */
	public static void cacheResult(ContactUsFormType contactUsFormType) {
		getPersistence().cacheResult(contactUsFormType);
	}

	/**
	 * Caches the contact us form types in the entity cache if it is enabled.
	 *
	 * @param contactUsFormTypes the contact us form types
	 */
	public static void cacheResult(List<ContactUsFormType> contactUsFormTypes) {
		getPersistence().cacheResult(contactUsFormTypes);
	}

	/**
	 * Creates a new contact us form type with the primary key. Does not add the contact us form type to the database.
	 *
	 * @param typeId the primary key for the new contact us form type
	 * @return the new contact us form type
	 */
	public static ContactUsFormType create(long typeId) {
		return getPersistence().create(typeId);
	}

	/**
	 * Removes the contact us form type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type that was removed
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType remove(long typeId)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().remove(typeId);
	}

	public static ContactUsFormType updateImpl(
		ContactUsFormType contactUsFormType) {

		return getPersistence().updateImpl(contactUsFormType);
	}

	/**
	 * Returns the contact us form type with the primary key or throws a <code>NoSuchContactUsFormTypeException</code> if it could not be found.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType findByPrimaryKey(long typeId)
		throws forms.exception.NoSuchContactUsFormTypeException {

		return getPersistence().findByPrimaryKey(typeId);
	}

	/**
	 * Returns the contact us form type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type, or <code>null</code> if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType fetchByPrimaryKey(long typeId) {
		return getPersistence().fetchByPrimaryKey(typeId);
	}

	/**
	 * Returns all the contact us form types.
	 *
	 * @return the contact us form types
	 */
	public static List<ContactUsFormType> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of contact us form types
	 */
	public static List<ContactUsFormType> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contact us form types
	 */
	public static List<ContactUsFormType> findAll(
		int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of contact us form types
	 */
	public static List<ContactUsFormType> findAll(
		int start, int end,
		OrderByComparator<ContactUsFormType> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the contact us form types from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of contact us form types.
	 *
	 * @return the number of contact us form types
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ContactUsFormTypePersistence getPersistence() {
		return _persistence;
	}

	private static volatile ContactUsFormTypePersistence _persistence;

}