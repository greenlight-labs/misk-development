/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import forms.model.ContactUsFormType;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for ContactUsFormType. This utility wraps
 * <code>forms.service.impl.ContactUsFormTypeLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ContactUsFormTypeLocalService
 * @generated
 */
public class ContactUsFormTypeLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>forms.service.impl.ContactUsFormTypeLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the contact us form type to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param contactUsFormType the contact us form type
	 * @return the contact us form type that was added
	 */
	public static ContactUsFormType addContactUsFormType(
		ContactUsFormType contactUsFormType) {

		return getService().addContactUsFormType(contactUsFormType);
	}

	public static ContactUsFormType addEntry(
			ContactUsFormType orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws forms.exception.ContactUsFormTypeValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static int countByGroupId(long groupId) {
		return getService().countByGroupId(groupId);
	}

	/**
	 * Creates a new contact us form type with the primary key. Does not add the contact us form type to the database.
	 *
	 * @param typeId the primary key for the new contact us form type
	 * @return the new contact us form type
	 */
	public static ContactUsFormType createContactUsFormType(long typeId) {
		return getService().createContactUsFormType(typeId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the contact us form type from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param contactUsFormType the contact us form type
	 * @return the contact us form type that was removed
	 */
	public static ContactUsFormType deleteContactUsFormType(
		ContactUsFormType contactUsFormType) {

		return getService().deleteContactUsFormType(contactUsFormType);
	}

	/**
	 * Deletes the contact us form type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type that was removed
	 * @throws PortalException if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType deleteContactUsFormType(long typeId)
		throws PortalException {

		return getService().deleteContactUsFormType(typeId);
	}

	public static ContactUsFormType deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>forms.model.impl.ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>forms.model.impl.ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ContactUsFormType fetchContactUsFormType(long typeId) {
		return getService().fetchContactUsFormType(typeId);
	}

	/**
	 * Returns the contact us form type matching the UUID and group.
	 *
	 * @param uuid the contact us form type's UUID
	 * @param groupId the primary key of the group
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public static ContactUsFormType fetchContactUsFormTypeByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchContactUsFormTypeByUuidAndGroupId(
			uuid, groupId);
	}

	public static List<ContactUsFormType> findByGroupId(long groupId) {
		return getService().findByGroupId(groupId);
	}

	public static List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end) {

		return getService().findByGroupId(groupId, start, end);
	}

	public static List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<ContactUsFormType> obc) {

		return getService().findByGroupId(groupId, start, end, obc);
	}

	public static ContactUsFormType findEmailByInquiryType(String inquiryType) {
		return getService().findEmailByInquiryType(inquiryType);
	}

	public static ContactUsFormType findEmailByInquiryTypeLike(
		String inquiryType) {

		return getService().findEmailByInquiryTypeLike(inquiryType);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the contact us form type with the primary key.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type
	 * @throws PortalException if a contact us form type with the primary key could not be found
	 */
	public static ContactUsFormType getContactUsFormType(long typeId)
		throws PortalException {

		return getService().getContactUsFormType(typeId);
	}

	/**
	 * Returns the contact us form type matching the UUID and group.
	 *
	 * @param uuid the contact us form type's UUID
	 * @param groupId the primary key of the group
	 * @return the matching contact us form type
	 * @throws PortalException if a matching contact us form type could not be found
	 */
	public static ContactUsFormType getContactUsFormTypeByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getContactUsFormTypeByUuidAndGroupId(uuid, groupId);
	}

	public static ContactUsFormType getContactUsFormTypeFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws forms.exception.ContactUsFormTypeValidateException,
			   javax.portlet.PortletException {

		return getService().getContactUsFormTypeFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>forms.model.impl.ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of contact us form types
	 */
	public static List<ContactUsFormType> getContactUsFormTypes(
		int start, int end) {

		return getService().getContactUsFormTypes(start, end);
	}

	/**
	 * Returns all the contact us form types matching the UUID and company.
	 *
	 * @param uuid the UUID of the contact us form types
	 * @param companyId the primary key of the company
	 * @return the matching contact us form types, or an empty list if no matches were found
	 */
	public static List<ContactUsFormType>
		getContactUsFormTypesByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getContactUsFormTypesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of contact us form types matching the UUID and company.
	 *
	 * @param uuid the UUID of the contact us form types
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching contact us form types, or an empty list if no matches were found
	 */
	public static List<ContactUsFormType>
		getContactUsFormTypesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			OrderByComparator<ContactUsFormType> orderByComparator) {

		return getService().getContactUsFormTypesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of contact us form types.
	 *
	 * @return the number of contact us form types
	 */
	public static int getContactUsFormTypesCount() {
		return getService().getContactUsFormTypesCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static ContactUsFormType getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the contact us form type in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ContactUsFormTypeLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param contactUsFormType the contact us form type
	 * @return the contact us form type that was updated
	 */
	public static ContactUsFormType updateContactUsFormType(
		ContactUsFormType contactUsFormType) {

		return getService().updateContactUsFormType(contactUsFormType);
	}

	public static ContactUsFormType updateEntry(
			ContactUsFormType orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws forms.exception.ContactUsFormTypeValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static ContactUsFormTypeLocalService getService() {
		return _service;
	}

	private static volatile ContactUsFormTypeLocalService _service;

}