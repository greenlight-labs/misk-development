/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import forms.model.Form;

import java.util.List;

/**
 * Provides the remote service utility for Form. This utility wraps
 * <code>forms.service.impl.FormServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see FormService
 * @generated
 */
public class FormServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>forms.service.impl.FormServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static List<Form> getForms(long groupId) {
		return getService().getForms(groupId);
	}

	public static List<Form> getForms(long groupId, int start, int end) {
		return getService().getForms(groupId, start, end);
	}

	public static List<Form> getForms(
		long groupId, int start, int end, OrderByComparator<Form> obc) {

		return getService().getForms(groupId, start, end, obc);
	}

	public static int getFormsCount(long groupId) {
		return getService().getFormsCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static FormService getService() {
		return _service;
	}

	private static volatile FormService _service;

}