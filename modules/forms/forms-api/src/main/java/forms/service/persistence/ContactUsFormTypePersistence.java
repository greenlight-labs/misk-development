/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import forms.exception.NoSuchContactUsFormTypeException;

import forms.model.ContactUsFormType;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the contact us form type service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContactUsFormTypeUtil
 * @generated
 */
@ProviderType
public interface ContactUsFormTypePersistence
	extends BasePersistence<ContactUsFormType> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ContactUsFormTypeUtil} to access the contact us form type persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the contact us form types where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid(String uuid);

	/**
	 * Returns a range of all the contact us form types where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns the contact us form types before and after the current contact us form type in the ordered set where uuid = &#63;.
	 *
	 * @param typeId the primary key of the current contact us form type
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public ContactUsFormType[] findByUuid_PrevAndNext(
			long typeId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Removes all the contact us form types where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of contact us form types where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching contact us form types
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the contact us form type where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchContactUsFormTypeException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByUUID_G(String uuid, long groupId)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the contact us form type where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the contact us form type where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the contact us form type where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the contact us form type that was removed
	 */
	public ContactUsFormType removeByUUID_G(String uuid, long groupId)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the number of contact us form types where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching contact us form types
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns an ordered range of all the contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the first contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the last contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns the contact us form types before and after the current contact us form type in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param typeId the primary key of the current contact us form type
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public ContactUsFormType[] findByUuid_C_PrevAndNext(
			long typeId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Removes all the contact us form types where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of contact us form types where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching contact us form types
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the contact us form types where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByGroupId(long groupId);

	/**
	 * Returns a range of all the contact us form types where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the contact us form types where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns an ordered range of all the contact us form types where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching contact us form types
	 */
	public java.util.List<ContactUsFormType> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the first contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns the last contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the last contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns the contact us form types before and after the current contact us form type in the ordered set where groupId = &#63;.
	 *
	 * @param typeId the primary key of the current contact us form type
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public ContactUsFormType[] findByGroupId_PrevAndNext(
			long typeId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
				orderByComparator)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Removes all the contact us form types where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of contact us form types where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching contact us form types
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns the contact us form type where name = &#63; or throws a <code>NoSuchContactUsFormTypeException</code> if it could not be found.
	 *
	 * @param name the name
	 * @return the matching contact us form type
	 * @throws NoSuchContactUsFormTypeException if a matching contact us form type could not be found
	 */
	public ContactUsFormType findByName(String name)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the contact us form type where name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param name the name
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByName(String name);

	/**
	 * Returns the contact us form type where name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param name the name
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching contact us form type, or <code>null</code> if a matching contact us form type could not be found
	 */
	public ContactUsFormType fetchByName(String name, boolean useFinderCache);

	/**
	 * Removes the contact us form type where name = &#63; from the database.
	 *
	 * @param name the name
	 * @return the contact us form type that was removed
	 */
	public ContactUsFormType removeByName(String name)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the number of contact us form types where name = &#63;.
	 *
	 * @param name the name
	 * @return the number of matching contact us form types
	 */
	public int countByName(String name);

	/**
	 * Caches the contact us form type in the entity cache if it is enabled.
	 *
	 * @param contactUsFormType the contact us form type
	 */
	public void cacheResult(ContactUsFormType contactUsFormType);

	/**
	 * Caches the contact us form types in the entity cache if it is enabled.
	 *
	 * @param contactUsFormTypes the contact us form types
	 */
	public void cacheResult(
		java.util.List<ContactUsFormType> contactUsFormTypes);

	/**
	 * Creates a new contact us form type with the primary key. Does not add the contact us form type to the database.
	 *
	 * @param typeId the primary key for the new contact us form type
	 * @return the new contact us form type
	 */
	public ContactUsFormType create(long typeId);

	/**
	 * Removes the contact us form type with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type that was removed
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public ContactUsFormType remove(long typeId)
		throws NoSuchContactUsFormTypeException;

	public ContactUsFormType updateImpl(ContactUsFormType contactUsFormType);

	/**
	 * Returns the contact us form type with the primary key or throws a <code>NoSuchContactUsFormTypeException</code> if it could not be found.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type
	 * @throws NoSuchContactUsFormTypeException if a contact us form type with the primary key could not be found
	 */
	public ContactUsFormType findByPrimaryKey(long typeId)
		throws NoSuchContactUsFormTypeException;

	/**
	 * Returns the contact us form type with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param typeId the primary key of the contact us form type
	 * @return the contact us form type, or <code>null</code> if a contact us form type with the primary key could not be found
	 */
	public ContactUsFormType fetchByPrimaryKey(long typeId);

	/**
	 * Returns all the contact us form types.
	 *
	 * @return the contact us form types
	 */
	public java.util.List<ContactUsFormType> findAll();

	/**
	 * Returns a range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @return the range of contact us form types
	 */
	public java.util.List<ContactUsFormType> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of contact us form types
	 */
	public java.util.List<ContactUsFormType> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator);

	/**
	 * Returns an ordered range of all the contact us form types.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ContactUsFormTypeModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of contact us form types
	 * @param end the upper bound of the range of contact us form types (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of contact us form types
	 */
	public java.util.List<ContactUsFormType> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactUsFormType>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the contact us form types from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of contact us form types.
	 *
	 * @return the number of contact us form types
	 */
	public int countAll();

}