<%@ page import="com.liferay.portal.kernel.xml.Document" %>
<%@ page import="com.liferay.portal.kernel.xml.SAXReaderUtil" %>
<%@ page import="com.liferay.portal.kernel.xml.Node" %>
<%@ page import="forms.service.util.DocumentMediaUtil" %>
<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@ page import="forms.model.ContactUsFormType" %>
<%@ page import="java.util.List" %>
<%@ page import="forms.service.ContactUsFormTypeLocalServiceUtil" %>
<%@ include file="/init.jsp" %>

<portlet:resourceURL var="submitContactFormURL"/>
<style type="text/css">
    .capt{background-color:grey;width: 300px;height:55px;}
    #mainCaptcha{position: relative;top: 5px;font-size:30px;color:white;<%=themeDisplay.getLanguageId().equals("ar_SA") ? "right : 60px;" : "left : 60px;" %>}
    /*#captchaInput{position: relative;left:40px;bottom: 40px;}*/
</style>

<%
    try{
        //String articleId = "46932";
        String articleId = "WEB_CONTACT_US_FORM";
        JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(scopeGroupId, articleId, 0 );
        String articleContent = article.getContentByLocale(themeDisplay.getLanguageId());
        Document document = SAXReaderUtil.read(articleContent);

        Node titleNode = document.selectSingleNode("/root/dynamic-element[@name='Title']/dynamic-content");
        Node subtitleNode = document.selectSingleNode("/root/dynamic-element[@name='Subtitle']/dynamic-content");
        Node formFirstNamePlaceholderNode = document.selectSingleNode("/root/dynamic-element[@name='FormFirstNamePlaceholder']/dynamic-content");
        Node formLastNamePlaceholderNode = document.selectSingleNode("/root/dynamic-element[@name='FormLastNamePlaceholder']/dynamic-content");
        Node formEmailPlaceholderNode = document.selectSingleNode("/root/dynamic-element[@name='FormEmailPlaceholder']/dynamic-content");
        Node formPhonePlaceholderNode = document.selectSingleNode("/root/dynamic-element[@name='FormPhonePlaceholder']/dynamic-content");
        Node formQuestionPlaceholderNode = document.selectSingleNode("/root/dynamic-element[@name='FormQuestionPlaceholder']/dynamic-content");
        Node buttonLabelNode = document.selectSingleNode("/root/dynamic-element[@name='ButtonLabel']/dynamic-content");
        Node formThanksTitleNode = document.selectSingleNode("/root/dynamic-element[@name='FormThanksTitle']/dynamic-content");
        Node formThanksDescriptionNode = document.selectSingleNode("/root/dynamic-element[@name='FormThanksDescription']/dynamic-content");
        Node imageNode = document.selectSingleNode("/root/dynamic-element[@name='Image2wiz']/dynamic-content");
        String title = titleNode.getText();
        String subtitle = subtitleNode.getText();
        String formFirstNamePlaceholder = formFirstNamePlaceholderNode.getText();
        String formLastNamePlaceholder = formLastNamePlaceholderNode.getText();
        String formEmailPlaceholder = formEmailPlaceholderNode.getText();
        String formPhonePlaceholder = formPhonePlaceholderNode.getText();
        String formQuestionPlaceholder = formQuestionPlaceholderNode.getText();
        String buttonLabel = buttonLabelNode.getText();
        String formThanksTitle = formThanksTitleNode.getText();
        String formThanksDescription = formThanksDescriptionNode.getText();
        DocumentMediaUtil documentMediaUtil = new DocumentMediaUtil();
        String image = documentMediaUtil.getImageURL(imageNode.getText());

        List<ContactUsFormType> contactUsFormTypes = ContactUsFormTypeLocalServiceUtil.findByGroupId(scopeGroupId);
%>

<section class="contact-us-section" data-scroll-section>
    <div class="liveable-right-shape animate" data-animation="fadeInRight" data-duration="700">
        <img src="/assets/svgs/pink-shape.svg" alt="" class="img-fluid"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h3 class="animate" data-animation="fadeInUp" data-duration="400"><%=title%>
                    <br>
                    <span class="text-primary"><%=subtitle%> </span></h3>
                <div class="form-wrapper contact-us-form animate" data-animation="fadeInUp" data-duration="500">
                    <form action="javascript:" id="contactUsForm">
                        <div class="form-group row no-gutters">
                            <div class="col-12 col-md-8 col-lg-9">
                                <input type="text" class="form-control" name="firstName" maxlength="100" placeholder="<%=formFirstNamePlaceholder%>"/>
                            </div>
                        </div>
                        <div class="form-group row no-gutters">
                            <div class="col-12 col-md-8 col-lg-9">
                                <input type="text" class="form-control" name="lastName" maxlength="100" placeholder="<%=formLastNamePlaceholder%>"/>
                            </div>
                        </div>
                        <div class="form-group row no-gutters">
                            <div class="col-12 col-md-8 col-lg-9">
                                <input type="text" class="form-control" maxlength="50" name="email" placeholder="<%=formEmailPlaceholder%>" />
                            </div>
                        </div>
                        <div class="form-group row no-gutters">
                            <div class="col-12">
                                <input type="tel" name="Telephone1" id="phone" placeholder="<%=formPhonePlaceholder%>" maxlength="40" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');"/>
                            </div>
                        </div>
                        <div class="form-group row no-gutters">
                            <div class="col-12">
                                <select name="inquiryType" id="inquiryType" class="selectpicker">
                                    <% for (ContactUsFormType curType : contactUsFormTypes) { %>
                                        <option value="<%=curType.getName(locale)%>"><%=curType.getName(locale)%></option>
                                    <% } %>
                                </select>
                            </div>
                        </div>
                    <div class="form-group row no-gutters">
                        <div class="col-12 col-md-8 col-lg-9">
                            <textarea class="form-control" maxlength="500" name="textarea" placeholder="<%=formQuestionPlaceholder%>"></textarea>
                        </div>
                    </div>
                    <div class="capt">
                        <div type="text" id="mainCaptcha" onclick="Captcha();"></div>
                    </div>
                    <div class="form-group row no-gutters">
                        <div class="col-12 col-md-8 col-lg-9">
                            <input type="text" class="form-control" id="captchaInput" name="captchaInput" data-fv-callback="true"
                                   data-fv-callback-message=" "
                                   data-fv-callback-callback="ValidCaptcha"/>
                        </div>
                    </div>
                    <div class="form-group row no-gutters border-0 mt-sp-7">
                        <div class="col-12">
                            <button type="submit" class="btn btn-outline-primary"><span><%=buttonLabel%></span><i class="dot-line"></i></button>
                        </div>
                    </div>
                </form>
                <div class="thanks">
                    <div class="thanks-inner">
                        <h4 class="mb-1"><%=formThanksTitle%></h4>
                        <p class="mb-0"><%=formThanksDescription%></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="liveable-img text-right animate" data-animation="fadeInRight" data-duration="400">
                <% if(image != null){ %>
                    <img alt="" src="<%=image%>" class="img-fluid" />
                <% } %>
        </div>
    </div>
    </div>
    </div>
</section>

<%
    } catch (PortalException e) {
        e.printStackTrace();
    }
%>

<script type="text/javascript">
    window.addEventListener('load', function() {
        var
            $contactUsForm = $('#contactUsForm'),
            emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            /*
            * Arabic letters: \u0621-\u064A\u066E-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\
            * Arabic Range: \u0600-\u06FF
            * Arabic Supplement: \u0750-\u077F
            * Arabic Extended-A: \u08A0-\u08FF
            * Arabic Presentation Forms-A: 0xFB50–0xFDFF
            * Arabic Presentation Forms-B: 0xFE70–0xFEFF
            * */
            var alphaRegex = /^[\u0621-\u064A\u066E-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFFa-zA-Z\s]+$/;
            var alphaNumericRegex = /^[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFFa-zA-Z0-9\s]+$/;
            var messageRegex = /^[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFFa-zA-Z0-9\s\.\,\-\_\?\!\@\%\&\(\)\:\'\"\=\+\n\r]+$/;
            /*
            * get translation keys from language file
            * */
            var messages = {
                required: '<liferay-ui:message key="forms_web_contact_us_field_required" />',
                email: '<liferay-ui:message key="forms_web_contact_us_field_email" />',
                alpha: '<liferay-ui:message key="forms_web_contact_us_field_alpha" />',
                message: '<liferay-ui:message key="forms_web_contact_us_field_message" />',
            };

        if ($contactUsForm.length > 0) {
            $contactUsForm
                .formValidation({
                    framework: 'bootstrap',
                    excluded: ':disabled',
                    fields: {
                        firstName: {
                            validators: {
                                notEmpty: {
                                    message: ' '
                                },
                                regexp: {
                                    regexp: alphaRegex,
                                    message: messages.alpha
                                }
                            }
                        },
                        lastName: {
                            validators: {
                                notEmpty: {
                                    message: ' '
                                },
                                regexp: {
                                    regexp: alphaRegex,
                                    message: messages.alpha
                                }
                            }
                        },
                        email: {
                            validators: {
                                notEmpty: {
                                    message: ' '
                                }, regexp: {
                                    regexp: emailRegex,
                                    message: messages.email
                                }
                            }
                        },
                        inquiryType: {
                            validators: {
                                notEmpty: {
                                    message: ' '
                                }
                            }
                        },
                        textarea: {
                            validators: {
                                notEmpty: {
                                    message: ' '
                                },
                                regexp: {
                                    regexp: messageRegex,
                                    message: messages.message
                                }
                            }
                        }
                    },
                    onSuccess: function (e) {
                        e.preventDefault();
                        var $form = $(e.target),
                            formId = '#' + $form[0].id;
                        $(formId).addClass('loading').append('<div class="loader"></div>');
                        /***************************/
                        const DOMAIN = Liferay.ThemeDisplay.getPortalURL();
                        $.ajax({
                            type: 'post',
                            url: `${submitContactFormURL}`,
                            data: {
                                <portlet:namespace />firstName: $(formId).find('input[name=firstName]').val(),
                                <portlet:namespace />lastName: $(formId).find('input[name=lastName]').val(),
                                <portlet:namespace />email: $(formId).find('input[name=email]').val(),
                                <portlet:namespace />phone: $(formId).find('input[name="fullPhoneNumber"]').val(),
                                <portlet:namespace />inquiryType: $(formId).find('select[name="inquiryType"] option:selected').val(),
                                <portlet:namespace />question: $(formId).find('textarea[name=textarea]').val(),
                            },
                            success: function (data) {
                                var fields = $(formId).data('formValidation').getOptions().fields,
                                    $parent, $icon;
                                for (var field in fields) {
                                    $parent = $('[name="' + field + '"]').parents('.form-group');
                                }
                                // Then reset the form
                                $('.alert-success').addClass('in');
                                $(formId).data('formValidation').resetForm(true);
                                $("input[type=text], textarea").val("");
                                $('.file-input-name').hide();
                                $('.thanks').show();
                                $(formId).removeClass('loading');
                                $(formId).find('.loader').remove();
                                setTimeout(function () {
                                    $('.thanks').hide();
                                    $(formId).data('formValidation').resetForm(true);
                                    Captcha();
                                }, 5000);
                            },
                        });
                    }
                });
            $contactUsForm
                .find('.filter-option-inner-inner').click(function () {
                console.log("asd");
                $(".selectpicker-2").selectpicker('toggle')
            })
        }
        Captcha();

        if ($('#phone').length > 0) {
            var input = document.querySelector("#phone");
            var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
            var errorMsg = document.querySelector("#error-msg");
            var validMsg = document.querySelector("#valid-msg");

            var iti = window.intlTelInput(input, {
                nationalMode: false,
                separateDialCode: true,
                formatOnDisplay: true,
                initialCountry: "sa",
                //utilsScript: "/assets/scripts/utils.js",
                utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@17.0.3/build/js/utils.js",
                autoPlaceholder: "aggressive",
                container: false,
                hiddenInput: 'fullPhoneNumber'
            });

            var reset = function () {
                input.classList.remove("error");
                //errorMsg.innerHTML = " ";
                //errorMsg.classList.add("hide");
                $('input[name="fullPhoneNumber"]').val(iti._getFullNumber());
            };

// on blur: validate
            input.addEventListener('blur', function () {
                reset();
                if (input.value.trim()) {
                    if (!iti.isValidNumber()) {
                        input.classList.add("error");
                        var errorCode = iti.getValidationError();
                        //errorMsg.innerHTML = errorMap[errorCode];
                        //errorMsg.classList.remove("hide");
                    }
                }
            });

// on keyup / change flag: reset
            input.addEventListener('change', reset);
            input.addEventListener('keyup', reset);
        }

        //*************dropdown issue fixed*****************/
        setTimeout(function () {
            $('.selectpicker').selectpicker('toggle');
            $('section.contact-us-section h3').trigger('click');
        }, 500);

    });

    //******************captcha****************************
    function Captcha(){
        var alpha = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            '0','1','2','3','4','5','6','7','8','9'];
        var i;
        for (i=0;i<6;i++){
            var a = alpha[Math.floor(Math.random() * alpha.length)];
            var b = alpha[Math.floor(Math.random() * alpha.length)];
            var c = alpha[Math.floor(Math.random() * alpha.length)];
            var d = alpha[Math.floor(Math.random() * alpha.length)];
            var e = alpha[Math.floor(Math.random() * alpha.length)];
            var f = alpha[Math.floor(Math.random() * alpha.length)];
            var g = alpha[Math.floor(Math.random() * alpha.length)];
        }
        var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
        document.getElementById("mainCaptcha").innerHTML = code
        document.getElementById("mainCaptcha").value = code
    }
    function ValidCaptcha(value, validator){
        var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
        var string2 = removeSpaces(document.getElementById('captchaInput').value);
        return string1 === string2;
    }
    function removeSpaces(string){
        return string.split(' ').join('');
    }
    //********************************************
</script>