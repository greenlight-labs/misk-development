package forms.web.portlet;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.template.*;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import forms.model.ContactUsFormType;
import forms.model.Form;
import forms.model.impl.FormImpl;
import forms.service.ContactUsFormTypeLocalService;
import forms.service.FormLocalService;
import forms.service.util.ContactUsFormValidator;
import forms.web.constants.FormsWebPortletKeys;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=category.misk",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.instanceable=false",
                "com.liferay.portlet.scopeable=true",
                "javax.portlet.display-name=Forms",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + FormsWebPortletKeys.FORMSWEB,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",
                "javax.portlet.supports.mime-type=text/html"
        },
        service = Portlet.class
)
public class FormsWebPortlet extends MVCPortlet {

    @Override
    public void serveResource(ResourceRequest request, ResourceResponse response)
            throws IOException, PortletException {

        ServiceContext serviceContext = null;
        try {
            serviceContext = ServiceContextFactory.getInstance(
                    Form.class.getName(), request);
        } catch (PortalException e) {
            e.printStackTrace();
        }

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
                WebKeys.THEME_DISPLAY);

        String firstName = ParamUtil.getString(request, "firstName");
        String lastName = ParamUtil.getString(request, "lastName");
        String email = ParamUtil.getString(request, "email");
        String phone = ParamUtil.getString(request, "phone");
        String inquiryType = ParamUtil.getString(request, "inquiryType");
        String question = ParamUtil.getString(request, "question");
        String languageId = themeDisplay.getLanguageId();

        // validate fields using ContactUsFormValidator
        ContactUsFormValidator contactUsFormValidator = new ContactUsFormValidator();
        // create form object and validate
        Form form = new FormImpl();
        form.setFirstName(firstName);
        form.setLastName(lastName);
        form.setEmail(email);
        form.setPhone(phone);
        form.setInquiryType(inquiryType);
        form.setQuestion(question);
        // this language id only for validator, not for saving
        form.setLanguage(languageId);

        // validate and show errors
        try {
            contactUsFormValidator.validate(form);
            List<String> errors = contactUsFormValidator.getErrors();
            if (errors.size() > 0) {
                // send json response with errors
                response.getWriter().write(errors.toString());
                return;
            }
        } catch (PortalException e) {
            throw new RuntimeException(e);
        }


        Date createDate = new Date();
        int the_year = LocalDate.now().getYear();

        HashMap<String, String> configs = new HashMap<>();
        configs.put("portalURL", themeDisplay.getPortalURL());
        configs.put("portletContextPath", themeDisplay.getPortalURL()+"/o/forms.web");
        configs.put("the_year", String.valueOf(the_year));

        String language = "English";

        if (languageId.equals("ar_SA")) {
            language = "Arabic";
        }

        try {
            _formLocalService.addForm(serviceContext.getUserId(),
                    firstName, lastName, email, phone, inquiryType, question, language,
                    serviceContext);
            /* **** send admin/client email ******* */
            sendMailUsingTemplate(firstName, lastName, email, phone, inquiryType, question, createDate, languageId, configs);
            /* ********************** */
        } catch (PortalException pe) {
            Logger.getLogger(FormsWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);
        }

        super.serveResource(request, response);
    }

    public void sendMailUsingTemplate(String firstName, String lastName, String email, String phone, String inquiryType, String question, Date createDate, String languageId, HashMap<String, String> configs) {
        InternetAddress fromAddress = null;
        InternetAddress toAddress = null;
        String adminBody = "";
        String clientBody = "";

		/*String body = ContentUtil.get(this.getClass().getClassLoader().getResource("META-INF/resources/templates/admin-email.ftl"), true);
		body = StringUtil.replace(body, new String[] { "[$NAME$]","[$RESULT$]","[$PERCENTAGE$]","[$EXAM$]" }, new String[] { "Ravi", "CONGRATULATION" ,"80%" , "CCLP"});
		/* ********************************************************/

        try {
            /* admin email */
            TemplateResource templateResource =
                    new URLTemplateResource("0", this.getClass().getClassLoader().getResource("META-INF/resources/templates/admin-email.ftl"));
            Template template = TemplateManagerUtil.getTemplate(
                    TemplateConstants.LANG_TYPE_FTL, templateResource, false);
            // Add the data-models
            template.put("firstName", firstName);
            template.put("lastName", lastName);
            template.put("email", email);
            template.put("phone", phone);
            template.put("inquiryType", inquiryType);
            template.put("question", question);
            template.put("createDate", createDate);
            template.put("portalURL", configs.get("portalURL"));
            template.put("portletContextPath", configs.get("portletContextPath"));
            template.put("the_year", configs.get("the_year"));
            StringWriter out = new StringWriter();
            template.processTemplate(out);
            adminBody = out.toString();
            /* client email */
            String templateFile = "META-INF/resources/templates/client-email-en.ftl";
            if (languageId.equals("ar_SA")) {
                templateFile = "META-INF/resources/templates/client-email-ar.ftl";
            }
            TemplateResource clientTemplateResource =
                    new URLTemplateResource("0", this.getClass().getClassLoader().getResource(templateFile));
            Template clientTemplate = TemplateManagerUtil.getTemplate(
                    TemplateConstants.LANG_TYPE_FTL, clientTemplateResource, false);
            // Add the data-models
            String name = firstName +' '+lastName;
            clientTemplate.put("name", name);
            clientTemplate.put("portalURL", configs.get("portalURL"));
            clientTemplate.put("portletContextPath", configs.get("portletContextPath"));
            clientTemplate.put("the_year", configs.get("the_year"));
            StringWriter clientOut = new StringWriter();
            clientTemplate.processTemplate(clientOut);
            clientBody = clientOut.toString();
        } catch (TemplateException e1) {
            e1.printStackTrace();
        }

        try {
            /* admin email */
            /* *** Get email by inquiryType */
            String adminToEmail = "city@misk.org.sa";
            String clientFromEmail = "misk-city@bcw-global.com";
            String cc = null;
            String bcc = null;
            ContactUsFormType contactUsFormType = _contactUsFormTypeLocalService.findEmailByInquiryTypeLike(inquiryType);
            if(Validator.isNotNull(contactUsFormType)){
                adminToEmail = contactUsFormType.getEmail();
                clientFromEmail = contactUsFormType.getEmail();
                cc = contactUsFormType.getCc();
                bcc = contactUsFormType.getBcc();
            }
            /* ******************************/
            fromAddress = new InternetAddress("misk-city@bcw-global.com");
            toAddress = new InternetAddress(adminToEmail);
            MailMessage mailMessage = new MailMessage();
            mailMessage.setTo(toAddress);
            mailMessage.setFrom(fromAddress);
            if(Validator.isNotNull(cc)){
                InternetAddress ccAddress = new InternetAddress(cc);
                mailMessage.setCC(ccAddress);
            }
            if(Validator.isNotNull(bcc)){
                InternetAddress bccAddress = new InternetAddress(bcc);
                mailMessage.setBCC(bccAddress);
            }
            mailMessage.setSubject("[Misk Foundation] - Contact Us Form");
            mailMessage.setBody(adminBody);
            mailMessage.setHTMLFormat(true);
            MailServiceUtil.sendEmail(mailMessage);
            /* client email */
            fromAddress = new InternetAddress(clientFromEmail);
            toAddress = new InternetAddress(email);
            MailMessage clientMailMessage = new MailMessage();
            clientMailMessage.setTo(toAddress);
            clientMailMessage.setFrom(fromAddress);
            clientMailMessage.setSubject("[Misk Foundation] - Contact Us Form");
            clientMailMessage.setBody(clientBody);
            clientMailMessage.setHTMLFormat(true);
            MailServiceUtil.sendEmail(clientMailMessage);
        } catch (AddressException e) {
            e.printStackTrace();
        }
    }

    @Reference
    private FormLocalService _formLocalService;

    @Reference
    private ContactUsFormTypeLocalService _contactUsFormTypeLocalService;

}