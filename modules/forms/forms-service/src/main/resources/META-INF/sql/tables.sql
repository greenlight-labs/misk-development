create table misk_form_contact_us (
	uuid_ VARCHAR(75) null,
	formId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	email VARCHAR(75) null,
	phone VARCHAR(75) null,
	inquiryType VARCHAR(75) null,
	question STRING null,
	language VARCHAR(75) null
);

create table misk_form_contact_us_type (
	uuid_ VARCHAR(75) null,
	typeId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	email VARCHAR(75) null,
	cc VARCHAR(75) null,
	bcc VARCHAR(75) null,
	status BOOLEAN
);

create table misk_form_feedback (
	uuid_ VARCHAR(75) null,
	feedbackId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	fullName VARCHAR(75) null,
	emailAddress VARCHAR(75) null,
	type_ VARCHAR(75) null,
	message TEXT null
);