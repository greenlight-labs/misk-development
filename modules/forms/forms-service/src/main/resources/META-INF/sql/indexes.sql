create index IX_6CB80D57 on misk_form_contact_us (groupId);
create index IX_3F545BE7 on misk_form_contact_us (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_640B029 on misk_form_contact_us (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_3B610818 on misk_form_contact_us_type (groupId);
create unique index IX_BF4F2311 on misk_form_contact_us_type (name[$COLUMN_LENGTH:75$]);
create index IX_5182EA46 on misk_form_contact_us_type (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_212D4A48 on misk_form_contact_us_type (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_18EEA527 on misk_form_feeback (groupId);
create index IX_BD438E17 on misk_form_feeback (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_89B6AE59 on misk_form_feeback (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_BD427EF on misk_form_feedback (groupId);
create index IX_60D0AC4F on misk_form_feedback (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_FBE5FA91 on misk_form_feedback (uuid_[$COLUMN_LENGTH:75$], groupId);