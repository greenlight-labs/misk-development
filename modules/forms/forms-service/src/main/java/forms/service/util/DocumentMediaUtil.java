package forms.service.util;

import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.util.DLURLHelperUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.repository.model.FileEntry;

public class DocumentMediaUtil {

    protected String getFileURLByID(long fileEntryId) throws PortalException {
        FileEntry fileEntry = DLAppLocalServiceUtil.getFileEntry(fileEntryId);
        return DLURLHelperUtil.getPreviewURL(fileEntry, fileEntry.getFileVersion(), null, "", false, false);
    }

    protected String getFileURLBYGroupUuid(long fileEntryGroupId, String fileEntryUuid) throws PortalException {
        FileEntry fileEntry = DLAppLocalServiceUtil.getFileEntryByUuidAndGroupId(fileEntryUuid, fileEntryGroupId);
        return DLURLHelperUtil.getPreviewURL(fileEntry, fileEntry.getFileVersion(), null, "", false, false);
        /*FileEntry fileEntry = PortletFileRepositoryUtil.getPortletFileEntry(fileEntryId);
        FileEntry fileEntry = PortletFileRepositoryUtil.getPortletFileEntry(fileEntryUuid, fileEntryGroupId);
        return PortletFileRepositoryUtil.getDownloadPortletFileEntryURL(themeDisplay, fileEntry, StringPool.BLANK);*/
    }

    public String getImageURL(String fieldXML) throws PortalException {
        String fileEntryURL = "";

        // here fieldValue is raw XML field value for your Image/DM field
        JSONObject jsonObject = JSONFactoryUtil.createJSONObject(fieldXML);

        long fileEntryId = jsonObject.getLong("fileEntryId");
        long fileEntryGroupId = jsonObject.getLong("groupId");
        String fileEntryUuid = jsonObject.getString("uuid");

        if (fileEntryId > 0) {
            fileEntryURL = getFileURLByID(fileEntryId);
        }else if(fileEntryGroupId > 0 && fileEntryUuid != null){
            fileEntryURL = getFileURLBYGroupUuid(fileEntryGroupId, fileEntryUuid);
        }

        return fileEntryURL;
    }
    
    public String getImageURL(long fileEntryId,long fileEntryGroupId,String fileEntryUuid) throws PortalException {
        String fileEntryURL = "";

        if (fileEntryId > 0) {
            fileEntryURL = getFileURLByID(fileEntryId);
        }else if(fileEntryGroupId > 0 && fileEntryUuid != null){
            fileEntryURL = getFileURLBYGroupUuid(fileEntryGroupId, fileEntryUuid);
        }

        return fileEntryURL;
    }
    public String getImageDescription(String fieldXML) throws PortalException {
        String fileEntryURL = "";
        System.out.println("fieldXML --- "+fieldXML);
        // here fieldValue is raw XML field value for your Image/DM field
        JSONObject jsonObject = JSONFactoryUtil.createJSONObject(fieldXML);

        String alt = jsonObject.getString("alt");
        return alt;
    }
}
