/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import forms.exception.FeedbackEmailAddressException;
import forms.exception.FeedbackFullNameException;
import forms.exception.FeedbackMessageException;
import forms.model.Feedback;
import forms.service.base.FeedbackLocalServiceBaseImpl;

/**
 * The implementation of the feedback local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>forms.service.FeedbackLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FeedbackLocalServiceBaseImpl
 */
@Component(property = "model.class.name=forms.model.Feedback", service = AopService.class)
public class FeedbackLocalServiceImpl extends FeedbackLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>forms.service.FeedbackLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>forms.service.FeedbackLocalServiceUtil</code>.
	 */

	public Feedback addFeedback(String fullName, String emailAddress, String type, String message,
			ServiceContext serviceContext) throws PortalException {
		long groupId = serviceContext.getScopeGroupId();

		/* User user = userLocalService.getUserById(serviceContext.getUserId()); */

		Date now = new Date();

		validate(fullName, emailAddress, message);

		long feedbackId = counterLocalService.increment();

		Feedback feedback = feedbackPersistence.create(feedbackId);
		feedback.setUuid(serviceContext.getUuid());
		feedback.setUserId(serviceContext.getUserId());
		feedback.setGroupId(groupId);
		feedback.setCompanyId(serviceContext.getCompanyId());
		/* feedback.setUserName(user.getFullName()); */
		feedback.setCreateDate(serviceContext.getCreateDate(now));
		feedback.setModifiedDate(serviceContext.getModifiedDate(now));
		feedback.setFullName(fullName);
		feedback.setEmailAddress(emailAddress);
		feedback.setType(type);
		feedback.setMessage(message);
		feedbackPersistence.update(feedback);
		feedbackPersistence.clearCache();
		return feedback;

	}

	public List<Feedback> getFeedbacks(long groupId, int start, int end) {

		return feedbackPersistence.findByGroupId(groupId, start, end);
	}

	public int getFeedbacks(long groupId) {

		return feedbackPersistence.countByGroupId(groupId);
	}

	protected void validate(String fullName, String emailAddress, String message) throws PortalException {

		if (Validator.isNull(fullName)) {
			throw new FeedbackFullNameException();
		}

		if (Validator.isNull(emailAddress)) {
			throw new FeedbackEmailAddressException();
		}

		if (Validator.isNull(message)) {
			throw new FeedbackMessageException();
		}

	}
}