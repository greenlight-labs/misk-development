/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import forms.exception.ContactUsFormTypeValidateException;
import forms.model.ContactUsFormType;
import forms.service.base.ContactUsFormTypeLocalServiceBaseImpl;

import forms.service.util.ContactUsFormTypeValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * The implementation of the contact us form type local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>forms.service.ContactUsFormTypeLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContactUsFormTypeLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=forms.model.ContactUsFormType",
	service = AopService.class
)
public class ContactUsFormTypeLocalServiceImpl
	extends ContactUsFormTypeLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>forms.service.ContactUsFormTypeLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>forms.service.ContactUsFormTypeLocalServiceUtil</code>.
	 */

	public ContactUsFormType addEntry(ContactUsFormType orgEntry, ServiceContext serviceContext)
			throws PortalException, ContactUsFormTypeValidateException {

		// Validation

		ModelValidator<ContactUsFormType> modelValidator = new ContactUsFormTypeValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		ContactUsFormType entry = _addEntry(orgEntry, serviceContext);

		ContactUsFormType addedEntry = contactUsFormTypePersistence.update(entry);

		contactUsFormTypePersistence.clearCache();

		return addedEntry;
	}

	public ContactUsFormType updateEntry(ContactUsFormType orgEntry, ServiceContext serviceContext)
			throws PortalException, ContactUsFormTypeValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<ContactUsFormType> modelValidator = new ContactUsFormTypeValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		ContactUsFormType entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		ContactUsFormType updatedEntry = contactUsFormTypePersistence.update(entry);
		contactUsFormTypePersistence.clearCache();

		return updatedEntry;
	}

	protected ContactUsFormType _addEntry(ContactUsFormType entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(ContactUsFormType.class.getName());

		ContactUsFormType newEntry = contactUsFormTypePersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setNameMap(entry.getNameMap());
		newEntry.setEmail(entry.getEmail());
		newEntry.setCc(entry.getCc());
		newEntry.setBcc(entry.getBcc());
		newEntry.setStatus(entry.getStatus());

		return newEntry;
	}

	protected ContactUsFormType _updateEntry(long primaryKey, ContactUsFormType entry, ServiceContext serviceContext)
			throws PortalException {

		ContactUsFormType updateEntry = fetchContactUsFormType(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setNameMap(entry.getNameMap());
		updateEntry.setEmail(entry.getEmail());
		updateEntry.setCc(entry.getCc());
		updateEntry.setBcc(entry.getBcc());
		updateEntry.setStatus(entry.getStatus());

		return updateEntry;
	}

	public ContactUsFormType deleteEntry(long primaryKey) throws PortalException {
		ContactUsFormType entry = getContactUsFormType(primaryKey);
		contactUsFormTypePersistence.remove(entry);

		return entry;
	}

	public List<ContactUsFormType> findByGroupId(long groupId) {

		return contactUsFormTypePersistence.findByGroupId(groupId);
	}

	public List<ContactUsFormType> findByGroupId(long groupId, int start, int end, OrderByComparator<ContactUsFormType> obc) {

		return contactUsFormTypePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<ContactUsFormType> findByGroupId(long groupId, int start, int end) {

		return contactUsFormTypePersistence.findByGroupId(groupId, start, end);
	}

	public int countByGroupId(long groupId) {

		return contactUsFormTypePersistence.countByGroupId(groupId);
	}

	public ContactUsFormType getContactUsFormTypeFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, ContactUsFormTypeValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		ContactUsFormType entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchContactUsFormType(primaryKey);
		}

		try {
			entry.setTypeId(primaryKey);

			entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
			entry.setEmail(ParamUtil.getString(request, "email"));
			entry.setCc(ParamUtil.getString(request, "cc"));
			entry.setBcc(ParamUtil.getString(request, "bcc"));
			entry.setStatus(ParamUtil.getBoolean(request, "status"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new ContactUsFormTypeValidateException(error);
		}

		return entry;
	}

	public ContactUsFormType getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(ContactUsFormType.class.getName());

		return createContactUsFormType(primaryKey);
	}

	public ContactUsFormType findEmailByInquiryType(String inquiryType) {
		return contactUsFormTypePersistence.fetchByName(inquiryType);
	}

	public ContactUsFormType findEmailByInquiryTypeLike(String inquiryType) {
		return formFinder.findByNameLike(inquiryType);
	}

	private static Log _log = LogFactoryUtil.getLog(ContactUsFormTypeLocalServiceImpl.class);
}