package forms.service.persistence.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import forms.model.ContactUsFormType;
import forms.model.impl.ContactUsFormTypeImpl;
import forms.service.persistence.FormFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;


@Component(service = FormFinder.class)
public class FormFinderImpl
        extends FormFinderBaseImpl implements FormFinder {

    public static final String FIND_BY_NAME_LIKE =
            FormFinder.class.getName() + ".findByNameLike";

    Log _log = LogFactoryUtil.getLog(FormFinderImpl.class);
    public ContactUsFormType findByNameLike(String name) {
        Session session = null;
        List<ContactUsFormType> types = Collections.emptyList();

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), FIND_BY_NAME_LIKE);

            sql = StringUtil.replace(sql, "[$NAME$]", name);

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);
            sqlQuery.setCacheable(false);
            sqlQuery.addEntity("ContactUsFormType", ContactUsFormTypeImpl.class);

            // get only one result
            types = (List<ContactUsFormType>) sqlQuery.list();
            ContactUsFormType type = null;
            if(types.size() > 0) {
                type = types.get(0);
            }

            return type;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    @Reference
    private CustomSQL _customSQL;
}
