package forms.service.util;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.language.UTF8Control;
import com.liferay.portal.kernel.util.LocaleUtil;
import forms.model.Form;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Contact Us Form Validator
 *
 * @author tz
 */
public class ContactUsFormValidator implements ModelValidator<Form> {

    /*
    * var alphaRegex = /^[\u0621-\u064A\u066E-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFFa-zA-Z\s]+$/;
    * var alphaNumericRegex = /^[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFFa-zA-Z0-9\s]+$/;
    * var messageRegex = /^[\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFFa-zA-Z0-9\s\.\,\-\_\?\!\@\%\&\(\)\:\'\"\=\+\n\r]+$/;
    * */
    private static final String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@(.+)$";
    private static final String ALPHA_REGEX = "^[\\u0621-\\u064A\\u066E-\\u06D3\\u06D5\\u06E5\\u06E6\\u06EE\\u06EF\\u06FA-\\u06FC\\u06FF\\u0750-\\u077F\\u08A0-\\u08FF\\uFB50-\\uFDFF\\uFE70-\\uFEFFa-zA-Z\\s]+$";
    private static final String ALPHA_NUMERIC_REGEX = "^[\\u0600-\\u06FF\\u0750-\\u077F\\u08A0-\\u08FF\\uFB50-\\uFDFF\\uFE70-\\uFEFFa-zA-Z0-9\\s]+$";
    private static final String MESSAGE_REGEX = "^[\\u0600-\\u06FF\\u0750-\\u077F\\u08A0-\\u08FF\\uFB50-\\uFDFF\\uFE70-\\uFEFFa-zA-Z0-9\\s\\.\\,\\-\\_\\?\\!\\@\\%\\&\\(\\)\\:\\'\\\"\\=\\+\\n\\r]+$";
    private static ResourceBundle resourceBundle;

    @Override
    public void validate(Form entry) throws PortalException {
        // set languageId
        String languageId = entry.getLanguage();
        setResourceBundle(languageId);
        // Validate fields
        validateFirstName(entry.getFirstName());
        validateLastName(entry.getLastName());
        validateEmail(entry.getEmail());
        validatePhone(entry.getPhone());
        validateInquiryType(entry.getInquiryType());
        validateMessage(entry.getQuestion());
    }

    private static void setResourceBundle(String languageId) {
        if (StringUtils.isBlank(languageId)) {
            languageId = LocaleUtil.getDefault().getLanguage();
        }
        Locale locale = LocaleUtils.toLocale(languageId);
        // get resource bundle (need to copy language.properties files to service module)
        resourceBundle = ResourceBundle.getBundle("content.Language", locale, UTF8Control.INSTANCE);
    }

    protected void validateFirstName(String field) {
        if (StringUtils.isBlank(field)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_first_name_required"));
        } else if (!field.matches(ALPHA_REGEX)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_first_name_alpha"));
        }
    }

    protected void validateLastName(String field) {
        if (StringUtils.isBlank(field)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_last_name_required"));
        } else if (!field.matches(ALPHA_REGEX)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_last_name_alpha"));
        }
    }

    protected void validateEmail(String field) {
        if (StringUtils.isBlank(field)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_email_required"));
        } else if (!field.matches(EMAIL_REGEX)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_email_invalid"));
        }
    }

    protected void validatePhone(String field) {
        if (StringUtils.isBlank(field)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_phone_required"));
        }
    }

    protected void validateInquiryType(String field) {
        if (StringUtils.isBlank(field)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_inquiry_type_required"));
        }
    }

    protected void validateMessage(String field) {
        if (StringUtils.isBlank(field)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_message_required"));
        } else if (!field.matches(MESSAGE_REGEX)) {
            _errors.add(LanguageUtil.get(resourceBundle, "forms_web_contact_us_message_invalid"));
        }
    }

    protected List<String> _errors = new ArrayList<>();

    public List<String> getErrors() {
        return _errors;
    }
}
