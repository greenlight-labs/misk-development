/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import forms.exception.*;
import forms.model.Form;
import forms.service.base.FormLocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the form local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>forms.service.FormLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FormLocalServiceBaseImpl
 */
@Component(
		property = "model.class.name=forms.model.Form", service = AopService.class
)
public class FormLocalServiceImpl extends FormLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>forms.service.FormLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>forms.service.FormLocalServiceUtil</code>.
	 */

	public Form addForm(long userId,
						String firstName, String lastName, String email, String phone, String inquiryType, String question, String language,
						ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(firstName, lastName, email, inquiryType, question);

		long formId = counterLocalService.increment();

		Form form = formPersistence.create(formId);

		form.setUuid(serviceContext.getUuid());
		form.setUserId(userId);
		form.setGroupId(groupId);
		form.setCompanyId(user.getCompanyId());
		form.setUserName(user.getFullName());
		form.setCreateDate(serviceContext.getCreateDate(now));
		form.setModifiedDate(serviceContext.getModifiedDate(now));

		form.setFirstName(firstName);
		form.setLastName(lastName);
		form.setEmail(email);
		form.setPhone(phone);
		form.setInquiryType(inquiryType);
		form.setQuestion(question);
		form.setLanguage(language);

		form.setExpandoBridgeAttributes(serviceContext);

		formPersistence.update(form);
		formPersistence.clearCache();

		return form;
	}

	public Form updateForm(long userId, long formId,
						   String firstName, String lastName, String email, String phone, String inquiryType, String question, String language,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(firstName, lastName, email, inquiryType, question);

		Form form = getForm(formId);

		User user = userLocalService.getUser(userId);

		form.setUserId(userId);
		form.setUserName(user.getFullName());
		form.setModifiedDate(serviceContext.getModifiedDate(now));

		form.setFirstName(firstName);
		form.setLastName(lastName);
		form.setEmail(email);
		form.setPhone(phone);
		form.setInquiryType(inquiryType);
		form.setQuestion(question);
		form.setLanguage(language);

		form.setExpandoBridgeAttributes(serviceContext);

		formPersistence.update(form);
		formPersistence.clearCache();

		return form;
	}

	public Form deleteForm(long formId,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Form form = getForm(formId);
		form = deleteForm(form);

		return form;
	}

	public List<Form> getForms(long groupId) {

		return formPersistence.findByGroupId(groupId);
	}

	public List<Form> getForms(long groupId, int start, int end,
							   OrderByComparator<Form> obc) {

		return formPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Form> getForms(long groupId, int start, int end) {

		return formPersistence.findByGroupId(groupId, start, end);
	}

	public int getFormsCount(long groupId) {

		return formPersistence.countByGroupId(groupId);
	}

	protected void validate(
			String firstName, String lastName, String email, String inquiryType, String question
	) throws PortalException {

		if (Validator.isNull(firstName)) {
			throw new FormFirstNameException();
		}

		if (Validator.isNull(lastName)) {
			throw new FormLastNameException();
		}

		if (Validator.isNull(email)) {
			throw new FormEmailException();
		}

		if (Validator.isNull(inquiryType)) {
			throw new FormInquiryTypeException();
		}

		if (Validator.isNull(question)) {
			throw new FormQuestionException();
		}

	}
}