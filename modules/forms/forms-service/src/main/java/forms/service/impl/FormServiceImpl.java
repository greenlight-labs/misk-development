/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.util.OrderByComparator;
import forms.model.Form;
import forms.service.base.FormServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the form remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>forms.service.FormService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FormServiceBaseImpl
 */
@Component(
		property = {
				"json.web.service.context.name=form",
				"json.web.service.context.path=Form"
		},
		service = AopService.class
)
public class FormServiceImpl extends FormServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>forms.service.FormServiceUtil</code> to access the form remote service.
	 */

	public List<Form> getForms(long groupId) {

		return formPersistence.findByGroupId(groupId);
	}

	public List<Form> getForms(long groupId, int start, int end,
							   OrderByComparator<Form> obc) {

		return formPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Form> getForms(long groupId, int start, int end) {

		return formPersistence.findByGroupId(groupId, start, end);
	}

	public int getFormsCount(long groupId) {

		return formPersistence.countByGroupId(groupId);
	}
}