package forms.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import forms.model.ContactUsFormType;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class ContactUsFormTypeValidator implements ModelValidator<ContactUsFormType> {

	@Override
	public void validate(ContactUsFormType entry) throws PortalException {
/*   */
        // Validate fields
        validateName(entry.getName());
        validateEmail(entry.getEmail());
        validateStatus(entry.getStatus());
	}

    protected void validateName(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("forms-contact-us-type-name-required");
        }
    }

    protected void validateEmail(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("forms-contact-us-type-email-required");
        }
    }

    protected void validateStatus(Boolean field) {}

	protected List<String> _errors = new ArrayList<>();

}
