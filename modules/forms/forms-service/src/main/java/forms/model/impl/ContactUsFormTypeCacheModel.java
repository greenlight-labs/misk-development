/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import forms.model.ContactUsFormType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ContactUsFormType in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ContactUsFormTypeCacheModel
	implements CacheModel<ContactUsFormType>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ContactUsFormTypeCacheModel)) {
			return false;
		}

		ContactUsFormTypeCacheModel contactUsFormTypeCacheModel =
			(ContactUsFormTypeCacheModel)object;

		if (typeId == contactUsFormTypeCacheModel.typeId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, typeId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", typeId=");
		sb.append(typeId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", name=");
		sb.append(name);
		sb.append(", email=");
		sb.append(email);
		sb.append(", cc=");
		sb.append(cc);
		sb.append(", bcc=");
		sb.append(bcc);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ContactUsFormType toEntityModel() {
		ContactUsFormTypeImpl contactUsFormTypeImpl =
			new ContactUsFormTypeImpl();

		if (uuid == null) {
			contactUsFormTypeImpl.setUuid("");
		}
		else {
			contactUsFormTypeImpl.setUuid(uuid);
		}

		contactUsFormTypeImpl.setTypeId(typeId);
		contactUsFormTypeImpl.setGroupId(groupId);
		contactUsFormTypeImpl.setCompanyId(companyId);
		contactUsFormTypeImpl.setUserId(userId);

		if (userName == null) {
			contactUsFormTypeImpl.setUserName("");
		}
		else {
			contactUsFormTypeImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			contactUsFormTypeImpl.setCreateDate(null);
		}
		else {
			contactUsFormTypeImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			contactUsFormTypeImpl.setModifiedDate(null);
		}
		else {
			contactUsFormTypeImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (name == null) {
			contactUsFormTypeImpl.setName("");
		}
		else {
			contactUsFormTypeImpl.setName(name);
		}

		if (email == null) {
			contactUsFormTypeImpl.setEmail("");
		}
		else {
			contactUsFormTypeImpl.setEmail(email);
		}

		if (cc == null) {
			contactUsFormTypeImpl.setCc("");
		}
		else {
			contactUsFormTypeImpl.setCc(cc);
		}

		if (bcc == null) {
			contactUsFormTypeImpl.setBcc("");
		}
		else {
			contactUsFormTypeImpl.setBcc(bcc);
		}

		contactUsFormTypeImpl.setStatus(status);

		contactUsFormTypeImpl.resetOriginalValues();

		return contactUsFormTypeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		typeId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		name = objectInput.readUTF();
		email = objectInput.readUTF();
		cc = objectInput.readUTF();
		bcc = objectInput.readUTF();

		status = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(typeId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (email == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (cc == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(cc);
		}

		if (bcc == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bcc);
		}

		objectOutput.writeBoolean(status);
	}

	public String uuid;
	public long typeId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String name;
	public String email;
	public String cc;
	public String bcc;
	public boolean status;

}