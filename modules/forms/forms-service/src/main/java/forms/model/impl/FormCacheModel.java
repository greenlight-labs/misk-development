/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package forms.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import forms.model.Form;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Form in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FormCacheModel implements CacheModel<Form>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FormCacheModel)) {
			return false;
		}

		FormCacheModel formCacheModel = (FormCacheModel)object;

		if (formId == formCacheModel.formId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, formId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", formId=");
		sb.append(formId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", email=");
		sb.append(email);
		sb.append(", phone=");
		sb.append(phone);
		sb.append(", inquiryType=");
		sb.append(inquiryType);
		sb.append(", question=");
		sb.append(question);
		sb.append(", language=");
		sb.append(language);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Form toEntityModel() {
		FormImpl formImpl = new FormImpl();

		if (uuid == null) {
			formImpl.setUuid("");
		}
		else {
			formImpl.setUuid(uuid);
		}

		formImpl.setFormId(formId);
		formImpl.setGroupId(groupId);
		formImpl.setCompanyId(companyId);
		formImpl.setUserId(userId);

		if (userName == null) {
			formImpl.setUserName("");
		}
		else {
			formImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			formImpl.setCreateDate(null);
		}
		else {
			formImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			formImpl.setModifiedDate(null);
		}
		else {
			formImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (firstName == null) {
			formImpl.setFirstName("");
		}
		else {
			formImpl.setFirstName(firstName);
		}

		if (lastName == null) {
			formImpl.setLastName("");
		}
		else {
			formImpl.setLastName(lastName);
		}

		if (email == null) {
			formImpl.setEmail("");
		}
		else {
			formImpl.setEmail(email);
		}

		if (phone == null) {
			formImpl.setPhone("");
		}
		else {
			formImpl.setPhone(phone);
		}

		if (inquiryType == null) {
			formImpl.setInquiryType("");
		}
		else {
			formImpl.setInquiryType(inquiryType);
		}

		if (question == null) {
			formImpl.setQuestion("");
		}
		else {
			formImpl.setQuestion(question);
		}

		if (language == null) {
			formImpl.setLanguage("");
		}
		else {
			formImpl.setLanguage(language);
		}

		formImpl.resetOriginalValues();

		return formImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		formId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		firstName = objectInput.readUTF();
		lastName = objectInput.readUTF();
		email = objectInput.readUTF();
		phone = objectInput.readUTF();
		inquiryType = objectInput.readUTF();
		question = objectInput.readUTF();
		language = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(formId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (firstName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(firstName);
		}

		if (lastName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lastName);
		}

		if (email == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (phone == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(phone);
		}

		if (inquiryType == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(inquiryType);
		}

		if (question == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(question);
		}

		if (language == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(language);
		}
	}

	public String uuid;
	public long formId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String firstName;
	public String lastName;
	public String email;
	public String phone;
	public String inquiryType;
	public String question;
	public String language;

}