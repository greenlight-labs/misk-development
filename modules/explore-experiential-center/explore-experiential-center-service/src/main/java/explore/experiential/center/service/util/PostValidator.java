package explore.experiential.center.service.util;

import explore.experiential.center.model.Post;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

/**
 * Gallery Validator
 *
 * @author tz
 */
public class PostValidator implements ModelValidator<Post> {

    @Override
    public void validate(Post entry) throws PortalException {
        /*   */
        // Validate fields
        validateCategoryId(entry.getCategoryId());
        validateName(entry.getName());
        validateDescription(entry.getDescription());
        validateEstimatedTourTimeLabel(entry.getEstimatedTourTimeLabel());
        validateEstimatedTourTime(entry.getEstimatedTourTime());
        validateType(entry.getType());
        validateImage(entry.getImage());
        validateVideo(entry.getVideo());
        validateButtonLabel(entry.getButtonLabel());
        validateAudio(entry.getAudio());
        validateAudioCatColor(entry.getAudioCatColor());
        validateAudioCatName(entry.getAudioCatName());
        validateAudioName(entry.getAudioName());
        validateAudioProfession(entry.getAudioProfession());
        validateAudioLocation(entry.getAudioLocation());
    }

    /**
     * categoryId field Validation
     *
     * @param field categoryId
     */
    protected void validateCategoryId(long field) {
        if (Validator.isNull(field)) {
            _errors.add("post-category-id-required");
        }
    }

    protected void validateName(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-name-required");
        }
    }

    protected void validateDescription(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-description-required");
        }
    }

    protected void validateEstimatedTourTimeLabel(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-estimated-tour-time-label-required");
        }
    }

    protected void validateEstimatedTourTime(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-estimated-tour-time-required");
        }
    }

    protected void validateType(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-type-required");
        }
    }

    protected void validateImage(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-image-required");
        }
    }

    protected void validateVideo(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-video-required");
        }
    }

    protected void validateButtonLabel(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-button-label-required");
        }
    }

    protected void validateAudio(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-audio-required");
        }
    }

    protected void validateAudioCatColor(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-audio-cat-color-required");
        }
    }

    protected void validateAudioCatName(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-audio-cat-name-required");
        }
    }

    protected void validateAudioName(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-audio-name-required");
        }
    }

    protected void validateAudioProfession(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-audio-profession-required");
        }
    }

    protected void validateAudioLocation(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("post-audio-location-required");
        }
    }

    protected List<String> _errors = new ArrayList<>();

}
