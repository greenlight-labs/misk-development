/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import explore.experiential.center.exception.PostValidateException;
import explore.experiential.center.model.Gallery;
import explore.experiential.center.model.Post;
import explore.experiential.center.service.GalleryLocalServiceUtil;
import explore.experiential.center.service.base.PostLocalServiceBaseImpl;
import explore.experiential.center.service.util.PostValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

import static com.liferay.portal.kernel.log.LogFactoryUtil.getLog;
import static com.liferay.portal.kernel.util.ParamUtil.*;

/**
 * @author Brian Wing Shun Chan
 */
@Component(
	property = "model.class.name=explore.experiential.center.model.Post",
	service = AopService.class
)
public class PostLocalServiceImpl extends PostLocalServiceBaseImpl {

	private static Log _log = getLog(PostLocalServiceImpl.class);

	public Post addEntry(Post orgEntry, ServiceContext serviceContext)
			throws PortalException, PostValidateException {

		// Validation

		ModelValidator<Post> modelValidator = new PostValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Post entry = _addEntry(orgEntry, serviceContext);

		Post addedEntry = postPersistence.update(entry);

		postPersistence.clearCache();

		return addedEntry;
	}

	public Post updateEntry(Post orgEntry, ServiceContext serviceContext)
			throws PortalException, PostValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Post> modelValidator = new PostValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Post entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Post updatedEntry = postPersistence.update(entry);
		postPersistence.clearCache();

		return updatedEntry;
	}

	protected Post _addEntry(Post entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Post.class.getName());

		Post newEntry = postPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());
		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());
		newEntry.setOrderNo(entry.getOrderNo()!=0?entry.getOrderNo():9999);
		newEntry.setCategoryId(entry.getCategoryId());
		newEntry.setName(entry.getName());
		newEntry.setDescription(entry.getDescription());
		//newEntry.setEstimatedTourTimeLabel(entry.getEstimatedTourTimeLabel());
		newEntry.setEstimatedTourTime(entry.getEstimatedTourTime());
		newEntry.setType(entry.getType());
		newEntry.setVideo(entry.getVideo());
		newEntry.setImage(entry.getImage());
		newEntry.setButtonLabel(entry.getButtonLabel());
		newEntry.setAudio(entry.getAudio());
		newEntry.setAudioCatColor(entry.getAudioCatColor());
		newEntry.setAudioCatName(entry.getAudioCatName());
		newEntry.setAudioName(entry.getAudioName());
		newEntry.setAudioProfession(entry.getAudioProfession());
		newEntry.setAudioLocation(entry.getAudioLocation());

		return newEntry;
	}

	protected Post _updateEntry(long primaryKey, Post entry, ServiceContext serviceContext)
			throws PortalException {

		Post updateEntry = fetchPost(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());
		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setCategoryId(entry.getCategoryId());
		updateEntry.setOrderNo(entry.getOrderNo()!=0?entry.getOrderNo():9999);
		updateEntry.setName(entry.getName());
		updateEntry.setDescription(entry.getDescription());
		//updateEntry.setEstimatedTourTimeLabel(entry.getEstimatedTourTimeLabel());
		updateEntry.setEstimatedTourTime(entry.getEstimatedTourTime());
		updateEntry.setType(entry.getType());
		updateEntry.setVideo(entry.getVideo());
		updateEntry.setImage(entry.getImage());
		updateEntry.setButtonLabel(entry.getButtonLabel());
		updateEntry.setAudio(entry.getAudio());
		updateEntry.setAudioCatColor(entry.getAudioCatColor());
		updateEntry.setAudioCatName(entry.getAudioCatName());
		updateEntry.setAudioName(entry.getAudioName());
		updateEntry.setAudioProfession(entry.getAudioProfession());
		updateEntry.setAudioLocation(entry.getAudioLocation());

		return updateEntry;
	}

	public Post deleteEntry(long primaryKey) throws PortalException {
		Post entry = getPost(primaryKey);
		postPersistence.remove(entry);

		return entry;
	}

	public List<Post> findByGroupId(long groupId) {

		return postPersistence.findByGroupId(groupId);
	}

	public List<Post> findByGroupId(long groupId, int start, int end, OrderByComparator<Post> obc) {

		return postPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Post> findByGroupId(long groupId, int start, int end) {

		return postPersistence.findByGroupId(groupId, start, end);
	}

	public int countByGroupId(long groupId) {

		return postPersistence.countByGroupId(groupId);
	}

	public List<Post> findByCategoryId(long type) {

		return postPersistence.findByCategoryId(type);
	}

	public List<Post> findByCategoryId(long type, int start, int end,
									   OrderByComparator<Post> obc) {

		return postPersistence.findByCategoryId(type, start, end, obc);
	}

	public List<Post> findByCategoryId(long type, int start, int end) {

		return postPersistence.findByCategoryId(type, start, end);
	}

	public int countByCategoryId(long type) {

		return postPersistence.countByCategoryId(type);
	}

	public Post getPostFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, PostValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Post entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchPost(primaryKey);
		}

		try {
			entry.setPostId(primaryKey);

			entry.setCategoryId(getLong(request, "categoryId"));
			entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
			entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));
			//entry.setEstimatedTourTimeLabelMap(LocalizationUtil.getLocalizationMap(request, "estimatedTourTimeLabel"));
			entry.setEstimatedTourTimeMap(LocalizationUtil.getLocalizationMap(request, "estimatedTourTime"));
			entry.setType(getString(request, "type"));
			entry.setVideo(getString(request, "video"));
			entry.setImage(getString(request, "image"));
			entry.setButtonLabelMap(LocalizationUtil.getLocalizationMap(request, "buttonLabel"));
			entry.setAudio(getString(request, "audio"));
			entry.setAudioCatColor(getString(request, "audioCatColor"));
			entry.setAudioCatNameMap(LocalizationUtil.getLocalizationMap(request, "audioCatName"));
			entry.setAudioNameMap(LocalizationUtil.getLocalizationMap(request, "audioName"));
			entry.setAudioProfessionMap(LocalizationUtil.getLocalizationMap(request, "audioProfession"));
			entry.setAudioLocationMap(LocalizationUtil.getLocalizationMap(request, "audioLocation"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new PostValidateException(error);
		}

		return entry;
	}

	public Post getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Post.class.getName());

		return createPost(primaryKey);
	}

	/* **********************************- Additional Code Here -***************************** */
	/* **********************************- START: Post Gallery Code -***************************** */
	public void addEntryGallery(Post orgEntry, PortletRequest request) throws PortletException {

		String galleryIndexesString = getString(request, "galleryIndexes");

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);
	}

	@Override
	public void updateGalleries(Post entry, List<Gallery> galleries, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> galleryIds = new HashSet<>();

		for (Gallery gallery : galleries) {
			long galleryId = gallery.getGalleryId();

			// additional step to set Ids - initially zero
			gallery.setPostId(entry.getPostId());
			gallery.setUserId(entry.getUserId());
			gallery.setCompanyId(entry.getCompanyId());
			gallery.setGroupId(entry.getGroupId());

			if (galleryId <= 0) {
				gallery = GalleryLocalServiceUtil.addEntry(gallery, serviceContext);

				galleryId = gallery.getGalleryId();
			} else {
				GalleryLocalServiceUtil.updateEntry(gallery, serviceContext);
			}

			galleryIds.add(galleryId);
		}

		galleries = GalleryLocalServiceUtil.findByPostId(entry.getPostId());

		for (Gallery gallery : galleries) {
			if (!galleryIds.contains(gallery.getGalleryId())) {
				GalleryLocalServiceUtil.deleteGallery(gallery.getGalleryId());
			}
		}
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest) {
		return getGalleries(actionRequest, Collections.<Gallery>emptyList());
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest, List<Gallery> defaultGalleries) {

		String galleryIndexesString = getString(actionRequest, "galleryIndexes");

		if (galleryIndexesString == null) {
			return defaultGalleries;
		}

		List<Gallery> galleries = new ArrayList<>();

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);

		for (int galleryIndex : galleryIndexes) {
			String image = getString(actionRequest, "galleryImage" + galleryIndex);

			if (Validator.isNull(image)) {
				continue;
			}

			long galleryId = getLong(actionRequest, "galleryId" + galleryIndex);

			Gallery gallery = GalleryLocalServiceUtil.createGallery(galleryId);

			gallery.setPostId(0);
			gallery.setImage(image);

			galleries.add(gallery);
		}

		return galleries;
	}

	/* **********************************- END: Post Gallery Code -***************************** */

}