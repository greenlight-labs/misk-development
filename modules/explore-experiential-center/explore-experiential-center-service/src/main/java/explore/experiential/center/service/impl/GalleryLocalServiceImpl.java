/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import explore.experiential.center.exception.GalleryValidateException;
import explore.experiential.center.model.Gallery;
import explore.experiential.center.service.base.GalleryLocalServiceBaseImpl;

import explore.experiential.center.service.util.GalleryValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.liferay.portal.kernel.log.LogFactoryUtil.getLog;

/**
 * @author Brian Wing Shun Chan
 */
@Component(
	property = "model.class.name=explore.experiential.center.model.Gallery",
	service = AopService.class
)
public class GalleryLocalServiceImpl extends GalleryLocalServiceBaseImpl {

	private static Log _log = getLog(GalleryLocalServiceImpl.class);

	public Gallery addEntry(Gallery orgEntry, ServiceContext serviceContext)
			throws PortalException, GalleryValidateException {

		// Validation

		ModelValidator<Gallery> modelValidator = new GalleryValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Gallery entry = _addEntry(orgEntry, serviceContext);

		Gallery addedEntry = galleryPersistence.update(entry);

		galleryPersistence.clearCache();

		return addedEntry;
	}

	public Gallery updateEntry(Gallery orgEntry, ServiceContext serviceContext)
			throws PortalException, GalleryValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Gallery> modelValidator = new GalleryValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Gallery entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Gallery updatedEntry = galleryPersistence.update(entry);
		galleryPersistence.clearCache();

		return updatedEntry;
	}

	protected Gallery _addEntry(Gallery entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Gallery.class.getName());

		Gallery newEntry = galleryPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setPostId(entry.getPostId());
		newEntry.setImage(entry.getImage());

		return newEntry;
	}

	protected Gallery _updateEntry(long primaryKey, Gallery entry, ServiceContext serviceContext)
			throws PortalException {

		Gallery updateEntry = fetchGallery(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setPostId(entry.getPostId());
		updateEntry.setImage(entry.getImage());

		return updateEntry;
	}

	public Gallery deleteEntry(long primaryKey) throws PortalException {
		Gallery entry = getGallery(primaryKey);
		galleryPersistence.remove(entry);

		return entry;
	}

	public List<Gallery> findByGroupId(long groupId) {

		return galleryPersistence.findByGroupId(groupId);
	}

	public List<Gallery> findByGroupId(long groupId, int start, int end, OrderByComparator<Gallery> obc) {

		return galleryPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Gallery> findByGroupId(long groupId, int start, int end) {

		return galleryPersistence.findByGroupId(groupId, start, end);
	}

	public int countByGroupId(long groupId) {

		return galleryPersistence.countByGroupId(groupId);
	}

	/* *********************- gallery by album id -*********************** */
	public List<Gallery> findByPostId(long postId) {

		return galleryPersistence.findByPostId(postId);
	}

	public List<Gallery> findByPostId(long postId, int start, int end,
									  OrderByComparator<Gallery> obc) {

		return galleryPersistence.findByPostId(postId, start, end, obc);
	}

	public List<Gallery> findByPostId(long postId, int start, int end) {

		return galleryPersistence.findByPostId(postId, start, end);
	}

	public int countByPostId(long postId) {

		return galleryPersistence.countByPostId(postId);
	}
	/* ********************************************************************** */
	public Gallery getGalleryFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, GalleryValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Gallery entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchGallery(primaryKey);
		}

		try {
			entry.setGalleryId(primaryKey);

			entry.setPostId(ParamUtil.getLong(request, "postId"));
			entry.setImage(ParamUtil.getString(request, "image"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new GalleryValidateException(error);
		}

		return entry;
	}

	public Gallery getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Gallery.class.getName());

		return createGallery(primaryKey);
	}

	/* **********************************- Additional Code Here -***************************** */

}