package explore.experiential.center.service.util;

import explore.experiential.center.model.Category;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator
 *
 * @author tz
 */
public class CategoryValidator implements ModelValidator<Category> {

    @Override
    public void validate(Category entry) throws PortalException {
        /*   */
        // Validate fields
        validateName(entry.getName());
        validateImage(entry.getImage());
    }

    /**
     * categoryId field Validation
     *
     * @param field categoryId
     */
    protected void validateName(String field) {
        if (Validator.isNull(field)) {
            _errors.add("category-name-required");
        }
    }

    protected void validateImage(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("category-image-required");
        }
    }

    protected List<String> _errors = new ArrayList<>();

}
