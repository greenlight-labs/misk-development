/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import explore.experiential.center.model.Post;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Post in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class PostCacheModel implements CacheModel<Post>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof PostCacheModel)) {
			return false;
		}

		PostCacheModel postCacheModel = (PostCacheModel)object;

		if (postId == postCacheModel.postId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, postId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", postId=");
		sb.append(postId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", orderNo=");
		sb.append(orderNo);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", estimatedTourTimeLabel=");
		sb.append(estimatedTourTimeLabel);
		sb.append(", estimatedTourTime=");
		sb.append(estimatedTourTime);
		sb.append(", type=");
		sb.append(type);
		sb.append(", video=");
		sb.append(video);
		sb.append(", image=");
		sb.append(image);
		sb.append(", buttonLabel=");
		sb.append(buttonLabel);
		sb.append(", audio=");
		sb.append(audio);
		sb.append(", audioCatColor=");
		sb.append(audioCatColor);
		sb.append(", audioCatName=");
		sb.append(audioCatName);
		sb.append(", audioName=");
		sb.append(audioName);
		sb.append(", audioProfession=");
		sb.append(audioProfession);
		sb.append(", audioLocation=");
		sb.append(audioLocation);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Post toEntityModel() {
		PostImpl postImpl = new PostImpl();

		if (uuid == null) {
			postImpl.setUuid("");
		}
		else {
			postImpl.setUuid(uuid);
		}

		postImpl.setPostId(postId);
		postImpl.setGroupId(groupId);
		postImpl.setCompanyId(companyId);
		postImpl.setUserId(userId);

		if (userName == null) {
			postImpl.setUserName("");
		}
		else {
			postImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			postImpl.setCreateDate(null);
		}
		else {
			postImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			postImpl.setModifiedDate(null);
		}
		else {
			postImpl.setModifiedDate(new Date(modifiedDate));
		}

		postImpl.setCategoryId(categoryId);
		postImpl.setOrderNo(orderNo);

		if (name == null) {
			postImpl.setName("");
		}
		else {
			postImpl.setName(name);
		}

		if (description == null) {
			postImpl.setDescription("");
		}
		else {
			postImpl.setDescription(description);
		}

		if (estimatedTourTimeLabel == null) {
			postImpl.setEstimatedTourTimeLabel("");
		}
		else {
			postImpl.setEstimatedTourTimeLabel(estimatedTourTimeLabel);
		}

		if (estimatedTourTime == null) {
			postImpl.setEstimatedTourTime("");
		}
		else {
			postImpl.setEstimatedTourTime(estimatedTourTime);
		}

		if (type == null) {
			postImpl.setType("");
		}
		else {
			postImpl.setType(type);
		}

		if (video == null) {
			postImpl.setVideo("");
		}
		else {
			postImpl.setVideo(video);
		}

		if (image == null) {
			postImpl.setImage("");
		}
		else {
			postImpl.setImage(image);
		}

		if (buttonLabel == null) {
			postImpl.setButtonLabel("");
		}
		else {
			postImpl.setButtonLabel(buttonLabel);
		}

		if (audio == null) {
			postImpl.setAudio("");
		}
		else {
			postImpl.setAudio(audio);
		}

		if (audioCatColor == null) {
			postImpl.setAudioCatColor("");
		}
		else {
			postImpl.setAudioCatColor(audioCatColor);
		}

		if (audioCatName == null) {
			postImpl.setAudioCatName("");
		}
		else {
			postImpl.setAudioCatName(audioCatName);
		}

		if (audioName == null) {
			postImpl.setAudioName("");
		}
		else {
			postImpl.setAudioName(audioName);
		}

		if (audioProfession == null) {
			postImpl.setAudioProfession("");
		}
		else {
			postImpl.setAudioProfession(audioProfession);
		}

		if (audioLocation == null) {
			postImpl.setAudioLocation("");
		}
		else {
			postImpl.setAudioLocation(audioLocation);
		}

		postImpl.resetOriginalValues();

		return postImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		postId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		categoryId = objectInput.readLong();

		orderNo = objectInput.readLong();
		name = objectInput.readUTF();
		description = objectInput.readUTF();
		estimatedTourTimeLabel = objectInput.readUTF();
		estimatedTourTime = objectInput.readUTF();
		type = objectInput.readUTF();
		video = objectInput.readUTF();
		image = objectInput.readUTF();
		buttonLabel = objectInput.readUTF();
		audio = objectInput.readUTF();
		audioCatColor = objectInput.readUTF();
		audioCatName = objectInput.readUTF();
		audioName = objectInput.readUTF();
		audioProfession = objectInput.readUTF();
		audioLocation = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(postId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(categoryId);

		objectOutput.writeLong(orderNo);

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (estimatedTourTimeLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(estimatedTourTimeLabel);
		}

		if (estimatedTourTime == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(estimatedTourTime);
		}

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}

		if (video == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(video);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (buttonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLabel);
		}

		if (audio == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(audio);
		}

		if (audioCatColor == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(audioCatColor);
		}

		if (audioCatName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(audioCatName);
		}

		if (audioName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(audioName);
		}

		if (audioProfession == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(audioProfession);
		}

		if (audioLocation == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(audioLocation);
		}
	}

	public String uuid;
	public long postId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long categoryId;
	public long orderNo;
	public String name;
	public String description;
	public String estimatedTourTimeLabel;
	public String estimatedTourTime;
	public String type;
	public String video;
	public String image;
	public String buttonLabel;
	public String audio;
	public String audioCatColor;
	public String audioCatName;
	public String audioName;
	public String audioProfession;
	public String audioLocation;

}