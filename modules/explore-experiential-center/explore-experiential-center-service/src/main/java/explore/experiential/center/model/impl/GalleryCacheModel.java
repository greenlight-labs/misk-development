/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import explore.experiential.center.model.Gallery;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Gallery in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class GalleryCacheModel implements CacheModel<Gallery>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GalleryCacheModel)) {
			return false;
		}

		GalleryCacheModel galleryCacheModel = (GalleryCacheModel)object;

		if (galleryId == galleryCacheModel.galleryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, galleryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", galleryId=");
		sb.append(galleryId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", postId=");
		sb.append(postId);
		sb.append(", image=");
		sb.append(image);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Gallery toEntityModel() {
		GalleryImpl galleryImpl = new GalleryImpl();

		if (uuid == null) {
			galleryImpl.setUuid("");
		}
		else {
			galleryImpl.setUuid(uuid);
		}

		galleryImpl.setGalleryId(galleryId);
		galleryImpl.setGroupId(groupId);
		galleryImpl.setCompanyId(companyId);
		galleryImpl.setUserId(userId);

		if (userName == null) {
			galleryImpl.setUserName("");
		}
		else {
			galleryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			galleryImpl.setCreateDate(null);
		}
		else {
			galleryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			galleryImpl.setModifiedDate(null);
		}
		else {
			galleryImpl.setModifiedDate(new Date(modifiedDate));
		}

		galleryImpl.setPostId(postId);

		if (image == null) {
			galleryImpl.setImage("");
		}
		else {
			galleryImpl.setImage(image);
		}

		galleryImpl.resetOriginalValues();

		return galleryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		galleryId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		postId = objectInput.readLong();
		image = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(galleryId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(postId);

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}
	}

	public String uuid;
	public long galleryId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long postId;
	public String image;

}