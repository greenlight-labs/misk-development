package explore.experiential.center.service.impl;

import explore.experiential.center.exception.CategoryValidateException;
import explore.experiential.center.model.Category;
import explore.experiential.center.service.base.CategoryLocalServiceBaseImpl;
import explore.experiential.center.service.util.CategoryValidator;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

@Component(
        property = "model.class.name=explore.experiential.center.model.Category", service = AopService.class
)
public class CategoryLocalServiceImpl extends CategoryLocalServiceBaseImpl {

    private static Log _log = LogFactoryUtil.getLog(CategoryLocalServiceImpl.class);

    public Category addEntry(Category orgEntry, ServiceContext serviceContext)
            throws PortalException, CategoryValidateException {

        // Validation

        ModelValidator<Category> modelValidator = new CategoryValidator();
        modelValidator.validate(orgEntry);

        // Add entry

        Category entry = _addEntry(orgEntry, serviceContext);

        Category addedEntry = categoryPersistence.update(entry);

        categoryPersistence.clearCache();

        return addedEntry;
    }

    public Category updateEntry(Category orgEntry, ServiceContext serviceContext)
            throws PortalException, CategoryValidateException {

        User user = userLocalService.getUser(orgEntry.getUserId());

        // Validation

        ModelValidator<Category> modelValidator = new CategoryValidator();
        modelValidator.validate(orgEntry);

        // Update entry

        Category entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

        Category updatedEntry = categoryPersistence.update(entry);
        categoryPersistence.clearCache();

        return updatedEntry;
    }

    protected Category _addEntry(Category entry, ServiceContext serviceContext) throws PortalException {

        long id = counterLocalService.increment(Category.class.getName());

        Category newEntry = categoryPersistence.create(id);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        newEntry.setCompanyId(entry.getCompanyId());
        newEntry.setGroupId(entry.getGroupId());
        newEntry.setUserId(user.getUserId());
        newEntry.setUserName(user.getFullName());
        newEntry.setCreateDate(now);
        newEntry.setModifiedDate(now);
        newEntry.setUuid(serviceContext.getUuid());

        newEntry.setName(entry.getName());
        newEntry.setImage(entry.getImage());
        newEntry.setHeading(entry.getHeading());
        newEntry.setDescription(entry.getDescription());
        //newEntry.setEstimatedTourTimeLabel(entry.getEstimatedTourTimeLabel());
        newEntry.setEstimatedTourTime(entry.getEstimatedTourTime());

        return newEntry;
    }

    protected Category _updateEntry(long primaryKey, Category entry, ServiceContext serviceContext)
            throws PortalException {

        Category updateEntry = fetchCategory(primaryKey);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        updateEntry.setCompanyId(entry.getCompanyId());
        updateEntry.setGroupId(entry.getGroupId());
        updateEntry.setUserId(user.getUserId());
        updateEntry.setUserName(user.getFullName());
        updateEntry.setCreateDate(entry.getCreateDate());
        updateEntry.setModifiedDate(now);
        updateEntry.setUuid(entry.getUuid());

        updateEntry.setName(entry.getName());
        updateEntry.setImage(entry.getImage());
        updateEntry.setHeading(entry.getHeading());
        updateEntry.setDescription(entry.getDescription());
        //updateEntry.setEstimatedTourTimeLabel(entry.getEstimatedTourTimeLabel());
        updateEntry.setEstimatedTourTime(entry.getEstimatedTourTime());

        return updateEntry;
    }

    public Category deleteEntry(long primaryKey) throws PortalException {
        Category entry = getCategory(primaryKey);
        categoryPersistence.remove(entry);

        return entry;
    }

    public List<Category> findByGroupId(long groupId) {

        return categoryPersistence.findByGroupId(groupId);
    }

    public List<Category> findByGroupId(long groupId, int start, int end, OrderByComparator<Category> obc) {

        return categoryPersistence.findByGroupId(groupId, start, end, obc);
    }

    public List<Category> findByGroupId(long groupId, int start, int end) {

        return categoryPersistence.findByGroupId(groupId, start, end);
    }

    public int countByGroupId(long groupId) {

        return categoryPersistence.countByGroupId(groupId);
    }

    public Category getCategoryFromRequest(long primaryKey, PortletRequest request)
            throws PortletException, CategoryValidateException {

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

        // Create or fetch existing data

        Category entry;

        if (primaryKey <= 0) {
            entry = getNewObject(primaryKey);
        } else {
            entry = fetchCategory(primaryKey);
        }

        try {
            entry.setCategoryId(primaryKey);

            entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
            entry.setImageMap(LocalizationUtil.getLocalizationMap(request, "image"));
            entry.setHeadingMap(LocalizationUtil.getLocalizationMap(request, "heading"));
            entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));
            //entry.setEstimatedTourTimeLabelMap(LocalizationUtil.getLocalizationMap(request, "estimatedTourTimeLabel"));
            entry.setEstimatedTourTimeMap(LocalizationUtil.getLocalizationMap(request, "estimatedTourTime"));

            entry.setCompanyId(themeDisplay.getCompanyId());
            entry.setGroupId(themeDisplay.getScopeGroupId());
            entry.setUserId(themeDisplay.getUserId());
        } catch (Exception e) {
            _log.error("Errors occur while populating the model", e);
            List<String> error = new ArrayList<>();
            error.add("value-convert-error");

            throw new CategoryValidateException(error);
        }

        return entry;
    }

    public Category getNewObject(long primaryKey) {
        primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Category.class.getName());

        return createCategory(primaryKey);
    }

    /* **********************************- Additional Code Here -***************************** */
}