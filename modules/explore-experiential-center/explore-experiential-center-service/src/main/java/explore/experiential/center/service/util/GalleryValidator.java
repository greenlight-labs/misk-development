package explore.experiential.center.service.util;

import explore.experiential.center.model.Gallery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator
 *
 * @author tz
 */
public class GalleryValidator implements ModelValidator<Gallery> {

    @Override
    public void validate(Gallery entry) throws PortalException {
        /*   */
        // Validate fields
        validatePostId(entry.getPostId());
        validateImage(entry.getImage());
    }

    /**
     * categoryId field Validation
     *
     * @param field categoryId
     */
    protected void validatePostId(long field) {
        if (Validator.isNull(field)) {
            _errors.add("gallery-post-id-required");
        }
    }

    protected void validateImage(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("gallery-image-required");
        }
    }

    protected List<String> _errors = new ArrayList<>();

}
