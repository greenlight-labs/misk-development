create table misk_experiential_center_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	image STRING null,
	heading STRING null,
	description STRING null,
	estimatedTourTimeLabel STRING null,
	estimatedTourTime STRING null
);

create table misk_experiential_center_posts (
	uuid_ VARCHAR(75) null,
	postId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	categoryId LONG,
	orderNo LONG,
	name STRING null,
	description STRING null,
	estimatedTourTimeLabel STRING null,
	estimatedTourTime STRING null,
	type_ VARCHAR(75) null,
	video VARCHAR(250) null,
	image VARCHAR(250) null,
	buttonLabel STRING null,
	audio VARCHAR(250) null,
	audioCatColor VARCHAR(75) null,
	audioCatName STRING null,
	audioName STRING null,
	audioProfession STRING null,
	audioLocation STRING null
);

create table misk_experiential_center_posts_galleries (
	uuid_ VARCHAR(75) null,
	galleryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	postId LONG,
	image VARCHAR(250) null
);