create index IX_F951428A on misk_experiential_center_categories (groupId);
create index IX_ADE85014 on misk_experiential_center_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_1BADCB96 on misk_experiential_center_categories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_2D3F5B48 on misk_experiential_center_posts (categoryId);
create index IX_495562F on misk_experiential_center_posts (groupId);
create index IX_F5DF860F on misk_experiential_center_posts (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_46604451 on misk_experiential_center_posts (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_108FBDDE on misk_experiential_center_posts_galleries (groupId);
create index IX_EE6C52DB on misk_experiential_center_posts_galleries (postId);
create index IX_4DDD5340 on misk_experiential_center_posts_galleries (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_4921A9C2 on misk_experiential_center_posts_galleries (uuid_[$COLUMN_LENGTH:75$], groupId);