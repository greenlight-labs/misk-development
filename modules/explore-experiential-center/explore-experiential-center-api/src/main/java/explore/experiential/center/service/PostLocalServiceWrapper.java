/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PostLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PostLocalService
 * @generated
 */
public class PostLocalServiceWrapper
	implements PostLocalService, ServiceWrapper<PostLocalService> {

	public PostLocalServiceWrapper(PostLocalService postLocalService) {
		_postLocalService = postLocalService;
	}

	@Override
	public explore.experiential.center.model.Post addEntry(
			explore.experiential.center.model.Post orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   explore.experiential.center.exception.PostValidateException {

		return _postLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public void addEntryGallery(
			explore.experiential.center.model.Post orgEntry,
			javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		_postLocalService.addEntryGallery(orgEntry, request);
	}

	/**
	 * Adds the post to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param post the post
	 * @return the post that was added
	 */
	@Override
	public explore.experiential.center.model.Post addPost(
		explore.experiential.center.model.Post post) {

		return _postLocalService.addPost(post);
	}

	@Override
	public int countByCategoryId(long type) {
		return _postLocalService.countByCategoryId(type);
	}

	@Override
	public int countByGroupId(long groupId) {
		return _postLocalService.countByGroupId(groupId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _postLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new post with the primary key. Does not add the post to the database.
	 *
	 * @param postId the primary key for the new post
	 * @return the new post
	 */
	@Override
	public explore.experiential.center.model.Post createPost(long postId) {
		return _postLocalService.createPost(postId);
	}

	@Override
	public explore.experiential.center.model.Post deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _postLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _postLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the post with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param postId the primary key of the post
	 * @return the post that was removed
	 * @throws PortalException if a post with the primary key could not be found
	 */
	@Override
	public explore.experiential.center.model.Post deletePost(long postId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _postLocalService.deletePost(postId);
	}

	/**
	 * Deletes the post from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param post the post
	 * @return the post that was removed
	 */
	@Override
	public explore.experiential.center.model.Post deletePost(
		explore.experiential.center.model.Post post) {

		return _postLocalService.deletePost(post);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _postLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _postLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.experiential.center.model.impl.PostModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _postLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.experiential.center.model.impl.PostModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _postLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _postLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _postLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public explore.experiential.center.model.Post fetchPost(long postId) {
		return _postLocalService.fetchPost(postId);
	}

	/**
	 * Returns the post matching the UUID and group.
	 *
	 * @param uuid the post's UUID
	 * @param groupId the primary key of the group
	 * @return the matching post, or <code>null</code> if a matching post could not be found
	 */
	@Override
	public explore.experiential.center.model.Post fetchPostByUuidAndGroupId(
		String uuid, long groupId) {

		return _postLocalService.fetchPostByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Post>
		findByCategoryId(long type) {

		return _postLocalService.findByCategoryId(type);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Post>
		findByCategoryId(long type, int start, int end) {

		return _postLocalService.findByCategoryId(type, start, end);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Post>
		findByCategoryId(
			long type, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<explore.experiential.center.model.Post> obc) {

		return _postLocalService.findByCategoryId(type, start, end, obc);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Post> findByGroupId(
		long groupId) {

		return _postLocalService.findByGroupId(groupId);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Post> findByGroupId(
		long groupId, int start, int end) {

		return _postLocalService.findByGroupId(groupId, start, end);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Post> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<explore.experiential.center.model.Post> obc) {

		return _postLocalService.findByGroupId(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _postLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _postLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Gallery>
		getGalleries(javax.portlet.ActionRequest actionRequest) {

		return _postLocalService.getGalleries(actionRequest);
	}

	@Override
	public java.util.List<explore.experiential.center.model.Gallery>
		getGalleries(
			javax.portlet.ActionRequest actionRequest,
			java.util.List<explore.experiential.center.model.Gallery>
				defaultGalleries) {

		return _postLocalService.getGalleries(actionRequest, defaultGalleries);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _postLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public explore.experiential.center.model.Post getNewObject(
		long primaryKey) {

		return _postLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _postLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _postLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the post with the primary key.
	 *
	 * @param postId the primary key of the post
	 * @return the post
	 * @throws PortalException if a post with the primary key could not be found
	 */
	@Override
	public explore.experiential.center.model.Post getPost(long postId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _postLocalService.getPost(postId);
	}

	/**
	 * Returns the post matching the UUID and group.
	 *
	 * @param uuid the post's UUID
	 * @param groupId the primary key of the group
	 * @return the matching post
	 * @throws PortalException if a matching post could not be found
	 */
	@Override
	public explore.experiential.center.model.Post getPostByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _postLocalService.getPostByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public explore.experiential.center.model.Post getPostFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws explore.experiential.center.exception.PostValidateException,
			   javax.portlet.PortletException {

		return _postLocalService.getPostFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the posts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.experiential.center.model.impl.PostModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of posts
	 * @param end the upper bound of the range of posts (not inclusive)
	 * @return the range of posts
	 */
	@Override
	public java.util.List<explore.experiential.center.model.Post> getPosts(
		int start, int end) {

		return _postLocalService.getPosts(start, end);
	}

	/**
	 * Returns all the posts matching the UUID and company.
	 *
	 * @param uuid the UUID of the posts
	 * @param companyId the primary key of the company
	 * @return the matching posts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<explore.experiential.center.model.Post>
		getPostsByUuidAndCompanyId(String uuid, long companyId) {

		return _postLocalService.getPostsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of posts matching the UUID and company.
	 *
	 * @param uuid the UUID of the posts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of posts
	 * @param end the upper bound of the range of posts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching posts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<explore.experiential.center.model.Post>
		getPostsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<explore.experiential.center.model.Post> orderByComparator) {

		return _postLocalService.getPostsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of posts.
	 *
	 * @return the number of posts
	 */
	@Override
	public int getPostsCount() {
		return _postLocalService.getPostsCount();
	}

	@Override
	public explore.experiential.center.model.Post updateEntry(
			explore.experiential.center.model.Post orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   explore.experiential.center.exception.PostValidateException {

		return _postLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public void updateGalleries(
			explore.experiential.center.model.Post entry,
			java.util.List<explore.experiential.center.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_postLocalService.updateGalleries(entry, galleries, serviceContext);
	}

	/**
	 * Updates the post in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param post the post
	 * @return the post that was updated
	 */
	@Override
	public explore.experiential.center.model.Post updatePost(
		explore.experiential.center.model.Post post) {

		return _postLocalService.updatePost(post);
	}

	@Override
	public PostLocalService getWrappedService() {
		return _postLocalService;
	}

	@Override
	public void setWrappedService(PostLocalService postLocalService) {
		_postLocalService = postLocalService;
	}

	private PostLocalService _postLocalService;

}