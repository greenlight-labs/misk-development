/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Category}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Category
 * @generated
 */
public class CategoryWrapper
	extends BaseModelWrapper<Category>
	implements Category, ModelWrapper<Category> {

	public CategoryWrapper(Category category) {
		super(category);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("categoryId", getCategoryId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("name", getName());
		attributes.put("image", getImage());
		attributes.put("heading", getHeading());
		attributes.put("description", getDescription());
		attributes.put("estimatedTourTimeLabel", getEstimatedTourTimeLabel());
		attributes.put("estimatedTourTime", getEstimatedTourTime());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String heading = (String)attributes.get("heading");

		if (heading != null) {
			setHeading(heading);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String estimatedTourTimeLabel = (String)attributes.get(
			"estimatedTourTimeLabel");

		if (estimatedTourTimeLabel != null) {
			setEstimatedTourTimeLabel(estimatedTourTimeLabel);
		}

		String estimatedTourTime = (String)attributes.get("estimatedTourTime");

		if (estimatedTourTime != null) {
			setEstimatedTourTime(estimatedTourTime);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the category ID of this category.
	 *
	 * @return the category ID of this category
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this category.
	 *
	 * @return the company ID of this category
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this category.
	 *
	 * @return the create date of this category
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this category.
	 *
	 * @return the description of this category
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this category
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this category
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this category
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this category.
	 *
	 * @return the locales and localized descriptions of this category
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the estimated tour time of this category.
	 *
	 * @return the estimated tour time of this category
	 */
	@Override
	public String getEstimatedTourTime() {
		return model.getEstimatedTourTime();
	}

	/**
	 * Returns the localized estimated tour time of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized estimated tour time of this category
	 */
	@Override
	public String getEstimatedTourTime(java.util.Locale locale) {
		return model.getEstimatedTourTime(locale);
	}

	/**
	 * Returns the localized estimated tour time of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getEstimatedTourTime(
		java.util.Locale locale, boolean useDefault) {

		return model.getEstimatedTourTime(locale, useDefault);
	}

	/**
	 * Returns the localized estimated tour time of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized estimated tour time of this category
	 */
	@Override
	public String getEstimatedTourTime(String languageId) {
		return model.getEstimatedTourTime(languageId);
	}

	/**
	 * Returns the localized estimated tour time of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time of this category
	 */
	@Override
	public String getEstimatedTourTime(String languageId, boolean useDefault) {
		return model.getEstimatedTourTime(languageId, useDefault);
	}

	@Override
	public String getEstimatedTourTimeCurrentLanguageId() {
		return model.getEstimatedTourTimeCurrentLanguageId();
	}

	@Override
	public String getEstimatedTourTimeCurrentValue() {
		return model.getEstimatedTourTimeCurrentValue();
	}

	/**
	 * Returns the estimated tour time label of this category.
	 *
	 * @return the estimated tour time label of this category
	 */
	@Override
	public String getEstimatedTourTimeLabel() {
		return model.getEstimatedTourTimeLabel();
	}

	/**
	 * Returns the localized estimated tour time label of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized estimated tour time label of this category
	 */
	@Override
	public String getEstimatedTourTimeLabel(java.util.Locale locale) {
		return model.getEstimatedTourTimeLabel(locale);
	}

	/**
	 * Returns the localized estimated tour time label of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time label of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getEstimatedTourTimeLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getEstimatedTourTimeLabel(locale, useDefault);
	}

	/**
	 * Returns the localized estimated tour time label of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized estimated tour time label of this category
	 */
	@Override
	public String getEstimatedTourTimeLabel(String languageId) {
		return model.getEstimatedTourTimeLabel(languageId);
	}

	/**
	 * Returns the localized estimated tour time label of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time label of this category
	 */
	@Override
	public String getEstimatedTourTimeLabel(
		String languageId, boolean useDefault) {

		return model.getEstimatedTourTimeLabel(languageId, useDefault);
	}

	@Override
	public String getEstimatedTourTimeLabelCurrentLanguageId() {
		return model.getEstimatedTourTimeLabelCurrentLanguageId();
	}

	@Override
	public String getEstimatedTourTimeLabelCurrentValue() {
		return model.getEstimatedTourTimeLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized estimated tour time labels of this category.
	 *
	 * @return the locales and localized estimated tour time labels of this category
	 */
	@Override
	public Map<java.util.Locale, String> getEstimatedTourTimeLabelMap() {
		return model.getEstimatedTourTimeLabelMap();
	}

	/**
	 * Returns a map of the locales and localized estimated tour times of this category.
	 *
	 * @return the locales and localized estimated tour times of this category
	 */
	@Override
	public Map<java.util.Locale, String> getEstimatedTourTimeMap() {
		return model.getEstimatedTourTimeMap();
	}

	/**
	 * Returns the group ID of this category.
	 *
	 * @return the group ID of this category
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the heading of this category.
	 *
	 * @return the heading of this category
	 */
	@Override
	public String getHeading() {
		return model.getHeading();
	}

	/**
	 * Returns the localized heading of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized heading of this category
	 */
	@Override
	public String getHeading(java.util.Locale locale) {
		return model.getHeading(locale);
	}

	/**
	 * Returns the localized heading of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized heading of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getHeading(java.util.Locale locale, boolean useDefault) {
		return model.getHeading(locale, useDefault);
	}

	/**
	 * Returns the localized heading of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized heading of this category
	 */
	@Override
	public String getHeading(String languageId) {
		return model.getHeading(languageId);
	}

	/**
	 * Returns the localized heading of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized heading of this category
	 */
	@Override
	public String getHeading(String languageId, boolean useDefault) {
		return model.getHeading(languageId, useDefault);
	}

	@Override
	public String getHeadingCurrentLanguageId() {
		return model.getHeadingCurrentLanguageId();
	}

	@Override
	public String getHeadingCurrentValue() {
		return model.getHeadingCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized headings of this category.
	 *
	 * @return the locales and localized headings of this category
	 */
	@Override
	public Map<java.util.Locale, String> getHeadingMap() {
		return model.getHeadingMap();
	}

	/**
	 * Returns the image of this category.
	 *
	 * @return the image of this category
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the localized image of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this category
	 */
	@Override
	public String getImage(java.util.Locale locale) {
		return model.getImage(locale);
	}

	/**
	 * Returns the localized image of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getImage(java.util.Locale locale, boolean useDefault) {
		return model.getImage(locale, useDefault);
	}

	/**
	 * Returns the localized image of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this category
	 */
	@Override
	public String getImage(String languageId) {
		return model.getImage(languageId);
	}

	/**
	 * Returns the localized image of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this category
	 */
	@Override
	public String getImage(String languageId, boolean useDefault) {
		return model.getImage(languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return model.getImageCurrentLanguageId();
	}

	@Override
	public String getImageCurrentValue() {
		return model.getImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized images of this category.
	 *
	 * @return the locales and localized images of this category
	 */
	@Override
	public Map<java.util.Locale, String> getImageMap() {
		return model.getImageMap();
	}

	/**
	 * Returns the modified date of this category.
	 *
	 * @return the modified date of this category
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this category.
	 *
	 * @return the name of this category
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the localized name of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized name of this category
	 */
	@Override
	public String getName(java.util.Locale locale) {
		return model.getName(locale);
	}

	/**
	 * Returns the localized name of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this category. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getName(java.util.Locale locale, boolean useDefault) {
		return model.getName(locale, useDefault);
	}

	/**
	 * Returns the localized name of this category in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized name of this category
	 */
	@Override
	public String getName(String languageId) {
		return model.getName(languageId);
	}

	/**
	 * Returns the localized name of this category in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this category
	 */
	@Override
	public String getName(String languageId, boolean useDefault) {
		return model.getName(languageId, useDefault);
	}

	@Override
	public String getNameCurrentLanguageId() {
		return model.getNameCurrentLanguageId();
	}

	@Override
	public String getNameCurrentValue() {
		return model.getNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized names of this category.
	 *
	 * @return the locales and localized names of this category
	 */
	@Override
	public Map<java.util.Locale, String> getNameMap() {
		return model.getNameMap();
	}

	/**
	 * Returns the primary key of this category.
	 *
	 * @return the primary key of this category
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this category.
	 *
	 * @return the user ID of this category
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this category.
	 *
	 * @return the user name of this category
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this category.
	 *
	 * @return the user uuid of this category
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this category.
	 *
	 * @return the uuid of this category
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the category ID of this category.
	 *
	 * @param categoryId the category ID of this category
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this category.
	 *
	 * @param companyId the company ID of this category
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this category.
	 *
	 * @param createDate the create date of this category
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this category.
	 *
	 * @param description the description of this category
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this category in the language.
	 *
	 * @param description the localized description of this category
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this category in the language, and sets the default locale.
	 *
	 * @param description the localized description of this category
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this category from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this category
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this category from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this category
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the estimated tour time of this category.
	 *
	 * @param estimatedTourTime the estimated tour time of this category
	 */
	@Override
	public void setEstimatedTourTime(String estimatedTourTime) {
		model.setEstimatedTourTime(estimatedTourTime);
	}

	/**
	 * Sets the localized estimated tour time of this category in the language.
	 *
	 * @param estimatedTourTime the localized estimated tour time of this category
	 * @param locale the locale of the language
	 */
	@Override
	public void setEstimatedTourTime(
		String estimatedTourTime, java.util.Locale locale) {

		model.setEstimatedTourTime(estimatedTourTime, locale);
	}

	/**
	 * Sets the localized estimated tour time of this category in the language, and sets the default locale.
	 *
	 * @param estimatedTourTime the localized estimated tour time of this category
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTime(
		String estimatedTourTime, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTime(estimatedTourTime, locale, defaultLocale);
	}

	@Override
	public void setEstimatedTourTimeCurrentLanguageId(String languageId) {
		model.setEstimatedTourTimeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the estimated tour time label of this category.
	 *
	 * @param estimatedTourTimeLabel the estimated tour time label of this category
	 */
	@Override
	public void setEstimatedTourTimeLabel(String estimatedTourTimeLabel) {
		model.setEstimatedTourTimeLabel(estimatedTourTimeLabel);
	}

	/**
	 * Sets the localized estimated tour time label of this category in the language.
	 *
	 * @param estimatedTourTimeLabel the localized estimated tour time label of this category
	 * @param locale the locale of the language
	 */
	@Override
	public void setEstimatedTourTimeLabel(
		String estimatedTourTimeLabel, java.util.Locale locale) {

		model.setEstimatedTourTimeLabel(estimatedTourTimeLabel, locale);
	}

	/**
	 * Sets the localized estimated tour time label of this category in the language, and sets the default locale.
	 *
	 * @param estimatedTourTimeLabel the localized estimated tour time label of this category
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTimeLabel(
		String estimatedTourTimeLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTimeLabel(
			estimatedTourTimeLabel, locale, defaultLocale);
	}

	@Override
	public void setEstimatedTourTimeLabelCurrentLanguageId(String languageId) {
		model.setEstimatedTourTimeLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized estimated tour time labels of this category from the map of locales and localized estimated tour time labels.
	 *
	 * @param estimatedTourTimeLabelMap the locales and localized estimated tour time labels of this category
	 */
	@Override
	public void setEstimatedTourTimeLabelMap(
		Map<java.util.Locale, String> estimatedTourTimeLabelMap) {

		model.setEstimatedTourTimeLabelMap(estimatedTourTimeLabelMap);
	}

	/**
	 * Sets the localized estimated tour time labels of this category from the map of locales and localized estimated tour time labels, and sets the default locale.
	 *
	 * @param estimatedTourTimeLabelMap the locales and localized estimated tour time labels of this category
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTimeLabelMap(
		Map<java.util.Locale, String> estimatedTourTimeLabelMap,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTimeLabelMap(
			estimatedTourTimeLabelMap, defaultLocale);
	}

	/**
	 * Sets the localized estimated tour times of this category from the map of locales and localized estimated tour times.
	 *
	 * @param estimatedTourTimeMap the locales and localized estimated tour times of this category
	 */
	@Override
	public void setEstimatedTourTimeMap(
		Map<java.util.Locale, String> estimatedTourTimeMap) {

		model.setEstimatedTourTimeMap(estimatedTourTimeMap);
	}

	/**
	 * Sets the localized estimated tour times of this category from the map of locales and localized estimated tour times, and sets the default locale.
	 *
	 * @param estimatedTourTimeMap the locales and localized estimated tour times of this category
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTimeMap(
		Map<java.util.Locale, String> estimatedTourTimeMap,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTimeMap(estimatedTourTimeMap, defaultLocale);
	}

	/**
	 * Sets the group ID of this category.
	 *
	 * @param groupId the group ID of this category
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the heading of this category.
	 *
	 * @param heading the heading of this category
	 */
	@Override
	public void setHeading(String heading) {
		model.setHeading(heading);
	}

	/**
	 * Sets the localized heading of this category in the language.
	 *
	 * @param heading the localized heading of this category
	 * @param locale the locale of the language
	 */
	@Override
	public void setHeading(String heading, java.util.Locale locale) {
		model.setHeading(heading, locale);
	}

	/**
	 * Sets the localized heading of this category in the language, and sets the default locale.
	 *
	 * @param heading the localized heading of this category
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setHeading(
		String heading, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setHeading(heading, locale, defaultLocale);
	}

	@Override
	public void setHeadingCurrentLanguageId(String languageId) {
		model.setHeadingCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized headings of this category from the map of locales and localized headings.
	 *
	 * @param headingMap the locales and localized headings of this category
	 */
	@Override
	public void setHeadingMap(Map<java.util.Locale, String> headingMap) {
		model.setHeadingMap(headingMap);
	}

	/**
	 * Sets the localized headings of this category from the map of locales and localized headings, and sets the default locale.
	 *
	 * @param headingMap the locales and localized headings of this category
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setHeadingMap(
		Map<java.util.Locale, String> headingMap,
		java.util.Locale defaultLocale) {

		model.setHeadingMap(headingMap, defaultLocale);
	}

	/**
	 * Sets the image of this category.
	 *
	 * @param image the image of this category
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the localized image of this category in the language.
	 *
	 * @param image the localized image of this category
	 * @param locale the locale of the language
	 */
	@Override
	public void setImage(String image, java.util.Locale locale) {
		model.setImage(image, locale);
	}

	/**
	 * Sets the localized image of this category in the language, and sets the default locale.
	 *
	 * @param image the localized image of this category
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImage(
		String image, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setImage(image, locale, defaultLocale);
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		model.setImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized images of this category from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this category
	 */
	@Override
	public void setImageMap(Map<java.util.Locale, String> imageMap) {
		model.setImageMap(imageMap);
	}

	/**
	 * Sets the localized images of this category from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this category
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImageMap(
		Map<java.util.Locale, String> imageMap,
		java.util.Locale defaultLocale) {

		model.setImageMap(imageMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this category.
	 *
	 * @param modifiedDate the modified date of this category
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this category.
	 *
	 * @param name the name of this category
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the localized name of this category in the language.
	 *
	 * @param name the localized name of this category
	 * @param locale the locale of the language
	 */
	@Override
	public void setName(String name, java.util.Locale locale) {
		model.setName(name, locale);
	}

	/**
	 * Sets the localized name of this category in the language, and sets the default locale.
	 *
	 * @param name the localized name of this category
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setName(
		String name, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setName(name, locale, defaultLocale);
	}

	@Override
	public void setNameCurrentLanguageId(String languageId) {
		model.setNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized names of this category from the map of locales and localized names.
	 *
	 * @param nameMap the locales and localized names of this category
	 */
	@Override
	public void setNameMap(Map<java.util.Locale, String> nameMap) {
		model.setNameMap(nameMap);
	}

	/**
	 * Sets the localized names of this category from the map of locales and localized names, and sets the default locale.
	 *
	 * @param nameMap the locales and localized names of this category
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setNameMap(
		Map<java.util.Locale, String> nameMap, java.util.Locale defaultLocale) {

		model.setNameMap(nameMap, defaultLocale);
	}

	/**
	 * Sets the primary key of this category.
	 *
	 * @param primaryKey the primary key of this category
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this category.
	 *
	 * @param userId the user ID of this category
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this category.
	 *
	 * @param userName the user name of this category
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this category.
	 *
	 * @param userUuid the user uuid of this category
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this category.
	 *
	 * @param uuid the uuid of this category
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected CategoryWrapper wrap(Category category) {
		return new CategoryWrapper(category);
	}

}