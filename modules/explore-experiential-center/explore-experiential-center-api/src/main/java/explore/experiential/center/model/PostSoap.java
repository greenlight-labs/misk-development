/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link explore.experiential.center.service.http.PostServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class PostSoap implements Serializable {

	public static PostSoap toSoapModel(Post model) {
		PostSoap soapModel = new PostSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setPostId(model.getPostId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setOrderNo(model.getOrderNo());
		soapModel.setName(model.getName());
		soapModel.setDescription(model.getDescription());
		soapModel.setEstimatedTourTimeLabel(model.getEstimatedTourTimeLabel());
		soapModel.setEstimatedTourTime(model.getEstimatedTourTime());
		soapModel.setType(model.getType());
		soapModel.setVideo(model.getVideo());
		soapModel.setImage(model.getImage());
		soapModel.setButtonLabel(model.getButtonLabel());
		soapModel.setAudio(model.getAudio());
		soapModel.setAudioCatColor(model.getAudioCatColor());
		soapModel.setAudioCatName(model.getAudioCatName());
		soapModel.setAudioName(model.getAudioName());
		soapModel.setAudioProfession(model.getAudioProfession());
		soapModel.setAudioLocation(model.getAudioLocation());

		return soapModel;
	}

	public static PostSoap[] toSoapModels(Post[] models) {
		PostSoap[] soapModels = new PostSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PostSoap[][] toSoapModels(Post[][] models) {
		PostSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PostSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PostSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PostSoap[] toSoapModels(List<Post> models) {
		List<PostSoap> soapModels = new ArrayList<PostSoap>(models.size());

		for (Post model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PostSoap[soapModels.size()]);
	}

	public PostSoap() {
	}

	public long getPrimaryKey() {
		return _postId;
	}

	public void setPrimaryKey(long pk) {
		setPostId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getPostId() {
		return _postId;
	}

	public void setPostId(long postId) {
		_postId = postId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public long getOrderNo() {
		return _orderNo;
	}

	public void setOrderNo(long orderNo) {
		_orderNo = orderNo;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getEstimatedTourTimeLabel() {
		return _estimatedTourTimeLabel;
	}

	public void setEstimatedTourTimeLabel(String estimatedTourTimeLabel) {
		_estimatedTourTimeLabel = estimatedTourTimeLabel;
	}

	public String getEstimatedTourTime() {
		return _estimatedTourTime;
	}

	public void setEstimatedTourTime(String estimatedTourTime) {
		_estimatedTourTime = estimatedTourTime;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		_type = type;
	}

	public String getVideo() {
		return _video;
	}

	public void setVideo(String video) {
		_video = video;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getButtonLabel() {
		return _buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		_buttonLabel = buttonLabel;
	}

	public String getAudio() {
		return _audio;
	}

	public void setAudio(String audio) {
		_audio = audio;
	}

	public String getAudioCatColor() {
		return _audioCatColor;
	}

	public void setAudioCatColor(String audioCatColor) {
		_audioCatColor = audioCatColor;
	}

	public String getAudioCatName() {
		return _audioCatName;
	}

	public void setAudioCatName(String audioCatName) {
		_audioCatName = audioCatName;
	}

	public String getAudioName() {
		return _audioName;
	}

	public void setAudioName(String audioName) {
		_audioName = audioName;
	}

	public String getAudioProfession() {
		return _audioProfession;
	}

	public void setAudioProfession(String audioProfession) {
		_audioProfession = audioProfession;
	}

	public String getAudioLocation() {
		return _audioLocation;
	}

	public void setAudioLocation(String audioLocation) {
		_audioLocation = audioLocation;
	}

	private String _uuid;
	private long _postId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _categoryId;
	private long _orderNo;
	private String _name;
	private String _description;
	private String _estimatedTourTimeLabel;
	private String _estimatedTourTime;
	private String _type;
	private String _video;
	private String _image;
	private String _buttonLabel;
	private String _audio;
	private String _audioCatColor;
	private String _audioCatName;
	private String _audioName;
	private String _audioProfession;
	private String _audioLocation;

}