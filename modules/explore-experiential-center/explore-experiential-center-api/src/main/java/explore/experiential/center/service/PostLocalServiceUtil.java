/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import explore.experiential.center.model.Post;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Post. This utility wraps
 * <code>explore.experiential.center.service.impl.PostLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PostLocalService
 * @generated
 */
public class PostLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>explore.experiential.center.service.impl.PostLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Post addEntry(
			Post orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws explore.experiential.center.exception.PostValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static void addEntryGallery(
			Post orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryGallery(orgEntry, request);
	}

	/**
	 * Adds the post to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param post the post
	 * @return the post that was added
	 */
	public static Post addPost(Post post) {
		return getService().addPost(post);
	}

	public static int countByCategoryId(long type) {
		return getService().countByCategoryId(type);
	}

	public static int countByGroupId(long groupId) {
		return getService().countByGroupId(groupId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new post with the primary key. Does not add the post to the database.
	 *
	 * @param postId the primary key for the new post
	 * @return the new post
	 */
	public static Post createPost(long postId) {
		return getService().createPost(postId);
	}

	public static Post deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the post with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param postId the primary key of the post
	 * @return the post that was removed
	 * @throws PortalException if a post with the primary key could not be found
	 */
	public static Post deletePost(long postId) throws PortalException {
		return getService().deletePost(postId);
	}

	/**
	 * Deletes the post from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param post the post
	 * @return the post that was removed
	 */
	public static Post deletePost(Post post) {
		return getService().deletePost(post);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.experiential.center.model.impl.PostModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.experiential.center.model.impl.PostModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Post fetchPost(long postId) {
		return getService().fetchPost(postId);
	}

	/**
	 * Returns the post matching the UUID and group.
	 *
	 * @param uuid the post's UUID
	 * @param groupId the primary key of the group
	 * @return the matching post, or <code>null</code> if a matching post could not be found
	 */
	public static Post fetchPostByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchPostByUuidAndGroupId(uuid, groupId);
	}

	public static List<Post> findByCategoryId(long type) {
		return getService().findByCategoryId(type);
	}

	public static List<Post> findByCategoryId(long type, int start, int end) {
		return getService().findByCategoryId(type, start, end);
	}

	public static List<Post> findByCategoryId(
		long type, int start, int end, OrderByComparator<Post> obc) {

		return getService().findByCategoryId(type, start, end, obc);
	}

	public static List<Post> findByGroupId(long groupId) {
		return getService().findByGroupId(groupId);
	}

	public static List<Post> findByGroupId(long groupId, int start, int end) {
		return getService().findByGroupId(groupId, start, end);
	}

	public static List<Post> findByGroupId(
		long groupId, int start, int end, OrderByComparator<Post> obc) {

		return getService().findByGroupId(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static List<explore.experiential.center.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getGalleries(actionRequest);
	}

	public static List<explore.experiential.center.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		List<explore.experiential.center.model.Gallery> defaultGalleries) {

		return getService().getGalleries(actionRequest, defaultGalleries);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Post getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the post with the primary key.
	 *
	 * @param postId the primary key of the post
	 * @return the post
	 * @throws PortalException if a post with the primary key could not be found
	 */
	public static Post getPost(long postId) throws PortalException {
		return getService().getPost(postId);
	}

	/**
	 * Returns the post matching the UUID and group.
	 *
	 * @param uuid the post's UUID
	 * @param groupId the primary key of the group
	 * @return the matching post
	 * @throws PortalException if a matching post could not be found
	 */
	public static Post getPostByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getPostByUuidAndGroupId(uuid, groupId);
	}

	public static Post getPostFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws explore.experiential.center.exception.PostValidateException,
			   javax.portlet.PortletException {

		return getService().getPostFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the posts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>explore.experiential.center.model.impl.PostModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of posts
	 * @param end the upper bound of the range of posts (not inclusive)
	 * @return the range of posts
	 */
	public static List<Post> getPosts(int start, int end) {
		return getService().getPosts(start, end);
	}

	/**
	 * Returns all the posts matching the UUID and company.
	 *
	 * @param uuid the UUID of the posts
	 * @param companyId the primary key of the company
	 * @return the matching posts, or an empty list if no matches were found
	 */
	public static List<Post> getPostsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getPostsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of posts matching the UUID and company.
	 *
	 * @param uuid the UUID of the posts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of posts
	 * @param end the upper bound of the range of posts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching posts, or an empty list if no matches were found
	 */
	public static List<Post> getPostsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Post> orderByComparator) {

		return getService().getPostsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of posts.
	 *
	 * @return the number of posts
	 */
	public static int getPostsCount() {
		return getService().getPostsCount();
	}

	public static Post updateEntry(
			Post orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws explore.experiential.center.exception.PostValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static void updateGalleries(
			Post entry,
			List<explore.experiential.center.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateGalleries(entry, galleries, serviceContext);
	}

	/**
	 * Updates the post in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PostLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param post the post
	 * @return the post that was updated
	 */
	public static Post updatePost(Post post) {
		return getService().updatePost(post);
	}

	public static PostLocalService getService() {
		return _service;
	}

	private static volatile PostLocalService _service;

}