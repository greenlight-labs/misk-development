/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package explore.experiential.center.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Post}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Post
 * @generated
 */
public class PostWrapper
	extends BaseModelWrapper<Post> implements ModelWrapper<Post>, Post {

	public PostWrapper(Post post) {
		super(post);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("postId", getPostId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("categoryId", getCategoryId());
		attributes.put("orderNo", getOrderNo());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("estimatedTourTimeLabel", getEstimatedTourTimeLabel());
		attributes.put("estimatedTourTime", getEstimatedTourTime());
		attributes.put("type", getType());
		attributes.put("video", getVideo());
		attributes.put("image", getImage());
		attributes.put("buttonLabel", getButtonLabel());
		attributes.put("audio", getAudio());
		attributes.put("audioCatColor", getAudioCatColor());
		attributes.put("audioCatName", getAudioCatName());
		attributes.put("audioName", getAudioName());
		attributes.put("audioProfession", getAudioProfession());
		attributes.put("audioLocation", getAudioLocation());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long postId = (Long)attributes.get("postId");

		if (postId != null) {
			setPostId(postId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long orderNo = (Long)attributes.get("orderNo");

		if (orderNo != null) {
			setOrderNo(orderNo);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String estimatedTourTimeLabel = (String)attributes.get(
			"estimatedTourTimeLabel");

		if (estimatedTourTimeLabel != null) {
			setEstimatedTourTimeLabel(estimatedTourTimeLabel);
		}

		String estimatedTourTime = (String)attributes.get("estimatedTourTime");

		if (estimatedTourTime != null) {
			setEstimatedTourTime(estimatedTourTime);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String video = (String)attributes.get("video");

		if (video != null) {
			setVideo(video);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String buttonLabel = (String)attributes.get("buttonLabel");

		if (buttonLabel != null) {
			setButtonLabel(buttonLabel);
		}

		String audio = (String)attributes.get("audio");

		if (audio != null) {
			setAudio(audio);
		}

		String audioCatColor = (String)attributes.get("audioCatColor");

		if (audioCatColor != null) {
			setAudioCatColor(audioCatColor);
		}

		String audioCatName = (String)attributes.get("audioCatName");

		if (audioCatName != null) {
			setAudioCatName(audioCatName);
		}

		String audioName = (String)attributes.get("audioName");

		if (audioName != null) {
			setAudioName(audioName);
		}

		String audioProfession = (String)attributes.get("audioProfession");

		if (audioProfession != null) {
			setAudioProfession(audioProfession);
		}

		String audioLocation = (String)attributes.get("audioLocation");

		if (audioLocation != null) {
			setAudioLocation(audioLocation);
		}
	}

	/**
	 * Returns the audio of this post.
	 *
	 * @return the audio of this post
	 */
	@Override
	public String getAudio() {
		return model.getAudio();
	}

	/**
	 * Returns the audio cat color of this post.
	 *
	 * @return the audio cat color of this post
	 */
	@Override
	public String getAudioCatColor() {
		return model.getAudioCatColor();
	}

	/**
	 * Returns the audio cat name of this post.
	 *
	 * @return the audio cat name of this post
	 */
	@Override
	public String getAudioCatName() {
		return model.getAudioCatName();
	}

	/**
	 * Returns the localized audio cat name of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized audio cat name of this post
	 */
	@Override
	public String getAudioCatName(java.util.Locale locale) {
		return model.getAudioCatName(locale);
	}

	/**
	 * Returns the localized audio cat name of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio cat name of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getAudioCatName(java.util.Locale locale, boolean useDefault) {
		return model.getAudioCatName(locale, useDefault);
	}

	/**
	 * Returns the localized audio cat name of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized audio cat name of this post
	 */
	@Override
	public String getAudioCatName(String languageId) {
		return model.getAudioCatName(languageId);
	}

	/**
	 * Returns the localized audio cat name of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio cat name of this post
	 */
	@Override
	public String getAudioCatName(String languageId, boolean useDefault) {
		return model.getAudioCatName(languageId, useDefault);
	}

	@Override
	public String getAudioCatNameCurrentLanguageId() {
		return model.getAudioCatNameCurrentLanguageId();
	}

	@Override
	public String getAudioCatNameCurrentValue() {
		return model.getAudioCatNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized audio cat names of this post.
	 *
	 * @return the locales and localized audio cat names of this post
	 */
	@Override
	public Map<java.util.Locale, String> getAudioCatNameMap() {
		return model.getAudioCatNameMap();
	}

	/**
	 * Returns the audio location of this post.
	 *
	 * @return the audio location of this post
	 */
	@Override
	public String getAudioLocation() {
		return model.getAudioLocation();
	}

	/**
	 * Returns the localized audio location of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized audio location of this post
	 */
	@Override
	public String getAudioLocation(java.util.Locale locale) {
		return model.getAudioLocation(locale);
	}

	/**
	 * Returns the localized audio location of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio location of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getAudioLocation(
		java.util.Locale locale, boolean useDefault) {

		return model.getAudioLocation(locale, useDefault);
	}

	/**
	 * Returns the localized audio location of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized audio location of this post
	 */
	@Override
	public String getAudioLocation(String languageId) {
		return model.getAudioLocation(languageId);
	}

	/**
	 * Returns the localized audio location of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio location of this post
	 */
	@Override
	public String getAudioLocation(String languageId, boolean useDefault) {
		return model.getAudioLocation(languageId, useDefault);
	}

	@Override
	public String getAudioLocationCurrentLanguageId() {
		return model.getAudioLocationCurrentLanguageId();
	}

	@Override
	public String getAudioLocationCurrentValue() {
		return model.getAudioLocationCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized audio locations of this post.
	 *
	 * @return the locales and localized audio locations of this post
	 */
	@Override
	public Map<java.util.Locale, String> getAudioLocationMap() {
		return model.getAudioLocationMap();
	}

	/**
	 * Returns the audio name of this post.
	 *
	 * @return the audio name of this post
	 */
	@Override
	public String getAudioName() {
		return model.getAudioName();
	}

	/**
	 * Returns the localized audio name of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized audio name of this post
	 */
	@Override
	public String getAudioName(java.util.Locale locale) {
		return model.getAudioName(locale);
	}

	/**
	 * Returns the localized audio name of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio name of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getAudioName(java.util.Locale locale, boolean useDefault) {
		return model.getAudioName(locale, useDefault);
	}

	/**
	 * Returns the localized audio name of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized audio name of this post
	 */
	@Override
	public String getAudioName(String languageId) {
		return model.getAudioName(languageId);
	}

	/**
	 * Returns the localized audio name of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio name of this post
	 */
	@Override
	public String getAudioName(String languageId, boolean useDefault) {
		return model.getAudioName(languageId, useDefault);
	}

	@Override
	public String getAudioNameCurrentLanguageId() {
		return model.getAudioNameCurrentLanguageId();
	}

	@Override
	public String getAudioNameCurrentValue() {
		return model.getAudioNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized audio names of this post.
	 *
	 * @return the locales and localized audio names of this post
	 */
	@Override
	public Map<java.util.Locale, String> getAudioNameMap() {
		return model.getAudioNameMap();
	}

	/**
	 * Returns the audio profession of this post.
	 *
	 * @return the audio profession of this post
	 */
	@Override
	public String getAudioProfession() {
		return model.getAudioProfession();
	}

	/**
	 * Returns the localized audio profession of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized audio profession of this post
	 */
	@Override
	public String getAudioProfession(java.util.Locale locale) {
		return model.getAudioProfession(locale);
	}

	/**
	 * Returns the localized audio profession of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio profession of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getAudioProfession(
		java.util.Locale locale, boolean useDefault) {

		return model.getAudioProfession(locale, useDefault);
	}

	/**
	 * Returns the localized audio profession of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized audio profession of this post
	 */
	@Override
	public String getAudioProfession(String languageId) {
		return model.getAudioProfession(languageId);
	}

	/**
	 * Returns the localized audio profession of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio profession of this post
	 */
	@Override
	public String getAudioProfession(String languageId, boolean useDefault) {
		return model.getAudioProfession(languageId, useDefault);
	}

	@Override
	public String getAudioProfessionCurrentLanguageId() {
		return model.getAudioProfessionCurrentLanguageId();
	}

	@Override
	public String getAudioProfessionCurrentValue() {
		return model.getAudioProfessionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized audio professions of this post.
	 *
	 * @return the locales and localized audio professions of this post
	 */
	@Override
	public Map<java.util.Locale, String> getAudioProfessionMap() {
		return model.getAudioProfessionMap();
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the button label of this post.
	 *
	 * @return the button label of this post
	 */
	@Override
	public String getButtonLabel() {
		return model.getButtonLabel();
	}

	/**
	 * Returns the localized button label of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button label of this post
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale) {
		return model.getButtonLabel(locale);
	}

	/**
	 * Returns the localized button label of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale, boolean useDefault) {
		return model.getButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized button label of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button label of this post
	 */
	@Override
	public String getButtonLabel(String languageId) {
		return model.getButtonLabel(languageId);
	}

	/**
	 * Returns the localized button label of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this post
	 */
	@Override
	public String getButtonLabel(String languageId, boolean useDefault) {
		return model.getButtonLabel(languageId, useDefault);
	}

	@Override
	public String getButtonLabelCurrentLanguageId() {
		return model.getButtonLabelCurrentLanguageId();
	}

	@Override
	public String getButtonLabelCurrentValue() {
		return model.getButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized button labels of this post.
	 *
	 * @return the locales and localized button labels of this post
	 */
	@Override
	public Map<java.util.Locale, String> getButtonLabelMap() {
		return model.getButtonLabelMap();
	}

	/**
	 * Returns the category ID of this post.
	 *
	 * @return the category ID of this post
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this post.
	 *
	 * @return the company ID of this post
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this post.
	 *
	 * @return the create date of this post
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this post.
	 *
	 * @return the description of this post
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this post
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this post
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this post
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this post.
	 *
	 * @return the locales and localized descriptions of this post
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the estimated tour time of this post.
	 *
	 * @return the estimated tour time of this post
	 */
	@Override
	public String getEstimatedTourTime() {
		return model.getEstimatedTourTime();
	}

	/**
	 * Returns the localized estimated tour time of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized estimated tour time of this post
	 */
	@Override
	public String getEstimatedTourTime(java.util.Locale locale) {
		return model.getEstimatedTourTime(locale);
	}

	/**
	 * Returns the localized estimated tour time of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getEstimatedTourTime(
		java.util.Locale locale, boolean useDefault) {

		return model.getEstimatedTourTime(locale, useDefault);
	}

	/**
	 * Returns the localized estimated tour time of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized estimated tour time of this post
	 */
	@Override
	public String getEstimatedTourTime(String languageId) {
		return model.getEstimatedTourTime(languageId);
	}

	/**
	 * Returns the localized estimated tour time of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time of this post
	 */
	@Override
	public String getEstimatedTourTime(String languageId, boolean useDefault) {
		return model.getEstimatedTourTime(languageId, useDefault);
	}

	@Override
	public String getEstimatedTourTimeCurrentLanguageId() {
		return model.getEstimatedTourTimeCurrentLanguageId();
	}

	@Override
	public String getEstimatedTourTimeCurrentValue() {
		return model.getEstimatedTourTimeCurrentValue();
	}

	/**
	 * Returns the estimated tour time label of this post.
	 *
	 * @return the estimated tour time label of this post
	 */
	@Override
	public String getEstimatedTourTimeLabel() {
		return model.getEstimatedTourTimeLabel();
	}

	/**
	 * Returns the localized estimated tour time label of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized estimated tour time label of this post
	 */
	@Override
	public String getEstimatedTourTimeLabel(java.util.Locale locale) {
		return model.getEstimatedTourTimeLabel(locale);
	}

	/**
	 * Returns the localized estimated tour time label of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time label of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getEstimatedTourTimeLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getEstimatedTourTimeLabel(locale, useDefault);
	}

	/**
	 * Returns the localized estimated tour time label of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized estimated tour time label of this post
	 */
	@Override
	public String getEstimatedTourTimeLabel(String languageId) {
		return model.getEstimatedTourTimeLabel(languageId);
	}

	/**
	 * Returns the localized estimated tour time label of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized estimated tour time label of this post
	 */
	@Override
	public String getEstimatedTourTimeLabel(
		String languageId, boolean useDefault) {

		return model.getEstimatedTourTimeLabel(languageId, useDefault);
	}

	@Override
	public String getEstimatedTourTimeLabelCurrentLanguageId() {
		return model.getEstimatedTourTimeLabelCurrentLanguageId();
	}

	@Override
	public String getEstimatedTourTimeLabelCurrentValue() {
		return model.getEstimatedTourTimeLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized estimated tour time labels of this post.
	 *
	 * @return the locales and localized estimated tour time labels of this post
	 */
	@Override
	public Map<java.util.Locale, String> getEstimatedTourTimeLabelMap() {
		return model.getEstimatedTourTimeLabelMap();
	}

	/**
	 * Returns a map of the locales and localized estimated tour times of this post.
	 *
	 * @return the locales and localized estimated tour times of this post
	 */
	@Override
	public Map<java.util.Locale, String> getEstimatedTourTimeMap() {
		return model.getEstimatedTourTimeMap();
	}

	/**
	 * Returns the group ID of this post.
	 *
	 * @return the group ID of this post
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the image of this post.
	 *
	 * @return the image of this post
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the modified date of this post.
	 *
	 * @return the modified date of this post
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this post.
	 *
	 * @return the name of this post
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the localized name of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized name of this post
	 */
	@Override
	public String getName(java.util.Locale locale) {
		return model.getName(locale);
	}

	/**
	 * Returns the localized name of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this post. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getName(java.util.Locale locale, boolean useDefault) {
		return model.getName(locale, useDefault);
	}

	/**
	 * Returns the localized name of this post in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized name of this post
	 */
	@Override
	public String getName(String languageId) {
		return model.getName(languageId);
	}

	/**
	 * Returns the localized name of this post in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this post
	 */
	@Override
	public String getName(String languageId, boolean useDefault) {
		return model.getName(languageId, useDefault);
	}

	@Override
	public String getNameCurrentLanguageId() {
		return model.getNameCurrentLanguageId();
	}

	@Override
	public String getNameCurrentValue() {
		return model.getNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized names of this post.
	 *
	 * @return the locales and localized names of this post
	 */
	@Override
	public Map<java.util.Locale, String> getNameMap() {
		return model.getNameMap();
	}

	/**
	 * Returns the order no of this post.
	 *
	 * @return the order no of this post
	 */
	@Override
	public long getOrderNo() {
		return model.getOrderNo();
	}

	/**
	 * Returns the post ID of this post.
	 *
	 * @return the post ID of this post
	 */
	@Override
	public long getPostId() {
		return model.getPostId();
	}

	/**
	 * Returns the primary key of this post.
	 *
	 * @return the primary key of this post
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the type of this post.
	 *
	 * @return the type of this post
	 */
	@Override
	public String getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this post.
	 *
	 * @return the user ID of this post
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this post.
	 *
	 * @return the user name of this post
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this post.
	 *
	 * @return the user uuid of this post
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this post.
	 *
	 * @return the uuid of this post
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the video of this post.
	 *
	 * @return the video of this post
	 */
	@Override
	public String getVideo() {
		return model.getVideo();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the audio of this post.
	 *
	 * @param audio the audio of this post
	 */
	@Override
	public void setAudio(String audio) {
		model.setAudio(audio);
	}

	/**
	 * Sets the audio cat color of this post.
	 *
	 * @param audioCatColor the audio cat color of this post
	 */
	@Override
	public void setAudioCatColor(String audioCatColor) {
		model.setAudioCatColor(audioCatColor);
	}

	/**
	 * Sets the audio cat name of this post.
	 *
	 * @param audioCatName the audio cat name of this post
	 */
	@Override
	public void setAudioCatName(String audioCatName) {
		model.setAudioCatName(audioCatName);
	}

	/**
	 * Sets the localized audio cat name of this post in the language.
	 *
	 * @param audioCatName the localized audio cat name of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setAudioCatName(String audioCatName, java.util.Locale locale) {
		model.setAudioCatName(audioCatName, locale);
	}

	/**
	 * Sets the localized audio cat name of this post in the language, and sets the default locale.
	 *
	 * @param audioCatName the localized audio cat name of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioCatName(
		String audioCatName, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setAudioCatName(audioCatName, locale, defaultLocale);
	}

	@Override
	public void setAudioCatNameCurrentLanguageId(String languageId) {
		model.setAudioCatNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized audio cat names of this post from the map of locales and localized audio cat names.
	 *
	 * @param audioCatNameMap the locales and localized audio cat names of this post
	 */
	@Override
	public void setAudioCatNameMap(
		Map<java.util.Locale, String> audioCatNameMap) {

		model.setAudioCatNameMap(audioCatNameMap);
	}

	/**
	 * Sets the localized audio cat names of this post from the map of locales and localized audio cat names, and sets the default locale.
	 *
	 * @param audioCatNameMap the locales and localized audio cat names of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioCatNameMap(
		Map<java.util.Locale, String> audioCatNameMap,
		java.util.Locale defaultLocale) {

		model.setAudioCatNameMap(audioCatNameMap, defaultLocale);
	}

	/**
	 * Sets the audio location of this post.
	 *
	 * @param audioLocation the audio location of this post
	 */
	@Override
	public void setAudioLocation(String audioLocation) {
		model.setAudioLocation(audioLocation);
	}

	/**
	 * Sets the localized audio location of this post in the language.
	 *
	 * @param audioLocation the localized audio location of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setAudioLocation(
		String audioLocation, java.util.Locale locale) {

		model.setAudioLocation(audioLocation, locale);
	}

	/**
	 * Sets the localized audio location of this post in the language, and sets the default locale.
	 *
	 * @param audioLocation the localized audio location of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioLocation(
		String audioLocation, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setAudioLocation(audioLocation, locale, defaultLocale);
	}

	@Override
	public void setAudioLocationCurrentLanguageId(String languageId) {
		model.setAudioLocationCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized audio locations of this post from the map of locales and localized audio locations.
	 *
	 * @param audioLocationMap the locales and localized audio locations of this post
	 */
	@Override
	public void setAudioLocationMap(
		Map<java.util.Locale, String> audioLocationMap) {

		model.setAudioLocationMap(audioLocationMap);
	}

	/**
	 * Sets the localized audio locations of this post from the map of locales and localized audio locations, and sets the default locale.
	 *
	 * @param audioLocationMap the locales and localized audio locations of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioLocationMap(
		Map<java.util.Locale, String> audioLocationMap,
		java.util.Locale defaultLocale) {

		model.setAudioLocationMap(audioLocationMap, defaultLocale);
	}

	/**
	 * Sets the audio name of this post.
	 *
	 * @param audioName the audio name of this post
	 */
	@Override
	public void setAudioName(String audioName) {
		model.setAudioName(audioName);
	}

	/**
	 * Sets the localized audio name of this post in the language.
	 *
	 * @param audioName the localized audio name of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setAudioName(String audioName, java.util.Locale locale) {
		model.setAudioName(audioName, locale);
	}

	/**
	 * Sets the localized audio name of this post in the language, and sets the default locale.
	 *
	 * @param audioName the localized audio name of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioName(
		String audioName, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setAudioName(audioName, locale, defaultLocale);
	}

	@Override
	public void setAudioNameCurrentLanguageId(String languageId) {
		model.setAudioNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized audio names of this post from the map of locales and localized audio names.
	 *
	 * @param audioNameMap the locales and localized audio names of this post
	 */
	@Override
	public void setAudioNameMap(Map<java.util.Locale, String> audioNameMap) {
		model.setAudioNameMap(audioNameMap);
	}

	/**
	 * Sets the localized audio names of this post from the map of locales and localized audio names, and sets the default locale.
	 *
	 * @param audioNameMap the locales and localized audio names of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioNameMap(
		Map<java.util.Locale, String> audioNameMap,
		java.util.Locale defaultLocale) {

		model.setAudioNameMap(audioNameMap, defaultLocale);
	}

	/**
	 * Sets the audio profession of this post.
	 *
	 * @param audioProfession the audio profession of this post
	 */
	@Override
	public void setAudioProfession(String audioProfession) {
		model.setAudioProfession(audioProfession);
	}

	/**
	 * Sets the localized audio profession of this post in the language.
	 *
	 * @param audioProfession the localized audio profession of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setAudioProfession(
		String audioProfession, java.util.Locale locale) {

		model.setAudioProfession(audioProfession, locale);
	}

	/**
	 * Sets the localized audio profession of this post in the language, and sets the default locale.
	 *
	 * @param audioProfession the localized audio profession of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioProfession(
		String audioProfession, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setAudioProfession(audioProfession, locale, defaultLocale);
	}

	@Override
	public void setAudioProfessionCurrentLanguageId(String languageId) {
		model.setAudioProfessionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized audio professions of this post from the map of locales and localized audio professions.
	 *
	 * @param audioProfessionMap the locales and localized audio professions of this post
	 */
	@Override
	public void setAudioProfessionMap(
		Map<java.util.Locale, String> audioProfessionMap) {

		model.setAudioProfessionMap(audioProfessionMap);
	}

	/**
	 * Sets the localized audio professions of this post from the map of locales and localized audio professions, and sets the default locale.
	 *
	 * @param audioProfessionMap the locales and localized audio professions of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioProfessionMap(
		Map<java.util.Locale, String> audioProfessionMap,
		java.util.Locale defaultLocale) {

		model.setAudioProfessionMap(audioProfessionMap, defaultLocale);
	}

	/**
	 * Sets the button label of this post.
	 *
	 * @param buttonLabel the button label of this post
	 */
	@Override
	public void setButtonLabel(String buttonLabel) {
		model.setButtonLabel(buttonLabel);
	}

	/**
	 * Sets the localized button label of this post in the language.
	 *
	 * @param buttonLabel the localized button label of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setButtonLabel(String buttonLabel, java.util.Locale locale) {
		model.setButtonLabel(buttonLabel, locale);
	}

	/**
	 * Sets the localized button label of this post in the language, and sets the default locale.
	 *
	 * @param buttonLabel the localized button label of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabel(
		String buttonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setButtonLabel(buttonLabel, locale, defaultLocale);
	}

	@Override
	public void setButtonLabelCurrentLanguageId(String languageId) {
		model.setButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized button labels of this post from the map of locales and localized button labels.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this post
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap) {

		model.setButtonLabelMap(buttonLabelMap);
	}

	/**
	 * Sets the localized button labels of this post from the map of locales and localized button labels, and sets the default locale.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap,
		java.util.Locale defaultLocale) {

		model.setButtonLabelMap(buttonLabelMap, defaultLocale);
	}

	/**
	 * Sets the category ID of this post.
	 *
	 * @param categoryId the category ID of this post
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this post.
	 *
	 * @param companyId the company ID of this post
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this post.
	 *
	 * @param createDate the create date of this post
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this post.
	 *
	 * @param description the description of this post
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this post in the language.
	 *
	 * @param description the localized description of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this post in the language, and sets the default locale.
	 *
	 * @param description the localized description of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this post from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this post
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this post from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the estimated tour time of this post.
	 *
	 * @param estimatedTourTime the estimated tour time of this post
	 */
	@Override
	public void setEstimatedTourTime(String estimatedTourTime) {
		model.setEstimatedTourTime(estimatedTourTime);
	}

	/**
	 * Sets the localized estimated tour time of this post in the language.
	 *
	 * @param estimatedTourTime the localized estimated tour time of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setEstimatedTourTime(
		String estimatedTourTime, java.util.Locale locale) {

		model.setEstimatedTourTime(estimatedTourTime, locale);
	}

	/**
	 * Sets the localized estimated tour time of this post in the language, and sets the default locale.
	 *
	 * @param estimatedTourTime the localized estimated tour time of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTime(
		String estimatedTourTime, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTime(estimatedTourTime, locale, defaultLocale);
	}

	@Override
	public void setEstimatedTourTimeCurrentLanguageId(String languageId) {
		model.setEstimatedTourTimeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the estimated tour time label of this post.
	 *
	 * @param estimatedTourTimeLabel the estimated tour time label of this post
	 */
	@Override
	public void setEstimatedTourTimeLabel(String estimatedTourTimeLabel) {
		model.setEstimatedTourTimeLabel(estimatedTourTimeLabel);
	}

	/**
	 * Sets the localized estimated tour time label of this post in the language.
	 *
	 * @param estimatedTourTimeLabel the localized estimated tour time label of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setEstimatedTourTimeLabel(
		String estimatedTourTimeLabel, java.util.Locale locale) {

		model.setEstimatedTourTimeLabel(estimatedTourTimeLabel, locale);
	}

	/**
	 * Sets the localized estimated tour time label of this post in the language, and sets the default locale.
	 *
	 * @param estimatedTourTimeLabel the localized estimated tour time label of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTimeLabel(
		String estimatedTourTimeLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTimeLabel(
			estimatedTourTimeLabel, locale, defaultLocale);
	}

	@Override
	public void setEstimatedTourTimeLabelCurrentLanguageId(String languageId) {
		model.setEstimatedTourTimeLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized estimated tour time labels of this post from the map of locales and localized estimated tour time labels.
	 *
	 * @param estimatedTourTimeLabelMap the locales and localized estimated tour time labels of this post
	 */
	@Override
	public void setEstimatedTourTimeLabelMap(
		Map<java.util.Locale, String> estimatedTourTimeLabelMap) {

		model.setEstimatedTourTimeLabelMap(estimatedTourTimeLabelMap);
	}

	/**
	 * Sets the localized estimated tour time labels of this post from the map of locales and localized estimated tour time labels, and sets the default locale.
	 *
	 * @param estimatedTourTimeLabelMap the locales and localized estimated tour time labels of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTimeLabelMap(
		Map<java.util.Locale, String> estimatedTourTimeLabelMap,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTimeLabelMap(
			estimatedTourTimeLabelMap, defaultLocale);
	}

	/**
	 * Sets the localized estimated tour times of this post from the map of locales and localized estimated tour times.
	 *
	 * @param estimatedTourTimeMap the locales and localized estimated tour times of this post
	 */
	@Override
	public void setEstimatedTourTimeMap(
		Map<java.util.Locale, String> estimatedTourTimeMap) {

		model.setEstimatedTourTimeMap(estimatedTourTimeMap);
	}

	/**
	 * Sets the localized estimated tour times of this post from the map of locales and localized estimated tour times, and sets the default locale.
	 *
	 * @param estimatedTourTimeMap the locales and localized estimated tour times of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setEstimatedTourTimeMap(
		Map<java.util.Locale, String> estimatedTourTimeMap,
		java.util.Locale defaultLocale) {

		model.setEstimatedTourTimeMap(estimatedTourTimeMap, defaultLocale);
	}

	/**
	 * Sets the group ID of this post.
	 *
	 * @param groupId the group ID of this post
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the image of this post.
	 *
	 * @param image the image of this post
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the modified date of this post.
	 *
	 * @param modifiedDate the modified date of this post
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this post.
	 *
	 * @param name the name of this post
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the localized name of this post in the language.
	 *
	 * @param name the localized name of this post
	 * @param locale the locale of the language
	 */
	@Override
	public void setName(String name, java.util.Locale locale) {
		model.setName(name, locale);
	}

	/**
	 * Sets the localized name of this post in the language, and sets the default locale.
	 *
	 * @param name the localized name of this post
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setName(
		String name, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setName(name, locale, defaultLocale);
	}

	@Override
	public void setNameCurrentLanguageId(String languageId) {
		model.setNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized names of this post from the map of locales and localized names.
	 *
	 * @param nameMap the locales and localized names of this post
	 */
	@Override
	public void setNameMap(Map<java.util.Locale, String> nameMap) {
		model.setNameMap(nameMap);
	}

	/**
	 * Sets the localized names of this post from the map of locales and localized names, and sets the default locale.
	 *
	 * @param nameMap the locales and localized names of this post
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setNameMap(
		Map<java.util.Locale, String> nameMap, java.util.Locale defaultLocale) {

		model.setNameMap(nameMap, defaultLocale);
	}

	/**
	 * Sets the order no of this post.
	 *
	 * @param orderNo the order no of this post
	 */
	@Override
	public void setOrderNo(long orderNo) {
		model.setOrderNo(orderNo);
	}

	/**
	 * Sets the post ID of this post.
	 *
	 * @param postId the post ID of this post
	 */
	@Override
	public void setPostId(long postId) {
		model.setPostId(postId);
	}

	/**
	 * Sets the primary key of this post.
	 *
	 * @param primaryKey the primary key of this post
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the type of this post.
	 *
	 * @param type the type of this post
	 */
	@Override
	public void setType(String type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this post.
	 *
	 * @param userId the user ID of this post
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this post.
	 *
	 * @param userName the user name of this post
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this post.
	 *
	 * @param userUuid the user uuid of this post
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this post.
	 *
	 * @param uuid the uuid of this post
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the video of this post.
	 *
	 * @param video the video of this post
	 */
	@Override
	public void setVideo(String video) {
		model.setVideo(video);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected PostWrapper wrap(Post post) {
		return new PostWrapper(post);
	}

}