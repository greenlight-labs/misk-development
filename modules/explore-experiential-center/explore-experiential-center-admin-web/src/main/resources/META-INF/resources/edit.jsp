<%@include file = "init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    Post entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = PostLocalServiceUtil.getPost(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    List<Category> categories = CategoryLocalServiceUtil.findByGroupId(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editEntryURL %>" name="fm">

    <aui:model-context bean="<%= entry %>" model="<%= Post.class %>" />

    <aui:input type="hidden" name="resourcePrimKey"
               value='<%= entry == null ? "" : entry.getPostId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:select required="true" label="Category" name="categoryId">
                <% for (Category curCategory : categories) { %>
                <aui:option label="<%= curCategory.getName(locale) %>" value="<%= curCategory.getCategoryId() %>" />
                <% } %>
            </aui:select>
            <aui:select required="true" label="Type" name="type">
                <aui:option label="Image" value="image" />
                <aui:option label="Audio" value="audio" />
                <aui:option label="Video" value="video" />
                <aui:option label="Gallery" value="gallery" />
                <aui:option label="Souvenir" value="souvenir" />
            </aui:select>
            <aui:input name="name" label="Name" autoSize="true" helpMessage="Max 65 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
             <aui:input name="orderNo" label="Order Number" helpMessage="Order Number default value 9999" placeholder="9999">                
            </aui:input>
            <aui:input name="description" label="Description" helpMessage="Max 500 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <%--<aui:input name="estimatedTourTimeLabel" label="Estimated Tour Time Label with Hours" />--%>
            <aui:input name="estimatedTourTime" label="Estimated Tour Time Label with Hours" />
            <aui:input name="image" label="Image" helpMessage="Image Dimensions: W x H pixels">
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="image">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="video" label="Video" helpMessage="Max 250 Characters (Recommended)" />
            <div class="button-holder">
                <button class="btn select-video-button btn-secondary" type="button" data-field-id="video">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="buttonLabel" label="Button Label" />
        </aui:fieldset>
    </aui:fieldset-group>
    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Audio Section">
            <aui:input name="audio" label="Audio" helpMessage="Max 250 Characters (Recommended)" />
            <div class="button-holder">
                <button class="btn select-audio-button btn-secondary" type="button" data-field-id="audio">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="audioCatColor" label="Category Color" />
            <aui:input name="audioCatName" label="Category Name" />
            <aui:input name="audioName" label="Name" />
            <aui:input name="audioProfession" label="Profession" />
            <aui:input name="audioLocation" label="Location" />
        </aui:fieldset>
    </aui:fieldset-group>

    <%@include file="/includes/gallery.jsp" %>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }

    const audioButton = window.document.querySelector( '.select-audio-button' );
    audioButton.addEventListener('click', function (event) {
        event.preventDefault();
        var element = event.currentTarget;
        var fieldId = element.dataset.fieldId;
        var itemSelectorDialog = new A.LiferayItemSelectorDialog
        (
            {
                eventName: 'selectAudioDocumentLibrary',
                on: {
                    selectedItemChange: function(event) {
                        var selectedItem = event.newVal;
                        if(selectedItem)
                        {
                            var itemValue = selectedItem.value;
                            itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                            window['<portlet:namespace />'+fieldId].value=itemValue;
                            var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                            var editLanguage = null;
                            if(currentEditLanguage === 'en-US'){
                                editLanguage = 'en_US';
                            }else if(currentEditLanguage === 'ar-SA'){
                                editLanguage = 'ar_SA';
                           }
                            if(editLanguage){
                                window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                            }
                        }
                    }
                },
                title: '<liferay-ui:message key="Select Audio File (MP3)" />',
                url: '${audioItemSelectorURL }'
            }
        );
        itemSelectorDialog.open();

    });

    const videoButton = window.document.querySelector( '.select-video-button' );
    videoButton.addEventListener('click', function (event) {
        event.preventDefault();
        var element = event.currentTarget;
        var fieldId = element.dataset.fieldId;
        var itemSelectorDialog = new A.LiferayItemSelectorDialog
        (
            {
                eventName: 'selectVideoDocumentLibrary',
                on: {
                    selectedItemChange: function(event) {
                        var selectedItem = event.newVal;
                        if(selectedItem)
                        {
                            var itemValue = selectedItem.value;
                            itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                            window['<portlet:namespace />'+fieldId].value=itemValue;
                            var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                            var editLanguage = null;
                            if(currentEditLanguage === 'en-US'){
                                editLanguage = 'en_US';
                            }else if(currentEditLanguage === 'ar-SA'){
                                editLanguage = 'ar_SA';
                           }
                            if(editLanguage){
                                window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                            }
                        }
                    }
                },
                title: '<liferay-ui:message key="Select Video File (MP4)" />',
                url: '${videoItemSelectorURL }'
            }
        );
        itemSelectorDialog.open();

    });
</aui:script>