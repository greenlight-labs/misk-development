<%@ include file="/init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/category/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= CategoryLocalServiceUtil.countByGroupId(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= CategoryLocalServiceUtil.findByGroupId(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="explore.experiential.center.model.Category" modelVar="entry">

            <liferay-ui:search-container-column-text
                    name="name"
                    value="<%= HtmlUtil.escape(entry.getName(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/category/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>