<%@ include file="init.jsp" %>
<%@page import="com.liferay.portal.kernel.portlet.PortalPreferences"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="java.util.Collections"%>
<%@page import="org.apache.commons.beanutils.BeanComparator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>

<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%
 
 PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);
 String orderByCol = ParamUtil.getString(request, "orderByCol");
 String orderByType = ParamUtil.getString(request, "orderByType");

 if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
 	portalPrefs.setValue("NAME_SPACE", "order-by-col", orderByCol);
 	portalPrefs.setValue("NAME_SPACE", "order-by-type", orderByType);

 } else {
 	orderByCol = portalPrefs.getValue("NAME_SPACE", "order-by-col", "orderNo");
 	orderByType = portalPrefs.getValue("NAME_SPACE", "order-by-type", "asc");
 }
if(Validator.isNull(orderByCol)){
	orderByCol="orderNo";
}
if(Validator.isNull(orderByType)){
	orderByType="asc";
}
String sortingOrder = orderByType;
 %>
<div class="container-fluid-1280">

    <aui:button-row cssClass="admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= PostLocalServiceUtil.countByGroupId(scopeGroupId) %>" orderByType="<%=orderByType %>" orderByCol="<%=orderByCol %>">
        <liferay-ui:search-container-results>
  <%
    
    List<Post> postList = PostLocalServiceUtil.findByGroupId(scopeGroupId,-1, -1);
    
    List<Post> postListPerPage = ListUtil.subList(postList, searchContainer.getStart(),searchContainer.getEnd());
    int totalRecords =  PostLocalServiceUtil.countByGroupId(scopeGroupId);
    
    List<Post> sortablePostList = new ArrayList<Post>(postListPerPage);
    if(Validator.isNotNull(orderByCol)){
        //Pass the column name to BeanComparator to get comparator object
        BeanComparator comparator = new BeanComparator(orderByCol);
        //It will sort in ascending order
       Collections.sort(sortablePostList, comparator);
        if(sortingOrder.equalsIgnoreCase("asc")){
        	 
        
        }else{
            //It will sort in descending order
            Collections.reverse(sortablePostList);
        }
 
    }
    pageContext.setAttribute("beginIndex",searchContainer.getStart());
	pageContext.setAttribute("endIndex",searchContainer.getEnd());
    pageContext.setAttribute("results", sortablePostList);
    pageContext.setAttribute("total", totalRecords);

    %>         
            
</liferay-ui:search-container-results>
        <liferay-ui:search-container-row
                className="explore.experiential.center.model.Post" modelVar="entry">

            <liferay-ui:search-container-column-text name="Name" value="<%= HtmlUtil.escape(entry.getName(locale)) %>" orderable="true" orderableProperty="name"/>
			<liferay-ui:search-container-column-text name="Category" value="<%= HtmlUtil.escape(CategoryLocalServiceUtil.getCategory(entry.getCategoryId()).getName(locale)) %>" orderable="true" orderableProperty="categoryId"/>
  			<liferay-ui:search-container-column-text name="Order Number" value="<%= String.valueOf(entry.getOrderNo()) %>" orderable="true" orderableProperty="orderNo"/>
            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>