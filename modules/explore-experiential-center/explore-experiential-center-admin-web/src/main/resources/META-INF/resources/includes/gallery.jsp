<%
    /* *****************************************************/
    List<Gallery> gallerySlides = Collections.emptyList();
    int[] galleryIndexes = null;

    String galleryIndexesParam = ParamUtil.getString(request, "galleryIndexes");
    if (Validator.isNotNull(galleryIndexesParam)) {
        gallerySlides = new ArrayList<Gallery>();

        galleryIndexes = StringUtil.split(galleryIndexesParam, 0);

        for (int galleryIndex : galleryIndexes) {
            gallerySlides.add(new GalleryImpl());
        }
    } else {
        if (entry != null) {
            gallerySlides = GalleryLocalServiceUtil.findByPostId(entry.getPostId());

            galleryIndexes = new int[gallerySlides.size()];

            for (int i = 0; i < gallerySlides.size(); i++) {
                galleryIndexes[i] = i;
            }
        }

        if (gallerySlides.isEmpty()) {
            gallerySlides = new ArrayList<Gallery>();

            gallerySlides.add(new GalleryImpl());

            galleryIndexes = new int[] {0};
        }

        if (galleryIndexes == null) {
            galleryIndexes = new int[0];
        }
    }
    /* *****************************************************/
%>

<aui:fieldset-group markupView="lexicon">
    <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Gallery Section" id='<%= renderResponse.getNamespace() + "gallerySlides" %>'>
        <%
            for (int i = 0; i < galleryIndexes.length; i++) {
                int gallerySlideIndex = galleryIndexes[i];
                Gallery gallerySlide = gallerySlides.get(i);
        %>
        <aui:model-context bean="<%= gallerySlide %>" model="<%= Gallery.class %>"/>
        <div class="field-row lfr-form-row lfr-form-row-inline">
            <div class="row-fields">
                <aui:field-wrapper>
                    <div style="color:#bc1a1ab3" name='<%= "slide" + gallerySlideIndex %>' class="slide-heading"><h3>Slide <%= gallerySlideIndex + 1 %></h3></div>
                    <aui:input name='<%= "galleryId" + gallerySlideIndex %>' type="hidden" value="<%= gallerySlide.getGalleryId() %>" />
                    <aui:input label="Image" fieldParam='<%= "galleryImage" + gallerySlideIndex %>' id='<%= "galleryImage" + gallerySlideIndex %>' name="image" helpMessage="Image Dimensions: W x H pixels">
                        <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                            function(val) {
                                var ext = val.substring(val.lastIndexOf('.') + 1);
                                return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                            }
                        </aui:validator>
                    </aui:input>
                    <div class="button-holder">
                        <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "galleryImage" + gallerySlideIndex %>">
                            <span class="lfr-btn-label">Select</span>
                        </button>
                    </div>
                </aui:field-wrapper>
            </div>
        </div>
        <%
            }
        %>
        <aui:input name="galleryIndexes" type="hidden" value="<%= StringUtil.merge(galleryIndexes) %>"/>
    </aui:fieldset>
</aui:fieldset-group>

<aui:script use="liferay-auto-fields">
    new Liferay.AutoFields({
    contentBox: '#<portlet:namespace />gallerySlides',
    fieldIndexes: '<portlet:namespace />galleryIndexes',
    namespace: '<portlet:namespace />',
    sortable: true,
    sortableHandle: '.lfr-form-row'
    }).render();
</aui:script>
<aui:script use="liferay-item-selector-dialog">
    /*Repeater fields image selection*/
    var gallerySlidesContainer = $('#<portlet:namespace />gallerySlides');
    gallerySlidesContainer.on('click','.select-button-repeater',function(event){
        event.preventDefault();
		var id=this.name.match(/(\d+)/)[0];
        var element = event.currentTarget;
        var fieldId = element.name;
		var itemSelectorDialog = new A.LiferayItemSelectorDialog
		(
			{
				eventName: 'selectDocumentLibrary',
				on: {
						selectedItemChange: function(event) {
							var selectedItem = event.newVal;
							if(selectedItem)
							{
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
						   }
						}
				},
				title: '<liferay-ui:message key="Select File" />',
				url: '${itemSelectorURL }'
			}
		);
		itemSelectorDialog.open();

	});
</aui:script>
<script type="text/javascript">
    var galleryContainer = $('#<portlet:namespace />gallerySlides');
    /*change slide number on add/delete row*/
    galleryContainer.on('click', '.add-row', function(event){
        event.preventDefault();
        var panelBody = $(this).parents('div.panel-body');
        var fieldRows = panelBody.find('div.field-row');
        fieldRows.each(function(index, item) {
            var slideHeading = $(item).find('div.slide-heading');
            var id = index+1;
            slideHeading.find('h3').text('Slide '+id);
        });
    });
</script>