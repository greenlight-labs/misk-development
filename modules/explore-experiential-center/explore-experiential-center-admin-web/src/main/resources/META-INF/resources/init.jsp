<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>

<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.ArrayList" %>

<%@ page import="explore.experiential.center.model.Post" %>
<%@ page import="explore.experiential.center.model.Gallery" %>
<%@ page import="explore.experiential.center.model.Category"%>
<%@ page import="explore.experiential.center.service.PostLocalServiceUtil" %>
<%@ page import="explore.experiential.center.service.GalleryLocalServiceUtil" %>
<%@ page import="explore.experiential.center.service.CategoryLocalServiceUtil"%>
<%@ page import="explore.experiential.center.model.impl.GalleryImpl" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />