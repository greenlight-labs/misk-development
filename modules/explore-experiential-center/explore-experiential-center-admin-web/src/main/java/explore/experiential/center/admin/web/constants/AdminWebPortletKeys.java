package explore.experiential.center.admin.web.constants;

/**
 * @author tz
 */
public class AdminWebPortletKeys {

	public static final String POSTADMINWEB =
		"explore_experiential_center_admin_web_PostAdminWebPortlet";

	public static final String CATEGORYADMINWEB =
			"explore_experiential_center_admin_web_CategoryAdminWebPortlet";

}