package explore.experiential.center.admin.web.application.list;

import explore.experiential.center.admin.web.constants.AdminWebPanelCategoryKeys;
import explore.experiential.center.admin.web.constants.AdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + AdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class PostAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return AdminWebPortletKeys.POSTADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + AdminWebPortletKeys.POSTADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}