package explore.experiential.center.admin.web.constants;

/**
 * @author tz
 */
public class AdminWebPanelCategoryKeys {

	public static final String CONTROL_PANEL_CATEGORY = "ExploreExperientialCenterAdminWeb";

}