package explore.experiential.center.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import explore.experiential.center.admin.web.constants.AdminWebPortletKeys;
import explore.experiential.center.admin.web.constants.AdminWebPanelCategoryKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=101",
		"panel.category.key=" + AdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class CategoryAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return AdminWebPortletKeys.CATEGORYADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + AdminWebPortletKeys.CATEGORYADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}