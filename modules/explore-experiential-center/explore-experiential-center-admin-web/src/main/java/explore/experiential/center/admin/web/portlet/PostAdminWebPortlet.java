package explore.experiential.center.admin.web.portlet;

import static com.liferay.portal.kernel.log.LogFactoryUtil.getLog;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.file.criterion.FileItemSelectorCriterion;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import explore.experiential.center.admin.web.constants.AdminWebPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.*;

import explore.experiential.center.exception.PostValidateException;
import explore.experiential.center.model.Post;
import explore.experiential.center.service.PostLocalServiceUtil;
import explore.experiential.center.service.impl.PostLocalServiceImpl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=ExploreExperientialCenterAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + AdminWebPortletKeys.POSTADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class PostAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		// image upload
		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();
		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);
		// file(audio/video) upload
		FileItemSelectorCriterion fileItemSelectorCriterion = new FileItemSelectorCriterion();
		fileItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		PortletURL audioItemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectAudioDocumentLibrary", fileItemSelectorCriterion);

		PortletURL videoItemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectVideoDocumentLibrary", fileItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);
		renderRequest.setAttribute("audioItemSelectorURL", audioItemSelectorURL);
		renderRequest.setAttribute("videoItemSelectorURL", videoItemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addEntry(ActionRequest request, ActionResponse response)
			throws Exception, PostValidateException {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
		long orderNo = ParamUtil.getLong(request, "orderNo", 9999);
		Post entry = PostLocalServiceUtil.getPostFromRequest(
				primaryKey, request);
		if (orderNo != 0) {
			entry.setOrderNo(orderNo);
		} else {
			entry.setOrderNo(9999);
		}
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Post.class.getName(), request);

		// Add entry
		PostLocalServiceUtil.addEntry(entry, serviceContext);

		// Add gallery images
		updateGallery(entry, request, serviceContext);

		SessionMessages.add(request, "postAddedSuccessfully");
	}

	public void updateEntry(ActionRequest request, ActionResponse response)
			throws Exception {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
		
		long orderNo = ParamUtil.getLong(request, "orderNo", 9999);

		
		Post entry = PostLocalServiceUtil.getPostFromRequest(
				primaryKey, request);
		if (orderNo != 0) {
			entry.setOrderNo(orderNo);
		} else {
			entry.setOrderNo(9999);
		}
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Post.class.getName(), request);

		//Update entry
		PostLocalServiceUtil.updateEntry(entry, serviceContext);

		// Add gallery images
		updateGallery(entry, request, serviceContext);

		SessionMessages.add(request, "postUpdatedSuccessfully");
	}

	public void deleteEntry(ActionRequest request, ActionResponse response)
			throws PortalException {

		long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

		try {
			PostLocalServiceUtil.deleteEntry(entryId);
		} catch (PortalException pe) {
			ReflectionUtil.throwException(pe);
		}
	}

	public void updateGallery(Post entry, ActionRequest actionRequest, ServiceContext serviceContext)
			throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
				WebKeys.THEME_DISPLAY);

		PostLocalServiceUtil.updateGalleries(
				entry,
				PostLocalServiceUtil.getGalleries(actionRequest),
				serviceContext
		);
	}

	private static Log _log = getLog(PostAdminWebPortlet.class);
	
	@Reference
	private ItemSelector _itemSelector;
}