package explore.experiential.center.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import explore.experiential.center.admin.web.constants.AdminWebPortletKeys;
import explore.experiential.center.exception.CategoryValidateException;
import explore.experiential.center.model.Category;
import explore.experiential.center.service.CategoryLocalServiceUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tz
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.add-default-resource=true",
                "com.liferay.portlet.display-category=category.hidden",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.layout-cacheable=true",
                "com.liferay.portlet.private-request-attributes=false",
                "com.liferay.portlet.private-session-attributes=false",
                "com.liferay.portlet.render-weight=50",
                "com.liferay.portlet.use-default-template=true",
                "javax.portlet.display-name=CategoriesAdminWeb",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/category/view.jsp",
                "javax.portlet.name=" + AdminWebPortletKeys.CATEGORYADMINWEB,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",

        },
        service = Portlet.class
)
public class CategoryAdminWebPortlet extends MVCPortlet {

    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
                .create(renderRequest);

        ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

        List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
        desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

        imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

        PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
                "selectDocumentLibrary", imageItemSelectorCriterion);

        renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

        super.render(renderRequest, renderResponse);
    }

    public void addEntry(ActionRequest request, ActionResponse response)
            throws Exception, CategoryValidateException {

        long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

        Category entry = CategoryLocalServiceUtil.getCategoryFromRequest(
                primaryKey, request);

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Category.class.getName(), request);

        // Add entry
        CategoryLocalServiceUtil.addEntry(entry, serviceContext);

        SessionMessages.add(request, "categoryAddedSuccessfully");
    }

    public void updateEntry(ActionRequest request, ActionResponse response)
            throws Exception {

        long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

        Category entry = CategoryLocalServiceUtil.getCategoryFromRequest(
                primaryKey, request);

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Category.class.getName(), request);

        //Update entry
        CategoryLocalServiceUtil.updateEntry(entry, serviceContext);

        SessionMessages.add(request, "categoryUpdatedSuccessfully");
    }

    public void deleteEntry(ActionRequest request, ActionResponse response)
            throws PortalException {

        long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

        try {
            CategoryLocalServiceUtil.deleteEntry(entryId);
        } catch (PortalException pe) {
            ReflectionUtil.throwException(pe);
        }
    }

    @Reference
    private ItemSelector _itemSelector;
}