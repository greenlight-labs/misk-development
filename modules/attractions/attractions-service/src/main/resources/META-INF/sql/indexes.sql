create index IX_AF917DE3 on misk_attractions (groupId);
create index IX_DA42C8DB on misk_attractions (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E00DCA1D on misk_attractions (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_E6633238 on misk_attractions_amenities (attractionsId);
create index IX_BC010733 on misk_attractions_amenities (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_608F8852 on misk_attractions_bookspace (attractionsId);
create index IX_2978C2D9 on misk_attractions_bookspace (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_8EC1B361 on misk_attractions_gallary (attractionsId);
create index IX_999896A on misk_attractions_gallary (uuid_[$COLUMN_LENGTH:75$], companyId);