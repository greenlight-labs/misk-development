create table misk_attractions (
	uuid_ VARCHAR(75) null,
	attractionsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	forumBannerImage STRING null,
	forumBannerTitle STRING null,
	forumButtonLabel STRING null,
	forumButtonColor STRING null,
	forumBannerIcon STRING null,
	workingHoursLabel STRING null,
	workingHours STRING null,
	workingDays STRING null,
	workingHoursLabelColor STRING null,
	workingHoursImage STRING null,
	workingDaysImage STRING null,
	description TEXT null,
	locationLabel STRING null,
	locationLatitude STRING null,
	locationLongitude STRING null,
	contactLabel STRING null,
	contactEmailAddress STRING null,
	contactTelephone STRING null,
	contactEmailAddressIcon STRING null,
	contactTelephoneIcon STRING null,
	amenitiesLabel STRING null,
	bookSpaceLabel STRING null,
	gallaryLabel STRING null,
	eventId VARCHAR(75) null
);

create table misk_attractions_amenities (
	uuid_ VARCHAR(75) null,
	amenitiesId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attractionsId LONG,
	title STRING null,
	image STRING null
);

create table misk_attractions_bookspace (
	uuid_ VARCHAR(75) null,
	bookspaceId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attractionsId LONG,
	bookSpaceImage STRING null,
	bookSpaceTitle STRING null,
	icon STRING null,
	capacity STRING null,
	area STRING null,
	duration STRING null
);

create table misk_attractions_gallary (
	uuid_ VARCHAR(75) null,
	gallaryId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	attractionsId LONG,
	image STRING null
);