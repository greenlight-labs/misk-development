/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service.impl;

import attractions.service.AttractionsLocalServiceUtil;
import attractions.service.indexer.AttractionIndexer;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.*;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;

import attractions.model.Amenities;
import attractions.model.Attractions;
import attractions.model.Gallary;
import attractions.service.AmenitiesLocalServiceUtil;
import attractions.service.GallaryLocalServiceUtil;
import attractions.service.base.AttractionsLocalServiceBaseImpl;

import attractions.model.BookSpace;
import attractions.service.BookSpaceLocalServiceUtil;

/**
 * The implementation of the attractions local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>attractions.service.AttractionsLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AttractionsLocalServiceBaseImpl
 */
@Component(property = "model.class.name=attractions.model.Attractions", service = AopService.class)
public class AttractionsLocalServiceImpl extends AttractionsLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>attractions.service.AttractionsLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>attractions.service.AttractionsLocalServiceUtil</code>.
	 */

	public Attractions addAttractions(long userId, Map<Locale, String> forumBannerImage,
			Map<Locale, String> forumBannerTitle, Map<Locale, String> forumButtonLabel,
			Map<Locale, String> forumButtonColor, Map<Locale, String> forumBannerIcon,
			Map<Locale, String> workingHoursLabel, Map<Locale, String> workingHours, Map<Locale, String> workingDays,
			Map<Locale, String> description, Map<Locale, String> locationLabel, Map<Locale, String> locationLatitude,
			Map<Locale, String> locationLongitude, Map<Locale, String> contactLabel,
			Map<Locale, String> contactEmailAddress, Map<Locale, String> contactTelephone,
			Map<Locale, String> amenitiesLabel, Map<Locale, String> bookSpaceLabel, Map<Locale, String> gallaryLabel,
			Map<Locale, String> workingHoursLabelColor, Map<Locale, String> workingHoursImage, Map<Locale, String> workingDaysImage,
			Map<Locale, String> contactEmailAddressIcon, Map<Locale, String> contactTelephoneIcon,
			String eventId, ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		long attractionsId = counterLocalService.increment();

		Attractions attractions = attractionsPersistence.create(attractionsId);

		attractions.setUuid(serviceContext.getUuid());
		attractions.setUserId(userId);
		attractions.setGroupId(groupId);
		attractions.setCompanyId(user.getCompanyId());
		attractions.setUserName(user.getFullName());
		attractions.setCreateDate(serviceContext.getCreateDate(now));
		attractions.setModifiedDate(serviceContext.getModifiedDate(now));
		attractions.setForumBannerImageMap(forumBannerImage);
		attractions.setForumBannerTitleMap(forumBannerTitle);
		attractions.setForumButtonLabelMap(forumButtonLabel);
		attractions.setForumButtonColorMap(forumButtonColor);
		attractions.setForumBannerIconMap(forumBannerIcon);
		attractions.setWorkingHoursLabelMap(workingHoursLabel);
		attractions.setWorkingHoursMap(workingHours);
		attractions.setWorkingDaysMap(workingDays);
		attractions.setDescriptionMap(description);
		attractions.setLocationLabelMap(locationLabel);
		attractions.setLocationLatitudeMap(locationLatitude);
		attractions.setLocationLongitudeMap(locationLongitude);
		attractions.setContactLabelMap(contactLabel);
		attractions.setContactEmailAddressMap(contactEmailAddress);
		attractions.setContactTelephoneMap(contactTelephone);
		attractions.setAmenitiesLabelMap(amenitiesLabel);
		attractions.setBookSpaceLabelMap(bookSpaceLabel);
		attractions.setGallaryLabelMap(gallaryLabel);
		attractions.setEventId(eventId);
		attractions.setWorkingHoursLabelColorMap(workingHoursLabelColor);
		attractions.setWorkingHoursImageMap(workingHoursImage);
		attractions.setWorkingDaysImageMap(workingDaysImage);
		attractions.setContactEmailAddressIconMap(contactEmailAddressIcon);
		attractions.setContactTelephoneIconMap(contactTelephoneIcon);
		attractionsPersistence.update(attractions);
		attractionsPersistence.clearCache();
		return attractions;
	}

	public Attractions updateAttractions(long userId, long attractionsId, Map<Locale, String> forumBannerImage,
			Map<Locale, String> forumBannerTitle, Map<Locale, String> forumButtonLabel,
			Map<Locale, String> forumButtonColor, Map<Locale, String> forumBannerIcon,
			Map<Locale, String> workingHoursLabel, Map<Locale, String> workingHours, Map<Locale, String> workingDays,
			Map<Locale, String> description, Map<Locale, String> locationLabel, Map<Locale, String> locationLatitude,
			Map<Locale, String> locationLongitude, Map<Locale, String> contactLabel,
			Map<Locale, String> contactEmailAddress, Map<Locale, String> contactTelephone,
			Map<Locale, String> amenitiesLabel, Map<Locale, String> bookSpaceLabel, Map<Locale, String> gallaryLabel,
			Map<Locale, String> workingHoursLabelColor, Map<Locale, String> workingHoursImage, Map<Locale, String> workingDaysImage,
			Map<Locale, String> contactEmailAddressIcon, Map<Locale, String> contactTelephoneIcon,
			String eventId, ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		Attractions attractions = getAttractions(attractionsId);

		attractions.setUuid(serviceContext.getUuid());
		attractions.setUserId(userId);
		attractions.setGroupId(groupId);
		attractions.setCompanyId(user.getCompanyId());
		attractions.setUserName(user.getFullName());
		attractions.setCreateDate(serviceContext.getCreateDate(now));
		attractions.setModifiedDate(serviceContext.getModifiedDate(now));
		attractions.setForumBannerImageMap(forumBannerImage);
		attractions.setForumBannerTitleMap(forumBannerTitle);
		attractions.setForumButtonLabelMap(forumButtonLabel);
		attractions.setForumButtonColorMap(forumButtonColor);
		attractions.setForumBannerIconMap(forumBannerIcon);
		attractions.setWorkingHoursLabelMap(workingHoursLabel);
		attractions.setWorkingHoursMap(workingHours);
		attractions.setWorkingDaysMap(workingDays);
		attractions.setDescriptionMap(description);
		attractions.setLocationLabelMap(locationLabel);
		attractions.setLocationLatitudeMap(locationLatitude);
		attractions.setLocationLongitudeMap(locationLongitude);
		attractions.setContactLabelMap(contactLabel);
		attractions.setContactEmailAddressMap(contactEmailAddress);
		attractions.setContactTelephoneMap(contactTelephone);
		attractions.setAmenitiesLabelMap(amenitiesLabel);
		attractions.setBookSpaceLabelMap(bookSpaceLabel);
		attractions.setGallaryLabelMap(gallaryLabel);
		attractions.setEventId(eventId);
		attractions.setWorkingHoursLabelColorMap(workingHoursLabelColor);
		attractions.setWorkingHoursImageMap(workingHoursImage);
		attractions.setWorkingDaysImageMap(workingDaysImage);
		attractions.setContactEmailAddressIconMap(contactEmailAddressIcon);
		attractions.setContactTelephoneIconMap(contactTelephoneIcon);
		attractionsPersistence.update(attractions);
		attractionsPersistence.clearCache();
		return attractions;
	}

	public Attractions deleteAttractions(long attractionsId, ServiceContext serviceContext)
			throws PortalException, SystemException {
		Attractions attractions = getAttractions(attractionsId);
		attractions = deleteAttractions(attractions);
		return attractions;
	}

	public List<Attractions> getAttractionsList(long groupId) {

		return attractionsPersistence.findByGroupId(groupId);
	}

	public List<Attractions> getAttractionsList(long groupId, int start, int end, OrderByComparator<Attractions> obc) {

		return attractionsPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Attractions> getAttractionsList(long groupId, int start, int end) {

		return attractionsPersistence.findByGroupId(groupId, start, end);
	}

	public int getAttractionsCount(long groupId) {

		return attractionsPersistence.countByGroupId(groupId);
	}

	/*
	 * **********************************- START: Attractionss Gallery Code
	 * -*****************************
	 */

	public void addEntryGallery(Attractions orgEntry, PortletRequest request) throws PortletException {

		String galleryIndexesString = ParamUtil.getString(request, "galleryIndexes");

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);
	}

	@Override
	public void updateAttractionssGalleries(Attractions entry, List<Gallary> galleries, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> galleryIds = new HashSet<>();

		for (Gallary gallery : galleries) {
			long galleryId = gallery.getGallaryId();

			// additional step to set Ids - initially zero
			gallery.setAttractionsId(entry.getAttractionsId());
			gallery.setUserId(entry.getUserId());
			gallery.setCompanyId(entry.getCompanyId());

			if (galleryId <= 0) {
				gallery = GallaryLocalServiceUtil.addEntry(gallery, serviceContext);

				galleryId = gallery.getGallaryId();
			} else {
				GallaryLocalServiceUtil.updateEntry(gallery, serviceContext);
			}

			galleryIds.add(galleryId);
		}

		galleries = GallaryLocalServiceUtil.findAllInAttractions(entry.getAttractionsId());

		for (Gallary gallery : galleries) {
			if (!galleryIds.contains(gallery.getGallaryId())) {
				GallaryLocalServiceUtil.deleteGallary(gallery.getGallaryId());
			}
		}

	}

	@Override
	public List<Gallary> getGalleries(ActionRequest actionRequest) {
		return getGalleries(actionRequest, Collections.<Gallary>emptyList());
	}

	@Override
	public List<Gallary> getGalleries(ActionRequest actionRequest, List<Gallary> defaultGalleries) {

		String galleryIndexesString = ParamUtil.getString(actionRequest, "galleryIndexes");

		if (galleryIndexesString == null) {
			return defaultGalleries;
		}

		List<Gallary> galleries = new ArrayList<>();

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);

		for (int galleryIndex : galleryIndexes) {
			String image = ParamUtil.getString(actionRequest, "galleryImage" + galleryIndex);

			if (Validator.isNull(image)) {
				continue;
			}

			String thumbnail = ParamUtil.getString(actionRequest, "galleryThumbnail" + galleryIndex);

			long galleryId = ParamUtil.getLong(actionRequest, "galleryId" + galleryIndex);

			Gallary gallery = GallaryLocalServiceUtil.createGallary(galleryId);

			gallery.setAttractionsId(0);
			gallery.setImage(image);

			galleries.add(gallery);
		}

		return galleries;
	}

	/*
	 * **********************************- END: Attractionss Gallery Code
	 * -*****************************
	 */

	/*
	 * **********************************- START: Event Amenities Code
	 * -*****************************
	 */
	public void addEntryAmenities(Attractions orgEntry, PortletRequest request) throws PortletException {

		String amenitiesIndexesString = ParamUtil.getString(request, "amenitiesIndexes");

		int[] amenitiesIndexes = StringUtil.split(amenitiesIndexesString, 0);
	}

	@Override
	public void updateAmenities(Attractions entry, List<Amenities> AmenitiesList, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> amenitiesIds = new HashSet<>();

		for (Amenities amenities : AmenitiesList) {
			long amenitiesId = amenities.getAmenitiesId();

			// additional step to set Ids - initially zero
			amenities.setAttractionsId(entry.getAttractionsId());
			amenities.setUserId(entry.getUserId());
			amenities.setCompanyId(entry.getCompanyId());

			if (amenitiesId <= 0) {
				amenities = AmenitiesLocalServiceUtil.addEntry(amenities, serviceContext);

				amenitiesId = amenities.getAmenitiesId();
			} else {
				AmenitiesLocalServiceUtil.updateEntry(amenities, serviceContext);
			}

			amenitiesIds.add(amenitiesId);
		}

		AmenitiesList = AmenitiesLocalServiceUtil.findAllInAttractions(entry.getAttractionsId());

		for (Amenities amenities : AmenitiesList) {
			if (!amenitiesIds.contains(amenities.getAmenitiesId())) {
				AmenitiesLocalServiceUtil.deleteAmenities(amenities.getAmenitiesId());
			}
		}
	}

	@Override
	public List<Amenities> getAmenities(ActionRequest actionRequest) {
		return getAmenities(actionRequest, Collections.<Amenities>emptyList());
	}

	@Override
	public List<Amenities> getAmenities(ActionRequest actionRequest, List<Amenities> defaultAmenities) {

		String amenitiesIndexesString = ParamUtil.getString(actionRequest, "amenitiesIndexes");

		if (amenitiesIndexesString == null) {
			return defaultAmenities;
		}

		List<Amenities> amenitiesList = new ArrayList<>();

		int[] amenitiesIndexes = StringUtil.split(amenitiesIndexesString, 0);

		for (int amenitiesIndex : amenitiesIndexes) {
			String title = ParamUtil.getString(actionRequest, "title" + amenitiesIndex);

			String image = ParamUtil.getString(actionRequest, "image" + amenitiesIndex);

			long amenitiesId = ParamUtil.getLong(actionRequest, "amenitiesId" + amenitiesIndex);

			Amenities amenities = AmenitiesLocalServiceUtil.createAmenities(amenitiesId);

			amenities.setAttractionsId(0);
			amenities.setTitle(title);
			amenities.setImage(image);

			amenitiesList.add(amenities);
		}

		return amenitiesList;
	}
	/*
	 * **********************************- END: Event Amenities Code
	 * -*****************************
	 */

	/*
	 * **********************************- START: Event BookSpaceedule Code
	 * -*****************************
	 */
	public void addEntryBookSpace(Attractions orgEntry, PortletRequest request) throws PortletException {

		String scheduleIndexesString = ParamUtil.getString(request, "bookSpaceIndexes");

		int[] bookSpaceIndexes = StringUtil.split(scheduleIndexesString, 0);
	}

	@Override
	public void updateBookSpace(Attractions entry, List<BookSpace> bookspaceList, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> scheduleIds = new HashSet<>();

		for (BookSpace bookspace : bookspaceList) {
			long bookspaceId = bookspace.getBookspaceId();

			// additional step to set Ids - initially zero
			bookspace.setAttractionsId(entry.getAttractionsId());
			bookspace.setUserId(entry.getUserId());
			bookspace.setCompanyId(entry.getCompanyId());

			if (bookspaceId <= 0) {
				bookspace = BookSpaceLocalServiceUtil.addEntry(bookspace, serviceContext);

				bookspaceId = bookspace.getBookspaceId();
			} else {
				BookSpaceLocalServiceUtil.updateEntry(bookspace, serviceContext);
			}

			scheduleIds.add(bookspaceId);
		}

		bookspaceList = BookSpaceLocalServiceUtil.findAllInAttractions(entry.getAttractionsId());

		for (BookSpace bookspace : bookspaceList) {
			if (!scheduleIds.contains(bookspace.getBookspaceId())) {
				BookSpaceLocalServiceUtil.deleteBookSpace(bookspace.getBookspaceId());
			}
		}
	}

	@Override
	public List<BookSpace> getBookSpace(ActionRequest actionRequest) {
		return getBookSpace(actionRequest, Collections.<BookSpace>emptyList());
	}

	@Override
	public List<BookSpace> getBookSpace(ActionRequest actionRequest, List<BookSpace> defaultSchedules) {

		String scheduleIndexesString = ParamUtil.getString(actionRequest, "bookSpaceIndexes");

		if (scheduleIndexesString == null) {
			return defaultSchedules;
		}

		List<BookSpace> bookspacesList = new ArrayList<>();

		int[] bookSpaceIndexes = StringUtil.split(scheduleIndexesString, 0);

		for (int bookspaceIndexe : bookSpaceIndexes) {
			String title = ParamUtil.getString(actionRequest, "bookSpaceTitle" + bookspaceIndexe);
			String image = ParamUtil.getString(actionRequest, "bookSpaceImage" + bookspaceIndexe);
			String icon = ParamUtil.getString(actionRequest, "icon" + bookspaceIndexe);
			String capacity = ParamUtil.getString(actionRequest, "capacity" + bookspaceIndexe);
			String area = ParamUtil.getString(actionRequest, "area" + bookspaceIndexe);
			String duration = ParamUtil.getString(actionRequest, "duration" + bookspaceIndexe);
			long bookspaceId = ParamUtil.getLong(actionRequest, "bookSpaceId" + bookspaceIndexe);

			BookSpace bookspace = BookSpaceLocalServiceUtil.createBookSpace(bookspaceId);

			bookspace.setAttractionsId(0);
			bookspace.setBookSpaceTitle(title);
			bookspace.setBookSpaceImage(image);
			bookspace.setIcon(icon);
			bookspace.setCapacity(capacity);
			bookspace.setArea(area);
			bookspace.setDuration(duration);

			bookspacesList.add(bookspace);
		}

		return bookspacesList;
	}
	/*
	 * **********************************- END: Event BookSpace Code
	 * -*****************************
	 */

	@Override
	public List<Attractions> searchAttractions(SearchContext searchContext) {

		Hits hits;

		List<Attractions> attractionsList = new ArrayList<>();
		AttractionIndexer indexer = (AttractionIndexer) IndexerRegistryUtil.getIndexer(Attractions.class);
		try {

			hits = indexer.search(searchContext);

			for (int i = 0; i < hits.getDocs().length; i++) {
				Document doc = hits.doc(i);

				long attractionId = GetterUtil.getLong(doc.get(Field.ENTRY_CLASS_PK));
				Attractions attraction = null;
				attraction = AttractionsLocalServiceUtil.getAttractions(attractionId);

				attractionsList.add(attraction);
			}
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}

		return attractionsList;
	}
}