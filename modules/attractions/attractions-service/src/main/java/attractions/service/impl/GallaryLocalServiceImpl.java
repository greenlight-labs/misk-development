/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import attractions.model.Gallary;
import attractions.service.base.GallaryLocalServiceBaseImpl;

/**
 * The implementation of the gallary local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>attractions.service.GallaryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GallaryLocalServiceBaseImpl
 */
@Component(property = "model.class.name=attractions.model.Gallary", service = AopService.class)
public class GallaryLocalServiceImpl extends GallaryLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>attractions.service.GallaryLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>attractions.service.GallaryLocalServiceUtil</code>.
	 */

	public Gallary addEntry(Gallary orgEntry, ServiceContext serviceContext) throws PortalException {

		// Add entry

		Gallary entry = _addEntry(orgEntry, serviceContext);

		Gallary addedEntry = gallaryPersistence.update(entry);
		gallaryPersistence.clearCache();

		return addedEntry;
	}

	public Gallary updateEntry(Gallary orgEntry, ServiceContext serviceContext) throws PortalException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Update entry

		Gallary entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Gallary updatedEntry = gallaryPersistence.update(entry);
		gallaryPersistence.clearCache();

		return updatedEntry;
	}

	protected Gallary _addEntry(Gallary entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Gallary.class.getName());

		Gallary newEntry = gallaryPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		// newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);

		newEntry.setUuid(serviceContext.getUuid());
		newEntry.setAttractionsId(entry.getAttractionsId());
		newEntry.setImage(entry.getImage());

		return newEntry;
	}

	protected Gallary _updateEntry(long primaryKey, Gallary entry, ServiceContext serviceContext)
			throws PortalException {

		Gallary updateEntry = fetchGallary(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());

		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);

		updateEntry.setUuid(entry.getUuid());

		updateEntry.setGallaryId(entry.getGallaryId());
		updateEntry.setAttractionsId(entry.getAttractionsId());
		updateEntry.setImage(entry.getImage());
		return updateEntry;
	}

	public Gallary deleteEntry(long primaryKey) throws PortalException {
		Gallary entry = getGallary(primaryKey);
		gallaryPersistence.remove(entry);

		return entry;
	}

	public List<Gallary> findAllInAttractions(long attractionId, int start, int end, OrderByComparator<Gallary> obc) {

		return gallaryPersistence.findByAttractionsId(attractionId, start, end, obc);
	}
	
	public List<Gallary> findAllInAttractions(long attractionId) {

		return gallaryPersistence.findByAttractionsId(attractionId);
	}
}