package attractions.service.indexer;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.*;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.GetterUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import attractions.model.Attractions;
import attractions.service.AttractionsLocalService;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import java.util.Locale;

@Component(immediate = true, service = Indexer.class)
public class AttractionIndexer extends BaseIndexer<Attractions> {
	public static final String CLASS_NAME = AttractionIndexer.class.getName();
	private static final String DESCRIPTION = "description";

	public AttractionIndexer() {
		setDefaultSelectedFieldNames(Field.ASSET_TAG_NAMES, Field.COMPANY_ID, Field.CONTENT, Field.ENTRY_CLASS_NAME,
				Field.ENTRY_CLASS_PK, Field.GROUP_ID, Field.MODIFIED_DATE, Field.SCOPE_GROUP_ID, Field.UID, Field.TITLE,
				DESCRIPTION);
		setPermissionAware(false);
		setFilterSearch(true);
	}

	@Override
	public String getClassName() {
		return Attractions.class.getName();
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest,
			PortletResponse portletResponse) throws Exception {
		Summary summary = createSummary(document);
		summary.setMaxContentLength(200);
		return summary;
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		Attractions event = attractionsLocalService.getAttractions(classPK);
		doReindex(event);

	}

	@Override
	protected void doReindex(String[] ids) throws Exception {
		long companyId = GetterUtil.getLong(ids[0]);
		reindexEmployees(companyId);

	}

	@Override
	protected void doReindex(Attractions object) throws Exception {
		Document document = getDocument(object);
		indexWriterHelper.updateDocument(getSearchEngineId(), object.getCompanyId(), document, isCommitImmediately());

	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter,
			SearchContext searchContext) throws Exception {
		addSearchLocalizedTerm(searchQuery, searchContext, Field.TITLE, false);
		addSearchLocalizedTerm(searchQuery, searchContext, DESCRIPTION, false);

	}

	@Override
	protected void doDelete(Attractions object) throws Exception {
		deleteDocument(object.getCompanyId(), object.getAttractionsId());

	}

	@Override
	protected Document doGetDocument(Attractions object) throws Exception {

		Document document = getBaseModelDocument(Attractions.class.getName(), object);

		document.addLocalizedKeyword(Field.TITLE, object.getForumBannerTitleMap());
		document.addLocalizedKeyword(DESCRIPTION, object.getDescriptionMap());

		return document;
	}

	protected void reindexEmployees(long companyId) throws PortalException {
		final IndexableActionableDynamicQuery indexableActionableDynamicQuery = attractionsLocalService
				.getIndexableActionableDynamicQuery();
		indexableActionableDynamicQuery.setCompanyId(companyId);
		indexableActionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod() {
			@Override
			public void performAction(Object event) {
				try {
					Document document = getDocument((Attractions) event);
					indexableActionableDynamicQuery.addDocuments(document);
				} catch (PortalException pe) {
					pe.printStackTrace();
				}
			}
		});
		indexableActionableDynamicQuery.setSearchEngineId(getSearchEngineId());
		indexableActionableDynamicQuery.performActions();
	}

	private static final Log log = LogFactoryUtil.getLog(AttractionIndexer.class);

	@Reference
	AttractionsLocalService attractionsLocalService;

	@Reference
	protected IndexWriterHelper indexWriterHelper;
}