/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import attractions.model.Amenities;
import attractions.service.base.AmenitiesLocalServiceBaseImpl;

/**
 * The implementation of the amenities local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>attractions.service.AmenitiesLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AmenitiesLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=attractions.model.Amenities",
	service = AopService.class
)
public class AmenitiesLocalServiceImpl extends AmenitiesLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>attractions.service.AmenitiesLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>attractions.service.AmenitiesLocalServiceUtil</code>.
	 */
	
	
	public Amenities addEntry(Amenities orgEntry, ServiceContext serviceContext)
			throws PortalException {
		// Add entry

		Amenities entry = _addEntry(orgEntry, serviceContext);

		Amenities addedEntry = amenitiesPersistence.update(entry);
		amenitiesPersistence.clearCache();

		return addedEntry;
	}

	public Amenities updateEntry(
			Amenities orgEntry, ServiceContext serviceContext)
			throws PortalException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Update entry

		Amenities entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Amenities updatedEntry = amenitiesPersistence.update(entry);
		amenitiesPersistence.clearCache();

		return updatedEntry;
	}

	protected Amenities _addEntry(Amenities entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(Amenities.class.getName());

		Amenities newEntry = amenitiesPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());

		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);

		newEntry.setUuid(serviceContext.getUuid());
		newEntry.setAttractionsId(entry.getAttractionsId());
		newEntry.setTitle(entry.getTitle());
		newEntry.setImage(entry.getImage());

		return newEntry;
	}

	protected Amenities _updateEntry(
			long primaryKey, Amenities entry, ServiceContext serviceContext)
			throws PortalException {

		Amenities updateEntry = fetchAmenities(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);

		
		updateEntry.setUuid(serviceContext.getUuid());
		updateEntry.setAttractionsId(entry.getAttractionsId());
		updateEntry.setTitle(entry.getTitle());
		updateEntry.setImage(entry.getImage());
		

		return updateEntry;
	}

	public Amenities deleteEntry(long primaryKey) throws PortalException {
		Amenities entry = getAmenities(primaryKey);
		amenitiesPersistence.remove(entry);

		return entry;
	}
	
	public List<Amenities> findAllInAttractions(long attractionId, int start, int end, OrderByComparator<Amenities> obc) {

		return amenitiesPersistence.findByAttractionsId(attractionId, start, end, obc);
	}
	
	public List<Amenities> findAllInAttractions(long attractionId) {

		return amenitiesPersistence.findByAttractionsId(attractionId);
	}

	
}