/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service.persistence.impl;

import attractions.exception.NoSuchGallaryException;

import attractions.model.Gallary;
import attractions.model.impl.GallaryImpl;
import attractions.model.impl.GallaryModelImpl;

import attractions.service.persistence.GallaryPersistence;
import attractions.service.persistence.GallaryUtil;
import attractions.service.persistence.impl.constants.AttractionsPersistenceConstants;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the gallary service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = GallaryPersistence.class)
public class GallaryPersistenceImpl
	extends BasePersistenceImpl<Gallary> implements GallaryPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>GallaryUtil</code> to access the gallary persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		GallaryImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the gallaries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the gallaries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Gallary> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Gallary> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Gallary> list = null;

		if (useFinderCache) {
			list = (List<Gallary>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Gallary gallary : list) {
					if (!uuid.equals(gallary.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_GALLARY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GallaryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<Gallary>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	@Override
	public Gallary findByUuid_First(
			String uuid, OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		Gallary gallary = fetchByUuid_First(uuid, orderByComparator);

		if (gallary != null) {
			return gallary;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchGallaryException(sb.toString());
	}

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	@Override
	public Gallary fetchByUuid_First(
		String uuid, OrderByComparator<Gallary> orderByComparator) {

		List<Gallary> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	@Override
	public Gallary findByUuid_Last(
			String uuid, OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		Gallary gallary = fetchByUuid_Last(uuid, orderByComparator);

		if (gallary != null) {
			return gallary;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchGallaryException(sb.toString());
	}

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	@Override
	public Gallary fetchByUuid_Last(
		String uuid, OrderByComparator<Gallary> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Gallary> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the gallaries before and after the current gallary in the ordered set where uuid = &#63;.
	 *
	 * @param gallaryId the primary key of the current gallary
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary[] findByUuid_PrevAndNext(
			long gallaryId, String uuid,
			OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		uuid = Objects.toString(uuid, "");

		Gallary gallary = findByPrimaryKey(gallaryId);

		Session session = null;

		try {
			session = openSession();

			Gallary[] array = new GallaryImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, gallary, uuid, orderByComparator, true);

			array[1] = gallary;

			array[2] = getByUuid_PrevAndNext(
				session, gallary, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Gallary getByUuid_PrevAndNext(
		Session session, Gallary gallary, String uuid,
		OrderByComparator<Gallary> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_GALLARY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GallaryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(gallary)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Gallary> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the gallaries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Gallary gallary :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(gallary);
		}
	}

	/**
	 * Returns the number of gallaries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching gallaries
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_GALLARY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 = "gallary.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(gallary.uuid IS NULL OR gallary.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Gallary> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching gallaries
	 */
	@Override
	public List<Gallary> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Gallary> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Gallary> list = null;

		if (useFinderCache) {
			list = (List<Gallary>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Gallary gallary : list) {
					if (!uuid.equals(gallary.getUuid()) ||
						(companyId != gallary.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_GALLARY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GallaryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<Gallary>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	@Override
	public Gallary findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		Gallary gallary = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (gallary != null) {
			return gallary;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchGallaryException(sb.toString());
	}

	/**
	 * Returns the first gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	@Override
	public Gallary fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Gallary> orderByComparator) {

		List<Gallary> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	@Override
	public Gallary findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		Gallary gallary = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (gallary != null) {
			return gallary;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchGallaryException(sb.toString());
	}

	/**
	 * Returns the last gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	@Override
	public Gallary fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Gallary> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Gallary> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the gallaries before and after the current gallary in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param gallaryId the primary key of the current gallary
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary[] findByUuid_C_PrevAndNext(
			long gallaryId, String uuid, long companyId,
			OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		uuid = Objects.toString(uuid, "");

		Gallary gallary = findByPrimaryKey(gallaryId);

		Session session = null;

		try {
			session = openSession();

			Gallary[] array = new GallaryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, gallary, uuid, companyId, orderByComparator, true);

			array[1] = gallary;

			array[2] = getByUuid_C_PrevAndNext(
				session, gallary, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Gallary getByUuid_C_PrevAndNext(
		Session session, Gallary gallary, String uuid, long companyId,
		OrderByComparator<Gallary> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_GALLARY_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GallaryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(gallary)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Gallary> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the gallaries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Gallary gallary :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(gallary);
		}
	}

	/**
	 * Returns the number of gallaries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching gallaries
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_GALLARY_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"gallary.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(gallary.uuid IS NULL OR gallary.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"gallary.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByAttractionsId;
	private FinderPath _finderPathWithoutPaginationFindByAttractionsId;
	private FinderPath _finderPathCountByAttractionsId;

	/**
	 * Returns all the gallaries where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the matching gallaries
	 */
	@Override
	public List<Gallary> findByAttractionsId(long attractionsId) {
		return findByAttractionsId(
			attractionsId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the gallaries where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of matching gallaries
	 */
	@Override
	public List<Gallary> findByAttractionsId(
		long attractionsId, int start, int end) {

		return findByAttractionsId(attractionsId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the gallaries where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching gallaries
	 */
	@Override
	public List<Gallary> findByAttractionsId(
		long attractionsId, int start, int end,
		OrderByComparator<Gallary> orderByComparator) {

		return findByAttractionsId(
			attractionsId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the gallaries where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching gallaries
	 */
	@Override
	public List<Gallary> findByAttractionsId(
		long attractionsId, int start, int end,
		OrderByComparator<Gallary> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByAttractionsId;
				finderArgs = new Object[] {attractionsId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByAttractionsId;
			finderArgs = new Object[] {
				attractionsId, start, end, orderByComparator
			};
		}

		List<Gallary> list = null;

		if (useFinderCache) {
			list = (List<Gallary>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Gallary gallary : list) {
					if (attractionsId != gallary.getAttractionsId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_GALLARY_WHERE);

			sb.append(_FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(GallaryModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(attractionsId);

				list = (List<Gallary>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	@Override
	public Gallary findByAttractionsId_First(
			long attractionsId, OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		Gallary gallary = fetchByAttractionsId_First(
			attractionsId, orderByComparator);

		if (gallary != null) {
			return gallary;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("attractionsId=");
		sb.append(attractionsId);

		sb.append("}");

		throw new NoSuchGallaryException(sb.toString());
	}

	/**
	 * Returns the first gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	@Override
	public Gallary fetchByAttractionsId_First(
		long attractionsId, OrderByComparator<Gallary> orderByComparator) {

		List<Gallary> list = findByAttractionsId(
			attractionsId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary
	 * @throws NoSuchGallaryException if a matching gallary could not be found
	 */
	@Override
	public Gallary findByAttractionsId_Last(
			long attractionsId, OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		Gallary gallary = fetchByAttractionsId_Last(
			attractionsId, orderByComparator);

		if (gallary != null) {
			return gallary;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("attractionsId=");
		sb.append(attractionsId);

		sb.append("}");

		throw new NoSuchGallaryException(sb.toString());
	}

	/**
	 * Returns the last gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	@Override
	public Gallary fetchByAttractionsId_Last(
		long attractionsId, OrderByComparator<Gallary> orderByComparator) {

		int count = countByAttractionsId(attractionsId);

		if (count == 0) {
			return null;
		}

		List<Gallary> list = findByAttractionsId(
			attractionsId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the gallaries before and after the current gallary in the ordered set where attractionsId = &#63;.
	 *
	 * @param gallaryId the primary key of the current gallary
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary[] findByAttractionsId_PrevAndNext(
			long gallaryId, long attractionsId,
			OrderByComparator<Gallary> orderByComparator)
		throws NoSuchGallaryException {

		Gallary gallary = findByPrimaryKey(gallaryId);

		Session session = null;

		try {
			session = openSession();

			Gallary[] array = new GallaryImpl[3];

			array[0] = getByAttractionsId_PrevAndNext(
				session, gallary, attractionsId, orderByComparator, true);

			array[1] = gallary;

			array[2] = getByAttractionsId_PrevAndNext(
				session, gallary, attractionsId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Gallary getByAttractionsId_PrevAndNext(
		Session session, Gallary gallary, long attractionsId,
		OrderByComparator<Gallary> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_GALLARY_WHERE);

		sb.append(_FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(GallaryModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(attractionsId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(gallary)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Gallary> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the gallaries where attractionsId = &#63; from the database.
	 *
	 * @param attractionsId the attractions ID
	 */
	@Override
	public void removeByAttractionsId(long attractionsId) {
		for (Gallary gallary :
				findByAttractionsId(
					attractionsId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(gallary);
		}
	}

	/**
	 * Returns the number of gallaries where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the number of matching gallaries
	 */
	@Override
	public int countByAttractionsId(long attractionsId) {
		FinderPath finderPath = _finderPathCountByAttractionsId;

		Object[] finderArgs = new Object[] {attractionsId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_GALLARY_WHERE);

			sb.append(_FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(attractionsId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ATTRACTIONSID_ATTRACTIONSID_2 =
		"gallary.attractionsId = ?";

	public GallaryPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(Gallary.class);

		setModelImplClass(GallaryImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the gallary in the entity cache if it is enabled.
	 *
	 * @param gallary the gallary
	 */
	@Override
	public void cacheResult(Gallary gallary) {
		entityCache.putResult(
			GallaryImpl.class, gallary.getPrimaryKey(), gallary);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the gallaries in the entity cache if it is enabled.
	 *
	 * @param gallaries the gallaries
	 */
	@Override
	public void cacheResult(List<Gallary> gallaries) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (gallaries.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (Gallary gallary : gallaries) {
			if (entityCache.getResult(
					GallaryImpl.class, gallary.getPrimaryKey()) == null) {

				cacheResult(gallary);
			}
		}
	}

	/**
	 * Clears the cache for all gallaries.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(GallaryImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the gallary.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Gallary gallary) {
		entityCache.removeResult(GallaryImpl.class, gallary);
	}

	@Override
	public void clearCache(List<Gallary> gallaries) {
		for (Gallary gallary : gallaries) {
			entityCache.removeResult(GallaryImpl.class, gallary);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(GallaryImpl.class, primaryKey);
		}
	}

	/**
	 * Creates a new gallary with the primary key. Does not add the gallary to the database.
	 *
	 * @param gallaryId the primary key for the new gallary
	 * @return the new gallary
	 */
	@Override
	public Gallary create(long gallaryId) {
		Gallary gallary = new GallaryImpl();

		gallary.setNew(true);
		gallary.setPrimaryKey(gallaryId);

		String uuid = PortalUUIDUtil.generate();

		gallary.setUuid(uuid);

		gallary.setCompanyId(CompanyThreadLocal.getCompanyId());

		return gallary;
	}

	/**
	 * Removes the gallary with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary that was removed
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary remove(long gallaryId) throws NoSuchGallaryException {
		return remove((Serializable)gallaryId);
	}

	/**
	 * Removes the gallary with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the gallary
	 * @return the gallary that was removed
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary remove(Serializable primaryKey)
		throws NoSuchGallaryException {

		Session session = null;

		try {
			session = openSession();

			Gallary gallary = (Gallary)session.get(
				GallaryImpl.class, primaryKey);

			if (gallary == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchGallaryException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(gallary);
		}
		catch (NoSuchGallaryException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Gallary removeImpl(Gallary gallary) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(gallary)) {
				gallary = (Gallary)session.get(
					GallaryImpl.class, gallary.getPrimaryKeyObj());
			}

			if (gallary != null) {
				session.delete(gallary);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (gallary != null) {
			clearCache(gallary);
		}

		return gallary;
	}

	@Override
	public Gallary updateImpl(Gallary gallary) {
		boolean isNew = gallary.isNew();

		if (!(gallary instanceof GallaryModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(gallary.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(gallary);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in gallary proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Gallary implementation " +
					gallary.getClass());
		}

		GallaryModelImpl gallaryModelImpl = (GallaryModelImpl)gallary;

		if (Validator.isNull(gallary.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			gallary.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (gallary.getCreateDate() == null)) {
			if (serviceContext == null) {
				gallary.setCreateDate(date);
			}
			else {
				gallary.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!gallaryModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				gallary.setModifiedDate(date);
			}
			else {
				gallary.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(gallary);
			}
			else {
				gallary = (Gallary)session.merge(gallary);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(GallaryImpl.class, gallaryModelImpl, false, true);

		if (isNew) {
			gallary.setNew(false);
		}

		gallary.resetOriginalValues();

		return gallary;
	}

	/**
	 * Returns the gallary with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the gallary
	 * @return the gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary findByPrimaryKey(Serializable primaryKey)
		throws NoSuchGallaryException {

		Gallary gallary = fetchByPrimaryKey(primaryKey);

		if (gallary == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchGallaryException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return gallary;
	}

	/**
	 * Returns the gallary with the primary key or throws a <code>NoSuchGallaryException</code> if it could not be found.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary
	 * @throws NoSuchGallaryException if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary findByPrimaryKey(long gallaryId)
		throws NoSuchGallaryException {

		return findByPrimaryKey((Serializable)gallaryId);
	}

	/**
	 * Returns the gallary with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary, or <code>null</code> if a gallary with the primary key could not be found
	 */
	@Override
	public Gallary fetchByPrimaryKey(long gallaryId) {
		return fetchByPrimaryKey((Serializable)gallaryId);
	}

	/**
	 * Returns all the gallaries.
	 *
	 * @return the gallaries
	 */
	@Override
	public List<Gallary> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of gallaries
	 */
	@Override
	public List<Gallary> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of gallaries
	 */
	@Override
	public List<Gallary> findAll(
		int start, int end, OrderByComparator<Gallary> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of gallaries
	 */
	@Override
	public List<Gallary> findAll(
		int start, int end, OrderByComparator<Gallary> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Gallary> list = null;

		if (useFinderCache) {
			list = (List<Gallary>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_GALLARY);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_GALLARY;

				sql = sql.concat(GallaryModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Gallary>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the gallaries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Gallary gallary : findAll()) {
			remove(gallary);
		}
	}

	/**
	 * Returns the number of gallaries.
	 *
	 * @return the number of gallaries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_GALLARY);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "gallaryId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_GALLARY;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return GallaryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the gallary persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class, new GallaryModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", Gallary.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByAttractionsId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAttractionsId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"attractionsId"}, true);

		_finderPathWithoutPaginationFindByAttractionsId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAttractionsId",
			new String[] {Long.class.getName()}, new String[] {"attractionsId"},
			true);

		_finderPathCountByAttractionsId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAttractionsId",
			new String[] {Long.class.getName()}, new String[] {"attractionsId"},
			false);

		_setGallaryUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setGallaryUtilPersistence(null);

		entityCache.removeCache(GallaryImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setGallaryUtilPersistence(
		GallaryPersistence gallaryPersistence) {

		try {
			Field field = GallaryUtil.class.getDeclaredField("_persistence");

			field.setAccessible(true);

			field.set(null, gallaryPersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = AttractionsPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = AttractionsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = AttractionsPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_GALLARY =
		"SELECT gallary FROM Gallary gallary";

	private static final String _SQL_SELECT_GALLARY_WHERE =
		"SELECT gallary FROM Gallary gallary WHERE ";

	private static final String _SQL_COUNT_GALLARY =
		"SELECT COUNT(gallary) FROM Gallary gallary";

	private static final String _SQL_COUNT_GALLARY_WHERE =
		"SELECT COUNT(gallary) FROM Gallary gallary WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "gallary.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Gallary exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Gallary exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		GallaryPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class GallaryModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			GallaryModelImpl gallaryModelImpl = (GallaryModelImpl)baseModel;

			long columnBitmask = gallaryModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(gallaryModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						gallaryModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(GallaryPersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(gallaryModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			GallaryModelImpl gallaryModelImpl, String[] columnNames,
			boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] = gallaryModelImpl.getColumnOriginalValue(
						columnName);
				}
				else {
					arguments[i] = gallaryModelImpl.getColumnValue(columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= GallaryModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}