/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import attractions.model.BookSpace;
import attractions.service.base.BookSpaceLocalServiceBaseImpl;

/**
 * The implementation of the book space local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>attractions.service.BookSpaceLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookSpaceLocalServiceBaseImpl
 */
@Component(property = "model.class.name=attractions.model.BookSpace", service = AopService.class)
public class BookSpaceLocalServiceImpl extends BookSpaceLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>attractions.service.BookSpaceLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>attractions.service.BookSpaceLocalServiceUtil</code>.
	 */

	
	public BookSpace addEntry(BookSpace orgEntry, ServiceContext serviceContext)
			throws PortalException {

	
		// Add entry

		BookSpace entry = _addEntry(orgEntry, serviceContext);

		BookSpace addedEntry = bookSpacePersistence.update(entry);
		bookSpacePersistence.clearCache();

		return addedEntry;
	}

	public BookSpace updateEntry(
			BookSpace orgEntry, ServiceContext serviceContext)
			throws PortalException {

		User user = userLocalService.getUser(orgEntry.getUserId());

	

		// Update entry

		BookSpace entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		BookSpace updatedEntry = bookSpacePersistence.update(entry);
		bookSpacePersistence.clearCache();

		return updatedEntry;
	}

	protected BookSpace _addEntry(BookSpace entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(BookSpace.class.getName());

		BookSpace newEntry = bookSpacePersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);

		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setAttractionsId(entry.getAttractionsId());
		newEntry.setBookSpaceTitle(entry.getBookSpaceTitle());
		newEntry.setIcon(entry.getIcon());
		newEntry.setCapacity(entry.getCapacity());
		newEntry.setArea(entry.getArea());
		newEntry.setDuration(entry.getDuration());
		newEntry.setBookSpaceImage(entry.getBookSpaceImage());

		return newEntry;
	}

	protected BookSpace _updateEntry(
			long primaryKey, BookSpace entry, ServiceContext serviceContext)
			throws PortalException {

		BookSpace updateEntry = fetchBookSpace(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);

		updateEntry.setUuid(entry.getUuid());

		updateEntry.setAttractionsId(entry.getAttractionsId());
		updateEntry.setBookSpaceTitle(entry.getBookSpaceTitle());
		updateEntry.setIcon(entry.getIcon());
		updateEntry.setCapacity(entry.getCapacity());
		updateEntry.setArea(entry.getArea());
		updateEntry.setDuration(entry.getDuration());
		updateEntry.setBookSpaceImage(entry.getBookSpaceImage());

		return updateEntry;
	}

	public BookSpace deleteEntry(long primaryKey) throws PortalException {
		BookSpace entry = getBookSpace(primaryKey);
		bookSpacePersistence.remove(entry);
		return entry;
	}
	
	
	
	public List<BookSpace> findAllInAttractions(long attractionId, int start, int end,
			OrderByComparator<BookSpace> obc) {

		return bookSpacePersistence.findByAttractionsId(attractionId, start, end, obc);
	}

	public List<BookSpace> findAllInAttractions(long attractionId) {

		return bookSpacePersistence.findByAttractionsId(attractionId);
	}
	
	
}