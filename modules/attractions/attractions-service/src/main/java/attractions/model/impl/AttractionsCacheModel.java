/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.model.impl;

import attractions.model.Attractions;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Attractions in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AttractionsCacheModel
	implements CacheModel<Attractions>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AttractionsCacheModel)) {
			return false;
		}

		AttractionsCacheModel attractionsCacheModel =
			(AttractionsCacheModel)object;

		if (attractionsId == attractionsCacheModel.attractionsId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, attractionsId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(65);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", attractionsId=");
		sb.append(attractionsId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", forumBannerImage=");
		sb.append(forumBannerImage);
		sb.append(", forumBannerTitle=");
		sb.append(forumBannerTitle);
		sb.append(", forumButtonLabel=");
		sb.append(forumButtonLabel);
		sb.append(", forumButtonColor=");
		sb.append(forumButtonColor);
		sb.append(", forumBannerIcon=");
		sb.append(forumBannerIcon);
		sb.append(", workingHoursLabel=");
		sb.append(workingHoursLabel);
		sb.append(", workingHours=");
		sb.append(workingHours);
		sb.append(", workingDays=");
		sb.append(workingDays);
		sb.append(", workingHoursLabelColor=");
		sb.append(workingHoursLabelColor);
		sb.append(", workingHoursImage=");
		sb.append(workingHoursImage);
		sb.append(", workingDaysImage=");
		sb.append(workingDaysImage);
		sb.append(", description=");
		sb.append(description);
		sb.append(", locationLabel=");
		sb.append(locationLabel);
		sb.append(", locationLatitude=");
		sb.append(locationLatitude);
		sb.append(", locationLongitude=");
		sb.append(locationLongitude);
		sb.append(", contactLabel=");
		sb.append(contactLabel);
		sb.append(", contactEmailAddress=");
		sb.append(contactEmailAddress);
		sb.append(", contactTelephone=");
		sb.append(contactTelephone);
		sb.append(", contactEmailAddressIcon=");
		sb.append(contactEmailAddressIcon);
		sb.append(", contactTelephoneIcon=");
		sb.append(contactTelephoneIcon);
		sb.append(", amenitiesLabel=");
		sb.append(amenitiesLabel);
		sb.append(", bookSpaceLabel=");
		sb.append(bookSpaceLabel);
		sb.append(", gallaryLabel=");
		sb.append(gallaryLabel);
		sb.append(", eventId=");
		sb.append(eventId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Attractions toEntityModel() {
		AttractionsImpl attractionsImpl = new AttractionsImpl();

		if (uuid == null) {
			attractionsImpl.setUuid("");
		}
		else {
			attractionsImpl.setUuid(uuid);
		}

		attractionsImpl.setAttractionsId(attractionsId);
		attractionsImpl.setGroupId(groupId);
		attractionsImpl.setCompanyId(companyId);
		attractionsImpl.setUserId(userId);

		if (userName == null) {
			attractionsImpl.setUserName("");
		}
		else {
			attractionsImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			attractionsImpl.setCreateDate(null);
		}
		else {
			attractionsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			attractionsImpl.setModifiedDate(null);
		}
		else {
			attractionsImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (forumBannerImage == null) {
			attractionsImpl.setForumBannerImage("");
		}
		else {
			attractionsImpl.setForumBannerImage(forumBannerImage);
		}

		if (forumBannerTitle == null) {
			attractionsImpl.setForumBannerTitle("");
		}
		else {
			attractionsImpl.setForumBannerTitle(forumBannerTitle);
		}

		if (forumButtonLabel == null) {
			attractionsImpl.setForumButtonLabel("");
		}
		else {
			attractionsImpl.setForumButtonLabel(forumButtonLabel);
		}

		if (forumButtonColor == null) {
			attractionsImpl.setForumButtonColor("");
		}
		else {
			attractionsImpl.setForumButtonColor(forumButtonColor);
		}

		if (forumBannerIcon == null) {
			attractionsImpl.setForumBannerIcon("");
		}
		else {
			attractionsImpl.setForumBannerIcon(forumBannerIcon);
		}

		if (workingHoursLabel == null) {
			attractionsImpl.setWorkingHoursLabel("");
		}
		else {
			attractionsImpl.setWorkingHoursLabel(workingHoursLabel);
		}

		if (workingHours == null) {
			attractionsImpl.setWorkingHours("");
		}
		else {
			attractionsImpl.setWorkingHours(workingHours);
		}

		if (workingDays == null) {
			attractionsImpl.setWorkingDays("");
		}
		else {
			attractionsImpl.setWorkingDays(workingDays);
		}

		if (workingHoursLabelColor == null) {
			attractionsImpl.setWorkingHoursLabelColor("");
		}
		else {
			attractionsImpl.setWorkingHoursLabelColor(workingHoursLabelColor);
		}

		if (workingHoursImage == null) {
			attractionsImpl.setWorkingHoursImage("");
		}
		else {
			attractionsImpl.setWorkingHoursImage(workingHoursImage);
		}

		if (workingDaysImage == null) {
			attractionsImpl.setWorkingDaysImage("");
		}
		else {
			attractionsImpl.setWorkingDaysImage(workingDaysImage);
		}

		if (description == null) {
			attractionsImpl.setDescription("");
		}
		else {
			attractionsImpl.setDescription(description);
		}

		if (locationLabel == null) {
			attractionsImpl.setLocationLabel("");
		}
		else {
			attractionsImpl.setLocationLabel(locationLabel);
		}

		if (locationLatitude == null) {
			attractionsImpl.setLocationLatitude("");
		}
		else {
			attractionsImpl.setLocationLatitude(locationLatitude);
		}

		if (locationLongitude == null) {
			attractionsImpl.setLocationLongitude("");
		}
		else {
			attractionsImpl.setLocationLongitude(locationLongitude);
		}

		if (contactLabel == null) {
			attractionsImpl.setContactLabel("");
		}
		else {
			attractionsImpl.setContactLabel(contactLabel);
		}

		if (contactEmailAddress == null) {
			attractionsImpl.setContactEmailAddress("");
		}
		else {
			attractionsImpl.setContactEmailAddress(contactEmailAddress);
		}

		if (contactTelephone == null) {
			attractionsImpl.setContactTelephone("");
		}
		else {
			attractionsImpl.setContactTelephone(contactTelephone);
		}

		if (contactEmailAddressIcon == null) {
			attractionsImpl.setContactEmailAddressIcon("");
		}
		else {
			attractionsImpl.setContactEmailAddressIcon(contactEmailAddressIcon);
		}

		if (contactTelephoneIcon == null) {
			attractionsImpl.setContactTelephoneIcon("");
		}
		else {
			attractionsImpl.setContactTelephoneIcon(contactTelephoneIcon);
		}

		if (amenitiesLabel == null) {
			attractionsImpl.setAmenitiesLabel("");
		}
		else {
			attractionsImpl.setAmenitiesLabel(amenitiesLabel);
		}

		if (bookSpaceLabel == null) {
			attractionsImpl.setBookSpaceLabel("");
		}
		else {
			attractionsImpl.setBookSpaceLabel(bookSpaceLabel);
		}

		if (gallaryLabel == null) {
			attractionsImpl.setGallaryLabel("");
		}
		else {
			attractionsImpl.setGallaryLabel(gallaryLabel);
		}

		if (eventId == null) {
			attractionsImpl.setEventId("");
		}
		else {
			attractionsImpl.setEventId(eventId);
		}

		attractionsImpl.resetOriginalValues();

		return attractionsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		attractionsId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		forumBannerImage = objectInput.readUTF();
		forumBannerTitle = objectInput.readUTF();
		forumButtonLabel = objectInput.readUTF();
		forumButtonColor = objectInput.readUTF();
		forumBannerIcon = objectInput.readUTF();
		workingHoursLabel = objectInput.readUTF();
		workingHours = objectInput.readUTF();
		workingDays = objectInput.readUTF();
		workingHoursLabelColor = objectInput.readUTF();
		workingHoursImage = objectInput.readUTF();
		workingDaysImage = objectInput.readUTF();
		description = (String)objectInput.readObject();
		locationLabel = objectInput.readUTF();
		locationLatitude = objectInput.readUTF();
		locationLongitude = objectInput.readUTF();
		contactLabel = objectInput.readUTF();
		contactEmailAddress = objectInput.readUTF();
		contactTelephone = objectInput.readUTF();
		contactEmailAddressIcon = objectInput.readUTF();
		contactTelephoneIcon = objectInput.readUTF();
		amenitiesLabel = objectInput.readUTF();
		bookSpaceLabel = objectInput.readUTF();
		gallaryLabel = objectInput.readUTF();
		eventId = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(attractionsId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (forumBannerImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(forumBannerImage);
		}

		if (forumBannerTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(forumBannerTitle);
		}

		if (forumButtonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(forumButtonLabel);
		}

		if (forumButtonColor == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(forumButtonColor);
		}

		if (forumBannerIcon == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(forumBannerIcon);
		}

		if (workingHoursLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(workingHoursLabel);
		}

		if (workingHours == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(workingHours);
		}

		if (workingDays == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(workingDays);
		}

		if (workingHoursLabelColor == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(workingHoursLabelColor);
		}

		if (workingHoursImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(workingHoursImage);
		}

		if (workingDaysImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(workingDaysImage);
		}

		if (description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(description);
		}

		if (locationLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(locationLabel);
		}

		if (locationLatitude == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(locationLatitude);
		}

		if (locationLongitude == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(locationLongitude);
		}

		if (contactLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contactLabel);
		}

		if (contactEmailAddress == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contactEmailAddress);
		}

		if (contactTelephone == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contactTelephone);
		}

		if (contactEmailAddressIcon == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contactEmailAddressIcon);
		}

		if (contactTelephoneIcon == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contactTelephoneIcon);
		}

		if (amenitiesLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(amenitiesLabel);
		}

		if (bookSpaceLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bookSpaceLabel);
		}

		if (gallaryLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(gallaryLabel);
		}

		if (eventId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(eventId);
		}
	}

	public String uuid;
	public long attractionsId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String forumBannerImage;
	public String forumBannerTitle;
	public String forumButtonLabel;
	public String forumButtonColor;
	public String forumBannerIcon;
	public String workingHoursLabel;
	public String workingHours;
	public String workingDays;
	public String workingHoursLabelColor;
	public String workingHoursImage;
	public String workingDaysImage;
	public String description;
	public String locationLabel;
	public String locationLatitude;
	public String locationLongitude;
	public String contactLabel;
	public String contactEmailAddress;
	public String contactTelephone;
	public String contactEmailAddressIcon;
	public String contactTelephoneIcon;
	public String amenitiesLabel;
	public String bookSpaceLabel;
	public String gallaryLabel;
	public String eventId;

}