/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.model.impl;

import attractions.model.BookSpace;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing BookSpace in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class BookSpaceCacheModel
	implements CacheModel<BookSpace>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BookSpaceCacheModel)) {
			return false;
		}

		BookSpaceCacheModel bookSpaceCacheModel = (BookSpaceCacheModel)object;

		if (bookspaceId == bookSpaceCacheModel.bookspaceId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, bookspaceId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", bookspaceId=");
		sb.append(bookspaceId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", attractionsId=");
		sb.append(attractionsId);
		sb.append(", bookSpaceImage=");
		sb.append(bookSpaceImage);
		sb.append(", bookSpaceTitle=");
		sb.append(bookSpaceTitle);
		sb.append(", icon=");
		sb.append(icon);
		sb.append(", capacity=");
		sb.append(capacity);
		sb.append(", area=");
		sb.append(area);
		sb.append(", duration=");
		sb.append(duration);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public BookSpace toEntityModel() {
		BookSpaceImpl bookSpaceImpl = new BookSpaceImpl();

		if (uuid == null) {
			bookSpaceImpl.setUuid("");
		}
		else {
			bookSpaceImpl.setUuid(uuid);
		}

		bookSpaceImpl.setBookspaceId(bookspaceId);
		bookSpaceImpl.setCompanyId(companyId);
		bookSpaceImpl.setUserId(userId);

		if (userName == null) {
			bookSpaceImpl.setUserName("");
		}
		else {
			bookSpaceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			bookSpaceImpl.setCreateDate(null);
		}
		else {
			bookSpaceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			bookSpaceImpl.setModifiedDate(null);
		}
		else {
			bookSpaceImpl.setModifiedDate(new Date(modifiedDate));
		}

		bookSpaceImpl.setAttractionsId(attractionsId);

		if (bookSpaceImage == null) {
			bookSpaceImpl.setBookSpaceImage("");
		}
		else {
			bookSpaceImpl.setBookSpaceImage(bookSpaceImage);
		}

		if (bookSpaceTitle == null) {
			bookSpaceImpl.setBookSpaceTitle("");
		}
		else {
			bookSpaceImpl.setBookSpaceTitle(bookSpaceTitle);
		}

		if (icon == null) {
			bookSpaceImpl.setIcon("");
		}
		else {
			bookSpaceImpl.setIcon(icon);
		}

		if (capacity == null) {
			bookSpaceImpl.setCapacity("");
		}
		else {
			bookSpaceImpl.setCapacity(capacity);
		}

		if (area == null) {
			bookSpaceImpl.setArea("");
		}
		else {
			bookSpaceImpl.setArea(area);
		}

		if (duration == null) {
			bookSpaceImpl.setDuration("");
		}
		else {
			bookSpaceImpl.setDuration(duration);
		}

		bookSpaceImpl.resetOriginalValues();

		return bookSpaceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		bookspaceId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		attractionsId = objectInput.readLong();
		bookSpaceImage = objectInput.readUTF();
		bookSpaceTitle = objectInput.readUTF();
		icon = objectInput.readUTF();
		capacity = objectInput.readUTF();
		area = objectInput.readUTF();
		duration = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(bookspaceId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(attractionsId);

		if (bookSpaceImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bookSpaceImage);
		}

		if (bookSpaceTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bookSpaceTitle);
		}

		if (icon == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(icon);
		}

		if (capacity == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(capacity);
		}

		if (area == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(area);
		}

		if (duration == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(duration);
		}
	}

	public String uuid;
	public long bookspaceId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long attractionsId;
	public String bookSpaceImage;
	public String bookSpaceTitle;
	public String icon;
	public String capacity;
	public String area;
	public String duration;

}