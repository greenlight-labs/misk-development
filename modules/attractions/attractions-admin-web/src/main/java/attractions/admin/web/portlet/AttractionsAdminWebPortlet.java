package attractions.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import attractions.admin.web.constants.AttractionsAdminWebPortletKeys;
import attractions.model.Attractions;
import attractions.service.AttractionsLocalService;

/**
 * @author manis
 */
@Component(
		immediate = true,
		property = {
			"com.liferay.portlet.add-default-resource=true",
			"com.liferay.portlet.display-category=category.hidden",
			"com.liferay.portlet.header-portlet-css=/css/main.css",
			"com.liferay.portlet.layout-cacheable=true",
			"com.liferay.portlet.private-request-attributes=false",
			"com.liferay.portlet.private-session-attributes=false",
			"com.liferay.portlet.render-weight=50",
			"com.liferay.portlet.use-default-template=true",
			"javax.portlet.display-name=AttractionsAdminWeb",
			"javax.portlet.expiration-cache=0",
			"javax.portlet.init-param.template-path=/",
			"javax.portlet.init-param.view-template=/view.jsp",
			"javax.portlet.name=" + AttractionsAdminWebPortletKeys.ATTRACTIONSADMINWEB,
			"javax.portlet.resource-bundle=content.Language",
			"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class AttractionsAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addAttraction(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Attractions.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Map<Locale, String> forumBannerImage = LocalizationUtil.getLocalizationMap(request, "forumBannerImage");
		Map<Locale, String> forumBannerTitle = LocalizationUtil.getLocalizationMap(request, "forumBannerTitle");
		Map<Locale, String> forumButtonLabel = LocalizationUtil.getLocalizationMap(request, "forumButtonLabel");
		Map<Locale, String> forumButtonColor = LocalizationUtil.getLocalizationMap(request, "forumButtonColor");
		Map<Locale, String> forumBannerIcon = LocalizationUtil.getLocalizationMap(request, "forumBannerIcon");
		Map<Locale, String> workingHoursLabel = LocalizationUtil.getLocalizationMap(request, "workingHoursLabel");
		Map<Locale, String> workingHours = LocalizationUtil.getLocalizationMap(request, "workingHours");
		Map<Locale, String> workingDays = LocalizationUtil.getLocalizationMap(request, "workingDays");
		Map<Locale, String> description = LocalizationUtil.getLocalizationMap(request, "description");
		Map<Locale, String> locationLabel = LocalizationUtil.getLocalizationMap(request, "locationLabel");
		Map<Locale, String> locationLatitude = LocalizationUtil.getLocalizationMap(request, "locationLatitude");
		Map<Locale, String> locationLongitude = LocalizationUtil.getLocalizationMap(request, "locationLongitude");
		Map<Locale, String> contactLabel = LocalizationUtil.getLocalizationMap(request, "contactLabel");
		Map<Locale, String> contactEmailAddress = LocalizationUtil.getLocalizationMap(request, "contactEmailAddress");
		Map<Locale, String> contactTelephone = LocalizationUtil.getLocalizationMap(request, "contactTelephone");
		Map<Locale, String> amenitiesLabel = LocalizationUtil.getLocalizationMap(request, "amenitiesLabel");
		Map<Locale, String> bookSpaceLabel = LocalizationUtil.getLocalizationMap(request, "bookSpaceLabel");
		Map<Locale, String> gallaryLabel = LocalizationUtil.getLocalizationMap(request, "gallaryLabel");
		Map<Locale, String> workingHoursLabelColor = LocalizationUtil.getLocalizationMap(request, "workingHoursLabelColor");
		Map<Locale, String> workingHoursImage = LocalizationUtil.getLocalizationMap(request, "workingHoursImage");
		Map<Locale, String> workingDaysImage = LocalizationUtil.getLocalizationMap(request, "workingDaysImage");
		Map<Locale, String> contactEmailAddressIcon = LocalizationUtil.getLocalizationMap(request, "contactEmailAddressIcon");
		Map<Locale, String> contactTelephoneIcon = LocalizationUtil.getLocalizationMap(request, "contactTelephoneIcon");
		String eventId = ParamUtil.getString(request, "eventId");

		// _attractionsService.
		Attractions attractions = _attractionsLocalService.addAttractions(themeDisplay.getUserId(), forumBannerImage,
				forumBannerTitle, forumButtonLabel, forumButtonColor, forumBannerIcon, workingHoursLabel, workingHours,
				workingDays, description, locationLabel, locationLatitude, locationLongitude, contactLabel,
				contactEmailAddress, contactTelephone, amenitiesLabel, bookSpaceLabel, gallaryLabel,				
				 workingHoursLabelColor, workingHoursImage, workingDaysImage,contactEmailAddressIcon,
				 contactTelephoneIcon, eventId,serviceContext);

		// Add gallery images
		_attractionsLocalService.updateAttractionssGalleries(attractions,
				_attractionsLocalService.getGalleries(request), serviceContext);

		// Add aminities

		_attractionsLocalService.updateAmenities(attractions, _attractionsLocalService.getAmenities(request),
				serviceContext);

		// Add BookSpace
		_attractionsLocalService.updateBookSpace(attractions, _attractionsLocalService.getBookSpace(request),
				serviceContext);
	}

	public void updateAttraction(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Attractions.class.getName(), request);
		long attractionsId = ParamUtil.getLong(request, "attractionsId");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Map<Locale, String> forumBannerImage = LocalizationUtil.getLocalizationMap(request, "forumBannerImage");
		Map<Locale, String> forumBannerTitle = LocalizationUtil.getLocalizationMap(request, "forumBannerTitle");
		Map<Locale, String> forumButtonLabel = LocalizationUtil.getLocalizationMap(request, "forumButtonLabel");
		Map<Locale, String> forumButtonColor = LocalizationUtil.getLocalizationMap(request, "forumButtonColor");
		Map<Locale, String> forumBannerIcon = LocalizationUtil.getLocalizationMap(request, "forumBannerIcon");
		Map<Locale, String> workingHoursLabel = LocalizationUtil.getLocalizationMap(request, "workingHoursLabel");
		Map<Locale, String> workingHours = LocalizationUtil.getLocalizationMap(request, "workingHours");
		Map<Locale, String> workingDays = LocalizationUtil.getLocalizationMap(request, "workingDays");
		Map<Locale, String> description = LocalizationUtil.getLocalizationMap(request, "description");
		Map<Locale, String> locationLabel = LocalizationUtil.getLocalizationMap(request, "locationLabel");
		Map<Locale, String> locationLatitude = LocalizationUtil.getLocalizationMap(request, "locationLatitude");
		Map<Locale, String> locationLongitude = LocalizationUtil.getLocalizationMap(request, "locationLongitude");
		Map<Locale, String> contactLabel = LocalizationUtil.getLocalizationMap(request, "contactLabel");
		Map<Locale, String> contactEmailAddress = LocalizationUtil.getLocalizationMap(request, "contactEmailAddress");
		Map<Locale, String> contactTelephone = LocalizationUtil.getLocalizationMap(request, "contactTelephone");
		Map<Locale, String> amenitiesLabel = LocalizationUtil.getLocalizationMap(request, "amenitiesLabel");
		Map<Locale, String> bookSpaceLabel = LocalizationUtil.getLocalizationMap(request, "bookSpaceLabel");
		Map<Locale, String> gallaryLabel = LocalizationUtil.getLocalizationMap(request, "gallaryLabel");
		Map<Locale, String> workingHoursLabelColor = LocalizationUtil.getLocalizationMap(request, "workingHoursLabelColor");
		Map<Locale, String> workingHoursImage = LocalizationUtil.getLocalizationMap(request, "workingHoursImage");
		Map<Locale, String> workingDaysImage = LocalizationUtil.getLocalizationMap(request, "workingDaysImage");
		Map<Locale, String> contactEmailAddressIcon = LocalizationUtil.getLocalizationMap(request, "contactEmailAddressIcon");
		Map<Locale, String> contactTelephoneIcon = LocalizationUtil.getLocalizationMap(request, "contactTelephoneIcon");
		
		String eventId = ParamUtil.getString(request, "eventId");
		// _attractionsService.
		Attractions attractions = _attractionsLocalService.updateAttractions(themeDisplay.getUserId(), attractionsId,
				forumBannerImage, forumBannerTitle, forumButtonLabel, forumButtonColor, forumBannerIcon,
				workingHoursLabel, workingHours, workingDays, description, locationLabel, locationLatitude,
				locationLongitude, contactLabel, contactEmailAddress, contactTelephone, amenitiesLabel, bookSpaceLabel,
				gallaryLabel, workingHoursLabelColor, workingHoursImage, workingDaysImage,contactEmailAddressIcon,
				 contactTelephoneIcon,eventId, serviceContext);

		// Add gallery images
		_attractionsLocalService.updateAttractionssGalleries(attractions,
				_attractionsLocalService.getGalleries(request), serviceContext);
		// Add aminities

		_attractionsLocalService.updateAmenities(attractions, _attractionsLocalService.getAmenities(request),
				serviceContext);

		// Add BookSpace
		_attractionsLocalService.updateBookSpace(attractions, _attractionsLocalService.getBookSpace(request),
				serviceContext);
	}

	public void deleteAttractions(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Attractions.class.getName(), request);

		long attractionsId = ParamUtil.getLong(request, "attractionsId");

		try {

			_attractionsLocalService.deleteAttractions(attractionsId, serviceContext);
		} catch (PortalException pe) {

		}
	}

	@Reference
	private AttractionsLocalService _attractionsLocalService;

	@Reference
	private ItemSelector _itemSelector;
}
