package attractions.admin.web.application.list;

import attractions.admin.web.constants.AttractionsAdminWebPanelCategoryKeys;
import attractions.admin.web.constants.AttractionsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author manis
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + AttractionsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class AttractionsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return AttractionsAdminWebPortletKeys.ATTRACTIONSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + AttractionsAdminWebPortletKeys.ATTRACTIONSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}