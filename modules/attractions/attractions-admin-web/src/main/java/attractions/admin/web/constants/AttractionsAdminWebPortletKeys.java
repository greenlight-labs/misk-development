package attractions.admin.web.constants;

/**
 * @author manis
 */
public class AttractionsAdminWebPortletKeys {

	public static final String ATTRACTIONSADMINWEB =
		"attractions_admin_web_AttractionsAdminWebPortlet";

}