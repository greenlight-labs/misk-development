<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="attractions.model.BookSpace" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="attractions.service.BookSpaceLocalServiceUtil" %>
<%@ page import="attractions.model.impl.BookSpaceImpl" %><%
    /* *****************************************************/
    List<BookSpace> bookSpaceSlides = Collections.emptyList();
    int[] bookSpaceIndexes = null;

    String bookSpaceIndexesParam = ParamUtil.getString(request, "bookSpaceIndexes");
    if (Validator.isNotNull(bookSpaceIndexesParam)) {
        bookSpaceSlides = new ArrayList<BookSpace>();

        bookSpaceIndexes = StringUtil.split(bookSpaceIndexesParam, 0);

        for (int scheduleIndex : bookSpaceIndexes) {
            bookSpaceSlides.add(new BookSpaceImpl());
        }
    } else {
    	if (attraction != null) {
            bookSpaceSlides = BookSpaceLocalServiceUtil.findAllInAttractions(attraction.getAttractionsId());

            bookSpaceIndexes = new int[bookSpaceSlides.size()];

            for (int i = 0; i < bookSpaceSlides.size(); i++) {
                bookSpaceIndexes[i] = i;
            }
        }

        if (bookSpaceSlides.isEmpty()) {
            bookSpaceSlides = new ArrayList<BookSpace>();

            bookSpaceSlides.add(new BookSpaceImpl());

            bookSpaceIndexes = new int[] {0};
        }

        if (bookSpaceIndexes == null) {
            bookSpaceIndexes = new int[0];
        }
    }
    /* *****************************************************/
%>

<aui:fieldset-group markupView="lexicon">
    <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="BookSpace Section" id='<%= renderResponse.getNamespace() + "bookSpaceSlides" %>'>
        <%
            for (int i = 0; i < bookSpaceIndexes.length; i++) {
                int bookSpaceSlideIndex = bookSpaceIndexes[i];
                BookSpace bookSpaceSlide = bookSpaceSlides.get(i);
        %>
        <aui:model-context bean="<%= bookSpaceSlide %>" model="<%= BookSpace.class %>"/>
        <div class="field-row lfr-form-row lfr-form-row-inline">
            <div class="row-fields">
                <aui:field-wrapper>
                    <div style="color:#bc1a1ab3" name='<%= "slide" + bookSpaceSlideIndex %>' class="slide-heading"><h3>Slide <%= bookSpaceSlideIndex + 1 %></h3></div>
                    <aui:input name='<%= "bookSpaceId" + bookSpaceSlideIndex %>' type="hidden" value="<%= bookSpaceSlide.getBookspaceId() %>" />
                    
                    <aui:input label="Title" fieldParam='<%= "bookSpaceTitle" + bookSpaceSlideIndex %>' id='<%= "bookSpaceTitle" + bookSpaceSlideIndex %>' name="bookSpaceTitle" helpMessage="Max 15 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                    <aui:input label="Image" fieldParam='<%= "bookSpaceImage" + bookSpaceSlideIndex %>' id='<%= "bookSpaceImage" + bookSpaceSlideIndex %>' name="bookSpaceImage" helpMessage="Image Dimensions: 1229 x 931 pixels">
                        <aui:validator name="required"/>
                        <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                            function(val) {
                                var ext = val.substring(val.lastIndexOf('.') + 1);
                                return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                            }
                        </aui:validator>
                    </aui:input>
                    <div class="button-holder">
                        <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "bookSpaceImage" + bookSpaceSlideIndex %>">
                            <span class="lfr-btn-label">Select</span>
                        </button>
                    </div>
                    <aui:input label="Icon" fieldParam='<%= "icon" + bookSpaceSlideIndex %>' id='<%= "icon" + bookSpaceSlideIndex %>' name="icon" helpMessage="icon Dimensions: 1229 x 931 pixels">
                        <aui:validator name="required"/>
                        <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                            function(val) {
                                var ext = val.substring(val.lastIndexOf('.') + 1);
                                return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                            }
                        </aui:validator>
                    </aui:input>
                    <div class="button-holder">
                        <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "icon" + bookSpaceSlideIndex %>">
                            <span class="lfr-btn-label">Select</span>
                        </button>
                    </div>
                     <aui:input label="Capacity" fieldParam='<%= "capacity" + bookSpaceSlideIndex %>' id='<%= "capacity" + bookSpaceSlideIndex %>' name="capacity" helpMessage="Max 15 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                     <aui:input label="Area" fieldParam='<%= "area" + bookSpaceSlideIndex %>' id='<%= "area" + bookSpaceSlideIndex %>' name="area" helpMessage="Max 5 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                     <aui:input label="Duration" fieldParam='<%= "duration" + bookSpaceSlideIndex %>' id='<%= "duration" + bookSpaceSlideIndex %>' name="duration" helpMessage="Max 10 Characters (Recommended)">
                        <aui:validator name="required"/>
                    </aui:input>
                </aui:field-wrapper>
            </div>
        </div>
        <%
            }
        %>
        <aui:input name="bookSpaceIndexes" type="hidden" value="<%= StringUtil.merge(bookSpaceIndexes) %>"/>
    </aui:fieldset>
</aui:fieldset-group>

<aui:script use="liferay-auto-fields">
    new Liferay.AutoFields({
    contentBox: '#<portlet:namespace />bookSpaceSlides',
    fieldIndexes: '<portlet:namespace />bookSpaceIndexes',
    namespace: '<portlet:namespace />',
    sortable: true,
    sortableHandle: '.lfr-form-row'
    }).render();
</aui:script>
<aui:script use="liferay-item-selector-dialog">
    /*Repeater fields image selection*/
    var bookSpaceSlidesContainer = $('#<portlet:namespace />bookSpaceSlides');
    bookSpaceSlidesContainer.on('click','.select-button-repeater',function(event){
        event.preventDefault();
		var id=this.name.match(/(\d+)/)[0];
        var element = event.currentTarget;
        var fieldId = element.name;
		var itemSelectorDialog = new A.LiferayItemSelectorDialog
		(
			{
				eventName: 'selectDocumentLibrary',
				on: {
						selectedItemChange: function(event) {
							var selectedItem = event.newVal;
							if(selectedItem)
							{
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
						   }
						}
				},
				title: '<liferay-ui:message key="Select File" />',
				url: '${itemSelectorURL }'
			}
		);
		itemSelectorDialog.open();

	});
</aui:script>
<script type="text/javascript">
    var scheduleContainer = $('#<portlet:namespace />bookSpaceSlides');
    /*change slide number on add/delete row*/
    scheduleContainer.on('click', '.add-row', function(event){
        event.preventDefault();
        var panelBody = $(this).parents('div.panel-body');
        var fieldRows = panelBody.find('div.field-row');
        fieldRows.each(function(index, item) {
            var slideHeading = $(item).find('div.slide-heading');
            var id = index+1;
            slideHeading.find('h3').text('Slide '+id);
        });
    });
</script>