<%@page import="attractions.service.AttractionsLocalServiceUtil"%>
<%@page import="attractions.model.Attractions"%>
<%@include file = "/init.jsp" %>

<%
    long attractionsId = ParamUtil.getLong(request, "attractionsId");

Attractions attraction = null;

    if (attractionsId > 0) {
        try {
        	attraction = AttractionsLocalServiceUtil.getAttractions(attractionsId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }

   
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= attraction == null ? "addAttraction" : "updateAttraction" %>' var="editAttractionURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editAttractionURL %>" name="fm">
    <aui:model-context bean="<%= attraction %>" model="<%= Attractions.class %>" />
    <aui:input type="hidden" name="attractionsId"
               value='<%= attraction == null ? "" : attraction.getAttractionsId() %>' />
    <aui:fieldset-group markupView="lexicon">     
        <aui:fieldset>
            <aui:input name="forumBannerTitle" label="Banner Title" helpMessage="Banner Title">
             <aui:validator name="required"/>
             </aui:input>
             <aui:input name="forumBannerImage" label="Banner Image" helpMessage="Banner Image">
                <aui:validator name="required"/>
                 <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
             </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="forumBannerImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="forumButtonLabel" label="Button Label" helpMessage="Button Label" />
            <aui:input name="forumButtonColor" label="Button Color" helpMessage="Button Color" />
            <aui:input name="forumBannerIcon" label="Banner Icon" helpMessage="Banner Icon">
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="forumBannerIcon">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="workingHoursLabel" label="Working Hours Label" helpMessage="Working Hours Label" />
            <aui:input name="workingHoursLabelColor" label="Working Hours Label Color" helpMessage="Working Hours Label Color" />
            <aui:input name="workingHours" label="Working Hours" helpMessage="Working Hours" />
            <aui:input name="workingDays" label="Working Days" helpMessage="Working Days">
             <aui:validator name="required"/>
             </aui:input>
              
              <aui:input name="workingHoursImage" label="Working Hours Image" helpMessage="Working Hours Image">
                  <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
              </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="workingHoursImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
             <aui:input name="workingDaysImage" label="Working Days Image" helpMessage="Working Days Image">
                 <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
             </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="workingDaysImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            
            <aui:input name="description" label="Descriptione" helpMessage="Description" />
            <aui:input name="locationLabel" label="Location Label" helpMessage="Location Label" >
             <aui:validator name="required"/>
             </aui:input>
            <aui:input name="locationLatitude" label="Location Latitude" helpMessage="Location Latitude" />
            <aui:input name="locationLongitude" label="Location Longitude" helpMessage="Location Longitude" />
            <aui:input name="contactLabel" label="Contact Title" helpMessage="Contact Title" />
            <aui:input name="contactEmailAddress" label="Contact Email Address" helpMessage="Contact Email Addres" />
            <aui:input name="contactTelephone" label="Contact Telephone" helpMessage="Contact Telephone" />
            
                 <aui:input name="contactEmailAddressIcon" label="Contact Email Address Icon" helpMessage="Contact Email Address Icon">
                    <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                        function(val) {
                            var ext = val.substring(val.lastIndexOf('.') + 1);
                            return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                        }
                    </aui:validator>
                 </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="contactEmailAddressIcon">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
             <aui:input name="contactTelephoneIcon" label="Contact Telephone Icon" helpMessage="Contact Telephone Icon">
                 <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
             </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="contactTelephoneIcon">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>            
            
            <aui:input name="amenitiesLabel" label="Amenities Label" helpMessage="Amenities Label" />
            <aui:input name="bookSpaceLabel" label="Book Space Label" helpMessage="Book Space Label" />
            <aui:input name="gallaryLabel" label="Gallary Label" helpMessage="Gallary Label" />
            <aui:input name="eventId" label="Event Id" helpMessage="Event Id" />
        </aui:fieldset>
    </aui:fieldset-group>
   		<%@include file="/includes/gallery.jsp" %>
	  <%@include file="/includes/amenities.jsp" %>
	  <%@include file="/includes/bookSpace.jsp" %>
        <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>


<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }  
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>