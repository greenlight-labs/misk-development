<%@page import="attractions.model.Attractions"%>
<%@page import="attractions.service.AttractionsLocalServiceUtil"%>
<%@ include file="/init.jsp" %>
<% /*  Attractions attractions =null; */ %>
<div class="container-fluid-1280">

    <aui:button-row cssClass="attractions-admin-buttons">
        <portlet:renderURL var="addAttractionsURL">
            <portlet:param name="mvcPath"
                           value="/edit_attractions.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addAttractionsURL.toString() %>"
                    value="Add Attractions"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= AttractionsLocalServiceUtil.getAttractionsesCount() %>">
        <liferay-ui:search-container-results
                results="<%= AttractionsLocalServiceUtil.getAttractionses(
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="attractions.model.Attractions" modelVar="attractions">
			<liferay-ui:search-container-column-text name="Attraction Id" value="<%= String.valueOf(attractions.getAttractionsId()) %>"/>
            <liferay-ui:search-container-column-text name="Title" value="<%= HtmlUtil.escape(attractions.getForumBannerTitle(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Created Date" value="<%= dateFormat.format(attractions.getCreateDate()) %>"/>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>