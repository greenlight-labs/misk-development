/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service;

import attractions.model.Attractions;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for Attractions. This utility wraps
 * <code>attractions.service.impl.AttractionsLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AttractionsLocalService
 * @generated
 */
public class AttractionsLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>attractions.service.impl.AttractionsLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the attractions to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was added
	 */
	public static Attractions addAttractions(Attractions attractions) {
		return getService().addAttractions(attractions);
	}

	public static Attractions addAttractions(
			long userId, Map<java.util.Locale, String> forumBannerImage,
			Map<java.util.Locale, String> forumBannerTitle,
			Map<java.util.Locale, String> forumButtonLabel,
			Map<java.util.Locale, String> forumButtonColor,
			Map<java.util.Locale, String> forumBannerIcon,
			Map<java.util.Locale, String> workingHoursLabel,
			Map<java.util.Locale, String> workingHours,
			Map<java.util.Locale, String> workingDays,
			Map<java.util.Locale, String> description,
			Map<java.util.Locale, String> locationLabel,
			Map<java.util.Locale, String> locationLatitude,
			Map<java.util.Locale, String> locationLongitude,
			Map<java.util.Locale, String> contactLabel,
			Map<java.util.Locale, String> contactEmailAddress,
			Map<java.util.Locale, String> contactTelephone,
			Map<java.util.Locale, String> amenitiesLabel,
			Map<java.util.Locale, String> bookSpaceLabel,
			Map<java.util.Locale, String> gallaryLabel,
			Map<java.util.Locale, String> workingHoursLabelColor,
			Map<java.util.Locale, String> workingHoursImage,
			Map<java.util.Locale, String> workingDaysImage,
			Map<java.util.Locale, String> contactEmailAddressIcon,
			Map<java.util.Locale, String> contactTelephoneIcon, String eventId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addAttractions(
			userId, forumBannerImage, forumBannerTitle, forumButtonLabel,
			forumButtonColor, forumBannerIcon, workingHoursLabel, workingHours,
			workingDays, description, locationLabel, locationLatitude,
			locationLongitude, contactLabel, contactEmailAddress,
			contactTelephone, amenitiesLabel, bookSpaceLabel, gallaryLabel,
			workingHoursLabelColor, workingHoursImage, workingDaysImage,
			contactEmailAddressIcon, contactTelephoneIcon, eventId,
			serviceContext);
	}

	public static void addEntryAmenities(
			Attractions orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryAmenities(orgEntry, request);
	}

	public static void addEntryBookSpace(
			Attractions orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryBookSpace(orgEntry, request);
	}

	public static void addEntryGallery(
			Attractions orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryGallery(orgEntry, request);
	}

	/**
	 * Creates a new attractions with the primary key. Does not add the attractions to the database.
	 *
	 * @param attractionsId the primary key for the new attractions
	 * @return the new attractions
	 */
	public static Attractions createAttractions(long attractionsId) {
		return getService().createAttractions(attractionsId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the attractions from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was removed
	 */
	public static Attractions deleteAttractions(Attractions attractions) {
		return getService().deleteAttractions(attractions);
	}

	/**
	 * Deletes the attractions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions that was removed
	 * @throws PortalException if a attractions with the primary key could not be found
	 */
	public static Attractions deleteAttractions(long attractionsId)
		throws PortalException {

		return getService().deleteAttractions(attractionsId);
	}

	public static Attractions deleteAttractions(
			long attractionsId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteAttractions(attractionsId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Attractions fetchAttractions(long attractionsId) {
		return getService().fetchAttractions(attractionsId);
	}

	/**
	 * Returns the attractions matching the UUID and group.
	 *
	 * @param uuid the attractions's UUID
	 * @param groupId the primary key of the group
	 * @return the matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	public static Attractions fetchAttractionsByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchAttractionsByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static List<attractions.model.Amenities> getAmenities(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getAmenities(actionRequest);
	}

	public static List<attractions.model.Amenities> getAmenities(
		javax.portlet.ActionRequest actionRequest,
		List<attractions.model.Amenities> defaultAmenities) {

		return getService().getAmenities(actionRequest, defaultAmenities);
	}

	/**
	 * Returns the attractions with the primary key.
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions
	 * @throws PortalException if a attractions with the primary key could not be found
	 */
	public static Attractions getAttractions(long attractionsId)
		throws PortalException {

		return getService().getAttractions(attractionsId);
	}

	/**
	 * Returns the attractions matching the UUID and group.
	 *
	 * @param uuid the attractions's UUID
	 * @param groupId the primary key of the group
	 * @return the matching attractions
	 * @throws PortalException if a matching attractions could not be found
	 */
	public static Attractions getAttractionsByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getAttractionsByUuidAndGroupId(uuid, groupId);
	}

	public static int getAttractionsCount(long groupId) {
		return getService().getAttractionsCount(groupId);
	}

	/**
	 * Returns a range of all the attractionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @return the range of attractionses
	 */
	public static List<Attractions> getAttractionses(int start, int end) {
		return getService().getAttractionses(start, end);
	}

	/**
	 * Returns all the attractionses matching the UUID and company.
	 *
	 * @param uuid the UUID of the attractionses
	 * @param companyId the primary key of the company
	 * @return the matching attractionses, or an empty list if no matches were found
	 */
	public static List<Attractions> getAttractionsesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getAttractionsesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of attractionses matching the UUID and company.
	 *
	 * @param uuid the UUID of the attractionses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching attractionses, or an empty list if no matches were found
	 */
	public static List<Attractions> getAttractionsesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Attractions> orderByComparator) {

		return getService().getAttractionsesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of attractionses.
	 *
	 * @return the number of attractionses
	 */
	public static int getAttractionsesCount() {
		return getService().getAttractionsesCount();
	}

	public static List<Attractions> getAttractionsList(long groupId) {
		return getService().getAttractionsList(groupId);
	}

	public static List<Attractions> getAttractionsList(
		long groupId, int start, int end) {

		return getService().getAttractionsList(groupId, start, end);
	}

	public static List<Attractions> getAttractionsList(
		long groupId, int start, int end, OrderByComparator<Attractions> obc) {

		return getService().getAttractionsList(groupId, start, end, obc);
	}

	public static List<attractions.model.BookSpace> getBookSpace(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getBookSpace(actionRequest);
	}

	public static List<attractions.model.BookSpace> getBookSpace(
		javax.portlet.ActionRequest actionRequest,
		List<attractions.model.BookSpace> defaultSchedules) {

		return getService().getBookSpace(actionRequest, defaultSchedules);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static List<attractions.model.Gallary> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getGalleries(actionRequest);
	}

	public static List<attractions.model.Gallary> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		List<attractions.model.Gallary> defaultGalleries) {

		return getService().getGalleries(actionRequest, defaultGalleries);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static List<Attractions> searchAttractions(
		SearchContext searchContext) {

		return getService().searchAttractions(searchContext);
	}

	public static void updateAmenities(
			Attractions entry, List<attractions.model.Amenities> AmenitiesList,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateAmenities(entry, AmenitiesList, serviceContext);
	}

	/**
	 * Updates the attractions in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was updated
	 */
	public static Attractions updateAttractions(Attractions attractions) {
		return getService().updateAttractions(attractions);
	}

	public static Attractions updateAttractions(
			long userId, long attractionsId,
			Map<java.util.Locale, String> forumBannerImage,
			Map<java.util.Locale, String> forumBannerTitle,
			Map<java.util.Locale, String> forumButtonLabel,
			Map<java.util.Locale, String> forumButtonColor,
			Map<java.util.Locale, String> forumBannerIcon,
			Map<java.util.Locale, String> workingHoursLabel,
			Map<java.util.Locale, String> workingHours,
			Map<java.util.Locale, String> workingDays,
			Map<java.util.Locale, String> description,
			Map<java.util.Locale, String> locationLabel,
			Map<java.util.Locale, String> locationLatitude,
			Map<java.util.Locale, String> locationLongitude,
			Map<java.util.Locale, String> contactLabel,
			Map<java.util.Locale, String> contactEmailAddress,
			Map<java.util.Locale, String> contactTelephone,
			Map<java.util.Locale, String> amenitiesLabel,
			Map<java.util.Locale, String> bookSpaceLabel,
			Map<java.util.Locale, String> gallaryLabel,
			Map<java.util.Locale, String> workingHoursLabelColor,
			Map<java.util.Locale, String> workingHoursImage,
			Map<java.util.Locale, String> workingDaysImage,
			Map<java.util.Locale, String> contactEmailAddressIcon,
			Map<java.util.Locale, String> contactTelephoneIcon, String eventId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().updateAttractions(
			userId, attractionsId, forumBannerImage, forumBannerTitle,
			forumButtonLabel, forumButtonColor, forumBannerIcon,
			workingHoursLabel, workingHours, workingDays, description,
			locationLabel, locationLatitude, locationLongitude, contactLabel,
			contactEmailAddress, contactTelephone, amenitiesLabel,
			bookSpaceLabel, gallaryLabel, workingHoursLabelColor,
			workingHoursImage, workingDaysImage, contactEmailAddressIcon,
			contactTelephoneIcon, eventId, serviceContext);
	}

	public static void updateAttractionssGalleries(
			Attractions entry, List<attractions.model.Gallary> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateAttractionssGalleries(
			entry, galleries, serviceContext);
	}

	public static void updateBookSpace(
			Attractions entry, List<attractions.model.BookSpace> bookspaceList,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateBookSpace(entry, bookspaceList, serviceContext);
	}

	public static AttractionsLocalService getService() {
		return _service;
	}

	private static volatile AttractionsLocalService _service;

}