/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service.persistence;

import attractions.model.BookSpace;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the book space service. This utility wraps <code>attractions.service.persistence.impl.BookSpacePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookSpacePersistence
 * @generated
 */
public class BookSpaceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(BookSpace bookSpace) {
		getPersistence().clearCache(bookSpace);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, BookSpace> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<BookSpace> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<BookSpace> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<BookSpace> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static BookSpace update(BookSpace bookSpace) {
		return getPersistence().update(bookSpace);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static BookSpace update(
		BookSpace bookSpace, ServiceContext serviceContext) {

		return getPersistence().update(bookSpace, serviceContext);
	}

	/**
	 * Returns all the book spaces where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching book spaces
	 */
	public static List<BookSpace> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	public static List<BookSpace> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	public static List<BookSpace> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	public static List<BookSpace> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public static BookSpace findByUuid_First(
			String uuid, OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public static BookSpace fetchByUuid_First(
		String uuid, OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public static BookSpace findByUuid_Last(
			String uuid, OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public static BookSpace fetchByUuid_Last(
		String uuid, OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where uuid = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public static BookSpace[] findByUuid_PrevAndNext(
			long bookspaceId, String uuid,
			OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByUuid_PrevAndNext(
			bookspaceId, uuid, orderByComparator);
	}

	/**
	 * Removes all the book spaces where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of book spaces where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching book spaces
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching book spaces
	 */
	public static List<BookSpace> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	public static List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	public static List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	public static List<BookSpace> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public static BookSpace findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public static BookSpace fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public static BookSpace findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public static BookSpace fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public static BookSpace[] findByUuid_C_PrevAndNext(
			long bookspaceId, String uuid, long companyId,
			OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByUuid_C_PrevAndNext(
			bookspaceId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the book spaces where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of book spaces where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching book spaces
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the book spaces where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the matching book spaces
	 */
	public static List<BookSpace> findByAttractionsId(long attractionsId) {
		return getPersistence().findByAttractionsId(attractionsId);
	}

	/**
	 * Returns a range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of matching book spaces
	 */
	public static List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end) {

		return getPersistence().findByAttractionsId(attractionsId, start, end);
	}

	/**
	 * Returns an ordered range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching book spaces
	 */
	public static List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().findByAttractionsId(
			attractionsId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the book spaces where attractionsId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param attractionsId the attractions ID
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching book spaces
	 */
	public static List<BookSpace> findByAttractionsId(
		long attractionsId, int start, int end,
		OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByAttractionsId(
			attractionsId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public static BookSpace findByAttractionsId_First(
			long attractionsId, OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByAttractionsId_First(
			attractionsId, orderByComparator);
	}

	/**
	 * Returns the first book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public static BookSpace fetchByAttractionsId_First(
		long attractionsId, OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().fetchByAttractionsId_First(
			attractionsId, orderByComparator);
	}

	/**
	 * Returns the last book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space
	 * @throws NoSuchBookSpaceException if a matching book space could not be found
	 */
	public static BookSpace findByAttractionsId_Last(
			long attractionsId, OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByAttractionsId_Last(
			attractionsId, orderByComparator);
	}

	/**
	 * Returns the last book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public static BookSpace fetchByAttractionsId_Last(
		long attractionsId, OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().fetchByAttractionsId_Last(
			attractionsId, orderByComparator);
	}

	/**
	 * Returns the book spaces before and after the current book space in the ordered set where attractionsId = &#63;.
	 *
	 * @param bookspaceId the primary key of the current book space
	 * @param attractionsId the attractions ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public static BookSpace[] findByAttractionsId_PrevAndNext(
			long bookspaceId, long attractionsId,
			OrderByComparator<BookSpace> orderByComparator)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByAttractionsId_PrevAndNext(
			bookspaceId, attractionsId, orderByComparator);
	}

	/**
	 * Removes all the book spaces where attractionsId = &#63; from the database.
	 *
	 * @param attractionsId the attractions ID
	 */
	public static void removeByAttractionsId(long attractionsId) {
		getPersistence().removeByAttractionsId(attractionsId);
	}

	/**
	 * Returns the number of book spaces where attractionsId = &#63;.
	 *
	 * @param attractionsId the attractions ID
	 * @return the number of matching book spaces
	 */
	public static int countByAttractionsId(long attractionsId) {
		return getPersistence().countByAttractionsId(attractionsId);
	}

	/**
	 * Caches the book space in the entity cache if it is enabled.
	 *
	 * @param bookSpace the book space
	 */
	public static void cacheResult(BookSpace bookSpace) {
		getPersistence().cacheResult(bookSpace);
	}

	/**
	 * Caches the book spaces in the entity cache if it is enabled.
	 *
	 * @param bookSpaces the book spaces
	 */
	public static void cacheResult(List<BookSpace> bookSpaces) {
		getPersistence().cacheResult(bookSpaces);
	}

	/**
	 * Creates a new book space with the primary key. Does not add the book space to the database.
	 *
	 * @param bookspaceId the primary key for the new book space
	 * @return the new book space
	 */
	public static BookSpace create(long bookspaceId) {
		return getPersistence().create(bookspaceId);
	}

	/**
	 * Removes the book space with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space that was removed
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public static BookSpace remove(long bookspaceId)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().remove(bookspaceId);
	}

	public static BookSpace updateImpl(BookSpace bookSpace) {
		return getPersistence().updateImpl(bookSpace);
	}

	/**
	 * Returns the book space with the primary key or throws a <code>NoSuchBookSpaceException</code> if it could not be found.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space
	 * @throws NoSuchBookSpaceException if a book space with the primary key could not be found
	 */
	public static BookSpace findByPrimaryKey(long bookspaceId)
		throws attractions.exception.NoSuchBookSpaceException {

		return getPersistence().findByPrimaryKey(bookspaceId);
	}

	/**
	 * Returns the book space with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space, or <code>null</code> if a book space with the primary key could not be found
	 */
	public static BookSpace fetchByPrimaryKey(long bookspaceId) {
		return getPersistence().fetchByPrimaryKey(bookspaceId);
	}

	/**
	 * Returns all the book spaces.
	 *
	 * @return the book spaces
	 */
	public static List<BookSpace> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of book spaces
	 */
	public static List<BookSpace> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of book spaces
	 */
	public static List<BookSpace> findAll(
		int start, int end, OrderByComparator<BookSpace> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of book spaces
	 */
	public static List<BookSpace> findAll(
		int start, int end, OrderByComparator<BookSpace> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the book spaces from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of book spaces.
	 *
	 * @return the number of book spaces
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static BookSpacePersistence getPersistence() {
		return _persistence;
	}

	private static volatile BookSpacePersistence _persistence;

}