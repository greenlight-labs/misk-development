/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookSpaceService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookSpaceService
 * @generated
 */
public class BookSpaceServiceWrapper
	implements BookSpaceService, ServiceWrapper<BookSpaceService> {

	public BookSpaceServiceWrapper(BookSpaceService bookSpaceService) {
		_bookSpaceService = bookSpaceService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _bookSpaceService.getOSGiServiceIdentifier();
	}

	@Override
	public BookSpaceService getWrappedService() {
		return _bookSpaceService;
	}

	@Override
	public void setWrappedService(BookSpaceService bookSpaceService) {
		_bookSpaceService = bookSpaceService;
	}

	private BookSpaceService _bookSpaceService;

}