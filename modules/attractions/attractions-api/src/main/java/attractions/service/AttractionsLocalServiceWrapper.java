/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AttractionsLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AttractionsLocalService
 * @generated
 */
public class AttractionsLocalServiceWrapper
	implements AttractionsLocalService,
			   ServiceWrapper<AttractionsLocalService> {

	public AttractionsLocalServiceWrapper(
		AttractionsLocalService attractionsLocalService) {

		_attractionsLocalService = attractionsLocalService;
	}

	/**
	 * Adds the attractions to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was added
	 */
	@Override
	public attractions.model.Attractions addAttractions(
		attractions.model.Attractions attractions) {

		return _attractionsLocalService.addAttractions(attractions);
	}

	@Override
	public attractions.model.Attractions addAttractions(
			long userId,
			java.util.Map<java.util.Locale, String> forumBannerImage,
			java.util.Map<java.util.Locale, String> forumBannerTitle,
			java.util.Map<java.util.Locale, String> forumButtonLabel,
			java.util.Map<java.util.Locale, String> forumButtonColor,
			java.util.Map<java.util.Locale, String> forumBannerIcon,
			java.util.Map<java.util.Locale, String> workingHoursLabel,
			java.util.Map<java.util.Locale, String> workingHours,
			java.util.Map<java.util.Locale, String> workingDays,
			java.util.Map<java.util.Locale, String> description,
			java.util.Map<java.util.Locale, String> locationLabel,
			java.util.Map<java.util.Locale, String> locationLatitude,
			java.util.Map<java.util.Locale, String> locationLongitude,
			java.util.Map<java.util.Locale, String> contactLabel,
			java.util.Map<java.util.Locale, String> contactEmailAddress,
			java.util.Map<java.util.Locale, String> contactTelephone,
			java.util.Map<java.util.Locale, String> amenitiesLabel,
			java.util.Map<java.util.Locale, String> bookSpaceLabel,
			java.util.Map<java.util.Locale, String> gallaryLabel,
			java.util.Map<java.util.Locale, String> workingHoursLabelColor,
			java.util.Map<java.util.Locale, String> workingHoursImage,
			java.util.Map<java.util.Locale, String> workingDaysImage,
			java.util.Map<java.util.Locale, String> contactEmailAddressIcon,
			java.util.Map<java.util.Locale, String> contactTelephoneIcon,
			String eventId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.addAttractions(
			userId, forumBannerImage, forumBannerTitle, forumButtonLabel,
			forumButtonColor, forumBannerIcon, workingHoursLabel, workingHours,
			workingDays, description, locationLabel, locationLatitude,
			locationLongitude, contactLabel, contactEmailAddress,
			contactTelephone, amenitiesLabel, bookSpaceLabel, gallaryLabel,
			workingHoursLabelColor, workingHoursImage, workingDaysImage,
			contactEmailAddressIcon, contactTelephoneIcon, eventId,
			serviceContext);
	}

	@Override
	public void addEntryAmenities(
			attractions.model.Attractions orgEntry,
			javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		_attractionsLocalService.addEntryAmenities(orgEntry, request);
	}

	@Override
	public void addEntryBookSpace(
			attractions.model.Attractions orgEntry,
			javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		_attractionsLocalService.addEntryBookSpace(orgEntry, request);
	}

	@Override
	public void addEntryGallery(
			attractions.model.Attractions orgEntry,
			javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		_attractionsLocalService.addEntryGallery(orgEntry, request);
	}

	/**
	 * Creates a new attractions with the primary key. Does not add the attractions to the database.
	 *
	 * @param attractionsId the primary key for the new attractions
	 * @return the new attractions
	 */
	@Override
	public attractions.model.Attractions createAttractions(long attractionsId) {
		return _attractionsLocalService.createAttractions(attractionsId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the attractions from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was removed
	 */
	@Override
	public attractions.model.Attractions deleteAttractions(
		attractions.model.Attractions attractions) {

		return _attractionsLocalService.deleteAttractions(attractions);
	}

	/**
	 * Deletes the attractions with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions that was removed
	 * @throws PortalException if a attractions with the primary key could not be found
	 */
	@Override
	public attractions.model.Attractions deleteAttractions(long attractionsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.deleteAttractions(attractionsId);
	}

	@Override
	public attractions.model.Attractions deleteAttractions(
			long attractionsId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _attractionsLocalService.deleteAttractions(
			attractionsId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _attractionsLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _attractionsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _attractionsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _attractionsLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _attractionsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _attractionsLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public attractions.model.Attractions fetchAttractions(long attractionsId) {
		return _attractionsLocalService.fetchAttractions(attractionsId);
	}

	/**
	 * Returns the attractions matching the UUID and group.
	 *
	 * @param uuid the attractions's UUID
	 * @param groupId the primary key of the group
	 * @return the matching attractions, or <code>null</code> if a matching attractions could not be found
	 */
	@Override
	public attractions.model.Attractions fetchAttractionsByUuidAndGroupId(
		String uuid, long groupId) {

		return _attractionsLocalService.fetchAttractionsByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _attractionsLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<attractions.model.Amenities> getAmenities(
		javax.portlet.ActionRequest actionRequest) {

		return _attractionsLocalService.getAmenities(actionRequest);
	}

	@Override
	public java.util.List<attractions.model.Amenities> getAmenities(
		javax.portlet.ActionRequest actionRequest,
		java.util.List<attractions.model.Amenities> defaultAmenities) {

		return _attractionsLocalService.getAmenities(
			actionRequest, defaultAmenities);
	}

	/**
	 * Returns the attractions with the primary key.
	 *
	 * @param attractionsId the primary key of the attractions
	 * @return the attractions
	 * @throws PortalException if a attractions with the primary key could not be found
	 */
	@Override
	public attractions.model.Attractions getAttractions(long attractionsId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.getAttractions(attractionsId);
	}

	/**
	 * Returns the attractions matching the UUID and group.
	 *
	 * @param uuid the attractions's UUID
	 * @param groupId the primary key of the group
	 * @return the matching attractions
	 * @throws PortalException if a matching attractions could not be found
	 */
	@Override
	public attractions.model.Attractions getAttractionsByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.getAttractionsByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public int getAttractionsCount(long groupId) {
		return _attractionsLocalService.getAttractionsCount(groupId);
	}

	/**
	 * Returns a range of all the attractionses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.AttractionsModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @return the range of attractionses
	 */
	@Override
	public java.util.List<attractions.model.Attractions> getAttractionses(
		int start, int end) {

		return _attractionsLocalService.getAttractionses(start, end);
	}

	/**
	 * Returns all the attractionses matching the UUID and company.
	 *
	 * @param uuid the UUID of the attractionses
	 * @param companyId the primary key of the company
	 * @return the matching attractionses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<attractions.model.Attractions>
		getAttractionsesByUuidAndCompanyId(String uuid, long companyId) {

		return _attractionsLocalService.getAttractionsesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of attractionses matching the UUID and company.
	 *
	 * @param uuid the UUID of the attractionses
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of attractionses
	 * @param end the upper bound of the range of attractionses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching attractionses, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<attractions.model.Attractions>
		getAttractionsesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<attractions.model.Attractions> orderByComparator) {

		return _attractionsLocalService.getAttractionsesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of attractionses.
	 *
	 * @return the number of attractionses
	 */
	@Override
	public int getAttractionsesCount() {
		return _attractionsLocalService.getAttractionsesCount();
	}

	@Override
	public java.util.List<attractions.model.Attractions> getAttractionsList(
		long groupId) {

		return _attractionsLocalService.getAttractionsList(groupId);
	}

	@Override
	public java.util.List<attractions.model.Attractions> getAttractionsList(
		long groupId, int start, int end) {

		return _attractionsLocalService.getAttractionsList(groupId, start, end);
	}

	@Override
	public java.util.List<attractions.model.Attractions> getAttractionsList(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<attractions.model.Attractions> obc) {

		return _attractionsLocalService.getAttractionsList(
			groupId, start, end, obc);
	}

	@Override
	public java.util.List<attractions.model.BookSpace> getBookSpace(
		javax.portlet.ActionRequest actionRequest) {

		return _attractionsLocalService.getBookSpace(actionRequest);
	}

	@Override
	public java.util.List<attractions.model.BookSpace> getBookSpace(
		javax.portlet.ActionRequest actionRequest,
		java.util.List<attractions.model.BookSpace> defaultSchedules) {

		return _attractionsLocalService.getBookSpace(
			actionRequest, defaultSchedules);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _attractionsLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public java.util.List<attractions.model.Gallary> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return _attractionsLocalService.getGalleries(actionRequest);
	}

	@Override
	public java.util.List<attractions.model.Gallary> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		java.util.List<attractions.model.Gallary> defaultGalleries) {

		return _attractionsLocalService.getGalleries(
			actionRequest, defaultGalleries);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _attractionsLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _attractionsLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public java.util.List<attractions.model.Attractions> searchAttractions(
		SearchContext searchContext) {

		return _attractionsLocalService.searchAttractions(searchContext);
	}

	@Override
	public void updateAmenities(
			attractions.model.Attractions entry,
			java.util.List<attractions.model.Amenities> AmenitiesList,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_attractionsLocalService.updateAmenities(
			entry, AmenitiesList, serviceContext);
	}

	/**
	 * Updates the attractions in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AttractionsLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param attractions the attractions
	 * @return the attractions that was updated
	 */
	@Override
	public attractions.model.Attractions updateAttractions(
		attractions.model.Attractions attractions) {

		return _attractionsLocalService.updateAttractions(attractions);
	}

	@Override
	public attractions.model.Attractions updateAttractions(
			long userId, long attractionsId,
			java.util.Map<java.util.Locale, String> forumBannerImage,
			java.util.Map<java.util.Locale, String> forumBannerTitle,
			java.util.Map<java.util.Locale, String> forumButtonLabel,
			java.util.Map<java.util.Locale, String> forumButtonColor,
			java.util.Map<java.util.Locale, String> forumBannerIcon,
			java.util.Map<java.util.Locale, String> workingHoursLabel,
			java.util.Map<java.util.Locale, String> workingHours,
			java.util.Map<java.util.Locale, String> workingDays,
			java.util.Map<java.util.Locale, String> description,
			java.util.Map<java.util.Locale, String> locationLabel,
			java.util.Map<java.util.Locale, String> locationLatitude,
			java.util.Map<java.util.Locale, String> locationLongitude,
			java.util.Map<java.util.Locale, String> contactLabel,
			java.util.Map<java.util.Locale, String> contactEmailAddress,
			java.util.Map<java.util.Locale, String> contactTelephone,
			java.util.Map<java.util.Locale, String> amenitiesLabel,
			java.util.Map<java.util.Locale, String> bookSpaceLabel,
			java.util.Map<java.util.Locale, String> gallaryLabel,
			java.util.Map<java.util.Locale, String> workingHoursLabelColor,
			java.util.Map<java.util.Locale, String> workingHoursImage,
			java.util.Map<java.util.Locale, String> workingDaysImage,
			java.util.Map<java.util.Locale, String> contactEmailAddressIcon,
			java.util.Map<java.util.Locale, String> contactTelephoneIcon,
			String eventId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _attractionsLocalService.updateAttractions(
			userId, attractionsId, forumBannerImage, forumBannerTitle,
			forumButtonLabel, forumButtonColor, forumBannerIcon,
			workingHoursLabel, workingHours, workingDays, description,
			locationLabel, locationLatitude, locationLongitude, contactLabel,
			contactEmailAddress, contactTelephone, amenitiesLabel,
			bookSpaceLabel, gallaryLabel, workingHoursLabelColor,
			workingHoursImage, workingDaysImage, contactEmailAddressIcon,
			contactTelephoneIcon, eventId, serviceContext);
	}

	@Override
	public void updateAttractionssGalleries(
			attractions.model.Attractions entry,
			java.util.List<attractions.model.Gallary> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_attractionsLocalService.updateAttractionssGalleries(
			entry, galleries, serviceContext);
	}

	@Override
	public void updateBookSpace(
			attractions.model.Attractions entry,
			java.util.List<attractions.model.BookSpace> bookspaceList,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_attractionsLocalService.updateBookSpace(
			entry, bookspaceList, serviceContext);
	}

	@Override
	public AttractionsLocalService getWrappedService() {
		return _attractionsLocalService;
	}

	@Override
	public void setWrappedService(
		AttractionsLocalService attractionsLocalService) {

		_attractionsLocalService = attractionsLocalService;
	}

	private AttractionsLocalService _attractionsLocalService;

}