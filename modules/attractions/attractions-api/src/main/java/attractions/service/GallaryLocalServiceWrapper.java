/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link GallaryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see GallaryLocalService
 * @generated
 */
public class GallaryLocalServiceWrapper
	implements GallaryLocalService, ServiceWrapper<GallaryLocalService> {

	public GallaryLocalServiceWrapper(GallaryLocalService gallaryLocalService) {
		_gallaryLocalService = gallaryLocalService;
	}

	@Override
	public attractions.model.Gallary addEntry(
			attractions.model.Gallary orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the gallary to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallary the gallary
	 * @return the gallary that was added
	 */
	@Override
	public attractions.model.Gallary addGallary(
		attractions.model.Gallary gallary) {

		return _gallaryLocalService.addGallary(gallary);
	}

	/**
	 * Creates a new gallary with the primary key. Does not add the gallary to the database.
	 *
	 * @param gallaryId the primary key for the new gallary
	 * @return the new gallary
	 */
	@Override
	public attractions.model.Gallary createGallary(long gallaryId) {
		return _gallaryLocalService.createGallary(gallaryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public attractions.model.Gallary deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.deleteEntry(primaryKey);
	}

	/**
	 * Deletes the gallary from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallary the gallary
	 * @return the gallary that was removed
	 */
	@Override
	public attractions.model.Gallary deleteGallary(
		attractions.model.Gallary gallary) {

		return _gallaryLocalService.deleteGallary(gallary);
	}

	/**
	 * Deletes the gallary with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary that was removed
	 * @throws PortalException if a gallary with the primary key could not be found
	 */
	@Override
	public attractions.model.Gallary deleteGallary(long gallaryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.deleteGallary(gallaryId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _gallaryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _gallaryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _gallaryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _gallaryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _gallaryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _gallaryLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public attractions.model.Gallary fetchGallary(long gallaryId) {
		return _gallaryLocalService.fetchGallary(gallaryId);
	}

	/**
	 * Returns the gallary with the matching UUID and company.
	 *
	 * @param uuid the gallary's UUID
	 * @param companyId the primary key of the company
	 * @return the matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	@Override
	public attractions.model.Gallary fetchGallaryByUuidAndCompanyId(
		String uuid, long companyId) {

		return _gallaryLocalService.fetchGallaryByUuidAndCompanyId(
			uuid, companyId);
	}

	@Override
	public java.util.List<attractions.model.Gallary> findAllInAttractions(
		long attractionId) {

		return _gallaryLocalService.findAllInAttractions(attractionId);
	}

	@Override
	public java.util.List<attractions.model.Gallary> findAllInAttractions(
		long attractionId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<attractions.model.Gallary> obc) {

		return _gallaryLocalService.findAllInAttractions(
			attractionId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _gallaryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _gallaryLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	/**
	 * Returns a range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of gallaries
	 */
	@Override
	public java.util.List<attractions.model.Gallary> getGallaries(
		int start, int end) {

		return _gallaryLocalService.getGallaries(start, end);
	}

	/**
	 * Returns the number of gallaries.
	 *
	 * @return the number of gallaries
	 */
	@Override
	public int getGallariesCount() {
		return _gallaryLocalService.getGallariesCount();
	}

	/**
	 * Returns the gallary with the primary key.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary
	 * @throws PortalException if a gallary with the primary key could not be found
	 */
	@Override
	public attractions.model.Gallary getGallary(long gallaryId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.getGallary(gallaryId);
	}

	/**
	 * Returns the gallary with the matching UUID and company.
	 *
	 * @param uuid the gallary's UUID
	 * @param companyId the primary key of the company
	 * @return the matching gallary
	 * @throws PortalException if a matching gallary could not be found
	 */
	@Override
	public attractions.model.Gallary getGallaryByUuidAndCompanyId(
			String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.getGallaryByUuidAndCompanyId(
			uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _gallaryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _gallaryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public attractions.model.Gallary updateEntry(
			attractions.model.Gallary orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _gallaryLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the gallary in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallary the gallary
	 * @return the gallary that was updated
	 */
	@Override
	public attractions.model.Gallary updateGallary(
		attractions.model.Gallary gallary) {

		return _gallaryLocalService.updateGallary(gallary);
	}

	@Override
	public GallaryLocalService getWrappedService() {
		return _gallaryLocalService;
	}

	@Override
	public void setWrappedService(GallaryLocalService gallaryLocalService) {
		_gallaryLocalService = gallaryLocalService;
	}

	private GallaryLocalService _gallaryLocalService;

}