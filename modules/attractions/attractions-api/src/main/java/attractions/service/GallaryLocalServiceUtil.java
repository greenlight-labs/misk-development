/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service;

import attractions.model.Gallary;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Gallary. This utility wraps
 * <code>attractions.service.impl.GallaryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see GallaryLocalService
 * @generated
 */
public class GallaryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>attractions.service.impl.GallaryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Gallary addEntry(
			Gallary orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the gallary to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallary the gallary
	 * @return the gallary that was added
	 */
	public static Gallary addGallary(Gallary gallary) {
		return getService().addGallary(gallary);
	}

	/**
	 * Creates a new gallary with the primary key. Does not add the gallary to the database.
	 *
	 * @param gallaryId the primary key for the new gallary
	 * @return the new gallary
	 */
	public static Gallary createGallary(long gallaryId) {
		return getService().createGallary(gallaryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static Gallary deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * Deletes the gallary from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallary the gallary
	 * @return the gallary that was removed
	 */
	public static Gallary deleteGallary(Gallary gallary) {
		return getService().deleteGallary(gallary);
	}

	/**
	 * Deletes the gallary with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary that was removed
	 * @throws PortalException if a gallary with the primary key could not be found
	 */
	public static Gallary deleteGallary(long gallaryId) throws PortalException {
		return getService().deleteGallary(gallaryId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Gallary fetchGallary(long gallaryId) {
		return getService().fetchGallary(gallaryId);
	}

	/**
	 * Returns the gallary with the matching UUID and company.
	 *
	 * @param uuid the gallary's UUID
	 * @param companyId the primary key of the company
	 * @return the matching gallary, or <code>null</code> if a matching gallary could not be found
	 */
	public static Gallary fetchGallaryByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchGallaryByUuidAndCompanyId(uuid, companyId);
	}

	public static List<Gallary> findAllInAttractions(long attractionId) {
		return getService().findAllInAttractions(attractionId);
	}

	public static List<Gallary> findAllInAttractions(
		long attractionId, int start, int end, OrderByComparator<Gallary> obc) {

		return getService().findAllInAttractions(attractionId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	 * Returns a range of all the gallaries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.GallaryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of gallaries
	 * @param end the upper bound of the range of gallaries (not inclusive)
	 * @return the range of gallaries
	 */
	public static List<Gallary> getGallaries(int start, int end) {
		return getService().getGallaries(start, end);
	}

	/**
	 * Returns the number of gallaries.
	 *
	 * @return the number of gallaries
	 */
	public static int getGallariesCount() {
		return getService().getGallariesCount();
	}

	/**
	 * Returns the gallary with the primary key.
	 *
	 * @param gallaryId the primary key of the gallary
	 * @return the gallary
	 * @throws PortalException if a gallary with the primary key could not be found
	 */
	public static Gallary getGallary(long gallaryId) throws PortalException {
		return getService().getGallary(gallaryId);
	}

	/**
	 * Returns the gallary with the matching UUID and company.
	 *
	 * @param uuid the gallary's UUID
	 * @param companyId the primary key of the company
	 * @return the matching gallary
	 * @throws PortalException if a matching gallary could not be found
	 */
	public static Gallary getGallaryByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getGallaryByUuidAndCompanyId(uuid, companyId);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static Gallary updateEntry(
			Gallary orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the gallary in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GallaryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallary the gallary
	 * @return the gallary that was updated
	 */
	public static Gallary updateGallary(Gallary gallary) {
		return getService().updateGallary(gallary);
	}

	public static GallaryLocalService getService() {
		return _service;
	}

	private static volatile GallaryLocalService _service;

}