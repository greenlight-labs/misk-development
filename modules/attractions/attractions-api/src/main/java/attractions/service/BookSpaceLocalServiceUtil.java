/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.service;

import attractions.model.BookSpace;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for BookSpace. This utility wraps
 * <code>attractions.service.impl.BookSpaceLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see BookSpaceLocalService
 * @generated
 */
public class BookSpaceLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>attractions.service.impl.BookSpaceLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the book space to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookSpace the book space
	 * @return the book space that was added
	 */
	public static BookSpace addBookSpace(BookSpace bookSpace) {
		return getService().addBookSpace(bookSpace);
	}

	public static BookSpace addEntry(
			BookSpace orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Creates a new book space with the primary key. Does not add the book space to the database.
	 *
	 * @param bookspaceId the primary key for the new book space
	 * @return the new book space
	 */
	public static BookSpace createBookSpace(long bookspaceId) {
		return getService().createBookSpace(bookspaceId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the book space from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookSpace the book space
	 * @return the book space that was removed
	 */
	public static BookSpace deleteBookSpace(BookSpace bookSpace) {
		return getService().deleteBookSpace(bookSpace);
	}

	/**
	 * Deletes the book space with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space that was removed
	 * @throws PortalException if a book space with the primary key could not be found
	 */
	public static BookSpace deleteBookSpace(long bookspaceId)
		throws PortalException {

		return getService().deleteBookSpace(bookspaceId);
	}

	public static BookSpace deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static BookSpace fetchBookSpace(long bookspaceId) {
		return getService().fetchBookSpace(bookspaceId);
	}

	/**
	 * Returns the book space with the matching UUID and company.
	 *
	 * @param uuid the book space's UUID
	 * @param companyId the primary key of the company
	 * @return the matching book space, or <code>null</code> if a matching book space could not be found
	 */
	public static BookSpace fetchBookSpaceByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchBookSpaceByUuidAndCompanyId(uuid, companyId);
	}

	public static List<BookSpace> findAllInAttractions(long attractionId) {
		return getService().findAllInAttractions(attractionId);
	}

	public static List<BookSpace> findAllInAttractions(
		long attractionId, int start, int end,
		OrderByComparator<BookSpace> obc) {

		return getService().findAllInAttractions(attractionId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the book space with the primary key.
	 *
	 * @param bookspaceId the primary key of the book space
	 * @return the book space
	 * @throws PortalException if a book space with the primary key could not be found
	 */
	public static BookSpace getBookSpace(long bookspaceId)
		throws PortalException {

		return getService().getBookSpace(bookspaceId);
	}

	/**
	 * Returns the book space with the matching UUID and company.
	 *
	 * @param uuid the book space's UUID
	 * @param companyId the primary key of the company
	 * @return the matching book space
	 * @throws PortalException if a matching book space could not be found
	 */
	public static BookSpace getBookSpaceByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getBookSpaceByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the book spaces.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>attractions.model.impl.BookSpaceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of book spaces
	 * @param end the upper bound of the range of book spaces (not inclusive)
	 * @return the range of book spaces
	 */
	public static List<BookSpace> getBookSpaces(int start, int end) {
		return getService().getBookSpaces(start, end);
	}

	/**
	 * Returns the number of book spaces.
	 *
	 * @return the number of book spaces
	 */
	public static int getBookSpacesCount() {
		return getService().getBookSpacesCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the book space in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookSpaceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookSpace the book space
	 * @return the book space that was updated
	 */
	public static BookSpace updateBookSpace(BookSpace bookSpace) {
		return getService().updateBookSpace(bookSpace);
	}

	public static BookSpace updateEntry(
			BookSpace orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static BookSpaceLocalService getService() {
		return _service;
	}

	private static volatile BookSpaceLocalService _service;

}