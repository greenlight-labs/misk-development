/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link attractions.service.http.AmenitiesServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class AmenitiesSoap implements Serializable {

	public static AmenitiesSoap toSoapModel(Amenities model) {
		AmenitiesSoap soapModel = new AmenitiesSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setAmenitiesId(model.getAmenitiesId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAttractionsId(model.getAttractionsId());
		soapModel.setTitle(model.getTitle());
		soapModel.setImage(model.getImage());

		return soapModel;
	}

	public static AmenitiesSoap[] toSoapModels(Amenities[] models) {
		AmenitiesSoap[] soapModels = new AmenitiesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AmenitiesSoap[][] toSoapModels(Amenities[][] models) {
		AmenitiesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AmenitiesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new AmenitiesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AmenitiesSoap[] toSoapModels(List<Amenities> models) {
		List<AmenitiesSoap> soapModels = new ArrayList<AmenitiesSoap>(
			models.size());

		for (Amenities model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AmenitiesSoap[soapModels.size()]);
	}

	public AmenitiesSoap() {
	}

	public long getPrimaryKey() {
		return _amenitiesId;
	}

	public void setPrimaryKey(long pk) {
		setAmenitiesId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getAmenitiesId() {
		return _amenitiesId;
	}

	public void setAmenitiesId(long amenitiesId) {
		_amenitiesId = amenitiesId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getAttractionsId() {
		return _attractionsId;
	}

	public void setAttractionsId(long attractionsId) {
		_attractionsId = attractionsId;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	private String _uuid;
	private long _amenitiesId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _attractionsId;
	private String _title;
	private String _image;

}