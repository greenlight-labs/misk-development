/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Attractions}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Attractions
 * @generated
 */
public class AttractionsWrapper
	extends BaseModelWrapper<Attractions>
	implements Attractions, ModelWrapper<Attractions> {

	public AttractionsWrapper(Attractions attractions) {
		super(attractions);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("attractionsId", getAttractionsId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("forumBannerImage", getForumBannerImage());
		attributes.put("forumBannerTitle", getForumBannerTitle());
		attributes.put("forumButtonLabel", getForumButtonLabel());
		attributes.put("forumButtonColor", getForumButtonColor());
		attributes.put("forumBannerIcon", getForumBannerIcon());
		attributes.put("workingHoursLabel", getWorkingHoursLabel());
		attributes.put("workingHours", getWorkingHours());
		attributes.put("workingDays", getWorkingDays());
		attributes.put("workingHoursLabelColor", getWorkingHoursLabelColor());
		attributes.put("workingHoursImage", getWorkingHoursImage());
		attributes.put("workingDaysImage", getWorkingDaysImage());
		attributes.put("description", getDescription());
		attributes.put("locationLabel", getLocationLabel());
		attributes.put("locationLatitude", getLocationLatitude());
		attributes.put("locationLongitude", getLocationLongitude());
		attributes.put("contactLabel", getContactLabel());
		attributes.put("contactEmailAddress", getContactEmailAddress());
		attributes.put("contactTelephone", getContactTelephone());
		attributes.put("contactEmailAddressIcon", getContactEmailAddressIcon());
		attributes.put("contactTelephoneIcon", getContactTelephoneIcon());
		attributes.put("amenitiesLabel", getAmenitiesLabel());
		attributes.put("bookSpaceLabel", getBookSpaceLabel());
		attributes.put("gallaryLabel", getGallaryLabel());
		attributes.put("eventId", getEventId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long attractionsId = (Long)attributes.get("attractionsId");

		if (attractionsId != null) {
			setAttractionsId(attractionsId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String forumBannerImage = (String)attributes.get("forumBannerImage");

		if (forumBannerImage != null) {
			setForumBannerImage(forumBannerImage);
		}

		String forumBannerTitle = (String)attributes.get("forumBannerTitle");

		if (forumBannerTitle != null) {
			setForumBannerTitle(forumBannerTitle);
		}

		String forumButtonLabel = (String)attributes.get("forumButtonLabel");

		if (forumButtonLabel != null) {
			setForumButtonLabel(forumButtonLabel);
		}

		String forumButtonColor = (String)attributes.get("forumButtonColor");

		if (forumButtonColor != null) {
			setForumButtonColor(forumButtonColor);
		}

		String forumBannerIcon = (String)attributes.get("forumBannerIcon");

		if (forumBannerIcon != null) {
			setForumBannerIcon(forumBannerIcon);
		}

		String workingHoursLabel = (String)attributes.get("workingHoursLabel");

		if (workingHoursLabel != null) {
			setWorkingHoursLabel(workingHoursLabel);
		}

		String workingHours = (String)attributes.get("workingHours");

		if (workingHours != null) {
			setWorkingHours(workingHours);
		}

		String workingDays = (String)attributes.get("workingDays");

		if (workingDays != null) {
			setWorkingDays(workingDays);
		}

		String workingHoursLabelColor = (String)attributes.get(
			"workingHoursLabelColor");

		if (workingHoursLabelColor != null) {
			setWorkingHoursLabelColor(workingHoursLabelColor);
		}

		String workingHoursImage = (String)attributes.get("workingHoursImage");

		if (workingHoursImage != null) {
			setWorkingHoursImage(workingHoursImage);
		}

		String workingDaysImage = (String)attributes.get("workingDaysImage");

		if (workingDaysImage != null) {
			setWorkingDaysImage(workingDaysImage);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String locationLabel = (String)attributes.get("locationLabel");

		if (locationLabel != null) {
			setLocationLabel(locationLabel);
		}

		String locationLatitude = (String)attributes.get("locationLatitude");

		if (locationLatitude != null) {
			setLocationLatitude(locationLatitude);
		}

		String locationLongitude = (String)attributes.get("locationLongitude");

		if (locationLongitude != null) {
			setLocationLongitude(locationLongitude);
		}

		String contactLabel = (String)attributes.get("contactLabel");

		if (contactLabel != null) {
			setContactLabel(contactLabel);
		}

		String contactEmailAddress = (String)attributes.get(
			"contactEmailAddress");

		if (contactEmailAddress != null) {
			setContactEmailAddress(contactEmailAddress);
		}

		String contactTelephone = (String)attributes.get("contactTelephone");

		if (contactTelephone != null) {
			setContactTelephone(contactTelephone);
		}

		String contactEmailAddressIcon = (String)attributes.get(
			"contactEmailAddressIcon");

		if (contactEmailAddressIcon != null) {
			setContactEmailAddressIcon(contactEmailAddressIcon);
		}

		String contactTelephoneIcon = (String)attributes.get(
			"contactTelephoneIcon");

		if (contactTelephoneIcon != null) {
			setContactTelephoneIcon(contactTelephoneIcon);
		}

		String amenitiesLabel = (String)attributes.get("amenitiesLabel");

		if (amenitiesLabel != null) {
			setAmenitiesLabel(amenitiesLabel);
		}

		String bookSpaceLabel = (String)attributes.get("bookSpaceLabel");

		if (bookSpaceLabel != null) {
			setBookSpaceLabel(bookSpaceLabel);
		}

		String gallaryLabel = (String)attributes.get("gallaryLabel");

		if (gallaryLabel != null) {
			setGallaryLabel(gallaryLabel);
		}

		String eventId = (String)attributes.get("eventId");

		if (eventId != null) {
			setEventId(eventId);
		}
	}

	/**
	 * Returns the amenities label of this attractions.
	 *
	 * @return the amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel() {
		return model.getAmenitiesLabel();
	}

	/**
	 * Returns the localized amenities label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel(java.util.Locale locale) {
		return model.getAmenitiesLabel(locale);
	}

	/**
	 * Returns the localized amenities label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized amenities label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getAmenitiesLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getAmenitiesLabel(locale, useDefault);
	}

	/**
	 * Returns the localized amenities label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel(String languageId) {
		return model.getAmenitiesLabel(languageId);
	}

	/**
	 * Returns the localized amenities label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized amenities label of this attractions
	 */
	@Override
	public String getAmenitiesLabel(String languageId, boolean useDefault) {
		return model.getAmenitiesLabel(languageId, useDefault);
	}

	@Override
	public String getAmenitiesLabelCurrentLanguageId() {
		return model.getAmenitiesLabelCurrentLanguageId();
	}

	@Override
	public String getAmenitiesLabelCurrentValue() {
		return model.getAmenitiesLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized amenities labels of this attractions.
	 *
	 * @return the locales and localized amenities labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getAmenitiesLabelMap() {
		return model.getAmenitiesLabelMap();
	}

	/**
	 * Returns the attractions ID of this attractions.
	 *
	 * @return the attractions ID of this attractions
	 */
	@Override
	public long getAttractionsId() {
		return model.getAttractionsId();
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the book space label of this attractions.
	 *
	 * @return the book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel() {
		return model.getBookSpaceLabel();
	}

	/**
	 * Returns the localized book space label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel(java.util.Locale locale) {
		return model.getBookSpaceLabel(locale);
	}

	/**
	 * Returns the localized book space label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBookSpaceLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getBookSpaceLabel(locale, useDefault);
	}

	/**
	 * Returns the localized book space label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel(String languageId) {
		return model.getBookSpaceLabel(languageId);
	}

	/**
	 * Returns the localized book space label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space label of this attractions
	 */
	@Override
	public String getBookSpaceLabel(String languageId, boolean useDefault) {
		return model.getBookSpaceLabel(languageId, useDefault);
	}

	@Override
	public String getBookSpaceLabelCurrentLanguageId() {
		return model.getBookSpaceLabelCurrentLanguageId();
	}

	@Override
	public String getBookSpaceLabelCurrentValue() {
		return model.getBookSpaceLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized book space labels of this attractions.
	 *
	 * @return the locales and localized book space labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getBookSpaceLabelMap() {
		return model.getBookSpaceLabelMap();
	}

	/**
	 * Returns the company ID of this attractions.
	 *
	 * @return the company ID of this attractions
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the contact email address of this attractions.
	 *
	 * @return the contact email address of this attractions
	 */
	@Override
	public String getContactEmailAddress() {
		return model.getContactEmailAddress();
	}

	/**
	 * Returns the localized contact email address of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized contact email address of this attractions
	 */
	@Override
	public String getContactEmailAddress(java.util.Locale locale) {
		return model.getContactEmailAddress(locale);
	}

	/**
	 * Returns the localized contact email address of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact email address of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getContactEmailAddress(
		java.util.Locale locale, boolean useDefault) {

		return model.getContactEmailAddress(locale, useDefault);
	}

	/**
	 * Returns the localized contact email address of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized contact email address of this attractions
	 */
	@Override
	public String getContactEmailAddress(String languageId) {
		return model.getContactEmailAddress(languageId);
	}

	/**
	 * Returns the localized contact email address of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact email address of this attractions
	 */
	@Override
	public String getContactEmailAddress(
		String languageId, boolean useDefault) {

		return model.getContactEmailAddress(languageId, useDefault);
	}

	@Override
	public String getContactEmailAddressCurrentLanguageId() {
		return model.getContactEmailAddressCurrentLanguageId();
	}

	@Override
	public String getContactEmailAddressCurrentValue() {
		return model.getContactEmailAddressCurrentValue();
	}

	/**
	 * Returns the contact email address icon of this attractions.
	 *
	 * @return the contact email address icon of this attractions
	 */
	@Override
	public String getContactEmailAddressIcon() {
		return model.getContactEmailAddressIcon();
	}

	/**
	 * Returns the localized contact email address icon of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized contact email address icon of this attractions
	 */
	@Override
	public String getContactEmailAddressIcon(java.util.Locale locale) {
		return model.getContactEmailAddressIcon(locale);
	}

	/**
	 * Returns the localized contact email address icon of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact email address icon of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getContactEmailAddressIcon(
		java.util.Locale locale, boolean useDefault) {

		return model.getContactEmailAddressIcon(locale, useDefault);
	}

	/**
	 * Returns the localized contact email address icon of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized contact email address icon of this attractions
	 */
	@Override
	public String getContactEmailAddressIcon(String languageId) {
		return model.getContactEmailAddressIcon(languageId);
	}

	/**
	 * Returns the localized contact email address icon of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact email address icon of this attractions
	 */
	@Override
	public String getContactEmailAddressIcon(
		String languageId, boolean useDefault) {

		return model.getContactEmailAddressIcon(languageId, useDefault);
	}

	@Override
	public String getContactEmailAddressIconCurrentLanguageId() {
		return model.getContactEmailAddressIconCurrentLanguageId();
	}

	@Override
	public String getContactEmailAddressIconCurrentValue() {
		return model.getContactEmailAddressIconCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized contact email address icons of this attractions.
	 *
	 * @return the locales and localized contact email address icons of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getContactEmailAddressIconMap() {
		return model.getContactEmailAddressIconMap();
	}

	/**
	 * Returns a map of the locales and localized contact email addresses of this attractions.
	 *
	 * @return the locales and localized contact email addresses of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getContactEmailAddressMap() {
		return model.getContactEmailAddressMap();
	}

	/**
	 * Returns the contact label of this attractions.
	 *
	 * @return the contact label of this attractions
	 */
	@Override
	public String getContactLabel() {
		return model.getContactLabel();
	}

	/**
	 * Returns the localized contact label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized contact label of this attractions
	 */
	@Override
	public String getContactLabel(java.util.Locale locale) {
		return model.getContactLabel(locale);
	}

	/**
	 * Returns the localized contact label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getContactLabel(java.util.Locale locale, boolean useDefault) {
		return model.getContactLabel(locale, useDefault);
	}

	/**
	 * Returns the localized contact label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized contact label of this attractions
	 */
	@Override
	public String getContactLabel(String languageId) {
		return model.getContactLabel(languageId);
	}

	/**
	 * Returns the localized contact label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact label of this attractions
	 */
	@Override
	public String getContactLabel(String languageId, boolean useDefault) {
		return model.getContactLabel(languageId, useDefault);
	}

	@Override
	public String getContactLabelCurrentLanguageId() {
		return model.getContactLabelCurrentLanguageId();
	}

	@Override
	public String getContactLabelCurrentValue() {
		return model.getContactLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized contact labels of this attractions.
	 *
	 * @return the locales and localized contact labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getContactLabelMap() {
		return model.getContactLabelMap();
	}

	/**
	 * Returns the contact telephone of this attractions.
	 *
	 * @return the contact telephone of this attractions
	 */
	@Override
	public String getContactTelephone() {
		return model.getContactTelephone();
	}

	/**
	 * Returns the localized contact telephone of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized contact telephone of this attractions
	 */
	@Override
	public String getContactTelephone(java.util.Locale locale) {
		return model.getContactTelephone(locale);
	}

	/**
	 * Returns the localized contact telephone of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact telephone of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getContactTelephone(
		java.util.Locale locale, boolean useDefault) {

		return model.getContactTelephone(locale, useDefault);
	}

	/**
	 * Returns the localized contact telephone of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized contact telephone of this attractions
	 */
	@Override
	public String getContactTelephone(String languageId) {
		return model.getContactTelephone(languageId);
	}

	/**
	 * Returns the localized contact telephone of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact telephone of this attractions
	 */
	@Override
	public String getContactTelephone(String languageId, boolean useDefault) {
		return model.getContactTelephone(languageId, useDefault);
	}

	@Override
	public String getContactTelephoneCurrentLanguageId() {
		return model.getContactTelephoneCurrentLanguageId();
	}

	@Override
	public String getContactTelephoneCurrentValue() {
		return model.getContactTelephoneCurrentValue();
	}

	/**
	 * Returns the contact telephone icon of this attractions.
	 *
	 * @return the contact telephone icon of this attractions
	 */
	@Override
	public String getContactTelephoneIcon() {
		return model.getContactTelephoneIcon();
	}

	/**
	 * Returns the localized contact telephone icon of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized contact telephone icon of this attractions
	 */
	@Override
	public String getContactTelephoneIcon(java.util.Locale locale) {
		return model.getContactTelephoneIcon(locale);
	}

	/**
	 * Returns the localized contact telephone icon of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact telephone icon of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getContactTelephoneIcon(
		java.util.Locale locale, boolean useDefault) {

		return model.getContactTelephoneIcon(locale, useDefault);
	}

	/**
	 * Returns the localized contact telephone icon of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized contact telephone icon of this attractions
	 */
	@Override
	public String getContactTelephoneIcon(String languageId) {
		return model.getContactTelephoneIcon(languageId);
	}

	/**
	 * Returns the localized contact telephone icon of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized contact telephone icon of this attractions
	 */
	@Override
	public String getContactTelephoneIcon(
		String languageId, boolean useDefault) {

		return model.getContactTelephoneIcon(languageId, useDefault);
	}

	@Override
	public String getContactTelephoneIconCurrentLanguageId() {
		return model.getContactTelephoneIconCurrentLanguageId();
	}

	@Override
	public String getContactTelephoneIconCurrentValue() {
		return model.getContactTelephoneIconCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized contact telephone icons of this attractions.
	 *
	 * @return the locales and localized contact telephone icons of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getContactTelephoneIconMap() {
		return model.getContactTelephoneIconMap();
	}

	/**
	 * Returns a map of the locales and localized contact telephones of this attractions.
	 *
	 * @return the locales and localized contact telephones of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getContactTelephoneMap() {
		return model.getContactTelephoneMap();
	}

	/**
	 * Returns the create date of this attractions.
	 *
	 * @return the create date of this attractions
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this attractions.
	 *
	 * @return the description of this attractions
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this attractions
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this attractions
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this attractions
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this attractions.
	 *
	 * @return the locales and localized descriptions of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the event ID of this attractions.
	 *
	 * @return the event ID of this attractions
	 */
	@Override
	public String getEventId() {
		return model.getEventId();
	}

	/**
	 * Returns the forum banner icon of this attractions.
	 *
	 * @return the forum banner icon of this attractions
	 */
	@Override
	public String getForumBannerIcon() {
		return model.getForumBannerIcon();
	}

	/**
	 * Returns the localized forum banner icon of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized forum banner icon of this attractions
	 */
	@Override
	public String getForumBannerIcon(java.util.Locale locale) {
		return model.getForumBannerIcon(locale);
	}

	/**
	 * Returns the localized forum banner icon of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum banner icon of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getForumBannerIcon(
		java.util.Locale locale, boolean useDefault) {

		return model.getForumBannerIcon(locale, useDefault);
	}

	/**
	 * Returns the localized forum banner icon of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized forum banner icon of this attractions
	 */
	@Override
	public String getForumBannerIcon(String languageId) {
		return model.getForumBannerIcon(languageId);
	}

	/**
	 * Returns the localized forum banner icon of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum banner icon of this attractions
	 */
	@Override
	public String getForumBannerIcon(String languageId, boolean useDefault) {
		return model.getForumBannerIcon(languageId, useDefault);
	}

	@Override
	public String getForumBannerIconCurrentLanguageId() {
		return model.getForumBannerIconCurrentLanguageId();
	}

	@Override
	public String getForumBannerIconCurrentValue() {
		return model.getForumBannerIconCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized forum banner icons of this attractions.
	 *
	 * @return the locales and localized forum banner icons of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getForumBannerIconMap() {
		return model.getForumBannerIconMap();
	}

	/**
	 * Returns the forum banner image of this attractions.
	 *
	 * @return the forum banner image of this attractions
	 */
	@Override
	public String getForumBannerImage() {
		return model.getForumBannerImage();
	}

	/**
	 * Returns the localized forum banner image of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized forum banner image of this attractions
	 */
	@Override
	public String getForumBannerImage(java.util.Locale locale) {
		return model.getForumBannerImage(locale);
	}

	/**
	 * Returns the localized forum banner image of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum banner image of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getForumBannerImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getForumBannerImage(locale, useDefault);
	}

	/**
	 * Returns the localized forum banner image of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized forum banner image of this attractions
	 */
	@Override
	public String getForumBannerImage(String languageId) {
		return model.getForumBannerImage(languageId);
	}

	/**
	 * Returns the localized forum banner image of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum banner image of this attractions
	 */
	@Override
	public String getForumBannerImage(String languageId, boolean useDefault) {
		return model.getForumBannerImage(languageId, useDefault);
	}

	@Override
	public String getForumBannerImageCurrentLanguageId() {
		return model.getForumBannerImageCurrentLanguageId();
	}

	@Override
	public String getForumBannerImageCurrentValue() {
		return model.getForumBannerImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized forum banner images of this attractions.
	 *
	 * @return the locales and localized forum banner images of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getForumBannerImageMap() {
		return model.getForumBannerImageMap();
	}

	/**
	 * Returns the forum banner title of this attractions.
	 *
	 * @return the forum banner title of this attractions
	 */
	@Override
	public String getForumBannerTitle() {
		return model.getForumBannerTitle();
	}

	/**
	 * Returns the localized forum banner title of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized forum banner title of this attractions
	 */
	@Override
	public String getForumBannerTitle(java.util.Locale locale) {
		return model.getForumBannerTitle(locale);
	}

	/**
	 * Returns the localized forum banner title of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum banner title of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getForumBannerTitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getForumBannerTitle(locale, useDefault);
	}

	/**
	 * Returns the localized forum banner title of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized forum banner title of this attractions
	 */
	@Override
	public String getForumBannerTitle(String languageId) {
		return model.getForumBannerTitle(languageId);
	}

	/**
	 * Returns the localized forum banner title of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum banner title of this attractions
	 */
	@Override
	public String getForumBannerTitle(String languageId, boolean useDefault) {
		return model.getForumBannerTitle(languageId, useDefault);
	}

	@Override
	public String getForumBannerTitleCurrentLanguageId() {
		return model.getForumBannerTitleCurrentLanguageId();
	}

	@Override
	public String getForumBannerTitleCurrentValue() {
		return model.getForumBannerTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized forum banner titles of this attractions.
	 *
	 * @return the locales and localized forum banner titles of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getForumBannerTitleMap() {
		return model.getForumBannerTitleMap();
	}

	/**
	 * Returns the forum button color of this attractions.
	 *
	 * @return the forum button color of this attractions
	 */
	@Override
	public String getForumButtonColor() {
		return model.getForumButtonColor();
	}

	/**
	 * Returns the localized forum button color of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized forum button color of this attractions
	 */
	@Override
	public String getForumButtonColor(java.util.Locale locale) {
		return model.getForumButtonColor(locale);
	}

	/**
	 * Returns the localized forum button color of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum button color of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getForumButtonColor(
		java.util.Locale locale, boolean useDefault) {

		return model.getForumButtonColor(locale, useDefault);
	}

	/**
	 * Returns the localized forum button color of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized forum button color of this attractions
	 */
	@Override
	public String getForumButtonColor(String languageId) {
		return model.getForumButtonColor(languageId);
	}

	/**
	 * Returns the localized forum button color of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum button color of this attractions
	 */
	@Override
	public String getForumButtonColor(String languageId, boolean useDefault) {
		return model.getForumButtonColor(languageId, useDefault);
	}

	@Override
	public String getForumButtonColorCurrentLanguageId() {
		return model.getForumButtonColorCurrentLanguageId();
	}

	@Override
	public String getForumButtonColorCurrentValue() {
		return model.getForumButtonColorCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized forum button colors of this attractions.
	 *
	 * @return the locales and localized forum button colors of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getForumButtonColorMap() {
		return model.getForumButtonColorMap();
	}

	/**
	 * Returns the forum button label of this attractions.
	 *
	 * @return the forum button label of this attractions
	 */
	@Override
	public String getForumButtonLabel() {
		return model.getForumButtonLabel();
	}

	/**
	 * Returns the localized forum button label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized forum button label of this attractions
	 */
	@Override
	public String getForumButtonLabel(java.util.Locale locale) {
		return model.getForumButtonLabel(locale);
	}

	/**
	 * Returns the localized forum button label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum button label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getForumButtonLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getForumButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized forum button label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized forum button label of this attractions
	 */
	@Override
	public String getForumButtonLabel(String languageId) {
		return model.getForumButtonLabel(languageId);
	}

	/**
	 * Returns the localized forum button label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized forum button label of this attractions
	 */
	@Override
	public String getForumButtonLabel(String languageId, boolean useDefault) {
		return model.getForumButtonLabel(languageId, useDefault);
	}

	@Override
	public String getForumButtonLabelCurrentLanguageId() {
		return model.getForumButtonLabelCurrentLanguageId();
	}

	@Override
	public String getForumButtonLabelCurrentValue() {
		return model.getForumButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized forum button labels of this attractions.
	 *
	 * @return the locales and localized forum button labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getForumButtonLabelMap() {
		return model.getForumButtonLabelMap();
	}

	/**
	 * Returns the gallary label of this attractions.
	 *
	 * @return the gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel() {
		return model.getGallaryLabel();
	}

	/**
	 * Returns the localized gallary label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel(java.util.Locale locale) {
		return model.getGallaryLabel(locale);
	}

	/**
	 * Returns the localized gallary label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized gallary label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getGallaryLabel(java.util.Locale locale, boolean useDefault) {
		return model.getGallaryLabel(locale, useDefault);
	}

	/**
	 * Returns the localized gallary label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel(String languageId) {
		return model.getGallaryLabel(languageId);
	}

	/**
	 * Returns the localized gallary label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized gallary label of this attractions
	 */
	@Override
	public String getGallaryLabel(String languageId, boolean useDefault) {
		return model.getGallaryLabel(languageId, useDefault);
	}

	@Override
	public String getGallaryLabelCurrentLanguageId() {
		return model.getGallaryLabelCurrentLanguageId();
	}

	@Override
	public String getGallaryLabelCurrentValue() {
		return model.getGallaryLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized gallary labels of this attractions.
	 *
	 * @return the locales and localized gallary labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getGallaryLabelMap() {
		return model.getGallaryLabelMap();
	}

	/**
	 * Returns the group ID of this attractions.
	 *
	 * @return the group ID of this attractions
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the location label of this attractions.
	 *
	 * @return the location label of this attractions
	 */
	@Override
	public String getLocationLabel() {
		return model.getLocationLabel();
	}

	/**
	 * Returns the localized location label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized location label of this attractions
	 */
	@Override
	public String getLocationLabel(java.util.Locale locale) {
		return model.getLocationLabel(locale);
	}

	/**
	 * Returns the localized location label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLocationLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getLocationLabel(locale, useDefault);
	}

	/**
	 * Returns the localized location label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized location label of this attractions
	 */
	@Override
	public String getLocationLabel(String languageId) {
		return model.getLocationLabel(languageId);
	}

	/**
	 * Returns the localized location label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location label of this attractions
	 */
	@Override
	public String getLocationLabel(String languageId, boolean useDefault) {
		return model.getLocationLabel(languageId, useDefault);
	}

	@Override
	public String getLocationLabelCurrentLanguageId() {
		return model.getLocationLabelCurrentLanguageId();
	}

	@Override
	public String getLocationLabelCurrentValue() {
		return model.getLocationLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized location labels of this attractions.
	 *
	 * @return the locales and localized location labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getLocationLabelMap() {
		return model.getLocationLabelMap();
	}

	/**
	 * Returns the location latitude of this attractions.
	 *
	 * @return the location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude() {
		return model.getLocationLatitude();
	}

	/**
	 * Returns the localized location latitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude(java.util.Locale locale) {
		return model.getLocationLatitude(locale);
	}

	/**
	 * Returns the localized location latitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location latitude of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLocationLatitude(
		java.util.Locale locale, boolean useDefault) {

		return model.getLocationLatitude(locale, useDefault);
	}

	/**
	 * Returns the localized location latitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude(String languageId) {
		return model.getLocationLatitude(languageId);
	}

	/**
	 * Returns the localized location latitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location latitude of this attractions
	 */
	@Override
	public String getLocationLatitude(String languageId, boolean useDefault) {
		return model.getLocationLatitude(languageId, useDefault);
	}

	@Override
	public String getLocationLatitudeCurrentLanguageId() {
		return model.getLocationLatitudeCurrentLanguageId();
	}

	@Override
	public String getLocationLatitudeCurrentValue() {
		return model.getLocationLatitudeCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized location latitudes of this attractions.
	 *
	 * @return the locales and localized location latitudes of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getLocationLatitudeMap() {
		return model.getLocationLatitudeMap();
	}

	/**
	 * Returns the location longitude of this attractions.
	 *
	 * @return the location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude() {
		return model.getLocationLongitude();
	}

	/**
	 * Returns the localized location longitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude(java.util.Locale locale) {
		return model.getLocationLongitude(locale);
	}

	/**
	 * Returns the localized location longitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location longitude of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLocationLongitude(
		java.util.Locale locale, boolean useDefault) {

		return model.getLocationLongitude(locale, useDefault);
	}

	/**
	 * Returns the localized location longitude of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude(String languageId) {
		return model.getLocationLongitude(languageId);
	}

	/**
	 * Returns the localized location longitude of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location longitude of this attractions
	 */
	@Override
	public String getLocationLongitude(String languageId, boolean useDefault) {
		return model.getLocationLongitude(languageId, useDefault);
	}

	@Override
	public String getLocationLongitudeCurrentLanguageId() {
		return model.getLocationLongitudeCurrentLanguageId();
	}

	@Override
	public String getLocationLongitudeCurrentValue() {
		return model.getLocationLongitudeCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized location longitudes of this attractions.
	 *
	 * @return the locales and localized location longitudes of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getLocationLongitudeMap() {
		return model.getLocationLongitudeMap();
	}

	/**
	 * Returns the modified date of this attractions.
	 *
	 * @return the modified date of this attractions
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this attractions.
	 *
	 * @return the primary key of this attractions
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this attractions.
	 *
	 * @return the user ID of this attractions
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this attractions.
	 *
	 * @return the user name of this attractions
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this attractions.
	 *
	 * @return the user uuid of this attractions
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this attractions.
	 *
	 * @return the uuid of this attractions
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the working days of this attractions.
	 *
	 * @return the working days of this attractions
	 */
	@Override
	public String getWorkingDays() {
		return model.getWorkingDays();
	}

	/**
	 * Returns the localized working days of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized working days of this attractions
	 */
	@Override
	public String getWorkingDays(java.util.Locale locale) {
		return model.getWorkingDays(locale);
	}

	/**
	 * Returns the localized working days of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working days of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getWorkingDays(java.util.Locale locale, boolean useDefault) {
		return model.getWorkingDays(locale, useDefault);
	}

	/**
	 * Returns the localized working days of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized working days of this attractions
	 */
	@Override
	public String getWorkingDays(String languageId) {
		return model.getWorkingDays(languageId);
	}

	/**
	 * Returns the localized working days of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working days of this attractions
	 */
	@Override
	public String getWorkingDays(String languageId, boolean useDefault) {
		return model.getWorkingDays(languageId, useDefault);
	}

	@Override
	public String getWorkingDaysCurrentLanguageId() {
		return model.getWorkingDaysCurrentLanguageId();
	}

	@Override
	public String getWorkingDaysCurrentValue() {
		return model.getWorkingDaysCurrentValue();
	}

	/**
	 * Returns the working days image of this attractions.
	 *
	 * @return the working days image of this attractions
	 */
	@Override
	public String getWorkingDaysImage() {
		return model.getWorkingDaysImage();
	}

	/**
	 * Returns the localized working days image of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized working days image of this attractions
	 */
	@Override
	public String getWorkingDaysImage(java.util.Locale locale) {
		return model.getWorkingDaysImage(locale);
	}

	/**
	 * Returns the localized working days image of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working days image of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getWorkingDaysImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getWorkingDaysImage(locale, useDefault);
	}

	/**
	 * Returns the localized working days image of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized working days image of this attractions
	 */
	@Override
	public String getWorkingDaysImage(String languageId) {
		return model.getWorkingDaysImage(languageId);
	}

	/**
	 * Returns the localized working days image of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working days image of this attractions
	 */
	@Override
	public String getWorkingDaysImage(String languageId, boolean useDefault) {
		return model.getWorkingDaysImage(languageId, useDefault);
	}

	@Override
	public String getWorkingDaysImageCurrentLanguageId() {
		return model.getWorkingDaysImageCurrentLanguageId();
	}

	@Override
	public String getWorkingDaysImageCurrentValue() {
		return model.getWorkingDaysImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized working days images of this attractions.
	 *
	 * @return the locales and localized working days images of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getWorkingDaysImageMap() {
		return model.getWorkingDaysImageMap();
	}

	/**
	 * Returns a map of the locales and localized working dayses of this attractions.
	 *
	 * @return the locales and localized working dayses of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getWorkingDaysMap() {
		return model.getWorkingDaysMap();
	}

	/**
	 * Returns the working hours of this attractions.
	 *
	 * @return the working hours of this attractions
	 */
	@Override
	public String getWorkingHours() {
		return model.getWorkingHours();
	}

	/**
	 * Returns the localized working hours of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized working hours of this attractions
	 */
	@Override
	public String getWorkingHours(java.util.Locale locale) {
		return model.getWorkingHours(locale);
	}

	/**
	 * Returns the localized working hours of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getWorkingHours(java.util.Locale locale, boolean useDefault) {
		return model.getWorkingHours(locale, useDefault);
	}

	/**
	 * Returns the localized working hours of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized working hours of this attractions
	 */
	@Override
	public String getWorkingHours(String languageId) {
		return model.getWorkingHours(languageId);
	}

	/**
	 * Returns the localized working hours of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours of this attractions
	 */
	@Override
	public String getWorkingHours(String languageId, boolean useDefault) {
		return model.getWorkingHours(languageId, useDefault);
	}

	@Override
	public String getWorkingHoursCurrentLanguageId() {
		return model.getWorkingHoursCurrentLanguageId();
	}

	@Override
	public String getWorkingHoursCurrentValue() {
		return model.getWorkingHoursCurrentValue();
	}

	/**
	 * Returns the working hours image of this attractions.
	 *
	 * @return the working hours image of this attractions
	 */
	@Override
	public String getWorkingHoursImage() {
		return model.getWorkingHoursImage();
	}

	/**
	 * Returns the localized working hours image of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized working hours image of this attractions
	 */
	@Override
	public String getWorkingHoursImage(java.util.Locale locale) {
		return model.getWorkingHoursImage(locale);
	}

	/**
	 * Returns the localized working hours image of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours image of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getWorkingHoursImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getWorkingHoursImage(locale, useDefault);
	}

	/**
	 * Returns the localized working hours image of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized working hours image of this attractions
	 */
	@Override
	public String getWorkingHoursImage(String languageId) {
		return model.getWorkingHoursImage(languageId);
	}

	/**
	 * Returns the localized working hours image of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours image of this attractions
	 */
	@Override
	public String getWorkingHoursImage(String languageId, boolean useDefault) {
		return model.getWorkingHoursImage(languageId, useDefault);
	}

	@Override
	public String getWorkingHoursImageCurrentLanguageId() {
		return model.getWorkingHoursImageCurrentLanguageId();
	}

	@Override
	public String getWorkingHoursImageCurrentValue() {
		return model.getWorkingHoursImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized working hours images of this attractions.
	 *
	 * @return the locales and localized working hours images of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getWorkingHoursImageMap() {
		return model.getWorkingHoursImageMap();
	}

	/**
	 * Returns the working hours label of this attractions.
	 *
	 * @return the working hours label of this attractions
	 */
	@Override
	public String getWorkingHoursLabel() {
		return model.getWorkingHoursLabel();
	}

	/**
	 * Returns the localized working hours label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized working hours label of this attractions
	 */
	@Override
	public String getWorkingHoursLabel(java.util.Locale locale) {
		return model.getWorkingHoursLabel(locale);
	}

	/**
	 * Returns the localized working hours label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours label of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getWorkingHoursLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getWorkingHoursLabel(locale, useDefault);
	}

	/**
	 * Returns the localized working hours label of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized working hours label of this attractions
	 */
	@Override
	public String getWorkingHoursLabel(String languageId) {
		return model.getWorkingHoursLabel(languageId);
	}

	/**
	 * Returns the localized working hours label of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours label of this attractions
	 */
	@Override
	public String getWorkingHoursLabel(String languageId, boolean useDefault) {
		return model.getWorkingHoursLabel(languageId, useDefault);
	}

	/**
	 * Returns the working hours label color of this attractions.
	 *
	 * @return the working hours label color of this attractions
	 */
	@Override
	public String getWorkingHoursLabelColor() {
		return model.getWorkingHoursLabelColor();
	}

	/**
	 * Returns the localized working hours label color of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized working hours label color of this attractions
	 */
	@Override
	public String getWorkingHoursLabelColor(java.util.Locale locale) {
		return model.getWorkingHoursLabelColor(locale);
	}

	/**
	 * Returns the localized working hours label color of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours label color of this attractions. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getWorkingHoursLabelColor(
		java.util.Locale locale, boolean useDefault) {

		return model.getWorkingHoursLabelColor(locale, useDefault);
	}

	/**
	 * Returns the localized working hours label color of this attractions in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized working hours label color of this attractions
	 */
	@Override
	public String getWorkingHoursLabelColor(String languageId) {
		return model.getWorkingHoursLabelColor(languageId);
	}

	/**
	 * Returns the localized working hours label color of this attractions in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized working hours label color of this attractions
	 */
	@Override
	public String getWorkingHoursLabelColor(
		String languageId, boolean useDefault) {

		return model.getWorkingHoursLabelColor(languageId, useDefault);
	}

	@Override
	public String getWorkingHoursLabelColorCurrentLanguageId() {
		return model.getWorkingHoursLabelColorCurrentLanguageId();
	}

	@Override
	public String getWorkingHoursLabelColorCurrentValue() {
		return model.getWorkingHoursLabelColorCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized working hours label colors of this attractions.
	 *
	 * @return the locales and localized working hours label colors of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getWorkingHoursLabelColorMap() {
		return model.getWorkingHoursLabelColorMap();
	}

	@Override
	public String getWorkingHoursLabelCurrentLanguageId() {
		return model.getWorkingHoursLabelCurrentLanguageId();
	}

	@Override
	public String getWorkingHoursLabelCurrentValue() {
		return model.getWorkingHoursLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized working hours labels of this attractions.
	 *
	 * @return the locales and localized working hours labels of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getWorkingHoursLabelMap() {
		return model.getWorkingHoursLabelMap();
	}

	/**
	 * Returns a map of the locales and localized working hourses of this attractions.
	 *
	 * @return the locales and localized working hourses of this attractions
	 */
	@Override
	public Map<java.util.Locale, String> getWorkingHoursMap() {
		return model.getWorkingHoursMap();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the amenities label of this attractions.
	 *
	 * @param amenitiesLabel the amenities label of this attractions
	 */
	@Override
	public void setAmenitiesLabel(String amenitiesLabel) {
		model.setAmenitiesLabel(amenitiesLabel);
	}

	/**
	 * Sets the localized amenities label of this attractions in the language.
	 *
	 * @param amenitiesLabel the localized amenities label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setAmenitiesLabel(
		String amenitiesLabel, java.util.Locale locale) {

		model.setAmenitiesLabel(amenitiesLabel, locale);
	}

	/**
	 * Sets the localized amenities label of this attractions in the language, and sets the default locale.
	 *
	 * @param amenitiesLabel the localized amenities label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAmenitiesLabel(
		String amenitiesLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setAmenitiesLabel(amenitiesLabel, locale, defaultLocale);
	}

	@Override
	public void setAmenitiesLabelCurrentLanguageId(String languageId) {
		model.setAmenitiesLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized amenities labels of this attractions from the map of locales and localized amenities labels.
	 *
	 * @param amenitiesLabelMap the locales and localized amenities labels of this attractions
	 */
	@Override
	public void setAmenitiesLabelMap(
		Map<java.util.Locale, String> amenitiesLabelMap) {

		model.setAmenitiesLabelMap(amenitiesLabelMap);
	}

	/**
	 * Sets the localized amenities labels of this attractions from the map of locales and localized amenities labels, and sets the default locale.
	 *
	 * @param amenitiesLabelMap the locales and localized amenities labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAmenitiesLabelMap(
		Map<java.util.Locale, String> amenitiesLabelMap,
		java.util.Locale defaultLocale) {

		model.setAmenitiesLabelMap(amenitiesLabelMap, defaultLocale);
	}

	/**
	 * Sets the attractions ID of this attractions.
	 *
	 * @param attractionsId the attractions ID of this attractions
	 */
	@Override
	public void setAttractionsId(long attractionsId) {
		model.setAttractionsId(attractionsId);
	}

	/**
	 * Sets the book space label of this attractions.
	 *
	 * @param bookSpaceLabel the book space label of this attractions
	 */
	@Override
	public void setBookSpaceLabel(String bookSpaceLabel) {
		model.setBookSpaceLabel(bookSpaceLabel);
	}

	/**
	 * Sets the localized book space label of this attractions in the language.
	 *
	 * @param bookSpaceLabel the localized book space label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setBookSpaceLabel(
		String bookSpaceLabel, java.util.Locale locale) {

		model.setBookSpaceLabel(bookSpaceLabel, locale);
	}

	/**
	 * Sets the localized book space label of this attractions in the language, and sets the default locale.
	 *
	 * @param bookSpaceLabel the localized book space label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceLabel(
		String bookSpaceLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBookSpaceLabel(bookSpaceLabel, locale, defaultLocale);
	}

	@Override
	public void setBookSpaceLabelCurrentLanguageId(String languageId) {
		model.setBookSpaceLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized book space labels of this attractions from the map of locales and localized book space labels.
	 *
	 * @param bookSpaceLabelMap the locales and localized book space labels of this attractions
	 */
	@Override
	public void setBookSpaceLabelMap(
		Map<java.util.Locale, String> bookSpaceLabelMap) {

		model.setBookSpaceLabelMap(bookSpaceLabelMap);
	}

	/**
	 * Sets the localized book space labels of this attractions from the map of locales and localized book space labels, and sets the default locale.
	 *
	 * @param bookSpaceLabelMap the locales and localized book space labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBookSpaceLabelMap(
		Map<java.util.Locale, String> bookSpaceLabelMap,
		java.util.Locale defaultLocale) {

		model.setBookSpaceLabelMap(bookSpaceLabelMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this attractions.
	 *
	 * @param companyId the company ID of this attractions
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the contact email address of this attractions.
	 *
	 * @param contactEmailAddress the contact email address of this attractions
	 */
	@Override
	public void setContactEmailAddress(String contactEmailAddress) {
		model.setContactEmailAddress(contactEmailAddress);
	}

	/**
	 * Sets the localized contact email address of this attractions in the language.
	 *
	 * @param contactEmailAddress the localized contact email address of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setContactEmailAddress(
		String contactEmailAddress, java.util.Locale locale) {

		model.setContactEmailAddress(contactEmailAddress, locale);
	}

	/**
	 * Sets the localized contact email address of this attractions in the language, and sets the default locale.
	 *
	 * @param contactEmailAddress the localized contact email address of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactEmailAddress(
		String contactEmailAddress, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setContactEmailAddress(
			contactEmailAddress, locale, defaultLocale);
	}

	@Override
	public void setContactEmailAddressCurrentLanguageId(String languageId) {
		model.setContactEmailAddressCurrentLanguageId(languageId);
	}

	/**
	 * Sets the contact email address icon of this attractions.
	 *
	 * @param contactEmailAddressIcon the contact email address icon of this attractions
	 */
	@Override
	public void setContactEmailAddressIcon(String contactEmailAddressIcon) {
		model.setContactEmailAddressIcon(contactEmailAddressIcon);
	}

	/**
	 * Sets the localized contact email address icon of this attractions in the language.
	 *
	 * @param contactEmailAddressIcon the localized contact email address icon of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setContactEmailAddressIcon(
		String contactEmailAddressIcon, java.util.Locale locale) {

		model.setContactEmailAddressIcon(contactEmailAddressIcon, locale);
	}

	/**
	 * Sets the localized contact email address icon of this attractions in the language, and sets the default locale.
	 *
	 * @param contactEmailAddressIcon the localized contact email address icon of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactEmailAddressIcon(
		String contactEmailAddressIcon, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setContactEmailAddressIcon(
			contactEmailAddressIcon, locale, defaultLocale);
	}

	@Override
	public void setContactEmailAddressIconCurrentLanguageId(String languageId) {
		model.setContactEmailAddressIconCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized contact email address icons of this attractions from the map of locales and localized contact email address icons.
	 *
	 * @param contactEmailAddressIconMap the locales and localized contact email address icons of this attractions
	 */
	@Override
	public void setContactEmailAddressIconMap(
		Map<java.util.Locale, String> contactEmailAddressIconMap) {

		model.setContactEmailAddressIconMap(contactEmailAddressIconMap);
	}

	/**
	 * Sets the localized contact email address icons of this attractions from the map of locales and localized contact email address icons, and sets the default locale.
	 *
	 * @param contactEmailAddressIconMap the locales and localized contact email address icons of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactEmailAddressIconMap(
		Map<java.util.Locale, String> contactEmailAddressIconMap,
		java.util.Locale defaultLocale) {

		model.setContactEmailAddressIconMap(
			contactEmailAddressIconMap, defaultLocale);
	}

	/**
	 * Sets the localized contact email addresses of this attractions from the map of locales and localized contact email addresses.
	 *
	 * @param contactEmailAddressMap the locales and localized contact email addresses of this attractions
	 */
	@Override
	public void setContactEmailAddressMap(
		Map<java.util.Locale, String> contactEmailAddressMap) {

		model.setContactEmailAddressMap(contactEmailAddressMap);
	}

	/**
	 * Sets the localized contact email addresses of this attractions from the map of locales and localized contact email addresses, and sets the default locale.
	 *
	 * @param contactEmailAddressMap the locales and localized contact email addresses of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactEmailAddressMap(
		Map<java.util.Locale, String> contactEmailAddressMap,
		java.util.Locale defaultLocale) {

		model.setContactEmailAddressMap(contactEmailAddressMap, defaultLocale);
	}

	/**
	 * Sets the contact label of this attractions.
	 *
	 * @param contactLabel the contact label of this attractions
	 */
	@Override
	public void setContactLabel(String contactLabel) {
		model.setContactLabel(contactLabel);
	}

	/**
	 * Sets the localized contact label of this attractions in the language.
	 *
	 * @param contactLabel the localized contact label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setContactLabel(String contactLabel, java.util.Locale locale) {
		model.setContactLabel(contactLabel, locale);
	}

	/**
	 * Sets the localized contact label of this attractions in the language, and sets the default locale.
	 *
	 * @param contactLabel the localized contact label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactLabel(
		String contactLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setContactLabel(contactLabel, locale, defaultLocale);
	}

	@Override
	public void setContactLabelCurrentLanguageId(String languageId) {
		model.setContactLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized contact labels of this attractions from the map of locales and localized contact labels.
	 *
	 * @param contactLabelMap the locales and localized contact labels of this attractions
	 */
	@Override
	public void setContactLabelMap(
		Map<java.util.Locale, String> contactLabelMap) {

		model.setContactLabelMap(contactLabelMap);
	}

	/**
	 * Sets the localized contact labels of this attractions from the map of locales and localized contact labels, and sets the default locale.
	 *
	 * @param contactLabelMap the locales and localized contact labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactLabelMap(
		Map<java.util.Locale, String> contactLabelMap,
		java.util.Locale defaultLocale) {

		model.setContactLabelMap(contactLabelMap, defaultLocale);
	}

	/**
	 * Sets the contact telephone of this attractions.
	 *
	 * @param contactTelephone the contact telephone of this attractions
	 */
	@Override
	public void setContactTelephone(String contactTelephone) {
		model.setContactTelephone(contactTelephone);
	}

	/**
	 * Sets the localized contact telephone of this attractions in the language.
	 *
	 * @param contactTelephone the localized contact telephone of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setContactTelephone(
		String contactTelephone, java.util.Locale locale) {

		model.setContactTelephone(contactTelephone, locale);
	}

	/**
	 * Sets the localized contact telephone of this attractions in the language, and sets the default locale.
	 *
	 * @param contactTelephone the localized contact telephone of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactTelephone(
		String contactTelephone, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setContactTelephone(contactTelephone, locale, defaultLocale);
	}

	@Override
	public void setContactTelephoneCurrentLanguageId(String languageId) {
		model.setContactTelephoneCurrentLanguageId(languageId);
	}

	/**
	 * Sets the contact telephone icon of this attractions.
	 *
	 * @param contactTelephoneIcon the contact telephone icon of this attractions
	 */
	@Override
	public void setContactTelephoneIcon(String contactTelephoneIcon) {
		model.setContactTelephoneIcon(contactTelephoneIcon);
	}

	/**
	 * Sets the localized contact telephone icon of this attractions in the language.
	 *
	 * @param contactTelephoneIcon the localized contact telephone icon of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setContactTelephoneIcon(
		String contactTelephoneIcon, java.util.Locale locale) {

		model.setContactTelephoneIcon(contactTelephoneIcon, locale);
	}

	/**
	 * Sets the localized contact telephone icon of this attractions in the language, and sets the default locale.
	 *
	 * @param contactTelephoneIcon the localized contact telephone icon of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactTelephoneIcon(
		String contactTelephoneIcon, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setContactTelephoneIcon(
			contactTelephoneIcon, locale, defaultLocale);
	}

	@Override
	public void setContactTelephoneIconCurrentLanguageId(String languageId) {
		model.setContactTelephoneIconCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized contact telephone icons of this attractions from the map of locales and localized contact telephone icons.
	 *
	 * @param contactTelephoneIconMap the locales and localized contact telephone icons of this attractions
	 */
	@Override
	public void setContactTelephoneIconMap(
		Map<java.util.Locale, String> contactTelephoneIconMap) {

		model.setContactTelephoneIconMap(contactTelephoneIconMap);
	}

	/**
	 * Sets the localized contact telephone icons of this attractions from the map of locales and localized contact telephone icons, and sets the default locale.
	 *
	 * @param contactTelephoneIconMap the locales and localized contact telephone icons of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactTelephoneIconMap(
		Map<java.util.Locale, String> contactTelephoneIconMap,
		java.util.Locale defaultLocale) {

		model.setContactTelephoneIconMap(
			contactTelephoneIconMap, defaultLocale);
	}

	/**
	 * Sets the localized contact telephones of this attractions from the map of locales and localized contact telephones.
	 *
	 * @param contactTelephoneMap the locales and localized contact telephones of this attractions
	 */
	@Override
	public void setContactTelephoneMap(
		Map<java.util.Locale, String> contactTelephoneMap) {

		model.setContactTelephoneMap(contactTelephoneMap);
	}

	/**
	 * Sets the localized contact telephones of this attractions from the map of locales and localized contact telephones, and sets the default locale.
	 *
	 * @param contactTelephoneMap the locales and localized contact telephones of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setContactTelephoneMap(
		Map<java.util.Locale, String> contactTelephoneMap,
		java.util.Locale defaultLocale) {

		model.setContactTelephoneMap(contactTelephoneMap, defaultLocale);
	}

	/**
	 * Sets the create date of this attractions.
	 *
	 * @param createDate the create date of this attractions
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this attractions.
	 *
	 * @param description the description of this attractions
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this attractions in the language.
	 *
	 * @param description the localized description of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this attractions in the language, and sets the default locale.
	 *
	 * @param description the localized description of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this attractions from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this attractions
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this attractions from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the event ID of this attractions.
	 *
	 * @param eventId the event ID of this attractions
	 */
	@Override
	public void setEventId(String eventId) {
		model.setEventId(eventId);
	}

	/**
	 * Sets the forum banner icon of this attractions.
	 *
	 * @param forumBannerIcon the forum banner icon of this attractions
	 */
	@Override
	public void setForumBannerIcon(String forumBannerIcon) {
		model.setForumBannerIcon(forumBannerIcon);
	}

	/**
	 * Sets the localized forum banner icon of this attractions in the language.
	 *
	 * @param forumBannerIcon the localized forum banner icon of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setForumBannerIcon(
		String forumBannerIcon, java.util.Locale locale) {

		model.setForumBannerIcon(forumBannerIcon, locale);
	}

	/**
	 * Sets the localized forum banner icon of this attractions in the language, and sets the default locale.
	 *
	 * @param forumBannerIcon the localized forum banner icon of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumBannerIcon(
		String forumBannerIcon, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setForumBannerIcon(forumBannerIcon, locale, defaultLocale);
	}

	@Override
	public void setForumBannerIconCurrentLanguageId(String languageId) {
		model.setForumBannerIconCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized forum banner icons of this attractions from the map of locales and localized forum banner icons.
	 *
	 * @param forumBannerIconMap the locales and localized forum banner icons of this attractions
	 */
	@Override
	public void setForumBannerIconMap(
		Map<java.util.Locale, String> forumBannerIconMap) {

		model.setForumBannerIconMap(forumBannerIconMap);
	}

	/**
	 * Sets the localized forum banner icons of this attractions from the map of locales and localized forum banner icons, and sets the default locale.
	 *
	 * @param forumBannerIconMap the locales and localized forum banner icons of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumBannerIconMap(
		Map<java.util.Locale, String> forumBannerIconMap,
		java.util.Locale defaultLocale) {

		model.setForumBannerIconMap(forumBannerIconMap, defaultLocale);
	}

	/**
	 * Sets the forum banner image of this attractions.
	 *
	 * @param forumBannerImage the forum banner image of this attractions
	 */
	@Override
	public void setForumBannerImage(String forumBannerImage) {
		model.setForumBannerImage(forumBannerImage);
	}

	/**
	 * Sets the localized forum banner image of this attractions in the language.
	 *
	 * @param forumBannerImage the localized forum banner image of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setForumBannerImage(
		String forumBannerImage, java.util.Locale locale) {

		model.setForumBannerImage(forumBannerImage, locale);
	}

	/**
	 * Sets the localized forum banner image of this attractions in the language, and sets the default locale.
	 *
	 * @param forumBannerImage the localized forum banner image of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumBannerImage(
		String forumBannerImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setForumBannerImage(forumBannerImage, locale, defaultLocale);
	}

	@Override
	public void setForumBannerImageCurrentLanguageId(String languageId) {
		model.setForumBannerImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized forum banner images of this attractions from the map of locales and localized forum banner images.
	 *
	 * @param forumBannerImageMap the locales and localized forum banner images of this attractions
	 */
	@Override
	public void setForumBannerImageMap(
		Map<java.util.Locale, String> forumBannerImageMap) {

		model.setForumBannerImageMap(forumBannerImageMap);
	}

	/**
	 * Sets the localized forum banner images of this attractions from the map of locales and localized forum banner images, and sets the default locale.
	 *
	 * @param forumBannerImageMap the locales and localized forum banner images of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumBannerImageMap(
		Map<java.util.Locale, String> forumBannerImageMap,
		java.util.Locale defaultLocale) {

		model.setForumBannerImageMap(forumBannerImageMap, defaultLocale);
	}

	/**
	 * Sets the forum banner title of this attractions.
	 *
	 * @param forumBannerTitle the forum banner title of this attractions
	 */
	@Override
	public void setForumBannerTitle(String forumBannerTitle) {
		model.setForumBannerTitle(forumBannerTitle);
	}

	/**
	 * Sets the localized forum banner title of this attractions in the language.
	 *
	 * @param forumBannerTitle the localized forum banner title of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setForumBannerTitle(
		String forumBannerTitle, java.util.Locale locale) {

		model.setForumBannerTitle(forumBannerTitle, locale);
	}

	/**
	 * Sets the localized forum banner title of this attractions in the language, and sets the default locale.
	 *
	 * @param forumBannerTitle the localized forum banner title of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumBannerTitle(
		String forumBannerTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setForumBannerTitle(forumBannerTitle, locale, defaultLocale);
	}

	@Override
	public void setForumBannerTitleCurrentLanguageId(String languageId) {
		model.setForumBannerTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized forum banner titles of this attractions from the map of locales and localized forum banner titles.
	 *
	 * @param forumBannerTitleMap the locales and localized forum banner titles of this attractions
	 */
	@Override
	public void setForumBannerTitleMap(
		Map<java.util.Locale, String> forumBannerTitleMap) {

		model.setForumBannerTitleMap(forumBannerTitleMap);
	}

	/**
	 * Sets the localized forum banner titles of this attractions from the map of locales and localized forum banner titles, and sets the default locale.
	 *
	 * @param forumBannerTitleMap the locales and localized forum banner titles of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumBannerTitleMap(
		Map<java.util.Locale, String> forumBannerTitleMap,
		java.util.Locale defaultLocale) {

		model.setForumBannerTitleMap(forumBannerTitleMap, defaultLocale);
	}

	/**
	 * Sets the forum button color of this attractions.
	 *
	 * @param forumButtonColor the forum button color of this attractions
	 */
	@Override
	public void setForumButtonColor(String forumButtonColor) {
		model.setForumButtonColor(forumButtonColor);
	}

	/**
	 * Sets the localized forum button color of this attractions in the language.
	 *
	 * @param forumButtonColor the localized forum button color of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setForumButtonColor(
		String forumButtonColor, java.util.Locale locale) {

		model.setForumButtonColor(forumButtonColor, locale);
	}

	/**
	 * Sets the localized forum button color of this attractions in the language, and sets the default locale.
	 *
	 * @param forumButtonColor the localized forum button color of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumButtonColor(
		String forumButtonColor, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setForumButtonColor(forumButtonColor, locale, defaultLocale);
	}

	@Override
	public void setForumButtonColorCurrentLanguageId(String languageId) {
		model.setForumButtonColorCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized forum button colors of this attractions from the map of locales and localized forum button colors.
	 *
	 * @param forumButtonColorMap the locales and localized forum button colors of this attractions
	 */
	@Override
	public void setForumButtonColorMap(
		Map<java.util.Locale, String> forumButtonColorMap) {

		model.setForumButtonColorMap(forumButtonColorMap);
	}

	/**
	 * Sets the localized forum button colors of this attractions from the map of locales and localized forum button colors, and sets the default locale.
	 *
	 * @param forumButtonColorMap the locales and localized forum button colors of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumButtonColorMap(
		Map<java.util.Locale, String> forumButtonColorMap,
		java.util.Locale defaultLocale) {

		model.setForumButtonColorMap(forumButtonColorMap, defaultLocale);
	}

	/**
	 * Sets the forum button label of this attractions.
	 *
	 * @param forumButtonLabel the forum button label of this attractions
	 */
	@Override
	public void setForumButtonLabel(String forumButtonLabel) {
		model.setForumButtonLabel(forumButtonLabel);
	}

	/**
	 * Sets the localized forum button label of this attractions in the language.
	 *
	 * @param forumButtonLabel the localized forum button label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setForumButtonLabel(
		String forumButtonLabel, java.util.Locale locale) {

		model.setForumButtonLabel(forumButtonLabel, locale);
	}

	/**
	 * Sets the localized forum button label of this attractions in the language, and sets the default locale.
	 *
	 * @param forumButtonLabel the localized forum button label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumButtonLabel(
		String forumButtonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setForumButtonLabel(forumButtonLabel, locale, defaultLocale);
	}

	@Override
	public void setForumButtonLabelCurrentLanguageId(String languageId) {
		model.setForumButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized forum button labels of this attractions from the map of locales and localized forum button labels.
	 *
	 * @param forumButtonLabelMap the locales and localized forum button labels of this attractions
	 */
	@Override
	public void setForumButtonLabelMap(
		Map<java.util.Locale, String> forumButtonLabelMap) {

		model.setForumButtonLabelMap(forumButtonLabelMap);
	}

	/**
	 * Sets the localized forum button labels of this attractions from the map of locales and localized forum button labels, and sets the default locale.
	 *
	 * @param forumButtonLabelMap the locales and localized forum button labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setForumButtonLabelMap(
		Map<java.util.Locale, String> forumButtonLabelMap,
		java.util.Locale defaultLocale) {

		model.setForumButtonLabelMap(forumButtonLabelMap, defaultLocale);
	}

	/**
	 * Sets the gallary label of this attractions.
	 *
	 * @param gallaryLabel the gallary label of this attractions
	 */
	@Override
	public void setGallaryLabel(String gallaryLabel) {
		model.setGallaryLabel(gallaryLabel);
	}

	/**
	 * Sets the localized gallary label of this attractions in the language.
	 *
	 * @param gallaryLabel the localized gallary label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setGallaryLabel(String gallaryLabel, java.util.Locale locale) {
		model.setGallaryLabel(gallaryLabel, locale);
	}

	/**
	 * Sets the localized gallary label of this attractions in the language, and sets the default locale.
	 *
	 * @param gallaryLabel the localized gallary label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setGallaryLabel(
		String gallaryLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setGallaryLabel(gallaryLabel, locale, defaultLocale);
	}

	@Override
	public void setGallaryLabelCurrentLanguageId(String languageId) {
		model.setGallaryLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized gallary labels of this attractions from the map of locales and localized gallary labels.
	 *
	 * @param gallaryLabelMap the locales and localized gallary labels of this attractions
	 */
	@Override
	public void setGallaryLabelMap(
		Map<java.util.Locale, String> gallaryLabelMap) {

		model.setGallaryLabelMap(gallaryLabelMap);
	}

	/**
	 * Sets the localized gallary labels of this attractions from the map of locales and localized gallary labels, and sets the default locale.
	 *
	 * @param gallaryLabelMap the locales and localized gallary labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setGallaryLabelMap(
		Map<java.util.Locale, String> gallaryLabelMap,
		java.util.Locale defaultLocale) {

		model.setGallaryLabelMap(gallaryLabelMap, defaultLocale);
	}

	/**
	 * Sets the group ID of this attractions.
	 *
	 * @param groupId the group ID of this attractions
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the location label of this attractions.
	 *
	 * @param locationLabel the location label of this attractions
	 */
	@Override
	public void setLocationLabel(String locationLabel) {
		model.setLocationLabel(locationLabel);
	}

	/**
	 * Sets the localized location label of this attractions in the language.
	 *
	 * @param locationLabel the localized location label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setLocationLabel(
		String locationLabel, java.util.Locale locale) {

		model.setLocationLabel(locationLabel, locale);
	}

	/**
	 * Sets the localized location label of this attractions in the language, and sets the default locale.
	 *
	 * @param locationLabel the localized location label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLabel(
		String locationLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLocationLabel(locationLabel, locale, defaultLocale);
	}

	@Override
	public void setLocationLabelCurrentLanguageId(String languageId) {
		model.setLocationLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized location labels of this attractions from the map of locales and localized location labels.
	 *
	 * @param locationLabelMap the locales and localized location labels of this attractions
	 */
	@Override
	public void setLocationLabelMap(
		Map<java.util.Locale, String> locationLabelMap) {

		model.setLocationLabelMap(locationLabelMap);
	}

	/**
	 * Sets the localized location labels of this attractions from the map of locales and localized location labels, and sets the default locale.
	 *
	 * @param locationLabelMap the locales and localized location labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLabelMap(
		Map<java.util.Locale, String> locationLabelMap,
		java.util.Locale defaultLocale) {

		model.setLocationLabelMap(locationLabelMap, defaultLocale);
	}

	/**
	 * Sets the location latitude of this attractions.
	 *
	 * @param locationLatitude the location latitude of this attractions
	 */
	@Override
	public void setLocationLatitude(String locationLatitude) {
		model.setLocationLatitude(locationLatitude);
	}

	/**
	 * Sets the localized location latitude of this attractions in the language.
	 *
	 * @param locationLatitude the localized location latitude of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setLocationLatitude(
		String locationLatitude, java.util.Locale locale) {

		model.setLocationLatitude(locationLatitude, locale);
	}

	/**
	 * Sets the localized location latitude of this attractions in the language, and sets the default locale.
	 *
	 * @param locationLatitude the localized location latitude of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLatitude(
		String locationLatitude, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLocationLatitude(locationLatitude, locale, defaultLocale);
	}

	@Override
	public void setLocationLatitudeCurrentLanguageId(String languageId) {
		model.setLocationLatitudeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized location latitudes of this attractions from the map of locales and localized location latitudes.
	 *
	 * @param locationLatitudeMap the locales and localized location latitudes of this attractions
	 */
	@Override
	public void setLocationLatitudeMap(
		Map<java.util.Locale, String> locationLatitudeMap) {

		model.setLocationLatitudeMap(locationLatitudeMap);
	}

	/**
	 * Sets the localized location latitudes of this attractions from the map of locales and localized location latitudes, and sets the default locale.
	 *
	 * @param locationLatitudeMap the locales and localized location latitudes of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLatitudeMap(
		Map<java.util.Locale, String> locationLatitudeMap,
		java.util.Locale defaultLocale) {

		model.setLocationLatitudeMap(locationLatitudeMap, defaultLocale);
	}

	/**
	 * Sets the location longitude of this attractions.
	 *
	 * @param locationLongitude the location longitude of this attractions
	 */
	@Override
	public void setLocationLongitude(String locationLongitude) {
		model.setLocationLongitude(locationLongitude);
	}

	/**
	 * Sets the localized location longitude of this attractions in the language.
	 *
	 * @param locationLongitude the localized location longitude of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setLocationLongitude(
		String locationLongitude, java.util.Locale locale) {

		model.setLocationLongitude(locationLongitude, locale);
	}

	/**
	 * Sets the localized location longitude of this attractions in the language, and sets the default locale.
	 *
	 * @param locationLongitude the localized location longitude of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLongitude(
		String locationLongitude, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLocationLongitude(locationLongitude, locale, defaultLocale);
	}

	@Override
	public void setLocationLongitudeCurrentLanguageId(String languageId) {
		model.setLocationLongitudeCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized location longitudes of this attractions from the map of locales and localized location longitudes.
	 *
	 * @param locationLongitudeMap the locales and localized location longitudes of this attractions
	 */
	@Override
	public void setLocationLongitudeMap(
		Map<java.util.Locale, String> locationLongitudeMap) {

		model.setLocationLongitudeMap(locationLongitudeMap);
	}

	/**
	 * Sets the localized location longitudes of this attractions from the map of locales and localized location longitudes, and sets the default locale.
	 *
	 * @param locationLongitudeMap the locales and localized location longitudes of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationLongitudeMap(
		Map<java.util.Locale, String> locationLongitudeMap,
		java.util.Locale defaultLocale) {

		model.setLocationLongitudeMap(locationLongitudeMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this attractions.
	 *
	 * @param modifiedDate the modified date of this attractions
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this attractions.
	 *
	 * @param primaryKey the primary key of this attractions
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this attractions.
	 *
	 * @param userId the user ID of this attractions
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this attractions.
	 *
	 * @param userName the user name of this attractions
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this attractions.
	 *
	 * @param userUuid the user uuid of this attractions
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this attractions.
	 *
	 * @param uuid the uuid of this attractions
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the working days of this attractions.
	 *
	 * @param workingDays the working days of this attractions
	 */
	@Override
	public void setWorkingDays(String workingDays) {
		model.setWorkingDays(workingDays);
	}

	/**
	 * Sets the localized working days of this attractions in the language.
	 *
	 * @param workingDays the localized working days of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setWorkingDays(String workingDays, java.util.Locale locale) {
		model.setWorkingDays(workingDays, locale);
	}

	/**
	 * Sets the localized working days of this attractions in the language, and sets the default locale.
	 *
	 * @param workingDays the localized working days of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingDays(
		String workingDays, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setWorkingDays(workingDays, locale, defaultLocale);
	}

	@Override
	public void setWorkingDaysCurrentLanguageId(String languageId) {
		model.setWorkingDaysCurrentLanguageId(languageId);
	}

	/**
	 * Sets the working days image of this attractions.
	 *
	 * @param workingDaysImage the working days image of this attractions
	 */
	@Override
	public void setWorkingDaysImage(String workingDaysImage) {
		model.setWorkingDaysImage(workingDaysImage);
	}

	/**
	 * Sets the localized working days image of this attractions in the language.
	 *
	 * @param workingDaysImage the localized working days image of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setWorkingDaysImage(
		String workingDaysImage, java.util.Locale locale) {

		model.setWorkingDaysImage(workingDaysImage, locale);
	}

	/**
	 * Sets the localized working days image of this attractions in the language, and sets the default locale.
	 *
	 * @param workingDaysImage the localized working days image of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingDaysImage(
		String workingDaysImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setWorkingDaysImage(workingDaysImage, locale, defaultLocale);
	}

	@Override
	public void setWorkingDaysImageCurrentLanguageId(String languageId) {
		model.setWorkingDaysImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized working days images of this attractions from the map of locales and localized working days images.
	 *
	 * @param workingDaysImageMap the locales and localized working days images of this attractions
	 */
	@Override
	public void setWorkingDaysImageMap(
		Map<java.util.Locale, String> workingDaysImageMap) {

		model.setWorkingDaysImageMap(workingDaysImageMap);
	}

	/**
	 * Sets the localized working days images of this attractions from the map of locales and localized working days images, and sets the default locale.
	 *
	 * @param workingDaysImageMap the locales and localized working days images of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingDaysImageMap(
		Map<java.util.Locale, String> workingDaysImageMap,
		java.util.Locale defaultLocale) {

		model.setWorkingDaysImageMap(workingDaysImageMap, defaultLocale);
	}

	/**
	 * Sets the localized working dayses of this attractions from the map of locales and localized working dayses.
	 *
	 * @param workingDaysMap the locales and localized working dayses of this attractions
	 */
	@Override
	public void setWorkingDaysMap(
		Map<java.util.Locale, String> workingDaysMap) {

		model.setWorkingDaysMap(workingDaysMap);
	}

	/**
	 * Sets the localized working dayses of this attractions from the map of locales and localized working dayses, and sets the default locale.
	 *
	 * @param workingDaysMap the locales and localized working dayses of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingDaysMap(
		Map<java.util.Locale, String> workingDaysMap,
		java.util.Locale defaultLocale) {

		model.setWorkingDaysMap(workingDaysMap, defaultLocale);
	}

	/**
	 * Sets the working hours of this attractions.
	 *
	 * @param workingHours the working hours of this attractions
	 */
	@Override
	public void setWorkingHours(String workingHours) {
		model.setWorkingHours(workingHours);
	}

	/**
	 * Sets the localized working hours of this attractions in the language.
	 *
	 * @param workingHours the localized working hours of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setWorkingHours(String workingHours, java.util.Locale locale) {
		model.setWorkingHours(workingHours, locale);
	}

	/**
	 * Sets the localized working hours of this attractions in the language, and sets the default locale.
	 *
	 * @param workingHours the localized working hours of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHours(
		String workingHours, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setWorkingHours(workingHours, locale, defaultLocale);
	}

	@Override
	public void setWorkingHoursCurrentLanguageId(String languageId) {
		model.setWorkingHoursCurrentLanguageId(languageId);
	}

	/**
	 * Sets the working hours image of this attractions.
	 *
	 * @param workingHoursImage the working hours image of this attractions
	 */
	@Override
	public void setWorkingHoursImage(String workingHoursImage) {
		model.setWorkingHoursImage(workingHoursImage);
	}

	/**
	 * Sets the localized working hours image of this attractions in the language.
	 *
	 * @param workingHoursImage the localized working hours image of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setWorkingHoursImage(
		String workingHoursImage, java.util.Locale locale) {

		model.setWorkingHoursImage(workingHoursImage, locale);
	}

	/**
	 * Sets the localized working hours image of this attractions in the language, and sets the default locale.
	 *
	 * @param workingHoursImage the localized working hours image of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHoursImage(
		String workingHoursImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setWorkingHoursImage(workingHoursImage, locale, defaultLocale);
	}

	@Override
	public void setWorkingHoursImageCurrentLanguageId(String languageId) {
		model.setWorkingHoursImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized working hours images of this attractions from the map of locales and localized working hours images.
	 *
	 * @param workingHoursImageMap the locales and localized working hours images of this attractions
	 */
	@Override
	public void setWorkingHoursImageMap(
		Map<java.util.Locale, String> workingHoursImageMap) {

		model.setWorkingHoursImageMap(workingHoursImageMap);
	}

	/**
	 * Sets the localized working hours images of this attractions from the map of locales and localized working hours images, and sets the default locale.
	 *
	 * @param workingHoursImageMap the locales and localized working hours images of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHoursImageMap(
		Map<java.util.Locale, String> workingHoursImageMap,
		java.util.Locale defaultLocale) {

		model.setWorkingHoursImageMap(workingHoursImageMap, defaultLocale);
	}

	/**
	 * Sets the working hours label of this attractions.
	 *
	 * @param workingHoursLabel the working hours label of this attractions
	 */
	@Override
	public void setWorkingHoursLabel(String workingHoursLabel) {
		model.setWorkingHoursLabel(workingHoursLabel);
	}

	/**
	 * Sets the localized working hours label of this attractions in the language.
	 *
	 * @param workingHoursLabel the localized working hours label of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setWorkingHoursLabel(
		String workingHoursLabel, java.util.Locale locale) {

		model.setWorkingHoursLabel(workingHoursLabel, locale);
	}

	/**
	 * Sets the localized working hours label of this attractions in the language, and sets the default locale.
	 *
	 * @param workingHoursLabel the localized working hours label of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHoursLabel(
		String workingHoursLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setWorkingHoursLabel(workingHoursLabel, locale, defaultLocale);
	}

	/**
	 * Sets the working hours label color of this attractions.
	 *
	 * @param workingHoursLabelColor the working hours label color of this attractions
	 */
	@Override
	public void setWorkingHoursLabelColor(String workingHoursLabelColor) {
		model.setWorkingHoursLabelColor(workingHoursLabelColor);
	}

	/**
	 * Sets the localized working hours label color of this attractions in the language.
	 *
	 * @param workingHoursLabelColor the localized working hours label color of this attractions
	 * @param locale the locale of the language
	 */
	@Override
	public void setWorkingHoursLabelColor(
		String workingHoursLabelColor, java.util.Locale locale) {

		model.setWorkingHoursLabelColor(workingHoursLabelColor, locale);
	}

	/**
	 * Sets the localized working hours label color of this attractions in the language, and sets the default locale.
	 *
	 * @param workingHoursLabelColor the localized working hours label color of this attractions
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHoursLabelColor(
		String workingHoursLabelColor, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setWorkingHoursLabelColor(
			workingHoursLabelColor, locale, defaultLocale);
	}

	@Override
	public void setWorkingHoursLabelColorCurrentLanguageId(String languageId) {
		model.setWorkingHoursLabelColorCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized working hours label colors of this attractions from the map of locales and localized working hours label colors.
	 *
	 * @param workingHoursLabelColorMap the locales and localized working hours label colors of this attractions
	 */
	@Override
	public void setWorkingHoursLabelColorMap(
		Map<java.util.Locale, String> workingHoursLabelColorMap) {

		model.setWorkingHoursLabelColorMap(workingHoursLabelColorMap);
	}

	/**
	 * Sets the localized working hours label colors of this attractions from the map of locales and localized working hours label colors, and sets the default locale.
	 *
	 * @param workingHoursLabelColorMap the locales and localized working hours label colors of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHoursLabelColorMap(
		Map<java.util.Locale, String> workingHoursLabelColorMap,
		java.util.Locale defaultLocale) {

		model.setWorkingHoursLabelColorMap(
			workingHoursLabelColorMap, defaultLocale);
	}

	@Override
	public void setWorkingHoursLabelCurrentLanguageId(String languageId) {
		model.setWorkingHoursLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized working hours labels of this attractions from the map of locales and localized working hours labels.
	 *
	 * @param workingHoursLabelMap the locales and localized working hours labels of this attractions
	 */
	@Override
	public void setWorkingHoursLabelMap(
		Map<java.util.Locale, String> workingHoursLabelMap) {

		model.setWorkingHoursLabelMap(workingHoursLabelMap);
	}

	/**
	 * Sets the localized working hours labels of this attractions from the map of locales and localized working hours labels, and sets the default locale.
	 *
	 * @param workingHoursLabelMap the locales and localized working hours labels of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHoursLabelMap(
		Map<java.util.Locale, String> workingHoursLabelMap,
		java.util.Locale defaultLocale) {

		model.setWorkingHoursLabelMap(workingHoursLabelMap, defaultLocale);
	}

	/**
	 * Sets the localized working hourses of this attractions from the map of locales and localized working hourses.
	 *
	 * @param workingHoursMap the locales and localized working hourses of this attractions
	 */
	@Override
	public void setWorkingHoursMap(
		Map<java.util.Locale, String> workingHoursMap) {

		model.setWorkingHoursMap(workingHoursMap);
	}

	/**
	 * Sets the localized working hourses of this attractions from the map of locales and localized working hourses, and sets the default locale.
	 *
	 * @param workingHoursMap the locales and localized working hourses of this attractions
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWorkingHoursMap(
		Map<java.util.Locale, String> workingHoursMap,
		java.util.Locale defaultLocale) {

		model.setWorkingHoursMap(workingHoursMap, defaultLocale);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected AttractionsWrapper wrap(Attractions attractions) {
		return new AttractionsWrapper(attractions);
	}

}