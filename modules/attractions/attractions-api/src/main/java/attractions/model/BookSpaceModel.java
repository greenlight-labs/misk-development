/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package attractions.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.LocaleException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.LocalizedModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.model.StagedAuditedModel;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the BookSpace service. Represents a row in the &quot;misk_attractions_bookspace&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>attractions.model.impl.BookSpaceModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>attractions.model.impl.BookSpaceImpl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookSpace
 * @generated
 */
@ProviderType
public interface BookSpaceModel
	extends BaseModel<BookSpace>, LocalizedModel, ShardedModel,
			StagedAuditedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a book space model instance should use the {@link BookSpace} interface instead.
	 */

	/**
	 * Returns the primary key of this book space.
	 *
	 * @return the primary key of this book space
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this book space.
	 *
	 * @param primaryKey the primary key of this book space
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this book space.
	 *
	 * @return the uuid of this book space
	 */
	@AutoEscape
	@Override
	public String getUuid();

	/**
	 * Sets the uuid of this book space.
	 *
	 * @param uuid the uuid of this book space
	 */
	@Override
	public void setUuid(String uuid);

	/**
	 * Returns the bookspace ID of this book space.
	 *
	 * @return the bookspace ID of this book space
	 */
	public long getBookspaceId();

	/**
	 * Sets the bookspace ID of this book space.
	 *
	 * @param bookspaceId the bookspace ID of this book space
	 */
	public void setBookspaceId(long bookspaceId);

	/**
	 * Returns the company ID of this book space.
	 *
	 * @return the company ID of this book space
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this book space.
	 *
	 * @param companyId the company ID of this book space
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this book space.
	 *
	 * @return the user ID of this book space
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this book space.
	 *
	 * @param userId the user ID of this book space
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this book space.
	 *
	 * @return the user uuid of this book space
	 */
	@Override
	public String getUserUuid();

	/**
	 * Sets the user uuid of this book space.
	 *
	 * @param userUuid the user uuid of this book space
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this book space.
	 *
	 * @return the user name of this book space
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this book space.
	 *
	 * @param userName the user name of this book space
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this book space.
	 *
	 * @return the create date of this book space
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this book space.
	 *
	 * @param createDate the create date of this book space
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this book space.
	 *
	 * @return the modified date of this book space
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this book space.
	 *
	 * @param modifiedDate the modified date of this book space
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the attractions ID of this book space.
	 *
	 * @return the attractions ID of this book space
	 */
	public long getAttractionsId();

	/**
	 * Sets the attractions ID of this book space.
	 *
	 * @param attractionsId the attractions ID of this book space
	 */
	public void setAttractionsId(long attractionsId);

	/**
	 * Returns the book space image of this book space.
	 *
	 * @return the book space image of this book space
	 */
	public String getBookSpaceImage();

	/**
	 * Returns the localized book space image of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized book space image of this book space
	 */
	@AutoEscape
	public String getBookSpaceImage(Locale locale);

	/**
	 * Returns the localized book space image of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space image of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getBookSpaceImage(Locale locale, boolean useDefault);

	/**
	 * Returns the localized book space image of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized book space image of this book space
	 */
	@AutoEscape
	public String getBookSpaceImage(String languageId);

	/**
	 * Returns the localized book space image of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space image of this book space
	 */
	@AutoEscape
	public String getBookSpaceImage(String languageId, boolean useDefault);

	@AutoEscape
	public String getBookSpaceImageCurrentLanguageId();

	@AutoEscape
	public String getBookSpaceImageCurrentValue();

	/**
	 * Returns a map of the locales and localized book space images of this book space.
	 *
	 * @return the locales and localized book space images of this book space
	 */
	public Map<Locale, String> getBookSpaceImageMap();

	/**
	 * Sets the book space image of this book space.
	 *
	 * @param bookSpaceImage the book space image of this book space
	 */
	public void setBookSpaceImage(String bookSpaceImage);

	/**
	 * Sets the localized book space image of this book space in the language.
	 *
	 * @param bookSpaceImage the localized book space image of this book space
	 * @param locale the locale of the language
	 */
	public void setBookSpaceImage(String bookSpaceImage, Locale locale);

	/**
	 * Sets the localized book space image of this book space in the language, and sets the default locale.
	 *
	 * @param bookSpaceImage the localized book space image of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setBookSpaceImage(
		String bookSpaceImage, Locale locale, Locale defaultLocale);

	public void setBookSpaceImageCurrentLanguageId(String languageId);

	/**
	 * Sets the localized book space images of this book space from the map of locales and localized book space images.
	 *
	 * @param bookSpaceImageMap the locales and localized book space images of this book space
	 */
	public void setBookSpaceImageMap(Map<Locale, String> bookSpaceImageMap);

	/**
	 * Sets the localized book space images of this book space from the map of locales and localized book space images, and sets the default locale.
	 *
	 * @param bookSpaceImageMap the locales and localized book space images of this book space
	 * @param defaultLocale the default locale
	 */
	public void setBookSpaceImageMap(
		Map<Locale, String> bookSpaceImageMap, Locale defaultLocale);

	/**
	 * Returns the book space title of this book space.
	 *
	 * @return the book space title of this book space
	 */
	public String getBookSpaceTitle();

	/**
	 * Returns the localized book space title of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized book space title of this book space
	 */
	@AutoEscape
	public String getBookSpaceTitle(Locale locale);

	/**
	 * Returns the localized book space title of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space title of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getBookSpaceTitle(Locale locale, boolean useDefault);

	/**
	 * Returns the localized book space title of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized book space title of this book space
	 */
	@AutoEscape
	public String getBookSpaceTitle(String languageId);

	/**
	 * Returns the localized book space title of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized book space title of this book space
	 */
	@AutoEscape
	public String getBookSpaceTitle(String languageId, boolean useDefault);

	@AutoEscape
	public String getBookSpaceTitleCurrentLanguageId();

	@AutoEscape
	public String getBookSpaceTitleCurrentValue();

	/**
	 * Returns a map of the locales and localized book space titles of this book space.
	 *
	 * @return the locales and localized book space titles of this book space
	 */
	public Map<Locale, String> getBookSpaceTitleMap();

	/**
	 * Sets the book space title of this book space.
	 *
	 * @param bookSpaceTitle the book space title of this book space
	 */
	public void setBookSpaceTitle(String bookSpaceTitle);

	/**
	 * Sets the localized book space title of this book space in the language.
	 *
	 * @param bookSpaceTitle the localized book space title of this book space
	 * @param locale the locale of the language
	 */
	public void setBookSpaceTitle(String bookSpaceTitle, Locale locale);

	/**
	 * Sets the localized book space title of this book space in the language, and sets the default locale.
	 *
	 * @param bookSpaceTitle the localized book space title of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setBookSpaceTitle(
		String bookSpaceTitle, Locale locale, Locale defaultLocale);

	public void setBookSpaceTitleCurrentLanguageId(String languageId);

	/**
	 * Sets the localized book space titles of this book space from the map of locales and localized book space titles.
	 *
	 * @param bookSpaceTitleMap the locales and localized book space titles of this book space
	 */
	public void setBookSpaceTitleMap(Map<Locale, String> bookSpaceTitleMap);

	/**
	 * Sets the localized book space titles of this book space from the map of locales and localized book space titles, and sets the default locale.
	 *
	 * @param bookSpaceTitleMap the locales and localized book space titles of this book space
	 * @param defaultLocale the default locale
	 */
	public void setBookSpaceTitleMap(
		Map<Locale, String> bookSpaceTitleMap, Locale defaultLocale);

	/**
	 * Returns the icon of this book space.
	 *
	 * @return the icon of this book space
	 */
	public String getIcon();

	/**
	 * Returns the localized icon of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized icon of this book space
	 */
	@AutoEscape
	public String getIcon(Locale locale);

	/**
	 * Returns the localized icon of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized icon of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getIcon(Locale locale, boolean useDefault);

	/**
	 * Returns the localized icon of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized icon of this book space
	 */
	@AutoEscape
	public String getIcon(String languageId);

	/**
	 * Returns the localized icon of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized icon of this book space
	 */
	@AutoEscape
	public String getIcon(String languageId, boolean useDefault);

	@AutoEscape
	public String getIconCurrentLanguageId();

	@AutoEscape
	public String getIconCurrentValue();

	/**
	 * Returns a map of the locales and localized icons of this book space.
	 *
	 * @return the locales and localized icons of this book space
	 */
	public Map<Locale, String> getIconMap();

	/**
	 * Sets the icon of this book space.
	 *
	 * @param icon the icon of this book space
	 */
	public void setIcon(String icon);

	/**
	 * Sets the localized icon of this book space in the language.
	 *
	 * @param icon the localized icon of this book space
	 * @param locale the locale of the language
	 */
	public void setIcon(String icon, Locale locale);

	/**
	 * Sets the localized icon of this book space in the language, and sets the default locale.
	 *
	 * @param icon the localized icon of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setIcon(String icon, Locale locale, Locale defaultLocale);

	public void setIconCurrentLanguageId(String languageId);

	/**
	 * Sets the localized icons of this book space from the map of locales and localized icons.
	 *
	 * @param iconMap the locales and localized icons of this book space
	 */
	public void setIconMap(Map<Locale, String> iconMap);

	/**
	 * Sets the localized icons of this book space from the map of locales and localized icons, and sets the default locale.
	 *
	 * @param iconMap the locales and localized icons of this book space
	 * @param defaultLocale the default locale
	 */
	public void setIconMap(Map<Locale, String> iconMap, Locale defaultLocale);

	/**
	 * Returns the capacity of this book space.
	 *
	 * @return the capacity of this book space
	 */
	public String getCapacity();

	/**
	 * Returns the localized capacity of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized capacity of this book space
	 */
	@AutoEscape
	public String getCapacity(Locale locale);

	/**
	 * Returns the localized capacity of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized capacity of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getCapacity(Locale locale, boolean useDefault);

	/**
	 * Returns the localized capacity of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized capacity of this book space
	 */
	@AutoEscape
	public String getCapacity(String languageId);

	/**
	 * Returns the localized capacity of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized capacity of this book space
	 */
	@AutoEscape
	public String getCapacity(String languageId, boolean useDefault);

	@AutoEscape
	public String getCapacityCurrentLanguageId();

	@AutoEscape
	public String getCapacityCurrentValue();

	/**
	 * Returns a map of the locales and localized capacities of this book space.
	 *
	 * @return the locales and localized capacities of this book space
	 */
	public Map<Locale, String> getCapacityMap();

	/**
	 * Sets the capacity of this book space.
	 *
	 * @param capacity the capacity of this book space
	 */
	public void setCapacity(String capacity);

	/**
	 * Sets the localized capacity of this book space in the language.
	 *
	 * @param capacity the localized capacity of this book space
	 * @param locale the locale of the language
	 */
	public void setCapacity(String capacity, Locale locale);

	/**
	 * Sets the localized capacity of this book space in the language, and sets the default locale.
	 *
	 * @param capacity the localized capacity of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setCapacity(
		String capacity, Locale locale, Locale defaultLocale);

	public void setCapacityCurrentLanguageId(String languageId);

	/**
	 * Sets the localized capacities of this book space from the map of locales and localized capacities.
	 *
	 * @param capacityMap the locales and localized capacities of this book space
	 */
	public void setCapacityMap(Map<Locale, String> capacityMap);

	/**
	 * Sets the localized capacities of this book space from the map of locales and localized capacities, and sets the default locale.
	 *
	 * @param capacityMap the locales and localized capacities of this book space
	 * @param defaultLocale the default locale
	 */
	public void setCapacityMap(
		Map<Locale, String> capacityMap, Locale defaultLocale);

	/**
	 * Returns the area of this book space.
	 *
	 * @return the area of this book space
	 */
	public String getArea();

	/**
	 * Returns the localized area of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized area of this book space
	 */
	@AutoEscape
	public String getArea(Locale locale);

	/**
	 * Returns the localized area of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized area of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getArea(Locale locale, boolean useDefault);

	/**
	 * Returns the localized area of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized area of this book space
	 */
	@AutoEscape
	public String getArea(String languageId);

	/**
	 * Returns the localized area of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized area of this book space
	 */
	@AutoEscape
	public String getArea(String languageId, boolean useDefault);

	@AutoEscape
	public String getAreaCurrentLanguageId();

	@AutoEscape
	public String getAreaCurrentValue();

	/**
	 * Returns a map of the locales and localized areas of this book space.
	 *
	 * @return the locales and localized areas of this book space
	 */
	public Map<Locale, String> getAreaMap();

	/**
	 * Sets the area of this book space.
	 *
	 * @param area the area of this book space
	 */
	public void setArea(String area);

	/**
	 * Sets the localized area of this book space in the language.
	 *
	 * @param area the localized area of this book space
	 * @param locale the locale of the language
	 */
	public void setArea(String area, Locale locale);

	/**
	 * Sets the localized area of this book space in the language, and sets the default locale.
	 *
	 * @param area the localized area of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setArea(String area, Locale locale, Locale defaultLocale);

	public void setAreaCurrentLanguageId(String languageId);

	/**
	 * Sets the localized areas of this book space from the map of locales and localized areas.
	 *
	 * @param areaMap the locales and localized areas of this book space
	 */
	public void setAreaMap(Map<Locale, String> areaMap);

	/**
	 * Sets the localized areas of this book space from the map of locales and localized areas, and sets the default locale.
	 *
	 * @param areaMap the locales and localized areas of this book space
	 * @param defaultLocale the default locale
	 */
	public void setAreaMap(Map<Locale, String> areaMap, Locale defaultLocale);

	/**
	 * Returns the duration of this book space.
	 *
	 * @return the duration of this book space
	 */
	public String getDuration();

	/**
	 * Returns the localized duration of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized duration of this book space
	 */
	@AutoEscape
	public String getDuration(Locale locale);

	/**
	 * Returns the localized duration of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized duration of this book space. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getDuration(Locale locale, boolean useDefault);

	/**
	 * Returns the localized duration of this book space in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized duration of this book space
	 */
	@AutoEscape
	public String getDuration(String languageId);

	/**
	 * Returns the localized duration of this book space in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized duration of this book space
	 */
	@AutoEscape
	public String getDuration(String languageId, boolean useDefault);

	@AutoEscape
	public String getDurationCurrentLanguageId();

	@AutoEscape
	public String getDurationCurrentValue();

	/**
	 * Returns a map of the locales and localized durations of this book space.
	 *
	 * @return the locales and localized durations of this book space
	 */
	public Map<Locale, String> getDurationMap();

	/**
	 * Sets the duration of this book space.
	 *
	 * @param duration the duration of this book space
	 */
	public void setDuration(String duration);

	/**
	 * Sets the localized duration of this book space in the language.
	 *
	 * @param duration the localized duration of this book space
	 * @param locale the locale of the language
	 */
	public void setDuration(String duration, Locale locale);

	/**
	 * Sets the localized duration of this book space in the language, and sets the default locale.
	 *
	 * @param duration the localized duration of this book space
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setDuration(
		String duration, Locale locale, Locale defaultLocale);

	public void setDurationCurrentLanguageId(String languageId);

	/**
	 * Sets the localized durations of this book space from the map of locales and localized durations.
	 *
	 * @param durationMap the locales and localized durations of this book space
	 */
	public void setDurationMap(Map<Locale, String> durationMap);

	/**
	 * Sets the localized durations of this book space from the map of locales and localized durations, and sets the default locale.
	 *
	 * @param durationMap the locales and localized durations of this book space
	 * @param defaultLocale the default locale
	 */
	public void setDurationMap(
		Map<Locale, String> durationMap, Locale defaultLocale);

	@Override
	public String[] getAvailableLanguageIds();

	@Override
	public String getDefaultLanguageId();

	@Override
	public void prepareLocalizedFieldsForImport() throws LocaleException;

	@Override
	public void prepareLocalizedFieldsForImport(Locale defaultImportLocale)
		throws LocaleException;

}