<%
    //@MY_TODO: implement pagination functionality
%>
<div class="row">
    <div class="col-12 text-center" >
        <div class="pagination-container">
            <nav aria-label="Pagination">
                <ul class="pagination animate" data-animation="fadeInUp" data-duration="300">
                    <li class="page-item disabled left-arrow-list">
                        <a class="page-link" href="#"><i class="icon-left-arrow"></i></a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item "><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                    <li class="page-item"><a class="page-link dotted" href="#">...</a></li>
                    <li class="page-item right-arrow-list">
                        <a class="page-link" href="#"><i class="icon-right-arrow"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>