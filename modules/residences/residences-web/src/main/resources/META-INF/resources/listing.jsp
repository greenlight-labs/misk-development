<div class="row">
    <%
        List<Residence> residences = ResidenceServiceUtil.getResidences(scopeGroupId);

        for (Residence curResidence : residences) {
    %>
    <portlet:renderURL var="detailPageURL2">
        <portlet:param name="residenceId" value="<%= String.valueOf(curResidence.getResidenceId()) %>" />
        <portlet:param name="mvcPath" value="/detail.jsp" />
    </portlet:renderURL>
    <div class="col-12 col-md-6">
        <div class="residence-card">
            <% if (curResidence.getResidenceId() == 71430){ %>
                <a href="<%= detailPageURL2.toString() %>" class="img-wrapper image-overlay">
                    <img src="<%= curResidence.getListingImage(locale) %>" alt="img" class="img-fluid">
                </a>
            <% }else{ %>
                <a href="javascript:" class="img-wrapper image-overlay">
                    <img src="<%= curResidence.getListingImage(locale) %>" alt="img" class="img-fluid">
                </a>
            <% } %>
            <div class="res-card-info">
                <h3><%= curResidence.getListingTitle(locale) %></h3>
                <p><%= curResidence.getListingDescription(locale) %></p>
                <% if (curResidence.getResidenceId() == 71430){ %>
                <a href="<%= detailPageURL2.toString() %>" class="btn btn-outline-white" tabindex="0">
                    <span><liferay-ui:message key="misk-explore-more" /></span><i class="dot-line"></i>
                </a>
                <% } %>
            </div>
        </div>
    </div>
    <%
        }
    %>
</div>