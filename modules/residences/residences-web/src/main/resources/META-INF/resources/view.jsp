<%@ include file="init.jsp" %>

<%@include file="top_section.jsp" %>

<section class="residence-listing-sec" data-scroll-section>
    <div class="container">

        <%@include file="listing.jsp" %>

        <%--<%@include file="includes/pagination.jsp" %>--%>

    </div>
</section>