<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="java.util.List" %>
<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.petra.string.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil" %>
<%@ page import="java.text.Format" %>
<%@ page import="com.liferay.portal.kernel.theme.ThemeDisplay" %>
<%@ page import="com.liferay.journal.model.JournalArticle" %>
<%@ page import="com.liferay.journal.model.JournalArticleDisplay"%>
<%@ page import="com.liferay.journal.service.JournalArticleLocalServiceUtil" %>
<%@ page import="residences.model.Residence" %>
<%@ page import="residences.service.ResidenceLocalServiceUtil" %>
<%@ page import="residences.service.ResidenceServiceUtil" %>
<%@ page import="residences.model.ResidenceSlider3" %>
<%@ page import="residences.model.ResidenceSlider4" %>
<%@ page import="residences.service.ResidenceSlider3LocalServiceUtil" %>
<%@ page import="residences.service.ResidenceSlider4LocalServiceUtil" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM dd, yyyy", locale, timeZone);
    Format day = FastDateFormatFactoryUtil.getSimpleDateFormat("dd", locale, timeZone);
    Format monthYear = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM, yyyy", locale, timeZone);
    Format shortMonthYear = FastDateFormatFactoryUtil.getSimpleDateFormat("MMM, yyyy", locale, timeZone);
%>