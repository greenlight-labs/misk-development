package residences.web.portlet.route;

import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;
import com.liferay.portal.kernel.portlet.FriendlyURLMapper;
import residences.web.constants.ResidencesWebPortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Adolfo P??rez
 */
@Component(
	property = {
		"com.liferay.portlet.friendly-url-routes=META-INF/friendly-url-routes/routes.xml",
		"javax.portlet.name=" + ResidencesWebPortletKeys.RESIDENCESWEB
	},
	service = FriendlyURLMapper.class
)
public class ResidenceFriendlyURLMapper extends DefaultFriendlyURLMapper {

	@Override
	public String getMapping() {
		return _MAPPING;
	}

	private static final String _MAPPING = "residences";

}