/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResidenceLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceLocalService
 * @generated
 */
public class ResidenceLocalServiceWrapper
	implements ResidenceLocalService, ServiceWrapper<ResidenceLocalService> {

	public ResidenceLocalServiceWrapper(
		ResidenceLocalService residenceLocalService) {

		_residenceLocalService = residenceLocalService;
	}

	@Override
	public residences.model.Residence addResidence(
			long userId, long residenceId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerTitleMap,
			java.util.Map<java.util.Locale, String> bannerSubtitleMap,
			java.util.Map<java.util.Locale, String> bannerDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerDesktopImageMap,
			java.util.Map<java.util.Locale, String> bannerMobileImageMap,
			java.util.Map<java.util.Locale, String> section2TitleMap,
			java.util.Map<java.util.Locale, String> section2SubtitleMap,
			java.util.Map<java.util.Locale, String> section2DescriptionMap,
			java.util.Map<java.util.Locale, String> section2ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section2ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section2ImageMap,
			java.util.Map<java.util.Locale, String> section5TitleMap,
			java.util.Map<java.util.Locale, String> section5SubtitleMap,
			java.util.Map<java.util.Locale, String> section5DescriptionMap,
			java.util.Map<java.util.Locale, String> section5ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section5ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section5ImageMap,
			java.util.Map<java.util.Locale, String> section6TitleMap,
			java.util.Map<java.util.Locale, String> section6SubtitleMap,
			java.util.Map<java.util.Locale, String> section6DescriptionMap,
			java.util.Map<java.util.Locale, String> section6ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section6ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section6ImageMap,
			java.util.Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceLocalService.addResidence(
			userId, residenceId, listingImageMap, listingTitleMap,
			listingDescriptionMap, bannerTitleMap, bannerSubtitleMap,
			bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
			section2TitleMap, section2SubtitleMap, section2DescriptionMap,
			section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
			section5TitleMap, section5SubtitleMap, section5DescriptionMap,
			section5ButtonLabelMap, section5ButtonLinkMap, section5ImageMap,
			section6TitleMap, section6SubtitleMap, section6DescriptionMap,
			section6ButtonLabelMap, section6ButtonLinkMap, section6ImageMap,
			section4SectionTitleMap, serviceContext);
	}

	/**
	 * Adds the residence to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was added
	 */
	@Override
	public residences.model.Residence addResidence(
		residences.model.Residence residence) {

		return _residenceLocalService.addResidence(residence);
	}

	@Override
	public int countByT_D(String query, String languageId) {
		return _residenceLocalService.countByT_D(query, languageId);
	}

	@Override
	public int countSearchCompleteWebsite(String query, String languageId) {
		return _residenceLocalService.countSearchCompleteWebsite(
			query, languageId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new residence with the primary key. Does not add the residence to the database.
	 *
	 * @param residenceId the primary key for the new residence
	 * @return the new residence
	 */
	@Override
	public residences.model.Residence createResidence(long residenceId) {
		return _residenceLocalService.createResidence(residenceId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the residence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence that was removed
	 * @throws PortalException if a residence with the primary key could not be found
	 */
	@Override
	public residences.model.Residence deleteResidence(long residenceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceLocalService.deleteResidence(residenceId);
	}

	@Override
	public residences.model.Residence deleteResidence(
			long residenceId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _residenceLocalService.deleteResidence(
			residenceId, serviceContext);
	}

	/**
	 * Deletes the residence from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was removed
	 */
	@Override
	public residences.model.Residence deleteResidence(
		residences.model.Residence residence) {

		return _residenceLocalService.deleteResidence(residence);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _residenceLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _residenceLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _residenceLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _residenceLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _residenceLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _residenceLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public residences.model.Residence fetchResidence(long residenceId) {
		return _residenceLocalService.fetchResidence(residenceId);
	}

	/**
	 * Returns the residence matching the UUID and group.
	 *
	 * @param uuid the residence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching residence, or <code>null</code> if a matching residence could not be found
	 */
	@Override
	public residences.model.Residence fetchResidenceByUuidAndGroupId(
		String uuid, long groupId) {

		return _residenceLocalService.fetchResidenceByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<residences.model.Residence> findByT_D(
		String query, int offset, int limit, String languageId) {

		return _residenceLocalService.findByT_D(
			query, offset, limit, languageId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _residenceLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _residenceLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _residenceLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _residenceLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the residence with the primary key.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence
	 * @throws PortalException if a residence with the primary key could not be found
	 */
	@Override
	public residences.model.Residence getResidence(long residenceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceLocalService.getResidence(residenceId);
	}

	/**
	 * Returns the residence matching the UUID and group.
	 *
	 * @param uuid the residence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching residence
	 * @throws PortalException if a matching residence could not be found
	 */
	@Override
	public residences.model.Residence getResidenceByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceLocalService.getResidenceByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of residences
	 */
	@Override
	public java.util.List<residences.model.Residence> getResidences(
		int start, int end) {

		return _residenceLocalService.getResidences(start, end);
	}

	@Override
	public java.util.List<residences.model.Residence> getResidences(
		long groupId) {

		return _residenceLocalService.getResidences(groupId);
	}

	@Override
	public java.util.List<residences.model.Residence> getResidences(
		long groupId, int start, int end) {

		return _residenceLocalService.getResidences(groupId, start, end);
	}

	@Override
	public java.util.List<residences.model.Residence> getResidences(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<residences.model.Residence> obc) {

		return _residenceLocalService.getResidences(groupId, start, end, obc);
	}

	/**
	 * Returns all the residences matching the UUID and company.
	 *
	 * @param uuid the UUID of the residences
	 * @param companyId the primary key of the company
	 * @return the matching residences, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<residences.model.Residence>
		getResidencesByUuidAndCompanyId(String uuid, long companyId) {

		return _residenceLocalService.getResidencesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of residences matching the UUID and company.
	 *
	 * @param uuid the UUID of the residences
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching residences, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<residences.model.Residence>
		getResidencesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<residences.model.Residence> orderByComparator) {

		return _residenceLocalService.getResidencesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of residences.
	 *
	 * @return the number of residences
	 */
	@Override
	public int getResidencesCount() {
		return _residenceLocalService.getResidencesCount();
	}

	@Override
	public int getResidencesCount(long groupId) {
		return _residenceLocalService.getResidencesCount(groupId);
	}

	@Override
	public java.util.List<Object> searchCompleteWebsite(
		String query, int offset, int limit, String languageId) {

		return _residenceLocalService.searchCompleteWebsite(
			query, offset, limit, languageId);
	}

	@Override
	public residences.model.Residence updateResidence(
			long userId, long residenceId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerTitleMap,
			java.util.Map<java.util.Locale, String> bannerSubtitleMap,
			java.util.Map<java.util.Locale, String> bannerDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerDesktopImageMap,
			java.util.Map<java.util.Locale, String> bannerMobileImageMap,
			java.util.Map<java.util.Locale, String> section2TitleMap,
			java.util.Map<java.util.Locale, String> section2SubtitleMap,
			java.util.Map<java.util.Locale, String> section2DescriptionMap,
			java.util.Map<java.util.Locale, String> section2ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section2ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section2ImageMap,
			java.util.Map<java.util.Locale, String> section5TitleMap,
			java.util.Map<java.util.Locale, String> section5SubtitleMap,
			java.util.Map<java.util.Locale, String> section5DescriptionMap,
			java.util.Map<java.util.Locale, String> section5ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section5ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section5ImageMap,
			java.util.Map<java.util.Locale, String> section6TitleMap,
			java.util.Map<java.util.Locale, String> section6SubtitleMap,
			java.util.Map<java.util.Locale, String> section6DescriptionMap,
			java.util.Map<java.util.Locale, String> section6ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section6ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section6ImageMap,
			java.util.Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _residenceLocalService.updateResidence(
			userId, residenceId, listingImageMap, listingTitleMap,
			listingDescriptionMap, bannerTitleMap, bannerSubtitleMap,
			bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
			section2TitleMap, section2SubtitleMap, section2DescriptionMap,
			section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
			section5TitleMap, section5SubtitleMap, section5DescriptionMap,
			section5ButtonLabelMap, section5ButtonLinkMap, section5ImageMap,
			section6TitleMap, section6SubtitleMap, section6DescriptionMap,
			section6ButtonLabelMap, section6ButtonLinkMap, section6ImageMap,
			section4SectionTitleMap, serviceContext);
	}

	/**
	 * Updates the residence in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was updated
	 */
	@Override
	public residences.model.Residence updateResidence(
		residences.model.Residence residence) {

		return _residenceLocalService.updateResidence(residence);
	}

	@Override
	public ResidenceLocalService getWrappedService() {
		return _residenceLocalService;
	}

	@Override
	public void setWrappedService(ResidenceLocalService residenceLocalService) {
		_residenceLocalService = residenceLocalService;
	}

	private ResidenceLocalService _residenceLocalService;

}