/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResidenceSlider3Service}.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3Service
 * @generated
 */
public class ResidenceSlider3ServiceWrapper
	implements ResidenceSlider3Service,
			   ServiceWrapper<ResidenceSlider3Service> {

	public ResidenceSlider3ServiceWrapper(
		ResidenceSlider3Service residenceSlider3Service) {

		_residenceSlider3Service = residenceSlider3Service;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _residenceSlider3Service.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider3>
		getResidenceSlider3Slides(long residenceId) {

		return _residenceSlider3Service.getResidenceSlider3Slides(residenceId);
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider3>
		getResidenceSlider3Slides(long residenceId, int start, int end) {

		return _residenceSlider3Service.getResidenceSlider3Slides(
			residenceId, start, end);
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider3>
		getResidenceSlider3Slides(
			long residenceId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<residences.model.ResidenceSlider3> obc) {

		return _residenceSlider3Service.getResidenceSlider3Slides(
			residenceId, start, end, obc);
	}

	@Override
	public int getResidenceSlider3SlidesCount(long residenceId) {
		return _residenceSlider3Service.getResidenceSlider3SlidesCount(
			residenceId);
	}

	@Override
	public ResidenceSlider3Service getWrappedService() {
		return _residenceSlider3Service;
	}

	@Override
	public void setWrappedService(
		ResidenceSlider3Service residenceSlider3Service) {

		_residenceSlider3Service = residenceSlider3Service;
	}

	private ResidenceSlider3Service _residenceSlider3Service;

}