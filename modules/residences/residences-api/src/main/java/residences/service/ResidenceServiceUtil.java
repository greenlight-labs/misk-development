/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

import residences.model.Residence;

/**
 * Provides the remote service utility for Residence. This utility wraps
 * <code>residences.service.impl.ResidenceServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceService
 * @generated
 */
public class ResidenceServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>residences.service.impl.ResidenceServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static List<Residence> getResidences(long groupId) {
		return getService().getResidences(groupId);
	}

	public static List<Residence> getResidences(
		long groupId, int start, int end) {

		return getService().getResidences(groupId, start, end);
	}

	public static List<Residence> getResidences(
		long groupId, int start, int end, OrderByComparator<Residence> obc) {

		return getService().getResidences(groupId, start, end, obc);
	}

	public static int getResidencesCount(long groupId) {
		return getService().getResidencesCount(groupId);
	}

	public static ResidenceService getService() {
		return _service;
	}

	private static volatile ResidenceService _service;

}