/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResidenceService}.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceService
 * @generated
 */
public class ResidenceServiceWrapper
	implements ResidenceService, ServiceWrapper<ResidenceService> {

	public ResidenceServiceWrapper(ResidenceService residenceService) {
		_residenceService = residenceService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _residenceService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<residences.model.Residence> getResidences(
		long groupId) {

		return _residenceService.getResidences(groupId);
	}

	@Override
	public java.util.List<residences.model.Residence> getResidences(
		long groupId, int start, int end) {

		return _residenceService.getResidences(groupId, start, end);
	}

	@Override
	public java.util.List<residences.model.Residence> getResidences(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<residences.model.Residence> obc) {

		return _residenceService.getResidences(groupId, start, end, obc);
	}

	@Override
	public int getResidencesCount(long groupId) {
		return _residenceService.getResidencesCount(groupId);
	}

	@Override
	public ResidenceService getWrappedService() {
		return _residenceService;
	}

	@Override
	public void setWrappedService(ResidenceService residenceService) {
		_residenceService = residenceService;
	}

	private ResidenceService _residenceService;

}