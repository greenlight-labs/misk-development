/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

import residences.exception.NoSuchSlider3Exception;

import residences.model.ResidenceSlider3;

/**
 * The persistence interface for the residence slider3 service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3Util
 * @generated
 */
@ProviderType
public interface ResidenceSlider3Persistence
	extends BasePersistence<ResidenceSlider3> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ResidenceSlider3Util} to access the residence slider3 persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the residence slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid(String uuid);

	/**
	 * Returns a range of all the residence slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns the residence slider3s before and after the current residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current residence slider3
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	public ResidenceSlider3[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Removes all the residence slider3s where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of residence slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching residence slider3s
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns the residence slider3s before and after the current residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current residence slider3
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	public ResidenceSlider3[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Removes all the residence slider3s where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching residence slider3s
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the residence slider3s where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @return the matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByResidenceId(long residenceId);

	/**
	 * Returns a range of all the residence slider3s where residenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param residenceId the residence ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByResidenceId(
		long residenceId, int start, int end);

	/**
	 * Returns an ordered range of all the residence slider3s where residenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param residenceId the residence ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByResidenceId(
		long residenceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residence slider3s where residenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param residenceId the residence ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findByResidenceId(
		long residenceId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 findByResidenceId_First(
			long residenceId,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the first residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 fetchByResidenceId_First(
		long residenceId,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns the last residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 findByResidenceId_Last(
			long residenceId,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the last residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	public ResidenceSlider3 fetchByResidenceId_Last(
		long residenceId,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns the residence slider3s before and after the current residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param slideId the primary key of the current residence slider3
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	public ResidenceSlider3[] findByResidenceId_PrevAndNext(
			long slideId, long residenceId,
			com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Removes all the residence slider3s where residenceId = &#63; from the database.
	 *
	 * @param residenceId the residence ID
	 */
	public void removeByResidenceId(long residenceId);

	/**
	 * Returns the number of residence slider3s where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @return the number of matching residence slider3s
	 */
	public int countByResidenceId(long residenceId);

	/**
	 * Caches the residence slider3 in the entity cache if it is enabled.
	 *
	 * @param residenceSlider3 the residence slider3
	 */
	public void cacheResult(ResidenceSlider3 residenceSlider3);

	/**
	 * Caches the residence slider3s in the entity cache if it is enabled.
	 *
	 * @param residenceSlider3s the residence slider3s
	 */
	public void cacheResult(java.util.List<ResidenceSlider3> residenceSlider3s);

	/**
	 * Creates a new residence slider3 with the primary key. Does not add the residence slider3 to the database.
	 *
	 * @param slideId the primary key for the new residence slider3
	 * @return the new residence slider3
	 */
	public ResidenceSlider3 create(long slideId);

	/**
	 * Removes the residence slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3 that was removed
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	public ResidenceSlider3 remove(long slideId) throws NoSuchSlider3Exception;

	public ResidenceSlider3 updateImpl(ResidenceSlider3 residenceSlider3);

	/**
	 * Returns the residence slider3 with the primary key or throws a <code>NoSuchSlider3Exception</code> if it could not be found.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	public ResidenceSlider3 findByPrimaryKey(long slideId)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the residence slider3 with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3, or <code>null</code> if a residence slider3 with the primary key could not be found
	 */
	public ResidenceSlider3 fetchByPrimaryKey(long slideId);

	/**
	 * Returns all the residence slider3s.
	 *
	 * @return the residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findAll();

	/**
	 * Returns a range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of residence slider3s
	 */
	public java.util.List<ResidenceSlider3> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ResidenceSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the residence slider3s from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of residence slider3s.
	 *
	 * @return the number of residence slider3s
	 */
	public int countAll();

}