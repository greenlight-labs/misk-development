/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

import residences.model.ResidenceSlider3;

/**
 * Provides the remote service utility for ResidenceSlider3. This utility wraps
 * <code>residences.service.impl.ResidenceSlider3ServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3Service
 * @generated
 */
public class ResidenceSlider3ServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>residences.service.impl.ResidenceSlider3ServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId) {

		return getService().getResidenceSlider3Slides(residenceId);
	}

	public static List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId, int start, int end) {

		return getService().getResidenceSlider3Slides(residenceId, start, end);
	}

	public static List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId, int start, int end,
		OrderByComparator<ResidenceSlider3> obc) {

		return getService().getResidenceSlider3Slides(
			residenceId, start, end, obc);
	}

	public static int getResidenceSlider3SlidesCount(long residenceId) {
		return getService().getResidenceSlider3SlidesCount(residenceId);
	}

	public static ResidenceSlider3Service getService() {
		return _service;
	}

	private static volatile ResidenceSlider3Service _service;

}