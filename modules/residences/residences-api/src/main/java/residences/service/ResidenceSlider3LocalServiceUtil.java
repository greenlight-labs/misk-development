/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import residences.model.ResidenceSlider3;

/**
 * Provides the local service utility for ResidenceSlider3. This utility wraps
 * <code>residences.service.impl.ResidenceSlider3LocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3LocalService
 * @generated
 */
public class ResidenceSlider3LocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>residences.service.impl.ResidenceSlider3LocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static ResidenceSlider3 addResidenceSlider3(
			long userId, long residenceId,
			Map<java.util.Locale, String> section3ImageMap,
			Map<java.util.Locale, String> section3DescriptionMap,
			Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addResidenceSlider3(
			userId, residenceId, section3ImageMap, section3DescriptionMap,
			section3ThumbnailMap, serviceContext);
	}

	/**
	 * Adds the residence slider3 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was added
	 */
	public static ResidenceSlider3 addResidenceSlider3(
		ResidenceSlider3 residenceSlider3) {

		return getService().addResidenceSlider3(residenceSlider3);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new residence slider3 with the primary key. Does not add the residence slider3 to the database.
	 *
	 * @param slideId the primary key for the new residence slider3
	 * @return the new residence slider3
	 */
	public static ResidenceSlider3 createResidenceSlider3(long slideId) {
		return getService().createResidenceSlider3(slideId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the residence slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3 that was removed
	 * @throws PortalException if a residence slider3 with the primary key could not be found
	 */
	public static ResidenceSlider3 deleteResidenceSlider3(long slideId)
		throws PortalException {

		return getService().deleteResidenceSlider3(slideId);
	}

	public static ResidenceSlider3 deleteResidenceSlider3(
			long slideId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteResidenceSlider3(slideId, serviceContext);
	}

	/**
	 * Deletes the residence slider3 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was removed
	 */
	public static ResidenceSlider3 deleteResidenceSlider3(
		ResidenceSlider3 residenceSlider3) {

		return getService().deleteResidenceSlider3(residenceSlider3);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ResidenceSlider3 fetchResidenceSlider3(long slideId) {
		return getService().fetchResidenceSlider3(slideId);
	}

	/**
	 * Returns the residence slider3 with the matching UUID and company.
	 *
	 * @param uuid the residence slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	public static ResidenceSlider3 fetchResidenceSlider3ByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchResidenceSlider3ByUuidAndCompanyId(
			uuid, companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the residence slider3 with the primary key.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3
	 * @throws PortalException if a residence slider3 with the primary key could not be found
	 */
	public static ResidenceSlider3 getResidenceSlider3(long slideId)
		throws PortalException {

		return getService().getResidenceSlider3(slideId);
	}

	/**
	 * Returns the residence slider3 with the matching UUID and company.
	 *
	 * @param uuid the residence slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider3
	 * @throws PortalException if a matching residence slider3 could not be found
	 */
	public static ResidenceSlider3 getResidenceSlider3ByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getResidenceSlider3ByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of residence slider3s
	 */
	public static List<ResidenceSlider3> getResidenceSlider3s(
		int start, int end) {

		return getService().getResidenceSlider3s(start, end);
	}

	/**
	 * Returns the number of residence slider3s.
	 *
	 * @return the number of residence slider3s
	 */
	public static int getResidenceSlider3sCount() {
		return getService().getResidenceSlider3sCount();
	}

	public static List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId) {

		return getService().getResidenceSlider3Slides(residenceId);
	}

	public static List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId, int start, int end) {

		return getService().getResidenceSlider3Slides(residenceId, start, end);
	}

	public static List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId, int start, int end,
		OrderByComparator<ResidenceSlider3> obc) {

		return getService().getResidenceSlider3Slides(
			residenceId, start, end, obc);
	}

	public static int getResidenceSlider3SlidesCount(long residenceId) {
		return getService().getResidenceSlider3SlidesCount(residenceId);
	}

	public static ResidenceSlider3 updateResidenceSlider3(
			long userId, long slideId, long residenceId,
			Map<java.util.Locale, String> section3ImageMap,
			Map<java.util.Locale, String> section3DescriptionMap,
			Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateResidenceSlider3(
			userId, slideId, residenceId, section3ImageMap,
			section3DescriptionMap, section3ThumbnailMap, serviceContext);
	}

	/**
	 * Updates the residence slider3 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was updated
	 */
	public static ResidenceSlider3 updateResidenceSlider3(
		ResidenceSlider3 residenceSlider3) {

		return getService().updateResidenceSlider3(residenceSlider3);
	}

	public static ResidenceSlider3LocalService getService() {
		return _service;
	}

	private static volatile ResidenceSlider3LocalService _service;

}