/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import residences.model.Residence;

/**
 * Provides the local service utility for Residence. This utility wraps
 * <code>residences.service.impl.ResidenceLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceLocalService
 * @generated
 */
public class ResidenceLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>residences.service.impl.ResidenceLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Residence addResidence(
			long userId, long residenceId,
			Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> bannerTitleMap,
			Map<java.util.Locale, String> bannerSubtitleMap,
			Map<java.util.Locale, String> bannerDescriptionMap,
			Map<java.util.Locale, String> bannerDesktopImageMap,
			Map<java.util.Locale, String> bannerMobileImageMap,
			Map<java.util.Locale, String> section2TitleMap,
			Map<java.util.Locale, String> section2SubtitleMap,
			Map<java.util.Locale, String> section2DescriptionMap,
			Map<java.util.Locale, String> section2ButtonLabelMap,
			Map<java.util.Locale, String> section2ButtonLinkMap,
			Map<java.util.Locale, String> section2ImageMap,
			Map<java.util.Locale, String> section5TitleMap,
			Map<java.util.Locale, String> section5SubtitleMap,
			Map<java.util.Locale, String> section5DescriptionMap,
			Map<java.util.Locale, String> section5ButtonLabelMap,
			Map<java.util.Locale, String> section5ButtonLinkMap,
			Map<java.util.Locale, String> section5ImageMap,
			Map<java.util.Locale, String> section6TitleMap,
			Map<java.util.Locale, String> section6SubtitleMap,
			Map<java.util.Locale, String> section6DescriptionMap,
			Map<java.util.Locale, String> section6ButtonLabelMap,
			Map<java.util.Locale, String> section6ButtonLinkMap,
			Map<java.util.Locale, String> section6ImageMap,
			Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addResidence(
			userId, residenceId, listingImageMap, listingTitleMap,
			listingDescriptionMap, bannerTitleMap, bannerSubtitleMap,
			bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
			section2TitleMap, section2SubtitleMap, section2DescriptionMap,
			section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
			section5TitleMap, section5SubtitleMap, section5DescriptionMap,
			section5ButtonLabelMap, section5ButtonLinkMap, section5ImageMap,
			section6TitleMap, section6SubtitleMap, section6DescriptionMap,
			section6ButtonLabelMap, section6ButtonLinkMap, section6ImageMap,
			section4SectionTitleMap, serviceContext);
	}

	/**
	 * Adds the residence to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was added
	 */
	public static Residence addResidence(Residence residence) {
		return getService().addResidence(residence);
	}

	public static int countByT_D(String query, String languageId) {
		return getService().countByT_D(query, languageId);
	}

	public static int countSearchCompleteWebsite(
		String query, String languageId) {

		return getService().countSearchCompleteWebsite(query, languageId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new residence with the primary key. Does not add the residence to the database.
	 *
	 * @param residenceId the primary key for the new residence
	 * @return the new residence
	 */
	public static Residence createResidence(long residenceId) {
		return getService().createResidence(residenceId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the residence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence that was removed
	 * @throws PortalException if a residence with the primary key could not be found
	 */
	public static Residence deleteResidence(long residenceId)
		throws PortalException {

		return getService().deleteResidence(residenceId);
	}

	public static Residence deleteResidence(
			long residenceId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteResidence(residenceId, serviceContext);
	}

	/**
	 * Deletes the residence from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was removed
	 */
	public static Residence deleteResidence(Residence residence) {
		return getService().deleteResidence(residence);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Residence fetchResidence(long residenceId) {
		return getService().fetchResidence(residenceId);
	}

	/**
	 * Returns the residence matching the UUID and group.
	 *
	 * @param uuid the residence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchResidenceByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchResidenceByUuidAndGroupId(uuid, groupId);
	}

	public static List<Residence> findByT_D(
		String query, int offset, int limit, String languageId) {

		return getService().findByT_D(query, offset, limit, languageId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the residence with the primary key.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence
	 * @throws PortalException if a residence with the primary key could not be found
	 */
	public static Residence getResidence(long residenceId)
		throws PortalException {

		return getService().getResidence(residenceId);
	}

	/**
	 * Returns the residence matching the UUID and group.
	 *
	 * @param uuid the residence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching residence
	 * @throws PortalException if a matching residence could not be found
	 */
	public static Residence getResidenceByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getResidenceByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of residences
	 */
	public static List<Residence> getResidences(int start, int end) {
		return getService().getResidences(start, end);
	}

	public static List<Residence> getResidences(long groupId) {
		return getService().getResidences(groupId);
	}

	public static List<Residence> getResidences(
		long groupId, int start, int end) {

		return getService().getResidences(groupId, start, end);
	}

	public static List<Residence> getResidences(
		long groupId, int start, int end, OrderByComparator<Residence> obc) {

		return getService().getResidences(groupId, start, end, obc);
	}

	/**
	 * Returns all the residences matching the UUID and company.
	 *
	 * @param uuid the UUID of the residences
	 * @param companyId the primary key of the company
	 * @return the matching residences, or an empty list if no matches were found
	 */
	public static List<Residence> getResidencesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getResidencesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of residences matching the UUID and company.
	 *
	 * @param uuid the UUID of the residences
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching residences, or an empty list if no matches were found
	 */
	public static List<Residence> getResidencesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Residence> orderByComparator) {

		return getService().getResidencesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of residences.
	 *
	 * @return the number of residences
	 */
	public static int getResidencesCount() {
		return getService().getResidencesCount();
	}

	public static int getResidencesCount(long groupId) {
		return getService().getResidencesCount(groupId);
	}

	public static List<Object> searchCompleteWebsite(
		String query, int offset, int limit, String languageId) {

		return getService().searchCompleteWebsite(
			query, offset, limit, languageId);
	}

	public static Residence updateResidence(
			long userId, long residenceId,
			Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> bannerTitleMap,
			Map<java.util.Locale, String> bannerSubtitleMap,
			Map<java.util.Locale, String> bannerDescriptionMap,
			Map<java.util.Locale, String> bannerDesktopImageMap,
			Map<java.util.Locale, String> bannerMobileImageMap,
			Map<java.util.Locale, String> section2TitleMap,
			Map<java.util.Locale, String> section2SubtitleMap,
			Map<java.util.Locale, String> section2DescriptionMap,
			Map<java.util.Locale, String> section2ButtonLabelMap,
			Map<java.util.Locale, String> section2ButtonLinkMap,
			Map<java.util.Locale, String> section2ImageMap,
			Map<java.util.Locale, String> section5TitleMap,
			Map<java.util.Locale, String> section5SubtitleMap,
			Map<java.util.Locale, String> section5DescriptionMap,
			Map<java.util.Locale, String> section5ButtonLabelMap,
			Map<java.util.Locale, String> section5ButtonLinkMap,
			Map<java.util.Locale, String> section5ImageMap,
			Map<java.util.Locale, String> section6TitleMap,
			Map<java.util.Locale, String> section6SubtitleMap,
			Map<java.util.Locale, String> section6DescriptionMap,
			Map<java.util.Locale, String> section6ButtonLabelMap,
			Map<java.util.Locale, String> section6ButtonLinkMap,
			Map<java.util.Locale, String> section6ImageMap,
			Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateResidence(
			userId, residenceId, listingImageMap, listingTitleMap,
			listingDescriptionMap, bannerTitleMap, bannerSubtitleMap,
			bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
			section2TitleMap, section2SubtitleMap, section2DescriptionMap,
			section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
			section5TitleMap, section5SubtitleMap, section5DescriptionMap,
			section5ButtonLabelMap, section5ButtonLinkMap, section5ImageMap,
			section6TitleMap, section6SubtitleMap, section6DescriptionMap,
			section6ButtonLabelMap, section6ButtonLinkMap, section6ImageMap,
			section4SectionTitleMap, serviceContext);
	}

	/**
	 * Updates the residence in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was updated
	 */
	public static Residence updateResidence(Residence residence) {
		return getService().updateResidence(residence);
	}

	public static ResidenceLocalService getService() {
		return _service;
	}

	private static volatile ResidenceLocalService _service;

}