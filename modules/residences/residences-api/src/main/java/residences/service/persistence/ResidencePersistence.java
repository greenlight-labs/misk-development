/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

import residences.exception.NoSuchResidenceException;

import residences.model.Residence;

/**
 * The persistence interface for the residence service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceUtil
 * @generated
 */
@ProviderType
public interface ResidencePersistence extends BasePersistence<Residence> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ResidenceUtil} to access the residence persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the residences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching residences
	 */
	public java.util.List<Residence> findByUuid(String uuid);

	/**
	 * Returns a range of all the residences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of matching residences
	 */
	public java.util.List<Residence> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the residences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residences
	 */
	public java.util.List<Residence> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residences
	 */
	public java.util.List<Residence> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public Residence findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Returns the first residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns the last residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public Residence findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Returns the last residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns the residences before and after the current residence in the ordered set where uuid = &#63;.
	 *
	 * @param residenceId the primary key of the current residence
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public Residence[] findByUuid_PrevAndNext(
			long residenceId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Removes all the residences where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of residences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching residences
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the residence where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchResidenceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public Residence findByUUID_G(String uuid, long groupId)
		throws NoSuchResidenceException;

	/**
	 * Returns the residence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the residence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the residence where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the residence that was removed
	 */
	public Residence removeByUUID_G(String uuid, long groupId)
		throws NoSuchResidenceException;

	/**
	 * Returns the number of residences where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching residences
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching residences
	 */
	public java.util.List<Residence> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of matching residences
	 */
	public java.util.List<Residence> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residences
	 */
	public java.util.List<Residence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residences
	 */
	public java.util.List<Residence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public Residence findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Returns the first residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns the last residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public Residence findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Returns the last residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns the residences before and after the current residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param residenceId the primary key of the current residence
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public Residence[] findByUuid_C_PrevAndNext(
			long residenceId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Removes all the residences where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of residences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching residences
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the residences where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching residences
	 */
	public java.util.List<Residence> findByGroupId(long groupId);

	/**
	 * Returns a range of all the residences where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of matching residences
	 */
	public java.util.List<Residence> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the residences where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residences
	 */
	public java.util.List<Residence> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residences where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residences
	 */
	public java.util.List<Residence> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public Residence findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Returns the first residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns the last residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public Residence findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Returns the last residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public Residence fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns the residences before and after the current residence in the ordered set where groupId = &#63;.
	 *
	 * @param residenceId the primary key of the current residence
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public Residence[] findByGroupId_PrevAndNext(
			long residenceId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Residence>
				orderByComparator)
		throws NoSuchResidenceException;

	/**
	 * Removes all the residences where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of residences where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching residences
	 */
	public int countByGroupId(long groupId);

	/**
	 * Caches the residence in the entity cache if it is enabled.
	 *
	 * @param residence the residence
	 */
	public void cacheResult(Residence residence);

	/**
	 * Caches the residences in the entity cache if it is enabled.
	 *
	 * @param residences the residences
	 */
	public void cacheResult(java.util.List<Residence> residences);

	/**
	 * Creates a new residence with the primary key. Does not add the residence to the database.
	 *
	 * @param residenceId the primary key for the new residence
	 * @return the new residence
	 */
	public Residence create(long residenceId);

	/**
	 * Removes the residence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence that was removed
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public Residence remove(long residenceId) throws NoSuchResidenceException;

	public Residence updateImpl(Residence residence);

	/**
	 * Returns the residence with the primary key or throws a <code>NoSuchResidenceException</code> if it could not be found.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public Residence findByPrimaryKey(long residenceId)
		throws NoSuchResidenceException;

	/**
	 * Returns the residence with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence, or <code>null</code> if a residence with the primary key could not be found
	 */
	public Residence fetchByPrimaryKey(long residenceId);

	/**
	 * Returns all the residences.
	 *
	 * @return the residences
	 */
	public java.util.List<Residence> findAll();

	/**
	 * Returns a range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of residences
	 */
	public java.util.List<Residence> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of residences
	 */
	public java.util.List<Residence> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator);

	/**
	 * Returns an ordered range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of residences
	 */
	public java.util.List<Residence> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Residence>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the residences from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of residences.
	 *
	 * @return the number of residences
	 */
	public int countAll();

}