/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResidenceSlider4LocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider4LocalService
 * @generated
 */
public class ResidenceSlider4LocalServiceWrapper
	implements ResidenceSlider4LocalService,
			   ServiceWrapper<ResidenceSlider4LocalService> {

	public ResidenceSlider4LocalServiceWrapper(
		ResidenceSlider4LocalService residenceSlider4LocalService) {

		_residenceSlider4LocalService = residenceSlider4LocalService;
	}

	@Override
	public residences.model.ResidenceSlider4 addResidenceSlider4(
			long userId, long residenceId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> descriptionMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider4LocalService.addResidenceSlider4(
			userId, residenceId, titleMap, descriptionMap, imageMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Adds the residence slider4 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider4 the residence slider4
	 * @return the residence slider4 that was added
	 */
	@Override
	public residences.model.ResidenceSlider4 addResidenceSlider4(
		residences.model.ResidenceSlider4 residenceSlider4) {

		return _residenceSlider4LocalService.addResidenceSlider4(
			residenceSlider4);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider4LocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new residence slider4 with the primary key. Does not add the residence slider4 to the database.
	 *
	 * @param slideId the primary key for the new residence slider4
	 * @return the new residence slider4
	 */
	@Override
	public residences.model.ResidenceSlider4 createResidenceSlider4(
		long slideId) {

		return _residenceSlider4LocalService.createResidenceSlider4(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider4LocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the residence slider4 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the residence slider4
	 * @return the residence slider4 that was removed
	 * @throws PortalException if a residence slider4 with the primary key could not be found
	 */
	@Override
	public residences.model.ResidenceSlider4 deleteResidenceSlider4(
			long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider4LocalService.deleteResidenceSlider4(slideId);
	}

	@Override
	public residences.model.ResidenceSlider4 deleteResidenceSlider4(
			long slideId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _residenceSlider4LocalService.deleteResidenceSlider4(
			slideId, serviceContext);
	}

	/**
	 * Deletes the residence slider4 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider4 the residence slider4
	 * @return the residence slider4 that was removed
	 */
	@Override
	public residences.model.ResidenceSlider4 deleteResidenceSlider4(
		residences.model.ResidenceSlider4 residenceSlider4) {

		return _residenceSlider4LocalService.deleteResidenceSlider4(
			residenceSlider4);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _residenceSlider4LocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _residenceSlider4LocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _residenceSlider4LocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _residenceSlider4LocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _residenceSlider4LocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _residenceSlider4LocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public residences.model.ResidenceSlider4 fetchResidenceSlider4(
		long slideId) {

		return _residenceSlider4LocalService.fetchResidenceSlider4(slideId);
	}

	/**
	 * Returns the residence slider4 with the matching UUID and company.
	 *
	 * @param uuid the residence slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider4, or <code>null</code> if a matching residence slider4 could not be found
	 */
	@Override
	public residences.model.ResidenceSlider4
		fetchResidenceSlider4ByUuidAndCompanyId(String uuid, long companyId) {

		return _residenceSlider4LocalService.
			fetchResidenceSlider4ByUuidAndCompanyId(uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _residenceSlider4LocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _residenceSlider4LocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _residenceSlider4LocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _residenceSlider4LocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider4LocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the residence slider4 with the primary key.
	 *
	 * @param slideId the primary key of the residence slider4
	 * @return the residence slider4
	 * @throws PortalException if a residence slider4 with the primary key could not be found
	 */
	@Override
	public residences.model.ResidenceSlider4 getResidenceSlider4(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider4LocalService.getResidenceSlider4(slideId);
	}

	/**
	 * Returns the residence slider4 with the matching UUID and company.
	 *
	 * @param uuid the residence slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider4
	 * @throws PortalException if a matching residence slider4 could not be found
	 */
	@Override
	public residences.model.ResidenceSlider4
			getResidenceSlider4ByUuidAndCompanyId(String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider4LocalService.
			getResidenceSlider4ByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the residence slider4s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider4s
	 * @param end the upper bound of the range of residence slider4s (not inclusive)
	 * @return the range of residence slider4s
	 */
	@Override
	public java.util.List<residences.model.ResidenceSlider4>
		getResidenceSlider4s(int start, int end) {

		return _residenceSlider4LocalService.getResidenceSlider4s(start, end);
	}

	/**
	 * Returns the number of residence slider4s.
	 *
	 * @return the number of residence slider4s
	 */
	@Override
	public int getResidenceSlider4sCount() {
		return _residenceSlider4LocalService.getResidenceSlider4sCount();
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider4>
		getResidenceSlider4Slides(long residenceId) {

		return _residenceSlider4LocalService.getResidenceSlider4Slides(
			residenceId);
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider4>
		getResidenceSlider4Slides(long residenceId, int start, int end) {

		return _residenceSlider4LocalService.getResidenceSlider4Slides(
			residenceId, start, end);
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider4>
		getResidenceSlider4Slides(
			long residenceId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<residences.model.ResidenceSlider4> obc) {

		return _residenceSlider4LocalService.getResidenceSlider4Slides(
			residenceId, start, end, obc);
	}

	@Override
	public int getResidenceSlider4SlidesCount(long residenceId) {
		return _residenceSlider4LocalService.getResidenceSlider4SlidesCount(
			residenceId);
	}

	@Override
	public residences.model.ResidenceSlider4 updateResidenceSlider4(
			long userId, long slideId, long residenceId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> descriptionMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _residenceSlider4LocalService.updateResidenceSlider4(
			userId, slideId, residenceId, titleMap, descriptionMap, imageMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Updates the residence slider4 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider4 the residence slider4
	 * @return the residence slider4 that was updated
	 */
	@Override
	public residences.model.ResidenceSlider4 updateResidenceSlider4(
		residences.model.ResidenceSlider4 residenceSlider4) {

		return _residenceSlider4LocalService.updateResidenceSlider4(
			residenceSlider4);
	}

	@Override
	public ResidenceSlider4LocalService getWrappedService() {
		return _residenceSlider4LocalService;
	}

	@Override
	public void setWrappedService(
		ResidenceSlider4LocalService residenceSlider4LocalService) {

		_residenceSlider4LocalService = residenceSlider4LocalService;
	}

	private ResidenceSlider4LocalService _residenceSlider4LocalService;

}