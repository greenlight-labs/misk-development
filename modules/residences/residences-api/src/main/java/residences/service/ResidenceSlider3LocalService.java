/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

import residences.exception.*;

import residences.model.ResidenceSlider3;

/**
 * Provides the local service interface for ResidenceSlider3. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3LocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface ResidenceSlider3LocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>residences.service.impl.ResidenceSlider3LocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the residence slider3 local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link ResidenceSlider3LocalServiceUtil} if injection and service tracking are not available.
	 */
	public ResidenceSlider3 addResidenceSlider3(
			long userId, long residenceId, Map<Locale, String> section3ImageMap,
			Map<Locale, String> section3DescriptionMap,
			Map<Locale, String> section3ThumbnailMap,
			ServiceContext serviceContext)
		throws PortalException;

	/**
	 * Adds the residence slider3 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public ResidenceSlider3 addResidenceSlider3(
		ResidenceSlider3 residenceSlider3);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Creates a new residence slider3 with the primary key. Does not add the residence slider3 to the database.
	 *
	 * @param slideId the primary key for the new residence slider3
	 * @return the new residence slider3
	 */
	@Transactional(enabled = false)
	public ResidenceSlider3 createResidenceSlider3(long slideId);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the residence slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3 that was removed
	 * @throws PortalException if a residence slider3 with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public ResidenceSlider3 deleteResidenceSlider3(long slideId)
		throws PortalException;

	public ResidenceSlider3 deleteResidenceSlider3(
			long slideId, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * Deletes the residence slider3 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public ResidenceSlider3 deleteResidenceSlider3(
		ResidenceSlider3 residenceSlider3);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ResidenceSlider3 fetchResidenceSlider3(long slideId);

	/**
	 * Returns the residence slider3 with the matching UUID and company.
	 *
	 * @param uuid the residence slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ResidenceSlider3 fetchResidenceSlider3ByUuidAndCompanyId(
		String uuid, long companyId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns the residence slider3 with the primary key.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3
	 * @throws PortalException if a residence slider3 with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ResidenceSlider3 getResidenceSlider3(long slideId)
		throws PortalException;

	/**
	 * Returns the residence slider3 with the matching UUID and company.
	 *
	 * @param uuid the residence slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider3
	 * @throws PortalException if a matching residence slider3 could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ResidenceSlider3 getResidenceSlider3ByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException;

	/**
	 * Returns a range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of residence slider3s
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ResidenceSlider3> getResidenceSlider3s(int start, int end);

	/**
	 * Returns the number of residence slider3s.
	 *
	 * @return the number of residence slider3s
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getResidenceSlider3sCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ResidenceSlider3> getResidenceSlider3Slides(long residenceId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ResidenceSlider3> getResidenceSlider3Slides(
		long residenceId, int start, int end,
		OrderByComparator<ResidenceSlider3> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getResidenceSlider3SlidesCount(long residenceId);

	public ResidenceSlider3 updateResidenceSlider3(
			long userId, long slideId, long residenceId,
			Map<Locale, String> section3ImageMap,
			Map<Locale, String> section3DescriptionMap,
			Map<Locale, String> section3ThumbnailMap,
			ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * Updates the residence slider3 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public ResidenceSlider3 updateResidenceSlider3(
		ResidenceSlider3 residenceSlider3);

}