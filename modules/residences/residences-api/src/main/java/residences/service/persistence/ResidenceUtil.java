/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import residences.model.Residence;

/**
 * The persistence utility for the residence service. This utility wraps <code>residences.service.persistence.impl.ResidencePersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidencePersistence
 * @generated
 */
public class ResidenceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Residence residence) {
		getPersistence().clearCache(residence);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Residence> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Residence> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Residence> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Residence> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Residence> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Residence update(Residence residence) {
		return getPersistence().update(residence);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Residence update(
		Residence residence, ServiceContext serviceContext) {

		return getPersistence().update(residence, serviceContext);
	}

	/**
	 * Returns all the residences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching residences
	 */
	public static List<Residence> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the residences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of matching residences
	 */
	public static List<Residence> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the residences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residences
	 */
	public static List<Residence> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Residence> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the residences where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residences
	 */
	public static List<Residence> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Residence> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public static Residence findByUuid_First(
			String uuid, OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByUuid_First(
		String uuid, OrderByComparator<Residence> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public static Residence findByUuid_Last(
			String uuid, OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last residence in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByUuid_Last(
		String uuid, OrderByComparator<Residence> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the residences before and after the current residence in the ordered set where uuid = &#63;.
	 *
	 * @param residenceId the primary key of the current residence
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public static Residence[] findByUuid_PrevAndNext(
			long residenceId, String uuid,
			OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByUuid_PrevAndNext(
			residenceId, uuid, orderByComparator);
	}

	/**
	 * Removes all the residences where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of residences where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching residences
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the residence where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchResidenceException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public static Residence findByUUID_G(String uuid, long groupId)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the residence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the residence where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the residence where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the residence that was removed
	 */
	public static Residence removeByUUID_G(String uuid, long groupId)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of residences where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching residences
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching residences
	 */
	public static List<Residence> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of matching residences
	 */
	public static List<Residence> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residences
	 */
	public static List<Residence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Residence> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the residences where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residences
	 */
	public static List<Residence> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Residence> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public static Residence findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Residence> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public static Residence findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Residence> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the residences before and after the current residence in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param residenceId the primary key of the current residence
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public static Residence[] findByUuid_C_PrevAndNext(
			long residenceId, String uuid, long companyId,
			OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByUuid_C_PrevAndNext(
			residenceId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the residences where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of residences where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching residences
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the residences where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching residences
	 */
	public static List<Residence> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the residences where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of matching residences
	 */
	public static List<Residence> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the residences where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residences
	 */
	public static List<Residence> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Residence> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the residences where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residences
	 */
	public static List<Residence> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Residence> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public static Residence findByGroupId_First(
			long groupId, OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByGroupId_First(
		long groupId, OrderByComparator<Residence> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence
	 * @throws NoSuchResidenceException if a matching residence could not be found
	 */
	public static Residence findByGroupId_Last(
			long groupId, OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last residence in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence, or <code>null</code> if a matching residence could not be found
	 */
	public static Residence fetchByGroupId_Last(
		long groupId, OrderByComparator<Residence> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the residences before and after the current residence in the ordered set where groupId = &#63;.
	 *
	 * @param residenceId the primary key of the current residence
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public static Residence[] findByGroupId_PrevAndNext(
			long residenceId, long groupId,
			OrderByComparator<Residence> orderByComparator)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByGroupId_PrevAndNext(
			residenceId, groupId, orderByComparator);
	}

	/**
	 * Removes all the residences where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of residences where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching residences
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Caches the residence in the entity cache if it is enabled.
	 *
	 * @param residence the residence
	 */
	public static void cacheResult(Residence residence) {
		getPersistence().cacheResult(residence);
	}

	/**
	 * Caches the residences in the entity cache if it is enabled.
	 *
	 * @param residences the residences
	 */
	public static void cacheResult(List<Residence> residences) {
		getPersistence().cacheResult(residences);
	}

	/**
	 * Creates a new residence with the primary key. Does not add the residence to the database.
	 *
	 * @param residenceId the primary key for the new residence
	 * @return the new residence
	 */
	public static Residence create(long residenceId) {
		return getPersistence().create(residenceId);
	}

	/**
	 * Removes the residence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence that was removed
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public static Residence remove(long residenceId)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().remove(residenceId);
	}

	public static Residence updateImpl(Residence residence) {
		return getPersistence().updateImpl(residence);
	}

	/**
	 * Returns the residence with the primary key or throws a <code>NoSuchResidenceException</code> if it could not be found.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence
	 * @throws NoSuchResidenceException if a residence with the primary key could not be found
	 */
	public static Residence findByPrimaryKey(long residenceId)
		throws residences.exception.NoSuchResidenceException {

		return getPersistence().findByPrimaryKey(residenceId);
	}

	/**
	 * Returns the residence with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence, or <code>null</code> if a residence with the primary key could not be found
	 */
	public static Residence fetchByPrimaryKey(long residenceId) {
		return getPersistence().fetchByPrimaryKey(residenceId);
	}

	/**
	 * Returns all the residences.
	 *
	 * @return the residences
	 */
	public static List<Residence> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of residences
	 */
	public static List<Residence> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of residences
	 */
	public static List<Residence> findAll(
		int start, int end, OrderByComparator<Residence> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of residences
	 */
	public static List<Residence> findAll(
		int start, int end, OrderByComparator<Residence> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the residences from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of residences.
	 *
	 * @return the number of residences
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ResidencePersistence getPersistence() {
		return _persistence;
	}

	private static volatile ResidencePersistence _persistence;

}