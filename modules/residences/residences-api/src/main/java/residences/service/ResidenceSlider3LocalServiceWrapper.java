/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResidenceSlider3LocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3LocalService
 * @generated
 */
public class ResidenceSlider3LocalServiceWrapper
	implements ResidenceSlider3LocalService,
			   ServiceWrapper<ResidenceSlider3LocalService> {

	public ResidenceSlider3LocalServiceWrapper(
		ResidenceSlider3LocalService residenceSlider3LocalService) {

		_residenceSlider3LocalService = residenceSlider3LocalService;
	}

	@Override
	public residences.model.ResidenceSlider3 addResidenceSlider3(
			long userId, long residenceId,
			java.util.Map<java.util.Locale, String> section3ImageMap,
			java.util.Map<java.util.Locale, String> section3DescriptionMap,
			java.util.Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider3LocalService.addResidenceSlider3(
			userId, residenceId, section3ImageMap, section3DescriptionMap,
			section3ThumbnailMap, serviceContext);
	}

	/**
	 * Adds the residence slider3 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was added
	 */
	@Override
	public residences.model.ResidenceSlider3 addResidenceSlider3(
		residences.model.ResidenceSlider3 residenceSlider3) {

		return _residenceSlider3LocalService.addResidenceSlider3(
			residenceSlider3);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider3LocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new residence slider3 with the primary key. Does not add the residence slider3 to the database.
	 *
	 * @param slideId the primary key for the new residence slider3
	 * @return the new residence slider3
	 */
	@Override
	public residences.model.ResidenceSlider3 createResidenceSlider3(
		long slideId) {

		return _residenceSlider3LocalService.createResidenceSlider3(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider3LocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the residence slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3 that was removed
	 * @throws PortalException if a residence slider3 with the primary key could not be found
	 */
	@Override
	public residences.model.ResidenceSlider3 deleteResidenceSlider3(
			long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider3LocalService.deleteResidenceSlider3(slideId);
	}

	@Override
	public residences.model.ResidenceSlider3 deleteResidenceSlider3(
			long slideId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _residenceSlider3LocalService.deleteResidenceSlider3(
			slideId, serviceContext);
	}

	/**
	 * Deletes the residence slider3 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was removed
	 */
	@Override
	public residences.model.ResidenceSlider3 deleteResidenceSlider3(
		residences.model.ResidenceSlider3 residenceSlider3) {

		return _residenceSlider3LocalService.deleteResidenceSlider3(
			residenceSlider3);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _residenceSlider3LocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _residenceSlider3LocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _residenceSlider3LocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _residenceSlider3LocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _residenceSlider3LocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _residenceSlider3LocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public residences.model.ResidenceSlider3 fetchResidenceSlider3(
		long slideId) {

		return _residenceSlider3LocalService.fetchResidenceSlider3(slideId);
	}

	/**
	 * Returns the residence slider3 with the matching UUID and company.
	 *
	 * @param uuid the residence slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Override
	public residences.model.ResidenceSlider3
		fetchResidenceSlider3ByUuidAndCompanyId(String uuid, long companyId) {

		return _residenceSlider3LocalService.
			fetchResidenceSlider3ByUuidAndCompanyId(uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _residenceSlider3LocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _residenceSlider3LocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _residenceSlider3LocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _residenceSlider3LocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider3LocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the residence slider3 with the primary key.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3
	 * @throws PortalException if a residence slider3 with the primary key could not be found
	 */
	@Override
	public residences.model.ResidenceSlider3 getResidenceSlider3(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider3LocalService.getResidenceSlider3(slideId);
	}

	/**
	 * Returns the residence slider3 with the matching UUID and company.
	 *
	 * @param uuid the residence slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider3
	 * @throws PortalException if a matching residence slider3 could not be found
	 */
	@Override
	public residences.model.ResidenceSlider3
			getResidenceSlider3ByUuidAndCompanyId(String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _residenceSlider3LocalService.
			getResidenceSlider3ByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of residence slider3s
	 */
	@Override
	public java.util.List<residences.model.ResidenceSlider3>
		getResidenceSlider3s(int start, int end) {

		return _residenceSlider3LocalService.getResidenceSlider3s(start, end);
	}

	/**
	 * Returns the number of residence slider3s.
	 *
	 * @return the number of residence slider3s
	 */
	@Override
	public int getResidenceSlider3sCount() {
		return _residenceSlider3LocalService.getResidenceSlider3sCount();
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider3>
		getResidenceSlider3Slides(long residenceId) {

		return _residenceSlider3LocalService.getResidenceSlider3Slides(
			residenceId);
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider3>
		getResidenceSlider3Slides(long residenceId, int start, int end) {

		return _residenceSlider3LocalService.getResidenceSlider3Slides(
			residenceId, start, end);
	}

	@Override
	public java.util.List<residences.model.ResidenceSlider3>
		getResidenceSlider3Slides(
			long residenceId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<residences.model.ResidenceSlider3> obc) {

		return _residenceSlider3LocalService.getResidenceSlider3Slides(
			residenceId, start, end, obc);
	}

	@Override
	public int getResidenceSlider3SlidesCount(long residenceId) {
		return _residenceSlider3LocalService.getResidenceSlider3SlidesCount(
			residenceId);
	}

	@Override
	public residences.model.ResidenceSlider3 updateResidenceSlider3(
			long userId, long slideId, long residenceId,
			java.util.Map<java.util.Locale, String> section3ImageMap,
			java.util.Map<java.util.Locale, String> section3DescriptionMap,
			java.util.Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _residenceSlider3LocalService.updateResidenceSlider3(
			userId, slideId, residenceId, section3ImageMap,
			section3DescriptionMap, section3ThumbnailMap, serviceContext);
	}

	/**
	 * Updates the residence slider3 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider3 the residence slider3
	 * @return the residence slider3 that was updated
	 */
	@Override
	public residences.model.ResidenceSlider3 updateResidenceSlider3(
		residences.model.ResidenceSlider3 residenceSlider3) {

		return _residenceSlider3LocalService.updateResidenceSlider3(
			residenceSlider3);
	}

	@Override
	public ResidenceSlider3LocalService getWrappedService() {
		return _residenceSlider3LocalService;
	}

	@Override
	public void setWrappedService(
		ResidenceSlider3LocalService residenceSlider3LocalService) {

		_residenceSlider3LocalService = residenceSlider3LocalService;
	}

	private ResidenceSlider3LocalService _residenceSlider3LocalService;

}