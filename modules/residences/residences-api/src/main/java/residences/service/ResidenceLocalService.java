/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

import residences.model.Residence;

/**
 * Provides the local service interface for Residence. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface ResidenceLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>residences.service.impl.ResidenceLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the residence local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link ResidenceLocalServiceUtil} if injection and service tracking are not available.
	 */
	public Residence addResidence(
			long userId, long residenceId, Map<Locale, String> listingImageMap,
			Map<Locale, String> listingTitleMap,
			Map<Locale, String> listingDescriptionMap,
			Map<Locale, String> bannerTitleMap,
			Map<Locale, String> bannerSubtitleMap,
			Map<Locale, String> bannerDescriptionMap,
			Map<Locale, String> bannerDesktopImageMap,
			Map<Locale, String> bannerMobileImageMap,
			Map<Locale, String> section2TitleMap,
			Map<Locale, String> section2SubtitleMap,
			Map<Locale, String> section2DescriptionMap,
			Map<Locale, String> section2ButtonLabelMap,
			Map<Locale, String> section2ButtonLinkMap,
			Map<Locale, String> section2ImageMap,
			Map<Locale, String> section5TitleMap,
			Map<Locale, String> section5SubtitleMap,
			Map<Locale, String> section5DescriptionMap,
			Map<Locale, String> section5ButtonLabelMap,
			Map<Locale, String> section5ButtonLinkMap,
			Map<Locale, String> section5ImageMap,
			Map<Locale, String> section6TitleMap,
			Map<Locale, String> section6SubtitleMap,
			Map<Locale, String> section6DescriptionMap,
			Map<Locale, String> section6ButtonLabelMap,
			Map<Locale, String> section6ButtonLinkMap,
			Map<Locale, String> section6ImageMap,
			Map<Locale, String> section4SectionTitleMap,
			ServiceContext serviceContext)
		throws PortalException;

	/**
	 * Adds the residence to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Residence addResidence(Residence residence);

	public int countByT_D(String query, String languageId);

	public int countSearchCompleteWebsite(String query, String languageId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Creates a new residence with the primary key. Does not add the residence to the database.
	 *
	 * @param residenceId the primary key for the new residence
	 * @return the new residence
	 */
	@Transactional(enabled = false)
	public Residence createResidence(long residenceId);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the residence with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence that was removed
	 * @throws PortalException if a residence with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Residence deleteResidence(long residenceId) throws PortalException;

	public Residence deleteResidence(
			long residenceId, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * Deletes the residence from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Residence deleteResidence(Residence residence);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Residence fetchResidence(long residenceId);

	/**
	 * Returns the residence matching the UUID and group.
	 *
	 * @param uuid the residence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching residence, or <code>null</code> if a matching residence could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Residence fetchResidenceByUuidAndGroupId(String uuid, long groupId);

	public List<Residence> findByT_D(
		String query, int offset, int limit, String languageId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns the residence with the primary key.
	 *
	 * @param residenceId the primary key of the residence
	 * @return the residence
	 * @throws PortalException if a residence with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Residence getResidence(long residenceId) throws PortalException;

	/**
	 * Returns the residence matching the UUID and group.
	 *
	 * @param uuid the residence's UUID
	 * @param groupId the primary key of the group
	 * @return the matching residence
	 * @throws PortalException if a matching residence could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Residence getResidenceByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the residences.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @return the range of residences
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Residence> getResidences(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Residence> getResidences(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Residence> getResidences(long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Residence> getResidences(
		long groupId, int start, int end, OrderByComparator<Residence> obc);

	/**
	 * Returns all the residences matching the UUID and company.
	 *
	 * @param uuid the UUID of the residences
	 * @param companyId the primary key of the company
	 * @return the matching residences, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Residence> getResidencesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of residences matching the UUID and company.
	 *
	 * @param uuid the UUID of the residences
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of residences
	 * @param end the upper bound of the range of residences (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching residences, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Residence> getResidencesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Residence> orderByComparator);

	/**
	 * Returns the number of residences.
	 *
	 * @return the number of residences
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getResidencesCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getResidencesCount(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Object> searchCompleteWebsite(
		String query, int offset, int limit, String languageId);

	public Residence updateResidence(
			long userId, long residenceId, Map<Locale, String> listingImageMap,
			Map<Locale, String> listingTitleMap,
			Map<Locale, String> listingDescriptionMap,
			Map<Locale, String> bannerTitleMap,
			Map<Locale, String> bannerSubtitleMap,
			Map<Locale, String> bannerDescriptionMap,
			Map<Locale, String> bannerDesktopImageMap,
			Map<Locale, String> bannerMobileImageMap,
			Map<Locale, String> section2TitleMap,
			Map<Locale, String> section2SubtitleMap,
			Map<Locale, String> section2DescriptionMap,
			Map<Locale, String> section2ButtonLabelMap,
			Map<Locale, String> section2ButtonLinkMap,
			Map<Locale, String> section2ImageMap,
			Map<Locale, String> section5TitleMap,
			Map<Locale, String> section5SubtitleMap,
			Map<Locale, String> section5DescriptionMap,
			Map<Locale, String> section5ButtonLabelMap,
			Map<Locale, String> section5ButtonLinkMap,
			Map<Locale, String> section5ImageMap,
			Map<Locale, String> section6TitleMap,
			Map<Locale, String> section6SubtitleMap,
			Map<Locale, String> section6DescriptionMap,
			Map<Locale, String> section6ButtonLabelMap,
			Map<Locale, String> section6ButtonLinkMap,
			Map<Locale, String> section6ImageMap,
			Map<Locale, String> section4SectionTitleMap,
			ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * Updates the residence in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residence the residence
	 * @return the residence that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Residence updateResidence(Residence residence);

}