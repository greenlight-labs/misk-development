/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ResidenceSlider4Service}.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider4Service
 * @generated
 */
public class ResidenceSlider4ServiceWrapper
	implements ResidenceSlider4Service,
			   ServiceWrapper<ResidenceSlider4Service> {

	public ResidenceSlider4ServiceWrapper(
		ResidenceSlider4Service residenceSlider4Service) {

		_residenceSlider4Service = residenceSlider4Service;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _residenceSlider4Service.getOSGiServiceIdentifier();
	}

	@Override
	public ResidenceSlider4Service getWrappedService() {
		return _residenceSlider4Service;
	}

	@Override
	public void setWrappedService(
		ResidenceSlider4Service residenceSlider4Service) {

		_residenceSlider4Service = residenceSlider4Service;
	}

	private ResidenceSlider4Service _residenceSlider4Service;

}