/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link residences.service.http.ResidenceSlider3ServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class ResidenceSlider3Soap implements Serializable {

	public static ResidenceSlider3Soap toSoapModel(ResidenceSlider3 model) {
		ResidenceSlider3Soap soapModel = new ResidenceSlider3Soap();

		soapModel.setUuid(model.getUuid());
		soapModel.setSlideId(model.getSlideId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setResidenceId(model.getResidenceId());
		soapModel.setImage(model.getImage());
		soapModel.setDescription(model.getDescription());
		soapModel.setThumbnail(model.getThumbnail());

		return soapModel;
	}

	public static ResidenceSlider3Soap[] toSoapModels(
		ResidenceSlider3[] models) {

		ResidenceSlider3Soap[] soapModels =
			new ResidenceSlider3Soap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ResidenceSlider3Soap[][] toSoapModels(
		ResidenceSlider3[][] models) {

		ResidenceSlider3Soap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new ResidenceSlider3Soap[models.length][models[0].length];
		}
		else {
			soapModels = new ResidenceSlider3Soap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ResidenceSlider3Soap[] toSoapModels(
		List<ResidenceSlider3> models) {

		List<ResidenceSlider3Soap> soapModels =
			new ArrayList<ResidenceSlider3Soap>(models.size());

		for (ResidenceSlider3 model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ResidenceSlider3Soap[soapModels.size()]);
	}

	public ResidenceSlider3Soap() {
	}

	public long getPrimaryKey() {
		return _slideId;
	}

	public void setPrimaryKey(long pk) {
		setSlideId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getSlideId() {
		return _slideId;
	}

	public void setSlideId(long slideId) {
		_slideId = slideId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getResidenceId() {
		return _residenceId;
	}

	public void setResidenceId(long residenceId) {
		_residenceId = residenceId;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getThumbnail() {
		return _thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		_thumbnail = thumbnail;
	}

	private String _uuid;
	private long _slideId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _residenceId;
	private String _image;
	private String _description;
	private String _thumbnail;

}