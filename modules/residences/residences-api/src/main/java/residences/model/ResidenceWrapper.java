/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Residence}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Residence
 * @generated
 */
public class ResidenceWrapper
	extends BaseModelWrapper<Residence>
	implements ModelWrapper<Residence>, Residence {

	public ResidenceWrapper(Residence residence) {
		super(residence);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("residenceId", getResidenceId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("listingImage", getListingImage());
		attributes.put("listingTitle", getListingTitle());
		attributes.put("listingDescription", getListingDescription());
		attributes.put("bannerTitle", getBannerTitle());
		attributes.put("bannerSubtitle", getBannerSubtitle());
		attributes.put("bannerDescription", getBannerDescription());
		attributes.put("bannerDesktopImage", getBannerDesktopImage());
		attributes.put("bannerMobileImage", getBannerMobileImage());
		attributes.put("section2Title", getSection2Title());
		attributes.put("section2Subtitle", getSection2Subtitle());
		attributes.put("section2Description", getSection2Description());
		attributes.put("section2ButtonLabel", getSection2ButtonLabel());
		attributes.put("section2ButtonLink", getSection2ButtonLink());
		attributes.put("section2Image", getSection2Image());
		attributes.put("section5Title", getSection5Title());
		attributes.put("section5Subtitle", getSection5Subtitle());
		attributes.put("section5Description", getSection5Description());
		attributes.put("section5ButtonLabel", getSection5ButtonLabel());
		attributes.put("section5ButtonLink", getSection5ButtonLink());
		attributes.put("section5Image", getSection5Image());
		attributes.put("section6Title", getSection6Title());
		attributes.put("section6Subtitle", getSection6Subtitle());
		attributes.put("section6Description", getSection6Description());
		attributes.put("section6ButtonLabel", getSection6ButtonLabel());
		attributes.put("section6ButtonLink", getSection6ButtonLink());
		attributes.put("section6Image", getSection6Image());
		attributes.put("section4SectionTitle", getSection4SectionTitle());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long residenceId = (Long)attributes.get("residenceId");

		if (residenceId != null) {
			setResidenceId(residenceId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String listingImage = (String)attributes.get("listingImage");

		if (listingImage != null) {
			setListingImage(listingImage);
		}

		String listingTitle = (String)attributes.get("listingTitle");

		if (listingTitle != null) {
			setListingTitle(listingTitle);
		}

		String listingDescription = (String)attributes.get(
			"listingDescription");

		if (listingDescription != null) {
			setListingDescription(listingDescription);
		}

		String bannerTitle = (String)attributes.get("bannerTitle");

		if (bannerTitle != null) {
			setBannerTitle(bannerTitle);
		}

		String bannerSubtitle = (String)attributes.get("bannerSubtitle");

		if (bannerSubtitle != null) {
			setBannerSubtitle(bannerSubtitle);
		}

		String bannerDescription = (String)attributes.get("bannerDescription");

		if (bannerDescription != null) {
			setBannerDescription(bannerDescription);
		}

		String bannerDesktopImage = (String)attributes.get(
			"bannerDesktopImage");

		if (bannerDesktopImage != null) {
			setBannerDesktopImage(bannerDesktopImage);
		}

		String bannerMobileImage = (String)attributes.get("bannerMobileImage");

		if (bannerMobileImage != null) {
			setBannerMobileImage(bannerMobileImage);
		}

		String section2Title = (String)attributes.get("section2Title");

		if (section2Title != null) {
			setSection2Title(section2Title);
		}

		String section2Subtitle = (String)attributes.get("section2Subtitle");

		if (section2Subtitle != null) {
			setSection2Subtitle(section2Subtitle);
		}

		String section2Description = (String)attributes.get(
			"section2Description");

		if (section2Description != null) {
			setSection2Description(section2Description);
		}

		String section2ButtonLabel = (String)attributes.get(
			"section2ButtonLabel");

		if (section2ButtonLabel != null) {
			setSection2ButtonLabel(section2ButtonLabel);
		}

		String section2ButtonLink = (String)attributes.get(
			"section2ButtonLink");

		if (section2ButtonLink != null) {
			setSection2ButtonLink(section2ButtonLink);
		}

		String section2Image = (String)attributes.get("section2Image");

		if (section2Image != null) {
			setSection2Image(section2Image);
		}

		String section5Title = (String)attributes.get("section5Title");

		if (section5Title != null) {
			setSection5Title(section5Title);
		}

		String section5Subtitle = (String)attributes.get("section5Subtitle");

		if (section5Subtitle != null) {
			setSection5Subtitle(section5Subtitle);
		}

		String section5Description = (String)attributes.get(
			"section5Description");

		if (section5Description != null) {
			setSection5Description(section5Description);
		}

		String section5ButtonLabel = (String)attributes.get(
			"section5ButtonLabel");

		if (section5ButtonLabel != null) {
			setSection5ButtonLabel(section5ButtonLabel);
		}

		String section5ButtonLink = (String)attributes.get(
			"section5ButtonLink");

		if (section5ButtonLink != null) {
			setSection5ButtonLink(section5ButtonLink);
		}

		String section5Image = (String)attributes.get("section5Image");

		if (section5Image != null) {
			setSection5Image(section5Image);
		}

		String section6Title = (String)attributes.get("section6Title");

		if (section6Title != null) {
			setSection6Title(section6Title);
		}

		String section6Subtitle = (String)attributes.get("section6Subtitle");

		if (section6Subtitle != null) {
			setSection6Subtitle(section6Subtitle);
		}

		String section6Description = (String)attributes.get(
			"section6Description");

		if (section6Description != null) {
			setSection6Description(section6Description);
		}

		String section6ButtonLabel = (String)attributes.get(
			"section6ButtonLabel");

		if (section6ButtonLabel != null) {
			setSection6ButtonLabel(section6ButtonLabel);
		}

		String section6ButtonLink = (String)attributes.get(
			"section6ButtonLink");

		if (section6ButtonLink != null) {
			setSection6ButtonLink(section6ButtonLink);
		}

		String section6Image = (String)attributes.get("section6Image");

		if (section6Image != null) {
			setSection6Image(section6Image);
		}

		String section4SectionTitle = (String)attributes.get(
			"section4SectionTitle");

		if (section4SectionTitle != null) {
			setSection4SectionTitle(section4SectionTitle);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the banner description of this residence.
	 *
	 * @return the banner description of this residence
	 */
	@Override
	public String getBannerDescription() {
		return model.getBannerDescription();
	}

	/**
	 * Returns the localized banner description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner description of this residence
	 */
	@Override
	public String getBannerDescription(java.util.Locale locale) {
		return model.getBannerDescription(locale);
	}

	/**
	 * Returns the localized banner description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner description of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerDescription(
		java.util.Locale locale, boolean useDefault) {

		return model.getBannerDescription(locale, useDefault);
	}

	/**
	 * Returns the localized banner description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner description of this residence
	 */
	@Override
	public String getBannerDescription(String languageId) {
		return model.getBannerDescription(languageId);
	}

	/**
	 * Returns the localized banner description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner description of this residence
	 */
	@Override
	public String getBannerDescription(String languageId, boolean useDefault) {
		return model.getBannerDescription(languageId, useDefault);
	}

	@Override
	public String getBannerDescriptionCurrentLanguageId() {
		return model.getBannerDescriptionCurrentLanguageId();
	}

	@Override
	public String getBannerDescriptionCurrentValue() {
		return model.getBannerDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner descriptions of this residence.
	 *
	 * @return the locales and localized banner descriptions of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getBannerDescriptionMap() {
		return model.getBannerDescriptionMap();
	}

	/**
	 * Returns the banner desktop image of this residence.
	 *
	 * @return the banner desktop image of this residence
	 */
	@Override
	public String getBannerDesktopImage() {
		return model.getBannerDesktopImage();
	}

	/**
	 * Returns the localized banner desktop image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner desktop image of this residence
	 */
	@Override
	public String getBannerDesktopImage(java.util.Locale locale) {
		return model.getBannerDesktopImage(locale);
	}

	/**
	 * Returns the localized banner desktop image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner desktop image of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerDesktopImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getBannerDesktopImage(locale, useDefault);
	}

	/**
	 * Returns the localized banner desktop image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner desktop image of this residence
	 */
	@Override
	public String getBannerDesktopImage(String languageId) {
		return model.getBannerDesktopImage(languageId);
	}

	/**
	 * Returns the localized banner desktop image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner desktop image of this residence
	 */
	@Override
	public String getBannerDesktopImage(String languageId, boolean useDefault) {
		return model.getBannerDesktopImage(languageId, useDefault);
	}

	@Override
	public String getBannerDesktopImageCurrentLanguageId() {
		return model.getBannerDesktopImageCurrentLanguageId();
	}

	@Override
	public String getBannerDesktopImageCurrentValue() {
		return model.getBannerDesktopImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner desktop images of this residence.
	 *
	 * @return the locales and localized banner desktop images of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getBannerDesktopImageMap() {
		return model.getBannerDesktopImageMap();
	}

	/**
	 * Returns the banner mobile image of this residence.
	 *
	 * @return the banner mobile image of this residence
	 */
	@Override
	public String getBannerMobileImage() {
		return model.getBannerMobileImage();
	}

	/**
	 * Returns the localized banner mobile image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner mobile image of this residence
	 */
	@Override
	public String getBannerMobileImage(java.util.Locale locale) {
		return model.getBannerMobileImage(locale);
	}

	/**
	 * Returns the localized banner mobile image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner mobile image of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerMobileImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getBannerMobileImage(locale, useDefault);
	}

	/**
	 * Returns the localized banner mobile image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner mobile image of this residence
	 */
	@Override
	public String getBannerMobileImage(String languageId) {
		return model.getBannerMobileImage(languageId);
	}

	/**
	 * Returns the localized banner mobile image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner mobile image of this residence
	 */
	@Override
	public String getBannerMobileImage(String languageId, boolean useDefault) {
		return model.getBannerMobileImage(languageId, useDefault);
	}

	@Override
	public String getBannerMobileImageCurrentLanguageId() {
		return model.getBannerMobileImageCurrentLanguageId();
	}

	@Override
	public String getBannerMobileImageCurrentValue() {
		return model.getBannerMobileImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner mobile images of this residence.
	 *
	 * @return the locales and localized banner mobile images of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getBannerMobileImageMap() {
		return model.getBannerMobileImageMap();
	}

	/**
	 * Returns the banner subtitle of this residence.
	 *
	 * @return the banner subtitle of this residence
	 */
	@Override
	public String getBannerSubtitle() {
		return model.getBannerSubtitle();
	}

	/**
	 * Returns the localized banner subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner subtitle of this residence
	 */
	@Override
	public String getBannerSubtitle(java.util.Locale locale) {
		return model.getBannerSubtitle(locale);
	}

	/**
	 * Returns the localized banner subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner subtitle of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerSubtitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getBannerSubtitle(locale, useDefault);
	}

	/**
	 * Returns the localized banner subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner subtitle of this residence
	 */
	@Override
	public String getBannerSubtitle(String languageId) {
		return model.getBannerSubtitle(languageId);
	}

	/**
	 * Returns the localized banner subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner subtitle of this residence
	 */
	@Override
	public String getBannerSubtitle(String languageId, boolean useDefault) {
		return model.getBannerSubtitle(languageId, useDefault);
	}

	@Override
	public String getBannerSubtitleCurrentLanguageId() {
		return model.getBannerSubtitleCurrentLanguageId();
	}

	@Override
	public String getBannerSubtitleCurrentValue() {
		return model.getBannerSubtitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner subtitles of this residence.
	 *
	 * @return the locales and localized banner subtitles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getBannerSubtitleMap() {
		return model.getBannerSubtitleMap();
	}

	/**
	 * Returns the banner title of this residence.
	 *
	 * @return the banner title of this residence
	 */
	@Override
	public String getBannerTitle() {
		return model.getBannerTitle();
	}

	/**
	 * Returns the localized banner title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner title of this residence
	 */
	@Override
	public String getBannerTitle(java.util.Locale locale) {
		return model.getBannerTitle(locale);
	}

	/**
	 * Returns the localized banner title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner title of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerTitle(java.util.Locale locale, boolean useDefault) {
		return model.getBannerTitle(locale, useDefault);
	}

	/**
	 * Returns the localized banner title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner title of this residence
	 */
	@Override
	public String getBannerTitle(String languageId) {
		return model.getBannerTitle(languageId);
	}

	/**
	 * Returns the localized banner title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner title of this residence
	 */
	@Override
	public String getBannerTitle(String languageId, boolean useDefault) {
		return model.getBannerTitle(languageId, useDefault);
	}

	@Override
	public String getBannerTitleCurrentLanguageId() {
		return model.getBannerTitleCurrentLanguageId();
	}

	@Override
	public String getBannerTitleCurrentValue() {
		return model.getBannerTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner titles of this residence.
	 *
	 * @return the locales and localized banner titles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getBannerTitleMap() {
		return model.getBannerTitleMap();
	}

	/**
	 * Returns the company ID of this residence.
	 *
	 * @return the company ID of this residence
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this residence.
	 *
	 * @return the create date of this residence
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the group ID of this residence.
	 *
	 * @return the group ID of this residence
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the listing description of this residence.
	 *
	 * @return the listing description of this residence
	 */
	@Override
	public String getListingDescription() {
		return model.getListingDescription();
	}

	/**
	 * Returns the localized listing description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing description of this residence
	 */
	@Override
	public String getListingDescription(java.util.Locale locale) {
		return model.getListingDescription(locale);
	}

	/**
	 * Returns the localized listing description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingDescription(
		java.util.Locale locale, boolean useDefault) {

		return model.getListingDescription(locale, useDefault);
	}

	/**
	 * Returns the localized listing description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing description of this residence
	 */
	@Override
	public String getListingDescription(String languageId) {
		return model.getListingDescription(languageId);
	}

	/**
	 * Returns the localized listing description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this residence
	 */
	@Override
	public String getListingDescription(String languageId, boolean useDefault) {
		return model.getListingDescription(languageId, useDefault);
	}

	@Override
	public String getListingDescriptionCurrentLanguageId() {
		return model.getListingDescriptionCurrentLanguageId();
	}

	@Override
	public String getListingDescriptionCurrentValue() {
		return model.getListingDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing descriptions of this residence.
	 *
	 * @return the locales and localized listing descriptions of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getListingDescriptionMap() {
		return model.getListingDescriptionMap();
	}

	/**
	 * Returns the listing image of this residence.
	 *
	 * @return the listing image of this residence
	 */
	@Override
	public String getListingImage() {
		return model.getListingImage();
	}

	/**
	 * Returns the localized listing image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing image of this residence
	 */
	@Override
	public String getListingImage(java.util.Locale locale) {
		return model.getListingImage(locale);
	}

	/**
	 * Returns the localized listing image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingImage(java.util.Locale locale, boolean useDefault) {
		return model.getListingImage(locale, useDefault);
	}

	/**
	 * Returns the localized listing image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing image of this residence
	 */
	@Override
	public String getListingImage(String languageId) {
		return model.getListingImage(languageId);
	}

	/**
	 * Returns the localized listing image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this residence
	 */
	@Override
	public String getListingImage(String languageId, boolean useDefault) {
		return model.getListingImage(languageId, useDefault);
	}

	@Override
	public String getListingImageCurrentLanguageId() {
		return model.getListingImageCurrentLanguageId();
	}

	@Override
	public String getListingImageCurrentValue() {
		return model.getListingImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing images of this residence.
	 *
	 * @return the locales and localized listing images of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getListingImageMap() {
		return model.getListingImageMap();
	}

	/**
	 * Returns the listing title of this residence.
	 *
	 * @return the listing title of this residence
	 */
	@Override
	public String getListingTitle() {
		return model.getListingTitle();
	}

	/**
	 * Returns the localized listing title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing title of this residence
	 */
	@Override
	public String getListingTitle(java.util.Locale locale) {
		return model.getListingTitle(locale);
	}

	/**
	 * Returns the localized listing title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingTitle(java.util.Locale locale, boolean useDefault) {
		return model.getListingTitle(locale, useDefault);
	}

	/**
	 * Returns the localized listing title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing title of this residence
	 */
	@Override
	public String getListingTitle(String languageId) {
		return model.getListingTitle(languageId);
	}

	/**
	 * Returns the localized listing title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this residence
	 */
	@Override
	public String getListingTitle(String languageId, boolean useDefault) {
		return model.getListingTitle(languageId, useDefault);
	}

	@Override
	public String getListingTitleCurrentLanguageId() {
		return model.getListingTitleCurrentLanguageId();
	}

	@Override
	public String getListingTitleCurrentValue() {
		return model.getListingTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing titles of this residence.
	 *
	 * @return the locales and localized listing titles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getListingTitleMap() {
		return model.getListingTitleMap();
	}

	/**
	 * Returns the modified date of this residence.
	 *
	 * @return the modified date of this residence
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this residence.
	 *
	 * @return the primary key of this residence
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the residence ID of this residence.
	 *
	 * @return the residence ID of this residence
	 */
	@Override
	public long getResidenceId() {
		return model.getResidenceId();
	}

	/**
	 * Returns the section2 button label of this residence.
	 *
	 * @return the section2 button label of this residence
	 */
	@Override
	public String getSection2ButtonLabel() {
		return model.getSection2ButtonLabel();
	}

	/**
	 * Returns the localized section2 button label of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 button label of this residence
	 */
	@Override
	public String getSection2ButtonLabel(java.util.Locale locale) {
		return model.getSection2ButtonLabel(locale);
	}

	/**
	 * Returns the localized section2 button label of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 button label of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2ButtonLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2ButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized section2 button label of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 button label of this residence
	 */
	@Override
	public String getSection2ButtonLabel(String languageId) {
		return model.getSection2ButtonLabel(languageId);
	}

	/**
	 * Returns the localized section2 button label of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 button label of this residence
	 */
	@Override
	public String getSection2ButtonLabel(
		String languageId, boolean useDefault) {

		return model.getSection2ButtonLabel(languageId, useDefault);
	}

	@Override
	public String getSection2ButtonLabelCurrentLanguageId() {
		return model.getSection2ButtonLabelCurrentLanguageId();
	}

	@Override
	public String getSection2ButtonLabelCurrentValue() {
		return model.getSection2ButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 button labels of this residence.
	 *
	 * @return the locales and localized section2 button labels of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection2ButtonLabelMap() {
		return model.getSection2ButtonLabelMap();
	}

	/**
	 * Returns the section2 button link of this residence.
	 *
	 * @return the section2 button link of this residence
	 */
	@Override
	public String getSection2ButtonLink() {
		return model.getSection2ButtonLink();
	}

	/**
	 * Returns the localized section2 button link of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 button link of this residence
	 */
	@Override
	public String getSection2ButtonLink(java.util.Locale locale) {
		return model.getSection2ButtonLink(locale);
	}

	/**
	 * Returns the localized section2 button link of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 button link of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2ButtonLink(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2ButtonLink(locale, useDefault);
	}

	/**
	 * Returns the localized section2 button link of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 button link of this residence
	 */
	@Override
	public String getSection2ButtonLink(String languageId) {
		return model.getSection2ButtonLink(languageId);
	}

	/**
	 * Returns the localized section2 button link of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 button link of this residence
	 */
	@Override
	public String getSection2ButtonLink(String languageId, boolean useDefault) {
		return model.getSection2ButtonLink(languageId, useDefault);
	}

	@Override
	public String getSection2ButtonLinkCurrentLanguageId() {
		return model.getSection2ButtonLinkCurrentLanguageId();
	}

	@Override
	public String getSection2ButtonLinkCurrentValue() {
		return model.getSection2ButtonLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 button links of this residence.
	 *
	 * @return the locales and localized section2 button links of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection2ButtonLinkMap() {
		return model.getSection2ButtonLinkMap();
	}

	/**
	 * Returns the section2 description of this residence.
	 *
	 * @return the section2 description of this residence
	 */
	@Override
	public String getSection2Description() {
		return model.getSection2Description();
	}

	/**
	 * Returns the localized section2 description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 description of this residence
	 */
	@Override
	public String getSection2Description(java.util.Locale locale) {
		return model.getSection2Description(locale);
	}

	/**
	 * Returns the localized section2 description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 description of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2Description(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2Description(locale, useDefault);
	}

	/**
	 * Returns the localized section2 description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 description of this residence
	 */
	@Override
	public String getSection2Description(String languageId) {
		return model.getSection2Description(languageId);
	}

	/**
	 * Returns the localized section2 description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 description of this residence
	 */
	@Override
	public String getSection2Description(
		String languageId, boolean useDefault) {

		return model.getSection2Description(languageId, useDefault);
	}

	@Override
	public String getSection2DescriptionCurrentLanguageId() {
		return model.getSection2DescriptionCurrentLanguageId();
	}

	@Override
	public String getSection2DescriptionCurrentValue() {
		return model.getSection2DescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 descriptions of this residence.
	 *
	 * @return the locales and localized section2 descriptions of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection2DescriptionMap() {
		return model.getSection2DescriptionMap();
	}

	/**
	 * Returns the section2 image of this residence.
	 *
	 * @return the section2 image of this residence
	 */
	@Override
	public String getSection2Image() {
		return model.getSection2Image();
	}

	/**
	 * Returns the localized section2 image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 image of this residence
	 */
	@Override
	public String getSection2Image(java.util.Locale locale) {
		return model.getSection2Image(locale);
	}

	/**
	 * Returns the localized section2 image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 image of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2Image(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2Image(locale, useDefault);
	}

	/**
	 * Returns the localized section2 image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 image of this residence
	 */
	@Override
	public String getSection2Image(String languageId) {
		return model.getSection2Image(languageId);
	}

	/**
	 * Returns the localized section2 image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 image of this residence
	 */
	@Override
	public String getSection2Image(String languageId, boolean useDefault) {
		return model.getSection2Image(languageId, useDefault);
	}

	@Override
	public String getSection2ImageCurrentLanguageId() {
		return model.getSection2ImageCurrentLanguageId();
	}

	@Override
	public String getSection2ImageCurrentValue() {
		return model.getSection2ImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 images of this residence.
	 *
	 * @return the locales and localized section2 images of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection2ImageMap() {
		return model.getSection2ImageMap();
	}

	/**
	 * Returns the section2 subtitle of this residence.
	 *
	 * @return the section2 subtitle of this residence
	 */
	@Override
	public String getSection2Subtitle() {
		return model.getSection2Subtitle();
	}

	/**
	 * Returns the localized section2 subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 subtitle of this residence
	 */
	@Override
	public String getSection2Subtitle(java.util.Locale locale) {
		return model.getSection2Subtitle(locale);
	}

	/**
	 * Returns the localized section2 subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 subtitle of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2Subtitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2Subtitle(locale, useDefault);
	}

	/**
	 * Returns the localized section2 subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 subtitle of this residence
	 */
	@Override
	public String getSection2Subtitle(String languageId) {
		return model.getSection2Subtitle(languageId);
	}

	/**
	 * Returns the localized section2 subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 subtitle of this residence
	 */
	@Override
	public String getSection2Subtitle(String languageId, boolean useDefault) {
		return model.getSection2Subtitle(languageId, useDefault);
	}

	@Override
	public String getSection2SubtitleCurrentLanguageId() {
		return model.getSection2SubtitleCurrentLanguageId();
	}

	@Override
	public String getSection2SubtitleCurrentValue() {
		return model.getSection2SubtitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 subtitles of this residence.
	 *
	 * @return the locales and localized section2 subtitles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection2SubtitleMap() {
		return model.getSection2SubtitleMap();
	}

	/**
	 * Returns the section2 title of this residence.
	 *
	 * @return the section2 title of this residence
	 */
	@Override
	public String getSection2Title() {
		return model.getSection2Title();
	}

	/**
	 * Returns the localized section2 title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 title of this residence
	 */
	@Override
	public String getSection2Title(java.util.Locale locale) {
		return model.getSection2Title(locale);
	}

	/**
	 * Returns the localized section2 title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 title of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2Title(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2Title(locale, useDefault);
	}

	/**
	 * Returns the localized section2 title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 title of this residence
	 */
	@Override
	public String getSection2Title(String languageId) {
		return model.getSection2Title(languageId);
	}

	/**
	 * Returns the localized section2 title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 title of this residence
	 */
	@Override
	public String getSection2Title(String languageId, boolean useDefault) {
		return model.getSection2Title(languageId, useDefault);
	}

	@Override
	public String getSection2TitleCurrentLanguageId() {
		return model.getSection2TitleCurrentLanguageId();
	}

	@Override
	public String getSection2TitleCurrentValue() {
		return model.getSection2TitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 titles of this residence.
	 *
	 * @return the locales and localized section2 titles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection2TitleMap() {
		return model.getSection2TitleMap();
	}

	/**
	 * Returns the section4 section title of this residence.
	 *
	 * @return the section4 section title of this residence
	 */
	@Override
	public String getSection4SectionTitle() {
		return model.getSection4SectionTitle();
	}

	/**
	 * Returns the localized section4 section title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section4 section title of this residence
	 */
	@Override
	public String getSection4SectionTitle(java.util.Locale locale) {
		return model.getSection4SectionTitle(locale);
	}

	/**
	 * Returns the localized section4 section title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section4 section title of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection4SectionTitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection4SectionTitle(locale, useDefault);
	}

	/**
	 * Returns the localized section4 section title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section4 section title of this residence
	 */
	@Override
	public String getSection4SectionTitle(String languageId) {
		return model.getSection4SectionTitle(languageId);
	}

	/**
	 * Returns the localized section4 section title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section4 section title of this residence
	 */
	@Override
	public String getSection4SectionTitle(
		String languageId, boolean useDefault) {

		return model.getSection4SectionTitle(languageId, useDefault);
	}

	@Override
	public String getSection4SectionTitleCurrentLanguageId() {
		return model.getSection4SectionTitleCurrentLanguageId();
	}

	@Override
	public String getSection4SectionTitleCurrentValue() {
		return model.getSection4SectionTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section4 section titles of this residence.
	 *
	 * @return the locales and localized section4 section titles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection4SectionTitleMap() {
		return model.getSection4SectionTitleMap();
	}

	/**
	 * Returns the section5 button label of this residence.
	 *
	 * @return the section5 button label of this residence
	 */
	@Override
	public String getSection5ButtonLabel() {
		return model.getSection5ButtonLabel();
	}

	/**
	 * Returns the localized section5 button label of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 button label of this residence
	 */
	@Override
	public String getSection5ButtonLabel(java.util.Locale locale) {
		return model.getSection5ButtonLabel(locale);
	}

	/**
	 * Returns the localized section5 button label of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 button label of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5ButtonLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5ButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized section5 button label of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 button label of this residence
	 */
	@Override
	public String getSection5ButtonLabel(String languageId) {
		return model.getSection5ButtonLabel(languageId);
	}

	/**
	 * Returns the localized section5 button label of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 button label of this residence
	 */
	@Override
	public String getSection5ButtonLabel(
		String languageId, boolean useDefault) {

		return model.getSection5ButtonLabel(languageId, useDefault);
	}

	@Override
	public String getSection5ButtonLabelCurrentLanguageId() {
		return model.getSection5ButtonLabelCurrentLanguageId();
	}

	@Override
	public String getSection5ButtonLabelCurrentValue() {
		return model.getSection5ButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 button labels of this residence.
	 *
	 * @return the locales and localized section5 button labels of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection5ButtonLabelMap() {
		return model.getSection5ButtonLabelMap();
	}

	/**
	 * Returns the section5 button link of this residence.
	 *
	 * @return the section5 button link of this residence
	 */
	@Override
	public String getSection5ButtonLink() {
		return model.getSection5ButtonLink();
	}

	/**
	 * Returns the localized section5 button link of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 button link of this residence
	 */
	@Override
	public String getSection5ButtonLink(java.util.Locale locale) {
		return model.getSection5ButtonLink(locale);
	}

	/**
	 * Returns the localized section5 button link of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 button link of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5ButtonLink(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5ButtonLink(locale, useDefault);
	}

	/**
	 * Returns the localized section5 button link of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 button link of this residence
	 */
	@Override
	public String getSection5ButtonLink(String languageId) {
		return model.getSection5ButtonLink(languageId);
	}

	/**
	 * Returns the localized section5 button link of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 button link of this residence
	 */
	@Override
	public String getSection5ButtonLink(String languageId, boolean useDefault) {
		return model.getSection5ButtonLink(languageId, useDefault);
	}

	@Override
	public String getSection5ButtonLinkCurrentLanguageId() {
		return model.getSection5ButtonLinkCurrentLanguageId();
	}

	@Override
	public String getSection5ButtonLinkCurrentValue() {
		return model.getSection5ButtonLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 button links of this residence.
	 *
	 * @return the locales and localized section5 button links of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection5ButtonLinkMap() {
		return model.getSection5ButtonLinkMap();
	}

	/**
	 * Returns the section5 description of this residence.
	 *
	 * @return the section5 description of this residence
	 */
	@Override
	public String getSection5Description() {
		return model.getSection5Description();
	}

	/**
	 * Returns the localized section5 description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 description of this residence
	 */
	@Override
	public String getSection5Description(java.util.Locale locale) {
		return model.getSection5Description(locale);
	}

	/**
	 * Returns the localized section5 description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 description of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5Description(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5Description(locale, useDefault);
	}

	/**
	 * Returns the localized section5 description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 description of this residence
	 */
	@Override
	public String getSection5Description(String languageId) {
		return model.getSection5Description(languageId);
	}

	/**
	 * Returns the localized section5 description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 description of this residence
	 */
	@Override
	public String getSection5Description(
		String languageId, boolean useDefault) {

		return model.getSection5Description(languageId, useDefault);
	}

	@Override
	public String getSection5DescriptionCurrentLanguageId() {
		return model.getSection5DescriptionCurrentLanguageId();
	}

	@Override
	public String getSection5DescriptionCurrentValue() {
		return model.getSection5DescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 descriptions of this residence.
	 *
	 * @return the locales and localized section5 descriptions of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection5DescriptionMap() {
		return model.getSection5DescriptionMap();
	}

	/**
	 * Returns the section5 image of this residence.
	 *
	 * @return the section5 image of this residence
	 */
	@Override
	public String getSection5Image() {
		return model.getSection5Image();
	}

	/**
	 * Returns the localized section5 image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 image of this residence
	 */
	@Override
	public String getSection5Image(java.util.Locale locale) {
		return model.getSection5Image(locale);
	}

	/**
	 * Returns the localized section5 image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 image of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5Image(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5Image(locale, useDefault);
	}

	/**
	 * Returns the localized section5 image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 image of this residence
	 */
	@Override
	public String getSection5Image(String languageId) {
		return model.getSection5Image(languageId);
	}

	/**
	 * Returns the localized section5 image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 image of this residence
	 */
	@Override
	public String getSection5Image(String languageId, boolean useDefault) {
		return model.getSection5Image(languageId, useDefault);
	}

	@Override
	public String getSection5ImageCurrentLanguageId() {
		return model.getSection5ImageCurrentLanguageId();
	}

	@Override
	public String getSection5ImageCurrentValue() {
		return model.getSection5ImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 images of this residence.
	 *
	 * @return the locales and localized section5 images of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection5ImageMap() {
		return model.getSection5ImageMap();
	}

	/**
	 * Returns the section5 subtitle of this residence.
	 *
	 * @return the section5 subtitle of this residence
	 */
	@Override
	public String getSection5Subtitle() {
		return model.getSection5Subtitle();
	}

	/**
	 * Returns the localized section5 subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 subtitle of this residence
	 */
	@Override
	public String getSection5Subtitle(java.util.Locale locale) {
		return model.getSection5Subtitle(locale);
	}

	/**
	 * Returns the localized section5 subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 subtitle of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5Subtitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5Subtitle(locale, useDefault);
	}

	/**
	 * Returns the localized section5 subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 subtitle of this residence
	 */
	@Override
	public String getSection5Subtitle(String languageId) {
		return model.getSection5Subtitle(languageId);
	}

	/**
	 * Returns the localized section5 subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 subtitle of this residence
	 */
	@Override
	public String getSection5Subtitle(String languageId, boolean useDefault) {
		return model.getSection5Subtitle(languageId, useDefault);
	}

	@Override
	public String getSection5SubtitleCurrentLanguageId() {
		return model.getSection5SubtitleCurrentLanguageId();
	}

	@Override
	public String getSection5SubtitleCurrentValue() {
		return model.getSection5SubtitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 subtitles of this residence.
	 *
	 * @return the locales and localized section5 subtitles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection5SubtitleMap() {
		return model.getSection5SubtitleMap();
	}

	/**
	 * Returns the section5 title of this residence.
	 *
	 * @return the section5 title of this residence
	 */
	@Override
	public String getSection5Title() {
		return model.getSection5Title();
	}

	/**
	 * Returns the localized section5 title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 title of this residence
	 */
	@Override
	public String getSection5Title(java.util.Locale locale) {
		return model.getSection5Title(locale);
	}

	/**
	 * Returns the localized section5 title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 title of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5Title(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5Title(locale, useDefault);
	}

	/**
	 * Returns the localized section5 title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 title of this residence
	 */
	@Override
	public String getSection5Title(String languageId) {
		return model.getSection5Title(languageId);
	}

	/**
	 * Returns the localized section5 title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 title of this residence
	 */
	@Override
	public String getSection5Title(String languageId, boolean useDefault) {
		return model.getSection5Title(languageId, useDefault);
	}

	@Override
	public String getSection5TitleCurrentLanguageId() {
		return model.getSection5TitleCurrentLanguageId();
	}

	@Override
	public String getSection5TitleCurrentValue() {
		return model.getSection5TitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 titles of this residence.
	 *
	 * @return the locales and localized section5 titles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection5TitleMap() {
		return model.getSection5TitleMap();
	}

	/**
	 * Returns the section6 button label of this residence.
	 *
	 * @return the section6 button label of this residence
	 */
	@Override
	public String getSection6ButtonLabel() {
		return model.getSection6ButtonLabel();
	}

	/**
	 * Returns the localized section6 button label of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section6 button label of this residence
	 */
	@Override
	public String getSection6ButtonLabel(java.util.Locale locale) {
		return model.getSection6ButtonLabel(locale);
	}

	/**
	 * Returns the localized section6 button label of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 button label of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection6ButtonLabel(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection6ButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized section6 button label of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section6 button label of this residence
	 */
	@Override
	public String getSection6ButtonLabel(String languageId) {
		return model.getSection6ButtonLabel(languageId);
	}

	/**
	 * Returns the localized section6 button label of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 button label of this residence
	 */
	@Override
	public String getSection6ButtonLabel(
		String languageId, boolean useDefault) {

		return model.getSection6ButtonLabel(languageId, useDefault);
	}

	@Override
	public String getSection6ButtonLabelCurrentLanguageId() {
		return model.getSection6ButtonLabelCurrentLanguageId();
	}

	@Override
	public String getSection6ButtonLabelCurrentValue() {
		return model.getSection6ButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section6 button labels of this residence.
	 *
	 * @return the locales and localized section6 button labels of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection6ButtonLabelMap() {
		return model.getSection6ButtonLabelMap();
	}

	/**
	 * Returns the section6 button link of this residence.
	 *
	 * @return the section6 button link of this residence
	 */
	@Override
	public String getSection6ButtonLink() {
		return model.getSection6ButtonLink();
	}

	/**
	 * Returns the localized section6 button link of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section6 button link of this residence
	 */
	@Override
	public String getSection6ButtonLink(java.util.Locale locale) {
		return model.getSection6ButtonLink(locale);
	}

	/**
	 * Returns the localized section6 button link of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 button link of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection6ButtonLink(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection6ButtonLink(locale, useDefault);
	}

	/**
	 * Returns the localized section6 button link of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section6 button link of this residence
	 */
	@Override
	public String getSection6ButtonLink(String languageId) {
		return model.getSection6ButtonLink(languageId);
	}

	/**
	 * Returns the localized section6 button link of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 button link of this residence
	 */
	@Override
	public String getSection6ButtonLink(String languageId, boolean useDefault) {
		return model.getSection6ButtonLink(languageId, useDefault);
	}

	@Override
	public String getSection6ButtonLinkCurrentLanguageId() {
		return model.getSection6ButtonLinkCurrentLanguageId();
	}

	@Override
	public String getSection6ButtonLinkCurrentValue() {
		return model.getSection6ButtonLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section6 button links of this residence.
	 *
	 * @return the locales and localized section6 button links of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection6ButtonLinkMap() {
		return model.getSection6ButtonLinkMap();
	}

	/**
	 * Returns the section6 description of this residence.
	 *
	 * @return the section6 description of this residence
	 */
	@Override
	public String getSection6Description() {
		return model.getSection6Description();
	}

	/**
	 * Returns the localized section6 description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section6 description of this residence
	 */
	@Override
	public String getSection6Description(java.util.Locale locale) {
		return model.getSection6Description(locale);
	}

	/**
	 * Returns the localized section6 description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 description of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection6Description(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection6Description(locale, useDefault);
	}

	/**
	 * Returns the localized section6 description of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section6 description of this residence
	 */
	@Override
	public String getSection6Description(String languageId) {
		return model.getSection6Description(languageId);
	}

	/**
	 * Returns the localized section6 description of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 description of this residence
	 */
	@Override
	public String getSection6Description(
		String languageId, boolean useDefault) {

		return model.getSection6Description(languageId, useDefault);
	}

	@Override
	public String getSection6DescriptionCurrentLanguageId() {
		return model.getSection6DescriptionCurrentLanguageId();
	}

	@Override
	public String getSection6DescriptionCurrentValue() {
		return model.getSection6DescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section6 descriptions of this residence.
	 *
	 * @return the locales and localized section6 descriptions of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection6DescriptionMap() {
		return model.getSection6DescriptionMap();
	}

	/**
	 * Returns the section6 image of this residence.
	 *
	 * @return the section6 image of this residence
	 */
	@Override
	public String getSection6Image() {
		return model.getSection6Image();
	}

	/**
	 * Returns the localized section6 image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section6 image of this residence
	 */
	@Override
	public String getSection6Image(java.util.Locale locale) {
		return model.getSection6Image(locale);
	}

	/**
	 * Returns the localized section6 image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 image of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection6Image(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection6Image(locale, useDefault);
	}

	/**
	 * Returns the localized section6 image of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section6 image of this residence
	 */
	@Override
	public String getSection6Image(String languageId) {
		return model.getSection6Image(languageId);
	}

	/**
	 * Returns the localized section6 image of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 image of this residence
	 */
	@Override
	public String getSection6Image(String languageId, boolean useDefault) {
		return model.getSection6Image(languageId, useDefault);
	}

	@Override
	public String getSection6ImageCurrentLanguageId() {
		return model.getSection6ImageCurrentLanguageId();
	}

	@Override
	public String getSection6ImageCurrentValue() {
		return model.getSection6ImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section6 images of this residence.
	 *
	 * @return the locales and localized section6 images of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection6ImageMap() {
		return model.getSection6ImageMap();
	}

	/**
	 * Returns the section6 subtitle of this residence.
	 *
	 * @return the section6 subtitle of this residence
	 */
	@Override
	public String getSection6Subtitle() {
		return model.getSection6Subtitle();
	}

	/**
	 * Returns the localized section6 subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section6 subtitle of this residence
	 */
	@Override
	public String getSection6Subtitle(java.util.Locale locale) {
		return model.getSection6Subtitle(locale);
	}

	/**
	 * Returns the localized section6 subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 subtitle of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection6Subtitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection6Subtitle(locale, useDefault);
	}

	/**
	 * Returns the localized section6 subtitle of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section6 subtitle of this residence
	 */
	@Override
	public String getSection6Subtitle(String languageId) {
		return model.getSection6Subtitle(languageId);
	}

	/**
	 * Returns the localized section6 subtitle of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 subtitle of this residence
	 */
	@Override
	public String getSection6Subtitle(String languageId, boolean useDefault) {
		return model.getSection6Subtitle(languageId, useDefault);
	}

	@Override
	public String getSection6SubtitleCurrentLanguageId() {
		return model.getSection6SubtitleCurrentLanguageId();
	}

	@Override
	public String getSection6SubtitleCurrentValue() {
		return model.getSection6SubtitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section6 subtitles of this residence.
	 *
	 * @return the locales and localized section6 subtitles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection6SubtitleMap() {
		return model.getSection6SubtitleMap();
	}

	/**
	 * Returns the section6 title of this residence.
	 *
	 * @return the section6 title of this residence
	 */
	@Override
	public String getSection6Title() {
		return model.getSection6Title();
	}

	/**
	 * Returns the localized section6 title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section6 title of this residence
	 */
	@Override
	public String getSection6Title(java.util.Locale locale) {
		return model.getSection6Title(locale);
	}

	/**
	 * Returns the localized section6 title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 title of this residence. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection6Title(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection6Title(locale, useDefault);
	}

	/**
	 * Returns the localized section6 title of this residence in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section6 title of this residence
	 */
	@Override
	public String getSection6Title(String languageId) {
		return model.getSection6Title(languageId);
	}

	/**
	 * Returns the localized section6 title of this residence in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section6 title of this residence
	 */
	@Override
	public String getSection6Title(String languageId, boolean useDefault) {
		return model.getSection6Title(languageId, useDefault);
	}

	@Override
	public String getSection6TitleCurrentLanguageId() {
		return model.getSection6TitleCurrentLanguageId();
	}

	@Override
	public String getSection6TitleCurrentValue() {
		return model.getSection6TitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section6 titles of this residence.
	 *
	 * @return the locales and localized section6 titles of this residence
	 */
	@Override
	public Map<java.util.Locale, String> getSection6TitleMap() {
		return model.getSection6TitleMap();
	}

	/**
	 * Returns the user ID of this residence.
	 *
	 * @return the user ID of this residence
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this residence.
	 *
	 * @return the user name of this residence
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this residence.
	 *
	 * @return the user uuid of this residence
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this residence.
	 *
	 * @return the uuid of this residence
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the banner description of this residence.
	 *
	 * @param bannerDescription the banner description of this residence
	 */
	@Override
	public void setBannerDescription(String bannerDescription) {
		model.setBannerDescription(bannerDescription);
	}

	/**
	 * Sets the localized banner description of this residence in the language.
	 *
	 * @param bannerDescription the localized banner description of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerDescription(
		String bannerDescription, java.util.Locale locale) {

		model.setBannerDescription(bannerDescription, locale);
	}

	/**
	 * Sets the localized banner description of this residence in the language, and sets the default locale.
	 *
	 * @param bannerDescription the localized banner description of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerDescription(
		String bannerDescription, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerDescription(bannerDescription, locale, defaultLocale);
	}

	@Override
	public void setBannerDescriptionCurrentLanguageId(String languageId) {
		model.setBannerDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner descriptions of this residence from the map of locales and localized banner descriptions.
	 *
	 * @param bannerDescriptionMap the locales and localized banner descriptions of this residence
	 */
	@Override
	public void setBannerDescriptionMap(
		Map<java.util.Locale, String> bannerDescriptionMap) {

		model.setBannerDescriptionMap(bannerDescriptionMap);
	}

	/**
	 * Sets the localized banner descriptions of this residence from the map of locales and localized banner descriptions, and sets the default locale.
	 *
	 * @param bannerDescriptionMap the locales and localized banner descriptions of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerDescriptionMap(
		Map<java.util.Locale, String> bannerDescriptionMap,
		java.util.Locale defaultLocale) {

		model.setBannerDescriptionMap(bannerDescriptionMap, defaultLocale);
	}

	/**
	 * Sets the banner desktop image of this residence.
	 *
	 * @param bannerDesktopImage the banner desktop image of this residence
	 */
	@Override
	public void setBannerDesktopImage(String bannerDesktopImage) {
		model.setBannerDesktopImage(bannerDesktopImage);
	}

	/**
	 * Sets the localized banner desktop image of this residence in the language.
	 *
	 * @param bannerDesktopImage the localized banner desktop image of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerDesktopImage(
		String bannerDesktopImage, java.util.Locale locale) {

		model.setBannerDesktopImage(bannerDesktopImage, locale);
	}

	/**
	 * Sets the localized banner desktop image of this residence in the language, and sets the default locale.
	 *
	 * @param bannerDesktopImage the localized banner desktop image of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerDesktopImage(
		String bannerDesktopImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerDesktopImage(bannerDesktopImage, locale, defaultLocale);
	}

	@Override
	public void setBannerDesktopImageCurrentLanguageId(String languageId) {
		model.setBannerDesktopImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner desktop images of this residence from the map of locales and localized banner desktop images.
	 *
	 * @param bannerDesktopImageMap the locales and localized banner desktop images of this residence
	 */
	@Override
	public void setBannerDesktopImageMap(
		Map<java.util.Locale, String> bannerDesktopImageMap) {

		model.setBannerDesktopImageMap(bannerDesktopImageMap);
	}

	/**
	 * Sets the localized banner desktop images of this residence from the map of locales and localized banner desktop images, and sets the default locale.
	 *
	 * @param bannerDesktopImageMap the locales and localized banner desktop images of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerDesktopImageMap(
		Map<java.util.Locale, String> bannerDesktopImageMap,
		java.util.Locale defaultLocale) {

		model.setBannerDesktopImageMap(bannerDesktopImageMap, defaultLocale);
	}

	/**
	 * Sets the banner mobile image of this residence.
	 *
	 * @param bannerMobileImage the banner mobile image of this residence
	 */
	@Override
	public void setBannerMobileImage(String bannerMobileImage) {
		model.setBannerMobileImage(bannerMobileImage);
	}

	/**
	 * Sets the localized banner mobile image of this residence in the language.
	 *
	 * @param bannerMobileImage the localized banner mobile image of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerMobileImage(
		String bannerMobileImage, java.util.Locale locale) {

		model.setBannerMobileImage(bannerMobileImage, locale);
	}

	/**
	 * Sets the localized banner mobile image of this residence in the language, and sets the default locale.
	 *
	 * @param bannerMobileImage the localized banner mobile image of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerMobileImage(
		String bannerMobileImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerMobileImage(bannerMobileImage, locale, defaultLocale);
	}

	@Override
	public void setBannerMobileImageCurrentLanguageId(String languageId) {
		model.setBannerMobileImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner mobile images of this residence from the map of locales and localized banner mobile images.
	 *
	 * @param bannerMobileImageMap the locales and localized banner mobile images of this residence
	 */
	@Override
	public void setBannerMobileImageMap(
		Map<java.util.Locale, String> bannerMobileImageMap) {

		model.setBannerMobileImageMap(bannerMobileImageMap);
	}

	/**
	 * Sets the localized banner mobile images of this residence from the map of locales and localized banner mobile images, and sets the default locale.
	 *
	 * @param bannerMobileImageMap the locales and localized banner mobile images of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerMobileImageMap(
		Map<java.util.Locale, String> bannerMobileImageMap,
		java.util.Locale defaultLocale) {

		model.setBannerMobileImageMap(bannerMobileImageMap, defaultLocale);
	}

	/**
	 * Sets the banner subtitle of this residence.
	 *
	 * @param bannerSubtitle the banner subtitle of this residence
	 */
	@Override
	public void setBannerSubtitle(String bannerSubtitle) {
		model.setBannerSubtitle(bannerSubtitle);
	}

	/**
	 * Sets the localized banner subtitle of this residence in the language.
	 *
	 * @param bannerSubtitle the localized banner subtitle of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerSubtitle(
		String bannerSubtitle, java.util.Locale locale) {

		model.setBannerSubtitle(bannerSubtitle, locale);
	}

	/**
	 * Sets the localized banner subtitle of this residence in the language, and sets the default locale.
	 *
	 * @param bannerSubtitle the localized banner subtitle of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerSubtitle(
		String bannerSubtitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerSubtitle(bannerSubtitle, locale, defaultLocale);
	}

	@Override
	public void setBannerSubtitleCurrentLanguageId(String languageId) {
		model.setBannerSubtitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner subtitles of this residence from the map of locales and localized banner subtitles.
	 *
	 * @param bannerSubtitleMap the locales and localized banner subtitles of this residence
	 */
	@Override
	public void setBannerSubtitleMap(
		Map<java.util.Locale, String> bannerSubtitleMap) {

		model.setBannerSubtitleMap(bannerSubtitleMap);
	}

	/**
	 * Sets the localized banner subtitles of this residence from the map of locales and localized banner subtitles, and sets the default locale.
	 *
	 * @param bannerSubtitleMap the locales and localized banner subtitles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerSubtitleMap(
		Map<java.util.Locale, String> bannerSubtitleMap,
		java.util.Locale defaultLocale) {

		model.setBannerSubtitleMap(bannerSubtitleMap, defaultLocale);
	}

	/**
	 * Sets the banner title of this residence.
	 *
	 * @param bannerTitle the banner title of this residence
	 */
	@Override
	public void setBannerTitle(String bannerTitle) {
		model.setBannerTitle(bannerTitle);
	}

	/**
	 * Sets the localized banner title of this residence in the language.
	 *
	 * @param bannerTitle the localized banner title of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerTitle(String bannerTitle, java.util.Locale locale) {
		model.setBannerTitle(bannerTitle, locale);
	}

	/**
	 * Sets the localized banner title of this residence in the language, and sets the default locale.
	 *
	 * @param bannerTitle the localized banner title of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerTitle(
		String bannerTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerTitle(bannerTitle, locale, defaultLocale);
	}

	@Override
	public void setBannerTitleCurrentLanguageId(String languageId) {
		model.setBannerTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner titles of this residence from the map of locales and localized banner titles.
	 *
	 * @param bannerTitleMap the locales and localized banner titles of this residence
	 */
	@Override
	public void setBannerTitleMap(
		Map<java.util.Locale, String> bannerTitleMap) {

		model.setBannerTitleMap(bannerTitleMap);
	}

	/**
	 * Sets the localized banner titles of this residence from the map of locales and localized banner titles, and sets the default locale.
	 *
	 * @param bannerTitleMap the locales and localized banner titles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerTitleMap(
		Map<java.util.Locale, String> bannerTitleMap,
		java.util.Locale defaultLocale) {

		model.setBannerTitleMap(bannerTitleMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this residence.
	 *
	 * @param companyId the company ID of this residence
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this residence.
	 *
	 * @param createDate the create date of this residence
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this residence.
	 *
	 * @param groupId the group ID of this residence
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the listing description of this residence.
	 *
	 * @param listingDescription the listing description of this residence
	 */
	@Override
	public void setListingDescription(String listingDescription) {
		model.setListingDescription(listingDescription);
	}

	/**
	 * Sets the localized listing description of this residence in the language.
	 *
	 * @param listingDescription the localized listing description of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingDescription(
		String listingDescription, java.util.Locale locale) {

		model.setListingDescription(listingDescription, locale);
	}

	/**
	 * Sets the localized listing description of this residence in the language, and sets the default locale.
	 *
	 * @param listingDescription the localized listing description of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingDescription(
		String listingDescription, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingDescription(listingDescription, locale, defaultLocale);
	}

	@Override
	public void setListingDescriptionCurrentLanguageId(String languageId) {
		model.setListingDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing descriptions of this residence from the map of locales and localized listing descriptions.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this residence
	 */
	@Override
	public void setListingDescriptionMap(
		Map<java.util.Locale, String> listingDescriptionMap) {

		model.setListingDescriptionMap(listingDescriptionMap);
	}

	/**
	 * Sets the localized listing descriptions of this residence from the map of locales and localized listing descriptions, and sets the default locale.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingDescriptionMap(
		Map<java.util.Locale, String> listingDescriptionMap,
		java.util.Locale defaultLocale) {

		model.setListingDescriptionMap(listingDescriptionMap, defaultLocale);
	}

	/**
	 * Sets the listing image of this residence.
	 *
	 * @param listingImage the listing image of this residence
	 */
	@Override
	public void setListingImage(String listingImage) {
		model.setListingImage(listingImage);
	}

	/**
	 * Sets the localized listing image of this residence in the language.
	 *
	 * @param listingImage the localized listing image of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingImage(String listingImage, java.util.Locale locale) {
		model.setListingImage(listingImage, locale);
	}

	/**
	 * Sets the localized listing image of this residence in the language, and sets the default locale.
	 *
	 * @param listingImage the localized listing image of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImage(
		String listingImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingImage(listingImage, locale, defaultLocale);
	}

	@Override
	public void setListingImageCurrentLanguageId(String languageId) {
		model.setListingImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing images of this residence from the map of locales and localized listing images.
	 *
	 * @param listingImageMap the locales and localized listing images of this residence
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap) {

		model.setListingImageMap(listingImageMap);
	}

	/**
	 * Sets the localized listing images of this residence from the map of locales and localized listing images, and sets the default locale.
	 *
	 * @param listingImageMap the locales and localized listing images of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap,
		java.util.Locale defaultLocale) {

		model.setListingImageMap(listingImageMap, defaultLocale);
	}

	/**
	 * Sets the listing title of this residence.
	 *
	 * @param listingTitle the listing title of this residence
	 */
	@Override
	public void setListingTitle(String listingTitle) {
		model.setListingTitle(listingTitle);
	}

	/**
	 * Sets the localized listing title of this residence in the language.
	 *
	 * @param listingTitle the localized listing title of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingTitle(String listingTitle, java.util.Locale locale) {
		model.setListingTitle(listingTitle, locale);
	}

	/**
	 * Sets the localized listing title of this residence in the language, and sets the default locale.
	 *
	 * @param listingTitle the localized listing title of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingTitle(
		String listingTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingTitle(listingTitle, locale, defaultLocale);
	}

	@Override
	public void setListingTitleCurrentLanguageId(String languageId) {
		model.setListingTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing titles of this residence from the map of locales and localized listing titles.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this residence
	 */
	@Override
	public void setListingTitleMap(
		Map<java.util.Locale, String> listingTitleMap) {

		model.setListingTitleMap(listingTitleMap);
	}

	/**
	 * Sets the localized listing titles of this residence from the map of locales and localized listing titles, and sets the default locale.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingTitleMap(
		Map<java.util.Locale, String> listingTitleMap,
		java.util.Locale defaultLocale) {

		model.setListingTitleMap(listingTitleMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this residence.
	 *
	 * @param modifiedDate the modified date of this residence
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this residence.
	 *
	 * @param primaryKey the primary key of this residence
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the residence ID of this residence.
	 *
	 * @param residenceId the residence ID of this residence
	 */
	@Override
	public void setResidenceId(long residenceId) {
		model.setResidenceId(residenceId);
	}

	/**
	 * Sets the section2 button label of this residence.
	 *
	 * @param section2ButtonLabel the section2 button label of this residence
	 */
	@Override
	public void setSection2ButtonLabel(String section2ButtonLabel) {
		model.setSection2ButtonLabel(section2ButtonLabel);
	}

	/**
	 * Sets the localized section2 button label of this residence in the language.
	 *
	 * @param section2ButtonLabel the localized section2 button label of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2ButtonLabel(
		String section2ButtonLabel, java.util.Locale locale) {

		model.setSection2ButtonLabel(section2ButtonLabel, locale);
	}

	/**
	 * Sets the localized section2 button label of this residence in the language, and sets the default locale.
	 *
	 * @param section2ButtonLabel the localized section2 button label of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2ButtonLabel(
		String section2ButtonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2ButtonLabel(
			section2ButtonLabel, locale, defaultLocale);
	}

	@Override
	public void setSection2ButtonLabelCurrentLanguageId(String languageId) {
		model.setSection2ButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 button labels of this residence from the map of locales and localized section2 button labels.
	 *
	 * @param section2ButtonLabelMap the locales and localized section2 button labels of this residence
	 */
	@Override
	public void setSection2ButtonLabelMap(
		Map<java.util.Locale, String> section2ButtonLabelMap) {

		model.setSection2ButtonLabelMap(section2ButtonLabelMap);
	}

	/**
	 * Sets the localized section2 button labels of this residence from the map of locales and localized section2 button labels, and sets the default locale.
	 *
	 * @param section2ButtonLabelMap the locales and localized section2 button labels of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2ButtonLabelMap(
		Map<java.util.Locale, String> section2ButtonLabelMap,
		java.util.Locale defaultLocale) {

		model.setSection2ButtonLabelMap(section2ButtonLabelMap, defaultLocale);
	}

	/**
	 * Sets the section2 button link of this residence.
	 *
	 * @param section2ButtonLink the section2 button link of this residence
	 */
	@Override
	public void setSection2ButtonLink(String section2ButtonLink) {
		model.setSection2ButtonLink(section2ButtonLink);
	}

	/**
	 * Sets the localized section2 button link of this residence in the language.
	 *
	 * @param section2ButtonLink the localized section2 button link of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2ButtonLink(
		String section2ButtonLink, java.util.Locale locale) {

		model.setSection2ButtonLink(section2ButtonLink, locale);
	}

	/**
	 * Sets the localized section2 button link of this residence in the language, and sets the default locale.
	 *
	 * @param section2ButtonLink the localized section2 button link of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2ButtonLink(
		String section2ButtonLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2ButtonLink(section2ButtonLink, locale, defaultLocale);
	}

	@Override
	public void setSection2ButtonLinkCurrentLanguageId(String languageId) {
		model.setSection2ButtonLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 button links of this residence from the map of locales and localized section2 button links.
	 *
	 * @param section2ButtonLinkMap the locales and localized section2 button links of this residence
	 */
	@Override
	public void setSection2ButtonLinkMap(
		Map<java.util.Locale, String> section2ButtonLinkMap) {

		model.setSection2ButtonLinkMap(section2ButtonLinkMap);
	}

	/**
	 * Sets the localized section2 button links of this residence from the map of locales and localized section2 button links, and sets the default locale.
	 *
	 * @param section2ButtonLinkMap the locales and localized section2 button links of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2ButtonLinkMap(
		Map<java.util.Locale, String> section2ButtonLinkMap,
		java.util.Locale defaultLocale) {

		model.setSection2ButtonLinkMap(section2ButtonLinkMap, defaultLocale);
	}

	/**
	 * Sets the section2 description of this residence.
	 *
	 * @param section2Description the section2 description of this residence
	 */
	@Override
	public void setSection2Description(String section2Description) {
		model.setSection2Description(section2Description);
	}

	/**
	 * Sets the localized section2 description of this residence in the language.
	 *
	 * @param section2Description the localized section2 description of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2Description(
		String section2Description, java.util.Locale locale) {

		model.setSection2Description(section2Description, locale);
	}

	/**
	 * Sets the localized section2 description of this residence in the language, and sets the default locale.
	 *
	 * @param section2Description the localized section2 description of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2Description(
		String section2Description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2Description(
			section2Description, locale, defaultLocale);
	}

	@Override
	public void setSection2DescriptionCurrentLanguageId(String languageId) {
		model.setSection2DescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 descriptions of this residence from the map of locales and localized section2 descriptions.
	 *
	 * @param section2DescriptionMap the locales and localized section2 descriptions of this residence
	 */
	@Override
	public void setSection2DescriptionMap(
		Map<java.util.Locale, String> section2DescriptionMap) {

		model.setSection2DescriptionMap(section2DescriptionMap);
	}

	/**
	 * Sets the localized section2 descriptions of this residence from the map of locales and localized section2 descriptions, and sets the default locale.
	 *
	 * @param section2DescriptionMap the locales and localized section2 descriptions of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2DescriptionMap(
		Map<java.util.Locale, String> section2DescriptionMap,
		java.util.Locale defaultLocale) {

		model.setSection2DescriptionMap(section2DescriptionMap, defaultLocale);
	}

	/**
	 * Sets the section2 image of this residence.
	 *
	 * @param section2Image the section2 image of this residence
	 */
	@Override
	public void setSection2Image(String section2Image) {
		model.setSection2Image(section2Image);
	}

	/**
	 * Sets the localized section2 image of this residence in the language.
	 *
	 * @param section2Image the localized section2 image of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2Image(
		String section2Image, java.util.Locale locale) {

		model.setSection2Image(section2Image, locale);
	}

	/**
	 * Sets the localized section2 image of this residence in the language, and sets the default locale.
	 *
	 * @param section2Image the localized section2 image of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2Image(
		String section2Image, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2Image(section2Image, locale, defaultLocale);
	}

	@Override
	public void setSection2ImageCurrentLanguageId(String languageId) {
		model.setSection2ImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 images of this residence from the map of locales and localized section2 images.
	 *
	 * @param section2ImageMap the locales and localized section2 images of this residence
	 */
	@Override
	public void setSection2ImageMap(
		Map<java.util.Locale, String> section2ImageMap) {

		model.setSection2ImageMap(section2ImageMap);
	}

	/**
	 * Sets the localized section2 images of this residence from the map of locales and localized section2 images, and sets the default locale.
	 *
	 * @param section2ImageMap the locales and localized section2 images of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2ImageMap(
		Map<java.util.Locale, String> section2ImageMap,
		java.util.Locale defaultLocale) {

		model.setSection2ImageMap(section2ImageMap, defaultLocale);
	}

	/**
	 * Sets the section2 subtitle of this residence.
	 *
	 * @param section2Subtitle the section2 subtitle of this residence
	 */
	@Override
	public void setSection2Subtitle(String section2Subtitle) {
		model.setSection2Subtitle(section2Subtitle);
	}

	/**
	 * Sets the localized section2 subtitle of this residence in the language.
	 *
	 * @param section2Subtitle the localized section2 subtitle of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2Subtitle(
		String section2Subtitle, java.util.Locale locale) {

		model.setSection2Subtitle(section2Subtitle, locale);
	}

	/**
	 * Sets the localized section2 subtitle of this residence in the language, and sets the default locale.
	 *
	 * @param section2Subtitle the localized section2 subtitle of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2Subtitle(
		String section2Subtitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2Subtitle(section2Subtitle, locale, defaultLocale);
	}

	@Override
	public void setSection2SubtitleCurrentLanguageId(String languageId) {
		model.setSection2SubtitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 subtitles of this residence from the map of locales and localized section2 subtitles.
	 *
	 * @param section2SubtitleMap the locales and localized section2 subtitles of this residence
	 */
	@Override
	public void setSection2SubtitleMap(
		Map<java.util.Locale, String> section2SubtitleMap) {

		model.setSection2SubtitleMap(section2SubtitleMap);
	}

	/**
	 * Sets the localized section2 subtitles of this residence from the map of locales and localized section2 subtitles, and sets the default locale.
	 *
	 * @param section2SubtitleMap the locales and localized section2 subtitles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2SubtitleMap(
		Map<java.util.Locale, String> section2SubtitleMap,
		java.util.Locale defaultLocale) {

		model.setSection2SubtitleMap(section2SubtitleMap, defaultLocale);
	}

	/**
	 * Sets the section2 title of this residence.
	 *
	 * @param section2Title the section2 title of this residence
	 */
	@Override
	public void setSection2Title(String section2Title) {
		model.setSection2Title(section2Title);
	}

	/**
	 * Sets the localized section2 title of this residence in the language.
	 *
	 * @param section2Title the localized section2 title of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2Title(
		String section2Title, java.util.Locale locale) {

		model.setSection2Title(section2Title, locale);
	}

	/**
	 * Sets the localized section2 title of this residence in the language, and sets the default locale.
	 *
	 * @param section2Title the localized section2 title of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2Title(
		String section2Title, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2Title(section2Title, locale, defaultLocale);
	}

	@Override
	public void setSection2TitleCurrentLanguageId(String languageId) {
		model.setSection2TitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 titles of this residence from the map of locales and localized section2 titles.
	 *
	 * @param section2TitleMap the locales and localized section2 titles of this residence
	 */
	@Override
	public void setSection2TitleMap(
		Map<java.util.Locale, String> section2TitleMap) {

		model.setSection2TitleMap(section2TitleMap);
	}

	/**
	 * Sets the localized section2 titles of this residence from the map of locales and localized section2 titles, and sets the default locale.
	 *
	 * @param section2TitleMap the locales and localized section2 titles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2TitleMap(
		Map<java.util.Locale, String> section2TitleMap,
		java.util.Locale defaultLocale) {

		model.setSection2TitleMap(section2TitleMap, defaultLocale);
	}

	/**
	 * Sets the section4 section title of this residence.
	 *
	 * @param section4SectionTitle the section4 section title of this residence
	 */
	@Override
	public void setSection4SectionTitle(String section4SectionTitle) {
		model.setSection4SectionTitle(section4SectionTitle);
	}

	/**
	 * Sets the localized section4 section title of this residence in the language.
	 *
	 * @param section4SectionTitle the localized section4 section title of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection4SectionTitle(
		String section4SectionTitle, java.util.Locale locale) {

		model.setSection4SectionTitle(section4SectionTitle, locale);
	}

	/**
	 * Sets the localized section4 section title of this residence in the language, and sets the default locale.
	 *
	 * @param section4SectionTitle the localized section4 section title of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection4SectionTitle(
		String section4SectionTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection4SectionTitle(
			section4SectionTitle, locale, defaultLocale);
	}

	@Override
	public void setSection4SectionTitleCurrentLanguageId(String languageId) {
		model.setSection4SectionTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section4 section titles of this residence from the map of locales and localized section4 section titles.
	 *
	 * @param section4SectionTitleMap the locales and localized section4 section titles of this residence
	 */
	@Override
	public void setSection4SectionTitleMap(
		Map<java.util.Locale, String> section4SectionTitleMap) {

		model.setSection4SectionTitleMap(section4SectionTitleMap);
	}

	/**
	 * Sets the localized section4 section titles of this residence from the map of locales and localized section4 section titles, and sets the default locale.
	 *
	 * @param section4SectionTitleMap the locales and localized section4 section titles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection4SectionTitleMap(
		Map<java.util.Locale, String> section4SectionTitleMap,
		java.util.Locale defaultLocale) {

		model.setSection4SectionTitleMap(
			section4SectionTitleMap, defaultLocale);
	}

	/**
	 * Sets the section5 button label of this residence.
	 *
	 * @param section5ButtonLabel the section5 button label of this residence
	 */
	@Override
	public void setSection5ButtonLabel(String section5ButtonLabel) {
		model.setSection5ButtonLabel(section5ButtonLabel);
	}

	/**
	 * Sets the localized section5 button label of this residence in the language.
	 *
	 * @param section5ButtonLabel the localized section5 button label of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5ButtonLabel(
		String section5ButtonLabel, java.util.Locale locale) {

		model.setSection5ButtonLabel(section5ButtonLabel, locale);
	}

	/**
	 * Sets the localized section5 button label of this residence in the language, and sets the default locale.
	 *
	 * @param section5ButtonLabel the localized section5 button label of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5ButtonLabel(
		String section5ButtonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5ButtonLabel(
			section5ButtonLabel, locale, defaultLocale);
	}

	@Override
	public void setSection5ButtonLabelCurrentLanguageId(String languageId) {
		model.setSection5ButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 button labels of this residence from the map of locales and localized section5 button labels.
	 *
	 * @param section5ButtonLabelMap the locales and localized section5 button labels of this residence
	 */
	@Override
	public void setSection5ButtonLabelMap(
		Map<java.util.Locale, String> section5ButtonLabelMap) {

		model.setSection5ButtonLabelMap(section5ButtonLabelMap);
	}

	/**
	 * Sets the localized section5 button labels of this residence from the map of locales and localized section5 button labels, and sets the default locale.
	 *
	 * @param section5ButtonLabelMap the locales and localized section5 button labels of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5ButtonLabelMap(
		Map<java.util.Locale, String> section5ButtonLabelMap,
		java.util.Locale defaultLocale) {

		model.setSection5ButtonLabelMap(section5ButtonLabelMap, defaultLocale);
	}

	/**
	 * Sets the section5 button link of this residence.
	 *
	 * @param section5ButtonLink the section5 button link of this residence
	 */
	@Override
	public void setSection5ButtonLink(String section5ButtonLink) {
		model.setSection5ButtonLink(section5ButtonLink);
	}

	/**
	 * Sets the localized section5 button link of this residence in the language.
	 *
	 * @param section5ButtonLink the localized section5 button link of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5ButtonLink(
		String section5ButtonLink, java.util.Locale locale) {

		model.setSection5ButtonLink(section5ButtonLink, locale);
	}

	/**
	 * Sets the localized section5 button link of this residence in the language, and sets the default locale.
	 *
	 * @param section5ButtonLink the localized section5 button link of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5ButtonLink(
		String section5ButtonLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5ButtonLink(section5ButtonLink, locale, defaultLocale);
	}

	@Override
	public void setSection5ButtonLinkCurrentLanguageId(String languageId) {
		model.setSection5ButtonLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 button links of this residence from the map of locales and localized section5 button links.
	 *
	 * @param section5ButtonLinkMap the locales and localized section5 button links of this residence
	 */
	@Override
	public void setSection5ButtonLinkMap(
		Map<java.util.Locale, String> section5ButtonLinkMap) {

		model.setSection5ButtonLinkMap(section5ButtonLinkMap);
	}

	/**
	 * Sets the localized section5 button links of this residence from the map of locales and localized section5 button links, and sets the default locale.
	 *
	 * @param section5ButtonLinkMap the locales and localized section5 button links of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5ButtonLinkMap(
		Map<java.util.Locale, String> section5ButtonLinkMap,
		java.util.Locale defaultLocale) {

		model.setSection5ButtonLinkMap(section5ButtonLinkMap, defaultLocale);
	}

	/**
	 * Sets the section5 description of this residence.
	 *
	 * @param section5Description the section5 description of this residence
	 */
	@Override
	public void setSection5Description(String section5Description) {
		model.setSection5Description(section5Description);
	}

	/**
	 * Sets the localized section5 description of this residence in the language.
	 *
	 * @param section5Description the localized section5 description of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5Description(
		String section5Description, java.util.Locale locale) {

		model.setSection5Description(section5Description, locale);
	}

	/**
	 * Sets the localized section5 description of this residence in the language, and sets the default locale.
	 *
	 * @param section5Description the localized section5 description of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5Description(
		String section5Description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5Description(
			section5Description, locale, defaultLocale);
	}

	@Override
	public void setSection5DescriptionCurrentLanguageId(String languageId) {
		model.setSection5DescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 descriptions of this residence from the map of locales and localized section5 descriptions.
	 *
	 * @param section5DescriptionMap the locales and localized section5 descriptions of this residence
	 */
	@Override
	public void setSection5DescriptionMap(
		Map<java.util.Locale, String> section5DescriptionMap) {

		model.setSection5DescriptionMap(section5DescriptionMap);
	}

	/**
	 * Sets the localized section5 descriptions of this residence from the map of locales and localized section5 descriptions, and sets the default locale.
	 *
	 * @param section5DescriptionMap the locales and localized section5 descriptions of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5DescriptionMap(
		Map<java.util.Locale, String> section5DescriptionMap,
		java.util.Locale defaultLocale) {

		model.setSection5DescriptionMap(section5DescriptionMap, defaultLocale);
	}

	/**
	 * Sets the section5 image of this residence.
	 *
	 * @param section5Image the section5 image of this residence
	 */
	@Override
	public void setSection5Image(String section5Image) {
		model.setSection5Image(section5Image);
	}

	/**
	 * Sets the localized section5 image of this residence in the language.
	 *
	 * @param section5Image the localized section5 image of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5Image(
		String section5Image, java.util.Locale locale) {

		model.setSection5Image(section5Image, locale);
	}

	/**
	 * Sets the localized section5 image of this residence in the language, and sets the default locale.
	 *
	 * @param section5Image the localized section5 image of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5Image(
		String section5Image, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5Image(section5Image, locale, defaultLocale);
	}

	@Override
	public void setSection5ImageCurrentLanguageId(String languageId) {
		model.setSection5ImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 images of this residence from the map of locales and localized section5 images.
	 *
	 * @param section5ImageMap the locales and localized section5 images of this residence
	 */
	@Override
	public void setSection5ImageMap(
		Map<java.util.Locale, String> section5ImageMap) {

		model.setSection5ImageMap(section5ImageMap);
	}

	/**
	 * Sets the localized section5 images of this residence from the map of locales and localized section5 images, and sets the default locale.
	 *
	 * @param section5ImageMap the locales and localized section5 images of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5ImageMap(
		Map<java.util.Locale, String> section5ImageMap,
		java.util.Locale defaultLocale) {

		model.setSection5ImageMap(section5ImageMap, defaultLocale);
	}

	/**
	 * Sets the section5 subtitle of this residence.
	 *
	 * @param section5Subtitle the section5 subtitle of this residence
	 */
	@Override
	public void setSection5Subtitle(String section5Subtitle) {
		model.setSection5Subtitle(section5Subtitle);
	}

	/**
	 * Sets the localized section5 subtitle of this residence in the language.
	 *
	 * @param section5Subtitle the localized section5 subtitle of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5Subtitle(
		String section5Subtitle, java.util.Locale locale) {

		model.setSection5Subtitle(section5Subtitle, locale);
	}

	/**
	 * Sets the localized section5 subtitle of this residence in the language, and sets the default locale.
	 *
	 * @param section5Subtitle the localized section5 subtitle of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5Subtitle(
		String section5Subtitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5Subtitle(section5Subtitle, locale, defaultLocale);
	}

	@Override
	public void setSection5SubtitleCurrentLanguageId(String languageId) {
		model.setSection5SubtitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 subtitles of this residence from the map of locales and localized section5 subtitles.
	 *
	 * @param section5SubtitleMap the locales and localized section5 subtitles of this residence
	 */
	@Override
	public void setSection5SubtitleMap(
		Map<java.util.Locale, String> section5SubtitleMap) {

		model.setSection5SubtitleMap(section5SubtitleMap);
	}

	/**
	 * Sets the localized section5 subtitles of this residence from the map of locales and localized section5 subtitles, and sets the default locale.
	 *
	 * @param section5SubtitleMap the locales and localized section5 subtitles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5SubtitleMap(
		Map<java.util.Locale, String> section5SubtitleMap,
		java.util.Locale defaultLocale) {

		model.setSection5SubtitleMap(section5SubtitleMap, defaultLocale);
	}

	/**
	 * Sets the section5 title of this residence.
	 *
	 * @param section5Title the section5 title of this residence
	 */
	@Override
	public void setSection5Title(String section5Title) {
		model.setSection5Title(section5Title);
	}

	/**
	 * Sets the localized section5 title of this residence in the language.
	 *
	 * @param section5Title the localized section5 title of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5Title(
		String section5Title, java.util.Locale locale) {

		model.setSection5Title(section5Title, locale);
	}

	/**
	 * Sets the localized section5 title of this residence in the language, and sets the default locale.
	 *
	 * @param section5Title the localized section5 title of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5Title(
		String section5Title, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5Title(section5Title, locale, defaultLocale);
	}

	@Override
	public void setSection5TitleCurrentLanguageId(String languageId) {
		model.setSection5TitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 titles of this residence from the map of locales and localized section5 titles.
	 *
	 * @param section5TitleMap the locales and localized section5 titles of this residence
	 */
	@Override
	public void setSection5TitleMap(
		Map<java.util.Locale, String> section5TitleMap) {

		model.setSection5TitleMap(section5TitleMap);
	}

	/**
	 * Sets the localized section5 titles of this residence from the map of locales and localized section5 titles, and sets the default locale.
	 *
	 * @param section5TitleMap the locales and localized section5 titles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5TitleMap(
		Map<java.util.Locale, String> section5TitleMap,
		java.util.Locale defaultLocale) {

		model.setSection5TitleMap(section5TitleMap, defaultLocale);
	}

	/**
	 * Sets the section6 button label of this residence.
	 *
	 * @param section6ButtonLabel the section6 button label of this residence
	 */
	@Override
	public void setSection6ButtonLabel(String section6ButtonLabel) {
		model.setSection6ButtonLabel(section6ButtonLabel);
	}

	/**
	 * Sets the localized section6 button label of this residence in the language.
	 *
	 * @param section6ButtonLabel the localized section6 button label of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection6ButtonLabel(
		String section6ButtonLabel, java.util.Locale locale) {

		model.setSection6ButtonLabel(section6ButtonLabel, locale);
	}

	/**
	 * Sets the localized section6 button label of this residence in the language, and sets the default locale.
	 *
	 * @param section6ButtonLabel the localized section6 button label of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6ButtonLabel(
		String section6ButtonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection6ButtonLabel(
			section6ButtonLabel, locale, defaultLocale);
	}

	@Override
	public void setSection6ButtonLabelCurrentLanguageId(String languageId) {
		model.setSection6ButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section6 button labels of this residence from the map of locales and localized section6 button labels.
	 *
	 * @param section6ButtonLabelMap the locales and localized section6 button labels of this residence
	 */
	@Override
	public void setSection6ButtonLabelMap(
		Map<java.util.Locale, String> section6ButtonLabelMap) {

		model.setSection6ButtonLabelMap(section6ButtonLabelMap);
	}

	/**
	 * Sets the localized section6 button labels of this residence from the map of locales and localized section6 button labels, and sets the default locale.
	 *
	 * @param section6ButtonLabelMap the locales and localized section6 button labels of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6ButtonLabelMap(
		Map<java.util.Locale, String> section6ButtonLabelMap,
		java.util.Locale defaultLocale) {

		model.setSection6ButtonLabelMap(section6ButtonLabelMap, defaultLocale);
	}

	/**
	 * Sets the section6 button link of this residence.
	 *
	 * @param section6ButtonLink the section6 button link of this residence
	 */
	@Override
	public void setSection6ButtonLink(String section6ButtonLink) {
		model.setSection6ButtonLink(section6ButtonLink);
	}

	/**
	 * Sets the localized section6 button link of this residence in the language.
	 *
	 * @param section6ButtonLink the localized section6 button link of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection6ButtonLink(
		String section6ButtonLink, java.util.Locale locale) {

		model.setSection6ButtonLink(section6ButtonLink, locale);
	}

	/**
	 * Sets the localized section6 button link of this residence in the language, and sets the default locale.
	 *
	 * @param section6ButtonLink the localized section6 button link of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6ButtonLink(
		String section6ButtonLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection6ButtonLink(section6ButtonLink, locale, defaultLocale);
	}

	@Override
	public void setSection6ButtonLinkCurrentLanguageId(String languageId) {
		model.setSection6ButtonLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section6 button links of this residence from the map of locales and localized section6 button links.
	 *
	 * @param section6ButtonLinkMap the locales and localized section6 button links of this residence
	 */
	@Override
	public void setSection6ButtonLinkMap(
		Map<java.util.Locale, String> section6ButtonLinkMap) {

		model.setSection6ButtonLinkMap(section6ButtonLinkMap);
	}

	/**
	 * Sets the localized section6 button links of this residence from the map of locales and localized section6 button links, and sets the default locale.
	 *
	 * @param section6ButtonLinkMap the locales and localized section6 button links of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6ButtonLinkMap(
		Map<java.util.Locale, String> section6ButtonLinkMap,
		java.util.Locale defaultLocale) {

		model.setSection6ButtonLinkMap(section6ButtonLinkMap, defaultLocale);
	}

	/**
	 * Sets the section6 description of this residence.
	 *
	 * @param section6Description the section6 description of this residence
	 */
	@Override
	public void setSection6Description(String section6Description) {
		model.setSection6Description(section6Description);
	}

	/**
	 * Sets the localized section6 description of this residence in the language.
	 *
	 * @param section6Description the localized section6 description of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection6Description(
		String section6Description, java.util.Locale locale) {

		model.setSection6Description(section6Description, locale);
	}

	/**
	 * Sets the localized section6 description of this residence in the language, and sets the default locale.
	 *
	 * @param section6Description the localized section6 description of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6Description(
		String section6Description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection6Description(
			section6Description, locale, defaultLocale);
	}

	@Override
	public void setSection6DescriptionCurrentLanguageId(String languageId) {
		model.setSection6DescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section6 descriptions of this residence from the map of locales and localized section6 descriptions.
	 *
	 * @param section6DescriptionMap the locales and localized section6 descriptions of this residence
	 */
	@Override
	public void setSection6DescriptionMap(
		Map<java.util.Locale, String> section6DescriptionMap) {

		model.setSection6DescriptionMap(section6DescriptionMap);
	}

	/**
	 * Sets the localized section6 descriptions of this residence from the map of locales and localized section6 descriptions, and sets the default locale.
	 *
	 * @param section6DescriptionMap the locales and localized section6 descriptions of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6DescriptionMap(
		Map<java.util.Locale, String> section6DescriptionMap,
		java.util.Locale defaultLocale) {

		model.setSection6DescriptionMap(section6DescriptionMap, defaultLocale);
	}

	/**
	 * Sets the section6 image of this residence.
	 *
	 * @param section6Image the section6 image of this residence
	 */
	@Override
	public void setSection6Image(String section6Image) {
		model.setSection6Image(section6Image);
	}

	/**
	 * Sets the localized section6 image of this residence in the language.
	 *
	 * @param section6Image the localized section6 image of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection6Image(
		String section6Image, java.util.Locale locale) {

		model.setSection6Image(section6Image, locale);
	}

	/**
	 * Sets the localized section6 image of this residence in the language, and sets the default locale.
	 *
	 * @param section6Image the localized section6 image of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6Image(
		String section6Image, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection6Image(section6Image, locale, defaultLocale);
	}

	@Override
	public void setSection6ImageCurrentLanguageId(String languageId) {
		model.setSection6ImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section6 images of this residence from the map of locales and localized section6 images.
	 *
	 * @param section6ImageMap the locales and localized section6 images of this residence
	 */
	@Override
	public void setSection6ImageMap(
		Map<java.util.Locale, String> section6ImageMap) {

		model.setSection6ImageMap(section6ImageMap);
	}

	/**
	 * Sets the localized section6 images of this residence from the map of locales and localized section6 images, and sets the default locale.
	 *
	 * @param section6ImageMap the locales and localized section6 images of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6ImageMap(
		Map<java.util.Locale, String> section6ImageMap,
		java.util.Locale defaultLocale) {

		model.setSection6ImageMap(section6ImageMap, defaultLocale);
	}

	/**
	 * Sets the section6 subtitle of this residence.
	 *
	 * @param section6Subtitle the section6 subtitle of this residence
	 */
	@Override
	public void setSection6Subtitle(String section6Subtitle) {
		model.setSection6Subtitle(section6Subtitle);
	}

	/**
	 * Sets the localized section6 subtitle of this residence in the language.
	 *
	 * @param section6Subtitle the localized section6 subtitle of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection6Subtitle(
		String section6Subtitle, java.util.Locale locale) {

		model.setSection6Subtitle(section6Subtitle, locale);
	}

	/**
	 * Sets the localized section6 subtitle of this residence in the language, and sets the default locale.
	 *
	 * @param section6Subtitle the localized section6 subtitle of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6Subtitle(
		String section6Subtitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection6Subtitle(section6Subtitle, locale, defaultLocale);
	}

	@Override
	public void setSection6SubtitleCurrentLanguageId(String languageId) {
		model.setSection6SubtitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section6 subtitles of this residence from the map of locales and localized section6 subtitles.
	 *
	 * @param section6SubtitleMap the locales and localized section6 subtitles of this residence
	 */
	@Override
	public void setSection6SubtitleMap(
		Map<java.util.Locale, String> section6SubtitleMap) {

		model.setSection6SubtitleMap(section6SubtitleMap);
	}

	/**
	 * Sets the localized section6 subtitles of this residence from the map of locales and localized section6 subtitles, and sets the default locale.
	 *
	 * @param section6SubtitleMap the locales and localized section6 subtitles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6SubtitleMap(
		Map<java.util.Locale, String> section6SubtitleMap,
		java.util.Locale defaultLocale) {

		model.setSection6SubtitleMap(section6SubtitleMap, defaultLocale);
	}

	/**
	 * Sets the section6 title of this residence.
	 *
	 * @param section6Title the section6 title of this residence
	 */
	@Override
	public void setSection6Title(String section6Title) {
		model.setSection6Title(section6Title);
	}

	/**
	 * Sets the localized section6 title of this residence in the language.
	 *
	 * @param section6Title the localized section6 title of this residence
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection6Title(
		String section6Title, java.util.Locale locale) {

		model.setSection6Title(section6Title, locale);
	}

	/**
	 * Sets the localized section6 title of this residence in the language, and sets the default locale.
	 *
	 * @param section6Title the localized section6 title of this residence
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6Title(
		String section6Title, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection6Title(section6Title, locale, defaultLocale);
	}

	@Override
	public void setSection6TitleCurrentLanguageId(String languageId) {
		model.setSection6TitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section6 titles of this residence from the map of locales and localized section6 titles.
	 *
	 * @param section6TitleMap the locales and localized section6 titles of this residence
	 */
	@Override
	public void setSection6TitleMap(
		Map<java.util.Locale, String> section6TitleMap) {

		model.setSection6TitleMap(section6TitleMap);
	}

	/**
	 * Sets the localized section6 titles of this residence from the map of locales and localized section6 titles, and sets the default locale.
	 *
	 * @param section6TitleMap the locales and localized section6 titles of this residence
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection6TitleMap(
		Map<java.util.Locale, String> section6TitleMap,
		java.util.Locale defaultLocale) {

		model.setSection6TitleMap(section6TitleMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this residence.
	 *
	 * @param userId the user ID of this residence
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this residence.
	 *
	 * @param userName the user name of this residence
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this residence.
	 *
	 * @param userUuid the user uuid of this residence
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this residence.
	 *
	 * @param uuid the uuid of this residence
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected ResidenceWrapper wrap(Residence residence) {
		return new ResidenceWrapper(residence);
	}

}