/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ResidenceSlider4}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider4
 * @generated
 */
public class ResidenceSlider4Wrapper
	extends BaseModelWrapper<ResidenceSlider4>
	implements ModelWrapper<ResidenceSlider4>, ResidenceSlider4 {

	public ResidenceSlider4Wrapper(ResidenceSlider4 residenceSlider4) {
		super(residenceSlider4);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("slideId", getSlideId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("residenceId", getResidenceId());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("image", getImage());
		attributes.put("buttonLabel", getButtonLabel());
		attributes.put("buttonLink", getButtonLink());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long slideId = (Long)attributes.get("slideId");

		if (slideId != null) {
			setSlideId(slideId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long residenceId = (Long)attributes.get("residenceId");

		if (residenceId != null) {
			setResidenceId(residenceId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String buttonLabel = (String)attributes.get("buttonLabel");

		if (buttonLabel != null) {
			setButtonLabel(buttonLabel);
		}

		String buttonLink = (String)attributes.get("buttonLink");

		if (buttonLink != null) {
			setButtonLink(buttonLink);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the button label of this residence slider4.
	 *
	 * @return the button label of this residence slider4
	 */
	@Override
	public String getButtonLabel() {
		return model.getButtonLabel();
	}

	/**
	 * Returns the localized button label of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button label of this residence slider4
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale) {
		return model.getButtonLabel(locale);
	}

	/**
	 * Returns the localized button label of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this residence slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale, boolean useDefault) {
		return model.getButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized button label of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button label of this residence slider4
	 */
	@Override
	public String getButtonLabel(String languageId) {
		return model.getButtonLabel(languageId);
	}

	/**
	 * Returns the localized button label of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this residence slider4
	 */
	@Override
	public String getButtonLabel(String languageId, boolean useDefault) {
		return model.getButtonLabel(languageId, useDefault);
	}

	@Override
	public String getButtonLabelCurrentLanguageId() {
		return model.getButtonLabelCurrentLanguageId();
	}

	@Override
	public String getButtonLabelCurrentValue() {
		return model.getButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized button labels of this residence slider4.
	 *
	 * @return the locales and localized button labels of this residence slider4
	 */
	@Override
	public Map<java.util.Locale, String> getButtonLabelMap() {
		return model.getButtonLabelMap();
	}

	/**
	 * Returns the button link of this residence slider4.
	 *
	 * @return the button link of this residence slider4
	 */
	@Override
	public String getButtonLink() {
		return model.getButtonLink();
	}

	/**
	 * Returns the localized button link of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button link of this residence slider4
	 */
	@Override
	public String getButtonLink(java.util.Locale locale) {
		return model.getButtonLink(locale);
	}

	/**
	 * Returns the localized button link of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this residence slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getButtonLink(java.util.Locale locale, boolean useDefault) {
		return model.getButtonLink(locale, useDefault);
	}

	/**
	 * Returns the localized button link of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button link of this residence slider4
	 */
	@Override
	public String getButtonLink(String languageId) {
		return model.getButtonLink(languageId);
	}

	/**
	 * Returns the localized button link of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this residence slider4
	 */
	@Override
	public String getButtonLink(String languageId, boolean useDefault) {
		return model.getButtonLink(languageId, useDefault);
	}

	@Override
	public String getButtonLinkCurrentLanguageId() {
		return model.getButtonLinkCurrentLanguageId();
	}

	@Override
	public String getButtonLinkCurrentValue() {
		return model.getButtonLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized button links of this residence slider4.
	 *
	 * @return the locales and localized button links of this residence slider4
	 */
	@Override
	public Map<java.util.Locale, String> getButtonLinkMap() {
		return model.getButtonLinkMap();
	}

	/**
	 * Returns the company ID of this residence slider4.
	 *
	 * @return the company ID of this residence slider4
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this residence slider4.
	 *
	 * @return the create date of this residence slider4
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this residence slider4.
	 *
	 * @return the description of this residence slider4
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this residence slider4
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this residence slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this residence slider4
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this residence slider4
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this residence slider4.
	 *
	 * @return the locales and localized descriptions of this residence slider4
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the image of this residence slider4.
	 *
	 * @return the image of this residence slider4
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the localized image of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this residence slider4
	 */
	@Override
	public String getImage(java.util.Locale locale) {
		return model.getImage(locale);
	}

	/**
	 * Returns the localized image of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this residence slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getImage(java.util.Locale locale, boolean useDefault) {
		return model.getImage(locale, useDefault);
	}

	/**
	 * Returns the localized image of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this residence slider4
	 */
	@Override
	public String getImage(String languageId) {
		return model.getImage(languageId);
	}

	/**
	 * Returns the localized image of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this residence slider4
	 */
	@Override
	public String getImage(String languageId, boolean useDefault) {
		return model.getImage(languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return model.getImageCurrentLanguageId();
	}

	@Override
	public String getImageCurrentValue() {
		return model.getImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized images of this residence slider4.
	 *
	 * @return the locales and localized images of this residence slider4
	 */
	@Override
	public Map<java.util.Locale, String> getImageMap() {
		return model.getImageMap();
	}

	/**
	 * Returns the modified date of this residence slider4.
	 *
	 * @return the modified date of this residence slider4
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this residence slider4.
	 *
	 * @return the primary key of this residence slider4
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the residence ID of this residence slider4.
	 *
	 * @return the residence ID of this residence slider4
	 */
	@Override
	public long getResidenceId() {
		return model.getResidenceId();
	}

	/**
	 * Returns the slide ID of this residence slider4.
	 *
	 * @return the slide ID of this residence slider4
	 */
	@Override
	public long getSlideId() {
		return model.getSlideId();
	}

	/**
	 * Returns the title of this residence slider4.
	 *
	 * @return the title of this residence slider4
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this residence slider4
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this residence slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this residence slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this residence slider4
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this residence slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this residence slider4
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this residence slider4.
	 *
	 * @return the locales and localized titles of this residence slider4
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the user ID of this residence slider4.
	 *
	 * @return the user ID of this residence slider4
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this residence slider4.
	 *
	 * @return the user name of this residence slider4
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this residence slider4.
	 *
	 * @return the user uuid of this residence slider4
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this residence slider4.
	 *
	 * @return the uuid of this residence slider4
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the button label of this residence slider4.
	 *
	 * @param buttonLabel the button label of this residence slider4
	 */
	@Override
	public void setButtonLabel(String buttonLabel) {
		model.setButtonLabel(buttonLabel);
	}

	/**
	 * Sets the localized button label of this residence slider4 in the language.
	 *
	 * @param buttonLabel the localized button label of this residence slider4
	 * @param locale the locale of the language
	 */
	@Override
	public void setButtonLabel(String buttonLabel, java.util.Locale locale) {
		model.setButtonLabel(buttonLabel, locale);
	}

	/**
	 * Sets the localized button label of this residence slider4 in the language, and sets the default locale.
	 *
	 * @param buttonLabel the localized button label of this residence slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabel(
		String buttonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setButtonLabel(buttonLabel, locale, defaultLocale);
	}

	@Override
	public void setButtonLabelCurrentLanguageId(String languageId) {
		model.setButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized button labels of this residence slider4 from the map of locales and localized button labels.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this residence slider4
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap) {

		model.setButtonLabelMap(buttonLabelMap);
	}

	/**
	 * Sets the localized button labels of this residence slider4 from the map of locales and localized button labels, and sets the default locale.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this residence slider4
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap,
		java.util.Locale defaultLocale) {

		model.setButtonLabelMap(buttonLabelMap, defaultLocale);
	}

	/**
	 * Sets the button link of this residence slider4.
	 *
	 * @param buttonLink the button link of this residence slider4
	 */
	@Override
	public void setButtonLink(String buttonLink) {
		model.setButtonLink(buttonLink);
	}

	/**
	 * Sets the localized button link of this residence slider4 in the language.
	 *
	 * @param buttonLink the localized button link of this residence slider4
	 * @param locale the locale of the language
	 */
	@Override
	public void setButtonLink(String buttonLink, java.util.Locale locale) {
		model.setButtonLink(buttonLink, locale);
	}

	/**
	 * Sets the localized button link of this residence slider4 in the language, and sets the default locale.
	 *
	 * @param buttonLink the localized button link of this residence slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLink(
		String buttonLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setButtonLink(buttonLink, locale, defaultLocale);
	}

	@Override
	public void setButtonLinkCurrentLanguageId(String languageId) {
		model.setButtonLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized button links of this residence slider4 from the map of locales and localized button links.
	 *
	 * @param buttonLinkMap the locales and localized button links of this residence slider4
	 */
	@Override
	public void setButtonLinkMap(Map<java.util.Locale, String> buttonLinkMap) {
		model.setButtonLinkMap(buttonLinkMap);
	}

	/**
	 * Sets the localized button links of this residence slider4 from the map of locales and localized button links, and sets the default locale.
	 *
	 * @param buttonLinkMap the locales and localized button links of this residence slider4
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLinkMap(
		Map<java.util.Locale, String> buttonLinkMap,
		java.util.Locale defaultLocale) {

		model.setButtonLinkMap(buttonLinkMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this residence slider4.
	 *
	 * @param companyId the company ID of this residence slider4
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this residence slider4.
	 *
	 * @param createDate the create date of this residence slider4
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this residence slider4.
	 *
	 * @param description the description of this residence slider4
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this residence slider4 in the language.
	 *
	 * @param description the localized description of this residence slider4
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this residence slider4 in the language, and sets the default locale.
	 *
	 * @param description the localized description of this residence slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this residence slider4 from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this residence slider4
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this residence slider4 from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this residence slider4
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the image of this residence slider4.
	 *
	 * @param image the image of this residence slider4
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the localized image of this residence slider4 in the language.
	 *
	 * @param image the localized image of this residence slider4
	 * @param locale the locale of the language
	 */
	@Override
	public void setImage(String image, java.util.Locale locale) {
		model.setImage(image, locale);
	}

	/**
	 * Sets the localized image of this residence slider4 in the language, and sets the default locale.
	 *
	 * @param image the localized image of this residence slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImage(
		String image, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setImage(image, locale, defaultLocale);
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		model.setImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized images of this residence slider4 from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this residence slider4
	 */
	@Override
	public void setImageMap(Map<java.util.Locale, String> imageMap) {
		model.setImageMap(imageMap);
	}

	/**
	 * Sets the localized images of this residence slider4 from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this residence slider4
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImageMap(
		Map<java.util.Locale, String> imageMap,
		java.util.Locale defaultLocale) {

		model.setImageMap(imageMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this residence slider4.
	 *
	 * @param modifiedDate the modified date of this residence slider4
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this residence slider4.
	 *
	 * @param primaryKey the primary key of this residence slider4
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the residence ID of this residence slider4.
	 *
	 * @param residenceId the residence ID of this residence slider4
	 */
	@Override
	public void setResidenceId(long residenceId) {
		model.setResidenceId(residenceId);
	}

	/**
	 * Sets the slide ID of this residence slider4.
	 *
	 * @param slideId the slide ID of this residence slider4
	 */
	@Override
	public void setSlideId(long slideId) {
		model.setSlideId(slideId);
	}

	/**
	 * Sets the title of this residence slider4.
	 *
	 * @param title the title of this residence slider4
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this residence slider4 in the language.
	 *
	 * @param title the localized title of this residence slider4
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this residence slider4 in the language, and sets the default locale.
	 *
	 * @param title the localized title of this residence slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this residence slider4 from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this residence slider4
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this residence slider4 from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this residence slider4
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this residence slider4.
	 *
	 * @param userId the user ID of this residence slider4
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this residence slider4.
	 *
	 * @param userName the user name of this residence slider4
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this residence slider4.
	 *
	 * @param userUuid the user uuid of this residence slider4
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this residence slider4.
	 *
	 * @param uuid the uuid of this residence slider4
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected ResidenceSlider4Wrapper wrap(ResidenceSlider4 residenceSlider4) {
		return new ResidenceSlider4Wrapper(residenceSlider4);
	}

}