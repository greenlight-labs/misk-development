/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the ResidenceSlider3 service. Represents a row in the &quot;misk_residences_slider3&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3Model
 * @generated
 */
@ImplementationClassName("residences.model.impl.ResidenceSlider3Impl")
@ProviderType
public interface ResidenceSlider3
	extends PersistedModel, ResidenceSlider3Model {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>residences.model.impl.ResidenceSlider3Impl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ResidenceSlider3, Long> SLIDE_ID_ACCESSOR =
		new Accessor<ResidenceSlider3, Long>() {

			@Override
			public Long get(ResidenceSlider3 residenceSlider3) {
				return residenceSlider3.getSlideId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<ResidenceSlider3> getTypeClass() {
				return ResidenceSlider3.class;
			}

		};

}