/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.base;

import com.liferay.exportimport.kernel.lar.ExportImportHelperUtil;
import com.liferay.exportimport.kernel.lar.ManifestSummary;
import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.exportimport.kernel.lar.StagedModelDataHandlerUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.persistence.BasePersistence;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Field;

import java.util.List;

import javax.sql.DataSource;

import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import residences.model.ResidenceSlider4;

import residences.service.ResidenceSlider4LocalService;
import residences.service.ResidenceSlider4LocalServiceUtil;
import residences.service.persistence.ResidenceFinder;
import residences.service.persistence.ResidencePersistence;
import residences.service.persistence.ResidenceSlider3Persistence;
import residences.service.persistence.ResidenceSlider4Persistence;

/**
 * Provides the base implementation for the residence slider4 local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link residences.service.impl.ResidenceSlider4LocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see residences.service.impl.ResidenceSlider4LocalServiceImpl
 * @generated
 */
public abstract class ResidenceSlider4LocalServiceBaseImpl
	extends BaseLocalServiceImpl
	implements AopService, IdentifiableOSGiService,
			   ResidenceSlider4LocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Use <code>ResidenceSlider4LocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>ResidenceSlider4LocalServiceUtil</code>.
	 */

	/**
	 * Adds the residence slider4 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider4 the residence slider4
	 * @return the residence slider4 that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public ResidenceSlider4 addResidenceSlider4(
		ResidenceSlider4 residenceSlider4) {

		residenceSlider4.setNew(true);

		return residenceSlider4Persistence.update(residenceSlider4);
	}

	/**
	 * Creates a new residence slider4 with the primary key. Does not add the residence slider4 to the database.
	 *
	 * @param slideId the primary key for the new residence slider4
	 * @return the new residence slider4
	 */
	@Override
	@Transactional(enabled = false)
	public ResidenceSlider4 createResidenceSlider4(long slideId) {
		return residenceSlider4Persistence.create(slideId);
	}

	/**
	 * Deletes the residence slider4 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the residence slider4
	 * @return the residence slider4 that was removed
	 * @throws PortalException if a residence slider4 with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public ResidenceSlider4 deleteResidenceSlider4(long slideId)
		throws PortalException {

		return residenceSlider4Persistence.remove(slideId);
	}

	/**
	 * Deletes the residence slider4 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider4 the residence slider4
	 * @return the residence slider4 that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public ResidenceSlider4 deleteResidenceSlider4(
		ResidenceSlider4 residenceSlider4) {

		return residenceSlider4Persistence.remove(residenceSlider4);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(
			ResidenceSlider4.class, clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return residenceSlider4Persistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return residenceSlider4Persistence.findWithDynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return residenceSlider4Persistence.findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return residenceSlider4Persistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection) {

		return residenceSlider4Persistence.countWithDynamicQuery(
			dynamicQuery, projection);
	}

	@Override
	public ResidenceSlider4 fetchResidenceSlider4(long slideId) {
		return residenceSlider4Persistence.fetchByPrimaryKey(slideId);
	}

	/**
	 * Returns the residence slider4 with the matching UUID and company.
	 *
	 * @param uuid the residence slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider4, or <code>null</code> if a matching residence slider4 could not be found
	 */
	@Override
	public ResidenceSlider4 fetchResidenceSlider4ByUuidAndCompanyId(
		String uuid, long companyId) {

		return residenceSlider4Persistence.fetchByUuid_C_First(
			uuid, companyId, null);
	}

	/**
	 * Returns the residence slider4 with the primary key.
	 *
	 * @param slideId the primary key of the residence slider4
	 * @return the residence slider4
	 * @throws PortalException if a residence slider4 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider4 getResidenceSlider4(long slideId)
		throws PortalException {

		return residenceSlider4Persistence.findByPrimaryKey(slideId);
	}

	@Override
	public ActionableDynamicQuery getActionableDynamicQuery() {
		ActionableDynamicQuery actionableDynamicQuery =
			new DefaultActionableDynamicQuery();

		actionableDynamicQuery.setBaseLocalService(
			residenceSlider4LocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(ResidenceSlider4.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("slideId");

		return actionableDynamicQuery;
	}

	@Override
	public IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		IndexableActionableDynamicQuery indexableActionableDynamicQuery =
			new IndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setBaseLocalService(
			residenceSlider4LocalService);
		indexableActionableDynamicQuery.setClassLoader(getClassLoader());
		indexableActionableDynamicQuery.setModelClass(ResidenceSlider4.class);

		indexableActionableDynamicQuery.setPrimaryKeyPropertyName("slideId");

		return indexableActionableDynamicQuery;
	}

	protected void initActionableDynamicQuery(
		ActionableDynamicQuery actionableDynamicQuery) {

		actionableDynamicQuery.setBaseLocalService(
			residenceSlider4LocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(ResidenceSlider4.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("slideId");
	}

	@Override
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		final PortletDataContext portletDataContext) {

		final ExportActionableDynamicQuery exportActionableDynamicQuery =
			new ExportActionableDynamicQuery() {

				@Override
				public long performCount() throws PortalException {
					ManifestSummary manifestSummary =
						portletDataContext.getManifestSummary();

					StagedModelType stagedModelType = getStagedModelType();

					long modelAdditionCount = super.performCount();

					manifestSummary.addModelAdditionCount(
						stagedModelType, modelAdditionCount);

					long modelDeletionCount =
						ExportImportHelperUtil.getModelDeletionCount(
							portletDataContext, stagedModelType);

					manifestSummary.addModelDeletionCount(
						stagedModelType, modelDeletionCount);

					return modelAdditionCount;
				}

			};

		initActionableDynamicQuery(exportActionableDynamicQuery);

		exportActionableDynamicQuery.setAddCriteriaMethod(
			new ActionableDynamicQuery.AddCriteriaMethod() {

				@Override
				public void addCriteria(DynamicQuery dynamicQuery) {
					portletDataContext.addDateRangeCriteria(
						dynamicQuery, "modifiedDate");
				}

			});

		exportActionableDynamicQuery.setCompanyId(
			portletDataContext.getCompanyId());

		exportActionableDynamicQuery.setPerformActionMethod(
			new ActionableDynamicQuery.PerformActionMethod<ResidenceSlider4>() {

				@Override
				public void performAction(ResidenceSlider4 residenceSlider4)
					throws PortalException {

					StagedModelDataHandlerUtil.exportStagedModel(
						portletDataContext, residenceSlider4);
				}

			});
		exportActionableDynamicQuery.setStagedModelType(
			new StagedModelType(
				PortalUtil.getClassNameId(ResidenceSlider4.class.getName())));

		return exportActionableDynamicQuery;
	}

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return residenceSlider4Persistence.create(
			((Long)primaryKeyObj).longValue());
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {

		return residenceSlider4LocalService.deleteResidenceSlider4(
			(ResidenceSlider4)persistedModel);
	}

	public BasePersistence<ResidenceSlider4> getBasePersistence() {
		return residenceSlider4Persistence;
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return residenceSlider4Persistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns the residence slider4 with the matching UUID and company.
	 *
	 * @param uuid the residence slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching residence slider4
	 * @throws PortalException if a matching residence slider4 could not be found
	 */
	@Override
	public ResidenceSlider4 getResidenceSlider4ByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return residenceSlider4Persistence.findByUuid_C_First(
			uuid, companyId, null);
	}

	/**
	 * Returns a range of all the residence slider4s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>residences.model.impl.ResidenceSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider4s
	 * @param end the upper bound of the range of residence slider4s (not inclusive)
	 * @return the range of residence slider4s
	 */
	@Override
	public List<ResidenceSlider4> getResidenceSlider4s(int start, int end) {
		return residenceSlider4Persistence.findAll(start, end);
	}

	/**
	 * Returns the number of residence slider4s.
	 *
	 * @return the number of residence slider4s
	 */
	@Override
	public int getResidenceSlider4sCount() {
		return residenceSlider4Persistence.countAll();
	}

	/**
	 * Updates the residence slider4 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ResidenceSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param residenceSlider4 the residence slider4
	 * @return the residence slider4 that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public ResidenceSlider4 updateResidenceSlider4(
		ResidenceSlider4 residenceSlider4) {

		return residenceSlider4Persistence.update(residenceSlider4);
	}

	@Deactivate
	protected void deactivate() {
		_setLocalServiceUtilService(null);
	}

	@Override
	public Class<?>[] getAopInterfaces() {
		return new Class<?>[] {
			ResidenceSlider4LocalService.class, IdentifiableOSGiService.class,
			PersistedModelLocalService.class
		};
	}

	@Override
	public void setAopProxy(Object aopProxy) {
		residenceSlider4LocalService = (ResidenceSlider4LocalService)aopProxy;

		_setLocalServiceUtilService(residenceSlider4LocalService);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return ResidenceSlider4LocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return ResidenceSlider4.class;
	}

	protected String getModelClassName() {
		return ResidenceSlider4.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = residenceSlider4Persistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(
				dataSource, sql);

			sqlUpdate.update();
		}
		catch (Exception exception) {
			throw new SystemException(exception);
		}
	}

	private void _setLocalServiceUtilService(
		ResidenceSlider4LocalService residenceSlider4LocalService) {

		try {
			Field field =
				ResidenceSlider4LocalServiceUtil.class.getDeclaredField(
					"_service");

			field.setAccessible(true);

			field.set(null, residenceSlider4LocalService);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Reference
	protected ResidencePersistence residencePersistence;

	@Reference
	protected ResidenceFinder residenceFinder;

	@Reference
	protected ResidenceSlider3Persistence residenceSlider3Persistence;

	protected ResidenceSlider4LocalService residenceSlider4LocalService;

	@Reference
	protected ResidenceSlider4Persistence residenceSlider4Persistence;

	@Reference
	protected com.liferay.counter.kernel.service.CounterLocalService
		counterLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ClassNameLocalService
		classNameLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.ResourceLocalService
		resourceLocalService;

	@Reference
	protected com.liferay.portal.kernel.service.UserLocalService
		userLocalService;

}