/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import residences.exception.ResidenceListingDescriptionException;
import residences.exception.ResidenceListingImageException;
import residences.exception.ResidenceListingTitleException;
import residences.model.Residence;
import residences.service.base.ResidenceLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the residence local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>residences.service.ResidenceLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceLocalServiceBaseImpl
 */
@Component(
        property = "model.class.name=residences.model.Residence",
        service = AopService.class
)
public class ResidenceLocalServiceImpl extends ResidenceLocalServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Use <code>residences.service.ResidenceLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>residences.service.ResidenceLocalServiceUtil</code>.
     */

    public Residence addResidence(long userId, long residenceId,
                                  Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap,
                                  Map<Locale, String> bannerTitleMap, Map<Locale, String> bannerSubtitleMap, Map<Locale, String> bannerDescriptionMap, Map<Locale, String> bannerDesktopImageMap, Map<Locale, String> bannerMobileImageMap,
                                  Map<Locale, String> section2TitleMap, Map<Locale, String> section2SubtitleMap, Map<Locale, String> section2DescriptionMap, Map<Locale, String> section2ButtonLabelMap, Map<Locale, String> section2ButtonLinkMap, Map<Locale, String> section2ImageMap,
                                  Map<Locale, String> section5TitleMap, Map<Locale, String> section5SubtitleMap, Map<Locale, String> section5DescriptionMap, Map<Locale, String> section5ButtonLabelMap, Map<Locale, String> section5ButtonLinkMap, Map<Locale, String> section5ImageMap,
                                  Map<Locale, String> section6TitleMap, Map<Locale, String> section6SubtitleMap, Map<Locale, String> section6DescriptionMap, Map<Locale, String> section6ButtonLabelMap, Map<Locale, String> section6ButtonLinkMap, Map<Locale, String> section6ImageMap,
                                  Map<Locale, String> section4SectionTitleMap,
                                  ServiceContext serviceContext) throws PortalException {

        long groupId = serviceContext.getScopeGroupId();

        User user = userLocalService.getUserById(userId);

        Date now = new Date();

        validate(listingImageMap, listingTitleMap, listingDescriptionMap);

        //long residenceId = counterLocalService.increment();

        Residence residence = residencePersistence.create(residenceId);

        residence.setUuid(serviceContext.getUuid());
        residence.setUserId(userId);
        residence.setGroupId(groupId);
        residence.setCompanyId(user.getCompanyId());
        residence.setUserName(user.getFullName());
        residence.setCreateDate(serviceContext.getCreateDate(now));
        residence.setModifiedDate(serviceContext.getModifiedDate(now));

        residence.setListingImageMap(listingImageMap);
        residence.setListingTitleMap(listingTitleMap);
        residence.setListingDescriptionMap(listingDescriptionMap);

        residence.setBannerTitleMap(bannerTitleMap);
        residence.setBannerSubtitleMap(bannerSubtitleMap);
        residence.setBannerDescriptionMap(bannerDescriptionMap);
        residence.setBannerDesktopImageMap(bannerDesktopImageMap);
        residence.setBannerMobileImageMap(bannerMobileImageMap);

        residence.setSection2TitleMap(section2TitleMap);
        residence.setSection2SubtitleMap(section2SubtitleMap);
        residence.setSection2DescriptionMap(section2DescriptionMap);
        residence.setSection2ButtonLabelMap(section2ButtonLabelMap);
        residence.setSection2ButtonLinkMap(section2ButtonLinkMap);
        residence.setSection2ImageMap(section2ImageMap);

        residence.setSection5TitleMap(section5TitleMap);
        residence.setSection5SubtitleMap(section5SubtitleMap);
        residence.setSection5DescriptionMap(section5DescriptionMap);
        residence.setSection5ButtonLabelMap(section5ButtonLabelMap);
        residence.setSection5ButtonLinkMap(section5ButtonLinkMap);
        residence.setSection5ImageMap(section5ImageMap);

        residence.setSection6TitleMap(section6TitleMap);
        residence.setSection6SubtitleMap(section6SubtitleMap);
        residence.setSection6DescriptionMap(section6DescriptionMap);
        residence.setSection6ButtonLabelMap(section6ButtonLabelMap);
        residence.setSection6ButtonLinkMap(section6ButtonLinkMap);
        residence.setSection6ImageMap(section6ImageMap);

        residence.setSection4SectionTitleMap(section4SectionTitleMap);

        residence.setExpandoBridgeAttributes(serviceContext);

        residencePersistence.update(residence);
        residencePersistence.clearCache();

        return residence;
    }

    public Residence updateResidence(long userId, long residenceId,
                                     Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap,
                                     Map<Locale, String> bannerTitleMap, Map<Locale, String> bannerSubtitleMap, Map<Locale, String> bannerDescriptionMap, Map<Locale, String> bannerDesktopImageMap, Map<Locale, String> bannerMobileImageMap,
                                     Map<Locale, String> section2TitleMap, Map<Locale, String> section2SubtitleMap, Map<Locale, String> section2DescriptionMap, Map<Locale, String> section2ButtonLabelMap, Map<Locale, String> section2ButtonLinkMap, Map<Locale, String> section2ImageMap,
                                     Map<Locale, String> section5TitleMap, Map<Locale, String> section5SubtitleMap, Map<Locale, String> section5DescriptionMap, Map<Locale, String> section5ButtonLabelMap, Map<Locale, String> section5ButtonLinkMap, Map<Locale, String> section5ImageMap,
                                     Map<Locale, String> section6TitleMap, Map<Locale, String> section6SubtitleMap, Map<Locale, String> section6DescriptionMap, Map<Locale, String> section6ButtonLabelMap, Map<Locale, String> section6ButtonLinkMap, Map<Locale, String> section6ImageMap,
                                     Map<Locale, String> section4SectionTitleMap,
                                     ServiceContext serviceContext) throws PortalException,
            SystemException {

        Date now = new Date();

        validate(listingImageMap, listingTitleMap, listingDescriptionMap);

        Residence residence = getResidence(residenceId);

        User user = userLocalService.getUser(userId);

        residence.setUserId(userId);
        residence.setUserName(user.getFullName());
        residence.setModifiedDate(serviceContext.getModifiedDate(now));

        residence.setListingImageMap(listingImageMap);
        residence.setListingTitleMap(listingTitleMap);
        residence.setListingDescriptionMap(listingDescriptionMap);

        residence.setBannerTitleMap(bannerTitleMap);
        residence.setBannerSubtitleMap(bannerSubtitleMap);
        residence.setBannerDescriptionMap(bannerDescriptionMap);
        residence.setBannerDesktopImageMap(bannerDesktopImageMap);
        residence.setBannerMobileImageMap(bannerMobileImageMap);

        residence.setSection2TitleMap(section2TitleMap);
        residence.setSection2SubtitleMap(section2SubtitleMap);
        residence.setSection2DescriptionMap(section2DescriptionMap);
        residence.setSection2ButtonLabelMap(section2ButtonLabelMap);
        residence.setSection2ButtonLinkMap(section2ButtonLinkMap);
        residence.setSection2ImageMap(section2ImageMap);

        residence.setSection5TitleMap(section5TitleMap);
        residence.setSection5SubtitleMap(section5SubtitleMap);
        residence.setSection5DescriptionMap(section5DescriptionMap);
        residence.setSection5ButtonLabelMap(section5ButtonLabelMap);
        residence.setSection5ButtonLinkMap(section5ButtonLinkMap);
        residence.setSection5ImageMap(section5ImageMap);

        residence.setSection6TitleMap(section6TitleMap);
        residence.setSection6SubtitleMap(section6SubtitleMap);
        residence.setSection6DescriptionMap(section6DescriptionMap);
        residence.setSection6ButtonLabelMap(section6ButtonLabelMap);
        residence.setSection6ButtonLinkMap(section6ButtonLinkMap);
        residence.setSection6ImageMap(section6ImageMap);

        residence.setSection4SectionTitleMap(section4SectionTitleMap);

        residence.setExpandoBridgeAttributes(serviceContext);

        residencePersistence.update(residence);
        residencePersistence.clearCache();

        return residence;
    }

    public Residence deleteResidence(long residenceId,
                                     ServiceContext serviceContext) throws PortalException,
            SystemException {

        Residence residence = getResidence(residenceId);
        residence = deleteResidence(residence);

        return residence;
    }

    public List<Residence> getResidences(long groupId) {

        return residencePersistence.findByGroupId(groupId);
    }

    public List<Residence> getResidences(long groupId, int start, int end,
                                         OrderByComparator<Residence> obc) {

        return residencePersistence.findByGroupId(groupId, start, end, obc);
    }

    public List<Residence> getResidences(long groupId, int start, int end) {

        return residencePersistence.findByGroupId(groupId, start, end);
    }

    public int getResidencesCount(long groupId) {

        return residencePersistence.countByGroupId(groupId);
    }

    protected void validate(
            Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap
    ) throws PortalException {

        Locale locale = LocaleUtil.getSiteDefault();

        String listingImage = listingImageMap.get(locale);

        if (Validator.isNull(listingImage)) {
            throw new ResidenceListingImageException();
        }

        String listingTitle = listingTitleMap.get(locale);

        if (Validator.isNull(listingTitle)) {
            throw new ResidenceListingTitleException();
        }

        String listingDescription = listingDescriptionMap.get(locale);

        if (Validator.isNull(listingDescription)) {
            throw new ResidenceListingDescriptionException();
        }

    }

    public List<Residence> findByT_D(String query, int offset, int limit, String languageId) {

        return residenceFinder.findByT_D(query, offset, limit, languageId);
    }

    public int countByT_D(String query, String languageId) {

        return residenceFinder.countByT_D(query, languageId);
    }

    public List<Object> searchCompleteWebsite(String query, int offset, int limit, String languageId) {

        return residenceFinder.searchCompleteWebsite(query, offset, limit, languageId);
    }

    public int countSearchCompleteWebsite(String query, String languageId) {

        return residenceFinder.countSearchCompleteWebsite(query, languageId);
    }

    /*public List<Residence> getResidences(long groupId, int start, int end,
                                         OrderByComparator<Residence> obc) {

        return residencePersistence.findByGroupId(groupId, start, end, obc);
    }

    public List<Residence> getResidences(long groupId, int start, int end) {

        return residencePersistence.findByGroupId(groupId, start, end);
    }

    public int getResidencesCount(long groupId) {

        return residencePersistence.countByGroupId(groupId);
    }*/
}