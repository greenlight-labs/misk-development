/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import residences.exception.*;
import residences.model.ResidenceSlider3;
import residences.service.base.ResidenceSlider3LocalServiceBaseImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the residence slider3 local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>residences.service.ResidenceSlider3LocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider3LocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=residences.model.ResidenceSlider3",
	service = AopService.class
)
public class ResidenceSlider3LocalServiceImpl
	extends ResidenceSlider3LocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>residences.service.ResidenceSlider3LocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>residences.service.ResidenceSlider3LocalServiceUtil</code>.
	 */

	public ResidenceSlider3 addResidenceSlider3(long userId,
								  long residenceId,
								  Map<Locale, String> section3ImageMap, Map<Locale, String> section3DescriptionMap, Map<Locale, String> section3ThumbnailMap,
								  ServiceContext serviceContext) throws PortalException {

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(section3ImageMap, section3DescriptionMap, section3ThumbnailMap);

		long slideId = counterLocalService.increment();

		ResidenceSlider3 slide = residenceSlider3Persistence.create(slideId);

		slide.setUuid(serviceContext.getUuid());
		slide.setUserId(userId);
		slide.setCompanyId(user.getCompanyId());
		slide.setUserName(user.getFullName());
		slide.setCreateDate(serviceContext.getCreateDate(now));
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setResidenceId(residenceId);
		slide.setImageMap(section3ImageMap);
		slide.setDescriptionMap(section3DescriptionMap);
		slide.setThumbnailMap(section3ThumbnailMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		residenceSlider3Persistence.update(slide);
		residenceSlider3Persistence.clearCache();

		return slide;
	}

	public ResidenceSlider3 updateResidenceSlider3(long userId, long slideId,
									 long residenceId,
									 Map<Locale, String> section3ImageMap, Map<Locale, String> section3DescriptionMap, Map<Locale, String> section3ThumbnailMap,
									 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(section3ImageMap, section3DescriptionMap, section3ThumbnailMap);

		ResidenceSlider3 slide = getResidenceSlider3(slideId);

		User user = userLocalService.getUser(userId);

		slide.setUserId(userId);
		slide.setUserName(user.getFullName());
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setResidenceId(residenceId);
		slide.setImageMap(section3ImageMap);
		slide.setDescriptionMap(section3DescriptionMap);
		slide.setThumbnailMap(section3ThumbnailMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		residenceSlider3Persistence.update(slide);
		residenceSlider3Persistence.clearCache();

		return slide;
	}

	public ResidenceSlider3 deleteResidenceSlider3(long slideId,
									 ServiceContext serviceContext) throws PortalException,
			SystemException {

		ResidenceSlider3 slide = getResidenceSlider3(slideId);
		slide = deleteResidenceSlider3(slide);

		return slide;
	}

	public List<ResidenceSlider3> getResidenceSlider3Slides(long residenceId) {

		return residenceSlider3Persistence.findByResidenceId(residenceId);
	}

	public List<ResidenceSlider3> getResidenceSlider3Slides(long residenceId, int start, int end,
										 OrderByComparator<ResidenceSlider3> obc) {

		return residenceSlider3Persistence.findByResidenceId(residenceId, start, end, obc);
	}

	public List<ResidenceSlider3> getResidenceSlider3Slides(long residenceId, int start, int end) {

		return residenceSlider3Persistence.findByResidenceId(residenceId, start, end);
	}

	public int getResidenceSlider3SlidesCount(long residenceId) {

		return residenceSlider3Persistence.countByResidenceId(residenceId);
	}

	protected void validate(
			Map<Locale, String> section3ImageMap, Map<Locale, String> section3DescriptionMap, Map<Locale, String> section3ThumbnailMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String section3Image = section3ImageMap.get(locale);

		if (Validator.isNull(section3Image)) {
			throw new ResidenceSlider3ImageException();
		}

		String section3Description = section3DescriptionMap.get(locale);

		if (Validator.isNull(section3Description)) {
			throw new ResidenceSlider3DescriptionException();
		}

		String section3Thumbnail = section3ThumbnailMap.get(locale);

		if (Validator.isNull(section3Thumbnail)) {
			throw new ResidenceSlider3ThumbnailException();
		}

	}
}