/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import org.osgi.service.component.annotations.Component;
import residences.model.Residence;
import residences.service.base.ResidenceServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the residence remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>residences.service.ResidenceService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=residence",
		"json.web.service.context.path=Residence"
	},
	service = AopService.class
)
public class ResidenceServiceImpl extends ResidenceServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>residences.service.ResidenceServiceUtil</code> to access the residence remote service.
	 */

	/* *
	 * get residences by group
	 * */
	public List<Residence> getResidences(long groupId) {

		return residencePersistence.findByGroupId(groupId);
	}

	public List<Residence> getResidences(long groupId, int start, int end,
								 OrderByComparator<Residence> obc) {

		return residencePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Residence> getResidences(long groupId, int start, int end) {

		return residencePersistence.findByGroupId(groupId, start, end);
	}

	public int getResidencesCount(long groupId) {

		return residencePersistence.countByGroupId(groupId);
	}
}