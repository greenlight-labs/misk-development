/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import residences.exception.ResidenceSlider4DescriptionException;
import residences.exception.ResidenceSlider4ImageException;
import residences.exception.ResidenceSlider4TitleException;
import residences.model.ResidenceSlider4;
import residences.service.base.ResidenceSlider4LocalServiceBaseImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the residence slider4 local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>residences.service.ResidenceSlider4LocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider4LocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=residences.model.ResidenceSlider4",
	service = AopService.class
)
public class ResidenceSlider4LocalServiceImpl
	extends ResidenceSlider4LocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>residences.service.ResidenceSlider4LocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>residences.service.ResidenceSlider4LocalServiceUtil</code>.
	 */

	public ResidenceSlider4 addResidenceSlider4(long userId,
												long residenceId,
												Map<Locale, String> titleMap, Map<Locale, String> descriptionMap, Map<Locale, String> imageMap,
												Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
												ServiceContext serviceContext) throws PortalException {

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(titleMap, descriptionMap, imageMap);

		long slideId = counterLocalService.increment();

		ResidenceSlider4 slide = residenceSlider4Persistence.create(slideId);

		slide.setUuid(serviceContext.getUuid());
		slide.setUserId(userId);
		slide.setCompanyId(user.getCompanyId());
		slide.setUserName(user.getFullName());
		slide.setCreateDate(serviceContext.getCreateDate(now));
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setResidenceId(residenceId);
		slide.setTitleMap(titleMap);
		slide.setDescriptionMap(descriptionMap);
		slide.setImageMap(imageMap);
		slide.setButtonLabelMap(buttonLabelMap);
		slide.setButtonLinkMap(buttonLinkMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		residenceSlider4Persistence.update(slide);
		residenceSlider4Persistence.clearCache();

		return slide;
	}

	public ResidenceSlider4 updateResidenceSlider4(long userId, long slideId,
												   long residenceId,
												   Map<Locale, String> titleMap, Map<Locale, String> descriptionMap, Map<Locale, String> imageMap,
												   Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
												   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(imageMap, descriptionMap, titleMap);

		ResidenceSlider4 slide = getResidenceSlider4(slideId);

		User user = userLocalService.getUser(userId);

		slide.setUserId(userId);
		slide.setUserName(user.getFullName());
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setResidenceId(residenceId);
		slide.setTitleMap(titleMap);
		slide.setDescriptionMap(descriptionMap);
		slide.setImageMap(imageMap);
		slide.setButtonLabelMap(buttonLabelMap);
		slide.setButtonLinkMap(buttonLinkMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		residenceSlider4Persistence.update(slide);
		residenceSlider4Persistence.clearCache();

		return slide;
	}

	public ResidenceSlider4 deleteResidenceSlider4(long slideId,
												   ServiceContext serviceContext) throws PortalException,
			SystemException {

		ResidenceSlider4 slide = getResidenceSlider4(slideId);
		slide = deleteResidenceSlider4(slide);

		return slide;
	}

	public List<ResidenceSlider4> getResidenceSlider4Slides(long residenceId) {

		return residenceSlider4Persistence.findByResidenceId(residenceId);
	}

	public List<ResidenceSlider4> getResidenceSlider4Slides(long residenceId, int start, int end,
															OrderByComparator<ResidenceSlider4> obc) {

		return residenceSlider4Persistence.findByResidenceId(residenceId, start, end, obc);
	}

	public List<ResidenceSlider4> getResidenceSlider4Slides(long residenceId, int start, int end) {

		return residenceSlider4Persistence.findByResidenceId(residenceId, start, end);
	}

	public int getResidenceSlider4SlidesCount(long residenceId) {

		return residenceSlider4Persistence.countByResidenceId(residenceId);
	}

	protected void validate(
			Map<Locale, String> titleMap, Map<Locale, String> descriptionMap, Map<Locale, String> imageMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String title = titleMap.get(locale);

		if (Validator.isNull(title)) {
			throw new ResidenceSlider4TitleException();
		}

		String description = descriptionMap.get(locale);

		if (Validator.isNull(description)) {
			throw new ResidenceSlider4DescriptionException();
		}

		String image = imageMap.get(locale);

		if (Validator.isNull(image)) {
			throw new ResidenceSlider4ImageException();
		}

	}
}