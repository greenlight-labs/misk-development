/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.impl;

import com.liferay.portal.aop.AopService;

import org.osgi.service.component.annotations.Component;

import residences.service.base.ResidenceSlider4ServiceBaseImpl;

/**
 * The implementation of the residence slider4 remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>residences.service.ResidenceSlider4Service</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ResidenceSlider4ServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=residence",
		"json.web.service.context.path=ResidenceSlider4"
	},
	service = AopService.class
)
public class ResidenceSlider4ServiceImpl
	extends ResidenceSlider4ServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>residences.service.ResidenceSlider4ServiceUtil</code> to access the residence slider4 remote service.
	 */
}