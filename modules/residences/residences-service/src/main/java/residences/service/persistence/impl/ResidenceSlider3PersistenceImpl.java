/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import residences.exception.NoSuchSlider3Exception;

import residences.model.ResidenceSlider3;
import residences.model.impl.ResidenceSlider3Impl;
import residences.model.impl.ResidenceSlider3ModelImpl;

import residences.service.persistence.ResidenceSlider3Persistence;
import residences.service.persistence.ResidenceSlider3Util;
import residences.service.persistence.impl.constants.ResidencePersistenceConstants;

/**
 * The persistence implementation for the residence slider3 service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = ResidenceSlider3Persistence.class)
public class ResidenceSlider3PersistenceImpl
	extends BasePersistenceImpl<ResidenceSlider3>
	implements ResidenceSlider3Persistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>ResidenceSlider3Util</code> to access the residence slider3 persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		ResidenceSlider3Impl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the residence slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the residence slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<ResidenceSlider3> list = null;

		if (useFinderCache) {
			list = (List<ResidenceSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ResidenceSlider3 residenceSlider3 : list) {
					if (!uuid.equals(residenceSlider3.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_RESIDENCESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ResidenceSlider3ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<ResidenceSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 findByUuid_First(
			String uuid, OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = fetchByUuid_First(
			uuid, orderByComparator);

		if (residenceSlider3 != null) {
			return residenceSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 fetchByUuid_First(
		String uuid, OrderByComparator<ResidenceSlider3> orderByComparator) {

		List<ResidenceSlider3> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 findByUuid_Last(
			String uuid, OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = fetchByUuid_Last(
			uuid, orderByComparator);

		if (residenceSlider3 != null) {
			return residenceSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 fetchByUuid_Last(
		String uuid, OrderByComparator<ResidenceSlider3> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ResidenceSlider3> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the residence slider3s before and after the current residence slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current residence slider3
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		uuid = Objects.toString(uuid, "");

		ResidenceSlider3 residenceSlider3 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			ResidenceSlider3[] array = new ResidenceSlider3Impl[3];

			array[0] = getByUuid_PrevAndNext(
				session, residenceSlider3, uuid, orderByComparator, true);

			array[1] = residenceSlider3;

			array[2] = getByUuid_PrevAndNext(
				session, residenceSlider3, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ResidenceSlider3 getByUuid_PrevAndNext(
		Session session, ResidenceSlider3 residenceSlider3, String uuid,
		OrderByComparator<ResidenceSlider3> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_RESIDENCESLIDER3_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ResidenceSlider3ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						residenceSlider3)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ResidenceSlider3> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the residence slider3s where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ResidenceSlider3 residenceSlider3 :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(residenceSlider3);
		}
	}

	/**
	 * Returns the number of residence slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching residence slider3s
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_RESIDENCESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"residenceSlider3.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(residenceSlider3.uuid IS NULL OR residenceSlider3.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<ResidenceSlider3> list = null;

		if (useFinderCache) {
			list = (List<ResidenceSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ResidenceSlider3 residenceSlider3 : list) {
					if (!uuid.equals(residenceSlider3.getUuid()) ||
						(companyId != residenceSlider3.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_RESIDENCESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ResidenceSlider3ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<ResidenceSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (residenceSlider3 != null) {
			return residenceSlider3;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the first residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		List<ResidenceSlider3> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (residenceSlider3 != null) {
			return residenceSlider3;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the last residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ResidenceSlider3> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the residence slider3s before and after the current residence slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current residence slider3
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		uuid = Objects.toString(uuid, "");

		ResidenceSlider3 residenceSlider3 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			ResidenceSlider3[] array = new ResidenceSlider3Impl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, residenceSlider3, uuid, companyId, orderByComparator,
				true);

			array[1] = residenceSlider3;

			array[2] = getByUuid_C_PrevAndNext(
				session, residenceSlider3, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ResidenceSlider3 getByUuid_C_PrevAndNext(
		Session session, ResidenceSlider3 residenceSlider3, String uuid,
		long companyId, OrderByComparator<ResidenceSlider3> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_RESIDENCESLIDER3_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ResidenceSlider3ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						residenceSlider3)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ResidenceSlider3> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the residence slider3s where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ResidenceSlider3 residenceSlider3 :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(residenceSlider3);
		}
	}

	/**
	 * Returns the number of residence slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching residence slider3s
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_RESIDENCESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"residenceSlider3.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(residenceSlider3.uuid IS NULL OR residenceSlider3.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"residenceSlider3.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByResidenceId;
	private FinderPath _finderPathWithoutPaginationFindByResidenceId;
	private FinderPath _finderPathCountByResidenceId;

	/**
	 * Returns all the residence slider3s where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @return the matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByResidenceId(long residenceId) {
		return findByResidenceId(
			residenceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the residence slider3s where residenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param residenceId the residence ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByResidenceId(
		long residenceId, int start, int end) {

		return findByResidenceId(residenceId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the residence slider3s where residenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param residenceId the residence ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByResidenceId(
		long residenceId, int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		return findByResidenceId(
			residenceId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the residence slider3s where residenceId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param residenceId the residence ID
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findByResidenceId(
		long residenceId, int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByResidenceId;
				finderArgs = new Object[] {residenceId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByResidenceId;
			finderArgs = new Object[] {
				residenceId, start, end, orderByComparator
			};
		}

		List<ResidenceSlider3> list = null;

		if (useFinderCache) {
			list = (List<ResidenceSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ResidenceSlider3 residenceSlider3 : list) {
					if (residenceId != residenceSlider3.getResidenceId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_RESIDENCESLIDER3_WHERE);

			sb.append(_FINDER_COLUMN_RESIDENCEID_RESIDENCEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(ResidenceSlider3ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(residenceId);

				list = (List<ResidenceSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 findByResidenceId_First(
			long residenceId,
			OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = fetchByResidenceId_First(
			residenceId, orderByComparator);

		if (residenceSlider3 != null) {
			return residenceSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("residenceId=");
		sb.append(residenceId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the first residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 fetchByResidenceId_First(
		long residenceId,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		List<ResidenceSlider3> list = findByResidenceId(
			residenceId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3
	 * @throws NoSuchSlider3Exception if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 findByResidenceId_Last(
			long residenceId,
			OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = fetchByResidenceId_Last(
			residenceId, orderByComparator);

		if (residenceSlider3 != null) {
			return residenceSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("residenceId=");
		sb.append(residenceId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the last residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching residence slider3, or <code>null</code> if a matching residence slider3 could not be found
	 */
	@Override
	public ResidenceSlider3 fetchByResidenceId_Last(
		long residenceId,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		int count = countByResidenceId(residenceId);

		if (count == 0) {
			return null;
		}

		List<ResidenceSlider3> list = findByResidenceId(
			residenceId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the residence slider3s before and after the current residence slider3 in the ordered set where residenceId = &#63;.
	 *
	 * @param slideId the primary key of the current residence slider3
	 * @param residenceId the residence ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3[] findByResidenceId_PrevAndNext(
			long slideId, long residenceId,
			OrderByComparator<ResidenceSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			ResidenceSlider3[] array = new ResidenceSlider3Impl[3];

			array[0] = getByResidenceId_PrevAndNext(
				session, residenceSlider3, residenceId, orderByComparator,
				true);

			array[1] = residenceSlider3;

			array[2] = getByResidenceId_PrevAndNext(
				session, residenceSlider3, residenceId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected ResidenceSlider3 getByResidenceId_PrevAndNext(
		Session session, ResidenceSlider3 residenceSlider3, long residenceId,
		OrderByComparator<ResidenceSlider3> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_RESIDENCESLIDER3_WHERE);

		sb.append(_FINDER_COLUMN_RESIDENCEID_RESIDENCEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(ResidenceSlider3ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(residenceId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						residenceSlider3)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<ResidenceSlider3> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the residence slider3s where residenceId = &#63; from the database.
	 *
	 * @param residenceId the residence ID
	 */
	@Override
	public void removeByResidenceId(long residenceId) {
		for (ResidenceSlider3 residenceSlider3 :
				findByResidenceId(
					residenceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(residenceSlider3);
		}
	}

	/**
	 * Returns the number of residence slider3s where residenceId = &#63;.
	 *
	 * @param residenceId the residence ID
	 * @return the number of matching residence slider3s
	 */
	@Override
	public int countByResidenceId(long residenceId) {
		FinderPath finderPath = _finderPathCountByResidenceId;

		Object[] finderArgs = new Object[] {residenceId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_RESIDENCESLIDER3_WHERE);

			sb.append(_FINDER_COLUMN_RESIDENCEID_RESIDENCEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(residenceId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RESIDENCEID_RESIDENCEID_2 =
		"residenceSlider3.residenceId = ?";

	public ResidenceSlider3PersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(ResidenceSlider3.class);

		setModelImplClass(ResidenceSlider3Impl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the residence slider3 in the entity cache if it is enabled.
	 *
	 * @param residenceSlider3 the residence slider3
	 */
	@Override
	public void cacheResult(ResidenceSlider3 residenceSlider3) {
		entityCache.putResult(
			ResidenceSlider3Impl.class, residenceSlider3.getPrimaryKey(),
			residenceSlider3);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the residence slider3s in the entity cache if it is enabled.
	 *
	 * @param residenceSlider3s the residence slider3s
	 */
	@Override
	public void cacheResult(List<ResidenceSlider3> residenceSlider3s) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (residenceSlider3s.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (ResidenceSlider3 residenceSlider3 : residenceSlider3s) {
			if (entityCache.getResult(
					ResidenceSlider3Impl.class,
					residenceSlider3.getPrimaryKey()) == null) {

				cacheResult(residenceSlider3);
			}
		}
	}

	/**
	 * Clears the cache for all residence slider3s.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ResidenceSlider3Impl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the residence slider3.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ResidenceSlider3 residenceSlider3) {
		entityCache.removeResult(ResidenceSlider3Impl.class, residenceSlider3);
	}

	@Override
	public void clearCache(List<ResidenceSlider3> residenceSlider3s) {
		for (ResidenceSlider3 residenceSlider3 : residenceSlider3s) {
			entityCache.removeResult(
				ResidenceSlider3Impl.class, residenceSlider3);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(ResidenceSlider3Impl.class, primaryKey);
		}
	}

	/**
	 * Creates a new residence slider3 with the primary key. Does not add the residence slider3 to the database.
	 *
	 * @param slideId the primary key for the new residence slider3
	 * @return the new residence slider3
	 */
	@Override
	public ResidenceSlider3 create(long slideId) {
		ResidenceSlider3 residenceSlider3 = new ResidenceSlider3Impl();

		residenceSlider3.setNew(true);
		residenceSlider3.setPrimaryKey(slideId);

		String uuid = PortalUUIDUtil.generate();

		residenceSlider3.setUuid(uuid);

		residenceSlider3.setCompanyId(CompanyThreadLocal.getCompanyId());

		return residenceSlider3;
	}

	/**
	 * Removes the residence slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3 that was removed
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3 remove(long slideId) throws NoSuchSlider3Exception {
		return remove((Serializable)slideId);
	}

	/**
	 * Removes the residence slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the residence slider3
	 * @return the residence slider3 that was removed
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3 remove(Serializable primaryKey)
		throws NoSuchSlider3Exception {

		Session session = null;

		try {
			session = openSession();

			ResidenceSlider3 residenceSlider3 = (ResidenceSlider3)session.get(
				ResidenceSlider3Impl.class, primaryKey);

			if (residenceSlider3 == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSlider3Exception(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(residenceSlider3);
		}
		catch (NoSuchSlider3Exception noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ResidenceSlider3 removeImpl(ResidenceSlider3 residenceSlider3) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(residenceSlider3)) {
				residenceSlider3 = (ResidenceSlider3)session.get(
					ResidenceSlider3Impl.class,
					residenceSlider3.getPrimaryKeyObj());
			}

			if (residenceSlider3 != null) {
				session.delete(residenceSlider3);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (residenceSlider3 != null) {
			clearCache(residenceSlider3);
		}

		return residenceSlider3;
	}

	@Override
	public ResidenceSlider3 updateImpl(ResidenceSlider3 residenceSlider3) {
		boolean isNew = residenceSlider3.isNew();

		if (!(residenceSlider3 instanceof ResidenceSlider3ModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(residenceSlider3.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					residenceSlider3);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in residenceSlider3 proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom ResidenceSlider3 implementation " +
					residenceSlider3.getClass());
		}

		ResidenceSlider3ModelImpl residenceSlider3ModelImpl =
			(ResidenceSlider3ModelImpl)residenceSlider3;

		if (Validator.isNull(residenceSlider3.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			residenceSlider3.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (residenceSlider3.getCreateDate() == null)) {
			if (serviceContext == null) {
				residenceSlider3.setCreateDate(date);
			}
			else {
				residenceSlider3.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!residenceSlider3ModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				residenceSlider3.setModifiedDate(date);
			}
			else {
				residenceSlider3.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(residenceSlider3);
			}
			else {
				residenceSlider3 = (ResidenceSlider3)session.merge(
					residenceSlider3);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			ResidenceSlider3Impl.class, residenceSlider3ModelImpl, false, true);

		if (isNew) {
			residenceSlider3.setNew(false);
		}

		residenceSlider3.resetOriginalValues();

		return residenceSlider3;
	}

	/**
	 * Returns the residence slider3 with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the residence slider3
	 * @return the residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3 findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSlider3Exception {

		ResidenceSlider3 residenceSlider3 = fetchByPrimaryKey(primaryKey);

		if (residenceSlider3 == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSlider3Exception(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return residenceSlider3;
	}

	/**
	 * Returns the residence slider3 with the primary key or throws a <code>NoSuchSlider3Exception</code> if it could not be found.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3
	 * @throws NoSuchSlider3Exception if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3 findByPrimaryKey(long slideId)
		throws NoSuchSlider3Exception {

		return findByPrimaryKey((Serializable)slideId);
	}

	/**
	 * Returns the residence slider3 with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the residence slider3
	 * @return the residence slider3, or <code>null</code> if a residence slider3 with the primary key could not be found
	 */
	@Override
	public ResidenceSlider3 fetchByPrimaryKey(long slideId) {
		return fetchByPrimaryKey((Serializable)slideId);
	}

	/**
	 * Returns all the residence slider3s.
	 *
	 * @return the residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @return the range of residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findAll(
		int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the residence slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ResidenceSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of residence slider3s
	 * @param end the upper bound of the range of residence slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of residence slider3s
	 */
	@Override
	public List<ResidenceSlider3> findAll(
		int start, int end,
		OrderByComparator<ResidenceSlider3> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<ResidenceSlider3> list = null;

		if (useFinderCache) {
			list = (List<ResidenceSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_RESIDENCESLIDER3);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_RESIDENCESLIDER3;

				sql = sql.concat(ResidenceSlider3ModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<ResidenceSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the residence slider3s from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ResidenceSlider3 residenceSlider3 : findAll()) {
			remove(residenceSlider3);
		}
	}

	/**
	 * Returns the number of residence slider3s.
	 *
	 * @return the number of residence slider3s
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_RESIDENCESLIDER3);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "slideId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_RESIDENCESLIDER3;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ResidenceSlider3ModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the residence slider3 persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class,
			new ResidenceSlider3ModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", ResidenceSlider3.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByResidenceId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByResidenceId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"residenceId"}, true);

		_finderPathWithoutPaginationFindByResidenceId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByResidenceId",
			new String[] {Long.class.getName()}, new String[] {"residenceId"},
			true);

		_finderPathCountByResidenceId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByResidenceId",
			new String[] {Long.class.getName()}, new String[] {"residenceId"},
			false);

		_setResidenceSlider3UtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setResidenceSlider3UtilPersistence(null);

		entityCache.removeCache(ResidenceSlider3Impl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setResidenceSlider3UtilPersistence(
		ResidenceSlider3Persistence residenceSlider3Persistence) {

		try {
			Field field = ResidenceSlider3Util.class.getDeclaredField(
				"_persistence");

			field.setAccessible(true);

			field.set(null, residenceSlider3Persistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = ResidencePersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = ResidencePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = ResidencePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_RESIDENCESLIDER3 =
		"SELECT residenceSlider3 FROM ResidenceSlider3 residenceSlider3";

	private static final String _SQL_SELECT_RESIDENCESLIDER3_WHERE =
		"SELECT residenceSlider3 FROM ResidenceSlider3 residenceSlider3 WHERE ";

	private static final String _SQL_COUNT_RESIDENCESLIDER3 =
		"SELECT COUNT(residenceSlider3) FROM ResidenceSlider3 residenceSlider3";

	private static final String _SQL_COUNT_RESIDENCESLIDER3_WHERE =
		"SELECT COUNT(residenceSlider3) FROM ResidenceSlider3 residenceSlider3 WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "residenceSlider3.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No ResidenceSlider3 exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No ResidenceSlider3 exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		ResidenceSlider3PersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class ResidenceSlider3ModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			ResidenceSlider3ModelImpl residenceSlider3ModelImpl =
				(ResidenceSlider3ModelImpl)baseModel;

			long columnBitmask = residenceSlider3ModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(
					residenceSlider3ModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						residenceSlider3ModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(ResidenceSlider3PersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(
					residenceSlider3ModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			ResidenceSlider3ModelImpl residenceSlider3ModelImpl,
			String[] columnNames, boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] =
						residenceSlider3ModelImpl.getColumnOriginalValue(
							columnName);
				}
				else {
					arguments[i] = residenceSlider3ModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= ResidenceSlider3ModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}