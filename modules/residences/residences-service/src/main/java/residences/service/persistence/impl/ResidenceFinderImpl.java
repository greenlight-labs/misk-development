package residences.service.persistence.impl;

import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import residences.model.Residence;
import residences.model.impl.ResidenceImpl;
import residences.service.persistence.ResidenceFinder;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@Component(service = ResidenceFinder.class)
public class ResidenceFinderImpl
        extends ResidenceFinderBaseImpl implements ResidenceFinder {

    public static final String FIND_BY_T_D_EN =
            ResidenceFinder.class.getName() + ".searchResidencesByTitleDescription_en_US";
    public static final String FIND_BY_T_D_AR =
            ResidenceFinder.class.getName() + ".searchResidencesByTitleDescription_ar_SA";
    public static final String COUNT_BY_T_D_EN =
            ResidenceFinder.class.getName() + ".countResidencesByTitleDescription_en_US";
    public static final String COUNT_BY_T_D_AR =
            ResidenceFinder.class.getName() + ".countResidencesByTitleDescription_ar_SA";
    public static final String SEARCH_ALL_EN =
            ResidenceFinder.class.getName() + ".searchAll_en_US";
    public static final String SEARCH_ALL_AR =
            ResidenceFinder.class.getName() + ".searchAll_ar_SA";
    public static final String COUNT_SEARCH_ALL_EN =
            ResidenceFinder.class.getName() + ".countSearchAll_en_US";
    public static final String COUNT_SEARCH_ALL_AR =
            ResidenceFinder.class.getName() + ".countSearchAll_ar_SA";

    public List<Residence> findByT_D(String query, int offset, int limit, String languageId) {
        Session session = null;
        List<Residence> residences = Collections.emptyList();

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), FIND_BY_T_D_EN);
            if("ar_SA".equals(languageId)){
                sql = _customSQL.get(getClass(), FIND_BY_T_D_AR);
            }

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.setCacheable(false);
            sqlQuery.addEntity("Residence", ResidenceImpl.class);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add('%'+query+'%');
            queryPos.add('%'+query+'%');
            queryPos.add(offset);
            queryPos.add(limit);

            residences = (List<Residence>) sqlQuery.list();

            return residences;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    public int countByT_D(String query, String languageId) {
        Session session = null;

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), COUNT_BY_T_D_EN);
            if("ar_SA".equals(languageId)){
                sql = _customSQL.get(getClass(), COUNT_BY_T_D_AR);
            }

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.addScalar(COUNT_COLUMN_NAME, Type.LONG);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add('%'+query+'%');
            queryPos.add('%'+query+'%');

            Iterator<Long> iterator = sqlQuery.iterate();

            if (iterator.hasNext()) {
                Long count = iterator.next();

                if (count != null) {
                    return count.intValue();
                }
            }

            return 0;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    public List<Object> searchCompleteWebsite(String query, int offset, int limit, String languageId) {
        Session session = null;
        List<Object> models = Collections.emptyList();

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), SEARCH_ALL_EN);
            if("ar_SA".equals(languageId)){
                sql = _customSQL.get(getClass(), SEARCH_ALL_AR);
            }

            sql = StringUtil.replace(sql, "[$SEARCH_KEYWORD$]", "%" +query+ "%");
            sql = StringUtil.replace(sql, "[$OFFSET$]", String.valueOf(offset));
            sql = StringUtil.replace(sql, "[$LIMIT$]", String.valueOf(limit));

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.setCacheable(false);
            //sqlQuery.addEntity("Residence", ResidenceImpl.class);

            /*QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add('%'+query+'%');
            queryPos.add('%'+query+'%');
            queryPos.add(offset);
            queryPos.add(limit);*/

            models = (List<Object>) sqlQuery.list();

            return models;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    public int countSearchCompleteWebsite(String query, String languageId) {
        Session session = null;

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), COUNT_SEARCH_ALL_EN);
            if("ar_SA".equals(languageId)){
                sql = _customSQL.get(getClass(), COUNT_SEARCH_ALL_AR);
            }

            sql = StringUtil.replace(sql, "[$SEARCH_KEYWORD$]", "%" +query+ "%");

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.addScalar(COUNT_COLUMN_NAME, Type.LONG);

            /*QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add('%'+query+'%');
            queryPos.add('%'+query+'%');*/

            Iterator<Long> iterator = sqlQuery.iterate();

            if (iterator.hasNext()) {
                Long count = iterator.next();

                if (count != null) {
                    return count.intValue();
                }
            }

            return 0;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    @Reference
    private CustomSQL _customSQL;
}
