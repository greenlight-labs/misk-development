/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import residences.model.Residence;

/**
 * The cache model class for representing Residence in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ResidenceCacheModel
	implements CacheModel<Residence>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ResidenceCacheModel)) {
			return false;
		}

		ResidenceCacheModel residenceCacheModel = (ResidenceCacheModel)object;

		if (residenceId == residenceCacheModel.residenceId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, residenceId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(71);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", residenceId=");
		sb.append(residenceId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", listingTitle=");
		sb.append(listingTitle);
		sb.append(", listingDescription=");
		sb.append(listingDescription);
		sb.append(", bannerTitle=");
		sb.append(bannerTitle);
		sb.append(", bannerSubtitle=");
		sb.append(bannerSubtitle);
		sb.append(", bannerDescription=");
		sb.append(bannerDescription);
		sb.append(", bannerDesktopImage=");
		sb.append(bannerDesktopImage);
		sb.append(", bannerMobileImage=");
		sb.append(bannerMobileImage);
		sb.append(", section2Title=");
		sb.append(section2Title);
		sb.append(", section2Subtitle=");
		sb.append(section2Subtitle);
		sb.append(", section2Description=");
		sb.append(section2Description);
		sb.append(", section2ButtonLabel=");
		sb.append(section2ButtonLabel);
		sb.append(", section2ButtonLink=");
		sb.append(section2ButtonLink);
		sb.append(", section2Image=");
		sb.append(section2Image);
		sb.append(", section5Title=");
		sb.append(section5Title);
		sb.append(", section5Subtitle=");
		sb.append(section5Subtitle);
		sb.append(", section5Description=");
		sb.append(section5Description);
		sb.append(", section5ButtonLabel=");
		sb.append(section5ButtonLabel);
		sb.append(", section5ButtonLink=");
		sb.append(section5ButtonLink);
		sb.append(", section5Image=");
		sb.append(section5Image);
		sb.append(", section6Title=");
		sb.append(section6Title);
		sb.append(", section6Subtitle=");
		sb.append(section6Subtitle);
		sb.append(", section6Description=");
		sb.append(section6Description);
		sb.append(", section6ButtonLabel=");
		sb.append(section6ButtonLabel);
		sb.append(", section6ButtonLink=");
		sb.append(section6ButtonLink);
		sb.append(", section6Image=");
		sb.append(section6Image);
		sb.append(", section4SectionTitle=");
		sb.append(section4SectionTitle);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Residence toEntityModel() {
		ResidenceImpl residenceImpl = new ResidenceImpl();

		if (uuid == null) {
			residenceImpl.setUuid("");
		}
		else {
			residenceImpl.setUuid(uuid);
		}

		residenceImpl.setResidenceId(residenceId);
		residenceImpl.setGroupId(groupId);
		residenceImpl.setCompanyId(companyId);
		residenceImpl.setUserId(userId);

		if (userName == null) {
			residenceImpl.setUserName("");
		}
		else {
			residenceImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			residenceImpl.setCreateDate(null);
		}
		else {
			residenceImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			residenceImpl.setModifiedDate(null);
		}
		else {
			residenceImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (listingImage == null) {
			residenceImpl.setListingImage("");
		}
		else {
			residenceImpl.setListingImage(listingImage);
		}

		if (listingTitle == null) {
			residenceImpl.setListingTitle("");
		}
		else {
			residenceImpl.setListingTitle(listingTitle);
		}

		if (listingDescription == null) {
			residenceImpl.setListingDescription("");
		}
		else {
			residenceImpl.setListingDescription(listingDescription);
		}

		if (bannerTitle == null) {
			residenceImpl.setBannerTitle("");
		}
		else {
			residenceImpl.setBannerTitle(bannerTitle);
		}

		if (bannerSubtitle == null) {
			residenceImpl.setBannerSubtitle("");
		}
		else {
			residenceImpl.setBannerSubtitle(bannerSubtitle);
		}

		if (bannerDescription == null) {
			residenceImpl.setBannerDescription("");
		}
		else {
			residenceImpl.setBannerDescription(bannerDescription);
		}

		if (bannerDesktopImage == null) {
			residenceImpl.setBannerDesktopImage("");
		}
		else {
			residenceImpl.setBannerDesktopImage(bannerDesktopImage);
		}

		if (bannerMobileImage == null) {
			residenceImpl.setBannerMobileImage("");
		}
		else {
			residenceImpl.setBannerMobileImage(bannerMobileImage);
		}

		if (section2Title == null) {
			residenceImpl.setSection2Title("");
		}
		else {
			residenceImpl.setSection2Title(section2Title);
		}

		if (section2Subtitle == null) {
			residenceImpl.setSection2Subtitle("");
		}
		else {
			residenceImpl.setSection2Subtitle(section2Subtitle);
		}

		if (section2Description == null) {
			residenceImpl.setSection2Description("");
		}
		else {
			residenceImpl.setSection2Description(section2Description);
		}

		if (section2ButtonLabel == null) {
			residenceImpl.setSection2ButtonLabel("");
		}
		else {
			residenceImpl.setSection2ButtonLabel(section2ButtonLabel);
		}

		if (section2ButtonLink == null) {
			residenceImpl.setSection2ButtonLink("");
		}
		else {
			residenceImpl.setSection2ButtonLink(section2ButtonLink);
		}

		if (section2Image == null) {
			residenceImpl.setSection2Image("");
		}
		else {
			residenceImpl.setSection2Image(section2Image);
		}

		if (section5Title == null) {
			residenceImpl.setSection5Title("");
		}
		else {
			residenceImpl.setSection5Title(section5Title);
		}

		if (section5Subtitle == null) {
			residenceImpl.setSection5Subtitle("");
		}
		else {
			residenceImpl.setSection5Subtitle(section5Subtitle);
		}

		if (section5Description == null) {
			residenceImpl.setSection5Description("");
		}
		else {
			residenceImpl.setSection5Description(section5Description);
		}

		if (section5ButtonLabel == null) {
			residenceImpl.setSection5ButtonLabel("");
		}
		else {
			residenceImpl.setSection5ButtonLabel(section5ButtonLabel);
		}

		if (section5ButtonLink == null) {
			residenceImpl.setSection5ButtonLink("");
		}
		else {
			residenceImpl.setSection5ButtonLink(section5ButtonLink);
		}

		if (section5Image == null) {
			residenceImpl.setSection5Image("");
		}
		else {
			residenceImpl.setSection5Image(section5Image);
		}

		if (section6Title == null) {
			residenceImpl.setSection6Title("");
		}
		else {
			residenceImpl.setSection6Title(section6Title);
		}

		if (section6Subtitle == null) {
			residenceImpl.setSection6Subtitle("");
		}
		else {
			residenceImpl.setSection6Subtitle(section6Subtitle);
		}

		if (section6Description == null) {
			residenceImpl.setSection6Description("");
		}
		else {
			residenceImpl.setSection6Description(section6Description);
		}

		if (section6ButtonLabel == null) {
			residenceImpl.setSection6ButtonLabel("");
		}
		else {
			residenceImpl.setSection6ButtonLabel(section6ButtonLabel);
		}

		if (section6ButtonLink == null) {
			residenceImpl.setSection6ButtonLink("");
		}
		else {
			residenceImpl.setSection6ButtonLink(section6ButtonLink);
		}

		if (section6Image == null) {
			residenceImpl.setSection6Image("");
		}
		else {
			residenceImpl.setSection6Image(section6Image);
		}

		if (section4SectionTitle == null) {
			residenceImpl.setSection4SectionTitle("");
		}
		else {
			residenceImpl.setSection4SectionTitle(section4SectionTitle);
		}

		residenceImpl.resetOriginalValues();

		return residenceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		residenceId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		listingImage = objectInput.readUTF();
		listingTitle = objectInput.readUTF();
		listingDescription = objectInput.readUTF();
		bannerTitle = objectInput.readUTF();
		bannerSubtitle = objectInput.readUTF();
		bannerDescription = objectInput.readUTF();
		bannerDesktopImage = objectInput.readUTF();
		bannerMobileImage = objectInput.readUTF();
		section2Title = objectInput.readUTF();
		section2Subtitle = objectInput.readUTF();
		section2Description = (String)objectInput.readObject();
		section2ButtonLabel = objectInput.readUTF();
		section2ButtonLink = objectInput.readUTF();
		section2Image = objectInput.readUTF();
		section5Title = objectInput.readUTF();
		section5Subtitle = objectInput.readUTF();
		section5Description = (String)objectInput.readObject();
		section5ButtonLabel = objectInput.readUTF();
		section5ButtonLink = objectInput.readUTF();
		section5Image = objectInput.readUTF();
		section6Title = objectInput.readUTF();
		section6Subtitle = objectInput.readUTF();
		section6Description = (String)objectInput.readObject();
		section6ButtonLabel = objectInput.readUTF();
		section6ButtonLink = objectInput.readUTF();
		section6Image = objectInput.readUTF();
		section4SectionTitle = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(residenceId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (listingTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingTitle);
		}

		if (listingDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingDescription);
		}

		if (bannerTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerTitle);
		}

		if (bannerSubtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerSubtitle);
		}

		if (bannerDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerDescription);
		}

		if (bannerDesktopImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerDesktopImage);
		}

		if (bannerMobileImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerMobileImage);
		}

		if (section2Title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Title);
		}

		if (section2Subtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Subtitle);
		}

		if (section2Description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(section2Description);
		}

		if (section2ButtonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2ButtonLabel);
		}

		if (section2ButtonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2ButtonLink);
		}

		if (section2Image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Image);
		}

		if (section5Title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section5Title);
		}

		if (section5Subtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section5Subtitle);
		}

		if (section5Description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(section5Description);
		}

		if (section5ButtonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section5ButtonLabel);
		}

		if (section5ButtonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section5ButtonLink);
		}

		if (section5Image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section5Image);
		}

		if (section6Title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section6Title);
		}

		if (section6Subtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section6Subtitle);
		}

		if (section6Description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(section6Description);
		}

		if (section6ButtonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section6ButtonLabel);
		}

		if (section6ButtonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section6ButtonLink);
		}

		if (section6Image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section6Image);
		}

		if (section4SectionTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section4SectionTitle);
		}
	}

	public String uuid;
	public long residenceId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String listingImage;
	public String listingTitle;
	public String listingDescription;
	public String bannerTitle;
	public String bannerSubtitle;
	public String bannerDescription;
	public String bannerDesktopImage;
	public String bannerMobileImage;
	public String section2Title;
	public String section2Subtitle;
	public String section2Description;
	public String section2ButtonLabel;
	public String section2ButtonLink;
	public String section2Image;
	public String section5Title;
	public String section5Subtitle;
	public String section5Description;
	public String section5ButtonLabel;
	public String section5ButtonLink;
	public String section5Image;
	public String section6Title;
	public String section6Subtitle;
	public String section6Description;
	public String section6ButtonLabel;
	public String section6ButtonLink;
	public String section6Image;
	public String section4SectionTitle;

}