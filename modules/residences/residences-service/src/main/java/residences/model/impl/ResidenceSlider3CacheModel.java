/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import residences.model.ResidenceSlider3;

/**
 * The cache model class for representing ResidenceSlider3 in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ResidenceSlider3CacheModel
	implements CacheModel<ResidenceSlider3>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ResidenceSlider3CacheModel)) {
			return false;
		}

		ResidenceSlider3CacheModel residenceSlider3CacheModel =
			(ResidenceSlider3CacheModel)object;

		if (slideId == residenceSlider3CacheModel.slideId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, slideId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", slideId=");
		sb.append(slideId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", residenceId=");
		sb.append(residenceId);
		sb.append(", image=");
		sb.append(image);
		sb.append(", description=");
		sb.append(description);
		sb.append(", thumbnail=");
		sb.append(thumbnail);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ResidenceSlider3 toEntityModel() {
		ResidenceSlider3Impl residenceSlider3Impl = new ResidenceSlider3Impl();

		if (uuid == null) {
			residenceSlider3Impl.setUuid("");
		}
		else {
			residenceSlider3Impl.setUuid(uuid);
		}

		residenceSlider3Impl.setSlideId(slideId);
		residenceSlider3Impl.setCompanyId(companyId);
		residenceSlider3Impl.setUserId(userId);

		if (userName == null) {
			residenceSlider3Impl.setUserName("");
		}
		else {
			residenceSlider3Impl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			residenceSlider3Impl.setCreateDate(null);
		}
		else {
			residenceSlider3Impl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			residenceSlider3Impl.setModifiedDate(null);
		}
		else {
			residenceSlider3Impl.setModifiedDate(new Date(modifiedDate));
		}

		residenceSlider3Impl.setResidenceId(residenceId);

		if (image == null) {
			residenceSlider3Impl.setImage("");
		}
		else {
			residenceSlider3Impl.setImage(image);
		}

		if (description == null) {
			residenceSlider3Impl.setDescription("");
		}
		else {
			residenceSlider3Impl.setDescription(description);
		}

		if (thumbnail == null) {
			residenceSlider3Impl.setThumbnail("");
		}
		else {
			residenceSlider3Impl.setThumbnail(thumbnail);
		}

		residenceSlider3Impl.resetOriginalValues();

		return residenceSlider3Impl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		slideId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		residenceId = objectInput.readLong();
		image = objectInput.readUTF();
		description = objectInput.readUTF();
		thumbnail = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(slideId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(residenceId);

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (thumbnail == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(thumbnail);
		}
	}

	public String uuid;
	public long slideId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long residenceId;
	public String image;
	public String description;
	public String thumbnail;

}