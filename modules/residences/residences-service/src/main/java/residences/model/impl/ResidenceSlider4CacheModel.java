/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package residences.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import residences.model.ResidenceSlider4;

/**
 * The cache model class for representing ResidenceSlider4 in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ResidenceSlider4CacheModel
	implements CacheModel<ResidenceSlider4>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ResidenceSlider4CacheModel)) {
			return false;
		}

		ResidenceSlider4CacheModel residenceSlider4CacheModel =
			(ResidenceSlider4CacheModel)object;

		if (slideId == residenceSlider4CacheModel.slideId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, slideId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", slideId=");
		sb.append(slideId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", residenceId=");
		sb.append(residenceId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", image=");
		sb.append(image);
		sb.append(", buttonLabel=");
		sb.append(buttonLabel);
		sb.append(", buttonLink=");
		sb.append(buttonLink);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ResidenceSlider4 toEntityModel() {
		ResidenceSlider4Impl residenceSlider4Impl = new ResidenceSlider4Impl();

		if (uuid == null) {
			residenceSlider4Impl.setUuid("");
		}
		else {
			residenceSlider4Impl.setUuid(uuid);
		}

		residenceSlider4Impl.setSlideId(slideId);
		residenceSlider4Impl.setCompanyId(companyId);
		residenceSlider4Impl.setUserId(userId);

		if (userName == null) {
			residenceSlider4Impl.setUserName("");
		}
		else {
			residenceSlider4Impl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			residenceSlider4Impl.setCreateDate(null);
		}
		else {
			residenceSlider4Impl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			residenceSlider4Impl.setModifiedDate(null);
		}
		else {
			residenceSlider4Impl.setModifiedDate(new Date(modifiedDate));
		}

		residenceSlider4Impl.setResidenceId(residenceId);

		if (title == null) {
			residenceSlider4Impl.setTitle("");
		}
		else {
			residenceSlider4Impl.setTitle(title);
		}

		if (description == null) {
			residenceSlider4Impl.setDescription("");
		}
		else {
			residenceSlider4Impl.setDescription(description);
		}

		if (image == null) {
			residenceSlider4Impl.setImage("");
		}
		else {
			residenceSlider4Impl.setImage(image);
		}

		if (buttonLabel == null) {
			residenceSlider4Impl.setButtonLabel("");
		}
		else {
			residenceSlider4Impl.setButtonLabel(buttonLabel);
		}

		if (buttonLink == null) {
			residenceSlider4Impl.setButtonLink("");
		}
		else {
			residenceSlider4Impl.setButtonLink(buttonLink);
		}

		residenceSlider4Impl.resetOriginalValues();

		return residenceSlider4Impl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		slideId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		residenceId = objectInput.readLong();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		image = objectInput.readUTF();
		buttonLabel = objectInput.readUTF();
		buttonLink = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(slideId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(residenceId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (buttonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLabel);
		}

		if (buttonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLink);
		}
	}

	public String uuid;
	public long slideId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long residenceId;
	public String title;
	public String description;
	public String image;
	public String buttonLabel;
	public String buttonLink;

}