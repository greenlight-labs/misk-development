create index IX_9AD6124C on misk_residences (groupId);
create index IX_3041A92 on misk_residences (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_49961D94 on misk_residences (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_E04D8CC0 on misk_residences_slider3 (residenceId);
create index IX_93DE7545 on misk_residences_slider3 (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_C1DF6A9F on misk_residences_slider4 (residenceId);
create index IX_A0776AC6 on misk_residences_slider4 (uuid_[$COLUMN_LENGTH:75$], companyId);