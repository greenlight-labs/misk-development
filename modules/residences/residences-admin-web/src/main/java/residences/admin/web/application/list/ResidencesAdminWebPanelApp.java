package residences.admin.web.application.list;

import residences.admin.web.constants.ResidencesAdminWebPanelCategoryKeys;
import residences.admin.web.constants.ResidencesAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + ResidencesAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class ResidencesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return ResidencesAdminWebPortletKeys.RESIDENCESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + ResidencesAdminWebPortletKeys.RESIDENCESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}