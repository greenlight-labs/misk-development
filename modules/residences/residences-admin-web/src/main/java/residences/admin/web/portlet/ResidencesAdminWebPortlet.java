package residences.admin.web.portlet;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import residences.admin.web.constants.ResidencesAdminWebPortletKeys;
import residences.model.Residence;
import residences.model.ResidenceSlider3;
import residences.model.ResidenceSlider4;
import residences.service.*;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=ResidencesAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ResidencesAdminWebPortletKeys.RESIDENCESADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class ResidencesAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addResidence(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Residence.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long residenceId = _counterLocalService.increment();

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");

		Map<Locale, String> bannerTitleMap = LocalizationUtil.getLocalizationMap(request, "bannerTitle");
		Map<Locale, String> bannerSubtitleMap = LocalizationUtil.getLocalizationMap(request, "bannerSubtitle");
		Map<Locale, String> bannerDescriptionMap = LocalizationUtil.getLocalizationMap(request, "bannerDescription");
		Map<Locale, String> bannerDesktopImageMap = LocalizationUtil.getLocalizationMap(request, "bannerDesktopImage");
		Map<Locale, String> bannerMobileImageMap = LocalizationUtil.getLocalizationMap(request, "bannerMobileImage");
		Map<Locale, String> section2TitleMap = LocalizationUtil.getLocalizationMap(request, "section2Title");
		Map<Locale, String> section2SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section2Subtitle");
		Map<Locale, String> section2DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section2Description");
		Map<Locale, String> section2ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLabel");
		Map<Locale, String> section2ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLink");
		Map<Locale, String> section2ImageMap = LocalizationUtil.getLocalizationMap(request, "section2Image");

		Map<Locale, String> section5TitleMap = LocalizationUtil.getLocalizationMap(request, "section5Title");
		Map<Locale, String> section5SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section5Subtitle");
		Map<Locale, String> section5DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section5Description");
		Map<Locale, String> section5ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section5ButtonLabel");
		Map<Locale, String> section5ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section5ButtonLink");
		Map<Locale, String> section5ImageMap = LocalizationUtil.getLocalizationMap(request, "section5Image");

		Map<Locale, String> section6TitleMap = LocalizationUtil.getLocalizationMap(request, "section6Title");
		Map<Locale, String> section6SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section6Subtitle");
		Map<Locale, String> section6DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section6Description");
		Map<Locale, String> section6ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section6ButtonLabel");
		Map<Locale, String> section6ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section6ButtonLink");
		Map<Locale, String> section6ImageMap = LocalizationUtil.getLocalizationMap(request, "section6Image");

		Map<Locale, String> section4SectionTitleMap = LocalizationUtil.getLocalizationMap(request, "section4SectionTitle");

		try {
			/* ****************************START Section 3***************************** */
			String section3SlidesIndexes = ParamUtil.getString(request, "section3SlidesIndexes");
			String[] section3RowIndexes = section3SlidesIndexes.split(",");
			/*delete all old slider3 entries*/
			List<ResidenceSlider3> residenceSlider3Slides = ResidenceSlider3LocalServiceUtil.getResidenceSlider3Slides(residenceId);
			for (ResidenceSlider3 curResidenceSlider3Slide : residenceSlider3Slides) {
				_residenceSlider3LocalService.deleteResidenceSlider3(curResidenceSlider3Slide);
			}
			/*add all slider3 entries*/
			for (int i = 0; i < section3RowIndexes.length; i++) {
				Map<Locale, String> section3ImageMap = LocalizationUtil.getLocalizationMap(request, "section3Image" + i);
				Map<Locale, String> section3DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section3Description" + i);
				Map<Locale, String> section3ThumbnailMap = LocalizationUtil.getLocalizationMap(request, "section3Thumbnail" + i);

				_residenceSlider3LocalService.addResidenceSlider3(serviceContext.getUserId(),
						residenceId,
						section3ImageMap, section3DescriptionMap, section3ThumbnailMap,
						serviceContext);
			}
			/* ****************************START Section 4***************************** */
			String section4SlidesIndexes = ParamUtil.getString(request, "section4SlidesIndexes");
			String[] section4RowIndexes = section4SlidesIndexes.split(",");

			/*delete all old slider3 entries*/
			List<ResidenceSlider4> residenceSlider4Slides = ResidenceSlider4LocalServiceUtil.getResidenceSlider4Slides(residenceId);
			for (ResidenceSlider4 curResidenceSlider4Slide : residenceSlider4Slides) {
				_residenceSlider4LocalService.deleteResidenceSlider4(curResidenceSlider4Slide);
			}
			/*add all slider4 entries*/
			for (int i = 0; i < section4RowIndexes.length; i++) {
				Map<Locale, String> section4TitleMap = LocalizationUtil.getLocalizationMap(request, "section4Title" + i);
				Map<Locale, String> section4ImageMap = LocalizationUtil.getLocalizationMap(request, "section4Image" + i);
				Map<Locale, String> section4DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section4Description" + i);
				Map<Locale, String> section4ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section4ButtonLabel" + i);
				Map<Locale, String> section4ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section4ButtonLink" + i);

				_residenceSlider4LocalService.addResidenceSlider4(serviceContext.getUserId(),
						residenceId,
						section4TitleMap, section4DescriptionMap, section4ImageMap, section4ButtonLabelMap, section4ButtonLinkMap,
						serviceContext);
			}
			/* *************************** */
			_residenceLocalService.addResidence(serviceContext.getUserId(), residenceId,
					listingImageMap, listingTitleMap, listingDescriptionMap,
					bannerTitleMap, bannerSubtitleMap, bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
					section2TitleMap, section2SubtitleMap, section2DescriptionMap, section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
					section5TitleMap, section5SubtitleMap, section5DescriptionMap, section5ButtonLabelMap, section5ButtonLinkMap, section5ImageMap,
					section6TitleMap, section6SubtitleMap, section6DescriptionMap, section6ButtonLabelMap, section6ButtonLinkMap, section6ImageMap,
					section4SectionTitleMap,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(ResidencesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_residence.jsp");
		}
	}

	public void updateResidence(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Residence.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long residenceId = ParamUtil.getLong(request, "residenceId");

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");

		Map<Locale, String> bannerTitleMap = LocalizationUtil.getLocalizationMap(request, "bannerTitle");
		Map<Locale, String> bannerSubtitleMap = LocalizationUtil.getLocalizationMap(request, "bannerSubtitle");
		Map<Locale, String> bannerDescriptionMap = LocalizationUtil.getLocalizationMap(request, "bannerDescription");
		Map<Locale, String> bannerDesktopImageMap = LocalizationUtil.getLocalizationMap(request, "bannerDesktopImage");
		Map<Locale, String> bannerMobileImageMap = LocalizationUtil.getLocalizationMap(request, "bannerMobileImage");
		Map<Locale, String> section2TitleMap = LocalizationUtil.getLocalizationMap(request, "section2Title");
		Map<Locale, String> section2SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section2Subtitle");
		Map<Locale, String> section2DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section2Description");
		Map<Locale, String> section2ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLabel");
		Map<Locale, String> section2ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLink");
		Map<Locale, String> section2ImageMap = LocalizationUtil.getLocalizationMap(request, "section2Image");

		Map<Locale, String> section5TitleMap = LocalizationUtil.getLocalizationMap(request, "section5Title");
		Map<Locale, String> section5SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section5Subtitle");
		Map<Locale, String> section5DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section5Description");
		Map<Locale, String> section5ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section5ButtonLabel");
		Map<Locale, String> section5ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section5ButtonLink");
		Map<Locale, String> section5ImageMap = LocalizationUtil.getLocalizationMap(request, "section5Image");

		Map<Locale, String> section6TitleMap = LocalizationUtil.getLocalizationMap(request, "section6Title");
		Map<Locale, String> section6SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section6Subtitle");
		Map<Locale, String> section6DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section6Description");
		Map<Locale, String> section6ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section6ButtonLabel");
		Map<Locale, String> section6ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section6ButtonLink");
		Map<Locale, String> section6ImageMap = LocalizationUtil.getLocalizationMap(request, "section6Image");

		Map<Locale, String> section4SectionTitleMap = LocalizationUtil.getLocalizationMap(request, "section4SectionTitle");

		try {
			/* ****************************START Section 3***************************** */
			String section3SlidesIndexes = ParamUtil.getString(request, "section3SlidesIndexes");
			String[] section3RowIndexes = section3SlidesIndexes.split(",");
			/*delete all old slider3 entries*/
			List<ResidenceSlider3> residenceSlider3Slides = ResidenceSlider3LocalServiceUtil.getResidenceSlider3Slides(residenceId);
			for (ResidenceSlider3 curResidenceSlider3Slide : residenceSlider3Slides) {
				_residenceSlider3LocalService.deleteResidenceSlider3(curResidenceSlider3Slide);
			}
			/*add all slider3 entries*/
			for (int i = 0; i < section3RowIndexes.length; i++) {
				Map<Locale, String> section3ImageMap = LocalizationUtil.getLocalizationMap(request, "section3Image" + i);
				Map<Locale, String> section3DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section3Description" + i);
				Map<Locale, String> section3ThumbnailMap = LocalizationUtil.getLocalizationMap(request, "section3Thumbnail" + i);

				_residenceSlider3LocalService.addResidenceSlider3(serviceContext.getUserId(),
						residenceId,
						section3ImageMap, section3DescriptionMap, section3ThumbnailMap,
						serviceContext);
			}
			/* ****************************START Section 4***************************** */
			String section4SlidesIndexes = ParamUtil.getString(request, "section4SlidesIndexes");
			String[] section4RowIndexes = section4SlidesIndexes.split(",");

			/*delete all old slider3 entries*/
			List<ResidenceSlider4> residenceSlider4Slides = ResidenceSlider4LocalServiceUtil.getResidenceSlider4Slides(residenceId);
			for (ResidenceSlider4 curResidenceSlider4Slide : residenceSlider4Slides) {
				_residenceSlider4LocalService.deleteResidenceSlider4(curResidenceSlider4Slide);
			}
			/*add all slider4 entries*/
			for (int i = 0; i < section4RowIndexes.length; i++) {
				Map<Locale, String> section4TitleMap = LocalizationUtil.getLocalizationMap(request, "section4Title" + i);
				Map<Locale, String> section4ImageMap = LocalizationUtil.getLocalizationMap(request, "section4Image" + i);
				Map<Locale, String> section4DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section4Description" + i);
				Map<Locale, String> section4ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section4ButtonLabel" + i);
				Map<Locale, String> section4ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section4ButtonLink" + i);

				_residenceSlider4LocalService.addResidenceSlider4(serviceContext.getUserId(),
						residenceId,
						section4TitleMap, section4DescriptionMap, section4ImageMap, section4ButtonLabelMap, section4ButtonLinkMap,
						serviceContext);
			}
			/* *************************** */
			_residenceLocalService.updateResidence(serviceContext.getUserId(), residenceId,
					listingImageMap, listingTitleMap, listingDescriptionMap,
					bannerTitleMap, bannerSubtitleMap, bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
					section2TitleMap, section2SubtitleMap, section2DescriptionMap, section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
					section5TitleMap, section5SubtitleMap, section5DescriptionMap, section5ButtonLabelMap, section5ButtonLinkMap, section5ImageMap,
					section6TitleMap, section6SubtitleMap, section6DescriptionMap, section6ButtonLabelMap, section6ButtonLinkMap, section6ImageMap,
					section4SectionTitleMap,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(ResidencesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_residence.jsp");
		}
	}

	public void deleteResidence(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Residence.class.getName(), request);

		long residenceId = ParamUtil.getLong(request, "residenceId");

		try {
			_residenceLocalService.deleteResidence(residenceId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(ResidencesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private ResidenceLocalService _residenceLocalService;

	@Reference
	private ResidenceSlider3LocalService _residenceSlider3LocalService;

	@Reference
	private ResidenceSlider4LocalService _residenceSlider4LocalService;

	@Reference
	private ItemSelector _itemSelector;

	@Reference
	protected CounterLocalService _counterLocalService;
}