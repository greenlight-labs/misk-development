package residences.admin.web.constants;

/**
 * @author tz
 */
public class ResidencesAdminWebPortletKeys {

	public static final String RESIDENCESADMINWEB =
		"residences_admin_web_ResidencesAdminWebPortlet";

}