<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="residences-admin-buttons">
        <portlet:renderURL var="addResidenceURL">
            <portlet:param name="mvcPath"
                           value="/edit_residence.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addResidenceURL.toString() %>"
                    value="Add Residence"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= ResidenceLocalServiceUtil.getResidencesCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= ResidenceLocalServiceUtil.getResidences(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="residences.model.Residence" modelVar="residence">

            <liferay-ui:search-container-column-text
                    name="Listing Title"
                    value="<%= HtmlUtil.escape(residence.getListingTitle(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>