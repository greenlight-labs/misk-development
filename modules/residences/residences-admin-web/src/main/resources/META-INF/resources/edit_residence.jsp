<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="residences.model.ResidenceSlider3" %>
<%@ page import="residences.model.ResidenceSlider4" %>
<%@ page import="residences.service.ResidenceSlider3LocalServiceUtil" %>
<%@ page import="residences.service.ResidenceSlider4LocalServiceUtil" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@include file = "init.jsp" %>

<%
    long residenceId = ParamUtil.getLong(request, "residenceId");

    Residence residence = null;

    if (residenceId > 0) {
        try {
            residence = ResidenceLocalServiceUtil.getResidence(residenceId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    /* *****************************************************/
    int[] section3SlidesIndexes = null;
    List<ResidenceSlider3> residenceSlider3Slides = Collections.emptyList();
    String section3SlidesIndexesParam = ParamUtil.getString(request, "section3SlidesIndexes");
    if (Validator.isNotNull(section3SlidesIndexesParam)) {
        residenceSlider3Slides = new ArrayList<ResidenceSlider3>();

        section3SlidesIndexes = StringUtil.split(section3SlidesIndexesParam, 0);

        for (int section3SlidesIndex : section3SlidesIndexes) {
            ResidenceSlider3 residenceSlider3Slide = ResidenceSlider3LocalServiceUtil.createResidenceSlider3(0);

            residenceSlider3Slides.add(residenceSlider3Slide);
        }
    }
    else {
        if (residence != null) {
            residenceSlider3Slides = ResidenceSlider3LocalServiceUtil.getResidenceSlider3Slides(residence.getResidenceId());

            section3SlidesIndexes = new int[residenceSlider3Slides.size()];

            for (int i = 0; i < residenceSlider3Slides.size(); i++) {
                section3SlidesIndexes[i] = i;
            }
        }

        if (residenceSlider3Slides.isEmpty()) {
            residenceSlider3Slides = new ArrayList<ResidenceSlider3>();

            ResidenceSlider3 residenceSlider3Slide = ResidenceSlider3LocalServiceUtil.createResidenceSlider3(0);

            residenceSlider3Slides.add(residenceSlider3Slide);

            section3SlidesIndexes = new int[] {0};
        }

        if (section3SlidesIndexes == null) {
            section3SlidesIndexes = new int[0];
        }
    }
    /* *****************************************************/
    int[] section4SlidesIndexes = null;
    List<ResidenceSlider4> residenceSlider4Slides = Collections.emptyList();
    String section4SlidesIndexesParam = ParamUtil.getString(request, "section4SlidesIndexes");
    if (Validator.isNotNull(section4SlidesIndexesParam)) {
        residenceSlider4Slides = new ArrayList<ResidenceSlider4>();

        section4SlidesIndexes = StringUtil.split(section4SlidesIndexesParam, 0);

        for (int categoryPropertiesIndex : section4SlidesIndexes) {
            ResidenceSlider4 residenceSlider4Slide = ResidenceSlider4LocalServiceUtil.createResidenceSlider4(0);

            residenceSlider4Slides.add(residenceSlider4Slide);
        }
    }
    else {
        if (residence != null) {
            residenceSlider4Slides = ResidenceSlider4LocalServiceUtil.getResidenceSlider4Slides(residence.getResidenceId());

            section4SlidesIndexes = new int[residenceSlider4Slides.size()];

            for (int i = 0; i < residenceSlider4Slides.size(); i++) {
                section4SlidesIndexes[i] = i;
            }
        }

        if (residenceSlider4Slides.isEmpty()) {
            residenceSlider4Slides = new ArrayList<ResidenceSlider4>();

            ResidenceSlider4 residenceSlider4Slide = ResidenceSlider4LocalServiceUtil.createResidenceSlider4(0);

            residenceSlider4Slides.add(residenceSlider4Slide);

            section4SlidesIndexes = new int[] {0};
        }

        if (section4SlidesIndexes == null) {
            section4SlidesIndexes = new int[0];
        }
    }
    /* *****************************************************/
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= residence == null ? "addResidence" : "updateResidence" %>' var="editResidenceURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editResidenceURL %>" name="fm">

    <aui:model-context bean="<%= residence %>" model="<%= Residence.class %>" />

    <aui:input type="hidden" name="residenceId"
               value='<%= residence == null ? "" : residence.getResidenceId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:input name="listingTitle" label="Listing Title" autoFocus="true" helpMessage="Max 45 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="listingImage" label="Listing Image" helpMessage="Image Dimensions: 570 x 420 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="listingImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="listingDescription" label="Listing Description" helpMessage="Max 125 Characters (Recommended)" />
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Banner Section">
            <aui:input name="bannerTitle" label="Title" helpMessage="Max 15 Characters (Recommended)" />
            <aui:input name="bannerSubtitle" label="Subtitle" helpMessage="Max 15 Characters (Recommended)" />
            <aui:input name="bannerDescription" label="Description" helpMessage="Max 115 Characters (Recommended)" />
            <aui:input name="bannerDesktopImage" label="Desktop Image" helpMessage="Image Dimensions: 1920 x 880 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="bannerDesktopImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="bannerMobileImage" label="Mobile Image" helpMessage="Image Dimensions: 1920 x 880 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="bannerMobileImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Section 2">
            <aui:input name="section2Title" label="Title" helpMessage="Max 35 Characters (Recommended)" />
            <aui:input name="section2Subtitle" label="Subtitle" helpMessage="Max 60 Characters (Recommended)" />
            <aui:input name="section2Description" label="Description" helpMessage="Max 330 Characters (Recommended)" />
            <aui:input name="section2ButtonLabel" label="Button Label" />
            <aui:input name="section2ButtonLink" label="Button Link" />
            <aui:input name="section2Image" label="Image" helpMessage="Image Dimensions: 704 x 1000 pixels">
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="section2Image">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Text Block Section 1">
            <aui:input name="section5Title" label="Title" helpMessage="Max 35 Characters (Recommended)" />
            <aui:input name="section5Subtitle" label="Subtitle" helpMessage="Max 60 Characters (Recommended)" />
            <aui:input name="section5Description" label="Description" helpMessage="Max 330 Characters (Recommended)" />
            <aui:input name="section5ButtonLabel" label="Button Label" />
            <aui:input name="section5ButtonLink" label="Button Link" />
            <aui:input name="section5Image" label="Image" helpMessage="Image Dimensions: 704 x 1000 pixels">
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="section5Image">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Text Block Section 2">
            <aui:input name="section6Title" label="Title" helpMessage="Max 35 Characters (Recommended)" />
            <aui:input name="section6Subtitle" label="Subtitle" helpMessage="Max 60 Characters (Recommended)" />
            <aui:input name="section6Description" label="Description" helpMessage="Max 330 Characters (Recommended)" />
            <aui:input name="section6ButtonLabel" label="Button Label" />
            <aui:input name="section6ButtonLink" label="Button Link" />
            <aui:input name="section6Image" label="Image" helpMessage="Image Dimensions: 704 x 1000 pixels">
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="section6Image">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
        </aui:fieldset>
        <aui:fieldset>
            <aui:input name="section4SectionTitle" label="Section 4 Section Title" helpMessage="Max 45 Characters (Recommended)" />
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Section 3" id='<%= renderResponse.getNamespace() + "section3Slides" %>'>
            <%
                for (int i = 0; i < section3SlidesIndexes.length; i++) {
                    int section3SlideIndex = section3SlidesIndexes[i];
                    ResidenceSlider3 residenceSlider3 = residenceSlider3Slides.get(i);
            %>
            <aui:model-context bean="<%= residenceSlider3 %>" model="<%= ResidenceSlider3.class %>" />
            <div class="field-row lfr-form-row lfr-form-row-inline">
                <div class="row-fields">
                    <aui:field-wrapper>
                        <div style="color:#bc1a1ab3" name='<%= "slide" + section3SlideIndex %>' class="slide-heading"><h3>Slide <%= section3SlideIndex+1 %></h3></div>
                        <aui:input label="Image" fieldParam='<%= "section3Image" + section3SlideIndex %>' id='<%= "section3Image" + section3SlideIndex %>' name="image" helpMessage="Image Dimensions: 1920 x 1080 pixels">
                            <aui:validator name="required"/>
                            <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                                function(val) {
                                    var ext = val.substring(val.lastIndexOf('.') + 1);
                                    return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                                }
                            </aui:validator>
                        </aui:input>
                        <div class="button-holder">
                            <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "section3Image" + section3SlideIndex %>">
                                <span class="lfr-btn-label">Select</span>
                            </button>
                        </div>
                        <aui:input label="Description" fieldParam='<%= "section3Description" + section3SlideIndex %>' id='<%= "section3Description" + section3SlideIndex %>' name="description" helpMessage="Max 45 Characters (Recommended)">
                            <aui:validator name="required"/>
                        </aui:input>
                        <aui:input label="Thumbnail" fieldParam='<%= "section3Thumbnail" + section3SlideIndex %>' id='<%= "section3Thumbnail" + section3SlideIndex %>' name="thumbnail" helpMessage="Image Dimensions: 204 x 156 pixels">
                            <aui:validator name="required"/>
                            <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                                function(val) {
                                    var ext = val.substring(val.lastIndexOf('.') + 1);
                                    return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                                }
                            </aui:validator>
                        </aui:input>
                        <div class="button-holder">
                            <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "section3Thumbnail" + section3SlideIndex %>">
                                <span class="lfr-btn-label">Select</span>
                            </button>
                        </div>
                    </aui:field-wrapper>
                </div>
            </div>
            <%
                }
            %>
            <aui:input name="section3SlidesIndexes" type="hidden" value="<%= StringUtil.merge(section3SlidesIndexes) %>" />
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Section 4" id='<%= renderResponse.getNamespace() + "section4Slides" %>'>
            <%
                for (int i = 0; i < section4SlidesIndexes.length; i++) {
                    int section4SlideIndex = section4SlidesIndexes[i];
                    ResidenceSlider4 residenceSlider4 = residenceSlider4Slides.get(i);
            %>
            <aui:model-context bean="<%= residenceSlider4 %>" model="<%= ResidenceSlider4.class %>" />
            <div class="field-row lfr-form-row lfr-form-row-inline">
                <div class="row-fields">
                    <aui:field-wrapper>
                        <div style="color:#bc1a1ab3" name='<%= "slide" + section4SlideIndex %>' class="slide-heading"><h3>Slide <%= section4SlideIndex+1 %></h3></div>
                        <aui:input label="Title" fieldParam='<%= "section4Title" + section4SlideIndex %>' id='<%= "section4Title" + section4SlideIndex %>' name="title" helpMessage="Max 45 Characters (Recommended)">
                            <aui:validator name="required"/>
                        </aui:input>
                        <aui:input label="Description" fieldParam='<%= "section4Description" + section4SlideIndex %>' id='<%= "section4Description" + section4SlideIndex %>' name="description" helpMessage="Max 45 Characters (Recommended)">
                            <aui:validator name="required"/>
                        </aui:input>
                        <aui:input label="Image" fieldParam='<%= "section4Image" + section4SlideIndex %>' id='<%= "section4Image" + section4SlideIndex %>' name="image" helpMessage="Image Dimensions: 1920 x 1080 pixels">
                            <aui:validator name="required"/>
                            <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                                function(val) {
                                    var ext = val.substring(val.lastIndexOf('.') + 1);
                                    return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                                }
                            </aui:validator>
                        </aui:input>
                        <div class="button-holder">
                            <button class="btn select-button-repeater btn-secondary" type="button" name="<%= "section4Image" + section4SlideIndex %>">
                                <span class="lfr-btn-label">Select</span>
                            </button>
                        </div>
                        <aui:input label="Button Label" fieldParam='<%= "section4ButtonLabel" + section4SlideIndex %>' id='<%= "section4ButtonLabel" + section4SlideIndex %>' name="buttonLabel" />
                        <aui:input label="Button Link" fieldParam='<%= "section4ButtonLink" + section4SlideIndex %>' id='<%= "section4ButtonLink" + section4SlideIndex %>' name="buttonLink" />
                    </aui:field-wrapper>
                </div>
            </div>
            <%
                }
            %>
            <aui:input name="section4SlidesIndexes" type="hidden" value="<%= StringUtil.merge(section4SlidesIndexes) %>" />
        </aui:fieldset>
        <aui:script use="liferay-auto-fields">
            new Liferay.AutoFields({
            contentBox: '#<portlet:namespace />section3Slides',
            fieldIndexes: '<portlet:namespace />section3SlidesIndexes',
            namespace: '<portlet:namespace />',
            sortable: true,
            sortableHandle: '.lfr-form-row'
            }).render();
            new Liferay.AutoFields({
            contentBox: '#<portlet:namespace />section4Slides',
            fieldIndexes: '<portlet:namespace />section4SlidesIndexes',
            namespace: '<portlet:namespace />',
            sortable: true,
            sortableHandle: '.lfr-form-row'
            }).render();
        </aui:script>
    </aui:fieldset-group>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
    /*Repeater fields image selection*/
    var section3Container = $('#<portlet:namespace />section3Slides');
    section3Container.on('click','.select-button-repeater',function(event){
        event.preventDefault();
		var id=this.name.match(/(\d+)/)[0];
        var element = event.currentTarget;
        var fieldId = element.name;
		var itemSelectorDialog = new A.LiferayItemSelectorDialog
		(
			{
				eventName: 'selectDocumentLibrary',
				on: {
						selectedItemChange: function(event) {
							var selectedItem = event.newVal;
							if(selectedItem)
							{
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
						   }
						}
				},
				title: '<liferay-ui:message key="Select File" />',
				url: '${itemSelectorURL }'
			}
		);
		itemSelectorDialog.open();

	});
    /*change slide number on add/delete row*/
    section3Container.on('click', '.add-row', function(event){
        event.preventDefault();
        var element = event.currentTarget;
        var panelBody = $(this).parents('div.panel-body');
        var fieldRows = panelBody.find('div.field-row');
        fieldRows.each(function(index, item) {
            var slideHeading = $(item).find('div.slide-heading');
            /*var fieldId = slideHeading.attr('name');
            var id0 = fieldId.match(/(\d+)/)[0];
            var id = parseInt(id0)+1;*/
            var id = index+1;
            slideHeading.find('h3').text('Slide '+id);
        });
	});
    /*hide help text appears below the fields when adding localized paramter for autofields*/
    var helpTextFields = section3Container.find('div.form-text');
    helpTextFields.each(function(index, item) {
        $(item).html('');
    });
    /*******************************Section 4**********************************************/
    /*Repeater fields image selection*/
    var section4Container = $('#<portlet:namespace />section4Slides');
    section4Container.on('click','.select-button-repeater',function(event){
        event.preventDefault();
		var id=this.name.match(/(\d+)/)[0];
        var element = event.currentTarget;
        var fieldId = element.name;
		var itemSelectorDialog = new A.LiferayItemSelectorDialog
		(
			{
				eventName: 'selectDocumentLibrary',
				on: {
						selectedItemChange: function(event) {
							var selectedItem = event.newVal;
							if(selectedItem)
							{
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
						   }
						}
				},
				title: '<liferay-ui:message key="Select File" />',
				url: '${itemSelectorURL }'
			}
		);
		itemSelectorDialog.open();

	});
    /*change slide number on add/delete row*/
    section4Container.on('click', '.add-row', function(event){
        event.preventDefault();
        var element = event.currentTarget;
        var panelBody = $(this).parents('div.panel-body');
        var fieldRows = panelBody.find('div.field-row');
        fieldRows.each(function(index, item) {
            var slideHeading = $(item).find('div.slide-heading');
            /*var fieldId = slideHeading.attr('name');
            var id0 = fieldId.match(/(\d+)/)[0];
            var id = parseInt(id0)+1;*/
            var id = index+1;
            slideHeading.find('h3').text('Slide '+id);
        });
	});
    /*hide help text appears below the fields when adding localized paramter for autofields*/
    var helpTextFields = section4Container.find('div.form-text');
    helpTextFields.each(function(index, item) {
        $(item).html('');
    });
</aui:script>