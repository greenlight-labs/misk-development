/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CourtLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CourtLocalService
 * @generated
 */
public class CourtLocalServiceWrapper
	implements CourtLocalService, ServiceWrapper<CourtLocalService> {

	public CourtLocalServiceWrapper(CourtLocalService courtLocalService) {
		_courtLocalService = courtLocalService;
	}

	/**
	 * Adds the court to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was added
	 */
	@Override
	public courts.model.Court addCourt(courts.model.Court court) {
		return _courtLocalService.addCourt(court);
	}

	@Override
	public courts.model.Court addEntry(
			courts.model.Court orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   courts.exception.CourtValidateException {

		return _courtLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public int countAllInCategory(long categoryId) {
		return _courtLocalService.countAllInCategory(categoryId);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _courtLocalService.countAllInGroup(groupId);
	}

	/**
	 * Creates a new court with the primary key. Does not add the court to the database.
	 *
	 * @param courtId the primary key for the new court
	 * @return the new court
	 */
	@Override
	public courts.model.Court createCourt(long courtId) {
		return _courtLocalService.createCourt(courtId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the court from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was removed
	 */
	@Override
	public courts.model.Court deleteCourt(courts.model.Court court) {
		return _courtLocalService.deleteCourt(court);
	}

	/**
	 * Deletes the court with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtId the primary key of the court
	 * @return the court that was removed
	 * @throws PortalException if a court with the primary key could not be found
	 */
	@Override
	public courts.model.Court deleteCourt(long courtId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtLocalService.deleteCourt(courtId);
	}

	@Override
	public courts.model.Court deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _courtLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _courtLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _courtLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _courtLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _courtLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _courtLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public courts.model.Court fetchCourt(long courtId) {
		return _courtLocalService.fetchCourt(courtId);
	}

	/**
	 * Returns the court matching the UUID and group.
	 *
	 * @param uuid the court's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court, or <code>null</code> if a matching court could not be found
	 */
	@Override
	public courts.model.Court fetchCourtByUuidAndGroupId(
		String uuid, long groupId) {

		return _courtLocalService.fetchCourtByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<courts.model.Court> findAllInCategory(
		long categoryId) {

		return _courtLocalService.findAllInCategory(categoryId);
	}

	@Override
	public java.util.List<courts.model.Court> findAllInCategory(
		long categoryId, int start, int end) {

		return _courtLocalService.findAllInCategory(categoryId, start, end);
	}

	@Override
	public java.util.List<courts.model.Court> findAllInCategory(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<courts.model.Court>
			obc) {

		return _courtLocalService.findAllInCategory(
			categoryId, start, end, obc);
	}

	@Override
	public java.util.List<courts.model.Court> findAllInGroup(long groupId) {
		return _courtLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<courts.model.Court> findAllInGroup(
		long groupId, int start, int end) {

		return _courtLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<courts.model.Court> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<courts.model.Court>
			obc) {

		return _courtLocalService.findAllInGroup(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _courtLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the court with the primary key.
	 *
	 * @param courtId the primary key of the court
	 * @return the court
	 * @throws PortalException if a court with the primary key could not be found
	 */
	@Override
	public courts.model.Court getCourt(long courtId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtLocalService.getCourt(courtId);
	}

	/**
	 * Returns the court matching the UUID and group.
	 *
	 * @param uuid the court's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court
	 * @throws PortalException if a matching court could not be found
	 */
	@Override
	public courts.model.Court getCourtByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtLocalService.getCourtByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public courts.model.Court getCourtFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws courts.exception.CourtValidateException,
			   javax.portlet.PortletException {

		return _courtLocalService.getCourtFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of courts
	 */
	@Override
	public java.util.List<courts.model.Court> getCourts(int start, int end) {
		return _courtLocalService.getCourts(start, end);
	}

	/**
	 * Returns all the courts matching the UUID and company.
	 *
	 * @param uuid the UUID of the courts
	 * @param companyId the primary key of the company
	 * @return the matching courts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<courts.model.Court> getCourtsByUuidAndCompanyId(
		String uuid, long companyId) {

		return _courtLocalService.getCourtsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of courts matching the UUID and company.
	 *
	 * @param uuid the UUID of the courts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching courts, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<courts.model.Court> getCourtsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<courts.model.Court>
			orderByComparator) {

		return _courtLocalService.getCourtsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of courts.
	 *
	 * @return the number of courts
	 */
	@Override
	public int getCourtsCount() {
		return _courtLocalService.getCourtsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _courtLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _courtLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public courts.model.Court getNewObject(long primaryKey) {
		return _courtLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _courtLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public java.util.List<courts.model.Court> searchCourts(
		SearchContext searchContext) {

		return _courtLocalService.searchCourts(searchContext);
	}

	/**
	 * Updates the court in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was updated
	 */
	@Override
	public courts.model.Court updateCourt(courts.model.Court court) {
		return _courtLocalService.updateCourt(court);
	}

	@Override
	public courts.model.Court updateEntry(
			courts.model.Court orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   courts.exception.CourtValidateException {

		return _courtLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public CourtLocalService getWrappedService() {
		return _courtLocalService;
	}

	@Override
	public void setWrappedService(CourtLocalService courtLocalService) {
		_courtLocalService = courtLocalService;
	}

	private CourtLocalService _courtLocalService;

}