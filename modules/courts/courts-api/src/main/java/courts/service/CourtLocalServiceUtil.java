/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import courts.model.Court;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Court. This utility wraps
 * <code>courts.service.impl.CourtLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see CourtLocalService
 * @generated
 */
public class CourtLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>courts.service.impl.CourtLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the court to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was added
	 */
	public static Court addCourt(Court court) {
		return getService().addCourt(court);
	}

	public static Court addEntry(
			Court orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws courts.exception.CourtValidateException, PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static int countAllInCategory(long categoryId) {
		return getService().countAllInCategory(categoryId);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * Creates a new court with the primary key. Does not add the court to the database.
	 *
	 * @param courtId the primary key for the new court
	 * @return the new court
	 */
	public static Court createCourt(long courtId) {
		return getService().createCourt(courtId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the court from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was removed
	 */
	public static Court deleteCourt(Court court) {
		return getService().deleteCourt(court);
	}

	/**
	 * Deletes the court with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtId the primary key of the court
	 * @return the court that was removed
	 * @throws PortalException if a court with the primary key could not be found
	 */
	public static Court deleteCourt(long courtId) throws PortalException {
		return getService().deleteCourt(courtId);
	}

	public static Court deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Court fetchCourt(long courtId) {
		return getService().fetchCourt(courtId);
	}

	/**
	 * Returns the court matching the UUID and group.
	 *
	 * @param uuid the court's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchCourtByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchCourtByUuidAndGroupId(uuid, groupId);
	}

	public static List<Court> findAllInCategory(long categoryId) {
		return getService().findAllInCategory(categoryId);
	}

	public static List<Court> findAllInCategory(
		long categoryId, int start, int end) {

		return getService().findAllInCategory(categoryId, start, end);
	}

	public static List<Court> findAllInCategory(
		long categoryId, int start, int end, OrderByComparator<Court> obc) {

		return getService().findAllInCategory(categoryId, start, end, obc);
	}

	public static List<Court> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<Court> findAllInGroup(long groupId, int start, int end) {
		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<Court> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Court> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the court with the primary key.
	 *
	 * @param courtId the primary key of the court
	 * @return the court
	 * @throws PortalException if a court with the primary key could not be found
	 */
	public static Court getCourt(long courtId) throws PortalException {
		return getService().getCourt(courtId);
	}

	/**
	 * Returns the court matching the UUID and group.
	 *
	 * @param uuid the court's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court
	 * @throws PortalException if a matching court could not be found
	 */
	public static Court getCourtByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getCourtByUuidAndGroupId(uuid, groupId);
	}

	public static Court getCourtFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws courts.exception.CourtValidateException,
			   javax.portlet.PortletException {

		return getService().getCourtFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of courts
	 */
	public static List<Court> getCourts(int start, int end) {
		return getService().getCourts(start, end);
	}

	/**
	 * Returns all the courts matching the UUID and company.
	 *
	 * @param uuid the UUID of the courts
	 * @param companyId the primary key of the company
	 * @return the matching courts, or an empty list if no matches were found
	 */
	public static List<Court> getCourtsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getCourtsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of courts matching the UUID and company.
	 *
	 * @param uuid the UUID of the courts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching courts, or an empty list if no matches were found
	 */
	public static List<Court> getCourtsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Court> orderByComparator) {

		return getService().getCourtsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of courts.
	 *
	 * @return the number of courts
	 */
	public static int getCourtsCount() {
		return getService().getCourtsCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Court getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static List<Court> searchCourts(SearchContext searchContext) {
		return getService().searchCourts(searchContext);
	}

	/**
	 * Updates the court in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was updated
	 */
	public static Court updateCourt(Court court) {
		return getService().updateCourt(court);
	}

	public static Court updateEntry(
			Court orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws courts.exception.CourtValidateException, PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static CourtLocalService getService() {
		return _service;
	}

	private static volatile CourtLocalService _service;

}