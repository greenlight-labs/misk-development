/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import courts.model.Court;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the court service. This utility wraps <code>courts.service.persistence.impl.CourtPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtPersistence
 * @generated
 */
public class CourtUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Court court) {
		getPersistence().clearCache(court);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Court> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Court> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Court> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Court> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Court> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Court update(Court court) {
		return getPersistence().update(court);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Court update(Court court, ServiceContext serviceContext) {
		return getPersistence().update(court, serviceContext);
	}

	/**
	 * Returns all the courts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching courts
	 */
	public static List<Court> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the courts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public static List<Court> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the courts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Court> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the courts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Court> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByUuid_First(
			String uuid, OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByUuid_First(
		String uuid, OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByUuid_Last(
			String uuid, OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByUuid_Last(
		String uuid, OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the courts before and after the current court in the ordered set where uuid = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public static Court[] findByUuid_PrevAndNext(
			long courtId, String uuid,
			OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByUuid_PrevAndNext(
			courtId, uuid, orderByComparator);
	}

	/**
	 * Removes all the courts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of courts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching courts
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the court where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourtException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByUUID_G(String uuid, long groupId)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the court where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the court where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the court where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the court that was removed
	 */
	public static Court removeByUUID_G(String uuid, long groupId)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of courts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching courts
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching courts
	 */
	public static List<Court> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public static List<Court> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Court> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Court> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the courts before and after the current court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public static Court[] findByUuid_C_PrevAndNext(
			long courtId, String uuid, long companyId,
			OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByUuid_C_PrevAndNext(
			courtId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the courts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of courts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching courts
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the courts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching courts
	 */
	public static List<Court> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the courts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public static List<Court> findByGroupId(long groupId, int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the courts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Court> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the courts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Court> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByGroupId_First(
			long groupId, OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByGroupId_First(
		long groupId, OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByGroupId_Last(
			long groupId, OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByGroupId_Last(
		long groupId, OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the courts before and after the current court in the ordered set where groupId = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public static Court[] findByGroupId_PrevAndNext(
			long courtId, long groupId,
			OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByGroupId_PrevAndNext(
			courtId, groupId, orderByComparator);
	}

	/**
	 * Removes all the courts where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of courts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching courts
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the courts where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching courts
	 */
	public static List<Court> findByCategoryId(long categoryId) {
		return getPersistence().findByCategoryId(categoryId);
	}

	/**
	 * Returns a range of all the courts where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public static List<Court> findByCategoryId(
		long categoryId, int start, int end) {

		return getPersistence().findByCategoryId(categoryId, start, end);
	}

	/**
	 * Returns an ordered range of all the courts where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Court> orderByComparator) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the courts where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public static List<Court> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Court> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByCategoryId_First(
			long categoryId, OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the first court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByCategoryId_First(
		long categoryId, OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public static Court findByCategoryId_Last(
			long categoryId, OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public static Court fetchByCategoryId_Last(
		long categoryId, OrderByComparator<Court> orderByComparator) {

		return getPersistence().fetchByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the courts before and after the current court in the ordered set where categoryId = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public static Court[] findByCategoryId_PrevAndNext(
			long courtId, long categoryId,
			OrderByComparator<Court> orderByComparator)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByCategoryId_PrevAndNext(
			courtId, categoryId, orderByComparator);
	}

	/**
	 * Removes all the courts where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public static void removeByCategoryId(long categoryId) {
		getPersistence().removeByCategoryId(categoryId);
	}

	/**
	 * Returns the number of courts where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching courts
	 */
	public static int countByCategoryId(long categoryId) {
		return getPersistence().countByCategoryId(categoryId);
	}

	/**
	 * Caches the court in the entity cache if it is enabled.
	 *
	 * @param court the court
	 */
	public static void cacheResult(Court court) {
		getPersistence().cacheResult(court);
	}

	/**
	 * Caches the courts in the entity cache if it is enabled.
	 *
	 * @param courts the courts
	 */
	public static void cacheResult(List<Court> courts) {
		getPersistence().cacheResult(courts);
	}

	/**
	 * Creates a new court with the primary key. Does not add the court to the database.
	 *
	 * @param courtId the primary key for the new court
	 * @return the new court
	 */
	public static Court create(long courtId) {
		return getPersistence().create(courtId);
	}

	/**
	 * Removes the court with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courtId the primary key of the court
	 * @return the court that was removed
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public static Court remove(long courtId)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().remove(courtId);
	}

	public static Court updateImpl(Court court) {
		return getPersistence().updateImpl(court);
	}

	/**
	 * Returns the court with the primary key or throws a <code>NoSuchCourtException</code> if it could not be found.
	 *
	 * @param courtId the primary key of the court
	 * @return the court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public static Court findByPrimaryKey(long courtId)
		throws courts.exception.NoSuchCourtException {

		return getPersistence().findByPrimaryKey(courtId);
	}

	/**
	 * Returns the court with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courtId the primary key of the court
	 * @return the court, or <code>null</code> if a court with the primary key could not be found
	 */
	public static Court fetchByPrimaryKey(long courtId) {
		return getPersistence().fetchByPrimaryKey(courtId);
	}

	/**
	 * Returns all the courts.
	 *
	 * @return the courts
	 */
	public static List<Court> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of courts
	 */
	public static List<Court> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of courts
	 */
	public static List<Court> findAll(
		int start, int end, OrderByComparator<Court> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of courts
	 */
	public static List<Court> findAll(
		int start, int end, OrderByComparator<Court> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the courts from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of courts.
	 *
	 * @return the number of courts
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static CourtPersistence getPersistence() {
		return _persistence;
	}

	private static volatile CourtPersistence _persistence;

}