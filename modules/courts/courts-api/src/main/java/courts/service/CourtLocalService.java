/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.*;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import courts.exception.CourtValidateException;

import courts.model.Court;

import java.io.Serializable;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Court. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see CourtLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface CourtLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>courts.service.impl.CourtLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the court local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link CourtLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the court to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Court addCourt(Court court);

	public Court addEntry(Court orgEntry, ServiceContext serviceContext)
		throws CourtValidateException, PortalException;

	public int countAllInCategory(long categoryId);

	public int countAllInGroup(long groupId);

	/**
	 * Creates a new court with the primary key. Does not add the court to the database.
	 *
	 * @param courtId the primary key for the new court
	 * @return the new court
	 */
	@Transactional(enabled = false)
	public Court createCourt(long courtId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the court from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Court deleteCourt(Court court);

	/**
	 * Deletes the court with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtId the primary key of the court
	 * @return the court that was removed
	 * @throws PortalException if a court with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Court deleteCourt(long courtId) throws PortalException;

	public Court deleteEntry(long primaryKey) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Court fetchCourt(long courtId);

	/**
	 * Returns the court matching the UUID and group.
	 *
	 * @param uuid the court's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court, or <code>null</code> if a matching court could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Court fetchCourtByUuidAndGroupId(String uuid, long groupId);

	public List<Court> findAllInCategory(long categoryId);

	public List<Court> findAllInCategory(long categoryId, int start, int end);

	public List<Court> findAllInCategory(
		long categoryId, int start, int end, OrderByComparator<Court> obc);

	public List<Court> findAllInGroup(long groupId);

	public List<Court> findAllInGroup(long groupId, int start, int end);

	public List<Court> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Court> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the court with the primary key.
	 *
	 * @param courtId the primary key of the court
	 * @return the court
	 * @throws PortalException if a court with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Court getCourt(long courtId) throws PortalException;

	/**
	 * Returns the court matching the UUID and group.
	 *
	 * @param uuid the court's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court
	 * @throws PortalException if a matching court could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Court getCourtByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Court getCourtFromRequest(long primaryKey, PortletRequest request)
		throws CourtValidateException, PortletException;

	/**
	 * Returns a range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of courts
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Court> getCourts(int start, int end);

	/**
	 * Returns all the courts matching the UUID and company.
	 *
	 * @param uuid the UUID of the courts
	 * @param companyId the primary key of the company
	 * @return the matching courts, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Court> getCourtsByUuidAndCompanyId(String uuid, long companyId);

	/**
	 * Returns a range of courts matching the UUID and company.
	 *
	 * @param uuid the UUID of the courts
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching courts, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Court> getCourtsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Court> orderByComparator);

	/**
	 * Returns the number of courts.
	 *
	 * @return the number of courts
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getCourtsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Court getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Court> searchCourts(SearchContext searchContext);

	/**
	 * Updates the court in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param court the court
	 * @return the court that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Court updateCourt(Court court);

	public Court updateEntry(Court orgEntry, ServiceContext serviceContext)
		throws CourtValidateException, PortalException;

}