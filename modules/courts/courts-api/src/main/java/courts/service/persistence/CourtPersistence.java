/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import courts.exception.NoSuchCourtException;

import courts.model.Court;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the court service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtUtil
 * @generated
 */
@ProviderType
public interface CourtPersistence extends BasePersistence<Court> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourtUtil} to access the court persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the courts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching courts
	 */
	public java.util.List<Court> findByUuid(String uuid);

	/**
	 * Returns a range of all the courts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public java.util.List<Court> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the courts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the first court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the last court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the last court in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the courts before and after the current court in the ordered set where uuid = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public Court[] findByUuid_PrevAndNext(
			long courtId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Removes all the courts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of courts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching courts
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the court where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourtException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByUUID_G(String uuid, long groupId)
		throws NoSuchCourtException;

	/**
	 * Returns the court where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the court where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the court where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the court that was removed
	 */
	public Court removeByUUID_G(String uuid, long groupId)
		throws NoSuchCourtException;

	/**
	 * Returns the number of courts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching courts
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching courts
	 */
	public java.util.List<Court> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public java.util.List<Court> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the first court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the last court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the last court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the courts before and after the current court in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public Court[] findByUuid_C_PrevAndNext(
			long courtId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Removes all the courts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of courts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching courts
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the courts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching courts
	 */
	public java.util.List<Court> findByGroupId(long groupId);

	/**
	 * Returns a range of all the courts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public java.util.List<Court> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the courts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the first court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the last court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the last court in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the courts before and after the current court in the ordered set where groupId = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public Court[] findByGroupId_PrevAndNext(
			long courtId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Removes all the courts where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of courts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching courts
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the courts where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching courts
	 */
	public java.util.List<Court> findByCategoryId(long categoryId);

	/**
	 * Returns a range of all the courts where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of matching courts
	 */
	public java.util.List<Court> findByCategoryId(
		long categoryId, int start, int end);

	/**
	 * Returns an ordered range of all the courts where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courts where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courts
	 */
	public java.util.List<Court> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByCategoryId_First(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the first court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByCategoryId_First(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the last court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court
	 * @throws NoSuchCourtException if a matching court could not be found
	 */
	public Court findByCategoryId_Last(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Returns the last court in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court, or <code>null</code> if a matching court could not be found
	 */
	public Court fetchByCategoryId_Last(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns the courts before and after the current court in the ordered set where categoryId = &#63;.
	 *
	 * @param courtId the primary key of the current court
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public Court[] findByCategoryId_PrevAndNext(
			long courtId, long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Court>
				orderByComparator)
		throws NoSuchCourtException;

	/**
	 * Removes all the courts where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public void removeByCategoryId(long categoryId);

	/**
	 * Returns the number of courts where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching courts
	 */
	public int countByCategoryId(long categoryId);

	/**
	 * Caches the court in the entity cache if it is enabled.
	 *
	 * @param court the court
	 */
	public void cacheResult(Court court);

	/**
	 * Caches the courts in the entity cache if it is enabled.
	 *
	 * @param courts the courts
	 */
	public void cacheResult(java.util.List<Court> courts);

	/**
	 * Creates a new court with the primary key. Does not add the court to the database.
	 *
	 * @param courtId the primary key for the new court
	 * @return the new court
	 */
	public Court create(long courtId);

	/**
	 * Removes the court with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courtId the primary key of the court
	 * @return the court that was removed
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public Court remove(long courtId) throws NoSuchCourtException;

	public Court updateImpl(Court court);

	/**
	 * Returns the court with the primary key or throws a <code>NoSuchCourtException</code> if it could not be found.
	 *
	 * @param courtId the primary key of the court
	 * @return the court
	 * @throws NoSuchCourtException if a court with the primary key could not be found
	 */
	public Court findByPrimaryKey(long courtId) throws NoSuchCourtException;

	/**
	 * Returns the court with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courtId the primary key of the court
	 * @return the court, or <code>null</code> if a court with the primary key could not be found
	 */
	public Court fetchByPrimaryKey(long courtId);

	/**
	 * Returns all the courts.
	 *
	 * @return the courts
	 */
	public java.util.List<Court> findAll();

	/**
	 * Returns a range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @return the range of courts
	 */
	public java.util.List<Court> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of courts
	 */
	public java.util.List<Court> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courts
	 * @param end the upper bound of the range of courts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of courts
	 */
	public java.util.List<Court> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Court>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the courts from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of courts.
	 *
	 * @return the number of courts
	 */
	public int countAll();

}