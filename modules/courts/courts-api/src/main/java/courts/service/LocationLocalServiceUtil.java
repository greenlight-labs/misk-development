/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import courts.model.Location;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Location. This utility wraps
 * <code>courts.service.impl.LocationLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see LocationLocalService
 * @generated
 */
public class LocationLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>courts.service.impl.LocationLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Location addEntry(
			Location orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws courts.exception.LocationValidateException, PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the location to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect LocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param location the location
	 * @return the location that was added
	 */
	public static Location addLocation(Location location) {
		return getService().addLocation(location);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * Creates a new location with the primary key. Does not add the location to the database.
	 *
	 * @param locationId the primary key for the new location
	 * @return the new location
	 */
	public static Location createLocation(long locationId) {
		return getService().createLocation(locationId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static Location deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * Deletes the location from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect LocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param location the location
	 * @return the location that was removed
	 */
	public static Location deleteLocation(Location location) {
		return getService().deleteLocation(location);
	}

	/**
	 * Deletes the location with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect LocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param locationId the primary key of the location
	 * @return the location that was removed
	 * @throws PortalException if a location with the primary key could not be found
	 */
	public static Location deleteLocation(long locationId)
		throws PortalException {

		return getService().deleteLocation(locationId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.LocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.LocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Location fetchLocation(long locationId) {
		return getService().fetchLocation(locationId);
	}

	/**
	 * Returns the location matching the UUID and group.
	 *
	 * @param uuid the location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching location, or <code>null</code> if a matching location could not be found
	 */
	public static Location fetchLocationByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchLocationByUuidAndGroupId(uuid, groupId);
	}

	public static List<Location> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<Location> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<Location> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Location> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the location with the primary key.
	 *
	 * @param locationId the primary key of the location
	 * @return the location
	 * @throws PortalException if a location with the primary key could not be found
	 */
	public static Location getLocation(long locationId) throws PortalException {
		return getService().getLocation(locationId);
	}

	/**
	 * Returns the location matching the UUID and group.
	 *
	 * @param uuid the location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching location
	 * @throws PortalException if a matching location could not be found
	 */
	public static Location getLocationByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getLocationByUuidAndGroupId(uuid, groupId);
	}

	public static Location getLocationFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws courts.exception.LocationValidateException,
			   javax.portlet.PortletException {

		return getService().getLocationFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the locations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>courts.model.impl.LocationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of locations
	 * @param end the upper bound of the range of locations (not inclusive)
	 * @return the range of locations
	 */
	public static List<Location> getLocations(int start, int end) {
		return getService().getLocations(start, end);
	}

	/**
	 * Returns all the locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the locations
	 * @param companyId the primary key of the company
	 * @return the matching locations, or an empty list if no matches were found
	 */
	public static List<Location> getLocationsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getLocationsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the locations
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of locations
	 * @param end the upper bound of the range of locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching locations, or an empty list if no matches were found
	 */
	public static List<Location> getLocationsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Location> orderByComparator) {

		return getService().getLocationsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of locations.
	 *
	 * @return the number of locations
	 */
	public static int getLocationsCount() {
		return getService().getLocationsCount();
	}

	public static Location getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static Location updateEntry(
			Location orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws courts.exception.LocationValidateException, PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the location in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect LocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param location the location
	 * @return the location that was updated
	 */
	public static Location updateLocation(Location location) {
		return getService().updateLocation(location);
	}

	public static LocationLocalService getService() {
		return _service;
	}

	private static volatile LocationLocalService _service;

}