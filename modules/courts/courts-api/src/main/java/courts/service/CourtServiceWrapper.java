/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CourtService}.
 *
 * @author Brian Wing Shun Chan
 * @see CourtService
 * @generated
 */
public class CourtServiceWrapper
	implements CourtService, ServiceWrapper<CourtService> {

	public CourtServiceWrapper(CourtService courtService) {
		_courtService = courtService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _courtService.getOSGiServiceIdentifier();
	}

	@Override
	public CourtService getWrappedService() {
		return _courtService;
	}

	@Override
	public void setWrappedService(CourtService courtService) {
		_courtService = courtService;
	}

	private CourtService _courtService;

}