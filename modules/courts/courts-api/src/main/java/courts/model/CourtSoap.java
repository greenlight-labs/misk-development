/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link courts.service.http.CourtServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class CourtSoap implements Serializable {

	public static CourtSoap toSoapModel(Court model) {
		CourtSoap soapModel = new CourtSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setCourtId(model.getCourtId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setHeading(model.getHeading());
		soapModel.setName(model.getName());
		soapModel.setListingImage(model.getListingImage());
		soapModel.setDetailImage(model.getDetailImage());
		soapModel.setVenue(model.getVenue());
		soapModel.setOpeningHours(model.getOpeningHours());
		soapModel.setOpeningDays(model.getOpeningDays());
		soapModel.setDescription(model.getDescription());
		soapModel.setLat(model.getLat());
		soapModel.setLon(model.getLon());
		soapModel.setContactEmail(model.getContactEmail());
		soapModel.setContactPhone(model.getContactPhone());
		soapModel.setStatus(model.isStatus());

		return soapModel;
	}

	public static CourtSoap[] toSoapModels(Court[] models) {
		CourtSoap[] soapModels = new CourtSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CourtSoap[][] toSoapModels(Court[][] models) {
		CourtSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CourtSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CourtSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CourtSoap[] toSoapModels(List<Court> models) {
		List<CourtSoap> soapModels = new ArrayList<CourtSoap>(models.size());

		for (Court model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CourtSoap[soapModels.size()]);
	}

	public CourtSoap() {
	}

	public long getPrimaryKey() {
		return _courtId;
	}

	public void setPrimaryKey(long pk) {
		setCourtId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getCourtId() {
		return _courtId;
	}

	public void setCourtId(long courtId) {
		_courtId = courtId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public String getHeading() {
		return _heading;
	}

	public void setHeading(String heading) {
		_heading = heading;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getListingImage() {
		return _listingImage;
	}

	public void setListingImage(String listingImage) {
		_listingImage = listingImage;
	}

	public String getDetailImage() {
		return _detailImage;
	}

	public void setDetailImage(String detailImage) {
		_detailImage = detailImage;
	}

	public String getVenue() {
		return _venue;
	}

	public void setVenue(String venue) {
		_venue = venue;
	}

	public String getOpeningHours() {
		return _openingHours;
	}

	public void setOpeningHours(String openingHours) {
		_openingHours = openingHours;
	}

	public String getOpeningDays() {
		return _openingDays;
	}

	public void setOpeningDays(String openingDays) {
		_openingDays = openingDays;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getLat() {
		return _lat;
	}

	public void setLat(String lat) {
		_lat = lat;
	}

	public String getLon() {
		return _lon;
	}

	public void setLon(String lon) {
		_lon = lon;
	}

	public String getContactEmail() {
		return _contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		_contactEmail = contactEmail;
	}

	public String getContactPhone() {
		return _contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		_contactPhone = contactPhone;
	}

	public boolean getStatus() {
		return _status;
	}

	public boolean isStatus() {
		return _status;
	}

	public void setStatus(boolean status) {
		_status = status;
	}

	private String _uuid;
	private long _courtId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _categoryId;
	private String _heading;
	private String _name;
	private String _listingImage;
	private String _detailImage;
	private String _venue;
	private String _openingHours;
	private String _openingDays;
	private String _description;
	private String _lat;
	private String _lon;
	private String _contactEmail;
	private String _contactPhone;
	private boolean _status;

}