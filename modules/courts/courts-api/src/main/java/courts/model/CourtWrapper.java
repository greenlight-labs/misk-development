/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Court}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Court
 * @generated
 */
public class CourtWrapper
	extends BaseModelWrapper<Court> implements Court, ModelWrapper<Court> {

	public CourtWrapper(Court court) {
		super(court);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("courtId", getCourtId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("categoryId", getCategoryId());
		attributes.put("heading", getHeading());
		attributes.put("name", getName());
		attributes.put("listingImage", getListingImage());
		attributes.put("detailImage", getDetailImage());
		attributes.put("venue", getVenue());
		attributes.put("openingHours", getOpeningHours());
		attributes.put("openingDays", getOpeningDays());
		attributes.put("description", getDescription());
		attributes.put("lat", getLat());
		attributes.put("lon", getLon());
		attributes.put("contactEmail", getContactEmail());
		attributes.put("contactPhone", getContactPhone());
		attributes.put("status", isStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long courtId = (Long)attributes.get("courtId");

		if (courtId != null) {
			setCourtId(courtId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		String heading = (String)attributes.get("heading");

		if (heading != null) {
			setHeading(heading);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String listingImage = (String)attributes.get("listingImage");

		if (listingImage != null) {
			setListingImage(listingImage);
		}

		String detailImage = (String)attributes.get("detailImage");

		if (detailImage != null) {
			setDetailImage(detailImage);
		}

		String venue = (String)attributes.get("venue");

		if (venue != null) {
			setVenue(venue);
		}

		String openingHours = (String)attributes.get("openingHours");

		if (openingHours != null) {
			setOpeningHours(openingHours);
		}

		String openingDays = (String)attributes.get("openingDays");

		if (openingDays != null) {
			setOpeningDays(openingDays);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String lat = (String)attributes.get("lat");

		if (lat != null) {
			setLat(lat);
		}

		String lon = (String)attributes.get("lon");

		if (lon != null) {
			setLon(lon);
		}

		String contactEmail = (String)attributes.get("contactEmail");

		if (contactEmail != null) {
			setContactEmail(contactEmail);
		}

		String contactPhone = (String)attributes.get("contactPhone");

		if (contactPhone != null) {
			setContactPhone(contactPhone);
		}

		Boolean status = (Boolean)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the category ID of this court.
	 *
	 * @return the category ID of this court
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this court.
	 *
	 * @return the company ID of this court
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the contact email of this court.
	 *
	 * @return the contact email of this court
	 */
	@Override
	public String getContactEmail() {
		return model.getContactEmail();
	}

	/**
	 * Returns the contact phone of this court.
	 *
	 * @return the contact phone of this court
	 */
	@Override
	public String getContactPhone() {
		return model.getContactPhone();
	}

	/**
	 * Returns the court ID of this court.
	 *
	 * @return the court ID of this court
	 */
	@Override
	public long getCourtId() {
		return model.getCourtId();
	}

	/**
	 * Returns the create date of this court.
	 *
	 * @return the create date of this court
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this court.
	 *
	 * @return the description of this court
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this court
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this court. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this court
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this court
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this court.
	 *
	 * @return the locales and localized descriptions of this court
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the detail image of this court.
	 *
	 * @return the detail image of this court
	 */
	@Override
	public String getDetailImage() {
		return model.getDetailImage();
	}

	/**
	 * Returns the group ID of this court.
	 *
	 * @return the group ID of this court
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the heading of this court.
	 *
	 * @return the heading of this court
	 */
	@Override
	public String getHeading() {
		return model.getHeading();
	}

	/**
	 * Returns the localized heading of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized heading of this court
	 */
	@Override
	public String getHeading(java.util.Locale locale) {
		return model.getHeading(locale);
	}

	/**
	 * Returns the localized heading of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized heading of this court. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getHeading(java.util.Locale locale, boolean useDefault) {
		return model.getHeading(locale, useDefault);
	}

	/**
	 * Returns the localized heading of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized heading of this court
	 */
	@Override
	public String getHeading(String languageId) {
		return model.getHeading(languageId);
	}

	/**
	 * Returns the localized heading of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized heading of this court
	 */
	@Override
	public String getHeading(String languageId, boolean useDefault) {
		return model.getHeading(languageId, useDefault);
	}

	@Override
	public String getHeadingCurrentLanguageId() {
		return model.getHeadingCurrentLanguageId();
	}

	@Override
	public String getHeadingCurrentValue() {
		return model.getHeadingCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized headings of this court.
	 *
	 * @return the locales and localized headings of this court
	 */
	@Override
	public Map<java.util.Locale, String> getHeadingMap() {
		return model.getHeadingMap();
	}

	/**
	 * Returns the lat of this court.
	 *
	 * @return the lat of this court
	 */
	@Override
	public String getLat() {
		return model.getLat();
	}

	/**
	 * Returns the listing image of this court.
	 *
	 * @return the listing image of this court
	 */
	@Override
	public String getListingImage() {
		return model.getListingImage();
	}

	/**
	 * Returns the lon of this court.
	 *
	 * @return the lon of this court
	 */
	@Override
	public String getLon() {
		return model.getLon();
	}

	/**
	 * Returns the modified date of this court.
	 *
	 * @return the modified date of this court
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this court.
	 *
	 * @return the name of this court
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the localized name of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized name of this court
	 */
	@Override
	public String getName(java.util.Locale locale) {
		return model.getName(locale);
	}

	/**
	 * Returns the localized name of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this court. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getName(java.util.Locale locale, boolean useDefault) {
		return model.getName(locale, useDefault);
	}

	/**
	 * Returns the localized name of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized name of this court
	 */
	@Override
	public String getName(String languageId) {
		return model.getName(languageId);
	}

	/**
	 * Returns the localized name of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this court
	 */
	@Override
	public String getName(String languageId, boolean useDefault) {
		return model.getName(languageId, useDefault);
	}

	@Override
	public String getNameCurrentLanguageId() {
		return model.getNameCurrentLanguageId();
	}

	@Override
	public String getNameCurrentValue() {
		return model.getNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized names of this court.
	 *
	 * @return the locales and localized names of this court
	 */
	@Override
	public Map<java.util.Locale, String> getNameMap() {
		return model.getNameMap();
	}

	/**
	 * Returns the opening days of this court.
	 *
	 * @return the opening days of this court
	 */
	@Override
	public String getOpeningDays() {
		return model.getOpeningDays();
	}

	/**
	 * Returns the localized opening days of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized opening days of this court
	 */
	@Override
	public String getOpeningDays(java.util.Locale locale) {
		return model.getOpeningDays(locale);
	}

	/**
	 * Returns the localized opening days of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized opening days of this court. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getOpeningDays(java.util.Locale locale, boolean useDefault) {
		return model.getOpeningDays(locale, useDefault);
	}

	/**
	 * Returns the localized opening days of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized opening days of this court
	 */
	@Override
	public String getOpeningDays(String languageId) {
		return model.getOpeningDays(languageId);
	}

	/**
	 * Returns the localized opening days of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized opening days of this court
	 */
	@Override
	public String getOpeningDays(String languageId, boolean useDefault) {
		return model.getOpeningDays(languageId, useDefault);
	}

	@Override
	public String getOpeningDaysCurrentLanguageId() {
		return model.getOpeningDaysCurrentLanguageId();
	}

	@Override
	public String getOpeningDaysCurrentValue() {
		return model.getOpeningDaysCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized opening dayses of this court.
	 *
	 * @return the locales and localized opening dayses of this court
	 */
	@Override
	public Map<java.util.Locale, String> getOpeningDaysMap() {
		return model.getOpeningDaysMap();
	}

	/**
	 * Returns the opening hours of this court.
	 *
	 * @return the opening hours of this court
	 */
	@Override
	public String getOpeningHours() {
		return model.getOpeningHours();
	}

	/**
	 * Returns the localized opening hours of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized opening hours of this court
	 */
	@Override
	public String getOpeningHours(java.util.Locale locale) {
		return model.getOpeningHours(locale);
	}

	/**
	 * Returns the localized opening hours of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized opening hours of this court. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getOpeningHours(java.util.Locale locale, boolean useDefault) {
		return model.getOpeningHours(locale, useDefault);
	}

	/**
	 * Returns the localized opening hours of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized opening hours of this court
	 */
	@Override
	public String getOpeningHours(String languageId) {
		return model.getOpeningHours(languageId);
	}

	/**
	 * Returns the localized opening hours of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized opening hours of this court
	 */
	@Override
	public String getOpeningHours(String languageId, boolean useDefault) {
		return model.getOpeningHours(languageId, useDefault);
	}

	@Override
	public String getOpeningHoursCurrentLanguageId() {
		return model.getOpeningHoursCurrentLanguageId();
	}

	@Override
	public String getOpeningHoursCurrentValue() {
		return model.getOpeningHoursCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized opening hourses of this court.
	 *
	 * @return the locales and localized opening hourses of this court
	 */
	@Override
	public Map<java.util.Locale, String> getOpeningHoursMap() {
		return model.getOpeningHoursMap();
	}

	/**
	 * Returns the primary key of this court.
	 *
	 * @return the primary key of this court
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the status of this court.
	 *
	 * @return the status of this court
	 */
	@Override
	public boolean getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the user ID of this court.
	 *
	 * @return the user ID of this court
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this court.
	 *
	 * @return the user name of this court
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this court.
	 *
	 * @return the user uuid of this court
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this court.
	 *
	 * @return the uuid of this court
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the venue of this court.
	 *
	 * @return the venue of this court
	 */
	@Override
	public String getVenue() {
		return model.getVenue();
	}

	/**
	 * Returns the localized venue of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized venue of this court
	 */
	@Override
	public String getVenue(java.util.Locale locale) {
		return model.getVenue(locale);
	}

	/**
	 * Returns the localized venue of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized venue of this court. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getVenue(java.util.Locale locale, boolean useDefault) {
		return model.getVenue(locale, useDefault);
	}

	/**
	 * Returns the localized venue of this court in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized venue of this court
	 */
	@Override
	public String getVenue(String languageId) {
		return model.getVenue(languageId);
	}

	/**
	 * Returns the localized venue of this court in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized venue of this court
	 */
	@Override
	public String getVenue(String languageId, boolean useDefault) {
		return model.getVenue(languageId, useDefault);
	}

	@Override
	public String getVenueCurrentLanguageId() {
		return model.getVenueCurrentLanguageId();
	}

	@Override
	public String getVenueCurrentValue() {
		return model.getVenueCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized venues of this court.
	 *
	 * @return the locales and localized venues of this court
	 */
	@Override
	public Map<java.util.Locale, String> getVenueMap() {
		return model.getVenueMap();
	}

	/**
	 * Returns <code>true</code> if this court is status.
	 *
	 * @return <code>true</code> if this court is status; <code>false</code> otherwise
	 */
	@Override
	public boolean isStatus() {
		return model.isStatus();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the category ID of this court.
	 *
	 * @param categoryId the category ID of this court
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this court.
	 *
	 * @param companyId the company ID of this court
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the contact email of this court.
	 *
	 * @param contactEmail the contact email of this court
	 */
	@Override
	public void setContactEmail(String contactEmail) {
		model.setContactEmail(contactEmail);
	}

	/**
	 * Sets the contact phone of this court.
	 *
	 * @param contactPhone the contact phone of this court
	 */
	@Override
	public void setContactPhone(String contactPhone) {
		model.setContactPhone(contactPhone);
	}

	/**
	 * Sets the court ID of this court.
	 *
	 * @param courtId the court ID of this court
	 */
	@Override
	public void setCourtId(long courtId) {
		model.setCourtId(courtId);
	}

	/**
	 * Sets the create date of this court.
	 *
	 * @param createDate the create date of this court
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this court.
	 *
	 * @param description the description of this court
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this court in the language.
	 *
	 * @param description the localized description of this court
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this court in the language, and sets the default locale.
	 *
	 * @param description the localized description of this court
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this court from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this court
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this court from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this court
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the detail image of this court.
	 *
	 * @param detailImage the detail image of this court
	 */
	@Override
	public void setDetailImage(String detailImage) {
		model.setDetailImage(detailImage);
	}

	/**
	 * Sets the group ID of this court.
	 *
	 * @param groupId the group ID of this court
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the heading of this court.
	 *
	 * @param heading the heading of this court
	 */
	@Override
	public void setHeading(String heading) {
		model.setHeading(heading);
	}

	/**
	 * Sets the localized heading of this court in the language.
	 *
	 * @param heading the localized heading of this court
	 * @param locale the locale of the language
	 */
	@Override
	public void setHeading(String heading, java.util.Locale locale) {
		model.setHeading(heading, locale);
	}

	/**
	 * Sets the localized heading of this court in the language, and sets the default locale.
	 *
	 * @param heading the localized heading of this court
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setHeading(
		String heading, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setHeading(heading, locale, defaultLocale);
	}

	@Override
	public void setHeadingCurrentLanguageId(String languageId) {
		model.setHeadingCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized headings of this court from the map of locales and localized headings.
	 *
	 * @param headingMap the locales and localized headings of this court
	 */
	@Override
	public void setHeadingMap(Map<java.util.Locale, String> headingMap) {
		model.setHeadingMap(headingMap);
	}

	/**
	 * Sets the localized headings of this court from the map of locales and localized headings, and sets the default locale.
	 *
	 * @param headingMap the locales and localized headings of this court
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setHeadingMap(
		Map<java.util.Locale, String> headingMap,
		java.util.Locale defaultLocale) {

		model.setHeadingMap(headingMap, defaultLocale);
	}

	/**
	 * Sets the lat of this court.
	 *
	 * @param lat the lat of this court
	 */
	@Override
	public void setLat(String lat) {
		model.setLat(lat);
	}

	/**
	 * Sets the listing image of this court.
	 *
	 * @param listingImage the listing image of this court
	 */
	@Override
	public void setListingImage(String listingImage) {
		model.setListingImage(listingImage);
	}

	/**
	 * Sets the lon of this court.
	 *
	 * @param lon the lon of this court
	 */
	@Override
	public void setLon(String lon) {
		model.setLon(lon);
	}

	/**
	 * Sets the modified date of this court.
	 *
	 * @param modifiedDate the modified date of this court
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this court.
	 *
	 * @param name the name of this court
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the localized name of this court in the language.
	 *
	 * @param name the localized name of this court
	 * @param locale the locale of the language
	 */
	@Override
	public void setName(String name, java.util.Locale locale) {
		model.setName(name, locale);
	}

	/**
	 * Sets the localized name of this court in the language, and sets the default locale.
	 *
	 * @param name the localized name of this court
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setName(
		String name, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setName(name, locale, defaultLocale);
	}

	@Override
	public void setNameCurrentLanguageId(String languageId) {
		model.setNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized names of this court from the map of locales and localized names.
	 *
	 * @param nameMap the locales and localized names of this court
	 */
	@Override
	public void setNameMap(Map<java.util.Locale, String> nameMap) {
		model.setNameMap(nameMap);
	}

	/**
	 * Sets the localized names of this court from the map of locales and localized names, and sets the default locale.
	 *
	 * @param nameMap the locales and localized names of this court
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setNameMap(
		Map<java.util.Locale, String> nameMap, java.util.Locale defaultLocale) {

		model.setNameMap(nameMap, defaultLocale);
	}

	/**
	 * Sets the opening days of this court.
	 *
	 * @param openingDays the opening days of this court
	 */
	@Override
	public void setOpeningDays(String openingDays) {
		model.setOpeningDays(openingDays);
	}

	/**
	 * Sets the localized opening days of this court in the language.
	 *
	 * @param openingDays the localized opening days of this court
	 * @param locale the locale of the language
	 */
	@Override
	public void setOpeningDays(String openingDays, java.util.Locale locale) {
		model.setOpeningDays(openingDays, locale);
	}

	/**
	 * Sets the localized opening days of this court in the language, and sets the default locale.
	 *
	 * @param openingDays the localized opening days of this court
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOpeningDays(
		String openingDays, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setOpeningDays(openingDays, locale, defaultLocale);
	}

	@Override
	public void setOpeningDaysCurrentLanguageId(String languageId) {
		model.setOpeningDaysCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized opening dayses of this court from the map of locales and localized opening dayses.
	 *
	 * @param openingDaysMap the locales and localized opening dayses of this court
	 */
	@Override
	public void setOpeningDaysMap(
		Map<java.util.Locale, String> openingDaysMap) {

		model.setOpeningDaysMap(openingDaysMap);
	}

	/**
	 * Sets the localized opening dayses of this court from the map of locales and localized opening dayses, and sets the default locale.
	 *
	 * @param openingDaysMap the locales and localized opening dayses of this court
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOpeningDaysMap(
		Map<java.util.Locale, String> openingDaysMap,
		java.util.Locale defaultLocale) {

		model.setOpeningDaysMap(openingDaysMap, defaultLocale);
	}

	/**
	 * Sets the opening hours of this court.
	 *
	 * @param openingHours the opening hours of this court
	 */
	@Override
	public void setOpeningHours(String openingHours) {
		model.setOpeningHours(openingHours);
	}

	/**
	 * Sets the localized opening hours of this court in the language.
	 *
	 * @param openingHours the localized opening hours of this court
	 * @param locale the locale of the language
	 */
	@Override
	public void setOpeningHours(String openingHours, java.util.Locale locale) {
		model.setOpeningHours(openingHours, locale);
	}

	/**
	 * Sets the localized opening hours of this court in the language, and sets the default locale.
	 *
	 * @param openingHours the localized opening hours of this court
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOpeningHours(
		String openingHours, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setOpeningHours(openingHours, locale, defaultLocale);
	}

	@Override
	public void setOpeningHoursCurrentLanguageId(String languageId) {
		model.setOpeningHoursCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized opening hourses of this court from the map of locales and localized opening hourses.
	 *
	 * @param openingHoursMap the locales and localized opening hourses of this court
	 */
	@Override
	public void setOpeningHoursMap(
		Map<java.util.Locale, String> openingHoursMap) {

		model.setOpeningHoursMap(openingHoursMap);
	}

	/**
	 * Sets the localized opening hourses of this court from the map of locales and localized opening hourses, and sets the default locale.
	 *
	 * @param openingHoursMap the locales and localized opening hourses of this court
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOpeningHoursMap(
		Map<java.util.Locale, String> openingHoursMap,
		java.util.Locale defaultLocale) {

		model.setOpeningHoursMap(openingHoursMap, defaultLocale);
	}

	/**
	 * Sets the primary key of this court.
	 *
	 * @param primaryKey the primary key of this court
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets whether this court is status.
	 *
	 * @param status the status of this court
	 */
	@Override
	public void setStatus(boolean status) {
		model.setStatus(status);
	}

	/**
	 * Sets the user ID of this court.
	 *
	 * @param userId the user ID of this court
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this court.
	 *
	 * @param userName the user name of this court
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this court.
	 *
	 * @param userUuid the user uuid of this court
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this court.
	 *
	 * @param uuid the uuid of this court
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the venue of this court.
	 *
	 * @param venue the venue of this court
	 */
	@Override
	public void setVenue(String venue) {
		model.setVenue(venue);
	}

	/**
	 * Sets the localized venue of this court in the language.
	 *
	 * @param venue the localized venue of this court
	 * @param locale the locale of the language
	 */
	@Override
	public void setVenue(String venue, java.util.Locale locale) {
		model.setVenue(venue, locale);
	}

	/**
	 * Sets the localized venue of this court in the language, and sets the default locale.
	 *
	 * @param venue the localized venue of this court
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVenue(
		String venue, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setVenue(venue, locale, defaultLocale);
	}

	@Override
	public void setVenueCurrentLanguageId(String languageId) {
		model.setVenueCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized venues of this court from the map of locales and localized venues.
	 *
	 * @param venueMap the locales and localized venues of this court
	 */
	@Override
	public void setVenueMap(Map<java.util.Locale, String> venueMap) {
		model.setVenueMap(venueMap);
	}

	/**
	 * Sets the localized venues of this court from the map of locales and localized venues, and sets the default locale.
	 *
	 * @param venueMap the locales and localized venues of this court
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVenueMap(
		Map<java.util.Locale, String> venueMap,
		java.util.Locale defaultLocale) {

		model.setVenueMap(venueMap, defaultLocale);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected CourtWrapper wrap(Court court) {
		return new CourtWrapper(court);
	}

}