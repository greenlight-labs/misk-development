/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import courts.model.Court;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Court in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CourtCacheModel implements CacheModel<Court>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CourtCacheModel)) {
			return false;
		}

		CourtCacheModel courtCacheModel = (CourtCacheModel)object;

		if (courtId == courtCacheModel.courtId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, courtId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(45);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", courtId=");
		sb.append(courtId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", heading=");
		sb.append(heading);
		sb.append(", name=");
		sb.append(name);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", detailImage=");
		sb.append(detailImage);
		sb.append(", venue=");
		sb.append(venue);
		sb.append(", openingHours=");
		sb.append(openingHours);
		sb.append(", openingDays=");
		sb.append(openingDays);
		sb.append(", description=");
		sb.append(description);
		sb.append(", lat=");
		sb.append(lat);
		sb.append(", lon=");
		sb.append(lon);
		sb.append(", contactEmail=");
		sb.append(contactEmail);
		sb.append(", contactPhone=");
		sb.append(contactPhone);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Court toEntityModel() {
		CourtImpl courtImpl = new CourtImpl();

		if (uuid == null) {
			courtImpl.setUuid("");
		}
		else {
			courtImpl.setUuid(uuid);
		}

		courtImpl.setCourtId(courtId);
		courtImpl.setGroupId(groupId);
		courtImpl.setCompanyId(companyId);
		courtImpl.setUserId(userId);

		if (userName == null) {
			courtImpl.setUserName("");
		}
		else {
			courtImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			courtImpl.setCreateDate(null);
		}
		else {
			courtImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			courtImpl.setModifiedDate(null);
		}
		else {
			courtImpl.setModifiedDate(new Date(modifiedDate));
		}

		courtImpl.setCategoryId(categoryId);

		if (heading == null) {
			courtImpl.setHeading("");
		}
		else {
			courtImpl.setHeading(heading);
		}

		if (name == null) {
			courtImpl.setName("");
		}
		else {
			courtImpl.setName(name);
		}

		if (listingImage == null) {
			courtImpl.setListingImage("");
		}
		else {
			courtImpl.setListingImage(listingImage);
		}

		if (detailImage == null) {
			courtImpl.setDetailImage("");
		}
		else {
			courtImpl.setDetailImage(detailImage);
		}

		if (venue == null) {
			courtImpl.setVenue("");
		}
		else {
			courtImpl.setVenue(venue);
		}

		if (openingHours == null) {
			courtImpl.setOpeningHours("");
		}
		else {
			courtImpl.setOpeningHours(openingHours);
		}

		if (openingDays == null) {
			courtImpl.setOpeningDays("");
		}
		else {
			courtImpl.setOpeningDays(openingDays);
		}

		if (description == null) {
			courtImpl.setDescription("");
		}
		else {
			courtImpl.setDescription(description);
		}

		if (lat == null) {
			courtImpl.setLat("");
		}
		else {
			courtImpl.setLat(lat);
		}

		if (lon == null) {
			courtImpl.setLon("");
		}
		else {
			courtImpl.setLon(lon);
		}

		if (contactEmail == null) {
			courtImpl.setContactEmail("");
		}
		else {
			courtImpl.setContactEmail(contactEmail);
		}

		if (contactPhone == null) {
			courtImpl.setContactPhone("");
		}
		else {
			courtImpl.setContactPhone(contactPhone);
		}

		courtImpl.setStatus(status);

		courtImpl.resetOriginalValues();

		return courtImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		courtId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		categoryId = objectInput.readLong();
		heading = objectInput.readUTF();
		name = objectInput.readUTF();
		listingImage = objectInput.readUTF();
		detailImage = objectInput.readUTF();
		venue = objectInput.readUTF();
		openingHours = objectInput.readUTF();
		openingDays = objectInput.readUTF();
		description = (String)objectInput.readObject();
		lat = objectInput.readUTF();
		lon = objectInput.readUTF();
		contactEmail = objectInput.readUTF();
		contactPhone = objectInput.readUTF();

		status = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(courtId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(categoryId);

		if (heading == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(heading);
		}

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (detailImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(detailImage);
		}

		if (venue == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(venue);
		}

		if (openingHours == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(openingHours);
		}

		if (openingDays == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(openingDays);
		}

		if (description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(description);
		}

		if (lat == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lat);
		}

		if (lon == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lon);
		}

		if (contactEmail == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contactEmail);
		}

		if (contactPhone == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(contactPhone);
		}

		objectOutput.writeBoolean(status);
	}

	public String uuid;
	public long courtId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long categoryId;
	public String heading;
	public String name;
	public String listingImage;
	public String detailImage;
	public String venue;
	public String openingHours;
	public String openingDays;
	public String description;
	public String lat;
	public String lon;
	public String contactEmail;
	public String contactPhone;
	public boolean status;

}