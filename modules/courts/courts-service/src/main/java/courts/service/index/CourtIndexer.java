package courts.service.index;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.IndexWriterHelper;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import com.liferay.portal.kernel.util.GetterUtil;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import courts.model.Court;
import courts.service.CourtLocalService;

@Component(immediate = true, service = Indexer.class)
public class CourtIndexer extends BaseIndexer<Court> {
	public static final String CLASS_NAME = CourtIndexer.class.getName();
	public static final String DISCRIPTION = "description";
	private static final String NAME = "name";
	private static final String HEADING = "heading";
	private static final String DESCRIPTION = "description";

	public CourtIndexer() {
		setDefaultSelectedFieldNames(Field.ASSET_TAG_NAMES, Field.COMPANY_ID, Field.CONTENT, Field.ENTRY_CLASS_NAME,
				Field.ENTRY_CLASS_PK, Field.GROUP_ID, Field.MODIFIED_DATE, Field.SCOPE_GROUP_ID, Field.UID, DISCRIPTION,
				NAME, HEADING);
		setPermissionAware(false);
		setFilterSearch(true);
	}

	@Override
	public String getClassName() {
		return Court.class.getName();
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletRequest portletRequest,
			PortletResponse portletResponse) throws Exception {
		Summary summary = createSummary(document);
		summary.setMaxContentLength(200);
		return summary;
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		Court event = courtLocalService.getCourt(classPK);
		doReindex(event);

	}

	@Override
	protected void doReindex(String[] ids) throws Exception {
		long companyId = GetterUtil.getLong(ids[0]);
		reindexEmployees(companyId);

	}

	@Override
	protected void doReindex(Court object) throws Exception {
		Document document = getDocument(object);
		indexWriterHelper.updateDocument(getSearchEngineId(), object.getCompanyId(), document, isCommitImmediately());

	}

	@Override
	public void postProcessSearchQuery(BooleanQuery searchQuery, BooleanFilter fullQueryBooleanFilter,
			SearchContext searchContext) throws Exception {
		addSearchLocalizedTerm(searchQuery, searchContext, NAME, false);
		addSearchLocalizedTerm(searchQuery, searchContext, DESCRIPTION, false);
		addSearchLocalizedTerm(searchQuery, searchContext, HEADING, false);

	}

	@Override
	protected void doDelete(Court object) throws Exception {
		deleteDocument(object.getCompanyId(), object.getCourtId());

	}

	@Override
	protected Document doGetDocument(Court object) throws Exception {

		Document document = getBaseModelDocument(Court.class.getName(), object);

		document.addLocalizedKeyword(NAME, object.getNameMap());
		document.addLocalizedKeyword(HEADING, object.getHeadingMap());
		document.addLocalizedKeyword(DESCRIPTION, object.getDescriptionMap());

		return document;
	}

	protected void reindexEmployees(long companyId) throws PortalException {
		final IndexableActionableDynamicQuery indexableActionableDynamicQuery = courtLocalService
				.getIndexableActionableDynamicQuery();
		indexableActionableDynamicQuery.setCompanyId(companyId);
		indexableActionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod() {
			@Override
			public void performAction(Object event) {
				try {
					Document document = getDocument((Court) event);
					indexableActionableDynamicQuery.addDocuments(document);
				} catch (PortalException pe) {
					pe.printStackTrace();
				}
			}
		});
		indexableActionableDynamicQuery.setSearchEngineId(getSearchEngineId());
		indexableActionableDynamicQuery.performActions();
	}

	private static final Log log = LogFactoryUtil.getLog(CourtIndexer.class);

	@Reference
	CourtLocalService courtLocalService;

	@Reference
	protected IndexWriterHelper indexWriterHelper;
}