/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import courts.exception.LocationValidateException;
import courts.model.Location;
import courts.service.base.LocationLocalServiceBaseImpl;

import courts.service.util.LocationValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the location local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>courts.service.LocationLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LocationLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=courts.model.Location",
	service = AopService.class
)
public class LocationLocalServiceImpl extends LocationLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>courts.service.LocationLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>courts.service.LocationLocalServiceUtil</code>.
	 */

	public Location addEntry(Location orgEntry, ServiceContext serviceContext)
			throws PortalException, LocationValidateException {

		// Validation

		ModelValidator<Location> modelValidator = new LocationValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Location entry = _addEntry(orgEntry, serviceContext);

		Location addedEntry = locationPersistence.update(entry);

		locationPersistence.clearCache();

		return addedEntry;
	}

	public Location updateEntry(
			Location orgEntry, ServiceContext serviceContext)
			throws PortalException, LocationValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Location> modelValidator = new LocationValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Location entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Location updatedEntry = locationPersistence.update(entry);
		locationPersistence.clearCache();

		return updatedEntry;
	}

	protected Location _addEntry(Location entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(Location.class.getName());

		Location newEntry = locationPersistence.create(id);

		User user = userLocalService.getUser(serviceContext.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(serviceContext.getCompanyId());
		newEntry.setGroupId(serviceContext.getScopeGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());

		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setName(entry.getName());
		newEntry.setStatus(entry.getStatus());

		//return locationPersistence.update(newEntry);
		return newEntry;
	}

	protected Location _updateEntry(
			long primaryKey, Location entry, ServiceContext serviceContext)
			throws PortalException {

		Location updateEntry = fetchLocation(primaryKey);

		User user = userLocalService.getUser(serviceContext.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(serviceContext.getCompanyId());
		updateEntry.setGroupId(serviceContext.getScopeGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setName(entry.getName());
		updateEntry.setStatus(entry.getStatus());

		return updateEntry;
	}

	public Location deleteEntry(long primaryKey) throws PortalException {
		Location entry = getLocation(primaryKey);
		locationPersistence.remove(entry);

		return entry;
	}

	public List<Location> findAllInGroup(long groupId) {

		return locationPersistence.findByGroupId(groupId);
	}

	public List<Location> findAllInGroup(long groupId, int start, int end,
										 OrderByComparator<Location> obc) {

		return locationPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Location> findAllInGroup(long groupId, int start, int end) {

		return locationPersistence.findByGroupId(groupId, start, end);
	}


	public int countAllInGroup(long groupId) {

		return locationPersistence.countByGroupId(groupId);
	}

	public Location getLocationFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, LocationValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Location entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchLocation(primaryKey);
		}

		try {
			entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
			entry.setStatus(ParamUtil.getBoolean(request, "status"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new LocationValidateException(error);
		}

		return entry;
	}

	public Location getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(Location.class.getName());

		return createLocation(primaryKey);
	}

	private static Log _log = LogFactoryUtil.getLog(
			LocationLocalServiceImpl.class);
}