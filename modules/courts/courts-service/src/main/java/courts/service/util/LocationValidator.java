package courts.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import courts.model.Location;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Location Validator
 * 
 * @author tz
 *
 */
public class LocationValidator implements ModelValidator<Location> {

	@Override
	public void validate(Location entry) throws PortalException {

        // Fields: locationId, name, icon, image, status
        validateLocationId(entry.getLocationId());
        validateName(entry.getName());
        validateStatus(entry.getStatus());

	}

    protected void validateLocationId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-location-required");
        }
    }

    protected void validateName(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-name-required");
        }
    }

    protected void validateStatus(boolean field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-status-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
