package courts.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import courts.model.Court;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Court Validator 
 * 
 * @author tz
 *
 */
public class CourtValidator implements ModelValidator<Court> {

	@Override
	public void validate(Court entry) throws PortalException {

        // Fields: courtId, categoryId, name, listingImage, detailImage, venue, openingHours, openingDays, description, lat, lon, contactEmail, contactPhone
        validateCourtId(entry.getCourtId());
        validateCategoryId(entry.getCategoryId());
        validateName(entry.getName());
        validateListingImage(entry.getListingImage());
        validateDetailImage(entry.getDetailImage());
        validateVenue(entry.getVenue());
        validateOpeningHours(entry.getOpeningHours());
        validateOpeningDays(entry.getOpeningDays());
        validateDescription(entry.getDescription());
        validateLat(entry.getLat());
        validateLon(entry.getLon());
        validateContactEmail(entry.getContactEmail());
        validateContactPhone(entry.getContactPhone());

	}

    protected void validateCourtId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-id-required");
        }
    }

    protected void validateCategoryId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-category-required");
        }
    }

    protected void validateName(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-name-required");
        }
    }

    protected void validateListingImage(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-listing-image-required");
        }
    }

    protected void validateDetailImage(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-detail-image-required");
        }
    }

    protected void validateVenue(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-venue-required");
        }
    }

    protected void validateOpeningHours(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-opening-hours-required");
        }
    }

    protected void validateOpeningDays(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-opening-days-required");
        }
    }

    protected void validateDescription(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-description-required");
        }
    }

    protected void validateLat(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-lat-required");
        }
    }

    protected void validateLon(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-lon-required");
        }
    }

    protected void validateContactEmail(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-contact-email-required");
        }
    }

    protected void validateContactPhone(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-contact-phone-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
