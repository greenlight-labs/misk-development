package courts.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import courts.model.Category;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Category Validator
 * 
 * @author tz
 *
 */
public class CategoryValidator implements ModelValidator<Category> {

	@Override
	public void validate(Category entry) throws PortalException {

        // Fields: categoryId, locationId, name, icon, image, status
        validateCategoryId(entry.getCategoryId());
        validateLocationId(entry.getLocationId());
        validateName(entry.getName());
        validateIcon(entry.getIcon());
        validateImage(entry.getImage());
        validateStatus(entry.getStatus());

	}

    protected void validateCategoryId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-category-required");
        }
    }

    protected void validateLocationId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-location-required");
        }
    }

    protected void validateName(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-name-required");
        }
    }

    protected void validateIcon(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-icon-required");
        }
    }

    protected void validateImage(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("court-image-required");
        }
    }

    protected void validateStatus(boolean field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-status-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
