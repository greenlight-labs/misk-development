/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.search.*;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import courts.exception.CourtValidateException;
import courts.model.Court;
import courts.service.CourtLocalServiceUtil;
import courts.service.base.CourtLocalServiceBaseImpl;
import courts.service.index.CourtIndexer;
import courts.service.util.CourtValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the court local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>courts.service.CourtLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtLocalServiceBaseImpl
 */
@Component(property = "model.class.name=courts.model.Court", service = AopService.class)
public class CourtLocalServiceImpl extends CourtLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>courts.service.CourtLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>courts.service.CourtLocalServiceUtil</code>.
	 */

	public Court addEntry(Court orgEntry, ServiceContext serviceContext)
			throws PortalException, CourtValidateException {

		// Validation

		ModelValidator<Court> modelValidator = new CourtValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Court entry = _addEntry(orgEntry, serviceContext);

		Court addedEntry = courtPersistence.update(entry);

		courtPersistence.clearCache();

		return addedEntry;
	}

	public Court updateEntry(Court orgEntry, ServiceContext serviceContext)
			throws PortalException, CourtValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Court> modelValidator = new CourtValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Court entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Court updatedEntry = courtPersistence.update(entry);
		courtPersistence.clearCache();

		return updatedEntry;
	}

	protected Court _addEntry(Court entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Court.class.getName());

		Court newEntry = courtPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setCategoryId(entry.getCategoryId());
		newEntry.setHeading(entry.getHeading());
		newEntry.setName(entry.getName());
		newEntry.setListingImage(entry.getListingImage());
		newEntry.setDetailImage(entry.getDetailImage());
		newEntry.setVenue(entry.getVenue());
		newEntry.setOpeningHours(entry.getOpeningHours());
		newEntry.setOpeningDays(entry.getOpeningDays());
		newEntry.setDescription(entry.getDescription());
		newEntry.setLat(entry.getLat());
		newEntry.setLon(entry.getLon());
		newEntry.setContactEmail(entry.getContactEmail());
		newEntry.setContactPhone(entry.getContactPhone());
		newEntry.setStatus(entry.getStatus());

		// return courtPersistence.update(newEntry);
		return newEntry;
	}

	protected Court _updateEntry(long primaryKey, Court entry, ServiceContext serviceContext) throws PortalException {

		Court updateEntry = fetchCourt(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setCategoryId(entry.getCategoryId());
		updateEntry.setHeading(entry.getHeading());
		updateEntry.setName(entry.getName());
		updateEntry.setListingImage(entry.getListingImage());
		updateEntry.setDetailImage(entry.getDetailImage());
		updateEntry.setVenue(entry.getVenue());
		updateEntry.setOpeningHours(entry.getOpeningHours());
		updateEntry.setOpeningDays(entry.getOpeningDays());
		updateEntry.setDescription(entry.getDescription());
		updateEntry.setLat(entry.getLat());
		updateEntry.setLon(entry.getLon());
		updateEntry.setContactEmail(entry.getContactEmail());
		updateEntry.setContactPhone(entry.getContactPhone());
		updateEntry.setStatus(entry.getStatus());

		return updateEntry;
	}

	public Court deleteEntry(long primaryKey) throws PortalException {
		Court entry = getCourt(primaryKey);
		courtPersistence.remove(entry);

		return entry;
	}

	public List<Court> findAllInGroup(long groupId) {

		return courtPersistence.findByGroupId(groupId);
	}

	public List<Court> findAllInGroup(long groupId, int start, int end, OrderByComparator<Court> obc) {

		return courtPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Court> findAllInGroup(long groupId, int start, int end) {

		return courtPersistence.findByGroupId(groupId, start, end);
	}

	public int countAllInGroup(long groupId) {

		return courtPersistence.countByGroupId(groupId);
	}

	public List<Court> findAllInCategory(long categoryId) {
		return courtPersistence.findByCategoryId(categoryId);
	}

	public List<Court> findAllInCategory(long categoryId, int start, int end, OrderByComparator<Court> obc) {
		return courtPersistence.findByCategoryId(categoryId, start, end, obc);
	}

	public List<Court> findAllInCategory(long categoryId, int start, int end) {
		return courtPersistence.findByCategoryId(categoryId, start, end);
	}

	public int countAllInCategory(long categoryId) {
		return courtPersistence.countByCategoryId(categoryId);
	}

	public Court getCourtFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, CourtValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Court entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchCourt(primaryKey);
		}

		try {
			entry.setCategoryId(ParamUtil.getLong(request, "categoryId"));
			entry.setHeadingMap(LocalizationUtil.getLocalizationMap(request, "heading"));
			entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
			entry.setListingImage(ParamUtil.getString(request, "listingImage"));
			entry.setDetailImage(ParamUtil.getString(request, "detailImage"));
			entry.setVenueMap(LocalizationUtil.getLocalizationMap(request, "venue"));
			entry.setOpeningHoursMap(LocalizationUtil.getLocalizationMap(request, "openingHours"));
			entry.setOpeningDaysMap(LocalizationUtil.getLocalizationMap(request, "openingDays"));
			entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));
			entry.setLat(ParamUtil.getString(request, "lat"));
			entry.setLon(ParamUtil.getString(request, "lon"));
			entry.setContactEmail(ParamUtil.getString(request, "contactEmail"));
			entry.setContactPhone(ParamUtil.getString(request, "contactPhone"));
			entry.setStatus(ParamUtil.getBoolean(request, "status"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new CourtValidateException(error);
		}

		return entry;
	}

	public Court getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Court.class.getName());

		return createCourt(primaryKey);
	}

	// search
	@Override
	public List<Court> searchCourts(SearchContext searchContext) {
		_log.info("searchCourts");
		Hits hits;
		List<Court> courtList = new ArrayList<>();
		CourtIndexer indexer = (CourtIndexer) IndexerRegistryUtil.getIndexer(Court.class);
		try {
			hits = indexer.search(searchContext);
			for (int i = 0; i < hits.getDocs().length; i++) {
				Document doc = hits.doc(i);
				long courtId = GetterUtil.getLong(doc.get(Field.ENTRY_CLASS_PK));
				Court court = null;
				court = CourtLocalServiceUtil.getCourt(courtId);
				courtList.add(court);
			}
		} catch (PortalException | SystemException e) {
			_log.error(e);
		}
		return courtList;
	}

	private static Log _log = LogFactoryUtil.getLog(CourtLocalServiceImpl.class);
}