/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package courts.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import courts.exception.CategoryValidateException;
import courts.model.Category;
import courts.service.base.CategoryLocalServiceBaseImpl;

import courts.service.util.CategoryValidator;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * The implementation of the category local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>courts.service.CategoryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CategoryLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=courts.model.Category",
	service = AopService.class
)
public class CategoryLocalServiceImpl extends CategoryLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>courts.service.CategoryLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>courts.service.CategoryLocalServiceUtil</code>.
	 */

	public Category addEntry(Category orgEntry, ServiceContext serviceContext)
			throws PortalException, CategoryValidateException {

		// Validation

		ModelValidator<Category> modelValidator = new CategoryValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Category entry = _addEntry(orgEntry, serviceContext);

		Category addedEntry = categoryPersistence.update(entry);

		categoryPersistence.clearCache();

		return addedEntry;
	}

	public Category updateEntry(
			Category orgEntry, ServiceContext serviceContext)
			throws PortalException, CategoryValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Category> modelValidator = new CategoryValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Category entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Category updatedEntry = categoryPersistence.update(entry);
		categoryPersistence.clearCache();

		return updatedEntry;
	}

	protected Category _addEntry(Category entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(Category.class.getName());

		Category newEntry = categoryPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setLocationId(entry.getLocationId());
		newEntry.setName(entry.getName());
		newEntry.setIcon(entry.getIcon());
		newEntry.setBlackIcon(entry.getBlackIcon());
		newEntry.setImage(entry.getImage());
		newEntry.setStatus(entry.getStatus());

		//return categoryPersistence.update(newEntry);
		return newEntry;
	}

	protected Category _updateEntry(
			long primaryKey, Category entry, ServiceContext serviceContext)
			throws PortalException {

		Category updateEntry = fetchCategory(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setLocationId(entry.getLocationId());
		updateEntry.setName(entry.getName());
		updateEntry.setIcon(entry.getIcon());
		updateEntry.setBlackIcon(entry.getBlackIcon());
		updateEntry.setImage(entry.getImage());
		updateEntry.setStatus(entry.getStatus());

		return updateEntry;
	}

	public Category deleteEntry(long primaryKey) throws PortalException {
		Category entry = getCategory(primaryKey);
		categoryPersistence.remove(entry);

		return entry;
	}

	public List<Category> findAllInGroup(long groupId) {

		return categoryPersistence.findByGroupId(groupId);
	}

	public List<Category> findAllInGroup(long groupId, int start, int end,
										 OrderByComparator<Category> obc) {

		return categoryPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Category> findAllInGroup(long groupId, int start, int end) {

		return categoryPersistence.findByGroupId(groupId, start, end);
	}


	public int countAllInGroup(long groupId) {

		return categoryPersistence.countByGroupId(groupId);
	}

	public Category getCategoryFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, CategoryValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Category entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchCategory(primaryKey);
		}

		try {
			entry.setLocationId(ParamUtil.getLong(request, "locationId"));
			entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
			entry.setIcon(ParamUtil.getString(request, "icon"));
			entry.setBlackIcon(ParamUtil.getString(request, "blackIcon"));
			entry.setImage(ParamUtil.getString(request, "image"));
			entry.setStatus(ParamUtil.getBoolean(request, "status"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new CategoryValidateException(error);
		}

		return entry;
	}

	public Category getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(Category.class.getName());

		return createCategory(primaryKey);
	}

	private static Log _log = LogFactoryUtil.getLog(
			CategoryLocalServiceImpl.class);
}