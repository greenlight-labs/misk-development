create index IX_902A0A66 on misk_courts (categoryId);
create index IX_5882BD1 on misk_courts (groupId);
create index IX_96C5E6AD on misk_courts (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_C802346F on misk_courts (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_DE1C56BC on misk_courts_categories (groupId);
create index IX_F5B722D2 on misk_courts_categories (locationId);
create index IX_70ABD422 on misk_courts_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_2DFEFB24 on misk_courts_categories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_AE25B452 on misk_courts_locations (groupId);
create index IX_6BE35F4C on misk_courts_locations (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_9D1348CE on misk_courts_locations (uuid_[$COLUMN_LENGTH:75$], groupId);