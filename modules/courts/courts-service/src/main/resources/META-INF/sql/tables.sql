create table misk_courts (
	uuid_ VARCHAR(75) null,
	courtId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	categoryId LONG,
	heading STRING null,
	name STRING null,
	listingImage VARCHAR(250) null,
	detailImage VARCHAR(250) null,
	venue STRING null,
	openingHours STRING null,
	openingDays STRING null,
	description TEXT null,
	lat VARCHAR(75) null,
	lon VARCHAR(75) null,
	contactEmail VARCHAR(75) null,
	contactPhone VARCHAR(75) null,
	status BOOLEAN
);

create table misk_courts_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	locationId LONG,
	name STRING null,
	icon VARCHAR(250) null,
	blackIcon VARCHAR(250) null,
	image VARCHAR(250) null,
	status BOOLEAN
);

create table misk_courts_locations (
	uuid_ VARCHAR(75) null,
	locationId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	status BOOLEAN
);