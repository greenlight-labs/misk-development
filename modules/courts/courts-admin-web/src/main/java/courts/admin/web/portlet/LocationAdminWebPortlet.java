package courts.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import courts.admin.web.constants.CourtsAdminWebPortletKeys;
import courts.exception.LocationValidateException;
import courts.model.Location;
import courts.service.LocationLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tz
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.add-default-resource=true",
                "com.liferay.portlet.display-category=category.hidden",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.layout-cacheable=true",
                "com.liferay.portlet.private-request-attributes=false",
                "com.liferay.portlet.private-session-attributes=false",
                "com.liferay.portlet.render-weight=50",
                "com.liferay.portlet.use-default-template=true",
                "javax.portlet.display-name=LocationAdminWeb",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/location/view.jsp",
                "javax.portlet.name=" + CourtsAdminWebPortletKeys.LOCATIONADMINWEB,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",

        },
        service = Portlet.class
)
public class LocationAdminWebPortlet extends MVCPortlet {

    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
                .create(renderRequest);

        ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

        List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
        desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

        imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

        PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
                "selectDocumentLibrary", imageItemSelectorCriterion);

        renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

        super.render(renderRequest, renderResponse);
    }

    public void addEntry(ActionRequest request, ActionResponse response)
            throws Exception, LocationValidateException {

        long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
        Location entry = _locationLocalService.getLocationFromRequest(
                primaryKey, request);
        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Location.class.getName(), request);

        // Add entry
        _locationLocalService.addEntry(entry, serviceContext);

        SessionMessages.add(request, "locationAddedSuccessfully");
    }

    public void updateEntry(ActionRequest request, ActionResponse response)
            throws Exception {

        long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

        Location entry = _locationLocalService.getLocationFromRequest(
                primaryKey, request);

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Location.class.getName(), request);

        //Update entry
        _locationLocalService.updateEntry(entry, serviceContext);

        SessionMessages.add(request, "locationUpdatedSuccessfully");
    }

    public void deleteEntry(ActionRequest request, ActionResponse response)
            throws PortalException {

        long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

        try {
            _locationLocalService.deleteEntry(entryId);
        } catch (PortalException pe) {
            ReflectionUtil.throwException(pe);
        }
    }

    @Reference
    private LocationLocalService _locationLocalService;

    @Reference
    private ItemSelector _itemSelector;
}