package courts.admin.web.application.list;

import courts.admin.web.constants.CourtsAdminWebPanelCategoryKeys;
import courts.admin.web.constants.CourtsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + CourtsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class CourtsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return CourtsAdminWebPortletKeys.COURTSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + CourtsAdminWebPortletKeys.COURTSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}