package courts.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import courts.admin.web.constants.CourtsAdminWebPanelCategoryKeys;
import courts.admin.web.constants.CourtsAdminWebPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=101",
		"panel.category.key=" + CourtsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class LocationAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return CourtsAdminWebPortletKeys.LOCATIONADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + CourtsAdminWebPortletKeys.LOCATIONADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}