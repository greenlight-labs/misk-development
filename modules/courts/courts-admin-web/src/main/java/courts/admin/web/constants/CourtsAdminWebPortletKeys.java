package courts.admin.web.constants;

/**
 * @author tz
 */
public class CourtsAdminWebPortletKeys {

	public static final String COURTSADMINWEB =
		"courts_admin_web_CourtsAdminWebPortlet";

	public static final String CATEGORIESADMINWEB =
		"courts_admin_web_CategoriesAdminWebPortlet";

	public static final String LOCATIONADMINWEB =
		"courts_admin_web_LocationAdminWebPortlet";

}