<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath" value="/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>" value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= CourtLocalServiceUtil.countAllInGroup(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= CourtLocalServiceUtil.findAllInGroup(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="courts.model.Court" modelVar="court">

            <%
                String categoryName = null;
                try {
                    categoryName = CategoryLocalServiceUtil.getCategory(court.getCategoryId()).getName(locale);
                } catch (PortalException e) {
                    e.printStackTrace();
                }
            %>

            <liferay-ui:search-container-column-text name="Category" value="<%= HtmlUtil.escape(categoryName) %>"/>
            <liferay-ui:search-container-column-text name="Name" value="<%= HtmlUtil.escape(court.getName(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Venue" value="<%= HtmlUtil.escape(court.getVenue(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Opening Hours" value="<%= HtmlUtil.escape(court.getOpeningHours(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Opening Days" value="<%= HtmlUtil.escape(court.getOpeningDays(locale)) %>"/>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>