<%@ include file="../init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="categories-admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/location/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Location"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= LocationLocalServiceUtil.countAllInGroup(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= LocationLocalServiceUtil.findAllInGroup(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="courts.model.Location" modelVar="location">

            <liferay-ui:search-container-column-text
                    name="name"
                    value="<%= HtmlUtil.escape(location.getName(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/location/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>