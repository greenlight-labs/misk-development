<%@include file = "init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    Court entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = CourtLocalServiceUtil.getCourt(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    List<Category> categories = CategoryLocalServiceUtil.findAllInGroup(scopeGroupId);
%>
 
<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= entry %>" model="<%= Court.class %>" />

        <aui:input type="hidden" name="resourcePrimKey"
                   value='<%= entry == null ? "" : entry.getCourtId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:select required="true" label="Category" name="categoryId">
                    <% for (Category curCategory : categories) { %>
                    <aui:option label="<%= curCategory.getName(locale) %>" value="<%= curCategory.getCategoryId() %>" />
                    <% } %>
                </aui:select>
                <aui:input name="heading" label="Heading" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="name" label="Name" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="listingImage" label="Listing Image" helpMessage="Image Dimensions: W x H pixels">
                    <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                        function(val) {
                            var ext = val.substring(val.lastIndexOf('.') + 1);
                            return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                        }
                    </aui:validator>
                </aui:input>
                <div class="button-holder">
                    <button class="btn select-button btn-secondary" type="button" data-field-id="listingImage">
                        <span class="lfr-btn-label">Select</span>
                    </button>
                </div>
                <aui:input name="detailImage" label="Detail Image" helpMessage="Image Dimensions: W x H pixels">
                    <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                        function(val) {
                            var ext = val.substring(val.lastIndexOf('.') + 1);
                            return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                        }
                    </aui:validator>
                </aui:input>
                <div class="button-holder">
                    <button class="btn select-button btn-secondary" type="button" data-field-id="detailImage">
                        <span class="lfr-btn-label">Select</span>
                    </button>
                </div>
                <aui:input name="venue" label="Venue" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="openingHours" label="Opening Hours" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="openingDays" label="Opening Days" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="description" label="Description" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="lat" label="Latitude" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="lon" label="Longitude" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="contactEmail" label="Contact Email" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="contactPhone" label="Contact Phone" helpMessage="Max 30 Characters (Recommended)"/>
                <aui:input name="status" label="Status" helpMessage="Max 30 Characters (Recommended)"/>
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>