/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import mobilities.model.Mobility;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Mobility. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see MobilityLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface MobilityLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>mobilities.service.impl.MobilityLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the mobility local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link MobilityLocalServiceUtil} if injection and service tracking are not available.
	 */
	public Mobility addMobility(
			long userId, Map<Locale, String> listingImageMap,
			Map<Locale, String> listingTitleMap,
			Map<Locale, String> listingDescriptionMap,
			Map<Locale, String> ButtonLabelMap,
			Map<Locale, String> ButtonLinkMap, ServiceContext serviceContext)
		throws PortalException;

	/**
	 * Adds the mobility to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Mobility addMobility(Mobility mobility);

	/**
	 * Creates a new mobility with the primary key. Does not add the mobility to the database.
	 *
	 * @param mobilityId the primary key for the new mobility
	 * @return the new mobility
	 */
	@Transactional(enabled = false)
	public Mobility createMobility(long mobilityId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the mobility with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility that was removed
	 * @throws PortalException if a mobility with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Mobility deleteMobility(long mobilityId) throws PortalException;

	public Mobility deleteMobility(
			long mobilityId, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * Deletes the mobility from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Mobility deleteMobility(Mobility mobility);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Mobility fetchMobility(long mobilityId);

	/**
	 * Returns the mobility matching the UUID and group.
	 *
	 * @param uuid the mobility's UUID
	 * @param groupId the primary key of the group
	 * @return the matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Mobility fetchMobilityByUuidAndGroupId(String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns a range of all the mobilities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @return the range of mobilities
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Mobility> getMobilities(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Mobility> getMobilities(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Mobility> getMobilities(long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Mobility> getMobilities(
		long groupId, int start, int end, OrderByComparator<Mobility> obc);

	/**
	 * Returns all the mobilities matching the UUID and company.
	 *
	 * @param uuid the UUID of the mobilities
	 * @param companyId the primary key of the company
	 * @return the matching mobilities, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Mobility> getMobilitiesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of mobilities matching the UUID and company.
	 *
	 * @param uuid the UUID of the mobilities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching mobilities, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Mobility> getMobilitiesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Mobility> orderByComparator);

	/**
	 * Returns the number of mobilities.
	 *
	 * @return the number of mobilities
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getMobilitiesCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getMobilitiesCount(long groupId);

	/**
	 * Returns the mobility with the primary key.
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility
	 * @throws PortalException if a mobility with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Mobility getMobility(long mobilityId) throws PortalException;

	/**
	 * Returns the mobility matching the UUID and group.
	 *
	 * @param uuid the mobility's UUID
	 * @param groupId the primary key of the group
	 * @return the matching mobility
	 * @throws PortalException if a matching mobility could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Mobility getMobilityByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public Mobility updateMobility(
			long userId, long mobilityId, Map<Locale, String> listingImageMap,
			Map<Locale, String> listingTitleMap,
			Map<Locale, String> listingDescriptionMap,
			Map<Locale, String> ButtonLabelMap,
			Map<Locale, String> ButtonLinkMap, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * Updates the mobility in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Mobility updateMobility(Mobility mobility);

}