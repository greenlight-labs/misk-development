/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MobilityLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MobilityLocalService
 * @generated
 */
public class MobilityLocalServiceWrapper
	implements MobilityLocalService, ServiceWrapper<MobilityLocalService> {

	public MobilityLocalServiceWrapper(
		MobilityLocalService mobilityLocalService) {

		_mobilityLocalService = mobilityLocalService;
	}

	@Override
	public mobilities.model.Mobility addMobility(
			long userId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> ButtonLabelMap,
			java.util.Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mobilityLocalService.addMobility(
			userId, listingImageMap, listingTitleMap, listingDescriptionMap,
			ButtonLabelMap, ButtonLinkMap, serviceContext);
	}

	/**
	 * Adds the mobility to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was added
	 */
	@Override
	public mobilities.model.Mobility addMobility(
		mobilities.model.Mobility mobility) {

		return _mobilityLocalService.addMobility(mobility);
	}

	/**
	 * Creates a new mobility with the primary key. Does not add the mobility to the database.
	 *
	 * @param mobilityId the primary key for the new mobility
	 * @return the new mobility
	 */
	@Override
	public mobilities.model.Mobility createMobility(long mobilityId) {
		return _mobilityLocalService.createMobility(mobilityId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mobilityLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the mobility with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility that was removed
	 * @throws PortalException if a mobility with the primary key could not be found
	 */
	@Override
	public mobilities.model.Mobility deleteMobility(long mobilityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mobilityLocalService.deleteMobility(mobilityId);
	}

	@Override
	public mobilities.model.Mobility deleteMobility(
			long mobilityId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _mobilityLocalService.deleteMobility(mobilityId, serviceContext);
	}

	/**
	 * Deletes the mobility from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was removed
	 */
	@Override
	public mobilities.model.Mobility deleteMobility(
		mobilities.model.Mobility mobility) {

		return _mobilityLocalService.deleteMobility(mobility);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mobilityLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _mobilityLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _mobilityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _mobilityLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _mobilityLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _mobilityLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _mobilityLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public mobilities.model.Mobility fetchMobility(long mobilityId) {
		return _mobilityLocalService.fetchMobility(mobilityId);
	}

	/**
	 * Returns the mobility matching the UUID and group.
	 *
	 * @param uuid the mobility's UUID
	 * @param groupId the primary key of the group
	 * @return the matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	@Override
	public mobilities.model.Mobility fetchMobilityByUuidAndGroupId(
		String uuid, long groupId) {

		return _mobilityLocalService.fetchMobilityByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _mobilityLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _mobilityLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _mobilityLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the mobilities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @return the range of mobilities
	 */
	@Override
	public java.util.List<mobilities.model.Mobility> getMobilities(
		int start, int end) {

		return _mobilityLocalService.getMobilities(start, end);
	}

	@Override
	public java.util.List<mobilities.model.Mobility> getMobilities(
		long groupId) {

		return _mobilityLocalService.getMobilities(groupId);
	}

	@Override
	public java.util.List<mobilities.model.Mobility> getMobilities(
		long groupId, int start, int end) {

		return _mobilityLocalService.getMobilities(groupId, start, end);
	}

	@Override
	public java.util.List<mobilities.model.Mobility> getMobilities(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<mobilities.model.Mobility> obc) {

		return _mobilityLocalService.getMobilities(groupId, start, end, obc);
	}

	/**
	 * Returns all the mobilities matching the UUID and company.
	 *
	 * @param uuid the UUID of the mobilities
	 * @param companyId the primary key of the company
	 * @return the matching mobilities, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<mobilities.model.Mobility>
		getMobilitiesByUuidAndCompanyId(String uuid, long companyId) {

		return _mobilityLocalService.getMobilitiesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of mobilities matching the UUID and company.
	 *
	 * @param uuid the UUID of the mobilities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching mobilities, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<mobilities.model.Mobility>
		getMobilitiesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<mobilities.model.Mobility> orderByComparator) {

		return _mobilityLocalService.getMobilitiesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of mobilities.
	 *
	 * @return the number of mobilities
	 */
	@Override
	public int getMobilitiesCount() {
		return _mobilityLocalService.getMobilitiesCount();
	}

	@Override
	public int getMobilitiesCount(long groupId) {
		return _mobilityLocalService.getMobilitiesCount(groupId);
	}

	/**
	 * Returns the mobility with the primary key.
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility
	 * @throws PortalException if a mobility with the primary key could not be found
	 */
	@Override
	public mobilities.model.Mobility getMobility(long mobilityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mobilityLocalService.getMobility(mobilityId);
	}

	/**
	 * Returns the mobility matching the UUID and group.
	 *
	 * @param uuid the mobility's UUID
	 * @param groupId the primary key of the group
	 * @return the matching mobility
	 * @throws PortalException if a matching mobility could not be found
	 */
	@Override
	public mobilities.model.Mobility getMobilityByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mobilityLocalService.getMobilityByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _mobilityLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mobilityLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public mobilities.model.Mobility updateMobility(
			long userId, long mobilityId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> ButtonLabelMap,
			java.util.Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _mobilityLocalService.updateMobility(
			userId, mobilityId, listingImageMap, listingTitleMap,
			listingDescriptionMap, ButtonLabelMap, ButtonLinkMap,
			serviceContext);
	}

	/**
	 * Updates the mobility in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was updated
	 */
	@Override
	public mobilities.model.Mobility updateMobility(
		mobilities.model.Mobility mobility) {

		return _mobilityLocalService.updateMobility(mobility);
	}

	@Override
	public MobilityLocalService getWrappedService() {
		return _mobilityLocalService;
	}

	@Override
	public void setWrappedService(MobilityLocalService mobilityLocalService) {
		_mobilityLocalService = mobilityLocalService;
	}

	private MobilityLocalService _mobilityLocalService;

}