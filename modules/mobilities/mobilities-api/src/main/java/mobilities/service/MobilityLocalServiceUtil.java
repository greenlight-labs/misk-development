/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import mobilities.model.Mobility;

/**
 * Provides the local service utility for Mobility. This utility wraps
 * <code>mobilities.service.impl.MobilityLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see MobilityLocalService
 * @generated
 */
public class MobilityLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>mobilities.service.impl.MobilityLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Mobility addMobility(
			long userId, Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> ButtonLabelMap,
			Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addMobility(
			userId, listingImageMap, listingTitleMap, listingDescriptionMap,
			ButtonLabelMap, ButtonLinkMap, serviceContext);
	}

	/**
	 * Adds the mobility to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was added
	 */
	public static Mobility addMobility(Mobility mobility) {
		return getService().addMobility(mobility);
	}

	/**
	 * Creates a new mobility with the primary key. Does not add the mobility to the database.
	 *
	 * @param mobilityId the primary key for the new mobility
	 * @return the new mobility
	 */
	public static Mobility createMobility(long mobilityId) {
		return getService().createMobility(mobilityId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the mobility with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility that was removed
	 * @throws PortalException if a mobility with the primary key could not be found
	 */
	public static Mobility deleteMobility(long mobilityId)
		throws PortalException {

		return getService().deleteMobility(mobilityId);
	}

	public static Mobility deleteMobility(
			long mobilityId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteMobility(mobilityId, serviceContext);
	}

	/**
	 * Deletes the mobility from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was removed
	 */
	public static Mobility deleteMobility(Mobility mobility) {
		return getService().deleteMobility(mobility);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Mobility fetchMobility(long mobilityId) {
		return getService().fetchMobility(mobilityId);
	}

	/**
	 * Returns the mobility matching the UUID and group.
	 *
	 * @param uuid the mobility's UUID
	 * @param groupId the primary key of the group
	 * @return the matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public static Mobility fetchMobilityByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchMobilityByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the mobilities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>mobilities.model.impl.MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @return the range of mobilities
	 */
	public static List<Mobility> getMobilities(int start, int end) {
		return getService().getMobilities(start, end);
	}

	public static List<Mobility> getMobilities(long groupId) {
		return getService().getMobilities(groupId);
	}

	public static List<Mobility> getMobilities(
		long groupId, int start, int end) {

		return getService().getMobilities(groupId, start, end);
	}

	public static List<Mobility> getMobilities(
		long groupId, int start, int end, OrderByComparator<Mobility> obc) {

		return getService().getMobilities(groupId, start, end, obc);
	}

	/**
	 * Returns all the mobilities matching the UUID and company.
	 *
	 * @param uuid the UUID of the mobilities
	 * @param companyId the primary key of the company
	 * @return the matching mobilities, or an empty list if no matches were found
	 */
	public static List<Mobility> getMobilitiesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getMobilitiesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of mobilities matching the UUID and company.
	 *
	 * @param uuid the UUID of the mobilities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching mobilities, or an empty list if no matches were found
	 */
	public static List<Mobility> getMobilitiesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Mobility> orderByComparator) {

		return getService().getMobilitiesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of mobilities.
	 *
	 * @return the number of mobilities
	 */
	public static int getMobilitiesCount() {
		return getService().getMobilitiesCount();
	}

	public static int getMobilitiesCount(long groupId) {
		return getService().getMobilitiesCount(groupId);
	}

	/**
	 * Returns the mobility with the primary key.
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility
	 * @throws PortalException if a mobility with the primary key could not be found
	 */
	public static Mobility getMobility(long mobilityId) throws PortalException {
		return getService().getMobility(mobilityId);
	}

	/**
	 * Returns the mobility matching the UUID and group.
	 *
	 * @param uuid the mobility's UUID
	 * @param groupId the primary key of the group
	 * @return the matching mobility
	 * @throws PortalException if a matching mobility could not be found
	 */
	public static Mobility getMobilityByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getMobilityByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static Mobility updateMobility(
			long userId, long mobilityId,
			Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> ButtonLabelMap,
			Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateMobility(
			userId, mobilityId, listingImageMap, listingTitleMap,
			listingDescriptionMap, ButtonLabelMap, ButtonLinkMap,
			serviceContext);
	}

	/**
	 * Updates the mobility in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MobilityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mobility the mobility
	 * @return the mobility that was updated
	 */
	public static Mobility updateMobility(Mobility mobility) {
		return getService().updateMobility(mobility);
	}

	public static MobilityLocalService getService() {
		return _service;
	}

	private static volatile MobilityLocalService _service;

}