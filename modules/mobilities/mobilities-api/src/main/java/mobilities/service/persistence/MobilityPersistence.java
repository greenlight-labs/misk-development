/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import mobilities.exception.NoSuchMobilityException;

import mobilities.model.Mobility;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the mobility service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MobilityUtil
 * @generated
 */
@ProviderType
public interface MobilityPersistence extends BasePersistence<Mobility> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link MobilityUtil} to access the mobility persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the mobilities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching mobilities
	 */
	public java.util.List<Mobility> findByUuid(String uuid);

	/**
	 * Returns a range of all the mobilities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @return the range of matching mobilities
	 */
	public java.util.List<Mobility> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the mobilities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching mobilities
	 */
	public java.util.List<Mobility> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns an ordered range of all the mobilities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching mobilities
	 */
	public java.util.List<Mobility> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first mobility in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mobility
	 * @throws NoSuchMobilityException if a matching mobility could not be found
	 */
	public Mobility findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Returns the first mobility in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns the last mobility in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mobility
	 * @throws NoSuchMobilityException if a matching mobility could not be found
	 */
	public Mobility findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Returns the last mobility in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns the mobilities before and after the current mobility in the ordered set where uuid = &#63;.
	 *
	 * @param mobilityId the primary key of the current mobility
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next mobility
	 * @throws NoSuchMobilityException if a mobility with the primary key could not be found
	 */
	public Mobility[] findByUuid_PrevAndNext(
			long mobilityId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Removes all the mobilities where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of mobilities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching mobilities
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the mobility where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchMobilityException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching mobility
	 * @throws NoSuchMobilityException if a matching mobility could not be found
	 */
	public Mobility findByUUID_G(String uuid, long groupId)
		throws NoSuchMobilityException;

	/**
	 * Returns the mobility where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the mobility where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the mobility where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the mobility that was removed
	 */
	public Mobility removeByUUID_G(String uuid, long groupId)
		throws NoSuchMobilityException;

	/**
	 * Returns the number of mobilities where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching mobilities
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the mobilities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching mobilities
	 */
	public java.util.List<Mobility> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the mobilities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @return the range of matching mobilities
	 */
	public java.util.List<Mobility> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the mobilities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching mobilities
	 */
	public java.util.List<Mobility> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns an ordered range of all the mobilities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching mobilities
	 */
	public java.util.List<Mobility> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first mobility in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mobility
	 * @throws NoSuchMobilityException if a matching mobility could not be found
	 */
	public Mobility findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Returns the first mobility in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns the last mobility in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mobility
	 * @throws NoSuchMobilityException if a matching mobility could not be found
	 */
	public Mobility findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Returns the last mobility in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns the mobilities before and after the current mobility in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param mobilityId the primary key of the current mobility
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next mobility
	 * @throws NoSuchMobilityException if a mobility with the primary key could not be found
	 */
	public Mobility[] findByUuid_C_PrevAndNext(
			long mobilityId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Removes all the mobilities where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of mobilities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching mobilities
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the mobilities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching mobilities
	 */
	public java.util.List<Mobility> findByGroupId(long groupId);

	/**
	 * Returns a range of all the mobilities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @return the range of matching mobilities
	 */
	public java.util.List<Mobility> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the mobilities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching mobilities
	 */
	public java.util.List<Mobility> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns an ordered range of all the mobilities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching mobilities
	 */
	public java.util.List<Mobility> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first mobility in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mobility
	 * @throws NoSuchMobilityException if a matching mobility could not be found
	 */
	public Mobility findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Returns the first mobility in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns the last mobility in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mobility
	 * @throws NoSuchMobilityException if a matching mobility could not be found
	 */
	public Mobility findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Returns the last mobility in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching mobility, or <code>null</code> if a matching mobility could not be found
	 */
	public Mobility fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns the mobilities before and after the current mobility in the ordered set where groupId = &#63;.
	 *
	 * @param mobilityId the primary key of the current mobility
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next mobility
	 * @throws NoSuchMobilityException if a mobility with the primary key could not be found
	 */
	public Mobility[] findByGroupId_PrevAndNext(
			long mobilityId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Mobility>
				orderByComparator)
		throws NoSuchMobilityException;

	/**
	 * Removes all the mobilities where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of mobilities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching mobilities
	 */
	public int countByGroupId(long groupId);

	/**
	 * Caches the mobility in the entity cache if it is enabled.
	 *
	 * @param mobility the mobility
	 */
	public void cacheResult(Mobility mobility);

	/**
	 * Caches the mobilities in the entity cache if it is enabled.
	 *
	 * @param mobilities the mobilities
	 */
	public void cacheResult(java.util.List<Mobility> mobilities);

	/**
	 * Creates a new mobility with the primary key. Does not add the mobility to the database.
	 *
	 * @param mobilityId the primary key for the new mobility
	 * @return the new mobility
	 */
	public Mobility create(long mobilityId);

	/**
	 * Removes the mobility with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility that was removed
	 * @throws NoSuchMobilityException if a mobility with the primary key could not be found
	 */
	public Mobility remove(long mobilityId) throws NoSuchMobilityException;

	public Mobility updateImpl(Mobility mobility);

	/**
	 * Returns the mobility with the primary key or throws a <code>NoSuchMobilityException</code> if it could not be found.
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility
	 * @throws NoSuchMobilityException if a mobility with the primary key could not be found
	 */
	public Mobility findByPrimaryKey(long mobilityId)
		throws NoSuchMobilityException;

	/**
	 * Returns the mobility with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param mobilityId the primary key of the mobility
	 * @return the mobility, or <code>null</code> if a mobility with the primary key could not be found
	 */
	public Mobility fetchByPrimaryKey(long mobilityId);

	/**
	 * Returns all the mobilities.
	 *
	 * @return the mobilities
	 */
	public java.util.List<Mobility> findAll();

	/**
	 * Returns a range of all the mobilities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @return the range of mobilities
	 */
	public java.util.List<Mobility> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the mobilities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of mobilities
	 */
	public java.util.List<Mobility> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator);

	/**
	 * Returns an ordered range of all the mobilities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MobilityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of mobilities
	 * @param end the upper bound of the range of mobilities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of mobilities
	 */
	public java.util.List<Mobility> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Mobility>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the mobilities from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of mobilities.
	 *
	 * @return the number of mobilities
	 */
	public int countAll();

}