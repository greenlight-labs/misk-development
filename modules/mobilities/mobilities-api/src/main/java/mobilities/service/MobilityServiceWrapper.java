/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MobilityService}.
 *
 * @author Brian Wing Shun Chan
 * @see MobilityService
 * @generated
 */
public class MobilityServiceWrapper
	implements MobilityService, ServiceWrapper<MobilityService> {

	public MobilityServiceWrapper(MobilityService mobilityService) {
		_mobilityService = mobilityService;
	}

	@Override
	public java.util.List<mobilities.model.Mobility> getMobilities(
		long groupId) {

		return _mobilityService.getMobilities(groupId);
	}

	@Override
	public java.util.List<mobilities.model.Mobility> getMobilities(
		long groupId, int start, int end) {

		return _mobilityService.getMobilities(groupId, start, end);
	}

	@Override
	public java.util.List<mobilities.model.Mobility> getMobilities(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<mobilities.model.Mobility> obc) {

		return _mobilityService.getMobilities(groupId, start, end, obc);
	}

	@Override
	public int getMobilitiesCount(long groupId) {
		return _mobilityService.getMobilitiesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _mobilityService.getOSGiServiceIdentifier();
	}

	@Override
	public MobilityService getWrappedService() {
		return _mobilityService;
	}

	@Override
	public void setWrappedService(MobilityService mobilityService) {
		_mobilityService = mobilityService;
	}

	private MobilityService _mobilityService;

}