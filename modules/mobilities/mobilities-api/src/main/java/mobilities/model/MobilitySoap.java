/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link mobilities.service.http.MobilityServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class MobilitySoap implements Serializable {

	public static MobilitySoap toSoapModel(Mobility model) {
		MobilitySoap soapModel = new MobilitySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setMobilityId(model.getMobilityId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setListingImage(model.getListingImage());
		soapModel.setListingTitle(model.getListingTitle());
		soapModel.setListingDescription(model.getListingDescription());
		soapModel.setButtonLabel(model.getButtonLabel());
		soapModel.setButtonLink(model.getButtonLink());

		return soapModel;
	}

	public static MobilitySoap[] toSoapModels(Mobility[] models) {
		MobilitySoap[] soapModels = new MobilitySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MobilitySoap[][] toSoapModels(Mobility[][] models) {
		MobilitySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MobilitySoap[models.length][models[0].length];
		}
		else {
			soapModels = new MobilitySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MobilitySoap[] toSoapModels(List<Mobility> models) {
		List<MobilitySoap> soapModels = new ArrayList<MobilitySoap>(
			models.size());

		for (Mobility model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MobilitySoap[soapModels.size()]);
	}

	public MobilitySoap() {
	}

	public long getPrimaryKey() {
		return _mobilityId;
	}

	public void setPrimaryKey(long pk) {
		setMobilityId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getMobilityId() {
		return _mobilityId;
	}

	public void setMobilityId(long mobilityId) {
		_mobilityId = mobilityId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getListingImage() {
		return _listingImage;
	}

	public void setListingImage(String listingImage) {
		_listingImage = listingImage;
	}

	public String getListingTitle() {
		return _listingTitle;
	}

	public void setListingTitle(String listingTitle) {
		_listingTitle = listingTitle;
	}

	public String getListingDescription() {
		return _listingDescription;
	}

	public void setListingDescription(String listingDescription) {
		_listingDescription = listingDescription;
	}

	public String getButtonLabel() {
		return _buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		_buttonLabel = buttonLabel;
	}

	public String getButtonLink() {
		return _buttonLink;
	}

	public void setButtonLink(String buttonLink) {
		_buttonLink = buttonLink;
	}

	private String _uuid;
	private long _mobilityId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _listingImage;
	private String _listingTitle;
	private String _listingDescription;
	private String _buttonLabel;
	private String _buttonLink;

}