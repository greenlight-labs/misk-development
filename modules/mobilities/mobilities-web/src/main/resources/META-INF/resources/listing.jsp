<%@ page import="java.util.Objects" %>
<section class="residence-listing-sec" data-scroll-section>
    <div class="container">
        <div class="row">
            <%
                List<Mobility> mobilities = MobilityServiceUtil.getMobilities(scopeGroupId);

                for (Mobility curMobility : mobilities) {
            %>
            <div class="col-12 col-md-6">
                <div class="residence-card">
                    <a href="javascript:" class="img-wrapper image-overlay">
                        <img src="<%= curMobility.getListingImage(locale) %>" alt="" class="img-fluid">
                    </a>
                    <div class="res-card-info">
                        <h3><%= curMobility.getListingTitle(locale) %></h3>
                        <p><%= curMobility.getListingDescription(locale) %></p>
                        <% if(curMobility.getButtonLabel(locale) != null && curMobility.getButtonLink(locale)  != null && !Objects.equals(curMobility.getButtonLabel(locale), "") && !Objects.equals(curMobility.getButtonLink(locale), "")){ %>
                            <a href="<%=curMobility.getButtonLink(locale)%>" class="btn btn-outline-white">
                                <span><%=curMobility.getButtonLabel(locale)%></span><i class="dot-line"></i>
                            </a>
                        <% } %>
                    </div>
                </div>
            </div>
            <%
                }
            %>
        </div>
    </div>
</section>