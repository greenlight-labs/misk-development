<section class="mobility-social-media-section">
    <div class="social-media-section" data-scroll-section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="related-news-heading">
                        <h3>misk social wall</h3>
                        <a href="/en/social-wall/" class="btn btn-outline-primary" data-animation="fadeInUp" data-duration="500"><span>VIEW SOCIAL WALL</span><i class="dot-line"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="social-media-img-slider-section">
            <div>
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a href="javascript:" class="image-overlay"><img class="img-fluid" src="/assets/images/social-media-image-slider-1.png" alt="" ></a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text twitter-color" href="javascript:">
                            <i class="so-icon"><img src="/assets/svgs/twitter.svg" alt="" class="img-fluid"></i>
                            <span>@MiSK City</span>
                        </a>
                        <h5>For High Quality and Fresh Environment Living Visit at MiSK City</h5>
                        <ul>
                            <li><a href="javascript:">#MiSK CITY</a></li>
                            <li><a href="javascript:">#Green Environment</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a href="javascript:" class="image-overlay"><img class="img-fluid" src="/assets/images/social-media-image-slider-2.png" alt="" ></a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text facebook-color" href="javascript:">
                            <i class="so-icon"><img src="/assets/svgs/facebook.svg" alt="" class="img-fluid"></i>
                            <span>/MiSK City</span>
                        </a>
                        <h5>Best Residencies for The High Standard of Living in MiSK City</h5>
                        <ul>
                            <li><a href="javascript:">#MiSK CITY</a></li>
                            <li><a href="javascript:">#High Standard Living</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a href="javascript:" class="image-overlay"><img class="img-fluid" src="/assets/images/social-media-image-slider-3.png" alt="" ></a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text instagram-color" href="javascript:">
                            <i class="so-icon"><img src="/assets/svgs/instagram.svg" alt="" class="img-fluid"></i>
                            <span>@MiSK City</span>
                        </a>
                        <h5>Best Residencies for The High Standard of Living in MiSK City</h5>
                        <ul>
                            <li><a href="javascript:">#MiSK CITY</a></li>
                            <li><a href="javascript:">#High Standard Living</a></li>
                            <li><a href="javascript:">#Instagrampost</a></li>
                            <li><a href="javascript:">#livehappy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a href="javascript:" class="image-overlay"><img class="img-fluid" src="/assets/images/social-media-image-slider-4.png" alt="" ></a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text twitter-color" href="javascript:">
                            <i class="so-icon"><img src="/assets/svgs/twitter.svg" alt="" class="img-fluid"></i>
                            <span>@MiSK City</span>
                        </a>
                        <h5>For High Quality and Fresh Environment Living Visit at MiSK City</h5>
                        <ul>
                            <li><a href="javascript:">#MiSK CITY</a></li>
                            <li><a href="javascript:">#Green Environment</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div>
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a href="javascript:" class="image-overlay"><img class="img-fluid" src="/assets/images/social-media-image-slider-5.png" alt="" ></a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text instagram-color" href="javascript:">
                            <i class="so-icon"><img src="/assets/svgs/instagram.svg" alt="" class="img-fluid"></i>
                            <span>@MiSK City</span>
                        </a>
                        <h5>Best Residencies for The High Standard of Living in MiSK City</h5>
                        <ul>
                            <li><a href="javascript:">#MiSK CITY</a></li>
                            <li><a href="javascript:">#High Standard Living i was take casual leave</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
