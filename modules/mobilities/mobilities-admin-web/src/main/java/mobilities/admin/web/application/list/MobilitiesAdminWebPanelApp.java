package mobilities.admin.web.application.list;

import mobilities.admin.web.constants.MobilitiesAdminWebPanelCategoryKeys;
import mobilities.admin.web.constants.MobilitiesAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + MobilitiesAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class MobilitiesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return MobilitiesAdminWebPortletKeys.MOBILITIESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + MobilitiesAdminWebPortletKeys.MOBILITIESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}