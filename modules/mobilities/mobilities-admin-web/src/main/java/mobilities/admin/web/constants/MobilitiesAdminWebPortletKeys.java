package mobilities.admin.web.constants;

/**
 * @author tz
 */
public class MobilitiesAdminWebPortletKeys {

	public static final String MOBILITIESADMINWEB =
		"mobilities_admin_web_MobilitiesAdminWebPortlet";

}