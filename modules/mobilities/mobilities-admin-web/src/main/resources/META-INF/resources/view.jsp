<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="mobilities-admin-buttons">
        <portlet:renderURL var="addMobilityURL">
            <portlet:param name="mvcPath"
                           value="/edit_mobility.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addMobilityURL.toString() %>"
                    value="Add Mobility"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= MobilityLocalServiceUtil.getMobilitiesCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= MobilityLocalServiceUtil.getMobilities(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="mobilities.model.Mobility" modelVar="mobility">

            <liferay-ui:search-container-column-text
                    name="Listing Title"
                    value="<%= HtmlUtil.escape(mobility.getListingTitle(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>