<%@include file="init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Mobility mobility = (Mobility) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="mobilityId"
                       value="<%=String.valueOf(mobility.getMobilityId()) %>" />
        <portlet:param name="mvcPath"
                       value="/edit_mobility.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteMobility" var="deleteURL">
        <portlet:param name="mobilityId"
                       value="<%= String.valueOf(mobility.getMobilityId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>