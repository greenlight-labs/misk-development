create index IX_31C8C770 on misk_mobilities (groupId);
create index IX_63A0EDEE on misk_mobilities (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_2EC817F0 on misk_mobilities (uuid_[$COLUMN_LENGTH:75$], groupId);