create table misk_mobilities (
	uuid_ VARCHAR(75) null,
	mobilityId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	listingImage STRING null,
	listingTitle STRING null,
	listingDescription STRING null,
	buttonLabel STRING null,
	buttonLink STRING null
);