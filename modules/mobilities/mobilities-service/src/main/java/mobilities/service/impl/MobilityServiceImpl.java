/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import mobilities.model.Mobility;
import mobilities.service.base.MobilityServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the mobility remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>mobilities.service.MobilityService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MobilityServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=mobility",
		"json.web.service.context.path=Mobility"
	},
	service = AopService.class
)
public class MobilityServiceImpl extends MobilityServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>mobilities.service.MobilityServiceUtil</code> to access the mobility remote service.
	 */

	/* *
	 * get Mobilities by group
	 * */
	public List<Mobility> getMobilities(long groupId) {

		return mobilityPersistence.findByGroupId(groupId);
	}

	public List<Mobility> getMobilities(long groupId, int start, int end,
									  OrderByComparator<Mobility> obc) {

		return mobilityPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Mobility> getMobilities(long groupId, int start, int end) {

		return mobilityPersistence.findByGroupId(groupId, start, end);
	}

	public int getMobilitiesCount(long groupId) {

		return mobilityPersistence.countByGroupId(groupId);
	}
}