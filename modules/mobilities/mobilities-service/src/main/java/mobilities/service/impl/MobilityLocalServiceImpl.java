/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import mobilities.exception.MobilityListingDescriptionException;
import mobilities.exception.MobilityListingImageException;
import mobilities.exception.MobilityListingTitleException;
import mobilities.model.Mobility;
import mobilities.service.base.MobilityLocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the mobility local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>mobilities.service.MobilityLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MobilityLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=mobilities.model.Mobility",
	service = AopService.class
)
public class MobilityLocalServiceImpl extends MobilityLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>mobilities.service.MobilityLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>mobilities.service.MobilityLocalServiceUtil</code>.
	 */

	public Mobility addMobility(long userId,
								Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap, Map<Locale, String> ButtonLabelMap, Map<Locale, String> ButtonLinkMap,
								ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(listingImageMap, listingTitleMap, listingDescriptionMap);

		long mobilityId = counterLocalService.increment();

		Mobility mobility = mobilityPersistence.create(mobilityId);

		mobility.setUuid(serviceContext.getUuid());
		mobility.setUserId(userId);
		mobility.setGroupId(groupId);
		mobility.setCompanyId(user.getCompanyId());
		mobility.setUserName(user.getFullName());
		mobility.setCreateDate(serviceContext.getCreateDate(now));
		mobility.setModifiedDate(serviceContext.getModifiedDate(now));

		mobility.setListingImageMap(listingImageMap);
		mobility.setListingTitleMap(listingTitleMap);
		mobility.setListingDescriptionMap(listingDescriptionMap);
		mobility.setButtonLabelMap(ButtonLabelMap);
		mobility.setButtonLinkMap(ButtonLinkMap);

		mobility.setExpandoBridgeAttributes(serviceContext);

		mobilityPersistence.update(mobility);
		mobilityPersistence.clearCache();

		return mobility;
	}

	public Mobility updateMobility(long userId, long mobilityId,
								 Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap, Map<Locale, String> ButtonLabelMap, Map<Locale, String> ButtonLinkMap,
								 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(listingImageMap, listingTitleMap, listingDescriptionMap);

		Mobility mobility = getMobility(mobilityId);

		User user = userLocalService.getUser(userId);

		mobility.setUserId(userId);
		mobility.setUserName(user.getFullName());
		mobility.setModifiedDate(serviceContext.getModifiedDate(now));

		mobility.setListingImageMap(listingImageMap);
		mobility.setListingTitleMap(listingTitleMap);
		mobility.setListingDescriptionMap(listingDescriptionMap);
		mobility.setButtonLabelMap(ButtonLabelMap);
		mobility.setButtonLinkMap(ButtonLinkMap);

		mobility.setExpandoBridgeAttributes(serviceContext);

		mobilityPersistence.update(mobility);
		mobilityPersistence.clearCache();

		return mobility;
	}

	public Mobility deleteMobility(long mobilityId,
								 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Mobility mobility = getMobility(mobilityId);
		mobility = deleteMobility(mobility);

		return mobility;
	}

	public List<Mobility> getMobilities(long groupId) {

		return mobilityPersistence.findByGroupId(groupId);
	}

	public List<Mobility> getMobilities(long groupId, int start, int end,
									  OrderByComparator<Mobility> obc) {

		return mobilityPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Mobility> getMobilities(long groupId, int start, int end) {

		return mobilityPersistence.findByGroupId(groupId, start, end);
	}

	public int getMobilitiesCount(long groupId) {

		return mobilityPersistence.countByGroupId(groupId);
	}

	protected void validate(
			Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String listingImage = listingImageMap.get(locale);

		if (Validator.isNull(listingImage)) {
			throw new MobilityListingImageException();
		}

		String listingTitle = listingTitleMap.get(locale);

		if (Validator.isNull(listingTitle)) {
			throw new MobilityListingTitleException();
		}

		String listingDescription = listingDescriptionMap.get(locale);

		if (Validator.isNull(listingDescription)) {
			throw new MobilityListingDescriptionException();
		}

	}

}