/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package mobilities.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import mobilities.model.Mobility;

/**
 * The cache model class for representing Mobility in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MobilityCacheModel
	implements CacheModel<Mobility>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MobilityCacheModel)) {
			return false;
		}

		MobilityCacheModel mobilityCacheModel = (MobilityCacheModel)object;

		if (mobilityId == mobilityCacheModel.mobilityId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, mobilityId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", mobilityId=");
		sb.append(mobilityId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", listingTitle=");
		sb.append(listingTitle);
		sb.append(", listingDescription=");
		sb.append(listingDescription);
		sb.append(", buttonLabel=");
		sb.append(buttonLabel);
		sb.append(", buttonLink=");
		sb.append(buttonLink);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Mobility toEntityModel() {
		MobilityImpl mobilityImpl = new MobilityImpl();

		if (uuid == null) {
			mobilityImpl.setUuid("");
		}
		else {
			mobilityImpl.setUuid(uuid);
		}

		mobilityImpl.setMobilityId(mobilityId);
		mobilityImpl.setGroupId(groupId);
		mobilityImpl.setCompanyId(companyId);
		mobilityImpl.setUserId(userId);

		if (userName == null) {
			mobilityImpl.setUserName("");
		}
		else {
			mobilityImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			mobilityImpl.setCreateDate(null);
		}
		else {
			mobilityImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			mobilityImpl.setModifiedDate(null);
		}
		else {
			mobilityImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (listingImage == null) {
			mobilityImpl.setListingImage("");
		}
		else {
			mobilityImpl.setListingImage(listingImage);
		}

		if (listingTitle == null) {
			mobilityImpl.setListingTitle("");
		}
		else {
			mobilityImpl.setListingTitle(listingTitle);
		}

		if (listingDescription == null) {
			mobilityImpl.setListingDescription("");
		}
		else {
			mobilityImpl.setListingDescription(listingDescription);
		}

		if (buttonLabel == null) {
			mobilityImpl.setButtonLabel("");
		}
		else {
			mobilityImpl.setButtonLabel(buttonLabel);
		}

		if (buttonLink == null) {
			mobilityImpl.setButtonLink("");
		}
		else {
			mobilityImpl.setButtonLink(buttonLink);
		}

		mobilityImpl.resetOriginalValues();

		return mobilityImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		mobilityId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		listingImage = objectInput.readUTF();
		listingTitle = objectInput.readUTF();
		listingDescription = objectInput.readUTF();
		buttonLabel = objectInput.readUTF();
		buttonLink = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(mobilityId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (listingTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingTitle);
		}

		if (listingDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingDescription);
		}

		if (buttonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLabel);
		}

		if (buttonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLink);
		}
	}

	public String uuid;
	public long mobilityId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String listingImage;
	public String listingTitle;
	public String listingDescription;
	public String buttonLabel;
	public String buttonLink;

}