/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link StoryService}.
 *
 * @author Brian Wing Shun Chan
 * @see StoryService
 * @generated
 */
public class StoryServiceWrapper
	implements ServiceWrapper<StoryService>, StoryService {

	public StoryServiceWrapper(StoryService storyService) {
		_storyService = storyService;
	}

	@Override
	public java.util.List<stories.model.Story> findByTitle_Type(
		String title, int type) {

		return _storyService.findByTitle_Type(title, type);
	}

	@Override
	public java.util.List<stories.model.Story> findByTitle_Type(
		String title, int type, int start, int end) {

		return _storyService.findByTitle_Type(title, type, start, end);
	}

	@Override
	public java.util.List<stories.model.Story> findByTitle_Type(
		String title, int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		return _storyService.findByTitle_Type(title, type, start, end, obc);
	}

	@Override
	public int findByTitle_TypeCount(String title, int type) {
		return _storyService.findByTitle_TypeCount(title, type);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _storyService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<stories.model.Story> getStories(long groupId) {
		return _storyService.getStories(groupId);
	}

	@Override
	public java.util.List<stories.model.Story> getStories(
		long groupId, int start, int end) {

		return _storyService.getStories(groupId, start, end);
	}

	@Override
	public java.util.List<stories.model.Story> getStories(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		return _storyService.getStories(groupId, start, end, obc);
	}

	@Override
	public java.util.List<stories.model.Story> getStoriesByCategory(
		long categoryId) {

		return _storyService.getStoriesByCategory(categoryId);
	}

	@Override
	public java.util.List<stories.model.Story> getStoriesByCategory(
		long categoryId, int start, int end) {

		return _storyService.getStoriesByCategory(categoryId, start, end);
	}

	@Override
	public java.util.List<stories.model.Story> getStoriesByCategory(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		return _storyService.getStoriesByCategory(categoryId, start, end, obc);
	}

	@Override
	public int getStoriesByCategoryCount(long categoryId) {
		return _storyService.getStoriesByCategoryCount(categoryId);
	}

	@Override
	public java.util.List<stories.model.Story> getStoriesByType(int type) {
		return _storyService.getStoriesByType(type);
	}

	@Override
	public java.util.List<stories.model.Story> getStoriesByType(
		int type, int start, int end) {

		return _storyService.getStoriesByType(type, start, end);
	}

	@Override
	public java.util.List<stories.model.Story> getStoriesByType(
		int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		return _storyService.getStoriesByType(type, start, end, obc);
	}

	@Override
	public int getStoriesByTypeCount(int type) {
		return _storyService.getStoriesByTypeCount(type);
	}

	@Override
	public int getStoriesCount(long groupId) {
		return _storyService.getStoriesCount(groupId);
	}

	@Override
	public stories.model.Story getStoryById(long storyId)
		throws stories.exception.NoSuchStoryException {

		return _storyService.getStoryById(storyId);
	}

	@Override
	public StoryService getWrappedService() {
		return _storyService;
	}

	@Override
	public void setWrappedService(StoryService storyService) {
		_storyService = storyService;
	}

	private StoryService _storyService;

}