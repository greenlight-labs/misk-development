/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

import stories.model.Story;

/**
 * Provides the remote service utility for Story. This utility wraps
 * <code>stories.service.impl.StoryServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see StoryService
 * @generated
 */
public class StoryServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>stories.service.impl.StoryServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static List<Story> findByTitle_Type(String title, int type) {
		return getService().findByTitle_Type(title, type);
	}

	public static List<Story> findByTitle_Type(
		String title, int type, int start, int end) {

		return getService().findByTitle_Type(title, type, start, end);
	}

	public static List<Story> findByTitle_Type(
		String title, int type, int start, int end,
		OrderByComparator<Story> obc) {

		return getService().findByTitle_Type(title, type, start, end, obc);
	}

	public static int findByTitle_TypeCount(String title, int type) {
		return getService().findByTitle_TypeCount(title, type);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static List<Story> getStories(long groupId) {
		return getService().getStories(groupId);
	}

	public static List<Story> getStories(long groupId, int start, int end) {
		return getService().getStories(groupId, start, end);
	}

	public static List<Story> getStories(
		long groupId, int start, int end, OrderByComparator<Story> obc) {

		return getService().getStories(groupId, start, end, obc);
	}

	public static List<Story> getStoriesByCategory(long categoryId) {
		return getService().getStoriesByCategory(categoryId);
	}

	public static List<Story> getStoriesByCategory(
		long categoryId, int start, int end) {

		return getService().getStoriesByCategory(categoryId, start, end);
	}

	public static List<Story> getStoriesByCategory(
		long categoryId, int start, int end, OrderByComparator<Story> obc) {

		return getService().getStoriesByCategory(categoryId, start, end, obc);
	}

	public static int getStoriesByCategoryCount(long categoryId) {
		return getService().getStoriesByCategoryCount(categoryId);
	}

	public static List<Story> getStoriesByType(int type) {
		return getService().getStoriesByType(type);
	}

	public static List<Story> getStoriesByType(int type, int start, int end) {
		return getService().getStoriesByType(type, start, end);
	}

	public static List<Story> getStoriesByType(
		int type, int start, int end, OrderByComparator<Story> obc) {

		return getService().getStoriesByType(type, start, end, obc);
	}

	public static int getStoriesByTypeCount(int type) {
		return getService().getStoriesByTypeCount(type);
	}

	public static int getStoriesCount(long groupId) {
		return getService().getStoriesCount(groupId);
	}

	public static Story getStoryById(long storyId)
		throws stories.exception.NoSuchStoryException {

		return getService().getStoryById(storyId);
	}

	public static StoryService getService() {
		return _service;
	}

	private static volatile StoryService _service;

}