/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import stories.model.Story;

/**
 * Provides the local service utility for Story. This utility wraps
 * <code>stories.service.impl.StoryLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see StoryLocalService
 * @generated
 */
public class StoryLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>stories.service.impl.StoryLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Story addStory(
			long userId, long categoryId, int type,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> smallImageMap,
			Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate,
			Map<java.util.Locale, String> descriptionField1Map,
			Map<java.util.Locale, String> detailImageMap,
			Map<java.util.Locale, String> pdfFileMap, long orderNo,
			boolean sendPushNotification,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addStory(
			userId, categoryId, type, titleMap, smallImageMap, bigImageMap,
			displayDate, descriptionField1Map, detailImageMap, pdfFileMap,
			orderNo, sendPushNotification, serviceContext);
	}

	/**
	 * Adds the story to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param story the story
	 * @return the story that was added
	 */
	public static Story addStory(Story story) {
		return getService().addStory(story);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new story with the primary key. Does not add the story to the database.
	 *
	 * @param storyId the primary key for the new story
	 * @return the new story
	 */
	public static Story createStory(long storyId) {
		return getService().createStory(storyId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the story with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param storyId the primary key of the story
	 * @return the story that was removed
	 * @throws PortalException if a story with the primary key could not be found
	 */
	public static Story deleteStory(long storyId) throws PortalException {
		return getService().deleteStory(storyId);
	}

	public static Story deleteStory(
			long storyId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteStory(storyId, serviceContext);
	}

	/**
	 * Deletes the story from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param story the story
	 * @return the story that was removed
	 */
	public static Story deleteStory(Story story) {
		return getService().deleteStory(story);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>stories.model.impl.StoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>stories.model.impl.StoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Story fetchStory(long storyId) {
		return getService().fetchStory(storyId);
	}

	/**
	 * Returns the story matching the UUID and group.
	 *
	 * @param uuid the story's UUID
	 * @param groupId the primary key of the group
	 * @return the matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchStoryByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchStoryByUuidAndGroupId(uuid, groupId);
	}

	public static List<Story> findByTitle(String title) {
		return getService().findByTitle(title);
	}

	public static List<Story> findByTitle(String title, int start, int end) {
		return getService().findByTitle(title, start, end);
	}

	public static List<Story> findByTitle(
		String title, int start, int end, OrderByComparator<Story> obc) {

		return getService().findByTitle(title, start, end, obc);
	}

	public static int findByTitleCount(String title) {
		return getService().findByTitleCount(title);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>stories.model.impl.StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of stories
	 */
	public static List<Story> getStories(int start, int end) {
		return getService().getStories(start, end);
	}

	public static List<Story> getStories(long groupId) {
		return getService().getStories(groupId);
	}

	public static List<Story> getStories(long groupId, int start, int end) {
		return getService().getStories(groupId, start, end);
	}

	public static List<Story> getStories(
		long groupId, int start, int end, OrderByComparator<Story> obc) {

		return getService().getStories(groupId, start, end, obc);
	}

	/**
	 * Returns all the stories matching the UUID and company.
	 *
	 * @param uuid the UUID of the stories
	 * @param companyId the primary key of the company
	 * @return the matching stories, or an empty list if no matches were found
	 */
	public static List<Story> getStoriesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getStoriesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of stories matching the UUID and company.
	 *
	 * @param uuid the UUID of the stories
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching stories, or an empty list if no matches were found
	 */
	public static List<Story> getStoriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getService().getStoriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of stories.
	 *
	 * @return the number of stories
	 */
	public static int getStoriesCount() {
		return getService().getStoriesCount();
	}

	public static int getStoriesCount(long groupId) {
		return getService().getStoriesCount(groupId);
	}

	/**
	 * Returns the story with the primary key.
	 *
	 * @param storyId the primary key of the story
	 * @return the story
	 * @throws PortalException if a story with the primary key could not be found
	 */
	public static Story getStory(long storyId) throws PortalException {
		return getService().getStory(storyId);
	}

	/**
	 * Returns the story matching the UUID and group.
	 *
	 * @param uuid the story's UUID
	 * @param groupId the primary key of the group
	 * @return the matching story
	 * @throws PortalException if a matching story could not be found
	 */
	public static Story getStoryByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getStoryByUuidAndGroupId(uuid, groupId);
	}

	public static List<Story> searchStories(
		com.liferay.portal.kernel.search.SearchContext searchContext) {

		return getService().searchStories(searchContext);
	}

	public static Story updateStory(
			long userId, long storyId, long categoryId, int type,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> smallImageMap,
			Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate,
			Map<java.util.Locale, String> descriptionField1Map,
			Map<java.util.Locale, String> detailImageMap,
			Map<java.util.Locale, String> pdfFileMap, long orderNo,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateStory(
			userId, storyId, categoryId, type, titleMap, smallImageMap,
			bigImageMap, displayDate, descriptionField1Map, detailImageMap,
			pdfFileMap, orderNo, serviceContext);
	}

	/**
	 * Updates the story in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param story the story
	 * @return the story that was updated
	 */
	public static Story updateStory(Story story) {
		return getService().updateStory(story);
	}

	public static StoryLocalService getService() {
		return _service;
	}

	private static volatile StoryLocalService _service;

}