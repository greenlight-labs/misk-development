/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import stories.model.Story;

/**
 * The persistence utility for the story service. This utility wraps <code>stories.service.persistence.impl.StoryPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StoryPersistence
 * @generated
 */
public class StoryUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Story story) {
		getPersistence().clearCache(story);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Story> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Story> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Story> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Story> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Story update(Story story) {
		return getPersistence().update(story);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Story update(Story story, ServiceContext serviceContext) {
		return getPersistence().update(story, serviceContext);
	}

	/**
	 * Returns all the stories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching stories
	 */
	public static List<Story> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the stories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public static List<Story> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the stories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Story> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByUuid_First(
			String uuid, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByUuid_First(
		String uuid, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByUuid_Last(
			String uuid, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByUuid_Last(
		String uuid, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the stories before and after the current story in the ordered set where uuid = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story[] findByUuid_PrevAndNext(
			long storyId, String uuid,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByUuid_PrevAndNext(
			storyId, uuid, orderByComparator);
	}

	/**
	 * Removes all the stories where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of stories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching stories
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the story where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchStoryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByUUID_G(String uuid, long groupId)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the story where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the story where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the story where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the story that was removed
	 */
	public static Story removeByUUID_G(String uuid, long groupId)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of stories where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching stories
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching stories
	 */
	public static List<Story> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public static List<Story> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Story> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the stories before and after the current story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story[] findByUuid_C_PrevAndNext(
			long storyId, String uuid, long companyId,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByUuid_C_PrevAndNext(
			storyId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the stories where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of stories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching stories
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the stories where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching stories
	 */
	public static List<Story> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the stories where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public static List<Story> findByGroupId(long groupId, int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the stories where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Story> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByGroupId_First(
			long groupId, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByGroupId_First(
		long groupId, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByGroupId_Last(
			long groupId, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByGroupId_Last(
		long groupId, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the stories before and after the current story in the ordered set where groupId = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story[] findByGroupId_PrevAndNext(
			long storyId, long groupId,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByGroupId_PrevAndNext(
			storyId, groupId, orderByComparator);
	}

	/**
	 * Removes all the stories where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of stories where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching stories
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the stories where type = &#63;.
	 *
	 * @param type the type
	 * @return the matching stories
	 */
	public static List<Story> findByType(int type) {
		return getPersistence().findByType(type);
	}

	/**
	 * Returns a range of all the stories where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public static List<Story> findByType(int type, int start, int end) {
		return getPersistence().findByType(type, start, end);
	}

	/**
	 * Returns an ordered range of all the stories where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByType(
		int type, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findByType(type, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByType(
		int type, int start, int end,
		OrderByComparator<Story> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByType(
			type, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByType_First(
			int type, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByType_First(type, orderByComparator);
	}

	/**
	 * Returns the first story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByType_First(
		int type, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByType_First(type, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByType_Last(
			int type, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByType_Last(type, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByType_Last(
		int type, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByType_Last(type, orderByComparator);
	}

	/**
	 * Returns the stories before and after the current story in the ordered set where type = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story[] findByType_PrevAndNext(
			long storyId, int type, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByType_PrevAndNext(
			storyId, type, orderByComparator);
	}

	/**
	 * Removes all the stories where type = &#63; from the database.
	 *
	 * @param type the type
	 */
	public static void removeByType(int type) {
		getPersistence().removeByType(type);
	}

	/**
	 * Returns the number of stories where type = &#63;.
	 *
	 * @param type the type
	 * @return the number of matching stories
	 */
	public static int countByType(int type) {
		return getPersistence().countByType(type);
	}

	/**
	 * Returns all the stories where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching stories
	 */
	public static List<Story> findByCategoryId(long categoryId) {
		return getPersistence().findByCategoryId(categoryId);
	}

	/**
	 * Returns a range of all the stories where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public static List<Story> findByCategoryId(
		long categoryId, int start, int end) {

		return getPersistence().findByCategoryId(categoryId, start, end);
	}

	/**
	 * Returns an ordered range of all the stories where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Story> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByCategoryId_First(
			long categoryId, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the first story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByCategoryId_First(
		long categoryId, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByCategoryId_Last(
			long categoryId, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByCategoryId_Last(
		long categoryId, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the stories before and after the current story in the ordered set where categoryId = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story[] findByCategoryId_PrevAndNext(
			long storyId, long categoryId,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByCategoryId_PrevAndNext(
			storyId, categoryId, orderByComparator);
	}

	/**
	 * Removes all the stories where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public static void removeByCategoryId(long categoryId) {
		getPersistence().removeByCategoryId(categoryId);
	}

	/**
	 * Returns the number of stories where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching stories
	 */
	public static int countByCategoryId(long categoryId) {
		return getPersistence().countByCategoryId(categoryId);
	}

	/**
	 * Returns all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @return the matching stories
	 */
	public static List<Story> findByTitle_Type(String title, int type) {
		return getPersistence().findByTitle_Type(title, type);
	}

	/**
	 * Returns a range of all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public static List<Story> findByTitle_Type(
		String title, int type, int start, int end) {

		return getPersistence().findByTitle_Type(title, type, start, end);
	}

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByTitle_Type(
		String title, int type, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findByTitle_Type(
			title, type, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByTitle_Type(
		String title, int type, int start, int end,
		OrderByComparator<Story> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByTitle_Type(
			title, type, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByTitle_Type_First(
			String title, int type, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByTitle_Type_First(
			title, type, orderByComparator);
	}

	/**
	 * Returns the first story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByTitle_Type_First(
		String title, int type, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByTitle_Type_First(
			title, type, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByTitle_Type_Last(
			String title, int type, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByTitle_Type_Last(
			title, type, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByTitle_Type_Last(
		String title, int type, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByTitle_Type_Last(
			title, type, orderByComparator);
	}

	/**
	 * Returns the stories before and after the current story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story[] findByTitle_Type_PrevAndNext(
			long storyId, String title, int type,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByTitle_Type_PrevAndNext(
			storyId, title, type, orderByComparator);
	}

	/**
	 * Removes all the stories where title LIKE &#63; and type = &#63; from the database.
	 *
	 * @param title the title
	 * @param type the type
	 */
	public static void removeByTitle_Type(String title, int type) {
		getPersistence().removeByTitle_Type(title, type);
	}

	/**
	 * Returns the number of stories where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @return the number of matching stories
	 */
	public static int countByTitle_Type(String title, int type) {
		return getPersistence().countByTitle_Type(title, type);
	}

	/**
	 * Returns all the stories where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the matching stories
	 */
	public static List<Story> findByTitle(String title) {
		return getPersistence().findByTitle(title);
	}

	/**
	 * Returns a range of all the stories where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public static List<Story> findByTitle(String title, int start, int end) {
		return getPersistence().findByTitle(title, start, end);
	}

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByTitle(
		String title, int start, int end,
		OrderByComparator<Story> orderByComparator) {

		return getPersistence().findByTitle(
			title, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public static List<Story> findByTitle(
		String title, int start, int end,
		OrderByComparator<Story> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByTitle(
			title, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByTitle_First(
			String title, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByTitle_First(title, orderByComparator);
	}

	/**
	 * Returns the first story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByTitle_First(
		String title, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByTitle_First(title, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public static Story findByTitle_Last(
			String title, OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByTitle_Last(title, orderByComparator);
	}

	/**
	 * Returns the last story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public static Story fetchByTitle_Last(
		String title, OrderByComparator<Story> orderByComparator) {

		return getPersistence().fetchByTitle_Last(title, orderByComparator);
	}

	/**
	 * Returns the stories before and after the current story in the ordered set where title LIKE &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story[] findByTitle_PrevAndNext(
			long storyId, String title,
			OrderByComparator<Story> orderByComparator)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByTitle_PrevAndNext(
			storyId, title, orderByComparator);
	}

	/**
	 * Removes all the stories where title LIKE &#63; from the database.
	 *
	 * @param title the title
	 */
	public static void removeByTitle(String title) {
		getPersistence().removeByTitle(title);
	}

	/**
	 * Returns the number of stories where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the number of matching stories
	 */
	public static int countByTitle(String title) {
		return getPersistence().countByTitle(title);
	}

	/**
	 * Caches the story in the entity cache if it is enabled.
	 *
	 * @param story the story
	 */
	public static void cacheResult(Story story) {
		getPersistence().cacheResult(story);
	}

	/**
	 * Caches the stories in the entity cache if it is enabled.
	 *
	 * @param stories the stories
	 */
	public static void cacheResult(List<Story> stories) {
		getPersistence().cacheResult(stories);
	}

	/**
	 * Creates a new story with the primary key. Does not add the story to the database.
	 *
	 * @param storyId the primary key for the new story
	 * @return the new story
	 */
	public static Story create(long storyId) {
		return getPersistence().create(storyId);
	}

	/**
	 * Removes the story with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param storyId the primary key of the story
	 * @return the story that was removed
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story remove(long storyId)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().remove(storyId);
	}

	public static Story updateImpl(Story story) {
		return getPersistence().updateImpl(story);
	}

	/**
	 * Returns the story with the primary key or throws a <code>NoSuchStoryException</code> if it could not be found.
	 *
	 * @param storyId the primary key of the story
	 * @return the story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public static Story findByPrimaryKey(long storyId)
		throws stories.exception.NoSuchStoryException {

		return getPersistence().findByPrimaryKey(storyId);
	}

	/**
	 * Returns the story with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param storyId the primary key of the story
	 * @return the story, or <code>null</code> if a story with the primary key could not be found
	 */
	public static Story fetchByPrimaryKey(long storyId) {
		return getPersistence().fetchByPrimaryKey(storyId);
	}

	/**
	 * Returns all the stories.
	 *
	 * @return the stories
	 */
	public static List<Story> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of stories
	 */
	public static List<Story> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of stories
	 */
	public static List<Story> findAll(
		int start, int end, OrderByComparator<Story> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of stories
	 */
	public static List<Story> findAll(
		int start, int end, OrderByComparator<Story> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the stories from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of stories.
	 *
	 * @return the number of stories
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static StoryPersistence getPersistence() {
		return _persistence;
	}

	private static volatile StoryPersistence _persistence;

}