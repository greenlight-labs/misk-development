/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

import stories.exception.NoSuchStoryException;

import stories.model.Story;

/**
 * The persistence interface for the story service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StoryUtil
 * @generated
 */
@ProviderType
public interface StoryPersistence extends BasePersistence<Story> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link StoryUtil} to access the story persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the stories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching stories
	 */
	public java.util.List<Story> findByUuid(String uuid);

	/**
	 * Returns a range of all the stories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public java.util.List<Story> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the stories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the first story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the last story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the last story in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the stories before and after the current story in the ordered set where uuid = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story[] findByUuid_PrevAndNext(
			long storyId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Removes all the stories where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of stories where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching stories
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the story where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchStoryException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByUUID_G(String uuid, long groupId)
		throws NoSuchStoryException;

	/**
	 * Returns the story where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the story where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the story where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the story that was removed
	 */
	public Story removeByUUID_G(String uuid, long groupId)
		throws NoSuchStoryException;

	/**
	 * Returns the number of stories where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching stories
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching stories
	 */
	public java.util.List<Story> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public java.util.List<Story> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the first story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the last story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the last story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the stories before and after the current story in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story[] findByUuid_C_PrevAndNext(
			long storyId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Removes all the stories where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of stories where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching stories
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the stories where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching stories
	 */
	public java.util.List<Story> findByGroupId(long groupId);

	/**
	 * Returns a range of all the stories where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public java.util.List<Story> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the stories where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the first story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the last story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the last story in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the stories before and after the current story in the ordered set where groupId = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story[] findByGroupId_PrevAndNext(
			long storyId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Removes all the stories where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of stories where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching stories
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the stories where type = &#63;.
	 *
	 * @param type the type
	 * @return the matching stories
	 */
	public java.util.List<Story> findByType(int type);

	/**
	 * Returns a range of all the stories where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public java.util.List<Story> findByType(int type, int start, int end);

	/**
	 * Returns an ordered range of all the stories where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByType(
		int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByType(
		int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByType_First(
			int type,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the first story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByType_First(
		int type,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the last story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByType_Last(
			int type,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the last story in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByType_Last(
		int type,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the stories before and after the current story in the ordered set where type = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story[] findByType_PrevAndNext(
			long storyId, int type,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Removes all the stories where type = &#63; from the database.
	 *
	 * @param type the type
	 */
	public void removeByType(int type);

	/**
	 * Returns the number of stories where type = &#63;.
	 *
	 * @param type the type
	 * @return the number of matching stories
	 */
	public int countByType(int type);

	/**
	 * Returns all the stories where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching stories
	 */
	public java.util.List<Story> findByCategoryId(long categoryId);

	/**
	 * Returns a range of all the stories where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public java.util.List<Story> findByCategoryId(
		long categoryId, int start, int end);

	/**
	 * Returns an ordered range of all the stories where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByCategoryId_First(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the first story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByCategoryId_First(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the last story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByCategoryId_Last(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the last story in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByCategoryId_Last(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the stories before and after the current story in the ordered set where categoryId = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story[] findByCategoryId_PrevAndNext(
			long storyId, long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Removes all the stories where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public void removeByCategoryId(long categoryId);

	/**
	 * Returns the number of stories where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching stories
	 */
	public int countByCategoryId(long categoryId);

	/**
	 * Returns all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @return the matching stories
	 */
	public java.util.List<Story> findByTitle_Type(String title, int type);

	/**
	 * Returns a range of all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public java.util.List<Story> findByTitle_Type(
		String title, int type, int start, int end);

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByTitle_Type(
		String title, int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63; and type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param type the type
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByTitle_Type(
		String title, int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByTitle_Type_First(
			String title, int type,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the first story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByTitle_Type_First(
		String title, int type,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the last story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByTitle_Type_Last(
			String title, int type,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the last story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByTitle_Type_Last(
		String title, int type,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the stories before and after the current story in the ordered set where title LIKE &#63; and type = &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param title the title
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story[] findByTitle_Type_PrevAndNext(
			long storyId, String title, int type,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Removes all the stories where title LIKE &#63; and type = &#63; from the database.
	 *
	 * @param title the title
	 * @param type the type
	 */
	public void removeByTitle_Type(String title, int type);

	/**
	 * Returns the number of stories where title LIKE &#63; and type = &#63;.
	 *
	 * @param title the title
	 * @param type the type
	 * @return the number of matching stories
	 */
	public int countByTitle_Type(String title, int type);

	/**
	 * Returns all the stories where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the matching stories
	 */
	public java.util.List<Story> findByTitle(String title);

	/**
	 * Returns a range of all the stories where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of matching stories
	 */
	public java.util.List<Story> findByTitle(String title, int start, int end);

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching stories
	 */
	public java.util.List<Story> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByTitle_First(
			String title,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the first story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByTitle_First(
		String title,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the last story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story
	 * @throws NoSuchStoryException if a matching story could not be found
	 */
	public Story findByTitle_Last(
			String title,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Returns the last story in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching story, or <code>null</code> if a matching story could not be found
	 */
	public Story fetchByTitle_Last(
		String title,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns the stories before and after the current story in the ordered set where title LIKE &#63;.
	 *
	 * @param storyId the primary key of the current story
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story[] findByTitle_PrevAndNext(
			long storyId, String title,
			com.liferay.portal.kernel.util.OrderByComparator<Story>
				orderByComparator)
		throws NoSuchStoryException;

	/**
	 * Removes all the stories where title LIKE &#63; from the database.
	 *
	 * @param title the title
	 */
	public void removeByTitle(String title);

	/**
	 * Returns the number of stories where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the number of matching stories
	 */
	public int countByTitle(String title);

	/**
	 * Caches the story in the entity cache if it is enabled.
	 *
	 * @param story the story
	 */
	public void cacheResult(Story story);

	/**
	 * Caches the stories in the entity cache if it is enabled.
	 *
	 * @param stories the stories
	 */
	public void cacheResult(java.util.List<Story> stories);

	/**
	 * Creates a new story with the primary key. Does not add the story to the database.
	 *
	 * @param storyId the primary key for the new story
	 * @return the new story
	 */
	public Story create(long storyId);

	/**
	 * Removes the story with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param storyId the primary key of the story
	 * @return the story that was removed
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story remove(long storyId) throws NoSuchStoryException;

	public Story updateImpl(Story story);

	/**
	 * Returns the story with the primary key or throws a <code>NoSuchStoryException</code> if it could not be found.
	 *
	 * @param storyId the primary key of the story
	 * @return the story
	 * @throws NoSuchStoryException if a story with the primary key could not be found
	 */
	public Story findByPrimaryKey(long storyId) throws NoSuchStoryException;

	/**
	 * Returns the story with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param storyId the primary key of the story
	 * @return the story, or <code>null</code> if a story with the primary key could not be found
	 */
	public Story fetchByPrimaryKey(long storyId);

	/**
	 * Returns all the stories.
	 *
	 * @return the stories
	 */
	public java.util.List<Story> findAll();

	/**
	 * Returns a range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of stories
	 */
	public java.util.List<Story> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of stories
	 */
	public java.util.List<Story> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator);

	/**
	 * Returns an ordered range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of stories
	 */
	public java.util.List<Story> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Story>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the stories from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of stories.
	 *
	 * @return the number of stories
	 */
	public int countAll();

}