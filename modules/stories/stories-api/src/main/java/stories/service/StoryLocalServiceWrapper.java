/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link StoryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see StoryLocalService
 * @generated
 */
public class StoryLocalServiceWrapper
	implements ServiceWrapper<StoryLocalService>, StoryLocalService {

	public StoryLocalServiceWrapper(StoryLocalService storyLocalService) {
		_storyLocalService = storyLocalService;
	}

	@Override
	public stories.model.Story addStory(
			long userId, long categoryId, int type,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> smallImageMap,
			java.util.Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate,
			java.util.Map<java.util.Locale, String> descriptionField1Map,
			java.util.Map<java.util.Locale, String> detailImageMap,
			java.util.Map<java.util.Locale, String> pdfFileMap, long orderNo,
			boolean sendPushNotification,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _storyLocalService.addStory(
			userId, categoryId, type, titleMap, smallImageMap, bigImageMap,
			displayDate, descriptionField1Map, detailImageMap, pdfFileMap,
			orderNo, sendPushNotification, serviceContext);
	}

	/**
	 * Adds the story to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param story the story
	 * @return the story that was added
	 */
	@Override
	public stories.model.Story addStory(stories.model.Story story) {
		return _storyLocalService.addStory(story);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _storyLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new story with the primary key. Does not add the story to the database.
	 *
	 * @param storyId the primary key for the new story
	 * @return the new story
	 */
	@Override
	public stories.model.Story createStory(long storyId) {
		return _storyLocalService.createStory(storyId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _storyLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the story with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param storyId the primary key of the story
	 * @return the story that was removed
	 * @throws PortalException if a story with the primary key could not be found
	 */
	@Override
	public stories.model.Story deleteStory(long storyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _storyLocalService.deleteStory(storyId);
	}

	@Override
	public stories.model.Story deleteStory(
			long storyId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _storyLocalService.deleteStory(storyId, serviceContext);
	}

	/**
	 * Deletes the story from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param story the story
	 * @return the story that was removed
	 */
	@Override
	public stories.model.Story deleteStory(stories.model.Story story) {
		return _storyLocalService.deleteStory(story);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _storyLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _storyLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>stories.model.impl.StoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _storyLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>stories.model.impl.StoryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _storyLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _storyLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _storyLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public stories.model.Story fetchStory(long storyId) {
		return _storyLocalService.fetchStory(storyId);
	}

	/**
	 * Returns the story matching the UUID and group.
	 *
	 * @param uuid the story's UUID
	 * @param groupId the primary key of the group
	 * @return the matching story, or <code>null</code> if a matching story could not be found
	 */
	@Override
	public stories.model.Story fetchStoryByUuidAndGroupId(
		String uuid, long groupId) {

		return _storyLocalService.fetchStoryByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<stories.model.Story> findByTitle(String title) {
		return _storyLocalService.findByTitle(title);
	}

	@Override
	public java.util.List<stories.model.Story> findByTitle(
		String title, int start, int end) {

		return _storyLocalService.findByTitle(title, start, end);
	}

	@Override
	public java.util.List<stories.model.Story> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		return _storyLocalService.findByTitle(title, start, end, obc);
	}

	@Override
	public int findByTitleCount(String title) {
		return _storyLocalService.findByTitleCount(title);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _storyLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _storyLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _storyLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _storyLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _storyLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the stories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>stories.model.impl.StoryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @return the range of stories
	 */
	@Override
	public java.util.List<stories.model.Story> getStories(int start, int end) {
		return _storyLocalService.getStories(start, end);
	}

	@Override
	public java.util.List<stories.model.Story> getStories(long groupId) {
		return _storyLocalService.getStories(groupId);
	}

	@Override
	public java.util.List<stories.model.Story> getStories(
		long groupId, int start, int end) {

		return _storyLocalService.getStories(groupId, start, end);
	}

	@Override
	public java.util.List<stories.model.Story> getStories(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		return _storyLocalService.getStories(groupId, start, end, obc);
	}

	/**
	 * Returns all the stories matching the UUID and company.
	 *
	 * @param uuid the UUID of the stories
	 * @param companyId the primary key of the company
	 * @return the matching stories, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<stories.model.Story> getStoriesByUuidAndCompanyId(
		String uuid, long companyId) {

		return _storyLocalService.getStoriesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of stories matching the UUID and company.
	 *
	 * @param uuid the UUID of the stories
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of stories
	 * @param end the upper bound of the range of stories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching stories, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<stories.model.Story> getStoriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			orderByComparator) {

		return _storyLocalService.getStoriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of stories.
	 *
	 * @return the number of stories
	 */
	@Override
	public int getStoriesCount() {
		return _storyLocalService.getStoriesCount();
	}

	@Override
	public int getStoriesCount(long groupId) {
		return _storyLocalService.getStoriesCount(groupId);
	}

	/**
	 * Returns the story with the primary key.
	 *
	 * @param storyId the primary key of the story
	 * @return the story
	 * @throws PortalException if a story with the primary key could not be found
	 */
	@Override
	public stories.model.Story getStory(long storyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _storyLocalService.getStory(storyId);
	}

	/**
	 * Returns the story matching the UUID and group.
	 *
	 * @param uuid the story's UUID
	 * @param groupId the primary key of the group
	 * @return the matching story
	 * @throws PortalException if a matching story could not be found
	 */
	@Override
	public stories.model.Story getStoryByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _storyLocalService.getStoryByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<stories.model.Story> searchStories(
		com.liferay.portal.kernel.search.SearchContext searchContext) {

		return _storyLocalService.searchStories(searchContext);
	}

	@Override
	public stories.model.Story updateStory(
			long userId, long storyId, long categoryId, int type,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> smallImageMap,
			java.util.Map<java.util.Locale, String> bigImageMap,
			java.util.Date displayDate,
			java.util.Map<java.util.Locale, String> descriptionField1Map,
			java.util.Map<java.util.Locale, String> detailImageMap,
			java.util.Map<java.util.Locale, String> pdfFileMap, long orderNo,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _storyLocalService.updateStory(
			userId, storyId, categoryId, type, titleMap, smallImageMap,
			bigImageMap, displayDate, descriptionField1Map, detailImageMap,
			pdfFileMap, orderNo, serviceContext);
	}

	/**
	 * Updates the story in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect StoryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param story the story
	 * @return the story that was updated
	 */
	@Override
	public stories.model.Story updateStory(stories.model.Story story) {
		return _storyLocalService.updateStory(story);
	}

	@Override
	public StoryLocalService getWrappedService() {
		return _storyLocalService;
	}

	@Override
	public void setWrappedService(StoryLocalService storyLocalService) {
		_storyLocalService = storyLocalService;
	}

	private StoryLocalService _storyLocalService;

}