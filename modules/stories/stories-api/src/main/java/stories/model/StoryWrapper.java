/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Story}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Story
 * @generated
 */
public class StoryWrapper
	extends BaseModelWrapper<Story> implements ModelWrapper<Story>, Story {

	public StoryWrapper(Story story) {
		super(story);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("storyId", getStoryId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("categoryId", getCategoryId());
		attributes.put("orderNo", getOrderNo());
		attributes.put("type", getType());
		attributes.put("title", getTitle());
		attributes.put("smallImage", getSmallImage());
		attributes.put("bigImage", getBigImage());
		attributes.put("displayDate", getDisplayDate());
		attributes.put("descriptionField1", getDescriptionField1());
		attributes.put("detailImage", getDetailImage());
		attributes.put("pdfFile", getPdfFile());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long storyId = (Long)attributes.get("storyId");

		if (storyId != null) {
			setStoryId(storyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long orderNo = (Long)attributes.get("orderNo");

		if (orderNo != null) {
			setOrderNo(orderNo);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String smallImage = (String)attributes.get("smallImage");

		if (smallImage != null) {
			setSmallImage(smallImage);
		}

		String bigImage = (String)attributes.get("bigImage");

		if (bigImage != null) {
			setBigImage(bigImage);
		}

		Date displayDate = (Date)attributes.get("displayDate");

		if (displayDate != null) {
			setDisplayDate(displayDate);
		}

		String descriptionField1 = (String)attributes.get("descriptionField1");

		if (descriptionField1 != null) {
			setDescriptionField1(descriptionField1);
		}

		String detailImage = (String)attributes.get("detailImage");

		if (detailImage != null) {
			setDetailImage(detailImage);
		}

		String pdfFile = (String)attributes.get("pdfFile");

		if (pdfFile != null) {
			setPdfFile(pdfFile);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the big image of this story.
	 *
	 * @return the big image of this story
	 */
	@Override
	public String getBigImage() {
		return model.getBigImage();
	}

	/**
	 * Returns the localized big image of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized big image of this story
	 */
	@Override
	public String getBigImage(java.util.Locale locale) {
		return model.getBigImage(locale);
	}

	/**
	 * Returns the localized big image of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized big image of this story. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBigImage(java.util.Locale locale, boolean useDefault) {
		return model.getBigImage(locale, useDefault);
	}

	/**
	 * Returns the localized big image of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized big image of this story
	 */
	@Override
	public String getBigImage(String languageId) {
		return model.getBigImage(languageId);
	}

	/**
	 * Returns the localized big image of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized big image of this story
	 */
	@Override
	public String getBigImage(String languageId, boolean useDefault) {
		return model.getBigImage(languageId, useDefault);
	}

	@Override
	public String getBigImageCurrentLanguageId() {
		return model.getBigImageCurrentLanguageId();
	}

	@Override
	public String getBigImageCurrentValue() {
		return model.getBigImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized big images of this story.
	 *
	 * @return the locales and localized big images of this story
	 */
	@Override
	public Map<java.util.Locale, String> getBigImageMap() {
		return model.getBigImageMap();
	}

	/**
	 * Returns the category ID of this story.
	 *
	 * @return the category ID of this story
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this story.
	 *
	 * @return the company ID of this story
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this story.
	 *
	 * @return the create date of this story
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description field1 of this story.
	 *
	 * @return the description field1 of this story
	 */
	@Override
	public String getDescriptionField1() {
		return model.getDescriptionField1();
	}

	/**
	 * Returns the localized description field1 of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description field1 of this story
	 */
	@Override
	public String getDescriptionField1(java.util.Locale locale) {
		return model.getDescriptionField1(locale);
	}

	/**
	 * Returns the localized description field1 of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field1 of this story. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescriptionField1(
		java.util.Locale locale, boolean useDefault) {

		return model.getDescriptionField1(locale, useDefault);
	}

	/**
	 * Returns the localized description field1 of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description field1 of this story
	 */
	@Override
	public String getDescriptionField1(String languageId) {
		return model.getDescriptionField1(languageId);
	}

	/**
	 * Returns the localized description field1 of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description field1 of this story
	 */
	@Override
	public String getDescriptionField1(String languageId, boolean useDefault) {
		return model.getDescriptionField1(languageId, useDefault);
	}

	@Override
	public String getDescriptionField1CurrentLanguageId() {
		return model.getDescriptionField1CurrentLanguageId();
	}

	@Override
	public String getDescriptionField1CurrentValue() {
		return model.getDescriptionField1CurrentValue();
	}

	/**
	 * Returns a map of the locales and localized description field1s of this story.
	 *
	 * @return the locales and localized description field1s of this story
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionField1Map() {
		return model.getDescriptionField1Map();
	}

	/**
	 * Returns the detail image of this story.
	 *
	 * @return the detail image of this story
	 */
	@Override
	public String getDetailImage() {
		return model.getDetailImage();
	}

	/**
	 * Returns the localized detail image of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized detail image of this story
	 */
	@Override
	public String getDetailImage(java.util.Locale locale) {
		return model.getDetailImage(locale);
	}

	/**
	 * Returns the localized detail image of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized detail image of this story. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDetailImage(java.util.Locale locale, boolean useDefault) {
		return model.getDetailImage(locale, useDefault);
	}

	/**
	 * Returns the localized detail image of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized detail image of this story
	 */
	@Override
	public String getDetailImage(String languageId) {
		return model.getDetailImage(languageId);
	}

	/**
	 * Returns the localized detail image of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized detail image of this story
	 */
	@Override
	public String getDetailImage(String languageId, boolean useDefault) {
		return model.getDetailImage(languageId, useDefault);
	}

	@Override
	public String getDetailImageCurrentLanguageId() {
		return model.getDetailImageCurrentLanguageId();
	}

	@Override
	public String getDetailImageCurrentValue() {
		return model.getDetailImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized detail images of this story.
	 *
	 * @return the locales and localized detail images of this story
	 */
	@Override
	public Map<java.util.Locale, String> getDetailImageMap() {
		return model.getDetailImageMap();
	}

	/**
	 * Returns the display date of this story.
	 *
	 * @return the display date of this story
	 */
	@Override
	public Date getDisplayDate() {
		return model.getDisplayDate();
	}

	/**
	 * Returns the group ID of this story.
	 *
	 * @return the group ID of this story
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this story.
	 *
	 * @return the modified date of this story
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the order no of this story.
	 *
	 * @return the order no of this story
	 */
	@Override
	public long getOrderNo() {
		return model.getOrderNo();
	}

	/**
	 * Returns the pdf file of this story.
	 *
	 * @return the pdf file of this story
	 */
	@Override
	public String getPdfFile() {
		return model.getPdfFile();
	}

	/**
	 * Returns the localized pdf file of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized pdf file of this story
	 */
	@Override
	public String getPdfFile(java.util.Locale locale) {
		return model.getPdfFile(locale);
	}

	/**
	 * Returns the localized pdf file of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized pdf file of this story. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getPdfFile(java.util.Locale locale, boolean useDefault) {
		return model.getPdfFile(locale, useDefault);
	}

	/**
	 * Returns the localized pdf file of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized pdf file of this story
	 */
	@Override
	public String getPdfFile(String languageId) {
		return model.getPdfFile(languageId);
	}

	/**
	 * Returns the localized pdf file of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized pdf file of this story
	 */
	@Override
	public String getPdfFile(String languageId, boolean useDefault) {
		return model.getPdfFile(languageId, useDefault);
	}

	@Override
	public String getPdfFileCurrentLanguageId() {
		return model.getPdfFileCurrentLanguageId();
	}

	@Override
	public String getPdfFileCurrentValue() {
		return model.getPdfFileCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized pdf files of this story.
	 *
	 * @return the locales and localized pdf files of this story
	 */
	@Override
	public Map<java.util.Locale, String> getPdfFileMap() {
		return model.getPdfFileMap();
	}

	/**
	 * Returns the primary key of this story.
	 *
	 * @return the primary key of this story
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the small image of this story.
	 *
	 * @return the small image of this story
	 */
	@Override
	public String getSmallImage() {
		return model.getSmallImage();
	}

	/**
	 * Returns the localized small image of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized small image of this story
	 */
	@Override
	public String getSmallImage(java.util.Locale locale) {
		return model.getSmallImage(locale);
	}

	/**
	 * Returns the localized small image of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized small image of this story. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSmallImage(java.util.Locale locale, boolean useDefault) {
		return model.getSmallImage(locale, useDefault);
	}

	/**
	 * Returns the localized small image of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized small image of this story
	 */
	@Override
	public String getSmallImage(String languageId) {
		return model.getSmallImage(languageId);
	}

	/**
	 * Returns the localized small image of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized small image of this story
	 */
	@Override
	public String getSmallImage(String languageId, boolean useDefault) {
		return model.getSmallImage(languageId, useDefault);
	}

	@Override
	public String getSmallImageCurrentLanguageId() {
		return model.getSmallImageCurrentLanguageId();
	}

	@Override
	public String getSmallImageCurrentValue() {
		return model.getSmallImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized small images of this story.
	 *
	 * @return the locales and localized small images of this story
	 */
	@Override
	public Map<java.util.Locale, String> getSmallImageMap() {
		return model.getSmallImageMap();
	}

	/**
	 * Returns the story ID of this story.
	 *
	 * @return the story ID of this story
	 */
	@Override
	public long getStoryId() {
		return model.getStoryId();
	}

	/**
	 * Returns the title of this story.
	 *
	 * @return the title of this story
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this story
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this story. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this story in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this story
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this story in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this story
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this story.
	 *
	 * @return the locales and localized titles of this story
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the type of this story.
	 *
	 * @return the type of this story
	 */
	@Override
	public int getType() {
		return model.getType();
	}

	/**
	 * Returns the user ID of this story.
	 *
	 * @return the user ID of this story
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this story.
	 *
	 * @return the user name of this story
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this story.
	 *
	 * @return the user uuid of this story
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this story.
	 *
	 * @return the uuid of this story
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the big image of this story.
	 *
	 * @param bigImage the big image of this story
	 */
	@Override
	public void setBigImage(String bigImage) {
		model.setBigImage(bigImage);
	}

	/**
	 * Sets the localized big image of this story in the language.
	 *
	 * @param bigImage the localized big image of this story
	 * @param locale the locale of the language
	 */
	@Override
	public void setBigImage(String bigImage, java.util.Locale locale) {
		model.setBigImage(bigImage, locale);
	}

	/**
	 * Sets the localized big image of this story in the language, and sets the default locale.
	 *
	 * @param bigImage the localized big image of this story
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBigImage(
		String bigImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBigImage(bigImage, locale, defaultLocale);
	}

	@Override
	public void setBigImageCurrentLanguageId(String languageId) {
		model.setBigImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized big images of this story from the map of locales and localized big images.
	 *
	 * @param bigImageMap the locales and localized big images of this story
	 */
	@Override
	public void setBigImageMap(Map<java.util.Locale, String> bigImageMap) {
		model.setBigImageMap(bigImageMap);
	}

	/**
	 * Sets the localized big images of this story from the map of locales and localized big images, and sets the default locale.
	 *
	 * @param bigImageMap the locales and localized big images of this story
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBigImageMap(
		Map<java.util.Locale, String> bigImageMap,
		java.util.Locale defaultLocale) {

		model.setBigImageMap(bigImageMap, defaultLocale);
	}

	/**
	 * Sets the category ID of this story.
	 *
	 * @param categoryId the category ID of this story
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this story.
	 *
	 * @param companyId the company ID of this story
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this story.
	 *
	 * @param createDate the create date of this story
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description field1 of this story.
	 *
	 * @param descriptionField1 the description field1 of this story
	 */
	@Override
	public void setDescriptionField1(String descriptionField1) {
		model.setDescriptionField1(descriptionField1);
	}

	/**
	 * Sets the localized description field1 of this story in the language.
	 *
	 * @param descriptionField1 the localized description field1 of this story
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescriptionField1(
		String descriptionField1, java.util.Locale locale) {

		model.setDescriptionField1(descriptionField1, locale);
	}

	/**
	 * Sets the localized description field1 of this story in the language, and sets the default locale.
	 *
	 * @param descriptionField1 the localized description field1 of this story
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField1(
		String descriptionField1, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescriptionField1(descriptionField1, locale, defaultLocale);
	}

	@Override
	public void setDescriptionField1CurrentLanguageId(String languageId) {
		model.setDescriptionField1CurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized description field1s of this story from the map of locales and localized description field1s.
	 *
	 * @param descriptionField1Map the locales and localized description field1s of this story
	 */
	@Override
	public void setDescriptionField1Map(
		Map<java.util.Locale, String> descriptionField1Map) {

		model.setDescriptionField1Map(descriptionField1Map);
	}

	/**
	 * Sets the localized description field1s of this story from the map of locales and localized description field1s, and sets the default locale.
	 *
	 * @param descriptionField1Map the locales and localized description field1s of this story
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionField1Map(
		Map<java.util.Locale, String> descriptionField1Map,
		java.util.Locale defaultLocale) {

		model.setDescriptionField1Map(descriptionField1Map, defaultLocale);
	}

	/**
	 * Sets the detail image of this story.
	 *
	 * @param detailImage the detail image of this story
	 */
	@Override
	public void setDetailImage(String detailImage) {
		model.setDetailImage(detailImage);
	}

	/**
	 * Sets the localized detail image of this story in the language.
	 *
	 * @param detailImage the localized detail image of this story
	 * @param locale the locale of the language
	 */
	@Override
	public void setDetailImage(String detailImage, java.util.Locale locale) {
		model.setDetailImage(detailImage, locale);
	}

	/**
	 * Sets the localized detail image of this story in the language, and sets the default locale.
	 *
	 * @param detailImage the localized detail image of this story
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDetailImage(
		String detailImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDetailImage(detailImage, locale, defaultLocale);
	}

	@Override
	public void setDetailImageCurrentLanguageId(String languageId) {
		model.setDetailImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized detail images of this story from the map of locales and localized detail images.
	 *
	 * @param detailImageMap the locales and localized detail images of this story
	 */
	@Override
	public void setDetailImageMap(
		Map<java.util.Locale, String> detailImageMap) {

		model.setDetailImageMap(detailImageMap);
	}

	/**
	 * Sets the localized detail images of this story from the map of locales and localized detail images, and sets the default locale.
	 *
	 * @param detailImageMap the locales and localized detail images of this story
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDetailImageMap(
		Map<java.util.Locale, String> detailImageMap,
		java.util.Locale defaultLocale) {

		model.setDetailImageMap(detailImageMap, defaultLocale);
	}

	/**
	 * Sets the display date of this story.
	 *
	 * @param displayDate the display date of this story
	 */
	@Override
	public void setDisplayDate(Date displayDate) {
		model.setDisplayDate(displayDate);
	}

	/**
	 * Sets the group ID of this story.
	 *
	 * @param groupId the group ID of this story
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this story.
	 *
	 * @param modifiedDate the modified date of this story
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the order no of this story.
	 *
	 * @param orderNo the order no of this story
	 */
	@Override
	public void setOrderNo(long orderNo) {
		model.setOrderNo(orderNo);
	}

	/**
	 * Sets the pdf file of this story.
	 *
	 * @param pdfFile the pdf file of this story
	 */
	@Override
	public void setPdfFile(String pdfFile) {
		model.setPdfFile(pdfFile);
	}

	/**
	 * Sets the localized pdf file of this story in the language.
	 *
	 * @param pdfFile the localized pdf file of this story
	 * @param locale the locale of the language
	 */
	@Override
	public void setPdfFile(String pdfFile, java.util.Locale locale) {
		model.setPdfFile(pdfFile, locale);
	}

	/**
	 * Sets the localized pdf file of this story in the language, and sets the default locale.
	 *
	 * @param pdfFile the localized pdf file of this story
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setPdfFile(
		String pdfFile, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setPdfFile(pdfFile, locale, defaultLocale);
	}

	@Override
	public void setPdfFileCurrentLanguageId(String languageId) {
		model.setPdfFileCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized pdf files of this story from the map of locales and localized pdf files.
	 *
	 * @param pdfFileMap the locales and localized pdf files of this story
	 */
	@Override
	public void setPdfFileMap(Map<java.util.Locale, String> pdfFileMap) {
		model.setPdfFileMap(pdfFileMap);
	}

	/**
	 * Sets the localized pdf files of this story from the map of locales and localized pdf files, and sets the default locale.
	 *
	 * @param pdfFileMap the locales and localized pdf files of this story
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setPdfFileMap(
		Map<java.util.Locale, String> pdfFileMap,
		java.util.Locale defaultLocale) {

		model.setPdfFileMap(pdfFileMap, defaultLocale);
	}

	/**
	 * Sets the primary key of this story.
	 *
	 * @param primaryKey the primary key of this story
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the small image of this story.
	 *
	 * @param smallImage the small image of this story
	 */
	@Override
	public void setSmallImage(String smallImage) {
		model.setSmallImage(smallImage);
	}

	/**
	 * Sets the localized small image of this story in the language.
	 *
	 * @param smallImage the localized small image of this story
	 * @param locale the locale of the language
	 */
	@Override
	public void setSmallImage(String smallImage, java.util.Locale locale) {
		model.setSmallImage(smallImage, locale);
	}

	/**
	 * Sets the localized small image of this story in the language, and sets the default locale.
	 *
	 * @param smallImage the localized small image of this story
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSmallImage(
		String smallImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSmallImage(smallImage, locale, defaultLocale);
	}

	@Override
	public void setSmallImageCurrentLanguageId(String languageId) {
		model.setSmallImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized small images of this story from the map of locales and localized small images.
	 *
	 * @param smallImageMap the locales and localized small images of this story
	 */
	@Override
	public void setSmallImageMap(Map<java.util.Locale, String> smallImageMap) {
		model.setSmallImageMap(smallImageMap);
	}

	/**
	 * Sets the localized small images of this story from the map of locales and localized small images, and sets the default locale.
	 *
	 * @param smallImageMap the locales and localized small images of this story
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSmallImageMap(
		Map<java.util.Locale, String> smallImageMap,
		java.util.Locale defaultLocale) {

		model.setSmallImageMap(smallImageMap, defaultLocale);
	}

	/**
	 * Sets the story ID of this story.
	 *
	 * @param storyId the story ID of this story
	 */
	@Override
	public void setStoryId(long storyId) {
		model.setStoryId(storyId);
	}

	/**
	 * Sets the title of this story.
	 *
	 * @param title the title of this story
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this story in the language.
	 *
	 * @param title the localized title of this story
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this story in the language, and sets the default locale.
	 *
	 * @param title the localized title of this story
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this story from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this story
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this story from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this story
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the type of this story.
	 *
	 * @param type the type of this story
	 */
	@Override
	public void setType(int type) {
		model.setType(type);
	}

	/**
	 * Sets the user ID of this story.
	 *
	 * @param userId the user ID of this story
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this story.
	 *
	 * @param userName the user name of this story
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this story.
	 *
	 * @param userUuid the user uuid of this story
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this story.
	 *
	 * @param uuid the uuid of this story
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected StoryWrapper wrap(Story story) {
		return new StoryWrapper(story);
	}

}