/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link stories.service.http.StoryServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class StorySoap implements Serializable {

	public static StorySoap toSoapModel(Story model) {
		StorySoap soapModel = new StorySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setStoryId(model.getStoryId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setOrderNo(model.getOrderNo());
		soapModel.setType(model.getType());
		soapModel.setTitle(model.getTitle());
		soapModel.setSmallImage(model.getSmallImage());
		soapModel.setBigImage(model.getBigImage());
		soapModel.setDisplayDate(model.getDisplayDate());
		soapModel.setDescriptionField1(model.getDescriptionField1());
		soapModel.setDetailImage(model.getDetailImage());
		soapModel.setPdfFile(model.getPdfFile());

		return soapModel;
	}

	public static StorySoap[] toSoapModels(Story[] models) {
		StorySoap[] soapModels = new StorySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static StorySoap[][] toSoapModels(Story[][] models) {
		StorySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new StorySoap[models.length][models[0].length];
		}
		else {
			soapModels = new StorySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static StorySoap[] toSoapModels(List<Story> models) {
		List<StorySoap> soapModels = new ArrayList<StorySoap>(models.size());

		for (Story model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new StorySoap[soapModels.size()]);
	}

	public StorySoap() {
	}

	public long getPrimaryKey() {
		return _storyId;
	}

	public void setPrimaryKey(long pk) {
		setStoryId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getStoryId() {
		return _storyId;
	}

	public void setStoryId(long storyId) {
		_storyId = storyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public long getOrderNo() {
		return _orderNo;
	}

	public void setOrderNo(long orderNo) {
		_orderNo = orderNo;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getSmallImage() {
		return _smallImage;
	}

	public void setSmallImage(String smallImage) {
		_smallImage = smallImage;
	}

	public String getBigImage() {
		return _bigImage;
	}

	public void setBigImage(String bigImage) {
		_bigImage = bigImage;
	}

	public Date getDisplayDate() {
		return _displayDate;
	}

	public void setDisplayDate(Date displayDate) {
		_displayDate = displayDate;
	}

	public String getDescriptionField1() {
		return _descriptionField1;
	}

	public void setDescriptionField1(String descriptionField1) {
		_descriptionField1 = descriptionField1;
	}

	public String getDetailImage() {
		return _detailImage;
	}

	public void setDetailImage(String detailImage) {
		_detailImage = detailImage;
	}

	public String getPdfFile() {
		return _pdfFile;
	}

	public void setPdfFile(String pdfFile) {
		_pdfFile = pdfFile;
	}

	private String _uuid;
	private long _storyId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _categoryId;
	private long _orderNo;
	private int _type;
	private String _title;
	private String _smallImage;
	private String _bigImage;
	private Date _displayDate;
	private String _descriptionField1;
	private String _detailImage;
	private String _pdfFile;

}