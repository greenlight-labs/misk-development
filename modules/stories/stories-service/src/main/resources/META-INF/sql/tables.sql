create table misk_stories (
	uuid_ VARCHAR(75) null,
	storyId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	categoryId LONG,
	orderNo LONG,
	type_ INTEGER,
	title STRING null,
	smallImage STRING null,
	bigImage STRING null,
	displayDate DATE null,
	descriptionField1 TEXT null,
	detailImage STRING null,
	pdfFile STRING null
);

create table misk_stories_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug VARCHAR(75) null
);