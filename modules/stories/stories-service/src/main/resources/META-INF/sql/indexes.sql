create index IX_83A73BBB on misk_stories (categoryId);
create index IX_DE1F829C on misk_stories (groupId);
create index IX_A6CC71B on misk_stories (title[$COLUMN_LENGTH:255$], type_);
create index IX_3F7D4C47 on misk_stories (type_);
create index IX_DA4C0442 on misk_stories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_6C2CB344 on misk_stories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_58916311 on misk_stories_categories (groupId);
create index IX_7786D76D on misk_stories_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_1C8552F on misk_stories_categories (uuid_[$COLUMN_LENGTH:75$], groupId);