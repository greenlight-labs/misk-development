/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service.impl;

import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.util.OrderByComparator;
import org.osgi.service.component.annotations.Component;

import stories.exception.NoSuchStoryException;
import stories.model.Story;
import stories.service.base.StoryServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the story remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>stories.service.StoryService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StoryServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=story",
		"json.web.service.context.path=Story"
	},
	service = AopService.class
)
@AccessControlled(guestAccessEnabled = true)
public class StoryServiceImpl extends StoryServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>stories.service.StoryServiceUtil</code> to access the story remote service.
	 */

	public Story getStoryById(long storyId) throws NoSuchStoryException {

		return storyPersistence.findByPrimaryKey(storyId);
	}

	/* *
	 * get stories by group
	 * */
	public List<Story> getStories(long groupId) {

		return storyPersistence.findByGroupId(groupId);
	}

	public List<Story> getStories(long groupId, int start, int end,
								  OrderByComparator<Story> obc) {

		return storyPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Story> getStories(long groupId, int start, int end) {

		return storyPersistence.findByGroupId(groupId, start, end);
	}

	public int getStoriesCount(long groupId) {

		return storyPersistence.countByGroupId(groupId);
	}

	/* *
	 * get stories by type
	 * */
	public List<Story> getStoriesByType(int type) {

		return storyPersistence.findByType(type);
	}

	public List<Story> getStoriesByType(int type, int start, int end,
								  OrderByComparator<Story> obc) {

		return storyPersistence.findByType(type, start, end, obc);
	}

	public List<Story> getStoriesByType(int type, int start, int end) {

		return storyPersistence.findByType(type, start, end);
	}

	public int getStoriesByTypeCount(int type) {

		return storyPersistence.countByType(type);
	}

	/* *
	 * find stories by title and type
	 * */
	public List<Story> findByTitle_Type(String title, int type) {

		return storyPersistence.findByTitle_Type('%'+title+'%', type);
	}

	public List<Story> findByTitle_Type(String title, int type, int start, int end,
								   OrderByComparator<Story> obc) {

		return storyPersistence.findByTitle_Type('%'+title+'%', type, start, end, obc);
	}

	public List<Story> findByTitle_Type(String title, int type, int start, int end) {

		return storyPersistence.findByTitle_Type('%'+title+'%', type, start, end);
	}

	public int findByTitle_TypeCount(String title, int type) {

		return storyPersistence.countByTitle_Type('%'+title+'%', type);
	}

	/* *
	 * get stories by category
	 * */
	public List<Story> getStoriesByCategory(long categoryId) {

		return storyPersistence.findByCategoryId(categoryId);
	}

	public List<Story> getStoriesByCategory(long categoryId, int start, int end,
										  OrderByComparator<Story> obc) {

		return storyPersistence.findByCategoryId(categoryId, start, end, obc);
	}

	public List<Story> getStoriesByCategory(long categoryId, int start, int end) {

		return storyPersistence.findByCategoryId(categoryId, start, end);
	}

	public int getStoriesByCategoryCount(long categoryId) {

		return storyPersistence.countByCategoryId(categoryId);
	}
}