/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.push.notification.constants.PushNotificationKeys;
import com.push.notification.service.PushNotificationLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import stories.exception.StoryDisplayDateException;
import stories.exception.StoryTitleException;
import stories.exception.StoryTypeException;
import stories.model.Story;
import stories.service.StoryLocalServiceUtil;
import stories.service.base.StoryLocalServiceBaseImpl;
import stories.service.indexer.StoryIndexer;

/**
 * The implementation of the story local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>stories.service.StoryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StoryLocalServiceBaseImpl
 */
@Component(property = "model.class.name=stories.model.Story", service = AopService.class)
public class StoryLocalServiceImpl extends StoryLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>stories.service.StoryLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>stories.service.StoryLocalServiceUtil</code>.
	 */

	public Story addStory(long userId, long categoryId, int type, Map<Locale, String> titleMap,
			Map<Locale, String> smallImageMap, Map<Locale, String> bigImageMap, Date displayDate,
			Map<Locale, String> descriptionField1Map, Map<Locale, String> detailImageMap, Map<Locale, String> pdfFileMap,
			long orderNo, boolean sendPushNotification, ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(type, titleMap, displayDate);

		long storyId = counterLocalService.increment();

		Story story = storyPersistence.create(storyId);
		// default Value of OrderNo to 9999

		if (orderNo == 0) {
			orderNo = 9999;
		}
		story.setUuid(serviceContext.getUuid());
		story.setUserId(userId);
		story.setGroupId(groupId);
		story.setCompanyId(user.getCompanyId());
		story.setUserName(user.getFullName());
		story.setOrderNo(orderNo);
		story.setCreateDate(serviceContext.getCreateDate(now));
		story.setModifiedDate(serviceContext.getModifiedDate(now));
		story.setCategoryId(categoryId);
		story.setType(type);
		story.setTitleMap(titleMap);
		story.setSmallImageMap(smallImageMap);
		story.setBigImageMap(bigImageMap);
		story.setDisplayDate(displayDate);
		story.setDescriptionField1Map(descriptionField1Map);
		story.setDetailImageMap(detailImageMap);
		story.setPdfFileMap(pdfFileMap);
		story.setExpandoBridgeAttributes(serviceContext);

		Story addedEntry = storyPersistence.update(story);
		storyPersistence.clearCache();

		if(sendPushNotification){
			// send item added push notification
			if(Validator.isNotNull(addedEntry)) {
				String notificationBody = "News update: \""+addedEntry.getTitle("en_US")+"\".";
				PushNotificationLocalServiceUtil.saveNotification(PushNotificationKeys.NOTIFICATION_TYPE_NEWS, addedEntry.getStoryId(), addedEntry.getTitle(), notificationBody, null);
			}
		}

		return story;
	}

	public Story updateStory(long userId, long storyId, long categoryId, int type, Map<Locale, String> titleMap,
			Map<Locale, String> smallImageMap, Map<Locale, String> bigImageMap, Date displayDate,
			Map<Locale, String> descriptionField1Map, Map<Locale, String> detailImageMap, Map<Locale, String> pdfFileMap,
			long orderNo,ServiceContext serviceContext)
			throws PortalException, SystemException {

		Date now = new Date();

		validate(type, titleMap, displayDate);

		Story story = getStory(storyId);

		User user = userLocalService.getUser(userId);
		// default Value of OrderNo to 9999

		if (orderNo == 0) {
			orderNo = 9999;
		}
		story.setUserId(userId);
		story.setUserName(user.getFullName());
		story.setModifiedDate(serviceContext.getModifiedDate(now));
		story.setCategoryId(categoryId);
		story.setType(type);
		story.setTitleMap(titleMap);
		story.setOrderNo(orderNo);
		story.setSmallImageMap(smallImageMap);
		story.setBigImageMap(bigImageMap);
		story.setDisplayDate(displayDate);
		story.setDescriptionField1Map(descriptionField1Map);
		story.setDetailImageMap(detailImageMap);
		story.setPdfFileMap(pdfFileMap);
		story.setExpandoBridgeAttributes(serviceContext);

		storyPersistence.update(story);
		storyPersistence.clearCache();

		return story;
	}

	public Story deleteStory(long storyId, ServiceContext serviceContext) throws PortalException, SystemException {

		Story story = getStory(storyId);
		story = deleteStory(story);

		return story;
	}

	public List<Story> getStories(long groupId) {

		return storyPersistence.findByGroupId(groupId);
	}

	public List<Story> getStories(long groupId, int start, int end, OrderByComparator<Story> obc) {

		return storyPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Story> getStories(long groupId, int start, int end) {

		return storyPersistence.findByGroupId(groupId, start, end);
	}

	public int getStoriesCount(long groupId) {

		return storyPersistence.countByGroupId(groupId);
	}

	protected void validate(int type, Map<Locale, String> titleMap, Date displayDate) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		if (Validator.isNull(type)) {
			throw new StoryTypeException();
		}

		String title = titleMap.get(locale);

		if (Validator.isNull(title)) {
			throw new StoryTitleException();
		}
		if (Validator.isNull(displayDate)) {
			throw new StoryDisplayDateException();
		}

	}

	/*
	 * * find stories by title and type
	 */
	public List<Story> findByTitle(String title) {

		return storyPersistence.findByTitle('%' + title + '%');
	}

	public List<Story> findByTitle(String title, int start, int end, OrderByComparator<Story> obc) {

		return storyPersistence.findByTitle('%' + title + '%', start, end, obc);
	}

	public List<Story> findByTitle(String title, int start, int end) {

		return storyPersistence.findByTitle('%' + title + '%', start, end);
	}

	public int findByTitleCount(String title) {

		return storyPersistence.countByTitle('%' + title + '%');
	}

	@Override
	public List<Story> searchStories(SearchContext searchContext) {

		Hits hits;

		List<Story> srotiesList = new ArrayList<>();
		StoryIndexer indexer = (StoryIndexer) IndexerRegistryUtil.getIndexer(Story.class);
		try {

			hits = indexer.search(searchContext);

			for (int i = 0; i < hits.getDocs().length; i++) {
				Document doc = hits.doc(i);

				long storyId = GetterUtil.getLong(doc.get(Field.ENTRY_CLASS_PK));
				Story story = null;
				story = StoryLocalServiceUtil.getStory(storyId);

				srotiesList.add(story);
			}
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}

		return srotiesList;
	}
}