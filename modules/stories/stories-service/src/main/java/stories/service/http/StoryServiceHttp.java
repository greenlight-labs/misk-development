/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import stories.service.StoryServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>StoryServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see StoryServiceSoap
 * @generated
 */
public class StoryServiceHttp {

	public static stories.model.Story getStoryById(
			HttpPrincipal httpPrincipal, long storyId)
		throws stories.exception.NoSuchStoryException {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoryById",
				_getStoryByIdParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey, storyId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						stories.exception.NoSuchStoryException) {

					throw (stories.exception.NoSuchStoryException)exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (stories.model.Story)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStories(
		HttpPrincipal httpPrincipal, long groupId) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStories",
				_getStoriesParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey, groupId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStories(
		HttpPrincipal httpPrincipal, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStories",
				_getStoriesParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, groupId, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStories(
		HttpPrincipal httpPrincipal, long groupId, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStories",
				_getStoriesParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, groupId, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getStoriesCount(
		HttpPrincipal httpPrincipal, long groupId) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesCount",
				_getStoriesCountParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey, groupId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStoriesByType(
		HttpPrincipal httpPrincipal, int type) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByType",
				_getStoriesByTypeParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey, type);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStoriesByType(
		HttpPrincipal httpPrincipal, int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByType",
				_getStoriesByTypeParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, type, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStoriesByType(
		HttpPrincipal httpPrincipal, int type, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByType",
				_getStoriesByTypeParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, type, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getStoriesByTypeCount(
		HttpPrincipal httpPrincipal, int type) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByTypeCount",
				_getStoriesByTypeCountParameterTypes8);

			MethodHandler methodHandler = new MethodHandler(methodKey, type);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> findByTitle_Type(
		HttpPrincipal httpPrincipal, String title, int type) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "findByTitle_Type",
				_findByTitle_TypeParameterTypes9);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, type);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> findByTitle_Type(
		HttpPrincipal httpPrincipal, String title, int type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "findByTitle_Type",
				_findByTitle_TypeParameterTypes10);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, type, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> findByTitle_Type(
		HttpPrincipal httpPrincipal, String title, int type, int start,
		int end) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "findByTitle_Type",
				_findByTitle_TypeParameterTypes11);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, type, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int findByTitle_TypeCount(
		HttpPrincipal httpPrincipal, String title, int type) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "findByTitle_TypeCount",
				_findByTitle_TypeCountParameterTypes12);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, type);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStoriesByCategory(
		HttpPrincipal httpPrincipal, long categoryId) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByCategory",
				_getStoriesByCategoryParameterTypes13);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStoriesByCategory(
		HttpPrincipal httpPrincipal, long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<stories.model.Story>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByCategory",
				_getStoriesByCategoryParameterTypes14);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<stories.model.Story> getStoriesByCategory(
		HttpPrincipal httpPrincipal, long categoryId, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByCategory",
				_getStoriesByCategoryParameterTypes15);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<stories.model.Story>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getStoriesByCategoryCount(
		HttpPrincipal httpPrincipal, long categoryId) {

		try {
			MethodKey methodKey = new MethodKey(
				StoryServiceUtil.class, "getStoriesByCategoryCount",
				_getStoriesByCategoryCountParameterTypes16);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(StoryServiceHttp.class);

	private static final Class<?>[] _getStoryByIdParameterTypes0 = new Class[] {
		long.class
	};
	private static final Class<?>[] _getStoriesParameterTypes1 = new Class[] {
		long.class
	};
	private static final Class<?>[] _getStoriesParameterTypes2 = new Class[] {
		long.class, int.class, int.class,
		com.liferay.portal.kernel.util.OrderByComparator.class
	};
	private static final Class<?>[] _getStoriesParameterTypes3 = new Class[] {
		long.class, int.class, int.class
	};
	private static final Class<?>[] _getStoriesCountParameterTypes4 =
		new Class[] {long.class};
	private static final Class<?>[] _getStoriesByTypeParameterTypes5 =
		new Class[] {int.class};
	private static final Class<?>[] _getStoriesByTypeParameterTypes6 =
		new Class[] {
			int.class, int.class, int.class,
			com.liferay.portal.kernel.util.OrderByComparator.class
		};
	private static final Class<?>[] _getStoriesByTypeParameterTypes7 =
		new Class[] {int.class, int.class, int.class};
	private static final Class<?>[] _getStoriesByTypeCountParameterTypes8 =
		new Class[] {int.class};
	private static final Class<?>[] _findByTitle_TypeParameterTypes9 =
		new Class[] {String.class, int.class};
	private static final Class<?>[] _findByTitle_TypeParameterTypes10 =
		new Class[] {
			String.class, int.class, int.class, int.class,
			com.liferay.portal.kernel.util.OrderByComparator.class
		};
	private static final Class<?>[] _findByTitle_TypeParameterTypes11 =
		new Class[] {String.class, int.class, int.class, int.class};
	private static final Class<?>[] _findByTitle_TypeCountParameterTypes12 =
		new Class[] {String.class, int.class};
	private static final Class<?>[] _getStoriesByCategoryParameterTypes13 =
		new Class[] {long.class};
	private static final Class<?>[] _getStoriesByCategoryParameterTypes14 =
		new Class[] {
			long.class, int.class, int.class,
			com.liferay.portal.kernel.util.OrderByComparator.class
		};
	private static final Class<?>[] _getStoriesByCategoryParameterTypes15 =
		new Class[] {long.class, int.class, int.class};
	private static final Class<?>[] _getStoriesByCategoryCountParameterTypes16 =
		new Class[] {long.class};

}