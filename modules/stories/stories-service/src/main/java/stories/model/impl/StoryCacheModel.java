/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package stories.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import stories.model.Story;

/**
 * The cache model class for representing Story in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class StoryCacheModel implements CacheModel<Story>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof StoryCacheModel)) {
			return false;
		}

		StoryCacheModel storyCacheModel = (StoryCacheModel)object;

		if (storyId == storyCacheModel.storyId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, storyId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(37);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", storyId=");
		sb.append(storyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", orderNo=");
		sb.append(orderNo);
		sb.append(", type=");
		sb.append(type);
		sb.append(", title=");
		sb.append(title);
		sb.append(", smallImage=");
		sb.append(smallImage);
		sb.append(", bigImage=");
		sb.append(bigImage);
		sb.append(", displayDate=");
		sb.append(displayDate);
		sb.append(", descriptionField1=");
		sb.append(descriptionField1);
		sb.append(", detailImage=");
		sb.append(detailImage);
		sb.append(", pdfFile=");
		sb.append(pdfFile);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Story toEntityModel() {
		StoryImpl storyImpl = new StoryImpl();

		if (uuid == null) {
			storyImpl.setUuid("");
		}
		else {
			storyImpl.setUuid(uuid);
		}

		storyImpl.setStoryId(storyId);
		storyImpl.setGroupId(groupId);
		storyImpl.setCompanyId(companyId);
		storyImpl.setUserId(userId);

		if (userName == null) {
			storyImpl.setUserName("");
		}
		else {
			storyImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			storyImpl.setCreateDate(null);
		}
		else {
			storyImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			storyImpl.setModifiedDate(null);
		}
		else {
			storyImpl.setModifiedDate(new Date(modifiedDate));
		}

		storyImpl.setCategoryId(categoryId);
		storyImpl.setOrderNo(orderNo);
		storyImpl.setType(type);

		if (title == null) {
			storyImpl.setTitle("");
		}
		else {
			storyImpl.setTitle(title);
		}

		if (smallImage == null) {
			storyImpl.setSmallImage("");
		}
		else {
			storyImpl.setSmallImage(smallImage);
		}

		if (bigImage == null) {
			storyImpl.setBigImage("");
		}
		else {
			storyImpl.setBigImage(bigImage);
		}

		if (displayDate == Long.MIN_VALUE) {
			storyImpl.setDisplayDate(null);
		}
		else {
			storyImpl.setDisplayDate(new Date(displayDate));
		}

		if (descriptionField1 == null) {
			storyImpl.setDescriptionField1("");
		}
		else {
			storyImpl.setDescriptionField1(descriptionField1);
		}

		if (detailImage == null) {
			storyImpl.setDetailImage("");
		}
		else {
			storyImpl.setDetailImage(detailImage);
		}

		if (pdfFile == null) {
			storyImpl.setPdfFile("");
		}
		else {
			storyImpl.setPdfFile(pdfFile);
		}

		storyImpl.resetOriginalValues();

		return storyImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		storyId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		categoryId = objectInput.readLong();

		orderNo = objectInput.readLong();

		type = objectInput.readInt();
		title = objectInput.readUTF();
		smallImage = objectInput.readUTF();
		bigImage = objectInput.readUTF();
		displayDate = objectInput.readLong();
		descriptionField1 = (String)objectInput.readObject();
		detailImage = objectInput.readUTF();
		pdfFile = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(storyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(categoryId);

		objectOutput.writeLong(orderNo);

		objectOutput.writeInt(type);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (smallImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(smallImage);
		}

		if (bigImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bigImage);
		}

		objectOutput.writeLong(displayDate);

		if (descriptionField1 == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(descriptionField1);
		}

		if (detailImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(detailImage);
		}

		if (pdfFile == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(pdfFile);
		}
	}

	public String uuid;
	public long storyId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long categoryId;
	public long orderNo;
	public int type;
	public String title;
	public String smallImage;
	public String bigImage;
	public long displayDate;
	public String descriptionField1;
	public String detailImage;
	public String pdfFile;

}