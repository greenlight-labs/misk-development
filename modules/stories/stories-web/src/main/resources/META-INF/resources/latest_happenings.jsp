<%
    int latestHappeningType = 2;
    List<Story> latestStories = StoryServiceUtil.getStoriesByType(latestHappeningType);
%>

<section class="latest-news-slider-section" data-scroll-section>
    <div class="slider-right-shape">
        <img src="/assets/svgs/yellow-shadow-stories.svg" class="img-fluid" alt="triangle">
    </div>
    <div class="latest-news-slider">
        <%
            for (Story curStory : latestStories) {
        %>
        <portlet:renderURL var="detailPageURL">
            <portlet:param name="storyId" value="<%= String.valueOf(curStory.getStoryId()) %>" />
            <portlet:param name="mvcPath" value="/detail.jsp" />
        </portlet:renderURL>

        <div class="latest-news-body">
            <div class="latest-news-image-box">
                <a href="<%= detailPageURL.toString() %>" class="image-overlay">
                    <img src="<%= curStory.getBigImage(locale) %>" alt="" class="img-fluid">
                </a>
            </div>
            <div class="latest-news-info-box">
                <p class="info-box-title mb-4">
                    <a href="<%= detailPageURL.toString() %>">
                        <%= curStory.getTitle(locale) %>
                    </a>
                </p>
                <p class="info-box-heading m-0">
                    <%= dateFormat.format(curStory.getDisplayDate()) %>
                </p>
            </div>
        </div>
        <%
            }
        %>
    </div>
</section>