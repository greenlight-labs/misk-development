<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ include file="/init.jsp" %>

<%@include file="includes/top_section.jsp" %>

<%@include file="latest_happenings.jsp" %>

<section class="latest-news-section" data-scroll-section>
    <div class="container">
        <%@include file="includes/search.jsp" %>

        <%@include file="listing.jsp" %>

        <%@include file="includes/pagination.jsp" %>
    </div>
</section>