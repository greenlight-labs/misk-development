<%@ include file="/init.jsp"%>

<%
    String redirect = ParamUtil.getString(request, "redirect");
%>

<liferay-portlet:actionURL portletConfiguration="<%=true%>" var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="<%=true%>" var="configurationRenderURL" />

<aui:form action="<%=configurationActionURL%>" method="post" name="fm">
    <aui:input name="<%=Constants.CMD%>" type="hidden" value="<%=Constants.UPDATE%>" />

    <aui:input name="redirect" type="hidden" value="<%=configurationRenderURL%>" />

    <div class="portlet-configuration-body-content">
        <div class="container-fluid-1280">
            <aui:fieldset-group markupView="lexicon">
                <aui:fieldset>
                    <aui:field-wrapper cssClass="lfr-textarea-container" label="Section Title">
                        <liferay-ui:input-localized
                                cssClass="lfr-input-text"
                                name="sectionTitle"
                                type="textarea"
                                xml="<%= sectionTitleXml %>"
                        />
                    </aui:field-wrapper>
                    <aui:field-wrapper cssClass="lfr-input-text-container" label="Button Label">
                        <liferay-ui:input-localized
                                cssClass="lfr-input-text"
                                name="buttonLabel"
                                xml="<%= buttonLabelXml %>"
                        />
                    </aui:field-wrapper>
                    <aui:field-wrapper cssClass="lfr-input-text-container" label="Button Link">
                        <liferay-ui:input-localized
                                cssClass="lfr-input-text"
                                name="buttonLink"
                                xml="<%= buttonLinkXml %>"
                        />
                    </aui:field-wrapper>
                </aui:fieldset>
            </aui:fieldset-group>
        </div>
    </div>

    <aui:button-row>
        <aui:button cssClass="btn-lg" type="submit" />
        <aui:button cssClass="btn-lg" href="<%= redirect %>" type="cancel" />
    </aui:button-row>
</aui:form>