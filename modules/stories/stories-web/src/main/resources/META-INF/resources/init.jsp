<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="java.util.List" %>
<%@ page import="com.liferay.petra.string.StringPool" %>
<%@ page import="java.text.Format" %>
<%@ page import="com.liferay.portal.kernel.theme.ThemeDisplay" %>
<%@ page import="com.liferay.journal.model.JournalArticle" %>
<%@ page import="com.liferay.journal.model.JournalArticleDisplay"%>
<%@ page import="com.liferay.journal.service.JournalArticleLocalServiceUtil" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="stories.model.Story" %>
<%@ page import="stories.service.StoryLocalServiceUtil" %>
<%@ page import="stories.service.StoryServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.*" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM dd, yyyy", locale, timeZone);
    Format day = FastDateFormatFactoryUtil.getSimpleDateFormat("dd", locale, timeZone);
    Format monthYear = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM, yyyy", locale, timeZone);

    // Latest Stories Slider Portlet Preferences
    String sectionTitleXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "sectionTitle", LanguageUtil.get(request, "stories_web_portlet_config_section_title")));
    String buttonLabelXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLabel", LanguageUtil.get(request, "stories_web_portlet_config_button_label")));
    String buttonLinkXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLink", LanguageUtil.get(request, "stories_web_portlet_config_button_link")));

    String sectionTitle = LocalizationUtil.getLocalization(sectionTitleXml, themeDisplay.getLanguageId());
    String buttonLabel = LocalizationUtil.getLocalization(buttonLabelXml, themeDisplay.getLanguageId());
    String buttonLink = LocalizationUtil.getLocalization(buttonLinkXml, themeDisplay.getLanguageId());

    /*HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String query = originalRequest.getParameter("q");
    int start = GetterUtil.getInteger(originalRequest.getParameter("start"));
    int end = GetterUtil.getInteger(originalRequest.getParameter("end"));*/
    int limit = 9;
    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String query = originalRequest.getParameter("q");
    int start = ParamUtil.getInteger(request, "start");
    int end = ParamUtil.getInteger(request, "end");
    end = end == 0 ? limit : end;
%>