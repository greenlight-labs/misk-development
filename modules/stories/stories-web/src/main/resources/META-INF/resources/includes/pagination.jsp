<%@ page import="javax.portlet.PortletURL" %>
<%
    int totalRecords = 0;
    if(Validator.isNotNull(query)){
        totalRecords = StoryServiceUtil.findByTitle_TypeCount(query, type);
    }else{
        totalRecords = StoryServiceUtil.getStoriesByTypeCount(type);
    }
    if(totalRecords > limit){
%>
<div class="row">
    <div class="col-12 text-center" >
        <div class="pagination-container">
            <nav aria-label="Pagination">
                <ul class="pagination animate" data-animation="fadeInUp" data-duration="300">
                    <%
                        int pages = (totalRecords+limit-1)/limit;
                        for (int i = 0; i < pages; i++) {
                            int pStart = limit * i;
                            int pEnd = limit * (i+1);
                    %>
                    <%
                        PortletURL paginationPageURL = renderResponse.createRenderURL();

                        paginationPageURL.setParameter("mvcPath", "/view.jsp");
                        paginationPageURL.setParameter("start", String.valueOf(pStart));
                        paginationPageURL.setParameter("end", String.valueOf(pEnd));

                        if(query != null){
                            paginationPageURL.setParameter("q", query);
                        }
                    %>
                    <%--<portlet:renderURL var="paginationPageURL">
                        <portlet:param name="q" value='<%=query != null ? query : ""%>' />
                        <portlet:param name="start" value="<%=String.valueOf(pStart)%>" />
                        <portlet:param name="end" value="<%=String.valueOf(pEnd)%>" />
                        <portlet:param name="mvcPath" value="/view.jsp" />
                    </portlet:renderURL>--%>
                    <%
                        int pageNo = i+1;
                        int currentPage = end/limit;
                        String className = (currentPage == pageNo ? "active" : "");
                        String leftArrowClass = (currentPage == 1 ? "disabled" : "");
                        String rightArrowClass = (currentPage == pages ? "disabled" : "");

                        String paginationPageURL2 = paginationPageURL.toString();
                        if(query != null){
                            paginationPageURL2 = paginationPageURL2 + "?" + "q=" + query;
                        }

                        if(i == 0){
                    %>
                    <li class="page-item left-arrow-list <%=leftArrowClass%>">
                        <a class="page-link" href="<%= paginationPageURL2 %>"><i class="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "icon-right-arrow" : "icon-left-arrow" %>"></i></a>
                    </li>
                    <%
                        }
                    %>
                    <li class="page-item <%=className%>"><a class="page-link" href="<%= paginationPageURL2 %>"><%=pageNo%></a></li>
                    <%
                        if(pageNo == pages){
                    %>
                    <li class="page-item right-arrow-list <%=rightArrowClass%>">
                        <a class="page-link" href="<%= paginationPageURL2 %>"><i class="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "icon-left-arrow" : "icon-right-arrow" %>"></i></a>
                    </li>
                    <%
                        }
                    %>
                    <%
                        }
                    %>
                </ul>
            </nav>
        </div>
    </div>
</div>
<%
    }
%>
