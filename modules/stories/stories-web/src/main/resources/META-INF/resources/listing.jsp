<div class="row">
    <%
        List<Story> stories;
        int type = 1;
        if(Validator.isNotNull(query)){
            stories = StoryServiceUtil.findByTitle_Type(query, type, start, end);
        }else{
            stories = StoryServiceUtil.getStoriesByType(type, start, end);
        }

        if(stories.size() > 0){
            for (Story curStory : stories) {
    %>
    <portlet:renderURL var="detailPageURL2">
        <portlet:param name="storyId" value="<%= String.valueOf(curStory.getStoryId()) %>" />
        <portlet:param name="mvcPath" value="/detail.jsp" />
    </portlet:renderURL>
    <div class="col-12 col-md-6 col-lg-4">
        <div class="list-box animate" data-animation="fadeInUp" data-duration="300">
            <div class="image-container">
                <a class="image-overlay" href="<%= detailPageURL2.toString() %>">
                    <img src="<%= curStory.getSmallImage(locale) %>" class="img-fluid" alt="news1">
                </a>
            </div>
            <div class="news-info">
                <h4><a href="<%= detailPageURL2.toString() %>"><%= curStory.getTitle(locale) %>
                </a></h4>
                <span class="text-info">
                    <%= dateFormat.format(curStory.getDisplayDate()) %>
                </span>
            </div>
        </div>
    </div>
    <%
        }
    } else {
    %>
    <div class="text-center pt-5">
        <h4><liferay-ui:message key="misk-no-record-found" /></h4>
    </div>
    <%
        }
    %>
</div>