<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@include file="/init.jsp" %>

<%
    long storyId = ParamUtil.getLong(renderRequest, "storyId");

    Story story = null;
    if (storyId > 0) {
        try {
            story = StoryLocalServiceUtil.getStory(storyId);
        } catch (PortalException e) {
            //e.printStackTrace();
        }
    }
%>

<%
    if (story == null) {
%>
<h4>No Record Found.</h4>
<%
} else {
%>
<section class="news-detail-content" data-scroll-section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="offset-md-1"></div>
            <div class="col-md-10">
                <div class="news-detail-header">
                    <div class="news-detail-date">
                        <h2><%= day.format(story.getDisplayDate()) %></h2>
                        <p><%= monthYear.format(story.getDisplayDate()) %></p>
                    </div>
                    <div class="col-7 d-md-none">
                        <div class="news-detail-header-buttons">
                            <div class="sharethis-inline-share-buttons share-button"></div>
                        </div>
                    </div>
                    <div class="news-detail-header-content">
                        <p class="news-sub-head text-lightgreen"><liferay-ui:message key="stories_web_portlet_detail_top_heading" /></p>
                        <h3><%= story.getTitle(locale) %></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-1 d-md-block d-none">
                <div class="news-detail-header-buttons">
                    <div class="sharethis-inline-share-buttons share-button"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="news-detail-text">
                    <%= story.getDescriptionField1(locale) %>
                </div>
                <% if(Validator.isNotNull(story.getPdfFile(locale))){ %>
                    <div class="download-pdf-link-box">
                        <a href="<%= story.getPdfFile(locale) %>" class="pdf-icon-link" target="_blank"><i class="icon-pdf"></i></a>
                        <div class="download-pdf-link-body">
                            <p class="download-pdf-text"><liferay-ui:message key="stories_web_portlet_detail_download_pdf" /></p>
                        </div>
                    </div>
                <% } %>
            </div>
            <div class="image-wrap">
                <img class="img-fluid" src="<%= story.getDetailImage(locale) %>" alt="">
            </div>
        </div>
    </div>
</section>
<%--
<section class="video-section d-flex align-items-center" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="video-play-btn" href="#">
                    <i class="icon-ft-arrow-right"></i>
                    <p class="m-0">PLAY VIDEO</p>
                </a>
                <a class="video-pause-btn" href="#">
                    <i class="icon-pause"></i>
                    <p class="m-0">PAUSE</p>
                </a>
            </div>
        </div>
    </div>
    <video data-scroll data-scroll-speed="4" muted="" loop="" id="pageVideo" preload="auto">
        <source src="<%= story.getVideoLink(locale) %>" type="video/mp4">
    </video>
</section>
<section class="news-detail-content" data-scroll-section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <%= story.getDescriptionField2(locale) %>
                <h4><%= story.getSubtitle(locale) %></h4>
                <%= story.getDescriptionField3(locale) %>

                <div class="image-wrap">
                    <img class="img-fluid" src="<%= story.getDetailImage(locale) %>" alt="">
                </div>
                <div class="author-content">
                    <div class="blue-box"><img src="/assets/svgs/quote.svg" alt=""></div>
                    <div class="author-data">
                        <h4><%= story.getQuoteText(locale) %></h4>
                        <p><%= story.getQuoteAuthor(locale) %></p>
                    </div>
                </div>
                <%= story.getDescriptionField4(locale) %>
                <div class="share-wrap">
                    <div class="share-box">
                        <a href="javascript:" class="share-link">Share</a>
                        <div class="news-detail-header-buttons bottom-btn">
                            <div class="sharethis-inline-share-buttons all-share-btn share-button"></div>
                        </div>
                    </div>
                    <div class="line-separator"></div>
                </div>
            </div>
        </div>
    </div>
</section>
--%>

<%--<%@ include file="includes/related_stories.jsp" %>--%>

<%
    }
%>
<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5fb4b2505447720012bb52b3&product=sop' async='async'></script>