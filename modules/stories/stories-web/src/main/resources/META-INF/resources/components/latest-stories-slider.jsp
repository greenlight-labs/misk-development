<%@ include file="/init.jsp" %>
<%
    List<Story> latestStories = StoryLocalServiceUtil.getStories(scopeGroupId, 0, 9);
    int count = latestStories.size();
%>

<%
    String editMode = originalRequest.getParameter("p_l_mode");
%>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
    <div class="row">
        <div class="col-12">
            <div class="page-editor__collection__block">
                <div class="page-editor__collection-item page-editor__topper">
                    <div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title">Latest Stories Slider Module</p></div>
                    <br><br>
                    <div class="alert alert-info portlet-configuration">
                        <aui:a href="<%= portletDisplay.getURLConfiguration() %>" label="please-configure-this-portlet-to-make-it-visible-to-all-users" onClick="<%= portletDisplay.getURLConfigurationJS() %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } else { %>

<% if(count > 0 ) { %>

<section class="latest-news-slider-section" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="related-news-heading">
                    <% if(themeDisplay.getLanguageId().equals("ar_SA")) { %>
                        <h3><%= sectionTitle.replaceAll("\n", "<br/>") %></h3>
                        <% if(buttonLabel != null && buttonLink != null) { %>
                            <a href="<%= buttonLink %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><liferay-ui:message key="stories_web_portlet_config_button_label"/></span><i class="dot-line"></i></a>
                        <% } %>
                    <% }else{ %>
                        <h3><%= sectionTitle.replaceAll("\n", "<br/>") %></h3>
                        <% if(buttonLabel != null && buttonLink != null) { %>
                            <a href="<%= buttonLink %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><%= buttonLabel %></span><i class="dot-line"></i></a>
                        <% } %>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="latest-news-slider">
        <% for (Story curStory : latestStories){ %>
            <% String friendlyURL = "/news/-/stories/detail/" + curStory.getStoryId(); %>

            <div class="latest-news-body">
                <div class="latest-news-image-box">
                    <a href="<%= friendlyURL %>" class="image-overlay"><img src="<%= curStory.getBigImage(locale) %>" alt="" class="img-fluid"></a>
                </div>
                <div class="latest-news-info-box">
                    <p class="info-box-title mb-4"><a href="<%= friendlyURL %>"><%= curStory.getTitle(locale) %></a></p>
                    <p class="info-box-heading m-0"><%= dateFormat.format(curStory.getDisplayDate()) %></p>
                </div>
            </div>
        <% } %>
    </div>
</section>

<% } %>
<% } %>