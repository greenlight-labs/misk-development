<%
    long categoryId = story.getCategoryId();
    List<Story> stories = StoryServiceUtil.getStoriesByCategory(categoryId);

    if(stories.size() > 0){
%>

<section class="related-news-section" data-scroll-section>

    <%
        JournalArticle articleRelatedStoriesSection = JournalArticleLocalServiceUtil.fetchLatestArticle(scopeGroupId, "76010", 0);
        JournalArticleDisplay articleRelatedStoriesSectionDisplay = JournalArticleLocalServiceUtil.getArticleDisplay(scopeGroupId, articleRelatedStoriesSection.getArticleId(), articleRelatedStoriesSection.getDDMTemplateKey(), null, themeDisplay.getLanguageId(), themeDisplay);
        String relatedStoriesSection = articleRelatedStoriesSectionDisplay.getContent();
    %>

    <%=relatedStoriesSection.toString() %>

    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="innovation-image-slider">
                    <div class="slider related-news-slider">
                        <%
                            for (Story curStory : stories) {
                                if(storyId == curStory.getStoryId()){
                                    continue;
                                }
                        %>
                        <portlet:renderURL var="detailPageURL">
                            <portlet:param name="storyId" value="<%= String.valueOf(curStory.getStoryId()) %>" />
                            <portlet:param name="mvcPath" value="/detail.jsp" />
                        </portlet:renderURL>
                            <div>
                                <div class="innovate-img-box">
                                    <a href="<%= detailPageURL.toString() %>" class="img-inno image-overlay">
                                        <img src="<%= curStory.getSmallImage(locale) %>" class="img-fluid" alt="">
                                    </a>
                                    <div class="img-content">
                                        <h4 class="animate" data-animation="fadeInUp" data-duration="500">
                                            <a href="<%= detailPageURL.toString() %>"><%= curStory.getTitle(locale) %></a>
                                        </h4>
                                        <p class="text-info"><%= dateFormat.format(curStory.getDisplayDate()) %></p>
                                    </div>
                                </div>
                            </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<%
    }
%>