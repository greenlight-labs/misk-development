package stories.web.portlet.action;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import stories.web.constants.StoriesWebPortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Map;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + StoriesWebPortletKeys.LATESTSTORIESSLIDER
        },
        service = ConfigurationAction.class
)
public class StoriesConfigurationAction extends DefaultConfigurationAction {

        @Override
        public String getJspPath(HttpServletRequest httpServletRequest) {
                return "/configuration.jsp";
        }

        @Override
        public void processAction(
                PortletConfig portletConfig, ActionRequest actionRequest,
                ActionResponse actionResponse)
                throws Exception {

                Locale defaultLocale = LocaleUtil.getSiteDefault();

                PortletPreferences preferences = actionRequest.getPreferences();

                // Save Localized Values
                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "sectionTitle");
                Map<Locale, String> sectionTitleMap = LocalizationUtil.getLocalizationMap(actionRequest, "sectionTitle");
                preferences.setValue("sectionTitle", sectionTitleMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "buttonLabel");
                Map<Locale, String> ButtonLabelMap = LocalizationUtil.getLocalizationMap(actionRequest, "buttonLabel");
                preferences.setValue("buttonLabel", ButtonLabelMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "buttonLink");
                Map<Locale, String> ButtonLinkMap = LocalizationUtil.getLocalizationMap(actionRequest, "buttonLink");
                preferences.setValue("buttonLink", ButtonLinkMap.get(defaultLocale));

                // Store these 'added' preferences
                preferences.store();

                super.processAction(portletConfig, actionRequest, actionResponse);
        }
}
