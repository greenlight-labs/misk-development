package stories.web.constants;

/**
 * @author tz
 */
public class StoriesWebPortletKeys {

	public static final String STORIESWEB =
		"stories_web_StoriesWebPortlet";

	public static final String LATESTSTORIESSLIDER =
		"stories_web_LatestStoriesSliderPortlet";

}