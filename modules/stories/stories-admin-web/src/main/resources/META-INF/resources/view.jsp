<%@page import="stories.service.StoryLocalServiceUtil"%>
<%@ include file="/init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="stories-admin-buttons">
        <portlet:renderURL var="addStoryURL">
            <portlet:param name="mvcPath"
                           value="/edit_story.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addStoryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= StoryLocalServiceUtil.getStoriesCount(scopeGroupId) %>">
	<liferay-ui:search-container-results results="<%= StoryLocalServiceUtil.getStories(scopeGroupId, searchContainer.getStart(), searchContainer.getEnd()) %>" />
        <liferay-ui:search-container-row
                className="stories.model.Story" modelVar="story">

            <liferay-ui:search-container-column-text name="Title" value="<%= HtmlUtil.escape(story.getTitle(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Display Date" value="<%= dateFormat.format(story.getDisplayDate()) %>"/>
            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>