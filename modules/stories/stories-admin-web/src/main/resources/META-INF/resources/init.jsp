<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/editor" prefix="liferay-editor" %>

<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@ page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.taglib.search.ResultRow" %>
<%@ page import="stories.model.Category" %>
<%@ page import="stories.service.CategoryLocalServiceUtil" %>
<%@ page import="stories.service.CategoryServiceUtil" %>
<%@ page import="stories.exception.StoryTitleException" %>
<%@ page import="stories.model.Story" %>
<%@ page import="stories.service.StoryLocalServiceUtil" %>
<%@ page import="java.text.Format" %>
<%@ page import="java.util.List" %>


<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MMMM dd, yyyy", locale, timeZone);
%>