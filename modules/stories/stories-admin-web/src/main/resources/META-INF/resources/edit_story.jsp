<%@include file = "/init.jsp" %>

<%
    long storyId = ParamUtil.getLong(request, "storyId");

    Story story = null;

    if (storyId > 0) {
        try {
            story = StoryLocalServiceUtil.getStory(storyId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }

    List<Category> categories = CategoryServiceUtil.getCategories(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= story == null ? "addStory" : "updateStory" %>' var="editStoryURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editStoryURL %>" name="fm">

    <aui:model-context bean="<%= story %>" model="<%= Story.class %>" />

    <aui:input type="hidden" name="storyId"
               value='<%= story == null ? "" : story.getStoryId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:select inlineField="<%= true %>" label="type" name="type">
                <aui:option label="default" value="1" />
                <aui:option label="latest-happenings" value="2" />
            </aui:select>
            <aui:select required="true" label="Category" name="categoryId" helpMessage="Category is used for related news section that is no longer there on the website.">
                <% for (Category curCategory : categories) { %>
                <aui:option label="<%= curCategory.getName(locale) %>" value="<%= curCategory.getCategoryId() %>" />
                <% } %>
            </aui:select>
            <aui:input name="title" helpMessage="Max 65 Characters (Recommended)" autoFocus="true">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="displayDate">
                <aui:validator name="required"/>
            </aui:input>
             <%--<aui:input name="orderNo" label="Order Number" helpMessage="Order Number default value 9999" placeholder="9999" />--%>
            <aui:input name="smallImage" label="Small Image" helpMessage="Image Dimensions: 454 x 558 pixels">
                <aui:validator name="required">
                    function() {
                        return AUI.$('#<portlet:namespace />type').val() === "1";
                    }
                </aui:validator>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="smallImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="bigImage" label="Big Image" helpMessage="Image Dimensions: 1162 x 728 pixels &#013;&#010; Used in Latest News Slider and Latest Happening Section">
                <aui:validator name="required">
                    function() {
                        return AUI.$('#<portlet:namespace />type').val() === "2";
                    }
                </aui:validator>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                            function(val) {
                                var ext = val.substring(val.lastIndexOf('.') + 1);
                                return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                            }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="bigImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="descriptionField1" label="Description field 1" helpMessage="Max 470 Characters (Recommended)" />
            <aui:input name="detailImage" label="Detail Image" helpMessage="Image Dimensions: 1103 x 426 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="detailImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="pdfFile" label="PDF File" helpMessage="PDF file only">
                <aui:validator name="custom" errorMessage="Please upload pdf file only">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext === "pdf";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-pdf-button btn-secondary" type="button" data-field-id="pdfFile">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:button-row>
        <%
            if (story == null) {
        %>
        <%--send push notification checkbox--%>
        <aui:input name="sendPushNotification" type="checkbox" label="Send Push Notification" />
        <%
            }
        %>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
    // show/hide conditional fields
    var type = window.document.querySelector('[name=<portlet:namespace />type]');
    var type_value = type.value;
    function toggleConditionalFields(type_value){
        var smallImage = $('[name="<portlet:namespace />smallImage"]').parents('.form-group');
        var smallImageButton = smallImage.next();
        var bigImage = $('[name="<portlet:namespace />bigImage"]').parents('.form-group');
        var bigImageButton = bigImage.next();
        if(type_value === 1 || type_value === "1"){
            smallImage.show();
            smallImageButton.show();
            bigImage.hide();
            bigImageButton.hide();
        }else if(type_value === 2 || type_value === "2" ){
            /*smallImage.hide();
            smallImageButton.hide();*/
            bigImage.show();
            bigImageButton.show();
        }
    }
    //toggleConditionalFields(type_value);
    type.addEventListener('change', function (event) {
        event.preventDefault();
        var type = window.document.querySelector('[name=<portlet:namespace />type]');
        var type_value = type.value;
        //toggleConditionalFields(type_value);
    });
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }

    const pdfButton = window.document.querySelector( '.select-pdf-button' );
    pdfButton.addEventListener('click', function (event) {
        event.preventDefault();
        var element = event.currentTarget;
        var fieldId = element.dataset.fieldId;
        var itemSelectorDialog = new A.LiferayItemSelectorDialog
        (
            {
                eventName: 'selectPDFDocumentLibrary',
                on: {
                    selectedItemChange: function(event) {
                        var selectedItem = event.newVal;
                        if(selectedItem)
                        {
                            var itemValue = selectedItem.value;
                            itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                            window['<portlet:namespace />'+fieldId].value=itemValue;
                            var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                            var editLanguage = null;
                            if(currentEditLanguage === 'en-US'){
                                editLanguage = 'en_US';
                            }else if(currentEditLanguage === 'ar-SA'){
                                editLanguage = 'ar_SA';
                           }
                            if(editLanguage){
                                window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                            }
                        }
                    }
                },
                title: '<liferay-ui:message key="Select PDF" />',
                url: '${pdfItemSelectorURL }'
            }
        );
        itemSelectorDialog.open();

    });
</aui:script>