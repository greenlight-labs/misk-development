package stories.admin.web.portlet;

import com.github.slugify.Slugify;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import stories.admin.web.constants.StoriesAdminWebPortletKeys;
import stories.model.Category;
import stories.service.CategoryLocalService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.add-default-resource=true",
                "com.liferay.portlet.display-category=category.hidden",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.layout-cacheable=true",
                "com.liferay.portlet.private-request-attributes=false",
                "com.liferay.portlet.private-session-attributes=false",
                "com.liferay.portlet.render-weight=50",
                "com.liferay.portlet.use-default-template=true",
                "javax.portlet.display-name=CategoriesAdminWeb",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/category/view.jsp",
                "javax.portlet.name=" + StoriesAdminWebPortletKeys.CATEGORIESADMINWEB,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",

        },
        service = Portlet.class
)
public class CategoriesAdminWebPortlet extends MVCPortlet {

    public void addCategory(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Category.class.getName(), request);

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
                WebKeys.THEME_DISPLAY);

        Map<Locale, String> nameMap = LocalizationUtil.getLocalizationMap(request, "name");
        Map<Locale, String> svgIconMap = LocalizationUtil.getLocalizationMap(request, "svgIcon");
        String name = ParamUtil.getString(request, "name");
        Slugify slg = new Slugify();
        String slug = slg.slugify(name);


        try {
            _categoryLocalService.addCategory(serviceContext.getUserId(),
                    nameMap, slug,
                    serviceContext);
        } catch (PortalException pe) {

            Logger.getLogger(CategoriesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);

            response.setRenderParameter(
                    "mvcPath", "/category/edit.jsp");
        }
    }

    public void updateCategory(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Category.class.getName(), request);

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
                WebKeys.THEME_DISPLAY);

        long categoryId = ParamUtil.getLong(request, "categoryId");

        Map<Locale, String> nameMap = LocalizationUtil.getLocalizationMap(request, "name");
        Map<Locale, String> svgIconMap = LocalizationUtil.getLocalizationMap(request, "svgIcon");
        String name = ParamUtil.getString(request, "name");
        Slugify slg = new Slugify();
        String slug = slg.slugify(name);

        try {
            _categoryLocalService.updateCategory(serviceContext.getUserId(), categoryId,
                    nameMap, slug,
                    serviceContext);

        } catch (PortalException pe) {

            Logger.getLogger(CategoriesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);

            response.setRenderParameter(
                    "mvcPath", "/category/edit.jsp");
        }
    }

    public void deleteCategory(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Category.class.getName(), request);

        long categoryId = ParamUtil.getLong(request, "categoryId");

        try {
            _categoryLocalService.deleteCategory(categoryId, serviceContext);
        } catch (PortalException pe) {

            Logger.getLogger(CategoriesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);
        }
    }

    @Reference
    private CategoryLocalService _categoryLocalService;
}