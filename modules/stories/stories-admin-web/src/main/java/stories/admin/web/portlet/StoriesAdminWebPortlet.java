package stories.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.file.criterion.FileItemSelectorCriterion;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import stories.admin.web.constants.StoriesAdminWebPortletKeys;
import stories.model.Story;
import stories.service.StoryLocalService;

import javax.portlet.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.add-default-resource=true",
                "com.liferay.portlet.display-category=category.hidden",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.layout-cacheable=true",
                "com.liferay.portlet.private-request-attributes=false",
                "com.liferay.portlet.private-session-attributes=false",
                "com.liferay.portlet.render-weight=50",
                "com.liferay.portlet.use-default-template=true",
                "javax.portlet.display-name=StoriesAdminWeb",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + StoriesAdminWebPortletKeys.STORIESADMINWEB,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",

        },
        service = Portlet.class
)
public class StoriesAdminWebPortlet extends MVCPortlet {

    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
                .create(renderRequest);

        List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
        desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());
        // image upload
        ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();
        imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);
        // pdf upload
        FileItemSelectorCriterion fileItemSelectorCriterion = new FileItemSelectorCriterion();
        fileItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

        PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
                "selectDocumentLibrary", imageItemSelectorCriterion);

        PortletURL pdfItemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
                "selectPDFDocumentLibrary", fileItemSelectorCriterion);

        renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);
        renderRequest.setAttribute("pdfItemSelectorURL", pdfItemSelectorURL);

        super.render(renderRequest, renderResponse);
    }

    public void addStory(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Story.class.getName(), request);

        ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
                WebKeys.THEME_DISPLAY);
        long orderNo = ParamUtil.getLong(request, "orderNo",9999);
        long categoryId = ParamUtil.getLong(request, "categoryId");
        int type = ParamUtil.getInteger(request, "type");
        Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
        Map<Locale, String> smallImageMap = LocalizationUtil.getLocalizationMap(request, "smallImage");
        Map<Locale, String> bigImageMap = LocalizationUtil.getLocalizationMap(request, "bigImage");
        Date displayDate = ParamUtil.getDate(request, "displayDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale()), null);
        Map<Locale, String> descriptionField1Map = LocalizationUtil.getLocalizationMap(request, "descriptionField1");
        Map<Locale, String> detailImageMap = LocalizationUtil.getLocalizationMap(request, "detailImage");
        Map<Locale, String> pdfFileMap = LocalizationUtil.getLocalizationMap(request, "pdfFile");
        boolean sendPushNotification = ParamUtil.getBoolean(request, "sendPushNotification");
		// default Value of OrderNo to 9999
		if (orderNo == 0) {
			orderNo = 9999;
		}
        try {
            _storyLocalService.addStory(serviceContext.getUserId(), categoryId,
                    type, titleMap, smallImageMap, bigImageMap, displayDate,
                    descriptionField1Map, detailImageMap, pdfFileMap,orderNo, sendPushNotification,
                    serviceContext);
        } catch (PortalException pe) {

            Logger.getLogger(StoriesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);

            response.setRenderParameter(
                    "mvcPath", "/edit_story.jsp");
        }
    }

    public void updateStory(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Story.class.getName(), request);

        ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
                WebKeys.THEME_DISPLAY);

        long storyId = ParamUtil.getLong(request, "storyId");
        long orderNo = ParamUtil.getLong(request, "orderNo",9999);
        long categoryId = ParamUtil.getLong(request, "categoryId");
        int type = ParamUtil.getInteger(request, "type");
        Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
        Map<Locale, String> smallImageMap = LocalizationUtil.getLocalizationMap(request, "smallImage");
        Map<Locale, String> bigImageMap = LocalizationUtil.getLocalizationMap(request, "bigImage");
        Date displayDate = ParamUtil.getDate(request, "displayDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale()), null);
        Map<Locale, String> descriptionField1Map = LocalizationUtil.getLocalizationMap(request, "descriptionField1");
        Map<Locale, String> detailImageMap = LocalizationUtil.getLocalizationMap(request, "detailImage");
        Map<Locale, String> pdfFileMap = LocalizationUtil.getLocalizationMap(request, "pdfFile");
		// default Value of OrderNo to 9999
		if (orderNo == 0) {
			orderNo = 9999;
		}
        try {
            _storyLocalService.updateStory(serviceContext.getUserId(), storyId, categoryId,
                    type, titleMap, smallImageMap, bigImageMap, displayDate,
                    descriptionField1Map, detailImageMap, pdfFileMap,orderNo,
                    serviceContext);

        } catch (PortalException pe) {

            Logger.getLogger(StoriesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);

            response.setRenderParameter(
                    "mvcPath", "/edit_story.jsp");
        }
    }

    public void deleteStory(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Story.class.getName(), request);

        long storyId = ParamUtil.getLong(request, "storyId");

        try {
            _storyLocalService.deleteStory(storyId, serviceContext);
        } catch (PortalException pe) {

            Logger.getLogger(StoriesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);
        }
    }

    @Reference
    private StoryLocalService _storyLocalService;

    @Reference
    private ItemSelector _itemSelector;
}