package stories.admin.web.constants;

/**
 * @author tz
 */
public class StoriesAdminWebPortletKeys {

	public static final String STORIESADMINWEB =
		"stories_admin_web_StoriesAdminWebPortlet";

	public static final String CATEGORIESADMINWEB =
			"stories_admin_web_CategoriesAdminWebPortlet";

}