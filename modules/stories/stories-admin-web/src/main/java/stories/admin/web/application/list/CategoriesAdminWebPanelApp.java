package stories.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import stories.admin.web.constants.StoriesAdminWebPanelCategoryKeys;
import stories.admin.web.constants.StoriesAdminWebPortletKeys;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=101",
		"panel.category.key=" + StoriesAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class CategoriesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return StoriesAdminWebPortletKeys.CATEGORIESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + StoriesAdminWebPortletKeys.CATEGORIESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}