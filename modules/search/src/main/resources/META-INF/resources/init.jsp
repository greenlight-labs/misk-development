<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.PortalUtil" %>
<%@ page import="java.util.List" %>
<%@ page import="stories.model.Story" %>
<%@ page import="events.model.Event" %>
<%@ page import="residences.model.Residence" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%!
    // sanitize input
    protected String sanitizeInput(String query) {
        String alphaRegex = "[^a-zA-Z\u0621-\u064A\u066E-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFF\\s]";
        // replce all non arabic and english characters with empty string
        if (StringUtils.isNotBlank(query)) {
            query = query.replaceAll(alphaRegex, "");
        }
        return query;
    }
%>

<%
    String type = (String) request.getAttribute("type");
    String query = (String) request.getAttribute("q");
    // sanitize query
    query = sanitizeInput(query);
%>
