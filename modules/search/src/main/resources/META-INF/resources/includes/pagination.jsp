<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="events.service.EventLocalServiceUtil" %>
<%@ page import="residences.service.ResidenceLocalServiceUtil" %>
<%@ page import="stories.service.StoryLocalServiceUtil" %>

<%
    int totalRecords = 0;
    int limit = request.getAttribute("limit") != null ? (Integer)request.getAttribute("limit") : 0;
    int end = request.getAttribute("end") != null ? (Integer)request.getAttribute("end") : 0;
    if(Validator.isNotNull(query)){
        if(type != null && type.equals("stories")){
            totalRecords = StoryLocalServiceUtil.findByTitleCount(query);
        }else if(type != null && type.equals("events")){
            totalRecords = EventLocalServiceUtil.findByTitleCount(query);
        }else if(type != null && type.equals("residences")){
            totalRecords = ResidenceLocalServiceUtil.countByT_D(query, themeDisplay.getLanguageId());
        }else{
            totalRecords = ResidenceLocalServiceUtil.countSearchCompleteWebsite(query, themeDisplay.getLanguageId());
        }
    }

    if(totalRecords > limit){
%>
<div class="row">
    <div class="col-12 text-center" >
        <div class="pagination-container">
            <nav aria-label="Pagination">
                <ul class="pagination animate" data-animation="fadeInUp" data-duration="300">
                    <%
                        int pages = (totalRecords+limit-1)/limit;
                        for (int i = 0; i < pages; i++) {
                            int pStart = limit * i;
                            int pEnd = limit * (i+1);
                    %>
                    <%
                        /*
                        generate pagination url by appending all query string parameters
                        /search-results/?q=test&type=all&start=0&end=3
                        */
                        String paginationPageURL = "/search-results/?q="+query+"&start="+pStart+"&end="+pEnd;
                        if(type != null){
                            paginationPageURL += "&type="+type;
                        }

                        int pageNo = i+1;
                        int currentPage = end/limit;
                        String className = (currentPage == pageNo ? "active" : "");
                        String leftArrowClass = (currentPage == 1 ? "disabled" : "");
                        String rightArrowClass = (currentPage == pages ? "disabled" : "");

                        if(i == 0){
                    %>
                    <li class="page-item left-arrow-list <%=leftArrowClass%>">
                        <a class="page-link" href="<%= paginationPageURL %>"><i class="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "icon-right-arrow" : "icon-left-arrow" %>"></i></a>
                    </li>
                    <%
                        }
                    %>
                    <li class="page-item <%=className%>"><a class="page-link" href="<%= paginationPageURL %>"><%=pageNo%></a></li>
                    <%
                        if(pageNo == pages){
                    %>
                    <li class="page-item right-arrow-list <%=rightArrowClass%>">
                        <a class="page-link" href="<%= paginationPageURL %>"><i class="<%=themeDisplay.getLanguageId().equals("ar_SA") ? "icon-left-arrow" : "icon-right-arrow" %>"></i></a>
                    </li>
                    <%
                        }
                    %>
                    <%
                        }
                    %>
                </ul>
            </nav>
        </div>
    </div>
</div>
<%
    }
%>
