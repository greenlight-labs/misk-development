<div class="search-result-counter">
    <%
        List<Residence> residences = (List<Residence>) request.getAttribute("residences");
        int totalRecords = (int) request.getAttribute("totalRecords");
    %>
    <h4><%= totalRecords %> <liferay-ui:message key="search_web_results_for" /> "<%=query%>"</h4>
</div>
<div class="search-results-mn">
    <ul>
        <%
            if(residences.size() > 0){
                for (Residence curResidence : residences) {
                    String image = curResidence.getListingImage(locale);
                    String detailPageURL = "/residences/-/residences/detail/" + curResidence.getResidenceId();
        %>
        <li>
            <div class="search-img">
                <% if(image != null && !"".equals(image)){ %>
                <img src="<%=image%>" alt="" style="max-height:300px!important;max-width:350px!important;">
                <% }else{ %>
                <img src="/assets/images/no-image-available.png" alt="" style="max-height:300px!important;max-width:350px!important;">
                <% } %>
            </div>
            <div class="search-text">
                <%
                    // clean html, limit text and append ... at end
                    String title = curResidence.getListingTitle(locale);
                    if(title.length() > 120){
                        title = title.substring(0, 120) + "...";
                    }
                    String description = HtmlUtil.extractText(curResidence.getListingDescription(locale));
                    if(description.length() > 285){
                        description = StringUtil.shorten(description, 285);
                        description = description + "...";
                    }
                %>
                <h4><%=title%></h4>
                <p><%=description%></p>
                <a class="text-info" href="<%= detailPageURL %>"><liferay-ui:message key="search_web_detail_page_link" /></a>
            </div>
        </li>
        <%
                }
            }
        %>
    </ul>
</div>