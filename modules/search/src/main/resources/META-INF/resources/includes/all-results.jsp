<%@ page import="com.liferay.portal.kernel.util.LocalizationUtil" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.Map" %>
<div class="search-result-counter">
    <%
        List<Object[]> models = (List<Object[]>) request.getAttribute("models");
        int totalRecords = (int) request.getAttribute("totalRecords");
    %>
    <h4><%= totalRecords %> <liferay-ui:message key="search_web_results_for" /> "<%=query%>"</h4>
</div>
<div class="search-results-mn">
    <ul>
        <%
            if(models.size() > 0){
                for (Object[] curItem : models) {
                    String titleMap = (String) curItem[0];
                    String descriptionMap = (String) curItem[1];
                    String imageMap = (String) curItem[2];
                    String modelType = (String) curItem[3];
                    long modelItemId = Long.parseLong(curItem[4].toString());

                    String title = LocalizationUtil.getLocalization(titleMap, themeDisplay.getLanguageId());
                    String image = LocalizationUtil.getLocalization(imageMap, themeDisplay.getLanguageId());
                    String description = LocalizationUtil.getLocalization(descriptionMap, themeDisplay.getLanguageId());

                    // gemerate url for type: event, story, residence, album, amenity, mobility, team, partner
                    String detailPageURL = "";
                    switch (modelType) {
                        case "event":
                            detailPageURL = "/events/-/events/detail/" + modelItemId;
                            break;
                        case "story":
                            detailPageURL = "/news/-/stories/detail/" + modelItemId;
                            break;
                        case "residence":
                            /*@MY_TODO: show only 71430 item link from residence*/
                            detailPageURL = modelItemId == 71430 ? "/residences/-/residences/detail/" + modelItemId : "/residences";
                            break;
                        case "album":
                            detailPageURL = "/gallery/-/albums/detail/" + modelItemId;
                            break;
                        case "amenity":
                            detailPageURL = "/amenities";
                            break;
                        case "mobility":
                            detailPageURL = "/mobility";
                            break;
                        case "team":
                            detailPageURL = "/our-teams";
                            break;
                        case "partner":
                            detailPageURL = "/our-partners";
                            break;
                    }
        %>
        <li>
            <div class="search-img">
                <% if(image != null && !"".equals(image)){ %>
                    <img src="<%=image%>" alt="" style="max-height:300px!important;max-width:350px!important;">
                <% }else{ %>
                    <img src="/assets/images/no-image-available.png" alt="" style="max-height:300px!important;max-width:350px!important;">
                <% } %>
            </div>
            <div class="search-text">
                <%
                    // clean html, limit text and append ... at end
                    if(title.length() > 120){
                        title = title.substring(0, 120) + "...";
                    }
                    description = HtmlUtil.extractText(description);
                    if(description.length() > 285){
                        description = StringUtil.shorten(description, 285);
                        description = description + "...";
                    }
                %>
                <h4><%=title%></h4>
                <p><%=description%></p>
                <a class="text-info" href="<%= detailPageURL %>"><liferay-ui:message key="search_web_detail_page_link" /></a>
            </div>
        </li>
        <%
                }
            }
        %>
    </ul>
</div>