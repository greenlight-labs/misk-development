<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>
<div class="search-result-counter">
    <%
        List<Event> events = (List<Event>) request.getAttribute("events");
        int totalRecords = (int) request.getAttribute("totalRecords");
    %>
    <h4><%= totalRecords %> <liferay-ui:message key="search_web_results_for" /> "<%=query%>"</h4>
</div>
<div class="search-results-mn">
    <ul>
        <%
            if(events.size() > 0){
                for (Event curEvent : events) {
                    String detailPageURL = "/events/-/events/detail/" + curEvent.getEventId();
                    String image = curEvent.getBigImage(locale);
        %>
        <li>
            <div class="search-img">
                <% if(image != null && !"".equals(image)){ %>
                <img src="<%=image%>" alt="" style="max-height:300px!important;max-width:350px!important;">
                <% }else{ %>
                <img src="/assets/images/no-image-available.png" alt="" style="max-height:300px!important;max-width:350px!important;">
                <% } %>
            </div>
            <div class="search-text">
                <%
                    // clean html, limit text and append ... at end
                    String title = curEvent.getTitle(locale);
                    if(title.length() > 120){
                        title = title.substring(0, 120) + "...";
                    }
                    String description = HtmlUtil.extractText(curEvent.getDescriptionField1(locale));
                    if(description.length() > 285){
                        description = StringUtil.shorten(description, 285);
                        description = description + "...";
                    }
                %>
                <h4><%=title%></h4>
                <p><%=description%></p>
                <a class="text-info" href="<%= detailPageURL %>"><liferay-ui:message key="search_web_detail_page_link" /></a>
            </div>
        </li>
        <%
                }
            }
        %>
    </ul>
</div>