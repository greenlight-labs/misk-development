<%@ include file="/init.jsp" %>

<section class="inner-content-section" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="mb-0"><liferay-ui:message key="search_web_search" /></h3>
                <h1 class="text-primary"><liferay-ui:message key="search_web_results" /></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 testing123">
                <% if(type != null && type.equals("stories")){ %>
                    <%@include file="/includes/stories-results.jsp" %>
                <% }else if(type != null && type.equals("events")){ %>
                    <%@include file="/includes/events-results.jsp" %>
                <% }else if(type != null && type.equals("residences")){ %>
                    <%@include file="/includes/residences-results.jsp" %>
                <% }else{ %>
                    <%@include file="/includes/all-results.jsp" %>
                <% } %>

            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <%@include file="/includes/pagination.jsp" %>
            </div>
        </div>
    </div>
</section>