package search.portlet;

import com.liferay.portal.kernel.portlet.LiferayRenderRequest;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import events.model.Event;
import events.service.EventLocalService;
import events.service.EventLocalServiceUtil;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import residences.model.Residence;
import residences.service.ResidenceLocalService;
import residences.service.ResidenceLocalServiceUtil;
import search.constants.SearchPortletKeys;
import stories.model.Story;
import stories.service.StoryLocalService;
import stories.service.StoryLocalServiceUtil;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.misk",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Search",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + SearchPortletKeys.SEARCH,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class SearchPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					Story.class.getName(), renderRequest);

			long groupId = serviceContext.getScopeGroupId();

			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(
					WebKeys.THEME_DISPLAY);
			String languageId = themeDisplay.getLanguageId();

			HttpServletRequest originalRequest = ((LiferayRenderRequest) renderRequest).getOriginalHttpServletRequest();
			String type = originalRequest.getParameter("type");
			String query = originalRequest.getParameter("q");

			// set type attribute in request
			renderRequest.setAttribute("type", type);
			if(StringUtils.isBlank(type)){
				renderRequest.setAttribute("type", "all");
			}

			if (StringUtils.isNotBlank(query)) {
				// set query attribute in request
				renderRequest.setAttribute("q", query);
				// sanitize query
				query = sanitizeInput(query);

				int limit = 9;
				String startStr = originalRequest.getParameter("start");
				String endStr = originalRequest.getParameter("end");
				int start = startStr != null ? Integer.parseInt(startStr) : 0;
				int end = endStr != null ? Integer.parseInt(endStr) : limit;

				renderRequest.setAttribute("end", end);
				renderRequest.setAttribute("limit", limit);

				int totalRecords = 0;

				if ("stories".equals(type)) {
					List<Story> stories = StoryLocalServiceUtil.findByTitle(query, start, end);
					renderRequest.setAttribute("stories", stories);
					totalRecords = StoryLocalServiceUtil.findByTitleCount(query);
				} else if ("events".equals(type)) {
					List<Event> events = EventLocalServiceUtil.findByTitle(query, start, end);
					renderRequest.setAttribute("events", events);
					totalRecords = EventLocalServiceUtil.findByTitleCount(query);
				} else if ("residences".equals(type)) {
					List<Residence> residences = ResidenceLocalServiceUtil.findByT_D(query, start, limit, languageId);
					renderRequest.setAttribute("residences", residences);
					totalRecords = ResidenceLocalServiceUtil.countByT_D(query, themeDisplay.getLanguageId());
				} else {
					List<Object> models = ResidenceLocalServiceUtil.searchCompleteWebsite(query, start, limit, languageId);
					renderRequest.setAttribute("models", models);
					totalRecords = ResidenceLocalServiceUtil.countSearchCompleteWebsite(query, themeDisplay.getLanguageId());
				}
				// set total records
				renderRequest.setAttribute("totalRecords", totalRecords);
			}else{
				// set query and total records in request
				renderRequest.setAttribute("q", "");
				renderRequest.setAttribute("totalRecords", 0);
				if ("stories".equals(type)) {
					List<Story> stories = new ArrayList<>();
					renderRequest.setAttribute("stories", stories);
				} else if ("events".equals(type)) {
					List<Event> events = new ArrayList<>();
					renderRequest.setAttribute("events", events);
				} else if ("residences".equals(type)) {
					List<Residence> residences = new ArrayList<>();
					renderRequest.setAttribute("residences", residences);
				} else {
					List<Object> models = new ArrayList<>();
					renderRequest.setAttribute("models", models);
				}
			}
		} catch (Exception e) {
			throw new PortletException(e);
		}

		super.render(renderRequest, renderResponse);
	}

	protected String sanitizeInput(String q) {
		String alphaRegex = "[^a-zA-Z\u0621-\u064A\u066E-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFDFF\uFE70-\uFEFF\\s]";
		// replce all non arabic and english characters with empty string
		if (StringUtils.isNotBlank(q)) {
			q = q.replaceAll(alphaRegex, "");
		}
		return q;
	}
}