/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PushNotificationLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationLocalService
 * @generated
 */
public class PushNotificationLocalServiceWrapper
	implements PushNotificationLocalService,
			   ServiceWrapper<PushNotificationLocalService> {

	public PushNotificationLocalServiceWrapper(
		PushNotificationLocalService pushNotificationLocalService) {

		_pushNotificationLocalService = pushNotificationLocalService;
	}

	@Override
	public com.push.notification.model.PushNotification addEntry(
			com.push.notification.model.PushNotification orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.push.notification.exception.
				   PushNotificationValidateException {

		return _pushNotificationLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the push notification to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was added
	 */
	@Override
	public com.push.notification.model.PushNotification addPushNotification(
		com.push.notification.model.PushNotification pushNotification) {

		return _pushNotificationLocalService.addPushNotification(
			pushNotification);
	}

	@Override
	public int countByType(String type) {
		return _pushNotificationLocalService.countByType(type);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new push notification with the primary key. Does not add the push notification to the database.
	 *
	 * @param notificationId the primary key for the new push notification
	 * @return the new push notification
	 */
	@Override
	public com.push.notification.model.PushNotification createPushNotification(
		long notificationId) {

		return _pushNotificationLocalService.createPushNotification(
			notificationId);
	}

	@Override
	public com.push.notification.model.PushNotification deleteEntry(
			long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationLocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the push notification with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification that was removed
	 * @throws PortalException if a push notification with the primary key could not be found
	 */
	@Override
	public com.push.notification.model.PushNotification deletePushNotification(
			long notificationId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationLocalService.deletePushNotification(
			notificationId);
	}

	/**
	 * Deletes the push notification from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was removed
	 */
	@Override
	public com.push.notification.model.PushNotification deletePushNotification(
		com.push.notification.model.PushNotification pushNotification) {

		return _pushNotificationLocalService.deletePushNotification(
			pushNotification);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _pushNotificationLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _pushNotificationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _pushNotificationLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _pushNotificationLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _pushNotificationLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _pushNotificationLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.push.notification.model.PushNotification fetchPushNotification(
		long notificationId) {

		return _pushNotificationLocalService.fetchPushNotification(
			notificationId);
	}

	/**
	 * Returns the push notification matching the UUID and group.
	 *
	 * @param uuid the push notification's UUID
	 * @param groupId the primary key of the group
	 * @return the matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	@Override
	public com.push.notification.model.PushNotification
		fetchPushNotificationByUuidAndGroupId(String uuid, long groupId) {

		return _pushNotificationLocalService.
			fetchPushNotificationByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<com.push.notification.model.PushNotification>
		findByNotificationIds(long[] notificationIds, int start, int end) {

		return _pushNotificationLocalService.findByNotificationIds(
			notificationIds, start, end);
	}

	@Override
	public java.util.List<com.push.notification.model.PushNotification>
		findByType(String type) {

		return _pushNotificationLocalService.findByType(type);
	}

	@Override
	public java.util.List<com.push.notification.model.PushNotification>
		findByType(String type, int start, int end) {

		return _pushNotificationLocalService.findByType(type, start, end);
	}

	@Override
	public java.util.List<com.push.notification.model.PushNotification>
		findByType(
			String type, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.push.notification.model.PushNotification>
					orderByComparator) {

		return _pushNotificationLocalService.findByType(
			type, start, end, orderByComparator);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _pushNotificationLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _pushNotificationLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _pushNotificationLocalService.
			getIndexableActionableDynamicQuery();
	}

	@Override
	public com.push.notification.model.PushNotification getNewObject(
		long primaryKey) {

		return _pushNotificationLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _pushNotificationLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the push notification with the primary key.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification
	 * @throws PortalException if a push notification with the primary key could not be found
	 */
	@Override
	public com.push.notification.model.PushNotification getPushNotification(
			long notificationId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationLocalService.getPushNotification(
			notificationId);
	}

	/**
	 * Returns the push notification matching the UUID and group.
	 *
	 * @param uuid the push notification's UUID
	 * @param groupId the primary key of the group
	 * @return the matching push notification
	 * @throws PortalException if a matching push notification could not be found
	 */
	@Override
	public com.push.notification.model.PushNotification
			getPushNotificationByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationLocalService.
			getPushNotificationByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.push.notification.model.PushNotification
			getPushNotificationFromRequest(
				long primaryKey, javax.portlet.PortletRequest request)
		throws com.push.notification.exception.
			PushNotificationValidateException,
			   javax.portlet.PortletException {

		return _pushNotificationLocalService.getPushNotificationFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of push notifications
	 */
	@Override
	public java.util.List<com.push.notification.model.PushNotification>
		getPushNotifications(int start, int end) {

		return _pushNotificationLocalService.getPushNotifications(start, end);
	}

	/**
	 * Returns all the push notifications matching the UUID and company.
	 *
	 * @param uuid the UUID of the push notifications
	 * @param companyId the primary key of the company
	 * @return the matching push notifications, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.push.notification.model.PushNotification>
		getPushNotificationsByUuidAndCompanyId(String uuid, long companyId) {

		return _pushNotificationLocalService.
			getPushNotificationsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of push notifications matching the UUID and company.
	 *
	 * @param uuid the UUID of the push notifications
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching push notifications, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.push.notification.model.PushNotification>
		getPushNotificationsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.push.notification.model.PushNotification>
					orderByComparator) {

		return _pushNotificationLocalService.
			getPushNotificationsByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of push notifications.
	 *
	 * @return the number of push notifications
	 */
	@Override
	public int getPushNotificationsCount() {
		return _pushNotificationLocalService.getPushNotificationsCount();
	}

	@Override
	public void saveNotification(
		String type, long typeId, String title, String body,
		com.liferay.portal.kernel.json.JSONObject typeAdditionalPayload) {

		_pushNotificationLocalService.saveNotification(
			type, typeId, title, body, typeAdditionalPayload);
	}

	@Override
	public void sendNotification(
			com.liferay.portal.kernel.json.JSONObject jsonObject,
			java.util.List<String> tokens)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		_pushNotificationLocalService.sendNotification(jsonObject, tokens);
	}

	@Override
	public com.push.notification.model.PushNotification updateEntry(
			com.push.notification.model.PushNotification orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.push.notification.exception.
				   PushNotificationValidateException {

		return _pushNotificationLocalService.updateEntry(
			orgEntry, serviceContext);
	}

	/**
	 * Updates the push notification in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was updated
	 */
	@Override
	public com.push.notification.model.PushNotification updatePushNotification(
		com.push.notification.model.PushNotification pushNotification) {

		return _pushNotificationLocalService.updatePushNotification(
			pushNotification);
	}

	@Override
	public PushNotificationLocalService getWrappedService() {
		return _pushNotificationLocalService;
	}

	@Override
	public void setWrappedService(
		PushNotificationLocalService pushNotificationLocalService) {

		_pushNotificationLocalService = pushNotificationLocalService;
	}

	private PushNotificationLocalService _pushNotificationLocalService;

}