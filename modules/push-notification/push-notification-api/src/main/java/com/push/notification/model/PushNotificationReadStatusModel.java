/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the PushNotificationReadStatus service. Represents a row in the &quot;misk_push_notification_read_status&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>com.push.notification.model.impl.PushNotificationReadStatusModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>com.push.notification.model.impl.PushNotificationReadStatusImpl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationReadStatus
 * @generated
 */
@ProviderType
public interface PushNotificationReadStatusModel
	extends BaseModel<PushNotificationReadStatus> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a push notification read status model instance should use the {@link PushNotificationReadStatus} interface instead.
	 */

	/**
	 * Returns the primary key of this push notification read status.
	 *
	 * @return the primary key of this push notification read status
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this push notification read status.
	 *
	 * @param primaryKey the primary key of this push notification read status
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this push notification read status.
	 *
	 * @return the uuid of this push notification read status
	 */
	@AutoEscape
	public String getUuid();

	/**
	 * Sets the uuid of this push notification read status.
	 *
	 * @param uuid the uuid of this push notification read status
	 */
	public void setUuid(String uuid);

	/**
	 * Returns the read status ID of this push notification read status.
	 *
	 * @return the read status ID of this push notification read status
	 */
	public long getReadStatusId();

	/**
	 * Sets the read status ID of this push notification read status.
	 *
	 * @param readStatusId the read status ID of this push notification read status
	 */
	public void setReadStatusId(long readStatusId);

	/**
	 * Returns the notification ID of this push notification read status.
	 *
	 * @return the notification ID of this push notification read status
	 */
	public long getNotificationId();

	/**
	 * Sets the notification ID of this push notification read status.
	 *
	 * @param notificationId the notification ID of this push notification read status
	 */
	public void setNotificationId(long notificationId);

	/**
	 * Returns the app user ID of this push notification read status.
	 *
	 * @return the app user ID of this push notification read status
	 */
	public long getAppUserId();

	/**
	 * Sets the app user ID of this push notification read status.
	 *
	 * @param appUserId the app user ID of this push notification read status
	 */
	public void setAppUserId(long appUserId);

	/**
	 * Returns the app user uuid of this push notification read status.
	 *
	 * @return the app user uuid of this push notification read status
	 */
	public String getAppUserUuid();

	/**
	 * Sets the app user uuid of this push notification read status.
	 *
	 * @param appUserUuid the app user uuid of this push notification read status
	 */
	public void setAppUserUuid(String appUserUuid);

	/**
	 * Returns the read status of this push notification read status.
	 *
	 * @return the read status of this push notification read status
	 */
	public boolean getReadStatus();

	/**
	 * Returns <code>true</code> if this push notification read status is read status.
	 *
	 * @return <code>true</code> if this push notification read status is read status; <code>false</code> otherwise
	 */
	public boolean isReadStatus();

	/**
	 * Sets whether this push notification read status is read status.
	 *
	 * @param readStatus the read status of this push notification read status
	 */
	public void setReadStatus(boolean readStatus);

}