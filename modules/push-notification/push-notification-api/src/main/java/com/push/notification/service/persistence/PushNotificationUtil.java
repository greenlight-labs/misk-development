/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.push.notification.model.PushNotification;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the push notification service. This utility wraps <code>com.push.notification.service.persistence.impl.PushNotificationPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationPersistence
 * @generated
 */
public class PushNotificationUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(PushNotification pushNotification) {
		getPersistence().clearCache(pushNotification);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, PushNotification> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PushNotification> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PushNotification> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PushNotification> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static PushNotification update(PushNotification pushNotification) {
		return getPersistence().update(pushNotification);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static PushNotification update(
		PushNotification pushNotification, ServiceContext serviceContext) {

		return getPersistence().update(pushNotification, serviceContext);
	}

	/**
	 * Returns all the push notifications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching push notifications
	 */
	public static List<PushNotification> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the push notifications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of matching push notifications
	 */
	public static List<PushNotification> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notifications
	 */
	public static List<PushNotification> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notifications
	 */
	public static List<PushNotification> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PushNotification> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public static PushNotification findByUuid_First(
			String uuid, OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByUuid_First(
		String uuid, OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public static PushNotification findByUuid_Last(
			String uuid, OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByUuid_Last(
		String uuid, OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the push notifications before and after the current push notification in the ordered set where uuid = &#63;.
	 *
	 * @param notificationId the primary key of the current push notification
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public static PushNotification[] findByUuid_PrevAndNext(
			long notificationId, String uuid,
			OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByUuid_PrevAndNext(
			notificationId, uuid, orderByComparator);
	}

	/**
	 * Removes all the push notifications where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of push notifications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching push notifications
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the push notification where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchPushNotificationException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public static PushNotification findByUUID_G(String uuid, long groupId)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the push notification where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the push notification where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the push notification where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the push notification that was removed
	 */
	public static PushNotification removeByUUID_G(String uuid, long groupId)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of push notifications where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching push notifications
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching push notifications
	 */
	public static List<PushNotification> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of matching push notifications
	 */
	public static List<PushNotification> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notifications
	 */
	public static List<PushNotification> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notifications
	 */
	public static List<PushNotification> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PushNotification> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public static PushNotification findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public static PushNotification findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the push notifications before and after the current push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param notificationId the primary key of the current push notification
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public static PushNotification[] findByUuid_C_PrevAndNext(
			long notificationId, String uuid, long companyId,
			OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByUuid_C_PrevAndNext(
			notificationId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the push notifications where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching push notifications
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the push notifications where type = &#63;.
	 *
	 * @param type the type
	 * @return the matching push notifications
	 */
	public static List<PushNotification> findByType(String type) {
		return getPersistence().findByType(type);
	}

	/**
	 * Returns a range of all the push notifications where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of matching push notifications
	 */
	public static List<PushNotification> findByType(
		String type, int start, int end) {

		return getPersistence().findByType(type, start, end);
	}

	/**
	 * Returns an ordered range of all the push notifications where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notifications
	 */
	public static List<PushNotification> findByType(
		String type, int start, int end,
		OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().findByType(type, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notifications where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notifications
	 */
	public static List<PushNotification> findByType(
		String type, int start, int end,
		OrderByComparator<PushNotification> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByType(
			type, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public static PushNotification findByType_First(
			String type, OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByType_First(type, orderByComparator);
	}

	/**
	 * Returns the first push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByType_First(
		String type, OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().fetchByType_First(type, orderByComparator);
	}

	/**
	 * Returns the last push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public static PushNotification findByType_Last(
			String type, OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByType_Last(type, orderByComparator);
	}

	/**
	 * Returns the last push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchByType_Last(
		String type, OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().fetchByType_Last(type, orderByComparator);
	}

	/**
	 * Returns the push notifications before and after the current push notification in the ordered set where type = &#63;.
	 *
	 * @param notificationId the primary key of the current push notification
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public static PushNotification[] findByType_PrevAndNext(
			long notificationId, String type,
			OrderByComparator<PushNotification> orderByComparator)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByType_PrevAndNext(
			notificationId, type, orderByComparator);
	}

	/**
	 * Removes all the push notifications where type = &#63; from the database.
	 *
	 * @param type the type
	 */
	public static void removeByType(String type) {
		getPersistence().removeByType(type);
	}

	/**
	 * Returns the number of push notifications where type = &#63;.
	 *
	 * @param type the type
	 * @return the number of matching push notifications
	 */
	public static int countByType(String type) {
		return getPersistence().countByType(type);
	}

	/**
	 * Caches the push notification in the entity cache if it is enabled.
	 *
	 * @param pushNotification the push notification
	 */
	public static void cacheResult(PushNotification pushNotification) {
		getPersistence().cacheResult(pushNotification);
	}

	/**
	 * Caches the push notifications in the entity cache if it is enabled.
	 *
	 * @param pushNotifications the push notifications
	 */
	public static void cacheResult(List<PushNotification> pushNotifications) {
		getPersistence().cacheResult(pushNotifications);
	}

	/**
	 * Creates a new push notification with the primary key. Does not add the push notification to the database.
	 *
	 * @param notificationId the primary key for the new push notification
	 * @return the new push notification
	 */
	public static PushNotification create(long notificationId) {
		return getPersistence().create(notificationId);
	}

	/**
	 * Removes the push notification with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification that was removed
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public static PushNotification remove(long notificationId)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().remove(notificationId);
	}

	public static PushNotification updateImpl(
		PushNotification pushNotification) {

		return getPersistence().updateImpl(pushNotification);
	}

	/**
	 * Returns the push notification with the primary key or throws a <code>NoSuchPushNotificationException</code> if it could not be found.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public static PushNotification findByPrimaryKey(long notificationId)
		throws com.push.notification.exception.NoSuchPushNotificationException {

		return getPersistence().findByPrimaryKey(notificationId);
	}

	/**
	 * Returns the push notification with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification, or <code>null</code> if a push notification with the primary key could not be found
	 */
	public static PushNotification fetchByPrimaryKey(long notificationId) {
		return getPersistence().fetchByPrimaryKey(notificationId);
	}

	/**
	 * Returns all the push notifications.
	 *
	 * @return the push notifications
	 */
	public static List<PushNotification> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of push notifications
	 */
	public static List<PushNotification> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of push notifications
	 */
	public static List<PushNotification> findAll(
		int start, int end,
		OrderByComparator<PushNotification> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of push notifications
	 */
	public static List<PushNotification> findAll(
		int start, int end,
		OrderByComparator<PushNotification> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the push notifications from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of push notifications.
	 *
	 * @return the number of push notifications
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static PushNotificationPersistence getPersistence() {
		return _persistence;
	}

	private static volatile PushNotificationPersistence _persistence;

}