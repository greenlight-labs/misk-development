/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PushNotificationReadStatusService}.
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationReadStatusService
 * @generated
 */
public class PushNotificationReadStatusServiceWrapper
	implements PushNotificationReadStatusService,
			   ServiceWrapper<PushNotificationReadStatusService> {

	public PushNotificationReadStatusServiceWrapper(
		PushNotificationReadStatusService pushNotificationReadStatusService) {

		_pushNotificationReadStatusService = pushNotificationReadStatusService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _pushNotificationReadStatusService.getOSGiServiceIdentifier();
	}

	@Override
	public PushNotificationReadStatusService getWrappedService() {
		return _pushNotificationReadStatusService;
	}

	@Override
	public void setWrappedService(
		PushNotificationReadStatusService pushNotificationReadStatusService) {

		_pushNotificationReadStatusService = pushNotificationReadStatusService;
	}

	private PushNotificationReadStatusService
		_pushNotificationReadStatusService;

}