/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.push.notification.model.PushNotificationReadStatus;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the push notification read status service. This utility wraps <code>com.push.notification.service.persistence.impl.PushNotificationReadStatusPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationReadStatusPersistence
 * @generated
 */
public class PushNotificationReadStatusUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(
		PushNotificationReadStatus pushNotificationReadStatus) {

		getPersistence().clearCache(pushNotificationReadStatus);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, PushNotificationReadStatus>
		fetchByPrimaryKeys(Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PushNotificationReadStatus> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PushNotificationReadStatus> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PushNotificationReadStatus> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static PushNotificationReadStatus update(
		PushNotificationReadStatus pushNotificationReadStatus) {

		return getPersistence().update(pushNotificationReadStatus);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static PushNotificationReadStatus update(
		PushNotificationReadStatus pushNotificationReadStatus,
		ServiceContext serviceContext) {

		return getPersistence().update(
			pushNotificationReadStatus, serviceContext);
	}

	/**
	 * Returns all the push notification read statuses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByUuid_First(
			String uuid,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByUuid_First(
		String uuid,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByUuid_Last(
			String uuid,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByUuid_Last(
		String uuid,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus[] findByUuid_PrevAndNext(
			long readStatusId, String uuid,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByUuid_PrevAndNext(
			readStatusId, uuid, orderByComparator);
	}

	/**
	 * Removes all the push notification read statuses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of push notification read statuses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching push notification read statuses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the push notification read statuses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserId(
		long appUserId) {

		return getPersistence().findByAppUserId(appUserId);
	}

	/**
	 * Returns a range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end) {

		return getPersistence().findByAppUserId(appUserId, start, end);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().findByAppUserId(
			appUserId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByAppUserId(
			appUserId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByAppUserId_First(
			long appUserId,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByAppUserId_First(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByAppUserId_First(
		long appUserId,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByAppUserId_First(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByAppUserId_Last(
			long appUserId,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByAppUserId_Last(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByAppUserId_Last(
		long appUserId,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByAppUserId_Last(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus[] findByAppUserId_PrevAndNext(
			long readStatusId, long appUserId,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByAppUserId_PrevAndNext(
			readStatusId, appUserId, orderByComparator);
	}

	/**
	 * Removes all the push notification read statuses where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 */
	public static void removeByAppUserId(long appUserId) {
		getPersistence().removeByAppUserId(appUserId);
	}

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching push notification read statuses
	 */
	public static int countByAppUserId(long appUserId) {
		return getPersistence().countByAppUserId(appUserId);
	}

	/**
	 * Returns all the push notification read statuses where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @return the matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus) {

		return getPersistence().findByReadStatus(readStatus);
	}

	/**
	 * Returns a range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end) {

		return getPersistence().findByReadStatus(readStatus, start, end);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().findByReadStatus(
			readStatus, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByReadStatus(
			readStatus, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByReadStatus_First(
			boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByReadStatus_First(
			readStatus, orderByComparator);
	}

	/**
	 * Returns the first push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByReadStatus_First(
		boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByReadStatus_First(
			readStatus, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByReadStatus_Last(
			boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByReadStatus_Last(
			readStatus, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByReadStatus_Last(
		boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByReadStatus_Last(
			readStatus, orderByComparator);
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus[] findByReadStatus_PrevAndNext(
			long readStatusId, boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByReadStatus_PrevAndNext(
			readStatusId, readStatus, orderByComparator);
	}

	/**
	 * Removes all the push notification read statuses where readStatus = &#63; from the database.
	 *
	 * @param readStatus the read status
	 */
	public static void removeByReadStatus(boolean readStatus) {
		getPersistence().removeByReadStatus(readStatus);
	}

	/**
	 * Returns the number of push notification read statuses where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @return the number of matching push notification read statuses
	 */
	public static int countByReadStatus(boolean readStatus) {
		return getPersistence().countByReadStatus(readStatus);
	}

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or throws a <code>NoSuchReadStatusException</code> if it could not be found.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByAppUserNotification(
			long appUserId, long notificationId)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByAppUserNotification(
			appUserId, notificationId);
	}

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByAppUserNotification(
		long appUserId, long notificationId) {

		return getPersistence().fetchByAppUserNotification(
			appUserId, notificationId);
	}

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByAppUserNotification(
		long appUserId, long notificationId, boolean useFinderCache) {

		return getPersistence().fetchByAppUserNotification(
			appUserId, notificationId, useFinderCache);
	}

	/**
	 * Removes the push notification read status where appUserId = &#63; and notificationId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the push notification read status that was removed
	 */
	public static PushNotificationReadStatus removeByAppUserNotification(
			long appUserId, long notificationId)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().removeByAppUserNotification(
			appUserId, notificationId);
	}

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63; and notificationId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the number of matching push notification read statuses
	 */
	public static int countByAppUserNotification(
		long appUserId, long notificationId) {

		return getPersistence().countByAppUserNotification(
			appUserId, notificationId);
	}

	/**
	 * Returns all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @return the matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus) {

		return getPersistence().findByAppUserReadStatus(appUserId, readStatus);
	}

	/**
	 * Returns a range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end) {

		return getPersistence().findByAppUserReadStatus(
			appUserId, readStatus, start, end);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().findByAppUserReadStatus(
			appUserId, readStatus, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByAppUserReadStatus(
			appUserId, readStatus, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByAppUserReadStatus_First(
			long appUserId, boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByAppUserReadStatus_First(
			appUserId, readStatus, orderByComparator);
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByAppUserReadStatus_First(
		long appUserId, boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByAppUserReadStatus_First(
			appUserId, readStatus, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus findByAppUserReadStatus_Last(
			long appUserId, boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByAppUserReadStatus_Last(
			appUserId, readStatus, orderByComparator);
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public static PushNotificationReadStatus fetchByAppUserReadStatus_Last(
		long appUserId, boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().fetchByAppUserReadStatus_Last(
			appUserId, readStatus, orderByComparator);
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus[]
			findByAppUserReadStatus_PrevAndNext(
				long readStatusId, long appUserId, boolean readStatus,
				OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByAppUserReadStatus_PrevAndNext(
			readStatusId, appUserId, readStatus, orderByComparator);
	}

	/**
	 * Removes all the push notification read statuses where appUserId = &#63; and readStatus = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 */
	public static void removeByAppUserReadStatus(
		long appUserId, boolean readStatus) {

		getPersistence().removeByAppUserReadStatus(appUserId, readStatus);
	}

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @return the number of matching push notification read statuses
	 */
	public static int countByAppUserReadStatus(
		long appUserId, boolean readStatus) {

		return getPersistence().countByAppUserReadStatus(appUserId, readStatus);
	}

	/**
	 * Caches the push notification read status in the entity cache if it is enabled.
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 */
	public static void cacheResult(
		PushNotificationReadStatus pushNotificationReadStatus) {

		getPersistence().cacheResult(pushNotificationReadStatus);
	}

	/**
	 * Caches the push notification read statuses in the entity cache if it is enabled.
	 *
	 * @param pushNotificationReadStatuses the push notification read statuses
	 */
	public static void cacheResult(
		List<PushNotificationReadStatus> pushNotificationReadStatuses) {

		getPersistence().cacheResult(pushNotificationReadStatuses);
	}

	/**
	 * Creates a new push notification read status with the primary key. Does not add the push notification read status to the database.
	 *
	 * @param readStatusId the primary key for the new push notification read status
	 * @return the new push notification read status
	 */
	public static PushNotificationReadStatus create(long readStatusId) {
		return getPersistence().create(readStatusId);
	}

	/**
	 * Removes the push notification read status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status that was removed
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus remove(long readStatusId)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().remove(readStatusId);
	}

	public static PushNotificationReadStatus updateImpl(
		PushNotificationReadStatus pushNotificationReadStatus) {

		return getPersistence().updateImpl(pushNotificationReadStatus);
	}

	/**
	 * Returns the push notification read status with the primary key or throws a <code>NoSuchReadStatusException</code> if it could not be found.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus findByPrimaryKey(long readStatusId)
		throws com.push.notification.exception.NoSuchReadStatusException {

		return getPersistence().findByPrimaryKey(readStatusId);
	}

	/**
	 * Returns the push notification read status with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status, or <code>null</code> if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus fetchByPrimaryKey(
		long readStatusId) {

		return getPersistence().fetchByPrimaryKey(readStatusId);
	}

	/**
	 * Returns all the push notification read statuses.
	 *
	 * @return the push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findAll(
		int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of push notification read statuses
	 */
	public static List<PushNotificationReadStatus> findAll(
		int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the push notification read statuses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of push notification read statuses.
	 *
	 * @return the number of push notification read statuses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static PushNotificationReadStatusPersistence getPersistence() {
		return _persistence;
	}

	private static volatile PushNotificationReadStatusPersistence _persistence;

}