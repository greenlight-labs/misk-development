/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.push.notification.model.PushNotificationReadStatus;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for PushNotificationReadStatus. This utility wraps
 * <code>com.push.notification.service.impl.PushNotificationReadStatusLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationReadStatusLocalService
 * @generated
 */
public class PushNotificationReadStatusLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.push.notification.service.impl.PushNotificationReadStatusLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static PushNotificationReadStatus addEntry(
			PushNotificationReadStatus orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.push.notification.exception.
			PushNotificationReadStatusValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the push notification read status to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 * @return the push notification read status that was added
	 */
	public static PushNotificationReadStatus addPushNotificationReadStatus(
		PushNotificationReadStatus pushNotificationReadStatus) {

		return getService().addPushNotificationReadStatus(
			pushNotificationReadStatus);
	}

	public static int countByAppUserId(long appUserId) {
		return getService().countByAppUserId(appUserId);
	}

	public static int countByAppUserReadStatus(
		long appUserId, boolean readStatus) {

		return getService().countByAppUserReadStatus(appUserId, readStatus);
	}

	public static int countByReadStatus(boolean readStatus) {
		return getService().countByReadStatus(readStatus);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new push notification read status with the primary key. Does not add the push notification read status to the database.
	 *
	 * @param readStatusId the primary key for the new push notification read status
	 * @return the new push notification read status
	 */
	public static PushNotificationReadStatus createPushNotificationReadStatus(
		long readStatusId) {

		return getService().createPushNotificationReadStatus(readStatusId);
	}

	public static PushNotificationReadStatus deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the push notification read status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status that was removed
	 * @throws PortalException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus deletePushNotificationReadStatus(
			long readStatusId)
		throws PortalException {

		return getService().deletePushNotificationReadStatus(readStatusId);
	}

	/**
	 * Deletes the push notification read status from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 * @return the push notification read status that was removed
	 */
	public static PushNotificationReadStatus deletePushNotificationReadStatus(
		PushNotificationReadStatus pushNotificationReadStatus) {

		return getService().deletePushNotificationReadStatus(
			pushNotificationReadStatus);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static PushNotificationReadStatus fetchByAppUserNotification(
		long appUserId, long notificationId) {

		return getService().fetchByAppUserNotification(
			appUserId, notificationId);
	}

	public static PushNotificationReadStatus fetchPushNotificationReadStatus(
		long readStatusId) {

		return getService().fetchPushNotificationReadStatus(readStatusId);
	}

	public static List<PushNotificationReadStatus> findByAppUserId(
		long appUserId) {

		return getService().findByAppUserId(appUserId);
	}

	public static List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end) {

		return getService().findByAppUserId(appUserId, start, end);
	}

	public static List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end,
		OrderByComparator<PushNotificationReadStatus> obc) {

		return getService().findByAppUserId(appUserId, start, end, obc);
	}

	public static List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus) {

		return getService().findByReadStatus(readStatus);
	}

	public static List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end) {

		return getService().findByReadStatus(readStatus, start, end);
	}

	public static List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> obc) {

		return getService().findByReadStatus(readStatus, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static PushNotificationReadStatus getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the push notification read status with the primary key.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status
	 * @throws PortalException if a push notification read status with the primary key could not be found
	 */
	public static PushNotificationReadStatus getPushNotificationReadStatus(
			long readStatusId)
		throws PortalException {

		return getService().getPushNotificationReadStatus(readStatusId);
	}

	/**
	 * Returns a range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of push notification read statuses
	 */
	public static List<PushNotificationReadStatus>
		getPushNotificationReadStatuses(int start, int end) {

		return getService().getPushNotificationReadStatuses(start, end);
	}

	/**
	 * Returns the number of push notification read statuses.
	 *
	 * @return the number of push notification read statuses
	 */
	public static int getPushNotificationReadStatusesCount() {
		return getService().getPushNotificationReadStatusesCount();
	}

	public static PushNotificationReadStatus
			getPushNotificationReadStatusFromRequest(
				long primaryKey, javax.portlet.PortletRequest request)
		throws com.push.notification.exception.
			PushNotificationReadStatusValidateException,
			   javax.portlet.PortletException {

		return getService().getPushNotificationReadStatusFromRequest(
			primaryKey, request);
	}

	public static PushNotificationReadStatus updateEntry(
			PushNotificationReadStatus orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.push.notification.exception.
			PushNotificationReadStatusValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the push notification read status in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 * @return the push notification read status that was updated
	 */
	public static PushNotificationReadStatus updatePushNotificationReadStatus(
		PushNotificationReadStatus pushNotificationReadStatus) {

		return getService().updatePushNotificationReadStatus(
			pushNotificationReadStatus);
	}

	public static PushNotificationReadStatusLocalService getService() {
		return _service;
	}

	private static volatile PushNotificationReadStatusLocalService _service;

}