/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.push.notification.exception.PushNotificationValidateException;
import com.push.notification.model.PushNotification;

import java.io.Serializable;

import java.util.*;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for PushNotification. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface PushNotificationLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>com.push.notification.service.impl.PushNotificationLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the push notification local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link PushNotificationLocalServiceUtil} if injection and service tracking are not available.
	 */
	public PushNotification addEntry(
			PushNotification orgEntry, ServiceContext serviceContext)
		throws PortalException, PushNotificationValidateException;

	/**
	 * Adds the push notification to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public PushNotification addPushNotification(
		PushNotification pushNotification);

	public int countByType(String type);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Creates a new push notification with the primary key. Does not add the push notification to the database.
	 *
	 * @param notificationId the primary key for the new push notification
	 * @return the new push notification
	 */
	@Transactional(enabled = false)
	public PushNotification createPushNotification(long notificationId);

	public PushNotification deleteEntry(long primaryKey) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	/**
	 * Deletes the push notification with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification that was removed
	 * @throws PortalException if a push notification with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public PushNotification deletePushNotification(long notificationId)
		throws PortalException;

	/**
	 * Deletes the push notification from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public PushNotification deletePushNotification(
		PushNotification pushNotification);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PushNotification fetchPushNotification(long notificationId);

	/**
	 * Returns the push notification matching the UUID and group.
	 *
	 * @param uuid the push notification's UUID
	 * @param groupId the primary key of the group
	 * @return the matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PushNotification fetchPushNotificationByUuidAndGroupId(
		String uuid, long groupId);

	public List<PushNotification> findByNotificationIds(
		long[] notificationIds, int start, int end);

	public List<PushNotification> findByType(String type);

	public List<PushNotification> findByType(String type, int start, int end);

	public List<PushNotification> findByType(
		String type, int start, int end,
		OrderByComparator<PushNotification> orderByComparator);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PushNotification getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Returns the push notification with the primary key.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification
	 * @throws PortalException if a push notification with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PushNotification getPushNotification(long notificationId)
		throws PortalException;

	/**
	 * Returns the push notification matching the UUID and group.
	 *
	 * @param uuid the push notification's UUID
	 * @param groupId the primary key of the group
	 * @return the matching push notification
	 * @throws PortalException if a matching push notification could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PushNotification getPushNotificationByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PushNotification getPushNotificationFromRequest(
			long primaryKey, PortletRequest request)
		throws PortletException, PushNotificationValidateException;

	/**
	 * Returns a range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of push notifications
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<PushNotification> getPushNotifications(int start, int end);

	/**
	 * Returns all the push notifications matching the UUID and company.
	 *
	 * @param uuid the UUID of the push notifications
	 * @param companyId the primary key of the company
	 * @return the matching push notifications, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<PushNotification> getPushNotificationsByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of push notifications matching the UUID and company.
	 *
	 * @param uuid the UUID of the push notifications
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching push notifications, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<PushNotification> getPushNotificationsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PushNotification> orderByComparator);

	/**
	 * Returns the number of push notifications.
	 *
	 * @return the number of push notifications
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getPushNotificationsCount();

	public void saveNotification(
		String type, long typeId, String title, String body,
		JSONObject typeAdditionalPayload);

	public void sendNotification(JSONObject jsonObject, List<String> tokens)
		throws PortalException, SystemException;

	public PushNotification updateEntry(
			PushNotification orgEntry, ServiceContext serviceContext)
		throws PortalException, PushNotificationValidateException;

	/**
	 * Updates the push notification in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public PushNotification updatePushNotification(
		PushNotification pushNotification);

}