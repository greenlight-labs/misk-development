/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PushNotification}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotification
 * @generated
 */
public class PushNotificationWrapper
	extends BaseModelWrapper<PushNotification>
	implements ModelWrapper<PushNotification>, PushNotification {

	public PushNotificationWrapper(PushNotification pushNotification) {
		super(pushNotification);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("notificationId", getNotificationId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("type", getType());
		attributes.put("typeId", getTypeId());
		attributes.put("title", getTitle());
		attributes.put("body", getBody());
		attributes.put("priority", getPriority());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long notificationId = (Long)attributes.get("notificationId");

		if (notificationId != null) {
			setNotificationId(notificationId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		Long typeId = (Long)attributes.get("typeId");

		if (typeId != null) {
			setTypeId(typeId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String body = (String)attributes.get("body");

		if (body != null) {
			setBody(body);
		}

		String priority = (String)attributes.get("priority");

		if (priority != null) {
			setPriority(priority);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the body of this push notification.
	 *
	 * @return the body of this push notification
	 */
	@Override
	public String getBody() {
		return model.getBody();
	}

	/**
	 * Returns the localized body of this push notification in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized body of this push notification
	 */
	@Override
	public String getBody(java.util.Locale locale) {
		return model.getBody(locale);
	}

	/**
	 * Returns the localized body of this push notification in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized body of this push notification. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBody(java.util.Locale locale, boolean useDefault) {
		return model.getBody(locale, useDefault);
	}

	/**
	 * Returns the localized body of this push notification in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized body of this push notification
	 */
	@Override
	public String getBody(String languageId) {
		return model.getBody(languageId);
	}

	/**
	 * Returns the localized body of this push notification in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized body of this push notification
	 */
	@Override
	public String getBody(String languageId, boolean useDefault) {
		return model.getBody(languageId, useDefault);
	}

	@Override
	public String getBodyCurrentLanguageId() {
		return model.getBodyCurrentLanguageId();
	}

	@Override
	public String getBodyCurrentValue() {
		return model.getBodyCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized bodies of this push notification.
	 *
	 * @return the locales and localized bodies of this push notification
	 */
	@Override
	public Map<java.util.Locale, String> getBodyMap() {
		return model.getBodyMap();
	}

	/**
	 * Returns the company ID of this push notification.
	 *
	 * @return the company ID of this push notification
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this push notification.
	 *
	 * @return the create date of this push notification
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the group ID of this push notification.
	 *
	 * @return the group ID of this push notification
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this push notification.
	 *
	 * @return the modified date of this push notification
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the notification ID of this push notification.
	 *
	 * @return the notification ID of this push notification
	 */
	@Override
	public long getNotificationId() {
		return model.getNotificationId();
	}

	/**
	 * Returns the primary key of this push notification.
	 *
	 * @return the primary key of this push notification
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the priority of this push notification.
	 *
	 * @return the priority of this push notification
	 */
	@Override
	public String getPriority() {
		return model.getPriority();
	}

	/**
	 * Returns the title of this push notification.
	 *
	 * @return the title of this push notification
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this push notification in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this push notification
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this push notification in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this push notification. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this push notification in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this push notification
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this push notification in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this push notification
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this push notification.
	 *
	 * @return the locales and localized titles of this push notification
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the type of this push notification.
	 *
	 * @return the type of this push notification
	 */
	@Override
	public String getType() {
		return model.getType();
	}

	/**
	 * Returns the type ID of this push notification.
	 *
	 * @return the type ID of this push notification
	 */
	@Override
	public long getTypeId() {
		return model.getTypeId();
	}

	/**
	 * Returns the uuid of this push notification.
	 *
	 * @return the uuid of this push notification
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the body of this push notification.
	 *
	 * @param body the body of this push notification
	 */
	@Override
	public void setBody(String body) {
		model.setBody(body);
	}

	/**
	 * Sets the localized body of this push notification in the language.
	 *
	 * @param body the localized body of this push notification
	 * @param locale the locale of the language
	 */
	@Override
	public void setBody(String body, java.util.Locale locale) {
		model.setBody(body, locale);
	}

	/**
	 * Sets the localized body of this push notification in the language, and sets the default locale.
	 *
	 * @param body the localized body of this push notification
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBody(
		String body, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setBody(body, locale, defaultLocale);
	}

	@Override
	public void setBodyCurrentLanguageId(String languageId) {
		model.setBodyCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized bodies of this push notification from the map of locales and localized bodies.
	 *
	 * @param bodyMap the locales and localized bodies of this push notification
	 */
	@Override
	public void setBodyMap(Map<java.util.Locale, String> bodyMap) {
		model.setBodyMap(bodyMap);
	}

	/**
	 * Sets the localized bodies of this push notification from the map of locales and localized bodies, and sets the default locale.
	 *
	 * @param bodyMap the locales and localized bodies of this push notification
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBodyMap(
		Map<java.util.Locale, String> bodyMap, java.util.Locale defaultLocale) {

		model.setBodyMap(bodyMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this push notification.
	 *
	 * @param companyId the company ID of this push notification
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this push notification.
	 *
	 * @param createDate the create date of this push notification
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this push notification.
	 *
	 * @param groupId the group ID of this push notification
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this push notification.
	 *
	 * @param modifiedDate the modified date of this push notification
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the notification ID of this push notification.
	 *
	 * @param notificationId the notification ID of this push notification
	 */
	@Override
	public void setNotificationId(long notificationId) {
		model.setNotificationId(notificationId);
	}

	/**
	 * Sets the primary key of this push notification.
	 *
	 * @param primaryKey the primary key of this push notification
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the priority of this push notification.
	 *
	 * @param priority the priority of this push notification
	 */
	@Override
	public void setPriority(String priority) {
		model.setPriority(priority);
	}

	/**
	 * Sets the title of this push notification.
	 *
	 * @param title the title of this push notification
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this push notification in the language.
	 *
	 * @param title the localized title of this push notification
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this push notification in the language, and sets the default locale.
	 *
	 * @param title the localized title of this push notification
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this push notification from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this push notification
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this push notification from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this push notification
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the type of this push notification.
	 *
	 * @param type the type of this push notification
	 */
	@Override
	public void setType(String type) {
		model.setType(type);
	}

	/**
	 * Sets the type ID of this push notification.
	 *
	 * @param typeId the type ID of this push notification
	 */
	@Override
	public void setTypeId(long typeId) {
		model.setTypeId(typeId);
	}

	/**
	 * Sets the uuid of this push notification.
	 *
	 * @param uuid the uuid of this push notification
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected PushNotificationWrapper wrap(PushNotification pushNotification) {
		return new PushNotificationWrapper(pushNotification);
	}

}