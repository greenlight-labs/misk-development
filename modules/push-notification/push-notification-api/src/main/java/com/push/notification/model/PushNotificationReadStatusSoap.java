/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.push.notification.service.http.PushNotificationReadStatusServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class PushNotificationReadStatusSoap implements Serializable {

	public static PushNotificationReadStatusSoap toSoapModel(
		PushNotificationReadStatus model) {

		PushNotificationReadStatusSoap soapModel =
			new PushNotificationReadStatusSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setReadStatusId(model.getReadStatusId());
		soapModel.setNotificationId(model.getNotificationId());
		soapModel.setAppUserId(model.getAppUserId());
		soapModel.setReadStatus(model.isReadStatus());

		return soapModel;
	}

	public static PushNotificationReadStatusSoap[] toSoapModels(
		PushNotificationReadStatus[] models) {

		PushNotificationReadStatusSoap[] soapModels =
			new PushNotificationReadStatusSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PushNotificationReadStatusSoap[][] toSoapModels(
		PushNotificationReadStatus[][] models) {

		PushNotificationReadStatusSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new PushNotificationReadStatusSoap
					[models.length][models[0].length];
		}
		else {
			soapModels = new PushNotificationReadStatusSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PushNotificationReadStatusSoap[] toSoapModels(
		List<PushNotificationReadStatus> models) {

		List<PushNotificationReadStatusSoap> soapModels =
			new ArrayList<PushNotificationReadStatusSoap>(models.size());

		for (PushNotificationReadStatus model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(
			new PushNotificationReadStatusSoap[soapModels.size()]);
	}

	public PushNotificationReadStatusSoap() {
	}

	public long getPrimaryKey() {
		return _readStatusId;
	}

	public void setPrimaryKey(long pk) {
		setReadStatusId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getReadStatusId() {
		return _readStatusId;
	}

	public void setReadStatusId(long readStatusId) {
		_readStatusId = readStatusId;
	}

	public long getNotificationId() {
		return _notificationId;
	}

	public void setNotificationId(long notificationId) {
		_notificationId = notificationId;
	}

	public long getAppUserId() {
		return _appUserId;
	}

	public void setAppUserId(long appUserId) {
		_appUserId = appUserId;
	}

	public boolean getReadStatus() {
		return _readStatus;
	}

	public boolean isReadStatus() {
		return _readStatus;
	}

	public void setReadStatus(boolean readStatus) {
		_readStatus = readStatus;
	}

	private String _uuid;
	private long _readStatusId;
	private long _notificationId;
	private long _appUserId;
	private boolean _readStatus;

}