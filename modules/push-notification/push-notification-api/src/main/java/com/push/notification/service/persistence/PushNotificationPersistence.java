/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.push.notification.exception.NoSuchPushNotificationException;
import com.push.notification.model.PushNotification;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the push notification service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationUtil
 * @generated
 */
@ProviderType
public interface PushNotificationPersistence
	extends BasePersistence<PushNotification> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PushNotificationUtil} to access the push notification persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the push notifications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid(String uuid);

	/**
	 * Returns a range of all the push notifications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public PushNotification findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public PushNotification findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns the push notifications before and after the current push notification in the ordered set where uuid = &#63;.
	 *
	 * @param notificationId the primary key of the current push notification
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public PushNotification[] findByUuid_PrevAndNext(
			long notificationId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Removes all the push notifications where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of push notifications where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching push notifications
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the push notification where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchPushNotificationException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public PushNotification findByUUID_G(String uuid, long groupId)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the push notification where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the push notification where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the push notification where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the push notification that was removed
	 */
	public PushNotification removeByUUID_G(String uuid, long groupId)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the number of push notifications where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching push notifications
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns an ordered range of all the push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notifications
	 */
	public java.util.List<PushNotification> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public PushNotification findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the first push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public PushNotification findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the last push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns the push notifications before and after the current push notification in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param notificationId the primary key of the current push notification
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public PushNotification[] findByUuid_C_PrevAndNext(
			long notificationId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Removes all the push notifications where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of push notifications where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching push notifications
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the push notifications where type = &#63;.
	 *
	 * @param type the type
	 * @return the matching push notifications
	 */
	public java.util.List<PushNotification> findByType(String type);

	/**
	 * Returns a range of all the push notifications where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of matching push notifications
	 */
	public java.util.List<PushNotification> findByType(
		String type, int start, int end);

	/**
	 * Returns an ordered range of all the push notifications where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notifications
	 */
	public java.util.List<PushNotification> findByType(
		String type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns an ordered range of all the push notifications where type = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param type the type
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notifications
	 */
	public java.util.List<PushNotification> findByType(
		String type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public PushNotification findByType_First(
			String type,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the first push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByType_First(
		String type,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns the last push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification
	 * @throws NoSuchPushNotificationException if a matching push notification could not be found
	 */
	public PushNotification findByType_Last(
			String type,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the last push notification in the ordered set where type = &#63;.
	 *
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public PushNotification fetchByType_Last(
		String type,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns the push notifications before and after the current push notification in the ordered set where type = &#63;.
	 *
	 * @param notificationId the primary key of the current push notification
	 * @param type the type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public PushNotification[] findByType_PrevAndNext(
			long notificationId, String type,
			com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
				orderByComparator)
		throws NoSuchPushNotificationException;

	/**
	 * Removes all the push notifications where type = &#63; from the database.
	 *
	 * @param type the type
	 */
	public void removeByType(String type);

	/**
	 * Returns the number of push notifications where type = &#63;.
	 *
	 * @param type the type
	 * @return the number of matching push notifications
	 */
	public int countByType(String type);

	/**
	 * Caches the push notification in the entity cache if it is enabled.
	 *
	 * @param pushNotification the push notification
	 */
	public void cacheResult(PushNotification pushNotification);

	/**
	 * Caches the push notifications in the entity cache if it is enabled.
	 *
	 * @param pushNotifications the push notifications
	 */
	public void cacheResult(java.util.List<PushNotification> pushNotifications);

	/**
	 * Creates a new push notification with the primary key. Does not add the push notification to the database.
	 *
	 * @param notificationId the primary key for the new push notification
	 * @return the new push notification
	 */
	public PushNotification create(long notificationId);

	/**
	 * Removes the push notification with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification that was removed
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public PushNotification remove(long notificationId)
		throws NoSuchPushNotificationException;

	public PushNotification updateImpl(PushNotification pushNotification);

	/**
	 * Returns the push notification with the primary key or throws a <code>NoSuchPushNotificationException</code> if it could not be found.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification
	 * @throws NoSuchPushNotificationException if a push notification with the primary key could not be found
	 */
	public PushNotification findByPrimaryKey(long notificationId)
		throws NoSuchPushNotificationException;

	/**
	 * Returns the push notification with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification, or <code>null</code> if a push notification with the primary key could not be found
	 */
	public PushNotification fetchByPrimaryKey(long notificationId);

	/**
	 * Returns all the push notifications.
	 *
	 * @return the push notifications
	 */
	public java.util.List<PushNotification> findAll();

	/**
	 * Returns a range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of push notifications
	 */
	public java.util.List<PushNotification> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of push notifications
	 */
	public java.util.List<PushNotification> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator);

	/**
	 * Returns an ordered range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of push notifications
	 */
	public java.util.List<PushNotification> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PushNotification>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the push notifications from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of push notifications.
	 *
	 * @return the number of push notifications
	 */
	public int countAll();

}