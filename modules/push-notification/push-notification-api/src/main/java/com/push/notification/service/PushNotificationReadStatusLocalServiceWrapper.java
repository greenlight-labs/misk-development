/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PushNotificationReadStatusLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationReadStatusLocalService
 * @generated
 */
public class PushNotificationReadStatusLocalServiceWrapper
	implements PushNotificationReadStatusLocalService,
			   ServiceWrapper<PushNotificationReadStatusLocalService> {

	public PushNotificationReadStatusLocalServiceWrapper(
		PushNotificationReadStatusLocalService
			pushNotificationReadStatusLocalService) {

		_pushNotificationReadStatusLocalService =
			pushNotificationReadStatusLocalService;
	}

	@Override
	public com.push.notification.model.PushNotificationReadStatus addEntry(
			com.push.notification.model.PushNotificationReadStatus orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.push.notification.exception.
				   PushNotificationReadStatusValidateException {

		return _pushNotificationReadStatusLocalService.addEntry(
			orgEntry, serviceContext);
	}

	/**
	 * Adds the push notification read status to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 * @return the push notification read status that was added
	 */
	@Override
	public com.push.notification.model.PushNotificationReadStatus
		addPushNotificationReadStatus(
			com.push.notification.model.PushNotificationReadStatus
				pushNotificationReadStatus) {

		return _pushNotificationReadStatusLocalService.
			addPushNotificationReadStatus(pushNotificationReadStatus);
	}

	@Override
	public int countByAppUserId(long appUserId) {
		return _pushNotificationReadStatusLocalService.countByAppUserId(
			appUserId);
	}

	@Override
	public int countByAppUserReadStatus(long appUserId, boolean readStatus) {
		return _pushNotificationReadStatusLocalService.countByAppUserReadStatus(
			appUserId, readStatus);
	}

	@Override
	public int countByReadStatus(boolean readStatus) {
		return _pushNotificationReadStatusLocalService.countByReadStatus(
			readStatus);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationReadStatusLocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Creates a new push notification read status with the primary key. Does not add the push notification read status to the database.
	 *
	 * @param readStatusId the primary key for the new push notification read status
	 * @return the new push notification read status
	 */
	@Override
	public com.push.notification.model.PushNotificationReadStatus
		createPushNotificationReadStatus(long readStatusId) {

		return _pushNotificationReadStatusLocalService.
			createPushNotificationReadStatus(readStatusId);
	}

	@Override
	public com.push.notification.model.PushNotificationReadStatus deleteEntry(
			long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationReadStatusLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationReadStatusLocalService.deletePersistedModel(
			persistedModel);
	}

	/**
	 * Deletes the push notification read status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status that was removed
	 * @throws PortalException if a push notification read status with the primary key could not be found
	 */
	@Override
	public com.push.notification.model.PushNotificationReadStatus
			deletePushNotificationReadStatus(long readStatusId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationReadStatusLocalService.
			deletePushNotificationReadStatus(readStatusId);
	}

	/**
	 * Deletes the push notification read status from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 * @return the push notification read status that was removed
	 */
	@Override
	public com.push.notification.model.PushNotificationReadStatus
		deletePushNotificationReadStatus(
			com.push.notification.model.PushNotificationReadStatus
				pushNotificationReadStatus) {

		return _pushNotificationReadStatusLocalService.
			deletePushNotificationReadStatus(pushNotificationReadStatus);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _pushNotificationReadStatusLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _pushNotificationReadStatusLocalService.dynamicQuery(
			dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _pushNotificationReadStatusLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _pushNotificationReadStatusLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _pushNotificationReadStatusLocalService.dynamicQueryCount(
			dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _pushNotificationReadStatusLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.push.notification.model.PushNotificationReadStatus
		fetchByAppUserNotification(long appUserId, long notificationId) {

		return _pushNotificationReadStatusLocalService.
			fetchByAppUserNotification(appUserId, notificationId);
	}

	@Override
	public com.push.notification.model.PushNotificationReadStatus
		fetchPushNotificationReadStatus(long readStatusId) {

		return _pushNotificationReadStatusLocalService.
			fetchPushNotificationReadStatus(readStatusId);
	}

	@Override
	public java.util.List
		<com.push.notification.model.PushNotificationReadStatus>
			findByAppUserId(long appUserId) {

		return _pushNotificationReadStatusLocalService.findByAppUserId(
			appUserId);
	}

	@Override
	public java.util.List
		<com.push.notification.model.PushNotificationReadStatus>
			findByAppUserId(long appUserId, int start, int end) {

		return _pushNotificationReadStatusLocalService.findByAppUserId(
			appUserId, start, end);
	}

	@Override
	public java.util.List
		<com.push.notification.model.PushNotificationReadStatus>
			findByAppUserId(
				long appUserId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.push.notification.model.PushNotificationReadStatus>
						obc) {

		return _pushNotificationReadStatusLocalService.findByAppUserId(
			appUserId, start, end, obc);
	}

	@Override
	public java.util.List
		<com.push.notification.model.PushNotificationReadStatus>
			findByReadStatus(boolean readStatus) {

		return _pushNotificationReadStatusLocalService.findByReadStatus(
			readStatus);
	}

	@Override
	public java.util.List
		<com.push.notification.model.PushNotificationReadStatus>
			findByReadStatus(boolean readStatus, int start, int end) {

		return _pushNotificationReadStatusLocalService.findByReadStatus(
			readStatus, start, end);
	}

	@Override
	public java.util.List
		<com.push.notification.model.PushNotificationReadStatus>
			findByReadStatus(
				boolean readStatus, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.push.notification.model.PushNotificationReadStatus>
						obc) {

		return _pushNotificationReadStatusLocalService.findByReadStatus(
			readStatus, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _pushNotificationReadStatusLocalService.
			getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _pushNotificationReadStatusLocalService.
			getIndexableActionableDynamicQuery();
	}

	@Override
	public com.push.notification.model.PushNotificationReadStatus getNewObject(
		long primaryKey) {

		return _pushNotificationReadStatusLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _pushNotificationReadStatusLocalService.
			getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationReadStatusLocalService.getPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Returns the push notification read status with the primary key.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status
	 * @throws PortalException if a push notification read status with the primary key could not be found
	 */
	@Override
	public com.push.notification.model.PushNotificationReadStatus
			getPushNotificationReadStatus(long readStatusId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _pushNotificationReadStatusLocalService.
			getPushNotificationReadStatus(readStatusId);
	}

	/**
	 * Returns a range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of push notification read statuses
	 */
	@Override
	public java.util.List
		<com.push.notification.model.PushNotificationReadStatus>
			getPushNotificationReadStatuses(int start, int end) {

		return _pushNotificationReadStatusLocalService.
			getPushNotificationReadStatuses(start, end);
	}

	/**
	 * Returns the number of push notification read statuses.
	 *
	 * @return the number of push notification read statuses
	 */
	@Override
	public int getPushNotificationReadStatusesCount() {
		return _pushNotificationReadStatusLocalService.
			getPushNotificationReadStatusesCount();
	}

	@Override
	public com.push.notification.model.PushNotificationReadStatus
			getPushNotificationReadStatusFromRequest(
				long primaryKey, javax.portlet.PortletRequest request)
		throws com.push.notification.exception.
			PushNotificationReadStatusValidateException,
			   javax.portlet.PortletException {

		return _pushNotificationReadStatusLocalService.
			getPushNotificationReadStatusFromRequest(primaryKey, request);
	}

	@Override
	public com.push.notification.model.PushNotificationReadStatus updateEntry(
			com.push.notification.model.PushNotificationReadStatus orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.push.notification.exception.
				   PushNotificationReadStatusValidateException {

		return _pushNotificationReadStatusLocalService.updateEntry(
			orgEntry, serviceContext);
	}

	/**
	 * Updates the push notification read status in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationReadStatusLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 * @return the push notification read status that was updated
	 */
	@Override
	public com.push.notification.model.PushNotificationReadStatus
		updatePushNotificationReadStatus(
			com.push.notification.model.PushNotificationReadStatus
				pushNotificationReadStatus) {

		return _pushNotificationReadStatusLocalService.
			updatePushNotificationReadStatus(pushNotificationReadStatus);
	}

	@Override
	public PushNotificationReadStatusLocalService getWrappedService() {
		return _pushNotificationReadStatusLocalService;
	}

	@Override
	public void setWrappedService(
		PushNotificationReadStatusLocalService
			pushNotificationReadStatusLocalService) {

		_pushNotificationReadStatusLocalService =
			pushNotificationReadStatusLocalService;
	}

	private PushNotificationReadStatusLocalService
		_pushNotificationReadStatusLocalService;

}