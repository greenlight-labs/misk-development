/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.push.notification.model.PushNotification;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for PushNotification. This utility wraps
 * <code>com.push.notification.service.impl.PushNotificationLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationLocalService
 * @generated
 */
public class PushNotificationLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.push.notification.service.impl.PushNotificationLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static PushNotification addEntry(
			PushNotification orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.push.notification.exception.
			PushNotificationValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the push notification to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was added
	 */
	public static PushNotification addPushNotification(
		PushNotification pushNotification) {

		return getService().addPushNotification(pushNotification);
	}

	public static int countByType(String type) {
		return getService().countByType(type);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new push notification with the primary key. Does not add the push notification to the database.
	 *
	 * @param notificationId the primary key for the new push notification
	 * @return the new push notification
	 */
	public static PushNotification createPushNotification(long notificationId) {
		return getService().createPushNotification(notificationId);
	}

	public static PushNotification deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the push notification with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification that was removed
	 * @throws PortalException if a push notification with the primary key could not be found
	 */
	public static PushNotification deletePushNotification(long notificationId)
		throws PortalException {

		return getService().deletePushNotification(notificationId);
	}

	/**
	 * Deletes the push notification from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was removed
	 */
	public static PushNotification deletePushNotification(
		PushNotification pushNotification) {

		return getService().deletePushNotification(pushNotification);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static PushNotification fetchPushNotification(long notificationId) {
		return getService().fetchPushNotification(notificationId);
	}

	/**
	 * Returns the push notification matching the UUID and group.
	 *
	 * @param uuid the push notification's UUID
	 * @param groupId the primary key of the group
	 * @return the matching push notification, or <code>null</code> if a matching push notification could not be found
	 */
	public static PushNotification fetchPushNotificationByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchPushNotificationByUuidAndGroupId(
			uuid, groupId);
	}

	public static List<PushNotification> findByNotificationIds(
		long[] notificationIds, int start, int end) {

		return getService().findByNotificationIds(notificationIds, start, end);
	}

	public static List<PushNotification> findByType(String type) {
		return getService().findByType(type);
	}

	public static List<PushNotification> findByType(
		String type, int start, int end) {

		return getService().findByType(type, start, end);
	}

	public static List<PushNotification> findByType(
		String type, int start, int end,
		OrderByComparator<PushNotification> orderByComparator) {

		return getService().findByType(type, start, end, orderByComparator);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static PushNotification getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the push notification with the primary key.
	 *
	 * @param notificationId the primary key of the push notification
	 * @return the push notification
	 * @throws PortalException if a push notification with the primary key could not be found
	 */
	public static PushNotification getPushNotification(long notificationId)
		throws PortalException {

		return getService().getPushNotification(notificationId);
	}

	/**
	 * Returns the push notification matching the UUID and group.
	 *
	 * @param uuid the push notification's UUID
	 * @param groupId the primary key of the group
	 * @return the matching push notification
	 * @throws PortalException if a matching push notification could not be found
	 */
	public static PushNotification getPushNotificationByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getPushNotificationByUuidAndGroupId(uuid, groupId);
	}

	public static PushNotification getPushNotificationFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws com.push.notification.exception.
			PushNotificationValidateException,
			   javax.portlet.PortletException {

		return getService().getPushNotificationFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the push notifications.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.push.notification.model.impl.PushNotificationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @return the range of push notifications
	 */
	public static List<PushNotification> getPushNotifications(
		int start, int end) {

		return getService().getPushNotifications(start, end);
	}

	/**
	 * Returns all the push notifications matching the UUID and company.
	 *
	 * @param uuid the UUID of the push notifications
	 * @param companyId the primary key of the company
	 * @return the matching push notifications, or an empty list if no matches were found
	 */
	public static List<PushNotification> getPushNotificationsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getPushNotificationsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of push notifications matching the UUID and company.
	 *
	 * @param uuid the UUID of the push notifications
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of push notifications
	 * @param end the upper bound of the range of push notifications (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching push notifications, or an empty list if no matches were found
	 */
	public static List<PushNotification> getPushNotificationsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<PushNotification> orderByComparator) {

		return getService().getPushNotificationsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of push notifications.
	 *
	 * @return the number of push notifications
	 */
	public static int getPushNotificationsCount() {
		return getService().getPushNotificationsCount();
	}

	public static void saveNotification(
		String type, long typeId, String title, String body,
		com.liferay.portal.kernel.json.JSONObject typeAdditionalPayload) {

		getService().saveNotification(
			type, typeId, title, body, typeAdditionalPayload);
	}

	public static void sendNotification(
			com.liferay.portal.kernel.json.JSONObject jsonObject,
			List<String> tokens)
		throws PortalException, SystemException {

		getService().sendNotification(jsonObject, tokens);
	}

	public static PushNotification updateEntry(
			PushNotification orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.push.notification.exception.
			PushNotificationValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the push notification in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect PushNotificationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param pushNotification the push notification
	 * @return the push notification that was updated
	 */
	public static PushNotification updatePushNotification(
		PushNotification pushNotification) {

		return getService().updatePushNotification(pushNotification);
	}

	public static PushNotificationLocalService getService() {
		return _service;
	}

	private static volatile PushNotificationLocalService _service;

}