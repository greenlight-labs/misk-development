/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PushNotificationReadStatus}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationReadStatus
 * @generated
 */
public class PushNotificationReadStatusWrapper
	extends BaseModelWrapper<PushNotificationReadStatus>
	implements ModelWrapper<PushNotificationReadStatus>,
			   PushNotificationReadStatus {

	public PushNotificationReadStatusWrapper(
		PushNotificationReadStatus pushNotificationReadStatus) {

		super(pushNotificationReadStatus);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("readStatusId", getReadStatusId());
		attributes.put("notificationId", getNotificationId());
		attributes.put("appUserId", getAppUserId());
		attributes.put("readStatus", isReadStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long readStatusId = (Long)attributes.get("readStatusId");

		if (readStatusId != null) {
			setReadStatusId(readStatusId);
		}

		Long notificationId = (Long)attributes.get("notificationId");

		if (notificationId != null) {
			setNotificationId(notificationId);
		}

		Long appUserId = (Long)attributes.get("appUserId");

		if (appUserId != null) {
			setAppUserId(appUserId);
		}

		Boolean readStatus = (Boolean)attributes.get("readStatus");

		if (readStatus != null) {
			setReadStatus(readStatus);
		}
	}

	/**
	 * Returns the app user ID of this push notification read status.
	 *
	 * @return the app user ID of this push notification read status
	 */
	@Override
	public long getAppUserId() {
		return model.getAppUserId();
	}

	/**
	 * Returns the app user uuid of this push notification read status.
	 *
	 * @return the app user uuid of this push notification read status
	 */
	@Override
	public String getAppUserUuid() {
		return model.getAppUserUuid();
	}

	/**
	 * Returns the notification ID of this push notification read status.
	 *
	 * @return the notification ID of this push notification read status
	 */
	@Override
	public long getNotificationId() {
		return model.getNotificationId();
	}

	/**
	 * Returns the primary key of this push notification read status.
	 *
	 * @return the primary key of this push notification read status
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the read status of this push notification read status.
	 *
	 * @return the read status of this push notification read status
	 */
	@Override
	public boolean getReadStatus() {
		return model.getReadStatus();
	}

	/**
	 * Returns the read status ID of this push notification read status.
	 *
	 * @return the read status ID of this push notification read status
	 */
	@Override
	public long getReadStatusId() {
		return model.getReadStatusId();
	}

	/**
	 * Returns the uuid of this push notification read status.
	 *
	 * @return the uuid of this push notification read status
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this push notification read status is read status.
	 *
	 * @return <code>true</code> if this push notification read status is read status; <code>false</code> otherwise
	 */
	@Override
	public boolean isReadStatus() {
		return model.isReadStatus();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the app user ID of this push notification read status.
	 *
	 * @param appUserId the app user ID of this push notification read status
	 */
	@Override
	public void setAppUserId(long appUserId) {
		model.setAppUserId(appUserId);
	}

	/**
	 * Sets the app user uuid of this push notification read status.
	 *
	 * @param appUserUuid the app user uuid of this push notification read status
	 */
	@Override
	public void setAppUserUuid(String appUserUuid) {
		model.setAppUserUuid(appUserUuid);
	}

	/**
	 * Sets the notification ID of this push notification read status.
	 *
	 * @param notificationId the notification ID of this push notification read status
	 */
	@Override
	public void setNotificationId(long notificationId) {
		model.setNotificationId(notificationId);
	}

	/**
	 * Sets the primary key of this push notification read status.
	 *
	 * @param primaryKey the primary key of this push notification read status
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets whether this push notification read status is read status.
	 *
	 * @param readStatus the read status of this push notification read status
	 */
	@Override
	public void setReadStatus(boolean readStatus) {
		model.setReadStatus(readStatus);
	}

	/**
	 * Sets the read status ID of this push notification read status.
	 *
	 * @param readStatusId the read status ID of this push notification read status
	 */
	@Override
	public void setReadStatusId(long readStatusId) {
		model.setReadStatusId(readStatusId);
	}

	/**
	 * Sets the uuid of this push notification read status.
	 *
	 * @param uuid the uuid of this push notification read status
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	protected PushNotificationReadStatusWrapper wrap(
		PushNotificationReadStatus pushNotificationReadStatus) {

		return new PushNotificationReadStatusWrapper(
			pushNotificationReadStatus);
	}

}