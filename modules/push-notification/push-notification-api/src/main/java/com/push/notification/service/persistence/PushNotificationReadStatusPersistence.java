/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.push.notification.exception.NoSuchReadStatusException;
import com.push.notification.model.PushNotificationReadStatus;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the push notification read status service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationReadStatusUtil
 * @generated
 */
@ProviderType
public interface PushNotificationReadStatusPersistence
	extends BasePersistence<PushNotificationReadStatus> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PushNotificationReadStatusUtil} to access the push notification read status persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the push notification read statuses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByUuid(String uuid);

	/**
	 * Returns a range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns an ordered range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the first push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the last push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the last push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public PushNotificationReadStatus[] findByUuid_PrevAndNext(
			long readStatusId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Removes all the push notification read statuses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of push notification read statuses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching push notification read statuses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the push notification read statuses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserId(
		long appUserId);

	/**
	 * Returns a range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end);

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByAppUserId_First(
			long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByAppUserId_First(
		long appUserId,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByAppUserId_Last(
			long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByAppUserId_Last(
		long appUserId,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public PushNotificationReadStatus[] findByAppUserId_PrevAndNext(
			long readStatusId, long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Removes all the push notification read statuses where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 */
	public void removeByAppUserId(long appUserId);

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching push notification read statuses
	 */
	public int countByAppUserId(long appUserId);

	/**
	 * Returns all the push notification read statuses where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @return the matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus);

	/**
	 * Returns a range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end);

	/**
	 * Returns an ordered range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns an ordered range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByReadStatus_First(
			boolean readStatus,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the first push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByReadStatus_First(
		boolean readStatus,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the last push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByReadStatus_Last(
			boolean readStatus,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the last push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByReadStatus_Last(
		boolean readStatus,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public PushNotificationReadStatus[] findByReadStatus_PrevAndNext(
			long readStatusId, boolean readStatus,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Removes all the push notification read statuses where readStatus = &#63; from the database.
	 *
	 * @param readStatus the read status
	 */
	public void removeByReadStatus(boolean readStatus);

	/**
	 * Returns the number of push notification read statuses where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @return the number of matching push notification read statuses
	 */
	public int countByReadStatus(boolean readStatus);

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or throws a <code>NoSuchReadStatusException</code> if it could not be found.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByAppUserNotification(
			long appUserId, long notificationId)
		throws NoSuchReadStatusException;

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByAppUserNotification(
		long appUserId, long notificationId);

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByAppUserNotification(
		long appUserId, long notificationId, boolean useFinderCache);

	/**
	 * Removes the push notification read status where appUserId = &#63; and notificationId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the push notification read status that was removed
	 */
	public PushNotificationReadStatus removeByAppUserNotification(
			long appUserId, long notificationId)
		throws NoSuchReadStatusException;

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63; and notificationId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the number of matching push notification read statuses
	 */
	public int countByAppUserNotification(long appUserId, long notificationId);

	/**
	 * Returns all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @return the matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus);

	/**
	 * Returns a range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end);

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByAppUserReadStatus_First(
			long appUserId, boolean readStatus,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByAppUserReadStatus_First(
		long appUserId, boolean readStatus,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus findByAppUserReadStatus_Last(
			long appUserId, boolean readStatus,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	public PushNotificationReadStatus fetchByAppUserReadStatus_Last(
		long appUserId, boolean readStatus,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public PushNotificationReadStatus[] findByAppUserReadStatus_PrevAndNext(
			long readStatusId, long appUserId, boolean readStatus,
			com.liferay.portal.kernel.util.OrderByComparator
				<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException;

	/**
	 * Removes all the push notification read statuses where appUserId = &#63; and readStatus = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 */
	public void removeByAppUserReadStatus(long appUserId, boolean readStatus);

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @return the number of matching push notification read statuses
	 */
	public int countByAppUserReadStatus(long appUserId, boolean readStatus);

	/**
	 * Caches the push notification read status in the entity cache if it is enabled.
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 */
	public void cacheResult(
		PushNotificationReadStatus pushNotificationReadStatus);

	/**
	 * Caches the push notification read statuses in the entity cache if it is enabled.
	 *
	 * @param pushNotificationReadStatuses the push notification read statuses
	 */
	public void cacheResult(
		java.util.List<PushNotificationReadStatus>
			pushNotificationReadStatuses);

	/**
	 * Creates a new push notification read status with the primary key. Does not add the push notification read status to the database.
	 *
	 * @param readStatusId the primary key for the new push notification read status
	 * @return the new push notification read status
	 */
	public PushNotificationReadStatus create(long readStatusId);

	/**
	 * Removes the push notification read status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status that was removed
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public PushNotificationReadStatus remove(long readStatusId)
		throws NoSuchReadStatusException;

	public PushNotificationReadStatus updateImpl(
		PushNotificationReadStatus pushNotificationReadStatus);

	/**
	 * Returns the push notification read status with the primary key or throws a <code>NoSuchReadStatusException</code> if it could not be found.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	public PushNotificationReadStatus findByPrimaryKey(long readStatusId)
		throws NoSuchReadStatusException;

	/**
	 * Returns the push notification read status with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status, or <code>null</code> if a push notification read status with the primary key could not be found
	 */
	public PushNotificationReadStatus fetchByPrimaryKey(long readStatusId);

	/**
	 * Returns all the push notification read statuses.
	 *
	 * @return the push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findAll();

	/**
	 * Returns a range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findAll(
		int start, int end);

	/**
	 * Returns an ordered range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator);

	/**
	 * Returns an ordered range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of push notification read statuses
	 */
	public java.util.List<PushNotificationReadStatus> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the push notification read statuses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of push notification read statuses.
	 *
	 * @return the number of push notification read statuses
	 */
	public int countAll();

}