create index IX_2114E58A on misk_push_notification (type_[$COLUMN_LENGTH:75$]);
create index IX_13A9131F on misk_push_notification (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_3849D561 on misk_push_notification (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_89D8024A on misk_push_notification_read_status (appUserId, notificationId);
create index IX_185A0C8C on misk_push_notification_read_status (appUserId, readStatus);
create index IX_79256783 on misk_push_notification_read_status (readStatus);
create index IX_A10036D on misk_push_notification_read_status (uuid_[$COLUMN_LENGTH:75$]);