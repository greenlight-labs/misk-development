create table misk_push_notification (
	uuid_ VARCHAR(75) null,
	notificationId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	type_ VARCHAR(75) null,
	typeId LONG,
	title STRING null,
	body STRING null,
	priority VARCHAR(75) null
);

create table misk_push_notification_read_status (
	uuid_ VARCHAR(75) null,
	readStatusId LONG not null primary key,
	notificationId LONG,
	appUserId LONG,
	readStatus BOOLEAN
);