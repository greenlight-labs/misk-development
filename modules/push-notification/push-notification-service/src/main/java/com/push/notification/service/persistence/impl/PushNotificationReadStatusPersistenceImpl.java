/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import com.push.notification.exception.NoSuchReadStatusException;
import com.push.notification.model.PushNotificationReadStatus;
import com.push.notification.model.impl.PushNotificationReadStatusImpl;
import com.push.notification.model.impl.PushNotificationReadStatusModelImpl;
import com.push.notification.service.persistence.PushNotificationReadStatusPersistence;
import com.push.notification.service.persistence.PushNotificationReadStatusUtil;
import com.push.notification.service.persistence.impl.constants.PushNotificationPersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the push notification read status service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = PushNotificationReadStatusPersistence.class)
public class PushNotificationReadStatusPersistenceImpl
	extends BasePersistenceImpl<PushNotificationReadStatus>
	implements PushNotificationReadStatusPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>PushNotificationReadStatusUtil</code> to access the push notification read status persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		PushNotificationReadStatusImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the push notification read statuses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end) {

		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<PushNotificationReadStatus> list = null;

		if (useFinderCache) {
			list = (List<PushNotificationReadStatus>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (PushNotificationReadStatus pushNotificationReadStatus :
						list) {

					if (!uuid.equals(pushNotificationReadStatus.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<PushNotificationReadStatus>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByUuid_First(
			String uuid,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByUuid_First(uuid, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the first push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByUuid_First(
		String uuid,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		List<PushNotificationReadStatus> list = findByUuid(
			uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByUuid_Last(
			String uuid,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByUuid_Last(uuid, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the last push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByUuid_Last(
		String uuid,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<PushNotificationReadStatus> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where uuid = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus[] findByUuid_PrevAndNext(
			long readStatusId, String uuid,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		uuid = Objects.toString(uuid, "");

		PushNotificationReadStatus pushNotificationReadStatus =
			findByPrimaryKey(readStatusId);

		Session session = null;

		try {
			session = openSession();

			PushNotificationReadStatus[] array =
				new PushNotificationReadStatusImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, pushNotificationReadStatus, uuid, orderByComparator,
				true);

			array[1] = pushNotificationReadStatus;

			array[2] = getByUuid_PrevAndNext(
				session, pushNotificationReadStatus, uuid, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected PushNotificationReadStatus getByUuid_PrevAndNext(
		Session session, PushNotificationReadStatus pushNotificationReadStatus,
		String uuid,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						pushNotificationReadStatus)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<PushNotificationReadStatus> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the push notification read statuses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (PushNotificationReadStatus pushNotificationReadStatus :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(pushNotificationReadStatus);
		}
	}

	/**
	 * Returns the number of push notification read statuses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching push notification read statuses
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"pushNotificationReadStatus.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(pushNotificationReadStatus.uuid IS NULL OR pushNotificationReadStatus.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByAppUserId;
	private FinderPath _finderPathWithoutPaginationFindByAppUserId;
	private FinderPath _finderPathCountByAppUserId;

	/**
	 * Returns all the push notification read statuses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserId(long appUserId) {
		return findByAppUserId(
			appUserId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end) {

		return findByAppUserId(appUserId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return findByAppUserId(appUserId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserId(
		long appUserId, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByAppUserId;
				finderArgs = new Object[] {appUserId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByAppUserId;
			finderArgs = new Object[] {
				appUserId, start, end, orderByComparator
			};
		}

		List<PushNotificationReadStatus> list = null;

		if (useFinderCache) {
			list = (List<PushNotificationReadStatus>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (PushNotificationReadStatus pushNotificationReadStatus :
						list) {

					if (appUserId !=
							pushNotificationReadStatus.getAppUserId()) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERID_APPUSERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				list = (List<PushNotificationReadStatus>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByAppUserId_First(
			long appUserId,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByAppUserId_First(appUserId, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("appUserId=");
		sb.append(appUserId);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByAppUserId_First(
		long appUserId,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		List<PushNotificationReadStatus> list = findByAppUserId(
			appUserId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByAppUserId_Last(
			long appUserId,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByAppUserId_Last(appUserId, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("appUserId=");
		sb.append(appUserId);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByAppUserId_Last(
		long appUserId,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		int count = countByAppUserId(appUserId);

		if (count == 0) {
			return null;
		}

		List<PushNotificationReadStatus> list = findByAppUserId(
			appUserId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where appUserId = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus[] findByAppUserId_PrevAndNext(
			long readStatusId, long appUserId,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			findByPrimaryKey(readStatusId);

		Session session = null;

		try {
			session = openSession();

			PushNotificationReadStatus[] array =
				new PushNotificationReadStatusImpl[3];

			array[0] = getByAppUserId_PrevAndNext(
				session, pushNotificationReadStatus, appUserId,
				orderByComparator, true);

			array[1] = pushNotificationReadStatus;

			array[2] = getByAppUserId_PrevAndNext(
				session, pushNotificationReadStatus, appUserId,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected PushNotificationReadStatus getByAppUserId_PrevAndNext(
		Session session, PushNotificationReadStatus pushNotificationReadStatus,
		long appUserId,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

		sb.append(_FINDER_COLUMN_APPUSERID_APPUSERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(appUserId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						pushNotificationReadStatus)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<PushNotificationReadStatus> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the push notification read statuses where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 */
	@Override
	public void removeByAppUserId(long appUserId) {
		for (PushNotificationReadStatus pushNotificationReadStatus :
				findByAppUserId(
					appUserId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(pushNotificationReadStatus);
		}
	}

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching push notification read statuses
	 */
	@Override
	public int countByAppUserId(long appUserId) {
		FinderPath finderPath = _finderPathCountByAppUserId;

		Object[] finderArgs = new Object[] {appUserId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERID_APPUSERID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPUSERID_APPUSERID_2 =
		"pushNotificationReadStatus.appUserId = ?";

	private FinderPath _finderPathWithPaginationFindByReadStatus;
	private FinderPath _finderPathWithoutPaginationFindByReadStatus;
	private FinderPath _finderPathCountByReadStatus;

	/**
	 * Returns all the push notification read statuses where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @return the matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus) {

		return findByReadStatus(
			readStatus, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end) {

		return findByReadStatus(readStatus, start, end, null);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return findByReadStatus(
			readStatus, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByReadStatus(
		boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByReadStatus;
				finderArgs = new Object[] {readStatus};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByReadStatus;
			finderArgs = new Object[] {
				readStatus, start, end, orderByComparator
			};
		}

		List<PushNotificationReadStatus> list = null;

		if (useFinderCache) {
			list = (List<PushNotificationReadStatus>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (PushNotificationReadStatus pushNotificationReadStatus :
						list) {

					if (readStatus !=
							pushNotificationReadStatus.isReadStatus()) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_READSTATUS_READSTATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(readStatus);

				list = (List<PushNotificationReadStatus>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByReadStatus_First(
			boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByReadStatus_First(readStatus, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("readStatus=");
		sb.append(readStatus);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the first push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByReadStatus_First(
		boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		List<PushNotificationReadStatus> list = findByReadStatus(
			readStatus, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByReadStatus_Last(
			boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByReadStatus_Last(readStatus, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("readStatus=");
		sb.append(readStatus);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the last push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByReadStatus_Last(
		boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		int count = countByReadStatus(readStatus);

		if (count == 0) {
			return null;
		}

		List<PushNotificationReadStatus> list = findByReadStatus(
			readStatus, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where readStatus = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus[] findByReadStatus_PrevAndNext(
			long readStatusId, boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			findByPrimaryKey(readStatusId);

		Session session = null;

		try {
			session = openSession();

			PushNotificationReadStatus[] array =
				new PushNotificationReadStatusImpl[3];

			array[0] = getByReadStatus_PrevAndNext(
				session, pushNotificationReadStatus, readStatus,
				orderByComparator, true);

			array[1] = pushNotificationReadStatus;

			array[2] = getByReadStatus_PrevAndNext(
				session, pushNotificationReadStatus, readStatus,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected PushNotificationReadStatus getByReadStatus_PrevAndNext(
		Session session, PushNotificationReadStatus pushNotificationReadStatus,
		boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

		sb.append(_FINDER_COLUMN_READSTATUS_READSTATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(readStatus);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						pushNotificationReadStatus)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<PushNotificationReadStatus> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the push notification read statuses where readStatus = &#63; from the database.
	 *
	 * @param readStatus the read status
	 */
	@Override
	public void removeByReadStatus(boolean readStatus) {
		for (PushNotificationReadStatus pushNotificationReadStatus :
				findByReadStatus(
					readStatus, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(pushNotificationReadStatus);
		}
	}

	/**
	 * Returns the number of push notification read statuses where readStatus = &#63;.
	 *
	 * @param readStatus the read status
	 * @return the number of matching push notification read statuses
	 */
	@Override
	public int countByReadStatus(boolean readStatus) {
		FinderPath finderPath = _finderPathCountByReadStatus;

		Object[] finderArgs = new Object[] {readStatus};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_READSTATUS_READSTATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(readStatus);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_READSTATUS_READSTATUS_2 =
		"pushNotificationReadStatus.readStatus = ?";

	private FinderPath _finderPathFetchByAppUserNotification;
	private FinderPath _finderPathCountByAppUserNotification;

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or throws a <code>NoSuchReadStatusException</code> if it could not be found.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByAppUserNotification(
			long appUserId, long notificationId)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByAppUserNotification(appUserId, notificationId);

		if (pushNotificationReadStatus == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("appUserId=");
			sb.append(appUserId);

			sb.append(", notificationId=");
			sb.append(notificationId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchReadStatusException(sb.toString());
		}

		return pushNotificationReadStatus;
	}

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByAppUserNotification(
		long appUserId, long notificationId) {

		return fetchByAppUserNotification(appUserId, notificationId, true);
	}

	/**
	 * Returns the push notification read status where appUserId = &#63; and notificationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByAppUserNotification(
		long appUserId, long notificationId, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {appUserId, notificationId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByAppUserNotification, finderArgs, this);
		}

		if (result instanceof PushNotificationReadStatus) {
			PushNotificationReadStatus pushNotificationReadStatus =
				(PushNotificationReadStatus)result;

			if ((appUserId != pushNotificationReadStatus.getAppUserId()) ||
				(notificationId !=
					pushNotificationReadStatus.getNotificationId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERNOTIFICATION_APPUSERID_2);

			sb.append(_FINDER_COLUMN_APPUSERNOTIFICATION_NOTIFICATIONID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				queryPos.add(notificationId);

				List<PushNotificationReadStatus> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByAppUserNotification, finderArgs,
							list);
					}
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							if (!useFinderCache) {
								finderArgs = new Object[] {
									appUserId, notificationId
								};
							}

							_log.warn(
								"PushNotificationReadStatusPersistenceImpl.fetchByAppUserNotification(long, long, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					PushNotificationReadStatus pushNotificationReadStatus =
						list.get(0);

					result = pushNotificationReadStatus;

					cacheResult(pushNotificationReadStatus);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PushNotificationReadStatus)result;
		}
	}

	/**
	 * Removes the push notification read status where appUserId = &#63; and notificationId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the push notification read status that was removed
	 */
	@Override
	public PushNotificationReadStatus removeByAppUserNotification(
			long appUserId, long notificationId)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			findByAppUserNotification(appUserId, notificationId);

		return remove(pushNotificationReadStatus);
	}

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63; and notificationId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param notificationId the notification ID
	 * @return the number of matching push notification read statuses
	 */
	@Override
	public int countByAppUserNotification(long appUserId, long notificationId) {
		FinderPath finderPath = _finderPathCountByAppUserNotification;

		Object[] finderArgs = new Object[] {appUserId, notificationId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERNOTIFICATION_APPUSERID_2);

			sb.append(_FINDER_COLUMN_APPUSERNOTIFICATION_NOTIFICATIONID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				queryPos.add(notificationId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPUSERNOTIFICATION_APPUSERID_2 =
		"pushNotificationReadStatus.appUserId = ? AND ";

	private static final String
		_FINDER_COLUMN_APPUSERNOTIFICATION_NOTIFICATIONID_2 =
			"pushNotificationReadStatus.notificationId = ?";

	private FinderPath _finderPathWithPaginationFindByAppUserReadStatus;
	private FinderPath _finderPathWithoutPaginationFindByAppUserReadStatus;
	private FinderPath _finderPathCountByAppUserReadStatus;

	/**
	 * Returns all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @return the matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus) {

		return findByAppUserReadStatus(
			appUserId, readStatus, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end) {

		return findByAppUserReadStatus(appUserId, readStatus, start, end, null);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return findByAppUserReadStatus(
			appUserId, readStatus, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findByAppUserReadStatus(
		long appUserId, boolean readStatus, int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath =
					_finderPathWithoutPaginationFindByAppUserReadStatus;
				finderArgs = new Object[] {appUserId, readStatus};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByAppUserReadStatus;
			finderArgs = new Object[] {
				appUserId, readStatus, start, end, orderByComparator
			};
		}

		List<PushNotificationReadStatus> list = null;

		if (useFinderCache) {
			list = (List<PushNotificationReadStatus>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (PushNotificationReadStatus pushNotificationReadStatus :
						list) {

					if ((appUserId !=
							pushNotificationReadStatus.getAppUserId()) ||
						(readStatus !=
							pushNotificationReadStatus.isReadStatus())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERREADSTATUS_APPUSERID_2);

			sb.append(_FINDER_COLUMN_APPUSERREADSTATUS_READSTATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				queryPos.add(readStatus);

				list = (List<PushNotificationReadStatus>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByAppUserReadStatus_First(
			long appUserId, boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByAppUserReadStatus_First(
				appUserId, readStatus, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("appUserId=");
		sb.append(appUserId);

		sb.append(", readStatus=");
		sb.append(readStatus);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the first push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByAppUserReadStatus_First(
		long appUserId, boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		List<PushNotificationReadStatus> list = findByAppUserReadStatus(
			appUserId, readStatus, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status
	 * @throws NoSuchReadStatusException if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus findByAppUserReadStatus_Last(
			long appUserId, boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByAppUserReadStatus_Last(
				appUserId, readStatus, orderByComparator);

		if (pushNotificationReadStatus != null) {
			return pushNotificationReadStatus;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("appUserId=");
		sb.append(appUserId);

		sb.append(", readStatus=");
		sb.append(readStatus);

		sb.append("}");

		throw new NoSuchReadStatusException(sb.toString());
	}

	/**
	 * Returns the last push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching push notification read status, or <code>null</code> if a matching push notification read status could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByAppUserReadStatus_Last(
		long appUserId, boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		int count = countByAppUserReadStatus(appUserId, readStatus);

		if (count == 0) {
			return null;
		}

		List<PushNotificationReadStatus> list = findByAppUserReadStatus(
			appUserId, readStatus, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the push notification read statuses before and after the current push notification read status in the ordered set where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param readStatusId the primary key of the current push notification read status
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus[] findByAppUserReadStatus_PrevAndNext(
			long readStatusId, long appUserId, boolean readStatus,
			OrderByComparator<PushNotificationReadStatus> orderByComparator)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			findByPrimaryKey(readStatusId);

		Session session = null;

		try {
			session = openSession();

			PushNotificationReadStatus[] array =
				new PushNotificationReadStatusImpl[3];

			array[0] = getByAppUserReadStatus_PrevAndNext(
				session, pushNotificationReadStatus, appUserId, readStatus,
				orderByComparator, true);

			array[1] = pushNotificationReadStatus;

			array[2] = getByAppUserReadStatus_PrevAndNext(
				session, pushNotificationReadStatus, appUserId, readStatus,
				orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected PushNotificationReadStatus getByAppUserReadStatus_PrevAndNext(
		Session session, PushNotificationReadStatus pushNotificationReadStatus,
		long appUserId, boolean readStatus,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE);

		sb.append(_FINDER_COLUMN_APPUSERREADSTATUS_APPUSERID_2);

		sb.append(_FINDER_COLUMN_APPUSERREADSTATUS_READSTATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(appUserId);

		queryPos.add(readStatus);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						pushNotificationReadStatus)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<PushNotificationReadStatus> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the push notification read statuses where appUserId = &#63; and readStatus = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 */
	@Override
	public void removeByAppUserReadStatus(long appUserId, boolean readStatus) {
		for (PushNotificationReadStatus pushNotificationReadStatus :
				findByAppUserReadStatus(
					appUserId, readStatus, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(pushNotificationReadStatus);
		}
	}

	/**
	 * Returns the number of push notification read statuses where appUserId = &#63; and readStatus = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param readStatus the read status
	 * @return the number of matching push notification read statuses
	 */
	@Override
	public int countByAppUserReadStatus(long appUserId, boolean readStatus) {
		FinderPath finderPath = _finderPathCountByAppUserReadStatus;

		Object[] finderArgs = new Object[] {appUserId, readStatus};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_PUSHNOTIFICATIONREADSTATUS_WHERE);

			sb.append(_FINDER_COLUMN_APPUSERREADSTATUS_APPUSERID_2);

			sb.append(_FINDER_COLUMN_APPUSERREADSTATUS_READSTATUS_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(appUserId);

				queryPos.add(readStatus);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_APPUSERREADSTATUS_APPUSERID_2 =
		"pushNotificationReadStatus.appUserId = ? AND ";

	private static final String _FINDER_COLUMN_APPUSERREADSTATUS_READSTATUS_2 =
		"pushNotificationReadStatus.readStatus = ?";

	public PushNotificationReadStatusPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(PushNotificationReadStatus.class);

		setModelImplClass(PushNotificationReadStatusImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the push notification read status in the entity cache if it is enabled.
	 *
	 * @param pushNotificationReadStatus the push notification read status
	 */
	@Override
	public void cacheResult(
		PushNotificationReadStatus pushNotificationReadStatus) {

		entityCache.putResult(
			PushNotificationReadStatusImpl.class,
			pushNotificationReadStatus.getPrimaryKey(),
			pushNotificationReadStatus);

		finderCache.putResult(
			_finderPathFetchByAppUserNotification,
			new Object[] {
				pushNotificationReadStatus.getAppUserId(),
				pushNotificationReadStatus.getNotificationId()
			},
			pushNotificationReadStatus);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the push notification read statuses in the entity cache if it is enabled.
	 *
	 * @param pushNotificationReadStatuses the push notification read statuses
	 */
	@Override
	public void cacheResult(
		List<PushNotificationReadStatus> pushNotificationReadStatuses) {

		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (pushNotificationReadStatuses.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (PushNotificationReadStatus pushNotificationReadStatus :
				pushNotificationReadStatuses) {

			if (entityCache.getResult(
					PushNotificationReadStatusImpl.class,
					pushNotificationReadStatus.getPrimaryKey()) == null) {

				cacheResult(pushNotificationReadStatus);
			}
		}
	}

	/**
	 * Clears the cache for all push notification read statuses.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(PushNotificationReadStatusImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the push notification read status.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(
		PushNotificationReadStatus pushNotificationReadStatus) {

		entityCache.removeResult(
			PushNotificationReadStatusImpl.class, pushNotificationReadStatus);
	}

	@Override
	public void clearCache(
		List<PushNotificationReadStatus> pushNotificationReadStatuses) {

		for (PushNotificationReadStatus pushNotificationReadStatus :
				pushNotificationReadStatuses) {

			entityCache.removeResult(
				PushNotificationReadStatusImpl.class,
				pushNotificationReadStatus);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(
				PushNotificationReadStatusImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		PushNotificationReadStatusModelImpl
			pushNotificationReadStatusModelImpl) {

		Object[] args = new Object[] {
			pushNotificationReadStatusModelImpl.getAppUserId(),
			pushNotificationReadStatusModelImpl.getNotificationId()
		};

		finderCache.putResult(
			_finderPathCountByAppUserNotification, args, Long.valueOf(1),
			false);
		finderCache.putResult(
			_finderPathFetchByAppUserNotification, args,
			pushNotificationReadStatusModelImpl, false);
	}

	/**
	 * Creates a new push notification read status with the primary key. Does not add the push notification read status to the database.
	 *
	 * @param readStatusId the primary key for the new push notification read status
	 * @return the new push notification read status
	 */
	@Override
	public PushNotificationReadStatus create(long readStatusId) {
		PushNotificationReadStatus pushNotificationReadStatus =
			new PushNotificationReadStatusImpl();

		pushNotificationReadStatus.setNew(true);
		pushNotificationReadStatus.setPrimaryKey(readStatusId);

		String uuid = PortalUUIDUtil.generate();

		pushNotificationReadStatus.setUuid(uuid);

		return pushNotificationReadStatus;
	}

	/**
	 * Removes the push notification read status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status that was removed
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus remove(long readStatusId)
		throws NoSuchReadStatusException {

		return remove((Serializable)readStatusId);
	}

	/**
	 * Removes the push notification read status with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the push notification read status
	 * @return the push notification read status that was removed
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus remove(Serializable primaryKey)
		throws NoSuchReadStatusException {

		Session session = null;

		try {
			session = openSession();

			PushNotificationReadStatus pushNotificationReadStatus =
				(PushNotificationReadStatus)session.get(
					PushNotificationReadStatusImpl.class, primaryKey);

			if (pushNotificationReadStatus == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchReadStatusException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(pushNotificationReadStatus);
		}
		catch (NoSuchReadStatusException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PushNotificationReadStatus removeImpl(
		PushNotificationReadStatus pushNotificationReadStatus) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(pushNotificationReadStatus)) {
				pushNotificationReadStatus =
					(PushNotificationReadStatus)session.get(
						PushNotificationReadStatusImpl.class,
						pushNotificationReadStatus.getPrimaryKeyObj());
			}

			if (pushNotificationReadStatus != null) {
				session.delete(pushNotificationReadStatus);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (pushNotificationReadStatus != null) {
			clearCache(pushNotificationReadStatus);
		}

		return pushNotificationReadStatus;
	}

	@Override
	public PushNotificationReadStatus updateImpl(
		PushNotificationReadStatus pushNotificationReadStatus) {

		boolean isNew = pushNotificationReadStatus.isNew();

		if (!(pushNotificationReadStatus instanceof
				PushNotificationReadStatusModelImpl)) {

			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(pushNotificationReadStatus.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					pushNotificationReadStatus);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in pushNotificationReadStatus proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom PushNotificationReadStatus implementation " +
					pushNotificationReadStatus.getClass());
		}

		PushNotificationReadStatusModelImpl
			pushNotificationReadStatusModelImpl =
				(PushNotificationReadStatusModelImpl)pushNotificationReadStatus;

		if (Validator.isNull(pushNotificationReadStatus.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			pushNotificationReadStatus.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(pushNotificationReadStatus);
			}
			else {
				pushNotificationReadStatus =
					(PushNotificationReadStatus)session.merge(
						pushNotificationReadStatus);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			PushNotificationReadStatusImpl.class,
			pushNotificationReadStatusModelImpl, false, true);

		cacheUniqueFindersCache(pushNotificationReadStatusModelImpl);

		if (isNew) {
			pushNotificationReadStatus.setNew(false);
		}

		pushNotificationReadStatus.resetOriginalValues();

		return pushNotificationReadStatus;
	}

	/**
	 * Returns the push notification read status with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the push notification read status
	 * @return the push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus findByPrimaryKey(Serializable primaryKey)
		throws NoSuchReadStatusException {

		PushNotificationReadStatus pushNotificationReadStatus =
			fetchByPrimaryKey(primaryKey);

		if (pushNotificationReadStatus == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchReadStatusException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return pushNotificationReadStatus;
	}

	/**
	 * Returns the push notification read status with the primary key or throws a <code>NoSuchReadStatusException</code> if it could not be found.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status
	 * @throws NoSuchReadStatusException if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus findByPrimaryKey(long readStatusId)
		throws NoSuchReadStatusException {

		return findByPrimaryKey((Serializable)readStatusId);
	}

	/**
	 * Returns the push notification read status with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param readStatusId the primary key of the push notification read status
	 * @return the push notification read status, or <code>null</code> if a push notification read status with the primary key could not be found
	 */
	@Override
	public PushNotificationReadStatus fetchByPrimaryKey(long readStatusId) {
		return fetchByPrimaryKey((Serializable)readStatusId);
	}

	/**
	 * Returns all the push notification read statuses.
	 *
	 * @return the push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @return the range of push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findAll(
		int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the push notification read statuses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>PushNotificationReadStatusModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of push notification read statuses
	 * @param end the upper bound of the range of push notification read statuses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of push notification read statuses
	 */
	@Override
	public List<PushNotificationReadStatus> findAll(
		int start, int end,
		OrderByComparator<PushNotificationReadStatus> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<PushNotificationReadStatus> list = null;

		if (useFinderCache) {
			list = (List<PushNotificationReadStatus>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_PUSHNOTIFICATIONREADSTATUS);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_PUSHNOTIFICATIONREADSTATUS;

				sql = sql.concat(
					PushNotificationReadStatusModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<PushNotificationReadStatus>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the push notification read statuses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (PushNotificationReadStatus pushNotificationReadStatus :
				findAll()) {

			remove(pushNotificationReadStatus);
		}
	}

	/**
	 * Returns the number of push notification read statuses.
	 *
	 * @return the number of push notification read statuses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(
					_SQL_COUNT_PUSHNOTIFICATIONREADSTATUS);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "readStatusId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_PUSHNOTIFICATIONREADSTATUS;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return PushNotificationReadStatusModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the push notification read status persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class,
			new PushNotificationReadStatusModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name",
				PushNotificationReadStatus.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByAppUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAppUserId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"appUserId"}, true);

		_finderPathWithoutPaginationFindByAppUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAppUserId",
			new String[] {Long.class.getName()}, new String[] {"appUserId"},
			true);

		_finderPathCountByAppUserId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAppUserId",
			new String[] {Long.class.getName()}, new String[] {"appUserId"},
			false);

		_finderPathWithPaginationFindByReadStatus = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByReadStatus",
			new String[] {
				Boolean.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"readStatus"}, true);

		_finderPathWithoutPaginationFindByReadStatus = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByReadStatus",
			new String[] {Boolean.class.getName()}, new String[] {"readStatus"},
			true);

		_finderPathCountByReadStatus = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByReadStatus",
			new String[] {Boolean.class.getName()}, new String[] {"readStatus"},
			false);

		_finderPathFetchByAppUserNotification = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByAppUserNotification",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"appUserId", "notificationId"}, true);

		_finderPathCountByAppUserNotification = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByAppUserNotification",
			new String[] {Long.class.getName(), Long.class.getName()},
			new String[] {"appUserId", "notificationId"}, false);

		_finderPathWithPaginationFindByAppUserReadStatus = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAppUserReadStatus",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"appUserId", "readStatus"}, true);

		_finderPathWithoutPaginationFindByAppUserReadStatus = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByAppUserReadStatus",
			new String[] {Long.class.getName(), Boolean.class.getName()},
			new String[] {"appUserId", "readStatus"}, true);

		_finderPathCountByAppUserReadStatus = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByAppUserReadStatus",
			new String[] {Long.class.getName(), Boolean.class.getName()},
			new String[] {"appUserId", "readStatus"}, false);

		_setPushNotificationReadStatusUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setPushNotificationReadStatusUtilPersistence(null);

		entityCache.removeCache(PushNotificationReadStatusImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setPushNotificationReadStatusUtilPersistence(
		PushNotificationReadStatusPersistence
			pushNotificationReadStatusPersistence) {

		try {
			Field field = PushNotificationReadStatusUtil.class.getDeclaredField(
				"_persistence");

			field.setAccessible(true);

			field.set(null, pushNotificationReadStatusPersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = PushNotificationPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = PushNotificationPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = PushNotificationPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_PUSHNOTIFICATIONREADSTATUS =
		"SELECT pushNotificationReadStatus FROM PushNotificationReadStatus pushNotificationReadStatus";

	private static final String _SQL_SELECT_PUSHNOTIFICATIONREADSTATUS_WHERE =
		"SELECT pushNotificationReadStatus FROM PushNotificationReadStatus pushNotificationReadStatus WHERE ";

	private static final String _SQL_COUNT_PUSHNOTIFICATIONREADSTATUS =
		"SELECT COUNT(pushNotificationReadStatus) FROM PushNotificationReadStatus pushNotificationReadStatus";

	private static final String _SQL_COUNT_PUSHNOTIFICATIONREADSTATUS_WHERE =
		"SELECT COUNT(pushNotificationReadStatus) FROM PushNotificationReadStatus pushNotificationReadStatus WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS =
		"pushNotificationReadStatus.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No PushNotificationReadStatus exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No PushNotificationReadStatus exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		PushNotificationReadStatusPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class PushNotificationReadStatusModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			PushNotificationReadStatusModelImpl
				pushNotificationReadStatusModelImpl =
					(PushNotificationReadStatusModelImpl)baseModel;

			long columnBitmask =
				pushNotificationReadStatusModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(
					pushNotificationReadStatusModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						pushNotificationReadStatusModelImpl.getColumnBitmask(
							columnName);
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(
					pushNotificationReadStatusModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			PushNotificationReadStatusModelImpl
				pushNotificationReadStatusModelImpl,
			String[] columnNames, boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] =
						pushNotificationReadStatusModelImpl.
							getColumnOriginalValue(columnName);
				}
				else {
					arguments[i] =
						pushNotificationReadStatusModelImpl.getColumnValue(
							columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

	}

}