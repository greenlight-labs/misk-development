/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.push.notification.model.PushNotification;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing PushNotification in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class PushNotificationCacheModel
	implements CacheModel<PushNotification>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof PushNotificationCacheModel)) {
			return false;
		}

		PushNotificationCacheModel pushNotificationCacheModel =
			(PushNotificationCacheModel)object;

		if (notificationId == pushNotificationCacheModel.notificationId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, notificationId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", notificationId=");
		sb.append(notificationId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", type=");
		sb.append(type);
		sb.append(", typeId=");
		sb.append(typeId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", body=");
		sb.append(body);
		sb.append(", priority=");
		sb.append(priority);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PushNotification toEntityModel() {
		PushNotificationImpl pushNotificationImpl = new PushNotificationImpl();

		if (uuid == null) {
			pushNotificationImpl.setUuid("");
		}
		else {
			pushNotificationImpl.setUuid(uuid);
		}

		pushNotificationImpl.setNotificationId(notificationId);
		pushNotificationImpl.setGroupId(groupId);
		pushNotificationImpl.setCompanyId(companyId);

		if (createDate == Long.MIN_VALUE) {
			pushNotificationImpl.setCreateDate(null);
		}
		else {
			pushNotificationImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			pushNotificationImpl.setModifiedDate(null);
		}
		else {
			pushNotificationImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (type == null) {
			pushNotificationImpl.setType("");
		}
		else {
			pushNotificationImpl.setType(type);
		}

		pushNotificationImpl.setTypeId(typeId);

		if (title == null) {
			pushNotificationImpl.setTitle("");
		}
		else {
			pushNotificationImpl.setTitle(title);
		}

		if (body == null) {
			pushNotificationImpl.setBody("");
		}
		else {
			pushNotificationImpl.setBody(body);
		}

		if (priority == null) {
			pushNotificationImpl.setPriority("");
		}
		else {
			pushNotificationImpl.setPriority(priority);
		}

		pushNotificationImpl.resetOriginalValues();

		return pushNotificationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		notificationId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		type = objectInput.readUTF();

		typeId = objectInput.readLong();
		title = objectInput.readUTF();
		body = objectInput.readUTF();
		priority = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(notificationId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (type == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(type);
		}

		objectOutput.writeLong(typeId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (body == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(body);
		}

		if (priority == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(priority);
		}
	}

	public String uuid;
	public long notificationId;
	public long groupId;
	public long companyId;
	public long createDate;
	public long modifiedDate;
	public String type;
	public long typeId;
	public String title;
	public String body;
	public String priority;

}