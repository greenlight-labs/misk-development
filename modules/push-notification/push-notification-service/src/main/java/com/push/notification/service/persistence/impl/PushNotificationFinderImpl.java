package com.push.notification.service.persistence.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.push.notification.model.PushNotification;
import com.push.notification.model.impl.PushNotificationImpl;
import com.push.notification.service.persistence.PushNotificationFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.util.Collections;
import java.util.List;


@Component(service = PushNotificationFinder.class)
public class PushNotificationFinderImpl
        extends PushNotificationFinderBaseImpl implements PushNotificationFinder {

    public static final String FIND_BY_NOTIFICATION_IDS =
            PushNotificationFinder.class.getName() + ".findByNotificationIds";

    Log _log = LogFactoryUtil.getLog(PushNotificationFinderImpl.class);

    public List<PushNotification> findByNotificationIds(long[] userIds, int start, int end) {
        Session session = null;
        List<PushNotification> pushNotifications = Collections.emptyList();

        try {
            if (userIds.length > 0) {
                session = openSession();
                String sql = _customSQL.get(getClass(), FIND_BY_NOTIFICATION_IDS);
                sql = StringUtil.replace(sql, "[$NOTIFICATION_IDS$]", StringUtil.merge(userIds, StringPool.COMMA));
                sql = StringUtil.replace(sql, "[$OFFSET$]", String.valueOf(start));
                sql = StringUtil.replace(sql, "[$LIMIT$]", String.valueOf(end));

                SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

                sqlQuery.setCacheable(false);
                sqlQuery.addEntity("PushNotification", PushNotificationImpl.class);

                /*QueryPos queryPos = QueryPos.getInstance(sqlQuery);
                queryPos.add(userIds);*/
                pushNotifications = (List<PushNotification>) sqlQuery.list();
            }
            return pushNotifications;
        } catch (Exception exception) {
            throw new SystemException(exception);
        } finally {
            closeSession(session);
        }
    }

    @Reference
    private CustomSQL _customSQL;
}
