/*
package com.push.notification.service.impl;

import java.util.ArrayList;

public class PayloadJsonObject {
    private String priority;
    ArrayList< Object > tokens = new ArrayList < Object > ();
    Data1 DataObject;
    Apns ApnsObject;


    // Getter Methods

    public String getPriority() {
        return priority;
    }

    public Data1 getData() {
        return DataObject;
    }

    public Apns getApns() {
        return ApnsObject;
    }

    // Setter Methods

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public void setData(Data1 dataObject) {
        this.DataObject = dataObject;
    }

    public void setApns(Apns apnsObject) {
        this.ApnsObject = apnsObject;
    }
}
public class Apns {
    Headers HeadersObject;
    Payload PayloadObject;


    // Getter Methods

    public Headers getHeaders() {
        return HeadersObject;
    }

    public Payload getPayload() {
        return PayloadObject;
    }

    // Setter Methods

    public void setHeaders(Headers headersObject) {
        this.HeadersObject = headersObject;
    }

    public void setPayload(Payload payloadObject) {
        this.PayloadObject = payloadObject;
    }
}
public class Payload {
    Aps ApsObject;


    // Getter Methods

    public Aps getAps() {
        return ApsObject;
    }

    // Setter Methods

    public void setAps(Aps apsObject) {
        this.ApsObject = apsObject;
    }
}
public class Aps {
    Alert AlertObject;
    Data DataObject;


    // Getter Methods

    public Alert getAlert() {
        return AlertObject;
    }

    public Data getData() {
        return DataObject;
    }

    // Setter Methods

    public void setAlert(Alert alertObject) {
        this.AlertObject = alertObject;
    }

    public void setData(Data dataObject) {
        this.DataObject = dataObject;
    }
}
public class Data1 {
    private float score;


    // Getter Methods

    public float getScore() {
        return score;
    }

    // Setter Methods

    public void setScore(float score) {
        this.score = score;
    }
}
public class Alert {
    private String title;
    private String body;


    // Getter Methods

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    // Setter Methods

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
public class Headers {
    private String apnsPriority;


    // Getter Methods

    public String getApnsPriority() {
        return apnsPriority;
    }

    // Setter Methods

    public void setApnsPriority(String apnsPriority) {
        this.apnsPriority = apnsPriority;
    }
}
public class Data2 {
    private String title;
    private String body;
    private float score;


    // Getter Methods

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public float getScore() {
        return score;
    }

    // Setter Methods

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setScore(float score) {
        this.score = score;
    }
}
*/
