/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.service.impl;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
//import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import com.liferay.push.notifications.service.PushNotificationsDeviceLocalServiceUtil;
import com.notification.settings.model.NotificationSettings;
import com.notification.settings.service.NotificationSettingsLocalServiceUtil;
import com.push.notification.constants.PushNotificationKeys;
import com.push.notification.exception.PushNotificationValidateException;
import com.push.notification.model.PushNotification;
import com.push.notification.model.PushNotificationReadStatus;
import com.push.notification.service.PushNotificationReadStatusLocalServiceUtil;
import com.push.notification.service.base.PushNotificationLocalServiceBaseImpl;
import com.push.notification.service.util.PushNotificationValidator;
//import highlights.model.Category;
//import highlights.model.Tag;
//import highlights.service.CategoryLocalServiceUtil;
//import highlights.service.HighlightLocalServiceUtil;
//import highlights.service.TagLocalServiceUtil;
import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalServiceUtil;
import org.apache.commons.lang3.LocaleUtils;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * The implementation of the push notification local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>com.push.notification.service.PushNotificationLocalService</code>
 * interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PushNotificationLocalServiceBaseImpl
 */
@Component(property = "model.class.name=com.push.notification.model.PushNotification", service = AopService.class)
public class PushNotificationLocalServiceImpl extends PushNotificationLocalServiceBaseImpl {

	public void saveNotification(String type, long typeId, String title, String body, JSONObject typeAdditionalPayload) {
		Date createdDate = new Date();
		try {
			PushNotification pushNotification = createPushNotification(CounterLocalServiceUtil.increment());

			pushNotification.setCompanyId(20101);
			pushNotification.setGroupId(91702);
			pushNotification.setCreateDate(createdDate);
			pushNotification.setType(type);
			pushNotification.setTypeId(typeId);
			pushNotification.setTitle(title);
			pushNotification.setBody(body);
			pushNotification.setPriority(getPriority(type));

			PushNotification addedEntry = addPushNotification(pushNotification);
			// get userIds of all users who have enabled notification for this type
			long[] userIds = getNotificationEnableUserIds(type, typeId);
			if(userIds.length > 0){
				// add push notification read status for each user
				for (long userId : userIds) {
					PushNotificationReadStatus pushNotificationReadStatus = PushNotificationReadStatusLocalServiceUtil.createPushNotificationReadStatus(CounterLocalServiceUtil.increment());
					pushNotificationReadStatus.setNotificationId(addedEntry.getNotificationId());
					pushNotificationReadStatus.setAppUserId(userId);
					pushNotificationReadStatus.setReadStatus(false);
					PushNotificationReadStatusLocalServiceUtil.addPushNotificationReadStatus(pushNotificationReadStatus);
				}
			}

			List<String> tokens = getDeviceTokens(type, typeId);
			if(tokens.size() > 0){
				JSONObject jsonObject = getJsonObject(type, typeId, title, body, typeAdditionalPayload);
				sendNotification(jsonObject, tokens);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendNotification(JSONObject jsonObject, List<String> tokens) throws PortalException, SystemException {
		PushNotificationsDeviceLocalServiceUtil.sendPushNotification("firebase", tokens, jsonObject);
	}

	private JSONObject getJsonObject(String type, long typeId, String title, String body, JSONObject typeAdditionalPayload)
			throws PortalException, SystemException {
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(title);
		Map<Locale, String> bodyMap = LocalizationUtil.getLocalizationMap(body);
		String title_EN = titleMap.get(LocaleUtils.toLocale("en_US"));
		String body_EN = bodyMap.get(LocaleUtils.toLocale("en_US"));

		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		// notification object keys
		jsonObject.put("badge", 1);
		jsonObject.put("title", title_EN);
		jsonObject.put("body", body_EN);
		jsonObject.put("content-available", 1);
		jsonObject.put("priority", PushNotificationKeys.NOTIFICATION_PRIORITY_HIGH);
		jsonObject.put("android_channel_id", "MISKCITY");
		jsonObject.put("sound", "default");
		jsonObject.put("silent", false);
		// data object keys
		jsonObject.put("type", type);
		jsonObject.put("typeId", typeId);
		if (type.equals(PushNotificationKeys.NOTIFICATION_TYPE_HIGHLIGHT)) {
			if(Validator.isNotNull(typeAdditionalPayload)){
				jsonObject.put("highlight", typeAdditionalPayload);
			}
		}

		return jsonObject;
	}

	private long[] getNotificationEnableUserIds(String type, long typeId) throws PortalException {
		// if type is news, highlights, broadcastMessage then get all notification enable users else if type is event then get all attendees ids of that event
		List<NotificationSettings> notificationEnableUsers = new ArrayList<>();
		if(type.equals(PushNotificationKeys.NOTIFICATION_TYPE_NEWS) ||
				type.equals(PushNotificationKeys.NOTIFICATION_TYPE_EVENT) ||
				type.equals(PushNotificationKeys.NOTIFICATION_TYPE_HIGHLIGHT) ||
				type.equals(PushNotificationKeys.NOTIFICATION_TYPE_BROADCAST_MESSAGE)){
			notificationEnableUsers = NotificationSettingsLocalServiceUtil.getNotificationEnabledUsers(type, true);
		}else if(type.equals(PushNotificationKeys.NOTIFICATION_TYPE_EVENT_UPDATE)){
			notificationEnableUsers = NotificationSettingsLocalServiceUtil.getNotificationEnabledUsers(type, typeId, true);
		}

		long[] userIds = new long[notificationEnableUsers.size()];
		int count = 0;

		if (Validator.isNotNull(notificationEnableUsers)) {
			for (NotificationSettings notificationEnableUser : notificationEnableUsers) {
				userIds[count] = notificationEnableUser.getAppUserId();
				count++;
			}
		}

		return userIds;
	}

	private List<AppUser> getNotificationEnabledUsers(String type, long typeId) throws PortalException {
		List<AppUser> notificationEnableUsers = null;

		long[] userIds = getNotificationEnableUserIds(type, typeId);
		if(userIds.length > 0){
			notificationEnableUsers = AppUserLocalServiceUtil.findByUserIds(userIds);
		}

		return notificationEnableUsers;
	}

	private List<String> getDeviceTokens(String type, long typeId) throws PortalException {
		List<String> deviceTokens = new ArrayList<>();
		List<AppUser> appUsers = getNotificationEnabledUsers(type, typeId);

		if(appUsers.size() > 0){
			for (AppUser appUser : appUsers) {
				// add android device token
				if(Validator.isNotNull(appUser.getAndroidDeviceToken())){
					deviceTokens.add(appUser.getAndroidDeviceToken());
				}
				// add ios device token
				if(Validator.isNotNull(appUser.getIosDeviceToken())){
					deviceTokens.add(appUser.getIosDeviceToken());
				}
			}
		}
		return deviceTokens;
	}

	private String getPriority(String type) {
		String priority = PushNotificationKeys.NOTIFICATION_PRIORITY_HIGH;
		if(Objects.equals(type, PushNotificationKeys.NOTIFICATION_TYPE_EVENT) ||
				Objects.equals(type, PushNotificationKeys.NOTIFICATION_TYPE_EVENT_UPDATE)){
			priority = PushNotificationKeys.NOTIFICATION_PRIORITY_HIGH;
		}else if(Objects.equals(type, PushNotificationKeys.NOTIFICATION_TYPE_BROADCAST_MESSAGE)){
			priority = PushNotificationKeys.NOTIFICATION_PRIORITY_MEDIUM;
		}

		return priority;
	}

	public List<PushNotification> findByNotificationIds(long[] notificationIds, int start, int end){
		return  pushNotificationFinder.findByNotificationIds(notificationIds, start, end);
	}

	/* ********************************************************************************** */
	/* ********************************************************************************** */
	/* ********************************************************************************** */

	public PushNotification addEntry(PushNotification orgEntry, ServiceContext serviceContext)
			throws PortalException, PushNotificationValidateException {

		// Validation

		ModelValidator<PushNotification> modelValidator = new PushNotificationValidator();
		modelValidator.validate(orgEntry);

		// Add entry
		/*PushNotification entry = _addEntry(orgEntry, serviceContext);
		PushNotification addedEntry = pushNotificationPersistence.update(entry);
		pushNotificationPersistence.clearCache();*/
		// send item added push notification
		saveNotification(PushNotificationKeys.NOTIFICATION_TYPE_BROADCAST_MESSAGE, 0, orgEntry.getTitle(), orgEntry.getBody(), null);
		return orgEntry;
	}

	public PushNotification updateEntry(PushNotification orgEntry, ServiceContext serviceContext)
			throws PortalException, PushNotificationValidateException {

		// Validation

		ModelValidator<PushNotification> modelValidator = new PushNotificationValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		PushNotification entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		PushNotification updatedEntry = pushNotificationPersistence.update(entry);
		pushNotificationPersistence.clearCache();

		return updatedEntry;
	}

	protected PushNotification _addEntry(PushNotification entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(PushNotification.class.getName());

		PushNotification newEntry = pushNotificationPersistence.create(id);

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setType(entry.getType());
		newEntry.setTypeId(entry.getTypeId());
		newEntry.setTitle(entry.getTitle());
		newEntry.setBody(entry.getBody());
		newEntry.setPriority(entry.getPriority());

		// return pushNotificationPersistence.update(newEntry);
		return newEntry;
	}

	protected PushNotification _updateEntry(long primaryKey, PushNotification entry, ServiceContext serviceContext) throws PortalException {

		PushNotification updateEntry = fetchPushNotification(primaryKey);

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setType(entry.getType());
		updateEntry.setTypeId(entry.getTypeId());
		updateEntry.setTitle(entry.getTitle());
		updateEntry.setBody(entry.getBody());
		updateEntry.setPriority(entry.getPriority());

		return updateEntry;
	}

	public PushNotification deleteEntry(long primaryKey) throws PortalException {
		PushNotification entry = getPushNotification(primaryKey);
		pushNotificationPersistence.remove(entry);

		return entry;
	}

	public PushNotification getPushNotificationFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, PushNotificationValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		PushNotification entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchPushNotification(primaryKey);
		}

		try {
			entry.setType(ParamUtil.getString(request, "type"));
			entry.setTypeId(ParamUtil.getLong(request, "typeId"));
			entry.setTitleMap(LocalizationUtil.getLocalizationMap(request, "title"));
			entry.setBodyMap(LocalizationUtil.getLocalizationMap(request, "body"));
			entry.setPriority(ParamUtil.getString(request, "priority"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new PushNotificationValidateException(error);
		}

		return entry;
	}

	public PushNotification getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(PushNotification.class.getName());

		return createPushNotification(primaryKey);
	}

	//get push notifications by type
	public List<PushNotification> findByType(String type){
		return pushNotificationPersistence.findByType(type);
	}
	public List<PushNotification> findByType(String type, int start, int end){
		return pushNotificationPersistence.findByType(type, start, end);
	}
	public List<PushNotification> findByType(String type, int start, int end, OrderByComparator<PushNotification> orderByComparator){
		return pushNotificationPersistence.findByType(type, start, end, orderByComparator);
	}

	public int countByType(String type){
		return pushNotificationPersistence.countByType(type);
	}

	private static Log _log = LogFactoryUtil.getLog(PushNotificationLocalServiceImpl.class);

}