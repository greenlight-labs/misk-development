package com.push.notification.constants;

public class PushNotificationKeys {
	public static final String NOTIFICATION_TYPE_NEWS = "news";
	public static final String NOTIFICATION_TYPE_EVENT = "event";
	public static final String NOTIFICATION_TYPE_EVENT_UPDATE = "eventUpdate";
	public static final String NOTIFICATION_TYPE_HIGHLIGHT = "highlight";
	public static final String NOTIFICATION_TYPE_BROADCAST_MESSAGE = "broadcastMessage";
	public static final String NOTIFICATION_PRIORITY_HIGH = "high";
	public static final String NOTIFICATION_PRIORITY_MEDIUM = "medium";
	public static final String NOTIFICATION_PRIORITY_LOW = "low";
	public static final String NOTIFICATION_TYPE_EVENT_ENTRY_ADDED = "Check out this new event <strong>[$ENTRY_TITLE$]</strong> happening.";
	public static final String NOTIFICATION_TYPE_EVENT_ENTRY_UPDATED = "Event name <strong>[$ENTRY_TITLE$]</strong> has been updated.";
	public static final String NOTIFICATION_TYPE_EVENT_ENTRY_DELETED = "Event name <strong>[$ENTRY_TITLE$]</strong> has been deleted.";
	public static final String NOTIFICATION_TYPE_HIGHLIGHT_ENTRY_ADDED = "Check out this new highlight <strong>[$ENTRY_TITLE$]</strong>.";
	public static final String NOTIFICATION_TYPE_NEWS_ENTRY_ADDED = "Check out this new story <strong>[$ENTRY_TITLE$]</strong>.";
	public static final String NOTIFICATION_TYPE_BROADCAST_MESSAGE_ENTRY_ADDED = "Check out this new event <strong>[$ENTRY_TITLE$]</strong> happening.";
}
