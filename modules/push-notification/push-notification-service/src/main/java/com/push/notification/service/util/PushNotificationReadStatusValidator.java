package com.push.notification.service.util;

import com.push.notification.model.PushNotificationReadStatus;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Gallery Validator
 *
 * @author tz
 */
public class PushNotificationReadStatusValidator implements ModelValidator<PushNotificationReadStatus> {

    @Override
    public void validate(PushNotificationReadStatus entry) throws PortalException {
        /*   */
        // Validate fields
        validateNotificationId(entry.getNotificationId());
        validateAppUserId(entry.getAppUserId());
        validateReadStatus(entry.getReadStatus());
    }

    /**
     * categoryId field Validation
     *
     * @param field categoryId
     */
    protected void validateNotificationId(long field) {
        if (Validator.isNull(field)) {
            _errors.add("push-notification-read-status-notification-id-required");
        }
    }

    protected void validateAppUserId(long field) {
        if (Validator.isNull(field)) {
            _errors.add("push-notification-read-status-app-user-id-required");
        }
    }

    protected void validateReadStatus(boolean field) {
        if (Validator.isNull(field)) {
            _errors.add("push-notification-read-status-read-status-required");
        }
    }

    protected List<String> _errors = new ArrayList<>();

}
