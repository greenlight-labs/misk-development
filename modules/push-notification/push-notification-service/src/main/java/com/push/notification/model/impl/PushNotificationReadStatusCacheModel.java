/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.push.notification.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.push.notification.model.PushNotificationReadStatus;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PushNotificationReadStatus in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class PushNotificationReadStatusCacheModel
	implements CacheModel<PushNotificationReadStatus>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof PushNotificationReadStatusCacheModel)) {
			return false;
		}

		PushNotificationReadStatusCacheModel
			pushNotificationReadStatusCacheModel =
				(PushNotificationReadStatusCacheModel)object;

		if (readStatusId == pushNotificationReadStatusCacheModel.readStatusId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, readStatusId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", readStatusId=");
		sb.append(readStatusId);
		sb.append(", notificationId=");
		sb.append(notificationId);
		sb.append(", appUserId=");
		sb.append(appUserId);
		sb.append(", readStatus=");
		sb.append(readStatus);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PushNotificationReadStatus toEntityModel() {
		PushNotificationReadStatusImpl pushNotificationReadStatusImpl =
			new PushNotificationReadStatusImpl();

		if (uuid == null) {
			pushNotificationReadStatusImpl.setUuid("");
		}
		else {
			pushNotificationReadStatusImpl.setUuid(uuid);
		}

		pushNotificationReadStatusImpl.setReadStatusId(readStatusId);
		pushNotificationReadStatusImpl.setNotificationId(notificationId);
		pushNotificationReadStatusImpl.setAppUserId(appUserId);
		pushNotificationReadStatusImpl.setReadStatus(readStatus);

		pushNotificationReadStatusImpl.resetOriginalValues();

		return pushNotificationReadStatusImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		readStatusId = objectInput.readLong();

		notificationId = objectInput.readLong();

		appUserId = objectInput.readLong();

		readStatus = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(readStatusId);

		objectOutput.writeLong(notificationId);

		objectOutput.writeLong(appUserId);

		objectOutput.writeBoolean(readStatus);
	}

	public String uuid;
	public long readStatusId;
	public long notificationId;
	public long appUserId;
	public boolean readStatus;

}