package com.push.notification.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import com.push.notification.model.PushNotification;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * PushNotification Validator 
 * 
 * @author tz
 *
 */
public class PushNotificationValidator implements ModelValidator<PushNotification> {

	@Override
	public void validate(PushNotification entry) throws PortalException {

        // Fields: push-notificationId, categoryId, name, listingImage, detailImage, venue, openingHours, openingDays, description, lat, lon, contactEmail, contactPhone
        validateNotificationId(entry.getNotificationId());
        validateType(entry.getType());
        validateTypeId(entry.getTypeId());
        validateTitle(entry.getTitle());
        validateBody(entry.getBody());
        validatePriority(entry.getPriority());

	}

    protected void validateNotificationId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("push-notification-id-required");
        }
    }

    protected void validateType(String field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("push-notification-type-required");
        }
    }

    protected void validateTypeId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("push-notification-type-id-required");
        }
    }

    protected void validateTitle(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("push-notification-title-required");
        }
    }

    protected void validateBody(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("push-notification-body-required");
        }
    }

    protected void validatePriority(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("push-notification-priority-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
