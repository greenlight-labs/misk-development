package com.push.notification.service.impl;

import com.push.notification.exception.PushNotificationReadStatusValidateException;
import com.push.notification.model.PushNotification;
import com.push.notification.model.PushNotificationReadStatus;
import com.push.notification.service.base.PushNotificationReadStatusLocalServiceBaseImpl;
import com.push.notification.service.util.PushNotificationReadStatusValidator;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

@Component(
        property = "model.class.name=com.push.notification.model.PushNotificationReadStatus", service = AopService.class
)
public class PushNotificationReadStatusLocalServiceImpl extends PushNotificationReadStatusLocalServiceBaseImpl {

    private static Log _log = LogFactoryUtil.getLog(PushNotificationReadStatusLocalServiceImpl.class);

    public PushNotificationReadStatus addEntry(PushNotificationReadStatus orgEntry, ServiceContext serviceContext)
            throws PortalException, PushNotificationReadStatusValidateException {

        // Validation

        ModelValidator<PushNotificationReadStatus> modelValidator = new PushNotificationReadStatusValidator();
        modelValidator.validate(orgEntry);

        // Add entry

        PushNotificationReadStatus entry = _addEntry(orgEntry, serviceContext);

        PushNotificationReadStatus addedEntry = pushNotificationReadStatusPersistence.update(entry);

        pushNotificationReadStatusPersistence.clearCache();

        return addedEntry;
    }

    public PushNotificationReadStatus updateEntry(PushNotificationReadStatus orgEntry, ServiceContext serviceContext)
            throws PortalException, PushNotificationReadStatusValidateException {

        // Validation

        ModelValidator<PushNotificationReadStatus> modelValidator = new PushNotificationReadStatusValidator();
        modelValidator.validate(orgEntry);

        // Update entry

        PushNotificationReadStatus entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

        PushNotificationReadStatus updatedEntry = pushNotificationReadStatusPersistence.update(entry);
        pushNotificationReadStatusPersistence.clearCache();

        return updatedEntry;
    }

    protected PushNotificationReadStatus _addEntry(PushNotificationReadStatus entry, ServiceContext serviceContext) throws PortalException {

        long id = counterLocalService.increment(PushNotificationReadStatus.class.getName());

        PushNotificationReadStatus newEntry = pushNotificationReadStatusPersistence.create(id);

        Date now = new Date();
        newEntry.setUuid(serviceContext.getUuid());

        newEntry.setNotificationId(entry.getNotificationId());
        newEntry.setAppUserId(entry.getAppUserId());
        newEntry.setReadStatus(entry.getReadStatus());

        return newEntry;
    }

    protected PushNotificationReadStatus _updateEntry(long primaryKey, PushNotificationReadStatus entry, ServiceContext serviceContext)
            throws PortalException {

        PushNotificationReadStatus updateEntry = fetchPushNotificationReadStatus(primaryKey);

        Date now = new Date();
        updateEntry.setUuid(entry.getUuid());

        updateEntry.setNotificationId(entry.getNotificationId());
        updateEntry.setAppUserId(entry.getAppUserId());
        updateEntry.setReadStatus(entry.getReadStatus());

        return updateEntry;
    }

    public PushNotificationReadStatus deleteEntry(long primaryKey) throws PortalException {
        PushNotificationReadStatus entry = getPushNotificationReadStatus(primaryKey);
        pushNotificationReadStatusPersistence.remove(entry);

        return entry;
    }

    public List<PushNotificationReadStatus> findByAppUserId(long appUserId) {

        return pushNotificationReadStatusPersistence.findByAppUserId(appUserId);
    }

    public List<PushNotificationReadStatus> findByAppUserId(long appUserId, int start, int end,
                                                          OrderByComparator<PushNotificationReadStatus> obc) {

        return pushNotificationReadStatusPersistence.findByAppUserId(appUserId, start, end, obc);
    }

    public List<PushNotificationReadStatus> findByAppUserId(long appUserId, int start, int end) {

        return pushNotificationReadStatusPersistence.findByAppUserId(appUserId, start, end);
    }

    public int countByAppUserId(long appUserId) {

        return pushNotificationReadStatusPersistence.countByAppUserId(appUserId);
    }

    public List<PushNotificationReadStatus> findByReadStatus(boolean readStatus) {

        return pushNotificationReadStatusPersistence.findByReadStatus(readStatus);
    }

    public List<PushNotificationReadStatus> findByReadStatus(boolean readStatus, int start, int end,
                                                   OrderByComparator<PushNotificationReadStatus> obc) {

        return pushNotificationReadStatusPersistence.findByReadStatus(readStatus, start, end, obc);
    }

    public List<PushNotificationReadStatus> findByReadStatus(boolean readStatus, int start, int end) {

        return pushNotificationReadStatusPersistence.findByReadStatus(readStatus, start, end);
    }

    public int countByReadStatus(boolean readStatus) {

        return pushNotificationReadStatusPersistence.countByReadStatus(readStatus);
    }

    public PushNotificationReadStatus fetchByAppUserNotification(long appUserId, long notificationId) {

        return pushNotificationReadStatusPersistence.fetchByAppUserNotification(appUserId, notificationId);
    }

    public int countByAppUserReadStatus(long appUserId, boolean readStatus) {

        return pushNotificationReadStatusPersistence.countByAppUserReadStatus(appUserId, readStatus);
    }

    public PushNotificationReadStatus getPushNotificationReadStatusFromRequest(long primaryKey, PortletRequest request)
            throws PortletException, PushNotificationReadStatusValidateException {

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

        // Create or fetch existing data

        PushNotificationReadStatus entry;

        if (primaryKey <= 0) {
            entry = getNewObject(primaryKey);
        } else {
            entry = fetchPushNotificationReadStatus(primaryKey);
        }

        try {
            entry.setReadStatusId(primaryKey);

            entry.setNotificationId(ParamUtil.getLong(request, "notificationId"));
            entry.setAppUserId(ParamUtil.getLong(request, "appUserId"));
            entry.setReadStatus(ParamUtil.getBoolean(request, "readStatus"));
        } catch (Exception e) {
            _log.error("Errors occur while populating the model", e);
            List<String> error = new ArrayList<>();
            error.add("value-convert-error");

            throw new PushNotificationReadStatusValidateException(error);
        }

        return entry;
    }

    public PushNotificationReadStatus getNewObject(long primaryKey) {
        primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(PushNotificationReadStatus.class.getName());

        return createPushNotificationReadStatus(primaryKey);
    }

    /* **********************************- Additional Code Here -***************************** */
}