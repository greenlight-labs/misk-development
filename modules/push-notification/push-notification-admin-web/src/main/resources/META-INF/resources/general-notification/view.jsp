<%@ page import="com.push.notification.service.PushNotificationLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.push.notification.constants.PushNotificationKeys" %>
<%@ include file="/init.jsp" %>

<div class="container-fluid-1280">
	<liferay-ui:search-container total="<%= PushNotificationLocalServiceUtil.countByType(PushNotificationKeys.NOTIFICATION_TYPE_BROADCAST_MESSAGE) %>">
		<liferay-ui:search-container-results
				results="<%= PushNotificationLocalServiceUtil.findByType(PushNotificationKeys.NOTIFICATION_TYPE_BROADCAST_MESSAGE,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

		<liferay-ui:search-container-row
				className="com.push.notification.model.PushNotification" modelVar="entry">

			<liferay-ui:search-container-column-text name="Title" value="<%= HtmlUtil.escape(entry.getTitle(locale)) %>"/>
			<liferay-ui:search-container-column-text name="Body" value="<%= HtmlUtil.escape(entry.getBody(locale)) %>"/>
			<%--<liferay-ui:search-container-column-text name="Priority" value="<%= HtmlUtil.escape(entry.getPriority()) %>"/>--%>

		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator/>
	</liferay-ui:search-container>
</div>