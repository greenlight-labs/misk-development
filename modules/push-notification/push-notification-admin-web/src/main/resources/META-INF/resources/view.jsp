<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@ page import="com.push.notification.service.PushNotificationLocalServiceUtil" %>
<%@ include file="/init.jsp" %>

<%
    PushNotification entry = null;
%>

<%--<portlet:renderURL var="viewURL">
	<portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>--%>

<portlet:actionURL name="addEntry" var="editEntryURL"/>

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= entry %>" model="<%= PushNotification.class %>"/>

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:input name="title" label="Title" autoSize="true" helpMessage="Max 75 Characters (Recommended)">
                    <aui:validator name="required"/>
                    <aui:validator name="custom">
                                function (val, fieldNode, ruleValue) {
                                    var value = window.document.querySelector('#<portlet:namespace />title_en_US').value;
                                    return value.length > 0;
                                }
                    </aui:validator>
                </aui:input>
                <aui:input name="body" label="Body" helpMessage="Max 500 Characters (Recommended)">
                    <aui:validator name="required"/>
                    <aui:validator name="custom">
                                function (val, fieldNode, ruleValue) {
                                    var value = window.document.querySelector('#<portlet:namespace />body_en_US').value;
                                    return value.length > 0;
                                }
                    </aui:validator>
                </aui:input>
                <%--<aui:select required="true" label="Priority" name="priority">
                    <aui:option label="High" value="high" />
                    <aui:option label="Medium" value="medium" />
                    <aui:option label="Low" value="low" />
                </aui:select>--%>
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit"/>
            <%--<aui:button onClick="<%= viewURL %>" type="cancel"  />--%>
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
</script>