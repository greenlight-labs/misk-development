package com.push.notification.admin.web.constants;

/**
 * @author tz
 */
public class PushNotificationAdminWebPanelCategoryKeys {

	public static final String CONTROL_PANEL_CATEGORY = "PushNotificationAdminWeb";

}