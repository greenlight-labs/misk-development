package com.push.notification.admin.web.constants;

/**
 * @author tz
 */
public class PushNotificationAdminWebPortletKeys {

	public static final String PUSHNOTIFICATIONADMINWEB =
		"com_push_notification_admin_web_PushNotificationAdminWebPortlet";

	public static final String GENERALNOTIFICATIONLIST =
		"com_push_notification_admin_web_GeneralNotificationListPortlet";

}