package com.push.notification.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import com.push.notification.admin.web.constants.PushNotificationAdminWebPanelCategoryKeys;
import com.push.notification.admin.web.constants.PushNotificationAdminWebPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=101",
		"panel.category.key=" + PushNotificationAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class GeneralNotificationListPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return PushNotificationAdminWebPortletKeys.GENERALNOTIFICATIONLIST;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + PushNotificationAdminWebPortletKeys.GENERALNOTIFICATIONLIST + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}