package com.push.notification.admin.web.portlet;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.push.notification.admin.web.constants.PushNotificationAdminWebPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import com.push.notification.exception.PushNotificationValidateException;
import com.push.notification.model.PushNotification;
import com.push.notification.service.PushNotificationLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=PushNotificationAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + PushNotificationAdminWebPortletKeys.PUSHNOTIFICATIONADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class PushNotificationAdminWebPortlet extends MVCPortlet {

	public void addEntry(ActionRequest request, ActionResponse response)
			throws Exception, PushNotificationValidateException {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

		PushNotification entry = _pushNotificationLocalService.getPushNotificationFromRequest(
				primaryKey, request);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				PushNotification.class.getName(), request);

		// Add entry

		_pushNotificationLocalService.addEntry(entry, serviceContext);

		SessionMessages.add(request, "pushNotificationAddedSuccessfully");
	}

	public void updateEntry(ActionRequest request, ActionResponse response)
			throws Exception {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

		PushNotification entry = _pushNotificationLocalService.getPushNotificationFromRequest(
				primaryKey, request);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				PushNotification.class.getName(), request);

		//Update entry
		_pushNotificationLocalService.updateEntry(entry, serviceContext);

		SessionMessages.add(request, "pushNotificationUpdatedSuccessfully");
	}

	public void deleteEntry(ActionRequest request, ActionResponse response)
			throws PortalException {

		long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

		try {
			_pushNotificationLocalService.deleteEntry(entryId);
		} catch (PortalException pe) {
			ReflectionUtil.throwException(pe);
		}
	}

	@Reference
	private PushNotificationLocalService _pushNotificationLocalService;
}