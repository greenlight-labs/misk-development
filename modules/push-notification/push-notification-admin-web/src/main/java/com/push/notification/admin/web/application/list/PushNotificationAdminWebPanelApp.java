package com.push.notification.admin.web.application.list;

import com.push.notification.admin.web.constants.PushNotificationAdminWebPanelCategoryKeys;
import com.push.notification.admin.web.constants.PushNotificationAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + PushNotificationAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class PushNotificationAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return PushNotificationAdminWebPortletKeys.PUSHNOTIFICATIONADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + PushNotificationAdminWebPortletKeys.PUSHNOTIFICATIONADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}