package faq.admin.web.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import faq.admin.web.constants.FaqAdminWebPortletKeys;
import faq.model.Faq;
import faq.service.FaqLocalService;

/**
 * @author manis
 */
@Component(immediate = true, property = { "com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden", "com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true", "com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false", "com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true", "javax.portlet.display-name=FaqsAdminWeb",
		"javax.portlet.expiration-cache=0", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + FaqAdminWebPortletKeys.FAQADMINWEB,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.security-role-ref=power-user,user",

}, service = Portlet.class)
public class FaqAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		super.render(renderRequest, renderResponse);
	}

	public void addFaq(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Faq.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		boolean active = ParamUtil.getBoolean(request, "active");
		long orderNo = ParamUtil.getLong(request, "orderNo",9999);
		Map<Locale, String> questionMap = LocalizationUtil.getLocalizationMap(request, "question");
		Map<Locale, String> answerMap = LocalizationUtil.getLocalizationMap(request, "answer");
		// default Value of OrderNo to 9999
		if (orderNo == 0) {
			orderNo = 9999;
		}
		try {

			Faq faqEntry = _faqLocalService.addFaq(serviceContext.getUserId(), questionMap, answerMap, active,orderNo,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(FaqAdminWebPortlet.class.getName()).log(Level.SEVERE, null, pe);

			response.setRenderParameter("mvcPath", "/edit_faq.jsp");
		}
	}

	public void updateFaq(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Faq.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		long faqId = ParamUtil.getLong(request, "faqId");
		long orderNo = ParamUtil.getLong(request, "orderNo",9999);
		boolean active = ParamUtil.getBoolean(request, "active");
		// default Value of OrderNo to 9999
		if (orderNo == 0) {
			orderNo = 9999;
		}
		
		Map<Locale, String> questionMap = LocalizationUtil.getLocalizationMap(request, "question");
		Map<Locale, String> answerMap = LocalizationUtil.getLocalizationMap(request, "answer");

		try {

			Faq faqEntry = _faqLocalService.updateFaq(serviceContext.getUserId(), faqId, questionMap, answerMap, active,orderNo,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(FaqAdminWebPortlet.class.getName()).log(Level.SEVERE, null, pe);

			response.setRenderParameter("mvcPath", "/edit_faq.jsp");
		}
	}

	public void deleteFaq(ActionRequest request, ActionResponse response) throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Faq.class.getName(), request);

		long faqId = ParamUtil.getLong(request, "faqId");

		try {

			_faqLocalService.deleteFaq(faqId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(FaqAdminWebPortlet.class.getName()).log(Level.SEVERE, null, pe);
		}
	}

	@Reference
	private FaqLocalService _faqLocalService;

	@Reference
	private Portal _portal;

	@Reference
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
}