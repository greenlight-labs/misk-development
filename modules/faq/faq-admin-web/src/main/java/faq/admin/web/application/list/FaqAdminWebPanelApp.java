package faq.admin.web.application.list;

import faq.admin.web.constants.FaqAdminWebPanelCategoryKeys;
import faq.admin.web.constants.FaqAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author manis
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + FaqAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class FaqAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return FaqAdminWebPortletKeys.FAQADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + FaqAdminWebPortletKeys.FAQADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}