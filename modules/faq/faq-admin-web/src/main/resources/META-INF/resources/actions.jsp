<%@include file="/init.jsp"%>

<%@ page import="faq.model.Faq" %>
<%@ page import="faq.service.FaqLocalServiceUtil" %>
<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Faq faq = (Faq) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="faqId"
                       value="<%=String.valueOf(faq.getFaqId()) %>" />
        <portlet:param name="mvcPath"
                       value="/edit_faq.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteFaq" var="deleteURL">
        <portlet:param name="faqId"
                       value="<%= String.valueOf(faq.getFaqId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>