<%@include file = "/init.jsp" %>
<%@ page import="faq.model.Faq" %>
<%@ page import="faq.service.FaqLocalServiceUtil" %>
<%
    long faqId = ParamUtil.getLong(request, "faqId");

    Faq faq = null;

    if (faqId > 0) {
        try {
        	faq = FaqLocalServiceUtil.getFaq(faqId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }

    
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= faq == null ? "addFaq" : "updateFaq" %>' var="editFaqURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editFaqURL %>" name="fm">

    <aui:model-context bean="<%= faq %>" model="<%= Faq.class %>" />

    <aui:input type="hidden" name="faqId"
               value='<%= faq == null ? "" : faq.getFaqId() %>' />

    <aui:fieldset-group markupView="lexicon">       
        <aui:fieldset>           
            <aui:input name="question" label="Question" helpMessage="Max 200 Characters (Recommended)" >
            <aui:validator name="required"/>
            </aui:input>
            
            <aui:input name="answer" label="Answer" helpMessage="Max 1050 Characters (Recommended)">
            <aui:validator name="required"/>
            </aui:input>
             <aui:input name="orderNo" label="Order Number" helpMessage="Order Number default value 9999" placeholder="9999">                
            </aui:input>
           <aui:input checked="<%=faq==null?true:faq.getActive()==true? true : false%>" inlineField="true" label="Enable" value="true" type="radio" name="active" id="active"></aui:input>
           <aui:input checked="<%=(faq != null && faq.getActive()==false)? true : false%>" inlineField="true" label="Disable" value="false" type="radio" name="active" id="active"></aui:input>
        </aui:fieldset>
    </aui:fieldset-group>

        <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

