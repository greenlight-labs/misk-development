
<%@page import="faq.model.Faq"%>
<%@page import="faq.service.FaqLocalServiceUtil"%>
<%@ include file="/init.jsp"%>
<%@page import="com.liferay.portal.kernel.portlet.PortalPreferences"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="java.util.Collections"%>
<%@page import="org.apache.commons.beanutils.BeanComparator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>

<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>

<%
 
 PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);
 String orderByCol = ParamUtil.getString(request, "orderByCol");
 String orderByType = ParamUtil.getString(request, "orderByType");

 if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
 	portalPrefs.setValue("NAME_SPACE", "order-by-col", orderByCol);
 	portalPrefs.setValue("NAME_SPACE", "order-by-type", orderByType);

 } else {
 	orderByCol = portalPrefs.getValue("NAME_SPACE", "order-by-col", "orderNo");
 	orderByType = portalPrefs.getValue("NAME_SPACE", "order-by-type", "asc");
 }
if(Validator.isNull(orderByCol)){
	orderByCol="orderNo";
}
if(Validator.isNull(orderByType)){
	orderByType="asc";
}
String sortingOrder = orderByType;
 %>

<div class="container-fluid-1280">

	<aui:button-row cssClass="faq-admin-buttons">
		<portlet:renderURL var="addFaqURL">
			<portlet:param name="mvcPath" value="/edit_faq.jsp" />
			<portlet:param name="redirect" value="<%= "currentURL" %>" />
		</portlet:renderURL>

		<aui:button onClick="<%= addFaqURL.toString() %>" value="Add Faq" />
	</aui:button-row>

	<liferay-ui:search-container
		total="<%= FaqLocalServiceUtil.getFaqsCount(scopeGroupId) %>"  orderByType="<%=orderByType %>" orderByCol="<%=orderByCol %>">
	<liferay-ui:search-container-results>
  <%
    
    List<Faq> faqList =FaqLocalServiceUtil.getFaqs(scopeGroupId,-1, -1);
    
    List<Faq> faqListPerPage = ListUtil.subList(faqList, searchContainer.getStart(),searchContainer.getEnd());
    int totalRecords =  FaqLocalServiceUtil.getFaqsCount(scopeGroupId);
    
    List<Faq> sortableFaqList = new ArrayList<Faq>(faqListPerPage);
    if(Validator.isNotNull(orderByCol)){
        //Pass the column name to BeanComparator to get comparator object
        BeanComparator comparator = new BeanComparator(orderByCol);
        //It will sort in ascending order
       Collections.sort(sortableFaqList, comparator);
        if(sortingOrder.equalsIgnoreCase("asc")){
        	 
        
        }else{
            //It will sort in descending order
            Collections.reverse(sortableFaqList);
        }
 
    }
    pageContext.setAttribute("beginIndex",searchContainer.getStart());
	pageContext.setAttribute("endIndex",searchContainer.getEnd());
    pageContext.setAttribute("results", sortableFaqList);
    pageContext.setAttribute("total", totalRecords);

    %> 
    
    </liferay-ui:search-container-results>
		<liferay-ui:search-container-row className="faq.model.Faq"
			modelVar="faq">

			<liferay-ui:search-container-column-text name="question"
				value="<%= HtmlUtil.escape(faq.getQuestion(locale)) %>" />
			<liferay-ui:search-container-column-text name="Created Date"
				value="<%= dateFormat.format(faq.getCreateDate()) %>" />
			<liferay-ui:search-container-column-text name="Order Number"
				value="<%=String.valueOf(faq.getOrderNo())%>" orderable="true" orderableProperty="orderNo"/>
			<liferay-ui:search-container-column-jsp align="right"
				path="/actions.jsp" />

		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator />
	</liferay-ui:search-container>
</div>