create index IX_41285D79 on misk_faq (groupId);
create index IX_CDFF1205 on misk_faq (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_7DBE55C7 on misk_faq (uuid_[$COLUMN_LENGTH:75$], groupId);