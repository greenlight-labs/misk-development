create table misk_faq (
	uuid_ VARCHAR(75) null,
	faqId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	orderNo LONG,
	question STRING null,
	answer STRING null,
	active_ BOOLEAN
);