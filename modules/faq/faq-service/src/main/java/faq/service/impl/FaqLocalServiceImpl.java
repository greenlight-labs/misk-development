/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package faq.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import faq.exception.FaqAnswerException;
import faq.exception.FaqQuestionException;
import faq.model.Faq;
import faq.service.base.FaqLocalServiceBaseImpl;

/**
 * The implementation of the faq local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>faq.service.FaqLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FaqLocalServiceBaseImpl
 */
@Component(property = "model.class.name=faq.model.Faq", service = AopService.class)
public class FaqLocalServiceImpl extends FaqLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>faq.service.FaqLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>faq.service.FaqLocalServiceUtil</code>.
	 */

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>faq.service.FaqLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>faq.service.FaqLocalServiceUtil</code>.
	 */
	public Faq addFaq(long userId, Map<Locale, String> questionMap, Map<Locale, String> answerMap, boolean active,
			long orderNo,ServiceContext serviceContext) throws PortalException {

		validate(questionMap, answerMap);
		long groupId = serviceContext.getScopeGroupId();
		User user = userLocalService.getUserById(userId);
		Date now = new Date();
		// default Value of OrderNo to 9999

		if (orderNo == 0) {
			orderNo = 9999;
		}
		long FaqId = counterLocalService.increment();
		Faq faq = faqPersistence.create(FaqId);
		faq.setUuid(serviceContext.getUuid());
		faq.setUserId(userId);
		faq.setGroupId(groupId);
		faq.setCompanyId(user.getCompanyId());
		faq.setUserName(user.getFullName());
		faq.setCreateDate(serviceContext.getCreateDate(now));
		faq.setModifiedDate(serviceContext.getModifiedDate(now));
		faq.setOrderNo(orderNo);
		faq.setQuestionMap(questionMap);
		faq.setAnswerMap(answerMap);
		faq.setActive(active);

		faqPersistence.update(faq);
		faqPersistence.clearCache();
		return faq;

	}

	public Faq updateFaq(long userId,long faqId, Map<Locale, String> questionMap, Map<Locale, String> answerMap, boolean active,
			long orderNo,ServiceContext serviceContext) throws PortalException {

		validate(questionMap, answerMap);

		long groupId = serviceContext.getScopeGroupId();
		// default Value of OrderNo to 9999

		if (orderNo == 0) {
			orderNo = 9999;
		}
		User user = userLocalService.getUserById(userId);
		Date now = new Date();
		Faq faq = getFaq(faqId);
		faq.setUuid(serviceContext.getUuid());
		faq.setUserId(userId);
		faq.setGroupId(groupId);
		faq.setCompanyId(user.getCompanyId());
		faq.setUserName(user.getFullName());
		faq.setModifiedDate(serviceContext.getModifiedDate(now));
		faq.setQuestionMap(questionMap);
		faq.setOrderNo(orderNo);
		faq.setAnswerMap(answerMap);
		faq.setActive(active);

		faqPersistence.update(faq);
		faqPersistence.clearCache();
		return faq;

	}

	@Override
	public Faq getFaq(long FaqId) throws PortalException {
		return faqPersistence.findByPrimaryKey(FaqId);
	}

	public Faq deleteFaq(long faqId, ServiceContext serviceContext) throws PortalException, SystemException {

		Faq faq = getFaq(faqId);
		faq = deleteFaq(faq);

		return faq;
	}

	public List<Faq> getFaqs(long groupId) {

		return faqPersistence.findByGroupId(groupId);
	}

	public List<Faq> getFaqs(long groupId, int start, int end, OrderByComparator<Faq> obc) {

		return faqPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Faq> getFaqs(long groupId, int start, int end) {

		return faqPersistence.findByGroupId(groupId, start, end);
	}

	public int getFaqsCount(long groupId) {

		return faqPersistence.countByGroupId(groupId);
	}

	protected void validate(Map<Locale, String> questionMap, Map<Locale, String> answerMap) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String question = questionMap.get(locale);

		if (Validator.isNull(question)) {
			throw new FaqQuestionException();
		}

		String answer = answerMap.get(locale);

		if (Validator.isNull(answer)) {
			throw new FaqAnswerException();
		}

	}

}