/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package faq.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import faq.model.Faq;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Faq in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FaqCacheModel implements CacheModel<Faq>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FaqCacheModel)) {
			return false;
		}

		FaqCacheModel faqCacheModel = (FaqCacheModel)object;

		if (faqId == faqCacheModel.faqId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, faqId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", faqId=");
		sb.append(faqId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", orderNo=");
		sb.append(orderNo);
		sb.append(", question=");
		sb.append(question);
		sb.append(", answer=");
		sb.append(answer);
		sb.append(", active=");
		sb.append(active);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Faq toEntityModel() {
		FaqImpl faqImpl = new FaqImpl();

		if (uuid == null) {
			faqImpl.setUuid("");
		}
		else {
			faqImpl.setUuid(uuid);
		}

		faqImpl.setFaqId(faqId);
		faqImpl.setGroupId(groupId);
		faqImpl.setCompanyId(companyId);
		faqImpl.setUserId(userId);

		if (userName == null) {
			faqImpl.setUserName("");
		}
		else {
			faqImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			faqImpl.setCreateDate(null);
		}
		else {
			faqImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			faqImpl.setModifiedDate(null);
		}
		else {
			faqImpl.setModifiedDate(new Date(modifiedDate));
		}

		faqImpl.setOrderNo(orderNo);

		if (question == null) {
			faqImpl.setQuestion("");
		}
		else {
			faqImpl.setQuestion(question);
		}

		if (answer == null) {
			faqImpl.setAnswer("");
		}
		else {
			faqImpl.setAnswer(answer);
		}

		faqImpl.setActive(active);

		faqImpl.resetOriginalValues();

		return faqImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		faqId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		orderNo = objectInput.readLong();
		question = objectInput.readUTF();
		answer = objectInput.readUTF();

		active = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(faqId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(orderNo);

		if (question == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(question);
		}

		if (answer == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(answer);
		}

		objectOutput.writeBoolean(active);
	}

	public String uuid;
	public long faqId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long orderNo;
	public String question;
	public String answer;
	public boolean active;

}