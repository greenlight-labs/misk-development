/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package faq.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FaqLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see FaqLocalService
 * @generated
 */
public class FaqLocalServiceWrapper
	implements FaqLocalService, ServiceWrapper<FaqLocalService> {

	public FaqLocalServiceWrapper(FaqLocalService faqLocalService) {
		_faqLocalService = faqLocalService;
	}

	/**
	 * Adds the faq to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FaqLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param faq the faq
	 * @return the faq that was added
	 */
	@Override
	public faq.model.Faq addFaq(faq.model.Faq faq) {
		return _faqLocalService.addFaq(faq);
	}

	@Override
	public faq.model.Faq addFaq(
			long userId, java.util.Map<java.util.Locale, String> questionMap,
			java.util.Map<java.util.Locale, String> answerMap, boolean active,
			long orderNo,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.addFaq(
			userId, questionMap, answerMap, active, orderNo, serviceContext);
	}

	/**
	 * Creates a new faq with the primary key. Does not add the faq to the database.
	 *
	 * @param faqId the primary key for the new faq
	 * @return the new faq
	 */
	@Override
	public faq.model.Faq createFaq(long faqId) {
		return _faqLocalService.createFaq(faqId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the faq from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FaqLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param faq the faq
	 * @return the faq that was removed
	 */
	@Override
	public faq.model.Faq deleteFaq(faq.model.Faq faq) {
		return _faqLocalService.deleteFaq(faq);
	}

	/**
	 * Deletes the faq with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FaqLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param faqId the primary key of the faq
	 * @return the faq that was removed
	 * @throws PortalException if a faq with the primary key could not be found
	 */
	@Override
	public faq.model.Faq deleteFaq(long faqId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.deleteFaq(faqId);
	}

	@Override
	public faq.model.Faq deleteFaq(
			long faqId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _faqLocalService.deleteFaq(faqId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _faqLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _faqLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>faq.model.impl.FaqModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _faqLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>faq.model.impl.FaqModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _faqLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _faqLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _faqLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public faq.model.Faq fetchFaq(long faqId) {
		return _faqLocalService.fetchFaq(faqId);
	}

	/**
	 * Returns the faq matching the UUID and group.
	 *
	 * @param uuid the faq's UUID
	 * @param groupId the primary key of the group
	 * @return the matching faq, or <code>null</code> if a matching faq could not be found
	 */
	@Override
	public faq.model.Faq fetchFaqByUuidAndGroupId(String uuid, long groupId) {
		return _faqLocalService.fetchFaqByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _faqLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _faqLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	/**
	 * Returns the faq with the primary key.
	 *
	 * @param faqId the primary key of the faq
	 * @return the faq
	 * @throws PortalException if a faq with the primary key could not be found
	 */
	@Override
	public faq.model.Faq getFaq(long faqId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.getFaq(faqId);
	}

	/**
	 * Returns the faq matching the UUID and group.
	 *
	 * @param uuid the faq's UUID
	 * @param groupId the primary key of the group
	 * @return the matching faq
	 * @throws PortalException if a matching faq could not be found
	 */
	@Override
	public faq.model.Faq getFaqByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.getFaqByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the faqs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>faq.model.impl.FaqModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of faqs
	 * @param end the upper bound of the range of faqs (not inclusive)
	 * @return the range of faqs
	 */
	@Override
	public java.util.List<faq.model.Faq> getFaqs(int start, int end) {
		return _faqLocalService.getFaqs(start, end);
	}

	@Override
	public java.util.List<faq.model.Faq> getFaqs(long groupId) {
		return _faqLocalService.getFaqs(groupId);
	}

	@Override
	public java.util.List<faq.model.Faq> getFaqs(
		long groupId, int start, int end) {

		return _faqLocalService.getFaqs(groupId, start, end);
	}

	@Override
	public java.util.List<faq.model.Faq> getFaqs(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<faq.model.Faq> obc) {

		return _faqLocalService.getFaqs(groupId, start, end, obc);
	}

	/**
	 * Returns all the faqs matching the UUID and company.
	 *
	 * @param uuid the UUID of the faqs
	 * @param companyId the primary key of the company
	 * @return the matching faqs, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<faq.model.Faq> getFaqsByUuidAndCompanyId(
		String uuid, long companyId) {

		return _faqLocalService.getFaqsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of faqs matching the UUID and company.
	 *
	 * @param uuid the UUID of the faqs
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of faqs
	 * @param end the upper bound of the range of faqs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching faqs, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<faq.model.Faq> getFaqsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<faq.model.Faq>
			orderByComparator) {

		return _faqLocalService.getFaqsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of faqs.
	 *
	 * @return the number of faqs
	 */
	@Override
	public int getFaqsCount() {
		return _faqLocalService.getFaqsCount();
	}

	@Override
	public int getFaqsCount(long groupId) {
		return _faqLocalService.getFaqsCount(groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _faqLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _faqLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the faq in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect FaqLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param faq the faq
	 * @return the faq that was updated
	 */
	@Override
	public faq.model.Faq updateFaq(faq.model.Faq faq) {
		return _faqLocalService.updateFaq(faq);
	}

	@Override
	public faq.model.Faq updateFaq(
			long userId, long faqId,
			java.util.Map<java.util.Locale, String> questionMap,
			java.util.Map<java.util.Locale, String> answerMap, boolean active,
			long orderNo,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _faqLocalService.updateFaq(
			userId, faqId, questionMap, answerMap, active, orderNo,
			serviceContext);
	}

	@Override
	public FaqLocalService getWrappedService() {
		return _faqLocalService;
	}

	@Override
	public void setWrappedService(FaqLocalService faqLocalService) {
		_faqLocalService = faqLocalService;
	}

	private FaqLocalService _faqLocalService;

}