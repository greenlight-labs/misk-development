/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import dines.model.Dine;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Dine in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class DineCacheModel implements CacheModel<Dine>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DineCacheModel)) {
			return false;
		}

		DineCacheModel dineCacheModel = (DineCacheModel)object;

		if (dineId == dineCacheModel.dineId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, dineId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(37);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", dineId=");
		sb.append(dineId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", image=");
		sb.append(image);
		sb.append(", overlayImage=");
		sb.append(overlayImage);
		sb.append(", openTimings=");
		sb.append(openTimings);
		sb.append(", deliveryTimings=");
		sb.append(deliveryTimings);
		sb.append(", call=");
		sb.append(call);
		sb.append(", website=");
		sb.append(website);
		sb.append(", buttonLabel=");
		sb.append(buttonLabel);
		sb.append(", buttonLink=");
		sb.append(buttonLink);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Dine toEntityModel() {
		DineImpl dineImpl = new DineImpl();

		if (uuid == null) {
			dineImpl.setUuid("");
		}
		else {
			dineImpl.setUuid(uuid);
		}

		dineImpl.setDineId(dineId);
		dineImpl.setGroupId(groupId);
		dineImpl.setCompanyId(companyId);
		dineImpl.setUserId(userId);

		if (userName == null) {
			dineImpl.setUserName("");
		}
		else {
			dineImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			dineImpl.setCreateDate(null);
		}
		else {
			dineImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			dineImpl.setModifiedDate(null);
		}
		else {
			dineImpl.setModifiedDate(new Date(modifiedDate));
		}

		dineImpl.setCategoryId(categoryId);

		if (title == null) {
			dineImpl.setTitle("");
		}
		else {
			dineImpl.setTitle(title);
		}

		if (image == null) {
			dineImpl.setImage("");
		}
		else {
			dineImpl.setImage(image);
		}

		if (overlayImage == null) {
			dineImpl.setOverlayImage("");
		}
		else {
			dineImpl.setOverlayImage(overlayImage);
		}

		if (openTimings == null) {
			dineImpl.setOpenTimings("");
		}
		else {
			dineImpl.setOpenTimings(openTimings);
		}

		if (deliveryTimings == null) {
			dineImpl.setDeliveryTimings("");
		}
		else {
			dineImpl.setDeliveryTimings(deliveryTimings);
		}

		if (call == null) {
			dineImpl.setCall("");
		}
		else {
			dineImpl.setCall(call);
		}

		if (website == null) {
			dineImpl.setWebsite("");
		}
		else {
			dineImpl.setWebsite(website);
		}

		if (buttonLabel == null) {
			dineImpl.setButtonLabel("");
		}
		else {
			dineImpl.setButtonLabel(buttonLabel);
		}

		if (buttonLink == null) {
			dineImpl.setButtonLink("");
		}
		else {
			dineImpl.setButtonLink(buttonLink);
		}

		dineImpl.resetOriginalValues();

		return dineImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		dineId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		categoryId = objectInput.readLong();
		title = objectInput.readUTF();
		image = objectInput.readUTF();
		overlayImage = objectInput.readUTF();
		openTimings = objectInput.readUTF();
		deliveryTimings = objectInput.readUTF();
		call = objectInput.readUTF();
		website = objectInput.readUTF();
		buttonLabel = objectInput.readUTF();
		buttonLink = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(dineId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(categoryId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (overlayImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(overlayImage);
		}

		if (openTimings == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(openTimings);
		}

		if (deliveryTimings == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(deliveryTimings);
		}

		if (call == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(call);
		}

		if (website == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(website);
		}

		if (buttonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLabel);
		}

		if (buttonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLink);
		}
	}

	public String uuid;
	public long dineId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long categoryId;
	public String title;
	public String image;
	public String overlayImage;
	public String openTimings;
	public String deliveryTimings;
	public String call;
	public String website;
	public String buttonLabel;
	public String buttonLink;

}