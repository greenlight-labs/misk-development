/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import dines.exception.DineCategoryIdException;
import dines.exception.DineImageException;
import dines.exception.DineTitleException;
import dines.model.Dine;
import dines.service.base.DineLocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the dine local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>dines.service.DineLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DineLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=dines.model.Dine", service = AopService.class
)
public class DineLocalServiceImpl extends DineLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>dines.service.DineLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>dines.service.DineLocalServiceUtil</code>.
	 */

	public Dine addDine(long userId,
						long categoryId, Map<Locale, String> titleMap, Map<Locale, String> imageMap, Map<Locale, String> overlayImageMap, Map<Locale, String> openTimingsMap,
						Map<Locale, String> deliveryTimingsMap, Map<Locale, String> callMap, Map<Locale, String> websiteMap, Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
						ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(categoryId, titleMap, imageMap);

		long dineId = counterLocalService.increment();

		Dine dine = dinePersistence.create(dineId);

		dine.setUuid(serviceContext.getUuid());
		dine.setUserId(userId);
		dine.setGroupId(groupId);
		dine.setCompanyId(user.getCompanyId());
		dine.setUserName(user.getFullName());
		dine.setCreateDate(serviceContext.getCreateDate(now));
		dine.setModifiedDate(serviceContext.getModifiedDate(now));

		dine.setCategoryId(categoryId);
		dine.setTitleMap(titleMap);
		dine.setImageMap(imageMap);
		dine.setOverlayImageMap(overlayImageMap);
		dine.setOpenTimingsMap(openTimingsMap);
		dine.setDeliveryTimingsMap(deliveryTimingsMap);
		dine.setCallMap(callMap);
		dine.setWebsiteMap(websiteMap);
		dine.setButtonLabelMap(buttonLabelMap);
		dine.setButtonLinkMap(buttonLinkMap);

		dine.setExpandoBridgeAttributes(serviceContext);

		dinePersistence.update(dine);
		dinePersistence.clearCache();

		return dine;
	}

	public Dine updateDine(long userId, long dineId,
						   long categoryId, Map<Locale, String> titleMap, Map<Locale, String> imageMap, Map<Locale, String> overlayImageMap, Map<Locale, String> openTimingsMap,
						   Map<Locale, String> deliveryTimingsMap, Map<Locale, String> callMap, Map<Locale, String> websiteMap, Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
							 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(categoryId, titleMap, imageMap);

		Dine dine = getDine(dineId);

		User user = userLocalService.getUser(userId);

		dine.setUserId(userId);
		dine.setUserName(user.getFullName());
		dine.setModifiedDate(serviceContext.getModifiedDate(now));

		dine.setCategoryId(categoryId);
		dine.setTitleMap(titleMap);
		dine.setImageMap(imageMap);
		dine.setOverlayImageMap(overlayImageMap);
		dine.setOpenTimingsMap(openTimingsMap);
		dine.setDeliveryTimingsMap(deliveryTimingsMap);
		dine.setCallMap(callMap);
		dine.setWebsiteMap(websiteMap);
		dine.setButtonLabelMap(buttonLabelMap);
		dine.setButtonLinkMap(buttonLinkMap);

		dine.setExpandoBridgeAttributes(serviceContext);

		dinePersistence.update(dine);
		dinePersistence.clearCache();

		return dine;
	}

	public Dine deleteDine(long dineId,
							 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Dine dine = getDine(dineId);
		dine = deleteDine(dine);

		return dine;
	}

	public List<Dine> getDines(long groupId) {

		return dinePersistence.findByGroupId(groupId);
	}

	public List<Dine> getDines(long groupId, int start, int end,
								 OrderByComparator<Dine> obc) {

		return dinePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Dine> getDines(long groupId, int start, int end) {

		return dinePersistence.findByGroupId(groupId, start, end);
	}

	public int getDinesCount(long groupId) {

		return dinePersistence.countByGroupId(groupId);
	}

	protected void validate(
			long categoryId, Map<Locale, String> titleMap, Map<Locale, String> imageMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		if (Validator.isNull(categoryId)) {
			throw new DineCategoryIdException();
		}

		String title = titleMap.get(locale);

		if (Validator.isNull(title)) {
			throw new DineTitleException();
		}

		String listingDescription = imageMap.get(locale);

		if (Validator.isNull(listingDescription)) {
			throw new DineImageException();
		}

	}
}