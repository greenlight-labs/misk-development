/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import dines.service.DineServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>DineServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DineServiceSoap
 * @generated
 */
public class DineServiceHttp {

	public static java.util.List<dines.model.Dine> getDines(
		HttpPrincipal httpPrincipal, long groupId) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDines", _getDinesParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey, groupId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> getDines(
		HttpPrincipal httpPrincipal, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDines", _getDinesParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, groupId, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> getDines(
		HttpPrincipal httpPrincipal, long groupId, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDines", _getDinesParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, groupId, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getDinesCount(HttpPrincipal httpPrincipal, long groupId) {
		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDinesCount",
				_getDinesCountParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey, groupId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> getDinesByCategory(
		HttpPrincipal httpPrincipal, long categoryId) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDinesByCategory",
				_getDinesByCategoryParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> getDinesByCategory(
		HttpPrincipal httpPrincipal, long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDinesByCategory",
				_getDinesByCategoryParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> getDinesByCategory(
		HttpPrincipal httpPrincipal, long categoryId, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDinesByCategory",
				_getDinesByCategoryParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getDinesByCategoryCount(
		HttpPrincipal httpPrincipal, long categoryId) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "getDinesByCategoryCount",
				_getDinesByCategoryCountParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, categoryId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> findByTitle(
		HttpPrincipal httpPrincipal, String title) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "findByTitle",
				_findByTitleParameterTypes8);

			MethodHandler methodHandler = new MethodHandler(methodKey, title);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> findByTitle(
		HttpPrincipal httpPrincipal, String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			obc) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "findByTitle",
				_findByTitleParameterTypes9);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, start, end, obc);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<dines.model.Dine> findByTitle(
		HttpPrincipal httpPrincipal, String title, int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "findByTitle",
				_findByTitleParameterTypes10);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<dines.model.Dine>)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int findByTitleCount(
		HttpPrincipal httpPrincipal, String title) {

		try {
			MethodKey methodKey = new MethodKey(
				DineServiceUtil.class, "findByTitleCount",
				_findByTitleCountParameterTypes11);

			MethodHandler methodHandler = new MethodHandler(methodKey, title);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(DineServiceHttp.class);

	private static final Class<?>[] _getDinesParameterTypes0 = new Class[] {
		long.class
	};
	private static final Class<?>[] _getDinesParameterTypes1 = new Class[] {
		long.class, int.class, int.class,
		com.liferay.portal.kernel.util.OrderByComparator.class
	};
	private static final Class<?>[] _getDinesParameterTypes2 = new Class[] {
		long.class, int.class, int.class
	};
	private static final Class<?>[] _getDinesCountParameterTypes3 =
		new Class[] {long.class};
	private static final Class<?>[] _getDinesByCategoryParameterTypes4 =
		new Class[] {long.class};
	private static final Class<?>[] _getDinesByCategoryParameterTypes5 =
		new Class[] {
			long.class, int.class, int.class,
			com.liferay.portal.kernel.util.OrderByComparator.class
		};
	private static final Class<?>[] _getDinesByCategoryParameterTypes6 =
		new Class[] {long.class, int.class, int.class};
	private static final Class<?>[] _getDinesByCategoryCountParameterTypes7 =
		new Class[] {long.class};
	private static final Class<?>[] _findByTitleParameterTypes8 = new Class[] {
		String.class
	};
	private static final Class<?>[] _findByTitleParameterTypes9 = new Class[] {
		String.class, int.class, int.class,
		com.liferay.portal.kernel.util.OrderByComparator.class
	};
	private static final Class<?>[] _findByTitleParameterTypes10 = new Class[] {
		String.class, int.class, int.class
	};
	private static final Class<?>[] _findByTitleCountParameterTypes11 =
		new Class[] {String.class};

}