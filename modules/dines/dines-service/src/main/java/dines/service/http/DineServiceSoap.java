/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import dines.service.DineServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * <code>DineServiceUtil</code> service
 * utility. The static methods of this class call the same methods of the
 * service utility. However, the signatures are different because it is
 * difficult for SOAP to support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a <code>java.util.List</code>,
 * that is translated to an array of
 * <code>dines.model.DineSoap</code>. If the method in the
 * service utility returns a
 * <code>dines.model.Dine</code>, that is translated to a
 * <code>dines.model.DineSoap</code>. Methods that SOAP
 * cannot safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DineServiceHttp
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class DineServiceSoap {

	public static dines.model.DineSoap[] getDines(long groupId)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.getDines(groupId);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] getDines(
			long groupId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
				obc)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.getDines(groupId, start, end, obc);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] getDines(
			long groupId, int start, int end)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.getDines(groupId, start, end);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static int getDinesCount(long groupId) throws RemoteException {
		try {
			int returnValue = DineServiceUtil.getDinesCount(groupId);

			return returnValue;
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] getDinesByCategory(long categoryId)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.getDinesByCategory(categoryId);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] getDinesByCategory(
			long categoryId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
				obc)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.getDinesByCategory(categoryId, start, end, obc);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] getDinesByCategory(
			long categoryId, int start, int end)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.getDinesByCategory(categoryId, start, end);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static int getDinesByCategoryCount(long categoryId)
		throws RemoteException {

		try {
			int returnValue = DineServiceUtil.getDinesByCategoryCount(
				categoryId);

			return returnValue;
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] findByTitle(String title)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.findByTitle(title);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] findByTitle(
			String title, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
				obc)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.findByTitle(title, start, end, obc);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static dines.model.DineSoap[] findByTitle(
			String title, int start, int end)
		throws RemoteException {

		try {
			java.util.List<dines.model.Dine> returnValue =
				DineServiceUtil.findByTitle(title, start, end);

			return dines.model.DineSoap.toSoapModels(returnValue);
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	public static int findByTitleCount(String title) throws RemoteException {
		try {
			int returnValue = DineServiceUtil.findByTitleCount(title);

			return returnValue;
		}
		catch (Exception exception) {
			_log.error(exception, exception);

			throw new RemoteException(exception.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(DineServiceSoap.class);

}