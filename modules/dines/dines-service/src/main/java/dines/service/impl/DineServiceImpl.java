/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import dines.model.Dine;
import dines.service.base.DineServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the dine remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>dines.service.DineService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DineServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=dine",
		"json.web.service.context.path=Dine"
	},
	service = AopService.class
)
public class DineServiceImpl extends DineServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>dines.service.DineServiceUtil</code> to access the dine remote service.
	 */
	public List<Dine> getDines(long groupId) {

		return dinePersistence.findByGroupId(groupId);
	}

	public List<Dine> getDines(long groupId, int start, int end,
								 OrderByComparator<Dine> obc) {

		return dinePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Dine> getDines(long groupId, int start, int end) {

		return dinePersistence.findByGroupId(groupId, start, end);
	}

	public int getDinesCount(long groupId) {

		return dinePersistence.countByGroupId(groupId);
	}

	/* *
	 * get dines by type
	 * */
	public List<Dine> getDinesByCategory(long categoryId) {

		return dinePersistence.findByCategoryId(categoryId);
	}

	public List<Dine> getDinesByCategory(long categoryId, int start, int end,
										OrderByComparator<Dine> obc) {

		return dinePersistence.findByCategoryId(categoryId, start, end, obc);
	}

	public List<Dine> getDinesByCategory(long categoryId, int start, int end) {

		return dinePersistence.findByCategoryId(categoryId, start, end);
	}

	public int getDinesByCategoryCount(long categoryId) {

		return dinePersistence.countByCategoryId(categoryId);
	}

	/* *
	 * find dines by title
	 * */
	public List<Dine> findByTitle(String title) {

		return dinePersistence.findByTitle('%'+title+'%');
	}

	public List<Dine> findByTitle(String title, int start, int end,
										 OrderByComparator<Dine> obc) {

		return dinePersistence.findByTitle('%'+title+'%', start, end, obc);
	}

	public List<Dine> findByTitle(String title, int start, int end) {

		return dinePersistence.findByTitle('%'+title+'%', start, end);
	}

	public int findByTitleCount(String title) {

		return dinePersistence.countByTitle('%'+title+'%');
	}
}