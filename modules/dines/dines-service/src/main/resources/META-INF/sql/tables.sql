create table misk_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug VARCHAR(75) null,
	svgIcon STRING null
);

create table misk_dines (
	uuid_ VARCHAR(75) null,
	dineId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	categoryId LONG,
	title STRING null,
	image STRING null,
	overlayImage STRING null,
	openTimings STRING null,
	deliveryTimings STRING null,
	call_ STRING null,
	website STRING null,
	buttonLabel STRING null,
	buttonLink STRING null
);

create table misk_dines_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug VARCHAR(75) null,
	svgIcon STRING null
);