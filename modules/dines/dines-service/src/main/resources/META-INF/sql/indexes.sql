create index IX_6F0BD6FD on misk_categories (groupId);
create index IX_3BD20D01 on misk_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_78CBDFC3 on misk_categories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_F725389F on misk_dines (categoryId);
create index IX_DD795038 on misk_dines (groupId);
create index IX_5BD3EA16 on misk_dines (title[$COLUMN_LENGTH:75$]);
create index IX_BB69C626 on misk_dines (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_C4D99E28 on misk_dines (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_69463BF5 on misk_dines_categories (groupId);
create index IX_7A2DBF09 on misk_dines_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_FC73CB on misk_dines_categories (uuid_[$COLUMN_LENGTH:75$], groupId);