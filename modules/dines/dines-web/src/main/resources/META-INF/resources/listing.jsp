<%
    List<Dine> dines;

    if(Validator.isNotNull(query)){
        dines = DineServiceUtil.findByTitle(query);
    }else if(Validator.isNotNull(categoryId)){
        dines = DineServiceUtil.getDinesByCategory(categoryId);
    }else{
        dines = DineServiceUtil.getDines(scopeGroupId);
    }

    if(dines.size() > 0){
        for (Dine curDine : dines) {
%>
<portlet:renderURL var="detailPageURL2">
    <portlet:param name="dineId" value="<%= String.valueOf(curDine.getDineId()) %>" />
    <portlet:param name="mvcPath" value="/detail.jsp" />
</portlet:renderURL>
<div class="dine-card">
    <div class="dine-image">
        <img class="img-fluid" src="<%= curDine.getImage(locale) %>" alt="">
        <a href="javascript:" class="dine-image-overlay">
            <img class="img-fluid" src="<%= curDine.getOverlayImage(locale) %>" alt="">
        </a>
    </div>
    <div class="dine-card-content">
        <h4><%= curDine.getTitle(locale) %></h4>
        <div class="dine-card-data">
            <ul class="dine-card-data-head">
                <li>Open Timings:</li>
                <li>Delivery Timings:</li>
                <li>Call:</li>
                <li>Website:</li>
            </ul>
            <ul class="dine-card-data-text">
                <li><%= curDine.getOpenTimings(locale) %></li>
                <li><%= curDine.getDeliveryTimings(locale) %></li>
                <li><a href="tel:<%= curDine.getCall(locale) %>"><%= curDine.getCall(locale) %></a></li>
                <li><a href="<%= curDine.getWebsite(locale) %>" target="_blank"><%= curDine.getWebsite(locale).replaceFirst("https?://", "") %></a></li>
            </ul>
        </div>
        <a href="<%= curDine.getButtonLink(locale) %>" class="btn btn-outline-primary" target="_blank">
            <span><%= curDine.getButtonLabel(locale) %></span><i class="dot-line"></i>
        </a>
    </div>
</div>
<%
        }
    } else {
%>
<div class="text-center pt-5">
    <h4>No Record Found.</h4>
</div>
<%
    }
%>