package dines.admin.web.application.list;

import dines.admin.web.constants.DinesAdminWebPanelCategoryKeys;
import dines.admin.web.constants.DinesAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + DinesAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class DinesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return DinesAdminWebPortletKeys.DINESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + DinesAdminWebPortletKeys.DINESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}