package dines.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import dines.admin.web.constants.DinesAdminWebPortletKeys;
import dines.model.Dine;
import dines.service.DineLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.add-default-resource=true",
                "com.liferay.portlet.display-category=category.hidden",
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.layout-cacheable=true",
                "com.liferay.portlet.private-request-attributes=false",
                "com.liferay.portlet.private-session-attributes=false",
                "com.liferay.portlet.render-weight=50",
                "com.liferay.portlet.use-default-template=true",
                "javax.portlet.display-name=DinesAdminWeb",
                "javax.portlet.expiration-cache=0",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + DinesAdminWebPortletKeys.DINESADMINWEB,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",

        },
        service = Portlet.class
)
public class DinesAdminWebPortlet extends MVCPortlet {

    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
                .create(renderRequest);

        ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

        List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
        desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

        imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

        PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
                "selectDocumentLibrary", imageItemSelectorCriterion);

        renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

        super.render(renderRequest, renderResponse);
    }

    public void addDine(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Dine.class.getName(), request);

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
                WebKeys.THEME_DISPLAY);

        long categoryId = ParamUtil.getLong(request, "categoryId");
        Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
        Map<Locale, String> imageMap = LocalizationUtil.getLocalizationMap(request, "image");
        Map<Locale, String> overlayImageMap = LocalizationUtil.getLocalizationMap(request, "overlayImage");
        Map<Locale, String> openTimingsMap = LocalizationUtil.getLocalizationMap(request, "openTimings");
        Map<Locale, String> deliveryTimingsMap = LocalizationUtil.getLocalizationMap(request, "deliveryTimings");
        Map<Locale, String> callMap = LocalizationUtil.getLocalizationMap(request, "call");
        Map<Locale, String> websiteMap = LocalizationUtil.getLocalizationMap(request, "website");
        Map<Locale, String> buttonLabelMap = LocalizationUtil.getLocalizationMap(request, "buttonLabel");
        Map<Locale, String> buttonLinkMap = LocalizationUtil.getLocalizationMap(request, "buttonLink");

        try {
            _dineLocalService.addDine(serviceContext.getUserId(),
                    categoryId, titleMap, imageMap, overlayImageMap, openTimingsMap,
                    deliveryTimingsMap, callMap, websiteMap, buttonLabelMap, buttonLinkMap,
                    serviceContext);
        } catch (PortalException pe) {

            Logger.getLogger(DinesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);

            response.setRenderParameter(
                    "mvcPath", "/edit_dine.jsp");
        }
    }

    public void updateDine(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Dine.class.getName(), request);

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
                WebKeys.THEME_DISPLAY);

        long dineId = ParamUtil.getLong(request, "dineId");

        long categoryId = ParamUtil.getLong(request, "categoryId");
        Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
        Map<Locale, String> imageMap = LocalizationUtil.getLocalizationMap(request, "image");
        Map<Locale, String> overlayImageMap = LocalizationUtil.getLocalizationMap(request, "overlayImage");
        Map<Locale, String> openTimingsMap = LocalizationUtil.getLocalizationMap(request, "openTimings");
        Map<Locale, String> deliveryTimingsMap = LocalizationUtil.getLocalizationMap(request, "deliveryTimings");
        Map<Locale, String> callMap = LocalizationUtil.getLocalizationMap(request, "call");
        Map<Locale, String> websiteMap = LocalizationUtil.getLocalizationMap(request, "website");
        Map<Locale, String> buttonLabelMap = LocalizationUtil.getLocalizationMap(request, "buttonLabel");
        Map<Locale, String> buttonLinkMap = LocalizationUtil.getLocalizationMap(request, "buttonLink");

        try {
            _dineLocalService.updateDine(serviceContext.getUserId(), dineId,
                    categoryId, titleMap, imageMap, overlayImageMap, openTimingsMap,
                    deliveryTimingsMap, callMap, websiteMap, buttonLabelMap, buttonLinkMap,
                    serviceContext);

        } catch (PortalException pe) {

            Logger.getLogger(DinesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);

            response.setRenderParameter(
                    "mvcPath", "/edit_dine.jsp");
        }
    }

    public void deleteDine(ActionRequest request, ActionResponse response)
            throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(
                Dine.class.getName(), request);

        long dineId = ParamUtil.getLong(request, "dineId");

        try {
            _dineLocalService.deleteDine(dineId, serviceContext);
        } catch (PortalException pe) {

            Logger.getLogger(DinesAdminWebPortlet.class.getName()).log(
                    Level.SEVERE, null, pe);
        }
    }

    @Reference
    private DineLocalService _dineLocalService;

    @Reference
    private ItemSelector _itemSelector;
}