package dines.admin.web.constants;

/**
 * @author tz
 */
public class DinesAdminWebPortletKeys {

	public static final String DINESADMINWEB =
		"dines_admin_web_DinesAdminWebPortlet";

	public static final String CATEGORIESADMINWEB =
		"dines_admin_web_CategoriesAdminWebPortlet";

}