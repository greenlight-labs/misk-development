<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="dines-admin-buttons">
        <portlet:renderURL var="addDineURL">
            <portlet:param name="mvcPath"
                           value="/edit_dine.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addDineURL.toString() %>"
                    value="Add Dine"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= DineLocalServiceUtil.getDinesCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= DineLocalServiceUtil.getDines(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="dines.model.Dine" modelVar="dine">

            <liferay-ui:search-container-column-text
                    name="Title"
                    value="<%= HtmlUtil.escape(dine.getTitle(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>