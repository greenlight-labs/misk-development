<%@include file = "init.jsp" %>

<%
    long dineId = ParamUtil.getLong(request, "dineId");

    Dine dine = null;

    if (dineId > 0) {
        try {
            dine = DineLocalServiceUtil.getDine(dineId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }

    List<Category> categories = CategoryServiceUtil.getCategories(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= dine == null ? "addDine" : "updateDine" %>' var="editDineURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editDineURL %>" name="fm">

    <aui:model-context bean="<%= dine %>" model="<%= Dine.class %>" />

    <aui:input type="hidden" name="dineId"
               value='<%= dine == null ? "" : dine.getDineId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:select required="true" inlineField="<%= true %>" label="Category" name="categoryId">
                <% for (Category curCategory : categories) { %>
                    <aui:option label="<%= curCategory.getName(locale) %>" value="<%= curCategory.getCategoryId() %>" />
                <% } %>
            </aui:select>
            <aui:input name="title" label="Title" autoSize="true" helpMessage="Max 15 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="image" label="Image" helpMessage="Image Dimensions: 530 x 500 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="image">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="overlayImage" label="Overlay Image" helpMessage="Image Dimensions: Max 350 x 200 pixels (Recommended)">
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="overlayImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="openTimings" label="Open Timings" helpMessage="Max 25 Characters (Recommended)" />
            <aui:input name="deliveryTimings" label="Delivery Timings" helpMessage="Max 25 Characters (Recommended)" />
            <aui:input name="call" label="Call" helpMessage="Max 25 Characters (Recommended)" />
            <aui:input name="website" label="Website">
                <aui:validator name="url"/>
                <aui:validator name="custom" errorMessage="Please add http or https at the start.">
                            function (val, fieldNode, ruleValue) {
                                if(!val.match(/^[a-zA-Z]+:\/\//)){
                                    return false;
                                }else{
                                    return true;
                                }
                            }
                </aui:validator>
            </aui:input>
            <aui:input name="buttonLabel" label="Button Label" />
            <aui:input name="buttonLink" label="Button Link" helpMessage="Please type external website link here.">
                <aui:validator name="url"/>
                <aui:validator name="custom" errorMessage="Please add http or https at the start.">
                    function (val, fieldNode, ruleValue) {
                        if(!val.match(/^[a-zA-Z]+:\/\//)){
                            return false;
                        }else{
                            return true;
                        }
                    }
                </aui:validator>
            </aui:input>
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>