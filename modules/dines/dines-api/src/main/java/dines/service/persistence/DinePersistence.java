/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import dines.exception.NoSuchDineException;

import dines.model.Dine;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the dine service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DineUtil
 * @generated
 */
@ProviderType
public interface DinePersistence extends BasePersistence<Dine> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DineUtil} to access the dine persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the dines where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching dines
	 */
	public java.util.List<Dine> findByUuid(String uuid);

	/**
	 * Returns a range of all the dines where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of matching dines
	 */
	public java.util.List<Dine> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the dines where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the dines where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first dine in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the first dine in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the last dine in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the last dine in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the dines before and after the current dine in the ordered set where uuid = &#63;.
	 *
	 * @param dineId the primary key of the current dine
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dine
	 * @throws NoSuchDineException if a dine with the primary key could not be found
	 */
	public Dine[] findByUuid_PrevAndNext(
			long dineId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Removes all the dines where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of dines where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching dines
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the dine where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDineException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByUUID_G(String uuid, long groupId)
		throws NoSuchDineException;

	/**
	 * Returns the dine where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the dine where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the dine where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the dine that was removed
	 */
	public Dine removeByUUID_G(String uuid, long groupId)
		throws NoSuchDineException;

	/**
	 * Returns the number of dines where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching dines
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the dines where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching dines
	 */
	public java.util.List<Dine> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the dines where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of matching dines
	 */
	public java.util.List<Dine> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the dines where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the dines where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first dine in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the first dine in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the last dine in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the last dine in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the dines before and after the current dine in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param dineId the primary key of the current dine
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dine
	 * @throws NoSuchDineException if a dine with the primary key could not be found
	 */
	public Dine[] findByUuid_C_PrevAndNext(
			long dineId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Removes all the dines where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of dines where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching dines
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the dines where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching dines
	 */
	public java.util.List<Dine> findByGroupId(long groupId);

	/**
	 * Returns a range of all the dines where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of matching dines
	 */
	public java.util.List<Dine> findByGroupId(long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the dines where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the dines where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first dine in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the first dine in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the last dine in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the last dine in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the dines before and after the current dine in the ordered set where groupId = &#63;.
	 *
	 * @param dineId the primary key of the current dine
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dine
	 * @throws NoSuchDineException if a dine with the primary key could not be found
	 */
	public Dine[] findByGroupId_PrevAndNext(
			long dineId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Removes all the dines where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of dines where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching dines
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the dines where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching dines
	 */
	public java.util.List<Dine> findByCategoryId(long categoryId);

	/**
	 * Returns a range of all the dines where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of matching dines
	 */
	public java.util.List<Dine> findByCategoryId(
		long categoryId, int start, int end);

	/**
	 * Returns an ordered range of all the dines where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the dines where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first dine in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByCategoryId_First(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the first dine in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByCategoryId_First(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the last dine in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByCategoryId_Last(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the last dine in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByCategoryId_Last(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the dines before and after the current dine in the ordered set where categoryId = &#63;.
	 *
	 * @param dineId the primary key of the current dine
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dine
	 * @throws NoSuchDineException if a dine with the primary key could not be found
	 */
	public Dine[] findByCategoryId_PrevAndNext(
			long dineId, long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Removes all the dines where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public void removeByCategoryId(long categoryId);

	/**
	 * Returns the number of dines where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching dines
	 */
	public int countByCategoryId(long categoryId);

	/**
	 * Returns all the dines where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the matching dines
	 */
	public java.util.List<Dine> findByTitle(String title);

	/**
	 * Returns a range of all the dines where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of matching dines
	 */
	public java.util.List<Dine> findByTitle(String title, int start, int end);

	/**
	 * Returns an ordered range of all the dines where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the dines where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching dines
	 */
	public java.util.List<Dine> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first dine in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByTitle_First(
			String title,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the first dine in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByTitle_First(
		String title,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the last dine in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine
	 * @throws NoSuchDineException if a matching dine could not be found
	 */
	public Dine findByTitle_Last(
			String title,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Returns the last dine in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public Dine fetchByTitle_Last(
		String title,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns the dines before and after the current dine in the ordered set where title LIKE &#63;.
	 *
	 * @param dineId the primary key of the current dine
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dine
	 * @throws NoSuchDineException if a dine with the primary key could not be found
	 */
	public Dine[] findByTitle_PrevAndNext(
			long dineId, String title,
			com.liferay.portal.kernel.util.OrderByComparator<Dine>
				orderByComparator)
		throws NoSuchDineException;

	/**
	 * Removes all the dines where title LIKE &#63; from the database.
	 *
	 * @param title the title
	 */
	public void removeByTitle(String title);

	/**
	 * Returns the number of dines where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the number of matching dines
	 */
	public int countByTitle(String title);

	/**
	 * Caches the dine in the entity cache if it is enabled.
	 *
	 * @param dine the dine
	 */
	public void cacheResult(Dine dine);

	/**
	 * Caches the dines in the entity cache if it is enabled.
	 *
	 * @param dines the dines
	 */
	public void cacheResult(java.util.List<Dine> dines);

	/**
	 * Creates a new dine with the primary key. Does not add the dine to the database.
	 *
	 * @param dineId the primary key for the new dine
	 * @return the new dine
	 */
	public Dine create(long dineId);

	/**
	 * Removes the dine with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine that was removed
	 * @throws NoSuchDineException if a dine with the primary key could not be found
	 */
	public Dine remove(long dineId) throws NoSuchDineException;

	public Dine updateImpl(Dine dine);

	/**
	 * Returns the dine with the primary key or throws a <code>NoSuchDineException</code> if it could not be found.
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine
	 * @throws NoSuchDineException if a dine with the primary key could not be found
	 */
	public Dine findByPrimaryKey(long dineId) throws NoSuchDineException;

	/**
	 * Returns the dine with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine, or <code>null</code> if a dine with the primary key could not be found
	 */
	public Dine fetchByPrimaryKey(long dineId);

	/**
	 * Returns all the dines.
	 *
	 * @return the dines
	 */
	public java.util.List<Dine> findAll();

	/**
	 * Returns a range of all the dines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of dines
	 */
	public java.util.List<Dine> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the dines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of dines
	 */
	public java.util.List<Dine> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator);

	/**
	 * Returns an ordered range of all the dines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of dines
	 */
	public java.util.List<Dine> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Dine>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the dines from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of dines.
	 *
	 * @return the number of dines
	 */
	public int countAll();

}