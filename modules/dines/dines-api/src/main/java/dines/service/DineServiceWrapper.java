/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DineService}.
 *
 * @author Brian Wing Shun Chan
 * @see DineService
 * @generated
 */
public class DineServiceWrapper
	implements DineService, ServiceWrapper<DineService> {

	public DineServiceWrapper(DineService dineService) {
		_dineService = dineService;
	}

	@Override
	public java.util.List<dines.model.Dine> findByTitle(String title) {
		return _dineService.findByTitle(title);
	}

	@Override
	public java.util.List<dines.model.Dine> findByTitle(
		String title, int start, int end) {

		return _dineService.findByTitle(title, start, end);
	}

	@Override
	public java.util.List<dines.model.Dine> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			obc) {

		return _dineService.findByTitle(title, start, end, obc);
	}

	@Override
	public int findByTitleCount(String title) {
		return _dineService.findByTitleCount(title);
	}

	@Override
	public java.util.List<dines.model.Dine> getDines(long groupId) {
		return _dineService.getDines(groupId);
	}

	@Override
	public java.util.List<dines.model.Dine> getDines(
		long groupId, int start, int end) {

		return _dineService.getDines(groupId, start, end);
	}

	@Override
	public java.util.List<dines.model.Dine> getDines(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			obc) {

		return _dineService.getDines(groupId, start, end, obc);
	}

	@Override
	public java.util.List<dines.model.Dine> getDinesByCategory(
		long categoryId) {

		return _dineService.getDinesByCategory(categoryId);
	}

	@Override
	public java.util.List<dines.model.Dine> getDinesByCategory(
		long categoryId, int start, int end) {

		return _dineService.getDinesByCategory(categoryId, start, end);
	}

	@Override
	public java.util.List<dines.model.Dine> getDinesByCategory(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			obc) {

		return _dineService.getDinesByCategory(categoryId, start, end, obc);
	}

	@Override
	public int getDinesByCategoryCount(long categoryId) {
		return _dineService.getDinesByCategoryCount(categoryId);
	}

	@Override
	public int getDinesCount(long groupId) {
		return _dineService.getDinesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _dineService.getOSGiServiceIdentifier();
	}

	@Override
	public DineService getWrappedService() {
		return _dineService;
	}

	@Override
	public void setWrappedService(DineService dineService) {
		_dineService = dineService;
	}

	private DineService _dineService;

}