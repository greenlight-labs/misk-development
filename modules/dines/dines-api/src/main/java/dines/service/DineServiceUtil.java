/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import dines.model.Dine;

import java.util.List;

/**
 * Provides the remote service utility for Dine. This utility wraps
 * <code>dines.service.impl.DineServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see DineService
 * @generated
 */
public class DineServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>dines.service.impl.DineServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static List<Dine> findByTitle(String title) {
		return getService().findByTitle(title);
	}

	public static List<Dine> findByTitle(String title, int start, int end) {
		return getService().findByTitle(title, start, end);
	}

	public static List<Dine> findByTitle(
		String title, int start, int end, OrderByComparator<Dine> obc) {

		return getService().findByTitle(title, start, end, obc);
	}

	public static int findByTitleCount(String title) {
		return getService().findByTitleCount(title);
	}

	public static List<Dine> getDines(long groupId) {
		return getService().getDines(groupId);
	}

	public static List<Dine> getDines(long groupId, int start, int end) {
		return getService().getDines(groupId, start, end);
	}

	public static List<Dine> getDines(
		long groupId, int start, int end, OrderByComparator<Dine> obc) {

		return getService().getDines(groupId, start, end, obc);
	}

	public static List<Dine> getDinesByCategory(long categoryId) {
		return getService().getDinesByCategory(categoryId);
	}

	public static List<Dine> getDinesByCategory(
		long categoryId, int start, int end) {

		return getService().getDinesByCategory(categoryId, start, end);
	}

	public static List<Dine> getDinesByCategory(
		long categoryId, int start, int end, OrderByComparator<Dine> obc) {

		return getService().getDinesByCategory(categoryId, start, end, obc);
	}

	public static int getDinesByCategoryCount(long categoryId) {
		return getService().getDinesByCategoryCount(categoryId);
	}

	public static int getDinesCount(long groupId) {
		return getService().getDinesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static DineService getService() {
		return _service;
	}

	private static volatile DineService _service;

}