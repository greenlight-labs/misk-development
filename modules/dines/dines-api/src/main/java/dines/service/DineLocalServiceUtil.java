/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import dines.model.Dine;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for Dine. This utility wraps
 * <code>dines.service.impl.DineLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DineLocalService
 * @generated
 */
public class DineLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>dines.service.impl.DineLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the dine to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was added
	 */
	public static Dine addDine(Dine dine) {
		return getService().addDine(dine);
	}

	public static Dine addDine(
			long userId, long categoryId,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> imageMap,
			Map<java.util.Locale, String> overlayImageMap,
			Map<java.util.Locale, String> openTimingsMap,
			Map<java.util.Locale, String> deliveryTimingsMap,
			Map<java.util.Locale, String> callMap,
			Map<java.util.Locale, String> websiteMap,
			Map<java.util.Locale, String> buttonLabelMap,
			Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addDine(
			userId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Creates a new dine with the primary key. Does not add the dine to the database.
	 *
	 * @param dineId the primary key for the new dine
	 * @return the new dine
	 */
	public static Dine createDine(long dineId) {
		return getService().createDine(dineId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the dine from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was removed
	 */
	public static Dine deleteDine(Dine dine) {
		return getService().deleteDine(dine);
	}

	/**
	 * Deletes the dine with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine that was removed
	 * @throws PortalException if a dine with the primary key could not be found
	 */
	public static Dine deleteDine(long dineId) throws PortalException {
		return getService().deleteDine(dineId);
	}

	public static Dine deleteDine(
			long dineId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteDine(dineId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Dine fetchDine(long dineId) {
		return getService().fetchDine(dineId);
	}

	/**
	 * Returns the dine matching the UUID and group.
	 *
	 * @param uuid the dine's UUID
	 * @param groupId the primary key of the group
	 * @return the matching dine, or <code>null</code> if a matching dine could not be found
	 */
	public static Dine fetchDineByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchDineByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the dine with the primary key.
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine
	 * @throws PortalException if a dine with the primary key could not be found
	 */
	public static Dine getDine(long dineId) throws PortalException {
		return getService().getDine(dineId);
	}

	/**
	 * Returns the dine matching the UUID and group.
	 *
	 * @param uuid the dine's UUID
	 * @param groupId the primary key of the group
	 * @return the matching dine
	 * @throws PortalException if a matching dine could not be found
	 */
	public static Dine getDineByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getDineByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the dines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of dines
	 */
	public static List<Dine> getDines(int start, int end) {
		return getService().getDines(start, end);
	}

	public static List<Dine> getDines(long groupId) {
		return getService().getDines(groupId);
	}

	public static List<Dine> getDines(long groupId, int start, int end) {
		return getService().getDines(groupId, start, end);
	}

	public static List<Dine> getDines(
		long groupId, int start, int end, OrderByComparator<Dine> obc) {

		return getService().getDines(groupId, start, end, obc);
	}

	/**
	 * Returns all the dines matching the UUID and company.
	 *
	 * @param uuid the UUID of the dines
	 * @param companyId the primary key of the company
	 * @return the matching dines, or an empty list if no matches were found
	 */
	public static List<Dine> getDinesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getDinesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of dines matching the UUID and company.
	 *
	 * @param uuid the UUID of the dines
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching dines, or an empty list if no matches were found
	 */
	public static List<Dine> getDinesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Dine> orderByComparator) {

		return getService().getDinesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of dines.
	 *
	 * @return the number of dines
	 */
	public static int getDinesCount() {
		return getService().getDinesCount();
	}

	public static int getDinesCount(long groupId) {
		return getService().getDinesCount(groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the dine in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was updated
	 */
	public static Dine updateDine(Dine dine) {
		return getService().updateDine(dine);
	}

	public static Dine updateDine(
			long userId, long dineId, long categoryId,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> imageMap,
			Map<java.util.Locale, String> overlayImageMap,
			Map<java.util.Locale, String> openTimingsMap,
			Map<java.util.Locale, String> deliveryTimingsMap,
			Map<java.util.Locale, String> callMap,
			Map<java.util.Locale, String> websiteMap,
			Map<java.util.Locale, String> buttonLabelMap,
			Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateDine(
			userId, dineId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	public static DineLocalService getService() {
		return _service;
	}

	private static volatile DineLocalService _service;

}