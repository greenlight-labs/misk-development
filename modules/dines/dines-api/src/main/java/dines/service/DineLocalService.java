/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import dines.model.Dine;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Dine. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see DineLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface DineLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>dines.service.impl.DineLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the dine local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link DineLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the dine to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Dine addDine(Dine dine);

	public Dine addDine(
			long userId, long categoryId, Map<Locale, String> titleMap,
			Map<Locale, String> imageMap, Map<Locale, String> overlayImageMap,
			Map<Locale, String> openTimingsMap,
			Map<Locale, String> deliveryTimingsMap, Map<Locale, String> callMap,
			Map<Locale, String> websiteMap, Map<Locale, String> buttonLabelMap,
			Map<Locale, String> buttonLinkMap, ServiceContext serviceContext)
		throws PortalException;

	/**
	 * Creates a new dine with the primary key. Does not add the dine to the database.
	 *
	 * @param dineId the primary key for the new dine
	 * @return the new dine
	 */
	@Transactional(enabled = false)
	public Dine createDine(long dineId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the dine from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Dine deleteDine(Dine dine);

	/**
	 * Deletes the dine with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine that was removed
	 * @throws PortalException if a dine with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Dine deleteDine(long dineId) throws PortalException;

	public Dine deleteDine(long dineId, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Dine fetchDine(long dineId);

	/**
	 * Returns the dine matching the UUID and group.
	 *
	 * @param uuid the dine's UUID
	 * @param groupId the primary key of the group
	 * @return the matching dine, or <code>null</code> if a matching dine could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Dine fetchDineByUuidAndGroupId(String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the dine with the primary key.
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine
	 * @throws PortalException if a dine with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Dine getDine(long dineId) throws PortalException;

	/**
	 * Returns the dine matching the UUID and group.
	 *
	 * @param uuid the dine's UUID
	 * @param groupId the primary key of the group
	 * @return the matching dine
	 * @throws PortalException if a matching dine could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Dine getDineByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the dines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of dines
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Dine> getDines(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Dine> getDines(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Dine> getDines(long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Dine> getDines(
		long groupId, int start, int end, OrderByComparator<Dine> obc);

	/**
	 * Returns all the dines matching the UUID and company.
	 *
	 * @param uuid the UUID of the dines
	 * @param companyId the primary key of the company
	 * @return the matching dines, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Dine> getDinesByUuidAndCompanyId(String uuid, long companyId);

	/**
	 * Returns a range of dines matching the UUID and company.
	 *
	 * @param uuid the UUID of the dines
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching dines, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Dine> getDinesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Dine> orderByComparator);

	/**
	 * Returns the number of dines.
	 *
	 * @return the number of dines
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getDinesCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getDinesCount(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Updates the dine in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Dine updateDine(Dine dine);

	public Dine updateDine(
			long userId, long dineId, long categoryId,
			Map<Locale, String> titleMap, Map<Locale, String> imageMap,
			Map<Locale, String> overlayImageMap,
			Map<Locale, String> openTimingsMap,
			Map<Locale, String> deliveryTimingsMap, Map<Locale, String> callMap,
			Map<Locale, String> websiteMap, Map<Locale, String> buttonLabelMap,
			Map<Locale, String> buttonLinkMap, ServiceContext serviceContext)
		throws PortalException, SystemException;

}