/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package dines.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DineLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DineLocalService
 * @generated
 */
public class DineLocalServiceWrapper
	implements DineLocalService, ServiceWrapper<DineLocalService> {

	public DineLocalServiceWrapper(DineLocalService dineLocalService) {
		_dineLocalService = dineLocalService;
	}

	/**
	 * Adds the dine to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was added
	 */
	@Override
	public dines.model.Dine addDine(dines.model.Dine dine) {
		return _dineLocalService.addDine(dine);
	}

	@Override
	public dines.model.Dine addDine(
			long userId, long categoryId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> overlayImageMap,
			java.util.Map<java.util.Locale, String> openTimingsMap,
			java.util.Map<java.util.Locale, String> deliveryTimingsMap,
			java.util.Map<java.util.Locale, String> callMap,
			java.util.Map<java.util.Locale, String> websiteMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dineLocalService.addDine(
			userId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Creates a new dine with the primary key. Does not add the dine to the database.
	 *
	 * @param dineId the primary key for the new dine
	 * @return the new dine
	 */
	@Override
	public dines.model.Dine createDine(long dineId) {
		return _dineLocalService.createDine(dineId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dineLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the dine from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was removed
	 */
	@Override
	public dines.model.Dine deleteDine(dines.model.Dine dine) {
		return _dineLocalService.deleteDine(dine);
	}

	/**
	 * Deletes the dine with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine that was removed
	 * @throws PortalException if a dine with the primary key could not be found
	 */
	@Override
	public dines.model.Dine deleteDine(long dineId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dineLocalService.deleteDine(dineId);
	}

	@Override
	public dines.model.Dine deleteDine(
			long dineId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _dineLocalService.deleteDine(dineId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dineLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _dineLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _dineLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _dineLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _dineLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _dineLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _dineLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public dines.model.Dine fetchDine(long dineId) {
		return _dineLocalService.fetchDine(dineId);
	}

	/**
	 * Returns the dine matching the UUID and group.
	 *
	 * @param uuid the dine's UUID
	 * @param groupId the primary key of the group
	 * @return the matching dine, or <code>null</code> if a matching dine could not be found
	 */
	@Override
	public dines.model.Dine fetchDineByUuidAndGroupId(
		String uuid, long groupId) {

		return _dineLocalService.fetchDineByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _dineLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the dine with the primary key.
	 *
	 * @param dineId the primary key of the dine
	 * @return the dine
	 * @throws PortalException if a dine with the primary key could not be found
	 */
	@Override
	public dines.model.Dine getDine(long dineId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dineLocalService.getDine(dineId);
	}

	/**
	 * Returns the dine matching the UUID and group.
	 *
	 * @param uuid the dine's UUID
	 * @param groupId the primary key of the group
	 * @return the matching dine
	 * @throws PortalException if a matching dine could not be found
	 */
	@Override
	public dines.model.Dine getDineByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dineLocalService.getDineByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the dines.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>dines.model.impl.DineModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @return the range of dines
	 */
	@Override
	public java.util.List<dines.model.Dine> getDines(int start, int end) {
		return _dineLocalService.getDines(start, end);
	}

	@Override
	public java.util.List<dines.model.Dine> getDines(long groupId) {
		return _dineLocalService.getDines(groupId);
	}

	@Override
	public java.util.List<dines.model.Dine> getDines(
		long groupId, int start, int end) {

		return _dineLocalService.getDines(groupId, start, end);
	}

	@Override
	public java.util.List<dines.model.Dine> getDines(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			obc) {

		return _dineLocalService.getDines(groupId, start, end, obc);
	}

	/**
	 * Returns all the dines matching the UUID and company.
	 *
	 * @param uuid the UUID of the dines
	 * @param companyId the primary key of the company
	 * @return the matching dines, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<dines.model.Dine> getDinesByUuidAndCompanyId(
		String uuid, long companyId) {

		return _dineLocalService.getDinesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of dines matching the UUID and company.
	 *
	 * @param uuid the UUID of the dines
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of dines
	 * @param end the upper bound of the range of dines (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching dines, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<dines.model.Dine> getDinesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<dines.model.Dine>
			orderByComparator) {

		return _dineLocalService.getDinesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of dines.
	 *
	 * @return the number of dines
	 */
	@Override
	public int getDinesCount() {
		return _dineLocalService.getDinesCount();
	}

	@Override
	public int getDinesCount(long groupId) {
		return _dineLocalService.getDinesCount(groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _dineLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _dineLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _dineLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _dineLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the dine in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DineLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param dine the dine
	 * @return the dine that was updated
	 */
	@Override
	public dines.model.Dine updateDine(dines.model.Dine dine) {
		return _dineLocalService.updateDine(dine);
	}

	@Override
	public dines.model.Dine updateDine(
			long userId, long dineId, long categoryId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> overlayImageMap,
			java.util.Map<java.util.Locale, String> openTimingsMap,
			java.util.Map<java.util.Locale, String> deliveryTimingsMap,
			java.util.Map<java.util.Locale, String> callMap,
			java.util.Map<java.util.Locale, String> websiteMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _dineLocalService.updateDine(
			userId, dineId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	@Override
	public DineLocalService getWrappedService() {
		return _dineLocalService;
	}

	@Override
	public void setWrappedService(DineLocalService dineLocalService) {
		_dineLocalService = dineLocalService;
	}

	private DineLocalService _dineLocalService;

}