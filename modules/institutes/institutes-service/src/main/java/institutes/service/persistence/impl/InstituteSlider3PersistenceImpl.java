/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import institutes.exception.NoSuchSlider3Exception;

import institutes.model.InstituteSlider3;
import institutes.model.impl.InstituteSlider3Impl;
import institutes.model.impl.InstituteSlider3ModelImpl;

import institutes.service.persistence.InstituteSlider3Persistence;
import institutes.service.persistence.InstituteSlider3Util;
import institutes.service.persistence.impl.constants.InstitutePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the institute slider3 service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = InstituteSlider3Persistence.class)
public class InstituteSlider3PersistenceImpl
	extends BasePersistenceImpl<InstituteSlider3>
	implements InstituteSlider3Persistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>InstituteSlider3Util</code> to access the institute slider3 persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		InstituteSlider3Impl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the institute slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<InstituteSlider3> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (InstituteSlider3 instituteSlider3 : list) {
					if (!uuid.equals(instituteSlider3.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_INSTITUTESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(InstituteSlider3ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<InstituteSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 findByUuid_First(
			String uuid, OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = fetchByUuid_First(
			uuid, orderByComparator);

		if (instituteSlider3 != null) {
			return instituteSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 fetchByUuid_First(
		String uuid, OrderByComparator<InstituteSlider3> orderByComparator) {

		List<InstituteSlider3> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 findByUuid_Last(
			String uuid, OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = fetchByUuid_Last(
			uuid, orderByComparator);

		if (instituteSlider3 != null) {
			return instituteSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 fetchByUuid_Last(
		String uuid, OrderByComparator<InstituteSlider3> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<InstituteSlider3> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		uuid = Objects.toString(uuid, "");

		InstituteSlider3 instituteSlider3 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			InstituteSlider3[] array = new InstituteSlider3Impl[3];

			array[0] = getByUuid_PrevAndNext(
				session, instituteSlider3, uuid, orderByComparator, true);

			array[1] = instituteSlider3;

			array[2] = getByUuid_PrevAndNext(
				session, instituteSlider3, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected InstituteSlider3 getByUuid_PrevAndNext(
		Session session, InstituteSlider3 instituteSlider3, String uuid,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_INSTITUTESLIDER3_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(InstituteSlider3ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						instituteSlider3)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<InstituteSlider3> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the institute slider3s where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (InstituteSlider3 instituteSlider3 :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(instituteSlider3);
		}
	}

	/**
	 * Returns the number of institute slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching institute slider3s
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_INSTITUTESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"instituteSlider3.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(instituteSlider3.uuid IS NULL OR instituteSlider3.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<InstituteSlider3> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (InstituteSlider3 instituteSlider3 : list) {
					if (!uuid.equals(instituteSlider3.getUuid()) ||
						(companyId != instituteSlider3.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_INSTITUTESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(InstituteSlider3ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<InstituteSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (instituteSlider3 != null) {
			return instituteSlider3;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		List<InstituteSlider3> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (instituteSlider3 != null) {
			return instituteSlider3;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<InstituteSlider3> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		uuid = Objects.toString(uuid, "");

		InstituteSlider3 instituteSlider3 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			InstituteSlider3[] array = new InstituteSlider3Impl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, instituteSlider3, uuid, companyId, orderByComparator,
				true);

			array[1] = instituteSlider3;

			array[2] = getByUuid_C_PrevAndNext(
				session, instituteSlider3, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected InstituteSlider3 getByUuid_C_PrevAndNext(
		Session session, InstituteSlider3 instituteSlider3, String uuid,
		long companyId, OrderByComparator<InstituteSlider3> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_INSTITUTESLIDER3_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(InstituteSlider3ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						instituteSlider3)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<InstituteSlider3> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the institute slider3s where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (InstituteSlider3 instituteSlider3 :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(instituteSlider3);
		}
	}

	/**
	 * Returns the number of institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching institute slider3s
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_INSTITUTESLIDER3_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"instituteSlider3.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(instituteSlider3.uuid IS NULL OR instituteSlider3.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"instituteSlider3.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByInstituteId;
	private FinderPath _finderPathWithoutPaginationFindByInstituteId;
	private FinderPath _finderPathCountByInstituteId;

	/**
	 * Returns all the institute slider3s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByInstituteId(long instituteId) {
		return findByInstituteId(
			instituteId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end) {

		return findByInstituteId(instituteId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return findByInstituteId(
			instituteId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByInstituteId;
				finderArgs = new Object[] {instituteId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByInstituteId;
			finderArgs = new Object[] {
				instituteId, start, end, orderByComparator
			};
		}

		List<InstituteSlider3> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (InstituteSlider3 instituteSlider3 : list) {
					if (instituteId != instituteSlider3.getInstituteId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_INSTITUTESLIDER3_WHERE);

			sb.append(_FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(InstituteSlider3ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(instituteId);

				list = (List<InstituteSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 findByInstituteId_First(
			long instituteId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = fetchByInstituteId_First(
			instituteId, orderByComparator);

		if (instituteSlider3 != null) {
			return instituteSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("instituteId=");
		sb.append(instituteId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the first institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 fetchByInstituteId_First(
		long instituteId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		List<InstituteSlider3> list = findByInstituteId(
			instituteId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 findByInstituteId_Last(
			long instituteId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = fetchByInstituteId_Last(
			instituteId, orderByComparator);

		if (instituteSlider3 != null) {
			return instituteSlider3;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("instituteId=");
		sb.append(instituteId);

		sb.append("}");

		throw new NoSuchSlider3Exception(sb.toString());
	}

	/**
	 * Returns the last institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	@Override
	public InstituteSlider3 fetchByInstituteId_Last(
		long instituteId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		int count = countByInstituteId(instituteId);

		if (count == 0) {
			return null;
		}

		List<InstituteSlider3> list = findByInstituteId(
			instituteId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3[] findByInstituteId_PrevAndNext(
			long slideId, long instituteId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			InstituteSlider3[] array = new InstituteSlider3Impl[3];

			array[0] = getByInstituteId_PrevAndNext(
				session, instituteSlider3, instituteId, orderByComparator,
				true);

			array[1] = instituteSlider3;

			array[2] = getByInstituteId_PrevAndNext(
				session, instituteSlider3, instituteId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected InstituteSlider3 getByInstituteId_PrevAndNext(
		Session session, InstituteSlider3 instituteSlider3, long instituteId,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_INSTITUTESLIDER3_WHERE);

		sb.append(_FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(InstituteSlider3ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(instituteId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						instituteSlider3)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<InstituteSlider3> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the institute slider3s where instituteId = &#63; from the database.
	 *
	 * @param instituteId the institute ID
	 */
	@Override
	public void removeByInstituteId(long instituteId) {
		for (InstituteSlider3 instituteSlider3 :
				findByInstituteId(
					instituteId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(instituteSlider3);
		}
	}

	/**
	 * Returns the number of institute slider3s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the number of matching institute slider3s
	 */
	@Override
	public int countByInstituteId(long instituteId) {
		FinderPath finderPath = _finderPathCountByInstituteId;

		Object[] finderArgs = new Object[] {instituteId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_INSTITUTESLIDER3_WHERE);

			sb.append(_FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(instituteId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2 =
		"instituteSlider3.instituteId = ?";

	public InstituteSlider3PersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(InstituteSlider3.class);

		setModelImplClass(InstituteSlider3Impl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the institute slider3 in the entity cache if it is enabled.
	 *
	 * @param instituteSlider3 the institute slider3
	 */
	@Override
	public void cacheResult(InstituteSlider3 instituteSlider3) {
		entityCache.putResult(
			InstituteSlider3Impl.class, instituteSlider3.getPrimaryKey(),
			instituteSlider3);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the institute slider3s in the entity cache if it is enabled.
	 *
	 * @param instituteSlider3s the institute slider3s
	 */
	@Override
	public void cacheResult(List<InstituteSlider3> instituteSlider3s) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (instituteSlider3s.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (InstituteSlider3 instituteSlider3 : instituteSlider3s) {
			if (entityCache.getResult(
					InstituteSlider3Impl.class,
					instituteSlider3.getPrimaryKey()) == null) {

				cacheResult(instituteSlider3);
			}
		}
	}

	/**
	 * Clears the cache for all institute slider3s.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(InstituteSlider3Impl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the institute slider3.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(InstituteSlider3 instituteSlider3) {
		entityCache.removeResult(InstituteSlider3Impl.class, instituteSlider3);
	}

	@Override
	public void clearCache(List<InstituteSlider3> instituteSlider3s) {
		for (InstituteSlider3 instituteSlider3 : instituteSlider3s) {
			entityCache.removeResult(
				InstituteSlider3Impl.class, instituteSlider3);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(InstituteSlider3Impl.class, primaryKey);
		}
	}

	/**
	 * Creates a new institute slider3 with the primary key. Does not add the institute slider3 to the database.
	 *
	 * @param slideId the primary key for the new institute slider3
	 * @return the new institute slider3
	 */
	@Override
	public InstituteSlider3 create(long slideId) {
		InstituteSlider3 instituteSlider3 = new InstituteSlider3Impl();

		instituteSlider3.setNew(true);
		instituteSlider3.setPrimaryKey(slideId);

		String uuid = PortalUUIDUtil.generate();

		instituteSlider3.setUuid(uuid);

		instituteSlider3.setCompanyId(CompanyThreadLocal.getCompanyId());

		return instituteSlider3;
	}

	/**
	 * Removes the institute slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3 that was removed
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3 remove(long slideId) throws NoSuchSlider3Exception {
		return remove((Serializable)slideId);
	}

	/**
	 * Removes the institute slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the institute slider3
	 * @return the institute slider3 that was removed
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3 remove(Serializable primaryKey)
		throws NoSuchSlider3Exception {

		Session session = null;

		try {
			session = openSession();

			InstituteSlider3 instituteSlider3 = (InstituteSlider3)session.get(
				InstituteSlider3Impl.class, primaryKey);

			if (instituteSlider3 == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSlider3Exception(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(instituteSlider3);
		}
		catch (NoSuchSlider3Exception noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected InstituteSlider3 removeImpl(InstituteSlider3 instituteSlider3) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(instituteSlider3)) {
				instituteSlider3 = (InstituteSlider3)session.get(
					InstituteSlider3Impl.class,
					instituteSlider3.getPrimaryKeyObj());
			}

			if (instituteSlider3 != null) {
				session.delete(instituteSlider3);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (instituteSlider3 != null) {
			clearCache(instituteSlider3);
		}

		return instituteSlider3;
	}

	@Override
	public InstituteSlider3 updateImpl(InstituteSlider3 instituteSlider3) {
		boolean isNew = instituteSlider3.isNew();

		if (!(instituteSlider3 instanceof InstituteSlider3ModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(instituteSlider3.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					instituteSlider3);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in instituteSlider3 proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom InstituteSlider3 implementation " +
					instituteSlider3.getClass());
		}

		InstituteSlider3ModelImpl instituteSlider3ModelImpl =
			(InstituteSlider3ModelImpl)instituteSlider3;

		if (Validator.isNull(instituteSlider3.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			instituteSlider3.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (instituteSlider3.getCreateDate() == null)) {
			if (serviceContext == null) {
				instituteSlider3.setCreateDate(date);
			}
			else {
				instituteSlider3.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!instituteSlider3ModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				instituteSlider3.setModifiedDate(date);
			}
			else {
				instituteSlider3.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(instituteSlider3);
			}
			else {
				instituteSlider3 = (InstituteSlider3)session.merge(
					instituteSlider3);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			InstituteSlider3Impl.class, instituteSlider3ModelImpl, false, true);

		if (isNew) {
			instituteSlider3.setNew(false);
		}

		instituteSlider3.resetOriginalValues();

		return instituteSlider3;
	}

	/**
	 * Returns the institute slider3 with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the institute slider3
	 * @return the institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3 findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSlider3Exception {

		InstituteSlider3 instituteSlider3 = fetchByPrimaryKey(primaryKey);

		if (instituteSlider3 == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSlider3Exception(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return instituteSlider3;
	}

	/**
	 * Returns the institute slider3 with the primary key or throws a <code>NoSuchSlider3Exception</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3 findByPrimaryKey(long slideId)
		throws NoSuchSlider3Exception {

		return findByPrimaryKey((Serializable)slideId);
	}

	/**
	 * Returns the institute slider3 with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3, or <code>null</code> if a institute slider3 with the primary key could not be found
	 */
	@Override
	public InstituteSlider3 fetchByPrimaryKey(long slideId) {
		return fetchByPrimaryKey((Serializable)slideId);
	}

	/**
	 * Returns all the institute slider3s.
	 *
	 * @return the institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findAll(
		int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of institute slider3s
	 */
	@Override
	public List<InstituteSlider3> findAll(
		int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<InstituteSlider3> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider3>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_INSTITUTESLIDER3);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_INSTITUTESLIDER3;

				sql = sql.concat(InstituteSlider3ModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<InstituteSlider3>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the institute slider3s from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (InstituteSlider3 instituteSlider3 : findAll()) {
			remove(instituteSlider3);
		}
	}

	/**
	 * Returns the number of institute slider3s.
	 *
	 * @return the number of institute slider3s
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_INSTITUTESLIDER3);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "slideId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_INSTITUTESLIDER3;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return InstituteSlider3ModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the institute slider3 persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class,
			new InstituteSlider3ModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", InstituteSlider3.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByInstituteId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByInstituteId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"instituteId"}, true);

		_finderPathWithoutPaginationFindByInstituteId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByInstituteId",
			new String[] {Long.class.getName()}, new String[] {"instituteId"},
			true);

		_finderPathCountByInstituteId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByInstituteId",
			new String[] {Long.class.getName()}, new String[] {"instituteId"},
			false);

		_setInstituteSlider3UtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setInstituteSlider3UtilPersistence(null);

		entityCache.removeCache(InstituteSlider3Impl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setInstituteSlider3UtilPersistence(
		InstituteSlider3Persistence instituteSlider3Persistence) {

		try {
			Field field = InstituteSlider3Util.class.getDeclaredField(
				"_persistence");

			field.setAccessible(true);

			field.set(null, instituteSlider3Persistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = InstitutePersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = InstitutePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = InstitutePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_INSTITUTESLIDER3 =
		"SELECT instituteSlider3 FROM InstituteSlider3 instituteSlider3";

	private static final String _SQL_SELECT_INSTITUTESLIDER3_WHERE =
		"SELECT instituteSlider3 FROM InstituteSlider3 instituteSlider3 WHERE ";

	private static final String _SQL_COUNT_INSTITUTESLIDER3 =
		"SELECT COUNT(instituteSlider3) FROM InstituteSlider3 instituteSlider3";

	private static final String _SQL_COUNT_INSTITUTESLIDER3_WHERE =
		"SELECT COUNT(instituteSlider3) FROM InstituteSlider3 instituteSlider3 WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "instituteSlider3.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No InstituteSlider3 exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No InstituteSlider3 exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		InstituteSlider3PersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class InstituteSlider3ModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			InstituteSlider3ModelImpl instituteSlider3ModelImpl =
				(InstituteSlider3ModelImpl)baseModel;

			long columnBitmask = instituteSlider3ModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(
					instituteSlider3ModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						instituteSlider3ModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(InstituteSlider3PersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(
					instituteSlider3ModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			InstituteSlider3ModelImpl instituteSlider3ModelImpl,
			String[] columnNames, boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] =
						instituteSlider3ModelImpl.getColumnOriginalValue(
							columnName);
				}
				else {
					arguments[i] = instituteSlider3ModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= InstituteSlider3ModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}