/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import institutes.exception.InstituteSlider3DescriptionException;
import institutes.exception.InstituteSlider3ImageException;
import institutes.exception.InstituteSlider3ThumbnailException;
import institutes.model.InstituteSlider3;
import institutes.service.base.InstituteSlider3LocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the institute slider3 local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>institutes.service.InstituteSlider3LocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider3LocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=institutes.model.InstituteSlider3",
	service = AopService.class
)
public class InstituteSlider3LocalServiceImpl
	extends InstituteSlider3LocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>institutes.service.InstituteSlider3LocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>institutes.service.InstituteSlider3LocalServiceUtil</code>.
	 */

	public InstituteSlider3 addInstituteSlider3(long userId,
												long instituteId,
												Map<Locale, String> section3ImageMap, Map<Locale, String> section3DescriptionMap, Map<Locale, String> section3ThumbnailMap,
												ServiceContext serviceContext) throws PortalException {

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(section3ImageMap, section3DescriptionMap, section3ThumbnailMap);

		long slideId = counterLocalService.increment();

		InstituteSlider3 slide = instituteSlider3Persistence.create(slideId);

		slide.setUuid(serviceContext.getUuid());
		slide.setUserId(userId);
		slide.setCompanyId(user.getCompanyId());
		slide.setUserName(user.getFullName());
		slide.setCreateDate(serviceContext.getCreateDate(now));
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setInstituteId(instituteId);
		slide.setImageMap(section3ImageMap);
		slide.setDescriptionMap(section3DescriptionMap);
		slide.setThumbnailMap(section3ThumbnailMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		instituteSlider3Persistence.update(slide);
		instituteSlider3Persistence.clearCache();

		return slide;
	}

	public InstituteSlider3 updateInstituteSlider3(long userId, long slideId,
												   long instituteId,
												   Map<Locale, String> section3ImageMap, Map<Locale, String> section3DescriptionMap, Map<Locale, String> section3ThumbnailMap,
												   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(section3ImageMap, section3DescriptionMap, section3ThumbnailMap);

		InstituteSlider3 slide = getInstituteSlider3(slideId);

		User user = userLocalService.getUser(userId);

		slide.setUserId(userId);
		slide.setUserName(user.getFullName());
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setInstituteId(instituteId);
		slide.setImageMap(section3ImageMap);
		slide.setDescriptionMap(section3DescriptionMap);
		slide.setThumbnailMap(section3ThumbnailMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		instituteSlider3Persistence.update(slide);
		instituteSlider3Persistence.clearCache();

		return slide;
	}

	public InstituteSlider3 deleteInstituteSlider3(long slideId,
												   ServiceContext serviceContext) throws PortalException,
			SystemException {

		InstituteSlider3 slide = getInstituteSlider3(slideId);
		slide = deleteInstituteSlider3(slide);

		return slide;
	}

	public List<InstituteSlider3> getInstituteSlider3Slides(long instituteId) {

		return instituteSlider3Persistence.findByInstituteId(instituteId);
	}

	public List<InstituteSlider3> getInstituteSlider3Slides(long instituteId, int start, int end,
															OrderByComparator<InstituteSlider3> obc) {

		return instituteSlider3Persistence.findByInstituteId(instituteId, start, end, obc);
	}

	public List<InstituteSlider3> getInstituteSlider3Slides(long instituteId, int start, int end) {

		return instituteSlider3Persistence.findByInstituteId(instituteId, start, end);
	}

	public int getInstituteSlider3SlidesCount(long instituteId) {

		return instituteSlider3Persistence.countByInstituteId(instituteId);
	}

	protected void validate(
			Map<Locale, String> section3ImageMap, Map<Locale, String> section3DescriptionMap, Map<Locale, String> section3ThumbnailMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String section3Image = section3ImageMap.get(locale);

		if (Validator.isNull(section3Image)) {
			throw new InstituteSlider3ImageException();
		}

		String section3Description = section3DescriptionMap.get(locale);

		if (Validator.isNull(section3Description)) {
			throw new InstituteSlider3DescriptionException();
		}

		String section3Thumbnail = section3ThumbnailMap.get(locale);

		if (Validator.isNull(section3Thumbnail)) {
			throw new InstituteSlider3ThumbnailException();
		}

	}
}