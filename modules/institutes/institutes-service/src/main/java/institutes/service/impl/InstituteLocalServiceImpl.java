/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import institutes.exception.InstituteListingDescriptionException;
import institutes.exception.InstituteListingImageException;
import institutes.exception.InstituteListingTitleException;
import institutes.model.Institute;
import institutes.service.base.InstituteLocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the institute local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>institutes.service.InstituteLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=institutes.model.Institute",
	service = AopService.class
)
public class InstituteLocalServiceImpl extends InstituteLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>institutes.service.InstituteLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>institutes.service.InstituteLocalServiceUtil</code>.
	 */

	public Institute addInstitute(long userId,
								  Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap,
								  Map<Locale, String> bannerTitleMap, Map<Locale, String> bannerSubtitleMap, Map<Locale, String> bannerDescriptionMap, Map<Locale, String> bannerDesktopImageMap, Map<Locale, String> bannerMobileImageMap,
								  Map<Locale, String> section2TitleMap, Map<Locale, String> section2SubtitleMap, Map<Locale, String> section2DescriptionMap, Map<Locale, String> section2ButtonLabelMap, Map<Locale, String> section2ButtonLinkMap, Map<Locale, String> section2ImageMap,
                                  Map<Locale, String> section4SectionTitleMap,
								  ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(listingImageMap, listingTitleMap, listingDescriptionMap);

		long instituteId = counterLocalService.increment();

		Institute institute = institutePersistence.create(instituteId);

		institute.setUuid(serviceContext.getUuid());
		institute.setUserId(userId);
		institute.setGroupId(groupId);
		institute.setCompanyId(user.getCompanyId());
		institute.setUserName(user.getFullName());
		institute.setCreateDate(serviceContext.getCreateDate(now));
		institute.setModifiedDate(serviceContext.getModifiedDate(now));

		institute.setListingImageMap(listingImageMap);
		institute.setListingTitleMap(listingTitleMap);
		institute.setListingDescriptionMap(listingDescriptionMap);

		institute.setBannerTitleMap(bannerTitleMap);
		institute.setBannerSubtitleMap(bannerSubtitleMap);
		institute.setBannerDescriptionMap(bannerDescriptionMap);
		institute.setBannerDesktopImageMap(bannerDesktopImageMap);
		institute.setBannerMobileImageMap(bannerMobileImageMap);

		institute.setSection2TitleMap(section2TitleMap);
		institute.setSection2SubtitleMap(section2SubtitleMap);
		institute.setSection2DescriptionMap(section2DescriptionMap);
		institute.setSection2ButtonLabelMap(section2ButtonLabelMap);
		institute.setSection2ButtonLinkMap(section2ButtonLinkMap);
		institute.setSection2ImageMap(section2ImageMap);

		institute.setSection4SectionTitleMap(section4SectionTitleMap);

		institute.setExpandoBridgeAttributes(serviceContext);

		institutePersistence.update(institute);
		institutePersistence.clearCache();

		return institute;
	}

	public Institute updateInstitute(long userId, long instituteId,
									 Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap,
									 Map<Locale, String> bannerTitleMap, Map<Locale, String> bannerSubtitleMap, Map<Locale, String> bannerDescriptionMap, Map<Locale, String> bannerDesktopImageMap, Map<Locale, String> bannerMobileImageMap,
									 Map<Locale, String> section2TitleMap, Map<Locale, String> section2SubtitleMap, Map<Locale, String> section2DescriptionMap, Map<Locale, String> section2ButtonLabelMap, Map<Locale, String> section2ButtonLinkMap, Map<Locale, String> section2ImageMap,
                                     Map<Locale, String> section4SectionTitleMap,
									 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(listingImageMap, listingTitleMap, listingDescriptionMap);

		Institute institute = getInstitute(instituteId);

		User user = userLocalService.getUser(userId);

		institute.setUserId(userId);
		institute.setUserName(user.getFullName());
		institute.setModifiedDate(serviceContext.getModifiedDate(now));

		institute.setListingImageMap(listingImageMap);
		institute.setListingTitleMap(listingTitleMap);
		institute.setListingDescriptionMap(listingDescriptionMap);

		institute.setBannerTitleMap(bannerTitleMap);
		institute.setBannerSubtitleMap(bannerSubtitleMap);
		institute.setBannerDescriptionMap(bannerDescriptionMap);
		institute.setBannerDesktopImageMap(bannerDesktopImageMap);
		institute.setBannerMobileImageMap(bannerMobileImageMap);

		institute.setSection2TitleMap(section2TitleMap);
		institute.setSection2SubtitleMap(section2SubtitleMap);
		institute.setSection2DescriptionMap(section2DescriptionMap);
		institute.setSection2ButtonLabelMap(section2ButtonLabelMap);
		institute.setSection2ButtonLinkMap(section2ButtonLinkMap);
		institute.setSection2ImageMap(section2ImageMap);

		institute.setSection4SectionTitleMap(section4SectionTitleMap);

		institute.setExpandoBridgeAttributes(serviceContext);

		institutePersistence.update(institute);
		institutePersistence.clearCache();

		return institute;
	}

	public Institute deleteInstitute(long instituteId,
									 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Institute institute = getInstitute(instituteId);
		institute = deleteInstitute(institute);

		return institute;
	}

	public List<Institute> getInstitutes(long groupId) {

		return institutePersistence.findByGroupId(groupId);
	}

	public List<Institute> getInstitutes(long groupId, int start, int end,
										 OrderByComparator<Institute> obc) {

		return institutePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Institute> getInstitutes(long groupId, int start, int end) {

		return institutePersistence.findByGroupId(groupId, start, end);
	}

	public int getInstitutesCount(long groupId) {

		return institutePersistence.countByGroupId(groupId);
	}

	protected void validate(
			Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String listingImage = listingImageMap.get(locale);

		if (Validator.isNull(listingImage)) {
			throw new InstituteListingImageException();
		}

		String listingTitle = listingTitleMap.get(locale);

		if (Validator.isNull(listingTitle)) {
			throw new InstituteListingTitleException();
		}

		String listingDescription = listingDescriptionMap.get(locale);

		if (Validator.isNull(listingDescription)) {
			throw new InstituteListingDescriptionException();
		}

	}
}