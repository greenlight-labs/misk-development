/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.persistence.impl;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import institutes.exception.NoSuchSlider4Exception;

import institutes.model.InstituteSlider4;
import institutes.model.impl.InstituteSlider4Impl;
import institutes.model.impl.InstituteSlider4ModelImpl;

import institutes.service.persistence.InstituteSlider4Persistence;
import institutes.service.persistence.InstituteSlider4Util;
import institutes.service.persistence.impl.constants.InstitutePersistenceConstants;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the institute slider4 service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = InstituteSlider4Persistence.class)
public class InstituteSlider4PersistenceImpl
	extends BasePersistenceImpl<InstituteSlider4>
	implements InstituteSlider4Persistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>InstituteSlider4Util</code> to access the institute slider4 persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		InstituteSlider4Impl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the institute slider4s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider4s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @return the range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider4s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider4s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<InstituteSlider4> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider4>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (InstituteSlider4 instituteSlider4 : list) {
					if (!uuid.equals(instituteSlider4.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_INSTITUTESLIDER4_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(InstituteSlider4ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<InstituteSlider4>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first institute slider4 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider4
	 * @throws NoSuchSlider4Exception if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 findByUuid_First(
			String uuid, OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = fetchByUuid_First(
			uuid, orderByComparator);

		if (instituteSlider4 != null) {
			return instituteSlider4;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSlider4Exception(sb.toString());
	}

	/**
	 * Returns the first institute slider4 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 fetchByUuid_First(
		String uuid, OrderByComparator<InstituteSlider4> orderByComparator) {

		List<InstituteSlider4> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last institute slider4 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider4
	 * @throws NoSuchSlider4Exception if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 findByUuid_Last(
			String uuid, OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = fetchByUuid_Last(
			uuid, orderByComparator);

		if (instituteSlider4 != null) {
			return instituteSlider4;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchSlider4Exception(sb.toString());
	}

	/**
	 * Returns the last institute slider4 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 fetchByUuid_Last(
		String uuid, OrderByComparator<InstituteSlider4> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<InstituteSlider4> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the institute slider4s before and after the current institute slider4 in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider4
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider4
	 * @throws NoSuchSlider4Exception if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		uuid = Objects.toString(uuid, "");

		InstituteSlider4 instituteSlider4 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			InstituteSlider4[] array = new InstituteSlider4Impl[3];

			array[0] = getByUuid_PrevAndNext(
				session, instituteSlider4, uuid, orderByComparator, true);

			array[1] = instituteSlider4;

			array[2] = getByUuid_PrevAndNext(
				session, instituteSlider4, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected InstituteSlider4 getByUuid_PrevAndNext(
		Session session, InstituteSlider4 instituteSlider4, String uuid,
		OrderByComparator<InstituteSlider4> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_INSTITUTESLIDER4_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(InstituteSlider4ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						instituteSlider4)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<InstituteSlider4> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the institute slider4s where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (InstituteSlider4 instituteSlider4 :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(instituteSlider4);
		}
	}

	/**
	 * Returns the number of institute slider4s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching institute slider4s
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_INSTITUTESLIDER4_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"instituteSlider4.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(instituteSlider4.uuid IS NULL OR instituteSlider4.uuid = '')";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the institute slider4s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider4s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @return the range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider4s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider4s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<InstituteSlider4> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider4>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (InstituteSlider4 instituteSlider4 : list) {
					if (!uuid.equals(instituteSlider4.getUuid()) ||
						(companyId != instituteSlider4.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_INSTITUTESLIDER4_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(InstituteSlider4ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<InstituteSlider4>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first institute slider4 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider4
	 * @throws NoSuchSlider4Exception if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (instituteSlider4 != null) {
			return instituteSlider4;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSlider4Exception(sb.toString());
	}

	/**
	 * Returns the first institute slider4 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		List<InstituteSlider4> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last institute slider4 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider4
	 * @throws NoSuchSlider4Exception if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (instituteSlider4 != null) {
			return instituteSlider4;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchSlider4Exception(sb.toString());
	}

	/**
	 * Returns the last institute slider4 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<InstituteSlider4> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the institute slider4s before and after the current institute slider4 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider4
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider4
	 * @throws NoSuchSlider4Exception if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		uuid = Objects.toString(uuid, "");

		InstituteSlider4 instituteSlider4 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			InstituteSlider4[] array = new InstituteSlider4Impl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, instituteSlider4, uuid, companyId, orderByComparator,
				true);

			array[1] = instituteSlider4;

			array[2] = getByUuid_C_PrevAndNext(
				session, instituteSlider4, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected InstituteSlider4 getByUuid_C_PrevAndNext(
		Session session, InstituteSlider4 instituteSlider4, String uuid,
		long companyId, OrderByComparator<InstituteSlider4> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_INSTITUTESLIDER4_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(InstituteSlider4ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						instituteSlider4)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<InstituteSlider4> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the institute slider4s where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (InstituteSlider4 instituteSlider4 :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(instituteSlider4);
		}
	}

	/**
	 * Returns the number of institute slider4s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching institute slider4s
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_INSTITUTESLIDER4_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"instituteSlider4.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(instituteSlider4.uuid IS NULL OR instituteSlider4.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"instituteSlider4.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByInstituteId;
	private FinderPath _finderPathWithoutPaginationFindByInstituteId;
	private FinderPath _finderPathCountByInstituteId;

	/**
	 * Returns all the institute slider4s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByInstituteId(long instituteId) {
		return findByInstituteId(
			instituteId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider4s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @return the range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByInstituteId(
		long instituteId, int start, int end) {

		return findByInstituteId(instituteId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider4s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByInstituteId(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		return findByInstituteId(
			instituteId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider4s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findByInstituteId(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByInstituteId;
				finderArgs = new Object[] {instituteId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByInstituteId;
			finderArgs = new Object[] {
				instituteId, start, end, orderByComparator
			};
		}

		List<InstituteSlider4> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider4>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (InstituteSlider4 instituteSlider4 : list) {
					if (instituteId != instituteSlider4.getInstituteId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_INSTITUTESLIDER4_WHERE);

			sb.append(_FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(InstituteSlider4ModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(instituteId);

				list = (List<InstituteSlider4>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first institute slider4 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider4
	 * @throws NoSuchSlider4Exception if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 findByInstituteId_First(
			long instituteId,
			OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = fetchByInstituteId_First(
			instituteId, orderByComparator);

		if (instituteSlider4 != null) {
			return instituteSlider4;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("instituteId=");
		sb.append(instituteId);

		sb.append("}");

		throw new NoSuchSlider4Exception(sb.toString());
	}

	/**
	 * Returns the first institute slider4 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 fetchByInstituteId_First(
		long instituteId,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		List<InstituteSlider4> list = findByInstituteId(
			instituteId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last institute slider4 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider4
	 * @throws NoSuchSlider4Exception if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 findByInstituteId_Last(
			long instituteId,
			OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = fetchByInstituteId_Last(
			instituteId, orderByComparator);

		if (instituteSlider4 != null) {
			return instituteSlider4;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("instituteId=");
		sb.append(instituteId);

		sb.append("}");

		throw new NoSuchSlider4Exception(sb.toString());
	}

	/**
	 * Returns the last institute slider4 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	@Override
	public InstituteSlider4 fetchByInstituteId_Last(
		long instituteId,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		int count = countByInstituteId(instituteId);

		if (count == 0) {
			return null;
		}

		List<InstituteSlider4> list = findByInstituteId(
			instituteId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the institute slider4s before and after the current institute slider4 in the ordered set where instituteId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider4
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider4
	 * @throws NoSuchSlider4Exception if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4[] findByInstituteId_PrevAndNext(
			long slideId, long instituteId,
			OrderByComparator<InstituteSlider4> orderByComparator)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = findByPrimaryKey(slideId);

		Session session = null;

		try {
			session = openSession();

			InstituteSlider4[] array = new InstituteSlider4Impl[3];

			array[0] = getByInstituteId_PrevAndNext(
				session, instituteSlider4, instituteId, orderByComparator,
				true);

			array[1] = instituteSlider4;

			array[2] = getByInstituteId_PrevAndNext(
				session, instituteSlider4, instituteId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected InstituteSlider4 getByInstituteId_PrevAndNext(
		Session session, InstituteSlider4 instituteSlider4, long instituteId,
		OrderByComparator<InstituteSlider4> orderByComparator,
		boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_INSTITUTESLIDER4_WHERE);

		sb.append(_FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(InstituteSlider4ModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(instituteId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						instituteSlider4)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<InstituteSlider4> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the institute slider4s where instituteId = &#63; from the database.
	 *
	 * @param instituteId the institute ID
	 */
	@Override
	public void removeByInstituteId(long instituteId) {
		for (InstituteSlider4 instituteSlider4 :
				findByInstituteId(
					instituteId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(instituteSlider4);
		}
	}

	/**
	 * Returns the number of institute slider4s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the number of matching institute slider4s
	 */
	@Override
	public int countByInstituteId(long instituteId) {
		FinderPath finderPath = _finderPathCountByInstituteId;

		Object[] finderArgs = new Object[] {instituteId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_INSTITUTESLIDER4_WHERE);

			sb.append(_FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(instituteId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_INSTITUTEID_INSTITUTEID_2 =
		"instituteSlider4.instituteId = ?";

	public InstituteSlider4PersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(InstituteSlider4.class);

		setModelImplClass(InstituteSlider4Impl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the institute slider4 in the entity cache if it is enabled.
	 *
	 * @param instituteSlider4 the institute slider4
	 */
	@Override
	public void cacheResult(InstituteSlider4 instituteSlider4) {
		entityCache.putResult(
			InstituteSlider4Impl.class, instituteSlider4.getPrimaryKey(),
			instituteSlider4);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the institute slider4s in the entity cache if it is enabled.
	 *
	 * @param instituteSlider4s the institute slider4s
	 */
	@Override
	public void cacheResult(List<InstituteSlider4> instituteSlider4s) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (instituteSlider4s.size() >
				 _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (InstituteSlider4 instituteSlider4 : instituteSlider4s) {
			if (entityCache.getResult(
					InstituteSlider4Impl.class,
					instituteSlider4.getPrimaryKey()) == null) {

				cacheResult(instituteSlider4);
			}
		}
	}

	/**
	 * Clears the cache for all institute slider4s.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(InstituteSlider4Impl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the institute slider4.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(InstituteSlider4 instituteSlider4) {
		entityCache.removeResult(InstituteSlider4Impl.class, instituteSlider4);
	}

	@Override
	public void clearCache(List<InstituteSlider4> instituteSlider4s) {
		for (InstituteSlider4 instituteSlider4 : instituteSlider4s) {
			entityCache.removeResult(
				InstituteSlider4Impl.class, instituteSlider4);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(InstituteSlider4Impl.class, primaryKey);
		}
	}

	/**
	 * Creates a new institute slider4 with the primary key. Does not add the institute slider4 to the database.
	 *
	 * @param slideId the primary key for the new institute slider4
	 * @return the new institute slider4
	 */
	@Override
	public InstituteSlider4 create(long slideId) {
		InstituteSlider4 instituteSlider4 = new InstituteSlider4Impl();

		instituteSlider4.setNew(true);
		instituteSlider4.setPrimaryKey(slideId);

		String uuid = PortalUUIDUtil.generate();

		instituteSlider4.setUuid(uuid);

		instituteSlider4.setCompanyId(CompanyThreadLocal.getCompanyId());

		return instituteSlider4;
	}

	/**
	 * Removes the institute slider4 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the institute slider4
	 * @return the institute slider4 that was removed
	 * @throws NoSuchSlider4Exception if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4 remove(long slideId) throws NoSuchSlider4Exception {
		return remove((Serializable)slideId);
	}

	/**
	 * Removes the institute slider4 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the institute slider4
	 * @return the institute slider4 that was removed
	 * @throws NoSuchSlider4Exception if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4 remove(Serializable primaryKey)
		throws NoSuchSlider4Exception {

		Session session = null;

		try {
			session = openSession();

			InstituteSlider4 instituteSlider4 = (InstituteSlider4)session.get(
				InstituteSlider4Impl.class, primaryKey);

			if (instituteSlider4 == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSlider4Exception(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(instituteSlider4);
		}
		catch (NoSuchSlider4Exception noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected InstituteSlider4 removeImpl(InstituteSlider4 instituteSlider4) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(instituteSlider4)) {
				instituteSlider4 = (InstituteSlider4)session.get(
					InstituteSlider4Impl.class,
					instituteSlider4.getPrimaryKeyObj());
			}

			if (instituteSlider4 != null) {
				session.delete(instituteSlider4);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (instituteSlider4 != null) {
			clearCache(instituteSlider4);
		}

		return instituteSlider4;
	}

	@Override
	public InstituteSlider4 updateImpl(InstituteSlider4 instituteSlider4) {
		boolean isNew = instituteSlider4.isNew();

		if (!(instituteSlider4 instanceof InstituteSlider4ModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(instituteSlider4.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					instituteSlider4);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in instituteSlider4 proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom InstituteSlider4 implementation " +
					instituteSlider4.getClass());
		}

		InstituteSlider4ModelImpl instituteSlider4ModelImpl =
			(InstituteSlider4ModelImpl)instituteSlider4;

		if (Validator.isNull(instituteSlider4.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			instituteSlider4.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (instituteSlider4.getCreateDate() == null)) {
			if (serviceContext == null) {
				instituteSlider4.setCreateDate(date);
			}
			else {
				instituteSlider4.setCreateDate(
					serviceContext.getCreateDate(date));
			}
		}

		if (!instituteSlider4ModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				instituteSlider4.setModifiedDate(date);
			}
			else {
				instituteSlider4.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(instituteSlider4);
			}
			else {
				instituteSlider4 = (InstituteSlider4)session.merge(
					instituteSlider4);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			InstituteSlider4Impl.class, instituteSlider4ModelImpl, false, true);

		if (isNew) {
			instituteSlider4.setNew(false);
		}

		instituteSlider4.resetOriginalValues();

		return instituteSlider4;
	}

	/**
	 * Returns the institute slider4 with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the institute slider4
	 * @return the institute slider4
	 * @throws NoSuchSlider4Exception if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4 findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSlider4Exception {

		InstituteSlider4 instituteSlider4 = fetchByPrimaryKey(primaryKey);

		if (instituteSlider4 == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSlider4Exception(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return instituteSlider4;
	}

	/**
	 * Returns the institute slider4 with the primary key or throws a <code>NoSuchSlider4Exception</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider4
	 * @return the institute slider4
	 * @throws NoSuchSlider4Exception if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4 findByPrimaryKey(long slideId)
		throws NoSuchSlider4Exception {

		return findByPrimaryKey((Serializable)slideId);
	}

	/**
	 * Returns the institute slider4 with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider4
	 * @return the institute slider4, or <code>null</code> if a institute slider4 with the primary key could not be found
	 */
	@Override
	public InstituteSlider4 fetchByPrimaryKey(long slideId) {
		return fetchByPrimaryKey((Serializable)slideId);
	}

	/**
	 * Returns all the institute slider4s.
	 *
	 * @return the institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the institute slider4s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @return the range of institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the institute slider4s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findAll(
		int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the institute slider4s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of institute slider4s
	 */
	@Override
	public List<InstituteSlider4> findAll(
		int start, int end,
		OrderByComparator<InstituteSlider4> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<InstituteSlider4> list = null;

		if (useFinderCache) {
			list = (List<InstituteSlider4>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_INSTITUTESLIDER4);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_INSTITUTESLIDER4;

				sql = sql.concat(InstituteSlider4ModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<InstituteSlider4>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the institute slider4s from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (InstituteSlider4 instituteSlider4 : findAll()) {
			remove(instituteSlider4);
		}
	}

	/**
	 * Returns the number of institute slider4s.
	 *
	 * @return the number of institute slider4s
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_INSTITUTESLIDER4);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "slideId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_INSTITUTESLIDER4;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return InstituteSlider4ModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the institute slider4 persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class,
			new InstituteSlider4ModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", InstituteSlider4.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByInstituteId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByInstituteId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"instituteId"}, true);

		_finderPathWithoutPaginationFindByInstituteId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByInstituteId",
			new String[] {Long.class.getName()}, new String[] {"instituteId"},
			true);

		_finderPathCountByInstituteId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByInstituteId",
			new String[] {Long.class.getName()}, new String[] {"instituteId"},
			false);

		_setInstituteSlider4UtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setInstituteSlider4UtilPersistence(null);

		entityCache.removeCache(InstituteSlider4Impl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setInstituteSlider4UtilPersistence(
		InstituteSlider4Persistence instituteSlider4Persistence) {

		try {
			Field field = InstituteSlider4Util.class.getDeclaredField(
				"_persistence");

			field.setAccessible(true);

			field.set(null, instituteSlider4Persistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = InstitutePersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = InstitutePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = InstitutePersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_INSTITUTESLIDER4 =
		"SELECT instituteSlider4 FROM InstituteSlider4 instituteSlider4";

	private static final String _SQL_SELECT_INSTITUTESLIDER4_WHERE =
		"SELECT instituteSlider4 FROM InstituteSlider4 instituteSlider4 WHERE ";

	private static final String _SQL_COUNT_INSTITUTESLIDER4 =
		"SELECT COUNT(instituteSlider4) FROM InstituteSlider4 instituteSlider4";

	private static final String _SQL_COUNT_INSTITUTESLIDER4_WHERE =
		"SELECT COUNT(instituteSlider4) FROM InstituteSlider4 instituteSlider4 WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "instituteSlider4.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No InstituteSlider4 exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No InstituteSlider4 exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		InstituteSlider4PersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class InstituteSlider4ModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			InstituteSlider4ModelImpl instituteSlider4ModelImpl =
				(InstituteSlider4ModelImpl)baseModel;

			long columnBitmask = instituteSlider4ModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(
					instituteSlider4ModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						instituteSlider4ModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(InstituteSlider4PersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(
					instituteSlider4ModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			InstituteSlider4ModelImpl instituteSlider4ModelImpl,
			String[] columnNames, boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] =
						instituteSlider4ModelImpl.getColumnOriginalValue(
							columnName);
				}
				else {
					arguments[i] = instituteSlider4ModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= InstituteSlider4ModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}