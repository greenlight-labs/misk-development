/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import institutes.model.Institute;
import institutes.service.base.InstituteServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the institute remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>institutes.service.InstituteService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=institute",
		"json.web.service.context.path=Institute"
	},
	service = AopService.class
)
public class InstituteServiceImpl extends InstituteServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>institutes.service.InstituteServiceUtil</code> to access the institute remote service.
	 */

	public List<Institute> getInstitutes(long groupId) {

		return institutePersistence.findByGroupId(groupId);
	}

	public List<Institute> getInstitutes(long groupId, int start, int end,
										 OrderByComparator<Institute> obc) {

		return institutePersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Institute> getInstitutes(long groupId, int start, int end) {

		return institutePersistence.findByGroupId(groupId, start, end);
	}

	public int getInstitutesCount(long groupId) {

		return institutePersistence.countByGroupId(groupId);
	}
}