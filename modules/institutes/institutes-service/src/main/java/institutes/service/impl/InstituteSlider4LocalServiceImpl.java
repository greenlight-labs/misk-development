/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import institutes.exception.InstituteSlider4DescriptionException;
import institutes.exception.InstituteSlider4ImageException;
import institutes.exception.InstituteSlider4TitleException;
import institutes.model.InstituteSlider4;
import institutes.service.base.InstituteSlider4LocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the institute slider4 local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>institutes.service.InstituteSlider4LocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider4LocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=institutes.model.InstituteSlider4",
	service = AopService.class
)
public class InstituteSlider4LocalServiceImpl
	extends InstituteSlider4LocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>institutes.service.InstituteSlider4LocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>institutes.service.InstituteSlider4LocalServiceUtil</code>.
	 */

	public InstituteSlider4 addInstituteSlider4(long userId,
												long instituteId,
												Map<Locale, String> titleMap, Map<Locale, String> descriptionMap, Map<Locale, String> imageMap,
												Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
												ServiceContext serviceContext) throws PortalException {

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(titleMap, descriptionMap, imageMap);

		long slideId = counterLocalService.increment();

		InstituteSlider4 slide = instituteSlider4Persistence.create(slideId);

		slide.setUuid(serviceContext.getUuid());
		slide.setUserId(userId);
		slide.setCompanyId(user.getCompanyId());
		slide.setUserName(user.getFullName());
		slide.setCreateDate(serviceContext.getCreateDate(now));
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setInstituteId(instituteId);
		slide.setTitleMap(titleMap);
		slide.setDescriptionMap(descriptionMap);
		slide.setImageMap(imageMap);
		slide.setButtonLabelMap(buttonLabelMap);
		slide.setButtonLinkMap(buttonLinkMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		instituteSlider4Persistence.update(slide);
		instituteSlider4Persistence.clearCache();

		return slide;
	}

	public InstituteSlider4 updateInstituteSlider4(long userId, long slideId,
												   long instituteId,
												   Map<Locale, String> titleMap, Map<Locale, String> descriptionMap, Map<Locale, String> imageMap,
												   Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
												   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(imageMap, descriptionMap, titleMap);

		InstituteSlider4 slide = getInstituteSlider4(slideId);

		User user = userLocalService.getUser(userId);

		slide.setUserId(userId);
		slide.setUserName(user.getFullName());
		slide.setModifiedDate(serviceContext.getModifiedDate(now));

		slide.setInstituteId(instituteId);
		slide.setTitleMap(titleMap);
		slide.setDescriptionMap(descriptionMap);
		slide.setImageMap(imageMap);
		slide.setButtonLabelMap(buttonLabelMap);
		slide.setButtonLinkMap(buttonLinkMap);

		slide.setExpandoBridgeAttributes(serviceContext);

		instituteSlider4Persistence.update(slide);
		instituteSlider4Persistence.clearCache();

		return slide;
	}

	public InstituteSlider4 deleteInstituteSlider4(long slideId,
												   ServiceContext serviceContext) throws PortalException,
			SystemException {

		InstituteSlider4 slide = getInstituteSlider4(slideId);
		slide = deleteInstituteSlider4(slide);

		return slide;
	}

	public List<InstituteSlider4> getInstituteSlider4Slides(long instituteId) {

		return instituteSlider4Persistence.findByInstituteId(instituteId);
	}

	public List<InstituteSlider4> getInstituteSlider4Slides(long instituteId, int start, int end,
															OrderByComparator<InstituteSlider4> obc) {

		return instituteSlider4Persistence.findByInstituteId(instituteId, start, end, obc);
	}

	public List<InstituteSlider4> getInstituteSlider4Slides(long instituteId, int start, int end) {

		return instituteSlider4Persistence.findByInstituteId(instituteId, start, end);
	}

	public int getInstituteSlider4SlidesCount(long instituteId) {

		return instituteSlider4Persistence.countByInstituteId(instituteId);
	}

	protected void validate(
			Map<Locale, String> titleMap, Map<Locale, String> descriptionMap, Map<Locale, String> imageMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String title = titleMap.get(locale);

		if (Validator.isNull(title)) {
			throw new InstituteSlider4TitleException();
		}

		String description = descriptionMap.get(locale);

		if (Validator.isNull(description)) {
			throw new InstituteSlider4DescriptionException();
		}

		String image = imageMap.get(locale);

		if (Validator.isNull(image)) {
			throw new InstituteSlider4ImageException();
		}

	}
}