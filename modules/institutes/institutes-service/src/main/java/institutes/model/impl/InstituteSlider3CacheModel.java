/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import institutes.model.InstituteSlider3;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing InstituteSlider3 in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InstituteSlider3CacheModel
	implements CacheModel<InstituteSlider3>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof InstituteSlider3CacheModel)) {
			return false;
		}

		InstituteSlider3CacheModel instituteSlider3CacheModel =
			(InstituteSlider3CacheModel)object;

		if (slideId == instituteSlider3CacheModel.slideId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, slideId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", slideId=");
		sb.append(slideId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", instituteId=");
		sb.append(instituteId);
		sb.append(", image=");
		sb.append(image);
		sb.append(", description=");
		sb.append(description);
		sb.append(", thumbnail=");
		sb.append(thumbnail);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public InstituteSlider3 toEntityModel() {
		InstituteSlider3Impl instituteSlider3Impl = new InstituteSlider3Impl();

		if (uuid == null) {
			instituteSlider3Impl.setUuid("");
		}
		else {
			instituteSlider3Impl.setUuid(uuid);
		}

		instituteSlider3Impl.setSlideId(slideId);
		instituteSlider3Impl.setCompanyId(companyId);
		instituteSlider3Impl.setUserId(userId);

		if (userName == null) {
			instituteSlider3Impl.setUserName("");
		}
		else {
			instituteSlider3Impl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			instituteSlider3Impl.setCreateDate(null);
		}
		else {
			instituteSlider3Impl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			instituteSlider3Impl.setModifiedDate(null);
		}
		else {
			instituteSlider3Impl.setModifiedDate(new Date(modifiedDate));
		}

		instituteSlider3Impl.setInstituteId(instituteId);

		if (image == null) {
			instituteSlider3Impl.setImage("");
		}
		else {
			instituteSlider3Impl.setImage(image);
		}

		if (description == null) {
			instituteSlider3Impl.setDescription("");
		}
		else {
			instituteSlider3Impl.setDescription(description);
		}

		if (thumbnail == null) {
			instituteSlider3Impl.setThumbnail("");
		}
		else {
			instituteSlider3Impl.setThumbnail(thumbnail);
		}

		instituteSlider3Impl.resetOriginalValues();

		return instituteSlider3Impl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		slideId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		instituteId = objectInput.readLong();
		image = objectInput.readUTF();
		description = objectInput.readUTF();
		thumbnail = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(slideId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(instituteId);

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (thumbnail == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(thumbnail);
		}
	}

	public String uuid;
	public long slideId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long instituteId;
	public String image;
	public String description;
	public String thumbnail;

}