/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import institutes.model.Institute;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Institute in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InstituteCacheModel
	implements CacheModel<Institute>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof InstituteCacheModel)) {
			return false;
		}

		InstituteCacheModel instituteCacheModel = (InstituteCacheModel)object;

		if (instituteId == instituteCacheModel.instituteId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, instituteId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", instituteId=");
		sb.append(instituteId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", listingTitle=");
		sb.append(listingTitle);
		sb.append(", listingDescription=");
		sb.append(listingDescription);
		sb.append(", bannerTitle=");
		sb.append(bannerTitle);
		sb.append(", bannerSubtitle=");
		sb.append(bannerSubtitle);
		sb.append(", bannerDescription=");
		sb.append(bannerDescription);
		sb.append(", bannerDesktopImage=");
		sb.append(bannerDesktopImage);
		sb.append(", bannerMobileImage=");
		sb.append(bannerMobileImage);
		sb.append(", section2Title=");
		sb.append(section2Title);
		sb.append(", section2Subtitle=");
		sb.append(section2Subtitle);
		sb.append(", section2Description=");
		sb.append(section2Description);
		sb.append(", section2ButtonLabel=");
		sb.append(section2ButtonLabel);
		sb.append(", section2ButtonLink=");
		sb.append(section2ButtonLink);
		sb.append(", section2Image=");
		sb.append(section2Image);
		sb.append(", section4SectionTitle=");
		sb.append(section4SectionTitle);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Institute toEntityModel() {
		InstituteImpl instituteImpl = new InstituteImpl();

		if (uuid == null) {
			instituteImpl.setUuid("");
		}
		else {
			instituteImpl.setUuid(uuid);
		}

		instituteImpl.setInstituteId(instituteId);
		instituteImpl.setGroupId(groupId);
		instituteImpl.setCompanyId(companyId);
		instituteImpl.setUserId(userId);

		if (userName == null) {
			instituteImpl.setUserName("");
		}
		else {
			instituteImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			instituteImpl.setCreateDate(null);
		}
		else {
			instituteImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			instituteImpl.setModifiedDate(null);
		}
		else {
			instituteImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (listingImage == null) {
			instituteImpl.setListingImage("");
		}
		else {
			instituteImpl.setListingImage(listingImage);
		}

		if (listingTitle == null) {
			instituteImpl.setListingTitle("");
		}
		else {
			instituteImpl.setListingTitle(listingTitle);
		}

		if (listingDescription == null) {
			instituteImpl.setListingDescription("");
		}
		else {
			instituteImpl.setListingDescription(listingDescription);
		}

		if (bannerTitle == null) {
			instituteImpl.setBannerTitle("");
		}
		else {
			instituteImpl.setBannerTitle(bannerTitle);
		}

		if (bannerSubtitle == null) {
			instituteImpl.setBannerSubtitle("");
		}
		else {
			instituteImpl.setBannerSubtitle(bannerSubtitle);
		}

		if (bannerDescription == null) {
			instituteImpl.setBannerDescription("");
		}
		else {
			instituteImpl.setBannerDescription(bannerDescription);
		}

		if (bannerDesktopImage == null) {
			instituteImpl.setBannerDesktopImage("");
		}
		else {
			instituteImpl.setBannerDesktopImage(bannerDesktopImage);
		}

		if (bannerMobileImage == null) {
			instituteImpl.setBannerMobileImage("");
		}
		else {
			instituteImpl.setBannerMobileImage(bannerMobileImage);
		}

		if (section2Title == null) {
			instituteImpl.setSection2Title("");
		}
		else {
			instituteImpl.setSection2Title(section2Title);
		}

		if (section2Subtitle == null) {
			instituteImpl.setSection2Subtitle("");
		}
		else {
			instituteImpl.setSection2Subtitle(section2Subtitle);
		}

		if (section2Description == null) {
			instituteImpl.setSection2Description("");
		}
		else {
			instituteImpl.setSection2Description(section2Description);
		}

		if (section2ButtonLabel == null) {
			instituteImpl.setSection2ButtonLabel("");
		}
		else {
			instituteImpl.setSection2ButtonLabel(section2ButtonLabel);
		}

		if (section2ButtonLink == null) {
			instituteImpl.setSection2ButtonLink("");
		}
		else {
			instituteImpl.setSection2ButtonLink(section2ButtonLink);
		}

		if (section2Image == null) {
			instituteImpl.setSection2Image("");
		}
		else {
			instituteImpl.setSection2Image(section2Image);
		}

		if (section4SectionTitle == null) {
			instituteImpl.setSection4SectionTitle("");
		}
		else {
			instituteImpl.setSection4SectionTitle(section4SectionTitle);
		}

		instituteImpl.resetOriginalValues();

		return instituteImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		instituteId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		listingImage = objectInput.readUTF();
		listingTitle = objectInput.readUTF();
		listingDescription = objectInput.readUTF();
		bannerTitle = objectInput.readUTF();
		bannerSubtitle = objectInput.readUTF();
		bannerDescription = objectInput.readUTF();
		bannerDesktopImage = objectInput.readUTF();
		bannerMobileImage = objectInput.readUTF();
		section2Title = objectInput.readUTF();
		section2Subtitle = objectInput.readUTF();
		section2Description = (String)objectInput.readObject();
		section2ButtonLabel = objectInput.readUTF();
		section2ButtonLink = objectInput.readUTF();
		section2Image = objectInput.readUTF();
		section4SectionTitle = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(instituteId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (listingTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingTitle);
		}

		if (listingDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingDescription);
		}

		if (bannerTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerTitle);
		}

		if (bannerSubtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerSubtitle);
		}

		if (bannerDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerDescription);
		}

		if (bannerDesktopImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerDesktopImage);
		}

		if (bannerMobileImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerMobileImage);
		}

		if (section2Title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Title);
		}

		if (section2Subtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Subtitle);
		}

		if (section2Description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(section2Description);
		}

		if (section2ButtonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2ButtonLabel);
		}

		if (section2ButtonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2ButtonLink);
		}

		if (section2Image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Image);
		}

		if (section4SectionTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section4SectionTitle);
		}
	}

	public String uuid;
	public long instituteId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String listingImage;
	public String listingTitle;
	public String listingDescription;
	public String bannerTitle;
	public String bannerSubtitle;
	public String bannerDescription;
	public String bannerDesktopImage;
	public String bannerMobileImage;
	public String section2Title;
	public String section2Subtitle;
	public String section2Description;
	public String section2ButtonLabel;
	public String section2ButtonLink;
	public String section2Image;
	public String section4SectionTitle;

}