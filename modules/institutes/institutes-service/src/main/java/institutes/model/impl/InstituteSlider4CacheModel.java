/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import institutes.model.InstituteSlider4;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing InstituteSlider4 in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class InstituteSlider4CacheModel
	implements CacheModel<InstituteSlider4>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof InstituteSlider4CacheModel)) {
			return false;
		}

		InstituteSlider4CacheModel instituteSlider4CacheModel =
			(InstituteSlider4CacheModel)object;

		if (slideId == instituteSlider4CacheModel.slideId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, slideId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", slideId=");
		sb.append(slideId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", instituteId=");
		sb.append(instituteId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", image=");
		sb.append(image);
		sb.append(", buttonLabel=");
		sb.append(buttonLabel);
		sb.append(", buttonLink=");
		sb.append(buttonLink);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public InstituteSlider4 toEntityModel() {
		InstituteSlider4Impl instituteSlider4Impl = new InstituteSlider4Impl();

		if (uuid == null) {
			instituteSlider4Impl.setUuid("");
		}
		else {
			instituteSlider4Impl.setUuid(uuid);
		}

		instituteSlider4Impl.setSlideId(slideId);
		instituteSlider4Impl.setCompanyId(companyId);
		instituteSlider4Impl.setUserId(userId);

		if (userName == null) {
			instituteSlider4Impl.setUserName("");
		}
		else {
			instituteSlider4Impl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			instituteSlider4Impl.setCreateDate(null);
		}
		else {
			instituteSlider4Impl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			instituteSlider4Impl.setModifiedDate(null);
		}
		else {
			instituteSlider4Impl.setModifiedDate(new Date(modifiedDate));
		}

		instituteSlider4Impl.setInstituteId(instituteId);

		if (title == null) {
			instituteSlider4Impl.setTitle("");
		}
		else {
			instituteSlider4Impl.setTitle(title);
		}

		if (description == null) {
			instituteSlider4Impl.setDescription("");
		}
		else {
			instituteSlider4Impl.setDescription(description);
		}

		if (image == null) {
			instituteSlider4Impl.setImage("");
		}
		else {
			instituteSlider4Impl.setImage(image);
		}

		if (buttonLabel == null) {
			instituteSlider4Impl.setButtonLabel("");
		}
		else {
			instituteSlider4Impl.setButtonLabel(buttonLabel);
		}

		if (buttonLink == null) {
			instituteSlider4Impl.setButtonLink("");
		}
		else {
			instituteSlider4Impl.setButtonLink(buttonLink);
		}

		instituteSlider4Impl.resetOriginalValues();

		return instituteSlider4Impl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		slideId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		instituteId = objectInput.readLong();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		image = objectInput.readUTF();
		buttonLabel = objectInput.readUTF();
		buttonLink = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(slideId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(instituteId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (buttonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLabel);
		}

		if (buttonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLink);
		}
	}

	public String uuid;
	public long slideId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long instituteId;
	public String title;
	public String description;
	public String image;
	public String buttonLabel;
	public String buttonLink;

}