create table misk_institutes (
	uuid_ VARCHAR(75) null,
	instituteId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	listingImage STRING null,
	listingTitle STRING null,
	listingDescription STRING null,
	bannerTitle STRING null,
	bannerSubtitle STRING null,
	bannerDescription STRING null,
	bannerDesktopImage STRING null,
	bannerMobileImage STRING null,
	section2Title STRING null,
	section2Subtitle STRING null,
	section2Description TEXT null,
	section2ButtonLabel STRING null,
	section2ButtonLink STRING null,
	section2Image STRING null,
	section4SectionTitle STRING null
);

create table misk_institutes_slider3 (
	uuid_ VARCHAR(75) null,
	slideId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	instituteId LONG,
	image STRING null,
	description STRING null,
	thumbnail STRING null
);

create table misk_institutes_slider4 (
	uuid_ VARCHAR(75) null,
	slideId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	instituteId LONG,
	title STRING null,
	description STRING null,
	image STRING null,
	buttonLabel STRING null,
	buttonLink STRING null
);