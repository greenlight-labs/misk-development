create index IX_252DB on misk_institutes (groupId);
create index IX_80A60AE3 on misk_institutes (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_4CE9EE25 on misk_institutes (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_CCEBAE5E on misk_institutes_slider3 (instituteId);
create index IX_F2378496 on misk_institutes_slider3 (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_AE7D8C3D on misk_institutes_slider4 (instituteId);
create index IX_FED07A17 on misk_institutes_slider4 (uuid_[$COLUMN_LENGTH:75$], companyId);