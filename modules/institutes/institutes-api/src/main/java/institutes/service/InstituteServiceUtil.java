/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import institutes.model.Institute;

import java.util.List;

/**
 * Provides the remote service utility for Institute. This utility wraps
 * <code>institutes.service.impl.InstituteServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteService
 * @generated
 */
public class InstituteServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>institutes.service.impl.InstituteServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static List<Institute> getInstitutes(long groupId) {
		return getService().getInstitutes(groupId);
	}

	public static List<Institute> getInstitutes(
		long groupId, int start, int end) {

		return getService().getInstitutes(groupId, start, end);
	}

	public static List<Institute> getInstitutes(
		long groupId, int start, int end, OrderByComparator<Institute> obc) {

		return getService().getInstitutes(groupId, start, end, obc);
	}

	public static int getInstitutesCount(long groupId) {
		return getService().getInstitutesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static InstituteService getService() {
		return _service;
	}

	private static volatile InstituteService _service;

}