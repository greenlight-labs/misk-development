/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import institutes.exception.NoSuchSlider3Exception;

import institutes.model.InstituteSlider3;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the institute slider3 service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider3Util
 * @generated
 */
@ProviderType
public interface InstituteSlider3Persistence
	extends BasePersistence<InstituteSlider3> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link InstituteSlider3Util} to access the institute slider3 persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the institute slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid(String uuid);

	/**
	 * Returns a range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public InstituteSlider3[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Removes all the institute slider3s where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of institute slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching institute slider3s
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public InstituteSlider3[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Removes all the institute slider3s where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching institute slider3s
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the institute slider3s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByInstituteId(long instituteId);

	/**
	 * Returns a range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end);

	/**
	 * Returns an ordered range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	public java.util.List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 findByInstituteId_First(
			long instituteId,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the first institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 fetchByInstituteId_First(
		long instituteId,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns the last institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 findByInstituteId_Last(
			long instituteId,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the last institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public InstituteSlider3 fetchByInstituteId_Last(
		long instituteId,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public InstituteSlider3[] findByInstituteId_PrevAndNext(
			long slideId, long instituteId,
			com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
				orderByComparator)
		throws NoSuchSlider3Exception;

	/**
	 * Removes all the institute slider3s where instituteId = &#63; from the database.
	 *
	 * @param instituteId the institute ID
	 */
	public void removeByInstituteId(long instituteId);

	/**
	 * Returns the number of institute slider3s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the number of matching institute slider3s
	 */
	public int countByInstituteId(long instituteId);

	/**
	 * Caches the institute slider3 in the entity cache if it is enabled.
	 *
	 * @param instituteSlider3 the institute slider3
	 */
	public void cacheResult(InstituteSlider3 instituteSlider3);

	/**
	 * Caches the institute slider3s in the entity cache if it is enabled.
	 *
	 * @param instituteSlider3s the institute slider3s
	 */
	public void cacheResult(java.util.List<InstituteSlider3> instituteSlider3s);

	/**
	 * Creates a new institute slider3 with the primary key. Does not add the institute slider3 to the database.
	 *
	 * @param slideId the primary key for the new institute slider3
	 * @return the new institute slider3
	 */
	public InstituteSlider3 create(long slideId);

	/**
	 * Removes the institute slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3 that was removed
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public InstituteSlider3 remove(long slideId) throws NoSuchSlider3Exception;

	public InstituteSlider3 updateImpl(InstituteSlider3 instituteSlider3);

	/**
	 * Returns the institute slider3 with the primary key or throws a <code>NoSuchSlider3Exception</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public InstituteSlider3 findByPrimaryKey(long slideId)
		throws NoSuchSlider3Exception;

	/**
	 * Returns the institute slider3 with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3, or <code>null</code> if a institute slider3 with the primary key could not be found
	 */
	public InstituteSlider3 fetchByPrimaryKey(long slideId);

	/**
	 * Returns all the institute slider3s.
	 *
	 * @return the institute slider3s
	 */
	public java.util.List<InstituteSlider3> findAll();

	/**
	 * Returns a range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of institute slider3s
	 */
	public java.util.List<InstituteSlider3> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of institute slider3s
	 */
	public java.util.List<InstituteSlider3> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of institute slider3s
	 */
	public java.util.List<InstituteSlider3> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<InstituteSlider3>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the institute slider3s from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of institute slider3s.
	 *
	 * @return the number of institute slider3s
	 */
	public int countAll();

}