/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import institutes.model.InstituteSlider4;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for InstituteSlider4. This utility wraps
 * <code>institutes.service.impl.InstituteSlider4LocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider4LocalService
 * @generated
 */
public class InstituteSlider4LocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>institutes.service.impl.InstituteSlider4LocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the institute slider4 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider4 the institute slider4
	 * @return the institute slider4 that was added
	 */
	public static InstituteSlider4 addInstituteSlider4(
		InstituteSlider4 instituteSlider4) {

		return getService().addInstituteSlider4(instituteSlider4);
	}

	public static InstituteSlider4 addInstituteSlider4(
			long userId, long instituteId,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> descriptionMap,
			Map<java.util.Locale, String> imageMap,
			Map<java.util.Locale, String> buttonLabelMap,
			Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addInstituteSlider4(
			userId, instituteId, titleMap, descriptionMap, imageMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Creates a new institute slider4 with the primary key. Does not add the institute slider4 to the database.
	 *
	 * @param slideId the primary key for the new institute slider4
	 * @return the new institute slider4
	 */
	public static InstituteSlider4 createInstituteSlider4(long slideId) {
		return getService().createInstituteSlider4(slideId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the institute slider4 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider4 the institute slider4
	 * @return the institute slider4 that was removed
	 */
	public static InstituteSlider4 deleteInstituteSlider4(
		InstituteSlider4 instituteSlider4) {

		return getService().deleteInstituteSlider4(instituteSlider4);
	}

	/**
	 * Deletes the institute slider4 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the institute slider4
	 * @return the institute slider4 that was removed
	 * @throws PortalException if a institute slider4 with the primary key could not be found
	 */
	public static InstituteSlider4 deleteInstituteSlider4(long slideId)
		throws PortalException {

		return getService().deleteInstituteSlider4(slideId);
	}

	public static InstituteSlider4 deleteInstituteSlider4(
			long slideId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteInstituteSlider4(slideId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static InstituteSlider4 fetchInstituteSlider4(long slideId) {
		return getService().fetchInstituteSlider4(slideId);
	}

	/**
	 * Returns the institute slider4 with the matching UUID and company.
	 *
	 * @param uuid the institute slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	public static InstituteSlider4 fetchInstituteSlider4ByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchInstituteSlider4ByUuidAndCompanyId(
			uuid, companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the institute slider4 with the primary key.
	 *
	 * @param slideId the primary key of the institute slider4
	 * @return the institute slider4
	 * @throws PortalException if a institute slider4 with the primary key could not be found
	 */
	public static InstituteSlider4 getInstituteSlider4(long slideId)
		throws PortalException {

		return getService().getInstituteSlider4(slideId);
	}

	/**
	 * Returns the institute slider4 with the matching UUID and company.
	 *
	 * @param uuid the institute slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider4
	 * @throws PortalException if a matching institute slider4 could not be found
	 */
	public static InstituteSlider4 getInstituteSlider4ByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getInstituteSlider4ByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of all the institute slider4s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @return the range of institute slider4s
	 */
	public static List<InstituteSlider4> getInstituteSlider4s(
		int start, int end) {

		return getService().getInstituteSlider4s(start, end);
	}

	/**
	 * Returns the number of institute slider4s.
	 *
	 * @return the number of institute slider4s
	 */
	public static int getInstituteSlider4sCount() {
		return getService().getInstituteSlider4sCount();
	}

	public static List<InstituteSlider4> getInstituteSlider4Slides(
		long instituteId) {

		return getService().getInstituteSlider4Slides(instituteId);
	}

	public static List<InstituteSlider4> getInstituteSlider4Slides(
		long instituteId, int start, int end) {

		return getService().getInstituteSlider4Slides(instituteId, start, end);
	}

	public static List<InstituteSlider4> getInstituteSlider4Slides(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider4> obc) {

		return getService().getInstituteSlider4Slides(
			instituteId, start, end, obc);
	}

	public static int getInstituteSlider4SlidesCount(long instituteId) {
		return getService().getInstituteSlider4SlidesCount(instituteId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the institute slider4 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider4 the institute slider4
	 * @return the institute slider4 that was updated
	 */
	public static InstituteSlider4 updateInstituteSlider4(
		InstituteSlider4 instituteSlider4) {

		return getService().updateInstituteSlider4(instituteSlider4);
	}

	public static InstituteSlider4 updateInstituteSlider4(
			long userId, long slideId, long instituteId,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> descriptionMap,
			Map<java.util.Locale, String> imageMap,
			Map<java.util.Locale, String> buttonLabelMap,
			Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateInstituteSlider4(
			userId, slideId, instituteId, titleMap, descriptionMap, imageMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	public static InstituteSlider4LocalService getService() {
		return _service;
	}

	private static volatile InstituteSlider4LocalService _service;

}