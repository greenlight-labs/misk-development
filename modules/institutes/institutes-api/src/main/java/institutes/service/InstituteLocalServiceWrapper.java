/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InstituteLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteLocalService
 * @generated
 */
public class InstituteLocalServiceWrapper
	implements InstituteLocalService, ServiceWrapper<InstituteLocalService> {

	public InstituteLocalServiceWrapper(
		InstituteLocalService instituteLocalService) {

		_instituteLocalService = instituteLocalService;
	}

	/**
	 * Adds the institute to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was added
	 */
	@Override
	public institutes.model.Institute addInstitute(
		institutes.model.Institute institute) {

		return _instituteLocalService.addInstitute(institute);
	}

	@Override
	public institutes.model.Institute addInstitute(
			long userId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerTitleMap,
			java.util.Map<java.util.Locale, String> bannerSubtitleMap,
			java.util.Map<java.util.Locale, String> bannerDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerDesktopImageMap,
			java.util.Map<java.util.Locale, String> bannerMobileImageMap,
			java.util.Map<java.util.Locale, String> section2TitleMap,
			java.util.Map<java.util.Locale, String> section2SubtitleMap,
			java.util.Map<java.util.Locale, String> section2DescriptionMap,
			java.util.Map<java.util.Locale, String> section2ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section2ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section2ImageMap,
			java.util.Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteLocalService.addInstitute(
			userId, listingImageMap, listingTitleMap, listingDescriptionMap,
			bannerTitleMap, bannerSubtitleMap, bannerDescriptionMap,
			bannerDesktopImageMap, bannerMobileImageMap, section2TitleMap,
			section2SubtitleMap, section2DescriptionMap, section2ButtonLabelMap,
			section2ButtonLinkMap, section2ImageMap, section4SectionTitleMap,
			serviceContext);
	}

	/**
	 * Creates a new institute with the primary key. Does not add the institute to the database.
	 *
	 * @param instituteId the primary key for the new institute
	 * @return the new institute
	 */
	@Override
	public institutes.model.Institute createInstitute(long instituteId) {
		return _instituteLocalService.createInstitute(instituteId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the institute from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was removed
	 */
	@Override
	public institutes.model.Institute deleteInstitute(
		institutes.model.Institute institute) {

		return _instituteLocalService.deleteInstitute(institute);
	}

	/**
	 * Deletes the institute with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute that was removed
	 * @throws PortalException if a institute with the primary key could not be found
	 */
	@Override
	public institutes.model.Institute deleteInstitute(long instituteId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteLocalService.deleteInstitute(instituteId);
	}

	@Override
	public institutes.model.Institute deleteInstitute(
			long instituteId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _instituteLocalService.deleteInstitute(
			instituteId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _instituteLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _instituteLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _instituteLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _instituteLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _instituteLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _instituteLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public institutes.model.Institute fetchInstitute(long instituteId) {
		return _instituteLocalService.fetchInstitute(instituteId);
	}

	/**
	 * Returns the institute matching the UUID and group.
	 *
	 * @param uuid the institute's UUID
	 * @param groupId the primary key of the group
	 * @return the matching institute, or <code>null</code> if a matching institute could not be found
	 */
	@Override
	public institutes.model.Institute fetchInstituteByUuidAndGroupId(
		String uuid, long groupId) {

		return _instituteLocalService.fetchInstituteByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _instituteLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _instituteLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _instituteLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the institute with the primary key.
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute
	 * @throws PortalException if a institute with the primary key could not be found
	 */
	@Override
	public institutes.model.Institute getInstitute(long instituteId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteLocalService.getInstitute(instituteId);
	}

	/**
	 * Returns the institute matching the UUID and group.
	 *
	 * @param uuid the institute's UUID
	 * @param groupId the primary key of the group
	 * @return the matching institute
	 * @throws PortalException if a matching institute could not be found
	 */
	@Override
	public institutes.model.Institute getInstituteByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteLocalService.getInstituteByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the institutes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @return the range of institutes
	 */
	@Override
	public java.util.List<institutes.model.Institute> getInstitutes(
		int start, int end) {

		return _instituteLocalService.getInstitutes(start, end);
	}

	@Override
	public java.util.List<institutes.model.Institute> getInstitutes(
		long groupId) {

		return _instituteLocalService.getInstitutes(groupId);
	}

	@Override
	public java.util.List<institutes.model.Institute> getInstitutes(
		long groupId, int start, int end) {

		return _instituteLocalService.getInstitutes(groupId, start, end);
	}

	@Override
	public java.util.List<institutes.model.Institute> getInstitutes(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<institutes.model.Institute> obc) {

		return _instituteLocalService.getInstitutes(groupId, start, end, obc);
	}

	/**
	 * Returns all the institutes matching the UUID and company.
	 *
	 * @param uuid the UUID of the institutes
	 * @param companyId the primary key of the company
	 * @return the matching institutes, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<institutes.model.Institute>
		getInstitutesByUuidAndCompanyId(String uuid, long companyId) {

		return _instituteLocalService.getInstitutesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of institutes matching the UUID and company.
	 *
	 * @param uuid the UUID of the institutes
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching institutes, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<institutes.model.Institute>
		getInstitutesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<institutes.model.Institute> orderByComparator) {

		return _instituteLocalService.getInstitutesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of institutes.
	 *
	 * @return the number of institutes
	 */
	@Override
	public int getInstitutesCount() {
		return _instituteLocalService.getInstitutesCount();
	}

	@Override
	public int getInstitutesCount(long groupId) {
		return _instituteLocalService.getInstitutesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _instituteLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the institute in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was updated
	 */
	@Override
	public institutes.model.Institute updateInstitute(
		institutes.model.Institute institute) {

		return _instituteLocalService.updateInstitute(institute);
	}

	@Override
	public institutes.model.Institute updateInstitute(
			long userId, long instituteId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerTitleMap,
			java.util.Map<java.util.Locale, String> bannerSubtitleMap,
			java.util.Map<java.util.Locale, String> bannerDescriptionMap,
			java.util.Map<java.util.Locale, String> bannerDesktopImageMap,
			java.util.Map<java.util.Locale, String> bannerMobileImageMap,
			java.util.Map<java.util.Locale, String> section2TitleMap,
			java.util.Map<java.util.Locale, String> section2SubtitleMap,
			java.util.Map<java.util.Locale, String> section2DescriptionMap,
			java.util.Map<java.util.Locale, String> section2ButtonLabelMap,
			java.util.Map<java.util.Locale, String> section2ButtonLinkMap,
			java.util.Map<java.util.Locale, String> section2ImageMap,
			java.util.Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _instituteLocalService.updateInstitute(
			userId, instituteId, listingImageMap, listingTitleMap,
			listingDescriptionMap, bannerTitleMap, bannerSubtitleMap,
			bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
			section2TitleMap, section2SubtitleMap, section2DescriptionMap,
			section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
			section4SectionTitleMap, serviceContext);
	}

	@Override
	public InstituteLocalService getWrappedService() {
		return _instituteLocalService;
	}

	@Override
	public void setWrappedService(InstituteLocalService instituteLocalService) {
		_instituteLocalService = instituteLocalService;
	}

	private InstituteLocalService _instituteLocalService;

}