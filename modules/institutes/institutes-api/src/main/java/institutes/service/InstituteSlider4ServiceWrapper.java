/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InstituteSlider4Service}.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider4Service
 * @generated
 */
public class InstituteSlider4ServiceWrapper
	implements InstituteSlider4Service,
			   ServiceWrapper<InstituteSlider4Service> {

	public InstituteSlider4ServiceWrapper(
		InstituteSlider4Service instituteSlider4Service) {

		_instituteSlider4Service = instituteSlider4Service;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _instituteSlider4Service.getOSGiServiceIdentifier();
	}

	@Override
	public InstituteSlider4Service getWrappedService() {
		return _instituteSlider4Service;
	}

	@Override
	public void setWrappedService(
		InstituteSlider4Service instituteSlider4Service) {

		_instituteSlider4Service = instituteSlider4Service;
	}

	private InstituteSlider4Service _instituteSlider4Service;

}