/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InstituteSlider4LocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider4LocalService
 * @generated
 */
public class InstituteSlider4LocalServiceWrapper
	implements InstituteSlider4LocalService,
			   ServiceWrapper<InstituteSlider4LocalService> {

	public InstituteSlider4LocalServiceWrapper(
		InstituteSlider4LocalService instituteSlider4LocalService) {

		_instituteSlider4LocalService = instituteSlider4LocalService;
	}

	/**
	 * Adds the institute slider4 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider4 the institute slider4
	 * @return the institute slider4 that was added
	 */
	@Override
	public institutes.model.InstituteSlider4 addInstituteSlider4(
		institutes.model.InstituteSlider4 instituteSlider4) {

		return _instituteSlider4LocalService.addInstituteSlider4(
			instituteSlider4);
	}

	@Override
	public institutes.model.InstituteSlider4 addInstituteSlider4(
			long userId, long instituteId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> descriptionMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider4LocalService.addInstituteSlider4(
			userId, instituteId, titleMap, descriptionMap, imageMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Creates a new institute slider4 with the primary key. Does not add the institute slider4 to the database.
	 *
	 * @param slideId the primary key for the new institute slider4
	 * @return the new institute slider4
	 */
	@Override
	public institutes.model.InstituteSlider4 createInstituteSlider4(
		long slideId) {

		return _instituteSlider4LocalService.createInstituteSlider4(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider4LocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the institute slider4 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider4 the institute slider4
	 * @return the institute slider4 that was removed
	 */
	@Override
	public institutes.model.InstituteSlider4 deleteInstituteSlider4(
		institutes.model.InstituteSlider4 instituteSlider4) {

		return _instituteSlider4LocalService.deleteInstituteSlider4(
			instituteSlider4);
	}

	/**
	 * Deletes the institute slider4 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the institute slider4
	 * @return the institute slider4 that was removed
	 * @throws PortalException if a institute slider4 with the primary key could not be found
	 */
	@Override
	public institutes.model.InstituteSlider4 deleteInstituteSlider4(
			long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider4LocalService.deleteInstituteSlider4(slideId);
	}

	@Override
	public institutes.model.InstituteSlider4 deleteInstituteSlider4(
			long slideId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _instituteSlider4LocalService.deleteInstituteSlider4(
			slideId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider4LocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _instituteSlider4LocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _instituteSlider4LocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _instituteSlider4LocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _instituteSlider4LocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _instituteSlider4LocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _instituteSlider4LocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public institutes.model.InstituteSlider4 fetchInstituteSlider4(
		long slideId) {

		return _instituteSlider4LocalService.fetchInstituteSlider4(slideId);
	}

	/**
	 * Returns the institute slider4 with the matching UUID and company.
	 *
	 * @param uuid the institute slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider4, or <code>null</code> if a matching institute slider4 could not be found
	 */
	@Override
	public institutes.model.InstituteSlider4
		fetchInstituteSlider4ByUuidAndCompanyId(String uuid, long companyId) {

		return _instituteSlider4LocalService.
			fetchInstituteSlider4ByUuidAndCompanyId(uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _instituteSlider4LocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _instituteSlider4LocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _instituteSlider4LocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the institute slider4 with the primary key.
	 *
	 * @param slideId the primary key of the institute slider4
	 * @return the institute slider4
	 * @throws PortalException if a institute slider4 with the primary key could not be found
	 */
	@Override
	public institutes.model.InstituteSlider4 getInstituteSlider4(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider4LocalService.getInstituteSlider4(slideId);
	}

	/**
	 * Returns the institute slider4 with the matching UUID and company.
	 *
	 * @param uuid the institute slider4's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider4
	 * @throws PortalException if a matching institute slider4 could not be found
	 */
	@Override
	public institutes.model.InstituteSlider4
			getInstituteSlider4ByUuidAndCompanyId(String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider4LocalService.
			getInstituteSlider4ByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the institute slider4s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider4ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider4s
	 * @param end the upper bound of the range of institute slider4s (not inclusive)
	 * @return the range of institute slider4s
	 */
	@Override
	public java.util.List<institutes.model.InstituteSlider4>
		getInstituteSlider4s(int start, int end) {

		return _instituteSlider4LocalService.getInstituteSlider4s(start, end);
	}

	/**
	 * Returns the number of institute slider4s.
	 *
	 * @return the number of institute slider4s
	 */
	@Override
	public int getInstituteSlider4sCount() {
		return _instituteSlider4LocalService.getInstituteSlider4sCount();
	}

	@Override
	public java.util.List<institutes.model.InstituteSlider4>
		getInstituteSlider4Slides(long instituteId) {

		return _instituteSlider4LocalService.getInstituteSlider4Slides(
			instituteId);
	}

	@Override
	public java.util.List<institutes.model.InstituteSlider4>
		getInstituteSlider4Slides(long instituteId, int start, int end) {

		return _instituteSlider4LocalService.getInstituteSlider4Slides(
			instituteId, start, end);
	}

	@Override
	public java.util.List<institutes.model.InstituteSlider4>
		getInstituteSlider4Slides(
			long instituteId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<institutes.model.InstituteSlider4> obc) {

		return _instituteSlider4LocalService.getInstituteSlider4Slides(
			instituteId, start, end, obc);
	}

	@Override
	public int getInstituteSlider4SlidesCount(long instituteId) {
		return _instituteSlider4LocalService.getInstituteSlider4SlidesCount(
			instituteId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _instituteSlider4LocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider4LocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the institute slider4 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider4LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider4 the institute slider4
	 * @return the institute slider4 that was updated
	 */
	@Override
	public institutes.model.InstituteSlider4 updateInstituteSlider4(
		institutes.model.InstituteSlider4 instituteSlider4) {

		return _instituteSlider4LocalService.updateInstituteSlider4(
			instituteSlider4);
	}

	@Override
	public institutes.model.InstituteSlider4 updateInstituteSlider4(
			long userId, long slideId, long instituteId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> descriptionMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _instituteSlider4LocalService.updateInstituteSlider4(
			userId, slideId, instituteId, titleMap, descriptionMap, imageMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	@Override
	public InstituteSlider4LocalService getWrappedService() {
		return _instituteSlider4LocalService;
	}

	@Override
	public void setWrappedService(
		InstituteSlider4LocalService instituteSlider4LocalService) {

		_instituteSlider4LocalService = instituteSlider4LocalService;
	}

	private InstituteSlider4LocalService _instituteSlider4LocalService;

}