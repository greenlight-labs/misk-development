/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import institutes.model.InstituteSlider3;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the institute slider3 service. This utility wraps <code>institutes.service.persistence.impl.InstituteSlider3PersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider3Persistence
 * @generated
 */
public class InstituteSlider3Util {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(InstituteSlider3 instituteSlider3) {
		getPersistence().clearCache(instituteSlider3);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, InstituteSlider3> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<InstituteSlider3> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<InstituteSlider3> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<InstituteSlider3> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static InstituteSlider3 update(InstituteSlider3 instituteSlider3) {
		return getPersistence().update(instituteSlider3);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static InstituteSlider3 update(
		InstituteSlider3 instituteSlider3, ServiceContext serviceContext) {

		return getPersistence().update(instituteSlider3, serviceContext);
	}

	/**
	 * Returns all the institute slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 findByUuid_First(
			String uuid, OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 fetchByUuid_First(
		String uuid, OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 findByUuid_Last(
			String uuid, OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 fetchByUuid_Last(
		String uuid, OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where uuid = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3[] findByUuid_PrevAndNext(
			long slideId, String uuid,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByUuid_PrevAndNext(
			slideId, uuid, orderByComparator);
	}

	/**
	 * Removes all the institute slider3s where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of institute slider3s where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching institute slider3s
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3[] findByUuid_C_PrevAndNext(
			long slideId, String uuid, long companyId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByUuid_C_PrevAndNext(
			slideId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the institute slider3s where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of institute slider3s where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching institute slider3s
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the institute slider3s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the matching institute slider3s
	 */
	public static List<InstituteSlider3> findByInstituteId(long instituteId) {
		return getPersistence().findByInstituteId(instituteId);
	}

	/**
	 * Returns a range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end) {

		return getPersistence().findByInstituteId(instituteId, start, end);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().findByInstituteId(
			instituteId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the institute slider3s where instituteId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param instituteId the institute ID
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institute slider3s
	 */
	public static List<InstituteSlider3> findByInstituteId(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByInstituteId(
			instituteId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 findByInstituteId_First(
			long instituteId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByInstituteId_First(
			instituteId, orderByComparator);
	}

	/**
	 * Returns the first institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 fetchByInstituteId_First(
		long instituteId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().fetchByInstituteId_First(
			instituteId, orderByComparator);
	}

	/**
	 * Returns the last institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3
	 * @throws NoSuchSlider3Exception if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 findByInstituteId_Last(
			long instituteId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByInstituteId_Last(
			instituteId, orderByComparator);
	}

	/**
	 * Returns the last institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 fetchByInstituteId_Last(
		long instituteId,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().fetchByInstituteId_Last(
			instituteId, orderByComparator);
	}

	/**
	 * Returns the institute slider3s before and after the current institute slider3 in the ordered set where instituteId = &#63;.
	 *
	 * @param slideId the primary key of the current institute slider3
	 * @param instituteId the institute ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3[] findByInstituteId_PrevAndNext(
			long slideId, long instituteId,
			OrderByComparator<InstituteSlider3> orderByComparator)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByInstituteId_PrevAndNext(
			slideId, instituteId, orderByComparator);
	}

	/**
	 * Removes all the institute slider3s where instituteId = &#63; from the database.
	 *
	 * @param instituteId the institute ID
	 */
	public static void removeByInstituteId(long instituteId) {
		getPersistence().removeByInstituteId(instituteId);
	}

	/**
	 * Returns the number of institute slider3s where instituteId = &#63;.
	 *
	 * @param instituteId the institute ID
	 * @return the number of matching institute slider3s
	 */
	public static int countByInstituteId(long instituteId) {
		return getPersistence().countByInstituteId(instituteId);
	}

	/**
	 * Caches the institute slider3 in the entity cache if it is enabled.
	 *
	 * @param instituteSlider3 the institute slider3
	 */
	public static void cacheResult(InstituteSlider3 instituteSlider3) {
		getPersistence().cacheResult(instituteSlider3);
	}

	/**
	 * Caches the institute slider3s in the entity cache if it is enabled.
	 *
	 * @param instituteSlider3s the institute slider3s
	 */
	public static void cacheResult(List<InstituteSlider3> instituteSlider3s) {
		getPersistence().cacheResult(instituteSlider3s);
	}

	/**
	 * Creates a new institute slider3 with the primary key. Does not add the institute slider3 to the database.
	 *
	 * @param slideId the primary key for the new institute slider3
	 * @return the new institute slider3
	 */
	public static InstituteSlider3 create(long slideId) {
		return getPersistence().create(slideId);
	}

	/**
	 * Removes the institute slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3 that was removed
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3 remove(long slideId)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().remove(slideId);
	}

	public static InstituteSlider3 updateImpl(
		InstituteSlider3 instituteSlider3) {

		return getPersistence().updateImpl(instituteSlider3);
	}

	/**
	 * Returns the institute slider3 with the primary key or throws a <code>NoSuchSlider3Exception</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3
	 * @throws NoSuchSlider3Exception if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3 findByPrimaryKey(long slideId)
		throws institutes.exception.NoSuchSlider3Exception {

		return getPersistence().findByPrimaryKey(slideId);
	}

	/**
	 * Returns the institute slider3 with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3, or <code>null</code> if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3 fetchByPrimaryKey(long slideId) {
		return getPersistence().fetchByPrimaryKey(slideId);
	}

	/**
	 * Returns all the institute slider3s.
	 *
	 * @return the institute slider3s
	 */
	public static List<InstituteSlider3> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of institute slider3s
	 */
	public static List<InstituteSlider3> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of institute slider3s
	 */
	public static List<InstituteSlider3> findAll(
		int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of institute slider3s
	 */
	public static List<InstituteSlider3> findAll(
		int start, int end,
		OrderByComparator<InstituteSlider3> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the institute slider3s from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of institute slider3s.
	 *
	 * @return the number of institute slider3s
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static InstituteSlider3Persistence getPersistence() {
		return _persistence;
	}

	private static volatile InstituteSlider3Persistence _persistence;

}