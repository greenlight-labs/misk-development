/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InstituteSlider3Service}.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider3Service
 * @generated
 */
public class InstituteSlider3ServiceWrapper
	implements InstituteSlider3Service,
			   ServiceWrapper<InstituteSlider3Service> {

	public InstituteSlider3ServiceWrapper(
		InstituteSlider3Service instituteSlider3Service) {

		_instituteSlider3Service = instituteSlider3Service;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _instituteSlider3Service.getOSGiServiceIdentifier();
	}

	@Override
	public InstituteSlider3Service getWrappedService() {
		return _instituteSlider3Service;
	}

	@Override
	public void setWrappedService(
		InstituteSlider3Service instituteSlider3Service) {

		_instituteSlider3Service = instituteSlider3Service;
	}

	private InstituteSlider3Service _instituteSlider3Service;

}