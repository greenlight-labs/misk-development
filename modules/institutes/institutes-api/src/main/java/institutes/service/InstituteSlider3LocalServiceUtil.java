/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import institutes.model.InstituteSlider3;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for InstituteSlider3. This utility wraps
 * <code>institutes.service.impl.InstituteSlider3LocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider3LocalService
 * @generated
 */
public class InstituteSlider3LocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>institutes.service.impl.InstituteSlider3LocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the institute slider3 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider3 the institute slider3
	 * @return the institute slider3 that was added
	 */
	public static InstituteSlider3 addInstituteSlider3(
		InstituteSlider3 instituteSlider3) {

		return getService().addInstituteSlider3(instituteSlider3);
	}

	public static InstituteSlider3 addInstituteSlider3(
			long userId, long instituteId,
			Map<java.util.Locale, String> section3ImageMap,
			Map<java.util.Locale, String> section3DescriptionMap,
			Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addInstituteSlider3(
			userId, instituteId, section3ImageMap, section3DescriptionMap,
			section3ThumbnailMap, serviceContext);
	}

	/**
	 * Creates a new institute slider3 with the primary key. Does not add the institute slider3 to the database.
	 *
	 * @param slideId the primary key for the new institute slider3
	 * @return the new institute slider3
	 */
	public static InstituteSlider3 createInstituteSlider3(long slideId) {
		return getService().createInstituteSlider3(slideId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the institute slider3 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider3 the institute slider3
	 * @return the institute slider3 that was removed
	 */
	public static InstituteSlider3 deleteInstituteSlider3(
		InstituteSlider3 instituteSlider3) {

		return getService().deleteInstituteSlider3(instituteSlider3);
	}

	/**
	 * Deletes the institute slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3 that was removed
	 * @throws PortalException if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3 deleteInstituteSlider3(long slideId)
		throws PortalException {

		return getService().deleteInstituteSlider3(slideId);
	}

	public static InstituteSlider3 deleteInstituteSlider3(
			long slideId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteInstituteSlider3(slideId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static InstituteSlider3 fetchInstituteSlider3(long slideId) {
		return getService().fetchInstituteSlider3(slideId);
	}

	/**
	 * Returns the institute slider3 with the matching UUID and company.
	 *
	 * @param uuid the institute slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 fetchInstituteSlider3ByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().fetchInstituteSlider3ByUuidAndCompanyId(
			uuid, companyId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the institute slider3 with the primary key.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3
	 * @throws PortalException if a institute slider3 with the primary key could not be found
	 */
	public static InstituteSlider3 getInstituteSlider3(long slideId)
		throws PortalException {

		return getService().getInstituteSlider3(slideId);
	}

	/**
	 * Returns the institute slider3 with the matching UUID and company.
	 *
	 * @param uuid the institute slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider3
	 * @throws PortalException if a matching institute slider3 could not be found
	 */
	public static InstituteSlider3 getInstituteSlider3ByUuidAndCompanyId(
			String uuid, long companyId)
		throws PortalException {

		return getService().getInstituteSlider3ByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of institute slider3s
	 */
	public static List<InstituteSlider3> getInstituteSlider3s(
		int start, int end) {

		return getService().getInstituteSlider3s(start, end);
	}

	/**
	 * Returns the number of institute slider3s.
	 *
	 * @return the number of institute slider3s
	 */
	public static int getInstituteSlider3sCount() {
		return getService().getInstituteSlider3sCount();
	}

	public static List<InstituteSlider3> getInstituteSlider3Slides(
		long instituteId) {

		return getService().getInstituteSlider3Slides(instituteId);
	}

	public static List<InstituteSlider3> getInstituteSlider3Slides(
		long instituteId, int start, int end) {

		return getService().getInstituteSlider3Slides(instituteId, start, end);
	}

	public static List<InstituteSlider3> getInstituteSlider3Slides(
		long instituteId, int start, int end,
		OrderByComparator<InstituteSlider3> obc) {

		return getService().getInstituteSlider3Slides(
			instituteId, start, end, obc);
	}

	public static int getInstituteSlider3SlidesCount(long instituteId) {
		return getService().getInstituteSlider3SlidesCount(instituteId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the institute slider3 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider3 the institute slider3
	 * @return the institute slider3 that was updated
	 */
	public static InstituteSlider3 updateInstituteSlider3(
		InstituteSlider3 instituteSlider3) {

		return getService().updateInstituteSlider3(instituteSlider3);
	}

	public static InstituteSlider3 updateInstituteSlider3(
			long userId, long slideId, long instituteId,
			Map<java.util.Locale, String> section3ImageMap,
			Map<java.util.Locale, String> section3DescriptionMap,
			Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateInstituteSlider3(
			userId, slideId, instituteId, section3ImageMap,
			section3DescriptionMap, section3ThumbnailMap, serviceContext);
	}

	public static InstituteSlider3LocalService getService() {
		return _service;
	}

	private static volatile InstituteSlider3LocalService _service;

}