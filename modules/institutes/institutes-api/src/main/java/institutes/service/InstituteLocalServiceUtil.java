/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import institutes.model.Institute;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for Institute. This utility wraps
 * <code>institutes.service.impl.InstituteLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteLocalService
 * @generated
 */
public class InstituteLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>institutes.service.impl.InstituteLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the institute to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was added
	 */
	public static Institute addInstitute(Institute institute) {
		return getService().addInstitute(institute);
	}

	public static Institute addInstitute(
			long userId, Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> bannerTitleMap,
			Map<java.util.Locale, String> bannerSubtitleMap,
			Map<java.util.Locale, String> bannerDescriptionMap,
			Map<java.util.Locale, String> bannerDesktopImageMap,
			Map<java.util.Locale, String> bannerMobileImageMap,
			Map<java.util.Locale, String> section2TitleMap,
			Map<java.util.Locale, String> section2SubtitleMap,
			Map<java.util.Locale, String> section2DescriptionMap,
			Map<java.util.Locale, String> section2ButtonLabelMap,
			Map<java.util.Locale, String> section2ButtonLinkMap,
			Map<java.util.Locale, String> section2ImageMap,
			Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addInstitute(
			userId, listingImageMap, listingTitleMap, listingDescriptionMap,
			bannerTitleMap, bannerSubtitleMap, bannerDescriptionMap,
			bannerDesktopImageMap, bannerMobileImageMap, section2TitleMap,
			section2SubtitleMap, section2DescriptionMap, section2ButtonLabelMap,
			section2ButtonLinkMap, section2ImageMap, section4SectionTitleMap,
			serviceContext);
	}

	/**
	 * Creates a new institute with the primary key. Does not add the institute to the database.
	 *
	 * @param instituteId the primary key for the new institute
	 * @return the new institute
	 */
	public static Institute createInstitute(long instituteId) {
		return getService().createInstitute(instituteId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the institute from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was removed
	 */
	public static Institute deleteInstitute(Institute institute) {
		return getService().deleteInstitute(institute);
	}

	/**
	 * Deletes the institute with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute that was removed
	 * @throws PortalException if a institute with the primary key could not be found
	 */
	public static Institute deleteInstitute(long instituteId)
		throws PortalException {

		return getService().deleteInstitute(instituteId);
	}

	public static Institute deleteInstitute(
			long instituteId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteInstitute(instituteId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Institute fetchInstitute(long instituteId) {
		return getService().fetchInstitute(instituteId);
	}

	/**
	 * Returns the institute matching the UUID and group.
	 *
	 * @param uuid the institute's UUID
	 * @param groupId the primary key of the group
	 * @return the matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public static Institute fetchInstituteByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchInstituteByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the institute with the primary key.
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute
	 * @throws PortalException if a institute with the primary key could not be found
	 */
	public static Institute getInstitute(long instituteId)
		throws PortalException {

		return getService().getInstitute(instituteId);
	}

	/**
	 * Returns the institute matching the UUID and group.
	 *
	 * @param uuid the institute's UUID
	 * @param groupId the primary key of the group
	 * @return the matching institute
	 * @throws PortalException if a matching institute could not be found
	 */
	public static Institute getInstituteByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getInstituteByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the institutes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @return the range of institutes
	 */
	public static List<Institute> getInstitutes(int start, int end) {
		return getService().getInstitutes(start, end);
	}

	public static List<Institute> getInstitutes(long groupId) {
		return getService().getInstitutes(groupId);
	}

	public static List<Institute> getInstitutes(
		long groupId, int start, int end) {

		return getService().getInstitutes(groupId, start, end);
	}

	public static List<Institute> getInstitutes(
		long groupId, int start, int end, OrderByComparator<Institute> obc) {

		return getService().getInstitutes(groupId, start, end, obc);
	}

	/**
	 * Returns all the institutes matching the UUID and company.
	 *
	 * @param uuid the UUID of the institutes
	 * @param companyId the primary key of the company
	 * @return the matching institutes, or an empty list if no matches were found
	 */
	public static List<Institute> getInstitutesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getInstitutesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of institutes matching the UUID and company.
	 *
	 * @param uuid the UUID of the institutes
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching institutes, or an empty list if no matches were found
	 */
	public static List<Institute> getInstitutesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Institute> orderByComparator) {

		return getService().getInstitutesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of institutes.
	 *
	 * @return the number of institutes
	 */
	public static int getInstitutesCount() {
		return getService().getInstitutesCount();
	}

	public static int getInstitutesCount(long groupId) {
		return getService().getInstitutesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the institute in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was updated
	 */
	public static Institute updateInstitute(Institute institute) {
		return getService().updateInstitute(institute);
	}

	public static Institute updateInstitute(
			long userId, long instituteId,
			Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> bannerTitleMap,
			Map<java.util.Locale, String> bannerSubtitleMap,
			Map<java.util.Locale, String> bannerDescriptionMap,
			Map<java.util.Locale, String> bannerDesktopImageMap,
			Map<java.util.Locale, String> bannerMobileImageMap,
			Map<java.util.Locale, String> section2TitleMap,
			Map<java.util.Locale, String> section2SubtitleMap,
			Map<java.util.Locale, String> section2DescriptionMap,
			Map<java.util.Locale, String> section2ButtonLabelMap,
			Map<java.util.Locale, String> section2ButtonLinkMap,
			Map<java.util.Locale, String> section2ImageMap,
			Map<java.util.Locale, String> section4SectionTitleMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateInstitute(
			userId, instituteId, listingImageMap, listingTitleMap,
			listingDescriptionMap, bannerTitleMap, bannerSubtitleMap,
			bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
			section2TitleMap, section2SubtitleMap, section2DescriptionMap,
			section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
			section4SectionTitleMap, serviceContext);
	}

	public static InstituteLocalService getService() {
		return _service;
	}

	private static volatile InstituteLocalService _service;

}