/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InstituteService}.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteService
 * @generated
 */
public class InstituteServiceWrapper
	implements InstituteService, ServiceWrapper<InstituteService> {

	public InstituteServiceWrapper(InstituteService instituteService) {
		_instituteService = instituteService;
	}

	@Override
	public java.util.List<institutes.model.Institute> getInstitutes(
		long groupId) {

		return _instituteService.getInstitutes(groupId);
	}

	@Override
	public java.util.List<institutes.model.Institute> getInstitutes(
		long groupId, int start, int end) {

		return _instituteService.getInstitutes(groupId, start, end);
	}

	@Override
	public java.util.List<institutes.model.Institute> getInstitutes(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<institutes.model.Institute> obc) {

		return _instituteService.getInstitutes(groupId, start, end, obc);
	}

	@Override
	public int getInstitutesCount(long groupId) {
		return _instituteService.getInstitutesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _instituteService.getOSGiServiceIdentifier();
	}

	@Override
	public InstituteService getWrappedService() {
		return _instituteService;
	}

	@Override
	public void setWrappedService(InstituteService instituteService) {
		_instituteService = instituteService;
	}

	private InstituteService _instituteService;

}