/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InstituteSlider3LocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider3LocalService
 * @generated
 */
public class InstituteSlider3LocalServiceWrapper
	implements InstituteSlider3LocalService,
			   ServiceWrapper<InstituteSlider3LocalService> {

	public InstituteSlider3LocalServiceWrapper(
		InstituteSlider3LocalService instituteSlider3LocalService) {

		_instituteSlider3LocalService = instituteSlider3LocalService;
	}

	/**
	 * Adds the institute slider3 to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider3 the institute slider3
	 * @return the institute slider3 that was added
	 */
	@Override
	public institutes.model.InstituteSlider3 addInstituteSlider3(
		institutes.model.InstituteSlider3 instituteSlider3) {

		return _instituteSlider3LocalService.addInstituteSlider3(
			instituteSlider3);
	}

	@Override
	public institutes.model.InstituteSlider3 addInstituteSlider3(
			long userId, long instituteId,
			java.util.Map<java.util.Locale, String> section3ImageMap,
			java.util.Map<java.util.Locale, String> section3DescriptionMap,
			java.util.Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider3LocalService.addInstituteSlider3(
			userId, instituteId, section3ImageMap, section3DescriptionMap,
			section3ThumbnailMap, serviceContext);
	}

	/**
	 * Creates a new institute slider3 with the primary key. Does not add the institute slider3 to the database.
	 *
	 * @param slideId the primary key for the new institute slider3
	 * @return the new institute slider3
	 */
	@Override
	public institutes.model.InstituteSlider3 createInstituteSlider3(
		long slideId) {

		return _instituteSlider3LocalService.createInstituteSlider3(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider3LocalService.createPersistedModel(
			primaryKeyObj);
	}

	/**
	 * Deletes the institute slider3 from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider3 the institute slider3
	 * @return the institute slider3 that was removed
	 */
	@Override
	public institutes.model.InstituteSlider3 deleteInstituteSlider3(
		institutes.model.InstituteSlider3 instituteSlider3) {

		return _instituteSlider3LocalService.deleteInstituteSlider3(
			instituteSlider3);
	}

	/**
	 * Deletes the institute slider3 with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3 that was removed
	 * @throws PortalException if a institute slider3 with the primary key could not be found
	 */
	@Override
	public institutes.model.InstituteSlider3 deleteInstituteSlider3(
			long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider3LocalService.deleteInstituteSlider3(slideId);
	}

	@Override
	public institutes.model.InstituteSlider3 deleteInstituteSlider3(
			long slideId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _instituteSlider3LocalService.deleteInstituteSlider3(
			slideId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider3LocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _instituteSlider3LocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _instituteSlider3LocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _instituteSlider3LocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _instituteSlider3LocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _instituteSlider3LocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _instituteSlider3LocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public institutes.model.InstituteSlider3 fetchInstituteSlider3(
		long slideId) {

		return _instituteSlider3LocalService.fetchInstituteSlider3(slideId);
	}

	/**
	 * Returns the institute slider3 with the matching UUID and company.
	 *
	 * @param uuid the institute slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider3, or <code>null</code> if a matching institute slider3 could not be found
	 */
	@Override
	public institutes.model.InstituteSlider3
		fetchInstituteSlider3ByUuidAndCompanyId(String uuid, long companyId) {

		return _instituteSlider3LocalService.
			fetchInstituteSlider3ByUuidAndCompanyId(uuid, companyId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _instituteSlider3LocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _instituteSlider3LocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _instituteSlider3LocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the institute slider3 with the primary key.
	 *
	 * @param slideId the primary key of the institute slider3
	 * @return the institute slider3
	 * @throws PortalException if a institute slider3 with the primary key could not be found
	 */
	@Override
	public institutes.model.InstituteSlider3 getInstituteSlider3(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider3LocalService.getInstituteSlider3(slideId);
	}

	/**
	 * Returns the institute slider3 with the matching UUID and company.
	 *
	 * @param uuid the institute slider3's UUID
	 * @param companyId the primary key of the company
	 * @return the matching institute slider3
	 * @throws PortalException if a matching institute slider3 could not be found
	 */
	@Override
	public institutes.model.InstituteSlider3
			getInstituteSlider3ByUuidAndCompanyId(String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider3LocalService.
			getInstituteSlider3ByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of all the institute slider3s.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteSlider3ModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institute slider3s
	 * @param end the upper bound of the range of institute slider3s (not inclusive)
	 * @return the range of institute slider3s
	 */
	@Override
	public java.util.List<institutes.model.InstituteSlider3>
		getInstituteSlider3s(int start, int end) {

		return _instituteSlider3LocalService.getInstituteSlider3s(start, end);
	}

	/**
	 * Returns the number of institute slider3s.
	 *
	 * @return the number of institute slider3s
	 */
	@Override
	public int getInstituteSlider3sCount() {
		return _instituteSlider3LocalService.getInstituteSlider3sCount();
	}

	@Override
	public java.util.List<institutes.model.InstituteSlider3>
		getInstituteSlider3Slides(long instituteId) {

		return _instituteSlider3LocalService.getInstituteSlider3Slides(
			instituteId);
	}

	@Override
	public java.util.List<institutes.model.InstituteSlider3>
		getInstituteSlider3Slides(long instituteId, int start, int end) {

		return _instituteSlider3LocalService.getInstituteSlider3Slides(
			instituteId, start, end);
	}

	@Override
	public java.util.List<institutes.model.InstituteSlider3>
		getInstituteSlider3Slides(
			long instituteId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<institutes.model.InstituteSlider3> obc) {

		return _instituteSlider3LocalService.getInstituteSlider3Slides(
			instituteId, start, end, obc);
	}

	@Override
	public int getInstituteSlider3SlidesCount(long instituteId) {
		return _instituteSlider3LocalService.getInstituteSlider3SlidesCount(
			instituteId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _instituteSlider3LocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _instituteSlider3LocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the institute slider3 in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteSlider3LocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteSlider3 the institute slider3
	 * @return the institute slider3 that was updated
	 */
	@Override
	public institutes.model.InstituteSlider3 updateInstituteSlider3(
		institutes.model.InstituteSlider3 instituteSlider3) {

		return _instituteSlider3LocalService.updateInstituteSlider3(
			instituteSlider3);
	}

	@Override
	public institutes.model.InstituteSlider3 updateInstituteSlider3(
			long userId, long slideId, long instituteId,
			java.util.Map<java.util.Locale, String> section3ImageMap,
			java.util.Map<java.util.Locale, String> section3DescriptionMap,
			java.util.Map<java.util.Locale, String> section3ThumbnailMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _instituteSlider3LocalService.updateInstituteSlider3(
			userId, slideId, instituteId, section3ImageMap,
			section3DescriptionMap, section3ThumbnailMap, serviceContext);
	}

	@Override
	public InstituteSlider3LocalService getWrappedService() {
		return _instituteSlider3LocalService;
	}

	@Override
	public void setWrappedService(
		InstituteSlider3LocalService instituteSlider3LocalService) {

		_instituteSlider3LocalService = instituteSlider3LocalService;
	}

	private InstituteSlider3LocalService _instituteSlider3LocalService;

}