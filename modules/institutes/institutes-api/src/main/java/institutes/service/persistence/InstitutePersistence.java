/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import institutes.exception.NoSuchInstituteException;

import institutes.model.Institute;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the institute service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteUtil
 * @generated
 */
@ProviderType
public interface InstitutePersistence extends BasePersistence<Institute> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link InstituteUtil} to access the institute persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the institutes where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching institutes
	 */
	public java.util.List<Institute> findByUuid(String uuid);

	/**
	 * Returns a range of all the institutes where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @return the range of matching institutes
	 */
	public java.util.List<Institute> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the institutes where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institutes
	 */
	public java.util.List<Institute> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institutes where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institutes
	 */
	public java.util.List<Institute> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first institute in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute
	 * @throws NoSuchInstituteException if a matching institute could not be found
	 */
	public Institute findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Returns the first institute in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns the last institute in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute
	 * @throws NoSuchInstituteException if a matching institute could not be found
	 */
	public Institute findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Returns the last institute in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns the institutes before and after the current institute in the ordered set where uuid = &#63;.
	 *
	 * @param instituteId the primary key of the current institute
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute
	 * @throws NoSuchInstituteException if a institute with the primary key could not be found
	 */
	public Institute[] findByUuid_PrevAndNext(
			long instituteId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Removes all the institutes where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of institutes where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching institutes
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the institute where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchInstituteException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching institute
	 * @throws NoSuchInstituteException if a matching institute could not be found
	 */
	public Institute findByUUID_G(String uuid, long groupId)
		throws NoSuchInstituteException;

	/**
	 * Returns the institute where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the institute where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the institute where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the institute that was removed
	 */
	public Institute removeByUUID_G(String uuid, long groupId)
		throws NoSuchInstituteException;

	/**
	 * Returns the number of institutes where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching institutes
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the institutes where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching institutes
	 */
	public java.util.List<Institute> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the institutes where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @return the range of matching institutes
	 */
	public java.util.List<Institute> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the institutes where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institutes
	 */
	public java.util.List<Institute> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institutes where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institutes
	 */
	public java.util.List<Institute> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first institute in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute
	 * @throws NoSuchInstituteException if a matching institute could not be found
	 */
	public Institute findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Returns the first institute in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns the last institute in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute
	 * @throws NoSuchInstituteException if a matching institute could not be found
	 */
	public Institute findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Returns the last institute in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns the institutes before and after the current institute in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param instituteId the primary key of the current institute
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute
	 * @throws NoSuchInstituteException if a institute with the primary key could not be found
	 */
	public Institute[] findByUuid_C_PrevAndNext(
			long instituteId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Removes all the institutes where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of institutes where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching institutes
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the institutes where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching institutes
	 */
	public java.util.List<Institute> findByGroupId(long groupId);

	/**
	 * Returns a range of all the institutes where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @return the range of matching institutes
	 */
	public java.util.List<Institute> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the institutes where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching institutes
	 */
	public java.util.List<Institute> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institutes where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching institutes
	 */
	public java.util.List<Institute> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first institute in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute
	 * @throws NoSuchInstituteException if a matching institute could not be found
	 */
	public Institute findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Returns the first institute in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns the last institute in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute
	 * @throws NoSuchInstituteException if a matching institute could not be found
	 */
	public Institute findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Returns the last institute in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching institute, or <code>null</code> if a matching institute could not be found
	 */
	public Institute fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns the institutes before and after the current institute in the ordered set where groupId = &#63;.
	 *
	 * @param instituteId the primary key of the current institute
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next institute
	 * @throws NoSuchInstituteException if a institute with the primary key could not be found
	 */
	public Institute[] findByGroupId_PrevAndNext(
			long instituteId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Institute>
				orderByComparator)
		throws NoSuchInstituteException;

	/**
	 * Removes all the institutes where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of institutes where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching institutes
	 */
	public int countByGroupId(long groupId);

	/**
	 * Caches the institute in the entity cache if it is enabled.
	 *
	 * @param institute the institute
	 */
	public void cacheResult(Institute institute);

	/**
	 * Caches the institutes in the entity cache if it is enabled.
	 *
	 * @param institutes the institutes
	 */
	public void cacheResult(java.util.List<Institute> institutes);

	/**
	 * Creates a new institute with the primary key. Does not add the institute to the database.
	 *
	 * @param instituteId the primary key for the new institute
	 * @return the new institute
	 */
	public Institute create(long instituteId);

	/**
	 * Removes the institute with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute that was removed
	 * @throws NoSuchInstituteException if a institute with the primary key could not be found
	 */
	public Institute remove(long instituteId) throws NoSuchInstituteException;

	public Institute updateImpl(Institute institute);

	/**
	 * Returns the institute with the primary key or throws a <code>NoSuchInstituteException</code> if it could not be found.
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute
	 * @throws NoSuchInstituteException if a institute with the primary key could not be found
	 */
	public Institute findByPrimaryKey(long instituteId)
		throws NoSuchInstituteException;

	/**
	 * Returns the institute with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute, or <code>null</code> if a institute with the primary key could not be found
	 */
	public Institute fetchByPrimaryKey(long instituteId);

	/**
	 * Returns all the institutes.
	 *
	 * @return the institutes
	 */
	public java.util.List<Institute> findAll();

	/**
	 * Returns a range of all the institutes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @return the range of institutes
	 */
	public java.util.List<Institute> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the institutes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of institutes
	 */
	public java.util.List<Institute> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator);

	/**
	 * Returns an ordered range of all the institutes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of institutes
	 */
	public java.util.List<Institute> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Institute>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the institutes from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of institutes.
	 *
	 * @return the number of institutes
	 */
	public int countAll();

}