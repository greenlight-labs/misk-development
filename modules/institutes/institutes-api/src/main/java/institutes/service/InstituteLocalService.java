/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import institutes.model.Institute;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Institute. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface InstituteLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>institutes.service.impl.InstituteLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the institute local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link InstituteLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the institute to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Institute addInstitute(Institute institute);

	public Institute addInstitute(
			long userId, Map<Locale, String> listingImageMap,
			Map<Locale, String> listingTitleMap,
			Map<Locale, String> listingDescriptionMap,
			Map<Locale, String> bannerTitleMap,
			Map<Locale, String> bannerSubtitleMap,
			Map<Locale, String> bannerDescriptionMap,
			Map<Locale, String> bannerDesktopImageMap,
			Map<Locale, String> bannerMobileImageMap,
			Map<Locale, String> section2TitleMap,
			Map<Locale, String> section2SubtitleMap,
			Map<Locale, String> section2DescriptionMap,
			Map<Locale, String> section2ButtonLabelMap,
			Map<Locale, String> section2ButtonLinkMap,
			Map<Locale, String> section2ImageMap,
			Map<Locale, String> section4SectionTitleMap,
			ServiceContext serviceContext)
		throws PortalException;

	/**
	 * Creates a new institute with the primary key. Does not add the institute to the database.
	 *
	 * @param instituteId the primary key for the new institute
	 * @return the new institute
	 */
	@Transactional(enabled = false)
	public Institute createInstitute(long instituteId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the institute from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Institute deleteInstitute(Institute institute);

	/**
	 * Deletes the institute with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute that was removed
	 * @throws PortalException if a institute with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Institute deleteInstitute(long instituteId) throws PortalException;

	public Institute deleteInstitute(
			long instituteId, ServiceContext serviceContext)
		throws PortalException, SystemException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Institute fetchInstitute(long instituteId);

	/**
	 * Returns the institute matching the UUID and group.
	 *
	 * @param uuid the institute's UUID
	 * @param groupId the primary key of the group
	 * @return the matching institute, or <code>null</code> if a matching institute could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Institute fetchInstituteByUuidAndGroupId(String uuid, long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the institute with the primary key.
	 *
	 * @param instituteId the primary key of the institute
	 * @return the institute
	 * @throws PortalException if a institute with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Institute getInstitute(long instituteId) throws PortalException;

	/**
	 * Returns the institute matching the UUID and group.
	 *
	 * @param uuid the institute's UUID
	 * @param groupId the primary key of the group
	 * @return the matching institute
	 * @throws PortalException if a matching institute could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Institute getInstituteByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	/**
	 * Returns a range of all the institutes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>institutes.model.impl.InstituteModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @return the range of institutes
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Institute> getInstitutes(int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Institute> getInstitutes(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Institute> getInstitutes(long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Institute> getInstitutes(
		long groupId, int start, int end, OrderByComparator<Institute> obc);

	/**
	 * Returns all the institutes matching the UUID and company.
	 *
	 * @param uuid the UUID of the institutes
	 * @param companyId the primary key of the company
	 * @return the matching institutes, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Institute> getInstitutesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of institutes matching the UUID and company.
	 *
	 * @param uuid the UUID of the institutes
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of institutes
	 * @param end the upper bound of the range of institutes (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching institutes, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Institute> getInstitutesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Institute> orderByComparator);

	/**
	 * Returns the number of institutes.
	 *
	 * @return the number of institutes
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getInstitutesCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getInstitutesCount(long groupId);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Updates the institute in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect InstituteLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param institute the institute
	 * @return the institute that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Institute updateInstitute(Institute institute);

	public Institute updateInstitute(
			long userId, long instituteId, Map<Locale, String> listingImageMap,
			Map<Locale, String> listingTitleMap,
			Map<Locale, String> listingDescriptionMap,
			Map<Locale, String> bannerTitleMap,
			Map<Locale, String> bannerSubtitleMap,
			Map<Locale, String> bannerDescriptionMap,
			Map<Locale, String> bannerDesktopImageMap,
			Map<Locale, String> bannerMobileImageMap,
			Map<Locale, String> section2TitleMap,
			Map<Locale, String> section2SubtitleMap,
			Map<Locale, String> section2DescriptionMap,
			Map<Locale, String> section2ButtonLabelMap,
			Map<Locale, String> section2ButtonLinkMap,
			Map<Locale, String> section2ImageMap,
			Map<Locale, String> section4SectionTitleMap,
			ServiceContext serviceContext)
		throws PortalException, SystemException;

}