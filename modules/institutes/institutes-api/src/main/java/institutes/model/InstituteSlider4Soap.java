/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link institutes.service.http.InstituteSlider4ServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class InstituteSlider4Soap implements Serializable {

	public static InstituteSlider4Soap toSoapModel(InstituteSlider4 model) {
		InstituteSlider4Soap soapModel = new InstituteSlider4Soap();

		soapModel.setUuid(model.getUuid());
		soapModel.setSlideId(model.getSlideId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setInstituteId(model.getInstituteId());
		soapModel.setTitle(model.getTitle());
		soapModel.setDescription(model.getDescription());
		soapModel.setImage(model.getImage());
		soapModel.setButtonLabel(model.getButtonLabel());
		soapModel.setButtonLink(model.getButtonLink());

		return soapModel;
	}

	public static InstituteSlider4Soap[] toSoapModels(
		InstituteSlider4[] models) {

		InstituteSlider4Soap[] soapModels =
			new InstituteSlider4Soap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static InstituteSlider4Soap[][] toSoapModels(
		InstituteSlider4[][] models) {

		InstituteSlider4Soap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new InstituteSlider4Soap[models.length][models[0].length];
		}
		else {
			soapModels = new InstituteSlider4Soap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static InstituteSlider4Soap[] toSoapModels(
		List<InstituteSlider4> models) {

		List<InstituteSlider4Soap> soapModels =
			new ArrayList<InstituteSlider4Soap>(models.size());

		for (InstituteSlider4 model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new InstituteSlider4Soap[soapModels.size()]);
	}

	public InstituteSlider4Soap() {
	}

	public long getPrimaryKey() {
		return _slideId;
	}

	public void setPrimaryKey(long pk) {
		setSlideId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getSlideId() {
		return _slideId;
	}

	public void setSlideId(long slideId) {
		_slideId = slideId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getInstituteId() {
		return _instituteId;
	}

	public void setInstituteId(long instituteId) {
		_instituteId = instituteId;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getButtonLabel() {
		return _buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		_buttonLabel = buttonLabel;
	}

	public String getButtonLink() {
		return _buttonLink;
	}

	public void setButtonLink(String buttonLink) {
		_buttonLink = buttonLink;
	}

	private String _uuid;
	private long _slideId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _instituteId;
	private String _title;
	private String _description;
	private String _image;
	private String _buttonLabel;
	private String _buttonLink;

}