/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The extended model interface for the InstituteSlider4 service. Represents a row in the &quot;misk_institutes_slider4&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider4Model
 * @generated
 */
@ImplementationClassName("institutes.model.impl.InstituteSlider4Impl")
@ProviderType
public interface InstituteSlider4
	extends InstituteSlider4Model, PersistedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to <code>institutes.model.impl.InstituteSlider4Impl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<InstituteSlider4, Long> SLIDE_ID_ACCESSOR =
		new Accessor<InstituteSlider4, Long>() {

			@Override
			public Long get(InstituteSlider4 instituteSlider4) {
				return instituteSlider4.getSlideId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<InstituteSlider4> getTypeClass() {
				return InstituteSlider4.class;
			}

		};

}