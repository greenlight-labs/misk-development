/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link InstituteSlider3}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider3
 * @generated
 */
public class InstituteSlider3Wrapper
	extends BaseModelWrapper<InstituteSlider3>
	implements InstituteSlider3, ModelWrapper<InstituteSlider3> {

	public InstituteSlider3Wrapper(InstituteSlider3 instituteSlider3) {
		super(instituteSlider3);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("slideId", getSlideId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("instituteId", getInstituteId());
		attributes.put("image", getImage());
		attributes.put("description", getDescription());
		attributes.put("thumbnail", getThumbnail());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long slideId = (Long)attributes.get("slideId");

		if (slideId != null) {
			setSlideId(slideId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long instituteId = (Long)attributes.get("instituteId");

		if (instituteId != null) {
			setInstituteId(instituteId);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String thumbnail = (String)attributes.get("thumbnail");

		if (thumbnail != null) {
			setThumbnail(thumbnail);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the company ID of this institute slider3.
	 *
	 * @return the company ID of this institute slider3
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this institute slider3.
	 *
	 * @return the create date of this institute slider3
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this institute slider3.
	 *
	 * @return the description of this institute slider3
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this institute slider3 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this institute slider3
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this institute slider3 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this institute slider3. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this institute slider3 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this institute slider3
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this institute slider3 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this institute slider3
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this institute slider3.
	 *
	 * @return the locales and localized descriptions of this institute slider3
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the image of this institute slider3.
	 *
	 * @return the image of this institute slider3
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the localized image of this institute slider3 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this institute slider3
	 */
	@Override
	public String getImage(java.util.Locale locale) {
		return model.getImage(locale);
	}

	/**
	 * Returns the localized image of this institute slider3 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this institute slider3. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getImage(java.util.Locale locale, boolean useDefault) {
		return model.getImage(locale, useDefault);
	}

	/**
	 * Returns the localized image of this institute slider3 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this institute slider3
	 */
	@Override
	public String getImage(String languageId) {
		return model.getImage(languageId);
	}

	/**
	 * Returns the localized image of this institute slider3 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this institute slider3
	 */
	@Override
	public String getImage(String languageId, boolean useDefault) {
		return model.getImage(languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return model.getImageCurrentLanguageId();
	}

	@Override
	public String getImageCurrentValue() {
		return model.getImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized images of this institute slider3.
	 *
	 * @return the locales and localized images of this institute slider3
	 */
	@Override
	public Map<java.util.Locale, String> getImageMap() {
		return model.getImageMap();
	}

	/**
	 * Returns the institute ID of this institute slider3.
	 *
	 * @return the institute ID of this institute slider3
	 */
	@Override
	public long getInstituteId() {
		return model.getInstituteId();
	}

	/**
	 * Returns the modified date of this institute slider3.
	 *
	 * @return the modified date of this institute slider3
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this institute slider3.
	 *
	 * @return the primary key of this institute slider3
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the slide ID of this institute slider3.
	 *
	 * @return the slide ID of this institute slider3
	 */
	@Override
	public long getSlideId() {
		return model.getSlideId();
	}

	/**
	 * Returns the thumbnail of this institute slider3.
	 *
	 * @return the thumbnail of this institute slider3
	 */
	@Override
	public String getThumbnail() {
		return model.getThumbnail();
	}

	/**
	 * Returns the localized thumbnail of this institute slider3 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized thumbnail of this institute slider3
	 */
	@Override
	public String getThumbnail(java.util.Locale locale) {
		return model.getThumbnail(locale);
	}

	/**
	 * Returns the localized thumbnail of this institute slider3 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized thumbnail of this institute slider3. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getThumbnail(java.util.Locale locale, boolean useDefault) {
		return model.getThumbnail(locale, useDefault);
	}

	/**
	 * Returns the localized thumbnail of this institute slider3 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized thumbnail of this institute slider3
	 */
	@Override
	public String getThumbnail(String languageId) {
		return model.getThumbnail(languageId);
	}

	/**
	 * Returns the localized thumbnail of this institute slider3 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized thumbnail of this institute slider3
	 */
	@Override
	public String getThumbnail(String languageId, boolean useDefault) {
		return model.getThumbnail(languageId, useDefault);
	}

	@Override
	public String getThumbnailCurrentLanguageId() {
		return model.getThumbnailCurrentLanguageId();
	}

	@Override
	public String getThumbnailCurrentValue() {
		return model.getThumbnailCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized thumbnails of this institute slider3.
	 *
	 * @return the locales and localized thumbnails of this institute slider3
	 */
	@Override
	public Map<java.util.Locale, String> getThumbnailMap() {
		return model.getThumbnailMap();
	}

	/**
	 * Returns the user ID of this institute slider3.
	 *
	 * @return the user ID of this institute slider3
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this institute slider3.
	 *
	 * @return the user name of this institute slider3
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this institute slider3.
	 *
	 * @return the user uuid of this institute slider3
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this institute slider3.
	 *
	 * @return the uuid of this institute slider3
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the company ID of this institute slider3.
	 *
	 * @param companyId the company ID of this institute slider3
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this institute slider3.
	 *
	 * @param createDate the create date of this institute slider3
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this institute slider3.
	 *
	 * @param description the description of this institute slider3
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this institute slider3 in the language.
	 *
	 * @param description the localized description of this institute slider3
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this institute slider3 in the language, and sets the default locale.
	 *
	 * @param description the localized description of this institute slider3
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this institute slider3 from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this institute slider3
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this institute slider3 from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this institute slider3
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the image of this institute slider3.
	 *
	 * @param image the image of this institute slider3
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the localized image of this institute slider3 in the language.
	 *
	 * @param image the localized image of this institute slider3
	 * @param locale the locale of the language
	 */
	@Override
	public void setImage(String image, java.util.Locale locale) {
		model.setImage(image, locale);
	}

	/**
	 * Sets the localized image of this institute slider3 in the language, and sets the default locale.
	 *
	 * @param image the localized image of this institute slider3
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImage(
		String image, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setImage(image, locale, defaultLocale);
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		model.setImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized images of this institute slider3 from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this institute slider3
	 */
	@Override
	public void setImageMap(Map<java.util.Locale, String> imageMap) {
		model.setImageMap(imageMap);
	}

	/**
	 * Sets the localized images of this institute slider3 from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this institute slider3
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImageMap(
		Map<java.util.Locale, String> imageMap,
		java.util.Locale defaultLocale) {

		model.setImageMap(imageMap, defaultLocale);
	}

	/**
	 * Sets the institute ID of this institute slider3.
	 *
	 * @param instituteId the institute ID of this institute slider3
	 */
	@Override
	public void setInstituteId(long instituteId) {
		model.setInstituteId(instituteId);
	}

	/**
	 * Sets the modified date of this institute slider3.
	 *
	 * @param modifiedDate the modified date of this institute slider3
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this institute slider3.
	 *
	 * @param primaryKey the primary key of this institute slider3
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the slide ID of this institute slider3.
	 *
	 * @param slideId the slide ID of this institute slider3
	 */
	@Override
	public void setSlideId(long slideId) {
		model.setSlideId(slideId);
	}

	/**
	 * Sets the thumbnail of this institute slider3.
	 *
	 * @param thumbnail the thumbnail of this institute slider3
	 */
	@Override
	public void setThumbnail(String thumbnail) {
		model.setThumbnail(thumbnail);
	}

	/**
	 * Sets the localized thumbnail of this institute slider3 in the language.
	 *
	 * @param thumbnail the localized thumbnail of this institute slider3
	 * @param locale the locale of the language
	 */
	@Override
	public void setThumbnail(String thumbnail, java.util.Locale locale) {
		model.setThumbnail(thumbnail, locale);
	}

	/**
	 * Sets the localized thumbnail of this institute slider3 in the language, and sets the default locale.
	 *
	 * @param thumbnail the localized thumbnail of this institute slider3
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setThumbnail(
		String thumbnail, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setThumbnail(thumbnail, locale, defaultLocale);
	}

	@Override
	public void setThumbnailCurrentLanguageId(String languageId) {
		model.setThumbnailCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized thumbnails of this institute slider3 from the map of locales and localized thumbnails.
	 *
	 * @param thumbnailMap the locales and localized thumbnails of this institute slider3
	 */
	@Override
	public void setThumbnailMap(Map<java.util.Locale, String> thumbnailMap) {
		model.setThumbnailMap(thumbnailMap);
	}

	/**
	 * Sets the localized thumbnails of this institute slider3 from the map of locales and localized thumbnails, and sets the default locale.
	 *
	 * @param thumbnailMap the locales and localized thumbnails of this institute slider3
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setThumbnailMap(
		Map<java.util.Locale, String> thumbnailMap,
		java.util.Locale defaultLocale) {

		model.setThumbnailMap(thumbnailMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this institute slider3.
	 *
	 * @param userId the user ID of this institute slider3
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this institute slider3.
	 *
	 * @param userName the user name of this institute slider3
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this institute slider3.
	 *
	 * @param userUuid the user uuid of this institute slider3
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this institute slider3.
	 *
	 * @param uuid the uuid of this institute slider3
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected InstituteSlider3Wrapper wrap(InstituteSlider3 instituteSlider3) {
		return new InstituteSlider3Wrapper(instituteSlider3);
	}

}