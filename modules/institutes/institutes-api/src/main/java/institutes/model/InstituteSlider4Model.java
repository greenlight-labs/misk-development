/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.LocaleException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.LocalizedModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.model.StagedAuditedModel;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the InstituteSlider4 service. Represents a row in the &quot;misk_institutes_slider4&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>institutes.model.impl.InstituteSlider4ModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>institutes.model.impl.InstituteSlider4Impl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see InstituteSlider4
 * @generated
 */
@ProviderType
public interface InstituteSlider4Model
	extends BaseModel<InstituteSlider4>, LocalizedModel, ShardedModel,
			StagedAuditedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a institute slider4 model instance should use the {@link InstituteSlider4} interface instead.
	 */

	/**
	 * Returns the primary key of this institute slider4.
	 *
	 * @return the primary key of this institute slider4
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this institute slider4.
	 *
	 * @param primaryKey the primary key of this institute slider4
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this institute slider4.
	 *
	 * @return the uuid of this institute slider4
	 */
	@AutoEscape
	@Override
	public String getUuid();

	/**
	 * Sets the uuid of this institute slider4.
	 *
	 * @param uuid the uuid of this institute slider4
	 */
	@Override
	public void setUuid(String uuid);

	/**
	 * Returns the slide ID of this institute slider4.
	 *
	 * @return the slide ID of this institute slider4
	 */
	public long getSlideId();

	/**
	 * Sets the slide ID of this institute slider4.
	 *
	 * @param slideId the slide ID of this institute slider4
	 */
	public void setSlideId(long slideId);

	/**
	 * Returns the company ID of this institute slider4.
	 *
	 * @return the company ID of this institute slider4
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this institute slider4.
	 *
	 * @param companyId the company ID of this institute slider4
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this institute slider4.
	 *
	 * @return the user ID of this institute slider4
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this institute slider4.
	 *
	 * @param userId the user ID of this institute slider4
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this institute slider4.
	 *
	 * @return the user uuid of this institute slider4
	 */
	@Override
	public String getUserUuid();

	/**
	 * Sets the user uuid of this institute slider4.
	 *
	 * @param userUuid the user uuid of this institute slider4
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this institute slider4.
	 *
	 * @return the user name of this institute slider4
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this institute slider4.
	 *
	 * @param userName the user name of this institute slider4
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this institute slider4.
	 *
	 * @return the create date of this institute slider4
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this institute slider4.
	 *
	 * @param createDate the create date of this institute slider4
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this institute slider4.
	 *
	 * @return the modified date of this institute slider4
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this institute slider4.
	 *
	 * @param modifiedDate the modified date of this institute slider4
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the institute ID of this institute slider4.
	 *
	 * @return the institute ID of this institute slider4
	 */
	public long getInstituteId();

	/**
	 * Sets the institute ID of this institute slider4.
	 *
	 * @param instituteId the institute ID of this institute slider4
	 */
	public void setInstituteId(long instituteId);

	/**
	 * Returns the title of this institute slider4.
	 *
	 * @return the title of this institute slider4
	 */
	public String getTitle();

	/**
	 * Returns the localized title of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this institute slider4
	 */
	@AutoEscape
	public String getTitle(Locale locale);

	/**
	 * Returns the localized title of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this institute slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getTitle(Locale locale, boolean useDefault);

	/**
	 * Returns the localized title of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this institute slider4
	 */
	@AutoEscape
	public String getTitle(String languageId);

	/**
	 * Returns the localized title of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this institute slider4
	 */
	@AutoEscape
	public String getTitle(String languageId, boolean useDefault);

	@AutoEscape
	public String getTitleCurrentLanguageId();

	@AutoEscape
	public String getTitleCurrentValue();

	/**
	 * Returns a map of the locales and localized titles of this institute slider4.
	 *
	 * @return the locales and localized titles of this institute slider4
	 */
	public Map<Locale, String> getTitleMap();

	/**
	 * Sets the title of this institute slider4.
	 *
	 * @param title the title of this institute slider4
	 */
	public void setTitle(String title);

	/**
	 * Sets the localized title of this institute slider4 in the language.
	 *
	 * @param title the localized title of this institute slider4
	 * @param locale the locale of the language
	 */
	public void setTitle(String title, Locale locale);

	/**
	 * Sets the localized title of this institute slider4 in the language, and sets the default locale.
	 *
	 * @param title the localized title of this institute slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setTitle(String title, Locale locale, Locale defaultLocale);

	public void setTitleCurrentLanguageId(String languageId);

	/**
	 * Sets the localized titles of this institute slider4 from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this institute slider4
	 */
	public void setTitleMap(Map<Locale, String> titleMap);

	/**
	 * Sets the localized titles of this institute slider4 from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this institute slider4
	 * @param defaultLocale the default locale
	 */
	public void setTitleMap(Map<Locale, String> titleMap, Locale defaultLocale);

	/**
	 * Returns the description of this institute slider4.
	 *
	 * @return the description of this institute slider4
	 */
	public String getDescription();

	/**
	 * Returns the localized description of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this institute slider4
	 */
	@AutoEscape
	public String getDescription(Locale locale);

	/**
	 * Returns the localized description of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this institute slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getDescription(Locale locale, boolean useDefault);

	/**
	 * Returns the localized description of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this institute slider4
	 */
	@AutoEscape
	public String getDescription(String languageId);

	/**
	 * Returns the localized description of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this institute slider4
	 */
	@AutoEscape
	public String getDescription(String languageId, boolean useDefault);

	@AutoEscape
	public String getDescriptionCurrentLanguageId();

	@AutoEscape
	public String getDescriptionCurrentValue();

	/**
	 * Returns a map of the locales and localized descriptions of this institute slider4.
	 *
	 * @return the locales and localized descriptions of this institute slider4
	 */
	public Map<Locale, String> getDescriptionMap();

	/**
	 * Sets the description of this institute slider4.
	 *
	 * @param description the description of this institute slider4
	 */
	public void setDescription(String description);

	/**
	 * Sets the localized description of this institute slider4 in the language.
	 *
	 * @param description the localized description of this institute slider4
	 * @param locale the locale of the language
	 */
	public void setDescription(String description, Locale locale);

	/**
	 * Sets the localized description of this institute slider4 in the language, and sets the default locale.
	 *
	 * @param description the localized description of this institute slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setDescription(
		String description, Locale locale, Locale defaultLocale);

	public void setDescriptionCurrentLanguageId(String languageId);

	/**
	 * Sets the localized descriptions of this institute slider4 from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this institute slider4
	 */
	public void setDescriptionMap(Map<Locale, String> descriptionMap);

	/**
	 * Sets the localized descriptions of this institute slider4 from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this institute slider4
	 * @param defaultLocale the default locale
	 */
	public void setDescriptionMap(
		Map<Locale, String> descriptionMap, Locale defaultLocale);

	/**
	 * Returns the image of this institute slider4.
	 *
	 * @return the image of this institute slider4
	 */
	public String getImage();

	/**
	 * Returns the localized image of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this institute slider4
	 */
	@AutoEscape
	public String getImage(Locale locale);

	/**
	 * Returns the localized image of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this institute slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getImage(Locale locale, boolean useDefault);

	/**
	 * Returns the localized image of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this institute slider4
	 */
	@AutoEscape
	public String getImage(String languageId);

	/**
	 * Returns the localized image of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this institute slider4
	 */
	@AutoEscape
	public String getImage(String languageId, boolean useDefault);

	@AutoEscape
	public String getImageCurrentLanguageId();

	@AutoEscape
	public String getImageCurrentValue();

	/**
	 * Returns a map of the locales and localized images of this institute slider4.
	 *
	 * @return the locales and localized images of this institute slider4
	 */
	public Map<Locale, String> getImageMap();

	/**
	 * Sets the image of this institute slider4.
	 *
	 * @param image the image of this institute slider4
	 */
	public void setImage(String image);

	/**
	 * Sets the localized image of this institute slider4 in the language.
	 *
	 * @param image the localized image of this institute slider4
	 * @param locale the locale of the language
	 */
	public void setImage(String image, Locale locale);

	/**
	 * Sets the localized image of this institute slider4 in the language, and sets the default locale.
	 *
	 * @param image the localized image of this institute slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setImage(String image, Locale locale, Locale defaultLocale);

	public void setImageCurrentLanguageId(String languageId);

	/**
	 * Sets the localized images of this institute slider4 from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this institute slider4
	 */
	public void setImageMap(Map<Locale, String> imageMap);

	/**
	 * Sets the localized images of this institute slider4 from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this institute slider4
	 * @param defaultLocale the default locale
	 */
	public void setImageMap(Map<Locale, String> imageMap, Locale defaultLocale);

	/**
	 * Returns the button label of this institute slider4.
	 *
	 * @return the button label of this institute slider4
	 */
	public String getButtonLabel();

	/**
	 * Returns the localized button label of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button label of this institute slider4
	 */
	@AutoEscape
	public String getButtonLabel(Locale locale);

	/**
	 * Returns the localized button label of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this institute slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getButtonLabel(Locale locale, boolean useDefault);

	/**
	 * Returns the localized button label of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button label of this institute slider4
	 */
	@AutoEscape
	public String getButtonLabel(String languageId);

	/**
	 * Returns the localized button label of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this institute slider4
	 */
	@AutoEscape
	public String getButtonLabel(String languageId, boolean useDefault);

	@AutoEscape
	public String getButtonLabelCurrentLanguageId();

	@AutoEscape
	public String getButtonLabelCurrentValue();

	/**
	 * Returns a map of the locales and localized button labels of this institute slider4.
	 *
	 * @return the locales and localized button labels of this institute slider4
	 */
	public Map<Locale, String> getButtonLabelMap();

	/**
	 * Sets the button label of this institute slider4.
	 *
	 * @param buttonLabel the button label of this institute slider4
	 */
	public void setButtonLabel(String buttonLabel);

	/**
	 * Sets the localized button label of this institute slider4 in the language.
	 *
	 * @param buttonLabel the localized button label of this institute slider4
	 * @param locale the locale of the language
	 */
	public void setButtonLabel(String buttonLabel, Locale locale);

	/**
	 * Sets the localized button label of this institute slider4 in the language, and sets the default locale.
	 *
	 * @param buttonLabel the localized button label of this institute slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setButtonLabel(
		String buttonLabel, Locale locale, Locale defaultLocale);

	public void setButtonLabelCurrentLanguageId(String languageId);

	/**
	 * Sets the localized button labels of this institute slider4 from the map of locales and localized button labels.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this institute slider4
	 */
	public void setButtonLabelMap(Map<Locale, String> buttonLabelMap);

	/**
	 * Sets the localized button labels of this institute slider4 from the map of locales and localized button labels, and sets the default locale.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this institute slider4
	 * @param defaultLocale the default locale
	 */
	public void setButtonLabelMap(
		Map<Locale, String> buttonLabelMap, Locale defaultLocale);

	/**
	 * Returns the button link of this institute slider4.
	 *
	 * @return the button link of this institute slider4
	 */
	public String getButtonLink();

	/**
	 * Returns the localized button link of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button link of this institute slider4
	 */
	@AutoEscape
	public String getButtonLink(Locale locale);

	/**
	 * Returns the localized button link of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this institute slider4. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getButtonLink(Locale locale, boolean useDefault);

	/**
	 * Returns the localized button link of this institute slider4 in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button link of this institute slider4
	 */
	@AutoEscape
	public String getButtonLink(String languageId);

	/**
	 * Returns the localized button link of this institute slider4 in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this institute slider4
	 */
	@AutoEscape
	public String getButtonLink(String languageId, boolean useDefault);

	@AutoEscape
	public String getButtonLinkCurrentLanguageId();

	@AutoEscape
	public String getButtonLinkCurrentValue();

	/**
	 * Returns a map of the locales and localized button links of this institute slider4.
	 *
	 * @return the locales and localized button links of this institute slider4
	 */
	public Map<Locale, String> getButtonLinkMap();

	/**
	 * Sets the button link of this institute slider4.
	 *
	 * @param buttonLink the button link of this institute slider4
	 */
	public void setButtonLink(String buttonLink);

	/**
	 * Sets the localized button link of this institute slider4 in the language.
	 *
	 * @param buttonLink the localized button link of this institute slider4
	 * @param locale the locale of the language
	 */
	public void setButtonLink(String buttonLink, Locale locale);

	/**
	 * Sets the localized button link of this institute slider4 in the language, and sets the default locale.
	 *
	 * @param buttonLink the localized button link of this institute slider4
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setButtonLink(
		String buttonLink, Locale locale, Locale defaultLocale);

	public void setButtonLinkCurrentLanguageId(String languageId);

	/**
	 * Sets the localized button links of this institute slider4 from the map of locales and localized button links.
	 *
	 * @param buttonLinkMap the locales and localized button links of this institute slider4
	 */
	public void setButtonLinkMap(Map<Locale, String> buttonLinkMap);

	/**
	 * Sets the localized button links of this institute slider4 from the map of locales and localized button links, and sets the default locale.
	 *
	 * @param buttonLinkMap the locales and localized button links of this institute slider4
	 * @param defaultLocale the default locale
	 */
	public void setButtonLinkMap(
		Map<Locale, String> buttonLinkMap, Locale defaultLocale);

	@Override
	public String[] getAvailableLanguageIds();

	@Override
	public String getDefaultLanguageId();

	@Override
	public void prepareLocalizedFieldsForImport() throws LocaleException;

	@Override
	public void prepareLocalizedFieldsForImport(Locale defaultImportLocale)
		throws LocaleException;

}