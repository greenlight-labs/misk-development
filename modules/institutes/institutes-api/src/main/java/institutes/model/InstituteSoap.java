/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package institutes.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link institutes.service.http.InstituteServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class InstituteSoap implements Serializable {

	public static InstituteSoap toSoapModel(Institute model) {
		InstituteSoap soapModel = new InstituteSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setInstituteId(model.getInstituteId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setListingImage(model.getListingImage());
		soapModel.setListingTitle(model.getListingTitle());
		soapModel.setListingDescription(model.getListingDescription());
		soapModel.setBannerTitle(model.getBannerTitle());
		soapModel.setBannerSubtitle(model.getBannerSubtitle());
		soapModel.setBannerDescription(model.getBannerDescription());
		soapModel.setBannerDesktopImage(model.getBannerDesktopImage());
		soapModel.setBannerMobileImage(model.getBannerMobileImage());
		soapModel.setSection2Title(model.getSection2Title());
		soapModel.setSection2Subtitle(model.getSection2Subtitle());
		soapModel.setSection2Description(model.getSection2Description());
		soapModel.setSection2ButtonLabel(model.getSection2ButtonLabel());
		soapModel.setSection2ButtonLink(model.getSection2ButtonLink());
		soapModel.setSection2Image(model.getSection2Image());
		soapModel.setSection4SectionTitle(model.getSection4SectionTitle());

		return soapModel;
	}

	public static InstituteSoap[] toSoapModels(Institute[] models) {
		InstituteSoap[] soapModels = new InstituteSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static InstituteSoap[][] toSoapModels(Institute[][] models) {
		InstituteSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new InstituteSoap[models.length][models[0].length];
		}
		else {
			soapModels = new InstituteSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static InstituteSoap[] toSoapModels(List<Institute> models) {
		List<InstituteSoap> soapModels = new ArrayList<InstituteSoap>(
			models.size());

		for (Institute model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new InstituteSoap[soapModels.size()]);
	}

	public InstituteSoap() {
	}

	public long getPrimaryKey() {
		return _instituteId;
	}

	public void setPrimaryKey(long pk) {
		setInstituteId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getInstituteId() {
		return _instituteId;
	}

	public void setInstituteId(long instituteId) {
		_instituteId = instituteId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getListingImage() {
		return _listingImage;
	}

	public void setListingImage(String listingImage) {
		_listingImage = listingImage;
	}

	public String getListingTitle() {
		return _listingTitle;
	}

	public void setListingTitle(String listingTitle) {
		_listingTitle = listingTitle;
	}

	public String getListingDescription() {
		return _listingDescription;
	}

	public void setListingDescription(String listingDescription) {
		_listingDescription = listingDescription;
	}

	public String getBannerTitle() {
		return _bannerTitle;
	}

	public void setBannerTitle(String bannerTitle) {
		_bannerTitle = bannerTitle;
	}

	public String getBannerSubtitle() {
		return _bannerSubtitle;
	}

	public void setBannerSubtitle(String bannerSubtitle) {
		_bannerSubtitle = bannerSubtitle;
	}

	public String getBannerDescription() {
		return _bannerDescription;
	}

	public void setBannerDescription(String bannerDescription) {
		_bannerDescription = bannerDescription;
	}

	public String getBannerDesktopImage() {
		return _bannerDesktopImage;
	}

	public void setBannerDesktopImage(String bannerDesktopImage) {
		_bannerDesktopImage = bannerDesktopImage;
	}

	public String getBannerMobileImage() {
		return _bannerMobileImage;
	}

	public void setBannerMobileImage(String bannerMobileImage) {
		_bannerMobileImage = bannerMobileImage;
	}

	public String getSection2Title() {
		return _section2Title;
	}

	public void setSection2Title(String section2Title) {
		_section2Title = section2Title;
	}

	public String getSection2Subtitle() {
		return _section2Subtitle;
	}

	public void setSection2Subtitle(String section2Subtitle) {
		_section2Subtitle = section2Subtitle;
	}

	public String getSection2Description() {
		return _section2Description;
	}

	public void setSection2Description(String section2Description) {
		_section2Description = section2Description;
	}

	public String getSection2ButtonLabel() {
		return _section2ButtonLabel;
	}

	public void setSection2ButtonLabel(String section2ButtonLabel) {
		_section2ButtonLabel = section2ButtonLabel;
	}

	public String getSection2ButtonLink() {
		return _section2ButtonLink;
	}

	public void setSection2ButtonLink(String section2ButtonLink) {
		_section2ButtonLink = section2ButtonLink;
	}

	public String getSection2Image() {
		return _section2Image;
	}

	public void setSection2Image(String section2Image) {
		_section2Image = section2Image;
	}

	public String getSection4SectionTitle() {
		return _section4SectionTitle;
	}

	public void setSection4SectionTitle(String section4SectionTitle) {
		_section4SectionTitle = section4SectionTitle;
	}

	private String _uuid;
	private long _instituteId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _listingImage;
	private String _listingTitle;
	private String _listingDescription;
	private String _bannerTitle;
	private String _bannerSubtitle;
	private String _bannerDescription;
	private String _bannerDesktopImage;
	private String _bannerMobileImage;
	private String _section2Title;
	private String _section2Subtitle;
	private String _section2Description;
	private String _section2ButtonLabel;
	private String _section2ButtonLink;
	private String _section2Image;
	private String _section4SectionTitle;

}