<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="institutes-admin-buttons">
        <portlet:renderURL var="addInstituteURL">
            <portlet:param name="mvcPath"
                           value="/edit_institute.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addInstituteURL.toString() %>"
                    value="Add Institute"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= InstituteLocalServiceUtil.getInstitutesCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= InstituteLocalServiceUtil.getInstitutes(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="institutes.model.Institute" modelVar="institute">

            <liferay-ui:search-container-column-text
                    name="Listing Title"
                    value="<%= HtmlUtil.escape(institute.getListingTitle(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>