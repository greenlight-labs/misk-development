package institutes.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import institutes.admin.web.constants.InstitutesAdminWebPortletKeys;
import institutes.model.Institute;
import institutes.model.InstituteSlider3;
import institutes.model.InstituteSlider4;
import institutes.service.*;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=InstitutesAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + InstitutesAdminWebPortletKeys.INSTITUTESADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class InstitutesAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addInstitute(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Institute.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");

		Map<Locale, String> bannerTitleMap = LocalizationUtil.getLocalizationMap(request, "bannerTitle");
		Map<Locale, String> bannerSubtitleMap = LocalizationUtil.getLocalizationMap(request, "bannerSubtitle");
		Map<Locale, String> bannerDescriptionMap = LocalizationUtil.getLocalizationMap(request, "bannerDescription");
		Map<Locale, String> bannerDesktopImageMap = LocalizationUtil.getLocalizationMap(request, "bannerDesktopImage");
		Map<Locale, String> bannerMobileImageMap = LocalizationUtil.getLocalizationMap(request, "bannerMobileImage");
		Map<Locale, String> section2TitleMap = LocalizationUtil.getLocalizationMap(request, "section2Title");
		Map<Locale, String> section2SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section2Subtitle");
		Map<Locale, String> section2DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section2Description");
		Map<Locale, String> section2ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLabel");
		Map<Locale, String> section2ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLink");
		Map<Locale, String> section2ImageMap = LocalizationUtil.getLocalizationMap(request, "section2Image");

		Map<Locale, String> section4SectionTitleMap = LocalizationUtil.getLocalizationMap(request, "section4SectionTitle");

		try {
			_instituteLocalService.addInstitute(serviceContext.getUserId(),
					listingImageMap, listingTitleMap, listingDescriptionMap,
					bannerTitleMap, bannerSubtitleMap, bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
					section2TitleMap, section2SubtitleMap, section2DescriptionMap, section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
					section4SectionTitleMap,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(InstitutesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_institute.jsp");
		}
	}

	public void updateInstitute(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Institute.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long instituteId = ParamUtil.getLong(request, "instituteId");

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");

		Map<Locale, String> bannerTitleMap = LocalizationUtil.getLocalizationMap(request, "bannerTitle");
		Map<Locale, String> bannerSubtitleMap = LocalizationUtil.getLocalizationMap(request, "bannerSubtitle");
		Map<Locale, String> bannerDescriptionMap = LocalizationUtil.getLocalizationMap(request, "bannerDescription");
		Map<Locale, String> bannerDesktopImageMap = LocalizationUtil.getLocalizationMap(request, "bannerDesktopImage");
		Map<Locale, String> bannerMobileImageMap = LocalizationUtil.getLocalizationMap(request, "bannerMobileImage");
		Map<Locale, String> section2TitleMap = LocalizationUtil.getLocalizationMap(request, "section2Title");
		Map<Locale, String> section2SubtitleMap = LocalizationUtil.getLocalizationMap(request, "section2Subtitle");
		Map<Locale, String> section2DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section2Description");
		Map<Locale, String> section2ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLabel");
		Map<Locale, String> section2ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section2ButtonLink");
		Map<Locale, String> section2ImageMap = LocalizationUtil.getLocalizationMap(request, "section2Image");

		Map<Locale, String> section4SectionTitleMap = LocalizationUtil.getLocalizationMap(request, "section4SectionTitle");

		try {
			/* ****************************START Section 3***************************** */
			String section3SlidesIndexes = ParamUtil.getString(request, "section3SlidesIndexes");
			String[] section3RowIndexes = section3SlidesIndexes.split(",");
			/*delete all old slider3 entries*/
			List<InstituteSlider3> instituteSlider3Slides = InstituteSlider3LocalServiceUtil.getInstituteSlider3Slides(instituteId);
			for (InstituteSlider3 curInstituteSlider3Slide : instituteSlider3Slides) {
				_instituteSlider3LocalService.deleteInstituteSlider3(curInstituteSlider3Slide);
			}
			/*add all slider3 entries*/
			for (int i = 0; i < section3RowIndexes.length; i++) {
				Map<Locale, String> section3ImageMap = LocalizationUtil.getLocalizationMap(request, "section3Image" + i);
				Map<Locale, String> section3DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section3Description" + i);
				Map<Locale, String> section3ThumbnailMap = LocalizationUtil.getLocalizationMap(request, "section3Thumbnail" + i);

				_instituteSlider3LocalService.addInstituteSlider3(serviceContext.getUserId(),
						instituteId,
						section3ImageMap, section3DescriptionMap, section3ThumbnailMap,
						serviceContext);
			}
			/* ****************************START Section 4***************************** */
			String section4SlidesIndexes = ParamUtil.getString(request, "section4SlidesIndexes");
			String[] section4RowIndexes = section4SlidesIndexes.split(",");

			/*delete all old slider4 entries*/
			List<InstituteSlider4> instituteSlider4Slides = InstituteSlider4LocalServiceUtil.getInstituteSlider4Slides(instituteId);
			for (InstituteSlider4 curInstituteSlider4Slide : instituteSlider4Slides) {
				_instituteSlider4LocalService.deleteInstituteSlider4(curInstituteSlider4Slide);
			}
			/*add all slider4 entries*/
			for (int i = 0; i < section4RowIndexes.length; i++) {
				Map<Locale, String> section4TitleMap = LocalizationUtil.getLocalizationMap(request, "section4Title" + i);
				Map<Locale, String> section4ImageMap = LocalizationUtil.getLocalizationMap(request, "section4Image" + i);
				Map<Locale, String> section4DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section4Description" + i);
				Map<Locale, String> section4ButtonLabelMap = LocalizationUtil.getLocalizationMap(request, "section4ButtonLabel" + i);
				Map<Locale, String> section4ButtonLinkMap = LocalizationUtil.getLocalizationMap(request, "section4ButtonLink" + i);

				_instituteSlider4LocalService.addInstituteSlider4(serviceContext.getUserId(),
						instituteId,
						section4TitleMap, section4DescriptionMap, section4ImageMap, section4ButtonLabelMap, section4ButtonLinkMap,
						serviceContext);
			}
			/* *************************** */
			_instituteLocalService.updateInstitute(serviceContext.getUserId(), instituteId,
					listingImageMap, listingTitleMap, listingDescriptionMap,
					bannerTitleMap, bannerSubtitleMap, bannerDescriptionMap, bannerDesktopImageMap, bannerMobileImageMap,
					section2TitleMap, section2SubtitleMap, section2DescriptionMap, section2ButtonLabelMap, section2ButtonLinkMap, section2ImageMap,
					section4SectionTitleMap,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(InstitutesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_institute.jsp");
		}
	}

	public void deleteInstitute(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Institute.class.getName(), request);

		long instituteId = ParamUtil.getLong(request, "instituteId");

		try {
			_instituteLocalService.deleteInstitute(instituteId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(InstitutesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private InstituteLocalService _instituteLocalService;

	@Reference
	private InstituteSlider3LocalService _instituteSlider3LocalService;

	@Reference
	private InstituteSlider4LocalService _instituteSlider4LocalService;

	@Reference
	private ItemSelector _itemSelector;
}