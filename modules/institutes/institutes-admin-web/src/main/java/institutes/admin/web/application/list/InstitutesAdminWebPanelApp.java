package institutes.admin.web.application.list;

import institutes.admin.web.constants.InstitutesAdminWebPanelCategoryKeys;
import institutes.admin.web.constants.InstitutesAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + InstitutesAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class InstitutesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return InstitutesAdminWebPortletKeys.INSTITUTESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + InstitutesAdminWebPortletKeys.INSTITUTESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}