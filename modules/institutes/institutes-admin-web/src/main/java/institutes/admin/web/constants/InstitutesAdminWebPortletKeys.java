package institutes.admin.web.constants;

/**
 * @author tz
 */
public class InstitutesAdminWebPortletKeys {

	public static final String INSTITUTESADMINWEB =
		"institutes_admin_web_InstitutesAdminWebPortlet";

}