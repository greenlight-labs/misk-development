package institutes.web.portlet.route;

import com.liferay.portal.kernel.portlet.DefaultFriendlyURLMapper;
import com.liferay.portal.kernel.portlet.FriendlyURLMapper;
import org.osgi.service.component.annotations.Component;

/**
 * @author Adolfo P??rez
 */
@Component(
	property = {
		"com.liferay.portlet.friendly-url-routes=META-INF/friendly-url-routes/routes.xml",
		//@MY_TODO: need to fix the friendly url
		//"javax.portlet.name=" + InstitutesWebPortletKeys.INSTITUTESWEB
	},
	service = FriendlyURLMapper.class
)
public class InstituteFriendlyURLMapper extends DefaultFriendlyURLMapper {

	@Override
	public String getMapping() {
		return _MAPPING;
	}

	private static final String _MAPPING = "entities";

}