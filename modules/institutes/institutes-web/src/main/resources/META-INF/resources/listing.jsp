<div class="row">
    <%
        List<Institute> institutes = InstituteServiceUtil.getInstitutes(scopeGroupId);

        for (Institute curInstitute : institutes) {
    %>
    <portlet:renderURL var="detailPageURL2">
        <portlet:param name="instituteId" value="<%= String.valueOf(curInstitute.getInstituteId()) %>" />
        <portlet:param name="mvcPath" value="/detail.jsp" />
    </portlet:renderURL>
    <div class="col-12 col-md-6">
        <div class="residence-card">
            <a href="<%= detailPageURL2.toString() %>" class="img-wrapper image-overlay">
                <img src="<%= curInstitute.getListingImage(locale) %>" alt="img" class="img-fluid">
            </a>
            <div class="res-card-info">
                <h3><%= curInstitute.getListingTitle(locale) %></h3>
                <p><%= curInstitute.getListingDescription(locale) %></p>
                <a href="<%= detailPageURL2.toString() %>" class="btn btn-outline-white" tabindex="0">
                    <span><liferay-ui:message key="misk-explore-more" /></span><i class="dot-line"></i>
                </a>
            </div>
        </div>
    </div>
    <%
        }
    %>
</div>