<%
    List<Institute> institutes = InstituteLocalServiceUtil.getInstitutes(scopeGroupId, 0, 9);

    if(institutes.size() > 0){
%>

<section class="related-news-section" data-scroll-section>
    <%
        JournalArticle articleRelatedSection = JournalArticleLocalServiceUtil.fetchLatestArticle(scopeGroupId, "75137", 0);
        JournalArticleDisplay articleRelatedSectionDisplay = JournalArticleLocalServiceUtil.getArticleDisplay(scopeGroupId, articleRelatedSection.getArticleId(), articleRelatedSection.getDDMTemplateKey(), null, themeDisplay.getLanguageId(), themeDisplay);
        String relatedSection = articleRelatedSectionDisplay.getContent();
    %>

    <%=relatedSection.toString() %>

    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="innovation-image-slider">
                    <div class="slider related-news-slider">
                        <%
                            for (Institute curInstitute : institutes) {
                                if(instituteId == curInstitute.getInstituteId()){
                                    continue;
                                }
                        %>
                        <portlet:renderURL var="detailPageURL">
                            <portlet:param name="instituteId" value="<%= String.valueOf(curInstitute.getInstituteId()) %>" />
                            <portlet:param name="mvcPath" value="/detail.jsp" />
                        </portlet:renderURL>
                        <div>
                            <div class="innovate-img-box">
                                <a href="<%= detailPageURL.toString() %>" class="img-inno image-overlay">
                                    <img src="<%= curInstitute.getListingImage(locale) %>" alt="" class="img-fluid">
                                </a>
                                <div class="img-content">
                                    <h4 class="animate" data-animation="fadeInUp" data-duration="500"><a href="<%= detailPageURL.toString() %>"><%= curInstitute.getListingTitle(locale) %></a></h4>
                                    <p class="text-info"><%= dateFormat.format(curInstitute.getCreateDate()) %></p>
                                </div>
                            </div>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<%
    }
%>