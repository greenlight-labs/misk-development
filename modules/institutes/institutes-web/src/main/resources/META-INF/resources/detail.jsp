<%@include file="/init.jsp" %>

<%
    long instituteId = ParamUtil.getLong(renderRequest, "instituteId");

    Institute institute = null;
    if (instituteId > 0) {
        try {
            institute = InstituteLocalServiceUtil.getInstitute(instituteId);
        } catch (PortalException e) {
            //e.printStackTrace();
        }
    }
%>

<%
    if (institute == null) {
%>
<h4>No Record Found.</h4>
<%
} else {
        String bannerDesktopImage = institute.getBannerDesktopImage(locale);
        String bannerMobileImage = institute.getBannerMobileImage(locale);
%>
<section class="residence-detail-banner-sec" data-scroll-section>
    <div class="home-image-wrapper residence-image-wrapper">
        <div class="homeImgBanner">
            <div class="item image">
                <div class="slide-image slide-media">
                    <div class="gradient-box"></div>
                    <div class="inner-banner-img d-none d-md-block" style="background-image: url('<%=bannerDesktopImage%>')"></div>
                    <div class="inner-banner-img d-block d-md-none" style="background-image: url('<%=bannerMobileImage%>')"></div>
                    <div class="container banner-container">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="banner-animate"><span class="misk-big-text-mask animate" data-animation="maskTextUp" data-duration="500"><span class="animate-up"><%=institute.getBannerTitle(locale)%></span></span></h3>
                                <h1 class="banner-animate"><span class="misk-big-text-mask animate" data-animation="maskTextUp" data-duration="600"><span class="animate-up"><%=institute.getBannerSubtitle(locale)%></span></span></h1>
                                <div class="banner-content banner-animate">
                                    <p class="lead misk-big-text-mask animate" data-animation="maskTextUp" data-duration="700"><span class="animate-up"><%=institute.getBannerDescription(locale)%></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-shape"><img src="/assets/images/banner-shape.png" alt="" class="img-fluid"></div>
    </div>
</section>
<section class="liveable-city-section residence-detail-wadi-sec" data-scroll-section>
    <div class="liveable-right-shape animate" data-animation="fadeInRight" data-duration="700"><img src="/assets/svgs/pink-shape.svg" alt="" class="img-fluid"></div>
    <div class="container liveable-main-div">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="liveable-content" data-scroll data-scroll-speed="2">
                    <h3><%=institute.getSection2Title(locale)%></h3>
                    <h4 class="text-primary"><%=institute.getSection2Subtitle(locale)%></h4>
                    <%=institute.getSection2Description(locale)%>
                    <% if(institute.getSection2ButtonLabel(locale) != null && institute.getSection2ButtonLink(locale)  != null){ %>
                    <a href="<%=institute.getSection2ButtonLink(locale)%>" class="btn btn-outline-primary download-pdf-button">
                        <i class="icon-pdf"></i>
                        <span><%=institute.getSection2ButtonLabel(locale)%></span><i class="dot-line"></i>
                    </a>
                    <% } %>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="liveable-img text-right" data-scroll data-scroll-speed="1">
                    <img src="<%=institute.getSection2Image(locale)%>" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>
<%
    List<InstituteSlider3> instituteSlider3Slides = InstituteSlider3LocalServiceUtil.getInstituteSlider3Slides(instituteId);
    if(instituteSlider3Slides.size() > 0){
%>
<section class="circle-slider-sec" data-scroll-section>
    <div class="residence-slider-wrapper">
        <div class="residence-slider-main">
            <%
                for (InstituteSlider3 curSlide : instituteSlider3Slides) {
            %>
            <div class="slide">
                <div class="slide-bg-img" style="background-image: url('<%=curSlide.getImage(locale)%>')"></div>
                <div class="container banner-container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-8 text-center">
                            <div class="banner-content banner-text-animate">
                                <p class="lead misk-big-text-mask animate" data-animation="maskTextUp" data-duration="700"><span class="animate-up"><%=curSlide.getDescription(locale)%></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%
                }
            %>
        </div>
        <div class="thumb-wrapper">
            <div class="residence-slider-thumb">
                <%
                    for (InstituteSlider3 curSlide : instituteSlider3Slides) {
                %>
                <div class="slide">
                    <div class="img-wrap">
                        <img src="<%=curSlide.getThumbnail(locale)%>" alt="" class="img-responsive img-fluid">
                        <div class="progress-bar"></div>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</section>
<%
    }
%>
<%
    String section4Title = institute.getSection4SectionTitle(locale);
    List<InstituteSlider4> instituteSlider4Slides = InstituteSlider4LocalServiceUtil.getInstituteSlider4Slides(instituteId);
    if(section4Title != null && instituteSlider4Slides.size() > 0){
%>
<section class="innovation-section residence-innovation-sec" data-scroll-section>
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <h3 class="text-center heading_"><%=institute.getSection4SectionTitle(locale).replaceAll("\n", "<br/>")%></h3>
                <div class="innovation-image-slider">
                    <div class="slider innovation-explore-slider">
                        <%
                            for (InstituteSlider4 curSlide : instituteSlider4Slides) {
                        %>
                        <div>
                            <div class="innovate-img-box">
                                <div class="img-inno"><img src="<%=curSlide.getImage(locale)%>" alt="" class="img-fluid"></div>
                                <div class="img-content">
                                    <h4 class="animate" data-animation="fadeInUp" data-duration="500"><a href="javascript:"><%=curSlide.getTitle(locale)%></a></h4>
                                    <p class="animate" data-animation="fadeInUp" data-duration="500"><%=curSlide.getDescription(locale)%></p>
                                    <% if(curSlide.getButtonLabel(locale) != null && curSlide.getButtonLink(locale)  != null){ %>
                                    <a data-animation="fadeInUp" data-duration="500" href="<%=curSlide.getButtonLink(locale)%>" class="btn btn-outline-white translate-animate animate" tabindex="0">
                                        <span><%=curSlide.getButtonLabel(locale)%></span><i class="dot-line"></i>
                                    </a>
                                    <% } %>
                                </div>
                            </div>
                        </div>
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<%
    }
%>

<%@include file="includes/related_posts_section.jsp" %>

<%@include file="includes/footer_section.jsp" %>

<%
    }
%>