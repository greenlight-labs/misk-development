package bookings.admin.web.portlet;

import bookings.admin.web.constants.BookingsAdminWebPortletKeys;

import bookings.exception.BookingValidateException;
import bookings.model.Booking;
import bookings.service.BookingLocalService;
import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.*;

import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=BookingsAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + BookingsAdminWebPortletKeys.BOOKINGSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class BookingsAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addEntry(ActionRequest request, ActionResponse response)
			throws Exception, BookingValidateException {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
		Booking entry = _bookingLocalService.getBookingFromRequest(
				primaryKey, request);
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Booking.class.getName(), request);

		// Add entry
		_bookingLocalService.addEntry(entry, serviceContext);

		SessionMessages.add(request, "bookingAddedSuccessfully");
	}

	public void updateEntry(ActionRequest request, ActionResponse response)
			throws Exception {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

		Booking entry = _bookingLocalService.getBookingFromRequest(
				primaryKey, request);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Booking.class.getName(), request);

		//Update entry
		_bookingLocalService.updateEntry(entry, serviceContext);

		SessionMessages.add(request, "bookingUpdatedSuccessfully");
	}

	public void deleteEntry(ActionRequest request, ActionResponse response)
			throws PortalException {

		long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

		try {
			_bookingLocalService.deleteEntry(entryId);
		} catch (PortalException pe) {
			ReflectionUtil.throwException(pe);
		}
	}

	@Reference
	private BookingLocalService _bookingLocalService;

	@Reference
	private ItemSelector _itemSelector;
}