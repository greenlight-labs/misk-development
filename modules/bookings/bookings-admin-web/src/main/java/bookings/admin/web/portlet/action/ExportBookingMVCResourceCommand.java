package bookings.admin.web.portlet.action;

import com.liferay.petra.string.StringBundler;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import bookings.admin.web.constants.BookingsAdminWebPortletKeys;
import bookings.model.Booking;
import bookings.service.BookingLocalService;
import courts.service.CourtLocalServiceUtil;
import misk.app.users.service.AppUserLocalServiceUtil;

@Component(immediate = true, property = { "javax.portlet.name=" + BookingsAdminWebPortletKeys.BOOKINGSADMINWEB,
		"mvc.command.name=/booking/export_booking" }, service = MVCResourceCommand.class)
public class ExportBookingMVCResourceCommand extends BaseMVCResourceCommand {

	@Override
	protected void doServeResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception {

		try {
			SessionMessages.add(resourceRequest,
					_portal.getPortletId(resourceRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

			String csv = getBookingCSV(resourceRequest, resourceResponse);

			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, "booking.csv", csv.getBytes(),
					ContentTypes.TEXT_CSV_UTF8);
		} catch (Exception exception) {
			SessionErrors.add(resourceRequest, exception.getClass());

			_log.error(exception, exception);
		}
	}

	protected String getBookingCSV(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

		List<Booking> bookingList = getBookingList(resourceRequest);

		StringBundler sb = new StringBundler(bookingList.size());
		sb.append("Court");
		sb.append(StringPool.COMMA);
		sb.append("Customer");
		sb.append(StringPool.COMMA);
		sb.append("Start Time");
		sb.append(StringPool.COMMA);
		sb.append("End Time");
		sb.append(StringPool.COMMA);
		sb.append("Status");
		sb.append(StringPool.NEW_LINE);
		for (int i = 0; i < bookingList.size(); i++) {
			sb.append(getBookingCSV(bookingList.get(i), themeDisplay));
		}

		return sb.toString();
	}

	protected String getBookingCSV(Booking booking, ThemeDisplay themeDisplay) {

		StringBundler sb = new StringBundler();
		Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("MM/dd/yyyy", themeDisplay.getLocale(),
				themeDisplay.getTimeZone());
		String courtName = StringPool.BLANK;
		String appUserName = StringPool.BLANK;
		try {
			courtName = CourtLocalServiceUtil.getCourt(booking.getCourtId()).getName(themeDisplay.getLocale());
			appUserName = AppUserLocalServiceUtil.getAppUser(booking.getAppUserId()).getFullName();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		sb.append(courtName);
		sb.append(StringPool.COMMA);
		sb.append(appUserName);
		sb.append(StringPool.COMMA);
		sb.append(dateFormat.format(booking.getSlotStartTime()));
		sb.append(StringPool.COMMA);
		sb.append(dateFormat.format(booking.getSlotEndTime()));
		sb.append(StringPool.COMMA);
		sb.append(booking.getStatus());
		sb.append(StringPool.NEW_LINE);
		return sb.toString();
	}

	List<Booking> getBookingList(ResourceRequest actionRequest) throws ParseException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(actionRequest);
		String startDateStr = PortalUtil.getOriginalServletRequest(request).getParameter("startDate");
		String endDateStr = PortalUtil.getOriginalServletRequest(request).getParameter("endDate");
		String courtId = PortalUtil.getOriginalServletRequest(request).getParameter("courtId");
		System.out.println(" courtId "+courtId+"---endDateStr--"+endDateStr );
		Date startDate = null;
		Date endDate = null;

		if (Validator.isNotNull(startDateStr)) {

			startDate = new SimpleDateFormat("MM/dd/yyyy").parse(startDateStr);

		}
		if (Validator.isNotNull(endDateStr)) {

			endDate = new SimpleDateFormat("MM/dd/yyyy").parse(endDateStr);

		}

		List<Booking> bookingList = new ArrayList<Booking>();
		if (courtId.equalsIgnoreCase("all") && startDateStr == "" && endDateStr == "") {

			bookingList = _bookingLocalService.getBookings(-1, -1);

		} else {

			bookingList = _bookingLocalService.findBookingsByCourtIdAndDate(courtId, startDate, endDate);
		}
		return bookingList;
	}

	private static final Log _log = LogFactoryUtil.getLog(ExportBookingMVCResourceCommand.class);

	@Reference
	private Portal _portal;

	@Reference
	private BookingLocalService _bookingLocalService;

}