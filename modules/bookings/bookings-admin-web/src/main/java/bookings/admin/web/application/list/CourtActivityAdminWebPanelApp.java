package bookings.admin.web.application.list;

import bookings.admin.web.constants.BookingsAdminWebPanelCategoryKeys;
import bookings.admin.web.constants.BookingsAdminWebPortletKeys;
import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + BookingsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class CourtActivityAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return BookingsAdminWebPortletKeys.COURTACTIVITYADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + BookingsAdminWebPortletKeys.COURTACTIVITYADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}