package bookings.admin.web.constants;

/**
 * @author tz
 */
public class BookingsAdminWebPortletKeys {

	public static final String BOOKINGSADMINWEB =
		"bookings_admin_web_BookingsAdminWebPortlet";

	public static final String COURTACTIVITYADMINWEB =
			"bookings_admin_web_CourtActivityAdminWebPortlet";

	public static final String COURTTIMINGADMINWEB =
			"bookings_admin_web_CourtTimingAdminWebPortlet";
}