package bookings.admin.web.portlet.action;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import bookings.admin.web.constants.BookingsAdminWebPortletKeys;
import bookings.model.Booking;
import bookings.service.BookingLocalService;

@Component(immediate = true, property = { "javax.portlet.name=" + BookingsAdminWebPortletKeys.BOOKINGSADMINWEB,
		"mvc.command.name=searchBooking" }, service = MVCActionCommand.class)
public class BookingSearchMVCActionCommand extends BaseMVCActionCommand {

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		String mvcPath = "/export/export-booking.jsp";
		String cmd = ParamUtil.getString(actionRequest, Constants.CMD);
		String courtId = ParamUtil.getString(actionRequest, "courtId");
		System.out.println("cmd " + cmd + " courtId " + courtId);
		if (cmd.equals(Constants.SEARCH)) {
			searchBooking(actionRequest);
		}
		actionResponse.setRenderParameter("mvcPath", mvcPath);
	}

	protected void searchBooking(ActionRequest actionRequest) throws Exception {
		String startDateStr = ParamUtil.getString(actionRequest, "startDate");
		String endDateStr = ParamUtil.getString(actionRequest, "endDate");
		String courtId = ParamUtil.getString(actionRequest, "courtId");
		String parseDate = StringPool.BLANK;
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		/*
		 * Format dateFormat =
		 * FastDateFormatFactoryUtil.getSimpleDateFormat("MM/dd/yyyy",
		 * themeDisplay.getLocale(), themeDisplay.getTimeZone());
		 */
		Date startDate = null;
		Date endDate = null;

		if (Validator.isNotNull(startDateStr)) {

			startDate = new SimpleDateFormat("MM/dd/yyyy").parse(startDateStr);

		}
		if (Validator.isNotNull(endDateStr)) {

			endDate = new SimpleDateFormat("MM/dd/yyyy").parse(endDateStr);

		}

		List<Booking> bookingList = new ArrayList<Booking>();
		if (courtId.equalsIgnoreCase("all") && startDateStr == "" && endDateStr == "") {

			bookingList = _bookingLocalService.getBookings(-1, -1);

		} else {

			bookingList = _bookingLocalService.findBookingsByCourtIdAndDate(courtId, startDate, endDate);
		}
		actionRequest.setAttribute("searchBookingList", bookingList);

	}

	@Reference
	private BookingLocalService _bookingLocalService;

}
