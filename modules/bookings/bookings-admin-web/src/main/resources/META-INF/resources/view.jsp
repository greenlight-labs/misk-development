<%@ page import="courts.service.CourtLocalServiceUtil" %>
<%@ page import="misk.app.users.service.AppUserLocalServiceUtil" %>
<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath" value="/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>
		<portlet:renderURL var="exportURL">
		    <portlet:param name="mvcPath" value="/export/export-booking.jsp" />		     
		</portlet:renderURL>
        <aui:button onClick="<%= addEntryURL.toString() %>" value="Add Entry"/>
         <aui:button onClick="<%= exportURL.toString() %>" value="Export Booking"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= BookingLocalServiceUtil.countAllInGroup(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= BookingLocalServiceUtil.findAllInGroup(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="bookings.model.Booking" modelVar="booking">

            <%
                String courtName = null;
                String appUserName = null;
                try {
                    courtName = CourtLocalServiceUtil.getCourt(booking.getCourtId()).getName(locale);
                    appUserName = AppUserLocalServiceUtil.getAppUser(booking.getAppUserId()).getFullName();
                } catch (PortalException e) {
                    e.printStackTrace();
                }
            %>

            <liferay-ui:search-container-column-text name="Court" value="<%= HtmlUtil.escape(courtName) %>"/>
            <liferay-ui:search-container-column-text name="Customer" value="<%= HtmlUtil.escape(appUserName) %>"/>
            <%--<liferay-ui:search-container-column-text name="Start Time" value="<%= HtmlUtil.escape(booking.getSlotStartTime()) %>"/>
            <liferay-ui:search-container-column-text name="End Time" value="<%= HtmlUtil.escape(booking.getSlotEndTime()) %>"/>
            <liferay-ui:search-container-column-text name="Status" value="<%= HtmlUtil.escape(booking.getStatus()) %>"/>--%>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>