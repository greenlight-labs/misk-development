<%@include file = "../init.jsp" %>

<%
    long courtTimingId = ParamUtil.getLong(request, "courtTimingId");

    CourtTiming courtTiming = null;

    if (courtTimingId > 0) {
        try {
            courtTiming = CourtTimingLocalServiceUtil.getCourtTiming(courtTimingId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    List<Court> courts = CourtLocalServiceUtil.findAllInGroup(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= courtTiming == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= courtTiming %>" model="<%= CourtTiming.class %>" />

        <aui:input type="hidden" name="courtTimingId"
                   value='<%= courtTiming == null ? "" : courtTiming.getCourtTimingId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:select required="true" label="Court" name="courtId">
                    <% for (Court curCourt : courts) { %>
                    <aui:option label="<%= curCourt.getName(locale) %>" value="<%= curCourt.getCourtId() %>" />
                    <% } %>
                </aui:select>
                <aui:select required="true" label="Day" name="day">
                    <aui:option label="Monday" value="1" />
                    <aui:option label="Tuesday" value="2" />
                    <aui:option label="Wednesday" value="3" />
                    <aui:option label="Thursday" value="4" />
                    <aui:option label="Friday" value="5" />
                    <aui:option label="Saturday" value="6" />
                    <aui:option label="Sunday" value="7" />
                </aui:select>
                <aui:input name="openTime" label="Open Time">
                    <aui:validator name="required"/>
                    <%--<aui:validator name="custom">
                        function (val, fieldNode, ruleValue) {
                            const closedDay = window.document.getElementById( '<portlet:namespace /><portlet:namespace />_closedDay' );
                            if(closedDay.checked){
                                return false;
                            }else{
                                return true;
                            }
                        }
                    </aui:validator>--%>
                </aui:input>
                <aui:input name="closeTime" label="Close Time">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="slotDuration" label="Slot Duration (in minutes)">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="breakTime" label="Break Time (in minutes)">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="closedDay" label="Closed Day" />
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.courtTiming.href );
    }
</script>