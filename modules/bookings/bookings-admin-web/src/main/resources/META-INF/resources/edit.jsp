<%@include file = "init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    Booking entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = BookingLocalServiceUtil.getBooking(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    List<Court> courts = CourtLocalServiceUtil.findAllInGroup(scopeGroupId);
    List<AppUser> appUsers = AppUserLocalServiceUtil.findAllInGroup(scopeGroupId);
%>
 
<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= entry %>" model="<%= Booking.class %>" />

        <aui:input type="hidden" name="resourcePrimKey"
                   value='<%= entry == null ? "" : entry.getBookingId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:select required="true" label="Court" name="courtId">
                    <% for (Court curCourt : courts) { %>
                    <aui:option label="<%= curCourt.getName(locale) %>" value="<%= curCourt.getCourtId() %>" />
                    <% } %>
                </aui:select>
                <aui:select required="true" label="Customer" name="appUserId">
                    <% for (AppUser curAppUser : appUsers) { %>
                    <aui:option label="<%= curAppUser.getFullName() %>" value="<%= curAppUser.getAppUserId() %>" />
                    <% } %>
                </aui:select>
                <aui:input name="slotStartTime" label="Start Time" />
                <aui:input name="slotEndTime" label="End Time" />
                <aui:input name="status" label="Status" checked="true" />
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>