
<%@include file="../init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    CourtTiming courtTiming = (CourtTiming) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="courtTimingId"
                       value="<%=String.valueOf(courtTiming.getCourtTimingId()) %>" />
        <portlet:param name="mvcPath"
                       value="/court-timing/edit.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteEntry" var="deleteURL">
        <portlet:param name="courtTimingId"
                       value="<%= String.valueOf(courtTiming.getCourtTimingId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>