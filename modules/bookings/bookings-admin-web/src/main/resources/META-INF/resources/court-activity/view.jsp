<%@ include file="../init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="categories-admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/court-activity/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= CourtActivityLocalServiceUtil.countAllInGroup(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= CourtActivityLocalServiceUtil.findAllInGroup(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="bookings.model.CourtActivity" modelVar="courtActivity">

            <%
                String courtName = null;
                try {
                    courtName = CourtLocalServiceUtil.getCourt(courtActivity.getCourtId()).getName(locale);
                } catch (PortalException e) {
                    e.printStackTrace();
                }
            %>

            <liferay-ui:search-container-column-text name="court" value="<%= HtmlUtil.escape(courtName) %>"/>
            <%--<liferay-ui:search-container-column-text name="Start Time" value="<%= courtActivity.getStartTime() %>"/>
            <liferay-ui:search-container-column-text name="End Time" value="<%= courtActivity.getEndTime() %>"/>
            <liferay-ui:search-container-column-text name="allDay" value="<%= courtActivity.getAllDay() %>"/>
            <liferay-ui:search-container-column-text name="status" value="<%= courtActivity.getStatus() %>"/>--%>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/court-activity/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>