<%@include file = "../init.jsp" %>

<%
    long courtActivityId = ParamUtil.getLong(request, "courtActivityId");

    CourtActivity courtActivity = null;

    if (courtActivityId > 0) {
        try {
            courtActivity = CourtActivityLocalServiceUtil.getCourtActivity(courtActivityId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
    List<Court> courts = CourtLocalServiceUtil.findAllInGroup(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= courtActivity == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= courtActivity %>" model="<%= CourtActivity.class %>" />

        <aui:input type="hidden" name="courtActivityId"
                   value='<%= courtActivity == null ? "" : courtActivity.getCourtActivityId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:select required="true" label="Court" name="courtId">
                    <% for (Court curCourt : courts) { %>
                    <aui:option label="<%= curCourt.getName(locale) %>" value="<%= curCourt.getCourtId() %>" />
                    <% } %>
                </aui:select>
                <aui:input name="startTime" label="Start Time">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="endTime" label="End Time">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="allDay" label="All Day" />
                <aui:input name="status" label="Status" />
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.courtActivity.href );
    }
</script>