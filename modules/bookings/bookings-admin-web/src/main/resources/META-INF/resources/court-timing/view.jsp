<%@ include file="../init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="categories-admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/court-timing/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= CourtTimingLocalServiceUtil.countAllInGroup(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= CourtTimingLocalServiceUtil.findAllInGroup(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="bookings.model.CourtTiming" modelVar="courtTiming">

            <%
                String courtName = null;
                try {
                    courtName = CourtLocalServiceUtil.getCourt(courtTiming.getCourtId()).getName(locale);
                } catch (PortalException e) {
                    e.printStackTrace();
                }
            %>

            <liferay-ui:search-container-column-text name="Court" value="<%= HtmlUtil.escape(courtName) %>"/>
            <%--<liferay-ui:search-container-column-text name="Day" value="<%= HtmlUtil.escape(courtTiming.getDay()) %>"/>
            <liferay-ui:search-container-column-text name="Open Time" value="<%= HtmlUtil.escape(courtTiming.getOpenTime()) %>"/>
            <liferay-ui:search-container-column-text name="Close Time" value="<%= HtmlUtil.escape(courtTiming.getCloseTime()) %>"/>
            <liferay-ui:search-container-column-text name="Status" value="<%= courtTiming.getStatus() %>"/>--%>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/court-timing/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>