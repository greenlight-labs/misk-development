
<%@page import="com.liferay.portal.kernel.util.Constants"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.liferay.portal.kernel.util.CalendarFactoryUtil"%>

<%@page import="javax.portlet.PortletURL"%>
<%@include file="../init.jsp"%>

<%

List<Booking> bookingList = (List<Booking>)request.getAttribute("searchBookingList");
int countBookingList=0;
int count =BookingLocalServiceUtil.countAllInGroup(scopeGroupId); 
if(bookingList!=null){
	countBookingList =bookingList.size();
}
/* else
{
	countBookingList =BookingLocalServiceUtil.countAllInGroup(scopeGroupId);
	bookingList=BookingLocalServiceUtil.findAllInGroup(scopeGroupId,-1, -1);
} */
List<Court> courts = CourtLocalServiceUtil.findAllInGroup(scopeGroupId);
Calendar defaultValueDate = CalendarFactoryUtil.getCalendar();
%>

<liferay-portlet:resourceURL id="/booking/export_booking"
	var="exportURL" copyCurrentRenderParameters="<%= false %>">
</liferay-portlet:resourceURL>

<portlet:renderURL var="viewURL">
	<portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>
<portlet:actionURL name='searchBooking' var="searchBookingURL" />

<div class="container-fluid-1280">
	<div>
		<aui:form name="fm"  action="<%= searchBookingURL %>">
			<aui:fieldset-group markupView="lexicon">
				<aui:fieldset>
				<aui:input name="<%= Constants.CMD %>" type="hidden" value="search"/>
					<aui:select required="true" label="Court" name="courtId">
					<aui:option label="All"
							value="all" />
						<% for (Court curCourt : courts) { %>
						<aui:option label="<%= curCourt.getName(locale) %>"
							value="<%= curCourt.getCourtId() %>" />
						<% } %>
					</aui:select>
					<div class="row">
						<div class="col-md-6">
							<span class="to"><liferay-ui:message key="from" /></span> <span
								class="start-date-container"
								id="<portlet:namespace />startDateContainer"> <liferay-ui:input-date
									dayParam="startTimeDay"
									dayValue="<%= defaultValueDate.get(Calendar.DATE) %>"
									disabled="<%= false %>"
									firstDayOfWeek="<%= defaultValueDate.getFirstDayOfWeek() - 1 %>"
									monthParam="startTimeMonth"
									monthValue="<%= defaultValueDate.get(Calendar.MONTH) %>"
									name="startDate" yearParam="startTimeYear"
									yearValue="<%= defaultValueDate.get(Calendar.YEAR) %>" />



							</span>
						</div>
						<div class="col-md-6">
							<span class="to"><liferay-ui:message key="to" /></span> <span
								class="end-date-container"
								id="<portlet:namespace />endDateContainer"> <liferay-ui:input-date
									dayParam="endTimeDay"
									dayValue="<%= defaultValueDate.get(Calendar.DATE) %>"
									disabled="<%= false %>"
									firstDayOfWeek="<%= defaultValueDate.getFirstDayOfWeek() - 1 %>"
									monthParam="endTimeMonth"
									monthValue="<%= defaultValueDate.get(Calendar.MONTH) %>"
									name="endDate" yearParam="endTimeYear"
									yearValue="<%= defaultValueDate.get(Calendar.YEAR) %>" />


							</span>
						</div>
					</div>
				</aui:fieldset>
			</aui:fieldset-group>

			<aui:button-row>
				<aui:button type="submit" value="Search" />
				<aui:button onClick="<%= viewURL %>" type="cancel" />
				<a href="javascript:void(0);" class="btn  btn-secondary"
					onClick='<%= liferayPortletResponse.getNamespace() + "exportBooking();" %>'>Export
					Booking in CSV</a>
			</aui:button-row>
		</aui:form>
	</div>
	<hr />

	<div>
		<liferay-ui:search-container
			total="<%=countBookingList %>"
			iteratorURL="<%=currentURLObj%>">
			<liferay-ui:search-container-results
				results="<%=bookingList  %>" />

			<liferay-ui:search-container-row className="bookings.model.Booking"
				modelVar="booking">

				<%
                String courtName = null;
                String appUserName = null;
                try {
                    courtName = CourtLocalServiceUtil.getCourt(booking.getCourtId()).getName(locale);
                    appUserName = AppUserLocalServiceUtil.getAppUser(booking.getAppUserId()).getFullName();
                } catch (PortalException e) {
                    e.printStackTrace();
                }
            %>

				<liferay-ui:search-container-column-text name="Court"
					value="<%= HtmlUtil.escape(courtName) %>" />
				<liferay-ui:search-container-column-text name="Customer"
					value="<%= HtmlUtil.escape(appUserName) %>" />
				<liferay-ui:search-container-column-text name="Start Time"
					value="<%= dateFormat.format(booking.getSlotStartTime()) %>" />
				<liferay-ui:search-container-column-text name="End Time"
					value="<%= dateFormat.format(booking.getSlotEndTime()) %>" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator />
		</liferay-ui:search-container>
	</div>
</div>

<aui:script>
function <portlet:namespace />exportBooking(){

var courtId=document.getElementById('<portlet:namespace />courtId').value;
var startDate=document.getElementById('<portlet:namespace />startDate').value;
var endDate=document.getElementById('<portlet:namespace />endDate').value;

console.log("courtId "+courtId+" startDate "+startDate+" endDate "+endDate);
var parameters="&courtId="+courtId+"&startDate="+startDate+"&endDate="+endDate;

window.location.href='<%= exportURL + "&compress=0&etag=0&strip=0"%>'+parameters;
      <%--  AUI().use('aui-base', 'io', 'aui-io-request', function(A){
            A.io.request('<%= exportURL + "&compress=0&etag=0&strip=0" %>', {
                   method: 'get',
                   data: {
                       <portlet:namespace />sampleParam: 'value2',
                   },                   
                   on: {
                           success: function() {
                            
                       }
                  }
            });
        });  --%>
    }
    
    </aui:script>