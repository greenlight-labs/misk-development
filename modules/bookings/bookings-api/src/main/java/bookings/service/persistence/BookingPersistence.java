/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.persistence;

import bookings.exception.NoSuchBookingException;

import bookings.model.Booking;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the booking service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingUtil
 * @generated
 */
@ProviderType
public interface BookingPersistence extends BasePersistence<Booking> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link BookingUtil} to access the booking persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the bookings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching bookings
	 */
	public java.util.List<Booking> findByUuid(String uuid);

	/**
	 * Returns a range of all the bookings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public java.util.List<Booking> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the first booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the last booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the last booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the bookings before and after the current booking in the ordered set where uuid = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking[] findByUuid_PrevAndNext(
			long bookingId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Removes all the bookings where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of bookings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching bookings
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the booking where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchBookingException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByUUID_G(String uuid, long groupId)
		throws NoSuchBookingException;

	/**
	 * Returns the booking where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the booking where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the booking where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the booking that was removed
	 */
	public Booking removeByUUID_G(String uuid, long groupId)
		throws NoSuchBookingException;

	/**
	 * Returns the number of bookings where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching bookings
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching bookings
	 */
	public java.util.List<Booking> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public java.util.List<Booking> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the first booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the last booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the last booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the bookings before and after the current booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking[] findByUuid_C_PrevAndNext(
			long bookingId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Removes all the bookings where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching bookings
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the bookings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching bookings
	 */
	public java.util.List<Booking> findByGroupId(long groupId);

	/**
	 * Returns a range of all the bookings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public java.util.List<Booking> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the bookings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the first booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the last booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the last booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the bookings before and after the current booking in the ordered set where groupId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking[] findByGroupId_PrevAndNext(
			long bookingId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Removes all the bookings where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of bookings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching bookings
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the bookings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the matching bookings
	 */
	public java.util.List<Booking> findByCourtId(long courtId);

	/**
	 * Returns a range of all the bookings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public java.util.List<Booking> findByCourtId(
		long courtId, int start, int end);

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByCourtId(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByCourtId(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByCourtId_First(
			long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the first booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByCourtId_First(
		long courtId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the last booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByCourtId_Last(
			long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the last booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByCourtId_Last(
		long courtId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the bookings before and after the current booking in the ordered set where courtId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking[] findByCourtId_PrevAndNext(
			long bookingId, long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Removes all the bookings where courtId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 */
	public void removeByCourtId(long courtId);

	/**
	 * Returns the number of bookings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the number of matching bookings
	 */
	public int countByCourtId(long courtId);

	/**
	 * Returns all the bookings where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the matching bookings
	 */
	public java.util.List<Booking> findByAppUserId(long appUserId);

	/**
	 * Returns a range of all the bookings where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public java.util.List<Booking> findByAppUserId(
		long appUserId, int start, int end);

	/**
	 * Returns an ordered range of all the bookings where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByAppUserId(
		long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByAppUserId(
		long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByAppUserId_First(
			long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the first booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByAppUserId_First(
		long appUserId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the last booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByAppUserId_Last(
			long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the last booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByAppUserId_Last(
		long appUserId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the bookings before and after the current booking in the ordered set where appUserId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking[] findByAppUserId_PrevAndNext(
			long bookingId, long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Removes all the bookings where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 */
	public void removeByAppUserId(long appUserId);

	/**
	 * Returns the number of bookings where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching bookings
	 */
	public int countByAppUserId(long appUserId);

	/**
	 * Returns all the bookings where sync = &#63;.
	 *
	 * @param sync the sync
	 * @return the matching bookings
	 */
	public java.util.List<Booking> findBySync(boolean sync);

	/**
	 * Returns a range of all the bookings where sync = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param sync the sync
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public java.util.List<Booking> findBySync(boolean sync, int start, int end);

	/**
	 * Returns an ordered range of all the bookings where sync = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param sync the sync
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findBySync(
		boolean sync, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings where sync = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param sync the sync
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findBySync(
		boolean sync, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findBySync_First(
			boolean sync,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the first booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchBySync_First(
		boolean sync,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the last booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findBySync_Last(
			boolean sync,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the last booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchBySync_Last(
		boolean sync,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the bookings before and after the current booking in the ordered set where sync = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking[] findBySync_PrevAndNext(
			long bookingId, boolean sync,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Removes all the bookings where sync = &#63; from the database.
	 *
	 * @param sync the sync
	 */
	public void removeBySync(boolean sync);

	/**
	 * Returns the number of bookings where sync = &#63;.
	 *
	 * @param sync the sync
	 * @return the number of matching bookings
	 */
	public int countBySync(boolean sync);

	/**
	 * Returns all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @return the matching bookings
	 */
	public java.util.List<Booking> findByCourt_AppUser(
		long courtId, long appUserId);

	/**
	 * Returns a range of all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public java.util.List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end);

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public java.util.List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByCourt_AppUser_First(
			long courtId, long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the first booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByCourt_AppUser_First(
		long courtId, long appUserId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the last booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public Booking findByCourt_AppUser_Last(
			long courtId, long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Returns the last booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public Booking fetchByCourt_AppUser_Last(
		long courtId, long appUserId,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns the bookings before and after the current booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking[] findByCourt_AppUser_PrevAndNext(
			long bookingId, long courtId, long appUserId,
			com.liferay.portal.kernel.util.OrderByComparator<Booking>
				orderByComparator)
		throws NoSuchBookingException;

	/**
	 * Removes all the bookings where courtId = &#63; and appUserId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 */
	public void removeByCourt_AppUser(long courtId, long appUserId);

	/**
	 * Returns the number of bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @return the number of matching bookings
	 */
	public int countByCourt_AppUser(long courtId, long appUserId);

	/**
	 * Caches the booking in the entity cache if it is enabled.
	 *
	 * @param booking the booking
	 */
	public void cacheResult(Booking booking);

	/**
	 * Caches the bookings in the entity cache if it is enabled.
	 *
	 * @param bookings the bookings
	 */
	public void cacheResult(java.util.List<Booking> bookings);

	/**
	 * Creates a new booking with the primary key. Does not add the booking to the database.
	 *
	 * @param bookingId the primary key for the new booking
	 * @return the new booking
	 */
	public Booking create(long bookingId);

	/**
	 * Removes the booking with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking that was removed
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking remove(long bookingId) throws NoSuchBookingException;

	public Booking updateImpl(Booking booking);

	/**
	 * Returns the booking with the primary key or throws a <code>NoSuchBookingException</code> if it could not be found.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public Booking findByPrimaryKey(long bookingId)
		throws NoSuchBookingException;

	/**
	 * Returns the booking with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking, or <code>null</code> if a booking with the primary key could not be found
	 */
	public Booking fetchByPrimaryKey(long bookingId);

	/**
	 * Returns all the bookings.
	 *
	 * @return the bookings
	 */
	public java.util.List<Booking> findAll();

	/**
	 * Returns a range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of bookings
	 */
	public java.util.List<Booking> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bookings
	 */
	public java.util.List<Booking> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator);

	/**
	 * Returns an ordered range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of bookings
	 */
	public java.util.List<Booking> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Booking>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the bookings from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of bookings.
	 *
	 * @return the number of bookings
	 */
	public int countAll();

}