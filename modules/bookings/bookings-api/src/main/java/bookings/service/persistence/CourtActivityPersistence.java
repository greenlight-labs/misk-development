/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.persistence;

import bookings.exception.NoSuchCourtActivityException;

import bookings.model.CourtActivity;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the court activity service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtActivityUtil
 * @generated
 */
@ProviderType
public interface CourtActivityPersistence
	extends BasePersistence<CourtActivity> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourtActivityUtil} to access the court activity persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the court activities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid(String uuid);

	/**
	 * Returns a range of all the court activities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where uuid = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public CourtActivity[] findByUuid_PrevAndNext(
			long courtActivityId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Removes all the court activities where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of court activities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching court activities
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the court activity where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourtActivityException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByUUID_G(String uuid, long groupId)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the court activity where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the court activity where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the court activity where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the court activity that was removed
	 */
	public CourtActivity removeByUUID_G(String uuid, long groupId)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the number of court activities where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching court activities
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public CourtActivity[] findByUuid_C_PrevAndNext(
			long courtActivityId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Removes all the court activities where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching court activities
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the court activities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching court activities
	 */
	public java.util.List<CourtActivity> findByGroupId(long groupId);

	/**
	 * Returns a range of all the court activities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public java.util.List<CourtActivity> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the court activities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court activities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the first court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the last court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the last court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where groupId = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public CourtActivity[] findByGroupId_PrevAndNext(
			long courtActivityId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Removes all the court activities where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of court activities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching court activities
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the court activities where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the matching court activities
	 */
	public java.util.List<CourtActivity> findByCourtId(long courtId);

	/**
	 * Returns a range of all the court activities where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public java.util.List<CourtActivity> findByCourtId(
		long courtId, int start, int end);

	/**
	 * Returns an ordered range of all the court activities where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByCourtId(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court activities where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public java.util.List<CourtActivity> findByCourtId(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByCourtId_First(
			long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the first court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByCourtId_First(
		long courtId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the last court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public CourtActivity findByCourtId_Last(
			long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the last court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public CourtActivity fetchByCourtId_Last(
		long courtId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public CourtActivity[] findByCourtId_PrevAndNext(
			long courtActivityId, long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
				orderByComparator)
		throws NoSuchCourtActivityException;

	/**
	 * Removes all the court activities where courtId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 */
	public void removeByCourtId(long courtId);

	/**
	 * Returns the number of court activities where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the number of matching court activities
	 */
	public int countByCourtId(long courtId);

	/**
	 * Caches the court activity in the entity cache if it is enabled.
	 *
	 * @param courtActivity the court activity
	 */
	public void cacheResult(CourtActivity courtActivity);

	/**
	 * Caches the court activities in the entity cache if it is enabled.
	 *
	 * @param courtActivities the court activities
	 */
	public void cacheResult(java.util.List<CourtActivity> courtActivities);

	/**
	 * Creates a new court activity with the primary key. Does not add the court activity to the database.
	 *
	 * @param courtActivityId the primary key for the new court activity
	 * @return the new court activity
	 */
	public CourtActivity create(long courtActivityId);

	/**
	 * Removes the court activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity that was removed
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public CourtActivity remove(long courtActivityId)
		throws NoSuchCourtActivityException;

	public CourtActivity updateImpl(CourtActivity courtActivity);

	/**
	 * Returns the court activity with the primary key or throws a <code>NoSuchCourtActivityException</code> if it could not be found.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public CourtActivity findByPrimaryKey(long courtActivityId)
		throws NoSuchCourtActivityException;

	/**
	 * Returns the court activity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity, or <code>null</code> if a court activity with the primary key could not be found
	 */
	public CourtActivity fetchByPrimaryKey(long courtActivityId);

	/**
	 * Returns all the court activities.
	 *
	 * @return the court activities
	 */
	public java.util.List<CourtActivity> findAll();

	/**
	 * Returns a range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of court activities
	 */
	public java.util.List<CourtActivity> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of court activities
	 */
	public java.util.List<CourtActivity> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of court activities
	 */
	public java.util.List<CourtActivity> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtActivity>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the court activities from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of court activities.
	 *
	 * @return the number of court activities
	 */
	public int countAll();

}