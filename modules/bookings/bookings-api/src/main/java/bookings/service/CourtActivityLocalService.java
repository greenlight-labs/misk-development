/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service;

import bookings.exception.CourtActivityValidateException;

import bookings.model.CourtActivity;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for CourtActivity. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see CourtActivityLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface CourtActivityLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>bookings.service.impl.CourtActivityLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the court activity local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link CourtActivityLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the court activity to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public CourtActivity addCourtActivity(CourtActivity courtActivity);

	public CourtActivity addEntry(
			CourtActivity orgEntry, ServiceContext serviceContext)
		throws CourtActivityValidateException, PortalException;

	public int countAllInCourt(long courtId);

	public int countAllInGroup(long groupId);

	/**
	 * Creates a new court activity with the primary key. Does not add the court activity to the database.
	 *
	 * @param courtActivityId the primary key for the new court activity
	 * @return the new court activity
	 */
	@Transactional(enabled = false)
	public CourtActivity createCourtActivity(long courtActivityId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the court activity from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public CourtActivity deleteCourtActivity(CourtActivity courtActivity);

	/**
	 * Deletes the court activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity that was removed
	 * @throws PortalException if a court activity with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public CourtActivity deleteCourtActivity(long courtActivityId)
		throws PortalException;

	public CourtActivity deleteEntry(long primaryKey) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CourtActivity fetchCourtActivity(long courtActivityId);

	/**
	 * Returns the court activity matching the UUID and group.
	 *
	 * @param uuid the court activity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CourtActivity fetchCourtActivityByUuidAndGroupId(
		String uuid, long groupId);

	public List<CourtActivity> findAllInCourt(long courtId);

	public List<CourtActivity> findAllInCourt(long courtId, int start, int end);

	public List<CourtActivity> findAllInCourt(
		long courtId, int start, int end, OrderByComparator<CourtActivity> obc);

	public List<CourtActivity> findAllInGroup(long groupId);

	public List<CourtActivity> findAllInGroup(long groupId, int start, int end);

	public List<CourtActivity> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<CourtActivity> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns a range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of court activities
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CourtActivity> getCourtActivities(int start, int end);

	/**
	 * Returns all the court activities matching the UUID and company.
	 *
	 * @param uuid the UUID of the court activities
	 * @param companyId the primary key of the company
	 * @return the matching court activities, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CourtActivity> getCourtActivitiesByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of court activities matching the UUID and company.
	 *
	 * @param uuid the UUID of the court activities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching court activities, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CourtActivity> getCourtActivitiesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator);

	/**
	 * Returns the number of court activities.
	 *
	 * @return the number of court activities
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getCourtActivitiesCount();

	/**
	 * Returns the court activity with the primary key.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity
	 * @throws PortalException if a court activity with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CourtActivity getCourtActivity(long courtActivityId)
		throws PortalException;

	/**
	 * Returns the court activity matching the UUID and group.
	 *
	 * @param uuid the court activity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court activity
	 * @throws PortalException if a matching court activity could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CourtActivity getCourtActivityByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CourtActivity getCourtActivityFromRequest(
			long primaryKey, PortletRequest request)
		throws CourtActivityValidateException, PortletException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Date getDateTimeFromRequest(PortletRequest request, String prefix);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public CourtActivity getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Updates the court activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public CourtActivity updateCourtActivity(CourtActivity courtActivity);

	public CourtActivity updateEntry(
			CourtActivity orgEntry, ServiceContext serviceContext)
		throws CourtActivityValidateException, PortalException;

}