/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CourtTimingLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CourtTimingLocalService
 * @generated
 */
public class CourtTimingLocalServiceWrapper
	implements CourtTimingLocalService,
			   ServiceWrapper<CourtTimingLocalService> {

	public CourtTimingLocalServiceWrapper(
		CourtTimingLocalService courtTimingLocalService) {

		_courtTimingLocalService = courtTimingLocalService;
	}

	/**
	 * Adds the court timing to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTiming the court timing
	 * @return the court timing that was added
	 */
	@Override
	public bookings.model.CourtTiming addCourtTiming(
		bookings.model.CourtTiming courtTiming) {

		return _courtTimingLocalService.addCourtTiming(courtTiming);
	}

	@Override
	public bookings.model.CourtTiming addEntry(
			bookings.model.CourtTiming orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtTimingValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public int countAllInCourt(long courtId) {
		return _courtTimingLocalService.countAllInCourt(courtId);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _courtTimingLocalService.countAllInGroup(groupId);
	}

	/**
	 * Creates a new court timing with the primary key. Does not add the court timing to the database.
	 *
	 * @param courtTimingId the primary key for the new court timing
	 * @return the new court timing
	 */
	@Override
	public bookings.model.CourtTiming createCourtTiming(long courtTimingId) {
		return _courtTimingLocalService.createCourtTiming(courtTimingId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the court timing from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTiming the court timing
	 * @return the court timing that was removed
	 */
	@Override
	public bookings.model.CourtTiming deleteCourtTiming(
		bookings.model.CourtTiming courtTiming) {

		return _courtTimingLocalService.deleteCourtTiming(courtTiming);
	}

	/**
	 * Deletes the court timing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing that was removed
	 * @throws PortalException if a court timing with the primary key could not be found
	 */
	@Override
	public bookings.model.CourtTiming deleteCourtTiming(long courtTimingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.deleteCourtTiming(courtTimingId);
	}

	@Override
	public bookings.model.CourtTiming deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _courtTimingLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _courtTimingLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _courtTimingLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _courtTimingLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _courtTimingLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _courtTimingLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public bookings.model.CourtTiming fetchByCourtDay(long courtId, int day) {
		return _courtTimingLocalService.fetchByCourtDay(courtId, day);
	}

	@Override
	public bookings.model.CourtTiming fetchCourtTiming(long courtTimingId) {
		return _courtTimingLocalService.fetchCourtTiming(courtTimingId);
	}

	/**
	 * Returns the court timing matching the UUID and group.
	 *
	 * @param uuid the court timing's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public bookings.model.CourtTiming fetchCourtTimingByUuidAndGroupId(
		String uuid, long groupId) {

		return _courtTimingLocalService.fetchCourtTimingByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<bookings.model.CourtTiming> findAllInCourt(
		long courtId) {

		return _courtTimingLocalService.findAllInCourt(courtId);
	}

	@Override
	public java.util.List<bookings.model.CourtTiming> findAllInCourt(
		long courtId, int start, int end) {

		return _courtTimingLocalService.findAllInCourt(courtId, start, end);
	}

	@Override
	public java.util.List<bookings.model.CourtTiming> findAllInCourt(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<bookings.model.CourtTiming> obc) {

		return _courtTimingLocalService.findAllInCourt(
			courtId, start, end, obc);
	}

	@Override
	public java.util.List<bookings.model.CourtTiming> findAllInGroup(
		long groupId) {

		return _courtTimingLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<bookings.model.CourtTiming> findAllInGroup(
		long groupId, int start, int end) {

		return _courtTimingLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<bookings.model.CourtTiming> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<bookings.model.CourtTiming> obc) {

		return _courtTimingLocalService.findAllInGroup(
			groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _courtTimingLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the court timing with the primary key.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing
	 * @throws PortalException if a court timing with the primary key could not be found
	 */
	@Override
	public bookings.model.CourtTiming getCourtTiming(long courtTimingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.getCourtTiming(courtTimingId);
	}

	/**
	 * Returns the court timing matching the UUID and group.
	 *
	 * @param uuid the court timing's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court timing
	 * @throws PortalException if a matching court timing could not be found
	 */
	@Override
	public bookings.model.CourtTiming getCourtTimingByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.getCourtTimingByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public bookings.model.CourtTiming getCourtTimingFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws bookings.exception.CourtTimingValidateException,
			   javax.portlet.PortletException {

		return _courtTimingLocalService.getCourtTimingFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of court timings
	 */
	@Override
	public java.util.List<bookings.model.CourtTiming> getCourtTimings(
		int start, int end) {

		return _courtTimingLocalService.getCourtTimings(start, end);
	}

	/**
	 * Returns all the court timings matching the UUID and company.
	 *
	 * @param uuid the UUID of the court timings
	 * @param companyId the primary key of the company
	 * @return the matching court timings, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<bookings.model.CourtTiming>
		getCourtTimingsByUuidAndCompanyId(String uuid, long companyId) {

		return _courtTimingLocalService.getCourtTimingsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of court timings matching the UUID and company.
	 *
	 * @param uuid the UUID of the court timings
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching court timings, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<bookings.model.CourtTiming>
		getCourtTimingsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<bookings.model.CourtTiming> orderByComparator) {

		return _courtTimingLocalService.getCourtTimingsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of court timings.
	 *
	 * @return the number of court timings
	 */
	@Override
	public int getCourtTimingsCount() {
		return _courtTimingLocalService.getCourtTimingsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _courtTimingLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _courtTimingLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public bookings.model.CourtTiming getNewObject(long primaryKey) {
		return _courtTimingLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _courtTimingLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the court timing in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTiming the court timing
	 * @return the court timing that was updated
	 */
	@Override
	public bookings.model.CourtTiming updateCourtTiming(
		bookings.model.CourtTiming courtTiming) {

		return _courtTimingLocalService.updateCourtTiming(courtTiming);
	}

	@Override
	public bookings.model.CourtTiming updateEntry(
			bookings.model.CourtTiming orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtTimingValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _courtTimingLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public CourtTimingLocalService getWrappedService() {
		return _courtTimingLocalService;
	}

	@Override
	public void setWrappedService(
		CourtTimingLocalService courtTimingLocalService) {

		_courtTimingLocalService = courtTimingLocalService;
	}

	private CourtTimingLocalService _courtTimingLocalService;

}