/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.persistence;

import bookings.model.CourtActivity;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the court activity service. This utility wraps <code>bookings.service.persistence.impl.CourtActivityPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtActivityPersistence
 * @generated
 */
public class CourtActivityUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(CourtActivity courtActivity) {
		getPersistence().clearCache(courtActivity);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, CourtActivity> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CourtActivity> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CourtActivity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CourtActivity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static CourtActivity update(CourtActivity courtActivity) {
		return getPersistence().update(courtActivity);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static CourtActivity update(
		CourtActivity courtActivity, ServiceContext serviceContext) {

		return getPersistence().update(courtActivity, serviceContext);
	}

	/**
	 * Returns all the court activities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching court activities
	 */
	public static List<CourtActivity> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the court activities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public static List<CourtActivity> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByUuid_First(
			String uuid, OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByUuid_First(
		String uuid, OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByUuid_Last(
			String uuid, OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByUuid_Last(
		String uuid, OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where uuid = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public static CourtActivity[] findByUuid_PrevAndNext(
			long courtActivityId, String uuid,
			OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByUuid_PrevAndNext(
			courtActivityId, uuid, orderByComparator);
	}

	/**
	 * Removes all the court activities where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of court activities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching court activities
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the court activity where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourtActivityException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByUUID_G(String uuid, long groupId)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the court activity where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the court activity where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the court activity where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the court activity that was removed
	 */
	public static CourtActivity removeByUUID_G(String uuid, long groupId)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of court activities where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching court activities
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching court activities
	 */
	public static List<CourtActivity> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public static List<CourtActivity> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public static CourtActivity[] findByUuid_C_PrevAndNext(
			long courtActivityId, String uuid, long companyId,
			OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByUuid_C_PrevAndNext(
			courtActivityId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the court activities where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of court activities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching court activities
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the court activities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching court activities
	 */
	public static List<CourtActivity> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the court activities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public static List<CourtActivity> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the court activities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court activities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByGroupId_First(
			long groupId, OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByGroupId_First(
		long groupId, OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByGroupId_Last(
			long groupId, OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByGroupId_Last(
		long groupId, OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where groupId = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public static CourtActivity[] findByGroupId_PrevAndNext(
			long courtActivityId, long groupId,
			OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByGroupId_PrevAndNext(
			courtActivityId, groupId, orderByComparator);
	}

	/**
	 * Removes all the court activities where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of court activities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching court activities
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the court activities where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the matching court activities
	 */
	public static List<CourtActivity> findByCourtId(long courtId) {
		return getPersistence().findByCourtId(courtId);
	}

	/**
	 * Returns a range of all the court activities where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of matching court activities
	 */
	public static List<CourtActivity> findByCourtId(
		long courtId, int start, int end) {

		return getPersistence().findByCourtId(courtId, start, end);
	}

	/**
	 * Returns an ordered range of all the court activities where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().findByCourtId(
			courtId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court activities where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court activities
	 */
	public static List<CourtActivity> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByCourtId(
			courtId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByCourtId_First(
			long courtId, OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByCourtId_First(courtId, orderByComparator);
	}

	/**
	 * Returns the first court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByCourtId_First(
		long courtId, OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByCourtId_First(
			courtId, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity
	 * @throws NoSuchCourtActivityException if a matching court activity could not be found
	 */
	public static CourtActivity findByCourtId_Last(
			long courtId, OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByCourtId_Last(courtId, orderByComparator);
	}

	/**
	 * Returns the last court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchByCourtId_Last(
		long courtId, OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().fetchByCourtId_Last(courtId, orderByComparator);
	}

	/**
	 * Returns the court activities before and after the current court activity in the ordered set where courtId = &#63;.
	 *
	 * @param courtActivityId the primary key of the current court activity
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public static CourtActivity[] findByCourtId_PrevAndNext(
			long courtActivityId, long courtId,
			OrderByComparator<CourtActivity> orderByComparator)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByCourtId_PrevAndNext(
			courtActivityId, courtId, orderByComparator);
	}

	/**
	 * Removes all the court activities where courtId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 */
	public static void removeByCourtId(long courtId) {
		getPersistence().removeByCourtId(courtId);
	}

	/**
	 * Returns the number of court activities where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the number of matching court activities
	 */
	public static int countByCourtId(long courtId) {
		return getPersistence().countByCourtId(courtId);
	}

	/**
	 * Caches the court activity in the entity cache if it is enabled.
	 *
	 * @param courtActivity the court activity
	 */
	public static void cacheResult(CourtActivity courtActivity) {
		getPersistence().cacheResult(courtActivity);
	}

	/**
	 * Caches the court activities in the entity cache if it is enabled.
	 *
	 * @param courtActivities the court activities
	 */
	public static void cacheResult(List<CourtActivity> courtActivities) {
		getPersistence().cacheResult(courtActivities);
	}

	/**
	 * Creates a new court activity with the primary key. Does not add the court activity to the database.
	 *
	 * @param courtActivityId the primary key for the new court activity
	 * @return the new court activity
	 */
	public static CourtActivity create(long courtActivityId) {
		return getPersistence().create(courtActivityId);
	}

	/**
	 * Removes the court activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity that was removed
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public static CourtActivity remove(long courtActivityId)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().remove(courtActivityId);
	}

	public static CourtActivity updateImpl(CourtActivity courtActivity) {
		return getPersistence().updateImpl(courtActivity);
	}

	/**
	 * Returns the court activity with the primary key or throws a <code>NoSuchCourtActivityException</code> if it could not be found.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity
	 * @throws NoSuchCourtActivityException if a court activity with the primary key could not be found
	 */
	public static CourtActivity findByPrimaryKey(long courtActivityId)
		throws bookings.exception.NoSuchCourtActivityException {

		return getPersistence().findByPrimaryKey(courtActivityId);
	}

	/**
	 * Returns the court activity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity, or <code>null</code> if a court activity with the primary key could not be found
	 */
	public static CourtActivity fetchByPrimaryKey(long courtActivityId) {
		return getPersistence().fetchByPrimaryKey(courtActivityId);
	}

	/**
	 * Returns all the court activities.
	 *
	 * @return the court activities
	 */
	public static List<CourtActivity> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of court activities
	 */
	public static List<CourtActivity> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of court activities
	 */
	public static List<CourtActivity> findAll(
		int start, int end,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of court activities
	 */
	public static List<CourtActivity> findAll(
		int start, int end, OrderByComparator<CourtActivity> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the court activities from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of court activities.
	 *
	 * @return the number of court activities
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static CourtActivityPersistence getPersistence() {
		return _persistence;
	}

	private static volatile CourtActivityPersistence _persistence;

}