/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CourtActivityLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CourtActivityLocalService
 * @generated
 */
public class CourtActivityLocalServiceWrapper
	implements CourtActivityLocalService,
			   ServiceWrapper<CourtActivityLocalService> {

	public CourtActivityLocalServiceWrapper(
		CourtActivityLocalService courtActivityLocalService) {

		_courtActivityLocalService = courtActivityLocalService;
	}

	/**
	 * Adds the court activity to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was added
	 */
	@Override
	public bookings.model.CourtActivity addCourtActivity(
		bookings.model.CourtActivity courtActivity) {

		return _courtActivityLocalService.addCourtActivity(courtActivity);
	}

	@Override
	public bookings.model.CourtActivity addEntry(
			bookings.model.CourtActivity orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtActivityValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public int countAllInCourt(long courtId) {
		return _courtActivityLocalService.countAllInCourt(courtId);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _courtActivityLocalService.countAllInGroup(groupId);
	}

	/**
	 * Creates a new court activity with the primary key. Does not add the court activity to the database.
	 *
	 * @param courtActivityId the primary key for the new court activity
	 * @return the new court activity
	 */
	@Override
	public bookings.model.CourtActivity createCourtActivity(
		long courtActivityId) {

		return _courtActivityLocalService.createCourtActivity(courtActivityId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the court activity from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was removed
	 */
	@Override
	public bookings.model.CourtActivity deleteCourtActivity(
		bookings.model.CourtActivity courtActivity) {

		return _courtActivityLocalService.deleteCourtActivity(courtActivity);
	}

	/**
	 * Deletes the court activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity that was removed
	 * @throws PortalException if a court activity with the primary key could not be found
	 */
	@Override
	public bookings.model.CourtActivity deleteCourtActivity(
			long courtActivityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.deleteCourtActivity(courtActivityId);
	}

	@Override
	public bookings.model.CourtActivity deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _courtActivityLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _courtActivityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _courtActivityLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _courtActivityLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _courtActivityLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _courtActivityLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public bookings.model.CourtActivity fetchCourtActivity(
		long courtActivityId) {

		return _courtActivityLocalService.fetchCourtActivity(courtActivityId);
	}

	/**
	 * Returns the court activity matching the UUID and group.
	 *
	 * @param uuid the court activity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	@Override
	public bookings.model.CourtActivity fetchCourtActivityByUuidAndGroupId(
		String uuid, long groupId) {

		return _courtActivityLocalService.fetchCourtActivityByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<bookings.model.CourtActivity> findAllInCourt(
		long courtId) {

		return _courtActivityLocalService.findAllInCourt(courtId);
	}

	@Override
	public java.util.List<bookings.model.CourtActivity> findAllInCourt(
		long courtId, int start, int end) {

		return _courtActivityLocalService.findAllInCourt(courtId, start, end);
	}

	@Override
	public java.util.List<bookings.model.CourtActivity> findAllInCourt(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<bookings.model.CourtActivity> obc) {

		return _courtActivityLocalService.findAllInCourt(
			courtId, start, end, obc);
	}

	@Override
	public java.util.List<bookings.model.CourtActivity> findAllInGroup(
		long groupId) {

		return _courtActivityLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<bookings.model.CourtActivity> findAllInGroup(
		long groupId, int start, int end) {

		return _courtActivityLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<bookings.model.CourtActivity> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<bookings.model.CourtActivity> obc) {

		return _courtActivityLocalService.findAllInGroup(
			groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _courtActivityLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of court activities
	 */
	@Override
	public java.util.List<bookings.model.CourtActivity> getCourtActivities(
		int start, int end) {

		return _courtActivityLocalService.getCourtActivities(start, end);
	}

	/**
	 * Returns all the court activities matching the UUID and company.
	 *
	 * @param uuid the UUID of the court activities
	 * @param companyId the primary key of the company
	 * @return the matching court activities, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<bookings.model.CourtActivity>
		getCourtActivitiesByUuidAndCompanyId(String uuid, long companyId) {

		return _courtActivityLocalService.getCourtActivitiesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of court activities matching the UUID and company.
	 *
	 * @param uuid the UUID of the court activities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching court activities, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<bookings.model.CourtActivity>
		getCourtActivitiesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<bookings.model.CourtActivity> orderByComparator) {

		return _courtActivityLocalService.getCourtActivitiesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of court activities.
	 *
	 * @return the number of court activities
	 */
	@Override
	public int getCourtActivitiesCount() {
		return _courtActivityLocalService.getCourtActivitiesCount();
	}

	/**
	 * Returns the court activity with the primary key.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity
	 * @throws PortalException if a court activity with the primary key could not be found
	 */
	@Override
	public bookings.model.CourtActivity getCourtActivity(long courtActivityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.getCourtActivity(courtActivityId);
	}

	/**
	 * Returns the court activity matching the UUID and group.
	 *
	 * @param uuid the court activity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court activity
	 * @throws PortalException if a matching court activity could not be found
	 */
	@Override
	public bookings.model.CourtActivity getCourtActivityByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.getCourtActivityByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public bookings.model.CourtActivity getCourtActivityFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws bookings.exception.CourtActivityValidateException,
			   javax.portlet.PortletException {

		return _courtActivityLocalService.getCourtActivityFromRequest(
			primaryKey, request);
	}

	@Override
	public java.util.Date getDateTimeFromRequest(
		javax.portlet.PortletRequest request, String prefix) {

		return _courtActivityLocalService.getDateTimeFromRequest(
			request, prefix);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _courtActivityLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _courtActivityLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public bookings.model.CourtActivity getNewObject(long primaryKey) {
		return _courtActivityLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _courtActivityLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the court activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was updated
	 */
	@Override
	public bookings.model.CourtActivity updateCourtActivity(
		bookings.model.CourtActivity courtActivity) {

		return _courtActivityLocalService.updateCourtActivity(courtActivity);
	}

	@Override
	public bookings.model.CourtActivity updateEntry(
			bookings.model.CourtActivity orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtActivityValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _courtActivityLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public CourtActivityLocalService getWrappedService() {
		return _courtActivityLocalService;
	}

	@Override
	public void setWrappedService(
		CourtActivityLocalService courtActivityLocalService) {

		_courtActivityLocalService = courtActivityLocalService;
	}

	private CourtActivityLocalService _courtActivityLocalService;

}