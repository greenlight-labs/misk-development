/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookingLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookingLocalService
 * @generated
 */
public class BookingLocalServiceWrapper
	implements BookingLocalService, ServiceWrapper<BookingLocalService> {

	public BookingLocalServiceWrapper(BookingLocalService bookingLocalService) {
		_bookingLocalService = bookingLocalService;
	}

	/**
	 * Adds the booking to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param booking the booking
	 * @return the booking that was added
	 */
	@Override
	public bookings.model.Booking addBooking(bookings.model.Booking booking) {
		return _bookingLocalService.addBooking(booking);
	}

	@Override
	public bookings.model.Booking addEntry(
			bookings.model.Booking orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.BookingValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public int countAllInAppUser(long appUserId) {
		return _bookingLocalService.countAllInAppUser(appUserId);
	}

	@Override
	public int countAllInCourt(long courtId) {
		return _bookingLocalService.countAllInCourt(courtId);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _bookingLocalService.countAllInGroup(groupId);
	}

	@Override
	public int countByCourt_AppUser(long courtId, long appUserId) {
		return _bookingLocalService.countByCourt_AppUser(courtId, appUserId);
	}

	@Override
	public int countBySync(boolean sync) {
		return _bookingLocalService.countBySync(sync);
	}

	/**
	 * Creates a new booking with the primary key. Does not add the booking to the database.
	 *
	 * @param bookingId the primary key for the new booking
	 * @return the new booking
	 */
	@Override
	public bookings.model.Booking createBooking(long bookingId) {
		return _bookingLocalService.createBooking(bookingId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the booking from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param booking the booking
	 * @return the booking that was removed
	 */
	@Override
	public bookings.model.Booking deleteBooking(
		bookings.model.Booking booking) {

		return _bookingLocalService.deleteBooking(booking);
	}

	/**
	 * Deletes the booking with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking that was removed
	 * @throws PortalException if a booking with the primary key could not be found
	 */
	@Override
	public bookings.model.Booking deleteBooking(long bookingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.deleteBooking(bookingId);
	}

	@Override
	public bookings.model.Booking deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _bookingLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _bookingLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.BookingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _bookingLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.BookingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _bookingLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _bookingLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _bookingLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public bookings.model.Booking fetchBooking(long bookingId) {
		return _bookingLocalService.fetchBooking(bookingId);
	}

	/**
	 * Returns the booking matching the UUID and group.
	 *
	 * @param uuid the booking's UUID
	 * @param groupId the primary key of the group
	 * @return the matching booking, or <code>null</code> if a matching booking could not be found
	 */
	@Override
	public bookings.model.Booking fetchBookingByUuidAndGroupId(
		String uuid, long groupId) {

		return _bookingLocalService.fetchBookingByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInAppUser(
		long appUserId) {

		return _bookingLocalService.findAllInAppUser(appUserId);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInAppUser(
		long appUserId, int start, int end) {

		return _bookingLocalService.findAllInAppUser(appUserId, start, end);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInAppUser(
		long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<bookings.model.Booking>
			obc) {

		return _bookingLocalService.findAllInAppUser(
			appUserId, start, end, obc);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInCourt(long courtId) {
		return _bookingLocalService.findAllInCourt(courtId);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInCourt(
		long courtId, int start, int end) {

		return _bookingLocalService.findAllInCourt(courtId, start, end);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInCourt(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<bookings.model.Booking>
			obc) {

		return _bookingLocalService.findAllInCourt(courtId, start, end, obc);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInGroup(long groupId) {
		return _bookingLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInGroup(
		long groupId, int start, int end) {

		return _bookingLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<bookings.model.Booking> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<bookings.model.Booking>
			obc) {

		return _bookingLocalService.findAllInGroup(groupId, start, end, obc);
	}

	@Override
	public java.util.List<bookings.model.Booking> findBookingsByCourtIdAndDate(
		String courtId, java.util.Date startTime, java.util.Date endTime) {

		return _bookingLocalService.findBookingsByCourtIdAndDate(
			courtId, startTime, endTime);
	}

	@Override
	public java.util.List<bookings.model.Booking> findByCourt_AppUser(
		long courtId, long appUserId) {

		return _bookingLocalService.findByCourt_AppUser(courtId, appUserId);
	}

	@Override
	public java.util.List<bookings.model.Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end) {

		return _bookingLocalService.findByCourt_AppUser(
			courtId, appUserId, start, end);
	}

	@Override
	public java.util.List<bookings.model.Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<bookings.model.Booking>
			obc) {

		return _bookingLocalService.findByCourt_AppUser(
			courtId, appUserId, start, end, obc);
	}

	@Override
	public java.util.List<bookings.model.Booking> findBySync(boolean sync) {
		return _bookingLocalService.findBySync(sync);
	}

	@Override
	public java.util.List<bookings.model.Booking> findBySync(
		boolean sync, int start, int end) {

		return _bookingLocalService.findBySync(sync, start, end);
	}

	@Override
	public java.util.List<bookings.model.Booking> findBySync(
		boolean sync, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<bookings.model.Booking>
			obc) {

		return _bookingLocalService.findBySync(sync, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _bookingLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<bookings.model.Booking> getBookedSlots(
		long courtId, java.util.Date date) {

		return _bookingLocalService.getBookedSlots(courtId, date);
	}

	/**
	 * Returns the booking with the primary key.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking
	 * @throws PortalException if a booking with the primary key could not be found
	 */
	@Override
	public bookings.model.Booking getBooking(long bookingId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.getBooking(bookingId);
	}

	/**
	 * Returns the booking matching the UUID and group.
	 *
	 * @param uuid the booking's UUID
	 * @param groupId the primary key of the group
	 * @return the matching booking
	 * @throws PortalException if a matching booking could not be found
	 */
	@Override
	public bookings.model.Booking getBookingByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.getBookingByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public bookings.model.Booking getBookingFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws bookings.exception.BookingValidateException,
			   javax.portlet.PortletException {

		return _bookingLocalService.getBookingFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of bookings
	 */
	@Override
	public java.util.List<bookings.model.Booking> getBookings(
		int start, int end) {

		return _bookingLocalService.getBookings(start, end);
	}

	/**
	 * Returns all the bookings matching the UUID and company.
	 *
	 * @param uuid the UUID of the bookings
	 * @param companyId the primary key of the company
	 * @return the matching bookings, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<bookings.model.Booking> getBookingsByUuidAndCompanyId(
		String uuid, long companyId) {

		return _bookingLocalService.getBookingsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of bookings matching the UUID and company.
	 *
	 * @param uuid the UUID of the bookings
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching bookings, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<bookings.model.Booking> getBookingsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<bookings.model.Booking>
			orderByComparator) {

		return _bookingLocalService.getBookingsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of bookings.
	 *
	 * @return the number of bookings
	 */
	@Override
	public int getBookingsCount() {
		return _bookingLocalService.getBookingsCount();
	}

	/**
	 * Converte Date Time into Date()
	 *
	 * @param request PortletRequest
	 * @param prefix  Prefix of the parameter
	 * @return Date object
	 */
	@Override
	public java.util.Date getDateTimeFromRequest(
		javax.portlet.PortletRequest request, String prefix) {

		return _bookingLocalService.getDateTimeFromRequest(request, prefix);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _bookingLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _bookingLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public bookings.model.Booking getNewObject(long primaryKey) {
		return _bookingLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _bookingLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public int isSlotBooked(
		long courtId, java.util.Date slotStartTime,
		java.util.Date slotEndTime) {

		return _bookingLocalService.isSlotBooked(
			courtId, slotStartTime, slotEndTime);
	}

	/**
	 * Updates the booking in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param booking the booking
	 * @return the booking that was updated
	 */
	@Override
	public bookings.model.Booking updateBooking(
		bookings.model.Booking booking) {

		return _bookingLocalService.updateBooking(booking);
	}

	@Override
	public bookings.model.Booking updateEntry(
			bookings.model.Booking orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.BookingValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _bookingLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public BookingLocalService getWrappedService() {
		return _bookingLocalService;
	}

	@Override
	public void setWrappedService(BookingLocalService bookingLocalService) {
		_bookingLocalService = bookingLocalService;
	}

	private BookingLocalService _bookingLocalService;

}