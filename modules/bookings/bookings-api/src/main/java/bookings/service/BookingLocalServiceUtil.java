/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service;

import bookings.model.Booking;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Booking. This utility wraps
 * <code>bookings.service.impl.BookingLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see BookingLocalService
 * @generated
 */
public class BookingLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>bookings.service.impl.BookingLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the booking to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param booking the booking
	 * @return the booking that was added
	 */
	public static Booking addBooking(Booking booking) {
		return getService().addBooking(booking);
	}

	public static Booking addEntry(
			Booking orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.BookingValidateException, PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static int countAllInAppUser(long appUserId) {
		return getService().countAllInAppUser(appUserId);
	}

	public static int countAllInCourt(long courtId) {
		return getService().countAllInCourt(courtId);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	public static int countByCourt_AppUser(long courtId, long appUserId) {
		return getService().countByCourt_AppUser(courtId, appUserId);
	}

	public static int countBySync(boolean sync) {
		return getService().countBySync(sync);
	}

	/**
	 * Creates a new booking with the primary key. Does not add the booking to the database.
	 *
	 * @param bookingId the primary key for the new booking
	 * @return the new booking
	 */
	public static Booking createBooking(long bookingId) {
		return getService().createBooking(bookingId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the booking from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param booking the booking
	 * @return the booking that was removed
	 */
	public static Booking deleteBooking(Booking booking) {
		return getService().deleteBooking(booking);
	}

	/**
	 * Deletes the booking with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking that was removed
	 * @throws PortalException if a booking with the primary key could not be found
	 */
	public static Booking deleteBooking(long bookingId) throws PortalException {
		return getService().deleteBooking(bookingId);
	}

	public static Booking deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.BookingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.BookingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Booking fetchBooking(long bookingId) {
		return getService().fetchBooking(bookingId);
	}

	/**
	 * Returns the booking matching the UUID and group.
	 *
	 * @param uuid the booking's UUID
	 * @param groupId the primary key of the group
	 * @return the matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchBookingByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchBookingByUuidAndGroupId(uuid, groupId);
	}

	public static List<Booking> findAllInAppUser(long appUserId) {
		return getService().findAllInAppUser(appUserId);
	}

	public static List<Booking> findAllInAppUser(
		long appUserId, int start, int end) {

		return getService().findAllInAppUser(appUserId, start, end);
	}

	public static List<Booking> findAllInAppUser(
		long appUserId, int start, int end, OrderByComparator<Booking> obc) {

		return getService().findAllInAppUser(appUserId, start, end, obc);
	}

	public static List<Booking> findAllInCourt(long courtId) {
		return getService().findAllInCourt(courtId);
	}

	public static List<Booking> findAllInCourt(
		long courtId, int start, int end) {

		return getService().findAllInCourt(courtId, start, end);
	}

	public static List<Booking> findAllInCourt(
		long courtId, int start, int end, OrderByComparator<Booking> obc) {

		return getService().findAllInCourt(courtId, start, end, obc);
	}

	public static List<Booking> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<Booking> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<Booking> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Booking> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static List<Booking> findBookingsByCourtIdAndDate(
		String courtId, java.util.Date startTime, java.util.Date endTime) {

		return getService().findBookingsByCourtIdAndDate(
			courtId, startTime, endTime);
	}

	public static List<Booking> findByCourt_AppUser(
		long courtId, long appUserId) {

		return getService().findByCourt_AppUser(courtId, appUserId);
	}

	public static List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end) {

		return getService().findByCourt_AppUser(courtId, appUserId, start, end);
	}

	public static List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end,
		OrderByComparator<Booking> obc) {

		return getService().findByCourt_AppUser(
			courtId, appUserId, start, end, obc);
	}

	public static List<Booking> findBySync(boolean sync) {
		return getService().findBySync(sync);
	}

	public static List<Booking> findBySync(boolean sync, int start, int end) {
		return getService().findBySync(sync, start, end);
	}

	public static List<Booking> findBySync(
		boolean sync, int start, int end, OrderByComparator<Booking> obc) {

		return getService().findBySync(sync, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static List<Booking> getBookedSlots(
		long courtId, java.util.Date date) {

		return getService().getBookedSlots(courtId, date);
	}

	/**
	 * Returns the booking with the primary key.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking
	 * @throws PortalException if a booking with the primary key could not be found
	 */
	public static Booking getBooking(long bookingId) throws PortalException {
		return getService().getBooking(bookingId);
	}

	/**
	 * Returns the booking matching the UUID and group.
	 *
	 * @param uuid the booking's UUID
	 * @param groupId the primary key of the group
	 * @return the matching booking
	 * @throws PortalException if a matching booking could not be found
	 */
	public static Booking getBookingByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getBookingByUuidAndGroupId(uuid, groupId);
	}

	public static Booking getBookingFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws bookings.exception.BookingValidateException,
			   javax.portlet.PortletException {

		return getService().getBookingFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of bookings
	 */
	public static List<Booking> getBookings(int start, int end) {
		return getService().getBookings(start, end);
	}

	/**
	 * Returns all the bookings matching the UUID and company.
	 *
	 * @param uuid the UUID of the bookings
	 * @param companyId the primary key of the company
	 * @return the matching bookings, or an empty list if no matches were found
	 */
	public static List<Booking> getBookingsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getBookingsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of bookings matching the UUID and company.
	 *
	 * @param uuid the UUID of the bookings
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching bookings, or an empty list if no matches were found
	 */
	public static List<Booking> getBookingsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getService().getBookingsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of bookings.
	 *
	 * @return the number of bookings
	 */
	public static int getBookingsCount() {
		return getService().getBookingsCount();
	}

	/**
	 * Converte Date Time into Date()
	 *
	 * @param request PortletRequest
	 * @param prefix  Prefix of the parameter
	 * @return Date object
	 */
	public static java.util.Date getDateTimeFromRequest(
		javax.portlet.PortletRequest request, String prefix) {

		return getService().getDateTimeFromRequest(request, prefix);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Booking getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static int isSlotBooked(
		long courtId, java.util.Date slotStartTime,
		java.util.Date slotEndTime) {

		return getService().isSlotBooked(courtId, slotStartTime, slotEndTime);
	}

	/**
	 * Updates the booking in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect BookingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param booking the booking
	 * @return the booking that was updated
	 */
	public static Booking updateBooking(Booking booking) {
		return getService().updateBooking(booking);
	}

	public static Booking updateEntry(
			Booking orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.BookingValidateException, PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static BookingLocalService getService() {
		return _service;
	}

	private static volatile BookingLocalService _service;

}