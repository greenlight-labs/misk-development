/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service;

import bookings.model.CourtActivity;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for CourtActivity. This utility wraps
 * <code>bookings.service.impl.CourtActivityLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see CourtActivityLocalService
 * @generated
 */
public class CourtActivityLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>bookings.service.impl.CourtActivityLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the court activity to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was added
	 */
	public static CourtActivity addCourtActivity(CourtActivity courtActivity) {
		return getService().addCourtActivity(courtActivity);
	}

	public static CourtActivity addEntry(
			CourtActivity orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtActivityValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static int countAllInCourt(long courtId) {
		return getService().countAllInCourt(courtId);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * Creates a new court activity with the primary key. Does not add the court activity to the database.
	 *
	 * @param courtActivityId the primary key for the new court activity
	 * @return the new court activity
	 */
	public static CourtActivity createCourtActivity(long courtActivityId) {
		return getService().createCourtActivity(courtActivityId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the court activity from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was removed
	 */
	public static CourtActivity deleteCourtActivity(
		CourtActivity courtActivity) {

		return getService().deleteCourtActivity(courtActivity);
	}

	/**
	 * Deletes the court activity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity that was removed
	 * @throws PortalException if a court activity with the primary key could not be found
	 */
	public static CourtActivity deleteCourtActivity(long courtActivityId)
		throws PortalException {

		return getService().deleteCourtActivity(courtActivityId);
	}

	public static CourtActivity deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static CourtActivity fetchCourtActivity(long courtActivityId) {
		return getService().fetchCourtActivity(courtActivityId);
	}

	/**
	 * Returns the court activity matching the UUID and group.
	 *
	 * @param uuid the court activity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court activity, or <code>null</code> if a matching court activity could not be found
	 */
	public static CourtActivity fetchCourtActivityByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchCourtActivityByUuidAndGroupId(uuid, groupId);
	}

	public static List<CourtActivity> findAllInCourt(long courtId) {
		return getService().findAllInCourt(courtId);
	}

	public static List<CourtActivity> findAllInCourt(
		long courtId, int start, int end) {

		return getService().findAllInCourt(courtId, start, end);
	}

	public static List<CourtActivity> findAllInCourt(
		long courtId, int start, int end,
		OrderByComparator<CourtActivity> obc) {

		return getService().findAllInCourt(courtId, start, end, obc);
	}

	public static List<CourtActivity> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<CourtActivity> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<CourtActivity> findAllInGroup(
		long groupId, int start, int end,
		OrderByComparator<CourtActivity> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the court activities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtActivityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @return the range of court activities
	 */
	public static List<CourtActivity> getCourtActivities(int start, int end) {
		return getService().getCourtActivities(start, end);
	}

	/**
	 * Returns all the court activities matching the UUID and company.
	 *
	 * @param uuid the UUID of the court activities
	 * @param companyId the primary key of the company
	 * @return the matching court activities, or an empty list if no matches were found
	 */
	public static List<CourtActivity> getCourtActivitiesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getCourtActivitiesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of court activities matching the UUID and company.
	 *
	 * @param uuid the UUID of the court activities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of court activities
	 * @param end the upper bound of the range of court activities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching court activities, or an empty list if no matches were found
	 */
	public static List<CourtActivity> getCourtActivitiesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtActivity> orderByComparator) {

		return getService().getCourtActivitiesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of court activities.
	 *
	 * @return the number of court activities
	 */
	public static int getCourtActivitiesCount() {
		return getService().getCourtActivitiesCount();
	}

	/**
	 * Returns the court activity with the primary key.
	 *
	 * @param courtActivityId the primary key of the court activity
	 * @return the court activity
	 * @throws PortalException if a court activity with the primary key could not be found
	 */
	public static CourtActivity getCourtActivity(long courtActivityId)
		throws PortalException {

		return getService().getCourtActivity(courtActivityId);
	}

	/**
	 * Returns the court activity matching the UUID and group.
	 *
	 * @param uuid the court activity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court activity
	 * @throws PortalException if a matching court activity could not be found
	 */
	public static CourtActivity getCourtActivityByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getCourtActivityByUuidAndGroupId(uuid, groupId);
	}

	public static CourtActivity getCourtActivityFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws bookings.exception.CourtActivityValidateException,
			   javax.portlet.PortletException {

		return getService().getCourtActivityFromRequest(primaryKey, request);
	}

	public static java.util.Date getDateTimeFromRequest(
		javax.portlet.PortletRequest request, String prefix) {

		return getService().getDateTimeFromRequest(request, prefix);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static CourtActivity getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the court activity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtActivityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtActivity the court activity
	 * @return the court activity that was updated
	 */
	public static CourtActivity updateCourtActivity(
		CourtActivity courtActivity) {

		return getService().updateCourtActivity(courtActivity);
	}

	public static CourtActivity updateEntry(
			CourtActivity orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtActivityValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static CourtActivityLocalService getService() {
		return _service;
	}

	private static volatile CourtActivityLocalService _service;

}