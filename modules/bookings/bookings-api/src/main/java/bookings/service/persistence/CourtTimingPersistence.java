/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.persistence;

import bookings.exception.NoSuchCourtTimingException;

import bookings.model.CourtTiming;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the court timing service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtTimingUtil
 * @generated
 */
@ProviderType
public interface CourtTimingPersistence extends BasePersistence<CourtTiming> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourtTimingUtil} to access the court timing persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the court timings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid(String uuid);

	/**
	 * Returns a range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where uuid = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public CourtTiming[] findByUuid_PrevAndNext(
			long courtTimingId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Removes all the court timings where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of court timings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching court timings
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByUUID_G(String uuid, long groupId)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the court timing where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the court timing that was removed
	 */
	public CourtTiming removeByUUID_G(String uuid, long groupId)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the number of court timings where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching court timings
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public CourtTiming[] findByUuid_C_PrevAndNext(
			long courtTimingId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Removes all the court timings where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching court timings
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the court timings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching court timings
	 */
	public java.util.List<CourtTiming> findByGroupId(long groupId);

	/**
	 * Returns a range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public java.util.List<CourtTiming> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the first court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the last court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the last court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where groupId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public CourtTiming[] findByGroupId_PrevAndNext(
			long courtTimingId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Removes all the court timings where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of court timings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching court timings
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the court timings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the matching court timings
	 */
	public java.util.List<CourtTiming> findByCourtId(long courtId);

	/**
	 * Returns a range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public java.util.List<CourtTiming> findByCourtId(
		long courtId, int start, int end);

	/**
	 * Returns an ordered range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByCourtId(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public java.util.List<CourtTiming> findByCourtId(
		long courtId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByCourtId_First(
			long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the first court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByCourtId_First(
		long courtId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the last court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByCourtId_Last(
			long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the last court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByCourtId_Last(
		long courtId,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public CourtTiming[] findByCourtId_PrevAndNext(
			long courtTimingId, long courtId,
			com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
				orderByComparator)
		throws NoSuchCourtTimingException;

	/**
	 * Removes all the court timings where courtId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 */
	public void removeByCourtId(long courtId);

	/**
	 * Returns the number of court timings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the number of matching court timings
	 */
	public int countByCourtId(long courtId);

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public CourtTiming findByCourtDay(long courtId, int day)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByCourtDay(long courtId, int day);

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public CourtTiming fetchByCourtDay(
		long courtId, int day, boolean useFinderCache);

	/**
	 * Removes the court timing where courtId = &#63; and day = &#63; from the database.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the court timing that was removed
	 */
	public CourtTiming removeByCourtDay(long courtId, int day)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the number of court timings where courtId = &#63; and day = &#63;.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the number of matching court timings
	 */
	public int countByCourtDay(long courtId, int day);

	/**
	 * Caches the court timing in the entity cache if it is enabled.
	 *
	 * @param courtTiming the court timing
	 */
	public void cacheResult(CourtTiming courtTiming);

	/**
	 * Caches the court timings in the entity cache if it is enabled.
	 *
	 * @param courtTimings the court timings
	 */
	public void cacheResult(java.util.List<CourtTiming> courtTimings);

	/**
	 * Creates a new court timing with the primary key. Does not add the court timing to the database.
	 *
	 * @param courtTimingId the primary key for the new court timing
	 * @return the new court timing
	 */
	public CourtTiming create(long courtTimingId);

	/**
	 * Removes the court timing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing that was removed
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public CourtTiming remove(long courtTimingId)
		throws NoSuchCourtTimingException;

	public CourtTiming updateImpl(CourtTiming courtTiming);

	/**
	 * Returns the court timing with the primary key or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public CourtTiming findByPrimaryKey(long courtTimingId)
		throws NoSuchCourtTimingException;

	/**
	 * Returns the court timing with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing, or <code>null</code> if a court timing with the primary key could not be found
	 */
	public CourtTiming fetchByPrimaryKey(long courtTimingId);

	/**
	 * Returns all the court timings.
	 *
	 * @return the court timings
	 */
	public java.util.List<CourtTiming> findAll();

	/**
	 * Returns a range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of court timings
	 */
	public java.util.List<CourtTiming> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of court timings
	 */
	public java.util.List<CourtTiming> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator);

	/**
	 * Returns an ordered range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of court timings
	 */
	public java.util.List<CourtTiming> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CourtTiming>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the court timings from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of court timings.
	 *
	 * @return the number of court timings
	 */
	public int countAll();

}