/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.persistence;

import bookings.model.Booking;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the booking service. This utility wraps <code>bookings.service.persistence.impl.BookingPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingPersistence
 * @generated
 */
public class BookingUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Booking booking) {
		getPersistence().clearCache(booking);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Booking> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Booking> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Booking> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Booking> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Booking update(Booking booking) {
		return getPersistence().update(booking);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Booking update(
		Booking booking, ServiceContext serviceContext) {

		return getPersistence().update(booking, serviceContext);
	}

	/**
	 * Returns all the bookings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching bookings
	 */
	public static List<Booking> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the bookings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public static List<Booking> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Booking> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByUuid_First(
			String uuid, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByUuid_First(
		String uuid, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByUuid_Last(
			String uuid, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByUuid_Last(
		String uuid, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the bookings before and after the current booking in the ordered set where uuid = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking[] findByUuid_PrevAndNext(
			long bookingId, String uuid,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByUuid_PrevAndNext(
			bookingId, uuid, orderByComparator);
	}

	/**
	 * Removes all the bookings where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of bookings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching bookings
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the booking where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchBookingException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByUUID_G(String uuid, long groupId)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the booking where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the booking where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the booking where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the booking that was removed
	 */
	public static Booking removeByUUID_G(String uuid, long groupId)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of bookings where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching bookings
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching bookings
	 */
	public static List<Booking> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public static List<Booking> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Booking> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the bookings before and after the current booking in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking[] findByUuid_C_PrevAndNext(
			long bookingId, String uuid, long companyId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByUuid_C_PrevAndNext(
			bookingId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the bookings where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of bookings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching bookings
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the bookings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching bookings
	 */
	public static List<Booking> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the bookings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public static List<Booking> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the bookings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Booking> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByGroupId_First(
			long groupId, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByGroupId_First(
		long groupId, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByGroupId_Last(
			long groupId, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByGroupId_Last(
		long groupId, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the bookings before and after the current booking in the ordered set where groupId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking[] findByGroupId_PrevAndNext(
			long bookingId, long groupId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByGroupId_PrevAndNext(
			bookingId, groupId, orderByComparator);
	}

	/**
	 * Removes all the bookings where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of bookings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching bookings
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the bookings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the matching bookings
	 */
	public static List<Booking> findByCourtId(long courtId) {
		return getPersistence().findByCourtId(courtId);
	}

	/**
	 * Returns a range of all the bookings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public static List<Booking> findByCourtId(
		long courtId, int start, int end) {

		return getPersistence().findByCourtId(courtId, start, end);
	}

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findByCourtId(
			courtId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<Booking> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCourtId(
			courtId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByCourtId_First(
			long courtId, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByCourtId_First(courtId, orderByComparator);
	}

	/**
	 * Returns the first booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByCourtId_First(
		long courtId, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByCourtId_First(
			courtId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByCourtId_Last(
			long courtId, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByCourtId_Last(courtId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByCourtId_Last(
		long courtId, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByCourtId_Last(courtId, orderByComparator);
	}

	/**
	 * Returns the bookings before and after the current booking in the ordered set where courtId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking[] findByCourtId_PrevAndNext(
			long bookingId, long courtId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByCourtId_PrevAndNext(
			bookingId, courtId, orderByComparator);
	}

	/**
	 * Removes all the bookings where courtId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 */
	public static void removeByCourtId(long courtId) {
		getPersistence().removeByCourtId(courtId);
	}

	/**
	 * Returns the number of bookings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the number of matching bookings
	 */
	public static int countByCourtId(long courtId) {
		return getPersistence().countByCourtId(courtId);
	}

	/**
	 * Returns all the bookings where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the matching bookings
	 */
	public static List<Booking> findByAppUserId(long appUserId) {
		return getPersistence().findByAppUserId(appUserId);
	}

	/**
	 * Returns a range of all the bookings where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public static List<Booking> findByAppUserId(
		long appUserId, int start, int end) {

		return getPersistence().findByAppUserId(appUserId, start, end);
	}

	/**
	 * Returns an ordered range of all the bookings where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByAppUserId(
		long appUserId, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findByAppUserId(
			appUserId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings where appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByAppUserId(
		long appUserId, int start, int end,
		OrderByComparator<Booking> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByAppUserId(
			appUserId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByAppUserId_First(
			long appUserId, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByAppUserId_First(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the first booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByAppUserId_First(
		long appUserId, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByAppUserId_First(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByAppUserId_Last(
			long appUserId, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByAppUserId_Last(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByAppUserId_Last(
		long appUserId, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByAppUserId_Last(
			appUserId, orderByComparator);
	}

	/**
	 * Returns the bookings before and after the current booking in the ordered set where appUserId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking[] findByAppUserId_PrevAndNext(
			long bookingId, long appUserId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByAppUserId_PrevAndNext(
			bookingId, appUserId, orderByComparator);
	}

	/**
	 * Removes all the bookings where appUserId = &#63; from the database.
	 *
	 * @param appUserId the app user ID
	 */
	public static void removeByAppUserId(long appUserId) {
		getPersistence().removeByAppUserId(appUserId);
	}

	/**
	 * Returns the number of bookings where appUserId = &#63;.
	 *
	 * @param appUserId the app user ID
	 * @return the number of matching bookings
	 */
	public static int countByAppUserId(long appUserId) {
		return getPersistence().countByAppUserId(appUserId);
	}

	/**
	 * Returns all the bookings where sync = &#63;.
	 *
	 * @param sync the sync
	 * @return the matching bookings
	 */
	public static List<Booking> findBySync(boolean sync) {
		return getPersistence().findBySync(sync);
	}

	/**
	 * Returns a range of all the bookings where sync = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param sync the sync
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public static List<Booking> findBySync(boolean sync, int start, int end) {
		return getPersistence().findBySync(sync, start, end);
	}

	/**
	 * Returns an ordered range of all the bookings where sync = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param sync the sync
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findBySync(
		boolean sync, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findBySync(sync, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings where sync = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param sync the sync
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findBySync(
		boolean sync, int start, int end,
		OrderByComparator<Booking> orderByComparator, boolean useFinderCache) {

		return getPersistence().findBySync(
			sync, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findBySync_First(
			boolean sync, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findBySync_First(sync, orderByComparator);
	}

	/**
	 * Returns the first booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchBySync_First(
		boolean sync, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchBySync_First(sync, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findBySync_Last(
			boolean sync, OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findBySync_Last(sync, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where sync = &#63;.
	 *
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchBySync_Last(
		boolean sync, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchBySync_Last(sync, orderByComparator);
	}

	/**
	 * Returns the bookings before and after the current booking in the ordered set where sync = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param sync the sync
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking[] findBySync_PrevAndNext(
			long bookingId, boolean sync,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findBySync_PrevAndNext(
			bookingId, sync, orderByComparator);
	}

	/**
	 * Removes all the bookings where sync = &#63; from the database.
	 *
	 * @param sync the sync
	 */
	public static void removeBySync(boolean sync) {
		getPersistence().removeBySync(sync);
	}

	/**
	 * Returns the number of bookings where sync = &#63;.
	 *
	 * @param sync the sync
	 * @return the number of matching bookings
	 */
	public static int countBySync(boolean sync) {
		return getPersistence().countBySync(sync);
	}

	/**
	 * Returns all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @return the matching bookings
	 */
	public static List<Booking> findByCourt_AppUser(
		long courtId, long appUserId) {

		return getPersistence().findByCourt_AppUser(courtId, appUserId);
	}

	/**
	 * Returns a range of all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of matching bookings
	 */
	public static List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end) {

		return getPersistence().findByCourt_AppUser(
			courtId, appUserId, start, end);
	}

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findByCourt_AppUser(
			courtId, appUserId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching bookings
	 */
	public static List<Booking> findByCourt_AppUser(
		long courtId, long appUserId, int start, int end,
		OrderByComparator<Booking> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCourt_AppUser(
			courtId, appUserId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByCourt_AppUser_First(
			long courtId, long appUserId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByCourt_AppUser_First(
			courtId, appUserId, orderByComparator);
	}

	/**
	 * Returns the first booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByCourt_AppUser_First(
		long courtId, long appUserId,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByCourt_AppUser_First(
			courtId, appUserId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking
	 * @throws NoSuchBookingException if a matching booking could not be found
	 */
	public static Booking findByCourt_AppUser_Last(
			long courtId, long appUserId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByCourt_AppUser_Last(
			courtId, appUserId, orderByComparator);
	}

	/**
	 * Returns the last booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching booking, or <code>null</code> if a matching booking could not be found
	 */
	public static Booking fetchByCourt_AppUser_Last(
		long courtId, long appUserId,
		OrderByComparator<Booking> orderByComparator) {

		return getPersistence().fetchByCourt_AppUser_Last(
			courtId, appUserId, orderByComparator);
	}

	/**
	 * Returns the bookings before and after the current booking in the ordered set where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param bookingId the primary key of the current booking
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking[] findByCourt_AppUser_PrevAndNext(
			long bookingId, long courtId, long appUserId,
			OrderByComparator<Booking> orderByComparator)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByCourt_AppUser_PrevAndNext(
			bookingId, courtId, appUserId, orderByComparator);
	}

	/**
	 * Removes all the bookings where courtId = &#63; and appUserId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 */
	public static void removeByCourt_AppUser(long courtId, long appUserId) {
		getPersistence().removeByCourt_AppUser(courtId, appUserId);
	}

	/**
	 * Returns the number of bookings where courtId = &#63; and appUserId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param appUserId the app user ID
	 * @return the number of matching bookings
	 */
	public static int countByCourt_AppUser(long courtId, long appUserId) {
		return getPersistence().countByCourt_AppUser(courtId, appUserId);
	}

	/**
	 * Caches the booking in the entity cache if it is enabled.
	 *
	 * @param booking the booking
	 */
	public static void cacheResult(Booking booking) {
		getPersistence().cacheResult(booking);
	}

	/**
	 * Caches the bookings in the entity cache if it is enabled.
	 *
	 * @param bookings the bookings
	 */
	public static void cacheResult(List<Booking> bookings) {
		getPersistence().cacheResult(bookings);
	}

	/**
	 * Creates a new booking with the primary key. Does not add the booking to the database.
	 *
	 * @param bookingId the primary key for the new booking
	 * @return the new booking
	 */
	public static Booking create(long bookingId) {
		return getPersistence().create(bookingId);
	}

	/**
	 * Removes the booking with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking that was removed
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking remove(long bookingId)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().remove(bookingId);
	}

	public static Booking updateImpl(Booking booking) {
		return getPersistence().updateImpl(booking);
	}

	/**
	 * Returns the booking with the primary key or throws a <code>NoSuchBookingException</code> if it could not be found.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking
	 * @throws NoSuchBookingException if a booking with the primary key could not be found
	 */
	public static Booking findByPrimaryKey(long bookingId)
		throws bookings.exception.NoSuchBookingException {

		return getPersistence().findByPrimaryKey(bookingId);
	}

	/**
	 * Returns the booking with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param bookingId the primary key of the booking
	 * @return the booking, or <code>null</code> if a booking with the primary key could not be found
	 */
	public static Booking fetchByPrimaryKey(long bookingId) {
		return getPersistence().fetchByPrimaryKey(bookingId);
	}

	/**
	 * Returns all the bookings.
	 *
	 * @return the bookings
	 */
	public static List<Booking> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @return the range of bookings
	 */
	public static List<Booking> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of bookings
	 */
	public static List<Booking> findAll(
		int start, int end, OrderByComparator<Booking> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the bookings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>BookingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of bookings
	 * @param end the upper bound of the range of bookings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of bookings
	 */
	public static List<Booking> findAll(
		int start, int end, OrderByComparator<Booking> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the bookings from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of bookings.
	 *
	 * @return the number of bookings
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static BookingPersistence getPersistence() {
		return _persistence;
	}

	private static volatile BookingPersistence _persistence;

}