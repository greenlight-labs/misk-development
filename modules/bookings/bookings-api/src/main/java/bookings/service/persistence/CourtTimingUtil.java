/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.persistence;

import bookings.model.CourtTiming;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the court timing service. This utility wraps <code>bookings.service.persistence.impl.CourtTimingPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtTimingPersistence
 * @generated
 */
public class CourtTimingUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(CourtTiming courtTiming) {
		getPersistence().clearCache(courtTiming);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, CourtTiming> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CourtTiming> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CourtTiming> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CourtTiming> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static CourtTiming update(CourtTiming courtTiming) {
		return getPersistence().update(courtTiming);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static CourtTiming update(
		CourtTiming courtTiming, ServiceContext serviceContext) {

		return getPersistence().update(courtTiming, serviceContext);
	}

	/**
	 * Returns all the court timings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching court timings
	 */
	public static List<CourtTiming> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public static List<CourtTiming> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByUuid_First(
			String uuid, OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByUuid_First(
		String uuid, OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByUuid_Last(
			String uuid, OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByUuid_Last(
		String uuid, OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where uuid = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public static CourtTiming[] findByUuid_PrevAndNext(
			long courtTimingId, String uuid,
			OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByUuid_PrevAndNext(
			courtTimingId, uuid, orderByComparator);
	}

	/**
	 * Removes all the court timings where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of court timings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching court timings
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByUUID_G(String uuid, long groupId)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the court timing where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the court timing that was removed
	 */
	public static CourtTiming removeByUUID_G(String uuid, long groupId)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of court timings where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching court timings
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching court timings
	 */
	public static List<CourtTiming> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public static List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public static CourtTiming[] findByUuid_C_PrevAndNext(
			long courtTimingId, String uuid, long companyId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByUuid_C_PrevAndNext(
			courtTimingId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the court timings where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching court timings
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the court timings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching court timings
	 */
	public static List<CourtTiming> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public static List<CourtTiming> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByGroupId_First(
			long groupId, OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByGroupId_First(
		long groupId, OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByGroupId_Last(
			long groupId, OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByGroupId_Last(
		long groupId, OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where groupId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public static CourtTiming[] findByGroupId_PrevAndNext(
			long courtTimingId, long groupId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByGroupId_PrevAndNext(
			courtTimingId, groupId, orderByComparator);
	}

	/**
	 * Removes all the court timings where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of court timings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching court timings
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the court timings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the matching court timings
	 */
	public static List<CourtTiming> findByCourtId(long courtId) {
		return getPersistence().findByCourtId(courtId);
	}

	/**
	 * Returns a range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	public static List<CourtTiming> findByCourtId(
		long courtId, int start, int end) {

		return getPersistence().findByCourtId(courtId, start, end);
	}

	/**
	 * Returns an ordered range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().findByCourtId(
			courtId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	public static List<CourtTiming> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByCourtId(
			courtId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByCourtId_First(
			long courtId, OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByCourtId_First(courtId, orderByComparator);
	}

	/**
	 * Returns the first court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByCourtId_First(
		long courtId, OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByCourtId_First(
			courtId, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByCourtId_Last(
			long courtId, OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByCourtId_Last(courtId, orderByComparator);
	}

	/**
	 * Returns the last court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByCourtId_Last(
		long courtId, OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().fetchByCourtId_Last(courtId, orderByComparator);
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public static CourtTiming[] findByCourtId_PrevAndNext(
			long courtTimingId, long courtId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByCourtId_PrevAndNext(
			courtTimingId, courtId, orderByComparator);
	}

	/**
	 * Removes all the court timings where courtId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 */
	public static void removeByCourtId(long courtId) {
		getPersistence().removeByCourtId(courtId);
	}

	/**
	 * Returns the number of court timings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the number of matching court timings
	 */
	public static int countByCourtId(long courtId) {
		return getPersistence().countByCourtId(courtId);
	}

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	public static CourtTiming findByCourtDay(long courtId, int day)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByCourtDay(courtId, day);
	}

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByCourtDay(long courtId, int day) {
		return getPersistence().fetchByCourtDay(courtId, day);
	}

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchByCourtDay(
		long courtId, int day, boolean useFinderCache) {

		return getPersistence().fetchByCourtDay(courtId, day, useFinderCache);
	}

	/**
	 * Removes the court timing where courtId = &#63; and day = &#63; from the database.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the court timing that was removed
	 */
	public static CourtTiming removeByCourtDay(long courtId, int day)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().removeByCourtDay(courtId, day);
	}

	/**
	 * Returns the number of court timings where courtId = &#63; and day = &#63;.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the number of matching court timings
	 */
	public static int countByCourtDay(long courtId, int day) {
		return getPersistence().countByCourtDay(courtId, day);
	}

	/**
	 * Caches the court timing in the entity cache if it is enabled.
	 *
	 * @param courtTiming the court timing
	 */
	public static void cacheResult(CourtTiming courtTiming) {
		getPersistence().cacheResult(courtTiming);
	}

	/**
	 * Caches the court timings in the entity cache if it is enabled.
	 *
	 * @param courtTimings the court timings
	 */
	public static void cacheResult(List<CourtTiming> courtTimings) {
		getPersistence().cacheResult(courtTimings);
	}

	/**
	 * Creates a new court timing with the primary key. Does not add the court timing to the database.
	 *
	 * @param courtTimingId the primary key for the new court timing
	 * @return the new court timing
	 */
	public static CourtTiming create(long courtTimingId) {
		return getPersistence().create(courtTimingId);
	}

	/**
	 * Removes the court timing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing that was removed
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public static CourtTiming remove(long courtTimingId)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().remove(courtTimingId);
	}

	public static CourtTiming updateImpl(CourtTiming courtTiming) {
		return getPersistence().updateImpl(courtTiming);
	}

	/**
	 * Returns the court timing with the primary key or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	public static CourtTiming findByPrimaryKey(long courtTimingId)
		throws bookings.exception.NoSuchCourtTimingException {

		return getPersistence().findByPrimaryKey(courtTimingId);
	}

	/**
	 * Returns the court timing with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing, or <code>null</code> if a court timing with the primary key could not be found
	 */
	public static CourtTiming fetchByPrimaryKey(long courtTimingId) {
		return getPersistence().fetchByPrimaryKey(courtTimingId);
	}

	/**
	 * Returns all the court timings.
	 *
	 * @return the court timings
	 */
	public static List<CourtTiming> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of court timings
	 */
	public static List<CourtTiming> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of court timings
	 */
	public static List<CourtTiming> findAll(
		int start, int end, OrderByComparator<CourtTiming> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of court timings
	 */
	public static List<CourtTiming> findAll(
		int start, int end, OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the court timings from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of court timings.
	 *
	 * @return the number of court timings
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static CourtTimingPersistence getPersistence() {
		return _persistence;
	}

	private static volatile CourtTimingPersistence _persistence;

}