/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service;

import bookings.model.CourtTiming;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for CourtTiming. This utility wraps
 * <code>bookings.service.impl.CourtTimingLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see CourtTimingLocalService
 * @generated
 */
public class CourtTimingLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>bookings.service.impl.CourtTimingLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the court timing to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTiming the court timing
	 * @return the court timing that was added
	 */
	public static CourtTiming addCourtTiming(CourtTiming courtTiming) {
		return getService().addCourtTiming(courtTiming);
	}

	public static CourtTiming addEntry(
			CourtTiming orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtTimingValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static int countAllInCourt(long courtId) {
		return getService().countAllInCourt(courtId);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	/**
	 * Creates a new court timing with the primary key. Does not add the court timing to the database.
	 *
	 * @param courtTimingId the primary key for the new court timing
	 * @return the new court timing
	 */
	public static CourtTiming createCourtTiming(long courtTimingId) {
		return getService().createCourtTiming(courtTimingId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the court timing from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTiming the court timing
	 * @return the court timing that was removed
	 */
	public static CourtTiming deleteCourtTiming(CourtTiming courtTiming) {
		return getService().deleteCourtTiming(courtTiming);
	}

	/**
	 * Deletes the court timing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing that was removed
	 * @throws PortalException if a court timing with the primary key could not be found
	 */
	public static CourtTiming deleteCourtTiming(long courtTimingId)
		throws PortalException {

		return getService().deleteCourtTiming(courtTimingId);
	}

	public static CourtTiming deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static CourtTiming fetchByCourtDay(long courtId, int day) {
		return getService().fetchByCourtDay(courtId, day);
	}

	public static CourtTiming fetchCourtTiming(long courtTimingId) {
		return getService().fetchCourtTiming(courtTimingId);
	}

	/**
	 * Returns the court timing matching the UUID and group.
	 *
	 * @param uuid the court timing's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	public static CourtTiming fetchCourtTimingByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchCourtTimingByUuidAndGroupId(uuid, groupId);
	}

	public static List<CourtTiming> findAllInCourt(long courtId) {
		return getService().findAllInCourt(courtId);
	}

	public static List<CourtTiming> findAllInCourt(
		long courtId, int start, int end) {

		return getService().findAllInCourt(courtId, start, end);
	}

	public static List<CourtTiming> findAllInCourt(
		long courtId, int start, int end, OrderByComparator<CourtTiming> obc) {

		return getService().findAllInCourt(courtId, start, end, obc);
	}

	public static List<CourtTiming> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<CourtTiming> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<CourtTiming> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<CourtTiming> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the court timing with the primary key.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing
	 * @throws PortalException if a court timing with the primary key could not be found
	 */
	public static CourtTiming getCourtTiming(long courtTimingId)
		throws PortalException {

		return getService().getCourtTiming(courtTimingId);
	}

	/**
	 * Returns the court timing matching the UUID and group.
	 *
	 * @param uuid the court timing's UUID
	 * @param groupId the primary key of the group
	 * @return the matching court timing
	 * @throws PortalException if a matching court timing could not be found
	 */
	public static CourtTiming getCourtTimingByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getCourtTimingByUuidAndGroupId(uuid, groupId);
	}

	public static CourtTiming getCourtTimingFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws bookings.exception.CourtTimingValidateException,
			   javax.portlet.PortletException {

		return getService().getCourtTimingFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>bookings.model.impl.CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of court timings
	 */
	public static List<CourtTiming> getCourtTimings(int start, int end) {
		return getService().getCourtTimings(start, end);
	}

	/**
	 * Returns all the court timings matching the UUID and company.
	 *
	 * @param uuid the UUID of the court timings
	 * @param companyId the primary key of the company
	 * @return the matching court timings, or an empty list if no matches were found
	 */
	public static List<CourtTiming> getCourtTimingsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getCourtTimingsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of court timings matching the UUID and company.
	 *
	 * @param uuid the UUID of the court timings
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching court timings, or an empty list if no matches were found
	 */
	public static List<CourtTiming> getCourtTimingsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return getService().getCourtTimingsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of court timings.
	 *
	 * @return the number of court timings
	 */
	public static int getCourtTimingsCount() {
		return getService().getCourtTimingsCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static CourtTiming getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the court timing in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect CourtTimingLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param courtTiming the court timing
	 * @return the court timing that was updated
	 */
	public static CourtTiming updateCourtTiming(CourtTiming courtTiming) {
		return getService().updateCourtTiming(courtTiming);
	}

	public static CourtTiming updateEntry(
			CourtTiming orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws bookings.exception.CourtTimingValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static CourtTimingLocalService getService() {
		return _service;
	}

	private static volatile CourtTimingLocalService _service;

}