/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CourtActivity}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtActivity
 * @generated
 */
public class CourtActivityWrapper
	extends BaseModelWrapper<CourtActivity>
	implements CourtActivity, ModelWrapper<CourtActivity> {

	public CourtActivityWrapper(CourtActivity courtActivity) {
		super(courtActivity);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("courtActivityId", getCourtActivityId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courtId", getCourtId());
		attributes.put("startTime", getStartTime());
		attributes.put("endTime", getEndTime());
		attributes.put("allDay", isAllDay());
		attributes.put("status", isStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long courtActivityId = (Long)attributes.get("courtActivityId");

		if (courtActivityId != null) {
			setCourtActivityId(courtActivityId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long courtId = (Long)attributes.get("courtId");

		if (courtId != null) {
			setCourtId(courtId);
		}

		Date startTime = (Date)attributes.get("startTime");

		if (startTime != null) {
			setStartTime(startTime);
		}

		Date endTime = (Date)attributes.get("endTime");

		if (endTime != null) {
			setEndTime(endTime);
		}

		Boolean allDay = (Boolean)attributes.get("allDay");

		if (allDay != null) {
			setAllDay(allDay);
		}

		Boolean status = (Boolean)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}
	}

	/**
	 * Returns the all day of this court activity.
	 *
	 * @return the all day of this court activity
	 */
	@Override
	public boolean getAllDay() {
		return model.getAllDay();
	}

	/**
	 * Returns the company ID of this court activity.
	 *
	 * @return the company ID of this court activity
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the court activity ID of this court activity.
	 *
	 * @return the court activity ID of this court activity
	 */
	@Override
	public long getCourtActivityId() {
		return model.getCourtActivityId();
	}

	/**
	 * Returns the court ID of this court activity.
	 *
	 * @return the court ID of this court activity
	 */
	@Override
	public long getCourtId() {
		return model.getCourtId();
	}

	/**
	 * Returns the create date of this court activity.
	 *
	 * @return the create date of this court activity
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the end time of this court activity.
	 *
	 * @return the end time of this court activity
	 */
	@Override
	public Date getEndTime() {
		return model.getEndTime();
	}

	/**
	 * Returns the group ID of this court activity.
	 *
	 * @return the group ID of this court activity
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this court activity.
	 *
	 * @return the modified date of this court activity
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this court activity.
	 *
	 * @return the primary key of this court activity
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the start time of this court activity.
	 *
	 * @return the start time of this court activity
	 */
	@Override
	public Date getStartTime() {
		return model.getStartTime();
	}

	/**
	 * Returns the status of this court activity.
	 *
	 * @return the status of this court activity
	 */
	@Override
	public boolean getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the user ID of this court activity.
	 *
	 * @return the user ID of this court activity
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this court activity.
	 *
	 * @return the user name of this court activity
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this court activity.
	 *
	 * @return the user uuid of this court activity
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this court activity.
	 *
	 * @return the uuid of this court activity
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this court activity is all day.
	 *
	 * @return <code>true</code> if this court activity is all day; <code>false</code> otherwise
	 */
	@Override
	public boolean isAllDay() {
		return model.isAllDay();
	}

	/**
	 * Returns <code>true</code> if this court activity is status.
	 *
	 * @return <code>true</code> if this court activity is status; <code>false</code> otherwise
	 */
	@Override
	public boolean isStatus() {
		return model.isStatus();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets whether this court activity is all day.
	 *
	 * @param allDay the all day of this court activity
	 */
	@Override
	public void setAllDay(boolean allDay) {
		model.setAllDay(allDay);
	}

	/**
	 * Sets the company ID of this court activity.
	 *
	 * @param companyId the company ID of this court activity
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the court activity ID of this court activity.
	 *
	 * @param courtActivityId the court activity ID of this court activity
	 */
	@Override
	public void setCourtActivityId(long courtActivityId) {
		model.setCourtActivityId(courtActivityId);
	}

	/**
	 * Sets the court ID of this court activity.
	 *
	 * @param courtId the court ID of this court activity
	 */
	@Override
	public void setCourtId(long courtId) {
		model.setCourtId(courtId);
	}

	/**
	 * Sets the create date of this court activity.
	 *
	 * @param createDate the create date of this court activity
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the end time of this court activity.
	 *
	 * @param endTime the end time of this court activity
	 */
	@Override
	public void setEndTime(Date endTime) {
		model.setEndTime(endTime);
	}

	/**
	 * Sets the group ID of this court activity.
	 *
	 * @param groupId the group ID of this court activity
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this court activity.
	 *
	 * @param modifiedDate the modified date of this court activity
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this court activity.
	 *
	 * @param primaryKey the primary key of this court activity
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the start time of this court activity.
	 *
	 * @param startTime the start time of this court activity
	 */
	@Override
	public void setStartTime(Date startTime) {
		model.setStartTime(startTime);
	}

	/**
	 * Sets whether this court activity is status.
	 *
	 * @param status the status of this court activity
	 */
	@Override
	public void setStatus(boolean status) {
		model.setStatus(status);
	}

	/**
	 * Sets the user ID of this court activity.
	 *
	 * @param userId the user ID of this court activity
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this court activity.
	 *
	 * @param userName the user name of this court activity
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this court activity.
	 *
	 * @param userUuid the user uuid of this court activity
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this court activity.
	 *
	 * @param uuid the uuid of this court activity
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected CourtActivityWrapper wrap(CourtActivity courtActivity) {
		return new CourtActivityWrapper(courtActivity);
	}

}