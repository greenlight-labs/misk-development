/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link bookings.service.http.CourtTimingServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class CourtTimingSoap implements Serializable {

	public static CourtTimingSoap toSoapModel(CourtTiming model) {
		CourtTimingSoap soapModel = new CourtTimingSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setCourtTimingId(model.getCourtTimingId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCourtId(model.getCourtId());
		soapModel.setDay(model.getDay());
		soapModel.setOpenTime(model.getOpenTime());
		soapModel.setCloseTime(model.getCloseTime());
		soapModel.setSlotDuration(model.getSlotDuration());
		soapModel.setBreakTime(model.getBreakTime());
		soapModel.setClosedDay(model.isClosedDay());

		return soapModel;
	}

	public static CourtTimingSoap[] toSoapModels(CourtTiming[] models) {
		CourtTimingSoap[] soapModels = new CourtTimingSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CourtTimingSoap[][] toSoapModels(CourtTiming[][] models) {
		CourtTimingSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CourtTimingSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CourtTimingSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CourtTimingSoap[] toSoapModels(List<CourtTiming> models) {
		List<CourtTimingSoap> soapModels = new ArrayList<CourtTimingSoap>(
			models.size());

		for (CourtTiming model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CourtTimingSoap[soapModels.size()]);
	}

	public CourtTimingSoap() {
	}

	public long getPrimaryKey() {
		return _courtTimingId;
	}

	public void setPrimaryKey(long pk) {
		setCourtTimingId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getCourtTimingId() {
		return _courtTimingId;
	}

	public void setCourtTimingId(long courtTimingId) {
		_courtTimingId = courtTimingId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCourtId() {
		return _courtId;
	}

	public void setCourtId(long courtId) {
		_courtId = courtId;
	}

	public int getDay() {
		return _day;
	}

	public void setDay(int day) {
		_day = day;
	}

	public String getOpenTime() {
		return _openTime;
	}

	public void setOpenTime(String openTime) {
		_openTime = openTime;
	}

	public String getCloseTime() {
		return _closeTime;
	}

	public void setCloseTime(String closeTime) {
		_closeTime = closeTime;
	}

	public int getSlotDuration() {
		return _slotDuration;
	}

	public void setSlotDuration(int slotDuration) {
		_slotDuration = slotDuration;
	}

	public int getBreakTime() {
		return _breakTime;
	}

	public void setBreakTime(int breakTime) {
		_breakTime = breakTime;
	}

	public boolean getClosedDay() {
		return _closedDay;
	}

	public boolean isClosedDay() {
		return _closedDay;
	}

	public void setClosedDay(boolean closedDay) {
		_closedDay = closedDay;
	}

	private String _uuid;
	private long _courtTimingId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _courtId;
	private int _day;
	private String _openTime;
	private String _closeTime;
	private int _slotDuration;
	private int _breakTime;
	private boolean _closedDay;

}