/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link bookings.service.http.CourtActivityServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class CourtActivitySoap implements Serializable {

	public static CourtActivitySoap toSoapModel(CourtActivity model) {
		CourtActivitySoap soapModel = new CourtActivitySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setCourtActivityId(model.getCourtActivityId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCourtId(model.getCourtId());
		soapModel.setStartTime(model.getStartTime());
		soapModel.setEndTime(model.getEndTime());
		soapModel.setAllDay(model.isAllDay());
		soapModel.setStatus(model.isStatus());

		return soapModel;
	}

	public static CourtActivitySoap[] toSoapModels(CourtActivity[] models) {
		CourtActivitySoap[] soapModels = new CourtActivitySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CourtActivitySoap[][] toSoapModels(CourtActivity[][] models) {
		CourtActivitySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CourtActivitySoap[models.length][models[0].length];
		}
		else {
			soapModels = new CourtActivitySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CourtActivitySoap[] toSoapModels(List<CourtActivity> models) {
		List<CourtActivitySoap> soapModels = new ArrayList<CourtActivitySoap>(
			models.size());

		for (CourtActivity model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CourtActivitySoap[soapModels.size()]);
	}

	public CourtActivitySoap() {
	}

	public long getPrimaryKey() {
		return _courtActivityId;
	}

	public void setPrimaryKey(long pk) {
		setCourtActivityId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getCourtActivityId() {
		return _courtActivityId;
	}

	public void setCourtActivityId(long courtActivityId) {
		_courtActivityId = courtActivityId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCourtId() {
		return _courtId;
	}

	public void setCourtId(long courtId) {
		_courtId = courtId;
	}

	public Date getStartTime() {
		return _startTime;
	}

	public void setStartTime(Date startTime) {
		_startTime = startTime;
	}

	public Date getEndTime() {
		return _endTime;
	}

	public void setEndTime(Date endTime) {
		_endTime = endTime;
	}

	public boolean getAllDay() {
		return _allDay;
	}

	public boolean isAllDay() {
		return _allDay;
	}

	public void setAllDay(boolean allDay) {
		_allDay = allDay;
	}

	public boolean getStatus() {
		return _status;
	}

	public boolean isStatus() {
		return _status;
	}

	public void setStatus(boolean status) {
		_status = status;
	}

	private String _uuid;
	private long _courtActivityId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _courtId;
	private Date _startTime;
	private Date _endTime;
	private boolean _allDay;
	private boolean _status;

}