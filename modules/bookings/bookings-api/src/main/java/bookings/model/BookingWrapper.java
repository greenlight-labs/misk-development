/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Booking}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Booking
 * @generated
 */
public class BookingWrapper
	extends BaseModelWrapper<Booking>
	implements Booking, ModelWrapper<Booking> {

	public BookingWrapper(Booking booking) {
		super(booking);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("bookingId", getBookingId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courtId", getCourtId());
		attributes.put("appUserId", getAppUserId());
		attributes.put("slotStartTime", getSlotStartTime());
		attributes.put("slotEndTime", getSlotEndTime());
		attributes.put("status", isStatus());
		attributes.put("sync", isSync());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long bookingId = (Long)attributes.get("bookingId");

		if (bookingId != null) {
			setBookingId(bookingId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long courtId = (Long)attributes.get("courtId");

		if (courtId != null) {
			setCourtId(courtId);
		}

		Long appUserId = (Long)attributes.get("appUserId");

		if (appUserId != null) {
			setAppUserId(appUserId);
		}

		Date slotStartTime = (Date)attributes.get("slotStartTime");

		if (slotStartTime != null) {
			setSlotStartTime(slotStartTime);
		}

		Date slotEndTime = (Date)attributes.get("slotEndTime");

		if (slotEndTime != null) {
			setSlotEndTime(slotEndTime);
		}

		Boolean status = (Boolean)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Boolean sync = (Boolean)attributes.get("sync");

		if (sync != null) {
			setSync(sync);
		}
	}

	/**
	 * Returns the app user ID of this booking.
	 *
	 * @return the app user ID of this booking
	 */
	@Override
	public long getAppUserId() {
		return model.getAppUserId();
	}

	/**
	 * Returns the app user uuid of this booking.
	 *
	 * @return the app user uuid of this booking
	 */
	@Override
	public String getAppUserUuid() {
		return model.getAppUserUuid();
	}

	/**
	 * Returns the booking ID of this booking.
	 *
	 * @return the booking ID of this booking
	 */
	@Override
	public long getBookingId() {
		return model.getBookingId();
	}

	/**
	 * Returns the company ID of this booking.
	 *
	 * @return the company ID of this booking
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the court ID of this booking.
	 *
	 * @return the court ID of this booking
	 */
	@Override
	public long getCourtId() {
		return model.getCourtId();
	}

	/**
	 * Returns the create date of this booking.
	 *
	 * @return the create date of this booking
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the group ID of this booking.
	 *
	 * @return the group ID of this booking
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this booking.
	 *
	 * @return the modified date of this booking
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this booking.
	 *
	 * @return the primary key of this booking
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the slot end time of this booking.
	 *
	 * @return the slot end time of this booking
	 */
	@Override
	public Date getSlotEndTime() {
		return model.getSlotEndTime();
	}

	/**
	 * Returns the slot start time of this booking.
	 *
	 * @return the slot start time of this booking
	 */
	@Override
	public Date getSlotStartTime() {
		return model.getSlotStartTime();
	}

	/**
	 * Returns the status of this booking.
	 *
	 * @return the status of this booking
	 */
	@Override
	public boolean getStatus() {
		return model.getStatus();
	}

	/**
	 * Returns the sync of this booking.
	 *
	 * @return the sync of this booking
	 */
	@Override
	public boolean getSync() {
		return model.getSync();
	}

	/**
	 * Returns the user ID of this booking.
	 *
	 * @return the user ID of this booking
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this booking.
	 *
	 * @return the user name of this booking
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this booking.
	 *
	 * @return the user uuid of this booking
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this booking.
	 *
	 * @return the uuid of this booking
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this booking is status.
	 *
	 * @return <code>true</code> if this booking is status; <code>false</code> otherwise
	 */
	@Override
	public boolean isStatus() {
		return model.isStatus();
	}

	/**
	 * Returns <code>true</code> if this booking is sync.
	 *
	 * @return <code>true</code> if this booking is sync; <code>false</code> otherwise
	 */
	@Override
	public boolean isSync() {
		return model.isSync();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the app user ID of this booking.
	 *
	 * @param appUserId the app user ID of this booking
	 */
	@Override
	public void setAppUserId(long appUserId) {
		model.setAppUserId(appUserId);
	}

	/**
	 * Sets the app user uuid of this booking.
	 *
	 * @param appUserUuid the app user uuid of this booking
	 */
	@Override
	public void setAppUserUuid(String appUserUuid) {
		model.setAppUserUuid(appUserUuid);
	}

	/**
	 * Sets the booking ID of this booking.
	 *
	 * @param bookingId the booking ID of this booking
	 */
	@Override
	public void setBookingId(long bookingId) {
		model.setBookingId(bookingId);
	}

	/**
	 * Sets the company ID of this booking.
	 *
	 * @param companyId the company ID of this booking
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the court ID of this booking.
	 *
	 * @param courtId the court ID of this booking
	 */
	@Override
	public void setCourtId(long courtId) {
		model.setCourtId(courtId);
	}

	/**
	 * Sets the create date of this booking.
	 *
	 * @param createDate the create date of this booking
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this booking.
	 *
	 * @param groupId the group ID of this booking
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this booking.
	 *
	 * @param modifiedDate the modified date of this booking
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this booking.
	 *
	 * @param primaryKey the primary key of this booking
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the slot end time of this booking.
	 *
	 * @param slotEndTime the slot end time of this booking
	 */
	@Override
	public void setSlotEndTime(Date slotEndTime) {
		model.setSlotEndTime(slotEndTime);
	}

	/**
	 * Sets the slot start time of this booking.
	 *
	 * @param slotStartTime the slot start time of this booking
	 */
	@Override
	public void setSlotStartTime(Date slotStartTime) {
		model.setSlotStartTime(slotStartTime);
	}

	/**
	 * Sets whether this booking is status.
	 *
	 * @param status the status of this booking
	 */
	@Override
	public void setStatus(boolean status) {
		model.setStatus(status);
	}

	/**
	 * Sets whether this booking is sync.
	 *
	 * @param sync the sync of this booking
	 */
	@Override
	public void setSync(boolean sync) {
		model.setSync(sync);
	}

	/**
	 * Sets the user ID of this booking.
	 *
	 * @param userId the user ID of this booking
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this booking.
	 *
	 * @param userName the user name of this booking
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this booking.
	 *
	 * @param userUuid the user uuid of this booking
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this booking.
	 *
	 * @param uuid the uuid of this booking
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected BookingWrapper wrap(Booking booking) {
		return new BookingWrapper(booking);
	}

}