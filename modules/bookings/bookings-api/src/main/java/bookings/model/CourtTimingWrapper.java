/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CourtTiming}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtTiming
 * @generated
 */
public class CourtTimingWrapper
	extends BaseModelWrapper<CourtTiming>
	implements CourtTiming, ModelWrapper<CourtTiming> {

	public CourtTimingWrapper(CourtTiming courtTiming) {
		super(courtTiming);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("courtTimingId", getCourtTimingId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("courtId", getCourtId());
		attributes.put("day", getDay());
		attributes.put("openTime", getOpenTime());
		attributes.put("closeTime", getCloseTime());
		attributes.put("slotDuration", getSlotDuration());
		attributes.put("breakTime", getBreakTime());
		attributes.put("closedDay", isClosedDay());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long courtTimingId = (Long)attributes.get("courtTimingId");

		if (courtTimingId != null) {
			setCourtTimingId(courtTimingId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long courtId = (Long)attributes.get("courtId");

		if (courtId != null) {
			setCourtId(courtId);
		}

		Integer day = (Integer)attributes.get("day");

		if (day != null) {
			setDay(day);
		}

		String openTime = (String)attributes.get("openTime");

		if (openTime != null) {
			setOpenTime(openTime);
		}

		String closeTime = (String)attributes.get("closeTime");

		if (closeTime != null) {
			setCloseTime(closeTime);
		}

		Integer slotDuration = (Integer)attributes.get("slotDuration");

		if (slotDuration != null) {
			setSlotDuration(slotDuration);
		}

		Integer breakTime = (Integer)attributes.get("breakTime");

		if (breakTime != null) {
			setBreakTime(breakTime);
		}

		Boolean closedDay = (Boolean)attributes.get("closedDay");

		if (closedDay != null) {
			setClosedDay(closedDay);
		}
	}

	/**
	 * Returns the break time of this court timing.
	 *
	 * @return the break time of this court timing
	 */
	@Override
	public int getBreakTime() {
		return model.getBreakTime();
	}

	/**
	 * Returns the closed day of this court timing.
	 *
	 * @return the closed day of this court timing
	 */
	@Override
	public boolean getClosedDay() {
		return model.getClosedDay();
	}

	/**
	 * Returns the close time of this court timing.
	 *
	 * @return the close time of this court timing
	 */
	@Override
	public String getCloseTime() {
		return model.getCloseTime();
	}

	/**
	 * Returns the company ID of this court timing.
	 *
	 * @return the company ID of this court timing
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the court ID of this court timing.
	 *
	 * @return the court ID of this court timing
	 */
	@Override
	public long getCourtId() {
		return model.getCourtId();
	}

	/**
	 * Returns the court timing ID of this court timing.
	 *
	 * @return the court timing ID of this court timing
	 */
	@Override
	public long getCourtTimingId() {
		return model.getCourtTimingId();
	}

	/**
	 * Returns the create date of this court timing.
	 *
	 * @return the create date of this court timing
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	/**
	 * Returns the day of this court timing.
	 *
	 * @return the day of this court timing
	 */
	@Override
	public int getDay() {
		return model.getDay();
	}

	/**
	 * Returns the group ID of this court timing.
	 *
	 * @return the group ID of this court timing
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the modified date of this court timing.
	 *
	 * @return the modified date of this court timing
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the open time of this court timing.
	 *
	 * @return the open time of this court timing
	 */
	@Override
	public String getOpenTime() {
		return model.getOpenTime();
	}

	/**
	 * Returns the primary key of this court timing.
	 *
	 * @return the primary key of this court timing
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the slot duration of this court timing.
	 *
	 * @return the slot duration of this court timing
	 */
	@Override
	public int getSlotDuration() {
		return model.getSlotDuration();
	}

	/**
	 * Returns the user ID of this court timing.
	 *
	 * @return the user ID of this court timing
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this court timing.
	 *
	 * @return the user name of this court timing
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this court timing.
	 *
	 * @return the user uuid of this court timing
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this court timing.
	 *
	 * @return the uuid of this court timing
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns <code>true</code> if this court timing is closed day.
	 *
	 * @return <code>true</code> if this court timing is closed day; <code>false</code> otherwise
	 */
	@Override
	public boolean isClosedDay() {
		return model.isClosedDay();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the break time of this court timing.
	 *
	 * @param breakTime the break time of this court timing
	 */
	@Override
	public void setBreakTime(int breakTime) {
		model.setBreakTime(breakTime);
	}

	/**
	 * Sets whether this court timing is closed day.
	 *
	 * @param closedDay the closed day of this court timing
	 */
	@Override
	public void setClosedDay(boolean closedDay) {
		model.setClosedDay(closedDay);
	}

	/**
	 * Sets the close time of this court timing.
	 *
	 * @param closeTime the close time of this court timing
	 */
	@Override
	public void setCloseTime(String closeTime) {
		model.setCloseTime(closeTime);
	}

	/**
	 * Sets the company ID of this court timing.
	 *
	 * @param companyId the company ID of this court timing
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the court ID of this court timing.
	 *
	 * @param courtId the court ID of this court timing
	 */
	@Override
	public void setCourtId(long courtId) {
		model.setCourtId(courtId);
	}

	/**
	 * Sets the court timing ID of this court timing.
	 *
	 * @param courtTimingId the court timing ID of this court timing
	 */
	@Override
	public void setCourtTimingId(long courtTimingId) {
		model.setCourtTimingId(courtTimingId);
	}

	/**
	 * Sets the create date of this court timing.
	 *
	 * @param createDate the create date of this court timing
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the day of this court timing.
	 *
	 * @param day the day of this court timing
	 */
	@Override
	public void setDay(int day) {
		model.setDay(day);
	}

	/**
	 * Sets the group ID of this court timing.
	 *
	 * @param groupId the group ID of this court timing
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this court timing.
	 *
	 * @param modifiedDate the modified date of this court timing
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the open time of this court timing.
	 *
	 * @param openTime the open time of this court timing
	 */
	@Override
	public void setOpenTime(String openTime) {
		model.setOpenTime(openTime);
	}

	/**
	 * Sets the primary key of this court timing.
	 *
	 * @param primaryKey the primary key of this court timing
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the slot duration of this court timing.
	 *
	 * @param slotDuration the slot duration of this court timing
	 */
	@Override
	public void setSlotDuration(int slotDuration) {
		model.setSlotDuration(slotDuration);
	}

	/**
	 * Sets the user ID of this court timing.
	 *
	 * @param userId the user ID of this court timing
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this court timing.
	 *
	 * @param userName the user name of this court timing
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this court timing.
	 *
	 * @param userUuid the user uuid of this court timing
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this court timing.
	 *
	 * @param uuid the uuid of this court timing
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected CourtTimingWrapper wrap(CourtTiming courtTiming) {
		return new CourtTimingWrapper(courtTiming);
	}

}