/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.GroupedModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.model.StagedAuditedModel;

import java.util.Date;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the CourtTiming service. Represents a row in the &quot;misk_courts_timings&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>bookings.model.impl.CourtTimingModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>bookings.model.impl.CourtTimingImpl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtTiming
 * @generated
 */
@ProviderType
public interface CourtTimingModel
	extends BaseModel<CourtTiming>, GroupedModel, ShardedModel,
			StagedAuditedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a court timing model instance should use the {@link CourtTiming} interface instead.
	 */

	/**
	 * Returns the primary key of this court timing.
	 *
	 * @return the primary key of this court timing
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this court timing.
	 *
	 * @param primaryKey the primary key of this court timing
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this court timing.
	 *
	 * @return the uuid of this court timing
	 */
	@AutoEscape
	@Override
	public String getUuid();

	/**
	 * Sets the uuid of this court timing.
	 *
	 * @param uuid the uuid of this court timing
	 */
	@Override
	public void setUuid(String uuid);

	/**
	 * Returns the court timing ID of this court timing.
	 *
	 * @return the court timing ID of this court timing
	 */
	public long getCourtTimingId();

	/**
	 * Sets the court timing ID of this court timing.
	 *
	 * @param courtTimingId the court timing ID of this court timing
	 */
	public void setCourtTimingId(long courtTimingId);

	/**
	 * Returns the group ID of this court timing.
	 *
	 * @return the group ID of this court timing
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this court timing.
	 *
	 * @param groupId the group ID of this court timing
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the company ID of this court timing.
	 *
	 * @return the company ID of this court timing
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this court timing.
	 *
	 * @param companyId the company ID of this court timing
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this court timing.
	 *
	 * @return the user ID of this court timing
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this court timing.
	 *
	 * @param userId the user ID of this court timing
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this court timing.
	 *
	 * @return the user uuid of this court timing
	 */
	@Override
	public String getUserUuid();

	/**
	 * Sets the user uuid of this court timing.
	 *
	 * @param userUuid the user uuid of this court timing
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this court timing.
	 *
	 * @return the user name of this court timing
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this court timing.
	 *
	 * @param userName the user name of this court timing
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this court timing.
	 *
	 * @return the create date of this court timing
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this court timing.
	 *
	 * @param createDate the create date of this court timing
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this court timing.
	 *
	 * @return the modified date of this court timing
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this court timing.
	 *
	 * @param modifiedDate the modified date of this court timing
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the court ID of this court timing.
	 *
	 * @return the court ID of this court timing
	 */
	public long getCourtId();

	/**
	 * Sets the court ID of this court timing.
	 *
	 * @param courtId the court ID of this court timing
	 */
	public void setCourtId(long courtId);

	/**
	 * Returns the day of this court timing.
	 *
	 * @return the day of this court timing
	 */
	public int getDay();

	/**
	 * Sets the day of this court timing.
	 *
	 * @param day the day of this court timing
	 */
	public void setDay(int day);

	/**
	 * Returns the open time of this court timing.
	 *
	 * @return the open time of this court timing
	 */
	@AutoEscape
	public String getOpenTime();

	/**
	 * Sets the open time of this court timing.
	 *
	 * @param openTime the open time of this court timing
	 */
	public void setOpenTime(String openTime);

	/**
	 * Returns the close time of this court timing.
	 *
	 * @return the close time of this court timing
	 */
	@AutoEscape
	public String getCloseTime();

	/**
	 * Sets the close time of this court timing.
	 *
	 * @param closeTime the close time of this court timing
	 */
	public void setCloseTime(String closeTime);

	/**
	 * Returns the slot duration of this court timing.
	 *
	 * @return the slot duration of this court timing
	 */
	public int getSlotDuration();

	/**
	 * Sets the slot duration of this court timing.
	 *
	 * @param slotDuration the slot duration of this court timing
	 */
	public void setSlotDuration(int slotDuration);

	/**
	 * Returns the break time of this court timing.
	 *
	 * @return the break time of this court timing
	 */
	public int getBreakTime();

	/**
	 * Sets the break time of this court timing.
	 *
	 * @param breakTime the break time of this court timing
	 */
	public void setBreakTime(int breakTime);

	/**
	 * Returns the closed day of this court timing.
	 *
	 * @return the closed day of this court timing
	 */
	public boolean getClosedDay();

	/**
	 * Returns <code>true</code> if this court timing is closed day.
	 *
	 * @return <code>true</code> if this court timing is closed day; <code>false</code> otherwise
	 */
	public boolean isClosedDay();

	/**
	 * Sets whether this court timing is closed day.
	 *
	 * @param closedDay the closed day of this court timing
	 */
	public void setClosedDay(boolean closedDay);

}