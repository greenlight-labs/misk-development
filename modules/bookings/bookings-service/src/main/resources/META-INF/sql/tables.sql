create table misk_courts_activities (
	uuid_ VARCHAR(75) null,
	courtActivityId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courtId LONG,
	startTime DATE null,
	endTime DATE null,
	allDay BOOLEAN,
	status BOOLEAN
);

create table misk_courts_bookings (
	uuid_ VARCHAR(75) null,
	bookingId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courtId LONG,
	appUserId LONG,
	slotStartTime DATE null,
	slotEndTime DATE null,
	status BOOLEAN,
	sync BOOLEAN
);

create table misk_courts_timings (
	uuid_ VARCHAR(75) null,
	courtTimingId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	courtId LONG,
	day INTEGER,
	openTime VARCHAR(75) null,
	closeTime VARCHAR(75) null,
	slotDuration INTEGER,
	breakTime INTEGER,
	closedDay BOOLEAN
);