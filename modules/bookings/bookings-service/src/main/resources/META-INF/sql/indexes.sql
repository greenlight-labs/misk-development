create index IX_7A7F3957 on misk_courts_activities (courtId);
create index IX_CAB5444B on misk_courts_activities (groupId);
create index IX_A1D3B773 on misk_courts_activities (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E3B97EB5 on misk_courts_activities (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_EC1D0FEB on misk_courts_bookings (appUserId);
create index IX_87C8468D on misk_courts_bookings (courtId, appUserId);
create index IX_75FCF7DE on misk_courts_bookings (groupId);
create index IX_11B951B on misk_courts_bookings (sync);
create index IX_56B15940 on misk_courts_bookings (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_633F2FC2 on misk_courts_bookings (uuid_[$COLUMN_LENGTH:75$], groupId);

create unique index IX_205D5539 on misk_courts_timings (courtId, day);
create index IX_4CE01067 on misk_courts_timings (groupId);
create index IX_2D16CAD7 on misk_courts_timings (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_4AB21B19 on misk_courts_timings (uuid_[$COLUMN_LENGTH:75$], groupId);