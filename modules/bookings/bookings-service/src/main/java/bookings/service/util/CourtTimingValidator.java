package bookings.service.util;

import bookings.model.CourtTiming;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * CourtTiming Validator 
 * 
 * @author tz
 *
 */
public class CourtTimingValidator implements ModelValidator<CourtTiming> {

	@Override
	public void validate(CourtTiming entry) throws PortalException {
        validateCourtTimingId(entry.getCourtTimingId());
        validateCourtId(entry.getCourtId());
        validateDay(entry.getDay());
        validateOpenTime(entry.getOpenTime());
        validateCloseTime(entry.getCloseTime());
	}

    protected void validateCourtTimingId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-timing-id-required");
        }
    }

    protected void validateCourtId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-timing-category-required");
        }
    }

    protected void validateDay(int field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-timing-name-required");
        }
    }

    protected void validateOpenTime(String field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-timing-listing-image-required");
        }
    }

    protected void validateCloseTime(String field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-timing-detail-image-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
