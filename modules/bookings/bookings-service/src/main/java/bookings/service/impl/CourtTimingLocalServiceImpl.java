/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.impl;

import bookings.exception.CourtTimingValidateException;
import bookings.model.CourtTiming;
import bookings.service.base.CourtTimingLocalServiceBaseImpl;

import bookings.service.util.CourtTimingValidator;
import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the court timing local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>bookings.service.CourtTimingLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtTimingLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=bookings.model.CourtTiming",
	service = AopService.class
)
public class CourtTimingLocalServiceImpl
	extends CourtTimingLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>bookings.service.CourtTimingLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>bookings.service.CourtTimingLocalServiceUtil</code>.
	 */

	public CourtTiming addEntry(CourtTiming orgEntry, ServiceContext serviceContext)
			throws PortalException, CourtTimingValidateException {

		// Validation

		ModelValidator<CourtTiming> modelValidator = new CourtTimingValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		CourtTiming entry = _addEntry(orgEntry, serviceContext);

		CourtTiming addedEntry = courtTimingPersistence.update(entry);

		courtTimingPersistence.clearCache();

		return addedEntry;
	}

	public CourtTiming updateEntry(
			CourtTiming orgEntry, ServiceContext serviceContext)
			throws PortalException, CourtTimingValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<CourtTiming> modelValidator = new CourtTimingValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		CourtTiming entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		CourtTiming updatedEntry = courtTimingPersistence.update(entry);
		courtTimingPersistence.clearCache();

		return updatedEntry;
	}

	protected CourtTiming _addEntry(CourtTiming entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(CourtTiming.class.getName());

		CourtTiming newEntry = courtTimingPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setCourtId(entry.getCourtId());
		newEntry.setDay(entry.getDay());
		newEntry.setOpenTime(entry.getOpenTime());
		newEntry.setCloseTime(entry.getCloseTime());
		newEntry.setSlotDuration(entry.getSlotDuration());
		newEntry.setBreakTime(entry.getBreakTime());
		newEntry.setClosedDay(entry.getClosedDay());

		//return courtTimingPersistence.update(newEntry);
		return newEntry;
	}

	protected CourtTiming _updateEntry(
			long primaryKey, CourtTiming entry, ServiceContext serviceContext)
			throws PortalException {

		CourtTiming updateEntry = fetchCourtTiming(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setCourtId(entry.getCourtId());
		updateEntry.setDay(entry.getDay());
		updateEntry.setOpenTime(entry.getOpenTime());
		updateEntry.setCloseTime(entry.getCloseTime());
		updateEntry.setSlotDuration(entry.getSlotDuration());
		updateEntry.setBreakTime(entry.getBreakTime());
		updateEntry.setClosedDay(entry.getClosedDay());

		return updateEntry;
	}

	public CourtTiming deleteEntry(long primaryKey) throws PortalException {
		CourtTiming entry = getCourtTiming(primaryKey);
		courtTimingPersistence.remove(entry);

		return entry;
	}

	public List<CourtTiming> findAllInGroup(long groupId) {

		return courtTimingPersistence.findByGroupId(groupId);
	}

	public List<CourtTiming> findAllInGroup(long groupId, int start, int end,
											  OrderByComparator<CourtTiming> obc) {

		return courtTimingPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<CourtTiming> findAllInGroup(long groupId, int start, int end) {

		return courtTimingPersistence.findByGroupId(groupId, start, end);
	}


	public int countAllInGroup(long groupId) {

		return courtTimingPersistence.countByGroupId(groupId);
	}

	public List<CourtTiming> findAllInCourt(long courtId) {
		return courtTimingPersistence.findByCourtId(courtId);
	}

	public List<CourtTiming> findAllInCourt(long courtId, int start, int end,
											  OrderByComparator<CourtTiming> obc) {
		return courtTimingPersistence.findByCourtId(courtId, start, end, obc);
	}

	public List<CourtTiming> findAllInCourt(long courtId, int start, int end) {
		return courtTimingPersistence.findByCourtId(courtId, start, end);
	}

	public int countAllInCourt(long courtId) {
		return courtTimingPersistence.countByCourtId(courtId);
	}

	public CourtTiming fetchByCourtDay(long courtId, int day) {
		return courtTimingPersistence.fetchByCourtDay(courtId, day);
	}

	public CourtTiming getCourtTimingFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, CourtTimingValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		CourtTiming entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchCourtTiming(primaryKey);
		}

		try {
			entry.setCourtId(ParamUtil.getLong(request, "courtId"));
			entry.setDay(ParamUtil.getInteger(request, "day"));
			entry.setOpenTime(ParamUtil.getString(request, "openTime"));
			entry.setCloseTime(ParamUtil.getString(request, "closeTime"));
			entry.setSlotDuration(ParamUtil.getInteger(request, "slotDuration"));
			entry.setBreakTime(ParamUtil.getInteger(request, "breakTime"));
			entry.setClosedDay(ParamUtil.getBoolean(request, "closedDay"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new CourtTimingValidateException(error);
		}

		return entry;
	}

	public CourtTiming getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(CourtTiming.class.getName());

		return createCourtTiming(primaryKey);
	}

	private static Log _log = LogFactoryUtil.getLog(
			CourtTimingLocalServiceImpl.class);
}