/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;

import bookings.exception.BookingValidateException;
import bookings.model.Booking;
import bookings.service.base.BookingLocalServiceBaseImpl;
import bookings.service.util.BookingValidator;

/**
 * The implementation of the booking local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>bookings.service.BookingLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingLocalServiceBaseImpl
 */
@Component(property = "model.class.name=bookings.model.Booking", service = AopService.class)
public class BookingLocalServiceImpl extends BookingLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>bookings.service.BookingLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>bookings.service.BookingLocalServiceUtil</code>.
	 */

	public Booking addEntry(Booking orgEntry, ServiceContext serviceContext)
			throws PortalException, BookingValidateException {

		// Validation

		ModelValidator<Booking> modelValidator = new BookingValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Booking entry = _addEntry(orgEntry, serviceContext);

		Booking addedEntry = bookingPersistence.update(entry);

		bookingPersistence.clearCache();

		return addedEntry;
	}

	public Booking updateEntry(Booking orgEntry, ServiceContext serviceContext)
			throws PortalException, BookingValidateException {

		// User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Booking> modelValidator = new BookingValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Booking entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Booking updatedEntry = bookingPersistence.update(entry);
		bookingPersistence.clearCache();

		return updatedEntry;
	}

	protected Booking _addEntry(Booking entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Booking.class.getName());

		Booking newEntry = bookingPersistence.create(id);

		// User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(serviceContext.getCompanyId());
		newEntry.setGroupId(serviceContext.getScopeGroupId());
		/*
		 * newEntry.setUserId(user.getUserId());
		 * newEntry.setUserName(user.getFullName());
		 */
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setCourtId(entry.getCourtId());
		newEntry.setAppUserId(entry.getAppUserId());
		newEntry.setSlotStartTime(entry.getSlotStartTime());
		newEntry.setSlotEndTime(entry.getSlotEndTime());
		newEntry.setStatus(entry.getStatus());
		newEntry.setSync(entry.getSync());

		// return bookingPersistence.update(newEntry);
		return newEntry;
	}

	protected Booking _updateEntry(long primaryKey, Booking entry, ServiceContext serviceContext)
			throws PortalException {

		Booking updateEntry = fetchBooking(primaryKey);

		// User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(serviceContext.getCompanyId());
		updateEntry.setGroupId(serviceContext.getScopeGroupId());
		/*
		 * updateEntry.setUserId(user.getUserId());
		 * updateEntry.setUserName(user.getFullName());
		 */
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setCourtId(entry.getCourtId());
		updateEntry.setAppUserId(entry.getAppUserId());
		updateEntry.setSlotStartTime(entry.getSlotStartTime());
		updateEntry.setSlotEndTime(entry.getSlotEndTime());
		updateEntry.setStatus(entry.getStatus());
		updateEntry.setSync(entry.getSync());

		return updateEntry;
	}

	public Booking deleteEntry(long primaryKey) throws PortalException {
		Booking entry = getBooking(primaryKey);
		bookingPersistence.remove(entry);

		return entry;
	}

	public List<Booking> findAllInGroup(long groupId) {

		return bookingPersistence.findByGroupId(groupId);
	}

	public List<Booking> findAllInGroup(long groupId, int start, int end, OrderByComparator<Booking> obc) {

		return bookingPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Booking> findAllInGroup(long groupId, int start, int end) {

		return bookingPersistence.findByGroupId(groupId, start, end);
	}

	public int countAllInGroup(long groupId) {

		return bookingPersistence.countByGroupId(groupId);
	}

	public List<Booking> findAllInCourt(long courtId) {
		return bookingPersistence.findByCourtId(courtId);
	}

	public List<Booking> findAllInCourt(long courtId, int start, int end, OrderByComparator<Booking> obc) {
		return bookingPersistence.findByCourtId(courtId, start, end, obc);
	}

	public List<Booking> findAllInCourt(long courtId, int start, int end) {
		return bookingPersistence.findByCourtId(courtId, start, end);
	}

	public int countAllInCourt(long courtId) {
		return bookingPersistence.countByCourtId(courtId);
	}

	public List<Booking> findAllInAppUser(long appUserId) {
		return bookingPersistence.findByAppUserId(appUserId);
	}

	public List<Booking> findAllInAppUser(long appUserId, int start, int end, OrderByComparator<Booking> obc) {
		return bookingPersistence.findByAppUserId(appUserId, start, end, obc);
	}

	public List<Booking> findAllInAppUser(long appUserId, int start, int end) {
		return bookingPersistence.findByAppUserId(appUserId, start, end);
	}

	public int countAllInAppUser(long appUserId) {
		return bookingPersistence.countByAppUserId(appUserId);
	}

	public List<Booking> findByCourt_AppUser(long courtId, long appUserId) {
		return bookingPersistence.findByCourt_AppUser(courtId, appUserId);
	}

	public List<Booking> findByCourt_AppUser(long courtId, long appUserId, int start, int end,
			OrderByComparator<Booking> obc) {
		return bookingPersistence.findByCourt_AppUser(courtId, appUserId, start, end, obc);
	}

	public List<Booking> findByCourt_AppUser(long courtId, long appUserId, int start, int end) {
		return bookingPersistence.findByCourt_AppUser(courtId, appUserId, start, end);
	}

	public int countByCourt_AppUser(long courtId, long appUserId) {
		return bookingPersistence.countByCourt_AppUser(courtId, appUserId);
	}

	public List<Booking> findBySync(boolean sync) {
		return bookingPersistence.findBySync(sync);
	}

	public List<Booking> findBySync(boolean sync, int start, int end) {
		return bookingPersistence.findBySync(sync, start, end);
	}

	public List<Booking> findBySync(boolean sync, int start, int end, OrderByComparator<Booking> obc) {
		return bookingPersistence.findBySync(sync, start, end, obc);
	}

	public int countBySync(boolean sync) {
		return bookingPersistence.countBySync(sync);
	}

	public List<Booking> findBookingsByCourtIdAndDate(String courtId, Date startTime, Date endTime) {

		System.out.println("courtId  " + courtId);
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Booking.class, getClassLoader());
		if (Validator.isNotNull(courtId) && !courtId.isEmpty() && Validator.isNumber(courtId)) {
			long courtIdL = Long.valueOf(courtId);
			Property property = PropertyFactoryUtil.forName("courtId");
			dynamicQuery.add(property.eq(courtIdL));
		}
		Property endTimeProperty = PropertyFactoryUtil.forName("slotEndTime");
		Property startTimeProperty = PropertyFactoryUtil.forName("slotStartTime");

		Criterion intervalCriterion = RestrictionsFactoryUtil.and(startTimeProperty.lt(endTime),
				endTimeProperty.gt(startTime));
		dynamicQuery.add(intervalCriterion);

		return dynamicQuery(dynamicQuery);	

	}

	public Booking getBookingFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, BookingValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Booking entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchBooking(primaryKey);
		}

		try {
			entry.setCourtId(ParamUtil.getLong(request, "courtId"));
			entry.setAppUserId(ParamUtil.getLong(request, "appUserId"));
			entry.setSlotStartTime(getDateTimeFromRequest(request, "slotStartTime"));
			entry.setSlotEndTime(getDateTimeFromRequest(request, "slotEndTime"));
			entry.setStatus(ParamUtil.getBoolean(request, "status"));
			entry.setSync(ParamUtil.getBoolean(request, "sync"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new BookingValidateException(error);
		}

		return entry;
	}

	public Booking getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Booking.class.getName());

		return createBooking(primaryKey);
	}

	/**
	 * Converte Date Time into Date()
	 *
	 * @param request PortletRequest
	 * @param prefix  Prefix of the parameter
	 * @return Date object
	 */
	public Date getDateTimeFromRequest(PortletRequest request, String prefix) {
		int Year = ParamUtil.getInteger(request, prefix + "Year");
		int Month = ParamUtil.getInteger(request, prefix + "Month") + 1;
		int Day = ParamUtil.getInteger(request, prefix + "Day");
		int Hour = ParamUtil.getInteger(request, prefix + "Hour");
		int Minute = ParamUtil.getInteger(request, prefix + "Minute");
		int AmPm = ParamUtil.getInteger(request, prefix + "AmPm");

		if (AmPm == Calendar.PM) {
			Hour += 12;
		}

		LocalDateTime ldt;

		try {
			ldt = LocalDateTime.of(Year, Month, Day, Hour, Minute, 0);
		} catch (Exception e) {
			_log.error("Unable get date data. Initialize with current date", e);
			Date in = new Date();

			Instant instant = in.toInstant();

			return Date.from(instant);
		}

		return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
	}

	public int isSlotBooked(long courtId, Date slotStartTime, Date slotEndTime) {

		return bookingFinder.isSlotBooked(courtId, slotStartTime, slotEndTime);
	}

	public List<Booking> getBookedSlots(long courtId, Date date) {

		return bookingFinder.getBookedSlots(courtId, date);
	}

	private static Log _log = LogFactoryUtil.getLog(BookingLocalServiceImpl.class);
}