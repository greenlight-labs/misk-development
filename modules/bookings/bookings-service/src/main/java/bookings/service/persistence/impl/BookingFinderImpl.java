package bookings.service.persistence.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.dao.orm.custom.sql.CustomSQL;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import bookings.model.Booking;
import bookings.model.impl.BookingImpl;
import bookings.service.persistence.BookingFinder;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.text.Format;
import java.util.*;


@Component(service = BookingFinder.class)
public class BookingFinderImpl
        extends BookingFinderBaseImpl implements BookingFinder {

    public static final String IS_SLOT_BOOKED =
            BookingFinder.class.getName() + ".isSlotBooked";

    public static final String IS_MAINTENANCE_DAR =
            BookingFinder.class.getName() + ".isMaintenanceDay";

    public static final String GET_BOOKED_SLOTS =
            BookingFinder.class.getName() + ".getBookedSlots";

    public int isSlotBooked(long courtId, Date slotStartTime, Date slotEndTime) {
        Session session = null;

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), IS_SLOT_BOOKED);

            sql = StringUtil.replace(sql, "[$END_TIME$]", "?");
            sql = StringUtil.replace(sql, "[$START_TIME$]", "?");

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.addScalar(COUNT_COLUMN_NAME, Type.LONG);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add(courtId);
            queryPos.add(slotEndTime);
            queryPos.add(slotStartTime);

            Iterator<Long> iterator = sqlQuery.iterate();

            if (iterator.hasNext()) {
                Long count = iterator.next();

                if (count != null) {
                    return count.intValue();
                }
            }

            return 0;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    public int isMaintenanceDay(long courtId, Date date) {
        Session session = null;

        try {
            session = openSession();

            String sql = _customSQL.get(getClass(), IS_SLOT_BOOKED);

            sql = StringUtil.replace(sql, "[$END_DATE$]", "?");
            sql = StringUtil.replace(sql, "[$START_DATE$]", "?");

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.addScalar(COUNT_COLUMN_NAME, Type.LONG);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add(courtId);
            queryPos.add(date);
            queryPos.add(date);

            Iterator<Long> iterator = sqlQuery.iterate();

            if (iterator.hasNext()) {
                Long count = iterator.next();

                if (count != null) {
                    return count.intValue();
                }
            }

            return 0;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    public List<Booking> getBookedSlots(long courtId, Date date) {
        Session session = null;
        List<Booking> bookings = Collections.emptyList();

        try {
            Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            String stringDate = dateTimeFormat.format(date);

            session = openSession();

            String sql = _customSQL.get(getClass(), GET_BOOKED_SLOTS);

            sql = StringUtil.replace(sql, "[$BOOK_DATE$]", "?");

            SQLQuery sqlQuery = session.createSynchronizedSQLQuery(sql);

            sqlQuery.setCacheable(false);
            sqlQuery.addEntity("Booking", BookingImpl.class);

            QueryPos queryPos = QueryPos.getInstance(sqlQuery);

            queryPos.add(courtId);
            queryPos.add(stringDate);

            bookings = (List<Booking>) sqlQuery.list();

            return bookings;
        }
        catch (Exception exception) {
            throw new SystemException(exception);
        }
        finally {
            closeSession(session);
        }
    }

    @Reference
    private CustomSQL _customSQL;
}
