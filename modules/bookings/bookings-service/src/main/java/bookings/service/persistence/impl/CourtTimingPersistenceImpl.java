/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.persistence.impl;

import bookings.exception.NoSuchCourtTimingException;

import bookings.model.CourtTiming;
import bookings.model.impl.CourtTimingImpl;
import bookings.model.impl.CourtTimingModelImpl;

import bookings.service.persistence.CourtTimingPersistence;
import bookings.service.persistence.CourtTimingUtil;
import bookings.service.persistence.impl.constants.BookingPersistenceConstants;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the court timing service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = CourtTimingPersistence.class)
public class CourtTimingPersistenceImpl
	extends BasePersistenceImpl<CourtTiming> implements CourtTimingPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>CourtTimingUtil</code> to access the court timing persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		CourtTimingImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the court timings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<CourtTiming> list = null;

		if (useFinderCache) {
			list = (List<CourtTiming>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CourtTiming courtTiming : list) {
					if (!uuid.equals(courtTiming.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_COURTTIMING_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<CourtTiming>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByUuid_First(
			String uuid, OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByUuid_First(uuid, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByUuid_First(
		String uuid, OrderByComparator<CourtTiming> orderByComparator) {

		List<CourtTiming> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByUuid_Last(
			String uuid, OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByUuid_Last(uuid, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByUuid_Last(
		String uuid, OrderByComparator<CourtTiming> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<CourtTiming> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where uuid = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming[] findByUuid_PrevAndNext(
			long courtTimingId, String uuid,
			OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		uuid = Objects.toString(uuid, "");

		CourtTiming courtTiming = findByPrimaryKey(courtTimingId);

		Session session = null;

		try {
			session = openSession();

			CourtTiming[] array = new CourtTimingImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, courtTiming, uuid, orderByComparator, true);

			array[1] = courtTiming;

			array[2] = getByUuid_PrevAndNext(
				session, courtTiming, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourtTiming getByUuid_PrevAndNext(
		Session session, CourtTiming courtTiming, String uuid,
		OrderByComparator<CourtTiming> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_COURTTIMING_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(courtTiming)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CourtTiming> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the court timings where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (CourtTiming courtTiming :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(courtTiming);
		}
	}

	/**
	 * Returns the number of court timings where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching court timings
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COURTTIMING_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"courtTiming.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(courtTiming.uuid IS NULL OR courtTiming.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByUUID_G(String uuid, long groupId)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByUUID_G(uuid, groupId);

		if (courtTiming == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchCourtTimingException(sb.toString());
		}

		return courtTiming;
	}

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the court timing where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof CourtTiming) {
			CourtTiming courtTiming = (CourtTiming)result;

			if (!Objects.equals(uuid, courtTiming.getUuid()) ||
				(groupId != courtTiming.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_COURTTIMING_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<CourtTiming> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					CourtTiming courtTiming = list.get(0);

					result = courtTiming;

					cacheResult(courtTiming);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CourtTiming)result;
		}
	}

	/**
	 * Removes the court timing where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the court timing that was removed
	 */
	@Override
	public CourtTiming removeByUUID_G(String uuid, long groupId)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = findByUUID_G(uuid, groupId);

		return remove(courtTiming);
	}

	/**
	 * Returns the number of court timings where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching court timings
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COURTTIMING_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"courtTiming.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(courtTiming.uuid IS NULL OR courtTiming.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"courtTiming.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<CourtTiming> list = null;

		if (useFinderCache) {
			list = (List<CourtTiming>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CourtTiming courtTiming : list) {
					if (!uuid.equals(courtTiming.getUuid()) ||
						(companyId != courtTiming.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_COURTTIMING_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<CourtTiming>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the first court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<CourtTiming> orderByComparator) {

		List<CourtTiming> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the last court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<CourtTiming> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<CourtTiming> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming[] findByUuid_C_PrevAndNext(
			long courtTimingId, String uuid, long companyId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		uuid = Objects.toString(uuid, "");

		CourtTiming courtTiming = findByPrimaryKey(courtTimingId);

		Session session = null;

		try {
			session = openSession();

			CourtTiming[] array = new CourtTimingImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, courtTiming, uuid, companyId, orderByComparator, true);

			array[1] = courtTiming;

			array[2] = getByUuid_C_PrevAndNext(
				session, courtTiming, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourtTiming getByUuid_C_PrevAndNext(
		Session session, CourtTiming courtTiming, String uuid, long companyId,
		OrderByComparator<CourtTiming> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_COURTTIMING_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(courtTiming)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CourtTiming> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the court timings where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (CourtTiming courtTiming :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(courtTiming);
		}
	}

	/**
	 * Returns the number of court timings where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching court timings
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COURTTIMING_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"courtTiming.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(courtTiming.uuid IS NULL OR courtTiming.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"courtTiming.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the court timings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching court timings
	 */
	@Override
	public List<CourtTiming> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the court timings where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<CourtTiming> list = null;

		if (useFinderCache) {
			list = (List<CourtTiming>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CourtTiming courtTiming : list) {
					if (groupId != courtTiming.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_COURTTIMING_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<CourtTiming>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByGroupId_First(
			long groupId, OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByGroupId_First(
			groupId, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the first court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByGroupId_First(
		long groupId, OrderByComparator<CourtTiming> orderByComparator) {

		List<CourtTiming> list = findByGroupId(
			groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByGroupId_Last(
			long groupId, OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByGroupId_Last(
			groupId, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the last court timing in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByGroupId_Last(
		long groupId, OrderByComparator<CourtTiming> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<CourtTiming> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where groupId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming[] findByGroupId_PrevAndNext(
			long courtTimingId, long groupId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = findByPrimaryKey(courtTimingId);

		Session session = null;

		try {
			session = openSession();

			CourtTiming[] array = new CourtTimingImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, courtTiming, groupId, orderByComparator, true);

			array[1] = courtTiming;

			array[2] = getByGroupId_PrevAndNext(
				session, courtTiming, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourtTiming getByGroupId_PrevAndNext(
		Session session, CourtTiming courtTiming, long groupId,
		OrderByComparator<CourtTiming> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_COURTTIMING_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(courtTiming)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CourtTiming> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the court timings where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (CourtTiming courtTiming :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(courtTiming);
		}
	}

	/**
	 * Returns the number of court timings where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching court timings
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COURTTIMING_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"courtTiming.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByCourtId;
	private FinderPath _finderPathWithoutPaginationFindByCourtId;
	private FinderPath _finderPathCountByCourtId;

	/**
	 * Returns all the court timings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the matching court timings
	 */
	@Override
	public List<CourtTiming> findByCourtId(long courtId) {
		return findByCourtId(
			courtId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByCourtId(long courtId, int start, int end) {
		return findByCourtId(courtId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator) {

		return findByCourtId(courtId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the court timings where courtId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param courtId the court ID
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching court timings
	 */
	@Override
	public List<CourtTiming> findByCourtId(
		long courtId, int start, int end,
		OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByCourtId;
				finderArgs = new Object[] {courtId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByCourtId;
			finderArgs = new Object[] {courtId, start, end, orderByComparator};
		}

		List<CourtTiming> list = null;

		if (useFinderCache) {
			list = (List<CourtTiming>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CourtTiming courtTiming : list) {
					if (courtId != courtTiming.getCourtId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_COURTTIMING_WHERE);

			sb.append(_FINDER_COLUMN_COURTID_COURTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(courtId);

				list = (List<CourtTiming>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByCourtId_First(
			long courtId, OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByCourtId_First(
			courtId, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("courtId=");
		sb.append(courtId);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the first court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByCourtId_First(
		long courtId, OrderByComparator<CourtTiming> orderByComparator) {

		List<CourtTiming> list = findByCourtId(
			courtId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByCourtId_Last(
			long courtId, OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByCourtId_Last(
			courtId, orderByComparator);

		if (courtTiming != null) {
			return courtTiming;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("courtId=");
		sb.append(courtId);

		sb.append("}");

		throw new NoSuchCourtTimingException(sb.toString());
	}

	/**
	 * Returns the last court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByCourtId_Last(
		long courtId, OrderByComparator<CourtTiming> orderByComparator) {

		int count = countByCourtId(courtId);

		if (count == 0) {
			return null;
		}

		List<CourtTiming> list = findByCourtId(
			courtId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the court timings before and after the current court timing in the ordered set where courtId = &#63;.
	 *
	 * @param courtTimingId the primary key of the current court timing
	 * @param courtId the court ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming[] findByCourtId_PrevAndNext(
			long courtTimingId, long courtId,
			OrderByComparator<CourtTiming> orderByComparator)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = findByPrimaryKey(courtTimingId);

		Session session = null;

		try {
			session = openSession();

			CourtTiming[] array = new CourtTimingImpl[3];

			array[0] = getByCourtId_PrevAndNext(
				session, courtTiming, courtId, orderByComparator, true);

			array[1] = courtTiming;

			array[2] = getByCourtId_PrevAndNext(
				session, courtTiming, courtId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected CourtTiming getByCourtId_PrevAndNext(
		Session session, CourtTiming courtTiming, long courtId,
		OrderByComparator<CourtTiming> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_COURTTIMING_WHERE);

		sb.append(_FINDER_COLUMN_COURTID_COURTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(CourtTimingModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(courtId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(courtTiming)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<CourtTiming> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the court timings where courtId = &#63; from the database.
	 *
	 * @param courtId the court ID
	 */
	@Override
	public void removeByCourtId(long courtId) {
		for (CourtTiming courtTiming :
				findByCourtId(
					courtId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(courtTiming);
		}
	}

	/**
	 * Returns the number of court timings where courtId = &#63;.
	 *
	 * @param courtId the court ID
	 * @return the number of matching court timings
	 */
	@Override
	public int countByCourtId(long courtId) {
		FinderPath finderPath = _finderPathCountByCourtId;

		Object[] finderArgs = new Object[] {courtId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_COURTTIMING_WHERE);

			sb.append(_FINDER_COLUMN_COURTID_COURTID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(courtId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COURTID_COURTID_2 =
		"courtTiming.courtId = ?";

	private FinderPath _finderPathFetchByCourtDay;
	private FinderPath _finderPathCountByCourtDay;

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the matching court timing
	 * @throws NoSuchCourtTimingException if a matching court timing could not be found
	 */
	@Override
	public CourtTiming findByCourtDay(long courtId, int day)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByCourtDay(courtId, day);

		if (courtTiming == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("courtId=");
			sb.append(courtId);

			sb.append(", day=");
			sb.append(day);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchCourtTimingException(sb.toString());
		}

		return courtTiming;
	}

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByCourtDay(long courtId, int day) {
		return fetchByCourtDay(courtId, day, true);
	}

	/**
	 * Returns the court timing where courtId = &#63; and day = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching court timing, or <code>null</code> if a matching court timing could not be found
	 */
	@Override
	public CourtTiming fetchByCourtDay(
		long courtId, int day, boolean useFinderCache) {

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {courtId, day};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByCourtDay, finderArgs, this);
		}

		if (result instanceof CourtTiming) {
			CourtTiming courtTiming = (CourtTiming)result;

			if ((courtId != courtTiming.getCourtId()) ||
				(day != courtTiming.getDay())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_COURTTIMING_WHERE);

			sb.append(_FINDER_COLUMN_COURTDAY_COURTID_2);

			sb.append(_FINDER_COLUMN_COURTDAY_DAY_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(courtId);

				queryPos.add(day);

				List<CourtTiming> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByCourtDay, finderArgs, list);
					}
				}
				else {
					CourtTiming courtTiming = list.get(0);

					result = courtTiming;

					cacheResult(courtTiming);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CourtTiming)result;
		}
	}

	/**
	 * Removes the court timing where courtId = &#63; and day = &#63; from the database.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the court timing that was removed
	 */
	@Override
	public CourtTiming removeByCourtDay(long courtId, int day)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = findByCourtDay(courtId, day);

		return remove(courtTiming);
	}

	/**
	 * Returns the number of court timings where courtId = &#63; and day = &#63;.
	 *
	 * @param courtId the court ID
	 * @param day the day
	 * @return the number of matching court timings
	 */
	@Override
	public int countByCourtDay(long courtId, int day) {
		FinderPath finderPath = _finderPathCountByCourtDay;

		Object[] finderArgs = new Object[] {courtId, day};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_COURTTIMING_WHERE);

			sb.append(_FINDER_COLUMN_COURTDAY_COURTID_2);

			sb.append(_FINDER_COLUMN_COURTDAY_DAY_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(courtId);

				queryPos.add(day);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COURTDAY_COURTID_2 =
		"courtTiming.courtId = ? AND ";

	private static final String _FINDER_COLUMN_COURTDAY_DAY_2 =
		"courtTiming.day = ?";

	public CourtTimingPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(CourtTiming.class);

		setModelImplClass(CourtTimingImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the court timing in the entity cache if it is enabled.
	 *
	 * @param courtTiming the court timing
	 */
	@Override
	public void cacheResult(CourtTiming courtTiming) {
		entityCache.putResult(
			CourtTimingImpl.class, courtTiming.getPrimaryKey(), courtTiming);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {courtTiming.getUuid(), courtTiming.getGroupId()},
			courtTiming);

		finderCache.putResult(
			_finderPathFetchByCourtDay,
			new Object[] {courtTiming.getCourtId(), courtTiming.getDay()},
			courtTiming);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the court timings in the entity cache if it is enabled.
	 *
	 * @param courtTimings the court timings
	 */
	@Override
	public void cacheResult(List<CourtTiming> courtTimings) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (courtTimings.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (CourtTiming courtTiming : courtTimings) {
			if (entityCache.getResult(
					CourtTimingImpl.class, courtTiming.getPrimaryKey()) ==
						null) {

				cacheResult(courtTiming);
			}
		}
	}

	/**
	 * Clears the cache for all court timings.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(CourtTimingImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the court timing.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CourtTiming courtTiming) {
		entityCache.removeResult(CourtTimingImpl.class, courtTiming);
	}

	@Override
	public void clearCache(List<CourtTiming> courtTimings) {
		for (CourtTiming courtTiming : courtTimings) {
			entityCache.removeResult(CourtTimingImpl.class, courtTiming);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(CourtTimingImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(
		CourtTimingModelImpl courtTimingModelImpl) {

		Object[] args = new Object[] {
			courtTimingModelImpl.getUuid(), courtTimingModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, courtTimingModelImpl, false);

		args = new Object[] {
			courtTimingModelImpl.getCourtId(), courtTimingModelImpl.getDay()
		};

		finderCache.putResult(
			_finderPathCountByCourtDay, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByCourtDay, args, courtTimingModelImpl, false);
	}

	/**
	 * Creates a new court timing with the primary key. Does not add the court timing to the database.
	 *
	 * @param courtTimingId the primary key for the new court timing
	 * @return the new court timing
	 */
	@Override
	public CourtTiming create(long courtTimingId) {
		CourtTiming courtTiming = new CourtTimingImpl();

		courtTiming.setNew(true);
		courtTiming.setPrimaryKey(courtTimingId);

		String uuid = PortalUUIDUtil.generate();

		courtTiming.setUuid(uuid);

		courtTiming.setCompanyId(CompanyThreadLocal.getCompanyId());

		return courtTiming;
	}

	/**
	 * Removes the court timing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing that was removed
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming remove(long courtTimingId)
		throws NoSuchCourtTimingException {

		return remove((Serializable)courtTimingId);
	}

	/**
	 * Removes the court timing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the court timing
	 * @return the court timing that was removed
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming remove(Serializable primaryKey)
		throws NoSuchCourtTimingException {

		Session session = null;

		try {
			session = openSession();

			CourtTiming courtTiming = (CourtTiming)session.get(
				CourtTimingImpl.class, primaryKey);

			if (courtTiming == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCourtTimingException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(courtTiming);
		}
		catch (NoSuchCourtTimingException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CourtTiming removeImpl(CourtTiming courtTiming) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(courtTiming)) {
				courtTiming = (CourtTiming)session.get(
					CourtTimingImpl.class, courtTiming.getPrimaryKeyObj());
			}

			if (courtTiming != null) {
				session.delete(courtTiming);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (courtTiming != null) {
			clearCache(courtTiming);
		}

		return courtTiming;
	}

	@Override
	public CourtTiming updateImpl(CourtTiming courtTiming) {
		boolean isNew = courtTiming.isNew();

		if (!(courtTiming instanceof CourtTimingModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(courtTiming.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(courtTiming);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in courtTiming proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom CourtTiming implementation " +
					courtTiming.getClass());
		}

		CourtTimingModelImpl courtTimingModelImpl =
			(CourtTimingModelImpl)courtTiming;

		if (Validator.isNull(courtTiming.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			courtTiming.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (courtTiming.getCreateDate() == null)) {
			if (serviceContext == null) {
				courtTiming.setCreateDate(date);
			}
			else {
				courtTiming.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!courtTimingModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				courtTiming.setModifiedDate(date);
			}
			else {
				courtTiming.setModifiedDate(
					serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(courtTiming);
			}
			else {
				courtTiming = (CourtTiming)session.merge(courtTiming);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(
			CourtTimingImpl.class, courtTimingModelImpl, false, true);

		cacheUniqueFindersCache(courtTimingModelImpl);

		if (isNew) {
			courtTiming.setNew(false);
		}

		courtTiming.resetOriginalValues();

		return courtTiming;
	}

	/**
	 * Returns the court timing with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the court timing
	 * @return the court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCourtTimingException {

		CourtTiming courtTiming = fetchByPrimaryKey(primaryKey);

		if (courtTiming == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCourtTimingException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return courtTiming;
	}

	/**
	 * Returns the court timing with the primary key or throws a <code>NoSuchCourtTimingException</code> if it could not be found.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing
	 * @throws NoSuchCourtTimingException if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming findByPrimaryKey(long courtTimingId)
		throws NoSuchCourtTimingException {

		return findByPrimaryKey((Serializable)courtTimingId);
	}

	/**
	 * Returns the court timing with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courtTimingId the primary key of the court timing
	 * @return the court timing, or <code>null</code> if a court timing with the primary key could not be found
	 */
	@Override
	public CourtTiming fetchByPrimaryKey(long courtTimingId) {
		return fetchByPrimaryKey((Serializable)courtTimingId);
	}

	/**
	 * Returns all the court timings.
	 *
	 * @return the court timings
	 */
	@Override
	public List<CourtTiming> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @return the range of court timings
	 */
	@Override
	public List<CourtTiming> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of court timings
	 */
	@Override
	public List<CourtTiming> findAll(
		int start, int end, OrderByComparator<CourtTiming> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the court timings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourtTimingModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of court timings
	 * @param end the upper bound of the range of court timings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of court timings
	 */
	@Override
	public List<CourtTiming> findAll(
		int start, int end, OrderByComparator<CourtTiming> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<CourtTiming> list = null;

		if (useFinderCache) {
			list = (List<CourtTiming>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_COURTTIMING);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_COURTTIMING;

				sql = sql.concat(CourtTimingModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<CourtTiming>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the court timings from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (CourtTiming courtTiming : findAll()) {
			remove(courtTiming);
		}
	}

	/**
	 * Returns the number of court timings.
	 *
	 * @return the number of court timings
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_COURTTIMING);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "courtTimingId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_COURTTIMING;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return CourtTimingModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the court timing persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class, new CourtTimingModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", CourtTiming.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathWithPaginationFindByCourtId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCourtId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"courtId"}, true);

		_finderPathWithoutPaginationFindByCourtId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCourtId",
			new String[] {Long.class.getName()}, new String[] {"courtId"},
			true);

		_finderPathCountByCourtId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCourtId",
			new String[] {Long.class.getName()}, new String[] {"courtId"},
			false);

		_finderPathFetchByCourtDay = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByCourtDay",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"courtId", "day"}, true);

		_finderPathCountByCourtDay = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCourtDay",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"courtId", "day"}, false);

		_setCourtTimingUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setCourtTimingUtilPersistence(null);

		entityCache.removeCache(CourtTimingImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setCourtTimingUtilPersistence(
		CourtTimingPersistence courtTimingPersistence) {

		try {
			Field field = CourtTimingUtil.class.getDeclaredField(
				"_persistence");

			field.setAccessible(true);

			field.set(null, courtTimingPersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = BookingPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = BookingPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = BookingPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_COURTTIMING =
		"SELECT courtTiming FROM CourtTiming courtTiming";

	private static final String _SQL_SELECT_COURTTIMING_WHERE =
		"SELECT courtTiming FROM CourtTiming courtTiming WHERE ";

	private static final String _SQL_COUNT_COURTTIMING =
		"SELECT COUNT(courtTiming) FROM CourtTiming courtTiming";

	private static final String _SQL_COUNT_COURTTIMING_WHERE =
		"SELECT COUNT(courtTiming) FROM CourtTiming courtTiming WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "courtTiming.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No CourtTiming exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No CourtTiming exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		CourtTimingPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class CourtTimingModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			CourtTimingModelImpl courtTimingModelImpl =
				(CourtTimingModelImpl)baseModel;

			long columnBitmask = courtTimingModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(courtTimingModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |=
						courtTimingModelImpl.getColumnBitmask(columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(CourtTimingPersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(courtTimingModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			CourtTimingModelImpl courtTimingModelImpl, String[] columnNames,
			boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] = courtTimingModelImpl.getColumnOriginalValue(
						columnName);
				}
				else {
					arguments[i] = courtTimingModelImpl.getColumnValue(
						columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= CourtTimingModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}