/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.service.impl;

import bookings.exception.CourtActivityValidateException;
import bookings.model.CourtActivity;
import bookings.service.base.CourtActivityLocalServiceBaseImpl;

import bookings.service.util.CourtActivityValidator;
import com.liferay.portal.aop.AopService;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * The implementation of the court activity local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>bookings.service.CourtActivityLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourtActivityLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=bookings.model.CourtActivity",
	service = AopService.class
)
public class CourtActivityLocalServiceImpl
	extends CourtActivityLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>bookings.service.CourtActivityLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>bookings.service.CourtActivityLocalServiceUtil</code>.
	 */

	public CourtActivity addEntry(CourtActivity orgEntry, ServiceContext serviceContext)
			throws PortalException, CourtActivityValidateException {

		// Validation

		ModelValidator<CourtActivity> modelValidator = new CourtActivityValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		CourtActivity entry = _addEntry(orgEntry, serviceContext);

		CourtActivity addedEntry = courtActivityPersistence.update(entry);

		courtActivityPersistence.clearCache();

		return addedEntry;
	}

	public CourtActivity updateEntry(
			CourtActivity orgEntry, ServiceContext serviceContext)
			throws PortalException, CourtActivityValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<CourtActivity> modelValidator = new CourtActivityValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		CourtActivity entry = _updateEntry(
				orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		CourtActivity updatedEntry = courtActivityPersistence.update(entry);
		courtActivityPersistence.clearCache();

		return updatedEntry;
	}

	protected CourtActivity _addEntry(CourtActivity entry, ServiceContext serviceContext)
			throws PortalException {

		long id = counterLocalService.increment(CourtActivity.class.getName());

		CourtActivity newEntry = courtActivityPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setCourtId(entry.getCourtId());
		newEntry.setStartTime(entry.getStartTime());
		newEntry.setEndTime(entry.getEndTime());
		newEntry.setAllDay(entry.getAllDay());
		newEntry.setStatus(entry.getStatus());

		//return courtActivityPersistence.update(newEntry);
		return newEntry;
	}

	protected CourtActivity _updateEntry(
			long primaryKey, CourtActivity entry, ServiceContext serviceContext)
			throws PortalException {

		CourtActivity updateEntry = fetchCourtActivity(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setCourtId(entry.getCourtId());
		updateEntry.setStartTime(entry.getStartTime());
		updateEntry.setEndTime(entry.getEndTime());
		updateEntry.setAllDay(entry.getAllDay());
		updateEntry.setStatus(entry.getStatus());

		return updateEntry;
	}

	public CourtActivity deleteEntry(long primaryKey) throws PortalException {
		CourtActivity entry = getCourtActivity(primaryKey);
		courtActivityPersistence.remove(entry);

		return entry;
	}

	public List<CourtActivity> findAllInGroup(long groupId) {

		return courtActivityPersistence.findByGroupId(groupId);
	}

	public List<CourtActivity> findAllInGroup(long groupId, int start, int end,
										OrderByComparator<CourtActivity> obc) {

		return courtActivityPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<CourtActivity> findAllInGroup(long groupId, int start, int end) {

		return courtActivityPersistence.findByGroupId(groupId, start, end);
	}


	public int countAllInGroup(long groupId) {

		return courtActivityPersistence.countByGroupId(groupId);
	}

	public List<CourtActivity> findAllInCourt(long courtId) {
		return courtActivityPersistence.findByCourtId(courtId);
	}

	public List<CourtActivity> findAllInCourt(long courtId, int start, int end,
										OrderByComparator<CourtActivity> obc) {
		return courtActivityPersistence.findByCourtId(courtId, start, end, obc);
	}

	public List<CourtActivity> findAllInCourt(long courtId, int start, int end) {
		return courtActivityPersistence.findByCourtId(courtId, start, end);
	}

	public int countAllInCourt(long courtId) {
		return courtActivityPersistence.countByCourtId(courtId);
	}

	public CourtActivity getCourtActivityFromRequest(
			long primaryKey, PortletRequest request)
			throws PortletException, CourtActivityValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		CourtActivity entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		}
		else {
			entry = fetchCourtActivity(primaryKey);
		}

		try {
			entry.setCourtId(ParamUtil.getLong(request, "courtId"));
			entry.setStartTime(getDateTimeFromRequest(request, "startTime"));
			entry.setEndTime(getDateTimeFromRequest(request, "endTime"));
			entry.setAllDay(ParamUtil.getBoolean(request, "allDay"));
			entry.setStatus(ParamUtil.getBoolean(request, "status"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		}
		catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new CourtActivityValidateException(error);
		}

		return entry;
	}

	public CourtActivity getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 :
				counterLocalService.increment(CourtActivity.class.getName());

		return createCourtActivity(primaryKey);
	}

	public Date getDateTimeFromRequest(PortletRequest request, String prefix) {
		int Year = ParamUtil.getInteger(request, prefix + "Year");
		int Month = ParamUtil.getInteger(request, prefix + "Month") + 1;
		int Day = ParamUtil.getInteger(request, prefix + "Day");
		int Hour = ParamUtil.getInteger(request, prefix + "Hour");
		int Minute = ParamUtil.getInteger(request, prefix + "Minute");
		int AmPm = ParamUtil.getInteger(request, prefix + "AmPm");

		if (AmPm == Calendar.PM) {
			Hour += 12;
		}

		LocalDateTime ldt;

		try {
			ldt = LocalDateTime.of(Year, Month, Day, Hour, Minute, 0);
		}
		catch (Exception e) {
			_log.error(
					"Unable get date data. Initialize with current date", e);
			Date in = new Date();

			Instant instant = in.toInstant();

			return Date.from(instant);
		}

		return Date.from(
				ldt.atZone(
						ZoneId.systemDefault()
				).toInstant());
	}

	private static Log _log = LogFactoryUtil.getLog(
			CourtActivityLocalServiceImpl.class);

}