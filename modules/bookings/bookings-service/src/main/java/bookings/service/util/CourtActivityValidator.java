package bookings.service.util;

import bookings.model.CourtActivity;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * CourtActivity Validator 
 * 
 * @author tz
 *
 */
public class CourtActivityValidator implements ModelValidator<CourtActivity> {

	@Override
	public void validate(CourtActivity entry) throws PortalException {
        validateCourtActivityId(entry.getCourtActivityId());
        validateCourtId(entry.getCourtId());
        validateStartTime(entry.getStartTime());
        validateEndTime(entry.getEndTime());
        validateAllDay(entry.getAllDay());
	}

    protected void validateCourtActivityId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-activity-id-required");
        }
    }

    protected void validateCourtId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-activity-court-id-required");
        }
    }

    protected void validateStartTime(Date field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-activity-start-time-required");
        }
    }

    protected void validateEndTime(Date field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-activity-end-time-required");
        }
    }

    protected void validateAllDay(Boolean field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("court-activity-all-day-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
