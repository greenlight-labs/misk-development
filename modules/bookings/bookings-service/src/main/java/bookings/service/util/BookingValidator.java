package bookings.service.util;

import bookings.model.Booking;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Booking Validator 
 * 
 * @author tz
 *
 */
public class BookingValidator implements ModelValidator<Booking> {

	@Override
	public void validate(Booking entry) throws PortalException {
        validateBookingId(entry.getBookingId());
        validateCourtId(entry.getCourtId());
        validateAppUserId(entry.getAppUserId());
        validateSlotStartTime(entry.getSlotStartTime());
        validateSlotEndTime(entry.getSlotEndTime());
	}

    protected void validateBookingId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("booking-id-required");
        }
    }

    protected void validateCourtId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("booking-court-required");
        }
    }

    protected void validateAppUserId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("booking-app-user-id-required");
        }
    }

    protected void validateSlotStartTime(Date field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("booking-slot-start-time-required");
        }
    }

    protected void validateSlotEndTime(Date field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("booking-slot-end-time-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
