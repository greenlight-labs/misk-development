/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model.impl;

import bookings.model.CourtActivity;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CourtActivity in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CourtActivityCacheModel
	implements CacheModel<CourtActivity>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CourtActivityCacheModel)) {
			return false;
		}

		CourtActivityCacheModel courtActivityCacheModel =
			(CourtActivityCacheModel)object;

		if (courtActivityId == courtActivityCacheModel.courtActivityId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, courtActivityId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", courtActivityId=");
		sb.append(courtActivityId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", courtId=");
		sb.append(courtId);
		sb.append(", startTime=");
		sb.append(startTime);
		sb.append(", endTime=");
		sb.append(endTime);
		sb.append(", allDay=");
		sb.append(allDay);
		sb.append(", status=");
		sb.append(status);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CourtActivity toEntityModel() {
		CourtActivityImpl courtActivityImpl = new CourtActivityImpl();

		if (uuid == null) {
			courtActivityImpl.setUuid("");
		}
		else {
			courtActivityImpl.setUuid(uuid);
		}

		courtActivityImpl.setCourtActivityId(courtActivityId);
		courtActivityImpl.setGroupId(groupId);
		courtActivityImpl.setCompanyId(companyId);
		courtActivityImpl.setUserId(userId);

		if (userName == null) {
			courtActivityImpl.setUserName("");
		}
		else {
			courtActivityImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			courtActivityImpl.setCreateDate(null);
		}
		else {
			courtActivityImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			courtActivityImpl.setModifiedDate(null);
		}
		else {
			courtActivityImpl.setModifiedDate(new Date(modifiedDate));
		}

		courtActivityImpl.setCourtId(courtId);

		if (startTime == Long.MIN_VALUE) {
			courtActivityImpl.setStartTime(null);
		}
		else {
			courtActivityImpl.setStartTime(new Date(startTime));
		}

		if (endTime == Long.MIN_VALUE) {
			courtActivityImpl.setEndTime(null);
		}
		else {
			courtActivityImpl.setEndTime(new Date(endTime));
		}

		courtActivityImpl.setAllDay(allDay);
		courtActivityImpl.setStatus(status);

		courtActivityImpl.resetOriginalValues();

		return courtActivityImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		courtActivityId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		courtId = objectInput.readLong();
		startTime = objectInput.readLong();
		endTime = objectInput.readLong();

		allDay = objectInput.readBoolean();

		status = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(courtActivityId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(courtId);
		objectOutput.writeLong(startTime);
		objectOutput.writeLong(endTime);

		objectOutput.writeBoolean(allDay);

		objectOutput.writeBoolean(status);
	}

	public String uuid;
	public long courtActivityId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long courtId;
	public long startTime;
	public long endTime;
	public boolean allDay;
	public boolean status;

}