/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model.impl;

import bookings.model.CourtTiming;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CourtTiming in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class CourtTimingCacheModel
	implements CacheModel<CourtTiming>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CourtTimingCacheModel)) {
			return false;
		}

		CourtTimingCacheModel courtTimingCacheModel =
			(CourtTimingCacheModel)object;

		if (courtTimingId == courtTimingCacheModel.courtTimingId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, courtTimingId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", courtTimingId=");
		sb.append(courtTimingId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", courtId=");
		sb.append(courtId);
		sb.append(", day=");
		sb.append(day);
		sb.append(", openTime=");
		sb.append(openTime);
		sb.append(", closeTime=");
		sb.append(closeTime);
		sb.append(", slotDuration=");
		sb.append(slotDuration);
		sb.append(", breakTime=");
		sb.append(breakTime);
		sb.append(", closedDay=");
		sb.append(closedDay);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CourtTiming toEntityModel() {
		CourtTimingImpl courtTimingImpl = new CourtTimingImpl();

		if (uuid == null) {
			courtTimingImpl.setUuid("");
		}
		else {
			courtTimingImpl.setUuid(uuid);
		}

		courtTimingImpl.setCourtTimingId(courtTimingId);
		courtTimingImpl.setGroupId(groupId);
		courtTimingImpl.setCompanyId(companyId);
		courtTimingImpl.setUserId(userId);

		if (userName == null) {
			courtTimingImpl.setUserName("");
		}
		else {
			courtTimingImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			courtTimingImpl.setCreateDate(null);
		}
		else {
			courtTimingImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			courtTimingImpl.setModifiedDate(null);
		}
		else {
			courtTimingImpl.setModifiedDate(new Date(modifiedDate));
		}

		courtTimingImpl.setCourtId(courtId);
		courtTimingImpl.setDay(day);

		if (openTime == null) {
			courtTimingImpl.setOpenTime("");
		}
		else {
			courtTimingImpl.setOpenTime(openTime);
		}

		if (closeTime == null) {
			courtTimingImpl.setCloseTime("");
		}
		else {
			courtTimingImpl.setCloseTime(closeTime);
		}

		courtTimingImpl.setSlotDuration(slotDuration);
		courtTimingImpl.setBreakTime(breakTime);
		courtTimingImpl.setClosedDay(closedDay);

		courtTimingImpl.resetOriginalValues();

		return courtTimingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		courtTimingId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		courtId = objectInput.readLong();

		day = objectInput.readInt();
		openTime = objectInput.readUTF();
		closeTime = objectInput.readUTF();

		slotDuration = objectInput.readInt();

		breakTime = objectInput.readInt();

		closedDay = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(courtTimingId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(courtId);

		objectOutput.writeInt(day);

		if (openTime == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(openTime);
		}

		if (closeTime == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(closeTime);
		}

		objectOutput.writeInt(slotDuration);

		objectOutput.writeInt(breakTime);

		objectOutput.writeBoolean(closedDay);
	}

	public String uuid;
	public long courtTimingId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long courtId;
	public int day;
	public String openTime;
	public String closeTime;
	public int slotDuration;
	public int breakTime;
	public boolean closedDay;

}