/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package bookings.model.impl;

import bookings.model.Booking;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Booking in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class BookingCacheModel implements CacheModel<Booking>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BookingCacheModel)) {
			return false;
		}

		BookingCacheModel bookingCacheModel = (BookingCacheModel)object;

		if (bookingId == bookingCacheModel.bookingId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, bookingId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", bookingId=");
		sb.append(bookingId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", courtId=");
		sb.append(courtId);
		sb.append(", appUserId=");
		sb.append(appUserId);
		sb.append(", slotStartTime=");
		sb.append(slotStartTime);
		sb.append(", slotEndTime=");
		sb.append(slotEndTime);
		sb.append(", status=");
		sb.append(status);
		sb.append(", sync=");
		sb.append(sync);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Booking toEntityModel() {
		BookingImpl bookingImpl = new BookingImpl();

		if (uuid == null) {
			bookingImpl.setUuid("");
		}
		else {
			bookingImpl.setUuid(uuid);
		}

		bookingImpl.setBookingId(bookingId);
		bookingImpl.setGroupId(groupId);
		bookingImpl.setCompanyId(companyId);
		bookingImpl.setUserId(userId);

		if (userName == null) {
			bookingImpl.setUserName("");
		}
		else {
			bookingImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			bookingImpl.setCreateDate(null);
		}
		else {
			bookingImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			bookingImpl.setModifiedDate(null);
		}
		else {
			bookingImpl.setModifiedDate(new Date(modifiedDate));
		}

		bookingImpl.setCourtId(courtId);
		bookingImpl.setAppUserId(appUserId);

		if (slotStartTime == Long.MIN_VALUE) {
			bookingImpl.setSlotStartTime(null);
		}
		else {
			bookingImpl.setSlotStartTime(new Date(slotStartTime));
		}

		if (slotEndTime == Long.MIN_VALUE) {
			bookingImpl.setSlotEndTime(null);
		}
		else {
			bookingImpl.setSlotEndTime(new Date(slotEndTime));
		}

		bookingImpl.setStatus(status);
		bookingImpl.setSync(sync);

		bookingImpl.resetOriginalValues();

		return bookingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		bookingId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		courtId = objectInput.readLong();

		appUserId = objectInput.readLong();
		slotStartTime = objectInput.readLong();
		slotEndTime = objectInput.readLong();

		status = objectInput.readBoolean();

		sync = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(bookingId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(courtId);

		objectOutput.writeLong(appUserId);
		objectOutput.writeLong(slotStartTime);
		objectOutput.writeLong(slotEndTime);

		objectOutput.writeBoolean(status);

		objectOutput.writeBoolean(sync);
	}

	public String uuid;
	public long bookingId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long courtId;
	public long appUserId;
	public long slotStartTime;
	public long slotEndTime;
	public boolean status;
	public boolean sync;

}