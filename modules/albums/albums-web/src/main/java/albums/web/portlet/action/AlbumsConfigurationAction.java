package albums.web.portlet.action;

import albums.web.constants.AlbumsWebPortletKeys;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Map;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AlbumsWebPortletKeys.ALBUMSWEB
        },
        service = ConfigurationAction.class
)
public class AlbumsConfigurationAction extends DefaultConfigurationAction {

        @Override
        public String getJspPath(HttpServletRequest httpServletRequest) {
                return "/configuration.jsp";
        }

        @Override
        public void processAction(
                PortletConfig portletConfig, ActionRequest actionRequest,
                ActionResponse actionResponse)
                throws Exception {

                Locale defaultLocale = LocaleUtil.getSiteDefault();

                PortletPreferences preferences = actionRequest.getPreferences();

                // Save Localized Values
                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "sectionTitle");
                Map<Locale, String> sectionTitleMap = LocalizationUtil.getLocalizationMap(actionRequest, "sectionTitle");
                preferences.setValue("sectionTitle", sectionTitleMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "filterAllLabel");
                Map<Locale, String> filterAllLabelMap = LocalizationUtil.getLocalizationMap(actionRequest, "filterAllLabel");
                preferences.setValue("filterAllLabel", filterAllLabelMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "filterYearLabel");
                Map<Locale, String> filterYearLabelMap = LocalizationUtil.getLocalizationMap(actionRequest, "filterYearLabel");
                preferences.setValue("filterYearLabel", filterYearLabelMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "filterMonthLabel");
                Map<Locale, String> filterMonthLabelMap = LocalizationUtil.getLocalizationMap(actionRequest, "filterMonthLabel");
                preferences.setValue("filterMonthLabel", filterMonthLabelMap.get(defaultLocale));

                LocalizationUtil.setLocalizedPreferencesValues(actionRequest, preferences, "filterButtonLabel");
                Map<Locale, String> filterButtonLabelMap = LocalizationUtil.getLocalizationMap(actionRequest, "filterButtonLabel");
                preferences.setValue("filterButtonLabel", filterButtonLabelMap.get(defaultLocale));

                // Store these 'added' preferences
                preferences.store();

                super.processAction(portletConfig, actionRequest, actionResponse);
        }
}
