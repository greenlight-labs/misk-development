package albums.web.constants;

/**
 * @author tz
 */
public class AlbumsWebPortletKeys {

	public static final String ALBUMSWEB =
		"albums_web_AlbumsWebPortlet";

	public static final String LATESTALBUMSSLIDER =
		"albums_web_LatestAlbumsSliderPortlet";

}