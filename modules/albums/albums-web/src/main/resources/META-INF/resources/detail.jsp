<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@ page import="albums.service.GalleryLocalServiceUtil" %>
<%@ page import="albums.model.Gallery" %>
<%@include file="/init.jsp" %>

<%
    long albumId = ParamUtil.getLong(renderRequest, "albumId");

    Album album = null;
    if (albumId > 0) {
        try {
            album = AlbumLocalServiceUtil.getAlbum(albumId);
        } catch (PortalException e) {
            //e.printStackTrace();
        }
    }

    List<Album> albums = AlbumLocalServiceUtil.findByGroupId(scopeGroupId);
    List<Gallery> gallerySlides = GalleryLocalServiceUtil.findByAlbumId(albumId);
    int countGalleryImages = GalleryLocalServiceUtil.countGalleryImages(gallerySlides);
    int countGalleryVideos = GalleryLocalServiceUtil.countGalleryVideos(gallerySlides);
%>

<%
    if (album == null) {
%>
<h4>No Record Found.</h4>
<%
} else {
%>
<section class="album-detail-sec" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <div class="album-info-wrap">
                    <h3 class="heading_"><liferay-ui:message key="albums_web_album" /></h3>
                    <p class="label"><liferay-ui:message key="albums_web_select_album" /></p>
                    <div class='SelectBox'>
                        <select name="property" class="selectpicker">
                            <%
                                if(albums.size() > 0){
                                    for (Album curAlbum : albums) {
                                        String selected = curAlbum.getAlbumId() == album.getAlbumId() ? "selected" : "";
                            %>
                            <portlet:renderURL var="detailPageURL2">
                                <portlet:param name="albumId" value="<%= String.valueOf(curAlbum.getAlbumId()) %>" />
                                <portlet:param name="mvcPath" value="/detail.jsp" />
                            </portlet:renderURL>
                            <option value="<%= curAlbum.getTitle(locale) %>" data-gallery-url="<%= detailPageURL2.toString() %>" <%= selected %>><%= curAlbum.getTitle(locale).length() > 40 ? (curAlbum.getTitle(locale)).substring(0, 40) : curAlbum.getTitle(locale) %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <div class="chevron">
                            <img src="/assets/svgs/option-arrow.svg" alt="">
                        </div>
                    </div>
                    <div class="date-potos-wrap">
                        <h3><%= album.getTitle(locale) %></h3>
                        <ul>
                            <li><span class="icon-wrap"> <img src="/assets/svgs/calendar.svg" alt="" class="img-fluid"> </span> <%= dateFormat.format(album.getDisplayDate()) %></li>
                            <li><span class="icon-wrap"> <img src="/assets/svgs/photo-icon.svg" alt="" class="img-fluid"> </span>
                                <% if (countGalleryImages > 0) { %>
                                    <%=countGalleryImages%> <liferay-ui:message key='<%=countGalleryImages == 1 ? "albums_web_listing_portlet_photo" : "albums_web_listing_portlet_photos"%>'/>
                                <% } %>
                                <% if (countGalleryVideos > 0) { %>
                                    <span>|</span>
                                    <%=countGalleryVideos%> <liferay-ui:message key='<%=countGalleryVideos == 1 ? "albums_web_listing_portlet_video" : "albums_web_listing_portlet_videos"%>'/>
                                <% } %>
                            </li>
                        </ul>
                        <p><%= album.getDescription(locale) %></p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-7">
                <div class="album-slider-wrapper">
                    <% if(gallerySlides.size() > 0) { %>
                        <div class="album-slider-main">
                            <% for (Gallery curGallerySlide : gallerySlides) { %>
                            <% if(curGallerySlide.getType() == 1) { %>
                                    <div class="slide">
                                        <div class="icon-wrap">
                                            <a href="<%=curGallerySlide.getImage(locale)%>" class="img-wrap" data-fancybox="gallery">
                                                <img src="/assets/svgs/expand-icon.svg" alt="">
                                            </a>
                                        </div>
                                        <a href="<%=curGallerySlide.getImage(locale)%>" data-fancybox="gallery">
                                            <img src="<%=curGallerySlide.getImage(locale)%>" alt="" class="img-fluid" />
                                        </a>
                                    </div>
                                <% } else if(curGallerySlide.getType() == 2) { %>
                                    <div class="slide">
                                        <div class="icon-wrap">
                                            <a href="<%=curGallerySlide.getVideoLink(locale)%>" class="img-wrap" data-fancybox="gallery">
                                                <img src="/assets/svgs/expand-icon.svg" alt="">
                                            </a>
                                        </div>
                                        <div class="col-md-12 text-center video-icon-container_">
                                            <a class="video-play-btn" href="<%=curGallerySlide.getVideoLink(locale)%>" data-fancybox="gallery">
                                                <i class="icon-ft-arrow-right"></i>
                                            </a>
                                        </div>
                                        <a href="<%=curGallerySlide.getVideoLink(locale)%>" data-fancybox="gallery">
                                            <img src="<%=curGallerySlide.getImage(locale)%>" alt="" class="img-fluid"/>
                                        </a>
                                    </div>
                                <% } %>
                            <% } %>
                        </div>
                        <div class="album-slider-thmb">
                            <%
                                if(gallerySlides.size() > 0){
                                    for (Gallery curGallerySlide : gallerySlides) {
                            %>
                                    <% if(curGallerySlide.getType() == 1) { %>
                                        <div class="slide"><img src="<%=curGallerySlide.getThumbnail(locale)%>" alt="" class="img-fluid"></div>
                                    <% } else if(curGallerySlide.getType() == 2) { %>
                                        <div class="slide">
                                            <a class="video-play-btn" href="javascript:">
                                                <i class="icon-ft-arrow-right"></i>
                                            </a>
                                            <img src="<%=curGallerySlide.getThumbnail(locale)%>" alt="" class="img-fluid">
                                        </div>
                            <%
                                        }
                                    }
                                }
                            %>
                        </div>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</section>
<%
    }
%>
<script type="text/javascript">
    window.addEventListener('load', function() {
        setTimeout(function () {
            $('.selectpicker').selectpicker('toggle');
            $('section.album-detail-sec h3').trigger('click');
        }, 500);
        $("select[name=property]").on('change', function (e) {
            var galleryUrl = $(e.currentTarget).find('option:selected').data('galleryUrl');
            if(galleryUrl){
                location.href = galleryUrl;
            }
        });
    });
</script>