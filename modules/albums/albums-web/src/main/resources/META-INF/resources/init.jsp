<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="albums.model.Album" %>
<%@ page import="albums.model.Category" %>
<%@ page import="albums.service.AlbumLocalServiceUtil" %>
<%@ page import="albums.service.CategoryLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.*" %>
<%@ page import="javax.portlet.PortletPreferences"%>
<%@ page import="java.text.Format" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@ page import="com.liferay.portal.kernel.language.Language" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>

<liferay-frontend:defineObjects />

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
    Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("dd MMM, yyyy", locale, timeZone);
    if(themeDisplay.getLanguageId().equals("ar_SA")){
        dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("dd MMMMM, yyyy", locale, timeZone);
    }

    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String editMode = originalRequest.getParameter("p_l_mode");

    int limit = 10;
    int start = ParamUtil.getInteger(request, "start");
    int end = ParamUtil.getInteger(request, "end");
    end = end == 0 ? limit : end;

    long searchType = NumberUtils.toLong(originalRequest.getParameter("type"));
    int searchYear = NumberUtils.toInt(originalRequest.getParameter("year"));
    int searchMonth = NumberUtils.toInt(originalRequest.getParameter("month"));

    // Album Listing Portlet Preferences
    String sectionTitlePortlet1Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "sectionTitle", LanguageUtil.get(request, "albums_web_listing_portlet_section_title")));
    String filterAllLabelXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "filterAllLabel", LanguageUtil.get(request, "albums_web_listing_portlet_filter_all")));
    String filterYearLabelXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "filterYearLabel", LanguageUtil.get(request, "albums_web_listing_portlet_filter_by_year")));
    String filterMonthLabelXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "filterMonthLabel", LanguageUtil.get(request, "albums_web_listing_portlet_filter_by_month")));
    String filterButtonLabelXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "filterButtonLabel", LanguageUtil.get(request, "albums_web_listing_portlet_filter")));

    String sectionTitlePortlet1 = LocalizationUtil.getLocalization(sectionTitlePortlet1Xml, themeDisplay.getLanguageId());
    String filterAllLabel = LocalizationUtil.getLocalization(filterAllLabelXml, themeDisplay.getLanguageId());
    String filterYearLabel = LocalizationUtil.getLocalization(filterYearLabelXml, themeDisplay.getLanguageId());
    String filterMonthLabel = LocalizationUtil.getLocalization(filterMonthLabelXml, themeDisplay.getLanguageId());
    String filterButtonLabel = LocalizationUtil.getLocalization(filterButtonLabelXml, themeDisplay.getLanguageId());
    // Latest Album Slider Portlet Preferences
    String sectionTitlePortlet2Xml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "sectionTitle", LanguageUtil.get(request, "albums_web_slider_portlet_section_title")));
    String buttonLabelXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLabel", LanguageUtil.get(request, "albums_web_slider_portlet_button_label")));
    String buttonLinkXml = GetterUtil.getString(LocalizationUtil.getLocalizationXmlFromPreferences(portletPreferences, renderRequest, "buttonLink", LanguageUtil.get(request, "albums_web_slider_portlet_button_link")));

    String sectionTitlePortlet2 = LocalizationUtil.getLocalization(sectionTitlePortlet2Xml, themeDisplay.getLanguageId());
    String buttonLabel = LocalizationUtil.getLocalization(buttonLabelXml, themeDisplay.getLanguageId());
    String buttonLink = LocalizationUtil.getLocalization(buttonLinkXml, themeDisplay.getLanguageId());

%>