<%@ include file="/init.jsp" %>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
	<div class="row">
		<div class="col-12">
			<div class="page-editor__collection__block">
				<div class="page-editor__collection-item page-editor__topper">
					<div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title">Albums Listing Module</p></div>
					<br><br>
					<div class="alert alert-info portlet-configuration">
						<aui:a href="<%= portletDisplay.getURLConfiguration() %>" label="please-configure-this-portlet-to-make-it-visible-to-all-users" onClick="<%= portletDisplay.getURLConfigurationJS() %>" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<% } else { %>

<%@include file="/includes/search.jsp" %>

<section class="residence-listing-sec" data-scroll-section>
	<div class="container">
		<div class="row">
			<%@include file="listing.jsp" %>
		</div>
		<div class="row">
			<div class="col-12 text-center">
				<%--<%@include file="includes/pagination.jsp" %>--%>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	window.addEventListener('load', function() {
		/*$('section.album-head-sec button.btn.dropdown-toggle.btn-light').trigger('click');
		$('section.album-head-sec select.selectpicker').selectpicker();
		$('section.album-head-sec button.btn.dropdown-toggle.btn-light').unbind("click");*/

		setTimeout(function () {
			$('.selectpicker').selectpicker('toggle');
			$('section.album-head-sec h3').trigger('click');
		}, 500);
	});
</script>

<% } %>