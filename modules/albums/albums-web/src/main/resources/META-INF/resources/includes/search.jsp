<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashSet" %><%
    /* Filters - Type, Year, Month */
    List<Album> searchAlbums = AlbumLocalServiceUtil.findByGroupId(scopeGroupId, start, end);
    HashSet<Integer> years = AlbumLocalServiceUtil.getAlbumYears(searchAlbums);
    HashSet<Integer> months = AlbumLocalServiceUtil.getAlbumMonths(searchAlbums);
    List<Category> categories = CategoryLocalServiceUtil.getCategories(scopeGroupId);
%>

<portlet:renderURL var="searchPageURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<section class="album-head-sec" data-scroll-section>
    <div class="container">
        <div class="row">

            <%@include file="top_section.jsp" %>

            <div class="col-12">
                <div class="filter-form-container">
                    <form action="<%= searchPageURL.toString() %>" id="albumfilter">
                        <div class='SelectBox'>
                            <select name="type" id="type" class="selectpicker">
                                <option value=""><%= filterAllLabel %></option>
                                <%
                                    for (Category curCategory : categories) {
                                        long categoryId = curCategory.getCategoryId();
                                        String selected = categoryId == searchType ? "selected" : "";
                                        %>
                                        <option value="<%= categoryId %>" <%= selected %>><%= curCategory.getName(locale) %></option>
                                        <%
                                    }
                                %>
                            </select>
                        </div>
                        <div class='SelectBox'>
                            <select name="year" id="year" class="selectpicker">
                                <option value=""><%= filterYearLabel %></option>
                                <%
                                    for (Integer year : years) {
                                        String selected = year.equals(searchYear) ? "selected" : "";
                                        %>
                                        <option value="<%=year%>" <%= selected %>><%=year%></option>
                                        <%
                                    }
                                %>
                            </select>
                        </div>
                        <div class='SelectBox'>
                            <select name="month" id="month" class="selectpicker">
                                <option value=""><%= filterMonthLabel %></option>
                                <%
                                    for (Integer month : months) {
                                        String selected = month.equals(searchMonth) ? "selected" : "";
                                        %>
                                        <% if(month == 1){ %>
                                            <option value="1" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_january" /></option>
                                        <% } else if(month == 2){ %>
                                            <option value="2" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_february" /></option>
                                        <% } else if(month == 3){ %>
                                            <option value="3" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_march" /></option>
                                        <% } else if(month == 4){ %>
                                            <option value="4" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_april" /></option>
                                        <% } else if(month == 5){ %>
                                            <option value="5" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_may" /></option>
                                        <% } else if(month == 6){ %>
                                            <option value="6" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_june" /></option>
                                        <% } else if(month == 7){ %>
                                            <option value="7" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_july" /></option>
                                        <% } else if(month == 8){ %>
                                            <option value="8" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_august" /></option>
                                        <% } else if(month == 9){ %>
                                            <option value="9" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_september" /></option>
                                        <% } else if(month == 10){ %>
                                            <option value="10" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_october" /></option>
                                        <% } else if(month == 11){ %>
                                            <option value="11" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_november" /></option>
                                        <% } else if(month == 12){ %>
                                            <option value="12" <%= selected %>><liferay-ui:message key="albums_web_slider_portlet_month_december" /></option>
                                        <% } %>
                                        <%
                                    }
                                %>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-outline-white filter-submit">
                            <span><%= filterButtonLabel %></span><i class="dot-line <%=themeDisplay.getLanguageId().equals("ar_SA") ? "filter-dl" : "" %>"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>