<%@ include file="/init.jsp" %>
<%
    List<Album> latestAlbums = AlbumLocalServiceUtil.findByGroupId(scopeGroupId, 0, 9);
    int count = latestAlbums.size();
%>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
    <div class="row">
        <div class="col-12">
            <div class="page-editor__collection__block">
                <div class="page-editor__collection-item page-editor__topper">
                    <div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title">Latest Albums Slider Module</p></div>
                    <br><br>
                    <div class="alert alert-info portlet-configuration">
                        <aui:a href="<%= portletDisplay.getURLConfiguration() %>" label="please-configure-this-portlet-to-make-it-visible-to-all-users" onClick="<%= portletDisplay.getURLConfigurationJS() %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } else { %>

<% if(count > 0 ) { %>

<section class="innovation-section" style="padding-bottom: 50px;" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="related-news-heading">
                    <h3><%= sectionTitlePortlet2.replaceAll("\n", "<br/>") %></h3>
                    <% if(buttonLabel != null && buttonLink != null) { %>
                        <a href="<%= buttonLink %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><%= buttonLabel %></span><i class="dot-line"></i></a>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="innovation-image-slider">
                    <div class="slider innovation-explore-slider">
                        <% for (Album curAlbum : latestAlbums){
                            String friendlyURL = "/gallery/-/albums/detail/" + curAlbum.getAlbumId();
                            String title = curAlbum.getTitle(locale).length() > 25 ? curAlbum.getTitle(locale).substring(0, 25)+"..." : curAlbum.getTitle(locale);
                            String description = curAlbum.getDescription(locale).length() > 123 ? curAlbum.getDescription(locale).substring(0, 123)+"..." : curAlbum.getDescription(locale);
                        %>
                            <div>
                                <div class="innovate-img-box">
                                    <div class="img-inno"><img src="<%= curAlbum.getListingImage() %>" alt="" class="img-fluid"></div>
                                    <div class="img-content">
                                        <h4 class="animate" data-animation="fadeInUp" data-duration="500"><a href="<%= friendlyURL %>"><%= title %></a></h4>
                                        <p class="animate" data-animation="fadeInUp" data-duration="500"><%= description %></p>
                                        <a data-animation="fadeInUp" data-duration="500" href="<%= friendlyURL %>" class="btn btn-outline-white translate-animate animate" tabindex="0"><span><liferay-ui:message key="albums_web_explore_more" /> </span><i class="dot-line"></i></a>
                                    </div>
                                </div>
                            </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<% } %>
<% } %>