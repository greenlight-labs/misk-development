<%@ page import="albums.service.GalleryLocalServiceUtil" %>
<%@ page import="albums.model.Gallery" %><%
    List<Album> albums;
    if(searchType != 0 && searchYear != 0 && searchMonth != 0){
        albums = AlbumLocalServiceUtil.findByT_Y_M(searchType, searchYear, searchMonth, start, end);
    }else if(searchType != 0 && searchYear != 0){
        albums = AlbumLocalServiceUtil.findByT_Y(searchType, searchYear, start, end);
    }else if(searchType != 0 && searchMonth != 0){
        albums = AlbumLocalServiceUtil.findByT_M(searchType, searchMonth, start, end);
    }else if(searchYear != 0 && searchMonth != 0){
        albums = AlbumLocalServiceUtil.findByY_M(searchYear, searchMonth, start, end);
    }else if(searchType != 0){
        albums = AlbumLocalServiceUtil.findByCategoryId(searchType, start, end);
    }else if(searchYear != 0){
        albums = AlbumLocalServiceUtil.findByYear(searchYear, start, end);
    }else if(searchMonth != 0){
        albums = AlbumLocalServiceUtil.findByMonth(searchMonth, start, end);
    }else{
        albums = AlbumLocalServiceUtil.findByGroupId(scopeGroupId, start, end);
    }

    if(albums.size() > 0){
        for (Album curAlbum : albums) {
            List<Gallery> gallerySlides = GalleryLocalServiceUtil.findByAlbumId(curAlbum.getAlbumId());
            int countGalleryImages = GalleryLocalServiceUtil.countGalleryImages(gallerySlides);
            int countGalleryVideos = GalleryLocalServiceUtil.countGalleryVideos(gallerySlides);
%>
<portlet:renderURL var="detailPageURL2">
    <portlet:param name="albumId" value="<%= String.valueOf(curAlbum.getAlbumId()) %>" />
    <portlet:param name="mvcPath" value="/detail.jsp" />
</portlet:renderURL>
<div class="col-12 col-md-6">
    <div class="residence-card">
        <a href="<%= detailPageURL2.toString() %>" class="img-wrapper image-overlay">
            <img src="<%= curAlbum.getListingImage() %>" alt="img" class="img-fluid">
        </a>
        <div class="res-card-info">
            <ul>
                <li><%= dateFormat.format(curAlbum.getDisplayDate()) %></li>
                <li>
                    <% if (countGalleryImages > 0) { %>
                        <%=countGalleryImages%> <liferay-ui:message key='<%=countGalleryImages == 1 ? "albums_web_listing_portlet_photo" : "albums_web_listing_portlet_photos"%>'/>
                    <% } %>
                    <% if (countGalleryVideos > 0) { %>
                        <span>|</span>
                        <%=countGalleryVideos%> <liferay-ui:message key='<%=countGalleryVideos == 1 ? "albums_web_listing_portlet_video" : "albums_web_listing_portlet_videos"%>'/>
                    <% } %>
                </li>
            </ul>
            <h3><%= curAlbum.getTitle(locale) %></h3>
            <a href="<%= detailPageURL2.toString() %>" class="btn btn-outline-white">
                <span><liferay-ui:message key="albums_web_explore_more" /></span><i class="dot-line"></i>
            </a>
        </div>
    </div>
</div>
<%
    }
} else {
%>
<div class="text-center pt-5">
    <h4><liferay-ui:message key="misk-no-record-found" /></h4>
</div>
<%
    }
%>