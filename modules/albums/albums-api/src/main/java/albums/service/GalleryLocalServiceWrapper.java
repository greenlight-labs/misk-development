/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link GalleryLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see GalleryLocalService
 * @generated
 */
public class GalleryLocalServiceWrapper
	implements GalleryLocalService, ServiceWrapper<GalleryLocalService> {

	public GalleryLocalServiceWrapper(GalleryLocalService galleryLocalService) {
		_galleryLocalService = galleryLocalService;
	}

	@Override
	public albums.model.Gallery addEntry(
			albums.model.Gallery orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws albums.exception.GalleryValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the gallery to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallery the gallery
	 * @return the gallery that was added
	 */
	@Override
	public albums.model.Gallery addGallery(albums.model.Gallery gallery) {
		return _galleryLocalService.addGallery(gallery);
	}

	@Override
	public int countByAlbumId(long albumId) {
		return _galleryLocalService.countByAlbumId(albumId);
	}

	@Override
	public int countByGroupId(long groupId) {
		return _galleryLocalService.countByGroupId(groupId);
	}

	@Override
	public int countGalleryImages(
		java.util.List<albums.model.Gallery> gallerySlides) {

		return _galleryLocalService.countGalleryImages(gallerySlides);
	}

	@Override
	public int countGalleryVideos(
		java.util.List<albums.model.Gallery> gallerySlides) {

		return _galleryLocalService.countGalleryVideos(gallerySlides);
	}

	/**
	 * Creates a new gallery with the primary key. Does not add the gallery to the database.
	 *
	 * @param slideId the primary key for the new gallery
	 * @return the new gallery
	 */
	@Override
	public albums.model.Gallery createGallery(long slideId) {
		return _galleryLocalService.createGallery(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public albums.model.Gallery deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.deleteEntry(primaryKey);
	}

	/**
	 * Deletes the gallery from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallery the gallery
	 * @return the gallery that was removed
	 */
	@Override
	public albums.model.Gallery deleteGallery(albums.model.Gallery gallery) {
		return _galleryLocalService.deleteGallery(gallery);
	}

	/**
	 * Deletes the gallery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery that was removed
	 * @throws PortalException if a gallery with the primary key could not be found
	 */
	@Override
	public albums.model.Gallery deleteGallery(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.deleteGallery(slideId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _galleryLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _galleryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _galleryLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _galleryLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _galleryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _galleryLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public albums.model.Gallery fetchGallery(long slideId) {
		return _galleryLocalService.fetchGallery(slideId);
	}

	/**
	 * Returns the gallery matching the UUID and group.
	 *
	 * @param uuid the gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching gallery, or <code>null</code> if a matching gallery could not be found
	 */
	@Override
	public albums.model.Gallery fetchGalleryByUuidAndGroupId(
		String uuid, long groupId) {

		return _galleryLocalService.fetchGalleryByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<albums.model.Gallery> findByAlbumId(long albumId) {
		return _galleryLocalService.findByAlbumId(albumId);
	}

	@Override
	public java.util.List<albums.model.Gallery> findByAlbumId(
		long albumId, int start, int end) {

		return _galleryLocalService.findByAlbumId(albumId, start, end);
	}

	@Override
	public java.util.List<albums.model.Gallery> findByAlbumId(
		long albumId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Gallery>
			obc) {

		return _galleryLocalService.findByAlbumId(albumId, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Gallery> findByGroupId(long groupId) {
		return _galleryLocalService.findByGroupId(groupId);
	}

	@Override
	public java.util.List<albums.model.Gallery> findByGroupId(
		long groupId, int start, int end) {

		return _galleryLocalService.findByGroupId(groupId, start, end);
	}

	@Override
	public java.util.List<albums.model.Gallery> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Gallery>
			obc) {

		return _galleryLocalService.findByGroupId(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _galleryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _galleryLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	/**
	 * Returns a range of all the galleries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.GalleryModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @return the range of galleries
	 */
	@Override
	public java.util.List<albums.model.Gallery> getGalleries(
		int start, int end) {

		return _galleryLocalService.getGalleries(start, end);
	}

	/**
	 * Returns all the galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the galleries
	 * @param companyId the primary key of the company
	 * @return the matching galleries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<albums.model.Gallery> getGalleriesByUuidAndCompanyId(
		String uuid, long companyId) {

		return _galleryLocalService.getGalleriesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of galleries matching the UUID and company.
	 *
	 * @param uuid the UUID of the galleries
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of galleries
	 * @param end the upper bound of the range of galleries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching galleries, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<albums.model.Gallery> getGalleriesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Gallery>
			orderByComparator) {

		return _galleryLocalService.getGalleriesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of galleries.
	 *
	 * @return the number of galleries
	 */
	@Override
	public int getGalleriesCount() {
		return _galleryLocalService.getGalleriesCount();
	}

	/**
	 * Returns the gallery with the primary key.
	 *
	 * @param slideId the primary key of the gallery
	 * @return the gallery
	 * @throws PortalException if a gallery with the primary key could not be found
	 */
	@Override
	public albums.model.Gallery getGallery(long slideId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.getGallery(slideId);
	}

	/**
	 * Returns the gallery matching the UUID and group.
	 *
	 * @param uuid the gallery's UUID
	 * @param groupId the primary key of the group
	 * @return the matching gallery
	 * @throws PortalException if a matching gallery could not be found
	 */
	@Override
	public albums.model.Gallery getGalleryByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.getGalleryByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public albums.model.Gallery getGalleryFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws albums.exception.GalleryValidateException,
			   javax.portlet.PortletException {

		return _galleryLocalService.getGalleryFromRequest(primaryKey, request);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _galleryLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public albums.model.Gallery getNewObject(long primaryKey) {
		return _galleryLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _galleryLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public albums.model.Gallery updateEntry(
			albums.model.Gallery orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws albums.exception.GalleryValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _galleryLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the gallery in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect GalleryLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param gallery the gallery
	 * @return the gallery that was updated
	 */
	@Override
	public albums.model.Gallery updateGallery(albums.model.Gallery gallery) {
		return _galleryLocalService.updateGallery(gallery);
	}

	@Override
	public GalleryLocalService getWrappedService() {
		return _galleryLocalService;
	}

	@Override
	public void setWrappedService(GalleryLocalService galleryLocalService) {
		_galleryLocalService = galleryLocalService;
	}

	private GalleryLocalService _galleryLocalService;

}