/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AlbumLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AlbumLocalService
 * @generated
 */
public class AlbumLocalServiceWrapper
	implements AlbumLocalService, ServiceWrapper<AlbumLocalService> {

	public AlbumLocalServiceWrapper(AlbumLocalService albumLocalService) {
		_albumLocalService = albumLocalService;
	}

	/**
	 * Adds the album to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was added
	 */
	@Override
	public albums.model.Album addAlbum(albums.model.Album album) {
		return _albumLocalService.addAlbum(album);
	}

	@Override
	public albums.model.Album addEntry(
			albums.model.Album orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws albums.exception.AlbumValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.addEntry(orgEntry, serviceContext);
	}

	@Override
	public void addEntryGallery(
			albums.model.Album orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		_albumLocalService.addEntryGallery(orgEntry, request);
	}

	@Override
	public int countByCategoryId(long type) {
		return _albumLocalService.countByCategoryId(type);
	}

	@Override
	public int countByGroupId(long groupId) {
		return _albumLocalService.countByGroupId(groupId);
	}

	@Override
	public int countByMonth(int month) {
		return _albumLocalService.countByMonth(month);
	}

	@Override
	public int countByT_M(long type, int month) {
		return _albumLocalService.countByT_M(type, month);
	}

	@Override
	public int countByT_Y(long type, int year) {
		return _albumLocalService.countByT_Y(type, year);
	}

	@Override
	public int countByT_Y_M(long type, int year, int month) {
		return _albumLocalService.countByT_Y_M(type, year, month);
	}

	@Override
	public int countByY_M(int year, int month) {
		return _albumLocalService.countByY_M(year, month);
	}

	@Override
	public int countByYear(int year) {
		return _albumLocalService.countByYear(year);
	}

	/**
	 * Creates a new album with the primary key. Does not add the album to the database.
	 *
	 * @param albumId the primary key for the new album
	 * @return the new album
	 */
	@Override
	public albums.model.Album createAlbum(long albumId) {
		return _albumLocalService.createAlbum(albumId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the album from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was removed
	 */
	@Override
	public albums.model.Album deleteAlbum(albums.model.Album album) {
		return _albumLocalService.deleteAlbum(album);
	}

	/**
	 * Deletes the album with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param albumId the primary key of the album
	 * @return the album that was removed
	 * @throws PortalException if a album with the primary key could not be found
	 */
	@Override
	public albums.model.Album deleteAlbum(long albumId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.deleteAlbum(albumId);
	}

	@Override
	public albums.model.Album deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _albumLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _albumLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _albumLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _albumLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _albumLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _albumLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public albums.model.Album fetchAlbum(long albumId) {
		return _albumLocalService.fetchAlbum(albumId);
	}

	/**
	 * Returns the album matching the UUID and group.
	 *
	 * @param uuid the album's UUID
	 * @param groupId the primary key of the group
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public albums.model.Album fetchAlbumByUuidAndGroupId(
		String uuid, long groupId) {

		return _albumLocalService.fetchAlbumByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public java.util.List<albums.model.Album> findByCategoryId(long type) {
		return _albumLocalService.findByCategoryId(type);
	}

	@Override
	public java.util.List<albums.model.Album> findByCategoryId(
		long type, int start, int end) {

		return _albumLocalService.findByCategoryId(type, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByCategoryId(
		long type, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByCategoryId(type, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Album> findByGroupId(long groupId) {
		return _albumLocalService.findByGroupId(groupId);
	}

	@Override
	public java.util.List<albums.model.Album> findByGroupId(
		long groupId, int start, int end) {

		return _albumLocalService.findByGroupId(groupId, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByGroupId(groupId, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Album> findByMonth(int month) {
		return _albumLocalService.findByMonth(month);
	}

	@Override
	public java.util.List<albums.model.Album> findByMonth(
		int month, int start, int end) {

		return _albumLocalService.findByMonth(month, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByMonth(
		int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByMonth(month, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_M(long type, int month) {
		return _albumLocalService.findByT_M(type, month);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_M(
		long type, int month, int start, int end) {

		return _albumLocalService.findByT_M(type, month, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_M(
		long type, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByT_M(type, month, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_Y(long type, int year) {
		return _albumLocalService.findByT_Y(type, year);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_Y(
		long type, int year, int start, int end) {

		return _albumLocalService.findByT_Y(type, year, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_Y(
		long type, int year, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByT_Y(type, year, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_Y_M(
		long type, int year, int month) {

		return _albumLocalService.findByT_Y_M(type, year, month);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_Y_M(
		long type, int year, int month, int start, int end) {

		return _albumLocalService.findByT_Y_M(type, year, month, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByT_Y_M(
		long type, int year, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByT_Y_M(
			type, year, month, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Album> findByY_M(int year, int month) {
		return _albumLocalService.findByY_M(year, month);
	}

	@Override
	public java.util.List<albums.model.Album> findByY_M(
		int year, int month, int start, int end) {

		return _albumLocalService.findByY_M(year, month, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByY_M(
		int year, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByY_M(year, month, start, end, obc);
	}

	@Override
	public java.util.List<albums.model.Album> findByYear(int year) {
		return _albumLocalService.findByYear(year);
	}

	@Override
	public java.util.List<albums.model.Album> findByYear(
		int year, int start, int end) {

		return _albumLocalService.findByYear(year, start, end);
	}

	@Override
	public java.util.List<albums.model.Album> findByYear(
		int year, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			obc) {

		return _albumLocalService.findByYear(year, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _albumLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the album with the primary key.
	 *
	 * @param albumId the primary key of the album
	 * @return the album
	 * @throws PortalException if a album with the primary key could not be found
	 */
	@Override
	public albums.model.Album getAlbum(long albumId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.getAlbum(albumId);
	}

	/**
	 * Returns the album matching the UUID and group.
	 *
	 * @param uuid the album's UUID
	 * @param groupId the primary key of the group
	 * @return the matching album
	 * @throws PortalException if a matching album could not be found
	 */
	@Override
	public albums.model.Album getAlbumByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.getAlbumByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public albums.model.Album getAlbumFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws albums.exception.AlbumValidateException,
			   javax.portlet.PortletException {

		return _albumLocalService.getAlbumFromRequest(primaryKey, request);
	}

	@Override
	public java.util.HashSet<Integer> getAlbumMonths(
		java.util.List<albums.model.Album> albums) {

		return _albumLocalService.getAlbumMonths(albums);
	}

	/**
	 * Returns a range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of albums
	 */
	@Override
	public java.util.List<albums.model.Album> getAlbums(int start, int end) {
		return _albumLocalService.getAlbums(start, end);
	}

	/**
	 * Returns all the albums matching the UUID and company.
	 *
	 * @param uuid the UUID of the albums
	 * @param companyId the primary key of the company
	 * @return the matching albums, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<albums.model.Album> getAlbumsByUuidAndCompanyId(
		String uuid, long companyId) {

		return _albumLocalService.getAlbumsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of albums matching the UUID and company.
	 *
	 * @param uuid the UUID of the albums
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching albums, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<albums.model.Album> getAlbumsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<albums.model.Album>
			orderByComparator) {

		return _albumLocalService.getAlbumsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of albums.
	 *
	 * @return the number of albums
	 */
	@Override
	public int getAlbumsCount() {
		return _albumLocalService.getAlbumsCount();
	}

	@Override
	public java.util.HashSet<Integer> getAlbumYears(
		java.util.List<albums.model.Album> albums) {

		return _albumLocalService.getAlbumYears(albums);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _albumLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public java.util.List<albums.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return _albumLocalService.getGalleries(actionRequest);
	}

	@Override
	public java.util.List<albums.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		java.util.List<albums.model.Gallery> defaultGalleries) {

		return _albumLocalService.getGalleries(actionRequest, defaultGalleries);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _albumLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public albums.model.Album getNewObject(long primaryKey) {
		return _albumLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _albumLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the album in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was updated
	 */
	@Override
	public albums.model.Album updateAlbum(albums.model.Album album) {
		return _albumLocalService.updateAlbum(album);
	}

	@Override
	public albums.model.Album updateEntry(
			albums.model.Album orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws albums.exception.AlbumValidateException,
			   com.liferay.portal.kernel.exception.PortalException {

		return _albumLocalService.updateEntry(orgEntry, serviceContext);
	}

	@Override
	public void updateGalleries(
			albums.model.Album entry,
			java.util.List<albums.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		_albumLocalService.updateGalleries(entry, galleries, serviceContext);
	}

	@Override
	public AlbumLocalService getWrappedService() {
		return _albumLocalService;
	}

	@Override
	public void setWrappedService(AlbumLocalService albumLocalService) {
		_albumLocalService = albumLocalService;
	}

	private AlbumLocalService _albumLocalService;

}