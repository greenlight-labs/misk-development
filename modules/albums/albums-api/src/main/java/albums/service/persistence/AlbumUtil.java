/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service.persistence;

import albums.model.Album;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the album service. This utility wraps <code>albums.service.persistence.impl.AlbumPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AlbumPersistence
 * @generated
 */
public class AlbumUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Album album) {
		getPersistence().clearCache(album);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Album> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Album> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Album> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Album> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Album update(Album album) {
		return getPersistence().update(album);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Album update(Album album, ServiceContext serviceContext) {
		return getPersistence().update(album, serviceContext);
	}

	/**
	 * Returns all the albums where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching albums
	 */
	public static List<Album> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByUuid_First(
			String uuid, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByUuid_First(
		String uuid, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByUuid_Last(
			String uuid, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByUuid_Last(
		String uuid, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where uuid = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByUuid_PrevAndNext(
			long albumId, String uuid,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByUuid_PrevAndNext(
			albumId, uuid, orderByComparator);
	}

	/**
	 * Removes all the albums where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of albums where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching albums
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAlbumException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByUUID_G(String uuid, long groupId)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the album where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the album that was removed
	 */
	public static Album removeByUUID_G(String uuid, long groupId)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of albums where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching albums
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching albums
	 */
	public static List<Album> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByUuid_C_PrevAndNext(
			long albumId, String uuid, long companyId,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByUuid_C_PrevAndNext(
			albumId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the albums where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of albums where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching albums
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the albums where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching albums
	 */
	public static List<Album> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByGroupId(long groupId, int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByGroupId_First(
			long groupId, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByGroupId_First(
		long groupId, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByGroupId_Last(
			long groupId, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByGroupId_Last(
		long groupId, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where groupId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByGroupId_PrevAndNext(
			long albumId, long groupId,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByGroupId_PrevAndNext(
			albumId, groupId, orderByComparator);
	}

	/**
	 * Removes all the albums where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of albums where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching albums
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the albums where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching albums
	 */
	public static List<Album> findByCategoryId(long categoryId) {
		return getPersistence().findByCategoryId(categoryId);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByCategoryId(
		long categoryId, int start, int end) {

		return getPersistence().findByCategoryId(categoryId, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByCategoryId_First(
			long categoryId, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByCategoryId_First(
		long categoryId, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByCategoryId_Last(
			long categoryId, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByCategoryId_Last(
		long categoryId, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByCategoryId_PrevAndNext(
			long albumId, long categoryId,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByCategoryId_PrevAndNext(
			albumId, categoryId, orderByComparator);
	}

	/**
	 * Removes all the albums where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public static void removeByCategoryId(long categoryId) {
		getPersistence().removeByCategoryId(categoryId);
	}

	/**
	 * Returns the number of albums where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching albums
	 */
	public static int countByCategoryId(long categoryId) {
		return getPersistence().countByCategoryId(categoryId);
	}

	/**
	 * Returns all the albums where year = &#63;.
	 *
	 * @param year the year
	 * @return the matching albums
	 */
	public static List<Album> findByYear(int year) {
		return getPersistence().findByYear(year);
	}

	/**
	 * Returns a range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByYear(int year, int start, int end) {
		return getPersistence().findByYear(year, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByYear(
		int year, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByYear(year, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByYear(
		int year, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByYear(
			year, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByYear_First(
			int year, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByYear_First(year, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByYear_First(
		int year, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByYear_First(year, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByYear_Last(
			int year, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByYear_Last(year, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByYear_Last(
		int year, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByYear_Last(year, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where year = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByYear_PrevAndNext(
			long albumId, int year, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByYear_PrevAndNext(
			albumId, year, orderByComparator);
	}

	/**
	 * Removes all the albums where year = &#63; from the database.
	 *
	 * @param year the year
	 */
	public static void removeByYear(int year) {
		getPersistence().removeByYear(year);
	}

	/**
	 * Returns the number of albums where year = &#63;.
	 *
	 * @param year the year
	 * @return the number of matching albums
	 */
	public static int countByYear(int year) {
		return getPersistence().countByYear(year);
	}

	/**
	 * Returns all the albums where month = &#63;.
	 *
	 * @param month the month
	 * @return the matching albums
	 */
	public static List<Album> findByMonth(int month) {
		return getPersistence().findByMonth(month);
	}

	/**
	 * Returns a range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByMonth(int month, int start, int end) {
		return getPersistence().findByMonth(month, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByMonth(
		int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByMonth(
			month, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByMonth(
		int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByMonth(
			month, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByMonth_First(
			int month, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByMonth_First(month, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByMonth_First(
		int month, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByMonth_First(month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByMonth_Last(
			int month, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByMonth_Last(month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByMonth_Last(
		int month, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByMonth_Last(month, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByMonth_PrevAndNext(
			long albumId, int month, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByMonth_PrevAndNext(
			albumId, month, orderByComparator);
	}

	/**
	 * Removes all the albums where month = &#63; from the database.
	 *
	 * @param month the month
	 */
	public static void removeByMonth(int month) {
		getPersistence().removeByMonth(month);
	}

	/**
	 * Returns the number of albums where month = &#63;.
	 *
	 * @param month the month
	 * @return the number of matching albums
	 */
	public static int countByMonth(int month) {
		return getPersistence().countByMonth(month);
	}

	/**
	 * Returns all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @return the matching albums
	 */
	public static List<Album> findByT_Y_M(
		long categoryId, int year, int month) {

		return getPersistence().findByT_Y_M(categoryId, year, month);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end) {

		return getPersistence().findByT_Y_M(
			categoryId, year, month, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByT_Y_M(
			categoryId, year, month, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByT_Y_M(
			categoryId, year, month, start, end, orderByComparator,
			useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByT_Y_M_First(
			long categoryId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_Y_M_First(
			categoryId, year, month, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByT_Y_M_First(
		long categoryId, int year, int month,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByT_Y_M_First(
			categoryId, year, month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByT_Y_M_Last(
			long categoryId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_Y_M_Last(
			categoryId, year, month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByT_Y_M_Last(
		long categoryId, int year, int month,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByT_Y_M_Last(
			categoryId, year, month, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByT_Y_M_PrevAndNext(
			long albumId, long categoryId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_Y_M_PrevAndNext(
			albumId, categoryId, year, month, orderByComparator);
	}

	/**
	 * Removes all the albums where categoryId = &#63; and year = &#63; and month = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 */
	public static void removeByT_Y_M(long categoryId, int year, int month) {
		getPersistence().removeByT_Y_M(categoryId, year, month);
	}

	/**
	 * Returns the number of albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @return the number of matching albums
	 */
	public static int countByT_Y_M(long categoryId, int year, int month) {
		return getPersistence().countByT_Y_M(categoryId, year, month);
	}

	/**
	 * Returns all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @return the matching albums
	 */
	public static List<Album> findByT_Y(long categoryId, int year) {
		return getPersistence().findByT_Y(categoryId, year);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByT_Y(
		long categoryId, int year, int start, int end) {

		return getPersistence().findByT_Y(categoryId, year, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByT_Y(
		long categoryId, int year, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByT_Y(
			categoryId, year, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByT_Y(
		long categoryId, int year, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByT_Y(
			categoryId, year, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByT_Y_First(
			long categoryId, int year,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_Y_First(
			categoryId, year, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByT_Y_First(
		long categoryId, int year, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByT_Y_First(
			categoryId, year, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByT_Y_Last(
			long categoryId, int year,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_Y_Last(
			categoryId, year, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByT_Y_Last(
		long categoryId, int year, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByT_Y_Last(
			categoryId, year, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByT_Y_PrevAndNext(
			long albumId, long categoryId, int year,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_Y_PrevAndNext(
			albumId, categoryId, year, orderByComparator);
	}

	/**
	 * Removes all the albums where categoryId = &#63; and year = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 */
	public static void removeByT_Y(long categoryId, int year) {
		getPersistence().removeByT_Y(categoryId, year);
	}

	/**
	 * Returns the number of albums where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @return the number of matching albums
	 */
	public static int countByT_Y(long categoryId, int year) {
		return getPersistence().countByT_Y(categoryId, year);
	}

	/**
	 * Returns all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @return the matching albums
	 */
	public static List<Album> findByT_M(long categoryId, int month) {
		return getPersistence().findByT_M(categoryId, month);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByT_M(
		long categoryId, int month, int start, int end) {

		return getPersistence().findByT_M(categoryId, month, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByT_M(
		long categoryId, int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByT_M(
			categoryId, month, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByT_M(
		long categoryId, int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByT_M(
			categoryId, month, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByT_M_First(
			long categoryId, int month,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_M_First(
			categoryId, month, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByT_M_First(
		long categoryId, int month,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByT_M_First(
			categoryId, month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByT_M_Last(
			long categoryId, int month,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_M_Last(
			categoryId, month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByT_M_Last(
		long categoryId, int month,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByT_M_Last(
			categoryId, month, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByT_M_PrevAndNext(
			long albumId, long categoryId, int month,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByT_M_PrevAndNext(
			albumId, categoryId, month, orderByComparator);
	}

	/**
	 * Removes all the albums where categoryId = &#63; and month = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 */
	public static void removeByT_M(long categoryId, int month) {
		getPersistence().removeByT_M(categoryId, month);
	}

	/**
	 * Returns the number of albums where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @return the number of matching albums
	 */
	public static int countByT_M(long categoryId, int month) {
		return getPersistence().countByT_M(categoryId, month);
	}

	/**
	 * Returns all the albums where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @return the matching albums
	 */
	public static List<Album> findByY_M(int year, int month) {
		return getPersistence().findByY_M(year, month);
	}

	/**
	 * Returns a range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public static List<Album> findByY_M(
		int year, int month, int start, int end) {

		return getPersistence().findByY_M(year, month, start, end);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByY_M(
		int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getPersistence().findByY_M(
			year, month, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public static List<Album> findByY_M(
		int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByY_M(
			year, month, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByY_M_First(
			int year, int month, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByY_M_First(year, month, orderByComparator);
	}

	/**
	 * Returns the first album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByY_M_First(
		int year, int month, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByY_M_First(
			year, month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public static Album findByY_M_Last(
			int year, int month, OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByY_M_Last(year, month, orderByComparator);
	}

	/**
	 * Returns the last album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchByY_M_Last(
		int year, int month, OrderByComparator<Album> orderByComparator) {

		return getPersistence().fetchByY_M_Last(year, month, orderByComparator);
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album[] findByY_M_PrevAndNext(
			long albumId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByY_M_PrevAndNext(
			albumId, year, month, orderByComparator);
	}

	/**
	 * Removes all the albums where year = &#63; and month = &#63; from the database.
	 *
	 * @param year the year
	 * @param month the month
	 */
	public static void removeByY_M(int year, int month) {
		getPersistence().removeByY_M(year, month);
	}

	/**
	 * Returns the number of albums where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @return the number of matching albums
	 */
	public static int countByY_M(int year, int month) {
		return getPersistence().countByY_M(year, month);
	}

	/**
	 * Caches the album in the entity cache if it is enabled.
	 *
	 * @param album the album
	 */
	public static void cacheResult(Album album) {
		getPersistence().cacheResult(album);
	}

	/**
	 * Caches the albums in the entity cache if it is enabled.
	 *
	 * @param albums the albums
	 */
	public static void cacheResult(List<Album> albums) {
		getPersistence().cacheResult(albums);
	}

	/**
	 * Creates a new album with the primary key. Does not add the album to the database.
	 *
	 * @param albumId the primary key for the new album
	 * @return the new album
	 */
	public static Album create(long albumId) {
		return getPersistence().create(albumId);
	}

	/**
	 * Removes the album with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param albumId the primary key of the album
	 * @return the album that was removed
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album remove(long albumId)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().remove(albumId);
	}

	public static Album updateImpl(Album album) {
		return getPersistence().updateImpl(album);
	}

	/**
	 * Returns the album with the primary key or throws a <code>NoSuchAlbumException</code> if it could not be found.
	 *
	 * @param albumId the primary key of the album
	 * @return the album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public static Album findByPrimaryKey(long albumId)
		throws albums.exception.NoSuchAlbumException {

		return getPersistence().findByPrimaryKey(albumId);
	}

	/**
	 * Returns the album with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param albumId the primary key of the album
	 * @return the album, or <code>null</code> if a album with the primary key could not be found
	 */
	public static Album fetchByPrimaryKey(long albumId) {
		return getPersistence().fetchByPrimaryKey(albumId);
	}

	/**
	 * Returns all the albums.
	 *
	 * @return the albums
	 */
	public static List<Album> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of albums
	 */
	public static List<Album> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of albums
	 */
	public static List<Album> findAll(
		int start, int end, OrderByComparator<Album> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of albums
	 */
	public static List<Album> findAll(
		int start, int end, OrderByComparator<Album> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the albums from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of albums.
	 *
	 * @return the number of albums
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AlbumPersistence getPersistence() {
		return _persistence;
	}

	private static volatile AlbumPersistence _persistence;

}