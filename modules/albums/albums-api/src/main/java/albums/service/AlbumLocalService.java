/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service;

import albums.exception.AlbumValidateException;

import albums.model.Album;
import albums.model.Gallery;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.*;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Album. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see AlbumLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface AlbumLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>albums.service.impl.AlbumLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the album local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link AlbumLocalServiceUtil} if injection and service tracking are not available.
	 */

	/**
	 * Adds the album to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Album addAlbum(Album album);

	public Album addEntry(Album orgEntry, ServiceContext serviceContext)
		throws AlbumValidateException, PortalException;

	public void addEntryGallery(Album orgEntry, PortletRequest request)
		throws PortletException;

	public int countByCategoryId(long type);

	public int countByGroupId(long groupId);

	public int countByMonth(int month);

	public int countByT_M(long type, int month);

	public int countByT_Y(long type, int year);

	public int countByT_Y_M(long type, int year, int month);

	public int countByY_M(int year, int month);

	public int countByYear(int year);

	/**
	 * Creates a new album with the primary key. Does not add the album to the database.
	 *
	 * @param albumId the primary key for the new album
	 * @return the new album
	 */
	@Transactional(enabled = false)
	public Album createAlbum(long albumId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Deletes the album from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Album deleteAlbum(Album album);

	/**
	 * Deletes the album with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param albumId the primary key of the album
	 * @return the album that was removed
	 * @throws PortalException if a album with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Album deleteAlbum(long albumId) throws PortalException;

	public Album deleteEntry(long primaryKey) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Album fetchAlbum(long albumId);

	/**
	 * Returns the album matching the UUID and group.
	 *
	 * @param uuid the album's UUID
	 * @param groupId the primary key of the group
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Album fetchAlbumByUuidAndGroupId(String uuid, long groupId);

	public List<Album> findByCategoryId(long type);

	public List<Album> findByCategoryId(long type, int start, int end);

	public List<Album> findByCategoryId(
		long type, int start, int end, OrderByComparator<Album> obc);

	public List<Album> findByGroupId(long groupId);

	public List<Album> findByGroupId(long groupId, int start, int end);

	public List<Album> findByGroupId(
		long groupId, int start, int end, OrderByComparator<Album> obc);

	public List<Album> findByMonth(int month);

	public List<Album> findByMonth(int month, int start, int end);

	public List<Album> findByMonth(
		int month, int start, int end, OrderByComparator<Album> obc);

	public List<Album> findByT_M(long type, int month);

	public List<Album> findByT_M(long type, int month, int start, int end);

	public List<Album> findByT_M(
		long type, int month, int start, int end, OrderByComparator<Album> obc);

	public List<Album> findByT_Y(long type, int year);

	public List<Album> findByT_Y(long type, int year, int start, int end);

	public List<Album> findByT_Y(
		long type, int year, int start, int end, OrderByComparator<Album> obc);

	public List<Album> findByT_Y_M(long type, int year, int month);

	public List<Album> findByT_Y_M(
		long type, int year, int month, int start, int end);

	public List<Album> findByT_Y_M(
		long type, int year, int month, int start, int end,
		OrderByComparator<Album> obc);

	public List<Album> findByY_M(int year, int month);

	public List<Album> findByY_M(int year, int month, int start, int end);

	public List<Album> findByY_M(
		int year, int month, int start, int end, OrderByComparator<Album> obc);

	public List<Album> findByYear(int year);

	public List<Album> findByYear(int year, int start, int end);

	public List<Album> findByYear(
		int year, int start, int end, OrderByComparator<Album> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	 * Returns the album with the primary key.
	 *
	 * @param albumId the primary key of the album
	 * @return the album
	 * @throws PortalException if a album with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Album getAlbum(long albumId) throws PortalException;

	/**
	 * Returns the album matching the UUID and group.
	 *
	 * @param uuid the album's UUID
	 * @param groupId the primary key of the group
	 * @return the matching album
	 * @throws PortalException if a matching album could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Album getAlbumByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Album getAlbumFromRequest(long primaryKey, PortletRequest request)
		throws AlbumValidateException, PortletException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.HashSet<Integer> getAlbumMonths(List<Album> albums);

	/**
	 * Returns a range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of albums
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Album> getAlbums(int start, int end);

	/**
	 * Returns all the albums matching the UUID and company.
	 *
	 * @param uuid the UUID of the albums
	 * @param companyId the primary key of the company
	 * @return the matching albums, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Album> getAlbumsByUuidAndCompanyId(String uuid, long companyId);

	/**
	 * Returns a range of albums matching the UUID and company.
	 *
	 * @param uuid the UUID of the albums
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching albums, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Album> getAlbumsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Album> orderByComparator);

	/**
	 * Returns the number of albums.
	 *
	 * @return the number of albums
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getAlbumsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.util.HashSet<Integer> getAlbumYears(List<Album> albums);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallery> getGalleries(ActionRequest actionRequest);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Gallery> getGalleries(
		ActionRequest actionRequest, List<Gallery> defaultGalleries);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Album getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	 * Updates the album in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Album updateAlbum(Album album);

	public Album updateEntry(Album orgEntry, ServiceContext serviceContext)
		throws AlbumValidateException, PortalException;

	public void updateGalleries(
			Album entry, List<Gallery> galleries, ServiceContext serviceContext)
		throws PortalException;

}