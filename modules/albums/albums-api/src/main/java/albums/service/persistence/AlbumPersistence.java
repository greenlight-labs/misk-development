/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service.persistence;

import albums.exception.NoSuchAlbumException;

import albums.model.Album;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the album service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AlbumUtil
 * @generated
 */
@ProviderType
public interface AlbumPersistence extends BasePersistence<Album> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link AlbumUtil} to access the album persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the albums where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching albums
	 */
	public java.util.List<Album> findByUuid(String uuid);

	/**
	 * Returns a range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where uuid = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByUuid_PrevAndNext(
			long albumId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of albums where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching albums
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAlbumException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByUUID_G(String uuid, long groupId)
		throws NoSuchAlbumException;

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the album where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the album that was removed
	 */
	public Album removeByUUID_G(String uuid, long groupId)
		throws NoSuchAlbumException;

	/**
	 * Returns the number of albums where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching albums
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching albums
	 */
	public java.util.List<Album> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByUuid_C_PrevAndNext(
			long albumId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of albums where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching albums
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the albums where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching albums
	 */
	public java.util.List<Album> findByGroupId(long groupId);

	/**
	 * Returns a range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where groupId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByGroupId_PrevAndNext(
			long albumId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of albums where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching albums
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the albums where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching albums
	 */
	public java.util.List<Album> findByCategoryId(long categoryId);

	/**
	 * Returns a range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByCategoryId(
		long categoryId, int start, int end);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByCategoryId_First(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByCategoryId_First(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByCategoryId_Last(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByCategoryId_Last(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByCategoryId_PrevAndNext(
			long albumId, long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public void removeByCategoryId(long categoryId);

	/**
	 * Returns the number of albums where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching albums
	 */
	public int countByCategoryId(long categoryId);

	/**
	 * Returns all the albums where year = &#63;.
	 *
	 * @param year the year
	 * @return the matching albums
	 */
	public java.util.List<Album> findByYear(int year);

	/**
	 * Returns a range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByYear(int year, int start, int end);

	/**
	 * Returns an ordered range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByYear(
		int year, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByYear(
		int year, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByYear_First(
			int year,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByYear_First(
		int year,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByYear_Last(
			int year,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByYear_Last(
		int year,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where year = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByYear_PrevAndNext(
			long albumId, int year,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where year = &#63; from the database.
	 *
	 * @param year the year
	 */
	public void removeByYear(int year);

	/**
	 * Returns the number of albums where year = &#63;.
	 *
	 * @param year the year
	 * @return the number of matching albums
	 */
	public int countByYear(int year);

	/**
	 * Returns all the albums where month = &#63;.
	 *
	 * @param month the month
	 * @return the matching albums
	 */
	public java.util.List<Album> findByMonth(int month);

	/**
	 * Returns a range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByMonth(int month, int start, int end);

	/**
	 * Returns an ordered range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByMonth(
		int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByMonth(
		int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByMonth_First(
			int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByMonth_First(
		int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByMonth_Last(
			int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByMonth_Last(
		int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByMonth_PrevAndNext(
			long albumId, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where month = &#63; from the database.
	 *
	 * @param month the month
	 */
	public void removeByMonth(int month);

	/**
	 * Returns the number of albums where month = &#63;.
	 *
	 * @param month the month
	 * @return the number of matching albums
	 */
	public int countByMonth(int month);

	/**
	 * Returns all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @return the matching albums
	 */
	public java.util.List<Album> findByT_Y_M(
		long categoryId, int year, int month);

	/**
	 * Returns a range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByT_Y_M_First(
			long categoryId, int year, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByT_Y_M_First(
		long categoryId, int year, int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByT_Y_M_Last(
			long categoryId, int year, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByT_Y_M_Last(
		long categoryId, int year, int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByT_Y_M_PrevAndNext(
			long albumId, long categoryId, int year, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where categoryId = &#63; and year = &#63; and month = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 */
	public void removeByT_Y_M(long categoryId, int year, int month);

	/**
	 * Returns the number of albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @return the number of matching albums
	 */
	public int countByT_Y_M(long categoryId, int year, int month);

	/**
	 * Returns all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @return the matching albums
	 */
	public java.util.List<Album> findByT_Y(long categoryId, int year);

	/**
	 * Returns a range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByT_Y(
		long categoryId, int year, int start, int end);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByT_Y(
		long categoryId, int year, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByT_Y(
		long categoryId, int year, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByT_Y_First(
			long categoryId, int year,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByT_Y_First(
		long categoryId, int year,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByT_Y_Last(
			long categoryId, int year,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByT_Y_Last(
		long categoryId, int year,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByT_Y_PrevAndNext(
			long albumId, long categoryId, int year,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where categoryId = &#63; and year = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 */
	public void removeByT_Y(long categoryId, int year);

	/**
	 * Returns the number of albums where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @return the number of matching albums
	 */
	public int countByT_Y(long categoryId, int year);

	/**
	 * Returns all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @return the matching albums
	 */
	public java.util.List<Album> findByT_M(long categoryId, int month);

	/**
	 * Returns a range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByT_M(
		long categoryId, int month, int start, int end);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByT_M(
		long categoryId, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByT_M(
		long categoryId, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByT_M_First(
			long categoryId, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByT_M_First(
		long categoryId, int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByT_M_Last(
			long categoryId, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByT_M_Last(
		long categoryId, int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByT_M_PrevAndNext(
			long albumId, long categoryId, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where categoryId = &#63; and month = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 */
	public void removeByT_M(long categoryId, int month);

	/**
	 * Returns the number of albums where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @return the number of matching albums
	 */
	public int countByT_M(long categoryId, int month);

	/**
	 * Returns all the albums where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @return the matching albums
	 */
	public java.util.List<Album> findByY_M(int year, int month);

	/**
	 * Returns a range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	public java.util.List<Album> findByY_M(
		int year, int month, int start, int end);

	/**
	 * Returns an ordered range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByY_M(
		int year, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	public java.util.List<Album> findByY_M(
		int year, int month, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByY_M_First(
			int year, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the first album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByY_M_First(
		int year, int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the last album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	public Album findByY_M_Last(
			int year, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Returns the last album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	public Album fetchByY_M_Last(
		int year, int month,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns the albums before and after the current album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album[] findByY_M_PrevAndNext(
			long albumId, int year, int month,
			com.liferay.portal.kernel.util.OrderByComparator<Album>
				orderByComparator)
		throws NoSuchAlbumException;

	/**
	 * Removes all the albums where year = &#63; and month = &#63; from the database.
	 *
	 * @param year the year
	 * @param month the month
	 */
	public void removeByY_M(int year, int month);

	/**
	 * Returns the number of albums where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @return the number of matching albums
	 */
	public int countByY_M(int year, int month);

	/**
	 * Caches the album in the entity cache if it is enabled.
	 *
	 * @param album the album
	 */
	public void cacheResult(Album album);

	/**
	 * Caches the albums in the entity cache if it is enabled.
	 *
	 * @param albums the albums
	 */
	public void cacheResult(java.util.List<Album> albums);

	/**
	 * Creates a new album with the primary key. Does not add the album to the database.
	 *
	 * @param albumId the primary key for the new album
	 * @return the new album
	 */
	public Album create(long albumId);

	/**
	 * Removes the album with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param albumId the primary key of the album
	 * @return the album that was removed
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album remove(long albumId) throws NoSuchAlbumException;

	public Album updateImpl(Album album);

	/**
	 * Returns the album with the primary key or throws a <code>NoSuchAlbumException</code> if it could not be found.
	 *
	 * @param albumId the primary key of the album
	 * @return the album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	public Album findByPrimaryKey(long albumId) throws NoSuchAlbumException;

	/**
	 * Returns the album with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param albumId the primary key of the album
	 * @return the album, or <code>null</code> if a album with the primary key could not be found
	 */
	public Album fetchByPrimaryKey(long albumId);

	/**
	 * Returns all the albums.
	 *
	 * @return the albums
	 */
	public java.util.List<Album> findAll();

	/**
	 * Returns a range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of albums
	 */
	public java.util.List<Album> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of albums
	 */
	public java.util.List<Album> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator);

	/**
	 * Returns an ordered range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of albums
	 */
	public java.util.List<Album> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Album>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the albums from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of albums.
	 *
	 * @return the number of albums
	 */
	public int countAll();

}