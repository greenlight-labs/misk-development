/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service;

import albums.model.Album;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Album. This utility wraps
 * <code>albums.service.impl.AlbumLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AlbumLocalService
 * @generated
 */
public class AlbumLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>albums.service.impl.AlbumLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the album to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was added
	 */
	public static Album addAlbum(Album album) {
		return getService().addAlbum(album);
	}

	public static Album addEntry(
			Album orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws albums.exception.AlbumValidateException, PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	public static void addEntryGallery(
			Album orgEntry, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException {

		getService().addEntryGallery(orgEntry, request);
	}

	public static int countByCategoryId(long type) {
		return getService().countByCategoryId(type);
	}

	public static int countByGroupId(long groupId) {
		return getService().countByGroupId(groupId);
	}

	public static int countByMonth(int month) {
		return getService().countByMonth(month);
	}

	public static int countByT_M(long type, int month) {
		return getService().countByT_M(type, month);
	}

	public static int countByT_Y(long type, int year) {
		return getService().countByT_Y(type, year);
	}

	public static int countByT_Y_M(long type, int year, int month) {
		return getService().countByT_Y_M(type, year, month);
	}

	public static int countByY_M(int year, int month) {
		return getService().countByY_M(year, month);
	}

	public static int countByYear(int year) {
		return getService().countByYear(year);
	}

	/**
	 * Creates a new album with the primary key. Does not add the album to the database.
	 *
	 * @param albumId the primary key for the new album
	 * @return the new album
	 */
	public static Album createAlbum(long albumId) {
		return getService().createAlbum(albumId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the album from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was removed
	 */
	public static Album deleteAlbum(Album album) {
		return getService().deleteAlbum(album);
	}

	/**
	 * Deletes the album with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param albumId the primary key of the album
	 * @return the album that was removed
	 * @throws PortalException if a album with the primary key could not be found
	 */
	public static Album deleteAlbum(long albumId) throws PortalException {
		return getService().deleteAlbum(albumId);
	}

	public static Album deleteEntry(long primaryKey) throws PortalException {
		return getService().deleteEntry(primaryKey);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Album fetchAlbum(long albumId) {
		return getService().fetchAlbum(albumId);
	}

	/**
	 * Returns the album matching the UUID and group.
	 *
	 * @param uuid the album's UUID
	 * @param groupId the primary key of the group
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	public static Album fetchAlbumByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchAlbumByUuidAndGroupId(uuid, groupId);
	}

	public static List<Album> findByCategoryId(long type) {
		return getService().findByCategoryId(type);
	}

	public static List<Album> findByCategoryId(long type, int start, int end) {
		return getService().findByCategoryId(type, start, end);
	}

	public static List<Album> findByCategoryId(
		long type, int start, int end, OrderByComparator<Album> obc) {

		return getService().findByCategoryId(type, start, end, obc);
	}

	public static List<Album> findByGroupId(long groupId) {
		return getService().findByGroupId(groupId);
	}

	public static List<Album> findByGroupId(long groupId, int start, int end) {
		return getService().findByGroupId(groupId, start, end);
	}

	public static List<Album> findByGroupId(
		long groupId, int start, int end, OrderByComparator<Album> obc) {

		return getService().findByGroupId(groupId, start, end, obc);
	}

	public static List<Album> findByMonth(int month) {
		return getService().findByMonth(month);
	}

	public static List<Album> findByMonth(int month, int start, int end) {
		return getService().findByMonth(month, start, end);
	}

	public static List<Album> findByMonth(
		int month, int start, int end, OrderByComparator<Album> obc) {

		return getService().findByMonth(month, start, end, obc);
	}

	public static List<Album> findByT_M(long type, int month) {
		return getService().findByT_M(type, month);
	}

	public static List<Album> findByT_M(
		long type, int month, int start, int end) {

		return getService().findByT_M(type, month, start, end);
	}

	public static List<Album> findByT_M(
		long type, int month, int start, int end,
		OrderByComparator<Album> obc) {

		return getService().findByT_M(type, month, start, end, obc);
	}

	public static List<Album> findByT_Y(long type, int year) {
		return getService().findByT_Y(type, year);
	}

	public static List<Album> findByT_Y(
		long type, int year, int start, int end) {

		return getService().findByT_Y(type, year, start, end);
	}

	public static List<Album> findByT_Y(
		long type, int year, int start, int end, OrderByComparator<Album> obc) {

		return getService().findByT_Y(type, year, start, end, obc);
	}

	public static List<Album> findByT_Y_M(long type, int year, int month) {
		return getService().findByT_Y_M(type, year, month);
	}

	public static List<Album> findByT_Y_M(
		long type, int year, int month, int start, int end) {

		return getService().findByT_Y_M(type, year, month, start, end);
	}

	public static List<Album> findByT_Y_M(
		long type, int year, int month, int start, int end,
		OrderByComparator<Album> obc) {

		return getService().findByT_Y_M(type, year, month, start, end, obc);
	}

	public static List<Album> findByY_M(int year, int month) {
		return getService().findByY_M(year, month);
	}

	public static List<Album> findByY_M(
		int year, int month, int start, int end) {

		return getService().findByY_M(year, month, start, end);
	}

	public static List<Album> findByY_M(
		int year, int month, int start, int end, OrderByComparator<Album> obc) {

		return getService().findByY_M(year, month, start, end, obc);
	}

	public static List<Album> findByYear(int year) {
		return getService().findByYear(year);
	}

	public static List<Album> findByYear(int year, int start, int end) {
		return getService().findByYear(year, start, end);
	}

	public static List<Album> findByYear(
		int year, int start, int end, OrderByComparator<Album> obc) {

		return getService().findByYear(year, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns the album with the primary key.
	 *
	 * @param albumId the primary key of the album
	 * @return the album
	 * @throws PortalException if a album with the primary key could not be found
	 */
	public static Album getAlbum(long albumId) throws PortalException {
		return getService().getAlbum(albumId);
	}

	/**
	 * Returns the album matching the UUID and group.
	 *
	 * @param uuid the album's UUID
	 * @param groupId the primary key of the group
	 * @return the matching album
	 * @throws PortalException if a matching album could not be found
	 */
	public static Album getAlbumByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getAlbumByUuidAndGroupId(uuid, groupId);
	}

	public static Album getAlbumFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws albums.exception.AlbumValidateException,
			   javax.portlet.PortletException {

		return getService().getAlbumFromRequest(primaryKey, request);
	}

	public static java.util.HashSet<Integer> getAlbumMonths(
		List<Album> albums) {

		return getService().getAlbumMonths(albums);
	}

	/**
	 * Returns a range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>albums.model.impl.AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of albums
	 */
	public static List<Album> getAlbums(int start, int end) {
		return getService().getAlbums(start, end);
	}

	/**
	 * Returns all the albums matching the UUID and company.
	 *
	 * @param uuid the UUID of the albums
	 * @param companyId the primary key of the company
	 * @return the matching albums, or an empty list if no matches were found
	 */
	public static List<Album> getAlbumsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getAlbumsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of albums matching the UUID and company.
	 *
	 * @param uuid the UUID of the albums
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching albums, or an empty list if no matches were found
	 */
	public static List<Album> getAlbumsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return getService().getAlbumsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of albums.
	 *
	 * @return the number of albums
	 */
	public static int getAlbumsCount() {
		return getService().getAlbumsCount();
	}

	public static java.util.HashSet<Integer> getAlbumYears(List<Album> albums) {
		return getService().getAlbumYears(albums);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static List<albums.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest) {

		return getService().getGalleries(actionRequest);
	}

	public static List<albums.model.Gallery> getGalleries(
		javax.portlet.ActionRequest actionRequest,
		List<albums.model.Gallery> defaultGalleries) {

		return getService().getGalleries(actionRequest, defaultGalleries);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Album getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the album in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AlbumLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param album the album
	 * @return the album that was updated
	 */
	public static Album updateAlbum(Album album) {
		return getService().updateAlbum(album);
	}

	public static Album updateEntry(
			Album orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws albums.exception.AlbumValidateException, PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	public static void updateGalleries(
			Album entry, List<albums.model.Gallery> galleries,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		getService().updateGalleries(entry, galleries, serviceContext);
	}

	public static AlbumLocalService getService() {
		return _service;
	}

	private static volatile AlbumLocalService _service;

}