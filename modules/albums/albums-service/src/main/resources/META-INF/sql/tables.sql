create table misk_albums (
	uuid_ VARCHAR(75) null,
	albumId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	categoryId LONG,
	title STRING null,
	displayDate DATE null,
	year INTEGER,
	month INTEGER,
	listingImage VARCHAR(200) null,
	description STRING null
);

create table misk_albums_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug VARCHAR(75) null
);

create table misk_albums_galleries (
	uuid_ VARCHAR(75) null,
	slideId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	albumId LONG,
	type_ INTEGER,
	image STRING null,
	thumbnail STRING null,
	videoLink STRING null
);