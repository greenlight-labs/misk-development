create index IX_B2B759EE on misk_albums (categoryId, month);
create index IX_E0D0D95D on misk_albums (categoryId, year, month);
create index IX_343A22B5 on misk_albums (groupId);
create index IX_C51CDFB on misk_albums (month);
create index IX_83517049 on misk_albums (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_C5DB750B on misk_albums (uuid_[$COLUMN_LENGTH:75$], groupId);
create index IX_21754FAA on misk_albums (year, month);

create index IX_D5EBE958 on misk_albums_categories (groupId);
create index IX_91017106 on misk_albums_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_581E8108 on misk_albums_categories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_989E2AD4 on misk_albums_galleries (albumId);
create index IX_315B1AE4 on misk_albums_galleries (groupId);
create index IX_49433CFA on misk_albums_galleries (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_FA8AB9FC on misk_albums_galleries (uuid_[$COLUMN_LENGTH:75$], groupId);