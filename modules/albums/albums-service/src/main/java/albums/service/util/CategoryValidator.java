package albums.service.util;

import albums.model.Category;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class CategoryValidator implements ModelValidator<Category> {

	@Override
	public void validate(Category entry) throws PortalException {
        ValidateName(entry.getName());
        ValidateSlug(entry.getSlug());
	}

    protected void ValidateName(String field) {
        if(StringUtils.isEmpty(field)){
            _errors.add("category-name-required");
        }
    }

    protected void ValidateSlug(String field) {
        if(StringUtils.isEmpty(field)){
            _errors.add("category-slug-required");
        }
    }

    protected List<String> _errors = new ArrayList<>();

}
