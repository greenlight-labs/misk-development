/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service.impl;

import albums.exception.GalleryValidateException;
import albums.model.Gallery;
import albums.service.base.GalleryLocalServiceBaseImpl;
import albums.service.util.GalleryValidator;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * The implementation of the gallery local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>albums.service.GalleryLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GalleryLocalServiceBaseImpl
 */
@Component(
        property = "model.class.name=albums.model.Gallery",
        service = AopService.class
)
public class GalleryLocalServiceImpl extends GalleryLocalServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Use <code>albums.service.GalleryLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>albums.service.GalleryLocalServiceUtil</code>.
     */

    public Gallery addEntry(Gallery orgEntry, ServiceContext serviceContext)
            throws PortalException, GalleryValidateException {

        // Validation

        ModelValidator<Gallery> modelValidator = new GalleryValidator();
        modelValidator.validate(orgEntry);

        // Add entry

        Gallery entry = _addEntry(orgEntry, serviceContext);

        Gallery addedEntry = galleryPersistence.update(entry);
        galleryPersistence.clearCache();

        return addedEntry;
    }

    public Gallery updateEntry(
            Gallery orgEntry, ServiceContext serviceContext)
            throws PortalException, GalleryValidateException {

        User user = userLocalService.getUser(orgEntry.getUserId());

        // Validation

        ModelValidator<Gallery> modelValidator = new GalleryValidator();
        modelValidator.validate(orgEntry);

        // Update entry

        Gallery entry = _updateEntry(
                orgEntry.getPrimaryKey(), orgEntry, serviceContext);

        Gallery updatedEntry = galleryPersistence.update(entry);
        galleryPersistence.clearCache();

        return updatedEntry;
    }

    protected Gallery _addEntry(Gallery entry, ServiceContext serviceContext)
            throws PortalException {

        long id = counterLocalService.increment(Gallery.class.getName());

        Gallery newEntry = galleryPersistence.create(id);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        newEntry.setCompanyId(entry.getCompanyId());
        newEntry.setGroupId(entry.getGroupId());
        newEntry.setUserId(user.getUserId());
        newEntry.setUserName(user.getFullName());
        newEntry.setCreateDate(now);
        newEntry.setModifiedDate(now);

        newEntry.setUuid(serviceContext.getUuid());

        newEntry.setAlbumId(entry.getAlbumId());
        newEntry.setType(entry.getType());
        newEntry.setImage(entry.getImage());
        newEntry.setThumbnail(entry.getThumbnail());
        newEntry.setVideoLink(entry.getVideoLink());

        return newEntry;
    }

    protected Gallery _updateEntry(
            long primaryKey, Gallery entry, ServiceContext serviceContext)
            throws PortalException {

        Gallery updateEntry = fetchGallery(primaryKey);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        updateEntry.setCompanyId(entry.getCompanyId());
        updateEntry.setGroupId(entry.getGroupId());
        updateEntry.setUserId(user.getUserId());
        updateEntry.setUserName(user.getFullName());
        updateEntry.setCreateDate(entry.getCreateDate());
        updateEntry.setModifiedDate(now);

        updateEntry.setUuid(entry.getUuid());

        updateEntry.setAlbumId(entry.getAlbumId());
        updateEntry.setType(entry.getType());
        updateEntry.setImage(entry.getImage());
        updateEntry.setThumbnail(entry.getThumbnail());
        updateEntry.setVideoLink(entry.getVideoLink());

        return updateEntry;
    }

    public Gallery deleteEntry(long primaryKey) throws PortalException {
        Gallery entry = getGallery(primaryKey);
        galleryPersistence.remove(entry);

        return entry;
    }

    public List<Gallery> findByGroupId(long groupId) {

        return galleryPersistence.findByGroupId(groupId);
    }

    public List<Gallery> findByGroupId(long groupId, int start, int end,
                                        OrderByComparator<Gallery> obc) {

        return galleryPersistence.findByGroupId(groupId, start, end, obc);
    }

    public List<Gallery> findByGroupId(long groupId, int start, int end) {

        return galleryPersistence.findByGroupId(groupId, start, end);
    }

    public int countByGroupId(long groupId) {

        return galleryPersistence.countByGroupId(groupId);
    }

    /* *********************- gallery by album id -*********************** */
    public List<Gallery> findByAlbumId(long albumId) {

        return galleryPersistence.findByAlbumId(albumId);
    }

    public List<Gallery> findByAlbumId(long albumId, int start, int end,
                                           OrderByComparator<Gallery> obc) {

        return galleryPersistence.findByAlbumId(albumId, start, end, obc);
    }

    public List<Gallery> findByAlbumId(long albumId, int start, int end) {

        return galleryPersistence.findByAlbumId(albumId, start, end);
    }

    public int countByAlbumId(long albumId) {

        return galleryPersistence.countByAlbumId(albumId);
    }
    /* ********************************************************************** */
    public Gallery getGalleryFromRequest(
            long primaryKey, PortletRequest request)
            throws PortletException, GalleryValidateException {

        ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
                WebKeys.THEME_DISPLAY);

        // Create or fetch existing data

        Gallery entry;

        if (primaryKey <= 0) {
            entry = getNewObject(primaryKey);
        }
        else {
            entry = fetchGallery(primaryKey);
        }

        try {
            entry.setSlideId(primaryKey);
            entry.setAlbumId(ParamUtil.getLong(request, "albumId"));
            entry.setType(ParamUtil.getInteger(request, "type"));
            entry.setImageMap(LocalizationUtil.getLocalizationMap(request, "image"));
            entry.setThumbnailMap(LocalizationUtil.getLocalizationMap(request, "thumbnail"));
            entry.setVideoLinkMap(LocalizationUtil.getLocalizationMap(request, "videoLink"));

            entry.setCompanyId(themeDisplay.getCompanyId());
            entry.setGroupId(themeDisplay.getScopeGroupId());
            entry.setUserId(themeDisplay.getUserId());
        }
        catch (Exception e) {
            _log.error("Errors occur while populating the model", e);
            List<String> error = new ArrayList<>();
            error.add("value-convert-error");

            throw new GalleryValidateException(error);
        }

        return entry;
    }

    public Gallery getNewObject(long primaryKey) {
        primaryKey = (primaryKey <= 0) ? 0 :
                counterLocalService.increment(Gallery.class.getName());

        return createGallery(primaryKey);
    }

    /* ********************************************************************** */
    public int countGalleryImages(List<Gallery> gallerySlides){
        int count = 0;
        for (Gallery gallery : gallerySlides) {
            if (gallery.getType() == 1) {
                count++;
            }
        }
        return count;
    }

    public int countGalleryVideos(List<Gallery> gallerySlides){
        int count = 0;
        for (Gallery gallery : gallerySlides) {
            if (gallery.getType() == 2) {
                count++;
            }
        }
        return count;
    }


    private static Log _log = LogFactoryUtil.getLog(
            GalleryLocalServiceImpl.class);
}