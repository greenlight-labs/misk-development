package albums.service.util;

import albums.model.Album;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class AlbumValidator implements ModelValidator<Album> {

	@Override
	public void validate(Album entry) throws PortalException {
/*   */
        // Validate fields
        validateCategoryId(entry.getCategoryId());
        validateTitle(entry.getTitle());
        validateDisplayDate(entry.getDisplayDate());
        validateYear(entry.getYear());
        validateMonth(entry.getMonth());
        validateListingImage(entry.getListingImage());
        validateDescription(entry.getDescription());
	}

    /**
     * categoryId field Validation
     *
     * @param field categoryId
     */
    protected void validateCategoryId(long field) {
        if (Validator.isNull(field)) {
            _errors.add("gallery-thumbnail-required");
        }
    }

    protected void validateTitle(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("album-title-required");
        }
    }

    protected void validateDisplayDate(Date field) {}
    protected void validateYear(int year) {}
    protected void validateMonth(int month) {}
    protected void validateListingImage(String listingImage) {}
    protected void validateDescription(String description) {}

	protected List<String> _errors = new ArrayList<>();

}
