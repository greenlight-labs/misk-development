/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service.impl;

import albums.exception.AlbumValidateException;
import albums.model.Album;
import albums.model.Gallery;
import albums.service.GalleryLocalServiceUtil;
import albums.service.base.AlbumLocalServiceBaseImpl;
import albums.service.util.AlbumValidator;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

/**
 * The implementation of the album local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>albums.service.AlbumLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AlbumLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=albums.model.Album", service = AopService.class
)
public class AlbumLocalServiceImpl extends AlbumLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>albums.service.AlbumLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>albums.service.AlbumLocalServiceUtil</code>.
	 */

	public Album addEntry(Album orgEntry, ServiceContext serviceContext)
			throws PortalException, AlbumValidateException {

		// Validation

		ModelValidator<Album> modelValidator = new AlbumValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Album entry = _addEntry(orgEntry, serviceContext);

		Album addedEntry = albumPersistence.update(entry);

		albumPersistence.clearCache();

		return addedEntry;
	}

	public Album updateEntry(Album orgEntry, ServiceContext serviceContext)
			throws PortalException, AlbumValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Album> modelValidator = new AlbumValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Album entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Album updatedEntry = albumPersistence.update(entry);
		albumPersistence.clearCache();

		return updatedEntry;
	}

	protected Album _addEntry(Album entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Album.class.getName());

		Album newEntry = albumPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);
		newEntry.setUuid(serviceContext.getUuid());

		newEntry.setCategoryId(entry.getCategoryId());
		newEntry.setTitle(entry.getTitle());
		newEntry.setDisplayDate(entry.getDisplayDate());
		newEntry.setYear(entry.getYear());
		newEntry.setMonth(entry.getMonth());
		newEntry.setListingImage(entry.getListingImage());
		newEntry.setDescription(entry.getDescription());

		return newEntry;
	}

	protected Album _updateEntry(long primaryKey, Album entry, ServiceContext serviceContext)
			throws PortalException {

		Album updateEntry = fetchAlbum(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);
		updateEntry.setUuid(entry.getUuid());

		updateEntry.setCategoryId(entry.getCategoryId());
		updateEntry.setTitle(entry.getTitle());
		updateEntry.setDisplayDate(entry.getDisplayDate());
		updateEntry.setYear(entry.getYear());
		updateEntry.setMonth(entry.getMonth());
		updateEntry.setListingImage(entry.getListingImage());
		updateEntry.setDescription(entry.getDescription());

		return updateEntry;
	}

	public Album deleteEntry(long primaryKey) throws PortalException {
		Album entry = getAlbum(primaryKey);
		albumPersistence.remove(entry);

		return entry;
	}

	public List<Album> findByGroupId(long groupId) {

		return albumPersistence.findByGroupId(groupId);
	}

	public List<Album> findByGroupId(long groupId, int start, int end, OrderByComparator<Album> obc) {

		return albumPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Album> findByGroupId(long groupId, int start, int end) {

		return albumPersistence.findByGroupId(groupId, start, end);
	}

	public int countByGroupId(long groupId) {

		return albumPersistence.countByGroupId(groupId);
	}

	public Album getAlbumFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, AlbumValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Album entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchAlbum(primaryKey);
		}

		try {
			entry.setAlbumId(primaryKey);

			entry.setCategoryId(ParamUtil.getLong(request, "categoryId"));
			entry.setTitleMap(LocalizationUtil.getLocalizationMap(request, "title"));
			entry.setDisplayDate(ParamUtil.getDate(request, "displayDate", DateFormatFactoryUtil.getDate(themeDisplay.getLocale()), null));
			entry.setYear(ParamUtil.getInteger(request, "displayDateYear"));
			entry.setMonth(ParamUtil.getInteger(request, "displayDateMonth") + 1);
			entry.setListingImage(ParamUtil.getString(request, "listingImage"));
			entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new AlbumValidateException(error);
		}

		return entry;
	}

	public Album getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Album.class.getName());

		return createAlbum(primaryKey);
	}

	/*
	 * Get Albums by Type
	 *  */

	public List<Album> findByCategoryId(long type) {

		return albumPersistence.findByCategoryId(type);
	}

	public List<Album> findByCategoryId(long type, int start, int end,
									   OrderByComparator<Album> obc) {

		return albumPersistence.findByCategoryId(type, start, end, obc);
	}

	public List<Album> findByCategoryId(long type, int start, int end) {

		return albumPersistence.findByCategoryId(type, start, end);
	}

	public int countByCategoryId(long type) {

		return albumPersistence.countByCategoryId(type);
	}

	/*
	 * Get Albums by Year
	 *  */

	public List<Album> findByYear(int year) {

		return albumPersistence.findByYear(year);
	}

	public List<Album> findByYear(int year, int start, int end,
									   OrderByComparator<Album> obc) {

		return albumPersistence.findByYear(year, start, end, obc);
	}

	public List<Album> findByYear(int year, int start, int end) {

		return albumPersistence.findByYear(year, start, end);
	}

	public int countByYear(int year) {

		return albumPersistence.countByYear(year);
	}
	/*
	 * Get Albums by Month
	 *  */

	public List<Album> findByMonth(int month) {

		return albumPersistence.findByMonth(month);
	}

	public List<Album> findByMonth(int month, int start, int end,
										OrderByComparator<Album> obc) {

		return albumPersistence.findByMonth(month, start, end, obc);
	}

	public List<Album> findByMonth(int month, int start, int end) {

		return albumPersistence.findByMonth(month, start, end);
	}

	public int countByMonth(int month) {

		return albumPersistence.countByMonth(month);
	}
	/*
	 * Get Albums by Type, Year, Month
	 *  */

	public List<Album> findByT_Y_M(long type, int year, int month) {

		return albumPersistence.findByT_Y_M(type, year, month);
	}

	public List<Album> findByT_Y_M(long type, int year, int month, int start, int end,
										OrderByComparator<Album> obc) {

		return albumPersistence.findByT_Y_M(type, year, month, start, end, obc);
	}

	public List<Album> findByT_Y_M(long type, int year, int month, int start, int end) {

		return albumPersistence.findByT_Y_M(type, year, month, start, end);
	}

	public int countByT_Y_M(long type, int year, int month) {

		return albumPersistence.countByT_Y_M(type, year, month);
	}
	/*
	 * Get Albums by Type, Year
	 *  */

	public List<Album> findByT_Y(long type, int year) {

		return albumPersistence.findByT_Y(type, year);
	}

	public List<Album> findByT_Y(long type, int year, int start, int end,
										OrderByComparator<Album> obc) {

		return albumPersistence.findByT_Y(type, year, start, end, obc);
	}

	public List<Album> findByT_Y(long type, int year, int start, int end) {

		return albumPersistence.findByT_Y(type, year, start, end);
	}

	public int countByT_Y(long type, int year) {

		return albumPersistence.countByT_Y(type, year);
	}
	/*
	 * Get Albums by Type, Month
	 *  */

	public List<Album> findByT_M(long type, int month) {

		return albumPersistence.findByT_M(type, month);
	}

	public List<Album> findByT_M(long type, int month, int start, int end,
										OrderByComparator<Album> obc) {

		return albumPersistence.findByT_M(type, month, start, end, obc);
	}

	public List<Album> findByT_M(long type, int month, int start, int end) {

		return albumPersistence.findByT_M(type, month, start, end);
	}

	public int countByT_M(long type, int month) {

		return albumPersistence.countByT_M(type, month);
	}
	/*
	 * Get Albums by Year, Month
	 *  */

	public List<Album> findByY_M(int year, int month) {

		return albumPersistence.findByY_M(year, month);
	}

	public List<Album> findByY_M(int year, int month, int start, int end,
									  OrderByComparator<Album> obc) {

		return albumPersistence.findByY_M(year, month, start, end, obc);
	}

	public List<Album> findByY_M(int year, int month, int start, int end) {

		return albumPersistence.findByY_M(year, month, start, end);
	}

	public int countByY_M(int year, int month) {

		return albumPersistence.countByY_M(year, month);
	}

	/*
	 * **********************************- START: Album Gallery Code
	 * -*****************************
	 */
	public void addEntryGallery(Album orgEntry, PortletRequest request) throws PortletException {

		String galleryIndexesString = ParamUtil.getString(request, "galleryIndexes");

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);
	}

	@Override
	public void updateGalleries(Album entry, List<Gallery> galleries, ServiceContext serviceContext)
			throws PortalException {

		Set<Long> galleryIds = new HashSet<>();

		for (Gallery gallery : galleries) {
			long galleryId = gallery.getSlideId();

			// additional step to set Ids - initially zero
			gallery.setAlbumId(entry.getAlbumId());
			gallery.setUserId(entry.getUserId());
			gallery.setCompanyId(entry.getCompanyId());
			gallery.setGroupId(entry.getGroupId());

			if (galleryId <= 0) {
				gallery = GalleryLocalServiceUtil.addEntry(gallery, serviceContext);

				galleryId = gallery.getSlideId();
			} else {
				GalleryLocalServiceUtil.updateEntry(gallery, serviceContext);
			}

			galleryIds.add(galleryId);
		}

		galleries = GalleryLocalServiceUtil.findByAlbumId(entry.getAlbumId());

		for (Gallery gallery : galleries) {
			if (!galleryIds.contains(gallery.getSlideId())) {
				GalleryLocalServiceUtil.deleteGallery(gallery.getSlideId());
			}
		}
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest) {
		return getGalleries(actionRequest, Collections.<Gallery>emptyList());
	}

	@Override
	public List<Gallery> getGalleries(ActionRequest actionRequest, List<Gallery> defaultGalleries) {

		String galleryIndexesString = ParamUtil.getString(actionRequest, "galleryIndexes");

		if (galleryIndexesString == null) {
			return defaultGalleries;
		}

		List<Gallery> galleries = new ArrayList<>();

		int[] galleryIndexes = StringUtil.split(galleryIndexesString, 0);

		for (int galleryIndex : galleryIndexes) {
			String image = ParamUtil.getString(actionRequest, "galleryImage" + galleryIndex);

			if (Validator.isNull(image)) {
				continue;
			}

			int type = ParamUtil.getInteger(actionRequest, "galleryType" + galleryIndex);
			String thumbnail = ParamUtil.getString(actionRequest, "galleryThumbnail" + galleryIndex);
			String videoLink = ParamUtil.getString(actionRequest, "galleryVideoLink" + galleryIndex);

			long galleryId = ParamUtil.getLong(actionRequest, "galleryId" + galleryIndex);

			Gallery gallery = GalleryLocalServiceUtil.createGallery(galleryId);

			gallery.setAlbumId(0);
			gallery.setType(type);
			gallery.setImage(image);
			gallery.setThumbnail(thumbnail);
			gallery.setVideoLink(videoLink);

			galleries.add(gallery);
		}

		return galleries;
	}

	/*
	 * **********************************- END: Album Gallery Code
	 * -*****************************
	 */

	public HashSet<Integer> getAlbumYears(List<Album> albums){
		HashSet<Integer> years = new HashSet<>();
		for (Album album: albums) {
			years.add(album.getYear());
		}
		return years;
	}

	public HashSet<Integer> getAlbumMonths(List<Album> albums){
		HashSet<Integer> months = new HashSet<>();
		for (Album album: albums) {
			months.add(album.getMonth());
		}
		return months;
	}

	private static Log _log = LogFactoryUtil.getLog(AlbumLocalServiceImpl.class);
}