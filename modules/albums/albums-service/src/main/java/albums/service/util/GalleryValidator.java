package albums.service.util;

import albums.model.Gallery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator 
 * 
 * @author tz
 *
 */
public class GalleryValidator implements ModelValidator<Gallery> {

	@Override
	public void validate(Gallery entry) throws PortalException {

        // validate fields
        validateAlbumId(entry.getAlbumId());
        validateType(entry.getType());
        validateImage(entry.getImage(), entry.getType());
        validateThumbnail(entry.getThumbnail(), entry.getType());
        validateVideoLink(entry.getVideoLink(), entry.getType());
	}

    /*   */

    protected void validateType(int field) {}
    /**
    * albumId field Validation
    *
    * @param field albumId
    */
    protected void validateAlbumId(long field) {
        if(Validator.isNull(field)){
            _errors.add("gallery-album-id-required");
        }
    }

    /**
    * image field Validation
    *
    * @param field image
    */
    protected void validateImage(String field, int type) {
        if (StringUtils.isEmpty(field) && type == 1) {
            _errors.add("gallery-image-required");
        }

    }

    /**
    * thumbnail field Validation
    *
    * @param field thumbnail
    */
    protected void validateThumbnail(String field, int type) {
        if (StringUtils.isEmpty(field) && type == 1) {
            _errors.add("gallery-thumbnail-required");
        }
    }

    protected void validateVideoLink(String field, int type) {
        if (StringUtils.isEmpty(field) && type == 2) {
            _errors.add("gallery-video-link-required");
        }
    }

/*  */

	protected List<String> _errors = new ArrayList<>();

}
