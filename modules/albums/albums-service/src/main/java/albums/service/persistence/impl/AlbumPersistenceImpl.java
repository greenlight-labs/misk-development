/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.service.persistence.impl;

import albums.exception.NoSuchAlbumException;

import albums.model.Album;
import albums.model.impl.AlbumImpl;
import albums.model.impl.AlbumModelImpl;

import albums.service.persistence.AlbumPersistence;
import albums.service.persistence.AlbumUtil;
import albums.service.persistence.impl.constants.AlbumPersistenceConstants;

import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.configuration.Configuration;
import com.liferay.portal.kernel.dao.orm.ArgumentsResolver;
import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.sql.DataSource;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

/**
 * The persistence implementation for the album service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@Component(service = AlbumPersistence.class)
public class AlbumPersistenceImpl
	extends BasePersistenceImpl<Album> implements AlbumPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>AlbumUtil</code> to access the album persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		AlbumImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the albums where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid;
				finderArgs = new Object[] {uuid};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if (!uuid.equals(album.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByUuid_First(
			String uuid, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByUuid_First(uuid, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByUuid_First(
		String uuid, OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByUuid_Last(
			String uuid, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByUuid_Last(uuid, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByUuid_Last(
		String uuid, OrderByComparator<Album> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where uuid = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByUuid_PrevAndNext(
			long albumId, String uuid,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		uuid = Objects.toString(uuid, "");

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, album, uuid, orderByComparator, true);

			array[1] = album;

			array[2] = getByUuid_PrevAndNext(
				session, album, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByUuid_PrevAndNext(
		Session session, Album album, String uuid,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Album album :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching albums
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 = "album.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(album.uuid IS NULL OR album.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAlbumException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByUUID_G(String uuid, long groupId)
		throws NoSuchAlbumException {

		Album album = fetchByUUID_G(uuid, groupId);

		if (album == null) {
			StringBundler sb = new StringBundler(6);

			sb.append(_NO_SUCH_ENTITY_WITH_KEY);

			sb.append("uuid=");
			sb.append(uuid);

			sb.append(", groupId=");
			sb.append(groupId);

			sb.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(sb.toString());
			}

			throw new NoSuchAlbumException(sb.toString());
		}

		return album;
	}

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the album where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = null;

		if (useFinderCache) {
			finderArgs = new Object[] {uuid, groupId};
		}

		Object result = null;

		if (useFinderCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof Album) {
			Album album = (Album)result;

			if (!Objects.equals(uuid, album.getUuid()) ||
				(groupId != album.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				List<Album> list = query.list();

				if (list.isEmpty()) {
					if (useFinderCache) {
						finderCache.putResult(
							_finderPathFetchByUUID_G, finderArgs, list);
					}
				}
				else {
					Album album = list.get(0);

					result = album;

					cacheResult(album);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Album)result;
		}
	}

	/**
	 * Removes the album where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the album that was removed
	 */
	@Override
	public Album removeByUUID_G(String uuid, long groupId)
		throws NoSuchAlbumException {

		Album album = findByUUID_G(uuid, groupId);

		return remove(album);
	}

	/**
	 * Returns the number of albums where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching albums
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"album.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(album.uuid IS NULL OR album.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"album.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByUuid_C;
				finderArgs = new Object[] {uuid, companyId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if (!uuid.equals(album.getUuid()) ||
						(companyId != album.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByUuid_C_First(uuid, companyId, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByUuid_C_Last(uuid, companyId, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("uuid=");
		sb.append(uuid);

		sb.append(", companyId=");
		sb.append(companyId);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Album> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByUuid_C_PrevAndNext(
			long albumId, String uuid, long companyId,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		uuid = Objects.toString(uuid, "");

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, album, uuid, companyId, orderByComparator, true);

			array[1] = album;

			array[2] = getByUuid_C_PrevAndNext(
				session, album, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByUuid_C_PrevAndNext(
		Session session, Album album, String uuid, long companyId,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		if (bindUuid) {
			queryPos.add(uuid);
		}

		queryPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Album album :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching albums
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				sb.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				sb.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			sb.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				if (bindUuid) {
					queryPos.add(uuid);
				}

				queryPos.add(companyId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"album.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(album.uuid IS NULL OR album.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"album.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByGroupId;
	private FinderPath _finderPathWithoutPaginationFindByGroupId;
	private FinderPath _finderPathCountByGroupId;

	/**
	 * Returns all the albums where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByGroupId(long groupId) {
		return findByGroupId(
			groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByGroupId;
				finderArgs = new Object[] {groupId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByGroupId;
			finderArgs = new Object[] {groupId, start, end, orderByComparator};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if (groupId != album.getGroupId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByGroupId_First(
			long groupId, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByGroupId_First(groupId, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByGroupId_First(
		long groupId, OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByGroupId_Last(
			long groupId, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByGroupId_Last(groupId, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("groupId=");
		sb.append(groupId);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByGroupId_Last(
		long groupId, OrderByComparator<Album> orderByComparator) {

		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByGroupId(
			groupId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where groupId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByGroupId_PrevAndNext(
			long albumId, long groupId,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByGroupId_PrevAndNext(
				session, album, groupId, orderByComparator, true);

			array[1] = album;

			array[2] = getByGroupId_PrevAndNext(
				session, album, groupId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByGroupId_PrevAndNext(
		Session session, Album album, long groupId,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(groupId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (Album album :
				findByGroupId(
					groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching albums
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = _finderPathCountByGroupId;

		Object[] finderArgs = new Object[] {groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(groupId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 =
		"album.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByCategoryId;
	private FinderPath _finderPathWithoutPaginationFindByCategoryId;
	private FinderPath _finderPathCountByCategoryId;

	/**
	 * Returns all the albums where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByCategoryId(long categoryId) {
		return findByCategoryId(
			categoryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByCategoryId(long categoryId, int start, int end) {
		return findByCategoryId(categoryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByCategoryId(
			categoryId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByCategoryId;
				finderArgs = new Object[] {categoryId};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByCategoryId;
			finderArgs = new Object[] {
				categoryId, start, end, orderByComparator
			};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if (categoryId != album.getCategoryId()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_CATEGORYID_CATEGORYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByCategoryId_First(
			long categoryId, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByCategoryId_First(categoryId, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByCategoryId_First(
		long categoryId, OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByCategoryId(
			categoryId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByCategoryId_Last(
			long categoryId, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByCategoryId_Last(categoryId, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByCategoryId_Last(
		long categoryId, OrderByComparator<Album> orderByComparator) {

		int count = countByCategoryId(categoryId);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByCategoryId(
			categoryId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByCategoryId_PrevAndNext(
			long albumId, long categoryId,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByCategoryId_PrevAndNext(
				session, album, categoryId, orderByComparator, true);

			array[1] = album;

			array[2] = getByCategoryId_PrevAndNext(
				session, album, categoryId, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByCategoryId_PrevAndNext(
		Session session, Album album, long categoryId,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_CATEGORYID_CATEGORYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(categoryId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	@Override
	public void removeByCategoryId(long categoryId) {
		for (Album album :
				findByCategoryId(
					categoryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching albums
	 */
	@Override
	public int countByCategoryId(long categoryId) {
		FinderPath finderPath = _finderPathCountByCategoryId;

		Object[] finderArgs = new Object[] {categoryId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_CATEGORYID_CATEGORYID_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CATEGORYID_CATEGORYID_2 =
		"album.categoryId = ?";

	private FinderPath _finderPathWithPaginationFindByYear;
	private FinderPath _finderPathWithoutPaginationFindByYear;
	private FinderPath _finderPathCountByYear;

	/**
	 * Returns all the albums where year = &#63;.
	 *
	 * @param year the year
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByYear(int year) {
		return findByYear(year, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByYear(int year, int start, int end) {
		return findByYear(year, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByYear(
		int year, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByYear(year, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByYear(
		int year, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByYear;
				finderArgs = new Object[] {year};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByYear;
			finderArgs = new Object[] {year, start, end, orderByComparator};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if (year != album.getYear()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_YEAR_YEAR_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(year);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByYear_First(
			int year, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByYear_First(year, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("year=");
		sb.append(year);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByYear_First(
		int year, OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByYear(year, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByYear_Last(
			int year, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByYear_Last(year, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("year=");
		sb.append(year);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where year = &#63;.
	 *
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByYear_Last(
		int year, OrderByComparator<Album> orderByComparator) {

		int count = countByYear(year);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByYear(
			year, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where year = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByYear_PrevAndNext(
			long albumId, int year, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByYear_PrevAndNext(
				session, album, year, orderByComparator, true);

			array[1] = album;

			array[2] = getByYear_PrevAndNext(
				session, album, year, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByYear_PrevAndNext(
		Session session, Album album, int year,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_YEAR_YEAR_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(year);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where year = &#63; from the database.
	 *
	 * @param year the year
	 */
	@Override
	public void removeByYear(int year) {
		for (Album album :
				findByYear(year, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where year = &#63;.
	 *
	 * @param year the year
	 * @return the number of matching albums
	 */
	@Override
	public int countByYear(int year) {
		FinderPath finderPath = _finderPathCountByYear;

		Object[] finderArgs = new Object[] {year};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_YEAR_YEAR_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(year);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_YEAR_YEAR_2 = "album.year = ?";

	private FinderPath _finderPathWithPaginationFindByMonth;
	private FinderPath _finderPathWithoutPaginationFindByMonth;
	private FinderPath _finderPathCountByMonth;

	/**
	 * Returns all the albums where month = &#63;.
	 *
	 * @param month the month
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByMonth(int month) {
		return findByMonth(month, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByMonth(int month, int start, int end) {
		return findByMonth(month, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByMonth(
		int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByMonth(month, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByMonth(
		int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByMonth;
				finderArgs = new Object[] {month};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByMonth;
			finderArgs = new Object[] {month, start, end, orderByComparator};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if (month != album.getMonth()) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(3);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_MONTH_MONTH_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(month);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByMonth_First(
			int month, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByMonth_First(month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByMonth_First(
		int month, OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByMonth(month, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByMonth_Last(
			int month, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByMonth_Last(month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(4);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where month = &#63;.
	 *
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByMonth_Last(
		int month, OrderByComparator<Album> orderByComparator) {

		int count = countByMonth(month);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByMonth(
			month, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByMonth_PrevAndNext(
			long albumId, int month, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByMonth_PrevAndNext(
				session, album, month, orderByComparator, true);

			array[1] = album;

			array[2] = getByMonth_PrevAndNext(
				session, album, month, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByMonth_PrevAndNext(
		Session session, Album album, int month,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(3);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_MONTH_MONTH_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(month);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where month = &#63; from the database.
	 *
	 * @param month the month
	 */
	@Override
	public void removeByMonth(int month) {
		for (Album album :
				findByMonth(
					month, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where month = &#63;.
	 *
	 * @param month the month
	 * @return the number of matching albums
	 */
	@Override
	public int countByMonth(int month) {
		FinderPath finderPath = _finderPathCountByMonth;

		Object[] finderArgs = new Object[] {month};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(2);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_MONTH_MONTH_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(month);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MONTH_MONTH_2 =
		"album.month = ?";

	private FinderPath _finderPathWithPaginationFindByT_Y_M;
	private FinderPath _finderPathWithoutPaginationFindByT_Y_M;
	private FinderPath _finderPathCountByT_Y_M;

	/**
	 * Returns all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByT_Y_M(long categoryId, int year, int month) {
		return findByT_Y_M(
			categoryId, year, month, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end) {

		return findByT_Y_M(categoryId, year, month, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByT_Y_M(
			categoryId, year, month, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByT_Y_M(
		long categoryId, int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByT_Y_M;
				finderArgs = new Object[] {categoryId, year, month};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByT_Y_M;
			finderArgs = new Object[] {
				categoryId, year, month, start, end, orderByComparator
			};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if ((categoryId != album.getCategoryId()) ||
						(year != album.getYear()) ||
						(month != album.getMonth())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					5 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(5);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_T_Y_M_CATEGORYID_2);

			sb.append(_FINDER_COLUMN_T_Y_M_YEAR_2);

			sb.append(_FINDER_COLUMN_T_Y_M_MONTH_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				queryPos.add(year);

				queryPos.add(month);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByT_Y_M_First(
			long categoryId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByT_Y_M_First(
			categoryId, year, month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(8);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append(", year=");
		sb.append(year);

		sb.append(", month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByT_Y_M_First(
		long categoryId, int year, int month,
		OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByT_Y_M(
			categoryId, year, month, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByT_Y_M_Last(
			long categoryId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByT_Y_M_Last(
			categoryId, year, month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(8);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append(", year=");
		sb.append(year);

		sb.append(", month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByT_Y_M_Last(
		long categoryId, int year, int month,
		OrderByComparator<Album> orderByComparator) {

		int count = countByT_Y_M(categoryId, year, month);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByT_Y_M(
			categoryId, year, month, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByT_Y_M_PrevAndNext(
			long albumId, long categoryId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByT_Y_M_PrevAndNext(
				session, album, categoryId, year, month, orderByComparator,
				true);

			array[1] = album;

			array[2] = getByT_Y_M_PrevAndNext(
				session, album, categoryId, year, month, orderByComparator,
				false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByT_Y_M_PrevAndNext(
		Session session, Album album, long categoryId, int year, int month,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				6 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(5);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_T_Y_M_CATEGORYID_2);

		sb.append(_FINDER_COLUMN_T_Y_M_YEAR_2);

		sb.append(_FINDER_COLUMN_T_Y_M_MONTH_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(categoryId);

		queryPos.add(year);

		queryPos.add(month);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where categoryId = &#63; and year = &#63; and month = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 */
	@Override
	public void removeByT_Y_M(long categoryId, int year, int month) {
		for (Album album :
				findByT_Y_M(
					categoryId, year, month, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where categoryId = &#63; and year = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param month the month
	 * @return the number of matching albums
	 */
	@Override
	public int countByT_Y_M(long categoryId, int year, int month) {
		FinderPath finderPath = _finderPathCountByT_Y_M;

		Object[] finderArgs = new Object[] {categoryId, year, month};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(4);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_T_Y_M_CATEGORYID_2);

			sb.append(_FINDER_COLUMN_T_Y_M_YEAR_2);

			sb.append(_FINDER_COLUMN_T_Y_M_MONTH_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				queryPos.add(year);

				queryPos.add(month);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_T_Y_M_CATEGORYID_2 =
		"album.categoryId = ? AND ";

	private static final String _FINDER_COLUMN_T_Y_M_YEAR_2 =
		"album.year = ? AND ";

	private static final String _FINDER_COLUMN_T_Y_M_MONTH_2 =
		"album.month = ?";

	private FinderPath _finderPathWithPaginationFindByT_Y;
	private FinderPath _finderPathWithoutPaginationFindByT_Y;
	private FinderPath _finderPathCountByT_Y;

	/**
	 * Returns all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByT_Y(long categoryId, int year) {
		return findByT_Y(
			categoryId, year, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByT_Y(
		long categoryId, int year, int start, int end) {

		return findByT_Y(categoryId, year, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByT_Y(
		long categoryId, int year, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByT_Y(categoryId, year, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and year = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByT_Y(
		long categoryId, int year, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByT_Y;
				finderArgs = new Object[] {categoryId, year};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByT_Y;
			finderArgs = new Object[] {
				categoryId, year, start, end, orderByComparator
			};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if ((categoryId != album.getCategoryId()) ||
						(year != album.getYear())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_T_Y_CATEGORYID_2);

			sb.append(_FINDER_COLUMN_T_Y_YEAR_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				queryPos.add(year);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByT_Y_First(
			long categoryId, int year,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByT_Y_First(categoryId, year, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append(", year=");
		sb.append(year);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByT_Y_First(
		long categoryId, int year, OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByT_Y(categoryId, year, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByT_Y_Last(
			long categoryId, int year,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByT_Y_Last(categoryId, year, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append(", year=");
		sb.append(year);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByT_Y_Last(
		long categoryId, int year, OrderByComparator<Album> orderByComparator) {

		int count = countByT_Y(categoryId, year);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByT_Y(
			categoryId, year, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and year = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param year the year
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByT_Y_PrevAndNext(
			long albumId, long categoryId, int year,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByT_Y_PrevAndNext(
				session, album, categoryId, year, orderByComparator, true);

			array[1] = album;

			array[2] = getByT_Y_PrevAndNext(
				session, album, categoryId, year, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByT_Y_PrevAndNext(
		Session session, Album album, long categoryId, int year,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_T_Y_CATEGORYID_2);

		sb.append(_FINDER_COLUMN_T_Y_YEAR_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(categoryId);

		queryPos.add(year);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where categoryId = &#63; and year = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 */
	@Override
	public void removeByT_Y(long categoryId, int year) {
		for (Album album :
				findByT_Y(
					categoryId, year, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where categoryId = &#63; and year = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param year the year
	 * @return the number of matching albums
	 */
	@Override
	public int countByT_Y(long categoryId, int year) {
		FinderPath finderPath = _finderPathCountByT_Y;

		Object[] finderArgs = new Object[] {categoryId, year};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_T_Y_CATEGORYID_2);

			sb.append(_FINDER_COLUMN_T_Y_YEAR_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				queryPos.add(year);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_T_Y_CATEGORYID_2 =
		"album.categoryId = ? AND ";

	private static final String _FINDER_COLUMN_T_Y_YEAR_2 = "album.year = ?";

	private FinderPath _finderPathWithPaginationFindByT_M;
	private FinderPath _finderPathWithoutPaginationFindByT_M;
	private FinderPath _finderPathCountByT_M;

	/**
	 * Returns all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByT_M(long categoryId, int month) {
		return findByT_M(
			categoryId, month, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByT_M(
		long categoryId, int month, int start, int end) {

		return findByT_M(categoryId, month, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByT_M(
		long categoryId, int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByT_M(
			categoryId, month, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where categoryId = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByT_M(
		long categoryId, int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByT_M;
				finderArgs = new Object[] {categoryId, month};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByT_M;
			finderArgs = new Object[] {
				categoryId, month, start, end, orderByComparator
			};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if ((categoryId != album.getCategoryId()) ||
						(month != album.getMonth())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_T_M_CATEGORYID_2);

			sb.append(_FINDER_COLUMN_T_M_MONTH_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				queryPos.add(month);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByT_M_First(
			long categoryId, int month,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByT_M_First(categoryId, month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append(", month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByT_M_First(
		long categoryId, int month,
		OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByT_M(
			categoryId, month, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByT_M_Last(
			long categoryId, int month,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByT_M_Last(categoryId, month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("categoryId=");
		sb.append(categoryId);

		sb.append(", month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByT_M_Last(
		long categoryId, int month,
		OrderByComparator<Album> orderByComparator) {

		int count = countByT_M(categoryId, month);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByT_M(
			categoryId, month, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where categoryId = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param categoryId the category ID
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByT_M_PrevAndNext(
			long albumId, long categoryId, int month,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByT_M_PrevAndNext(
				session, album, categoryId, month, orderByComparator, true);

			array[1] = album;

			array[2] = getByT_M_PrevAndNext(
				session, album, categoryId, month, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByT_M_PrevAndNext(
		Session session, Album album, long categoryId, int month,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_T_M_CATEGORYID_2);

		sb.append(_FINDER_COLUMN_T_M_MONTH_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(categoryId);

		queryPos.add(month);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where categoryId = &#63; and month = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 */
	@Override
	public void removeByT_M(long categoryId, int month) {
		for (Album album :
				findByT_M(
					categoryId, month, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where categoryId = &#63; and month = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param month the month
	 * @return the number of matching albums
	 */
	@Override
	public int countByT_M(long categoryId, int month) {
		FinderPath finderPath = _finderPathCountByT_M;

		Object[] finderArgs = new Object[] {categoryId, month};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_T_M_CATEGORYID_2);

			sb.append(_FINDER_COLUMN_T_M_MONTH_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(categoryId);

				queryPos.add(month);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_T_M_CATEGORYID_2 =
		"album.categoryId = ? AND ";

	private static final String _FINDER_COLUMN_T_M_MONTH_2 = "album.month = ?";

	private FinderPath _finderPathWithPaginationFindByY_M;
	private FinderPath _finderPathWithoutPaginationFindByY_M;
	private FinderPath _finderPathCountByY_M;

	/**
	 * Returns all the albums where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @return the matching albums
	 */
	@Override
	public List<Album> findByY_M(int year, int month) {
		return findByY_M(
			year, month, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of matching albums
	 */
	@Override
	public List<Album> findByY_M(int year, int month, int start, int end) {
		return findByY_M(year, month, start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByY_M(
		int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator) {

		return findByY_M(year, month, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums where year = &#63; and month = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param year the year
	 * @param month the month
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching albums
	 */
	@Override
	public List<Album> findByY_M(
		int year, int month, int start, int end,
		OrderByComparator<Album> orderByComparator, boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindByY_M;
				finderArgs = new Object[] {year, month};
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindByY_M;
			finderArgs = new Object[] {
				year, month, start, end, orderByComparator
			};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Album album : list) {
					if ((year != album.getYear()) ||
						(month != album.getMonth())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler sb = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				sb = new StringBundler(4);
			}

			sb.append(_SQL_SELECT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_Y_M_YEAR_2);

			sb.append(_FINDER_COLUMN_Y_M_MONTH_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else {
				sb.append(AlbumModelImpl.ORDER_BY_JPQL);
			}

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(year);

				queryPos.add(month);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByY_M_First(
			int year, int month, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByY_M_First(year, month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("year=");
		sb.append(year);

		sb.append(", month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the first album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByY_M_First(
		int year, int month, OrderByComparator<Album> orderByComparator) {

		List<Album> list = findByY_M(year, month, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album
	 * @throws NoSuchAlbumException if a matching album could not be found
	 */
	@Override
	public Album findByY_M_Last(
			int year, int month, OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = fetchByY_M_Last(year, month, orderByComparator);

		if (album != null) {
			return album;
		}

		StringBundler sb = new StringBundler(6);

		sb.append(_NO_SUCH_ENTITY_WITH_KEY);

		sb.append("year=");
		sb.append(year);

		sb.append(", month=");
		sb.append(month);

		sb.append("}");

		throw new NoSuchAlbumException(sb.toString());
	}

	/**
	 * Returns the last album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching album, or <code>null</code> if a matching album could not be found
	 */
	@Override
	public Album fetchByY_M_Last(
		int year, int month, OrderByComparator<Album> orderByComparator) {

		int count = countByY_M(year, month);

		if (count == 0) {
			return null;
		}

		List<Album> list = findByY_M(
			year, month, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the albums before and after the current album in the ordered set where year = &#63; and month = &#63;.
	 *
	 * @param albumId the primary key of the current album
	 * @param year the year
	 * @param month the month
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album[] findByY_M_PrevAndNext(
			long albumId, int year, int month,
			OrderByComparator<Album> orderByComparator)
		throws NoSuchAlbumException {

		Album album = findByPrimaryKey(albumId);

		Session session = null;

		try {
			session = openSession();

			Album[] array = new AlbumImpl[3];

			array[0] = getByY_M_PrevAndNext(
				session, album, year, month, orderByComparator, true);

			array[1] = album;

			array[2] = getByY_M_PrevAndNext(
				session, album, year, month, orderByComparator, false);

			return array;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	protected Album getByY_M_PrevAndNext(
		Session session, Album album, int year, int month,
		OrderByComparator<Album> orderByComparator, boolean previous) {

		StringBundler sb = null;

		if (orderByComparator != null) {
			sb = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			sb = new StringBundler(4);
		}

		sb.append(_SQL_SELECT_ALBUM_WHERE);

		sb.append(_FINDER_COLUMN_Y_M_YEAR_2);

		sb.append(_FINDER_COLUMN_Y_M_MONTH_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				sb.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						sb.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(WHERE_GREATER_THAN);
					}
					else {
						sb.append(WHERE_LESSER_THAN);
					}
				}
			}

			sb.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				sb.append(_ORDER_BY_ENTITY_ALIAS);
				sb.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						sb.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						sb.append(ORDER_BY_ASC);
					}
					else {
						sb.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			sb.append(AlbumModelImpl.ORDER_BY_JPQL);
		}

		String sql = sb.toString();

		Query query = session.createQuery(sql);

		query.setFirstResult(0);
		query.setMaxResults(2);

		QueryPos queryPos = QueryPos.getInstance(query);

		queryPos.add(year);

		queryPos.add(month);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(album)) {

				queryPos.add(orderByConditionValue);
			}
		}

		List<Album> list = query.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the albums where year = &#63; and month = &#63; from the database.
	 *
	 * @param year the year
	 * @param month the month
	 */
	@Override
	public void removeByY_M(int year, int month) {
		for (Album album :
				findByY_M(
					year, month, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(album);
		}
	}

	/**
	 * Returns the number of albums where year = &#63; and month = &#63;.
	 *
	 * @param year the year
	 * @param month the month
	 * @return the number of matching albums
	 */
	@Override
	public int countByY_M(int year, int month) {
		FinderPath finderPath = _finderPathCountByY_M;

		Object[] finderArgs = new Object[] {year, month};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler sb = new StringBundler(3);

			sb.append(_SQL_COUNT_ALBUM_WHERE);

			sb.append(_FINDER_COLUMN_Y_M_YEAR_2);

			sb.append(_FINDER_COLUMN_Y_M_MONTH_2);

			String sql = sb.toString();

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				QueryPos queryPos = QueryPos.getInstance(query);

				queryPos.add(year);

				queryPos.add(month);

				count = (Long)query.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_Y_M_YEAR_2 =
		"album.year = ? AND ";

	private static final String _FINDER_COLUMN_Y_M_MONTH_2 = "album.month = ?";

	public AlbumPersistenceImpl() {
		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		setDBColumnNames(dbColumnNames);

		setModelClass(Album.class);

		setModelImplClass(AlbumImpl.class);
		setModelPKClass(long.class);
	}

	/**
	 * Caches the album in the entity cache if it is enabled.
	 *
	 * @param album the album
	 */
	@Override
	public void cacheResult(Album album) {
		entityCache.putResult(AlbumImpl.class, album.getPrimaryKey(), album);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {album.getUuid(), album.getGroupId()}, album);
	}

	private int _valueObjectFinderCacheListThreshold;

	/**
	 * Caches the albums in the entity cache if it is enabled.
	 *
	 * @param albums the albums
	 */
	@Override
	public void cacheResult(List<Album> albums) {
		if ((_valueObjectFinderCacheListThreshold == 0) ||
			((_valueObjectFinderCacheListThreshold > 0) &&
			 (albums.size() > _valueObjectFinderCacheListThreshold))) {

			return;
		}

		for (Album album : albums) {
			if (entityCache.getResult(AlbumImpl.class, album.getPrimaryKey()) ==
					null) {

				cacheResult(album);
			}
		}
	}

	/**
	 * Clears the cache for all albums.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(AlbumImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the album.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Album album) {
		entityCache.removeResult(AlbumImpl.class, album);
	}

	@Override
	public void clearCache(List<Album> albums) {
		for (Album album : albums) {
			entityCache.removeResult(AlbumImpl.class, album);
		}
	}

	@Override
	public void clearCache(Set<Serializable> primaryKeys) {
		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Serializable primaryKey : primaryKeys) {
			entityCache.removeResult(AlbumImpl.class, primaryKey);
		}
	}

	protected void cacheUniqueFindersCache(AlbumModelImpl albumModelImpl) {
		Object[] args = new Object[] {
			albumModelImpl.getUuid(), albumModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, albumModelImpl, false);
	}

	/**
	 * Creates a new album with the primary key. Does not add the album to the database.
	 *
	 * @param albumId the primary key for the new album
	 * @return the new album
	 */
	@Override
	public Album create(long albumId) {
		Album album = new AlbumImpl();

		album.setNew(true);
		album.setPrimaryKey(albumId);

		String uuid = PortalUUIDUtil.generate();

		album.setUuid(uuid);

		album.setCompanyId(CompanyThreadLocal.getCompanyId());

		return album;
	}

	/**
	 * Removes the album with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param albumId the primary key of the album
	 * @return the album that was removed
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album remove(long albumId) throws NoSuchAlbumException {
		return remove((Serializable)albumId);
	}

	/**
	 * Removes the album with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the album
	 * @return the album that was removed
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album remove(Serializable primaryKey) throws NoSuchAlbumException {
		Session session = null;

		try {
			session = openSession();

			Album album = (Album)session.get(AlbumImpl.class, primaryKey);

			if (album == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchAlbumException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(album);
		}
		catch (NoSuchAlbumException noSuchEntityException) {
			throw noSuchEntityException;
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Album removeImpl(Album album) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(album)) {
				album = (Album)session.get(
					AlbumImpl.class, album.getPrimaryKeyObj());
			}

			if (album != null) {
				session.delete(album);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		if (album != null) {
			clearCache(album);
		}

		return album;
	}

	@Override
	public Album updateImpl(Album album) {
		boolean isNew = album.isNew();

		if (!(album instanceof AlbumModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(album.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(album);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in album proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Album implementation " +
					album.getClass());
		}

		AlbumModelImpl albumModelImpl = (AlbumModelImpl)album;

		if (Validator.isNull(album.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			album.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date date = new Date();

		if (isNew && (album.getCreateDate() == null)) {
			if (serviceContext == null) {
				album.setCreateDate(date);
			}
			else {
				album.setCreateDate(serviceContext.getCreateDate(date));
			}
		}

		if (!albumModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				album.setModifiedDate(date);
			}
			else {
				album.setModifiedDate(serviceContext.getModifiedDate(date));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (isNew) {
				session.save(album);
			}
			else {
				album = (Album)session.merge(album);
			}
		}
		catch (Exception exception) {
			throw processException(exception);
		}
		finally {
			closeSession(session);
		}

		entityCache.putResult(AlbumImpl.class, albumModelImpl, false, true);

		cacheUniqueFindersCache(albumModelImpl);

		if (isNew) {
			album.setNew(false);
		}

		album.resetOriginalValues();

		return album;
	}

	/**
	 * Returns the album with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the album
	 * @return the album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album findByPrimaryKey(Serializable primaryKey)
		throws NoSuchAlbumException {

		Album album = fetchByPrimaryKey(primaryKey);

		if (album == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchAlbumException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return album;
	}

	/**
	 * Returns the album with the primary key or throws a <code>NoSuchAlbumException</code> if it could not be found.
	 *
	 * @param albumId the primary key of the album
	 * @return the album
	 * @throws NoSuchAlbumException if a album with the primary key could not be found
	 */
	@Override
	public Album findByPrimaryKey(long albumId) throws NoSuchAlbumException {
		return findByPrimaryKey((Serializable)albumId);
	}

	/**
	 * Returns the album with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param albumId the primary key of the album
	 * @return the album, or <code>null</code> if a album with the primary key could not be found
	 */
	@Override
	public Album fetchByPrimaryKey(long albumId) {
		return fetchByPrimaryKey((Serializable)albumId);
	}

	/**
	 * Returns all the albums.
	 *
	 * @return the albums
	 */
	@Override
	public List<Album> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @return the range of albums
	 */
	@Override
	public List<Album> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of albums
	 */
	@Override
	public List<Album> findAll(
		int start, int end, OrderByComparator<Album> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the albums.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AlbumModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of albums
	 * @param end the upper bound of the range of albums (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of albums
	 */
	@Override
	public List<Album> findAll(
		int start, int end, OrderByComparator<Album> orderByComparator,
		boolean useFinderCache) {

		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			if (useFinderCache) {
				finderPath = _finderPathWithoutPaginationFindAll;
				finderArgs = FINDER_ARGS_EMPTY;
			}
		}
		else if (useFinderCache) {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Album> list = null;

		if (useFinderCache) {
			list = (List<Album>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler sb = null;
			String sql = null;

			if (orderByComparator != null) {
				sb = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				sb.append(_SQL_SELECT_ALBUM);

				appendOrderByComparator(
					sb, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = sb.toString();
			}
			else {
				sql = _SQL_SELECT_ALBUM;

				sql = sql.concat(AlbumModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(sql);

				list = (List<Album>)QueryUtil.list(
					query, getDialect(), start, end);

				cacheResult(list);

				if (useFinderCache) {
					finderCache.putResult(finderPath, finderArgs, list);
				}
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the albums from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Album album : findAll()) {
			remove(album);
		}
	}

	/**
	 * Returns the number of albums.
	 *
	 * @return the number of albums
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query query = session.createQuery(_SQL_COUNT_ALBUM);

				count = (Long)query.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception exception) {
				throw processException(exception);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected EntityCache getEntityCache() {
		return entityCache;
	}

	@Override
	protected String getPKDBName() {
		return "albumId";
	}

	@Override
	protected String getSelectSQL() {
		return _SQL_SELECT_ALBUM;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return AlbumModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the album persistence.
	 */
	@Activate
	public void activate(BundleContext bundleContext) {
		_bundleContext = bundleContext;

		_argumentsResolverServiceRegistration = _bundleContext.registerService(
			ArgumentsResolver.class, new AlbumModelArgumentsResolver(),
			MapUtil.singletonDictionary(
				"model.class.name", Album.class.getName()));

		_valueObjectFinderCacheListThreshold = GetterUtil.getInteger(
			PropsUtil.get(PropsKeys.VALUE_OBJECT_FINDER_CACHE_LIST_THRESHOLD));

		_finderPathWithPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathWithoutPaginationFindAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0],
			new String[0], true);

		_finderPathCountAll = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0], new String[0], false);

		_finderPathWithPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"uuid_"}, true);

		_finderPathWithoutPaginationFindByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			true);

		_finderPathCountByUuid = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()}, new String[] {"uuid_"},
			false);

		_finderPathFetchByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, true);

		_finderPathCountByUUID_G = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "groupId"}, false);

		_finderPathWithPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathWithoutPaginationFindByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, true);

		_finderPathCountByUuid_C = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			new String[] {"uuid_", "companyId"}, false);

		_finderPathWithPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"groupId"}, true);

		_finderPathWithoutPaginationFindByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			true);

		_finderPathCountByGroupId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] {Long.class.getName()}, new String[] {"groupId"},
			false);

		_finderPathWithPaginationFindByCategoryId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategoryId",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"categoryId"}, true);

		_finderPathWithoutPaginationFindByCategoryId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCategoryId",
			new String[] {Long.class.getName()}, new String[] {"categoryId"},
			true);

		_finderPathCountByCategoryId = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCategoryId",
			new String[] {Long.class.getName()}, new String[] {"categoryId"},
			false);

		_finderPathWithPaginationFindByYear = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByYear",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"year"}, true);

		_finderPathWithoutPaginationFindByYear = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByYear",
			new String[] {Integer.class.getName()}, new String[] {"year"},
			true);

		_finderPathCountByYear = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByYear",
			new String[] {Integer.class.getName()}, new String[] {"year"},
			false);

		_finderPathWithPaginationFindByMonth = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByMonth",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"month"}, true);

		_finderPathWithoutPaginationFindByMonth = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByMonth",
			new String[] {Integer.class.getName()}, new String[] {"month"},
			true);

		_finderPathCountByMonth = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByMonth",
			new String[] {Integer.class.getName()}, new String[] {"month"},
			false);

		_finderPathWithPaginationFindByT_Y_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByT_Y_M",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			},
			new String[] {"categoryId", "year", "month"}, true);

		_finderPathWithoutPaginationFindByT_Y_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByT_Y_M",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName()
			},
			new String[] {"categoryId", "year", "month"}, true);

		_finderPathCountByT_Y_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByT_Y_M",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName()
			},
			new String[] {"categoryId", "year", "month"}, false);

		_finderPathWithPaginationFindByT_Y = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByT_Y",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"categoryId", "year"}, true);

		_finderPathWithoutPaginationFindByT_Y = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByT_Y",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"categoryId", "year"}, true);

		_finderPathCountByT_Y = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByT_Y",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"categoryId", "year"}, false);

		_finderPathWithPaginationFindByT_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByT_M",
			new String[] {
				Long.class.getName(), Integer.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"categoryId", "month"}, true);

		_finderPathWithoutPaginationFindByT_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByT_M",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"categoryId", "month"}, true);

		_finderPathCountByT_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByT_M",
			new String[] {Long.class.getName(), Integer.class.getName()},
			new String[] {"categoryId", "month"}, false);

		_finderPathWithPaginationFindByY_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByY_M",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			},
			new String[] {"year", "month"}, true);

		_finderPathWithoutPaginationFindByY_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByY_M",
			new String[] {Integer.class.getName(), Integer.class.getName()},
			new String[] {"year", "month"}, true);

		_finderPathCountByY_M = _createFinderPath(
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByY_M",
			new String[] {Integer.class.getName(), Integer.class.getName()},
			new String[] {"year", "month"}, false);

		_setAlbumUtilPersistence(this);
	}

	@Deactivate
	public void deactivate() {
		_setAlbumUtilPersistence(null);

		entityCache.removeCache(AlbumImpl.class.getName());

		_argumentsResolverServiceRegistration.unregister();

		for (ServiceRegistration<FinderPath> serviceRegistration :
				_serviceRegistrations) {

			serviceRegistration.unregister();
		}
	}

	private void _setAlbumUtilPersistence(AlbumPersistence albumPersistence) {
		try {
			Field field = AlbumUtil.class.getDeclaredField("_persistence");

			field.setAccessible(true);

			field.set(null, albumPersistence);
		}
		catch (ReflectiveOperationException reflectiveOperationException) {
			throw new RuntimeException(reflectiveOperationException);
		}
	}

	@Override
	@Reference(
		target = AlbumPersistenceConstants.SERVICE_CONFIGURATION_FILTER,
		unbind = "-"
	)
	public void setConfiguration(Configuration configuration) {
	}

	@Override
	@Reference(
		target = AlbumPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Override
	@Reference(
		target = AlbumPersistenceConstants.ORIGIN_BUNDLE_SYMBOLIC_NAME_FILTER,
		unbind = "-"
	)
	public void setSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	private BundleContext _bundleContext;

	@Reference
	protected EntityCache entityCache;

	@Reference
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_ALBUM =
		"SELECT album FROM Album album";

	private static final String _SQL_SELECT_ALBUM_WHERE =
		"SELECT album FROM Album album WHERE ";

	private static final String _SQL_COUNT_ALBUM =
		"SELECT COUNT(album) FROM Album album";

	private static final String _SQL_COUNT_ALBUM_WHERE =
		"SELECT COUNT(album) FROM Album album WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "album.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Album exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Album exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		AlbumPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

	private FinderPath _createFinderPath(
		String cacheName, String methodName, String[] params,
		String[] columnNames, boolean baseModelResult) {

		FinderPath finderPath = new FinderPath(
			cacheName, methodName, params, columnNames, baseModelResult);

		if (!cacheName.equals(FINDER_CLASS_NAME_LIST_WITH_PAGINATION)) {
			_serviceRegistrations.add(
				_bundleContext.registerService(
					FinderPath.class, finderPath,
					MapUtil.singletonDictionary("cache.name", cacheName)));
		}

		return finderPath;
	}

	private Set<ServiceRegistration<FinderPath>> _serviceRegistrations =
		new HashSet<>();
	private ServiceRegistration<ArgumentsResolver>
		_argumentsResolverServiceRegistration;

	private static class AlbumModelArgumentsResolver
		implements ArgumentsResolver {

		@Override
		public Object[] getArguments(
			FinderPath finderPath, BaseModel<?> baseModel, boolean checkColumn,
			boolean original) {

			String[] columnNames = finderPath.getColumnNames();

			if ((columnNames == null) || (columnNames.length == 0)) {
				if (baseModel.isNew()) {
					return new Object[0];
				}

				return null;
			}

			AlbumModelImpl albumModelImpl = (AlbumModelImpl)baseModel;

			long columnBitmask = albumModelImpl.getColumnBitmask();

			if (!checkColumn || (columnBitmask == 0)) {
				return _getValue(albumModelImpl, columnNames, original);
			}

			Long finderPathColumnBitmask = _finderPathColumnBitmasksCache.get(
				finderPath);

			if (finderPathColumnBitmask == null) {
				finderPathColumnBitmask = 0L;

				for (String columnName : columnNames) {
					finderPathColumnBitmask |= albumModelImpl.getColumnBitmask(
						columnName);
				}

				if (finderPath.isBaseModelResult() &&
					(AlbumPersistenceImpl.
						FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION ==
							finderPath.getCacheName())) {

					finderPathColumnBitmask |= _ORDER_BY_COLUMNS_BITMASK;
				}

				_finderPathColumnBitmasksCache.put(
					finderPath, finderPathColumnBitmask);
			}

			if ((columnBitmask & finderPathColumnBitmask) != 0) {
				return _getValue(albumModelImpl, columnNames, original);
			}

			return null;
		}

		private static Object[] _getValue(
			AlbumModelImpl albumModelImpl, String[] columnNames,
			boolean original) {

			Object[] arguments = new Object[columnNames.length];

			for (int i = 0; i < arguments.length; i++) {
				String columnName = columnNames[i];

				if (original) {
					arguments[i] = albumModelImpl.getColumnOriginalValue(
						columnName);
				}
				else {
					arguments[i] = albumModelImpl.getColumnValue(columnName);
				}
			}

			return arguments;
		}

		private static final Map<FinderPath, Long>
			_finderPathColumnBitmasksCache = new ConcurrentHashMap<>();

		private static final long _ORDER_BY_COLUMNS_BITMASK;

		static {
			long orderByColumnsBitmask = 0;

			orderByColumnsBitmask |= AlbumModelImpl.getColumnBitmask(
				"createDate");

			_ORDER_BY_COLUMNS_BITMASK = orderByColumnsBitmask;
		}

	}

}