/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package albums.model.impl;

import albums.model.Album;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Album in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AlbumCacheModel implements CacheModel<Album>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AlbumCacheModel)) {
			return false;
		}

		AlbumCacheModel albumCacheModel = (AlbumCacheModel)object;

		if (albumId == albumCacheModel.albumId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, albumId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", albumId=");
		sb.append(albumId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", displayDate=");
		sb.append(displayDate);
		sb.append(", year=");
		sb.append(year);
		sb.append(", month=");
		sb.append(month);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Album toEntityModel() {
		AlbumImpl albumImpl = new AlbumImpl();

		if (uuid == null) {
			albumImpl.setUuid("");
		}
		else {
			albumImpl.setUuid(uuid);
		}

		albumImpl.setAlbumId(albumId);
		albumImpl.setGroupId(groupId);
		albumImpl.setCompanyId(companyId);
		albumImpl.setUserId(userId);

		if (userName == null) {
			albumImpl.setUserName("");
		}
		else {
			albumImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			albumImpl.setCreateDate(null);
		}
		else {
			albumImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			albumImpl.setModifiedDate(null);
		}
		else {
			albumImpl.setModifiedDate(new Date(modifiedDate));
		}

		albumImpl.setCategoryId(categoryId);

		if (title == null) {
			albumImpl.setTitle("");
		}
		else {
			albumImpl.setTitle(title);
		}

		if (displayDate == Long.MIN_VALUE) {
			albumImpl.setDisplayDate(null);
		}
		else {
			albumImpl.setDisplayDate(new Date(displayDate));
		}

		albumImpl.setYear(year);
		albumImpl.setMonth(month);

		if (listingImage == null) {
			albumImpl.setListingImage("");
		}
		else {
			albumImpl.setListingImage(listingImage);
		}

		if (description == null) {
			albumImpl.setDescription("");
		}
		else {
			albumImpl.setDescription(description);
		}

		albumImpl.resetOriginalValues();

		return albumImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		albumId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		categoryId = objectInput.readLong();
		title = objectInput.readUTF();
		displayDate = objectInput.readLong();

		year = objectInput.readInt();

		month = objectInput.readInt();
		listingImage = objectInput.readUTF();
		description = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(albumId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(categoryId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		objectOutput.writeLong(displayDate);

		objectOutput.writeInt(year);

		objectOutput.writeInt(month);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}
	}

	public String uuid;
	public long albumId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long categoryId;
	public String title;
	public long displayDate;
	public int year;
	public int month;
	public String listingImage;
	public String description;

}