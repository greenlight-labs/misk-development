<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= AlbumLocalServiceUtil.countByGroupId(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= AlbumLocalServiceUtil.findByGroupId(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="albums.model.Album" modelVar="album">

            <liferay-ui:search-container-column-text name="Name" value="<%= HtmlUtil.escape(album.getTitle(locale)) %>"/>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>