package albums.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import albums.admin.web.constants.AlbumsAdminWebPanelCategoryKeys;
import albums.admin.web.constants.AlbumsAdminWebPortletKeys;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=101",
		"panel.category.key=" + AlbumsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class CategoriesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return AlbumsAdminWebPortletKeys.CATEGORIESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + AlbumsAdminWebPortletKeys.CATEGORIESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}