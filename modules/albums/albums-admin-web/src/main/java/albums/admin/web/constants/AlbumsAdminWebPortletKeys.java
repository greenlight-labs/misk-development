package albums.admin.web.constants;

/**
 * @author tz
 */
public class AlbumsAdminWebPortletKeys {

	public static final String ALBUMSADMINWEB =
		"albums_admin_web_AlbumsAdminWebPortlet";

	public static final String CATEGORIESADMINWEB =
			"albums_admin_web_CategoriesAdminWebPortlet";
}