package albums.admin.web.application.list;

import albums.admin.web.constants.AlbumsAdminWebPanelCategoryKeys;
import albums.admin.web.constants.AlbumsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + AlbumsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class AlbumsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return AlbumsAdminWebPortletKeys.ALBUMSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + AlbumsAdminWebPortletKeys.ALBUMSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}