create table misk_highlights (
	uuid_ VARCHAR(75) null,
	highlightId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	tagId LONG,
	categoryId LONG,
	orderNo LONG,
	name STRING null,
	profession STRING null,
	location STRING null,
	image STRING null,
	description STRING null,
	audio STRING null,
	video STRING null,
	facebook STRING null,
	twitter STRING null,
	linkedin STRING null
);

create table misk_highlights_categories (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	color VARCHAR(75) null,
	status BOOLEAN
);

create table misk_highlights_tags (
	uuid_ VARCHAR(75) null,
	tagId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name STRING null,
	slug VARCHAR(75) null,
	description STRING null
);