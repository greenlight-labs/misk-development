create index IX_4C6E2EFA on misk_highlights (groupId);
create index IX_E627AE15 on misk_highlights (tagId);
create index IX_AEB661A4 on misk_highlights (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_58F30126 on misk_highlights (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_BBD9EFF3 on misk_highlights_categories (groupId);
create index IX_17F1A8CB on misk_highlights_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_B6D3E60D on misk_highlights_categories (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_FFCE5E16 on misk_highlights_tags (groupId);
create unique index IX_19BBBDD3 on misk_highlights_tags (name[$COLUMN_LENGTH:75$]);
create unique index IX_22E2A5F3 on misk_highlights_tags (slug[$COLUMN_LENGTH:75$]);
create index IX_FB237208 on misk_highlights_tags (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_CD70DA8A on misk_highlights_tags (uuid_[$COLUMN_LENGTH:75$], groupId);