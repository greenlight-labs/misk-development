package highlights.service.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriInfo;

import com.push.notification.constants.PushNotificationKeys;
import com.push.notification.service.PushNotificationLocalServiceUtil;
import highlights.model.Category;
import highlights.model.Tag;
import highlights.service.CategoryLocalServiceUtil;
import highlights.service.TagLocalServiceUtil;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;

import highlights.exception.HighlightValidateException;
import highlights.model.Highlight;
import highlights.service.HighlightLocalServiceUtil;
import highlights.service.base.HighlightLocalServiceBaseImpl;
import highlights.service.indexer.HighlightIndexer;
import highlights.service.util.HighlightValidator;

/**
 * The implementation of the highlight local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>highlights.service.HighlightLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see HighlightLocalServiceBaseImpl
 */
@Component(property = "model.class.name=highlights.model.Highlight", service = AopService.class)
public class HighlightLocalServiceImpl extends HighlightLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>highlights.service.HighlightLocalService</code> via injection or a
	 * <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>highlights.service.HighlightLocalServiceUtil</code>.
	 */

	public Highlight addEntry(Highlight orgEntry, ServiceContext serviceContext)
			throws PortalException, HighlightValidateException {

		// Validation

		ModelValidator<Highlight> modelValidator = new HighlightValidator();
		modelValidator.validate(orgEntry);

		// Add entry

		Highlight entry = _addEntry(orgEntry, serviceContext);

		Highlight addedEntry = highlightPersistence.update(entry);
		highlightPersistence.clearCache();

		// get request from serviceContext
		HttpServletRequest httpServletRequest = serviceContext.getRequest();
		// get sendPushNotification parameter from request
		boolean sendPushNotification = ParamUtil.getBoolean(httpServletRequest, "sendPushNotification");

		if(sendPushNotification){
			// send item added push notification
			if(Validator.isNotNull(addedEntry)) {
				String notificationBody = "New Highlight: \""+addedEntry.getName("en_US")+"\".";
				// get type additional payload
				JSONObject additionalPayload = getAdditionalPayload(addedEntry, serviceContext);
				PushNotificationLocalServiceUtil.saveNotification(PushNotificationKeys.NOTIFICATION_TYPE_HIGHLIGHT, addedEntry.getHighlightId(), addedEntry.getName(), notificationBody, additionalPayload);
			}
		}

		return addedEntry;
	}

	private JSONObject getAdditionalPayload(Highlight entry, ServiceContext serviceContext) {
		//get language id from request
		ThemeDisplay themeDisplay = (ThemeDisplay) serviceContext.getRequest().getAttribute(WebKeys.THEME_DISPLAY);
		String languageId = themeDisplay.getLanguageId();
		// get base url from service context
		String baseUrl = serviceContext.getPortalURL();
		// create typeAdditionalPayload for highlight object
		Category category = CategoryLocalServiceUtil.fetchCategory(entry.getCategoryId());
		Tag tag = TagLocalServiceUtil.fetchTag(entry.getTagId());
		// create json object
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		jsonObject.put("highlightId", entry.getHighlightId());
		jsonObject.put("name", entry.getName(languageId));
		jsonObject.put("profession", entry.getProfession(languageId));
		jsonObject.put("location", entry.getLocation(languageId));
		jsonObject.put("image", getMediaUrl(entry.getImage(languageId), baseUrl));
		jsonObject.put("description", entry.getDescription(languageId));
		jsonObject.put("facebook", entry.getFacebook(languageId));
		jsonObject.put("twitter", entry.getTwitter(languageId));
		jsonObject.put("linkedin", entry.getLinkedin(languageId));
		jsonObject.put("isFavourite", Boolean.FALSE);
		jsonObject.put("type", "image");
		if(Validator.isNotNull(entry.getVideo(languageId))){
			jsonObject.put("type", "video");
			jsonObject.put("video", getMediaUrl(entry.getVideo(languageId), baseUrl));
		} else if(Validator.isNotNull(entry.getAudio(languageId))){
			jsonObject.put("type", "audio");
			jsonObject.put("audio", getMediaUrl(entry.getAudio(languageId), baseUrl));
		}
		// set category and tag fields empty initially and then set them if they are not null
		jsonObject.put("catName", "");
		jsonObject.put("catColor", "");
		jsonObject.put("tagSlug", "");
		jsonObject.put("tagTitle", "");
		jsonObject.put("tagDescription", "");

		if (Validator.isNotNull(category)) {
			jsonObject.put("catName", category.getName(languageId));
			jsonObject.put("catColor", category.getColor());
		}
		if (Validator.isNotNull(tag)) {
			jsonObject.put("tagSlug", tag.getSlug());
			jsonObject.put("tagTitle", tag.getName(languageId));
			jsonObject.put("tagDescription", tag.getDescription(languageId));
		}

		return jsonObject;
	}

	public static String getMediaUrl(String mediaUrl, String baseUrl){
		// check mediaUrl is not empty and return it with base url
		if(StringUtils.isNotEmpty(mediaUrl)){
			return baseUrl + mediaUrl;
		}
		return StringPool.BLANK;
	}

	public Highlight updateEntry(Highlight orgEntry, ServiceContext serviceContext)
			throws PortalException, HighlightValidateException {

		User user = userLocalService.getUser(orgEntry.getUserId());

		// Validation

		ModelValidator<Highlight> modelValidator = new HighlightValidator();
		modelValidator.validate(orgEntry);

		// Update entry

		Highlight entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

		Highlight updatedEntry = highlightPersistence.update(entry);
		highlightPersistence.clearCache();

		return updatedEntry;
	}

	protected Highlight _addEntry(Highlight entry, ServiceContext serviceContext) throws PortalException {

		long id = counterLocalService.increment(Highlight.class.getName());

		Highlight newEntry = highlightPersistence.create(id);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		newEntry.setCompanyId(entry.getCompanyId());
		newEntry.setGroupId(entry.getGroupId());
		newEntry.setUserId(user.getUserId());
		newEntry.setUserName(user.getFullName());
		newEntry.setCreateDate(now);
		newEntry.setModifiedDate(now);

		newEntry.setUuid(serviceContext.getUuid());
		newEntry.setOrderNo(entry.getOrderNo()!=0?entry.getOrderNo():9999);
		newEntry.setTagId(entry.getTagId());
		newEntry.setCategoryId(entry.getCategoryId());
		newEntry.setName(entry.getName());
		newEntry.setProfession(entry.getProfession());
		newEntry.setLocation(entry.getLocation());
		newEntry.setImage(entry.getImage());
		newEntry.setDescription(entry.getDescription());
		newEntry.setAudio(entry.getAudio());
		newEntry.setVideo(entry.getVideo());
		newEntry.setFacebook(entry.getFacebook());
		newEntry.setTwitter(entry.getTwitter());
		newEntry.setLinkedin(entry.getLinkedin());

		return newEntry;
	}

	protected Highlight _updateEntry(long primaryKey, Highlight entry, ServiceContext serviceContext)
			throws PortalException {

		Highlight updateEntry = fetchHighlight(primaryKey);

		User user = userLocalService.getUser(entry.getUserId());

		Date now = new Date();
		updateEntry.setCompanyId(entry.getCompanyId());
		updateEntry.setGroupId(entry.getGroupId());
		updateEntry.setUserId(user.getUserId());
		updateEntry.setUserName(user.getFullName());
		updateEntry.setCreateDate(entry.getCreateDate());
		updateEntry.setModifiedDate(now);

		updateEntry.setUuid(entry.getUuid());
		updateEntry.setOrderNo(entry.getOrderNo()!=0?entry.getOrderNo():9999);
		updateEntry.setHighlightId(entry.getHighlightId());
		updateEntry.setTagId(entry.getTagId());
		updateEntry.setCategoryId(entry.getCategoryId());
		updateEntry.setName(entry.getName());
		updateEntry.setProfession(entry.getProfession());
		updateEntry.setLocation(entry.getLocation());
		updateEntry.setImage(entry.getImage());
		updateEntry.setDescription(entry.getDescription());
		updateEntry.setAudio(entry.getAudio());
		updateEntry.setVideo(entry.getVideo());
		updateEntry.setFacebook(entry.getFacebook());
		updateEntry.setTwitter(entry.getTwitter());
		updateEntry.setLinkedin(entry.getLinkedin());

		return updateEntry;
	}

	public Highlight deleteEntry(long primaryKey) throws PortalException {
		Highlight entry = getHighlight(primaryKey);
		highlightPersistence.remove(entry);

		return entry;
	}

	public List<Highlight> findAllInGroup(long groupId) {

		return highlightPersistence.findByGroupId(groupId);
	}

	public List<Highlight> findAllInGroup(long groupId, int start, int end, OrderByComparator<Highlight> obc) {

		return highlightPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Highlight> findAllInGroup(long groupId, int start, int end) {

		return highlightPersistence.findByGroupId(groupId, start, end);
	}

	public int countAllInGroup(long groupId) {

		return highlightPersistence.countByGroupId(groupId);
	}

	public List<Highlight> findByTagId(long tagId) {

		return highlightPersistence.findByTagId(tagId);
	}

	public List<Highlight> findByTagId(long tagId, int start, int end, OrderByComparator<Highlight> obc) {

		return highlightPersistence.findByTagId(tagId, start, end, obc);
	}

	public List<Highlight> findByTagId(long tagId, int start, int end) {

		return highlightPersistence.findByTagId(tagId, start, end);
	}

	public int countByTagId(long tagId) {

		return highlightPersistence.countByTagId(tagId);
	}

	public Highlight getHighlightFromRequest(long primaryKey, PortletRequest request)
			throws PortletException, HighlightValidateException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		// Create or fetch existing data

		Highlight entry;

		if (primaryKey <= 0) {
			entry = getNewObject(primaryKey);
		} else {
			entry = fetchHighlight(primaryKey);
		}

		try {
			entry.setHighlightId(primaryKey);
			entry.setTagId(ParamUtil.getLong(request, "tagId"));
			entry.setCategoryId(ParamUtil.getLong(request, "categoryId"));
			entry.setNameMap(LocalizationUtil.getLocalizationMap(request, "name"));
			entry.setProfessionMap(LocalizationUtil.getLocalizationMap(request, "profession"));
			entry.setLocationMap(LocalizationUtil.getLocalizationMap(request, "location"));
			entry.setImageMap(LocalizationUtil.getLocalizationMap(request, "image"));
			entry.setDescriptionMap(LocalizationUtil.getLocalizationMap(request, "description"));
			entry.setAudioMap(LocalizationUtil.getLocalizationMap(request, "audio"));
			entry.setVideoMap(LocalizationUtil.getLocalizationMap(request, "video"));
			entry.setFacebookMap(LocalizationUtil.getLocalizationMap(request, "facebook"));
			entry.setTwitterMap(LocalizationUtil.getLocalizationMap(request, "twitter"));
			entry.setLinkedinMap(LocalizationUtil.getLocalizationMap(request, "linkedin"));

			entry.setCompanyId(themeDisplay.getCompanyId());
			entry.setGroupId(themeDisplay.getScopeGroupId());
			entry.setUserId(themeDisplay.getUserId());
		} catch (Exception e) {
			_log.error("Errors occur while populating the model", e);
			List<String> error = new ArrayList<>();
			error.add("value-convert-error");

			throw new HighlightValidateException(error);
		}

		return entry;
	}

	public Highlight getNewObject(long primaryKey) {
		primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(Highlight.class.getName());

		return createHighlight(primaryKey);
	}

	// search
	@Override
	public List<Highlight> searchHighlights(SearchContext searchContext) {

		Hits hits;
		_log.info("searchHighlights");
		List<Highlight> highlightList = new ArrayList<>();
		HighlightIndexer indexer = (HighlightIndexer) IndexerRegistryUtil.getIndexer(Highlight.class);
		try {

			hits = indexer.search(searchContext);

			_log.info("indexer " + indexer.search(searchContext));
			for (int i = 0; i < hits.getDocs().length; i++) {
				Document doc = hits.doc(i);

				long highlightId = GetterUtil.getLong(doc.get(Field.ENTRY_CLASS_PK));
				Highlight highlight = null;
				highlight = HighlightLocalServiceUtil.getHighlight(highlightId);

				highlightList.add(highlight);
			}
		} catch (PortalException | SystemException e) {
			_log.error(e);
		}

		return highlightList;
	}

	private static Log _log = LogFactoryUtil.getLog(HighlightLocalServiceImpl.class);

}