package highlights.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import highlights.model.Category;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Category Validator
 * 
 * @author tz
 *
 */
public class CategoryValidator implements ModelValidator<Category> {

	@Override
	public void validate(Category entry) throws PortalException {

        // Fields: categoryId, name, color, status
        validateCategoryId(entry.getCategoryId());
        validateName(entry.getName());
        validateColor(entry.getColor());
        validateStatus(entry.getStatus());

	}

    protected void validateCategoryId(long field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("highlight-category-required");
        }
    }

    protected void validateName(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("highlight-name-required");
        }
    }

    protected void validateColor(String field) {
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("highlight-color-required");
        }
    }

    protected void validateStatus(boolean field) {
        if (!Validator.isNotNull(field)) {
            _errors.add("highlight-status-required");
        }
    }

	protected List<String> _errors = new ArrayList<>();

}
