package highlights.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ModelHintsUtil;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import highlights.exception.HighlightValidateException;
import highlights.model.Highlight;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Highlight Validator 
 * 
 * @author tz
 *
 */
public class HighlightValidator implements ModelValidator<Highlight> {

	@Override
	public void validate(Highlight entry) throws PortalException {
/*   */
        // Field highlightId
        validateHighlightId(entry.getHighlightId());

        // Field name
        validateName(entry.getName());

        // Field profession
        validateProfession(entry.getProfession());

        // Field location
        validateLocation(entry.getLocation());

        // Field image
        validateImage(entry.getImage());

        // Field description
        validateDescription(entry.getDescription());

		if (0 < _errors.size()) {
			throw new HighlightValidateException(_errors);
		}
		
	}

/*   */
    /**
    * highlightId field Validation
    *
    * @param field highlightId
    */
    protected void validateHighlightId(long field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
    }

    /**
    * name field Validation
    *
    * @param field name
    */
    protected void validateName(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("highlight-name-required");
        }

    }

    /**
    * profession field Validation
    *
    * @param field profession
    */
    protected void validateProfession(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("highlight-profession-required");
        }

    }

    /**
    * location field Validation
    *
    * @param field location
    */
    protected void validateLocation(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("highlight-location-required");
        }

    }

    /**
    * image field Validation
    *
    * @param field image
    */
    protected void validateImage(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.
        if (!StringUtils.isNotEmpty(field)) {
            _errors.add("highlight-image-required");
        }

    }

    /**
    * description field Validation
    *
    * @param field description
    */
    protected void validateDescription(String field) {
        //TODO : This validation needs to be implemented. Add error message key into _errors when an error occurs.

    }
	

	protected List<String> _errors = new ArrayList<>();

}
