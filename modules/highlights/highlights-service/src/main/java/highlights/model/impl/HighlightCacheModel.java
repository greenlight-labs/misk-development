/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import highlights.model.Highlight;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Highlight in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class HighlightCacheModel
	implements CacheModel<Highlight>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof HighlightCacheModel)) {
			return false;
		}

		HighlightCacheModel highlightCacheModel = (HighlightCacheModel)object;

		if (highlightId == highlightCacheModel.highlightId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, highlightId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(43);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", highlightId=");
		sb.append(highlightId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", tagId=");
		sb.append(tagId);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", orderNo=");
		sb.append(orderNo);
		sb.append(", name=");
		sb.append(name);
		sb.append(", profession=");
		sb.append(profession);
		sb.append(", location=");
		sb.append(location);
		sb.append(", image=");
		sb.append(image);
		sb.append(", description=");
		sb.append(description);
		sb.append(", audio=");
		sb.append(audio);
		sb.append(", video=");
		sb.append(video);
		sb.append(", facebook=");
		sb.append(facebook);
		sb.append(", twitter=");
		sb.append(twitter);
		sb.append(", linkedin=");
		sb.append(linkedin);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Highlight toEntityModel() {
		HighlightImpl highlightImpl = new HighlightImpl();

		if (uuid == null) {
			highlightImpl.setUuid("");
		}
		else {
			highlightImpl.setUuid(uuid);
		}

		highlightImpl.setHighlightId(highlightId);
		highlightImpl.setGroupId(groupId);
		highlightImpl.setCompanyId(companyId);
		highlightImpl.setUserId(userId);

		if (userName == null) {
			highlightImpl.setUserName("");
		}
		else {
			highlightImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			highlightImpl.setCreateDate(null);
		}
		else {
			highlightImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			highlightImpl.setModifiedDate(null);
		}
		else {
			highlightImpl.setModifiedDate(new Date(modifiedDate));
		}

		highlightImpl.setTagId(tagId);
		highlightImpl.setCategoryId(categoryId);
		highlightImpl.setOrderNo(orderNo);

		if (name == null) {
			highlightImpl.setName("");
		}
		else {
			highlightImpl.setName(name);
		}

		if (profession == null) {
			highlightImpl.setProfession("");
		}
		else {
			highlightImpl.setProfession(profession);
		}

		if (location == null) {
			highlightImpl.setLocation("");
		}
		else {
			highlightImpl.setLocation(location);
		}

		if (image == null) {
			highlightImpl.setImage("");
		}
		else {
			highlightImpl.setImage(image);
		}

		if (description == null) {
			highlightImpl.setDescription("");
		}
		else {
			highlightImpl.setDescription(description);
		}

		if (audio == null) {
			highlightImpl.setAudio("");
		}
		else {
			highlightImpl.setAudio(audio);
		}

		if (video == null) {
			highlightImpl.setVideo("");
		}
		else {
			highlightImpl.setVideo(video);
		}

		if (facebook == null) {
			highlightImpl.setFacebook("");
		}
		else {
			highlightImpl.setFacebook(facebook);
		}

		if (twitter == null) {
			highlightImpl.setTwitter("");
		}
		else {
			highlightImpl.setTwitter(twitter);
		}

		if (linkedin == null) {
			highlightImpl.setLinkedin("");
		}
		else {
			highlightImpl.setLinkedin(linkedin);
		}

		highlightImpl.resetOriginalValues();

		return highlightImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		highlightId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		tagId = objectInput.readLong();

		categoryId = objectInput.readLong();

		orderNo = objectInput.readLong();
		name = objectInput.readUTF();
		profession = objectInput.readUTF();
		location = objectInput.readUTF();
		image = objectInput.readUTF();
		description = objectInput.readUTF();
		audio = objectInput.readUTF();
		video = objectInput.readUTF();
		facebook = objectInput.readUTF();
		twitter = objectInput.readUTF();
		linkedin = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(highlightId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(tagId);

		objectOutput.writeLong(categoryId);

		objectOutput.writeLong(orderNo);

		if (name == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (profession == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(profession);
		}

		if (location == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(location);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (audio == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(audio);
		}

		if (video == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(video);
		}

		if (facebook == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(facebook);
		}

		if (twitter == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(twitter);
		}

		if (linkedin == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(linkedin);
		}
	}

	public String uuid;
	public long highlightId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long tagId;
	public long categoryId;
	public long orderNo;
	public String name;
	public String profession;
	public String location;
	public String image;
	public String description;
	public String audio;
	public String video;
	public String facebook;
	public String twitter;
	public String linkedin;

}