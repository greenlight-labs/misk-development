<%@ include file="init.jsp" %>
<%@page import="com.liferay.portal.kernel.portlet.PortalPreferences"%>
<%@page import="com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="java.util.Collections"%>
<%@page import="org.apache.commons.beanutils.BeanComparator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>

<%

 PortalPreferences portalPrefs = PortletPreferencesFactoryUtil.getPortalPreferences(request);
 String orderByCol = ParamUtil.getString(request, "orderByCol");
 String orderByType = ParamUtil.getString(request, "orderByType");

 if (Validator.isNotNull(orderByCol) && Validator.isNotNull(orderByType)) {
 	portalPrefs.setValue("NAME_SPACE", "order-by-col", orderByCol);
 	portalPrefs.setValue("NAME_SPACE", "order-by-type", orderByType);

 } else {
 	orderByCol = portalPrefs.getValue("NAME_SPACE", "order-by-col", "orderNo");
 	orderByType = portalPrefs.getValue("NAME_SPACE", "order-by-type", "asc");
 }
if(Validator.isNull(orderByCol)){
	orderByCol="orderNo";
}
if(Validator.isNull(orderByType)){
	orderByType="asc";
}
String sortingOrder = orderByType;
 %>
<div class="container-fluid-1280">

	<aui:button-row cssClass="admin-buttons">
		<portlet:renderURL var="addEntryURL">
			<portlet:param name="mvcPath" value="/edit.jsp"/>
			<portlet:param name="redirect" value="<%= "currentURL" %>"/>
		</portlet:renderURL>

		<aui:button onClick="<%= addEntryURL.toString() %>" value="Add Entry"/>
	</aui:button-row>

	<liferay-ui:search-container total="<%= HighlightLocalServiceUtil.countAllInGroup(scopeGroupId) %>"  orderByType="<%=orderByType %>" orderByCol="<%=orderByCol %>">
	<liferay-ui:search-container-results>
  <%

    List<Highlight> highlightList =HighlightLocalServiceUtil.findAllInGroup(scopeGroupId,-1, -1);
    List<Highlight> highlightListPerPage = ListUtil.subList(highlightList, searchContainer.getStart(),searchContainer.getEnd());
    int totalRecords =  HighlightLocalServiceUtil.countAllInGroup(scopeGroupId);
    List<Highlight> sortableHighlightList = new ArrayList<Highlight>(highlightListPerPage);
    if(Validator.isNotNull(orderByCol)){
        //Pass the column name to BeanComparator to get comparator object
        BeanComparator comparator = new BeanComparator(orderByCol);
        //It will sort in ascending order
       Collections.sort(sortableHighlightList, comparator);
        if(sortingOrder.equalsIgnoreCase("asc")){


        }else{
            //It will sort in descending order
            Collections.reverse(sortableHighlightList);
        }

    }
    pageContext.setAttribute("beginIndex",searchContainer.getStart());
	pageContext.setAttribute("endIndex",searchContainer.getEnd());
    pageContext.setAttribute("results", sortableHighlightList);
    pageContext.setAttribute("total", totalRecords);

    %>
    </liferay-ui:search-container-results>


		<liferay-ui:search-container-row
				className="highlights.model.Highlight" modelVar="highlight">

			<%
			// check categoryId and get category name
			long categoryId = highlight.getCategoryId();
			String categoryName = "";
			if (categoryId != 0) {
				Category category = CategoryLocalServiceUtil.fetchCategory(categoryId);
				if(category != null) {
					categoryName = category.getName(locale);
				}
			}
			// check tagId and get tag name
			long tagId = highlight.getTagId();
			String tagName = "";
			if (tagId != 0) {
				Tag tag = TagLocalServiceUtil.fetchTag(tagId);
				if(tag != null) {
					tagName = tag.getName(locale);
				}
			}
			%>
			<liferay-ui:search-container-column-text name="Name" value="<%= HtmlUtil.escape(highlight.getName(locale)) %>"/>
			<liferay-ui:search-container-column-text name="Profession" value="<%= HtmlUtil.escape(highlight.getProfession(locale)) %>"/>
			<liferay-ui:search-container-column-text name="Location" value="<%= HtmlUtil.escape(highlight.getLocation(locale)) %>"/>
			<liferay-ui:search-container-column-text name="Category" value="<%= HtmlUtil.escape(categoryName) %>"/>
			<liferay-ui:search-container-column-text name="Tag" value="<%= HtmlUtil.escape(tagName) %>"/>
			<liferay-ui:search-container-column-text name="Order Number" value="<%= String.valueOf(highlight.getOrderNo()) %>"  orderable="true" orderableProperty="orderNo"/>
			<liferay-ui:search-container-column-jsp
					align="right"
					path="/actions.jsp"/>

		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator/>
	</liferay-ui:search-container>
</div>