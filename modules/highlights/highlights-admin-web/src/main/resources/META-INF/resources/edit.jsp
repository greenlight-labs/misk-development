<%@include file = "init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    Highlight entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = HighlightLocalServiceUtil.getHighlight(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }

    List<Category> categories = CategoryLocalServiceUtil.findAllInGroup(scopeGroupId);
    List<Tag> tags = TagLocalServiceUtil.findAllInGroup(scopeGroupId);
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editEntryURL %>" name="fm">

    <aui:model-context bean="<%= entry %>" model="<%= Highlight.class %>" />

    <aui:input type="hidden" name="resourcePrimKey"
               value='<%= entry == null ? "" : entry.getHighlightId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:select required="true" label="Tag" name="tagId">
                <% for (Tag curTag : tags) { %>
                <aui:option label="<%= curTag.getName() %>" value="<%= curTag.getTagId() %>" />
                <% } %>
            </aui:select>
            <aui:select required="true" label="Category" name="categoryId">
                <% for (Category curCategory : categories) { %>
                <aui:option label="<%= curCategory.getName(locale) %>" value="<%= curCategory.getCategoryId() %>" />
                <% } %>
            </aui:select>
            <aui:input name="name" label="Name" autoSize="true" helpMessage="Max 30 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="profession" label="Profession" autoSize="true" helpMessage="Max 30 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="location" label="Location" autoSize="true" helpMessage="Max 30 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
             <aui:input name="orderNo" label="Order Number" helpMessage="Order Number default value 9999" placeholder="9999">                
            </aui:input>
            <aui:input name="image" label="Image" helpMessage="Image Dimensions: W x H pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="image">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="description" label="Description" helpMessage="Max 160 Characters (Recommended)" />
            <aui:input name="audio" label="Audio" helpMessage="Max 200 Characters (Recommended)" />
            <div class="button-holder">
                <button class="btn select-audio-button btn-secondary" type="button" data-field-id="audio">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="video" label="Video" helpMessage="Max 200 Characters (Recommended)" />
            <div class="button-holder">
                <button class="btn select-video-button btn-secondary" type="button" data-field-id="video">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="facebook" label="Instagram" helpMessage="Max 75 Characters (Recommended)" />
            <aui:input name="twitter" label="Twitter" helpMessage="Max 75 Characters (Recommended)" />
            <aui:input name="linkedin" label="Linkedin" helpMessage="Max 75 Characters (Recommended)" />
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:button-row>
        <%
            if (entry == null) {
        %>
        <%--send push notification checkbox--%>
        <aui:input name="sendPushNotification" type="checkbox" label="Send Push Notification" />
        <%
            }
        %>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }

    const audioButton = window.document.querySelector( '.select-audio-button' );
    audioButton.addEventListener('click', function (event) {
        event.preventDefault();
        var element = event.currentTarget;
        var fieldId = element.dataset.fieldId;
        var itemSelectorDialog = new A.LiferayItemSelectorDialog
        (
            {
                eventName: 'selectAudioDocumentLibrary',
                on: {
                    selectedItemChange: function(event) {
                        var selectedItem = event.newVal;
                        if(selectedItem)
                        {
                            var itemValue = selectedItem.value;
                            itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                            window['<portlet:namespace />'+fieldId].value=itemValue;
                            var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                            var editLanguage = null;
                            if(currentEditLanguage === 'en-US'){
                                editLanguage = 'en_US';
                            }else if(currentEditLanguage === 'ar-SA'){
                                editLanguage = 'ar_SA';
                           }
                            if(editLanguage){
                                window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                            }
                        }
                    }
                },
                title: '<liferay-ui:message key="Select Audio File (MP3)" />',
                url: '${audioItemSelectorURL }'
            }
        );
        itemSelectorDialog.open();

    });

    const videoButton = window.document.querySelector( '.select-video-button' );
    videoButton.addEventListener('click', function (event) {
        event.preventDefault();
        var element = event.currentTarget;
        var fieldId = element.dataset.fieldId;
        var itemSelectorDialog = new A.LiferayItemSelectorDialog
        (
            {
                eventName: 'selectVideoDocumentLibrary',
                on: {
                    selectedItemChange: function(event) {
                        var selectedItem = event.newVal;
                        if(selectedItem)
                        {
                            var itemValue = selectedItem.value;
                            itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                            window['<portlet:namespace />'+fieldId].value=itemValue;
                            var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                            var editLanguage = null;
                            if(currentEditLanguage === 'en-US'){
                                editLanguage = 'en_US';
                            }else if(currentEditLanguage === 'ar-SA'){
                                editLanguage = 'ar_SA';
                           }
                            if(editLanguage){
                                window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                            }
                        }
                    }
                },
                title: '<liferay-ui:message key="Select Video File (MP4)" />',
                url: '${videoItemSelectorURL }'
            }
        );
        itemSelectorDialog.open();

    });
</aui:script>