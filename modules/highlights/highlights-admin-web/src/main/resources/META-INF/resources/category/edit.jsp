<%@include file = "../init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    Category category = null;

    if (resourcePrimKey > 0) {
        try {
            category = CategoryLocalServiceUtil.getCategory(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= category == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

    <aui:form action="<%= editEntryURL %>" name="fm">

        <aui:model-context bean="<%= category %>" model="<%= Category.class %>" />

        <aui:input type="hidden" name="resourcePrimKey"
                   value='<%= category == null ? "" : category.getCategoryId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:input name="name" label="Name" autoSize="true" helpMessage="Max 25 Characters (Recommended)">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="color" label="Color" helpMessage="Example: #FFFFFF">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="status" label="Status" />
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
