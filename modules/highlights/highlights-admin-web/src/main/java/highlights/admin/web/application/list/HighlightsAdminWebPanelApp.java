package highlights.admin.web.application.list;

import highlights.admin.web.constants.HighlightsAdminWebPanelCategoryKeys;
import highlights.admin.web.constants.HighlightsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + HighlightsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class HighlightsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return HighlightsAdminWebPortletKeys.HIGHLIGHTSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + HighlightsAdminWebPortletKeys.HIGHLIGHTSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}