package highlights.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import highlights.admin.web.constants.HighlightsAdminWebPanelTagKeys;
import highlights.admin.web.constants.HighlightsAdminWebPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=102",
		"panel.category.key=" + HighlightsAdminWebPanelTagKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class TagsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return HighlightsAdminWebPortletKeys.TAGSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + HighlightsAdminWebPortletKeys.TAGSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}