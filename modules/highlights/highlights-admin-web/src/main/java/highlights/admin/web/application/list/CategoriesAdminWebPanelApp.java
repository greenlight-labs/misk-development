package highlights.admin.web.application.list;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;
import highlights.admin.web.constants.HighlightsAdminWebPanelCategoryKeys;
import highlights.admin.web.constants.HighlightsAdminWebPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;


/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=101",
		"panel.category.key=" + HighlightsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class CategoriesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return HighlightsAdminWebPortletKeys.CATEGORIESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + HighlightsAdminWebPortletKeys.CATEGORIESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}