package highlights.admin.web.constants;

/**
 * @author tz
 */
public class HighlightsAdminWebPortletKeys {

	public static final String HIGHLIGHTSADMINWEB =
		"highlights_admin_web_HighlightsAdminWebPortlet";

	public static final String CATEGORIESADMINWEB =
			"highlights_admin_web_CategoriesAdminWebPortlet";

	public static final String TAGSADMINWEB =
			"highlights_admin_web_TagsAdminWebPortlet";

}