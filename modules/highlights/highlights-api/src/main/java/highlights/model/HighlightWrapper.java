/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Highlight}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Highlight
 * @generated
 */
public class HighlightWrapper
	extends BaseModelWrapper<Highlight>
	implements Highlight, ModelWrapper<Highlight> {

	public HighlightWrapper(Highlight highlight) {
		super(highlight);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("highlightId", getHighlightId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("tagId", getTagId());
		attributes.put("categoryId", getCategoryId());
		attributes.put("orderNo", getOrderNo());
		attributes.put("name", getName());
		attributes.put("profession", getProfession());
		attributes.put("location", getLocation());
		attributes.put("image", getImage());
		attributes.put("description", getDescription());
		attributes.put("audio", getAudio());
		attributes.put("video", getVideo());
		attributes.put("facebook", getFacebook());
		attributes.put("twitter", getTwitter());
		attributes.put("linkedin", getLinkedin());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long highlightId = (Long)attributes.get("highlightId");

		if (highlightId != null) {
			setHighlightId(highlightId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long tagId = (Long)attributes.get("tagId");

		if (tagId != null) {
			setTagId(tagId);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		Long orderNo = (Long)attributes.get("orderNo");

		if (orderNo != null) {
			setOrderNo(orderNo);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String profession = (String)attributes.get("profession");

		if (profession != null) {
			setProfession(profession);
		}

		String location = (String)attributes.get("location");

		if (location != null) {
			setLocation(location);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String audio = (String)attributes.get("audio");

		if (audio != null) {
			setAudio(audio);
		}

		String video = (String)attributes.get("video");

		if (video != null) {
			setVideo(video);
		}

		String facebook = (String)attributes.get("facebook");

		if (facebook != null) {
			setFacebook(facebook);
		}

		String twitter = (String)attributes.get("twitter");

		if (twitter != null) {
			setTwitter(twitter);
		}

		String linkedin = (String)attributes.get("linkedin");

		if (linkedin != null) {
			setLinkedin(linkedin);
		}
	}

	/**
	 * Returns the audio of this highlight.
	 *
	 * @return the audio of this highlight
	 */
	@Override
	public String getAudio() {
		return model.getAudio();
	}

	/**
	 * Returns the localized audio of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized audio of this highlight
	 */
	@Override
	public String getAudio(java.util.Locale locale) {
		return model.getAudio(locale);
	}

	/**
	 * Returns the localized audio of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getAudio(java.util.Locale locale, boolean useDefault) {
		return model.getAudio(locale, useDefault);
	}

	/**
	 * Returns the localized audio of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized audio of this highlight
	 */
	@Override
	public String getAudio(String languageId) {
		return model.getAudio(languageId);
	}

	/**
	 * Returns the localized audio of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized audio of this highlight
	 */
	@Override
	public String getAudio(String languageId, boolean useDefault) {
		return model.getAudio(languageId, useDefault);
	}

	@Override
	public String getAudioCurrentLanguageId() {
		return model.getAudioCurrentLanguageId();
	}

	@Override
	public String getAudioCurrentValue() {
		return model.getAudioCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized audios of this highlight.
	 *
	 * @return the locales and localized audios of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getAudioMap() {
		return model.getAudioMap();
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the category ID of this highlight.
	 *
	 * @return the category ID of this highlight
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this highlight.
	 *
	 * @return the company ID of this highlight
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this highlight.
	 *
	 * @return the create date of this highlight
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the description of this highlight.
	 *
	 * @return the description of this highlight
	 */
	@Override
	public String getDescription() {
		return model.getDescription();
	}

	/**
	 * Returns the localized description of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this highlight
	 */
	@Override
	public String getDescription(java.util.Locale locale) {
		return model.getDescription(locale);
	}

	/**
	 * Returns the localized description of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDescription(java.util.Locale locale, boolean useDefault) {
		return model.getDescription(locale, useDefault);
	}

	/**
	 * Returns the localized description of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this highlight
	 */
	@Override
	public String getDescription(String languageId) {
		return model.getDescription(languageId);
	}

	/**
	 * Returns the localized description of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this highlight
	 */
	@Override
	public String getDescription(String languageId, boolean useDefault) {
		return model.getDescription(languageId, useDefault);
	}

	@Override
	public String getDescriptionCurrentLanguageId() {
		return model.getDescriptionCurrentLanguageId();
	}

	@Override
	public String getDescriptionCurrentValue() {
		return model.getDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized descriptions of this highlight.
	 *
	 * @return the locales and localized descriptions of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getDescriptionMap() {
		return model.getDescriptionMap();
	}

	/**
	 * Returns the facebook of this highlight.
	 *
	 * @return the facebook of this highlight
	 */
	@Override
	public String getFacebook() {
		return model.getFacebook();
	}

	/**
	 * Returns the localized facebook of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized facebook of this highlight
	 */
	@Override
	public String getFacebook(java.util.Locale locale) {
		return model.getFacebook(locale);
	}

	/**
	 * Returns the localized facebook of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized facebook of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getFacebook(java.util.Locale locale, boolean useDefault) {
		return model.getFacebook(locale, useDefault);
	}

	/**
	 * Returns the localized facebook of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized facebook of this highlight
	 */
	@Override
	public String getFacebook(String languageId) {
		return model.getFacebook(languageId);
	}

	/**
	 * Returns the localized facebook of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized facebook of this highlight
	 */
	@Override
	public String getFacebook(String languageId, boolean useDefault) {
		return model.getFacebook(languageId, useDefault);
	}

	@Override
	public String getFacebookCurrentLanguageId() {
		return model.getFacebookCurrentLanguageId();
	}

	@Override
	public String getFacebookCurrentValue() {
		return model.getFacebookCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized facebooks of this highlight.
	 *
	 * @return the locales and localized facebooks of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getFacebookMap() {
		return model.getFacebookMap();
	}

	/**
	 * Returns the group ID of this highlight.
	 *
	 * @return the group ID of this highlight
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the highlight ID of this highlight.
	 *
	 * @return the highlight ID of this highlight
	 */
	@Override
	public long getHighlightId() {
		return model.getHighlightId();
	}

	/**
	 * Returns the image of this highlight.
	 *
	 * @return the image of this highlight
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the localized image of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this highlight
	 */
	@Override
	public String getImage(java.util.Locale locale) {
		return model.getImage(locale);
	}

	/**
	 * Returns the localized image of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getImage(java.util.Locale locale, boolean useDefault) {
		return model.getImage(locale, useDefault);
	}

	/**
	 * Returns the localized image of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this highlight
	 */
	@Override
	public String getImage(String languageId) {
		return model.getImage(languageId);
	}

	/**
	 * Returns the localized image of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this highlight
	 */
	@Override
	public String getImage(String languageId, boolean useDefault) {
		return model.getImage(languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return model.getImageCurrentLanguageId();
	}

	@Override
	public String getImageCurrentValue() {
		return model.getImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized images of this highlight.
	 *
	 * @return the locales and localized images of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getImageMap() {
		return model.getImageMap();
	}

	/**
	 * Returns the linkedin of this highlight.
	 *
	 * @return the linkedin of this highlight
	 */
	@Override
	public String getLinkedin() {
		return model.getLinkedin();
	}

	/**
	 * Returns the localized linkedin of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized linkedin of this highlight
	 */
	@Override
	public String getLinkedin(java.util.Locale locale) {
		return model.getLinkedin(locale);
	}

	/**
	 * Returns the localized linkedin of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized linkedin of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLinkedin(java.util.Locale locale, boolean useDefault) {
		return model.getLinkedin(locale, useDefault);
	}

	/**
	 * Returns the localized linkedin of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized linkedin of this highlight
	 */
	@Override
	public String getLinkedin(String languageId) {
		return model.getLinkedin(languageId);
	}

	/**
	 * Returns the localized linkedin of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized linkedin of this highlight
	 */
	@Override
	public String getLinkedin(String languageId, boolean useDefault) {
		return model.getLinkedin(languageId, useDefault);
	}

	@Override
	public String getLinkedinCurrentLanguageId() {
		return model.getLinkedinCurrentLanguageId();
	}

	@Override
	public String getLinkedinCurrentValue() {
		return model.getLinkedinCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized linkedins of this highlight.
	 *
	 * @return the locales and localized linkedins of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getLinkedinMap() {
		return model.getLinkedinMap();
	}

	/**
	 * Returns the location of this highlight.
	 *
	 * @return the location of this highlight
	 */
	@Override
	public String getLocation() {
		return model.getLocation();
	}

	/**
	 * Returns the localized location of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized location of this highlight
	 */
	@Override
	public String getLocation(java.util.Locale locale) {
		return model.getLocation(locale);
	}

	/**
	 * Returns the localized location of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getLocation(java.util.Locale locale, boolean useDefault) {
		return model.getLocation(locale, useDefault);
	}

	/**
	 * Returns the localized location of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized location of this highlight
	 */
	@Override
	public String getLocation(String languageId) {
		return model.getLocation(languageId);
	}

	/**
	 * Returns the localized location of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized location of this highlight
	 */
	@Override
	public String getLocation(String languageId, boolean useDefault) {
		return model.getLocation(languageId, useDefault);
	}

	@Override
	public String getLocationCurrentLanguageId() {
		return model.getLocationCurrentLanguageId();
	}

	@Override
	public String getLocationCurrentValue() {
		return model.getLocationCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized locations of this highlight.
	 *
	 * @return the locales and localized locations of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getLocationMap() {
		return model.getLocationMap();
	}

	/**
	 * Returns the modified date of this highlight.
	 *
	 * @return the modified date of this highlight
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the name of this highlight.
	 *
	 * @return the name of this highlight
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	/**
	 * Returns the localized name of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized name of this highlight
	 */
	@Override
	public String getName(java.util.Locale locale) {
		return model.getName(locale);
	}

	/**
	 * Returns the localized name of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getName(java.util.Locale locale, boolean useDefault) {
		return model.getName(locale, useDefault);
	}

	/**
	 * Returns the localized name of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized name of this highlight
	 */
	@Override
	public String getName(String languageId) {
		return model.getName(languageId);
	}

	/**
	 * Returns the localized name of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this highlight
	 */
	@Override
	public String getName(String languageId, boolean useDefault) {
		return model.getName(languageId, useDefault);
	}

	@Override
	public String getNameCurrentLanguageId() {
		return model.getNameCurrentLanguageId();
	}

	@Override
	public String getNameCurrentValue() {
		return model.getNameCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized names of this highlight.
	 *
	 * @return the locales and localized names of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getNameMap() {
		return model.getNameMap();
	}

	/**
	 * Returns the order no of this highlight.
	 *
	 * @return the order no of this highlight
	 */
	@Override
	public long getOrderNo() {
		return model.getOrderNo();
	}

	/**
	 * Returns the primary key of this highlight.
	 *
	 * @return the primary key of this highlight
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the profession of this highlight.
	 *
	 * @return the profession of this highlight
	 */
	@Override
	public String getProfession() {
		return model.getProfession();
	}

	/**
	 * Returns the localized profession of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized profession of this highlight
	 */
	@Override
	public String getProfession(java.util.Locale locale) {
		return model.getProfession(locale);
	}

	/**
	 * Returns the localized profession of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized profession of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getProfession(java.util.Locale locale, boolean useDefault) {
		return model.getProfession(locale, useDefault);
	}

	/**
	 * Returns the localized profession of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized profession of this highlight
	 */
	@Override
	public String getProfession(String languageId) {
		return model.getProfession(languageId);
	}

	/**
	 * Returns the localized profession of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized profession of this highlight
	 */
	@Override
	public String getProfession(String languageId, boolean useDefault) {
		return model.getProfession(languageId, useDefault);
	}

	@Override
	public String getProfessionCurrentLanguageId() {
		return model.getProfessionCurrentLanguageId();
	}

	@Override
	public String getProfessionCurrentValue() {
		return model.getProfessionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized professions of this highlight.
	 *
	 * @return the locales and localized professions of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getProfessionMap() {
		return model.getProfessionMap();
	}

	/**
	 * Returns the tag ID of this highlight.
	 *
	 * @return the tag ID of this highlight
	 */
	@Override
	public long getTagId() {
		return model.getTagId();
	}

	/**
	 * Returns the twitter of this highlight.
	 *
	 * @return the twitter of this highlight
	 */
	@Override
	public String getTwitter() {
		return model.getTwitter();
	}

	/**
	 * Returns the localized twitter of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized twitter of this highlight
	 */
	@Override
	public String getTwitter(java.util.Locale locale) {
		return model.getTwitter(locale);
	}

	/**
	 * Returns the localized twitter of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized twitter of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTwitter(java.util.Locale locale, boolean useDefault) {
		return model.getTwitter(locale, useDefault);
	}

	/**
	 * Returns the localized twitter of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized twitter of this highlight
	 */
	@Override
	public String getTwitter(String languageId) {
		return model.getTwitter(languageId);
	}

	/**
	 * Returns the localized twitter of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized twitter of this highlight
	 */
	@Override
	public String getTwitter(String languageId, boolean useDefault) {
		return model.getTwitter(languageId, useDefault);
	}

	@Override
	public String getTwitterCurrentLanguageId() {
		return model.getTwitterCurrentLanguageId();
	}

	@Override
	public String getTwitterCurrentValue() {
		return model.getTwitterCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized twitters of this highlight.
	 *
	 * @return the locales and localized twitters of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getTwitterMap() {
		return model.getTwitterMap();
	}

	/**
	 * Returns the user ID of this highlight.
	 *
	 * @return the user ID of this highlight
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this highlight.
	 *
	 * @return the user name of this highlight
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this highlight.
	 *
	 * @return the user uuid of this highlight
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this highlight.
	 *
	 * @return the uuid of this highlight
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the video of this highlight.
	 *
	 * @return the video of this highlight
	 */
	@Override
	public String getVideo() {
		return model.getVideo();
	}

	/**
	 * Returns the localized video of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized video of this highlight
	 */
	@Override
	public String getVideo(java.util.Locale locale) {
		return model.getVideo(locale);
	}

	/**
	 * Returns the localized video of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized video of this highlight. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getVideo(java.util.Locale locale, boolean useDefault) {
		return model.getVideo(locale, useDefault);
	}

	/**
	 * Returns the localized video of this highlight in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized video of this highlight
	 */
	@Override
	public String getVideo(String languageId) {
		return model.getVideo(languageId);
	}

	/**
	 * Returns the localized video of this highlight in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized video of this highlight
	 */
	@Override
	public String getVideo(String languageId, boolean useDefault) {
		return model.getVideo(languageId, useDefault);
	}

	@Override
	public String getVideoCurrentLanguageId() {
		return model.getVideoCurrentLanguageId();
	}

	@Override
	public String getVideoCurrentValue() {
		return model.getVideoCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized videos of this highlight.
	 *
	 * @return the locales and localized videos of this highlight
	 */
	@Override
	public Map<java.util.Locale, String> getVideoMap() {
		return model.getVideoMap();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the audio of this highlight.
	 *
	 * @param audio the audio of this highlight
	 */
	@Override
	public void setAudio(String audio) {
		model.setAudio(audio);
	}

	/**
	 * Sets the localized audio of this highlight in the language.
	 *
	 * @param audio the localized audio of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setAudio(String audio, java.util.Locale locale) {
		model.setAudio(audio, locale);
	}

	/**
	 * Sets the localized audio of this highlight in the language, and sets the default locale.
	 *
	 * @param audio the localized audio of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudio(
		String audio, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setAudio(audio, locale, defaultLocale);
	}

	@Override
	public void setAudioCurrentLanguageId(String languageId) {
		model.setAudioCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized audios of this highlight from the map of locales and localized audios.
	 *
	 * @param audioMap the locales and localized audios of this highlight
	 */
	@Override
	public void setAudioMap(Map<java.util.Locale, String> audioMap) {
		model.setAudioMap(audioMap);
	}

	/**
	 * Sets the localized audios of this highlight from the map of locales and localized audios, and sets the default locale.
	 *
	 * @param audioMap the locales and localized audios of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setAudioMap(
		Map<java.util.Locale, String> audioMap,
		java.util.Locale defaultLocale) {

		model.setAudioMap(audioMap, defaultLocale);
	}

	/**
	 * Sets the category ID of this highlight.
	 *
	 * @param categoryId the category ID of this highlight
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this highlight.
	 *
	 * @param companyId the company ID of this highlight
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this highlight.
	 *
	 * @param createDate the create date of this highlight
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the description of this highlight.
	 *
	 * @param description the description of this highlight
	 */
	@Override
	public void setDescription(String description) {
		model.setDescription(description);
	}

	/**
	 * Sets the localized description of this highlight in the language.
	 *
	 * @param description the localized description of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setDescription(String description, java.util.Locale locale) {
		model.setDescription(description, locale);
	}

	/**
	 * Sets the localized description of this highlight in the language, and sets the default locale.
	 *
	 * @param description the localized description of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescription(
		String description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDescription(description, locale, defaultLocale);
	}

	@Override
	public void setDescriptionCurrentLanguageId(String languageId) {
		model.setDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized descriptions of this highlight from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this highlight
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap) {

		model.setDescriptionMap(descriptionMap);
	}

	/**
	 * Sets the localized descriptions of this highlight from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDescriptionMap(
		Map<java.util.Locale, String> descriptionMap,
		java.util.Locale defaultLocale) {

		model.setDescriptionMap(descriptionMap, defaultLocale);
	}

	/**
	 * Sets the facebook of this highlight.
	 *
	 * @param facebook the facebook of this highlight
	 */
	@Override
	public void setFacebook(String facebook) {
		model.setFacebook(facebook);
	}

	/**
	 * Sets the localized facebook of this highlight in the language.
	 *
	 * @param facebook the localized facebook of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setFacebook(String facebook, java.util.Locale locale) {
		model.setFacebook(facebook, locale);
	}

	/**
	 * Sets the localized facebook of this highlight in the language, and sets the default locale.
	 *
	 * @param facebook the localized facebook of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setFacebook(
		String facebook, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setFacebook(facebook, locale, defaultLocale);
	}

	@Override
	public void setFacebookCurrentLanguageId(String languageId) {
		model.setFacebookCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized facebooks of this highlight from the map of locales and localized facebooks.
	 *
	 * @param facebookMap the locales and localized facebooks of this highlight
	 */
	@Override
	public void setFacebookMap(Map<java.util.Locale, String> facebookMap) {
		model.setFacebookMap(facebookMap);
	}

	/**
	 * Sets the localized facebooks of this highlight from the map of locales and localized facebooks, and sets the default locale.
	 *
	 * @param facebookMap the locales and localized facebooks of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setFacebookMap(
		Map<java.util.Locale, String> facebookMap,
		java.util.Locale defaultLocale) {

		model.setFacebookMap(facebookMap, defaultLocale);
	}

	/**
	 * Sets the group ID of this highlight.
	 *
	 * @param groupId the group ID of this highlight
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the highlight ID of this highlight.
	 *
	 * @param highlightId the highlight ID of this highlight
	 */
	@Override
	public void setHighlightId(long highlightId) {
		model.setHighlightId(highlightId);
	}

	/**
	 * Sets the image of this highlight.
	 *
	 * @param image the image of this highlight
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the localized image of this highlight in the language.
	 *
	 * @param image the localized image of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setImage(String image, java.util.Locale locale) {
		model.setImage(image, locale);
	}

	/**
	 * Sets the localized image of this highlight in the language, and sets the default locale.
	 *
	 * @param image the localized image of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImage(
		String image, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setImage(image, locale, defaultLocale);
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		model.setImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized images of this highlight from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this highlight
	 */
	@Override
	public void setImageMap(Map<java.util.Locale, String> imageMap) {
		model.setImageMap(imageMap);
	}

	/**
	 * Sets the localized images of this highlight from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImageMap(
		Map<java.util.Locale, String> imageMap,
		java.util.Locale defaultLocale) {

		model.setImageMap(imageMap, defaultLocale);
	}

	/**
	 * Sets the linkedin of this highlight.
	 *
	 * @param linkedin the linkedin of this highlight
	 */
	@Override
	public void setLinkedin(String linkedin) {
		model.setLinkedin(linkedin);
	}

	/**
	 * Sets the localized linkedin of this highlight in the language.
	 *
	 * @param linkedin the localized linkedin of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setLinkedin(String linkedin, java.util.Locale locale) {
		model.setLinkedin(linkedin, locale);
	}

	/**
	 * Sets the localized linkedin of this highlight in the language, and sets the default locale.
	 *
	 * @param linkedin the localized linkedin of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLinkedin(
		String linkedin, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLinkedin(linkedin, locale, defaultLocale);
	}

	@Override
	public void setLinkedinCurrentLanguageId(String languageId) {
		model.setLinkedinCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized linkedins of this highlight from the map of locales and localized linkedins.
	 *
	 * @param linkedinMap the locales and localized linkedins of this highlight
	 */
	@Override
	public void setLinkedinMap(Map<java.util.Locale, String> linkedinMap) {
		model.setLinkedinMap(linkedinMap);
	}

	/**
	 * Sets the localized linkedins of this highlight from the map of locales and localized linkedins, and sets the default locale.
	 *
	 * @param linkedinMap the locales and localized linkedins of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLinkedinMap(
		Map<java.util.Locale, String> linkedinMap,
		java.util.Locale defaultLocale) {

		model.setLinkedinMap(linkedinMap, defaultLocale);
	}

	/**
	 * Sets the location of this highlight.
	 *
	 * @param location the location of this highlight
	 */
	@Override
	public void setLocation(String location) {
		model.setLocation(location);
	}

	/**
	 * Sets the localized location of this highlight in the language.
	 *
	 * @param location the localized location of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setLocation(String location, java.util.Locale locale) {
		model.setLocation(location, locale);
	}

	/**
	 * Sets the localized location of this highlight in the language, and sets the default locale.
	 *
	 * @param location the localized location of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocation(
		String location, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setLocation(location, locale, defaultLocale);
	}

	@Override
	public void setLocationCurrentLanguageId(String languageId) {
		model.setLocationCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized locations of this highlight from the map of locales and localized locations.
	 *
	 * @param locationMap the locales and localized locations of this highlight
	 */
	@Override
	public void setLocationMap(Map<java.util.Locale, String> locationMap) {
		model.setLocationMap(locationMap);
	}

	/**
	 * Sets the localized locations of this highlight from the map of locales and localized locations, and sets the default locale.
	 *
	 * @param locationMap the locales and localized locations of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setLocationMap(
		Map<java.util.Locale, String> locationMap,
		java.util.Locale defaultLocale) {

		model.setLocationMap(locationMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this highlight.
	 *
	 * @param modifiedDate the modified date of this highlight
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the name of this highlight.
	 *
	 * @param name the name of this highlight
	 */
	@Override
	public void setName(String name) {
		model.setName(name);
	}

	/**
	 * Sets the localized name of this highlight in the language.
	 *
	 * @param name the localized name of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setName(String name, java.util.Locale locale) {
		model.setName(name, locale);
	}

	/**
	 * Sets the localized name of this highlight in the language, and sets the default locale.
	 *
	 * @param name the localized name of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setName(
		String name, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setName(name, locale, defaultLocale);
	}

	@Override
	public void setNameCurrentLanguageId(String languageId) {
		model.setNameCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized names of this highlight from the map of locales and localized names.
	 *
	 * @param nameMap the locales and localized names of this highlight
	 */
	@Override
	public void setNameMap(Map<java.util.Locale, String> nameMap) {
		model.setNameMap(nameMap);
	}

	/**
	 * Sets the localized names of this highlight from the map of locales and localized names, and sets the default locale.
	 *
	 * @param nameMap the locales and localized names of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setNameMap(
		Map<java.util.Locale, String> nameMap, java.util.Locale defaultLocale) {

		model.setNameMap(nameMap, defaultLocale);
	}

	/**
	 * Sets the order no of this highlight.
	 *
	 * @param orderNo the order no of this highlight
	 */
	@Override
	public void setOrderNo(long orderNo) {
		model.setOrderNo(orderNo);
	}

	/**
	 * Sets the primary key of this highlight.
	 *
	 * @param primaryKey the primary key of this highlight
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the profession of this highlight.
	 *
	 * @param profession the profession of this highlight
	 */
	@Override
	public void setProfession(String profession) {
		model.setProfession(profession);
	}

	/**
	 * Sets the localized profession of this highlight in the language.
	 *
	 * @param profession the localized profession of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setProfession(String profession, java.util.Locale locale) {
		model.setProfession(profession, locale);
	}

	/**
	 * Sets the localized profession of this highlight in the language, and sets the default locale.
	 *
	 * @param profession the localized profession of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setProfession(
		String profession, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setProfession(profession, locale, defaultLocale);
	}

	@Override
	public void setProfessionCurrentLanguageId(String languageId) {
		model.setProfessionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized professions of this highlight from the map of locales and localized professions.
	 *
	 * @param professionMap the locales and localized professions of this highlight
	 */
	@Override
	public void setProfessionMap(Map<java.util.Locale, String> professionMap) {
		model.setProfessionMap(professionMap);
	}

	/**
	 * Sets the localized professions of this highlight from the map of locales and localized professions, and sets the default locale.
	 *
	 * @param professionMap the locales and localized professions of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setProfessionMap(
		Map<java.util.Locale, String> professionMap,
		java.util.Locale defaultLocale) {

		model.setProfessionMap(professionMap, defaultLocale);
	}

	/**
	 * Sets the tag ID of this highlight.
	 *
	 * @param tagId the tag ID of this highlight
	 */
	@Override
	public void setTagId(long tagId) {
		model.setTagId(tagId);
	}

	/**
	 * Sets the twitter of this highlight.
	 *
	 * @param twitter the twitter of this highlight
	 */
	@Override
	public void setTwitter(String twitter) {
		model.setTwitter(twitter);
	}

	/**
	 * Sets the localized twitter of this highlight in the language.
	 *
	 * @param twitter the localized twitter of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setTwitter(String twitter, java.util.Locale locale) {
		model.setTwitter(twitter, locale);
	}

	/**
	 * Sets the localized twitter of this highlight in the language, and sets the default locale.
	 *
	 * @param twitter the localized twitter of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTwitter(
		String twitter, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setTwitter(twitter, locale, defaultLocale);
	}

	@Override
	public void setTwitterCurrentLanguageId(String languageId) {
		model.setTwitterCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized twitters of this highlight from the map of locales and localized twitters.
	 *
	 * @param twitterMap the locales and localized twitters of this highlight
	 */
	@Override
	public void setTwitterMap(Map<java.util.Locale, String> twitterMap) {
		model.setTwitterMap(twitterMap);
	}

	/**
	 * Sets the localized twitters of this highlight from the map of locales and localized twitters, and sets the default locale.
	 *
	 * @param twitterMap the locales and localized twitters of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTwitterMap(
		Map<java.util.Locale, String> twitterMap,
		java.util.Locale defaultLocale) {

		model.setTwitterMap(twitterMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this highlight.
	 *
	 * @param userId the user ID of this highlight
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this highlight.
	 *
	 * @param userName the user name of this highlight
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this highlight.
	 *
	 * @param userUuid the user uuid of this highlight
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this highlight.
	 *
	 * @param uuid the uuid of this highlight
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the video of this highlight.
	 *
	 * @param video the video of this highlight
	 */
	@Override
	public void setVideo(String video) {
		model.setVideo(video);
	}

	/**
	 * Sets the localized video of this highlight in the language.
	 *
	 * @param video the localized video of this highlight
	 * @param locale the locale of the language
	 */
	@Override
	public void setVideo(String video, java.util.Locale locale) {
		model.setVideo(video, locale);
	}

	/**
	 * Sets the localized video of this highlight in the language, and sets the default locale.
	 *
	 * @param video the localized video of this highlight
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVideo(
		String video, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setVideo(video, locale, defaultLocale);
	}

	@Override
	public void setVideoCurrentLanguageId(String languageId) {
		model.setVideoCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized videos of this highlight from the map of locales and localized videos.
	 *
	 * @param videoMap the locales and localized videos of this highlight
	 */
	@Override
	public void setVideoMap(Map<java.util.Locale, String> videoMap) {
		model.setVideoMap(videoMap);
	}

	/**
	 * Sets the localized videos of this highlight from the map of locales and localized videos, and sets the default locale.
	 *
	 * @param videoMap the locales and localized videos of this highlight
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setVideoMap(
		Map<java.util.Locale, String> videoMap,
		java.util.Locale defaultLocale) {

		model.setVideoMap(videoMap, defaultLocale);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected HighlightWrapper wrap(Highlight highlight) {
		return new HighlightWrapper(highlight);
	}

}