/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.LocaleException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.GroupedModel;
import com.liferay.portal.kernel.model.LocalizedModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.model.StagedAuditedModel;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the Tag service. Represents a row in the &quot;misk_highlights_tags&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>highlights.model.impl.TagModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>highlights.model.impl.TagImpl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Tag
 * @generated
 */
@ProviderType
public interface TagModel
	extends BaseModel<Tag>, GroupedModel, LocalizedModel, ShardedModel,
			StagedAuditedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a tag model instance should use the {@link Tag} interface instead.
	 */

	/**
	 * Returns the primary key of this tag.
	 *
	 * @return the primary key of this tag
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this tag.
	 *
	 * @param primaryKey the primary key of this tag
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this tag.
	 *
	 * @return the uuid of this tag
	 */
	@AutoEscape
	@Override
	public String getUuid();

	/**
	 * Sets the uuid of this tag.
	 *
	 * @param uuid the uuid of this tag
	 */
	@Override
	public void setUuid(String uuid);

	/**
	 * Returns the tag ID of this tag.
	 *
	 * @return the tag ID of this tag
	 */
	public long getTagId();

	/**
	 * Sets the tag ID of this tag.
	 *
	 * @param tagId the tag ID of this tag
	 */
	public void setTagId(long tagId);

	/**
	 * Returns the group ID of this tag.
	 *
	 * @return the group ID of this tag
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this tag.
	 *
	 * @param groupId the group ID of this tag
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the company ID of this tag.
	 *
	 * @return the company ID of this tag
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this tag.
	 *
	 * @param companyId the company ID of this tag
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this tag.
	 *
	 * @return the user ID of this tag
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this tag.
	 *
	 * @param userId the user ID of this tag
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this tag.
	 *
	 * @return the user uuid of this tag
	 */
	@Override
	public String getUserUuid();

	/**
	 * Sets the user uuid of this tag.
	 *
	 * @param userUuid the user uuid of this tag
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this tag.
	 *
	 * @return the user name of this tag
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this tag.
	 *
	 * @param userName the user name of this tag
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this tag.
	 *
	 * @return the create date of this tag
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this tag.
	 *
	 * @param createDate the create date of this tag
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this tag.
	 *
	 * @return the modified date of this tag
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this tag.
	 *
	 * @param modifiedDate the modified date of this tag
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the name of this tag.
	 *
	 * @return the name of this tag
	 */
	public String getName();

	/**
	 * Returns the localized name of this tag in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized name of this tag
	 */
	@AutoEscape
	public String getName(Locale locale);

	/**
	 * Returns the localized name of this tag in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this tag. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getName(Locale locale, boolean useDefault);

	/**
	 * Returns the localized name of this tag in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized name of this tag
	 */
	@AutoEscape
	public String getName(String languageId);

	/**
	 * Returns the localized name of this tag in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized name of this tag
	 */
	@AutoEscape
	public String getName(String languageId, boolean useDefault);

	@AutoEscape
	public String getNameCurrentLanguageId();

	@AutoEscape
	public String getNameCurrentValue();

	/**
	 * Returns a map of the locales and localized names of this tag.
	 *
	 * @return the locales and localized names of this tag
	 */
	public Map<Locale, String> getNameMap();

	/**
	 * Sets the name of this tag.
	 *
	 * @param name the name of this tag
	 */
	public void setName(String name);

	/**
	 * Sets the localized name of this tag in the language.
	 *
	 * @param name the localized name of this tag
	 * @param locale the locale of the language
	 */
	public void setName(String name, Locale locale);

	/**
	 * Sets the localized name of this tag in the language, and sets the default locale.
	 *
	 * @param name the localized name of this tag
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setName(String name, Locale locale, Locale defaultLocale);

	public void setNameCurrentLanguageId(String languageId);

	/**
	 * Sets the localized names of this tag from the map of locales and localized names.
	 *
	 * @param nameMap the locales and localized names of this tag
	 */
	public void setNameMap(Map<Locale, String> nameMap);

	/**
	 * Sets the localized names of this tag from the map of locales and localized names, and sets the default locale.
	 *
	 * @param nameMap the locales and localized names of this tag
	 * @param defaultLocale the default locale
	 */
	public void setNameMap(Map<Locale, String> nameMap, Locale defaultLocale);

	/**
	 * Returns the slug of this tag.
	 *
	 * @return the slug of this tag
	 */
	@AutoEscape
	public String getSlug();

	/**
	 * Sets the slug of this tag.
	 *
	 * @param slug the slug of this tag
	 */
	public void setSlug(String slug);

	/**
	 * Returns the description of this tag.
	 *
	 * @return the description of this tag
	 */
	public String getDescription();

	/**
	 * Returns the localized description of this tag in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized description of this tag
	 */
	@AutoEscape
	public String getDescription(Locale locale);

	/**
	 * Returns the localized description of this tag in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this tag. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getDescription(Locale locale, boolean useDefault);

	/**
	 * Returns the localized description of this tag in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized description of this tag
	 */
	@AutoEscape
	public String getDescription(String languageId);

	/**
	 * Returns the localized description of this tag in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized description of this tag
	 */
	@AutoEscape
	public String getDescription(String languageId, boolean useDefault);

	@AutoEscape
	public String getDescriptionCurrentLanguageId();

	@AutoEscape
	public String getDescriptionCurrentValue();

	/**
	 * Returns a map of the locales and localized descriptions of this tag.
	 *
	 * @return the locales and localized descriptions of this tag
	 */
	public Map<Locale, String> getDescriptionMap();

	/**
	 * Sets the description of this tag.
	 *
	 * @param description the description of this tag
	 */
	public void setDescription(String description);

	/**
	 * Sets the localized description of this tag in the language.
	 *
	 * @param description the localized description of this tag
	 * @param locale the locale of the language
	 */
	public void setDescription(String description, Locale locale);

	/**
	 * Sets the localized description of this tag in the language, and sets the default locale.
	 *
	 * @param description the localized description of this tag
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setDescription(
		String description, Locale locale, Locale defaultLocale);

	public void setDescriptionCurrentLanguageId(String languageId);

	/**
	 * Sets the localized descriptions of this tag from the map of locales and localized descriptions.
	 *
	 * @param descriptionMap the locales and localized descriptions of this tag
	 */
	public void setDescriptionMap(Map<Locale, String> descriptionMap);

	/**
	 * Sets the localized descriptions of this tag from the map of locales and localized descriptions, and sets the default locale.
	 *
	 * @param descriptionMap the locales and localized descriptions of this tag
	 * @param defaultLocale the default locale
	 */
	public void setDescriptionMap(
		Map<Locale, String> descriptionMap, Locale defaultLocale);

	@Override
	public String[] getAvailableLanguageIds();

	@Override
	public String getDefaultLanguageId();

	@Override
	public void prepareLocalizedFieldsForImport() throws LocaleException;

	@Override
	public void prepareLocalizedFieldsForImport(Locale defaultImportLocale)
		throws LocaleException;

}