/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link highlights.service.http.HighlightServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class HighlightSoap implements Serializable {

	public static HighlightSoap toSoapModel(Highlight model) {
		HighlightSoap soapModel = new HighlightSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setHighlightId(model.getHighlightId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setTagId(model.getTagId());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setOrderNo(model.getOrderNo());
		soapModel.setName(model.getName());
		soapModel.setProfession(model.getProfession());
		soapModel.setLocation(model.getLocation());
		soapModel.setImage(model.getImage());
		soapModel.setDescription(model.getDescription());
		soapModel.setAudio(model.getAudio());
		soapModel.setVideo(model.getVideo());
		soapModel.setFacebook(model.getFacebook());
		soapModel.setTwitter(model.getTwitter());
		soapModel.setLinkedin(model.getLinkedin());

		return soapModel;
	}

	public static HighlightSoap[] toSoapModels(Highlight[] models) {
		HighlightSoap[] soapModels = new HighlightSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static HighlightSoap[][] toSoapModels(Highlight[][] models) {
		HighlightSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new HighlightSoap[models.length][models[0].length];
		}
		else {
			soapModels = new HighlightSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static HighlightSoap[] toSoapModels(List<Highlight> models) {
		List<HighlightSoap> soapModels = new ArrayList<HighlightSoap>(
			models.size());

		for (Highlight model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new HighlightSoap[soapModels.size()]);
	}

	public HighlightSoap() {
	}

	public long getPrimaryKey() {
		return _highlightId;
	}

	public void setPrimaryKey(long pk) {
		setHighlightId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getHighlightId() {
		return _highlightId;
	}

	public void setHighlightId(long highlightId) {
		_highlightId = highlightId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getTagId() {
		return _tagId;
	}

	public void setTagId(long tagId) {
		_tagId = tagId;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public long getOrderNo() {
		return _orderNo;
	}

	public void setOrderNo(long orderNo) {
		_orderNo = orderNo;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getProfession() {
		return _profession;
	}

	public void setProfession(String profession) {
		_profession = profession;
	}

	public String getLocation() {
		return _location;
	}

	public void setLocation(String location) {
		_location = location;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getAudio() {
		return _audio;
	}

	public void setAudio(String audio) {
		_audio = audio;
	}

	public String getVideo() {
		return _video;
	}

	public void setVideo(String video) {
		_video = video;
	}

	public String getFacebook() {
		return _facebook;
	}

	public void setFacebook(String facebook) {
		_facebook = facebook;
	}

	public String getTwitter() {
		return _twitter;
	}

	public void setTwitter(String twitter) {
		_twitter = twitter;
	}

	public String getLinkedin() {
		return _linkedin;
	}

	public void setLinkedin(String linkedin) {
		_linkedin = linkedin;
	}

	private String _uuid;
	private long _highlightId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _tagId;
	private long _categoryId;
	private long _orderNo;
	private String _name;
	private String _profession;
	private String _location;
	private String _image;
	private String _description;
	private String _audio;
	private String _video;
	private String _facebook;
	private String _twitter;
	private String _linkedin;

}