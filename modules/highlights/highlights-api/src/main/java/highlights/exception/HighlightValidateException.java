package highlights.exception;

import com.liferay.portal.kernel.exception.PortalException;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

/**
 * @author tz
 */
@ProviderType
public class HighlightValidateException extends PortalException {

	public HighlightValidateException() {
	}

	public HighlightValidateException(List<String> errors) {
		_errors = errors;
	}

	public List<String> getErrors() {
		return _errors;
	}

	protected List<String> _errors = null;

}