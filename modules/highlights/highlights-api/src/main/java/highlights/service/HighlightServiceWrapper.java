/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link HighlightService}.
 *
 * @author Brian Wing Shun Chan
 * @see HighlightService
 * @generated
 */
public class HighlightServiceWrapper
	implements HighlightService, ServiceWrapper<HighlightService> {

	public HighlightServiceWrapper(HighlightService highlightService) {
		_highlightService = highlightService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _highlightService.getOSGiServiceIdentifier();
	}

	@Override
	public HighlightService getWrappedService() {
		return _highlightService;
	}

	@Override
	public void setWrappedService(HighlightService highlightService) {
		_highlightService = highlightService;
	}

	private HighlightService _highlightService;

}