/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link HighlightLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see HighlightLocalService
 * @generated
 */
public class HighlightLocalServiceWrapper
	implements HighlightLocalService, ServiceWrapper<HighlightLocalService> {

	public HighlightLocalServiceWrapper(
		HighlightLocalService highlightLocalService) {

		_highlightLocalService = highlightLocalService;
	}

	@Override
	public highlights.model.Highlight addEntry(
			highlights.model.Highlight orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   highlights.exception.HighlightValidateException {

		return _highlightLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the highlight to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was added
	 */
	@Override
	public highlights.model.Highlight addHighlight(
		highlights.model.Highlight highlight) {

		return _highlightLocalService.addHighlight(highlight);
	}

	@Override
	public int countAllInGroup(long groupId) {
		return _highlightLocalService.countAllInGroup(groupId);
	}

	@Override
	public int countByTagId(long tagId) {
		return _highlightLocalService.countByTagId(tagId);
	}

	/**
	 * Creates a new highlight with the primary key. Does not add the highlight to the database.
	 *
	 * @param highlightId the primary key for the new highlight
	 * @return the new highlight
	 */
	@Override
	public highlights.model.Highlight createHighlight(long highlightId) {
		return _highlightLocalService.createHighlight(highlightId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _highlightLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public highlights.model.Highlight deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _highlightLocalService.deleteEntry(primaryKey);
	}

	/**
	 * Deletes the highlight from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was removed
	 */
	@Override
	public highlights.model.Highlight deleteHighlight(
		highlights.model.Highlight highlight) {

		return _highlightLocalService.deleteHighlight(highlight);
	}

	/**
	 * Deletes the highlight with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight that was removed
	 * @throws PortalException if a highlight with the primary key could not be found
	 */
	@Override
	public highlights.model.Highlight deleteHighlight(long highlightId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _highlightLocalService.deleteHighlight(highlightId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _highlightLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _highlightLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _highlightLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _highlightLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _highlightLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _highlightLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _highlightLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public highlights.model.Highlight fetchHighlight(long highlightId) {
		return _highlightLocalService.fetchHighlight(highlightId);
	}

	/**
	 * Returns the highlight matching the UUID and group.
	 *
	 * @param uuid the highlight's UUID
	 * @param groupId the primary key of the group
	 * @return the matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	@Override
	public highlights.model.Highlight fetchHighlightByUuidAndGroupId(
		String uuid, long groupId) {

		return _highlightLocalService.fetchHighlightByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<highlights.model.Highlight> findAllInGroup(
		long groupId) {

		return _highlightLocalService.findAllInGroup(groupId);
	}

	@Override
	public java.util.List<highlights.model.Highlight> findAllInGroup(
		long groupId, int start, int end) {

		return _highlightLocalService.findAllInGroup(groupId, start, end);
	}

	@Override
	public java.util.List<highlights.model.Highlight> findAllInGroup(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<highlights.model.Highlight> obc) {

		return _highlightLocalService.findAllInGroup(groupId, start, end, obc);
	}

	@Override
	public java.util.List<highlights.model.Highlight> findByTagId(long tagId) {
		return _highlightLocalService.findByTagId(tagId);
	}

	@Override
	public java.util.List<highlights.model.Highlight> findByTagId(
		long tagId, int start, int end) {

		return _highlightLocalService.findByTagId(tagId, start, end);
	}

	@Override
	public java.util.List<highlights.model.Highlight> findByTagId(
		long tagId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<highlights.model.Highlight> obc) {

		return _highlightLocalService.findByTagId(tagId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _highlightLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _highlightLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	/**
	 * Returns the highlight with the primary key.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight
	 * @throws PortalException if a highlight with the primary key could not be found
	 */
	@Override
	public highlights.model.Highlight getHighlight(long highlightId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _highlightLocalService.getHighlight(highlightId);
	}

	/**
	 * Returns the highlight matching the UUID and group.
	 *
	 * @param uuid the highlight's UUID
	 * @param groupId the primary key of the group
	 * @return the matching highlight
	 * @throws PortalException if a matching highlight could not be found
	 */
	@Override
	public highlights.model.Highlight getHighlightByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _highlightLocalService.getHighlightByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public highlights.model.Highlight getHighlightFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws highlights.exception.HighlightValidateException,
			   javax.portlet.PortletException {

		return _highlightLocalService.getHighlightFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of highlights
	 */
	@Override
	public java.util.List<highlights.model.Highlight> getHighlights(
		int start, int end) {

		return _highlightLocalService.getHighlights(start, end);
	}

	/**
	 * Returns all the highlights matching the UUID and company.
	 *
	 * @param uuid the UUID of the highlights
	 * @param companyId the primary key of the company
	 * @return the matching highlights, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<highlights.model.Highlight>
		getHighlightsByUuidAndCompanyId(String uuid, long companyId) {

		return _highlightLocalService.getHighlightsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of highlights matching the UUID and company.
	 *
	 * @param uuid the UUID of the highlights
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching highlights, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<highlights.model.Highlight>
		getHighlightsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<highlights.model.Highlight> orderByComparator) {

		return _highlightLocalService.getHighlightsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of highlights.
	 *
	 * @return the number of highlights
	 */
	@Override
	public int getHighlightsCount() {
		return _highlightLocalService.getHighlightsCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _highlightLocalService.getIndexableActionableDynamicQuery();
	}

	@Override
	public highlights.model.Highlight getNewObject(long primaryKey) {
		return _highlightLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _highlightLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _highlightLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public java.util.List<highlights.model.Highlight> searchHighlights(
		com.liferay.portal.kernel.search.SearchContext searchContext) {

		return _highlightLocalService.searchHighlights(searchContext);
	}

	@Override
	public highlights.model.Highlight updateEntry(
			highlights.model.Highlight orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   highlights.exception.HighlightValidateException {

		return _highlightLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the highlight in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was updated
	 */
	@Override
	public highlights.model.Highlight updateHighlight(
		highlights.model.Highlight highlight) {

		return _highlightLocalService.updateHighlight(highlight);
	}

	@Override
	public HighlightLocalService getWrappedService() {
		return _highlightLocalService;
	}

	@Override
	public void setWrappedService(HighlightLocalService highlightLocalService) {
		_highlightLocalService = highlightLocalService;
	}

	private HighlightLocalService _highlightLocalService;

}