/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import highlights.model.Highlight;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the highlight service. This utility wraps <code>highlights.service.persistence.impl.HighlightPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see HighlightPersistence
 * @generated
 */
public class HighlightUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Highlight highlight) {
		getPersistence().clearCache(highlight);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Highlight> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Highlight> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Highlight> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Highlight> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Highlight update(Highlight highlight) {
		return getPersistence().update(highlight);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Highlight update(
		Highlight highlight, ServiceContext serviceContext) {

		return getPersistence().update(highlight, serviceContext);
	}

	/**
	 * Returns all the highlights where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching highlights
	 */
	public static List<Highlight> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the highlights where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public static List<Highlight> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Highlight> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByUuid_First(
			String uuid, OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByUuid_First(
		String uuid, OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByUuid_Last(
			String uuid, OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByUuid_Last(
		String uuid, OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where uuid = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public static Highlight[] findByUuid_PrevAndNext(
			long highlightId, String uuid,
			OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByUuid_PrevAndNext(
			highlightId, uuid, orderByComparator);
	}

	/**
	 * Removes all the highlights where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of highlights where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching highlights
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the highlight where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchHighlightException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByUUID_G(String uuid, long groupId)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the highlight where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the highlight where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the highlight where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the highlight that was removed
	 */
	public static Highlight removeByUUID_G(String uuid, long groupId)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of highlights where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching highlights
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching highlights
	 */
	public static List<Highlight> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public static List<Highlight> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Highlight> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public static Highlight[] findByUuid_C_PrevAndNext(
			long highlightId, String uuid, long companyId,
			OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByUuid_C_PrevAndNext(
			highlightId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the highlights where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching highlights
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the highlights where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching highlights
	 */
	public static List<Highlight> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the highlights where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public static List<Highlight> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the highlights where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the highlights where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Highlight> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByGroupId_First(
			long groupId, OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByGroupId_First(
		long groupId, OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByGroupId_Last(
			long groupId, OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByGroupId_Last(
		long groupId, OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where groupId = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public static Highlight[] findByGroupId_PrevAndNext(
			long highlightId, long groupId,
			OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByGroupId_PrevAndNext(
			highlightId, groupId, orderByComparator);
	}

	/**
	 * Removes all the highlights where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of highlights where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching highlights
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the highlights where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @return the matching highlights
	 */
	public static List<Highlight> findByTagId(long tagId) {
		return getPersistence().findByTagId(tagId);
	}

	/**
	 * Returns a range of all the highlights where tagId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param tagId the tag ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public static List<Highlight> findByTagId(long tagId, int start, int end) {
		return getPersistence().findByTagId(tagId, start, end);
	}

	/**
	 * Returns an ordered range of all the highlights where tagId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param tagId the tag ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByTagId(
		long tagId, int start, int end,
		OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().findByTagId(
			tagId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the highlights where tagId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param tagId the tag ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public static List<Highlight> findByTagId(
		long tagId, int start, int end,
		OrderByComparator<Highlight> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByTagId(
			tagId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByTagId_First(
			long tagId, OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByTagId_First(tagId, orderByComparator);
	}

	/**
	 * Returns the first highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByTagId_First(
		long tagId, OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByTagId_First(tagId, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public static Highlight findByTagId_Last(
			long tagId, OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByTagId_Last(tagId, orderByComparator);
	}

	/**
	 * Returns the last highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchByTagId_Last(
		long tagId, OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().fetchByTagId_Last(tagId, orderByComparator);
	}

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where tagId = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public static Highlight[] findByTagId_PrevAndNext(
			long highlightId, long tagId,
			OrderByComparator<Highlight> orderByComparator)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByTagId_PrevAndNext(
			highlightId, tagId, orderByComparator);
	}

	/**
	 * Removes all the highlights where tagId = &#63; from the database.
	 *
	 * @param tagId the tag ID
	 */
	public static void removeByTagId(long tagId) {
		getPersistence().removeByTagId(tagId);
	}

	/**
	 * Returns the number of highlights where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @return the number of matching highlights
	 */
	public static int countByTagId(long tagId) {
		return getPersistence().countByTagId(tagId);
	}

	/**
	 * Caches the highlight in the entity cache if it is enabled.
	 *
	 * @param highlight the highlight
	 */
	public static void cacheResult(Highlight highlight) {
		getPersistence().cacheResult(highlight);
	}

	/**
	 * Caches the highlights in the entity cache if it is enabled.
	 *
	 * @param highlights the highlights
	 */
	public static void cacheResult(List<Highlight> highlights) {
		getPersistence().cacheResult(highlights);
	}

	/**
	 * Creates a new highlight with the primary key. Does not add the highlight to the database.
	 *
	 * @param highlightId the primary key for the new highlight
	 * @return the new highlight
	 */
	public static Highlight create(long highlightId) {
		return getPersistence().create(highlightId);
	}

	/**
	 * Removes the highlight with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight that was removed
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public static Highlight remove(long highlightId)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().remove(highlightId);
	}

	public static Highlight updateImpl(Highlight highlight) {
		return getPersistence().updateImpl(highlight);
	}

	/**
	 * Returns the highlight with the primary key or throws a <code>NoSuchHighlightException</code> if it could not be found.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public static Highlight findByPrimaryKey(long highlightId)
		throws highlights.exception.NoSuchHighlightException {

		return getPersistence().findByPrimaryKey(highlightId);
	}

	/**
	 * Returns the highlight with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight, or <code>null</code> if a highlight with the primary key could not be found
	 */
	public static Highlight fetchByPrimaryKey(long highlightId) {
		return getPersistence().fetchByPrimaryKey(highlightId);
	}

	/**
	 * Returns all the highlights.
	 *
	 * @return the highlights
	 */
	public static List<Highlight> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of highlights
	 */
	public static List<Highlight> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of highlights
	 */
	public static List<Highlight> findAll(
		int start, int end, OrderByComparator<Highlight> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of highlights
	 */
	public static List<Highlight> findAll(
		int start, int end, OrderByComparator<Highlight> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the highlights from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of highlights.
	 *
	 * @return the number of highlights
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static HighlightPersistence getPersistence() {
		return _persistence;
	}

	private static volatile HighlightPersistence _persistence;

}