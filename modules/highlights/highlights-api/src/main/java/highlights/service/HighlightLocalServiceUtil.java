/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import highlights.model.Highlight;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service utility for Highlight. This utility wraps
 * <code>highlights.service.impl.HighlightLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see HighlightLocalService
 * @generated
 */
public class HighlightLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>highlights.service.impl.HighlightLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Highlight addEntry(
			Highlight orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws highlights.exception.HighlightValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the highlight to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was added
	 */
	public static Highlight addHighlight(Highlight highlight) {
		return getService().addHighlight(highlight);
	}

	public static int countAllInGroup(long groupId) {
		return getService().countAllInGroup(groupId);
	}

	public static int countByTagId(long tagId) {
		return getService().countByTagId(tagId);
	}

	/**
	 * Creates a new highlight with the primary key. Does not add the highlight to the database.
	 *
	 * @param highlightId the primary key for the new highlight
	 * @return the new highlight
	 */
	public static Highlight createHighlight(long highlightId) {
		return getService().createHighlight(highlightId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static Highlight deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * Deletes the highlight from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was removed
	 */
	public static Highlight deleteHighlight(Highlight highlight) {
		return getService().deleteHighlight(highlight);
	}

	/**
	 * Deletes the highlight with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight that was removed
	 * @throws PortalException if a highlight with the primary key could not be found
	 */
	public static Highlight deleteHighlight(long highlightId)
		throws PortalException {

		return getService().deleteHighlight(highlightId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Highlight fetchHighlight(long highlightId) {
		return getService().fetchHighlight(highlightId);
	}

	/**
	 * Returns the highlight matching the UUID and group.
	 *
	 * @param uuid the highlight's UUID
	 * @param groupId the primary key of the group
	 * @return the matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public static Highlight fetchHighlightByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchHighlightByUuidAndGroupId(uuid, groupId);
	}

	public static List<Highlight> findAllInGroup(long groupId) {
		return getService().findAllInGroup(groupId);
	}

	public static List<Highlight> findAllInGroup(
		long groupId, int start, int end) {

		return getService().findAllInGroup(groupId, start, end);
	}

	public static List<Highlight> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Highlight> obc) {

		return getService().findAllInGroup(groupId, start, end, obc);
	}

	public static List<Highlight> findByTagId(long tagId) {
		return getService().findByTagId(tagId);
	}

	public static List<Highlight> findByTagId(long tagId, int start, int end) {
		return getService().findByTagId(tagId, start, end);
	}

	public static List<Highlight> findByTagId(
		long tagId, int start, int end, OrderByComparator<Highlight> obc) {

		return getService().findByTagId(tagId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	 * Returns the highlight with the primary key.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight
	 * @throws PortalException if a highlight with the primary key could not be found
	 */
	public static Highlight getHighlight(long highlightId)
		throws PortalException {

		return getService().getHighlight(highlightId);
	}

	/**
	 * Returns the highlight matching the UUID and group.
	 *
	 * @param uuid the highlight's UUID
	 * @param groupId the primary key of the group
	 * @return the matching highlight
	 * @throws PortalException if a matching highlight could not be found
	 */
	public static Highlight getHighlightByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getHighlightByUuidAndGroupId(uuid, groupId);
	}

	public static Highlight getHighlightFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws highlights.exception.HighlightValidateException,
			   javax.portlet.PortletException {

		return getService().getHighlightFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of highlights
	 */
	public static List<Highlight> getHighlights(int start, int end) {
		return getService().getHighlights(start, end);
	}

	/**
	 * Returns all the highlights matching the UUID and company.
	 *
	 * @param uuid the UUID of the highlights
	 * @param companyId the primary key of the company
	 * @return the matching highlights, or an empty list if no matches were found
	 */
	public static List<Highlight> getHighlightsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getHighlightsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of highlights matching the UUID and company.
	 *
	 * @param uuid the UUID of the highlights
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching highlights, or an empty list if no matches were found
	 */
	public static List<Highlight> getHighlightsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Highlight> orderByComparator) {

		return getService().getHighlightsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of highlights.
	 *
	 * @return the number of highlights
	 */
	public static int getHighlightsCount() {
		return getService().getHighlightsCount();
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	public static Highlight getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static List<Highlight> searchHighlights(
		com.liferay.portal.kernel.search.SearchContext searchContext) {

		return getService().searchHighlights(searchContext);
	}

	public static Highlight updateEntry(
			Highlight orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws highlights.exception.HighlightValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the highlight in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was updated
	 */
	public static Highlight updateHighlight(Highlight highlight) {
		return getService().updateHighlight(highlight);
	}

	public static HighlightLocalService getService() {
		return _service;
	}

	private static volatile HighlightLocalService _service;

}