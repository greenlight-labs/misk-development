/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import highlights.exception.NoSuchHighlightException;

import highlights.model.Highlight;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the highlight service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see HighlightUtil
 * @generated
 */
@ProviderType
public interface HighlightPersistence extends BasePersistence<Highlight> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link HighlightUtil} to access the highlight persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the highlights where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching highlights
	 */
	public java.util.List<Highlight> findByUuid(String uuid);

	/**
	 * Returns a range of all the highlights where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public java.util.List<Highlight> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where uuid = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public Highlight[] findByUuid_PrevAndNext(
			long highlightId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Removes all the highlights where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of highlights where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching highlights
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the highlight where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchHighlightException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByUUID_G(String uuid, long groupId)
		throws NoSuchHighlightException;

	/**
	 * Returns the highlight where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the highlight where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the highlight where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the highlight that was removed
	 */
	public Highlight removeByUUID_G(String uuid, long groupId)
		throws NoSuchHighlightException;

	/**
	 * Returns the number of highlights where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching highlights
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching highlights
	 */
	public java.util.List<Highlight> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public java.util.List<Highlight> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns an ordered range of all the highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the first highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the last highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public Highlight[] findByUuid_C_PrevAndNext(
			long highlightId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Removes all the highlights where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of highlights where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching highlights
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the highlights where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching highlights
	 */
	public java.util.List<Highlight> findByGroupId(long groupId);

	/**
	 * Returns a range of all the highlights where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public java.util.List<Highlight> findByGroupId(
		long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the highlights where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns an ordered range of all the highlights where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the first highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the last highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the last highlight in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where groupId = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public Highlight[] findByGroupId_PrevAndNext(
			long highlightId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Removes all the highlights where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of highlights where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching highlights
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the highlights where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @return the matching highlights
	 */
	public java.util.List<Highlight> findByTagId(long tagId);

	/**
	 * Returns a range of all the highlights where tagId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param tagId the tag ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of matching highlights
	 */
	public java.util.List<Highlight> findByTagId(
		long tagId, int start, int end);

	/**
	 * Returns an ordered range of all the highlights where tagId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param tagId the tag ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByTagId(
		long tagId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns an ordered range of all the highlights where tagId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param tagId the tag ID
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching highlights
	 */
	public java.util.List<Highlight> findByTagId(
		long tagId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByTagId_First(
			long tagId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the first highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByTagId_First(
		long tagId,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the last highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight
	 * @throws NoSuchHighlightException if a matching highlight could not be found
	 */
	public Highlight findByTagId_Last(
			long tagId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Returns the last highlight in the ordered set where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	public Highlight fetchByTagId_Last(
		long tagId,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns the highlights before and after the current highlight in the ordered set where tagId = &#63;.
	 *
	 * @param highlightId the primary key of the current highlight
	 * @param tagId the tag ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public Highlight[] findByTagId_PrevAndNext(
			long highlightId, long tagId,
			com.liferay.portal.kernel.util.OrderByComparator<Highlight>
				orderByComparator)
		throws NoSuchHighlightException;

	/**
	 * Removes all the highlights where tagId = &#63; from the database.
	 *
	 * @param tagId the tag ID
	 */
	public void removeByTagId(long tagId);

	/**
	 * Returns the number of highlights where tagId = &#63;.
	 *
	 * @param tagId the tag ID
	 * @return the number of matching highlights
	 */
	public int countByTagId(long tagId);

	/**
	 * Caches the highlight in the entity cache if it is enabled.
	 *
	 * @param highlight the highlight
	 */
	public void cacheResult(Highlight highlight);

	/**
	 * Caches the highlights in the entity cache if it is enabled.
	 *
	 * @param highlights the highlights
	 */
	public void cacheResult(java.util.List<Highlight> highlights);

	/**
	 * Creates a new highlight with the primary key. Does not add the highlight to the database.
	 *
	 * @param highlightId the primary key for the new highlight
	 * @return the new highlight
	 */
	public Highlight create(long highlightId);

	/**
	 * Removes the highlight with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight that was removed
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public Highlight remove(long highlightId) throws NoSuchHighlightException;

	public Highlight updateImpl(Highlight highlight);

	/**
	 * Returns the highlight with the primary key or throws a <code>NoSuchHighlightException</code> if it could not be found.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight
	 * @throws NoSuchHighlightException if a highlight with the primary key could not be found
	 */
	public Highlight findByPrimaryKey(long highlightId)
		throws NoSuchHighlightException;

	/**
	 * Returns the highlight with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight, or <code>null</code> if a highlight with the primary key could not be found
	 */
	public Highlight fetchByPrimaryKey(long highlightId);

	/**
	 * Returns all the highlights.
	 *
	 * @return the highlights
	 */
	public java.util.List<Highlight> findAll();

	/**
	 * Returns a range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of highlights
	 */
	public java.util.List<Highlight> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of highlights
	 */
	public java.util.List<Highlight> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator);

	/**
	 * Returns an ordered range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of highlights
	 */
	public java.util.List<Highlight> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Highlight>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the highlights from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of highlights.
	 *
	 * @return the number of highlights
	 */
	public int countAll();

}