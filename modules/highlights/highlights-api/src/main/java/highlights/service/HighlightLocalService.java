/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package highlights.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import highlights.exception.HighlightValidateException;

import highlights.model.Highlight;

import java.io.Serializable;

import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for Highlight. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see HighlightLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface HighlightLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>highlights.service.impl.HighlightLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the highlight local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link HighlightLocalServiceUtil} if injection and service tracking are not available.
	 */
	public Highlight addEntry(Highlight orgEntry, ServiceContext serviceContext)
		throws HighlightValidateException, PortalException;

	/**
	 * Adds the highlight to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Highlight addHighlight(Highlight highlight);

	public int countAllInGroup(long groupId);

	public int countByTagId(long tagId);

	/**
	 * Creates a new highlight with the primary key. Does not add the highlight to the database.
	 *
	 * @param highlightId the primary key for the new highlight
	 * @return the new highlight
	 */
	@Transactional(enabled = false)
	public Highlight createHighlight(long highlightId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public Highlight deleteEntry(long primaryKey) throws PortalException;

	/**
	 * Deletes the highlight from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public Highlight deleteHighlight(Highlight highlight);

	/**
	 * Deletes the highlight with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight that was removed
	 * @throws PortalException if a highlight with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public Highlight deleteHighlight(long highlightId) throws PortalException;

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Highlight fetchHighlight(long highlightId);

	/**
	 * Returns the highlight matching the UUID and group.
	 *
	 * @param uuid the highlight's UUID
	 * @param groupId the primary key of the group
	 * @return the matching highlight, or <code>null</code> if a matching highlight could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Highlight fetchHighlightByUuidAndGroupId(String uuid, long groupId);

	public List<Highlight> findAllInGroup(long groupId);

	public List<Highlight> findAllInGroup(long groupId, int start, int end);

	public List<Highlight> findAllInGroup(
		long groupId, int start, int end, OrderByComparator<Highlight> obc);

	public List<Highlight> findByTagId(long tagId);

	public List<Highlight> findByTagId(long tagId, int start, int end);

	public List<Highlight> findByTagId(
		long tagId, int start, int end, OrderByComparator<Highlight> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	/**
	 * Returns the highlight with the primary key.
	 *
	 * @param highlightId the primary key of the highlight
	 * @return the highlight
	 * @throws PortalException if a highlight with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Highlight getHighlight(long highlightId) throws PortalException;

	/**
	 * Returns the highlight matching the UUID and group.
	 *
	 * @param uuid the highlight's UUID
	 * @param groupId the primary key of the group
	 * @return the matching highlight
	 * @throws PortalException if a matching highlight could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Highlight getHighlightByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Highlight getHighlightFromRequest(
			long primaryKey, PortletRequest request)
		throws HighlightValidateException, PortletException;

	/**
	 * Returns a range of all the highlights.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>highlights.model.impl.HighlightModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @return the range of highlights
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Highlight> getHighlights(int start, int end);

	/**
	 * Returns all the highlights matching the UUID and company.
	 *
	 * @param uuid the UUID of the highlights
	 * @param companyId the primary key of the company
	 * @return the matching highlights, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Highlight> getHighlightsByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of highlights matching the UUID and company.
	 *
	 * @param uuid the UUID of the highlights
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of highlights
	 * @param end the upper bound of the range of highlights (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching highlights, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Highlight> getHighlightsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Highlight> orderByComparator);

	/**
	 * Returns the number of highlights.
	 *
	 * @return the number of highlights
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getHighlightsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Highlight getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Highlight> searchHighlights(SearchContext searchContext);

	public Highlight updateEntry(
			Highlight orgEntry, ServiceContext serviceContext)
		throws HighlightValidateException, PortalException;

	/**
	 * Updates the highlight in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect HighlightLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param highlight the highlight
	 * @return the highlight that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public Highlight updateHighlight(Highlight highlight);

}