/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ShopLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ShopLocalService
 * @generated
 */
public class ShopLocalServiceWrapper
	implements ServiceWrapper<ShopLocalService>, ShopLocalService {

	public ShopLocalServiceWrapper(ShopLocalService shopLocalService) {
		_shopLocalService = shopLocalService;
	}

	@Override
	public shops.model.Shop addShop(
			long userId, long categoryId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> overlayImageMap,
			java.util.Map<java.util.Locale, String> openTimingsMap,
			java.util.Map<java.util.Locale, String> deliveryTimingsMap,
			java.util.Map<java.util.Locale, String> callMap,
			java.util.Map<java.util.Locale, String> websiteMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _shopLocalService.addShop(
			userId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Adds the shop to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shop the shop
	 * @return the shop that was added
	 */
	@Override
	public shops.model.Shop addShop(shops.model.Shop shop) {
		return _shopLocalService.addShop(shop);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _shopLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new shop with the primary key. Does not add the shop to the database.
	 *
	 * @param shopId the primary key for the new shop
	 * @return the new shop
	 */
	@Override
	public shops.model.Shop createShop(long shopId) {
		return _shopLocalService.createShop(shopId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _shopLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the shop with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop that was removed
	 * @throws PortalException if a shop with the primary key could not be found
	 */
	@Override
	public shops.model.Shop deleteShop(long shopId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _shopLocalService.deleteShop(shopId);
	}

	@Override
	public shops.model.Shop deleteShop(
			long shopId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _shopLocalService.deleteShop(shopId, serviceContext);
	}

	/**
	 * Deletes the shop from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shop the shop
	 * @return the shop that was removed
	 */
	@Override
	public shops.model.Shop deleteShop(shops.model.Shop shop) {
		return _shopLocalService.deleteShop(shop);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _shopLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _shopLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>shops.model.impl.ShopModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _shopLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>shops.model.impl.ShopModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _shopLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _shopLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _shopLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public shops.model.Shop fetchShop(long shopId) {
		return _shopLocalService.fetchShop(shopId);
	}

	/**
	 * Returns the shop matching the UUID and group.
	 *
	 * @param uuid the shop's UUID
	 * @param groupId the primary key of the group
	 * @return the matching shop, or <code>null</code> if a matching shop could not be found
	 */
	@Override
	public shops.model.Shop fetchShopByUuidAndGroupId(
		String uuid, long groupId) {

		return _shopLocalService.fetchShopByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _shopLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _shopLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _shopLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _shopLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _shopLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the shop with the primary key.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop
	 * @throws PortalException if a shop with the primary key could not be found
	 */
	@Override
	public shops.model.Shop getShop(long shopId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _shopLocalService.getShop(shopId);
	}

	/**
	 * Returns the shop matching the UUID and group.
	 *
	 * @param uuid the shop's UUID
	 * @param groupId the primary key of the group
	 * @return the matching shop
	 * @throws PortalException if a matching shop could not be found
	 */
	@Override
	public shops.model.Shop getShopByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _shopLocalService.getShopByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>shops.model.impl.ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of shops
	 */
	@Override
	public java.util.List<shops.model.Shop> getShops(int start, int end) {
		return _shopLocalService.getShops(start, end);
	}

	@Override
	public java.util.List<shops.model.Shop> getShops(long groupId) {
		return _shopLocalService.getShops(groupId);
	}

	@Override
	public java.util.List<shops.model.Shop> getShops(
		long groupId, int start, int end) {

		return _shopLocalService.getShops(groupId, start, end);
	}

	@Override
	public java.util.List<shops.model.Shop> getShops(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<shops.model.Shop>
			obc) {

		return _shopLocalService.getShops(groupId, start, end, obc);
	}

	/**
	 * Returns all the shops matching the UUID and company.
	 *
	 * @param uuid the UUID of the shops
	 * @param companyId the primary key of the company
	 * @return the matching shops, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<shops.model.Shop> getShopsByUuidAndCompanyId(
		String uuid, long companyId) {

		return _shopLocalService.getShopsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of shops matching the UUID and company.
	 *
	 * @param uuid the UUID of the shops
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching shops, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<shops.model.Shop> getShopsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<shops.model.Shop>
			orderByComparator) {

		return _shopLocalService.getShopsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of shops.
	 *
	 * @return the number of shops
	 */
	@Override
	public int getShopsCount() {
		return _shopLocalService.getShopsCount();
	}

	@Override
	public int getShopsCount(long groupId) {
		return _shopLocalService.getShopsCount(groupId);
	}

	@Override
	public shops.model.Shop updateShop(
			long userId, long shopId, long categoryId,
			java.util.Map<java.util.Locale, String> titleMap,
			java.util.Map<java.util.Locale, String> imageMap,
			java.util.Map<java.util.Locale, String> overlayImageMap,
			java.util.Map<java.util.Locale, String> openTimingsMap,
			java.util.Map<java.util.Locale, String> deliveryTimingsMap,
			java.util.Map<java.util.Locale, String> callMap,
			java.util.Map<java.util.Locale, String> websiteMap,
			java.util.Map<java.util.Locale, String> buttonLabelMap,
			java.util.Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _shopLocalService.updateShop(
			userId, shopId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Updates the shop in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shop the shop
	 * @return the shop that was updated
	 */
	@Override
	public shops.model.Shop updateShop(shops.model.Shop shop) {
		return _shopLocalService.updateShop(shop);
	}

	@Override
	public ShopLocalService getWrappedService() {
		return _shopLocalService;
	}

	@Override
	public void setWrappedService(ShopLocalService shopLocalService) {
		_shopLocalService = shopLocalService;
	}

	private ShopLocalService _shopLocalService;

}