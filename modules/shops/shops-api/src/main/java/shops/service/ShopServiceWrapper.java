/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ShopService}.
 *
 * @author Brian Wing Shun Chan
 * @see ShopService
 * @generated
 */
public class ShopServiceWrapper
	implements ServiceWrapper<ShopService>, ShopService {

	public ShopServiceWrapper(ShopService shopService) {
		_shopService = shopService;
	}

	@Override
	public java.util.List<shops.model.Shop> findByTitle(String title) {
		return _shopService.findByTitle(title);
	}

	@Override
	public java.util.List<shops.model.Shop> findByTitle(
		String title, int start, int end) {

		return _shopService.findByTitle(title, start, end);
	}

	@Override
	public java.util.List<shops.model.Shop> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<shops.model.Shop>
			obc) {

		return _shopService.findByTitle(title, start, end, obc);
	}

	@Override
	public int findByTitleCount(String title) {
		return _shopService.findByTitleCount(title);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _shopService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<shops.model.Shop> getShops(long groupId) {
		return _shopService.getShops(groupId);
	}

	@Override
	public java.util.List<shops.model.Shop> getShops(
		long groupId, int start, int end) {

		return _shopService.getShops(groupId, start, end);
	}

	@Override
	public java.util.List<shops.model.Shop> getShops(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<shops.model.Shop>
			obc) {

		return _shopService.getShops(groupId, start, end, obc);
	}

	@Override
	public java.util.List<shops.model.Shop> getShopsByCategory(
		long categoryId) {

		return _shopService.getShopsByCategory(categoryId);
	}

	@Override
	public java.util.List<shops.model.Shop> getShopsByCategory(
		long categoryId, int start, int end) {

		return _shopService.getShopsByCategory(categoryId, start, end);
	}

	@Override
	public java.util.List<shops.model.Shop> getShopsByCategory(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<shops.model.Shop>
			obc) {

		return _shopService.getShopsByCategory(categoryId, start, end, obc);
	}

	@Override
	public int getShopsByCategoryCount(long categoryId) {
		return _shopService.getShopsByCategoryCount(categoryId);
	}

	@Override
	public int getShopsCount(long groupId) {
		return _shopService.getShopsCount(groupId);
	}

	@Override
	public ShopService getWrappedService() {
		return _shopService;
	}

	@Override
	public void setWrappedService(ShopService shopService) {
		_shopService = shopService;
	}

	private ShopService _shopService;

}