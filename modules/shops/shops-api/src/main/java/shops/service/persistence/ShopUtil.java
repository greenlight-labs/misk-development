/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import shops.model.Shop;

/**
 * The persistence utility for the shop service. This utility wraps <code>shops.service.persistence.impl.ShopPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ShopPersistence
 * @generated
 */
public class ShopUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Shop shop) {
		getPersistence().clearCache(shop);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Shop> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Shop> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Shop> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Shop> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Shop update(Shop shop) {
		return getPersistence().update(shop);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Shop update(Shop shop, ServiceContext serviceContext) {
		return getPersistence().update(shop, serviceContext);
	}

	/**
	 * Returns all the shops where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching shops
	 */
	public static List<Shop> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the shops where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public static List<Shop> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the shops where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the shops where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Shop> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByUuid_First(
			String uuid, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByUuid_First(
		String uuid, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByUuid_Last(
			String uuid, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByUuid_Last(
		String uuid, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the shops before and after the current shop in the ordered set where uuid = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public static Shop[] findByUuid_PrevAndNext(
			long shopId, String uuid, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByUuid_PrevAndNext(
			shopId, uuid, orderByComparator);
	}

	/**
	 * Removes all the shops where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of shops where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching shops
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the shop where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchShopException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByUUID_G(String uuid, long groupId)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the shop where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the shop where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the shop where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the shop that was removed
	 */
	public static Shop removeByUUID_G(String uuid, long groupId)
		throws shops.exception.NoSuchShopException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of shops where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching shops
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching shops
	 */
	public static List<Shop> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public static List<Shop> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Shop> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the shops before and after the current shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public static Shop[] findByUuid_C_PrevAndNext(
			long shopId, String uuid, long companyId,
			OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByUuid_C_PrevAndNext(
			shopId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the shops where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of shops where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching shops
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the shops where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching shops
	 */
	public static List<Shop> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the shops where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public static List<Shop> findByGroupId(long groupId, int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the shops where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the shops where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Shop> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByGroupId_First(
			long groupId, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByGroupId_First(
		long groupId, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByGroupId_Last(
			long groupId, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByGroupId_Last(
		long groupId, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the shops before and after the current shop in the ordered set where groupId = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public static Shop[] findByGroupId_PrevAndNext(
			long shopId, long groupId,
			OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByGroupId_PrevAndNext(
			shopId, groupId, orderByComparator);
	}

	/**
	 * Removes all the shops where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of shops where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching shops
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Returns all the shops where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching shops
	 */
	public static List<Shop> findByCategoryId(long categoryId) {
		return getPersistence().findByCategoryId(categoryId);
	}

	/**
	 * Returns a range of all the shops where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public static List<Shop> findByCategoryId(
		long categoryId, int start, int end) {

		return getPersistence().findByCategoryId(categoryId, start, end);
	}

	/**
	 * Returns an ordered range of all the shops where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the shops where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByCategoryId(
		long categoryId, int start, int end,
		OrderByComparator<Shop> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByCategoryId(
			categoryId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByCategoryId_First(
			long categoryId, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the first shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByCategoryId_First(
		long categoryId, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByCategoryId_First(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByCategoryId_Last(
			long categoryId, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByCategoryId_Last(
		long categoryId, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByCategoryId_Last(
			categoryId, orderByComparator);
	}

	/**
	 * Returns the shops before and after the current shop in the ordered set where categoryId = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public static Shop[] findByCategoryId_PrevAndNext(
			long shopId, long categoryId,
			OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByCategoryId_PrevAndNext(
			shopId, categoryId, orderByComparator);
	}

	/**
	 * Removes all the shops where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public static void removeByCategoryId(long categoryId) {
		getPersistence().removeByCategoryId(categoryId);
	}

	/**
	 * Returns the number of shops where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching shops
	 */
	public static int countByCategoryId(long categoryId) {
		return getPersistence().countByCategoryId(categoryId);
	}

	/**
	 * Returns all the shops where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the matching shops
	 */
	public static List<Shop> findByTitle(String title) {
		return getPersistence().findByTitle(title);
	}

	/**
	 * Returns a range of all the shops where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public static List<Shop> findByTitle(String title, int start, int end) {
		return getPersistence().findByTitle(title, start, end);
	}

	/**
	 * Returns an ordered range of all the shops where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByTitle(
		String title, int start, int end,
		OrderByComparator<Shop> orderByComparator) {

		return getPersistence().findByTitle(
			title, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the shops where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public static List<Shop> findByTitle(
		String title, int start, int end,
		OrderByComparator<Shop> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByTitle(
			title, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByTitle_First(
			String title, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByTitle_First(title, orderByComparator);
	}

	/**
	 * Returns the first shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByTitle_First(
		String title, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByTitle_First(title, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public static Shop findByTitle_Last(
			String title, OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByTitle_Last(title, orderByComparator);
	}

	/**
	 * Returns the last shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchByTitle_Last(
		String title, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().fetchByTitle_Last(title, orderByComparator);
	}

	/**
	 * Returns the shops before and after the current shop in the ordered set where title LIKE &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public static Shop[] findByTitle_PrevAndNext(
			long shopId, String title,
			OrderByComparator<Shop> orderByComparator)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByTitle_PrevAndNext(
			shopId, title, orderByComparator);
	}

	/**
	 * Removes all the shops where title LIKE &#63; from the database.
	 *
	 * @param title the title
	 */
	public static void removeByTitle(String title) {
		getPersistence().removeByTitle(title);
	}

	/**
	 * Returns the number of shops where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the number of matching shops
	 */
	public static int countByTitle(String title) {
		return getPersistence().countByTitle(title);
	}

	/**
	 * Caches the shop in the entity cache if it is enabled.
	 *
	 * @param shop the shop
	 */
	public static void cacheResult(Shop shop) {
		getPersistence().cacheResult(shop);
	}

	/**
	 * Caches the shops in the entity cache if it is enabled.
	 *
	 * @param shops the shops
	 */
	public static void cacheResult(List<Shop> shops) {
		getPersistence().cacheResult(shops);
	}

	/**
	 * Creates a new shop with the primary key. Does not add the shop to the database.
	 *
	 * @param shopId the primary key for the new shop
	 * @return the new shop
	 */
	public static Shop create(long shopId) {
		return getPersistence().create(shopId);
	}

	/**
	 * Removes the shop with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop that was removed
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public static Shop remove(long shopId)
		throws shops.exception.NoSuchShopException {

		return getPersistence().remove(shopId);
	}

	public static Shop updateImpl(Shop shop) {
		return getPersistence().updateImpl(shop);
	}

	/**
	 * Returns the shop with the primary key or throws a <code>NoSuchShopException</code> if it could not be found.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public static Shop findByPrimaryKey(long shopId)
		throws shops.exception.NoSuchShopException {

		return getPersistence().findByPrimaryKey(shopId);
	}

	/**
	 * Returns the shop with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop, or <code>null</code> if a shop with the primary key could not be found
	 */
	public static Shop fetchByPrimaryKey(long shopId) {
		return getPersistence().fetchByPrimaryKey(shopId);
	}

	/**
	 * Returns all the shops.
	 *
	 * @return the shops
	 */
	public static List<Shop> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of shops
	 */
	public static List<Shop> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of shops
	 */
	public static List<Shop> findAll(
		int start, int end, OrderByComparator<Shop> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of shops
	 */
	public static List<Shop> findAll(
		int start, int end, OrderByComparator<Shop> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the shops from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of shops.
	 *
	 * @return the number of shops
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ShopPersistence getPersistence() {
		return _persistence;
	}

	private static volatile ShopPersistence _persistence;

}