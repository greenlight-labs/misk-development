/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

import shops.model.Shop;

/**
 * Provides the remote service interface for Shop. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ShopServiceUtil
 * @generated
 */
@AccessControlled
@JSONWebService
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface ShopService extends BaseService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>shops.service.impl.ShopServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the shop remote service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link ShopServiceUtil} if injection and service tracking are not available.
	 */
	public List<Shop> findByTitle(String title);

	public List<Shop> findByTitle(String title, int start, int end);

	public List<Shop> findByTitle(
		String title, int start, int end, OrderByComparator<Shop> obc);

	public int findByTitleCount(String title);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Shop> getShops(long groupId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Shop> getShops(long groupId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Shop> getShops(
		long groupId, int start, int end, OrderByComparator<Shop> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Shop> getShopsByCategory(long categoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Shop> getShopsByCategory(long categoryId, int start, int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Shop> getShopsByCategory(
		long categoryId, int start, int end, OrderByComparator<Shop> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getShopsByCategoryCount(long categoryId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getShopsCount(long groupId);

}