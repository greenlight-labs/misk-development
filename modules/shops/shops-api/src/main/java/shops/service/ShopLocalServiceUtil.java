/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import shops.model.Shop;

/**
 * Provides the local service utility for Shop. This utility wraps
 * <code>shops.service.impl.ShopLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ShopLocalService
 * @generated
 */
public class ShopLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>shops.service.impl.ShopLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Shop addShop(
			long userId, long categoryId,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> imageMap,
			Map<java.util.Locale, String> overlayImageMap,
			Map<java.util.Locale, String> openTimingsMap,
			Map<java.util.Locale, String> deliveryTimingsMap,
			Map<java.util.Locale, String> callMap,
			Map<java.util.Locale, String> websiteMap,
			Map<java.util.Locale, String> buttonLabelMap,
			Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addShop(
			userId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Adds the shop to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shop the shop
	 * @return the shop that was added
	 */
	public static Shop addShop(Shop shop) {
		return getService().addShop(shop);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Creates a new shop with the primary key. Does not add the shop to the database.
	 *
	 * @param shopId the primary key for the new shop
	 * @return the new shop
	 */
	public static Shop createShop(long shopId) {
		return getService().createShop(shopId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the shop with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop that was removed
	 * @throws PortalException if a shop with the primary key could not be found
	 */
	public static Shop deleteShop(long shopId) throws PortalException {
		return getService().deleteShop(shopId);
	}

	public static Shop deleteShop(
			long shopId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteShop(shopId, serviceContext);
	}

	/**
	 * Deletes the shop from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shop the shop
	 * @return the shop that was removed
	 */
	public static Shop deleteShop(Shop shop) {
		return getService().deleteShop(shop);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>shops.model.impl.ShopModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>shops.model.impl.ShopModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Shop fetchShop(long shopId) {
		return getService().fetchShop(shopId);
	}

	/**
	 * Returns the shop matching the UUID and group.
	 *
	 * @param uuid the shop's UUID
	 * @param groupId the primary key of the group
	 * @return the matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public static Shop fetchShopByUuidAndGroupId(String uuid, long groupId) {
		return getService().fetchShopByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the shop with the primary key.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop
	 * @throws PortalException if a shop with the primary key could not be found
	 */
	public static Shop getShop(long shopId) throws PortalException {
		return getService().getShop(shopId);
	}

	/**
	 * Returns the shop matching the UUID and group.
	 *
	 * @param uuid the shop's UUID
	 * @param groupId the primary key of the group
	 * @return the matching shop
	 * @throws PortalException if a matching shop could not be found
	 */
	public static Shop getShopByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getShopByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>shops.model.impl.ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of shops
	 */
	public static List<Shop> getShops(int start, int end) {
		return getService().getShops(start, end);
	}

	public static List<Shop> getShops(long groupId) {
		return getService().getShops(groupId);
	}

	public static List<Shop> getShops(long groupId, int start, int end) {
		return getService().getShops(groupId, start, end);
	}

	public static List<Shop> getShops(
		long groupId, int start, int end, OrderByComparator<Shop> obc) {

		return getService().getShops(groupId, start, end, obc);
	}

	/**
	 * Returns all the shops matching the UUID and company.
	 *
	 * @param uuid the UUID of the shops
	 * @param companyId the primary key of the company
	 * @return the matching shops, or an empty list if no matches were found
	 */
	public static List<Shop> getShopsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getShopsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of shops matching the UUID and company.
	 *
	 * @param uuid the UUID of the shops
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching shops, or an empty list if no matches were found
	 */
	public static List<Shop> getShopsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Shop> orderByComparator) {

		return getService().getShopsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of shops.
	 *
	 * @return the number of shops
	 */
	public static int getShopsCount() {
		return getService().getShopsCount();
	}

	public static int getShopsCount(long groupId) {
		return getService().getShopsCount(groupId);
	}

	public static Shop updateShop(
			long userId, long shopId, long categoryId,
			Map<java.util.Locale, String> titleMap,
			Map<java.util.Locale, String> imageMap,
			Map<java.util.Locale, String> overlayImageMap,
			Map<java.util.Locale, String> openTimingsMap,
			Map<java.util.Locale, String> deliveryTimingsMap,
			Map<java.util.Locale, String> callMap,
			Map<java.util.Locale, String> websiteMap,
			Map<java.util.Locale, String> buttonLabelMap,
			Map<java.util.Locale, String> buttonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateShop(
			userId, shopId, categoryId, titleMap, imageMap, overlayImageMap,
			openTimingsMap, deliveryTimingsMap, callMap, websiteMap,
			buttonLabelMap, buttonLinkMap, serviceContext);
	}

	/**
	 * Updates the shop in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect ShopLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param shop the shop
	 * @return the shop that was updated
	 */
	public static Shop updateShop(Shop shop) {
		return getService().updateShop(shop);
	}

	public static ShopLocalService getService() {
		return _service;
	}

	private static volatile ShopLocalService _service;

}