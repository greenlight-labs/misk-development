/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

import shops.model.Shop;

/**
 * Provides the remote service utility for Shop. This utility wraps
 * <code>shops.service.impl.ShopServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ShopService
 * @generated
 */
public class ShopServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>shops.service.impl.ShopServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static List<Shop> findByTitle(String title) {
		return getService().findByTitle(title);
	}

	public static List<Shop> findByTitle(String title, int start, int end) {
		return getService().findByTitle(title, start, end);
	}

	public static List<Shop> findByTitle(
		String title, int start, int end, OrderByComparator<Shop> obc) {

		return getService().findByTitle(title, start, end, obc);
	}

	public static int findByTitleCount(String title) {
		return getService().findByTitleCount(title);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static List<Shop> getShops(long groupId) {
		return getService().getShops(groupId);
	}

	public static List<Shop> getShops(long groupId, int start, int end) {
		return getService().getShops(groupId, start, end);
	}

	public static List<Shop> getShops(
		long groupId, int start, int end, OrderByComparator<Shop> obc) {

		return getService().getShops(groupId, start, end, obc);
	}

	public static List<Shop> getShopsByCategory(long categoryId) {
		return getService().getShopsByCategory(categoryId);
	}

	public static List<Shop> getShopsByCategory(
		long categoryId, int start, int end) {

		return getService().getShopsByCategory(categoryId, start, end);
	}

	public static List<Shop> getShopsByCategory(
		long categoryId, int start, int end, OrderByComparator<Shop> obc) {

		return getService().getShopsByCategory(categoryId, start, end, obc);
	}

	public static int getShopsByCategoryCount(long categoryId) {
		return getService().getShopsByCategoryCount(categoryId);
	}

	public static int getShopsCount(long groupId) {
		return getService().getShopsCount(groupId);
	}

	public static ShopService getService() {
		return _service;
	}

	private static volatile ShopService _service;

}