/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

import shops.exception.NoSuchShopException;

import shops.model.Shop;

/**
 * The persistence interface for the shop service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ShopUtil
 * @generated
 */
@ProviderType
public interface ShopPersistence extends BasePersistence<Shop> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ShopUtil} to access the shop persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the shops where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching shops
	 */
	public java.util.List<Shop> findByUuid(String uuid);

	/**
	 * Returns a range of all the shops where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public java.util.List<Shop> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the shops where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns an ordered range of all the shops where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the first shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the last shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the last shop in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the shops before and after the current shop in the ordered set where uuid = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public Shop[] findByUuid_PrevAndNext(
			long shopId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Removes all the shops where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of shops where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching shops
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the shop where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchShopException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByUUID_G(String uuid, long groupId)
		throws NoSuchShopException;

	/**
	 * Returns the shop where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the shop where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the shop where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the shop that was removed
	 */
	public Shop removeByUUID_G(String uuid, long groupId)
		throws NoSuchShopException;

	/**
	 * Returns the number of shops where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching shops
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching shops
	 */
	public java.util.List<Shop> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public java.util.List<Shop> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns an ordered range of all the shops where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the first shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the last shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the last shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the shops before and after the current shop in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public Shop[] findByUuid_C_PrevAndNext(
			long shopId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Removes all the shops where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of shops where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching shops
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the shops where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching shops
	 */
	public java.util.List<Shop> findByGroupId(long groupId);

	/**
	 * Returns a range of all the shops where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public java.util.List<Shop> findByGroupId(long groupId, int start, int end);

	/**
	 * Returns an ordered range of all the shops where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns an ordered range of all the shops where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByGroupId_First(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the first shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the last shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByGroupId_Last(
			long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the last shop in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the shops before and after the current shop in the ordered set where groupId = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public Shop[] findByGroupId_PrevAndNext(
			long shopId, long groupId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Removes all the shops where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public void removeByGroupId(long groupId);

	/**
	 * Returns the number of shops where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching shops
	 */
	public int countByGroupId(long groupId);

	/**
	 * Returns all the shops where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the matching shops
	 */
	public java.util.List<Shop> findByCategoryId(long categoryId);

	/**
	 * Returns a range of all the shops where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public java.util.List<Shop> findByCategoryId(
		long categoryId, int start, int end);

	/**
	 * Returns an ordered range of all the shops where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns an ordered range of all the shops where categoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param categoryId the category ID
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByCategoryId(
		long categoryId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByCategoryId_First(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the first shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByCategoryId_First(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the last shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByCategoryId_Last(
			long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the last shop in the ordered set where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByCategoryId_Last(
		long categoryId,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the shops before and after the current shop in the ordered set where categoryId = &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param categoryId the category ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public Shop[] findByCategoryId_PrevAndNext(
			long shopId, long categoryId,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Removes all the shops where categoryId = &#63; from the database.
	 *
	 * @param categoryId the category ID
	 */
	public void removeByCategoryId(long categoryId);

	/**
	 * Returns the number of shops where categoryId = &#63;.
	 *
	 * @param categoryId the category ID
	 * @return the number of matching shops
	 */
	public int countByCategoryId(long categoryId);

	/**
	 * Returns all the shops where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the matching shops
	 */
	public java.util.List<Shop> findByTitle(String title);

	/**
	 * Returns a range of all the shops where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of matching shops
	 */
	public java.util.List<Shop> findByTitle(String title, int start, int end);

	/**
	 * Returns an ordered range of all the shops where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns an ordered range of all the shops where title LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param title the title
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching shops
	 */
	public java.util.List<Shop> findByTitle(
		String title, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByTitle_First(
			String title,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the first shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByTitle_First(
		String title,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the last shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop
	 * @throws NoSuchShopException if a matching shop could not be found
	 */
	public Shop findByTitle_Last(
			String title,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Returns the last shop in the ordered set where title LIKE &#63;.
	 *
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shop, or <code>null</code> if a matching shop could not be found
	 */
	public Shop fetchByTitle_Last(
		String title,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns the shops before and after the current shop in the ordered set where title LIKE &#63;.
	 *
	 * @param shopId the primary key of the current shop
	 * @param title the title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public Shop[] findByTitle_PrevAndNext(
			long shopId, String title,
			com.liferay.portal.kernel.util.OrderByComparator<Shop>
				orderByComparator)
		throws NoSuchShopException;

	/**
	 * Removes all the shops where title LIKE &#63; from the database.
	 *
	 * @param title the title
	 */
	public void removeByTitle(String title);

	/**
	 * Returns the number of shops where title LIKE &#63;.
	 *
	 * @param title the title
	 * @return the number of matching shops
	 */
	public int countByTitle(String title);

	/**
	 * Caches the shop in the entity cache if it is enabled.
	 *
	 * @param shop the shop
	 */
	public void cacheResult(Shop shop);

	/**
	 * Caches the shops in the entity cache if it is enabled.
	 *
	 * @param shops the shops
	 */
	public void cacheResult(java.util.List<Shop> shops);

	/**
	 * Creates a new shop with the primary key. Does not add the shop to the database.
	 *
	 * @param shopId the primary key for the new shop
	 * @return the new shop
	 */
	public Shop create(long shopId);

	/**
	 * Removes the shop with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop that was removed
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public Shop remove(long shopId) throws NoSuchShopException;

	public Shop updateImpl(Shop shop);

	/**
	 * Returns the shop with the primary key or throws a <code>NoSuchShopException</code> if it could not be found.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop
	 * @throws NoSuchShopException if a shop with the primary key could not be found
	 */
	public Shop findByPrimaryKey(long shopId) throws NoSuchShopException;

	/**
	 * Returns the shop with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param shopId the primary key of the shop
	 * @return the shop, or <code>null</code> if a shop with the primary key could not be found
	 */
	public Shop fetchByPrimaryKey(long shopId);

	/**
	 * Returns all the shops.
	 *
	 * @return the shops
	 */
	public java.util.List<Shop> findAll();

	/**
	 * Returns a range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @return the range of shops
	 */
	public java.util.List<Shop> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of shops
	 */
	public java.util.List<Shop> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator);

	/**
	 * Returns an ordered range of all the shops.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>ShopModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of shops
	 * @param end the upper bound of the range of shops (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of shops
	 */
	public java.util.List<Shop> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Shop>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the shops from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of shops.
	 *
	 * @return the number of shops
	 */
	public int countAll();

}