/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link shops.service.http.ShopServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class ShopSoap implements Serializable {

	public static ShopSoap toSoapModel(Shop model) {
		ShopSoap soapModel = new ShopSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setShopId(model.getShopId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCategoryId(model.getCategoryId());
		soapModel.setTitle(model.getTitle());
		soapModel.setImage(model.getImage());
		soapModel.setOverlayImage(model.getOverlayImage());
		soapModel.setOpenTimings(model.getOpenTimings());
		soapModel.setDeliveryTimings(model.getDeliveryTimings());
		soapModel.setCall(model.getCall());
		soapModel.setWebsite(model.getWebsite());
		soapModel.setButtonLabel(model.getButtonLabel());
		soapModel.setButtonLink(model.getButtonLink());

		return soapModel;
	}

	public static ShopSoap[] toSoapModels(Shop[] models) {
		ShopSoap[] soapModels = new ShopSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ShopSoap[][] toSoapModels(Shop[][] models) {
		ShopSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ShopSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ShopSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ShopSoap[] toSoapModels(List<Shop> models) {
		List<ShopSoap> soapModels = new ArrayList<ShopSoap>(models.size());

		for (Shop model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ShopSoap[soapModels.size()]);
	}

	public ShopSoap() {
	}

	public long getPrimaryKey() {
		return _shopId;
	}

	public void setPrimaryKey(long pk) {
		setShopId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getShopId() {
		return _shopId;
	}

	public void setShopId(long shopId) {
		_shopId = shopId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCategoryId() {
		return _categoryId;
	}

	public void setCategoryId(long categoryId) {
		_categoryId = categoryId;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getImage() {
		return _image;
	}

	public void setImage(String image) {
		_image = image;
	}

	public String getOverlayImage() {
		return _overlayImage;
	}

	public void setOverlayImage(String overlayImage) {
		_overlayImage = overlayImage;
	}

	public String getOpenTimings() {
		return _openTimings;
	}

	public void setOpenTimings(String openTimings) {
		_openTimings = openTimings;
	}

	public String getDeliveryTimings() {
		return _deliveryTimings;
	}

	public void setDeliveryTimings(String deliveryTimings) {
		_deliveryTimings = deliveryTimings;
	}

	public String getCall() {
		return _call;
	}

	public void setCall(String call) {
		_call = call;
	}

	public String getWebsite() {
		return _website;
	}

	public void setWebsite(String website) {
		_website = website;
	}

	public String getButtonLabel() {
		return _buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		_buttonLabel = buttonLabel;
	}

	public String getButtonLink() {
		return _buttonLink;
	}

	public void setButtonLink(String buttonLink) {
		_buttonLink = buttonLink;
	}

	private String _uuid;
	private long _shopId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _categoryId;
	private String _title;
	private String _image;
	private String _overlayImage;
	private String _openTimings;
	private String _deliveryTimings;
	private String _call;
	private String _website;
	private String _buttonLabel;
	private String _buttonLink;

}