/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Shop}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Shop
 * @generated
 */
public class ShopWrapper
	extends BaseModelWrapper<Shop> implements ModelWrapper<Shop>, Shop {

	public ShopWrapper(Shop shop) {
		super(shop);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("shopId", getShopId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("categoryId", getCategoryId());
		attributes.put("title", getTitle());
		attributes.put("image", getImage());
		attributes.put("overlayImage", getOverlayImage());
		attributes.put("openTimings", getOpenTimings());
		attributes.put("deliveryTimings", getDeliveryTimings());
		attributes.put("call", getCall());
		attributes.put("website", getWebsite());
		attributes.put("buttonLabel", getButtonLabel());
		attributes.put("buttonLink", getButtonLink());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long shopId = (Long)attributes.get("shopId");

		if (shopId != null) {
			setShopId(shopId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long categoryId = (Long)attributes.get("categoryId");

		if (categoryId != null) {
			setCategoryId(categoryId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String image = (String)attributes.get("image");

		if (image != null) {
			setImage(image);
		}

		String overlayImage = (String)attributes.get("overlayImage");

		if (overlayImage != null) {
			setOverlayImage(overlayImage);
		}

		String openTimings = (String)attributes.get("openTimings");

		if (openTimings != null) {
			setOpenTimings(openTimings);
		}

		String deliveryTimings = (String)attributes.get("deliveryTimings");

		if (deliveryTimings != null) {
			setDeliveryTimings(deliveryTimings);
		}

		String call = (String)attributes.get("call");

		if (call != null) {
			setCall(call);
		}

		String website = (String)attributes.get("website");

		if (website != null) {
			setWebsite(website);
		}

		String buttonLabel = (String)attributes.get("buttonLabel");

		if (buttonLabel != null) {
			setButtonLabel(buttonLabel);
		}

		String buttonLink = (String)attributes.get("buttonLink");

		if (buttonLink != null) {
			setButtonLink(buttonLink);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the button label of this shop.
	 *
	 * @return the button label of this shop
	 */
	@Override
	public String getButtonLabel() {
		return model.getButtonLabel();
	}

	/**
	 * Returns the localized button label of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button label of this shop
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale) {
		return model.getButtonLabel(locale);
	}

	/**
	 * Returns the localized button label of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale, boolean useDefault) {
		return model.getButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized button label of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button label of this shop
	 */
	@Override
	public String getButtonLabel(String languageId) {
		return model.getButtonLabel(languageId);
	}

	/**
	 * Returns the localized button label of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this shop
	 */
	@Override
	public String getButtonLabel(String languageId, boolean useDefault) {
		return model.getButtonLabel(languageId, useDefault);
	}

	@Override
	public String getButtonLabelCurrentLanguageId() {
		return model.getButtonLabelCurrentLanguageId();
	}

	@Override
	public String getButtonLabelCurrentValue() {
		return model.getButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized button labels of this shop.
	 *
	 * @return the locales and localized button labels of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getButtonLabelMap() {
		return model.getButtonLabelMap();
	}

	/**
	 * Returns the button link of this shop.
	 *
	 * @return the button link of this shop
	 */
	@Override
	public String getButtonLink() {
		return model.getButtonLink();
	}

	/**
	 * Returns the localized button link of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button link of this shop
	 */
	@Override
	public String getButtonLink(java.util.Locale locale) {
		return model.getButtonLink(locale);
	}

	/**
	 * Returns the localized button link of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getButtonLink(java.util.Locale locale, boolean useDefault) {
		return model.getButtonLink(locale, useDefault);
	}

	/**
	 * Returns the localized button link of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button link of this shop
	 */
	@Override
	public String getButtonLink(String languageId) {
		return model.getButtonLink(languageId);
	}

	/**
	 * Returns the localized button link of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this shop
	 */
	@Override
	public String getButtonLink(String languageId, boolean useDefault) {
		return model.getButtonLink(languageId, useDefault);
	}

	@Override
	public String getButtonLinkCurrentLanguageId() {
		return model.getButtonLinkCurrentLanguageId();
	}

	@Override
	public String getButtonLinkCurrentValue() {
		return model.getButtonLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized button links of this shop.
	 *
	 * @return the locales and localized button links of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getButtonLinkMap() {
		return model.getButtonLinkMap();
	}

	/**
	 * Returns the call of this shop.
	 *
	 * @return the call of this shop
	 */
	@Override
	public String getCall() {
		return model.getCall();
	}

	/**
	 * Returns the localized call of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized call of this shop
	 */
	@Override
	public String getCall(java.util.Locale locale) {
		return model.getCall(locale);
	}

	/**
	 * Returns the localized call of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized call of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getCall(java.util.Locale locale, boolean useDefault) {
		return model.getCall(locale, useDefault);
	}

	/**
	 * Returns the localized call of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized call of this shop
	 */
	@Override
	public String getCall(String languageId) {
		return model.getCall(languageId);
	}

	/**
	 * Returns the localized call of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized call of this shop
	 */
	@Override
	public String getCall(String languageId, boolean useDefault) {
		return model.getCall(languageId, useDefault);
	}

	@Override
	public String getCallCurrentLanguageId() {
		return model.getCallCurrentLanguageId();
	}

	@Override
	public String getCallCurrentValue() {
		return model.getCallCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized calls of this shop.
	 *
	 * @return the locales and localized calls of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getCallMap() {
		return model.getCallMap();
	}

	/**
	 * Returns the category ID of this shop.
	 *
	 * @return the category ID of this shop
	 */
	@Override
	public long getCategoryId() {
		return model.getCategoryId();
	}

	/**
	 * Returns the company ID of this shop.
	 *
	 * @return the company ID of this shop
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this shop.
	 *
	 * @return the create date of this shop
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the delivery timings of this shop.
	 *
	 * @return the delivery timings of this shop
	 */
	@Override
	public String getDeliveryTimings() {
		return model.getDeliveryTimings();
	}

	/**
	 * Returns the localized delivery timings of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized delivery timings of this shop
	 */
	@Override
	public String getDeliveryTimings(java.util.Locale locale) {
		return model.getDeliveryTimings(locale);
	}

	/**
	 * Returns the localized delivery timings of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized delivery timings of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getDeliveryTimings(
		java.util.Locale locale, boolean useDefault) {

		return model.getDeliveryTimings(locale, useDefault);
	}

	/**
	 * Returns the localized delivery timings of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized delivery timings of this shop
	 */
	@Override
	public String getDeliveryTimings(String languageId) {
		return model.getDeliveryTimings(languageId);
	}

	/**
	 * Returns the localized delivery timings of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized delivery timings of this shop
	 */
	@Override
	public String getDeliveryTimings(String languageId, boolean useDefault) {
		return model.getDeliveryTimings(languageId, useDefault);
	}

	@Override
	public String getDeliveryTimingsCurrentLanguageId() {
		return model.getDeliveryTimingsCurrentLanguageId();
	}

	@Override
	public String getDeliveryTimingsCurrentValue() {
		return model.getDeliveryTimingsCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized delivery timingses of this shop.
	 *
	 * @return the locales and localized delivery timingses of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getDeliveryTimingsMap() {
		return model.getDeliveryTimingsMap();
	}

	/**
	 * Returns the group ID of this shop.
	 *
	 * @return the group ID of this shop
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the image of this shop.
	 *
	 * @return the image of this shop
	 */
	@Override
	public String getImage() {
		return model.getImage();
	}

	/**
	 * Returns the localized image of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized image of this shop
	 */
	@Override
	public String getImage(java.util.Locale locale) {
		return model.getImage(locale);
	}

	/**
	 * Returns the localized image of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getImage(java.util.Locale locale, boolean useDefault) {
		return model.getImage(locale, useDefault);
	}

	/**
	 * Returns the localized image of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized image of this shop
	 */
	@Override
	public String getImage(String languageId) {
		return model.getImage(languageId);
	}

	/**
	 * Returns the localized image of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized image of this shop
	 */
	@Override
	public String getImage(String languageId, boolean useDefault) {
		return model.getImage(languageId, useDefault);
	}

	@Override
	public String getImageCurrentLanguageId() {
		return model.getImageCurrentLanguageId();
	}

	@Override
	public String getImageCurrentValue() {
		return model.getImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized images of this shop.
	 *
	 * @return the locales and localized images of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getImageMap() {
		return model.getImageMap();
	}

	/**
	 * Returns the modified date of this shop.
	 *
	 * @return the modified date of this shop
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the open timings of this shop.
	 *
	 * @return the open timings of this shop
	 */
	@Override
	public String getOpenTimings() {
		return model.getOpenTimings();
	}

	/**
	 * Returns the localized open timings of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized open timings of this shop
	 */
	@Override
	public String getOpenTimings(java.util.Locale locale) {
		return model.getOpenTimings(locale);
	}

	/**
	 * Returns the localized open timings of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized open timings of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getOpenTimings(java.util.Locale locale, boolean useDefault) {
		return model.getOpenTimings(locale, useDefault);
	}

	/**
	 * Returns the localized open timings of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized open timings of this shop
	 */
	@Override
	public String getOpenTimings(String languageId) {
		return model.getOpenTimings(languageId);
	}

	/**
	 * Returns the localized open timings of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized open timings of this shop
	 */
	@Override
	public String getOpenTimings(String languageId, boolean useDefault) {
		return model.getOpenTimings(languageId, useDefault);
	}

	@Override
	public String getOpenTimingsCurrentLanguageId() {
		return model.getOpenTimingsCurrentLanguageId();
	}

	@Override
	public String getOpenTimingsCurrentValue() {
		return model.getOpenTimingsCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized open timingses of this shop.
	 *
	 * @return the locales and localized open timingses of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getOpenTimingsMap() {
		return model.getOpenTimingsMap();
	}

	/**
	 * Returns the overlay image of this shop.
	 *
	 * @return the overlay image of this shop
	 */
	@Override
	public String getOverlayImage() {
		return model.getOverlayImage();
	}

	/**
	 * Returns the localized overlay image of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized overlay image of this shop
	 */
	@Override
	public String getOverlayImage(java.util.Locale locale) {
		return model.getOverlayImage(locale);
	}

	/**
	 * Returns the localized overlay image of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized overlay image of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getOverlayImage(java.util.Locale locale, boolean useDefault) {
		return model.getOverlayImage(locale, useDefault);
	}

	/**
	 * Returns the localized overlay image of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized overlay image of this shop
	 */
	@Override
	public String getOverlayImage(String languageId) {
		return model.getOverlayImage(languageId);
	}

	/**
	 * Returns the localized overlay image of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized overlay image of this shop
	 */
	@Override
	public String getOverlayImage(String languageId, boolean useDefault) {
		return model.getOverlayImage(languageId, useDefault);
	}

	@Override
	public String getOverlayImageCurrentLanguageId() {
		return model.getOverlayImageCurrentLanguageId();
	}

	@Override
	public String getOverlayImageCurrentValue() {
		return model.getOverlayImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized overlay images of this shop.
	 *
	 * @return the locales and localized overlay images of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getOverlayImageMap() {
		return model.getOverlayImageMap();
	}

	/**
	 * Returns the primary key of this shop.
	 *
	 * @return the primary key of this shop
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the shop ID of this shop.
	 *
	 * @return the shop ID of this shop
	 */
	@Override
	public long getShopId() {
		return model.getShopId();
	}

	/**
	 * Returns the title of this shop.
	 *
	 * @return the title of this shop
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this shop
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this shop
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this shop
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this shop.
	 *
	 * @return the locales and localized titles of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the user ID of this shop.
	 *
	 * @return the user ID of this shop
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this shop.
	 *
	 * @return the user name of this shop
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this shop.
	 *
	 * @return the user uuid of this shop
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this shop.
	 *
	 * @return the uuid of this shop
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the website of this shop.
	 *
	 * @return the website of this shop
	 */
	@Override
	public String getWebsite() {
		return model.getWebsite();
	}

	/**
	 * Returns the localized website of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized website of this shop
	 */
	@Override
	public String getWebsite(java.util.Locale locale) {
		return model.getWebsite(locale);
	}

	/**
	 * Returns the localized website of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized website of this shop. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getWebsite(java.util.Locale locale, boolean useDefault) {
		return model.getWebsite(locale, useDefault);
	}

	/**
	 * Returns the localized website of this shop in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized website of this shop
	 */
	@Override
	public String getWebsite(String languageId) {
		return model.getWebsite(languageId);
	}

	/**
	 * Returns the localized website of this shop in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized website of this shop
	 */
	@Override
	public String getWebsite(String languageId, boolean useDefault) {
		return model.getWebsite(languageId, useDefault);
	}

	@Override
	public String getWebsiteCurrentLanguageId() {
		return model.getWebsiteCurrentLanguageId();
	}

	@Override
	public String getWebsiteCurrentValue() {
		return model.getWebsiteCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized websites of this shop.
	 *
	 * @return the locales and localized websites of this shop
	 */
	@Override
	public Map<java.util.Locale, String> getWebsiteMap() {
		return model.getWebsiteMap();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the button label of this shop.
	 *
	 * @param buttonLabel the button label of this shop
	 */
	@Override
	public void setButtonLabel(String buttonLabel) {
		model.setButtonLabel(buttonLabel);
	}

	/**
	 * Sets the localized button label of this shop in the language.
	 *
	 * @param buttonLabel the localized button label of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setButtonLabel(String buttonLabel, java.util.Locale locale) {
		model.setButtonLabel(buttonLabel, locale);
	}

	/**
	 * Sets the localized button label of this shop in the language, and sets the default locale.
	 *
	 * @param buttonLabel the localized button label of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabel(
		String buttonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setButtonLabel(buttonLabel, locale, defaultLocale);
	}

	@Override
	public void setButtonLabelCurrentLanguageId(String languageId) {
		model.setButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized button labels of this shop from the map of locales and localized button labels.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this shop
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap) {

		model.setButtonLabelMap(buttonLabelMap);
	}

	/**
	 * Sets the localized button labels of this shop from the map of locales and localized button labels, and sets the default locale.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap,
		java.util.Locale defaultLocale) {

		model.setButtonLabelMap(buttonLabelMap, defaultLocale);
	}

	/**
	 * Sets the button link of this shop.
	 *
	 * @param buttonLink the button link of this shop
	 */
	@Override
	public void setButtonLink(String buttonLink) {
		model.setButtonLink(buttonLink);
	}

	/**
	 * Sets the localized button link of this shop in the language.
	 *
	 * @param buttonLink the localized button link of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setButtonLink(String buttonLink, java.util.Locale locale) {
		model.setButtonLink(buttonLink, locale);
	}

	/**
	 * Sets the localized button link of this shop in the language, and sets the default locale.
	 *
	 * @param buttonLink the localized button link of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLink(
		String buttonLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setButtonLink(buttonLink, locale, defaultLocale);
	}

	@Override
	public void setButtonLinkCurrentLanguageId(String languageId) {
		model.setButtonLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized button links of this shop from the map of locales and localized button links.
	 *
	 * @param buttonLinkMap the locales and localized button links of this shop
	 */
	@Override
	public void setButtonLinkMap(Map<java.util.Locale, String> buttonLinkMap) {
		model.setButtonLinkMap(buttonLinkMap);
	}

	/**
	 * Sets the localized button links of this shop from the map of locales and localized button links, and sets the default locale.
	 *
	 * @param buttonLinkMap the locales and localized button links of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLinkMap(
		Map<java.util.Locale, String> buttonLinkMap,
		java.util.Locale defaultLocale) {

		model.setButtonLinkMap(buttonLinkMap, defaultLocale);
	}

	/**
	 * Sets the call of this shop.
	 *
	 * @param call the call of this shop
	 */
	@Override
	public void setCall(String call) {
		model.setCall(call);
	}

	/**
	 * Sets the localized call of this shop in the language.
	 *
	 * @param call the localized call of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setCall(String call, java.util.Locale locale) {
		model.setCall(call, locale);
	}

	/**
	 * Sets the localized call of this shop in the language, and sets the default locale.
	 *
	 * @param call the localized call of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setCall(
		String call, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setCall(call, locale, defaultLocale);
	}

	@Override
	public void setCallCurrentLanguageId(String languageId) {
		model.setCallCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized calls of this shop from the map of locales and localized calls.
	 *
	 * @param callMap the locales and localized calls of this shop
	 */
	@Override
	public void setCallMap(Map<java.util.Locale, String> callMap) {
		model.setCallMap(callMap);
	}

	/**
	 * Sets the localized calls of this shop from the map of locales and localized calls, and sets the default locale.
	 *
	 * @param callMap the locales and localized calls of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setCallMap(
		Map<java.util.Locale, String> callMap, java.util.Locale defaultLocale) {

		model.setCallMap(callMap, defaultLocale);
	}

	/**
	 * Sets the category ID of this shop.
	 *
	 * @param categoryId the category ID of this shop
	 */
	@Override
	public void setCategoryId(long categoryId) {
		model.setCategoryId(categoryId);
	}

	/**
	 * Sets the company ID of this shop.
	 *
	 * @param companyId the company ID of this shop
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this shop.
	 *
	 * @param createDate the create date of this shop
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the delivery timings of this shop.
	 *
	 * @param deliveryTimings the delivery timings of this shop
	 */
	@Override
	public void setDeliveryTimings(String deliveryTimings) {
		model.setDeliveryTimings(deliveryTimings);
	}

	/**
	 * Sets the localized delivery timings of this shop in the language.
	 *
	 * @param deliveryTimings the localized delivery timings of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setDeliveryTimings(
		String deliveryTimings, java.util.Locale locale) {

		model.setDeliveryTimings(deliveryTimings, locale);
	}

	/**
	 * Sets the localized delivery timings of this shop in the language, and sets the default locale.
	 *
	 * @param deliveryTimings the localized delivery timings of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDeliveryTimings(
		String deliveryTimings, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setDeliveryTimings(deliveryTimings, locale, defaultLocale);
	}

	@Override
	public void setDeliveryTimingsCurrentLanguageId(String languageId) {
		model.setDeliveryTimingsCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized delivery timingses of this shop from the map of locales and localized delivery timingses.
	 *
	 * @param deliveryTimingsMap the locales and localized delivery timingses of this shop
	 */
	@Override
	public void setDeliveryTimingsMap(
		Map<java.util.Locale, String> deliveryTimingsMap) {

		model.setDeliveryTimingsMap(deliveryTimingsMap);
	}

	/**
	 * Sets the localized delivery timingses of this shop from the map of locales and localized delivery timingses, and sets the default locale.
	 *
	 * @param deliveryTimingsMap the locales and localized delivery timingses of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setDeliveryTimingsMap(
		Map<java.util.Locale, String> deliveryTimingsMap,
		java.util.Locale defaultLocale) {

		model.setDeliveryTimingsMap(deliveryTimingsMap, defaultLocale);
	}

	/**
	 * Sets the group ID of this shop.
	 *
	 * @param groupId the group ID of this shop
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the image of this shop.
	 *
	 * @param image the image of this shop
	 */
	@Override
	public void setImage(String image) {
		model.setImage(image);
	}

	/**
	 * Sets the localized image of this shop in the language.
	 *
	 * @param image the localized image of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setImage(String image, java.util.Locale locale) {
		model.setImage(image, locale);
	}

	/**
	 * Sets the localized image of this shop in the language, and sets the default locale.
	 *
	 * @param image the localized image of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImage(
		String image, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setImage(image, locale, defaultLocale);
	}

	@Override
	public void setImageCurrentLanguageId(String languageId) {
		model.setImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized images of this shop from the map of locales and localized images.
	 *
	 * @param imageMap the locales and localized images of this shop
	 */
	@Override
	public void setImageMap(Map<java.util.Locale, String> imageMap) {
		model.setImageMap(imageMap);
	}

	/**
	 * Sets the localized images of this shop from the map of locales and localized images, and sets the default locale.
	 *
	 * @param imageMap the locales and localized images of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setImageMap(
		Map<java.util.Locale, String> imageMap,
		java.util.Locale defaultLocale) {

		model.setImageMap(imageMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this shop.
	 *
	 * @param modifiedDate the modified date of this shop
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the open timings of this shop.
	 *
	 * @param openTimings the open timings of this shop
	 */
	@Override
	public void setOpenTimings(String openTimings) {
		model.setOpenTimings(openTimings);
	}

	/**
	 * Sets the localized open timings of this shop in the language.
	 *
	 * @param openTimings the localized open timings of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setOpenTimings(String openTimings, java.util.Locale locale) {
		model.setOpenTimings(openTimings, locale);
	}

	/**
	 * Sets the localized open timings of this shop in the language, and sets the default locale.
	 *
	 * @param openTimings the localized open timings of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOpenTimings(
		String openTimings, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setOpenTimings(openTimings, locale, defaultLocale);
	}

	@Override
	public void setOpenTimingsCurrentLanguageId(String languageId) {
		model.setOpenTimingsCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized open timingses of this shop from the map of locales and localized open timingses.
	 *
	 * @param openTimingsMap the locales and localized open timingses of this shop
	 */
	@Override
	public void setOpenTimingsMap(
		Map<java.util.Locale, String> openTimingsMap) {

		model.setOpenTimingsMap(openTimingsMap);
	}

	/**
	 * Sets the localized open timingses of this shop from the map of locales and localized open timingses, and sets the default locale.
	 *
	 * @param openTimingsMap the locales and localized open timingses of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOpenTimingsMap(
		Map<java.util.Locale, String> openTimingsMap,
		java.util.Locale defaultLocale) {

		model.setOpenTimingsMap(openTimingsMap, defaultLocale);
	}

	/**
	 * Sets the overlay image of this shop.
	 *
	 * @param overlayImage the overlay image of this shop
	 */
	@Override
	public void setOverlayImage(String overlayImage) {
		model.setOverlayImage(overlayImage);
	}

	/**
	 * Sets the localized overlay image of this shop in the language.
	 *
	 * @param overlayImage the localized overlay image of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setOverlayImage(String overlayImage, java.util.Locale locale) {
		model.setOverlayImage(overlayImage, locale);
	}

	/**
	 * Sets the localized overlay image of this shop in the language, and sets the default locale.
	 *
	 * @param overlayImage the localized overlay image of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOverlayImage(
		String overlayImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setOverlayImage(overlayImage, locale, defaultLocale);
	}

	@Override
	public void setOverlayImageCurrentLanguageId(String languageId) {
		model.setOverlayImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized overlay images of this shop from the map of locales and localized overlay images.
	 *
	 * @param overlayImageMap the locales and localized overlay images of this shop
	 */
	@Override
	public void setOverlayImageMap(
		Map<java.util.Locale, String> overlayImageMap) {

		model.setOverlayImageMap(overlayImageMap);
	}

	/**
	 * Sets the localized overlay images of this shop from the map of locales and localized overlay images, and sets the default locale.
	 *
	 * @param overlayImageMap the locales and localized overlay images of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setOverlayImageMap(
		Map<java.util.Locale, String> overlayImageMap,
		java.util.Locale defaultLocale) {

		model.setOverlayImageMap(overlayImageMap, defaultLocale);
	}

	/**
	 * Sets the primary key of this shop.
	 *
	 * @param primaryKey the primary key of this shop
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the shop ID of this shop.
	 *
	 * @param shopId the shop ID of this shop
	 */
	@Override
	public void setShopId(long shopId) {
		model.setShopId(shopId);
	}

	/**
	 * Sets the title of this shop.
	 *
	 * @param title the title of this shop
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this shop in the language.
	 *
	 * @param title the localized title of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this shop in the language, and sets the default locale.
	 *
	 * @param title the localized title of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this shop from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this shop
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this shop from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this shop.
	 *
	 * @param userId the user ID of this shop
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this shop.
	 *
	 * @param userName the user name of this shop
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this shop.
	 *
	 * @param userUuid the user uuid of this shop
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this shop.
	 *
	 * @param uuid the uuid of this shop
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the website of this shop.
	 *
	 * @param website the website of this shop
	 */
	@Override
	public void setWebsite(String website) {
		model.setWebsite(website);
	}

	/**
	 * Sets the localized website of this shop in the language.
	 *
	 * @param website the localized website of this shop
	 * @param locale the locale of the language
	 */
	@Override
	public void setWebsite(String website, java.util.Locale locale) {
		model.setWebsite(website, locale);
	}

	/**
	 * Sets the localized website of this shop in the language, and sets the default locale.
	 *
	 * @param website the localized website of this shop
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWebsite(
		String website, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setWebsite(website, locale, defaultLocale);
	}

	@Override
	public void setWebsiteCurrentLanguageId(String languageId) {
		model.setWebsiteCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized websites of this shop from the map of locales and localized websites.
	 *
	 * @param websiteMap the locales and localized websites of this shop
	 */
	@Override
	public void setWebsiteMap(Map<java.util.Locale, String> websiteMap) {
		model.setWebsiteMap(websiteMap);
	}

	/**
	 * Sets the localized websites of this shop from the map of locales and localized websites, and sets the default locale.
	 *
	 * @param websiteMap the locales and localized websites of this shop
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setWebsiteMap(
		Map<java.util.Locale, String> websiteMap,
		java.util.Locale defaultLocale) {

		model.setWebsiteMap(websiteMap, defaultLocale);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected ShopWrapper wrap(Shop shop) {
		return new ShopWrapper(shop);
	}

}