package shops.admin.web.application.list;

import shops.admin.web.constants.ShopsAdminWebPanelCategoryKeys;
import shops.admin.web.constants.ShopsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + ShopsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class ShopsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return ShopsAdminWebPortletKeys.SHOPSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + ShopsAdminWebPortletKeys.SHOPSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}