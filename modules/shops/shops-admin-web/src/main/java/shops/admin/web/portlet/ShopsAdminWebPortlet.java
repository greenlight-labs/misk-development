package shops.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import shops.admin.web.constants.ShopsAdminWebPortletKeys;
import shops.model.Shop;
import shops.service.ShopLocalService;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=ShopsAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ShopsAdminWebPortletKeys.SHOPSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class ShopsAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addShop(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Shop.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long categoryId = ParamUtil.getLong(request, "categoryId");
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
		Map<Locale, String> imageMap = LocalizationUtil.getLocalizationMap(request, "image");
		Map<Locale, String> overlayImageMap = LocalizationUtil.getLocalizationMap(request, "overlayImage");
		Map<Locale, String> openTimingsMap = LocalizationUtil.getLocalizationMap(request, "openTimings");
		Map<Locale, String> deliveryTimingsMap = LocalizationUtil.getLocalizationMap(request, "deliveryTimings");
		Map<Locale, String> callMap = LocalizationUtil.getLocalizationMap(request, "call");
		Map<Locale, String> websiteMap = LocalizationUtil.getLocalizationMap(request, "website");
		Map<Locale, String> buttonLabelMap = LocalizationUtil.getLocalizationMap(request, "buttonLabel");
		Map<Locale, String> buttonLinkMap = LocalizationUtil.getLocalizationMap(request, "buttonLink");

		try {
			_shopLocalService.addShop(serviceContext.getUserId(),
					categoryId, titleMap, imageMap, overlayImageMap, openTimingsMap,
					deliveryTimingsMap, callMap, websiteMap, buttonLabelMap, buttonLinkMap,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(ShopsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_shop.jsp");
		}
	}

	public void updateShop(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Shop.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long shopId = ParamUtil.getLong(request, "shopId");

		long categoryId = ParamUtil.getLong(request, "categoryId");
		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(request, "title");
		Map<Locale, String> imageMap = LocalizationUtil.getLocalizationMap(request, "image");
		Map<Locale, String> overlayImageMap = LocalizationUtil.getLocalizationMap(request, "overlayImage");
		Map<Locale, String> openTimingsMap = LocalizationUtil.getLocalizationMap(request, "openTimings");
		Map<Locale, String> deliveryTimingsMap = LocalizationUtil.getLocalizationMap(request, "deliveryTimings");
		Map<Locale, String> callMap = LocalizationUtil.getLocalizationMap(request, "call");
		Map<Locale, String> websiteMap = LocalizationUtil.getLocalizationMap(request, "website");
		Map<Locale, String> buttonLabelMap = LocalizationUtil.getLocalizationMap(request, "buttonLabel");
		Map<Locale, String> buttonLinkMap = LocalizationUtil.getLocalizationMap(request, "buttonLink");

		try {
			_shopLocalService.updateShop(serviceContext.getUserId(), shopId,
					categoryId, titleMap, imageMap, overlayImageMap, openTimingsMap,
					deliveryTimingsMap, callMap, websiteMap, buttonLabelMap, buttonLinkMap,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(ShopsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_shop.jsp");
		}
	}

	public void deleteShop(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Shop.class.getName(), request);

		long shopId = ParamUtil.getLong(request, "shopId");

		try {
			_shopLocalService.deleteShop(shopId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(ShopsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private ShopLocalService _shopLocalService;

	@Reference
	private ItemSelector _itemSelector;
}