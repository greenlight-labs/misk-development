package shops.admin.web.constants;

/**
 * @author tz
 */
public class ShopsAdminWebPortletKeys {

	public static final String SHOPSADMINWEB =
		"shops_admin_web_ShopsAdminWebPortlet";

	public static final String CATEGORIESADMINWEB =
			"shops_admin_web_CategoriesAdminWebPortlet";

}