<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="shops-admin-buttons">
        <portlet:renderURL var="addShopURL">
            <portlet:param name="mvcPath"
                           value="/edit_shop.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addShopURL.toString() %>"
                    value="Add Shop"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= ShopLocalServiceUtil.getShopsCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= ShopLocalServiceUtil.getShops(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="shops.model.Shop" modelVar="shop">

            <liferay-ui:search-container-column-text
                    name="Title"
                    value="<%= HtmlUtil.escape(shop.getTitle(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>