<%@ include file="/init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="categories-admin-buttons">
        <portlet:renderURL var="addCategoryURL">
            <portlet:param name="mvcPath"
                           value="/category/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addCategoryURL.toString() %>"
                    value="Add Category"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= CategoryLocalServiceUtil.getCategoriesCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= CategoryLocalServiceUtil.getCategories(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="shops.model.Category" modelVar="category">

            <liferay-ui:search-container-column-text
                    name="name"
                    value="<%= HtmlUtil.escape(category.getName(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/category/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>