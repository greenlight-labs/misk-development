<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="java.util.List" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.portal.kernel.model.PersistedModel" %>
<%@ page import="com.liferay.portal.kernel.dao.search.SearchContainer" %>
<%@ page import="com.liferay.taglib.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>

<%@ page import="shops.model.Category" %>
<%@ page import="shops.service.CategoryLocalServiceUtil" %>
<%@ page import="shops.service.CategoryServiceUtil" %>
<%@ page import="shops.service.ShopLocalServiceUtil" %>
<%@ page import="shops.model.Shop" %>
<%@ page import="shops.exception.ShopCategoryIdException" %>
<%@ page import="shops.exception.ShopImageException" %>
<%@ page import="shops.exception.ShopTitleException" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />