<%@include file = "/init.jsp" %>

<%
    long categoryId = ParamUtil.getLong(request, "categoryId");

    Category category = null;

    if (categoryId > 0) {
        try {
            category = CategoryLocalServiceUtil.getCategory(categoryId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= category == null ? "addCategory" : "updateCategory" %>' var="editCategoryURL" />

<div class="container-fluid-1280">

    <aui:form action="<%= editCategoryURL %>" name="fm">

        <aui:model-context bean="<%= category %>" model="<%= Category.class %>" />

        <aui:input type="hidden" name="categoryId"
                   value='<%= category == null ? "" : category.getCategoryId() %>' />

        <aui:fieldset-group markupView="lexicon">
            <aui:fieldset>
                <aui:input name="name" label="Name" autoSize="true" helpMessage="Max 15 Characters (Recommended)">
                    <aui:validator name="required"/>
                </aui:input>
                <aui:input name="svgIcon" label="SVG Icon" helpMessage="Image Dimensions: 42 x 36 pixels">
                    <aui:validator name="required"/>
                    <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                        function(val) {
                            var ext = val.substring(val.lastIndexOf('.') + 1);
                            return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                        }
                    </aui:validator>
                </aui:input>
                <div class="button-holder">
                    <button class="btn select-button btn-secondary" type="button" data-field-id="svgIcon">
                        <span class="lfr-btn-label">Select</span>
                    </button>
                </div>
            </aui:fieldset>
        </aui:fieldset-group>

        <aui:button-row>
            <aui:button type="submit" />
            <aui:button onClick="<%= viewURL %>" type="cancel"  />
        </aui:button-row>
    </aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>