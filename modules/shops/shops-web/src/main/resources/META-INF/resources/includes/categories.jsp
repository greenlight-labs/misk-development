<portlet:renderURL var="categoryPageURL" />

<div class="col-12 col-md-3">
    <ul class="dine-tab" data-scroll data-scroll-sticky data-scroll-target="#fixed-target">
        <li class="<%= (Validator.isNull(categoryId) ? "dine-tab-active": "") %>">
            <a href="<%= categoryPageURL.toString() %>">
                <span class="dine-tab-icon">
                    <img src="/assets/svgs/dine-tab-icon-4.svg" alt="" class="img-fluid"></span><liferay-ui:message key="misk-all" /><i class="dot-line"></i>
            </a>
        </li>
        <%
            List<Category> categories = CategoryServiceUtil.getCategories(scopeGroupId);

            for (Category curCategory : categories) {
        %>
            <portlet:renderURL var="categoryPageURL">
                <portlet:param name="categoryId" value="<%= String.valueOf(curCategory.getCategoryId()) %>" />
            </portlet:renderURL>
            <li class="<%= (categoryId == curCategory.getCategoryId() ? "dine-tab-active": "") %>">
                <a href="<%= categoryPageURL.toString() %>" data-category-id="<%= curCategory.getCategoryId() %>">
                    <span class="dine-tab-icon">
                    <img src="<%= curCategory.getSvgIcon(locale) %>" alt="" class="img-fluid"></span><%= curCategory.getName(locale) %> <i class="dot-line"></i>
                </a>
            </li>
        <%
            }
        %>
    </ul>
</div>