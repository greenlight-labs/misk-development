<%
    List<Shop> shops;

    if(Validator.isNotNull(query)){
        shops = ShopServiceUtil.findByTitle(query);
    }else if(Validator.isNotNull(categoryId)){
        shops = ShopServiceUtil.getShopsByCategory(categoryId);
    }else{
        shops = ShopServiceUtil.getShops(scopeGroupId);
    }

    if(shops.size() > 0){
        for (Shop curShop : shops) {
%>
<portlet:renderURL var="detailPageURL2">
    <portlet:param name="shopId" value="<%= String.valueOf(curShop.getShopId()) %>" />
    <portlet:param name="mvcPath" value="/detail.jsp" />
</portlet:renderURL>
<div class="dine-card">
    <div class="dine-image">
        <img class="img-fluid" src="<%= curShop.getImage(locale) %>" alt="">
        <a href="javascript:" class="dine-image-overlay">
            <img class="img-fluid" src="<%= curShop.getOverlayImage(locale) %>" alt="">
        </a>
    </div>
    <div class="dine-card-content">
        <h4><%= curShop.getTitle(locale) %></h4>
        <div class="dine-card-data">
            <ul class="dine-card-data-head">
                <li>Open Timings:</li>
                <li>Delivery Timings:</li>
                <li>Call:</li>
                <li>Website:</li>
            </ul>
            <ul class="dine-card-data-text">
                <li><%= curShop.getOpenTimings(locale) %></li>
                <li><%= curShop.getDeliveryTimings(locale) %></li>
                <li><a href="tel:<%= curShop.getCall(locale) %>"><%= curShop.getCall(locale) %></a></li>
                <li><a href="<%= curShop.getWebsite(locale) %>" target="_blank"><%= curShop.getWebsite(locale).replaceFirst("https?://", "") %></a></li>
            </ul>
        </div>
        <a href="<%= curShop.getButtonLink(locale) %>" class="btn btn-outline-primary" target="_blank">
            <span><%= curShop.getButtonLabel(locale) %></span><i class="dot-line"></i>
        </a>
    </div>
</div>
<%
        }
    } else {
%>
<div class="text-center pt-5">
    <h4>No Record Found.</h4>
</div>
<%
    }
%>