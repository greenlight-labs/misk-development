<%@ include file="init.jsp" %>

<%
    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String query = originalRequest.getParameter("q");
%>

<section class="dinein-section" data-scroll-section id="fixed-target">
    <div class="container-fluid p-0">
        <div class="row no-gutters">

            <%@include file="includes/categories.jsp" %>

            <div class="col-12 col-md-8">

                <%@include file="includes/search.jsp" %>

                <div class="dine-right-sec">

                    <%@include file="listing.jsp" %>

                    <div class="text-center">
                        <%--<%@include file="includes/pagination.jsp" %>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
