/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import shops.model.Shop;

/**
 * The cache model class for representing Shop in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ShopCacheModel implements CacheModel<Shop>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ShopCacheModel)) {
			return false;
		}

		ShopCacheModel shopCacheModel = (ShopCacheModel)object;

		if (shopId == shopCacheModel.shopId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, shopId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(37);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", shopId=");
		sb.append(shopId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", categoryId=");
		sb.append(categoryId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", image=");
		sb.append(image);
		sb.append(", overlayImage=");
		sb.append(overlayImage);
		sb.append(", openTimings=");
		sb.append(openTimings);
		sb.append(", deliveryTimings=");
		sb.append(deliveryTimings);
		sb.append(", call=");
		sb.append(call);
		sb.append(", website=");
		sb.append(website);
		sb.append(", buttonLabel=");
		sb.append(buttonLabel);
		sb.append(", buttonLink=");
		sb.append(buttonLink);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Shop toEntityModel() {
		ShopImpl shopImpl = new ShopImpl();

		if (uuid == null) {
			shopImpl.setUuid("");
		}
		else {
			shopImpl.setUuid(uuid);
		}

		shopImpl.setShopId(shopId);
		shopImpl.setGroupId(groupId);
		shopImpl.setCompanyId(companyId);
		shopImpl.setUserId(userId);

		if (userName == null) {
			shopImpl.setUserName("");
		}
		else {
			shopImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			shopImpl.setCreateDate(null);
		}
		else {
			shopImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			shopImpl.setModifiedDate(null);
		}
		else {
			shopImpl.setModifiedDate(new Date(modifiedDate));
		}

		shopImpl.setCategoryId(categoryId);

		if (title == null) {
			shopImpl.setTitle("");
		}
		else {
			shopImpl.setTitle(title);
		}

		if (image == null) {
			shopImpl.setImage("");
		}
		else {
			shopImpl.setImage(image);
		}

		if (overlayImage == null) {
			shopImpl.setOverlayImage("");
		}
		else {
			shopImpl.setOverlayImage(overlayImage);
		}

		if (openTimings == null) {
			shopImpl.setOpenTimings("");
		}
		else {
			shopImpl.setOpenTimings(openTimings);
		}

		if (deliveryTimings == null) {
			shopImpl.setDeliveryTimings("");
		}
		else {
			shopImpl.setDeliveryTimings(deliveryTimings);
		}

		if (call == null) {
			shopImpl.setCall("");
		}
		else {
			shopImpl.setCall(call);
		}

		if (website == null) {
			shopImpl.setWebsite("");
		}
		else {
			shopImpl.setWebsite(website);
		}

		if (buttonLabel == null) {
			shopImpl.setButtonLabel("");
		}
		else {
			shopImpl.setButtonLabel(buttonLabel);
		}

		if (buttonLink == null) {
			shopImpl.setButtonLink("");
		}
		else {
			shopImpl.setButtonLink(buttonLink);
		}

		shopImpl.resetOriginalValues();

		return shopImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		shopId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		categoryId = objectInput.readLong();
		title = objectInput.readUTF();
		image = objectInput.readUTF();
		overlayImage = objectInput.readUTF();
		openTimings = objectInput.readUTF();
		deliveryTimings = objectInput.readUTF();
		call = objectInput.readUTF();
		website = objectInput.readUTF();
		buttonLabel = objectInput.readUTF();
		buttonLink = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(shopId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(categoryId);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (image == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(image);
		}

		if (overlayImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(overlayImage);
		}

		if (openTimings == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(openTimings);
		}

		if (deliveryTimings == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(deliveryTimings);
		}

		if (call == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(call);
		}

		if (website == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(website);
		}

		if (buttonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLabel);
		}

		if (buttonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLink);
		}
	}

	public String uuid;
	public long shopId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long categoryId;
	public String title;
	public String image;
	public String overlayImage;
	public String openTimings;
	public String deliveryTimings;
	public String call;
	public String website;
	public String buttonLabel;
	public String buttonLink;

}