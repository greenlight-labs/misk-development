/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import org.osgi.service.component.annotations.Component;
import shops.model.Shop;
import shops.service.base.ShopServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the shop remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>shops.service.ShopService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ShopServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=shop",
		"json.web.service.context.path=Shop"
	},
	service = AopService.class
)
public class ShopServiceImpl extends ShopServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>shops.service.ShopServiceUtil</code> to access the shop remote service.
	 */

	public List<Shop> getShops(long groupId) {

		return shopPersistence.findByGroupId(groupId);
	}

	public List<Shop> getShops(long groupId, int start, int end,
							   OrderByComparator<Shop> obc) {

		return shopPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Shop> getShops(long groupId, int start, int end) {

		return shopPersistence.findByGroupId(groupId, start, end);
	}

	public int getShopsCount(long groupId) {

		return shopPersistence.countByGroupId(groupId);
	}

	/* *
	 * get shops by type
	 * */
	public List<Shop> getShopsByCategory(long categoryId) {

		return shopPersistence.findByCategoryId(categoryId);
	}

	public List<Shop> getShopsByCategory(long categoryId, int start, int end,
										 OrderByComparator<Shop> obc) {

		return shopPersistence.findByCategoryId(categoryId, start, end, obc);
	}

	public List<Shop> getShopsByCategory(long categoryId, int start, int end) {

		return shopPersistence.findByCategoryId(categoryId, start, end);
	}

	public int getShopsByCategoryCount(long categoryId) {

		return shopPersistence.countByCategoryId(categoryId);
	}

	/* *
	 * find shops by title
	 * */
	public List<Shop> findByTitle(String title) {

		return shopPersistence.findByTitle('%'+title+'%');
	}

	public List<Shop> findByTitle(String title, int start, int end,
								  OrderByComparator<Shop> obc) {

		return shopPersistence.findByTitle('%'+title+'%', start, end, obc);
	}

	public List<Shop> findByTitle(String title, int start, int end) {

		return shopPersistence.findByTitle('%'+title+'%', start, end);
	}

	public int findByTitleCount(String title) {

		return shopPersistence.countByTitle('%'+title+'%');
	}
}