/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package shops.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;
import shops.exception.ShopCategoryIdException;
import shops.exception.ShopImageException;
import shops.exception.ShopTitleException;
import shops.model.Shop;
import shops.service.base.ShopLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the shop local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>shops.service.ShopLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ShopLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=shops.model.Shop", service = AopService.class
)
public class ShopLocalServiceImpl extends ShopLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>shops.service.ShopLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>shops.service.ShopLocalServiceUtil</code>.
	 */

	public Shop addShop(long userId,
						long categoryId, Map<Locale, String> titleMap, Map<Locale, String> imageMap, Map<Locale, String> overlayImageMap, Map<Locale, String> openTimingsMap,
						Map<Locale, String> deliveryTimingsMap, Map<Locale, String> callMap, Map<Locale, String> websiteMap, Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
						ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(categoryId, titleMap, imageMap);

		long shopId = counterLocalService.increment();

		Shop shop = shopPersistence.create(shopId);

		shop.setUuid(serviceContext.getUuid());
		shop.setUserId(userId);
		shop.setGroupId(groupId);
		shop.setCompanyId(user.getCompanyId());
		shop.setUserName(user.getFullName());
		shop.setCreateDate(serviceContext.getCreateDate(now));
		shop.setModifiedDate(serviceContext.getModifiedDate(now));

		shop.setCategoryId(categoryId);
		shop.setTitleMap(titleMap);
		shop.setImageMap(imageMap);
		shop.setOverlayImageMap(overlayImageMap);
		shop.setOpenTimingsMap(openTimingsMap);
		shop.setDeliveryTimingsMap(deliveryTimingsMap);
		shop.setCallMap(callMap);
		shop.setWebsiteMap(websiteMap);
		shop.setButtonLabelMap(buttonLabelMap);
		shop.setButtonLinkMap(buttonLinkMap);

		shop.setExpandoBridgeAttributes(serviceContext);

		shopPersistence.update(shop);
		shopPersistence.clearCache();

		return shop;
	}

	public Shop updateShop(long userId, long shopId,
						   long categoryId, Map<Locale, String> titleMap, Map<Locale, String> imageMap, Map<Locale, String> overlayImageMap, Map<Locale, String> openTimingsMap,
						   Map<Locale, String> deliveryTimingsMap, Map<Locale, String> callMap, Map<Locale, String> websiteMap, Map<Locale, String> buttonLabelMap, Map<Locale, String> buttonLinkMap,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(categoryId, titleMap, imageMap);

		Shop shop = getShop(shopId);

		User user = userLocalService.getUser(userId);

		shop.setUserId(userId);
		shop.setUserName(user.getFullName());
		shop.setModifiedDate(serviceContext.getModifiedDate(now));

		shop.setCategoryId(categoryId);
		shop.setTitleMap(titleMap);
		shop.setImageMap(imageMap);
		shop.setOverlayImageMap(overlayImageMap);
		shop.setOpenTimingsMap(openTimingsMap);
		shop.setDeliveryTimingsMap(deliveryTimingsMap);
		shop.setCallMap(callMap);
		shop.setWebsiteMap(websiteMap);
		shop.setButtonLabelMap(buttonLabelMap);
		shop.setButtonLinkMap(buttonLinkMap);

		shop.setExpandoBridgeAttributes(serviceContext);

		shopPersistence.update(shop);
		shopPersistence.clearCache();

		return shop;
	}

	public Shop deleteShop(long shopId,
						   ServiceContext serviceContext) throws PortalException,
			SystemException {

		Shop shop = getShop(shopId);
		shop = deleteShop(shop);

		return shop;
	}

	public List<Shop> getShops(long groupId) {

		return shopPersistence.findByGroupId(groupId);
	}

	public List<Shop> getShops(long groupId, int start, int end,
							   OrderByComparator<Shop> obc) {

		return shopPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Shop> getShops(long groupId, int start, int end) {

		return shopPersistence.findByGroupId(groupId, start, end);
	}

	public int getShopsCount(long groupId) {

		return shopPersistence.countByGroupId(groupId);
	}

	protected void validate(
			long categoryId, Map<Locale, String> titleMap, Map<Locale, String> imageMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		if (Validator.isNull(categoryId)) {
			throw new ShopCategoryIdException();
		}

		String title = titleMap.get(locale);

		if (Validator.isNull(title)) {
			throw new ShopTitleException();
		}

		String listingDescription = imageMap.get(locale);

		if (Validator.isNull(listingDescription)) {
			throw new ShopImageException();
		}

	}
}