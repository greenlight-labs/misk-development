create index IX_417D7765 on misk_shops (categoryId);
create index IX_DCA8F732 on misk_shops (groupId);
create index IX_16D50790 on misk_shops (title[$COLUMN_LENGTH:75$]);
create index IX_902D586C on misk_shops (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_97C909EE on misk_shops (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_1B6E54BB on misk_shops_categories (groupId);
create index IX_886E2503 on misk_shops_categories (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_86321045 on misk_shops_categories (uuid_[$COLUMN_LENGTH:75$], groupId);