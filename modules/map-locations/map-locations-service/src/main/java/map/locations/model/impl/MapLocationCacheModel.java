/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

import map.locations.model.MapLocation;

/**
 * The cache model class for representing MapLocation in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MapLocationCacheModel
	implements CacheModel<MapLocation>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MapLocationCacheModel)) {
			return false;
		}

		MapLocationCacheModel mapLocationCacheModel =
			(MapLocationCacheModel)object;

		if (locationId == mapLocationCacheModel.locationId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, locationId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", locationId=");
		sb.append(locationId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", title=");
		sb.append(title);
		sb.append(", latitude=");
		sb.append(latitude);
		sb.append(", longitude=");
		sb.append(longitude);
		sb.append(", phone=");
		sb.append(phone);
		sb.append(", whatsapp=");
		sb.append(whatsapp);
		sb.append(", email=");
		sb.append(email);
		sb.append(", defaultSelected=");
		sb.append(defaultSelected);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public MapLocation toEntityModel() {
		MapLocationImpl mapLocationImpl = new MapLocationImpl();

		if (uuid == null) {
			mapLocationImpl.setUuid("");
		}
		else {
			mapLocationImpl.setUuid(uuid);
		}

		mapLocationImpl.setLocationId(locationId);
		mapLocationImpl.setGroupId(groupId);
		mapLocationImpl.setCompanyId(companyId);
		mapLocationImpl.setUserId(userId);

		if (userName == null) {
			mapLocationImpl.setUserName("");
		}
		else {
			mapLocationImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			mapLocationImpl.setCreateDate(null);
		}
		else {
			mapLocationImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			mapLocationImpl.setModifiedDate(null);
		}
		else {
			mapLocationImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (title == null) {
			mapLocationImpl.setTitle("");
		}
		else {
			mapLocationImpl.setTitle(title);
		}

		if (latitude == null) {
			mapLocationImpl.setLatitude("");
		}
		else {
			mapLocationImpl.setLatitude(latitude);
		}

		if (longitude == null) {
			mapLocationImpl.setLongitude("");
		}
		else {
			mapLocationImpl.setLongitude(longitude);
		}

		if (phone == null) {
			mapLocationImpl.setPhone("");
		}
		else {
			mapLocationImpl.setPhone(phone);
		}

		if (whatsapp == null) {
			mapLocationImpl.setWhatsapp("");
		}
		else {
			mapLocationImpl.setWhatsapp(whatsapp);
		}

		if (email == null) {
			mapLocationImpl.setEmail("");
		}
		else {
			mapLocationImpl.setEmail(email);
		}

		mapLocationImpl.setDefaultSelected(defaultSelected);

		mapLocationImpl.resetOriginalValues();

		return mapLocationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		locationId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		title = objectInput.readUTF();
		latitude = objectInput.readUTF();
		longitude = objectInput.readUTF();
		phone = objectInput.readUTF();
		whatsapp = objectInput.readUTF();
		email = objectInput.readUTF();

		defaultSelected = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(locationId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (latitude == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(latitude);
		}

		if (longitude == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(longitude);
		}

		if (phone == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(phone);
		}

		if (whatsapp == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(whatsapp);
		}

		if (email == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(email);
		}

		objectOutput.writeBoolean(defaultSelected);
	}

	public String uuid;
	public long locationId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String title;
	public String latitude;
	public String longitude;
	public String phone;
	public String whatsapp;
	public String email;
	public boolean defaultSelected;

}