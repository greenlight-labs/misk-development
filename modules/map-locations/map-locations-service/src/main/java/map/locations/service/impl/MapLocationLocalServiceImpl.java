package map.locations.service.impl;

import map.locations.exception.MapLocationValidateException;
import map.locations.model.MapLocation;
import map.locations.service.MapLocationLocalServiceUtil;
import map.locations.service.base.MapLocationLocalServiceBaseImpl;
import map.locations.service.util.MapLocationValidator;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.*;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import java.util.*;

@Component(
        property = "model.class.name=map.locations.model.MapLocation", service = AopService.class
)
public class MapLocationLocalServiceImpl extends MapLocationLocalServiceBaseImpl {

    private static Log _log = LogFactoryUtil.getLog(MapLocationLocalServiceImpl.class);

    public MapLocation addEntry(MapLocation orgEntry, ServiceContext serviceContext)
            throws PortalException, MapLocationValidateException {

        // Validation

        ModelValidator<MapLocation> modelValidator = new MapLocationValidator();
        modelValidator.validate(orgEntry);

        // Add entry

        MapLocation entry = _addEntry(orgEntry, serviceContext);

        MapLocation addedEntry = mapLocationPersistence.update(entry);

        mapLocationPersistence.clearCache();

        return addedEntry;
    }

    public MapLocation updateEntry(MapLocation orgEntry, ServiceContext serviceContext)
            throws PortalException, MapLocationValidateException {

        User user = userLocalService.getUser(orgEntry.getUserId());

        // Validation

        ModelValidator<MapLocation> modelValidator = new MapLocationValidator();
        modelValidator.validate(orgEntry);

        // Update entry

        MapLocation entry = _updateEntry(orgEntry.getPrimaryKey(), orgEntry, serviceContext);

        MapLocation updatedEntry = mapLocationPersistence.update(entry);
        mapLocationPersistence.clearCache();

        return updatedEntry;
    }

    protected MapLocation _addEntry(MapLocation entry, ServiceContext serviceContext) throws PortalException {

        long id = counterLocalService.increment(MapLocation.class.getName());

        MapLocation newEntry = mapLocationPersistence.create(id);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        newEntry.setCompanyId(entry.getCompanyId());
        newEntry.setGroupId(entry.getGroupId());
        newEntry.setUserId(user.getUserId());
        newEntry.setUserName(user.getFullName());
        newEntry.setCreateDate(now);
        newEntry.setModifiedDate(now);
        newEntry.setUuid(serviceContext.getUuid());

        newEntry.setTitle(entry.getTitle());
        newEntry.setLatitude(entry.getLatitude());
        newEntry.setLongitude(entry.getLongitude());
        newEntry.setPhone(entry.getPhone());
        newEntry.setWhatsapp(entry.getWhatsapp());
        newEntry.setEmail(entry.getEmail());
        newEntry.setDefaultSelected(entry.getDefaultSelected());

        return newEntry;
    }

    protected MapLocation _updateEntry(long primaryKey, MapLocation entry, ServiceContext serviceContext)
            throws PortalException {

        MapLocation updateEntry = fetchMapLocation(primaryKey);

        User user = userLocalService.getUser(entry.getUserId());

        Date now = new Date();
        updateEntry.setCompanyId(entry.getCompanyId());
        updateEntry.setGroupId(entry.getGroupId());
        updateEntry.setUserId(user.getUserId());
        updateEntry.setUserName(user.getFullName());
        updateEntry.setCreateDate(entry.getCreateDate());
        updateEntry.setModifiedDate(now);
        updateEntry.setUuid(entry.getUuid());

        updateEntry.setTitle(entry.getTitle());
        updateEntry.setLatitude(entry.getLatitude());
        updateEntry.setLongitude(entry.getLongitude());
        updateEntry.setPhone(entry.getPhone());
        updateEntry.setWhatsapp(entry.getWhatsapp());
        updateEntry.setEmail(entry.getEmail());
        updateEntry.setDefaultSelected(entry.getDefaultSelected());

        return updateEntry;
    }

    public MapLocation deleteEntry(long primaryKey) throws PortalException {
        MapLocation entry = getMapLocation(primaryKey);
        mapLocationPersistence.remove(entry);

        return entry;
    }

    public List<MapLocation> findByGroupId(long groupId) {

        return mapLocationPersistence.findByGroupId(groupId);
    }

    public List<MapLocation> findByGroupId(long groupId, int start, int end, OrderByComparator<MapLocation> obc) {

        return mapLocationPersistence.findByGroupId(groupId, start, end, obc);
    }

    public List<MapLocation> findByGroupId(long groupId, int start, int end) {

        return mapLocationPersistence.findByGroupId(groupId, start, end);
    }

    public int countByGroupId(long groupId) {

        return mapLocationPersistence.countByGroupId(groupId);
    }

    public MapLocation getMapLocationFromRequest(long primaryKey, PortletRequest request)
            throws PortletException, MapLocationValidateException {

        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

        // Create or fetch existing data

        MapLocation entry;

        if (primaryKey <= 0) {
            entry = getNewObject(primaryKey);
        } else {
            entry = fetchMapLocation(primaryKey);
        }

        try {
            entry.setLocationId(primaryKey);

            entry.setTitleMap(LocalizationUtil.getLocalizationMap(request, "title"));
            entry.setLatitude(ParamUtil.getString(request, "latitude"));
            entry.setLongitude(ParamUtil.getString(request, "longitude"));
            entry.setPhone(ParamUtil.getString(request, "phone"));
            entry.setWhatsapp(ParamUtil.getString(request, "whatsapp"));
            entry.setEmail(ParamUtil.getString(request, "email"));
            entry.setDefaultSelected(ParamUtil.getBoolean(request, "defaultSelected"));

            entry.setCompanyId(themeDisplay.getCompanyId());
            entry.setGroupId(themeDisplay.getScopeGroupId());
            entry.setUserId(themeDisplay.getUserId());

            // update default selected to false for all other entries
            updateDefaultSelectedOtherEntries(entry);

        } catch (Exception e) {
            _log.error("Errors occur while populating the model", e);
            List<String> error = new ArrayList<>();
            error.add("value-convert-error");

            throw new MapLocationValidateException(error);
        }

        return entry;
    }

    private void updateDefaultSelectedOtherEntries(MapLocation entry) {
        if (entry.getDefaultSelected()) {
            List<MapLocation> mapLocations = mapLocationPersistence.findByGroupId(entry.getGroupId());
            for (MapLocation mapLocation : mapLocations) {
                if (mapLocation.getLocationId() != entry.getLocationId()) {
                    mapLocation.setDefaultSelected(false);
                    MapLocationLocalServiceUtil.updateMapLocation(mapLocation);
                }
            }
        }
    }

    public MapLocation getNewObject(long primaryKey) {
        primaryKey = (primaryKey <= 0) ? 0 : counterLocalService.increment(MapLocation.class.getName());

        return createMapLocation(primaryKey);
    }


    /* **********************************- Additional Code Here -***************************** */
}