package map.locations.service.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.repository.model.ModelValidator;
import com.liferay.portal.kernel.util.Validator;
import map.locations.model.MapLocation;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Gallery Validator
 *
 * @author tz
 */
public class MapLocationValidator implements ModelValidator<MapLocation> {

    @Override
    public void validate(MapLocation entry) throws PortalException {
        /*   */
        // Validate fields
        validateTitle(entry.getTitle());
        validateLatitude(entry.getLatitude());
        validateLongitude(entry.getLongitude());
        validatePhone(entry.getPhone());
        validateWhatsapp(entry.getWhatsapp());
        validateEmail(entry.getEmail());
    }

    /**
     * categoryId field Validation
     *
     * @param field categoryId
     */
    protected void validateTitle(String field) {
        if (Validator.isNull(field)) {
            _errors.add("map-title-required");
        }
    }

    protected void validateLatitude(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("map-latitude-required");
        }
    }

    protected void validateLongitude(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("map-longitude-required");
        }
    }

    protected void validatePhone(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("map-phone-required");
        }
    }

    protected void validateWhatsapp(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("map-whatsapp-required");
        }
    }

    protected void validateEmail(String field) {
        if (StringUtils.isEmpty(field)) {
            _errors.add("map-email-required");
        }
    }

    protected List<String> _errors = new ArrayList<>();

}
