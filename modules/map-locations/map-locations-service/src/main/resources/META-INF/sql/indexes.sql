create index IX_BB972174 on misk_map_locations (groupId);
create index IX_A47D986A on misk_map_locations (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_6A85316C on misk_map_locations (uuid_[$COLUMN_LENGTH:75$], groupId);