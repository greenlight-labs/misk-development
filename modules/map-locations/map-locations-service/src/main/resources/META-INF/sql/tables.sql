create table misk_map_locations (
	uuid_ VARCHAR(75) null,
	locationId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	title STRING null,
	latitude VARCHAR(75) null,
	longitude VARCHAR(75) null,
	phone VARCHAR(75) null,
	whatsapp VARCHAR(75) null,
	email VARCHAR(75) null,
	defaultSelected BOOLEAN
);