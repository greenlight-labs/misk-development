/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MapLocationLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MapLocationLocalService
 * @generated
 */
public class MapLocationLocalServiceWrapper
	implements MapLocationLocalService,
			   ServiceWrapper<MapLocationLocalService> {

	public MapLocationLocalServiceWrapper(
		MapLocationLocalService mapLocationLocalService) {

		_mapLocationLocalService = mapLocationLocalService;
	}

	@Override
	public map.locations.model.MapLocation addEntry(
			map.locations.model.MapLocation orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   map.locations.exception.MapLocationValidateException {

		return _mapLocationLocalService.addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the map location to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was added
	 */
	@Override
	public map.locations.model.MapLocation addMapLocation(
		map.locations.model.MapLocation mapLocation) {

		return _mapLocationLocalService.addMapLocation(mapLocation);
	}

	@Override
	public int countByGroupId(long groupId) {
		return _mapLocationLocalService.countByGroupId(groupId);
	}

	/**
	 * Creates a new map location with the primary key. Does not add the map location to the database.
	 *
	 * @param locationId the primary key for the new map location
	 * @return the new map location
	 */
	@Override
	public map.locations.model.MapLocation createMapLocation(long locationId) {
		return _mapLocationLocalService.createMapLocation(locationId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mapLocationLocalService.createPersistedModel(primaryKeyObj);
	}

	@Override
	public map.locations.model.MapLocation deleteEntry(long primaryKey)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mapLocationLocalService.deleteEntry(primaryKey);
	}

	/**
	 * Deletes the map location with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location that was removed
	 * @throws PortalException if a map location with the primary key could not be found
	 */
	@Override
	public map.locations.model.MapLocation deleteMapLocation(long locationId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mapLocationLocalService.deleteMapLocation(locationId);
	}

	/**
	 * Deletes the map location from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was removed
	 */
	@Override
	public map.locations.model.MapLocation deleteMapLocation(
		map.locations.model.MapLocation mapLocation) {

		return _mapLocationLocalService.deleteMapLocation(mapLocation);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mapLocationLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _mapLocationLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _mapLocationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _mapLocationLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _mapLocationLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _mapLocationLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _mapLocationLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public map.locations.model.MapLocation fetchMapLocation(long locationId) {
		return _mapLocationLocalService.fetchMapLocation(locationId);
	}

	/**
	 * Returns the map location matching the UUID and group.
	 *
	 * @param uuid the map location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching map location, or <code>null</code> if a matching map location could not be found
	 */
	@Override
	public map.locations.model.MapLocation fetchMapLocationByUuidAndGroupId(
		String uuid, long groupId) {

		return _mapLocationLocalService.fetchMapLocationByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public java.util.List<map.locations.model.MapLocation> findByGroupId(
		long groupId) {

		return _mapLocationLocalService.findByGroupId(groupId);
	}

	@Override
	public java.util.List<map.locations.model.MapLocation> findByGroupId(
		long groupId, int start, int end) {

		return _mapLocationLocalService.findByGroupId(groupId, start, end);
	}

	@Override
	public java.util.List<map.locations.model.MapLocation> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<map.locations.model.MapLocation> obc) {

		return _mapLocationLocalService.findByGroupId(groupId, start, end, obc);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _mapLocationLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _mapLocationLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _mapLocationLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the map location with the primary key.
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location
	 * @throws PortalException if a map location with the primary key could not be found
	 */
	@Override
	public map.locations.model.MapLocation getMapLocation(long locationId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mapLocationLocalService.getMapLocation(locationId);
	}

	/**
	 * Returns the map location matching the UUID and group.
	 *
	 * @param uuid the map location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching map location
	 * @throws PortalException if a matching map location could not be found
	 */
	@Override
	public map.locations.model.MapLocation getMapLocationByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mapLocationLocalService.getMapLocationByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public map.locations.model.MapLocation getMapLocationFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException,
			   map.locations.exception.MapLocationValidateException {

		return _mapLocationLocalService.getMapLocationFromRequest(
			primaryKey, request);
	}

	/**
	 * Returns a range of all the map locations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @return the range of map locations
	 */
	@Override
	public java.util.List<map.locations.model.MapLocation> getMapLocations(
		int start, int end) {

		return _mapLocationLocalService.getMapLocations(start, end);
	}

	/**
	 * Returns all the map locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the map locations
	 * @param companyId the primary key of the company
	 * @return the matching map locations, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<map.locations.model.MapLocation>
		getMapLocationsByUuidAndCompanyId(String uuid, long companyId) {

		return _mapLocationLocalService.getMapLocationsByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of map locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the map locations
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching map locations, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<map.locations.model.MapLocation>
		getMapLocationsByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<map.locations.model.MapLocation> orderByComparator) {

		return _mapLocationLocalService.getMapLocationsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of map locations.
	 *
	 * @return the number of map locations
	 */
	@Override
	public int getMapLocationsCount() {
		return _mapLocationLocalService.getMapLocationsCount();
	}

	@Override
	public map.locations.model.MapLocation getNewObject(long primaryKey) {
		return _mapLocationLocalService.getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _mapLocationLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _mapLocationLocalService.getPersistedModel(primaryKeyObj);
	}

	@Override
	public map.locations.model.MapLocation updateEntry(
			map.locations.model.MapLocation orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   map.locations.exception.MapLocationValidateException {

		return _mapLocationLocalService.updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the map location in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was updated
	 */
	@Override
	public map.locations.model.MapLocation updateMapLocation(
		map.locations.model.MapLocation mapLocation) {

		return _mapLocationLocalService.updateMapLocation(mapLocation);
	}

	@Override
	public MapLocationLocalService getWrappedService() {
		return _mapLocationLocalService;
	}

	@Override
	public void setWrappedService(
		MapLocationLocalService mapLocationLocalService) {

		_mapLocationLocalService = mapLocationLocalService;
	}

	private MapLocationLocalService _mapLocationLocalService;

}