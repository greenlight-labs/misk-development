/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link map.locations.service.http.MapLocationServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class MapLocationSoap implements Serializable {

	public static MapLocationSoap toSoapModel(MapLocation model) {
		MapLocationSoap soapModel = new MapLocationSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setLocationId(model.getLocationId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setTitle(model.getTitle());
		soapModel.setLatitude(model.getLatitude());
		soapModel.setLongitude(model.getLongitude());
		soapModel.setPhone(model.getPhone());
		soapModel.setWhatsapp(model.getWhatsapp());
		soapModel.setEmail(model.getEmail());
		soapModel.setDefaultSelected(model.isDefaultSelected());

		return soapModel;
	}

	public static MapLocationSoap[] toSoapModels(MapLocation[] models) {
		MapLocationSoap[] soapModels = new MapLocationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static MapLocationSoap[][] toSoapModels(MapLocation[][] models) {
		MapLocationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new MapLocationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new MapLocationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static MapLocationSoap[] toSoapModels(List<MapLocation> models) {
		List<MapLocationSoap> soapModels = new ArrayList<MapLocationSoap>(
			models.size());

		for (MapLocation model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new MapLocationSoap[soapModels.size()]);
	}

	public MapLocationSoap() {
	}

	public long getPrimaryKey() {
		return _locationId;
	}

	public void setPrimaryKey(long pk) {
		setLocationId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getLocationId() {
		return _locationId;
	}

	public void setLocationId(long locationId) {
		_locationId = locationId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getLatitude() {
		return _latitude;
	}

	public void setLatitude(String latitude) {
		_latitude = latitude;
	}

	public String getLongitude() {
		return _longitude;
	}

	public void setLongitude(String longitude) {
		_longitude = longitude;
	}

	public String getPhone() {
		return _phone;
	}

	public void setPhone(String phone) {
		_phone = phone;
	}

	public String getWhatsapp() {
		return _whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		_whatsapp = whatsapp;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public boolean getDefaultSelected() {
		return _defaultSelected;
	}

	public boolean isDefaultSelected() {
		return _defaultSelected;
	}

	public void setDefaultSelected(boolean defaultSelected) {
		_defaultSelected = defaultSelected;
	}

	private String _uuid;
	private long _locationId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _title;
	private String _latitude;
	private String _longitude;
	private String _phone;
	private String _whatsapp;
	private String _email;
	private boolean _defaultSelected;

}