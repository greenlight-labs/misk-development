/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MapLocation}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MapLocation
 * @generated
 */
public class MapLocationWrapper
	extends BaseModelWrapper<MapLocation>
	implements MapLocation, ModelWrapper<MapLocation> {

	public MapLocationWrapper(MapLocation mapLocation) {
		super(mapLocation);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("locationId", getLocationId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("phone", getPhone());
		attributes.put("whatsapp", getWhatsapp());
		attributes.put("email", getEmail());
		attributes.put("defaultSelected", isDefaultSelected());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long locationId = (Long)attributes.get("locationId");

		if (locationId != null) {
			setLocationId(locationId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String latitude = (String)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		String longitude = (String)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		String phone = (String)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}

		String whatsapp = (String)attributes.get("whatsapp");

		if (whatsapp != null) {
			setWhatsapp(whatsapp);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		Boolean defaultSelected = (Boolean)attributes.get("defaultSelected");

		if (defaultSelected != null) {
			setDefaultSelected(defaultSelected);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the company ID of this map location.
	 *
	 * @return the company ID of this map location
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this map location.
	 *
	 * @return the create date of this map location
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the default selected of this map location.
	 *
	 * @return the default selected of this map location
	 */
	@Override
	public boolean getDefaultSelected() {
		return model.getDefaultSelected();
	}

	/**
	 * Returns the email of this map location.
	 *
	 * @return the email of this map location
	 */
	@Override
	public String getEmail() {
		return model.getEmail();
	}

	/**
	 * Returns the group ID of this map location.
	 *
	 * @return the group ID of this map location
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the latitude of this map location.
	 *
	 * @return the latitude of this map location
	 */
	@Override
	public String getLatitude() {
		return model.getLatitude();
	}

	/**
	 * Returns the location ID of this map location.
	 *
	 * @return the location ID of this map location
	 */
	@Override
	public long getLocationId() {
		return model.getLocationId();
	}

	/**
	 * Returns the longitude of this map location.
	 *
	 * @return the longitude of this map location
	 */
	@Override
	public String getLongitude() {
		return model.getLongitude();
	}

	/**
	 * Returns the modified date of this map location.
	 *
	 * @return the modified date of this map location
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the phone of this map location.
	 *
	 * @return the phone of this map location
	 */
	@Override
	public String getPhone() {
		return model.getPhone();
	}

	/**
	 * Returns the primary key of this map location.
	 *
	 * @return the primary key of this map location
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the title of this map location.
	 *
	 * @return the title of this map location
	 */
	@Override
	public String getTitle() {
		return model.getTitle();
	}

	/**
	 * Returns the localized title of this map location in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized title of this map location
	 */
	@Override
	public String getTitle(java.util.Locale locale) {
		return model.getTitle(locale);
	}

	/**
	 * Returns the localized title of this map location in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this map location. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getTitle(java.util.Locale locale, boolean useDefault) {
		return model.getTitle(locale, useDefault);
	}

	/**
	 * Returns the localized title of this map location in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized title of this map location
	 */
	@Override
	public String getTitle(String languageId) {
		return model.getTitle(languageId);
	}

	/**
	 * Returns the localized title of this map location in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized title of this map location
	 */
	@Override
	public String getTitle(String languageId, boolean useDefault) {
		return model.getTitle(languageId, useDefault);
	}

	@Override
	public String getTitleCurrentLanguageId() {
		return model.getTitleCurrentLanguageId();
	}

	@Override
	public String getTitleCurrentValue() {
		return model.getTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized titles of this map location.
	 *
	 * @return the locales and localized titles of this map location
	 */
	@Override
	public Map<java.util.Locale, String> getTitleMap() {
		return model.getTitleMap();
	}

	/**
	 * Returns the user ID of this map location.
	 *
	 * @return the user ID of this map location
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this map location.
	 *
	 * @return the user name of this map location
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this map location.
	 *
	 * @return the user uuid of this map location
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this map location.
	 *
	 * @return the uuid of this map location
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	/**
	 * Returns the whatsapp of this map location.
	 *
	 * @return the whatsapp of this map location
	 */
	@Override
	public String getWhatsapp() {
		return model.getWhatsapp();
	}

	/**
	 * Returns <code>true</code> if this map location is default selected.
	 *
	 * @return <code>true</code> if this map location is default selected; <code>false</code> otherwise
	 */
	@Override
	public boolean isDefaultSelected() {
		return model.isDefaultSelected();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the company ID of this map location.
	 *
	 * @param companyId the company ID of this map location
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this map location.
	 *
	 * @param createDate the create date of this map location
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets whether this map location is default selected.
	 *
	 * @param defaultSelected the default selected of this map location
	 */
	@Override
	public void setDefaultSelected(boolean defaultSelected) {
		model.setDefaultSelected(defaultSelected);
	}

	/**
	 * Sets the email of this map location.
	 *
	 * @param email the email of this map location
	 */
	@Override
	public void setEmail(String email) {
		model.setEmail(email);
	}

	/**
	 * Sets the group ID of this map location.
	 *
	 * @param groupId the group ID of this map location
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the latitude of this map location.
	 *
	 * @param latitude the latitude of this map location
	 */
	@Override
	public void setLatitude(String latitude) {
		model.setLatitude(latitude);
	}

	/**
	 * Sets the location ID of this map location.
	 *
	 * @param locationId the location ID of this map location
	 */
	@Override
	public void setLocationId(long locationId) {
		model.setLocationId(locationId);
	}

	/**
	 * Sets the longitude of this map location.
	 *
	 * @param longitude the longitude of this map location
	 */
	@Override
	public void setLongitude(String longitude) {
		model.setLongitude(longitude);
	}

	/**
	 * Sets the modified date of this map location.
	 *
	 * @param modifiedDate the modified date of this map location
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the phone of this map location.
	 *
	 * @param phone the phone of this map location
	 */
	@Override
	public void setPhone(String phone) {
		model.setPhone(phone);
	}

	/**
	 * Sets the primary key of this map location.
	 *
	 * @param primaryKey the primary key of this map location
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the title of this map location.
	 *
	 * @param title the title of this map location
	 */
	@Override
	public void setTitle(String title) {
		model.setTitle(title);
	}

	/**
	 * Sets the localized title of this map location in the language.
	 *
	 * @param title the localized title of this map location
	 * @param locale the locale of the language
	 */
	@Override
	public void setTitle(String title, java.util.Locale locale) {
		model.setTitle(title, locale);
	}

	/**
	 * Sets the localized title of this map location in the language, and sets the default locale.
	 *
	 * @param title the localized title of this map location
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitle(
		String title, java.util.Locale locale, java.util.Locale defaultLocale) {

		model.setTitle(title, locale, defaultLocale);
	}

	@Override
	public void setTitleCurrentLanguageId(String languageId) {
		model.setTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized titles of this map location from the map of locales and localized titles.
	 *
	 * @param titleMap the locales and localized titles of this map location
	 */
	@Override
	public void setTitleMap(Map<java.util.Locale, String> titleMap) {
		model.setTitleMap(titleMap);
	}

	/**
	 * Sets the localized titles of this map location from the map of locales and localized titles, and sets the default locale.
	 *
	 * @param titleMap the locales and localized titles of this map location
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setTitleMap(
		Map<java.util.Locale, String> titleMap,
		java.util.Locale defaultLocale) {

		model.setTitleMap(titleMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this map location.
	 *
	 * @param userId the user ID of this map location
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this map location.
	 *
	 * @param userName the user name of this map location
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this map location.
	 *
	 * @param userUuid the user uuid of this map location
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this map location.
	 *
	 * @param uuid the uuid of this map location
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	/**
	 * Sets the whatsapp of this map location.
	 *
	 * @param whatsapp the whatsapp of this map location
	 */
	@Override
	public void setWhatsapp(String whatsapp) {
		model.setWhatsapp(whatsapp);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected MapLocationWrapper wrap(MapLocation mapLocation) {
		return new MapLocationWrapper(mapLocation);
	}

}