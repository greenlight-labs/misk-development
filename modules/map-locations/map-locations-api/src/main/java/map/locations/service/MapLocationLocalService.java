/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.service;

import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.*;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;

import map.locations.exception.MapLocationValidateException;
import map.locations.model.MapLocation;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides the local service interface for MapLocation. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see MapLocationLocalServiceUtil
 * @generated
 */
@ProviderType
@Transactional(
	isolation = Isolation.PORTAL,
	rollbackFor = {PortalException.class, SystemException.class}
)
public interface MapLocationLocalService
	extends BaseLocalService, PersistedModelLocalService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add custom service methods to <code>map.locations.service.impl.MapLocationLocalServiceImpl</code> and rerun ServiceBuilder to automatically copy the method declarations to this interface. Consume the map location local service via injection or a <code>org.osgi.util.tracker.ServiceTracker</code>. Use {@link MapLocationLocalServiceUtil} if injection and service tracking are not available.
	 */
	public MapLocation addEntry(
			MapLocation orgEntry, ServiceContext serviceContext)
		throws MapLocationValidateException, PortalException;

	/**
	 * Adds the map location to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	public MapLocation addMapLocation(MapLocation mapLocation);

	public int countByGroupId(long groupId);

	/**
	 * Creates a new map location with the primary key. Does not add the map location to the database.
	 *
	 * @param locationId the primary key for the new map location
	 * @return the new map location
	 */
	@Transactional(enabled = false)
	public MapLocation createMapLocation(long locationId);

	/**
	 * @throws PortalException
	 */
	public PersistedModel createPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public MapLocation deleteEntry(long primaryKey) throws PortalException;

	/**
	 * Deletes the map location with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location that was removed
	 * @throws PortalException if a map location with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	public MapLocation deleteMapLocation(long locationId)
		throws PortalException;

	/**
	 * Deletes the map location from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	public MapLocation deleteMapLocation(MapLocation mapLocation);

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end);

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MapLocation fetchMapLocation(long locationId);

	/**
	 * Returns the map location matching the UUID and group.
	 *
	 * @param uuid the map location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching map location, or <code>null</code> if a matching map location could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MapLocation fetchMapLocationByUuidAndGroupId(
		String uuid, long groupId);

	public List<MapLocation> findByGroupId(long groupId);

	public List<MapLocation> findByGroupId(long groupId, int start, int end);

	public List<MapLocation> findByGroupId(
		long groupId, int start, int end, OrderByComparator<MapLocation> obc);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	 * Returns the map location with the primary key.
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location
	 * @throws PortalException if a map location with the primary key could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MapLocation getMapLocation(long locationId) throws PortalException;

	/**
	 * Returns the map location matching the UUID and group.
	 *
	 * @param uuid the map location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching map location
	 * @throws PortalException if a matching map location could not be found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MapLocation getMapLocationByUuidAndGroupId(String uuid, long groupId)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MapLocation getMapLocationFromRequest(
			long primaryKey, PortletRequest request)
		throws MapLocationValidateException, PortletException;

	/**
	 * Returns a range of all the map locations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @return the range of map locations
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MapLocation> getMapLocations(int start, int end);

	/**
	 * Returns all the map locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the map locations
	 * @param companyId the primary key of the company
	 * @return the matching map locations, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MapLocation> getMapLocationsByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	 * Returns a range of map locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the map locations
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching map locations, or an empty list if no matches were found
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MapLocation> getMapLocationsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MapLocation> orderByComparator);

	/**
	 * Returns the number of map locations.
	 *
	 * @return the number of map locations
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getMapLocationsCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public MapLocation getNewObject(long primaryKey);

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public String getOSGiServiceIdentifier();

	/**
	 * @throws PortalException
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	public MapLocation updateEntry(
			MapLocation orgEntry, ServiceContext serviceContext)
		throws MapLocationValidateException, PortalException;

	/**
	 * Updates the map location in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	public MapLocation updateMapLocation(MapLocation mapLocation);

}