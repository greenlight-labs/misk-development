/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import map.locations.model.MapLocation;

/**
 * The persistence utility for the map location service. This utility wraps <code>map.locations.service.persistence.impl.MapLocationPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MapLocationPersistence
 * @generated
 */
public class MapLocationUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(MapLocation mapLocation) {
		getPersistence().clearCache(mapLocation);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, MapLocation> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<MapLocation> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<MapLocation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<MapLocation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static MapLocation update(MapLocation mapLocation) {
		return getPersistence().update(mapLocation);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static MapLocation update(
		MapLocation mapLocation, ServiceContext serviceContext) {

		return getPersistence().update(mapLocation, serviceContext);
	}

	/**
	 * Returns all the map locations where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching map locations
	 */
	public static List<MapLocation> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the map locations where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @return the range of matching map locations
	 */
	public static List<MapLocation> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the map locations where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching map locations
	 */
	public static List<MapLocation> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the map locations where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching map locations
	 */
	public static List<MapLocation> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<MapLocation> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first map location in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching map location
	 * @throws NoSuchMapLocationException if a matching map location could not be found
	 */
	public static MapLocation findByUuid_First(
			String uuid, OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first map location in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByUuid_First(
		String uuid, OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last map location in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching map location
	 * @throws NoSuchMapLocationException if a matching map location could not be found
	 */
	public static MapLocation findByUuid_Last(
			String uuid, OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last map location in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByUuid_Last(
		String uuid, OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the map locations before and after the current map location in the ordered set where uuid = &#63;.
	 *
	 * @param locationId the primary key of the current map location
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next map location
	 * @throws NoSuchMapLocationException if a map location with the primary key could not be found
	 */
	public static MapLocation[] findByUuid_PrevAndNext(
			long locationId, String uuid,
			OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByUuid_PrevAndNext(
			locationId, uuid, orderByComparator);
	}

	/**
	 * Removes all the map locations where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of map locations where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching map locations
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the map location where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchMapLocationException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching map location
	 * @throws NoSuchMapLocationException if a matching map location could not be found
	 */
	public static MapLocation findByUUID_G(String uuid, long groupId)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the map location where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the map location where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the map location where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the map location that was removed
	 */
	public static MapLocation removeByUUID_G(String uuid, long groupId)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of map locations where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching map locations
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the map locations where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching map locations
	 */
	public static List<MapLocation> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the map locations where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @return the range of matching map locations
	 */
	public static List<MapLocation> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the map locations where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching map locations
	 */
	public static List<MapLocation> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the map locations where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching map locations
	 */
	public static List<MapLocation> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MapLocation> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first map location in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching map location
	 * @throws NoSuchMapLocationException if a matching map location could not be found
	 */
	public static MapLocation findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first map location in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last map location in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching map location
	 * @throws NoSuchMapLocationException if a matching map location could not be found
	 */
	public static MapLocation findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last map location in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the map locations before and after the current map location in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param locationId the primary key of the current map location
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next map location
	 * @throws NoSuchMapLocationException if a map location with the primary key could not be found
	 */
	public static MapLocation[] findByUuid_C_PrevAndNext(
			long locationId, String uuid, long companyId,
			OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByUuid_C_PrevAndNext(
			locationId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the map locations where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of map locations where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching map locations
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the map locations where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching map locations
	 */
	public static List<MapLocation> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the map locations where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @return the range of matching map locations
	 */
	public static List<MapLocation> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the map locations where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching map locations
	 */
	public static List<MapLocation> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the map locations where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching map locations
	 */
	public static List<MapLocation> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<MapLocation> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first map location in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching map location
	 * @throws NoSuchMapLocationException if a matching map location could not be found
	 */
	public static MapLocation findByGroupId_First(
			long groupId, OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first map location in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByGroupId_First(
		long groupId, OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last map location in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching map location
	 * @throws NoSuchMapLocationException if a matching map location could not be found
	 */
	public static MapLocation findByGroupId_Last(
			long groupId, OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last map location in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchByGroupId_Last(
		long groupId, OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the map locations before and after the current map location in the ordered set where groupId = &#63;.
	 *
	 * @param locationId the primary key of the current map location
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next map location
	 * @throws NoSuchMapLocationException if a map location with the primary key could not be found
	 */
	public static MapLocation[] findByGroupId_PrevAndNext(
			long locationId, long groupId,
			OrderByComparator<MapLocation> orderByComparator)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByGroupId_PrevAndNext(
			locationId, groupId, orderByComparator);
	}

	/**
	 * Removes all the map locations where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of map locations where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching map locations
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Caches the map location in the entity cache if it is enabled.
	 *
	 * @param mapLocation the map location
	 */
	public static void cacheResult(MapLocation mapLocation) {
		getPersistence().cacheResult(mapLocation);
	}

	/**
	 * Caches the map locations in the entity cache if it is enabled.
	 *
	 * @param mapLocations the map locations
	 */
	public static void cacheResult(List<MapLocation> mapLocations) {
		getPersistence().cacheResult(mapLocations);
	}

	/**
	 * Creates a new map location with the primary key. Does not add the map location to the database.
	 *
	 * @param locationId the primary key for the new map location
	 * @return the new map location
	 */
	public static MapLocation create(long locationId) {
		return getPersistence().create(locationId);
	}

	/**
	 * Removes the map location with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location that was removed
	 * @throws NoSuchMapLocationException if a map location with the primary key could not be found
	 */
	public static MapLocation remove(long locationId)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().remove(locationId);
	}

	public static MapLocation updateImpl(MapLocation mapLocation) {
		return getPersistence().updateImpl(mapLocation);
	}

	/**
	 * Returns the map location with the primary key or throws a <code>NoSuchMapLocationException</code> if it could not be found.
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location
	 * @throws NoSuchMapLocationException if a map location with the primary key could not be found
	 */
	public static MapLocation findByPrimaryKey(long locationId)
		throws map.locations.exception.NoSuchMapLocationException {

		return getPersistence().findByPrimaryKey(locationId);
	}

	/**
	 * Returns the map location with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location, or <code>null</code> if a map location with the primary key could not be found
	 */
	public static MapLocation fetchByPrimaryKey(long locationId) {
		return getPersistence().fetchByPrimaryKey(locationId);
	}

	/**
	 * Returns all the map locations.
	 *
	 * @return the map locations
	 */
	public static List<MapLocation> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the map locations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @return the range of map locations
	 */
	public static List<MapLocation> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the map locations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of map locations
	 */
	public static List<MapLocation> findAll(
		int start, int end, OrderByComparator<MapLocation> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the map locations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of map locations
	 */
	public static List<MapLocation> findAll(
		int start, int end, OrderByComparator<MapLocation> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the map locations from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of map locations.
	 *
	 * @return the number of map locations
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static MapLocationPersistence getPersistence() {
		return _persistence;
	}

	private static volatile MapLocationPersistence _persistence;

}