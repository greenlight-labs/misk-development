/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.service;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

import map.locations.model.MapLocation;

/**
 * Provides the local service utility for MapLocation. This utility wraps
 * <code>map.locations.service.impl.MapLocationLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see MapLocationLocalService
 * @generated
 */
public class MapLocationLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>map.locations.service.impl.MapLocationLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static MapLocation addEntry(
			MapLocation orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws map.locations.exception.MapLocationValidateException,
			   PortalException {

		return getService().addEntry(orgEntry, serviceContext);
	}

	/**
	 * Adds the map location to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was added
	 */
	public static MapLocation addMapLocation(MapLocation mapLocation) {
		return getService().addMapLocation(mapLocation);
	}

	public static int countByGroupId(long groupId) {
		return getService().countByGroupId(groupId);
	}

	/**
	 * Creates a new map location with the primary key. Does not add the map location to the database.
	 *
	 * @param locationId the primary key for the new map location
	 * @return the new map location
	 */
	public static MapLocation createMapLocation(long locationId) {
		return getService().createMapLocation(locationId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	public static MapLocation deleteEntry(long primaryKey)
		throws PortalException {

		return getService().deleteEntry(primaryKey);
	}

	/**
	 * Deletes the map location with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location that was removed
	 * @throws PortalException if a map location with the primary key could not be found
	 */
	public static MapLocation deleteMapLocation(long locationId)
		throws PortalException {

		return getService().deleteMapLocation(locationId);
	}

	/**
	 * Deletes the map location from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was removed
	 */
	public static MapLocation deleteMapLocation(MapLocation mapLocation) {
		return getService().deleteMapLocation(mapLocation);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static MapLocation fetchMapLocation(long locationId) {
		return getService().fetchMapLocation(locationId);
	}

	/**
	 * Returns the map location matching the UUID and group.
	 *
	 * @param uuid the map location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching map location, or <code>null</code> if a matching map location could not be found
	 */
	public static MapLocation fetchMapLocationByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchMapLocationByUuidAndGroupId(uuid, groupId);
	}

	public static List<MapLocation> findByGroupId(long groupId) {
		return getService().findByGroupId(groupId);
	}

	public static List<MapLocation> findByGroupId(
		long groupId, int start, int end) {

		return getService().findByGroupId(groupId, start, end);
	}

	public static List<MapLocation> findByGroupId(
		long groupId, int start, int end, OrderByComparator<MapLocation> obc) {

		return getService().findByGroupId(groupId, start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the map location with the primary key.
	 *
	 * @param locationId the primary key of the map location
	 * @return the map location
	 * @throws PortalException if a map location with the primary key could not be found
	 */
	public static MapLocation getMapLocation(long locationId)
		throws PortalException {

		return getService().getMapLocation(locationId);
	}

	/**
	 * Returns the map location matching the UUID and group.
	 *
	 * @param uuid the map location's UUID
	 * @param groupId the primary key of the group
	 * @return the matching map location
	 * @throws PortalException if a matching map location could not be found
	 */
	public static MapLocation getMapLocationByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return getService().getMapLocationByUuidAndGroupId(uuid, groupId);
	}

	public static MapLocation getMapLocationFromRequest(
			long primaryKey, javax.portlet.PortletRequest request)
		throws javax.portlet.PortletException,
			   map.locations.exception.MapLocationValidateException {

		return getService().getMapLocationFromRequest(primaryKey, request);
	}

	/**
	 * Returns a range of all the map locations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>map.locations.model.impl.MapLocationModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @return the range of map locations
	 */
	public static List<MapLocation> getMapLocations(int start, int end) {
		return getService().getMapLocations(start, end);
	}

	/**
	 * Returns all the map locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the map locations
	 * @param companyId the primary key of the company
	 * @return the matching map locations, or an empty list if no matches were found
	 */
	public static List<MapLocation> getMapLocationsByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getMapLocationsByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of map locations matching the UUID and company.
	 *
	 * @param uuid the UUID of the map locations
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of map locations
	 * @param end the upper bound of the range of map locations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching map locations, or an empty list if no matches were found
	 */
	public static List<MapLocation> getMapLocationsByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<MapLocation> orderByComparator) {

		return getService().getMapLocationsByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of map locations.
	 *
	 * @return the number of map locations
	 */
	public static int getMapLocationsCount() {
		return getService().getMapLocationsCount();
	}

	public static MapLocation getNewObject(long primaryKey) {
		return getService().getNewObject(primaryKey);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	public static MapLocation updateEntry(
			MapLocation orgEntry,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws map.locations.exception.MapLocationValidateException,
			   PortalException {

		return getService().updateEntry(orgEntry, serviceContext);
	}

	/**
	 * Updates the map location in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect MapLocationLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param mapLocation the map location
	 * @return the map location that was updated
	 */
	public static MapLocation updateMapLocation(MapLocation mapLocation) {
		return getService().updateMapLocation(mapLocation);
	}

	public static MapLocationLocalService getService() {
		return _service;
	}

	private static volatile MapLocationLocalService _service;

}