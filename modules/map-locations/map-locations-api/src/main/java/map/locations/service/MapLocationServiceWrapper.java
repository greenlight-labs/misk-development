/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package map.locations.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MapLocationService}.
 *
 * @author Brian Wing Shun Chan
 * @see MapLocationService
 * @generated
 */
public class MapLocationServiceWrapper
	implements MapLocationService, ServiceWrapper<MapLocationService> {

	public MapLocationServiceWrapper(MapLocationService mapLocationService) {
		_mapLocationService = mapLocationService;
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _mapLocationService.getOSGiServiceIdentifier();
	}

	@Override
	public MapLocationService getWrappedService() {
		return _mapLocationService;
	}

	@Override
	public void setWrappedService(MapLocationService mapLocationService) {
		_mapLocationService = mapLocationService;
	}

	private MapLocationService _mapLocationService;

}