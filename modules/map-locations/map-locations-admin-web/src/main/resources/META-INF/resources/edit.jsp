<%@include file = "init.jsp" %>

<%
    long resourcePrimKey = ParamUtil.getLong(request, "resourcePrimKey");

    MapLocation entry = null;

    if (resourcePrimKey > 0) {
        try {
            entry = MapLocationLocalServiceUtil.getMapLocation(resourcePrimKey);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= entry == null ? "addEntry" : "updateEntry" %>' var="editEntryURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editEntryURL %>" name="fm">

    <aui:model-context bean="<%= entry %>" model="<%= MapLocation.class %>" />

    <aui:input type="hidden" name="resourcePrimKey"
               value='<%= entry == null ? "" : entry.getLocationId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:input name="title" label="Title" autoSize="true" helpMessage="Max 65 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="latitude" label="Latitude">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="longitude" label="Longitude">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="phone" label="Phone Number" />
            <aui:input name="whatsapp" label="WhatsApp Number" />
            <aui:input name="email" label="Email Address" />
            <aui:input name="defaultSelected" label="Default Selected" helpMessage="Make this location as default selected on the map" />
        </aui:fieldset>
    </aui:fieldset-group>
    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
