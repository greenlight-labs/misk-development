<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="admin-buttons">
        <portlet:renderURL var="addEntryURL">
            <portlet:param name="mvcPath"
                           value="/edit.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addEntryURL.toString() %>"
                    value="Add Entry"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= MapLocationLocalServiceUtil.countByGroupId(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= MapLocationLocalServiceUtil.findByGroupId(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row className="map.locations.model.MapLocation" modelVar="entry">

            <liferay-ui:search-container-column-text name="Title" value="<%= HtmlUtil.escape(entry.getTitle(locale)) %>"/>
            <liferay-ui:search-container-column-text name="Latitude" value="<%= HtmlUtil.escape(entry.getLatitude()) %>"/>
            <liferay-ui:search-container-column-text name="Longitude" value="<%= HtmlUtil.escape(entry.getLongitude()) %>"/>
            <liferay-ui:search-container-column-text name="Email Address" value="<%= HtmlUtil.escape(entry.getEmail()) %>"/>
            <liferay-ui:search-container-column-text name="Default Selected" value="<%= String.valueOf(entry.getDefaultSelected() ? 1 : 0) %>"/>

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>