<%@include file="init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    MapLocation entry = (MapLocation) row.getObject();
    String primKey = String.valueOf(entry.getPrimaryKey());
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="resourcePrimKey" value="<%= primKey %>" />
        <portlet:param name="mvcPath" value="/edit.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit" url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteEntry" var="deleteURL">
        <portlet:param name="resourcePrimKey" value="<%= primKey %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>