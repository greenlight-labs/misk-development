package map.locations.admin.web.constants;

/**
 * @author tz
 */
public class MapLocationsAdminWebPortletKeys {

	public static final String MAPLOCATIONSADMINWEB =
		"map_locations_admin_web_MapLocationsAdminWebPortlet";

}