package map.locations.admin.web.application.list;

import map.locations.admin.web.constants.MapLocationsAdminWebPanelCategoryKeys;
import map.locations.admin.web.constants.MapLocationsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + MapLocationsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class MapLocationsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return MapLocationsAdminWebPortletKeys.MAPLOCATIONSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + MapLocationsAdminWebPortletKeys.MAPLOCATIONSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}