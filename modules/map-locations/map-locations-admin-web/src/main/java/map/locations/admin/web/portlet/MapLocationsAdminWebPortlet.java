package map.locations.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.file.criterion.FileItemSelectorCriterion;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import map.locations.admin.web.constants.MapLocationsAdminWebPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.*;

import map.locations.exception.MapLocationValidateException;
import map.locations.model.MapLocation;
import map.locations.service.MapLocationLocalServiceUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.liferay.portal.kernel.log.LogFactoryUtil.getLog;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=MapLocationsAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + MapLocationsAdminWebPortletKeys.MAPLOCATIONSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class MapLocationsAdminWebPortlet extends MVCPortlet {

	public void addEntry(ActionRequest request, ActionResponse response)
			throws Exception, MapLocationValidateException {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);
		MapLocation entry = MapLocationLocalServiceUtil.getMapLocationFromRequest(
				primaryKey, request);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				MapLocation.class.getName(), request);

		// Add entry
		MapLocationLocalServiceUtil.addEntry(entry, serviceContext);

		SessionMessages.add(request, "postAddedSuccessfully");
	}

	public void updateEntry(ActionRequest request, ActionResponse response)
			throws Exception {

		long primaryKey = ParamUtil.getLong(request, "resourcePrimKey", 0);

		long orderNo = ParamUtil.getLong(request, "orderNo", 9999);


		MapLocation entry = MapLocationLocalServiceUtil.getMapLocationFromRequest(
				primaryKey, request);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				MapLocation.class.getName(), request);

		//Update entry
		MapLocationLocalServiceUtil.updateEntry(entry, serviceContext);

		SessionMessages.add(request, "postUpdatedSuccessfully");
	}

	public void deleteEntry(ActionRequest request, ActionResponse response)
			throws PortalException {

		long entryId = ParamUtil.getLong(request, "resourcePrimKey", 0L);

		try {
			MapLocationLocalServiceUtil.deleteEntry(entryId);
		} catch (PortalException pe) {
			ReflectionUtil.throwException(pe);
		}
	}

	private static Log _log = getLog(MapLocationsAdminWebPortlet.class);

}