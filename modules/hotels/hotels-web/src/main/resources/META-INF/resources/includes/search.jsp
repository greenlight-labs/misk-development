<portlet:renderURL var="searchPageURL" />

<div class="row justify-content-end">
    <div class="col-12 col-md-6 col-lg-4">
        <div class="search-box-main">
            <form action="<%= searchPageURL.toString() %>" method="get">
                <div class="input-search-box animate" data-animation="fadeInUp" data-duration="300">
                    <input type="text" class="form-control" placeholder="<liferay-ui:message key="misk-search-by-keyword"/>" name="q" autocomplete="off" value="<%=query != null ? query : ""%>">
                    <button class="input-search-btn"><i class="icon-search"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>