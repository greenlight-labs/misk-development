<%@ include file="init.jsp" %>

<%@include file="includes/top_section.jsp" %>

<section data-scroll-section class="hotels-list">
    <div class="container">

        <%@include file="includes/search.jsp" %>

        <%@include file="listing.jsp" %>

        <%--<%@include file="includes/pagination.jsp" %>--%>
    </div>
</section>
