<div class="row">
    <div class="col-12">
        <%
            List<Hotel> hotels;

            if(Validator.isNotNull(query)){
                hotels = HotelLocalServiceUtil.findByListingTitle(query);
            }else{
                hotels = HotelLocalServiceUtil.getHotels(scopeGroupId);
            }

            if(hotels.size() > 0){
                for (Hotel curHotel : hotels) {
        %>
        <portlet:renderURL var="detailPageURL2">
            <portlet:param name="hotelId" value="<%= String.valueOf(curHotel.getHotelId()) %>" />
            <portlet:param name="mvcPath" value="/detail.jsp" />
        </portlet:renderURL>
        <div class="hotels-list-item">
            <div class="hotels-list-img">
                <img src="<%= curHotel.getListingImage(locale) %>" alt="" class="img-fluid">
            </div>
            <div class="hotels-list-content">
                <h2><%= curHotel.getListingTitle(locale) %></h2>
                <p><%= curHotel.getListingDescription(locale) %></p>
                <a href="<%= detailPageURL2 %>" class="btn btn-outline-primary">
                    <span><liferay-ui:message key="misk-read-more"/></span>
                    <i class="dot-line"></i>
                </a>
            </div>
        </div>
        <%
            }
        } else {
        %>
        <div class="text-center pt-5">
            <h4><liferay-ui:message key="misk-no-record-found" /></h4>
        </div>
        <%
            }
        %>
    </div>
</div>