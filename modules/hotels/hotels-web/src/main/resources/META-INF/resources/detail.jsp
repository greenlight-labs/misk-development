<%@include file="init.jsp" %>

<%
    long hotelId = ParamUtil.getLong(renderRequest, "hotelId");

    Hotel hotel = null;
    if (hotelId > 0) {
        try {
            hotel = HotelLocalServiceUtil.getHotel(hotelId);
        } catch (PortalException e) {
            //e.printStackTrace();
        }
    }
%>

<%
    if (hotel == null) {
%>
<h4>No Record Found.</h4>
<%
} else {
%>

<section class="residence-detail-banner-sec" data-scroll-section>
    <div class="home-image-wrapper residence-image-wrapper">
        <div class="homeImgBanner">
            <div class="item image">
                <div class="slide-image slide-media">
                    <div class="gradient-box"></div>
                    <div class="inner-banner-img d-none d-md-block" style="background-image: url('<%=hotel.getBannerDesktopImage(locale)%>')"></div>
                    <div class="inner-banner-img d-block d-md-none" style="background-image: url('<%=hotel.getBannerMobileImage(locale)%>')"></div>
                    <div class="container banner-container">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="banner-animate"><span class="misk-big-text-mask animate" data-animation="maskTextUp" data-duration="500"><span class="animate-up"><%=hotel.getBannerTitle(locale)%></span></span></h3>
                                <h1 class="banner-animate"><span class="misk-big-text-mask animate" data-animation="maskTextUp" data-duration="600"><span class="animate-up"><%=hotel.getBannerSubtitle(locale)%></span></span></h1>
                                <div class="banner-content banner-animate">
                                    <p class="lead misk-big-text-mask animate" data-animation="maskTextUp" data-duration="700"><span class="animate-up"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-shape"><img src="/assets/images/banner-shape.png" alt="" class="img-fluid"></div>
    </div>
</section>
<section class="inner-content-section pb-0" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <div class="left-content">
                    <h4><%=hotel.getSection2Title(locale)%></h4>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="left-content">
                    <p><%=hotel.getSection2Description(locale)%></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="innovation-section" data-scroll-section>
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="innovation-image-slider">
                    <div class="slider innovation-explore-slider">
                        <div>
                            <div class="innovate-img-box">
                                <div class="img-inno"><img src="/assets/images/hotel-room-01.png" alt="" class="img-fluid"></div>
                                <div class="img-content">
                                    <h4 class="animate" data-animation="fadeInUp" data-duration="500"><a href="javascript:">Comfy and Relaxing Interior</a></h4>
                                    <p class="animate" data-animation="fadeInUp" data-duration="500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a data-animation="fadeInUp" data-duration="500" href="javascript:" class="btn btn-outline-white translate-animate animate" tabindex="0"><span><liferay-ui:message key="misk-explore-more"/></span><i class="dot-line"></i></a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="innovate-img-box">
                                <div class="img-inno"><img src="/assets/images/hotel-room-02.png" alt="" class="img-fluid"></div>
                                <div class="img-content">
                                    <h4 class="animate" data-animation="fadeInUp" data-duration="500"><a href="javascript:">Luxurious Rooms</a></h4>
                                    <p class="animate" data-animation="fadeInUp" data-duration="500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a data-animation="fadeInUp" data-duration="500" href="javascript:" class="btn btn-outline-white translate-animate animate" tabindex="0"><span><liferay-ui:message key="misk-explore-more"/></span><i class="dot-line"></i></a>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="innovate-img-box">
                                <div class="img-inno"><img src="/assets/images/hotel-room-03.png" alt="" class="img-fluid"></div>
                                <div class="img-content">
                                    <h4 class="animate" data-animation="fadeInUp" data-duration="500"><a href="javascript:">Comfy and Relaxing Interior</a></h4>
                                    <p class="animate" data-animation="fadeInUp" data-duration="500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a data-animation="fadeInUp" data-duration="500" href="javascript:" class="btn btn-outline-white translate-animate animate" tabindex="0"><span><liferay-ui:message key="misk-explore-more"/></span><i class="dot-line"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="vision-slider position-relative" data-scroll-section>
    <div class="hotel-vision-slider">
        <div class="container-fluid p-0">
            <div class="row no-gutters">
                <div class="col-md-3">
                    <div class="VisionTextArrow">
                        <button class="portLeftArrow slick-arrow" aria-disabled="false" style=""><i class="icon-left-arrow"></i></button>
                        <div class="number-box"><span class="pagingInfo">1 / 4</span></div>
                        <button class="portRightArrow slick-arrow" aria-disabled="false" style=""><i class="icon-right-arrow"></i></button>
                    </div>
                    <div class="vision-text-mn vision-text-slider bg-primary">
                        <div class="vision-text-box">
                            <h3><span class="translate-animate">Amenities and Services</span></h3>
                            <p><span class="translate-animate">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt</span></p>
                        </div>
                        <div class="vision-text-box">
                            <h3><span class="translate-animate">Amenities and Services</span></h3>
                            <p><span class="translate-animate">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt</span></p>
                        </div>
                        <div class="vision-text-box">
                            <h3><span class="translate-animate">Amenities and Services</span></h3>
                            <p><span class="translate-animate">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt</span></p>
                        </div>
                        <div class="vision-text-box">
                            <h3><span class="translate-animate">Amenities and Services</span></h3>
                            <p><span class="translate-animate">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <ul class="vision-img-mn vision-img-slider">
                        <li>
                            <img src="/assets/images/hotel-services-01.jpg" alt="" class="img-fluid">
                        </li>
                        <li>
                            <img src="/assets/images/hotel-services-02.jpg" alt="" class="img-fluid">
                        </li>
                        <li>
                            <img src="/assets/images/hotel-services-01.jpg" alt="" class="img-fluid">
                        </li>
                        <li>
                            <img src="/assets/images/hotel-services-02.jpg" alt="" class="img-fluid">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="inner-content-section" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <div class="left-content">
                    <h4><%=hotel.getSection5Title(locale)%></h4>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="left-content">
                    <%=hotel.getSection5Description(locale)%>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="creativity-section modern-fitness-section" data-scroll-section>
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="creativity-slider-wrap">
                    <div class="cd-slider-wrapper">
                        <ul class="cd-slider" data-step1="M1920,1000h-2c0,0,0-213,0-423s0-377,0-377h1c0.6,0,1,0.4,1,1V800z" data-step2="M1400,800H724c0,0-297-155-297-423C427,139,728,0,728,0h671c0.6,0,1,0.4,1,1V800z" data-step3="M1400,800H0c0,0,1-213,1-423S1,0,1,0h1398c0.6,0,1,0.4,1,1V800z" data-step4="M-2,800h2c0,0,0-213,0-423S0,0,0,0h-1c-0.6,0-1,0.4-1,1V800z" data-step5="M0,800h676c0,0,297-155,297-423C973,139,672,0,672,0L1,0C0.4,0,0,0.4,0,1L0,800z" data-step6="M0,800h1400c0,0-1-213-1-423s0-377,0-377L1,0C0.4,0,0,0.4,0,1L0,800z">
                            <li class="visible">
                                <div class="cd-svg-wrapper">
                                    <svg viewBox="0 0 1398 998">
                                        <defs>
                                            <clipPath id="cd-image-1">
                                                <path id="cd-changing-path-1" d="M1920,1000H0c0,0,1-213,1-423S1,0,1,0h1398c0.6,0,1,0.4,1,1V800z"/>
                                            </clipPath>
                                        </defs>
                                        <image width="100%" height="1000" clip-path="url(#cd-image-1)" xlink:href="/assets/images/hotel-restaurant.jpg"></image>
                                    </svg>
                                </div>
                            </li>
                            <li>
                                <div class="cd-svg-wrapper">
                                    <svg viewBox="0 0 1398 998">
                                        <defs>
                                            <clipPath id="cd-image-2">
                                                <path id="cd-changing-path-2" d="M1920,1000H0c0,0,1-213,1-423S1,0,1,0h1398c0.6,0,1,0.4,1,1V800z"/>
                                            </clipPath>
                                        </defs>

                                        <image width="100%" height="1000" clip-path="url(#cd-image-2)" xlink:href="/assets/images/modern-image-2.jpg"></image>
                                    </svg>
                                </div>
                            </li>
                            <li>
                                <div class="cd-svg-wrapper">
                                    <svg viewBox="0 0 1398 998">
                                        <defs>
                                            <clipPath id="cd-image-3">
                                                <path id="cd-changing-path-3" d="M1920,1000H0c0,0,1-213,1-423S1,0,1,0h1398c0.6,0,1,0.4,1,1V800z"/>
                                            </clipPath>
                                        </defs>

                                        <image width="100%" height="1000" clip-path="url(#cd-image-3)" xlink:href="/assets/images/modern-image-3.jpg"></image>
                                    </svg>
                                </div>
                            </li>
                            <li>
                                <div class="cd-svg-wrapper">
                                    <svg viewBox="0 0 1398 998">
                                        <defs>
                                            <clipPath id="cd-image-4">
                                                <path id="cd-changing-path-4" d="M1920,1000H0c0,0,1-213,1-423S1,0,1,0h1398c0.6,0,1,0.4,1,1V800z"/>
                                            </clipPath>
                                        </defs>

                                        <image width="100%" height="1000" clip-path="url(#cd-image-4)" xlink:href="/assets/images/modern-image.jpg"></image>
                                    </svg>
                                </div>
                            </li>
                            <li>
                                <div class="cd-svg-wrapper">
                                    <svg viewBox="0 0 1398 998">
                                        <defs>
                                            <clipPath id="cd-image-5">
                                                <path id="cd-changing-path-5" d="M1920,1000H0c0,0,1-213,1-423S1,0,1,0h1398c0.6,0,1,0.4,1,1V800z"/>
                                            </clipPath>
                                        </defs>

                                        <image width="100%" height="1000" clip-path="url(#cd-image-5)" xlink:href="/assets/images/modern-image-2.jpg"></image>
                                    </svg>
                                </div>
                            </li>

                        </ul>
                        <ul class="cd-slider-navigation" aria-hidden="true">
                            <li><a href="#0" class="next-slide">Next</a></li>
                            <li><a href="#0" class="prev-slide">Prev</a></li>
                        </ul>
                    </div>




                    <div data-scroll data-scroll-speed="4" class="modren-text-wrap">


                        <div class="slider creativity-text-slider">

                            <div class="creative-box">
                                <div class="modern-content-box">
                                    <h3>The <br>Restaurant
                                    </h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a rhoncus urna. Nam sed rutrum est.</p>
                                    <a href="javascript:" class="btn btn-outline-primary"><span><liferay-ui:message key="misk-read-more"/></span><i class="dot-line"></i></a>
                                </div>
                            </div>

                            <div class="creative-box">
                                <div class="modern-content-box">
                                    <h3> Modern fitness <br>
                                        <span>Gym facilities</span>
                                    </h3>
                                    <p>Nunc scelerisque sed magna sit amet varius. Class aptent taciti sociosqu ad.</p>
                                    <ul class="modern-icon-list">
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/fully-equiped-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully Equiped with <br> All Accessories</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/air-conditioned-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully <br> Air-conditioned</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/gym-instructor-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Gym Instructor <br> Available</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/changing-rooms-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Changing Rooms <br> And Lockers</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="creative-box">
                                <div class="modern-content-box">
                                    <h3> Modern fitness <br>
                                        <span>Gym facilities</span>
                                    </h3>
                                    <p>Nunc scelerisque sed magna sit amet varius. Class aptent taciti sociosqu ad.</p>
                                    <ul class="modern-icon-list">
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/fully-equiped-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully Equiped with <br> All Accessories</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/air-conditioned-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully <br> Air-conditioned</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/gym-instructor-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Gym Instructor <br> Available</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/changing-rooms-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Changing Rooms <br> And Lockers</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="creative-box">
                                <div class="modern-content-box">
                                    <h3> Modern fitness <br>
                                        <span>Gym facilities</span>
                                    </h3>
                                    <p>Nunc scelerisque sed magna sit amet varius. Class aptent taciti sociosqu ad.</p>
                                    <ul class="modern-icon-list">
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/fully-equiped-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully Equiped with <br> All Accessories</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/air-conditioned-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully <br> Air-conditioned</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/gym-instructor-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Gym Instructor <br> Available</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/changing-rooms-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Changing Rooms <br> And Lockers</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="creative-box">
                                <div class="modern-content-box">
                                    <h3> Modern fitness <br>
                                        <span>Gym facilities</span>
                                    </h3>
                                    <p>Nunc scelerisque sed magna sit amet varius. Class aptent taciti sociosqu ad.</p>
                                    <ul class="modern-icon-list">
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/fully-equiped-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully Equiped with <br> All Accessories</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/air-conditioned-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Fully <br> Air-conditioned</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/gym-instructor-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Gym Instructor <br> Available</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="modern-icon-box">
                                                <i><img src="/assets/svgs/changing-rooms-icon.svg" class="img-fluid" alt=""></i>
                                                <span>Changing Rooms <br> And Lockers</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="portTextArrow">
                            <button class="portLeftArrow"><i class="icon-left-arrow"></i></button>
                            <div class="number-box"><span class="pagingInfo text-black-50"></span></div>
                            <button class="portRightArrow"><i class="icon-right-arrow"></i></button>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>

<%@include file="includes/footer_section.jsp" %>

<%
    }
%>