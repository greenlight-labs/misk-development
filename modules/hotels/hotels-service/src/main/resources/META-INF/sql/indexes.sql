create index IX_52A2A67A on misk_hotels (groupId);
create index IX_D7258758 on misk_hotels (listingTitle[$COLUMN_LENGTH:4000$]);
create index IX_AB505A24 on misk_hotels (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_4CB119A6 on misk_hotels (uuid_[$COLUMN_LENGTH:75$], groupId);