/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package hotels.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import hotels.model.Hotel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Hotel in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class HotelCacheModel implements CacheModel<Hotel>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof HotelCacheModel)) {
			return false;
		}

		HotelCacheModel hotelCacheModel = (HotelCacheModel)object;

		if (hotelId == hotelCacheModel.hotelId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, hotelId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(39);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", hotelId=");
		sb.append(hotelId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", listingTitle=");
		sb.append(listingTitle);
		sb.append(", listingDescription=");
		sb.append(listingDescription);
		sb.append(", bannerTitle=");
		sb.append(bannerTitle);
		sb.append(", bannerSubtitle=");
		sb.append(bannerSubtitle);
		sb.append(", bannerDesktopImage=");
		sb.append(bannerDesktopImage);
		sb.append(", bannerMobileImage=");
		sb.append(bannerMobileImage);
		sb.append(", section2Title=");
		sb.append(section2Title);
		sb.append(", section2Description=");
		sb.append(section2Description);
		sb.append(", section5Title=");
		sb.append(section5Title);
		sb.append(", section5Description=");
		sb.append(section5Description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Hotel toEntityModel() {
		HotelImpl hotelImpl = new HotelImpl();

		if (uuid == null) {
			hotelImpl.setUuid("");
		}
		else {
			hotelImpl.setUuid(uuid);
		}

		hotelImpl.setHotelId(hotelId);
		hotelImpl.setGroupId(groupId);
		hotelImpl.setCompanyId(companyId);
		hotelImpl.setUserId(userId);

		if (userName == null) {
			hotelImpl.setUserName("");
		}
		else {
			hotelImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			hotelImpl.setCreateDate(null);
		}
		else {
			hotelImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			hotelImpl.setModifiedDate(null);
		}
		else {
			hotelImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (listingImage == null) {
			hotelImpl.setListingImage("");
		}
		else {
			hotelImpl.setListingImage(listingImage);
		}

		if (listingTitle == null) {
			hotelImpl.setListingTitle("");
		}
		else {
			hotelImpl.setListingTitle(listingTitle);
		}

		if (listingDescription == null) {
			hotelImpl.setListingDescription("");
		}
		else {
			hotelImpl.setListingDescription(listingDescription);
		}

		if (bannerTitle == null) {
			hotelImpl.setBannerTitle("");
		}
		else {
			hotelImpl.setBannerTitle(bannerTitle);
		}

		if (bannerSubtitle == null) {
			hotelImpl.setBannerSubtitle("");
		}
		else {
			hotelImpl.setBannerSubtitle(bannerSubtitle);
		}

		if (bannerDesktopImage == null) {
			hotelImpl.setBannerDesktopImage("");
		}
		else {
			hotelImpl.setBannerDesktopImage(bannerDesktopImage);
		}

		if (bannerMobileImage == null) {
			hotelImpl.setBannerMobileImage("");
		}
		else {
			hotelImpl.setBannerMobileImage(bannerMobileImage);
		}

		if (section2Title == null) {
			hotelImpl.setSection2Title("");
		}
		else {
			hotelImpl.setSection2Title(section2Title);
		}

		if (section2Description == null) {
			hotelImpl.setSection2Description("");
		}
		else {
			hotelImpl.setSection2Description(section2Description);
		}

		if (section5Title == null) {
			hotelImpl.setSection5Title("");
		}
		else {
			hotelImpl.setSection5Title(section5Title);
		}

		if (section5Description == null) {
			hotelImpl.setSection5Description("");
		}
		else {
			hotelImpl.setSection5Description(section5Description);
		}

		hotelImpl.resetOriginalValues();

		return hotelImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput)
		throws ClassNotFoundException, IOException {

		uuid = objectInput.readUTF();

		hotelId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		listingImage = objectInput.readUTF();
		listingTitle = objectInput.readUTF();
		listingDescription = objectInput.readUTF();
		bannerTitle = objectInput.readUTF();
		bannerSubtitle = objectInput.readUTF();
		bannerDesktopImage = objectInput.readUTF();
		bannerMobileImage = objectInput.readUTF();
		section2Title = objectInput.readUTF();
		section2Description = objectInput.readUTF();
		section5Title = objectInput.readUTF();
		section5Description = (String)objectInput.readObject();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(hotelId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (listingTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingTitle);
		}

		if (listingDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingDescription);
		}

		if (bannerTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerTitle);
		}

		if (bannerSubtitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerSubtitle);
		}

		if (bannerDesktopImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerDesktopImage);
		}

		if (bannerMobileImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(bannerMobileImage);
		}

		if (section2Title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Title);
		}

		if (section2Description == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section2Description);
		}

		if (section5Title == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(section5Title);
		}

		if (section5Description == null) {
			objectOutput.writeObject("");
		}
		else {
			objectOutput.writeObject(section5Description);
		}
	}

	public String uuid;
	public long hotelId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String listingImage;
	public String listingTitle;
	public String listingDescription;
	public String bannerTitle;
	public String bannerSubtitle;
	public String bannerDesktopImage;
	public String bannerMobileImage;
	public String section2Title;
	public String section2Description;
	public String section5Title;
	public String section5Description;

}