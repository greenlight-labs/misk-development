/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package hotels.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import hotels.model.Hotel;
import hotels.service.base.HotelServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the hotel remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>hotels.service.HotelService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see HotelServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=hotel",
		"json.web.service.context.path=Hotel"
	},
	service = AopService.class
)
public class HotelServiceImpl extends HotelServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>hotels.service.HotelServiceUtil</code> to access the hotel remote service.
	 */

	/* *
	 * get hotels by group
	 * */
	public List<Hotel> getHotels(long groupId) {

		return hotelPersistence.findByGroupId(groupId);
	}

	public List<Hotel> getHotels(long groupId, int start, int end,
										 OrderByComparator<Hotel> obc) {

		return hotelPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Hotel> getHotels(long groupId, int start, int end) {

		return hotelPersistence.findByGroupId(groupId, start, end);
	}

	public int getHotelsCount(long groupId) {

		return hotelPersistence.countByGroupId(groupId);
	}
}