/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package hotels.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import hotels.exception.HotelListingDescriptionException;
import hotels.exception.HotelListingImageException;
import hotels.exception.HotelListingTitleException;
import hotels.model.Hotel;
import hotels.service.base.HotelLocalServiceBaseImpl;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the hotel local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>hotels.service.HotelLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see HotelLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=hotels.model.Hotel", service = AopService.class
)
public class HotelLocalServiceImpl extends HotelLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>hotels.service.HotelLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>hotels.service.HotelLocalServiceUtil</code>.
	 */

	public Hotel addHotel(long userId,
						  Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap,
						  Map<Locale, String> bannerTitleMap, Map<Locale, String> bannerSubtitleMap, Map<Locale, String> bannerDesktopImageMap, Map<Locale, String> bannerMobileImageMap,
						  Map<Locale, String> section2TitleMap, Map<Locale, String> section2DescriptionMap,
						  Map<Locale, String> section5TitleMap, Map<Locale, String> section5DescriptionMap,
						  ServiceContext serviceContext) throws PortalException {

		long groupId = serviceContext.getScopeGroupId();

		User user = userLocalService.getUserById(userId);

		Date now = new Date();

		validate(listingImageMap, listingTitleMap, listingDescriptionMap);

		long hotelId = counterLocalService.increment();

		Hotel hotel = hotelPersistence.create(hotelId);

		hotel.setUuid(serviceContext.getUuid());
		hotel.setUserId(userId);
		hotel.setGroupId(groupId);
		hotel.setCompanyId(user.getCompanyId());
		hotel.setUserName(user.getFullName());
		hotel.setCreateDate(serviceContext.getCreateDate(now));
		hotel.setModifiedDate(serviceContext.getModifiedDate(now));

		hotel.setListingImageMap(listingImageMap);
		hotel.setListingTitleMap(listingTitleMap);
		hotel.setListingDescriptionMap(listingDescriptionMap);

		hotel.setBannerTitleMap(bannerTitleMap);
		hotel.setBannerSubtitleMap(bannerSubtitleMap);
		hotel.setBannerDesktopImageMap(bannerDesktopImageMap);
		hotel.setBannerMobileImageMap(bannerMobileImageMap);

		hotel.setSection2TitleMap(section2TitleMap);
		hotel.setSection2DescriptionMap(section2DescriptionMap);

		hotel.setSection5TitleMap(section5TitleMap);
		hotel.setSection5DescriptionMap(section5DescriptionMap);

		hotel.setExpandoBridgeAttributes(serviceContext);

		hotelPersistence.update(hotel);
		hotelPersistence.clearCache();

		return hotel;
	}

	public Hotel updateHotel(long userId, long hotelId,
							 		Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap,
									 Map<Locale, String> bannerTitleMap, Map<Locale, String> bannerSubtitleMap, Map<Locale, String> bannerDesktopImageMap, Map<Locale, String> bannerMobileImageMap,
									 Map<Locale, String> section2TitleMap, Map<Locale, String> section2DescriptionMap,
									 Map<Locale, String> section5TitleMap, Map<Locale, String> section5DescriptionMap,
									 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Date now = new Date();

		validate(listingImageMap, listingTitleMap, listingDescriptionMap);

		Hotel hotel = getHotel(hotelId);

		User user = userLocalService.getUser(userId);

		hotel.setUserId(userId);
		hotel.setUserName(user.getFullName());
		hotel.setModifiedDate(serviceContext.getModifiedDate(now));

		hotel.setListingImageMap(listingImageMap);
		hotel.setListingTitleMap(listingTitleMap);
		hotel.setListingDescriptionMap(listingDescriptionMap);

		hotel.setBannerTitleMap(bannerTitleMap);
		hotel.setBannerSubtitleMap(bannerSubtitleMap);
		hotel.setBannerDesktopImageMap(bannerDesktopImageMap);
		hotel.setBannerMobileImageMap(bannerMobileImageMap);

		hotel.setSection2TitleMap(section2TitleMap);
		hotel.setSection2DescriptionMap(section2DescriptionMap);

		hotel.setSection5TitleMap(section5TitleMap);
		hotel.setSection5DescriptionMap(section5DescriptionMap);

		hotel.setExpandoBridgeAttributes(serviceContext);

		hotelPersistence.update(hotel);
		hotelPersistence.clearCache();

		return hotel;
	}

	public Hotel deleteHotel(long hotelId,
									 ServiceContext serviceContext) throws PortalException,
			SystemException {

		Hotel hotel = getHotel(hotelId);
		hotel = deleteHotel(hotel);

		return hotel;
	}

	public List<Hotel> getHotels(long groupId) {

		return hotelPersistence.findByGroupId(groupId);
	}

	public List<Hotel> getHotels(long groupId, int start, int end,
										 OrderByComparator<Hotel> obc) {

		return hotelPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Hotel> getHotels(long groupId, int start, int end) {

		return hotelPersistence.findByGroupId(groupId, start, end);
	}

	public int getHotelsCount(long groupId) {

		return hotelPersistence.countByGroupId(groupId);
	}

	public List<Hotel> findByListingTitle(String listingTitle) {

		return hotelPersistence.findByListingTitle(listingTitle);
	}

	public List<Hotel> findByListingTitle(String listingTitle, int start, int end,
										 OrderByComparator<Hotel> obc) {

		return hotelPersistence.findByListingTitle(listingTitle, start, end, obc);
	}

	public List<Hotel> findByListingTitle(String listingTitle, int start, int end) {

		return hotelPersistence.findByListingTitle(listingTitle, start, end);
	}

	public int countByListingTitle(String listingTitle) {

		return hotelPersistence.countByListingTitle(listingTitle);
	}

	protected void validate(
			Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap
	) throws PortalException {

		Locale locale = LocaleUtil.getSiteDefault();

		String listingImage = listingImageMap.get(locale);

		if (Validator.isNull(listingImage)) {
			throw new HotelListingImageException();
		}

		String listingTitle = listingTitleMap.get(locale);

		if (Validator.isNull(listingTitle)) {
			throw new HotelListingTitleException();
		}

		String listingDescription = listingDescriptionMap.get(locale);

		if (Validator.isNull(listingDescription)) {
			throw new HotelListingDescriptionException();
		}

	}
}