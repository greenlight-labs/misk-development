/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package hotels.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Hotel}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Hotel
 * @generated
 */
public class HotelWrapper
	extends BaseModelWrapper<Hotel> implements Hotel, ModelWrapper<Hotel> {

	public HotelWrapper(Hotel hotel) {
		super(hotel);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("hotelId", getHotelId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("listingImage", getListingImage());
		attributes.put("listingTitle", getListingTitle());
		attributes.put("listingDescription", getListingDescription());
		attributes.put("bannerTitle", getBannerTitle());
		attributes.put("bannerSubtitle", getBannerSubtitle());
		attributes.put("bannerDesktopImage", getBannerDesktopImage());
		attributes.put("bannerMobileImage", getBannerMobileImage());
		attributes.put("section2Title", getSection2Title());
		attributes.put("section2Description", getSection2Description());
		attributes.put("section5Title", getSection5Title());
		attributes.put("section5Description", getSection5Description());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long hotelId = (Long)attributes.get("hotelId");

		if (hotelId != null) {
			setHotelId(hotelId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String listingImage = (String)attributes.get("listingImage");

		if (listingImage != null) {
			setListingImage(listingImage);
		}

		String listingTitle = (String)attributes.get("listingTitle");

		if (listingTitle != null) {
			setListingTitle(listingTitle);
		}

		String listingDescription = (String)attributes.get(
			"listingDescription");

		if (listingDescription != null) {
			setListingDescription(listingDescription);
		}

		String bannerTitle = (String)attributes.get("bannerTitle");

		if (bannerTitle != null) {
			setBannerTitle(bannerTitle);
		}

		String bannerSubtitle = (String)attributes.get("bannerSubtitle");

		if (bannerSubtitle != null) {
			setBannerSubtitle(bannerSubtitle);
		}

		String bannerDesktopImage = (String)attributes.get(
			"bannerDesktopImage");

		if (bannerDesktopImage != null) {
			setBannerDesktopImage(bannerDesktopImage);
		}

		String bannerMobileImage = (String)attributes.get("bannerMobileImage");

		if (bannerMobileImage != null) {
			setBannerMobileImage(bannerMobileImage);
		}

		String section2Title = (String)attributes.get("section2Title");

		if (section2Title != null) {
			setSection2Title(section2Title);
		}

		String section2Description = (String)attributes.get(
			"section2Description");

		if (section2Description != null) {
			setSection2Description(section2Description);
		}

		String section5Title = (String)attributes.get("section5Title");

		if (section5Title != null) {
			setSection5Title(section5Title);
		}

		String section5Description = (String)attributes.get(
			"section5Description");

		if (section5Description != null) {
			setSection5Description(section5Description);
		}
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the banner desktop image of this hotel.
	 *
	 * @return the banner desktop image of this hotel
	 */
	@Override
	public String getBannerDesktopImage() {
		return model.getBannerDesktopImage();
	}

	/**
	 * Returns the localized banner desktop image of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner desktop image of this hotel
	 */
	@Override
	public String getBannerDesktopImage(java.util.Locale locale) {
		return model.getBannerDesktopImage(locale);
	}

	/**
	 * Returns the localized banner desktop image of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner desktop image of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerDesktopImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getBannerDesktopImage(locale, useDefault);
	}

	/**
	 * Returns the localized banner desktop image of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner desktop image of this hotel
	 */
	@Override
	public String getBannerDesktopImage(String languageId) {
		return model.getBannerDesktopImage(languageId);
	}

	/**
	 * Returns the localized banner desktop image of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner desktop image of this hotel
	 */
	@Override
	public String getBannerDesktopImage(String languageId, boolean useDefault) {
		return model.getBannerDesktopImage(languageId, useDefault);
	}

	@Override
	public String getBannerDesktopImageCurrentLanguageId() {
		return model.getBannerDesktopImageCurrentLanguageId();
	}

	@Override
	public String getBannerDesktopImageCurrentValue() {
		return model.getBannerDesktopImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner desktop images of this hotel.
	 *
	 * @return the locales and localized banner desktop images of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getBannerDesktopImageMap() {
		return model.getBannerDesktopImageMap();
	}

	/**
	 * Returns the banner mobile image of this hotel.
	 *
	 * @return the banner mobile image of this hotel
	 */
	@Override
	public String getBannerMobileImage() {
		return model.getBannerMobileImage();
	}

	/**
	 * Returns the localized banner mobile image of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner mobile image of this hotel
	 */
	@Override
	public String getBannerMobileImage(java.util.Locale locale) {
		return model.getBannerMobileImage(locale);
	}

	/**
	 * Returns the localized banner mobile image of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner mobile image of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerMobileImage(
		java.util.Locale locale, boolean useDefault) {

		return model.getBannerMobileImage(locale, useDefault);
	}

	/**
	 * Returns the localized banner mobile image of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner mobile image of this hotel
	 */
	@Override
	public String getBannerMobileImage(String languageId) {
		return model.getBannerMobileImage(languageId);
	}

	/**
	 * Returns the localized banner mobile image of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner mobile image of this hotel
	 */
	@Override
	public String getBannerMobileImage(String languageId, boolean useDefault) {
		return model.getBannerMobileImage(languageId, useDefault);
	}

	@Override
	public String getBannerMobileImageCurrentLanguageId() {
		return model.getBannerMobileImageCurrentLanguageId();
	}

	@Override
	public String getBannerMobileImageCurrentValue() {
		return model.getBannerMobileImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner mobile images of this hotel.
	 *
	 * @return the locales and localized banner mobile images of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getBannerMobileImageMap() {
		return model.getBannerMobileImageMap();
	}

	/**
	 * Returns the banner subtitle of this hotel.
	 *
	 * @return the banner subtitle of this hotel
	 */
	@Override
	public String getBannerSubtitle() {
		return model.getBannerSubtitle();
	}

	/**
	 * Returns the localized banner subtitle of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner subtitle of this hotel
	 */
	@Override
	public String getBannerSubtitle(java.util.Locale locale) {
		return model.getBannerSubtitle(locale);
	}

	/**
	 * Returns the localized banner subtitle of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner subtitle of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerSubtitle(
		java.util.Locale locale, boolean useDefault) {

		return model.getBannerSubtitle(locale, useDefault);
	}

	/**
	 * Returns the localized banner subtitle of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner subtitle of this hotel
	 */
	@Override
	public String getBannerSubtitle(String languageId) {
		return model.getBannerSubtitle(languageId);
	}

	/**
	 * Returns the localized banner subtitle of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner subtitle of this hotel
	 */
	@Override
	public String getBannerSubtitle(String languageId, boolean useDefault) {
		return model.getBannerSubtitle(languageId, useDefault);
	}

	@Override
	public String getBannerSubtitleCurrentLanguageId() {
		return model.getBannerSubtitleCurrentLanguageId();
	}

	@Override
	public String getBannerSubtitleCurrentValue() {
		return model.getBannerSubtitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner subtitles of this hotel.
	 *
	 * @return the locales and localized banner subtitles of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getBannerSubtitleMap() {
		return model.getBannerSubtitleMap();
	}

	/**
	 * Returns the banner title of this hotel.
	 *
	 * @return the banner title of this hotel
	 */
	@Override
	public String getBannerTitle() {
		return model.getBannerTitle();
	}

	/**
	 * Returns the localized banner title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized banner title of this hotel
	 */
	@Override
	public String getBannerTitle(java.util.Locale locale) {
		return model.getBannerTitle(locale);
	}

	/**
	 * Returns the localized banner title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner title of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getBannerTitle(java.util.Locale locale, boolean useDefault) {
		return model.getBannerTitle(locale, useDefault);
	}

	/**
	 * Returns the localized banner title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized banner title of this hotel
	 */
	@Override
	public String getBannerTitle(String languageId) {
		return model.getBannerTitle(languageId);
	}

	/**
	 * Returns the localized banner title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized banner title of this hotel
	 */
	@Override
	public String getBannerTitle(String languageId, boolean useDefault) {
		return model.getBannerTitle(languageId, useDefault);
	}

	@Override
	public String getBannerTitleCurrentLanguageId() {
		return model.getBannerTitleCurrentLanguageId();
	}

	@Override
	public String getBannerTitleCurrentValue() {
		return model.getBannerTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized banner titles of this hotel.
	 *
	 * @return the locales and localized banner titles of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getBannerTitleMap() {
		return model.getBannerTitleMap();
	}

	/**
	 * Returns the company ID of this hotel.
	 *
	 * @return the company ID of this hotel
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this hotel.
	 *
	 * @return the create date of this hotel
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the group ID of this hotel.
	 *
	 * @return the group ID of this hotel
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the hotel ID of this hotel.
	 *
	 * @return the hotel ID of this hotel
	 */
	@Override
	public long getHotelId() {
		return model.getHotelId();
	}

	/**
	 * Returns the listing description of this hotel.
	 *
	 * @return the listing description of this hotel
	 */
	@Override
	public String getListingDescription() {
		return model.getListingDescription();
	}

	/**
	 * Returns the localized listing description of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing description of this hotel
	 */
	@Override
	public String getListingDescription(java.util.Locale locale) {
		return model.getListingDescription(locale);
	}

	/**
	 * Returns the localized listing description of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingDescription(
		java.util.Locale locale, boolean useDefault) {

		return model.getListingDescription(locale, useDefault);
	}

	/**
	 * Returns the localized listing description of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing description of this hotel
	 */
	@Override
	public String getListingDescription(String languageId) {
		return model.getListingDescription(languageId);
	}

	/**
	 * Returns the localized listing description of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this hotel
	 */
	@Override
	public String getListingDescription(String languageId, boolean useDefault) {
		return model.getListingDescription(languageId, useDefault);
	}

	@Override
	public String getListingDescriptionCurrentLanguageId() {
		return model.getListingDescriptionCurrentLanguageId();
	}

	@Override
	public String getListingDescriptionCurrentValue() {
		return model.getListingDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing descriptions of this hotel.
	 *
	 * @return the locales and localized listing descriptions of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getListingDescriptionMap() {
		return model.getListingDescriptionMap();
	}

	/**
	 * Returns the listing image of this hotel.
	 *
	 * @return the listing image of this hotel
	 */
	@Override
	public String getListingImage() {
		return model.getListingImage();
	}

	/**
	 * Returns the localized listing image of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing image of this hotel
	 */
	@Override
	public String getListingImage(java.util.Locale locale) {
		return model.getListingImage(locale);
	}

	/**
	 * Returns the localized listing image of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingImage(java.util.Locale locale, boolean useDefault) {
		return model.getListingImage(locale, useDefault);
	}

	/**
	 * Returns the localized listing image of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing image of this hotel
	 */
	@Override
	public String getListingImage(String languageId) {
		return model.getListingImage(languageId);
	}

	/**
	 * Returns the localized listing image of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this hotel
	 */
	@Override
	public String getListingImage(String languageId, boolean useDefault) {
		return model.getListingImage(languageId, useDefault);
	}

	@Override
	public String getListingImageCurrentLanguageId() {
		return model.getListingImageCurrentLanguageId();
	}

	@Override
	public String getListingImageCurrentValue() {
		return model.getListingImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing images of this hotel.
	 *
	 * @return the locales and localized listing images of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getListingImageMap() {
		return model.getListingImageMap();
	}

	/**
	 * Returns the listing title of this hotel.
	 *
	 * @return the listing title of this hotel
	 */
	@Override
	public String getListingTitle() {
		return model.getListingTitle();
	}

	/**
	 * Returns the localized listing title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing title of this hotel
	 */
	@Override
	public String getListingTitle(java.util.Locale locale) {
		return model.getListingTitle(locale);
	}

	/**
	 * Returns the localized listing title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingTitle(java.util.Locale locale, boolean useDefault) {
		return model.getListingTitle(locale, useDefault);
	}

	/**
	 * Returns the localized listing title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing title of this hotel
	 */
	@Override
	public String getListingTitle(String languageId) {
		return model.getListingTitle(languageId);
	}

	/**
	 * Returns the localized listing title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this hotel
	 */
	@Override
	public String getListingTitle(String languageId, boolean useDefault) {
		return model.getListingTitle(languageId, useDefault);
	}

	@Override
	public String getListingTitleCurrentLanguageId() {
		return model.getListingTitleCurrentLanguageId();
	}

	@Override
	public String getListingTitleCurrentValue() {
		return model.getListingTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing titles of this hotel.
	 *
	 * @return the locales and localized listing titles of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getListingTitleMap() {
		return model.getListingTitleMap();
	}

	/**
	 * Returns the modified date of this hotel.
	 *
	 * @return the modified date of this hotel
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this hotel.
	 *
	 * @return the primary key of this hotel
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the section2 description of this hotel.
	 *
	 * @return the section2 description of this hotel
	 */
	@Override
	public String getSection2Description() {
		return model.getSection2Description();
	}

	/**
	 * Returns the localized section2 description of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 description of this hotel
	 */
	@Override
	public String getSection2Description(java.util.Locale locale) {
		return model.getSection2Description(locale);
	}

	/**
	 * Returns the localized section2 description of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 description of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2Description(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2Description(locale, useDefault);
	}

	/**
	 * Returns the localized section2 description of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 description of this hotel
	 */
	@Override
	public String getSection2Description(String languageId) {
		return model.getSection2Description(languageId);
	}

	/**
	 * Returns the localized section2 description of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 description of this hotel
	 */
	@Override
	public String getSection2Description(
		String languageId, boolean useDefault) {

		return model.getSection2Description(languageId, useDefault);
	}

	@Override
	public String getSection2DescriptionCurrentLanguageId() {
		return model.getSection2DescriptionCurrentLanguageId();
	}

	@Override
	public String getSection2DescriptionCurrentValue() {
		return model.getSection2DescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 descriptions of this hotel.
	 *
	 * @return the locales and localized section2 descriptions of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getSection2DescriptionMap() {
		return model.getSection2DescriptionMap();
	}

	/**
	 * Returns the section2 title of this hotel.
	 *
	 * @return the section2 title of this hotel
	 */
	@Override
	public String getSection2Title() {
		return model.getSection2Title();
	}

	/**
	 * Returns the localized section2 title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section2 title of this hotel
	 */
	@Override
	public String getSection2Title(java.util.Locale locale) {
		return model.getSection2Title(locale);
	}

	/**
	 * Returns the localized section2 title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 title of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection2Title(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection2Title(locale, useDefault);
	}

	/**
	 * Returns the localized section2 title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section2 title of this hotel
	 */
	@Override
	public String getSection2Title(String languageId) {
		return model.getSection2Title(languageId);
	}

	/**
	 * Returns the localized section2 title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section2 title of this hotel
	 */
	@Override
	public String getSection2Title(String languageId, boolean useDefault) {
		return model.getSection2Title(languageId, useDefault);
	}

	@Override
	public String getSection2TitleCurrentLanguageId() {
		return model.getSection2TitleCurrentLanguageId();
	}

	@Override
	public String getSection2TitleCurrentValue() {
		return model.getSection2TitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section2 titles of this hotel.
	 *
	 * @return the locales and localized section2 titles of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getSection2TitleMap() {
		return model.getSection2TitleMap();
	}

	/**
	 * Returns the section5 description of this hotel.
	 *
	 * @return the section5 description of this hotel
	 */
	@Override
	public String getSection5Description() {
		return model.getSection5Description();
	}

	/**
	 * Returns the localized section5 description of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 description of this hotel
	 */
	@Override
	public String getSection5Description(java.util.Locale locale) {
		return model.getSection5Description(locale);
	}

	/**
	 * Returns the localized section5 description of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 description of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5Description(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5Description(locale, useDefault);
	}

	/**
	 * Returns the localized section5 description of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 description of this hotel
	 */
	@Override
	public String getSection5Description(String languageId) {
		return model.getSection5Description(languageId);
	}

	/**
	 * Returns the localized section5 description of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 description of this hotel
	 */
	@Override
	public String getSection5Description(
		String languageId, boolean useDefault) {

		return model.getSection5Description(languageId, useDefault);
	}

	@Override
	public String getSection5DescriptionCurrentLanguageId() {
		return model.getSection5DescriptionCurrentLanguageId();
	}

	@Override
	public String getSection5DescriptionCurrentValue() {
		return model.getSection5DescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 descriptions of this hotel.
	 *
	 * @return the locales and localized section5 descriptions of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getSection5DescriptionMap() {
		return model.getSection5DescriptionMap();
	}

	/**
	 * Returns the section5 title of this hotel.
	 *
	 * @return the section5 title of this hotel
	 */
	@Override
	public String getSection5Title() {
		return model.getSection5Title();
	}

	/**
	 * Returns the localized section5 title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized section5 title of this hotel
	 */
	@Override
	public String getSection5Title(java.util.Locale locale) {
		return model.getSection5Title(locale);
	}

	/**
	 * Returns the localized section5 title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 title of this hotel. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getSection5Title(
		java.util.Locale locale, boolean useDefault) {

		return model.getSection5Title(locale, useDefault);
	}

	/**
	 * Returns the localized section5 title of this hotel in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized section5 title of this hotel
	 */
	@Override
	public String getSection5Title(String languageId) {
		return model.getSection5Title(languageId);
	}

	/**
	 * Returns the localized section5 title of this hotel in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized section5 title of this hotel
	 */
	@Override
	public String getSection5Title(String languageId, boolean useDefault) {
		return model.getSection5Title(languageId, useDefault);
	}

	@Override
	public String getSection5TitleCurrentLanguageId() {
		return model.getSection5TitleCurrentLanguageId();
	}

	@Override
	public String getSection5TitleCurrentValue() {
		return model.getSection5TitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized section5 titles of this hotel.
	 *
	 * @return the locales and localized section5 titles of this hotel
	 */
	@Override
	public Map<java.util.Locale, String> getSection5TitleMap() {
		return model.getSection5TitleMap();
	}

	/**
	 * Returns the user ID of this hotel.
	 *
	 * @return the user ID of this hotel
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this hotel.
	 *
	 * @return the user name of this hotel
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this hotel.
	 *
	 * @return the user uuid of this hotel
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this hotel.
	 *
	 * @return the uuid of this hotel
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the banner desktop image of this hotel.
	 *
	 * @param bannerDesktopImage the banner desktop image of this hotel
	 */
	@Override
	public void setBannerDesktopImage(String bannerDesktopImage) {
		model.setBannerDesktopImage(bannerDesktopImage);
	}

	/**
	 * Sets the localized banner desktop image of this hotel in the language.
	 *
	 * @param bannerDesktopImage the localized banner desktop image of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerDesktopImage(
		String bannerDesktopImage, java.util.Locale locale) {

		model.setBannerDesktopImage(bannerDesktopImage, locale);
	}

	/**
	 * Sets the localized banner desktop image of this hotel in the language, and sets the default locale.
	 *
	 * @param bannerDesktopImage the localized banner desktop image of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerDesktopImage(
		String bannerDesktopImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerDesktopImage(bannerDesktopImage, locale, defaultLocale);
	}

	@Override
	public void setBannerDesktopImageCurrentLanguageId(String languageId) {
		model.setBannerDesktopImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner desktop images of this hotel from the map of locales and localized banner desktop images.
	 *
	 * @param bannerDesktopImageMap the locales and localized banner desktop images of this hotel
	 */
	@Override
	public void setBannerDesktopImageMap(
		Map<java.util.Locale, String> bannerDesktopImageMap) {

		model.setBannerDesktopImageMap(bannerDesktopImageMap);
	}

	/**
	 * Sets the localized banner desktop images of this hotel from the map of locales and localized banner desktop images, and sets the default locale.
	 *
	 * @param bannerDesktopImageMap the locales and localized banner desktop images of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerDesktopImageMap(
		Map<java.util.Locale, String> bannerDesktopImageMap,
		java.util.Locale defaultLocale) {

		model.setBannerDesktopImageMap(bannerDesktopImageMap, defaultLocale);
	}

	/**
	 * Sets the banner mobile image of this hotel.
	 *
	 * @param bannerMobileImage the banner mobile image of this hotel
	 */
	@Override
	public void setBannerMobileImage(String bannerMobileImage) {
		model.setBannerMobileImage(bannerMobileImage);
	}

	/**
	 * Sets the localized banner mobile image of this hotel in the language.
	 *
	 * @param bannerMobileImage the localized banner mobile image of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerMobileImage(
		String bannerMobileImage, java.util.Locale locale) {

		model.setBannerMobileImage(bannerMobileImage, locale);
	}

	/**
	 * Sets the localized banner mobile image of this hotel in the language, and sets the default locale.
	 *
	 * @param bannerMobileImage the localized banner mobile image of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerMobileImage(
		String bannerMobileImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerMobileImage(bannerMobileImage, locale, defaultLocale);
	}

	@Override
	public void setBannerMobileImageCurrentLanguageId(String languageId) {
		model.setBannerMobileImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner mobile images of this hotel from the map of locales and localized banner mobile images.
	 *
	 * @param bannerMobileImageMap the locales and localized banner mobile images of this hotel
	 */
	@Override
	public void setBannerMobileImageMap(
		Map<java.util.Locale, String> bannerMobileImageMap) {

		model.setBannerMobileImageMap(bannerMobileImageMap);
	}

	/**
	 * Sets the localized banner mobile images of this hotel from the map of locales and localized banner mobile images, and sets the default locale.
	 *
	 * @param bannerMobileImageMap the locales and localized banner mobile images of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerMobileImageMap(
		Map<java.util.Locale, String> bannerMobileImageMap,
		java.util.Locale defaultLocale) {

		model.setBannerMobileImageMap(bannerMobileImageMap, defaultLocale);
	}

	/**
	 * Sets the banner subtitle of this hotel.
	 *
	 * @param bannerSubtitle the banner subtitle of this hotel
	 */
	@Override
	public void setBannerSubtitle(String bannerSubtitle) {
		model.setBannerSubtitle(bannerSubtitle);
	}

	/**
	 * Sets the localized banner subtitle of this hotel in the language.
	 *
	 * @param bannerSubtitle the localized banner subtitle of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerSubtitle(
		String bannerSubtitle, java.util.Locale locale) {

		model.setBannerSubtitle(bannerSubtitle, locale);
	}

	/**
	 * Sets the localized banner subtitle of this hotel in the language, and sets the default locale.
	 *
	 * @param bannerSubtitle the localized banner subtitle of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerSubtitle(
		String bannerSubtitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerSubtitle(bannerSubtitle, locale, defaultLocale);
	}

	@Override
	public void setBannerSubtitleCurrentLanguageId(String languageId) {
		model.setBannerSubtitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner subtitles of this hotel from the map of locales and localized banner subtitles.
	 *
	 * @param bannerSubtitleMap the locales and localized banner subtitles of this hotel
	 */
	@Override
	public void setBannerSubtitleMap(
		Map<java.util.Locale, String> bannerSubtitleMap) {

		model.setBannerSubtitleMap(bannerSubtitleMap);
	}

	/**
	 * Sets the localized banner subtitles of this hotel from the map of locales and localized banner subtitles, and sets the default locale.
	 *
	 * @param bannerSubtitleMap the locales and localized banner subtitles of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerSubtitleMap(
		Map<java.util.Locale, String> bannerSubtitleMap,
		java.util.Locale defaultLocale) {

		model.setBannerSubtitleMap(bannerSubtitleMap, defaultLocale);
	}

	/**
	 * Sets the banner title of this hotel.
	 *
	 * @param bannerTitle the banner title of this hotel
	 */
	@Override
	public void setBannerTitle(String bannerTitle) {
		model.setBannerTitle(bannerTitle);
	}

	/**
	 * Sets the localized banner title of this hotel in the language.
	 *
	 * @param bannerTitle the localized banner title of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setBannerTitle(String bannerTitle, java.util.Locale locale) {
		model.setBannerTitle(bannerTitle, locale);
	}

	/**
	 * Sets the localized banner title of this hotel in the language, and sets the default locale.
	 *
	 * @param bannerTitle the localized banner title of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerTitle(
		String bannerTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setBannerTitle(bannerTitle, locale, defaultLocale);
	}

	@Override
	public void setBannerTitleCurrentLanguageId(String languageId) {
		model.setBannerTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized banner titles of this hotel from the map of locales and localized banner titles.
	 *
	 * @param bannerTitleMap the locales and localized banner titles of this hotel
	 */
	@Override
	public void setBannerTitleMap(
		Map<java.util.Locale, String> bannerTitleMap) {

		model.setBannerTitleMap(bannerTitleMap);
	}

	/**
	 * Sets the localized banner titles of this hotel from the map of locales and localized banner titles, and sets the default locale.
	 *
	 * @param bannerTitleMap the locales and localized banner titles of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setBannerTitleMap(
		Map<java.util.Locale, String> bannerTitleMap,
		java.util.Locale defaultLocale) {

		model.setBannerTitleMap(bannerTitleMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this hotel.
	 *
	 * @param companyId the company ID of this hotel
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this hotel.
	 *
	 * @param createDate the create date of this hotel
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this hotel.
	 *
	 * @param groupId the group ID of this hotel
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the hotel ID of this hotel.
	 *
	 * @param hotelId the hotel ID of this hotel
	 */
	@Override
	public void setHotelId(long hotelId) {
		model.setHotelId(hotelId);
	}

	/**
	 * Sets the listing description of this hotel.
	 *
	 * @param listingDescription the listing description of this hotel
	 */
	@Override
	public void setListingDescription(String listingDescription) {
		model.setListingDescription(listingDescription);
	}

	/**
	 * Sets the localized listing description of this hotel in the language.
	 *
	 * @param listingDescription the localized listing description of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingDescription(
		String listingDescription, java.util.Locale locale) {

		model.setListingDescription(listingDescription, locale);
	}

	/**
	 * Sets the localized listing description of this hotel in the language, and sets the default locale.
	 *
	 * @param listingDescription the localized listing description of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingDescription(
		String listingDescription, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingDescription(listingDescription, locale, defaultLocale);
	}

	@Override
	public void setListingDescriptionCurrentLanguageId(String languageId) {
		model.setListingDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing descriptions of this hotel from the map of locales and localized listing descriptions.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this hotel
	 */
	@Override
	public void setListingDescriptionMap(
		Map<java.util.Locale, String> listingDescriptionMap) {

		model.setListingDescriptionMap(listingDescriptionMap);
	}

	/**
	 * Sets the localized listing descriptions of this hotel from the map of locales and localized listing descriptions, and sets the default locale.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingDescriptionMap(
		Map<java.util.Locale, String> listingDescriptionMap,
		java.util.Locale defaultLocale) {

		model.setListingDescriptionMap(listingDescriptionMap, defaultLocale);
	}

	/**
	 * Sets the listing image of this hotel.
	 *
	 * @param listingImage the listing image of this hotel
	 */
	@Override
	public void setListingImage(String listingImage) {
		model.setListingImage(listingImage);
	}

	/**
	 * Sets the localized listing image of this hotel in the language.
	 *
	 * @param listingImage the localized listing image of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingImage(String listingImage, java.util.Locale locale) {
		model.setListingImage(listingImage, locale);
	}

	/**
	 * Sets the localized listing image of this hotel in the language, and sets the default locale.
	 *
	 * @param listingImage the localized listing image of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImage(
		String listingImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingImage(listingImage, locale, defaultLocale);
	}

	@Override
	public void setListingImageCurrentLanguageId(String languageId) {
		model.setListingImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing images of this hotel from the map of locales and localized listing images.
	 *
	 * @param listingImageMap the locales and localized listing images of this hotel
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap) {

		model.setListingImageMap(listingImageMap);
	}

	/**
	 * Sets the localized listing images of this hotel from the map of locales and localized listing images, and sets the default locale.
	 *
	 * @param listingImageMap the locales and localized listing images of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap,
		java.util.Locale defaultLocale) {

		model.setListingImageMap(listingImageMap, defaultLocale);
	}

	/**
	 * Sets the listing title of this hotel.
	 *
	 * @param listingTitle the listing title of this hotel
	 */
	@Override
	public void setListingTitle(String listingTitle) {
		model.setListingTitle(listingTitle);
	}

	/**
	 * Sets the localized listing title of this hotel in the language.
	 *
	 * @param listingTitle the localized listing title of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingTitle(String listingTitle, java.util.Locale locale) {
		model.setListingTitle(listingTitle, locale);
	}

	/**
	 * Sets the localized listing title of this hotel in the language, and sets the default locale.
	 *
	 * @param listingTitle the localized listing title of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingTitle(
		String listingTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingTitle(listingTitle, locale, defaultLocale);
	}

	@Override
	public void setListingTitleCurrentLanguageId(String languageId) {
		model.setListingTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing titles of this hotel from the map of locales and localized listing titles.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this hotel
	 */
	@Override
	public void setListingTitleMap(
		Map<java.util.Locale, String> listingTitleMap) {

		model.setListingTitleMap(listingTitleMap);
	}

	/**
	 * Sets the localized listing titles of this hotel from the map of locales and localized listing titles, and sets the default locale.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingTitleMap(
		Map<java.util.Locale, String> listingTitleMap,
		java.util.Locale defaultLocale) {

		model.setListingTitleMap(listingTitleMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this hotel.
	 *
	 * @param modifiedDate the modified date of this hotel
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this hotel.
	 *
	 * @param primaryKey the primary key of this hotel
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the section2 description of this hotel.
	 *
	 * @param section2Description the section2 description of this hotel
	 */
	@Override
	public void setSection2Description(String section2Description) {
		model.setSection2Description(section2Description);
	}

	/**
	 * Sets the localized section2 description of this hotel in the language.
	 *
	 * @param section2Description the localized section2 description of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2Description(
		String section2Description, java.util.Locale locale) {

		model.setSection2Description(section2Description, locale);
	}

	/**
	 * Sets the localized section2 description of this hotel in the language, and sets the default locale.
	 *
	 * @param section2Description the localized section2 description of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2Description(
		String section2Description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2Description(
			section2Description, locale, defaultLocale);
	}

	@Override
	public void setSection2DescriptionCurrentLanguageId(String languageId) {
		model.setSection2DescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 descriptions of this hotel from the map of locales and localized section2 descriptions.
	 *
	 * @param section2DescriptionMap the locales and localized section2 descriptions of this hotel
	 */
	@Override
	public void setSection2DescriptionMap(
		Map<java.util.Locale, String> section2DescriptionMap) {

		model.setSection2DescriptionMap(section2DescriptionMap);
	}

	/**
	 * Sets the localized section2 descriptions of this hotel from the map of locales and localized section2 descriptions, and sets the default locale.
	 *
	 * @param section2DescriptionMap the locales and localized section2 descriptions of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2DescriptionMap(
		Map<java.util.Locale, String> section2DescriptionMap,
		java.util.Locale defaultLocale) {

		model.setSection2DescriptionMap(section2DescriptionMap, defaultLocale);
	}

	/**
	 * Sets the section2 title of this hotel.
	 *
	 * @param section2Title the section2 title of this hotel
	 */
	@Override
	public void setSection2Title(String section2Title) {
		model.setSection2Title(section2Title);
	}

	/**
	 * Sets the localized section2 title of this hotel in the language.
	 *
	 * @param section2Title the localized section2 title of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection2Title(
		String section2Title, java.util.Locale locale) {

		model.setSection2Title(section2Title, locale);
	}

	/**
	 * Sets the localized section2 title of this hotel in the language, and sets the default locale.
	 *
	 * @param section2Title the localized section2 title of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2Title(
		String section2Title, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection2Title(section2Title, locale, defaultLocale);
	}

	@Override
	public void setSection2TitleCurrentLanguageId(String languageId) {
		model.setSection2TitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section2 titles of this hotel from the map of locales and localized section2 titles.
	 *
	 * @param section2TitleMap the locales and localized section2 titles of this hotel
	 */
	@Override
	public void setSection2TitleMap(
		Map<java.util.Locale, String> section2TitleMap) {

		model.setSection2TitleMap(section2TitleMap);
	}

	/**
	 * Sets the localized section2 titles of this hotel from the map of locales and localized section2 titles, and sets the default locale.
	 *
	 * @param section2TitleMap the locales and localized section2 titles of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection2TitleMap(
		Map<java.util.Locale, String> section2TitleMap,
		java.util.Locale defaultLocale) {

		model.setSection2TitleMap(section2TitleMap, defaultLocale);
	}

	/**
	 * Sets the section5 description of this hotel.
	 *
	 * @param section5Description the section5 description of this hotel
	 */
	@Override
	public void setSection5Description(String section5Description) {
		model.setSection5Description(section5Description);
	}

	/**
	 * Sets the localized section5 description of this hotel in the language.
	 *
	 * @param section5Description the localized section5 description of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5Description(
		String section5Description, java.util.Locale locale) {

		model.setSection5Description(section5Description, locale);
	}

	/**
	 * Sets the localized section5 description of this hotel in the language, and sets the default locale.
	 *
	 * @param section5Description the localized section5 description of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5Description(
		String section5Description, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5Description(
			section5Description, locale, defaultLocale);
	}

	@Override
	public void setSection5DescriptionCurrentLanguageId(String languageId) {
		model.setSection5DescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 descriptions of this hotel from the map of locales and localized section5 descriptions.
	 *
	 * @param section5DescriptionMap the locales and localized section5 descriptions of this hotel
	 */
	@Override
	public void setSection5DescriptionMap(
		Map<java.util.Locale, String> section5DescriptionMap) {

		model.setSection5DescriptionMap(section5DescriptionMap);
	}

	/**
	 * Sets the localized section5 descriptions of this hotel from the map of locales and localized section5 descriptions, and sets the default locale.
	 *
	 * @param section5DescriptionMap the locales and localized section5 descriptions of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5DescriptionMap(
		Map<java.util.Locale, String> section5DescriptionMap,
		java.util.Locale defaultLocale) {

		model.setSection5DescriptionMap(section5DescriptionMap, defaultLocale);
	}

	/**
	 * Sets the section5 title of this hotel.
	 *
	 * @param section5Title the section5 title of this hotel
	 */
	@Override
	public void setSection5Title(String section5Title) {
		model.setSection5Title(section5Title);
	}

	/**
	 * Sets the localized section5 title of this hotel in the language.
	 *
	 * @param section5Title the localized section5 title of this hotel
	 * @param locale the locale of the language
	 */
	@Override
	public void setSection5Title(
		String section5Title, java.util.Locale locale) {

		model.setSection5Title(section5Title, locale);
	}

	/**
	 * Sets the localized section5 title of this hotel in the language, and sets the default locale.
	 *
	 * @param section5Title the localized section5 title of this hotel
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5Title(
		String section5Title, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setSection5Title(section5Title, locale, defaultLocale);
	}

	@Override
	public void setSection5TitleCurrentLanguageId(String languageId) {
		model.setSection5TitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized section5 titles of this hotel from the map of locales and localized section5 titles.
	 *
	 * @param section5TitleMap the locales and localized section5 titles of this hotel
	 */
	@Override
	public void setSection5TitleMap(
		Map<java.util.Locale, String> section5TitleMap) {

		model.setSection5TitleMap(section5TitleMap);
	}

	/**
	 * Sets the localized section5 titles of this hotel from the map of locales and localized section5 titles, and sets the default locale.
	 *
	 * @param section5TitleMap the locales and localized section5 titles of this hotel
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setSection5TitleMap(
		Map<java.util.Locale, String> section5TitleMap,
		java.util.Locale defaultLocale) {

		model.setSection5TitleMap(section5TitleMap, defaultLocale);
	}

	/**
	 * Sets the user ID of this hotel.
	 *
	 * @param userId the user ID of this hotel
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this hotel.
	 *
	 * @param userName the user name of this hotel
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this hotel.
	 *
	 * @param userUuid the user uuid of this hotel
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this hotel.
	 *
	 * @param uuid the uuid of this hotel
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected HotelWrapper wrap(Hotel hotel) {
		return new HotelWrapper(hotel);
	}

}