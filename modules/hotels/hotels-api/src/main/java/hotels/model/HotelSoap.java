/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package hotels.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link hotels.service.http.HotelServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class HotelSoap implements Serializable {

	public static HotelSoap toSoapModel(Hotel model) {
		HotelSoap soapModel = new HotelSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setHotelId(model.getHotelId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setListingImage(model.getListingImage());
		soapModel.setListingTitle(model.getListingTitle());
		soapModel.setListingDescription(model.getListingDescription());
		soapModel.setBannerTitle(model.getBannerTitle());
		soapModel.setBannerSubtitle(model.getBannerSubtitle());
		soapModel.setBannerDesktopImage(model.getBannerDesktopImage());
		soapModel.setBannerMobileImage(model.getBannerMobileImage());
		soapModel.setSection2Title(model.getSection2Title());
		soapModel.setSection2Description(model.getSection2Description());
		soapModel.setSection5Title(model.getSection5Title());
		soapModel.setSection5Description(model.getSection5Description());

		return soapModel;
	}

	public static HotelSoap[] toSoapModels(Hotel[] models) {
		HotelSoap[] soapModels = new HotelSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static HotelSoap[][] toSoapModels(Hotel[][] models) {
		HotelSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new HotelSoap[models.length][models[0].length];
		}
		else {
			soapModels = new HotelSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static HotelSoap[] toSoapModels(List<Hotel> models) {
		List<HotelSoap> soapModels = new ArrayList<HotelSoap>(models.size());

		for (Hotel model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new HotelSoap[soapModels.size()]);
	}

	public HotelSoap() {
	}

	public long getPrimaryKey() {
		return _hotelId;
	}

	public void setPrimaryKey(long pk) {
		setHotelId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getHotelId() {
		return _hotelId;
	}

	public void setHotelId(long hotelId) {
		_hotelId = hotelId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getListingImage() {
		return _listingImage;
	}

	public void setListingImage(String listingImage) {
		_listingImage = listingImage;
	}

	public String getListingTitle() {
		return _listingTitle;
	}

	public void setListingTitle(String listingTitle) {
		_listingTitle = listingTitle;
	}

	public String getListingDescription() {
		return _listingDescription;
	}

	public void setListingDescription(String listingDescription) {
		_listingDescription = listingDescription;
	}

	public String getBannerTitle() {
		return _bannerTitle;
	}

	public void setBannerTitle(String bannerTitle) {
		_bannerTitle = bannerTitle;
	}

	public String getBannerSubtitle() {
		return _bannerSubtitle;
	}

	public void setBannerSubtitle(String bannerSubtitle) {
		_bannerSubtitle = bannerSubtitle;
	}

	public String getBannerDesktopImage() {
		return _bannerDesktopImage;
	}

	public void setBannerDesktopImage(String bannerDesktopImage) {
		_bannerDesktopImage = bannerDesktopImage;
	}

	public String getBannerMobileImage() {
		return _bannerMobileImage;
	}

	public void setBannerMobileImage(String bannerMobileImage) {
		_bannerMobileImage = bannerMobileImage;
	}

	public String getSection2Title() {
		return _section2Title;
	}

	public void setSection2Title(String section2Title) {
		_section2Title = section2Title;
	}

	public String getSection2Description() {
		return _section2Description;
	}

	public void setSection2Description(String section2Description) {
		_section2Description = section2Description;
	}

	public String getSection5Title() {
		return _section5Title;
	}

	public void setSection5Title(String section5Title) {
		_section5Title = section5Title;
	}

	public String getSection5Description() {
		return _section5Description;
	}

	public void setSection5Description(String section5Description) {
		_section5Description = section5Description;
	}

	private String _uuid;
	private long _hotelId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _listingImage;
	private String _listingTitle;
	private String _listingDescription;
	private String _bannerTitle;
	private String _bannerSubtitle;
	private String _bannerDesktopImage;
	private String _bannerMobileImage;
	private String _section2Title;
	private String _section2Description;
	private String _section5Title;
	private String _section5Description;

}