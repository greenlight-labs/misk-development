/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package hotels.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link HotelService}.
 *
 * @author Brian Wing Shun Chan
 * @see HotelService
 * @generated
 */
public class HotelServiceWrapper
	implements HotelService, ServiceWrapper<HotelService> {

	public HotelServiceWrapper(HotelService hotelService) {
		_hotelService = hotelService;
	}

	@Override
	public java.util.List<hotels.model.Hotel> getHotels(long groupId) {
		return _hotelService.getHotels(groupId);
	}

	@Override
	public java.util.List<hotels.model.Hotel> getHotels(
		long groupId, int start, int end) {

		return _hotelService.getHotels(groupId, start, end);
	}

	@Override
	public java.util.List<hotels.model.Hotel> getHotels(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<hotels.model.Hotel>
			obc) {

		return _hotelService.getHotels(groupId, start, end, obc);
	}

	@Override
	public int getHotelsCount(long groupId) {
		return _hotelService.getHotelsCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _hotelService.getOSGiServiceIdentifier();
	}

	@Override
	public HotelService getWrappedService() {
		return _hotelService;
	}

	@Override
	public void setWrappedService(HotelService hotelService) {
		_hotelService = hotelService;
	}

	private HotelService _hotelService;

}