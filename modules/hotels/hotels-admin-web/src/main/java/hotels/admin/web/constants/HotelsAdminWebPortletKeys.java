package hotels.admin.web.constants;

/**
 * @author tz
 */
public class HotelsAdminWebPortletKeys {

	public static final String HOTELSADMINWEB =
		"hotels_admin_web_HotelsAdminWebPortlet";

}