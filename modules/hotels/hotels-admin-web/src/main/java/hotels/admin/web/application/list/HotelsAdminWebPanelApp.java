package hotels.admin.web.application.list;

import hotels.admin.web.constants.HotelsAdminWebPanelCategoryKeys;
import hotels.admin.web.constants.HotelsAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + HotelsAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class HotelsAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return HotelsAdminWebPortletKeys.HOTELSADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + HotelsAdminWebPortletKeys.HOTELSADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}