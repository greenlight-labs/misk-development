package hotels.admin.web.portlet;

import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import hotels.admin.web.constants.HotelsAdminWebPortletKeys;
import hotels.model.Hotel;
import hotels.service.HotelLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=HotelsAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + HotelsAdminWebPortletKeys.HOTELSADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class HotelsAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addHotel(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Hotel.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");

		Map<Locale, String> bannerTitleMap = LocalizationUtil.getLocalizationMap(request, "bannerTitle");
		Map<Locale, String> bannerSubtitleMap = LocalizationUtil.getLocalizationMap(request, "bannerSubtitle");
		Map<Locale, String> bannerDesktopImageMap = LocalizationUtil.getLocalizationMap(request, "bannerDesktopImage");
		Map<Locale, String> bannerMobileImageMap = LocalizationUtil.getLocalizationMap(request, "bannerMobileImage");
		Map<Locale, String> section2TitleMap = LocalizationUtil.getLocalizationMap(request, "section2Title");
		Map<Locale, String> section2DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section2Description");
		Map<Locale, String> section5TitleMap = LocalizationUtil.getLocalizationMap(request, "section5Title");
		Map<Locale, String> section5DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section5Description");

		try {
			_hotelLocalService.addHotel(serviceContext.getUserId(),
					listingImageMap, listingTitleMap, listingDescriptionMap,
					bannerTitleMap, bannerSubtitleMap, bannerDesktopImageMap, bannerMobileImageMap,
					section2TitleMap, section2DescriptionMap,
					section5TitleMap, section5DescriptionMap,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(HotelsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_hotel.jsp");
		}
	}

	public void updateHotel(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Hotel.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long hotelId = ParamUtil.getLong(request, "hotelId");

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");

		Map<Locale, String> bannerTitleMap = LocalizationUtil.getLocalizationMap(request, "bannerTitle");
		Map<Locale, String> bannerSubtitleMap = LocalizationUtil.getLocalizationMap(request, "bannerSubtitle");
		Map<Locale, String> bannerDesktopImageMap = LocalizationUtil.getLocalizationMap(request, "bannerDesktopImage");
		Map<Locale, String> bannerMobileImageMap = LocalizationUtil.getLocalizationMap(request, "bannerMobileImage");
		Map<Locale, String> section2TitleMap = LocalizationUtil.getLocalizationMap(request, "section2Title");
		Map<Locale, String> section2DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section2Description");
		Map<Locale, String> section5TitleMap = LocalizationUtil.getLocalizationMap(request, "section5Title");
		Map<Locale, String> section5DescriptionMap = LocalizationUtil.getLocalizationMap(request, "section5Description");

		try {
			_hotelLocalService.updateHotel(serviceContext.getUserId(), hotelId,
					listingImageMap, listingTitleMap, listingDescriptionMap,
					bannerTitleMap, bannerSubtitleMap, bannerDesktopImageMap, bannerMobileImageMap,
					section2TitleMap, section2DescriptionMap,
					section5TitleMap, section5DescriptionMap,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(HotelsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_hotel.jsp");
		}
	}

	public void deleteHotel(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Hotel.class.getName(), request);

		long hotelId = ParamUtil.getLong(request, "hotelId");

		try {
			_hotelLocalService.deleteHotel(hotelId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(HotelsAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private HotelLocalService _hotelLocalService;

	@Reference
	private ItemSelector _itemSelector;
}