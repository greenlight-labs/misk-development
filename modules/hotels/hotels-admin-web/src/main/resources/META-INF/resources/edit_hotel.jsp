<%@include file = "init.jsp" %>

<%
    long hotelId = ParamUtil.getLong(request, "hotelId");

    Hotel hotel = null;

    if (hotelId > 0) {
        try {
            hotel = HotelLocalServiceUtil.getHotel(hotelId);
        } catch (PortalException e) {
            e.printStackTrace();
        }
    }
%>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<portlet:actionURL name='<%= hotel == null ? "addHotel" : "updateHotel" %>' var="editHotelURL" />

<div class="container-fluid-1280">

<aui:form action="<%= editHotelURL %>" name="fm">

    <aui:model-context bean="<%= hotel %>" model="<%= Hotel.class %>" />

    <aui:input type="hidden" name="hotelId"
               value='<%= hotel == null ? "" : hotel.getHotelId() %>' />

    <aui:fieldset-group markupView="lexicon">
        <aui:fieldset>
            <aui:input name="listingTitle" label="Listing Title" autoSize="true" helpMessage="Max 30 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="listingImage" label="Listing Image" helpMessage="Image Dimensions: 1549 x 850 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="listingImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="listingDescription" label="Listing Description" helpMessage="Max 160 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Banner Section">
            <aui:input name="bannerTitle" label="Title" helpMessage="Max 15 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="bannerSubtitle" label="Subtitle" helpMessage="Max 15 Characters (Recommended)">
                <aui:validator name="required"/>
            </aui:input>
            <aui:input name="bannerDesktopImage" label="Desktop Image" helpMessage="Image Dimensions: 1920 x 880 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="bannerDesktopImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
            <aui:input name="bannerMobileImage" label="Mobile Image" helpMessage="Image Dimensions: 1920 x 880 pixels">
                <aui:validator name="required"/>
                <aui:validator name="custom" errorMessage="Please upload image files only (jpeg, jpg, png, svg, gif)">
                    function(val) {
                        var ext = val.substring(val.lastIndexOf('.') + 1);
                        return ext == "jpeg" || ext == "jpg" || ext == "png" || ext == "svg" || ext == "gif";
                    }
                </aui:validator>
            </aui:input>
            <div class="button-holder">
                <button class="btn select-button btn-secondary" type="button" data-field-id="bannerMobileImage">
                    <span class="lfr-btn-label">Select</span>
                </button>
            </div>
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Section 2">
            <aui:input name="section2Title" label="Title" helpMessage="Max 65 Characters (Recommended)" />
            <aui:input name="section2Description" label="Description" helpMessage="Max 235 Characters (Recommended)" />
        </aui:fieldset>
        <aui:fieldset collapsed="<%= true %>" collapsible="<%= true %>" label="Section 5">
            <aui:input name="section5Title" label="Title" helpMessage="Max 65 Characters (Recommended)" />
            <aui:input name="section5Description" label="Description" helpMessage="Max 835 Characters (Recommended)" />
        </aui:fieldset>
    </aui:fieldset-group>

    <aui:button-row>
        <aui:button type="submit" />
        <aui:button onClick="<%= viewURL %>" type="cancel"  />
    </aui:button-row>
</aui:form>

</div>

<script type="text/javascript">
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<aui:script use="liferay-item-selector-dialog">
    const selectButtons = window.document.querySelectorAll( '.select-button' );
    for (let i = 0; i < selectButtons.length; i++) {
    selectButtons[i].addEventListener('click', function (event) {
            event.preventDefault();
            var element = event.currentTarget;
            var fieldId = element.dataset.fieldId;
            var itemSelectorDialog = new A.LiferayItemSelectorDialog
            (
                {
                    eventName: 'selectDocumentLibrary',
                    on: {
                        selectedItemChange: function(event) {
                            var selectedItem = event.newVal;
                            if(selectedItem)
                            {
                                var itemValue = selectedItem.value;
                                itemValue = itemValue.substring(0, itemValue.lastIndexOf("/"));
                                window['<portlet:namespace />'+fieldId].value=itemValue;
                                var currentEditLanguage = window['<portlet:namespace /><portlet:namespace />'+fieldId+'Menu']?.querySelector('.btn-section').innerText;
                                var editLanguage = null;
                                if(currentEditLanguage === 'en-US'){
                                    editLanguage = 'en_US';
                                }else if(currentEditLanguage === 'ar-SA'){
                                    editLanguage = 'ar_SA';
                               }
                                if(editLanguage){
                                    window['<portlet:namespace />'+fieldId+'_'+editLanguage].value=itemValue;
                                }
                            }
                        }
                    },
                    title: '<liferay-ui:message key="Select Image" />',
                    url: '${itemSelectorURL }'
                }
            );
            itemSelectorDialog.open();

        });
    }
</aui:script>