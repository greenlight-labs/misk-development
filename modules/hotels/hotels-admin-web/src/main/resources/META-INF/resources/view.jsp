<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="hotels-admin-buttons">
        <portlet:renderURL var="addHotelURL">
            <portlet:param name="mvcPath"
                           value="/edit_hotel.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addHotelURL.toString() %>"
                    value="Add Hotel"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= HotelLocalServiceUtil.getHotelsCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= HotelLocalServiceUtil.getHotels(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="hotels.model.Hotel" modelVar="hotel">

            <liferay-ui:search-container-column-text
                    name="Listing Title"
                    value="<%= HtmlUtil.escape(hotel.getListingTitle(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>