package socialwall.web.constants;

/**
 * @author tz
 */
public class SocialwallWebPortletKeys {

	public static final String SOCIALWALLWEB =
		"socialwall_web_SocialwallWebPortlet";

	public static final String LATESTPOSTS =
		"socialwall_web_LatestPostsPortlet";

}