package socialwall.web.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import socialwall.web.constants.SocialwallWebPortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.misk",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=LatestPosts",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/components/latest-posts-section.jsp",
		"javax.portlet.name=" + SocialwallWebPortletKeys.LATESTPOSTS,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class LatestPostsPortlet extends MVCPortlet {
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		System.out.println("LatestPostsPortlet.render()");
		super.render(renderRequest, renderResponse);
	}
}