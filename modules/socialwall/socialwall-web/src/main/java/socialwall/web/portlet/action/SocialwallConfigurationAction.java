package socialwall.web.portlet.action;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import socialwall.web.constants.SocialwallWebPortletKeys;
import org.osgi.service.component.annotations.Component;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + SocialwallWebPortletKeys.LATESTPOSTS
        },
        service = ConfigurationAction.class
)
public class SocialwallConfigurationAction extends DefaultConfigurationAction {
}
