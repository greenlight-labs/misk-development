export class TwitterOld {

    posts = [];
    api = 'https://graph.twitter.com';
    userId = '17841447582374265';
    fields = 'id,caption,media_type,media_url,username,permalink,timestamp';
    accessToken = 'IGQVJYb2w4WUNXd2J3NVBSYWxHRWtIakY5T3BIdG4zNzFlWjJ5Mmc1M0JHWGhQR2dfVFM4RFVJWXM0SDVaelhwemtIalR3cHRGSFVJLXdFY29CeS10aWdPbGc3X1NTQ0syTXdUY3paTGJCV0c3S2hBXwZDZD';
    url = null;
    container = null;
    container_id = 'social-wall-posts-container';

    constructor() {
        this.url = `${this.api}/${this.userId}/media?fields=${this.fields}&access_token=${this.accessToken}`;
        this.el = document.getElementById(this.container_id);
    }

    init() {
        let self = this;
        this.loadJSONP(this.url, function (data) {
            var i = 0,
                items = [];
            console.log(data);

            if (data.data) {
                for (i = 0; i < data.data.length; i += 1) {
                    items.push({
                        type: 'instagram',
                        id: data.data[i].id,
                        caption: data.data[i].caption,
                        media_type: data.data[i].media_type,
                        media_url: data.data[i].media_url,
                        username: data.data[i].username,
                        permalink: data.data[i].permalink,
                        thumbnail_url: data.data[i].thumbnail_url || null,
                        published: new Date(Number(data.data[i].timestamp) * 1000).toUTCString()
                    });
                }
                self.add(items);
                self.render(self.posts);
            }
        });
    }

    loadJSONP(url, callback) {
        var timestamp = 'callback' + new Date().getTime() + Math.round(Math.random() * 100),
            script = document.createElement('script');
        window[timestamp] = callback;
        script.src = url + '&callback=' + timestamp;
        document.getElementsByTagName('head')[0].appendChild(script);
    }

    add(items) {
        var i = 0;
        for (i = 0; i < items.length; i += 1) {
            this.posts.push(items[i]);
        }
    }

    render(items) {
        var i = 0,
            html = '';
        items.sort(function (a, b) {
            return Date.parse(b.published) - Date.parse(a.published);
        });
        for (i = 0; i < items.length; i += 1) {
            if(items[i].media_type === 'CAROUSEL_ALBUM'){
                continue;
            }
            if(items[i].media_type === 'IMAGE'){
                let media_url = items[i].media_url;
            }else if(items[i].media_type === 'VIDEO'){
                let media_url = items[i].thumbnail_url;
            }
            html += `
            <div class="col-12 col-md-4">
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a class="image-overlay" href="${items[i].permalink}" target="_blank">                                                
                            <img class="img-fluid" src="${items[i].media_url}" alt=""/>
                        </a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text twitter-color" href="${items[i].permalink}" target="_blank">
                            <i class="so-icon"><img src="/assets/svgs/instagram.svg" alt="" class="img-fluid"/></i>
                            <span>@${items[i].username}</span>
                        </a>
                        <h5>${this.removeHashTags(items[i].caption)}</h5>
                        <ul>
                            ${this.replaceHashTags(items[i].caption)}
                        </ul>
                    </div>
                </div>
            </div>
            `;
        }
        this.el.innerHTML = html;
    }

    timeSince(date) {
        var s = Math.floor((new Date() - date) / 1000),
            i = Math.floor(s / 31536000);
        if (i > 1) {
            return i + " years";
        }
        i = Math.floor(s / 2592000);
        if (i > 1) {
            return i + " months";
        }
        i = Math.floor(s / 86400);
        if (i > 1) {
            return i + " days";
        }
        i = Math.floor(s / 3600);
        if (i > 1) {
            return i + " hours";
        }
        i = Math.floor(s / 60);
        if (i > 1) {
            return i + " minutes";
        }
        return Math.floor(s) + " seconds";
    }

    findHashTags(text){
        return text.match(/#(\w)+/g);
    }

    removeHashTags(text){
        return text.replace(/##?(\w+)/g, '');
    }

    replaceHashTags(text){
        let hashTags = this.findHashTags(text);
        let html = '';
        for (let hashTag of hashTags) {
            hashTag = hashTag.replace(/##?/g, '');
            html += `<li><a href="https://www.instagram.com/explore/tags/${hashTag.toLowerCase()}/" target="_blank">#${hashTag}</a></li>`;
        }
        return html;
    }

    loading(){

    }
}