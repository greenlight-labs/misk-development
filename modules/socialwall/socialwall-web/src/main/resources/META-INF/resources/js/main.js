import {Instagram} from "./instagram.js";
import {Facebook} from "./facebook.js";
import {Twitter} from "./twitter.js";
import {Youtube} from "./youtube.js";

class Main{

    container = null;
    items = [];

    constructor() {
        let self = this;
        this.container = document.querySelector('#social-media-filter-section');
        let items = this['container'].querySelectorAll('a');
        this.items = items;
        Array.from(items, item => {
            item.addEventListener("click", function(event){
                let social_media_id = this.dataset.id;
                self.addActiveClass(event);
                self.update(social_media_id);
            });
        });
    }

    removeActiveClass(){
        Array.from(this.items, item => {
            //item.classList.remove('active');
            $(item).parent().removeClass('active');
        });
    }

    addActiveClass(event){
        let element = event.currentTarget;
        this.removeActiveClass();
        $(element).parent('li').addClass('active');
        //element.classList.add('active');
    }

    update(social_media_id){
        if(social_media_id === "facebook"){
            this.facebook();
        }else if(social_media_id === "instagram"){
            this.instagram();
        }else if(social_media_id === "youtube"){
            this.youtube();
        }else if(social_media_id === "twitter"){
            this.twitter();
        }
        this.updateScroll();
    }

    instagram(){
        let instagram = new Instagram();
        instagram.init();
    }

    facebook(){
        let facebook = new Facebook();
        facebook.init();
    }

    twitter(){
        let twitter = new Twitter();
        twitter.init();
    }

    youtube(){
        let youtube = new Youtube();
        youtube.init();
    }

    updateScroll(){
        setTimeout(function () {
            scroll.update();
        }, 1000);
    }
}

window.addEventListener('load', function() {
    const main = new Main();
    main.youtube();
    main.updateScroll();
});