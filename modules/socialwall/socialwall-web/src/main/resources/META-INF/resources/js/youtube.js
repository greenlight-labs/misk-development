export class Youtube {

    posts = [];
    api = 'https://www.googleapis.com/youtube/v3/playlistItems';
    playlistId = 'UUtjBay22pmN37xp3MH1ZtBQ';
    maxResults = 9;
    key = 'AIzaSyCwQ6pTfLB8TLA2foQHa3_i-mHb0uIYuX8';
    url = null;
    container = null;
    container_id = 'social-wall-posts-container';

    constructor() {
        this.url = `${this.api}?part=snippet&maxResults=${this.maxResults}&playlistId=${this.playlistId}&key=${this.key}`;
        this.el = document.getElementById(this.container_id);
    }

    init() {
        let self = this;
        this.loadJSONP(this.url, function (data) {
            var i = 0,
                items = [];
            console.log(data);

            if (data.items) {
                for (i = 0; i < data.items.length; i += 1) {
                    items.push({
                        type: 'youtube',
                        id: data.items[i].id,
                        caption: data.items[i].snippet.description,
                        title: data.items[i].snippet.title,
                        media_url: data.items[i].snippet.thumbnails.high.url,
                        username: data.items[i].snippet.channelTitle,
                        permalink: 'https://www.youtube.com/watch?v='+data.items[i].snippet.resourceId.videoId,
                        published: new Date(Number(data.items[i].snippet.publishedAt) * 1000).toUTCString()
                    });
                }
                self.add(items);
                self.render(self.posts);
            }
        });
    }

    loadJSONP(url, callback) {
        var timestamp = 'callback' + new Date().getTime() + Math.round(Math.random() * 100),
            script = document.createElement('script');
        window[timestamp] = callback;
        script.src = url + '&callback=' + timestamp;
        document.getElementsByTagName('head')[0].appendChild(script);
    }

    add(items) {
        var i = 0;
        for (i = 0; i < items.length; i += 1) {
            this.posts.push(items[i]);
        }
    }

    render(items) {
        var i = 0,
            html = '';
        items.sort(function (a, b) {
            return Date.parse(b.published) - Date.parse(a.published);
        });
        for (i = 0; i < items.length; i += 1) {
            if(items[i].media_type === 'CAROUSEL_ALBUM'){
                continue;
            }
            if(items[i].media_type === 'IMAGE'){
                let media_url = items[i].media_url;
            }else if(items[i].media_type === 'VIDEO'){
                let media_url = items[i].thumbnail_url;
            }
            html += `
            <div class="col-12 col-md-4">
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a class="image-overlay" href="${items[i].permalink}" target="_blank">                                                
                            <img class="img-fluid" src="${items[i].media_url}" alt=""/>
                        </a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text twitter-color" href="${items[i].permalink}" target="_blank">
                            <i class="so-icon"><img src="/assets/svgs/youtube.svg" alt="" class="img-fluid"/></i>
                            <span>@${items[i].username}</span>
                        </a>
                        <h5>${this.removeHashTags(items[i].title)}</h5>
                        <ul>
                            ${this.replaceHashTags(items[i].caption)}
                        </ul>
                    </div>
                </div>
            </div>
            `;
        }
        this.el.innerHTML = html;
    }

    timeSince(date) {
        var s = Math.floor((new Date() - date) / 1000),
            i = Math.floor(s / 31536000);
        if (i > 1) {
            return i + " years";
        }
        i = Math.floor(s / 2592000);
        if (i > 1) {
            return i + " months";
        }
        i = Math.floor(s / 86400);
        if (i > 1) {
            return i + " days";
        }
        i = Math.floor(s / 3600);
        if (i > 1) {
            return i + " hours";
        }
        i = Math.floor(s / 60);
        if (i > 1) {
            return i + " minutes";
        }
        return Math.floor(s) + " seconds";
    }

    findHashTags(text){
        return text.match(/#(\w)+/g);
    }

    removeHashTags(text){
        return text.replace(/##?(\w+)/g, '');
    }

    replaceHashTags(text){
        let hashTags = this.findHashTags(text);
        let html = '';
        if(hashTags && hashTags.length > 0) {
            for (let hashTag of hashTags) {
                hashTag = hashTag.replace(/##?/g, '');
                html += `<li><a href="https://www.youtube.com/explore/tags/${hashTag.toLowerCase()}/" target="_blank">#${hashTag}</a></li>`;
            }
        }
        return html;
    }

    loading(){

    }
}