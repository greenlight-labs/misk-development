<%@ include file="/init.jsp" %>

<%@ include file="top_section.jsp" %>

<section class="social-media-filter-section" id="social-media-filter-section" data-scroll-section>
	<div class="container">
		<div class="row">
			<%@ include file="filter_section.jsp" %>

			<div class="col-12 col-md-8 col-lg-9">
				<ul class="icons-text">
					<li class="active">
						<a href="javascript:" class="youtube-link" data-id="youtube"><i class="icon-youtube-play"></i><span class="socialText"><%=themeDisplay.getLanguageId().equals("ar_SA") ? "يوتوب" : "youtube" %></span></a>
					</li>
					<li>
						<a href="javascript:" class="facebook-link" data-id="facebook"><i class="icon-facebook"></i><span class="socialText"><%=themeDisplay.getLanguageId().equals("ar_SA") ? "فيسبوك" : "facebook" %></span></a>
					</li>
					<li>
						<a href="javascript:" class="instagram-link" data-id="instagram"><i class="icon-instagram"></i><span class="socialText"><%=themeDisplay.getLanguageId().equals("ar_SA") ? "انستغرام" : "instagram" %></span></a>
					</li>
					<li>
						<a href="javascript:" class="twitter-link" data-id="twitter"><i class="icon-twitter"></i><span class="socialText"><%=themeDisplay.getLanguageId().equals("ar_SA") ? "تويتر" : "twitter" %></span></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="social-wall-section" data-scroll-section>
	<div class="container">
		<div class="row" id="social-wall-posts-container">
		</div>
	</div>
</section>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/twitterFetcher/twitterFetcher_min.js"></script>
<script type="module" src="<%=request.getContextPath()%>/js/main.js"></script>