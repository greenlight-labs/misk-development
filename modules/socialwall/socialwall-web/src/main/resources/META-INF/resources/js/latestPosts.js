import {Instagram} from "./instagram.js";
import {Facebook} from "./facebook.js";
import {Twitter} from "./twitter.js";

class LatestPosts{

    container = null;
    items = [];

    constructor() {
        let self = this;
        this.container = document.querySelector('#social-media-img-slider-section');
    }

    instagram(callback){
        let self = this;
        let instagram = new Instagram();
        instagram.getLatestPosts(function (posts) {
            Array.prototype.push.apply(self.items,posts);
            callback();
        });
    }

    facebook(callback){
        let self = this;
        let facebook = new Facebook();
        facebook.getLatestPosts(function (posts) {
            Array.prototype.push.apply(self.items,posts);
            callback();
        });
    }

    twitter(callback){
        let self = this;
        let twitter = new Twitter();
        twitter.getLatestPosts(function (posts) {
            Array.prototype.push.apply(self.items,posts);
            callback();
        });
    }

    nl2br(text){
        return text.replace(/(?:\r\n|\r|\n)/g, '<br>');
    }

    replaceHashTags(text, socialType){
        const uw = String.raw`[\p{Alphabetic}\p{Mark}\p{Decimal_Number}\p{Connector_Punctuation}\p{Join_Control}]`; // uw = Unicode \w
        const regex = new RegExp(`(?:#(${uw}+)|${uw}+#)(?!${uw})`, "gu");
        let link = 'https://www.facebook.com/hashtag/$1/';
        if(socialType === 'facebook'){
            link = 'https://www.facebook.com/hashtag/$1/';
        }else if(socialType === 'twitter'){
            link = 'https://www.twitter.com/hashtag/$1/';
        }else if(socialType === 'instagram'){
            link = 'https://www.instagram.com/explore/tags/$1/';
        }
        const replacer = `<a style="color:#00b2e3;" href="${link}" target="_blank">#$1</a>`;
        return text.replace(regex, replacer);
    }

    shuffleArray(array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }

    render() {
        let items = this.items;
        items = this.shuffleArray(items);
        var i = 0,
            html = '';
        for (i = 0; i < items.length; i += 1) {
            let social_media_color_class = 'facebook-color';
            let social_media_svg_icon = '/assets/svgs/facebook.svg';
            let media_url = items[i].media_url;
            if(items[i].type === 'facebook'){
                social_media_color_class = 'facebook-color';
                social_media_svg_icon = '/assets/svgs/facebook.svg';
                if(items[i].media_type !== 'photo' && items[i].media_type !== 'status'){
                    continue;
                }
            }
            if(items[i].type === 'instagram'){
                social_media_color_class = 'instagram-color';
                social_media_svg_icon = '/assets/svgs/instagram.svg';
                if(items[i].media_type === 'CAROUSEL_ALBUM'){
                    continue;
                }
                if(items[i].media_type === 'IMAGE'){
                    media_url = items[i].media_url;
                }else if(items[i].media_type === 'VIDEO'){
                    media_url = items[i].thumbnail_url;
                }
            }
            if(items[i].type === 'twitter'){
                social_media_color_class = 'twitter-color';
                social_media_svg_icon = '/assets/svgs/twitter.svg';
            }
            html += `
            <div>
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a href="${items[i].permalink}" class="image-overlay" target="_blank">                            
                            <img class="img-fluid" src="${media_url}" alt="" style="height:25em;" />
                        </a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text ${social_media_color_class}" href="${items[i].permalink}" target="_blank">
                            <i class="so-icon"><img src="${social_media_svg_icon}" alt="" class="img-fluid"></i>
                            <span>@${items[i].username}</span>
                        </a>
                        <h5>${this.replaceHashTags(this.nl2br(items[i].caption), items[i].type)}</h5>                        
                    </div>
                </div>
            </div>                                  
            `;
        }
        this.container.innerHTML = html;
    }

    updateScroll(){
        setTimeout(function () {
            scroll.update();
        }, 1000);
    }
}

window.addEventListener('load', function() {
    const latestPosts = new LatestPosts();
    let facebookFetchDone = false;
    let instagramFetchDone = false;
    let twitterFetchDone = false;
    latestPosts.facebook(function () {
        facebookFetchDone = true;
        if(instagramFetchDone && twitterFetchDone){
            latestPosts.render();
            socialMediaImgSlider.slick('refresh');
            latestPosts.updateScroll();
        }
    });
    latestPosts.instagram(function () {
        instagramFetchDone = true;
        if(facebookFetchDone && twitterFetchDone){
            latestPosts.render();
            socialMediaImgSlider.slick('refresh');
            latestPosts.updateScroll();
        }
    });
    latestPosts.twitter(function () {
        twitterFetchDone = true;
        if(facebookFetchDone && instagramFetchDone){
            latestPosts.render();
            socialMediaImgSlider.slick('refresh');
            latestPosts.updateScroll();
        }
    });
});