<%@ include file="/init.jsp" %>

<%
    HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(request);
    String editMode = originalRequest.getParameter("p_l_mode");
%>

<% if (editMode != null && editMode.equalsIgnoreCase("EDIT")) { %>
<div class="page-editor__collection">
    <div class="row">
        <div class="col-12">
            <div class="page-editor__collection__block">
                <div class="page-editor__collection-item page-editor__topper">
                    <div class="page-editor__collection-item__border"><p class="page-editor__collection-item__title">Social Wall Posts</p></div>
                    <br><br>
                    <div class="alert alert-info portlet-configuration">
                        <aui:a href="<%= portletDisplay.getURLConfiguration() %>" label="please-configure-this-portlet-to-make-it-visible-to-all-users" onClick="<%= portletDisplay.getURLConfigurationJS() %>" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<% } else { %>
<div class="social-media-section" data-scroll-section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="related-news-heading">
                    <h3><%= sectionTitle.replaceAll("\n", "<br/>") %></h3>
                    <% if(buttonLabel != null && buttonLink != null) { %>
                    <a href="<%= buttonLink %>" class="btn btn-outline-primary animate" data-animation="fadeInUp" data-duration="500"><span><%= buttonLabel %></span><i class="dot-line"></i></a>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
    <div class="social-media-img-slider-section" id="social-media-img-slider-section"></div>
</div>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/twitterFetcher/twitterFetcher_min.js"></script>
<script type="module" src="<%=request.getContextPath()%>/js/latestPosts.js"></script>
<% } %>