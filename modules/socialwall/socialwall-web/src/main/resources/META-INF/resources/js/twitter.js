export class Twitter {

    posts = [];
    api = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    userId = 'tayyabgreenlig1';
    fields = 'trim_user=true&include_entities=false&include_rts=false&exclude_replies=true&count=9';
    tokenBearer = 'AAAAAAAAAAAAAAAAAAAAAEe2UgAAAAAA8Cdnit5YRLjR1KpuvbgLQHMJDHA%3DeRaGhAIwVIuRaJrk2ro50qQwRrcGRFie2SkPv0YkV4qMe4KB3H';
    url = null;
    container = null;
    container_id = 'social-wall-posts-container';

    constructor() {
        this.url = `${this.api}?${this.fields}&screen_name=${this.userId}`;
        this.el = document.getElementById(this.container_id);
    }

    init() {
        let self = this;

        var configProfile = {
            "profile": {"screenName": 'tayyabgreenlig1'},
            "domId": 'social-wall-posts-container',
            "maxTweets": 3,
            "enableLinks": false,
            "showUser": true,
            "showTime": true,
            "showImages": true,
            "showRetweet": false,
            "showInteraction": false,
            "customCallback": handleTweets,
            "dataOnly": true,
            "lang": 'en'
        };

        function handleTweets(tweets){
            var i = 0,
                items = [];
            if (tweets) {
                for (i = 0; i < tweets.length; i += 1) {
                    items.push({
                        type: 'twitter',
                        id: tweets[i].tid,
                        caption: tweets[i].tweetText,
                        media_url: tweets[i].image || '/assets/svgs/twitter.svg',
                        username: tweets[i].author_data.screen_name,
                        profile_url: tweets[i].author_data.profile_url,
                        permalink: tweets[i].permalinkURL,
                        published: new Date(Number(tweets[i].timestamp) * 1000).toUTCString()
                    });
                }
                self.add(items);
                self.render(self.posts);
            }
        }

        twitterFetcher.fetch(configProfile);
    }

    add(items) {
        var i = 0;
        for (i = 0; i < items.length; i += 1) {
            this.posts.push(items[i]);
        }
    }

    render(items) {
        var i = 0,
            html = '';
        items.sort(function (a, b) {
            return Date.parse(b.published) - Date.parse(a.published);
        });
        for (i = 0; i < items.length; i += 1) {
            html += `
            <div class="col-12 col-md-4">
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a class="image-overlay" href="${items[i].permalink}" target="_blank">                                                
                            <img class="img-fluid" src="${items[i].media_url}" alt="" style="height:25em;"/>
                        </a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text twitter-color" href="${items[i].profile_url}" target="_blank">
                            <i class="so-icon"><img src="/assets/svgs/twitter.svg" alt="" class="img-fluid"/></i>
                            <span>${items[i].username}</span>
                        </a>                        
                        <h5>${this.replaceHashTags(this.nl2br(items[i].caption))}</h5>                        
                    </div>
                </div>
            </div>
            `;
        }
        this.el.innerHTML = html;
    }

    nl2br(text){
        return text.replace(/(?:\r\n|\r|\n)/g, '<br>');
    }

    replaceHashTags(text){
        const uw = String.raw`[\p{Alphabetic}\p{Mark}\p{Decimal_Number}\p{Connector_Punctuation}\p{Join_Control}]`; // uw = Unicode \w
        const regex = new RegExp(`(?:#(${uw}+)|${uw}+#)(?!${uw})`, "gu");
        const replacer = `<a style="color:#00b2e3;" href="https://www.twitter.com/hashtag/$1/" target="_blank">#$1</a>`;
        return text.replace(regex, replacer);
    }

    getLatestPosts(callback){
        let items = [];

        var configProfile = {
            "profile": {"screenName": 'tayyabgreenlig1'},
            "domId": 'social-wall-posts-container',
            "maxTweets": 3,
            "enableLinks": false,
            "showUser": true,
            "showTime": true,
            "showImages": true,
            "showRetweet": false,
            "showInteraction": false,
            "customCallback": handleTweets,
            "dataOnly": true,
            "lang": 'en'
        };

        function handleTweets(tweets){
            var i = 0,
                items = [];
            if (tweets) {
                for (i = 0; i < tweets.length; i += 1) {
                    items.push({
                        type: 'twitter',
                        id: tweets[i].tid,
                        caption: tweets[i].tweetText,
                        media_type: 'status',
                        media_url: tweets[i].image || '/assets/svgs/twitter.svg',
                        username: tweets[i].author_data.screen_name.replace('@', ''),
                        profile_url: tweets[i].author_data.profile_url,
                        permalink: tweets[i].permalinkURL,
                        published: new Date(Number(tweets[i].timestamp) * 1000).toUTCString()
                    });
                }
                callback(items);
            }
        }

        twitterFetcher.fetch(configProfile);
    }
}