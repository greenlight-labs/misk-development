export class Facebook {

    posts = [];
    api = 'https://graph.facebook.com';
    userId = 'me';
    fields = 'id,permalink_url,picture,source,type,full_picture,message,created_time,from';
    accessToken = 'EAAHKzPw62icBAIm27cJrw6KU7GCDLEbf5BJZBwOGAR4JclYrEVJBuAKmg8iNyGaY31tnmTeFhdXgAGcN1nvqrLhMsFOWwCmqDyFNLMxWsgrgJBgen6SDZBJdGmuJEvS65VH4Xr3Dxp9wf2ZC7LFojeOdIDyZAZCl3JuMpqvGuIUIE1fK9ZCxKa';
    url = null;
    container = null;
    container_id = 'social-wall-posts-container';

    constructor() {
        this.url = `${this.api}/${this.userId}/posts?fields=${this.fields}&access_token=${this.accessToken}`;
        this.el = document.getElementById(this.container_id);
    }

    init() {
        let self = this;
        this.loadJSONP(this.url, function (data) {
            var i = 0,
                items = [];
            console.log(data);

            if (data.data) {
                for (i = 0; i < data.data.length; i += 1) {
                    items.push({
                        type: 'facebook',
                        id: data.data[i].id,
                        caption: data.data[i].message,
                        media_type: data.data[i].type,
                        media_url: data.data[i].full_picture || '/assets/svgs/facebook.svg',
                        username: data.data[i].from.name,
                        permalink: data.data[i].permalink_url,
                        thumbnail_url: data.data[i].thumbnail_url || null,
                        published: new Date(Number(data.data[i].created_time) * 1000).toUTCString()
                    });
                }
                self.add(items);
                self.render(self.posts);
            }
        });
    }

    loadJSONP(url, callback) {
        var timestamp = 'callback' + new Date().getTime() + Math.round(Math.random() * 100),
            script = document.createElement('script');
        window[timestamp] = callback;
        script.src = url + '&callback=' + timestamp;
        document.getElementsByTagName('head')[0].appendChild(script);
    }

    add(items) {
        var i = 0;
        for (i = 0; i < items.length; i += 1) {
            this.posts.push(items[i]);
        }
    }

    render(items) {
        var i = 0,
            html = '';
        items.sort(function (a, b) {
            return Date.parse(b.published) - Date.parse(a.published);
        });
        for (i = 0; i < items.length; i += 1) {
            if(items[i].media_type !== 'photo' && items[i].media_type !== 'status'){
                continue;
            }
            html += `
            <div class="col-12 col-md-4">
                <div class="social-media-img-slider-body">
                    <div class="social-media-img-box">
                        <a class="image-overlay" href="${items[i].permalink}" target="_blank">                                                
                            <img class="img-fluid" src="${items[i].media_url}" alt="" style="height:25em;" />
                        </a>
                    </div>
                    <div class="social-media-icon-box">
                        <a class="social-icon-text twitter-color" href="${items[i].permalink}" target="_blank">
                            <i class="so-icon"><img src="/assets/svgs/facebook.svg" alt="" class="img-fluid"/></i>
                            <span>@${items[i].username}</span>
                        </a>
                        <h5>${this.replaceHashTags(this.nl2br(items[i].caption))}</h5>                        
                    </div>
                </div>
            </div>
            `;
        }
        this.el.innerHTML = html;
    }

    timeSince(date) {
        var s = Math.floor((new Date() - date) / 1000),
            i = Math.floor(s / 31536000);
        if (i > 1) {
            return i + " years";
        }
        i = Math.floor(s / 2592000);
        if (i > 1) {
            return i + " months";
        }
        i = Math.floor(s / 86400);
        if (i > 1) {
            return i + " days";
        }
        i = Math.floor(s / 3600);
        if (i > 1) {
            return i + " hours";
        }
        i = Math.floor(s / 60);
        if (i > 1) {
            return i + " minutes";
        }
        return Math.floor(s) + " seconds";
    }

    /*findHashTags(text){
        return text.match(/#(\w)+/g);
    }

    removeHashTags(text){
        return text.replace(/#(\w+)/g, '');
    }

    replaceHashTags(text){
        let hashTags = this.findHashTags(text);
        let html = '';
        if(hashTags && hashTags.length > 0){
            for (let hashTag of hashTags) {
                hashTag = hashTag.replace(/##?/g, '');
                html += `<li><a href="https://www.facebook.com/hashtag/${hashTag.toLowerCase()}/" target="_blank">#${hashTag}</a></li>`;
            }
        }
        return html;
    }*/

    nl2br(text){
        return text.replace(/(?:\r\n|\r|\n)/g, '<br>');
    }

    replaceHashTags(text){
        const uw = String.raw`[\p{Alphabetic}\p{Mark}\p{Decimal_Number}\p{Connector_Punctuation}\p{Join_Control}]`; // uw = Unicode \w
        const regex = new RegExp(`(?:#(${uw}+)|${uw}+#)(?!${uw})`, "gu");
        const replacer = `<a style="color:#00b2e3;" href="https://www.facebook.com/hashtag/$1/" target="_blank">#$1</a>`;
        return text.replace(regex, replacer);
    }

    getLatestPosts(callback) {
        let self = this;
        let items = [];
        self.loadJSONP(self.url, function (data) {
            var i = 0;
            if (data.data) {
                for (i = 0; i < 2; i += 1) {
                    items.push({
                        type: 'facebook',
                        id: data.data[i].id,
                        caption: data.data[i].message,
                        media_type: data.data[i].type,
                        media_url: data.data[i].full_picture || '/assets/svgs/facebook.svg',
                        username: data.data[i].from.name,
                        permalink: data.data[i].permalink_url,
                        thumbnail_url: data.data[i].thumbnail_url || null,
                        published: new Date(Number(data.data[i].created_time) * 1000).toUTCString()
                    });
                }
            }
            callback(items);
        });
    }
}