package amenities.admin.web.portlet;

import amenities.admin.web.constants.AmenitiesAdminWebPortletKeys;
import amenities.model.Amenity;
import amenities.service.AmenityLocalService;
import com.liferay.item.selector.ItemSelector;
import com.liferay.item.selector.ItemSelectorReturnType;
import com.liferay.item.selector.criteria.URLItemSelectorReturnType;
import com.liferay.item.selector.criteria.image.criterion.ImageItemSelectorCriterion;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactory;
import com.liferay.portal.kernel.portlet.RequestBackedPortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.hidden",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.layout-cacheable=true",
		"com.liferay.portlet.private-request-attributes=false",
		"com.liferay.portlet.private-session-attributes=false",
		"com.liferay.portlet.render-weight=50",
		"com.liferay.portlet.use-default-template=true",
		"javax.portlet.display-name=AmenitiesAdminWeb",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + AmenitiesAdminWebPortletKeys.AMENITIESADMINWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class AmenitiesAdminWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		RequestBackedPortletURLFactory requestBackedPortletURLFactory = RequestBackedPortletURLFactoryUtil
				.create(renderRequest);

		ImageItemSelectorCriterion imageItemSelectorCriterion = new ImageItemSelectorCriterion();

		List<ItemSelectorReturnType> desiredItemSelectorReturnTypes = new ArrayList<>();
		desiredItemSelectorReturnTypes.add(new URLItemSelectorReturnType());

		imageItemSelectorCriterion.setDesiredItemSelectorReturnTypes(desiredItemSelectorReturnTypes);

		PortletURL itemSelectorURL = _itemSelector.getItemSelectorURL(requestBackedPortletURLFactory,
				"selectDocumentLibrary", imageItemSelectorCriterion);

		renderRequest.setAttribute("itemSelectorURL", itemSelectorURL);

		super.render(renderRequest, renderResponse);
	}

	public void addAmenity(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Amenity.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");
		Map<Locale, String> buttonLabelMap = LocalizationUtil.getLocalizationMap(request, "buttonLabel");
		Map<Locale, String> buttonLinkMap = LocalizationUtil.getLocalizationMap(request, "buttonLink");

		try {
			_amenityLocalService.addAmenity(serviceContext.getUserId(),
					listingImageMap, listingTitleMap, listingDescriptionMap, buttonLabelMap, buttonLinkMap,
					serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(AmenitiesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_amenity.jsp");
		}
	}

	public void updateAmenity(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Amenity.class.getName(), request);

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
				WebKeys.THEME_DISPLAY);

		long amenityId = ParamUtil.getLong(request, "amenityId");

		Map<Locale, String> listingImageMap = LocalizationUtil.getLocalizationMap(request, "listingImage");
		Map<Locale, String> listingTitleMap = LocalizationUtil.getLocalizationMap(request, "listingTitle");
		Map<Locale, String> listingDescriptionMap = LocalizationUtil.getLocalizationMap(request, "listingDescription");
		Map<Locale, String> buttonLabelMap = LocalizationUtil.getLocalizationMap(request, "buttonLabel");
		Map<Locale, String> buttonLinkMap = LocalizationUtil.getLocalizationMap(request, "buttonLink");

		try {
			_amenityLocalService.updateAmenity(serviceContext.getUserId(), amenityId,
					listingImageMap, listingTitleMap, listingDescriptionMap, buttonLabelMap, buttonLinkMap,
					serviceContext);

		} catch (PortalException pe) {

			Logger.getLogger(AmenitiesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);

			response.setRenderParameter(
					"mvcPath", "/edit_amenity.jsp");
		}
	}

	public void deleteAmenity(ActionRequest request, ActionResponse response)
			throws PortalException {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Amenity.class.getName(), request);

		long amenityId = ParamUtil.getLong(request, "amenityId");

		try {
			_amenityLocalService.deleteAmenity(amenityId, serviceContext);
		} catch (PortalException pe) {

			Logger.getLogger(AmenitiesAdminWebPortlet.class.getName()).log(
					Level.SEVERE, null, pe);
		}
	}

	@Reference
	private AmenityLocalService _amenityLocalService;

	@Reference
	private ItemSelector _itemSelector;
}