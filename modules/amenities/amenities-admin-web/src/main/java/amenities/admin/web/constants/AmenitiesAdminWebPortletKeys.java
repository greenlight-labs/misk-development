package amenities.admin.web.constants;

/**
 * @author tz
 */
public class AmenitiesAdminWebPortletKeys {

	public static final String AMENITIESADMINWEB =
		"amenities_admin_web_AmenitiesAdminWebPortlet";

}