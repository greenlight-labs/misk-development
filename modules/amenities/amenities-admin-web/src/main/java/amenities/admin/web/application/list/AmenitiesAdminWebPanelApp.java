package amenities.admin.web.application.list;

import amenities.admin.web.constants.AmenitiesAdminWebPanelCategoryKeys;
import amenities.admin.web.constants.AmenitiesAdminWebPortletKeys;

import com.liferay.application.list.BasePanelApp;
import com.liferay.application.list.PanelApp;
import com.liferay.portal.kernel.model.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author tz
 */
@Component(
	immediate = true,
	property = {
		"panel.app.order:Integer=100",
		"panel.category.key=" + AmenitiesAdminWebPanelCategoryKeys.CONTROL_PANEL_CATEGORY
	},
	service = PanelApp.class
)
public class AmenitiesAdminWebPanelApp extends BasePanelApp {

	@Override
	public String getPortletId() {
		return AmenitiesAdminWebPortletKeys.AMENITIESADMINWEB;
	}

	@Override
	@Reference(
		target = "(javax.portlet.name=" + AmenitiesAdminWebPortletKeys.AMENITIESADMINWEB + ")",
		unbind = "-"
	)
	public void setPortlet(Portlet portlet) {
		super.setPortlet(portlet);
	}

}