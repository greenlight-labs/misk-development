<%@include file="init.jsp"%>

<%
    String mvcPath = ParamUtil.getString(request, "mvcPath");

    ResultRow row = (ResultRow) request
            .getAttribute("SEARCH_CONTAINER_RESULT_ROW");

    Amenity amenity = (Amenity) row.getObject();
%>

<liferay-ui:icon-menu>

    <portlet:renderURL var="editURL">
        <portlet:param name="amenityId"
                       value="<%=String.valueOf(amenity.getAmenityId()) %>" />
        <portlet:param name="mvcPath"
                       value="/edit_amenity.jsp" />
    </portlet:renderURL>

    <liferay-ui:icon image="edit" message="Edit"
                     url="<%=editURL.toString() %>" />

    <portlet:actionURL name="deleteAmenity" var="deleteURL">
        <portlet:param name="amenityId"
                       value="<%= String.valueOf(amenity.getAmenityId()) %>" />
    </portlet:actionURL>

    <liferay-ui:icon-delete url="<%=deleteURL.toString() %>" />

</liferay-ui:icon-menu>