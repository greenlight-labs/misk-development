<%@ include file="init.jsp" %>

<div class="container-fluid-1280">

    <aui:button-row cssClass="amenities-admin-buttons">
        <portlet:renderURL var="addAmenityURL">
            <portlet:param name="mvcPath"
                           value="/edit_amenity.jsp"/>
            <portlet:param name="redirect" value="<%= "currentURL" %>"/>
        </portlet:renderURL>

        <aui:button onClick="<%= addAmenityURL.toString() %>"
                    value="Add Amenity"/>
    </aui:button-row>

    <liferay-ui:search-container total="<%= AmenityLocalServiceUtil.getAmenitiesCount(scopeGroupId) %>">
        <liferay-ui:search-container-results
                results="<%= AmenityLocalServiceUtil.getAmenities(scopeGroupId,
            searchContainer.getStart(), searchContainer.getEnd()) %>"/>

        <liferay-ui:search-container-row
                className="amenities.model.Amenity" modelVar="amenity">

            <liferay-ui:search-container-column-text
                    name="Listing Title"
                    value="<%= HtmlUtil.escape(amenity.getListingTitle(locale)) %>"
            />

            <liferay-ui:search-container-column-jsp
                    align="right"
                    path="/actions.jsp"/>

        </liferay-ui:search-container-row>

        <liferay-ui:search-iterator/>
    </liferay-ui:search-container>
</div>