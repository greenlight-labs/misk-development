/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.service;

import amenities.model.Amenity;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

/**
 * Provides the local service utility for Amenity. This utility wraps
 * <code>amenities.service.impl.AmenityLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AmenityLocalService
 * @generated
 */
public class AmenityLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>amenities.service.impl.AmenityLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the amenity to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenity the amenity
	 * @return the amenity that was added
	 */
	public static Amenity addAmenity(Amenity amenity) {
		return getService().addAmenity(amenity);
	}

	public static Amenity addAmenity(
			long userId, Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> ButtonLabelMap,
			Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addAmenity(
			userId, listingImageMap, listingTitleMap, listingDescriptionMap,
			ButtonLabelMap, ButtonLinkMap, serviceContext);
	}

	/**
	 * Creates a new amenity with the primary key. Does not add the amenity to the database.
	 *
	 * @param amenityId the primary key for the new amenity
	 * @return the new amenity
	 */
	public static Amenity createAmenity(long amenityId) {
		return getService().createAmenity(amenityId);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel createPersistedModel(
			Serializable primaryKeyObj)
		throws PortalException {

		return getService().createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the amenity from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenity the amenity
	 * @return the amenity that was removed
	 */
	public static Amenity deleteAmenity(Amenity amenity) {
		return getService().deleteAmenity(amenity);
	}

	/**
	 * Deletes the amenity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenityId the primary key of the amenity
	 * @return the amenity that was removed
	 * @throws PortalException if a amenity with the primary key could not be found
	 */
	public static Amenity deleteAmenity(long amenityId) throws PortalException {
		return getService().deleteAmenity(amenityId);
	}

	public static Amenity deleteAmenity(
			long amenityId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().deleteAmenity(amenityId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel deletePersistedModel(
			PersistedModel persistedModel)
		throws PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>amenities.model.impl.AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>amenities.model.impl.AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static Amenity fetchAmenity(long amenityId) {
		return getService().fetchAmenity(amenityId);
	}

	/**
	 * Returns the amenity matching the UUID and group.
	 *
	 * @param uuid the amenity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchAmenityByUuidAndGroupId(
		String uuid, long groupId) {

		return getService().fetchAmenityByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the amenities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>amenities.model.impl.AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @return the range of amenities
	 */
	public static List<Amenity> getAmenities(int start, int end) {
		return getService().getAmenities(start, end);
	}

	public static List<Amenity> getAmenities(long groupId) {
		return getService().getAmenities(groupId);
	}

	public static List<Amenity> getAmenities(long groupId, int start, int end) {
		return getService().getAmenities(groupId, start, end);
	}

	public static List<Amenity> getAmenities(
		long groupId, int start, int end, OrderByComparator<Amenity> obc) {

		return getService().getAmenities(groupId, start, end, obc);
	}

	/**
	 * Returns all the amenities matching the UUID and company.
	 *
	 * @param uuid the UUID of the amenities
	 * @param companyId the primary key of the company
	 * @return the matching amenities, or an empty list if no matches were found
	 */
	public static List<Amenity> getAmenitiesByUuidAndCompanyId(
		String uuid, long companyId) {

		return getService().getAmenitiesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of amenities matching the UUID and company.
	 *
	 * @param uuid the UUID of the amenities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching amenities, or an empty list if no matches were found
	 */
	public static List<Amenity> getAmenitiesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Amenity> orderByComparator) {

		return getService().getAmenitiesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of amenities.
	 *
	 * @return the number of amenities
	 */
	public static int getAmenitiesCount() {
		return getService().getAmenitiesCount();
	}

	public static int getAmenitiesCount(long groupId) {
		return getService().getAmenitiesCount(groupId);
	}

	/**
	 * Returns the amenity with the primary key.
	 *
	 * @param amenityId the primary key of the amenity
	 * @return the amenity
	 * @throws PortalException if a amenity with the primary key could not be found
	 */
	public static Amenity getAmenity(long amenityId) throws PortalException {
		return getService().getAmenity(amenityId);
	}

	/**
	 * Returns the amenity matching the UUID and group.
	 *
	 * @param uuid the amenity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching amenity
	 * @throws PortalException if a matching amenity could not be found
	 */
	public static Amenity getAmenityByUuidAndGroupId(String uuid, long groupId)
		throws PortalException {

		return getService().getAmenityByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	public static PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the amenity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenity the amenity
	 * @return the amenity that was updated
	 */
	public static Amenity updateAmenity(Amenity amenity) {
		return getService().updateAmenity(amenity);
	}

	public static Amenity updateAmenity(
			long userId, long amenityId,
			Map<java.util.Locale, String> listingImageMap,
			Map<java.util.Locale, String> listingTitleMap,
			Map<java.util.Locale, String> listingDescriptionMap,
			Map<java.util.Locale, String> ButtonLabelMap,
			Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException, SystemException {

		return getService().updateAmenity(
			userId, amenityId, listingImageMap, listingTitleMap,
			listingDescriptionMap, ButtonLabelMap, ButtonLinkMap,
			serviceContext);
	}

	public static AmenityLocalService getService() {
		return _service;
	}

	private static volatile AmenityLocalService _service;

}