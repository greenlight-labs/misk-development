/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AmenityLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AmenityLocalService
 * @generated
 */
public class AmenityLocalServiceWrapper
	implements AmenityLocalService, ServiceWrapper<AmenityLocalService> {

	public AmenityLocalServiceWrapper(AmenityLocalService amenityLocalService) {
		_amenityLocalService = amenityLocalService;
	}

	/**
	 * Adds the amenity to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenity the amenity
	 * @return the amenity that was added
	 */
	@Override
	public amenities.model.Amenity addAmenity(amenities.model.Amenity amenity) {
		return _amenityLocalService.addAmenity(amenity);
	}

	@Override
	public amenities.model.Amenity addAmenity(
			long userId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> ButtonLabelMap,
			java.util.Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _amenityLocalService.addAmenity(
			userId, listingImageMap, listingTitleMap, listingDescriptionMap,
			ButtonLabelMap, ButtonLinkMap, serviceContext);
	}

	/**
	 * Creates a new amenity with the primary key. Does not add the amenity to the database.
	 *
	 * @param amenityId the primary key for the new amenity
	 * @return the new amenity
	 */
	@Override
	public amenities.model.Amenity createAmenity(long amenityId) {
		return _amenityLocalService.createAmenity(amenityId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _amenityLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the amenity from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenity the amenity
	 * @return the amenity that was removed
	 */
	@Override
	public amenities.model.Amenity deleteAmenity(
		amenities.model.Amenity amenity) {

		return _amenityLocalService.deleteAmenity(amenity);
	}

	/**
	 * Deletes the amenity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenityId the primary key of the amenity
	 * @return the amenity that was removed
	 * @throws PortalException if a amenity with the primary key could not be found
	 */
	@Override
	public amenities.model.Amenity deleteAmenity(long amenityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _amenityLocalService.deleteAmenity(amenityId);
	}

	@Override
	public amenities.model.Amenity deleteAmenity(
			long amenityId,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _amenityLocalService.deleteAmenity(amenityId, serviceContext);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _amenityLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _amenityLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _amenityLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>amenities.model.impl.AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _amenityLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>amenities.model.impl.AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _amenityLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _amenityLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _amenityLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public amenities.model.Amenity fetchAmenity(long amenityId) {
		return _amenityLocalService.fetchAmenity(amenityId);
	}

	/**
	 * Returns the amenity matching the UUID and group.
	 *
	 * @param uuid the amenity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	@Override
	public amenities.model.Amenity fetchAmenityByUuidAndGroupId(
		String uuid, long groupId) {

		return _amenityLocalService.fetchAmenityByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _amenityLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns a range of all the amenities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>amenities.model.impl.AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @return the range of amenities
	 */
	@Override
	public java.util.List<amenities.model.Amenity> getAmenities(
		int start, int end) {

		return _amenityLocalService.getAmenities(start, end);
	}

	@Override
	public java.util.List<amenities.model.Amenity> getAmenities(long groupId) {
		return _amenityLocalService.getAmenities(groupId);
	}

	@Override
	public java.util.List<amenities.model.Amenity> getAmenities(
		long groupId, int start, int end) {

		return _amenityLocalService.getAmenities(groupId, start, end);
	}

	@Override
	public java.util.List<amenities.model.Amenity> getAmenities(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<amenities.model.Amenity> obc) {

		return _amenityLocalService.getAmenities(groupId, start, end, obc);
	}

	/**
	 * Returns all the amenities matching the UUID and company.
	 *
	 * @param uuid the UUID of the amenities
	 * @param companyId the primary key of the company
	 * @return the matching amenities, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<amenities.model.Amenity>
		getAmenitiesByUuidAndCompanyId(String uuid, long companyId) {

		return _amenityLocalService.getAmenitiesByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of amenities matching the UUID and company.
	 *
	 * @param uuid the UUID of the amenities
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching amenities, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<amenities.model.Amenity>
		getAmenitiesByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<amenities.model.Amenity> orderByComparator) {

		return _amenityLocalService.getAmenitiesByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of amenities.
	 *
	 * @return the number of amenities
	 */
	@Override
	public int getAmenitiesCount() {
		return _amenityLocalService.getAmenitiesCount();
	}

	@Override
	public int getAmenitiesCount(long groupId) {
		return _amenityLocalService.getAmenitiesCount(groupId);
	}

	/**
	 * Returns the amenity with the primary key.
	 *
	 * @param amenityId the primary key of the amenity
	 * @return the amenity
	 * @throws PortalException if a amenity with the primary key could not be found
	 */
	@Override
	public amenities.model.Amenity getAmenity(long amenityId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _amenityLocalService.getAmenity(amenityId);
	}

	/**
	 * Returns the amenity matching the UUID and group.
	 *
	 * @param uuid the amenity's UUID
	 * @param groupId the primary key of the group
	 * @return the matching amenity
	 * @throws PortalException if a matching amenity could not be found
	 */
	@Override
	public amenities.model.Amenity getAmenityByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _amenityLocalService.getAmenityByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _amenityLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _amenityLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _amenityLocalService.getOSGiServiceIdentifier();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _amenityLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the amenity in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect AmenityLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param amenity the amenity
	 * @return the amenity that was updated
	 */
	@Override
	public amenities.model.Amenity updateAmenity(
		amenities.model.Amenity amenity) {

		return _amenityLocalService.updateAmenity(amenity);
	}

	@Override
	public amenities.model.Amenity updateAmenity(
			long userId, long amenityId,
			java.util.Map<java.util.Locale, String> listingImageMap,
			java.util.Map<java.util.Locale, String> listingTitleMap,
			java.util.Map<java.util.Locale, String> listingDescriptionMap,
			java.util.Map<java.util.Locale, String> ButtonLabelMap,
			java.util.Map<java.util.Locale, String> ButtonLinkMap,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			   com.liferay.portal.kernel.exception.SystemException {

		return _amenityLocalService.updateAmenity(
			userId, amenityId, listingImageMap, listingTitleMap,
			listingDescriptionMap, ButtonLabelMap, ButtonLinkMap,
			serviceContext);
	}

	@Override
	public AmenityLocalService getWrappedService() {
		return _amenityLocalService;
	}

	@Override
	public void setWrappedService(AmenityLocalService amenityLocalService) {
		_amenityLocalService = amenityLocalService;
	}

	private AmenityLocalService _amenityLocalService;

}