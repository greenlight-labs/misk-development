/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.service.persistence;

import amenities.model.Amenity;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence utility for the amenity service. This utility wraps <code>amenities.service.persistence.impl.AmenityPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AmenityPersistence
 * @generated
 */
public class AmenityUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Amenity amenity) {
		getPersistence().clearCache(amenity);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Amenity> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Amenity> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Amenity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Amenity> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Amenity update(Amenity amenity) {
		return getPersistence().update(amenity);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Amenity update(
		Amenity amenity, ServiceContext serviceContext) {

		return getPersistence().update(amenity, serviceContext);
	}

	/**
	 * Returns all the amenities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching amenities
	 */
	public static List<Amenity> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the amenities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @return the range of matching amenities
	 */
	public static List<Amenity> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the amenities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching amenities
	 */
	public static List<Amenity> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the amenities where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching amenities
	 */
	public static List<Amenity> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Amenity> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first amenity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenity
	 * @throws NoSuchAmenityException if a matching amenity could not be found
	 */
	public static Amenity findByUuid_First(
			String uuid, OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first amenity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByUuid_First(
		String uuid, OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last amenity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenity
	 * @throws NoSuchAmenityException if a matching amenity could not be found
	 */
	public static Amenity findByUuid_Last(
			String uuid, OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last amenity in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByUuid_Last(
		String uuid, OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the amenities before and after the current amenity in the ordered set where uuid = &#63;.
	 *
	 * @param amenityId the primary key of the current amenity
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next amenity
	 * @throws NoSuchAmenityException if a amenity with the primary key could not be found
	 */
	public static Amenity[] findByUuid_PrevAndNext(
			long amenityId, String uuid,
			OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByUuid_PrevAndNext(
			amenityId, uuid, orderByComparator);
	}

	/**
	 * Removes all the amenities where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of amenities where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching amenities
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the amenity where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchAmenityException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching amenity
	 * @throws NoSuchAmenityException if a matching amenity could not be found
	 */
	public static Amenity findByUUID_G(String uuid, long groupId)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the amenity where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the amenity where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the amenity where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the amenity that was removed
	 */
	public static Amenity removeByUUID_G(String uuid, long groupId)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of amenities where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching amenities
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the amenities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching amenities
	 */
	public static List<Amenity> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the amenities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @return the range of matching amenities
	 */
	public static List<Amenity> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the amenities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching amenities
	 */
	public static List<Amenity> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the amenities where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching amenities
	 */
	public static List<Amenity> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Amenity> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first amenity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenity
	 * @throws NoSuchAmenityException if a matching amenity could not be found
	 */
	public static Amenity findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first amenity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last amenity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenity
	 * @throws NoSuchAmenityException if a matching amenity could not be found
	 */
	public static Amenity findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last amenity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the amenities before and after the current amenity in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param amenityId the primary key of the current amenity
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next amenity
	 * @throws NoSuchAmenityException if a amenity with the primary key could not be found
	 */
	public static Amenity[] findByUuid_C_PrevAndNext(
			long amenityId, String uuid, long companyId,
			OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByUuid_C_PrevAndNext(
			amenityId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the amenities where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of amenities where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching amenities
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the amenities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching amenities
	 */
	public static List<Amenity> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	 * Returns a range of all the amenities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @return the range of matching amenities
	 */
	public static List<Amenity> findByGroupId(
		long groupId, int start, int end) {

		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	 * Returns an ordered range of all the amenities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching amenities
	 */
	public static List<Amenity> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the amenities where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching amenities
	 */
	public static List<Amenity> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<Amenity> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByGroupId(
			groupId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first amenity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenity
	 * @throws NoSuchAmenityException if a matching amenity could not be found
	 */
	public static Amenity findByGroupId_First(
			long groupId, OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	 * Returns the first amenity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByGroupId_First(
		long groupId, OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().fetchByGroupId_First(
			groupId, orderByComparator);
	}

	/**
	 * Returns the last amenity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenity
	 * @throws NoSuchAmenityException if a matching amenity could not be found
	 */
	public static Amenity findByGroupId_Last(
			long groupId, OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the last amenity in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching amenity, or <code>null</code> if a matching amenity could not be found
	 */
	public static Amenity fetchByGroupId_Last(
		long groupId, OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	 * Returns the amenities before and after the current amenity in the ordered set where groupId = &#63;.
	 *
	 * @param amenityId the primary key of the current amenity
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next amenity
	 * @throws NoSuchAmenityException if a amenity with the primary key could not be found
	 */
	public static Amenity[] findByGroupId_PrevAndNext(
			long amenityId, long groupId,
			OrderByComparator<Amenity> orderByComparator)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByGroupId_PrevAndNext(
			amenityId, groupId, orderByComparator);
	}

	/**
	 * Removes all the amenities where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	 * Returns the number of amenities where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching amenities
	 */
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	 * Caches the amenity in the entity cache if it is enabled.
	 *
	 * @param amenity the amenity
	 */
	public static void cacheResult(Amenity amenity) {
		getPersistence().cacheResult(amenity);
	}

	/**
	 * Caches the amenities in the entity cache if it is enabled.
	 *
	 * @param amenities the amenities
	 */
	public static void cacheResult(List<Amenity> amenities) {
		getPersistence().cacheResult(amenities);
	}

	/**
	 * Creates a new amenity with the primary key. Does not add the amenity to the database.
	 *
	 * @param amenityId the primary key for the new amenity
	 * @return the new amenity
	 */
	public static Amenity create(long amenityId) {
		return getPersistence().create(amenityId);
	}

	/**
	 * Removes the amenity with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param amenityId the primary key of the amenity
	 * @return the amenity that was removed
	 * @throws NoSuchAmenityException if a amenity with the primary key could not be found
	 */
	public static Amenity remove(long amenityId)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().remove(amenityId);
	}

	public static Amenity updateImpl(Amenity amenity) {
		return getPersistence().updateImpl(amenity);
	}

	/**
	 * Returns the amenity with the primary key or throws a <code>NoSuchAmenityException</code> if it could not be found.
	 *
	 * @param amenityId the primary key of the amenity
	 * @return the amenity
	 * @throws NoSuchAmenityException if a amenity with the primary key could not be found
	 */
	public static Amenity findByPrimaryKey(long amenityId)
		throws amenities.exception.NoSuchAmenityException {

		return getPersistence().findByPrimaryKey(amenityId);
	}

	/**
	 * Returns the amenity with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param amenityId the primary key of the amenity
	 * @return the amenity, or <code>null</code> if a amenity with the primary key could not be found
	 */
	public static Amenity fetchByPrimaryKey(long amenityId) {
		return getPersistence().fetchByPrimaryKey(amenityId);
	}

	/**
	 * Returns all the amenities.
	 *
	 * @return the amenities
	 */
	public static List<Amenity> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the amenities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @return the range of amenities
	 */
	public static List<Amenity> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the amenities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of amenities
	 */
	public static List<Amenity> findAll(
		int start, int end, OrderByComparator<Amenity> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the amenities.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>AmenityModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of amenities
	 * @param end the upper bound of the range of amenities (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of amenities
	 */
	public static List<Amenity> findAll(
		int start, int end, OrderByComparator<Amenity> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the amenities from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of amenities.
	 *
	 * @return the number of amenities
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static AmenityPersistence getPersistence() {
		return _persistence;
	}

	private static volatile AmenityPersistence _persistence;

}