/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AmenityService}.
 *
 * @author Brian Wing Shun Chan
 * @see AmenityService
 * @generated
 */
public class AmenityServiceWrapper
	implements AmenityService, ServiceWrapper<AmenityService> {

	public AmenityServiceWrapper(AmenityService amenityService) {
		_amenityService = amenityService;
	}

	@Override
	public java.util.List<amenities.model.Amenity> getAmenities(long groupId) {
		return _amenityService.getAmenities(groupId);
	}

	@Override
	public java.util.List<amenities.model.Amenity> getAmenities(
		long groupId, int start, int end) {

		return _amenityService.getAmenities(groupId, start, end);
	}

	@Override
	public java.util.List<amenities.model.Amenity> getAmenities(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator
			<amenities.model.Amenity> obc) {

		return _amenityService.getAmenities(groupId, start, end, obc);
	}

	@Override
	public int getAmenitiesCount(long groupId) {
		return _amenityService.getAmenitiesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _amenityService.getOSGiServiceIdentifier();
	}

	@Override
	public AmenityService getWrappedService() {
		return _amenityService;
	}

	@Override
	public void setWrappedService(AmenityService amenityService) {
		_amenityService = amenityService;
	}

	private AmenityService _amenityService;

}