/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.service;

import amenities.model.Amenity;

import com.liferay.portal.kernel.util.OrderByComparator;

import java.util.List;

/**
 * Provides the remote service utility for Amenity. This utility wraps
 * <code>amenities.service.impl.AmenityServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see AmenityService
 * @generated
 */
public class AmenityServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>amenities.service.impl.AmenityServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static List<Amenity> getAmenities(long groupId) {
		return getService().getAmenities(groupId);
	}

	public static List<Amenity> getAmenities(long groupId, int start, int end) {
		return getService().getAmenities(groupId, start, end);
	}

	public static List<Amenity> getAmenities(
		long groupId, int start, int end, OrderByComparator<Amenity> obc) {

		return getService().getAmenities(groupId, start, end, obc);
	}

	public static int getAmenitiesCount(long groupId) {
		return getService().getAmenitiesCount(groupId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static AmenityService getService() {
		return _service;
	}

	private static volatile AmenityService _service;

}