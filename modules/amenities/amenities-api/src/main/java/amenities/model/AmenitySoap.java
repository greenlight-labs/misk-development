/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link amenities.service.http.AmenityServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class AmenitySoap implements Serializable {

	public static AmenitySoap toSoapModel(Amenity model) {
		AmenitySoap soapModel = new AmenitySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setAmenityId(model.getAmenityId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setListingImage(model.getListingImage());
		soapModel.setListingTitle(model.getListingTitle());
		soapModel.setListingDescription(model.getListingDescription());
		soapModel.setButtonLabel(model.getButtonLabel());
		soapModel.setButtonLink(model.getButtonLink());

		return soapModel;
	}

	public static AmenitySoap[] toSoapModels(Amenity[] models) {
		AmenitySoap[] soapModels = new AmenitySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static AmenitySoap[][] toSoapModels(Amenity[][] models) {
		AmenitySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new AmenitySoap[models.length][models[0].length];
		}
		else {
			soapModels = new AmenitySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static AmenitySoap[] toSoapModels(List<Amenity> models) {
		List<AmenitySoap> soapModels = new ArrayList<AmenitySoap>(
			models.size());

		for (Amenity model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new AmenitySoap[soapModels.size()]);
	}

	public AmenitySoap() {
	}

	public long getPrimaryKey() {
		return _amenityId;
	}

	public void setPrimaryKey(long pk) {
		setAmenityId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getAmenityId() {
		return _amenityId;
	}

	public void setAmenityId(long amenityId) {
		_amenityId = amenityId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getListingImage() {
		return _listingImage;
	}

	public void setListingImage(String listingImage) {
		_listingImage = listingImage;
	}

	public String getListingTitle() {
		return _listingTitle;
	}

	public void setListingTitle(String listingTitle) {
		_listingTitle = listingTitle;
	}

	public String getListingDescription() {
		return _listingDescription;
	}

	public void setListingDescription(String listingDescription) {
		_listingDescription = listingDescription;
	}

	public String getButtonLabel() {
		return _buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		_buttonLabel = buttonLabel;
	}

	public String getButtonLink() {
		return _buttonLink;
	}

	public void setButtonLink(String buttonLink) {
		_buttonLink = buttonLink;
	}

	private String _uuid;
	private long _amenityId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _listingImage;
	private String _listingTitle;
	private String _listingDescription;
	private String _buttonLabel;
	private String _buttonLink;

}