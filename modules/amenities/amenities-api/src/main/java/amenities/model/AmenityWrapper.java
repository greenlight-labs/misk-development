/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.model;

import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Amenity}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Amenity
 * @generated
 */
public class AmenityWrapper
	extends BaseModelWrapper<Amenity>
	implements Amenity, ModelWrapper<Amenity> {

	public AmenityWrapper(Amenity amenity) {
		super(amenity);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("amenityId", getAmenityId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("listingImage", getListingImage());
		attributes.put("listingTitle", getListingTitle());
		attributes.put("listingDescription", getListingDescription());
		attributes.put("buttonLabel", getButtonLabel());
		attributes.put("buttonLink", getButtonLink());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long amenityId = (Long)attributes.get("amenityId");

		if (amenityId != null) {
			setAmenityId(amenityId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String listingImage = (String)attributes.get("listingImage");

		if (listingImage != null) {
			setListingImage(listingImage);
		}

		String listingTitle = (String)attributes.get("listingTitle");

		if (listingTitle != null) {
			setListingTitle(listingTitle);
		}

		String listingDescription = (String)attributes.get(
			"listingDescription");

		if (listingDescription != null) {
			setListingDescription(listingDescription);
		}

		String buttonLabel = (String)attributes.get("buttonLabel");

		if (buttonLabel != null) {
			setButtonLabel(buttonLabel);
		}

		String buttonLink = (String)attributes.get("buttonLink");

		if (buttonLink != null) {
			setButtonLink(buttonLink);
		}
	}

	/**
	 * Returns the amenity ID of this amenity.
	 *
	 * @return the amenity ID of this amenity
	 */
	@Override
	public long getAmenityId() {
		return model.getAmenityId();
	}

	@Override
	public String[] getAvailableLanguageIds() {
		return model.getAvailableLanguageIds();
	}

	/**
	 * Returns the button label of this amenity.
	 *
	 * @return the button label of this amenity
	 */
	@Override
	public String getButtonLabel() {
		return model.getButtonLabel();
	}

	/**
	 * Returns the localized button label of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button label of this amenity
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale) {
		return model.getButtonLabel(locale);
	}

	/**
	 * Returns the localized button label of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getButtonLabel(java.util.Locale locale, boolean useDefault) {
		return model.getButtonLabel(locale, useDefault);
	}

	/**
	 * Returns the localized button label of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button label of this amenity
	 */
	@Override
	public String getButtonLabel(String languageId) {
		return model.getButtonLabel(languageId);
	}

	/**
	 * Returns the localized button label of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this amenity
	 */
	@Override
	public String getButtonLabel(String languageId, boolean useDefault) {
		return model.getButtonLabel(languageId, useDefault);
	}

	@Override
	public String getButtonLabelCurrentLanguageId() {
		return model.getButtonLabelCurrentLanguageId();
	}

	@Override
	public String getButtonLabelCurrentValue() {
		return model.getButtonLabelCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized button labels of this amenity.
	 *
	 * @return the locales and localized button labels of this amenity
	 */
	@Override
	public Map<java.util.Locale, String> getButtonLabelMap() {
		return model.getButtonLabelMap();
	}

	/**
	 * Returns the button link of this amenity.
	 *
	 * @return the button link of this amenity
	 */
	@Override
	public String getButtonLink() {
		return model.getButtonLink();
	}

	/**
	 * Returns the localized button link of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button link of this amenity
	 */
	@Override
	public String getButtonLink(java.util.Locale locale) {
		return model.getButtonLink(locale);
	}

	/**
	 * Returns the localized button link of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getButtonLink(java.util.Locale locale, boolean useDefault) {
		return model.getButtonLink(locale, useDefault);
	}

	/**
	 * Returns the localized button link of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button link of this amenity
	 */
	@Override
	public String getButtonLink(String languageId) {
		return model.getButtonLink(languageId);
	}

	/**
	 * Returns the localized button link of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this amenity
	 */
	@Override
	public String getButtonLink(String languageId, boolean useDefault) {
		return model.getButtonLink(languageId, useDefault);
	}

	@Override
	public String getButtonLinkCurrentLanguageId() {
		return model.getButtonLinkCurrentLanguageId();
	}

	@Override
	public String getButtonLinkCurrentValue() {
		return model.getButtonLinkCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized button links of this amenity.
	 *
	 * @return the locales and localized button links of this amenity
	 */
	@Override
	public Map<java.util.Locale, String> getButtonLinkMap() {
		return model.getButtonLinkMap();
	}

	/**
	 * Returns the company ID of this amenity.
	 *
	 * @return the company ID of this amenity
	 */
	@Override
	public long getCompanyId() {
		return model.getCompanyId();
	}

	/**
	 * Returns the create date of this amenity.
	 *
	 * @return the create date of this amenity
	 */
	@Override
	public Date getCreateDate() {
		return model.getCreateDate();
	}

	@Override
	public String getDefaultLanguageId() {
		return model.getDefaultLanguageId();
	}

	/**
	 * Returns the group ID of this amenity.
	 *
	 * @return the group ID of this amenity
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the listing description of this amenity.
	 *
	 * @return the listing description of this amenity
	 */
	@Override
	public String getListingDescription() {
		return model.getListingDescription();
	}

	/**
	 * Returns the localized listing description of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing description of this amenity
	 */
	@Override
	public String getListingDescription(java.util.Locale locale) {
		return model.getListingDescription(locale);
	}

	/**
	 * Returns the localized listing description of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingDescription(
		java.util.Locale locale, boolean useDefault) {

		return model.getListingDescription(locale, useDefault);
	}

	/**
	 * Returns the localized listing description of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing description of this amenity
	 */
	@Override
	public String getListingDescription(String languageId) {
		return model.getListingDescription(languageId);
	}

	/**
	 * Returns the localized listing description of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this amenity
	 */
	@Override
	public String getListingDescription(String languageId, boolean useDefault) {
		return model.getListingDescription(languageId, useDefault);
	}

	@Override
	public String getListingDescriptionCurrentLanguageId() {
		return model.getListingDescriptionCurrentLanguageId();
	}

	@Override
	public String getListingDescriptionCurrentValue() {
		return model.getListingDescriptionCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing descriptions of this amenity.
	 *
	 * @return the locales and localized listing descriptions of this amenity
	 */
	@Override
	public Map<java.util.Locale, String> getListingDescriptionMap() {
		return model.getListingDescriptionMap();
	}

	/**
	 * Returns the listing image of this amenity.
	 *
	 * @return the listing image of this amenity
	 */
	@Override
	public String getListingImage() {
		return model.getListingImage();
	}

	/**
	 * Returns the localized listing image of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing image of this amenity
	 */
	@Override
	public String getListingImage(java.util.Locale locale) {
		return model.getListingImage(locale);
	}

	/**
	 * Returns the localized listing image of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingImage(java.util.Locale locale, boolean useDefault) {
		return model.getListingImage(locale, useDefault);
	}

	/**
	 * Returns the localized listing image of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing image of this amenity
	 */
	@Override
	public String getListingImage(String languageId) {
		return model.getListingImage(languageId);
	}

	/**
	 * Returns the localized listing image of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this amenity
	 */
	@Override
	public String getListingImage(String languageId, boolean useDefault) {
		return model.getListingImage(languageId, useDefault);
	}

	@Override
	public String getListingImageCurrentLanguageId() {
		return model.getListingImageCurrentLanguageId();
	}

	@Override
	public String getListingImageCurrentValue() {
		return model.getListingImageCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing images of this amenity.
	 *
	 * @return the locales and localized listing images of this amenity
	 */
	@Override
	public Map<java.util.Locale, String> getListingImageMap() {
		return model.getListingImageMap();
	}

	/**
	 * Returns the listing title of this amenity.
	 *
	 * @return the listing title of this amenity
	 */
	@Override
	public String getListingTitle() {
		return model.getListingTitle();
	}

	/**
	 * Returns the localized listing title of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing title of this amenity
	 */
	@Override
	public String getListingTitle(java.util.Locale locale) {
		return model.getListingTitle(locale);
	}

	/**
	 * Returns the localized listing title of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@Override
	public String getListingTitle(java.util.Locale locale, boolean useDefault) {
		return model.getListingTitle(locale, useDefault);
	}

	/**
	 * Returns the localized listing title of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing title of this amenity
	 */
	@Override
	public String getListingTitle(String languageId) {
		return model.getListingTitle(languageId);
	}

	/**
	 * Returns the localized listing title of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this amenity
	 */
	@Override
	public String getListingTitle(String languageId, boolean useDefault) {
		return model.getListingTitle(languageId, useDefault);
	}

	@Override
	public String getListingTitleCurrentLanguageId() {
		return model.getListingTitleCurrentLanguageId();
	}

	@Override
	public String getListingTitleCurrentValue() {
		return model.getListingTitleCurrentValue();
	}

	/**
	 * Returns a map of the locales and localized listing titles of this amenity.
	 *
	 * @return the locales and localized listing titles of this amenity
	 */
	@Override
	public Map<java.util.Locale, String> getListingTitleMap() {
		return model.getListingTitleMap();
	}

	/**
	 * Returns the modified date of this amenity.
	 *
	 * @return the modified date of this amenity
	 */
	@Override
	public Date getModifiedDate() {
		return model.getModifiedDate();
	}

	/**
	 * Returns the primary key of this amenity.
	 *
	 * @return the primary key of this amenity
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the user ID of this amenity.
	 *
	 * @return the user ID of this amenity
	 */
	@Override
	public long getUserId() {
		return model.getUserId();
	}

	/**
	 * Returns the user name of this amenity.
	 *
	 * @return the user name of this amenity
	 */
	@Override
	public String getUserName() {
		return model.getUserName();
	}

	/**
	 * Returns the user uuid of this amenity.
	 *
	 * @return the user uuid of this amenity
	 */
	@Override
	public String getUserUuid() {
		return model.getUserUuid();
	}

	/**
	 * Returns the uuid of this amenity.
	 *
	 * @return the uuid of this amenity
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	@Override
	public void prepareLocalizedFieldsForImport()
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport();
	}

	@Override
	public void prepareLocalizedFieldsForImport(
			java.util.Locale defaultImportLocale)
		throws com.liferay.portal.kernel.exception.LocaleException {

		model.prepareLocalizedFieldsForImport(defaultImportLocale);
	}

	/**
	 * Sets the amenity ID of this amenity.
	 *
	 * @param amenityId the amenity ID of this amenity
	 */
	@Override
	public void setAmenityId(long amenityId) {
		model.setAmenityId(amenityId);
	}

	/**
	 * Sets the button label of this amenity.
	 *
	 * @param buttonLabel the button label of this amenity
	 */
	@Override
	public void setButtonLabel(String buttonLabel) {
		model.setButtonLabel(buttonLabel);
	}

	/**
	 * Sets the localized button label of this amenity in the language.
	 *
	 * @param buttonLabel the localized button label of this amenity
	 * @param locale the locale of the language
	 */
	@Override
	public void setButtonLabel(String buttonLabel, java.util.Locale locale) {
		model.setButtonLabel(buttonLabel, locale);
	}

	/**
	 * Sets the localized button label of this amenity in the language, and sets the default locale.
	 *
	 * @param buttonLabel the localized button label of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabel(
		String buttonLabel, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setButtonLabel(buttonLabel, locale, defaultLocale);
	}

	@Override
	public void setButtonLabelCurrentLanguageId(String languageId) {
		model.setButtonLabelCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized button labels of this amenity from the map of locales and localized button labels.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this amenity
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap) {

		model.setButtonLabelMap(buttonLabelMap);
	}

	/**
	 * Sets the localized button labels of this amenity from the map of locales and localized button labels, and sets the default locale.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this amenity
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLabelMap(
		Map<java.util.Locale, String> buttonLabelMap,
		java.util.Locale defaultLocale) {

		model.setButtonLabelMap(buttonLabelMap, defaultLocale);
	}

	/**
	 * Sets the button link of this amenity.
	 *
	 * @param buttonLink the button link of this amenity
	 */
	@Override
	public void setButtonLink(String buttonLink) {
		model.setButtonLink(buttonLink);
	}

	/**
	 * Sets the localized button link of this amenity in the language.
	 *
	 * @param buttonLink the localized button link of this amenity
	 * @param locale the locale of the language
	 */
	@Override
	public void setButtonLink(String buttonLink, java.util.Locale locale) {
		model.setButtonLink(buttonLink, locale);
	}

	/**
	 * Sets the localized button link of this amenity in the language, and sets the default locale.
	 *
	 * @param buttonLink the localized button link of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLink(
		String buttonLink, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setButtonLink(buttonLink, locale, defaultLocale);
	}

	@Override
	public void setButtonLinkCurrentLanguageId(String languageId) {
		model.setButtonLinkCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized button links of this amenity from the map of locales and localized button links.
	 *
	 * @param buttonLinkMap the locales and localized button links of this amenity
	 */
	@Override
	public void setButtonLinkMap(Map<java.util.Locale, String> buttonLinkMap) {
		model.setButtonLinkMap(buttonLinkMap);
	}

	/**
	 * Sets the localized button links of this amenity from the map of locales and localized button links, and sets the default locale.
	 *
	 * @param buttonLinkMap the locales and localized button links of this amenity
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setButtonLinkMap(
		Map<java.util.Locale, String> buttonLinkMap,
		java.util.Locale defaultLocale) {

		model.setButtonLinkMap(buttonLinkMap, defaultLocale);
	}

	/**
	 * Sets the company ID of this amenity.
	 *
	 * @param companyId the company ID of this amenity
	 */
	@Override
	public void setCompanyId(long companyId) {
		model.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this amenity.
	 *
	 * @param createDate the create date of this amenity
	 */
	@Override
	public void setCreateDate(Date createDate) {
		model.setCreateDate(createDate);
	}

	/**
	 * Sets the group ID of this amenity.
	 *
	 * @param groupId the group ID of this amenity
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the listing description of this amenity.
	 *
	 * @param listingDescription the listing description of this amenity
	 */
	@Override
	public void setListingDescription(String listingDescription) {
		model.setListingDescription(listingDescription);
	}

	/**
	 * Sets the localized listing description of this amenity in the language.
	 *
	 * @param listingDescription the localized listing description of this amenity
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingDescription(
		String listingDescription, java.util.Locale locale) {

		model.setListingDescription(listingDescription, locale);
	}

	/**
	 * Sets the localized listing description of this amenity in the language, and sets the default locale.
	 *
	 * @param listingDescription the localized listing description of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingDescription(
		String listingDescription, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingDescription(listingDescription, locale, defaultLocale);
	}

	@Override
	public void setListingDescriptionCurrentLanguageId(String languageId) {
		model.setListingDescriptionCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing descriptions of this amenity from the map of locales and localized listing descriptions.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this amenity
	 */
	@Override
	public void setListingDescriptionMap(
		Map<java.util.Locale, String> listingDescriptionMap) {

		model.setListingDescriptionMap(listingDescriptionMap);
	}

	/**
	 * Sets the localized listing descriptions of this amenity from the map of locales and localized listing descriptions, and sets the default locale.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this amenity
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingDescriptionMap(
		Map<java.util.Locale, String> listingDescriptionMap,
		java.util.Locale defaultLocale) {

		model.setListingDescriptionMap(listingDescriptionMap, defaultLocale);
	}

	/**
	 * Sets the listing image of this amenity.
	 *
	 * @param listingImage the listing image of this amenity
	 */
	@Override
	public void setListingImage(String listingImage) {
		model.setListingImage(listingImage);
	}

	/**
	 * Sets the localized listing image of this amenity in the language.
	 *
	 * @param listingImage the localized listing image of this amenity
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingImage(String listingImage, java.util.Locale locale) {
		model.setListingImage(listingImage, locale);
	}

	/**
	 * Sets the localized listing image of this amenity in the language, and sets the default locale.
	 *
	 * @param listingImage the localized listing image of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImage(
		String listingImage, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingImage(listingImage, locale, defaultLocale);
	}

	@Override
	public void setListingImageCurrentLanguageId(String languageId) {
		model.setListingImageCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing images of this amenity from the map of locales and localized listing images.
	 *
	 * @param listingImageMap the locales and localized listing images of this amenity
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap) {

		model.setListingImageMap(listingImageMap);
	}

	/**
	 * Sets the localized listing images of this amenity from the map of locales and localized listing images, and sets the default locale.
	 *
	 * @param listingImageMap the locales and localized listing images of this amenity
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingImageMap(
		Map<java.util.Locale, String> listingImageMap,
		java.util.Locale defaultLocale) {

		model.setListingImageMap(listingImageMap, defaultLocale);
	}

	/**
	 * Sets the listing title of this amenity.
	 *
	 * @param listingTitle the listing title of this amenity
	 */
	@Override
	public void setListingTitle(String listingTitle) {
		model.setListingTitle(listingTitle);
	}

	/**
	 * Sets the localized listing title of this amenity in the language.
	 *
	 * @param listingTitle the localized listing title of this amenity
	 * @param locale the locale of the language
	 */
	@Override
	public void setListingTitle(String listingTitle, java.util.Locale locale) {
		model.setListingTitle(listingTitle, locale);
	}

	/**
	 * Sets the localized listing title of this amenity in the language, and sets the default locale.
	 *
	 * @param listingTitle the localized listing title of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingTitle(
		String listingTitle, java.util.Locale locale,
		java.util.Locale defaultLocale) {

		model.setListingTitle(listingTitle, locale, defaultLocale);
	}

	@Override
	public void setListingTitleCurrentLanguageId(String languageId) {
		model.setListingTitleCurrentLanguageId(languageId);
	}

	/**
	 * Sets the localized listing titles of this amenity from the map of locales and localized listing titles.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this amenity
	 */
	@Override
	public void setListingTitleMap(
		Map<java.util.Locale, String> listingTitleMap) {

		model.setListingTitleMap(listingTitleMap);
	}

	/**
	 * Sets the localized listing titles of this amenity from the map of locales and localized listing titles, and sets the default locale.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this amenity
	 * @param defaultLocale the default locale
	 */
	@Override
	public void setListingTitleMap(
		Map<java.util.Locale, String> listingTitleMap,
		java.util.Locale defaultLocale) {

		model.setListingTitleMap(listingTitleMap, defaultLocale);
	}

	/**
	 * Sets the modified date of this amenity.
	 *
	 * @param modifiedDate the modified date of this amenity
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		model.setModifiedDate(modifiedDate);
	}

	/**
	 * Sets the primary key of this amenity.
	 *
	 * @param primaryKey the primary key of this amenity
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the user ID of this amenity.
	 *
	 * @param userId the user ID of this amenity
	 */
	@Override
	public void setUserId(long userId) {
		model.setUserId(userId);
	}

	/**
	 * Sets the user name of this amenity.
	 *
	 * @param userName the user name of this amenity
	 */
	@Override
	public void setUserName(String userName) {
		model.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this amenity.
	 *
	 * @param userUuid the user uuid of this amenity
	 */
	@Override
	public void setUserUuid(String userUuid) {
		model.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this amenity.
	 *
	 * @param uuid the uuid of this amenity
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	public StagedModelType getStagedModelType() {
		return model.getStagedModelType();
	}

	@Override
	protected AmenityWrapper wrap(Amenity amenity) {
		return new AmenityWrapper(amenity);
	}

}