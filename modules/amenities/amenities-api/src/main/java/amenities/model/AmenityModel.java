/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.exception.LocaleException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.GroupedModel;
import com.liferay.portal.kernel.model.LocalizedModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.model.StagedAuditedModel;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The base model interface for the Amenity service. Represents a row in the &quot;misk_amenities&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>amenities.model.impl.AmenityModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>amenities.model.impl.AmenityImpl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Amenity
 * @generated
 */
@ProviderType
public interface AmenityModel
	extends BaseModel<Amenity>, GroupedModel, LocalizedModel, ShardedModel,
			StagedAuditedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a amenity model instance should use the {@link Amenity} interface instead.
	 */

	/**
	 * Returns the primary key of this amenity.
	 *
	 * @return the primary key of this amenity
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this amenity.
	 *
	 * @param primaryKey the primary key of this amenity
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this amenity.
	 *
	 * @return the uuid of this amenity
	 */
	@AutoEscape
	@Override
	public String getUuid();

	/**
	 * Sets the uuid of this amenity.
	 *
	 * @param uuid the uuid of this amenity
	 */
	@Override
	public void setUuid(String uuid);

	/**
	 * Returns the amenity ID of this amenity.
	 *
	 * @return the amenity ID of this amenity
	 */
	public long getAmenityId();

	/**
	 * Sets the amenity ID of this amenity.
	 *
	 * @param amenityId the amenity ID of this amenity
	 */
	public void setAmenityId(long amenityId);

	/**
	 * Returns the group ID of this amenity.
	 *
	 * @return the group ID of this amenity
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this amenity.
	 *
	 * @param groupId the group ID of this amenity
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the company ID of this amenity.
	 *
	 * @return the company ID of this amenity
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this amenity.
	 *
	 * @param companyId the company ID of this amenity
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this amenity.
	 *
	 * @return the user ID of this amenity
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this amenity.
	 *
	 * @param userId the user ID of this amenity
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this amenity.
	 *
	 * @return the user uuid of this amenity
	 */
	@Override
	public String getUserUuid();

	/**
	 * Sets the user uuid of this amenity.
	 *
	 * @param userUuid the user uuid of this amenity
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this amenity.
	 *
	 * @return the user name of this amenity
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this amenity.
	 *
	 * @param userName the user name of this amenity
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this amenity.
	 *
	 * @return the create date of this amenity
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this amenity.
	 *
	 * @param createDate the create date of this amenity
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this amenity.
	 *
	 * @return the modified date of this amenity
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this amenity.
	 *
	 * @param modifiedDate the modified date of this amenity
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the listing image of this amenity.
	 *
	 * @return the listing image of this amenity
	 */
	public String getListingImage();

	/**
	 * Returns the localized listing image of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing image of this amenity
	 */
	@AutoEscape
	public String getListingImage(Locale locale);

	/**
	 * Returns the localized listing image of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getListingImage(Locale locale, boolean useDefault);

	/**
	 * Returns the localized listing image of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing image of this amenity
	 */
	@AutoEscape
	public String getListingImage(String languageId);

	/**
	 * Returns the localized listing image of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing image of this amenity
	 */
	@AutoEscape
	public String getListingImage(String languageId, boolean useDefault);

	@AutoEscape
	public String getListingImageCurrentLanguageId();

	@AutoEscape
	public String getListingImageCurrentValue();

	/**
	 * Returns a map of the locales and localized listing images of this amenity.
	 *
	 * @return the locales and localized listing images of this amenity
	 */
	public Map<Locale, String> getListingImageMap();

	/**
	 * Sets the listing image of this amenity.
	 *
	 * @param listingImage the listing image of this amenity
	 */
	public void setListingImage(String listingImage);

	/**
	 * Sets the localized listing image of this amenity in the language.
	 *
	 * @param listingImage the localized listing image of this amenity
	 * @param locale the locale of the language
	 */
	public void setListingImage(String listingImage, Locale locale);

	/**
	 * Sets the localized listing image of this amenity in the language, and sets the default locale.
	 *
	 * @param listingImage the localized listing image of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setListingImage(
		String listingImage, Locale locale, Locale defaultLocale);

	public void setListingImageCurrentLanguageId(String languageId);

	/**
	 * Sets the localized listing images of this amenity from the map of locales and localized listing images.
	 *
	 * @param listingImageMap the locales and localized listing images of this amenity
	 */
	public void setListingImageMap(Map<Locale, String> listingImageMap);

	/**
	 * Sets the localized listing images of this amenity from the map of locales and localized listing images, and sets the default locale.
	 *
	 * @param listingImageMap the locales and localized listing images of this amenity
	 * @param defaultLocale the default locale
	 */
	public void setListingImageMap(
		Map<Locale, String> listingImageMap, Locale defaultLocale);

	/**
	 * Returns the listing title of this amenity.
	 *
	 * @return the listing title of this amenity
	 */
	public String getListingTitle();

	/**
	 * Returns the localized listing title of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing title of this amenity
	 */
	@AutoEscape
	public String getListingTitle(Locale locale);

	/**
	 * Returns the localized listing title of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getListingTitle(Locale locale, boolean useDefault);

	/**
	 * Returns the localized listing title of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing title of this amenity
	 */
	@AutoEscape
	public String getListingTitle(String languageId);

	/**
	 * Returns the localized listing title of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing title of this amenity
	 */
	@AutoEscape
	public String getListingTitle(String languageId, boolean useDefault);

	@AutoEscape
	public String getListingTitleCurrentLanguageId();

	@AutoEscape
	public String getListingTitleCurrentValue();

	/**
	 * Returns a map of the locales and localized listing titles of this amenity.
	 *
	 * @return the locales and localized listing titles of this amenity
	 */
	public Map<Locale, String> getListingTitleMap();

	/**
	 * Sets the listing title of this amenity.
	 *
	 * @param listingTitle the listing title of this amenity
	 */
	public void setListingTitle(String listingTitle);

	/**
	 * Sets the localized listing title of this amenity in the language.
	 *
	 * @param listingTitle the localized listing title of this amenity
	 * @param locale the locale of the language
	 */
	public void setListingTitle(String listingTitle, Locale locale);

	/**
	 * Sets the localized listing title of this amenity in the language, and sets the default locale.
	 *
	 * @param listingTitle the localized listing title of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setListingTitle(
		String listingTitle, Locale locale, Locale defaultLocale);

	public void setListingTitleCurrentLanguageId(String languageId);

	/**
	 * Sets the localized listing titles of this amenity from the map of locales and localized listing titles.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this amenity
	 */
	public void setListingTitleMap(Map<Locale, String> listingTitleMap);

	/**
	 * Sets the localized listing titles of this amenity from the map of locales and localized listing titles, and sets the default locale.
	 *
	 * @param listingTitleMap the locales and localized listing titles of this amenity
	 * @param defaultLocale the default locale
	 */
	public void setListingTitleMap(
		Map<Locale, String> listingTitleMap, Locale defaultLocale);

	/**
	 * Returns the listing description of this amenity.
	 *
	 * @return the listing description of this amenity
	 */
	public String getListingDescription();

	/**
	 * Returns the localized listing description of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized listing description of this amenity
	 */
	@AutoEscape
	public String getListingDescription(Locale locale);

	/**
	 * Returns the localized listing description of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getListingDescription(Locale locale, boolean useDefault);

	/**
	 * Returns the localized listing description of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized listing description of this amenity
	 */
	@AutoEscape
	public String getListingDescription(String languageId);

	/**
	 * Returns the localized listing description of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized listing description of this amenity
	 */
	@AutoEscape
	public String getListingDescription(String languageId, boolean useDefault);

	@AutoEscape
	public String getListingDescriptionCurrentLanguageId();

	@AutoEscape
	public String getListingDescriptionCurrentValue();

	/**
	 * Returns a map of the locales and localized listing descriptions of this amenity.
	 *
	 * @return the locales and localized listing descriptions of this amenity
	 */
	public Map<Locale, String> getListingDescriptionMap();

	/**
	 * Sets the listing description of this amenity.
	 *
	 * @param listingDescription the listing description of this amenity
	 */
	public void setListingDescription(String listingDescription);

	/**
	 * Sets the localized listing description of this amenity in the language.
	 *
	 * @param listingDescription the localized listing description of this amenity
	 * @param locale the locale of the language
	 */
	public void setListingDescription(String listingDescription, Locale locale);

	/**
	 * Sets the localized listing description of this amenity in the language, and sets the default locale.
	 *
	 * @param listingDescription the localized listing description of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setListingDescription(
		String listingDescription, Locale locale, Locale defaultLocale);

	public void setListingDescriptionCurrentLanguageId(String languageId);

	/**
	 * Sets the localized listing descriptions of this amenity from the map of locales and localized listing descriptions.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this amenity
	 */
	public void setListingDescriptionMap(
		Map<Locale, String> listingDescriptionMap);

	/**
	 * Sets the localized listing descriptions of this amenity from the map of locales and localized listing descriptions, and sets the default locale.
	 *
	 * @param listingDescriptionMap the locales and localized listing descriptions of this amenity
	 * @param defaultLocale the default locale
	 */
	public void setListingDescriptionMap(
		Map<Locale, String> listingDescriptionMap, Locale defaultLocale);

	/**
	 * Returns the button label of this amenity.
	 *
	 * @return the button label of this amenity
	 */
	public String getButtonLabel();

	/**
	 * Returns the localized button label of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button label of this amenity
	 */
	@AutoEscape
	public String getButtonLabel(Locale locale);

	/**
	 * Returns the localized button label of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getButtonLabel(Locale locale, boolean useDefault);

	/**
	 * Returns the localized button label of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button label of this amenity
	 */
	@AutoEscape
	public String getButtonLabel(String languageId);

	/**
	 * Returns the localized button label of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button label of this amenity
	 */
	@AutoEscape
	public String getButtonLabel(String languageId, boolean useDefault);

	@AutoEscape
	public String getButtonLabelCurrentLanguageId();

	@AutoEscape
	public String getButtonLabelCurrentValue();

	/**
	 * Returns a map of the locales and localized button labels of this amenity.
	 *
	 * @return the locales and localized button labels of this amenity
	 */
	public Map<Locale, String> getButtonLabelMap();

	/**
	 * Sets the button label of this amenity.
	 *
	 * @param buttonLabel the button label of this amenity
	 */
	public void setButtonLabel(String buttonLabel);

	/**
	 * Sets the localized button label of this amenity in the language.
	 *
	 * @param buttonLabel the localized button label of this amenity
	 * @param locale the locale of the language
	 */
	public void setButtonLabel(String buttonLabel, Locale locale);

	/**
	 * Sets the localized button label of this amenity in the language, and sets the default locale.
	 *
	 * @param buttonLabel the localized button label of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setButtonLabel(
		String buttonLabel, Locale locale, Locale defaultLocale);

	public void setButtonLabelCurrentLanguageId(String languageId);

	/**
	 * Sets the localized button labels of this amenity from the map of locales and localized button labels.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this amenity
	 */
	public void setButtonLabelMap(Map<Locale, String> buttonLabelMap);

	/**
	 * Sets the localized button labels of this amenity from the map of locales and localized button labels, and sets the default locale.
	 *
	 * @param buttonLabelMap the locales and localized button labels of this amenity
	 * @param defaultLocale the default locale
	 */
	public void setButtonLabelMap(
		Map<Locale, String> buttonLabelMap, Locale defaultLocale);

	/**
	 * Returns the button link of this amenity.
	 *
	 * @return the button link of this amenity
	 */
	public String getButtonLink();

	/**
	 * Returns the localized button link of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param locale the locale of the language
	 * @return the localized button link of this amenity
	 */
	@AutoEscape
	public String getButtonLink(Locale locale);

	/**
	 * Returns the localized button link of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param locale the local of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this amenity. If <code>useDefault</code> is <code>false</code> and no localization exists for the requested language, an empty string will be returned.
	 */
	@AutoEscape
	public String getButtonLink(Locale locale, boolean useDefault);

	/**
	 * Returns the localized button link of this amenity in the language. Uses the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @return the localized button link of this amenity
	 */
	@AutoEscape
	public String getButtonLink(String languageId);

	/**
	 * Returns the localized button link of this amenity in the language, optionally using the default language if no localization exists for the requested language.
	 *
	 * @param languageId the ID of the language
	 * @param useDefault whether to use the default language if no localization exists for the requested language
	 * @return the localized button link of this amenity
	 */
	@AutoEscape
	public String getButtonLink(String languageId, boolean useDefault);

	@AutoEscape
	public String getButtonLinkCurrentLanguageId();

	@AutoEscape
	public String getButtonLinkCurrentValue();

	/**
	 * Returns a map of the locales and localized button links of this amenity.
	 *
	 * @return the locales and localized button links of this amenity
	 */
	public Map<Locale, String> getButtonLinkMap();

	/**
	 * Sets the button link of this amenity.
	 *
	 * @param buttonLink the button link of this amenity
	 */
	public void setButtonLink(String buttonLink);

	/**
	 * Sets the localized button link of this amenity in the language.
	 *
	 * @param buttonLink the localized button link of this amenity
	 * @param locale the locale of the language
	 */
	public void setButtonLink(String buttonLink, Locale locale);

	/**
	 * Sets the localized button link of this amenity in the language, and sets the default locale.
	 *
	 * @param buttonLink the localized button link of this amenity
	 * @param locale the locale of the language
	 * @param defaultLocale the default locale
	 */
	public void setButtonLink(
		String buttonLink, Locale locale, Locale defaultLocale);

	public void setButtonLinkCurrentLanguageId(String languageId);

	/**
	 * Sets the localized button links of this amenity from the map of locales and localized button links.
	 *
	 * @param buttonLinkMap the locales and localized button links of this amenity
	 */
	public void setButtonLinkMap(Map<Locale, String> buttonLinkMap);

	/**
	 * Sets the localized button links of this amenity from the map of locales and localized button links, and sets the default locale.
	 *
	 * @param buttonLinkMap the locales and localized button links of this amenity
	 * @param defaultLocale the default locale
	 */
	public void setButtonLinkMap(
		Map<Locale, String> buttonLinkMap, Locale defaultLocale);

	@Override
	public String[] getAvailableLanguageIds();

	@Override
	public String getDefaultLanguageId();

	@Override
	public void prepareLocalizedFieldsForImport() throws LocaleException;

	@Override
	public void prepareLocalizedFieldsForImport(Locale defaultImportLocale)
		throws LocaleException;

}