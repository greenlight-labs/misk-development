<%@ page import="java.util.Objects" %>
<section class="residence-listing-sec" data-scroll-section>
    <div class="container">
        <div class="row">
            <%
                List<Amenity> amenities = AmenityServiceUtil.getAmenities(scopeGroupId);

                for (Amenity curAmenity : amenities) {
            %>
            <div class="col-12 col-md-6">
                <div class="residence-card">
                    <a href="javascript:" class="img-wrapper image-overlay">
                        <img src="<%= curAmenity.getListingImage(locale) %>" alt="" class="img-fluid">
                    </a>
                    <div class="res-card-info">
                        <h3><%= curAmenity.getListingTitle(locale) %></h3>
                        <p><%= curAmenity.getListingDescription(locale) %></p>
                        <% if(curAmenity.getButtonLabel(locale) != null && curAmenity.getButtonLink(locale)  != null && !Objects.equals(curAmenity.getButtonLabel(locale), "") && !Objects.equals(curAmenity.getButtonLink(locale), "")){ %>
                            <a href="<%=curAmenity.getButtonLink(locale)%>" class="btn btn-outline-white">
                                <span><%=curAmenity.getButtonLabel(locale)%></span><i class="dot-line"></i>
                            </a>
                        <% } %>
                    </div>
                </div>
            </div>
            <%
                }
            %>
        </div>
    </div>
</section>