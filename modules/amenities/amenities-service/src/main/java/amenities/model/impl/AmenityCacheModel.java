/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.model.impl;

import amenities.model.Amenity;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Amenity in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AmenityCacheModel implements CacheModel<Amenity>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AmenityCacheModel)) {
			return false;
		}

		AmenityCacheModel amenityCacheModel = (AmenityCacheModel)object;

		if (amenityId == amenityCacheModel.amenityId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, amenityId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", amenityId=");
		sb.append(amenityId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", listingImage=");
		sb.append(listingImage);
		sb.append(", listingTitle=");
		sb.append(listingTitle);
		sb.append(", listingDescription=");
		sb.append(listingDescription);
		sb.append(", buttonLabel=");
		sb.append(buttonLabel);
		sb.append(", buttonLink=");
		sb.append(buttonLink);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Amenity toEntityModel() {
		AmenityImpl amenityImpl = new AmenityImpl();

		if (uuid == null) {
			amenityImpl.setUuid("");
		}
		else {
			amenityImpl.setUuid(uuid);
		}

		amenityImpl.setAmenityId(amenityId);
		amenityImpl.setGroupId(groupId);
		amenityImpl.setCompanyId(companyId);
		amenityImpl.setUserId(userId);

		if (userName == null) {
			amenityImpl.setUserName("");
		}
		else {
			amenityImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			amenityImpl.setCreateDate(null);
		}
		else {
			amenityImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			amenityImpl.setModifiedDate(null);
		}
		else {
			amenityImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (listingImage == null) {
			amenityImpl.setListingImage("");
		}
		else {
			amenityImpl.setListingImage(listingImage);
		}

		if (listingTitle == null) {
			amenityImpl.setListingTitle("");
		}
		else {
			amenityImpl.setListingTitle(listingTitle);
		}

		if (listingDescription == null) {
			amenityImpl.setListingDescription("");
		}
		else {
			amenityImpl.setListingDescription(listingDescription);
		}

		if (buttonLabel == null) {
			amenityImpl.setButtonLabel("");
		}
		else {
			amenityImpl.setButtonLabel(buttonLabel);
		}

		if (buttonLink == null) {
			amenityImpl.setButtonLink("");
		}
		else {
			amenityImpl.setButtonLink(buttonLink);
		}

		amenityImpl.resetOriginalValues();

		return amenityImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		amenityId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		listingImage = objectInput.readUTF();
		listingTitle = objectInput.readUTF();
		listingDescription = objectInput.readUTF();
		buttonLabel = objectInput.readUTF();
		buttonLink = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(amenityId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (listingImage == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingImage);
		}

		if (listingTitle == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingTitle);
		}

		if (listingDescription == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(listingDescription);
		}

		if (buttonLabel == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLabel);
		}

		if (buttonLink == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(buttonLink);
		}
	}

	public String uuid;
	public long amenityId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String listingImage;
	public String listingTitle;
	public String listingDescription;
	public String buttonLabel;
	public String buttonLink;

}