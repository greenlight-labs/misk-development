/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.service.impl;

import amenities.model.Amenity;
import amenities.service.base.AmenityServiceBaseImpl;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.util.OrderByComparator;
import org.osgi.service.component.annotations.Component;

import java.util.List;

/**
 * The implementation of the amenity remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>amenities.service.AmenityService</code> interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AmenityServiceBaseImpl
 */
@Component(
	property = {
		"json.web.service.context.name=amenity",
		"json.web.service.context.path=Amenity"
	},
	service = AopService.class
)
public class AmenityServiceImpl extends AmenityServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use <code>amenities.service.AmenityServiceUtil</code> to access the amenity remote service.
	 */

	/* *
	 * get Amenities by group
	 * */
	public List<Amenity> getAmenities(long groupId) {

		return amenityPersistence.findByGroupId(groupId);
	}

	public List<Amenity> getAmenities(long groupId, int start, int end,
									  OrderByComparator<Amenity> obc) {

		return amenityPersistence.findByGroupId(groupId, start, end, obc);
	}

	public List<Amenity> getAmenities(long groupId, int start, int end) {

		return amenityPersistence.findByGroupId(groupId, start, end);
	}

	public int getAmenitiesCount(long groupId) {

		return amenityPersistence.countByGroupId(groupId);
	}
}