/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package amenities.service.impl;

import amenities.exception.AmenityListingDescriptionException;
import amenities.exception.AmenityListingImageException;
import amenities.exception.AmenityListingTitleException;
import amenities.model.Amenity;
import amenities.service.base.AmenityLocalServiceBaseImpl;
import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;
import org.osgi.service.component.annotations.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The implementation of the amenity local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>amenities.service.AmenityLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AmenityLocalServiceBaseImpl
 */
@Component(
        property = "model.class.name=amenities.model.Amenity",
        service = AopService.class
)
public class AmenityLocalServiceImpl extends AmenityLocalServiceBaseImpl {

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Use <code>amenities.service.AmenityLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>amenities.service.AmenityLocalServiceUtil</code>.
     */

    public Amenity addAmenity(long userId,
                              Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap, Map<Locale, String> ButtonLabelMap, Map<Locale, String> ButtonLinkMap,
                              ServiceContext serviceContext) throws PortalException {

        long groupId = serviceContext.getScopeGroupId();

        User user = userLocalService.getUserById(userId);

        Date now = new Date();

        validate(listingImageMap, listingTitleMap, listingDescriptionMap);

        long amenityId = counterLocalService.increment();

        Amenity amenity = amenityPersistence.create(amenityId);

        amenity.setUuid(serviceContext.getUuid());
        amenity.setUserId(userId);
        amenity.setGroupId(groupId);
        amenity.setCompanyId(user.getCompanyId());
        amenity.setUserName(user.getFullName());
        amenity.setCreateDate(serviceContext.getCreateDate(now));
        amenity.setModifiedDate(serviceContext.getModifiedDate(now));

        amenity.setListingImageMap(listingImageMap);
        amenity.setListingTitleMap(listingTitleMap);
        amenity.setListingDescriptionMap(listingDescriptionMap);
        amenity.setButtonLabelMap(ButtonLabelMap);
        amenity.setButtonLinkMap(ButtonLinkMap);

        amenity.setExpandoBridgeAttributes(serviceContext);

        amenityPersistence.update(amenity);
        amenityPersistence.clearCache();

        return amenity;
    }

    public Amenity updateAmenity(long userId, long amenityId,
                                 Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap, Map<Locale, String> ButtonLabelMap, Map<Locale, String> ButtonLinkMap,
                                 ServiceContext serviceContext) throws PortalException,
            SystemException {

        Date now = new Date();

        validate(listingImageMap, listingTitleMap, listingDescriptionMap);

        Amenity amenity = getAmenity(amenityId);

        User user = userLocalService.getUser(userId);

        amenity.setUserId(userId);
        amenity.setUserName(user.getFullName());
        amenity.setModifiedDate(serviceContext.getModifiedDate(now));

        amenity.setListingImageMap(listingImageMap);
        amenity.setListingTitleMap(listingTitleMap);
        amenity.setListingDescriptionMap(listingDescriptionMap);
        amenity.setButtonLabelMap(ButtonLabelMap);
        amenity.setButtonLinkMap(ButtonLinkMap);

        amenity.setExpandoBridgeAttributes(serviceContext);

        amenityPersistence.update(amenity);
        amenityPersistence.clearCache();

        return amenity;
    }

    public Amenity deleteAmenity(long amenityId,
                                 ServiceContext serviceContext) throws PortalException,
            SystemException {

        Amenity amenity = getAmenity(amenityId);
        amenity = deleteAmenity(amenity);

        return amenity;
    }

    public List<Amenity> getAmenities(long groupId) {

        return amenityPersistence.findByGroupId(groupId);
    }

    public List<Amenity> getAmenities(long groupId, int start, int end,
                                      OrderByComparator<Amenity> obc) {

        return amenityPersistence.findByGroupId(groupId, start, end, obc);
    }

    public List<Amenity> getAmenities(long groupId, int start, int end) {

        return amenityPersistence.findByGroupId(groupId, start, end);
    }

    public int getAmenitiesCount(long groupId) {

        return amenityPersistence.countByGroupId(groupId);
    }

    protected void validate(
            Map<Locale, String> listingImageMap, Map<Locale, String> listingTitleMap, Map<Locale, String> listingDescriptionMap
    ) throws PortalException {

        Locale locale = LocaleUtil.getSiteDefault();

        String listingImage = listingImageMap.get(locale);

        if (Validator.isNull(listingImage)) {
            throw new AmenityListingImageException();
        }

        String listingTitle = listingTitleMap.get(locale);

        if (Validator.isNull(listingTitle)) {
            throw new AmenityListingTitleException();
        }

        String listingDescription = listingDescriptionMap.get(locale);

        if (Validator.isNull(listingDescription)) {
            throw new AmenityListingDescriptionException();
        }

    }
}