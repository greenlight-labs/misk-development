create index IX_4AEACC98 on misk_amenities (groupId);
create index IX_B6E35C6 on misk_amenities (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_8C7575C8 on misk_amenities (uuid_[$COLUMN_LENGTH:75$], groupId);