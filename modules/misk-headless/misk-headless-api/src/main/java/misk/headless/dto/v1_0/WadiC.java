package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("WadiC")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "WadiC")
public class WadiC implements Serializable {

	public static WadiC toDTO(String json) {
		return ObjectMapperUtil.readValue(WadiC.class, json);
	}

	@Schema
	@Valid
	public AttractionSection getAttractionSection() {
		return attractionSection;
	}

	public void setAttractionSection(AttractionSection attractionSection) {
		this.attractionSection = attractionSection;
	}

	@JsonIgnore
	public void setAttractionSection(
		UnsafeSupplier<AttractionSection, Exception>
			attractionSectionUnsafeSupplier) {

		try {
			attractionSection = attractionSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected AttractionSection attractionSection;

	@Schema
	@Valid
	public BannerSection getBannerSection() {
		return bannerSection;
	}

	public void setBannerSection(BannerSection bannerSection) {
		this.bannerSection = bannerSection;
	}

	@JsonIgnore
	public void setBannerSection(
		UnsafeSupplier<BannerSection, Exception> bannerSectionUnsafeSupplier) {

		try {
			bannerSection = bannerSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected BannerSection bannerSection;

	@Schema
	@Valid
	public BookCourtSection getBookCourtSection() {
		return bookCourtSection;
	}

	public void setBookCourtSection(BookCourtSection bookCourtSection) {
		this.bookCourtSection = bookCourtSection;
	}

	@JsonIgnore
	public void setBookCourtSection(
		UnsafeSupplier<BookCourtSection, Exception>
			bookCourtSectionUnsafeSupplier) {

		try {
			bookCourtSection = bookCourtSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected BookCourtSection bookCourtSection;

	@Schema
	@Valid
	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	@JsonIgnore
	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected EventsSection eventsSection;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof WadiC)) {
			return false;
		}

		WadiC wadiC = (WadiC)object;

		return Objects.equals(toString(), wadiC.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (attractionSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionSection\": ");

			sb.append(String.valueOf(attractionSection));
		}

		if (bannerSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bannerSection\": ");

			sb.append(String.valueOf(bannerSection));
		}

		if (bookCourtSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookCourtSection\": ");

			sb.append(String.valueOf(bookCourtSection));
		}

		if (eventsSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(eventsSection));
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.WadiC", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}