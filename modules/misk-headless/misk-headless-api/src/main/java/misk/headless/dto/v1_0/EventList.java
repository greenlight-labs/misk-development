package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("EventList")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "EventList")
public class EventList implements Serializable {

	public static EventList toDTO(String json) {
		return ObjectMapperUtil.readValue(EventList.class, json);
	}

	@Schema
	@Valid
	public AddVisitor getAttendesList() {
		return attendesList;
	}

	public void setAttendesList(AddVisitor attendesList) {
		this.attendesList = attendesList;
	}

	@JsonIgnore
	public void setAttendesList(
		UnsafeSupplier<AddVisitor, Exception> attendesListUnsafeSupplier) {

		try {
			attendesList = attendesListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected AddVisitor attendesList;

	@Schema
	public String getBuyTicketLink() {
		return buyTicketLink;
	}

	public void setBuyTicketLink(String buyTicketLink) {
		this.buyTicketLink = buyTicketLink;
	}

	@JsonIgnore
	public void setBuyTicketLink(
		UnsafeSupplier<String, Exception> buyTicketLinkUnsafeSupplier) {

		try {
			buyTicketLink = buyTicketLinkUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String buyTicketLink;

	@Schema
	public String getCatColor() {
		return catColor;
	}

	public void setCatColor(String catColor) {
		this.catColor = catColor;
	}

	@JsonIgnore
	public void setCatColor(
		UnsafeSupplier<String, Exception> catColorUnsafeSupplier) {

		try {
			catColor = catColorUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String catColor;

	@Schema
	@Valid
	public CategoryList[] getCategoryLists() {
		return categoryLists;
	}

	public void setCategoryLists(CategoryList[] categoryLists) {
		this.categoryLists = categoryLists;
	}

	@JsonIgnore
	public void setCategoryLists(
		UnsafeSupplier<CategoryList[], Exception> categoryListsUnsafeSupplier) {

		try {
			categoryLists = categoryListsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected CategoryList[] categoryLists;

	@Schema
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@JsonIgnore
	public void setCurrency(
		UnsafeSupplier<String, Exception> currencyUnsafeSupplier) {

		try {
			currency = currencyUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String currency;

	@Schema
	public String getDisplaydate() {
		return displaydate;
	}

	public void setDisplaydate(String displaydate) {
		this.displaydate = displaydate;
	}

	@JsonIgnore
	public void setDisplaydate(
		UnsafeSupplier<String, Exception> displaydateUnsafeSupplier) {

		try {
			displaydate = displaydateUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String displaydate;

	@Schema
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnore
	public void setEmail(
		UnsafeSupplier<String, Exception> emailUnsafeSupplier) {

		try {
			email = emailUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String email;

	@Schema
	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	@JsonIgnore
	public void setEventDescription(
		UnsafeSupplier<String, Exception> eventDescriptionUnsafeSupplier) {

		try {
			eventDescription = eventDescriptionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventDescription;

	@Schema
	public String getEventEndDateTime() {
		return eventEndDateTime;
	}

	public void setEventEndDateTime(String eventEndDateTime) {
		this.eventEndDateTime = eventEndDateTime;
	}

	@JsonIgnore
	public void setEventEndDateTime(
		UnsafeSupplier<String, Exception> eventEndDateTimeUnsafeSupplier) {

		try {
			eventEndDateTime = eventEndDateTimeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventEndDateTime;

	@Schema
	public String getEventFeaturedImage() {
		return eventFeaturedImage;
	}

	public void setEventFeaturedImage(String eventFeaturedImage) {
		this.eventFeaturedImage = eventFeaturedImage;
	}

	@JsonIgnore
	public void setEventFeaturedImage(
		UnsafeSupplier<String, Exception> eventFeaturedImageUnsafeSupplier) {

		try {
			eventFeaturedImage = eventFeaturedImageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventFeaturedImage;

	@Schema
	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	@JsonIgnore
	public void setEventId(
		UnsafeSupplier<Long, Exception> eventIdUnsafeSupplier) {

		try {
			eventId = eventIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long eventId;

	@Schema
	@Valid
	public EventList[] getEventLists() {
		return eventLists;
	}

	public void setEventLists(EventList[] eventLists) {
		this.eventLists = eventLists;
	}

	@JsonIgnore
	public void setEventLists(
		UnsafeSupplier<EventList[], Exception> eventListsUnsafeSupplier) {

		try {
			eventLists = eventListsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected EventList[] eventLists;

	@Schema
	public String getEventLocation() {
		return eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	@JsonIgnore
	public void setEventLocation(
		UnsafeSupplier<String, Exception> eventLocationUnsafeSupplier) {

		try {
			eventLocation = eventLocationUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventLocation;

	@Schema
	public String getEventMonth() {
		return eventMonth;
	}

	public void setEventMonth(String eventMonth) {
		this.eventMonth = eventMonth;
	}

	@JsonIgnore
	public void setEventMonth(
		UnsafeSupplier<String, Exception> eventMonthUnsafeSupplier) {

		try {
			eventMonth = eventMonthUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventMonth;

	@Schema
	public String getEventPrice() {
		return eventPrice;
	}

	public void setEventPrice(String eventPrice) {
		this.eventPrice = eventPrice;
	}

	@JsonIgnore
	public void setEventPrice(
		UnsafeSupplier<String, Exception> eventPriceUnsafeSupplier) {

		try {
			eventPrice = eventPriceUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventPrice;

	@Schema
	public String getEventStartDateTime() {
		return eventStartDateTime;
	}

	public void setEventStartDateTime(String eventStartDateTime) {
		this.eventStartDateTime = eventStartDateTime;
	}

	@JsonIgnore
	public void setEventStartDateTime(
		UnsafeSupplier<String, Exception> eventStartDateTimeUnsafeSupplier) {

		try {
			eventStartDateTime = eventStartDateTimeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventStartDateTime;

	@Schema
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	@JsonIgnore
	public void setEventType(
		UnsafeSupplier<String, Exception> eventTypeUnsafeSupplier) {

		try {
			eventType = eventTypeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventType;

	@Schema
	public String getEventcategory() {
		return eventcategory;
	}

	public void setEventcategory(String eventcategory) {
		this.eventcategory = eventcategory;
	}

	@JsonIgnore
	public void setEventcategory(
		UnsafeSupplier<String, Exception> eventcategoryUnsafeSupplier) {

		try {
			eventcategory = eventcategoryUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String eventcategory;

	@Schema
	public Boolean getIsFavourite() {
		return isFavourite;
	}

	public void setIsFavourite(Boolean isFavourite) {
		this.isFavourite = isFavourite;
	}

	@JsonIgnore
	public void setIsFavourite(
		UnsafeSupplier<Boolean, Exception> isFavouriteUnsafeSupplier) {

		try {
			isFavourite = isFavouriteUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean isFavourite;

	@Schema
	public String getLan() {
		return lan;
	}

	public void setLan(String lan) {
		this.lan = lan;
	}

	@JsonIgnore
	public void setLan(UnsafeSupplier<String, Exception> lanUnsafeSupplier) {
		try {
			lan = lanUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String lan;

	@Schema
	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	@JsonIgnore
	public void setLat(UnsafeSupplier<String, Exception> latUnsafeSupplier) {
		try {
			lat = latUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String lat;

	@Schema
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@JsonIgnore
	public void setLink(UnsafeSupplier<String, Exception> linkUnsafeSupplier) {
		try {
			link = linkUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String link;

	@Schema
	public String getLocationLabel() {
		return locationLabel;
	}

	public void setLocationLabel(String locationLabel) {
		this.locationLabel = locationLabel;
	}

	@JsonIgnore
	public void setLocationLabel(
		UnsafeSupplier<String, Exception> locationLabelUnsafeSupplier) {

		try {
			locationLabel = locationLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String locationLabel;

	@Schema
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@JsonIgnore
	public void setPayType(
		UnsafeSupplier<String, Exception> payTypeUnsafeSupplier) {

		try {
			payType = payTypeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String payType;

	@Schema
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonIgnore
	public void setPhone(
		UnsafeSupplier<String, Exception> phoneUnsafeSupplier) {

		try {
			phone = phoneUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String phone;

	@Schema
	@Valid
	public Schedule[] getSchedules() {
		return schedules;
	}

	public void setSchedules(Schedule[] schedules) {
		this.schedules = schedules;
	}

	@JsonIgnore
	public void setSchedules(
		UnsafeSupplier<Schedule[], Exception> schedulesUnsafeSupplier) {

		try {
			schedules = schedulesUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Schedule[] schedules;

	@Schema
	public String[] getSliderimage() {
		return sliderimage;
	}

	public void setSliderimage(String[] sliderimage) {
		this.sliderimage = sliderimage;
	}

	@JsonIgnore
	public void setSliderimage(
		UnsafeSupplier<String[], Exception> sliderimageUnsafeSupplier) {

		try {
			sliderimage = sliderimageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String[] sliderimage;

	@Schema
	public String getTagSlug() {
		return tagSlug;
	}

	public void setTagSlug(String tagSlug) {
		this.tagSlug = tagSlug;
	}

	@JsonIgnore
	public void setTagSlug(
		UnsafeSupplier<String, Exception> tagSlugUnsafeSupplier) {

		try {
			tagSlug = tagSlugUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String tagSlug;

	@Schema
	public String getTagTitle() {
		return tagTitle;
	}

	public void setTagTitle(String tagTitle) {
		this.tagTitle = tagTitle;
	}

	@JsonIgnore
	public void setTagTitle(
		UnsafeSupplier<String, Exception> tagTitleUnsafeSupplier) {

		try {
			tagTitle = tagTitleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String tagTitle;

	@Schema
	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@JsonIgnore
	public void setTime(UnsafeSupplier<String, Exception> timeUnsafeSupplier) {
		try {
			time = timeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String time;

	@Schema
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@JsonIgnore
	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String title;

	@Schema
	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	@JsonIgnore
	public void setVenue(
		UnsafeSupplier<String, Exception> venueUnsafeSupplier) {

		try {
			venue = venueUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String venue;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EventList)) {
			return false;
		}

		EventList eventList = (EventList)object;

		return Objects.equals(toString(), eventList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (attendesList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attendesList\": ");

			sb.append(String.valueOf(attendesList));
		}

		if (buyTicketLink != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"buyTicketLink\": ");

			sb.append("\"");

			sb.append(_escape(buyTicketLink));

			sb.append("\"");
		}

		if (catColor != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catColor\": ");

			sb.append("\"");

			sb.append(_escape(catColor));

			sb.append("\"");
		}

		if (categoryLists != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"categoryLists\": ");

			sb.append("[");

			for (int i = 0; i < categoryLists.length; i++) {
				sb.append(String.valueOf(categoryLists[i]));

				if ((i + 1) < categoryLists.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (currency != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"currency\": ");

			sb.append("\"");

			sb.append(_escape(currency));

			sb.append("\"");
		}

		if (displaydate != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"displaydate\": ");

			sb.append("\"");

			sb.append(_escape(displaydate));

			sb.append("\"");
		}

		if (email != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"email\": ");

			sb.append("\"");

			sb.append(_escape(email));

			sb.append("\"");
		}

		if (eventDescription != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventDescription\": ");

			sb.append("\"");

			sb.append(_escape(eventDescription));

			sb.append("\"");
		}

		if (eventEndDateTime != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventEndDateTime\": ");

			sb.append("\"");

			sb.append(_escape(eventEndDateTime));

			sb.append("\"");
		}

		if (eventFeaturedImage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventFeaturedImage\": ");

			sb.append("\"");

			sb.append(_escape(eventFeaturedImage));

			sb.append("\"");
		}

		if (eventId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventId\": ");

			sb.append(eventId);
		}

		if (eventLists != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventLists\": ");

			sb.append("[");

			for (int i = 0; i < eventLists.length; i++) {
				sb.append(String.valueOf(eventLists[i]));

				if ((i + 1) < eventLists.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (eventLocation != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventLocation\": ");

			sb.append("\"");

			sb.append(_escape(eventLocation));

			sb.append("\"");
		}

		if (eventMonth != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventMonth\": ");

			sb.append("\"");

			sb.append(_escape(eventMonth));

			sb.append("\"");
		}

		if (eventPrice != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventPrice\": ");

			sb.append("\"");

			sb.append(_escape(eventPrice));

			sb.append("\"");
		}

		if (eventStartDateTime != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventStartDateTime\": ");

			sb.append("\"");

			sb.append(_escape(eventStartDateTime));

			sb.append("\"");
		}

		if (eventType != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventType\": ");

			sb.append("\"");

			sb.append(_escape(eventType));

			sb.append("\"");
		}

		if (eventcategory != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventcategory\": ");

			sb.append("\"");

			sb.append(_escape(eventcategory));

			sb.append("\"");
		}

		if (isFavourite != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"isFavourite\": ");

			sb.append(isFavourite);
		}

		if (lan != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lan\": ");

			sb.append("\"");

			sb.append(_escape(lan));

			sb.append("\"");
		}

		if (lat != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lat\": ");

			sb.append("\"");

			sb.append(_escape(lat));

			sb.append("\"");
		}

		if (link != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"link\": ");

			sb.append("\"");

			sb.append(_escape(link));

			sb.append("\"");
		}

		if (locationLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLabel\": ");

			sb.append("\"");

			sb.append(_escape(locationLabel));

			sb.append("\"");
		}

		if (payType != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"payType\": ");

			sb.append("\"");

			sb.append(_escape(payType));

			sb.append("\"");
		}

		if (phone != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"phone\": ");

			sb.append("\"");

			sb.append(_escape(phone));

			sb.append("\"");
		}

		if (schedules != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"schedules\": ");

			sb.append("[");

			for (int i = 0; i < schedules.length; i++) {
				sb.append(String.valueOf(schedules[i]));

				if ((i + 1) < schedules.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (sliderimage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"sliderimage\": ");

			sb.append("[");

			for (int i = 0; i < sliderimage.length; i++) {
				sb.append("\"");

				sb.append(_escape(sliderimage[i]));

				sb.append("\"");

				if ((i + 1) < sliderimage.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (tagSlug != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"tagSlug\": ");

			sb.append("\"");

			sb.append(_escape(tagSlug));

			sb.append("\"");
		}

		if (tagTitle != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"tagTitle\": ");

			sb.append("\"");

			sb.append(_escape(tagTitle));

			sb.append("\"");
		}

		if (time != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"time\": ");

			sb.append("\"");

			sb.append(_escape(time));

			sb.append("\"");
		}

		if (title != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(title));

			sb.append("\"");
		}

		if (venue != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"venue\": ");

			sb.append("\"");

			sb.append(_escape(venue));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.EventList", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}