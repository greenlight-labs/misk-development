package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("BookSpaceList")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "BookSpaceList")
public class BookSpaceList implements Serializable {

	public static BookSpaceList toDTO(String json) {
		return ObjectMapperUtil.readValue(BookSpaceList.class, json);
	}

	@Schema
	public String getArea() {
		return Area;
	}

	public void setArea(String Area) {
		this.Area = Area;
	}

	@JsonIgnore
	public void setArea(UnsafeSupplier<String, Exception> AreaUnsafeSupplier) {
		try {
			Area = AreaUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String Area;

	@Schema
	public String getBookSpaceIcon() {
		return BookSpaceIcon;
	}

	public void setBookSpaceIcon(String BookSpaceIcon) {
		this.BookSpaceIcon = BookSpaceIcon;
	}

	@JsonIgnore
	public void setBookSpaceIcon(
		UnsafeSupplier<String, Exception> BookSpaceIconUnsafeSupplier) {

		try {
			BookSpaceIcon = BookSpaceIconUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String BookSpaceIcon;

	@Schema
	public String getBookSpaceImages() {
		return BookSpaceImages;
	}

	public void setBookSpaceImages(String BookSpaceImages) {
		this.BookSpaceImages = BookSpaceImages;
	}

	@JsonIgnore
	public void setBookSpaceImages(
		UnsafeSupplier<String, Exception> BookSpaceImagesUnsafeSupplier) {

		try {
			BookSpaceImages = BookSpaceImagesUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String BookSpaceImages;

	@Schema
	public String getBookSpaceTitle() {
		return BookSpaceTitle;
	}

	public void setBookSpaceTitle(String BookSpaceTitle) {
		this.BookSpaceTitle = BookSpaceTitle;
	}

	@JsonIgnore
	public void setBookSpaceTitle(
		UnsafeSupplier<String, Exception> BookSpaceTitleUnsafeSupplier) {

		try {
			BookSpaceTitle = BookSpaceTitleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String BookSpaceTitle;

	@Schema
	public String getCapacity() {
		return Capacity;
	}

	public void setCapacity(String Capacity) {
		this.Capacity = Capacity;
	}

	@JsonIgnore
	public void setCapacity(
		UnsafeSupplier<String, Exception> CapacityUnsafeSupplier) {

		try {
			Capacity = CapacityUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String Capacity;

	@Schema
	public String getDuration() {
		return Duration;
	}

	public void setDuration(String Duration) {
		this.Duration = Duration;
	}

	@JsonIgnore
	public void setDuration(
		UnsafeSupplier<String, Exception> DurationUnsafeSupplier) {

		try {
			Duration = DurationUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String Duration;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BookSpaceList)) {
			return false;
		}

		BookSpaceList bookSpaceList = (BookSpaceList)object;

		return Objects.equals(toString(), bookSpaceList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (Area != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Area\": ");

			sb.append("\"");

			sb.append(_escape(Area));

			sb.append("\"");
		}

		if (BookSpaceIcon != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceIcon\": ");

			sb.append("\"");

			sb.append(_escape(BookSpaceIcon));

			sb.append("\"");
		}

		if (BookSpaceImages != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceImages\": ");

			sb.append("\"");

			sb.append(_escape(BookSpaceImages));

			sb.append("\"");
		}

		if (BookSpaceTitle != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceTitle\": ");

			sb.append("\"");

			sb.append(_escape(BookSpaceTitle));

			sb.append("\"");
		}

		if (Capacity != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Capacity\": ");

			sb.append("\"");

			sb.append(_escape(Capacity));

			sb.append("\"");
		}

		if (Duration != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Duration\": ");

			sb.append("\"");

			sb.append(_escape(Duration));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.BookSpaceList",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}