package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("Category")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "Category")
public class Category implements Serializable {

	public static Category toDTO(String json) {
		return ObjectMapperUtil.readValue(Category.class, json);
	}

	@Schema
	public String getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}

	@JsonIgnore
	public void setItemCategory(
		UnsafeSupplier<String, Exception> itemCategoryUnsafeSupplier) {

		try {
			itemCategory = itemCategoryUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String itemCategory;

	@Schema
	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}

	@JsonIgnore
	public void setItemImage(
		UnsafeSupplier<String, Exception> itemImageUnsafeSupplier) {

		try {
			itemImage = itemImageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String itemImage;

	@Schema
	public String getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}

	@JsonIgnore
	public void setItemPrice(
		UnsafeSupplier<String, Exception> itemPriceUnsafeSupplier) {

		try {
			itemPrice = itemPriceUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String itemPrice;

	@Schema
	public String getItemSummary() {
		return itemSummary;
	}

	public void setItemSummary(String itemSummary) {
		this.itemSummary = itemSummary;
	}

	@JsonIgnore
	public void setItemSummary(
		UnsafeSupplier<String, Exception> itemSummaryUnsafeSupplier) {

		try {
			itemSummary = itemSummaryUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String itemSummary;

	@Schema
	public String getItemTitle() {
		return itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}

	@JsonIgnore
	public void setItemTitle(
		UnsafeSupplier<String, Exception> itemTitleUnsafeSupplier) {

		try {
			itemTitle = itemTitleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String itemTitle;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Category)) {
			return false;
		}

		Category category = (Category)object;

		return Objects.equals(toString(), category.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (itemCategory != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemCategory\": ");

			sb.append("\"");

			sb.append(_escape(itemCategory));

			sb.append("\"");
		}

		if (itemImage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemImage\": ");

			sb.append("\"");

			sb.append(_escape(itemImage));

			sb.append("\"");
		}

		if (itemPrice != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemPrice\": ");

			sb.append("\"");

			sb.append(_escape(itemPrice));

			sb.append("\"");
		}

		if (itemSummary != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemSummary\": ");

			sb.append("\"");

			sb.append(_escape(itemSummary));

			sb.append("\"");
		}

		if (itemTitle != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemTitle\": ");

			sb.append("\"");

			sb.append(_escape(itemTitle));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.Category", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}