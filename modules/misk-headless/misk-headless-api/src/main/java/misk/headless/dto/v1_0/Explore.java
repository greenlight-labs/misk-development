package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("Explore")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "Explore")
public class Explore implements Serializable {

	public static Explore toDTO(String json) {
		return ObjectMapperUtil.readValue(Explore.class, json);
	}

	@Schema
	@Valid
	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	@JsonIgnore
	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected EventsSection eventsSection;

	@Schema
	public String getExploreLabel() {
		return exploreLabel;
	}

	public void setExploreLabel(String exploreLabel) {
		this.exploreLabel = exploreLabel;
	}

	@JsonIgnore
	public void setExploreLabel(
		UnsafeSupplier<String, Exception> exploreLabelUnsafeSupplier) {

		try {
			exploreLabel = exploreLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String exploreLabel;

	@Schema
	@Valid
	public ExploreList[] getExploreList() {
		return exploreList;
	}

	public void setExploreList(ExploreList[] exploreList) {
		this.exploreList = exploreList;
	}

	@JsonIgnore
	public void setExploreList(
		UnsafeSupplier<ExploreList[], Exception> exploreListUnsafeSupplier) {

		try {
			exploreList = exploreListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected ExploreList[] exploreList;

	@Schema
	public String getExpolerId() {
		return expolerId;
	}

	public void setExpolerId(String expolerId) {
		this.expolerId = expolerId;
	}

	@JsonIgnore
	public void setExpolerId(
		UnsafeSupplier<String, Exception> expolerIdUnsafeSupplier) {

		try {
			expolerId = expolerIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String expolerId;

	@Schema
	public String getFaclitiesLabel() {
		return faclitiesLabel;
	}

	public void setFaclitiesLabel(String faclitiesLabel) {
		this.faclitiesLabel = faclitiesLabel;
	}

	@JsonIgnore
	public void setFaclitiesLabel(
		UnsafeSupplier<String, Exception> faclitiesLabelUnsafeSupplier) {

		try {
			faclitiesLabel = faclitiesLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String faclitiesLabel;

	@Schema
	@Valid
	public FaclitiesList[] getFaclitiesList() {
		return faclitiesList;
	}

	public void setFaclitiesList(FaclitiesList[] faclitiesList) {
		this.faclitiesList = faclitiesList;
	}

	@JsonIgnore
	public void setFaclitiesList(
		UnsafeSupplier<FaclitiesList[], Exception>
			faclitiesListUnsafeSupplier) {

		try {
			faclitiesList = faclitiesListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected FaclitiesList[] faclitiesList;

	@Schema
	public String[] getGalleryImagesList() {
		return galleryImagesList;
	}

	public void setGalleryImagesList(String[] galleryImagesList) {
		this.galleryImagesList = galleryImagesList;
	}

	@JsonIgnore
	public void setGalleryImagesList(
		UnsafeSupplier<String[], Exception> galleryImagesListUnsafeSupplier) {

		try {
			galleryImagesList = galleryImagesListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String[] galleryImagesList;

	@Schema
	public String getGalleryLabel() {
		return galleryLabel;
	}

	public void setGalleryLabel(String galleryLabel) {
		this.galleryLabel = galleryLabel;
	}

	@JsonIgnore
	public void setGalleryLabel(
		UnsafeSupplier<String, Exception> galleryLabelUnsafeSupplier) {

		try {
			galleryLabel = galleryLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String galleryLabel;

	@Schema
	public String getHeaderBanner() {
		return headerBanner;
	}

	public void setHeaderBanner(String headerBanner) {
		this.headerBanner = headerBanner;
	}

	@JsonIgnore
	public void setHeaderBanner(
		UnsafeSupplier<String, Exception> headerBannerUnsafeSupplier) {

		try {
			headerBanner = headerBannerUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String headerBanner;

	@Schema
	public String getHeaderDesc() {
		return headerDesc;
	}

	public void setHeaderDesc(String headerDesc) {
		this.headerDesc = headerDesc;
	}

	@JsonIgnore
	public void setHeaderDesc(
		UnsafeSupplier<String, Exception> headerDescUnsafeSupplier) {

		try {
			headerDesc = headerDescUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String headerDesc;

	@Schema
	public String getHeaderTitle() {
		return headerTitle;
	}

	public void setHeaderTitle(String headerTitle) {
		this.headerTitle = headerTitle;
	}

	@JsonIgnore
	public void setHeaderTitle(
		UnsafeSupplier<String, Exception> headerTitleUnsafeSupplier) {

		try {
			headerTitle = headerTitleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String headerTitle;

	@Schema
	public String getLocationLabel() {
		return locationLabel;
	}

	public void setLocationLabel(String locationLabel) {
		this.locationLabel = locationLabel;
	}

	@JsonIgnore
	public void setLocationLabel(
		UnsafeSupplier<String, Exception> locationLabelUnsafeSupplier) {

		try {
			locationLabel = locationLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String locationLabel;

	@Schema
	public String getLocationLatitude() {
		return locationLatitude;
	}

	public void setLocationLatitude(String locationLatitude) {
		this.locationLatitude = locationLatitude;
	}

	@JsonIgnore
	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> locationLatitudeUnsafeSupplier) {

		try {
			locationLatitude = locationLatitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String locationLatitude;

	@Schema
	public String getLocationLongitude() {
		return locationLongitude;
	}

	public void setLocationLongitude(String locationLongitude) {
		this.locationLongitude = locationLongitude;
	}

	@JsonIgnore
	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> locationLongitudeUnsafeSupplier) {

		try {
			locationLongitude = locationLongitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String locationLongitude;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Explore)) {
			return false;
		}

		Explore explore = (Explore)object;

		return Objects.equals(toString(), explore.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (eventsSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(eventsSection));
		}

		if (exploreLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreLabel\": ");

			sb.append("\"");

			sb.append(_escape(exploreLabel));

			sb.append("\"");
		}

		if (exploreList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreList\": ");

			sb.append("[");

			for (int i = 0; i < exploreList.length; i++) {
				sb.append(String.valueOf(exploreList[i]));

				if ((i + 1) < exploreList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (expolerId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"expolerId\": ");

			sb.append("\"");

			sb.append(_escape(expolerId));

			sb.append("\"");
		}

		if (faclitiesLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesLabel\": ");

			sb.append("\"");

			sb.append(_escape(faclitiesLabel));

			sb.append("\"");
		}

		if (faclitiesList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesList\": ");

			sb.append("[");

			for (int i = 0; i < faclitiesList.length; i++) {
				sb.append(String.valueOf(faclitiesList[i]));

				if ((i + 1) < faclitiesList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (galleryImagesList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryImagesList\": ");

			sb.append("[");

			for (int i = 0; i < galleryImagesList.length; i++) {
				sb.append("\"");

				sb.append(_escape(galleryImagesList[i]));

				sb.append("\"");

				if ((i + 1) < galleryImagesList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (galleryLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryLabel\": ");

			sb.append("\"");

			sb.append(_escape(galleryLabel));

			sb.append("\"");
		}

		if (headerBanner != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"headerBanner\": ");

			sb.append("\"");

			sb.append(_escape(headerBanner));

			sb.append("\"");
		}

		if (headerDesc != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"headerDesc\": ");

			sb.append("\"");

			sb.append(_escape(headerDesc));

			sb.append("\"");
		}

		if (headerTitle != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"headerTitle\": ");

			sb.append("\"");

			sb.append(_escape(headerTitle));

			sb.append("\"");
		}

		if (locationLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLabel\": ");

			sb.append("\"");

			sb.append(_escape(locationLabel));

			sb.append("\"");
		}

		if (locationLatitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(locationLatitude));

			sb.append("\"");
		}

		if (locationLongitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(locationLongitude));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.Explore", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}