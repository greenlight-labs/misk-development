package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("AppUser")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "AppUser")
public class AppUser implements Serializable {

	public static AppUser toDTO(String json) {
		return ObjectMapperUtil.readValue(AppUser.class, json);
	}

	@Schema
	public String getAndroidDeviceToken() {
		return androidDeviceToken;
	}

	public void setAndroidDeviceToken(String androidDeviceToken) {
		this.androidDeviceToken = androidDeviceToken;
	}

	@JsonIgnore
	public void setAndroidDeviceToken(
		UnsafeSupplier<String, Exception> androidDeviceTokenUnsafeSupplier) {

		try {
			androidDeviceToken = androidDeviceTokenUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String androidDeviceToken;

	@Schema(description = "The App User identifier")
	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	@JsonIgnore
	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(description = "The App User identifier")
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long appUserId;

	@Schema
	public String getAppleUserId() {
		return appleUserId;
	}

	public void setAppleUserId(String appleUserId) {
		this.appleUserId = appleUserId;
	}

	@JsonIgnore
	public void setAppleUserId(
		UnsafeSupplier<String, Exception> appleUserIdUnsafeSupplier) {

		try {
			appleUserId = appleUserIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String appleUserId;

	@Schema
	public Boolean getAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(Boolean autoLogin) {
		this.autoLogin = autoLogin;
	}

	@JsonIgnore
	public void setAutoLogin(
		UnsafeSupplier<Boolean, Exception> autoLoginUnsafeSupplier) {

		try {
			autoLogin = autoLoginUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean autoLogin;

	@Schema
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	@JsonIgnore
	public void setConfirmPassword(
		UnsafeSupplier<String, Exception> confirmPasswordUnsafeSupplier) {

		try {
			confirmPassword = confirmPasswordUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String confirmPassword;

	@Schema
	public String getDeactivateReason() {
		return deactivateReason;
	}

	public void setDeactivateReason(String deactivateReason) {
		this.deactivateReason = deactivateReason;
	}

	@JsonIgnore
	public void setDeactivateReason(
		UnsafeSupplier<String, Exception> deactivateReasonUnsafeSupplier) {

		try {
			deactivateReason = deactivateReasonUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String deactivateReason;

	@Schema
	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	@JsonIgnore
	public void setDeleteReason(
		UnsafeSupplier<String, Exception> deleteReasonUnsafeSupplier) {

		try {
			deleteReason = deleteReasonUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String deleteReason;

	@Schema
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@JsonIgnore
	public void setEmailAddress(
		UnsafeSupplier<String, Exception> emailAddressUnsafeSupplier) {

		try {
			emailAddress = emailAddressUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String emailAddress;

	@Schema
	public Boolean getEmailAddressVerified() {
		return emailAddressVerified;
	}

	public void setEmailAddressVerified(Boolean emailAddressVerified) {
		this.emailAddressVerified = emailAddressVerified;
	}

	@JsonIgnore
	public void setEmailAddressVerified(
		UnsafeSupplier<Boolean, Exception> emailAddressVerifiedUnsafeSupplier) {

		try {
			emailAddressVerified = emailAddressVerifiedUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean emailAddressVerified;

	@Schema
	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	@JsonIgnore
	public void setFacebookId(
		UnsafeSupplier<String, Exception> facebookIdUnsafeSupplier) {

		try {
			facebookId = facebookIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String facebookId;

	@Schema
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonIgnore
	public void setFullName(
		UnsafeSupplier<String, Exception> fullNameUnsafeSupplier) {

		try {
			fullName = fullNameUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String fullName;

	@Schema
	public String getGcpToken() {
		return gcpToken;
	}

	public void setGcpToken(String gcpToken) {
		this.gcpToken = gcpToken;
	}

	@JsonIgnore
	public void setGcpToken(
		UnsafeSupplier<String, Exception> gcpTokenUnsafeSupplier) {

		try {
			gcpToken = gcpTokenUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String gcpToken;

	@Schema
	public String getGoogleUserId() {
		return googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		this.googleUserId = googleUserId;
	}

	@JsonIgnore
	public void setGoogleUserId(
		UnsafeSupplier<String, Exception> googleUserIdUnsafeSupplier) {

		try {
			googleUserId = googleUserIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String googleUserId;

	@Schema
	public String getIosDeviceToken() {
		return iosDeviceToken;
	}

	public void setIosDeviceToken(String iosDeviceToken) {
		this.iosDeviceToken = iosDeviceToken;
	}

	@JsonIgnore
	public void setIosDeviceToken(
		UnsafeSupplier<String, Exception> iosDeviceTokenUnsafeSupplier) {

		try {
			iosDeviceToken = iosDeviceTokenUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String iosDeviceToken;

	@Schema
	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	@JsonIgnore
	public void setIsVerified(
		UnsafeSupplier<Boolean, Exception> isVerifiedUnsafeSupplier) {

		try {
			isVerified = isVerifiedUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean isVerified;

	@Schema
	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	@JsonIgnore
	public void setLanguageId(
		UnsafeSupplier<String, Exception> languageIdUnsafeSupplier) {

		try {
			languageId = languageIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String languageId;

	@Schema
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@JsonIgnore
	public void setNewPassword(
		UnsafeSupplier<String, Exception> newPasswordUnsafeSupplier) {

		try {
			newPassword = newPasswordUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String newPassword;

	@Schema
	public String getPasscode() {
		return passcode;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	@JsonIgnore
	public void setPasscode(
		UnsafeSupplier<String, Exception> passcodeUnsafeSupplier) {

		try {
			passcode = passcodeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String passcode;

	@Schema
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore
	public void setPassword(
		UnsafeSupplier<String, Exception> passwordUnsafeSupplier) {

		try {
			password = passwordUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String password;

	@Schema
	public String getPasswordResetToken() {
		return passwordResetToken;
	}

	public void setPasswordResetToken(String passwordResetToken) {
		this.passwordResetToken = passwordResetToken;
	}

	@JsonIgnore
	public void setPasswordResetToken(
		UnsafeSupplier<String, Exception> passwordResetTokenUnsafeSupplier) {

		try {
			passwordResetToken = passwordResetTokenUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String passwordResetToken;

	@Schema
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@JsonIgnore
	public void setPhoneNumber(
		UnsafeSupplier<String, Exception> phoneNumberUnsafeSupplier) {

		try {
			phoneNumber = phoneNumberUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String phoneNumber;

	@Schema
	public Boolean getPhoneNumberVerified() {
		return phoneNumberVerified;
	}

	public void setPhoneNumberVerified(Boolean phoneNumberVerified) {
		this.phoneNumberVerified = phoneNumberVerified;
	}

	@JsonIgnore
	public void setPhoneNumberVerified(
		UnsafeSupplier<Boolean, Exception> phoneNumberVerifiedUnsafeSupplier) {

		try {
			phoneNumberVerified = phoneNumberVerifiedUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean phoneNumberVerified;

	@Schema
	public String getProfileBannerImage() {
		return profileBannerImage;
	}

	public void setProfileBannerImage(String profileBannerImage) {
		this.profileBannerImage = profileBannerImage;
	}

	@JsonIgnore
	public void setProfileBannerImage(
		UnsafeSupplier<String, Exception> profileBannerImageUnsafeSupplier) {

		try {
			profileBannerImage = profileBannerImageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String profileBannerImage;

	@Schema
	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	@JsonIgnore
	public void setProfileImage(
		UnsafeSupplier<String, Exception> profileImageUnsafeSupplier) {

		try {
			profileImage = profileImageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String profileImage;

	@Schema
	@Valid
	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	@JsonIgnore
	public void setRequestStatus(
		UnsafeSupplier<RequestStatus, Exception> requestStatusUnsafeSupplier) {

		try {
			requestStatus = requestStatusUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected RequestStatus requestStatus;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AppUser)) {
			return false;
		}

		AppUser appUser = (AppUser)object;

		return Objects.equals(toString(), appUser.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (androidDeviceToken != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"androidDeviceToken\": ");

			sb.append("\"");

			sb.append(_escape(androidDeviceToken));

			sb.append("\"");
		}

		if (appUserId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(appUserId);
		}

		if (appleUserId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appleUserId\": ");

			sb.append("\"");

			sb.append(_escape(appleUserId));

			sb.append("\"");
		}

		if (autoLogin != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"autoLogin\": ");

			sb.append(autoLogin);
		}

		if (confirmPassword != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"confirmPassword\": ");

			sb.append("\"");

			sb.append(_escape(confirmPassword));

			sb.append("\"");
		}

		if (deactivateReason != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deactivateReason\": ");

			sb.append("\"");

			sb.append(_escape(deactivateReason));

			sb.append("\"");
		}

		if (deleteReason != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deleteReason\": ");

			sb.append("\"");

			sb.append(_escape(deleteReason));

			sb.append("\"");
		}

		if (emailAddress != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"emailAddress\": ");

			sb.append("\"");

			sb.append(_escape(emailAddress));

			sb.append("\"");
		}

		if (emailAddressVerified != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"emailAddressVerified\": ");

			sb.append(emailAddressVerified);
		}

		if (facebookId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"facebookId\": ");

			sb.append("\"");

			sb.append(_escape(facebookId));

			sb.append("\"");
		}

		if (fullName != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"fullName\": ");

			sb.append("\"");

			sb.append(_escape(fullName));

			sb.append("\"");
		}

		if (gcpToken != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"gcpToken\": ");

			sb.append("\"");

			sb.append(_escape(gcpToken));

			sb.append("\"");
		}

		if (googleUserId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"googleUserId\": ");

			sb.append("\"");

			sb.append(_escape(googleUserId));

			sb.append("\"");
		}

		if (iosDeviceToken != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"iosDeviceToken\": ");

			sb.append("\"");

			sb.append(_escape(iosDeviceToken));

			sb.append("\"");
		}

		if (isVerified != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"isVerified\": ");

			sb.append(isVerified);
		}

		if (languageId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"languageId\": ");

			sb.append("\"");

			sb.append(_escape(languageId));

			sb.append("\"");
		}

		if (newPassword != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"newPassword\": ");

			sb.append("\"");

			sb.append(_escape(newPassword));

			sb.append("\"");
		}

		if (passcode != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"passcode\": ");

			sb.append("\"");

			sb.append(_escape(passcode));

			sb.append("\"");
		}

		if (password != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"password\": ");

			sb.append("\"");

			sb.append(_escape(password));

			sb.append("\"");
		}

		if (passwordResetToken != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"passwordResetToken\": ");

			sb.append("\"");

			sb.append(_escape(passwordResetToken));

			sb.append("\"");
		}

		if (phoneNumber != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"phoneNumber\": ");

			sb.append("\"");

			sb.append(_escape(phoneNumber));

			sb.append("\"");
		}

		if (phoneNumberVerified != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"phoneNumberVerified\": ");

			sb.append(phoneNumberVerified);
		}

		if (profileBannerImage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"profileBannerImage\": ");

			sb.append("\"");

			sb.append(_escape(profileBannerImage));

			sb.append("\"");
		}

		if (profileImage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"profileImage\": ");

			sb.append("\"");

			sb.append(_escape(profileImage));

			sb.append("\"");
		}

		if (requestStatus != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"requestStatus\": ");

			sb.append(String.valueOf(requestStatus));
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.AppUser", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}