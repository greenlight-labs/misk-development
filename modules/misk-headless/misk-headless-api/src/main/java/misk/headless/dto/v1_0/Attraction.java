package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("Attraction")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "Attraction")
public class Attraction implements Serializable {

	public static Attraction toDTO(String json) {
		return ObjectMapperUtil.readValue(Attraction.class, json);
	}

	@Schema
	public String getAmenitiesLabel() {
		return amenitiesLabel;
	}

	public void setAmenitiesLabel(String amenitiesLabel) {
		this.amenitiesLabel = amenitiesLabel;
	}

	@JsonIgnore
	public void setAmenitiesLabel(
		UnsafeSupplier<String, Exception> amenitiesLabelUnsafeSupplier) {

		try {
			amenitiesLabel = amenitiesLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String amenitiesLabel;

	@Schema
	@Valid
	public AmenitiesList[] getAmenitiesList() {
		return amenitiesList;
	}

	public void setAmenitiesList(AmenitiesList[] amenitiesList) {
		this.amenitiesList = amenitiesList;
	}

	@JsonIgnore
	public void setAmenitiesList(
		UnsafeSupplier<AmenitiesList[], Exception>
			amenitiesListUnsafeSupplier) {

		try {
			amenitiesList = amenitiesListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected AmenitiesList[] amenitiesList;

	@Schema
	public String getAttractionBanner() {
		return attractionBanner;
	}

	public void setAttractionBanner(String attractionBanner) {
		this.attractionBanner = attractionBanner;
	}

	@JsonIgnore
	public void setAttractionBanner(
		UnsafeSupplier<String, Exception> attractionBannerUnsafeSupplier) {

		try {
			attractionBanner = attractionBannerUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String attractionBanner;

	@Schema
	public String getAttractionBannerIcon() {
		return attractionBannerIcon;
	}

	public void setAttractionBannerIcon(String attractionBannerIcon) {
		this.attractionBannerIcon = attractionBannerIcon;
	}

	@JsonIgnore
	public void setAttractionBannerIcon(
		UnsafeSupplier<String, Exception> attractionBannerIconUnsafeSupplier) {

		try {
			attractionBannerIcon = attractionBannerIconUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String attractionBannerIcon;

	@Schema
	public String getAttractionButtonLabel() {
		return attractionButtonLabel;
	}

	public void setAttractionButtonLabel(String attractionButtonLabel) {
		this.attractionButtonLabel = attractionButtonLabel;
	}

	@JsonIgnore
	public void setAttractionButtonLabel(
		UnsafeSupplier<String, Exception> attractionButtonLabelUnsafeSupplier) {

		try {
			attractionButtonLabel = attractionButtonLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String attractionButtonLabel;

	@Schema
	public String getAttractionId() {
		return attractionId;
	}

	public void setAttractionId(String attractionId) {
		this.attractionId = attractionId;
	}

	@JsonIgnore
	public void setAttractionId(
		UnsafeSupplier<String, Exception> attractionIdUnsafeSupplier) {

		try {
			attractionId = attractionIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String attractionId;

	@Schema
	public String getAttractionTitle() {
		return attractionTitle;
	}

	public void setAttractionTitle(String attractionTitle) {
		this.attractionTitle = attractionTitle;
	}

	@JsonIgnore
	public void setAttractionTitle(
		UnsafeSupplier<String, Exception> attractionTitleUnsafeSupplier) {

		try {
			attractionTitle = attractionTitleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String attractionTitle;

	@Schema
	public String getBannerButtonColor() {
		return bannerButtonColor;
	}

	public void setBannerButtonColor(String bannerButtonColor) {
		this.bannerButtonColor = bannerButtonColor;
	}

	@JsonIgnore
	public void setBannerButtonColor(
		UnsafeSupplier<String, Exception> bannerButtonColorUnsafeSupplier) {

		try {
			bannerButtonColor = bannerButtonColorUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String bannerButtonColor;

	@Schema
	public String getBookSpaceLabel() {
		return bookSpaceLabel;
	}

	public void setBookSpaceLabel(String bookSpaceLabel) {
		this.bookSpaceLabel = bookSpaceLabel;
	}

	@JsonIgnore
	public void setBookSpaceLabel(
		UnsafeSupplier<String, Exception> bookSpaceLabelUnsafeSupplier) {

		try {
			bookSpaceLabel = bookSpaceLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String bookSpaceLabel;

	@Schema
	@Valid
	public BookSpaceList[] getBookSpaceList() {
		return bookSpaceList;
	}

	public void setBookSpaceList(BookSpaceList[] bookSpaceList) {
		this.bookSpaceList = bookSpaceList;
	}

	@JsonIgnore
	public void setBookSpaceList(
		UnsafeSupplier<BookSpaceList[], Exception>
			bookSpaceListUnsafeSupplier) {

		try {
			bookSpaceList = bookSpaceListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected BookSpaceList[] bookSpaceList;

	@Schema
	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	@JsonIgnore
	public void setContactEmailAddress(
		UnsafeSupplier<String, Exception> contactEmailAddressUnsafeSupplier) {

		try {
			contactEmailAddress = contactEmailAddressUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String contactEmailAddress;

	@Schema
	public String getContactEmailAddressIcon() {
		return contactEmailAddressIcon;
	}

	public void setContactEmailAddressIcon(String contactEmailAddressIcon) {
		this.contactEmailAddressIcon = contactEmailAddressIcon;
	}

	@JsonIgnore
	public void setContactEmailAddressIcon(
		UnsafeSupplier<String, Exception>
			contactEmailAddressIconUnsafeSupplier) {

		try {
			contactEmailAddressIcon =
				contactEmailAddressIconUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String contactEmailAddressIcon;

	@Schema
	public String getContactLabel() {
		return contactLabel;
	}

	public void setContactLabel(String contactLabel) {
		this.contactLabel = contactLabel;
	}

	@JsonIgnore
	public void setContactLabel(
		UnsafeSupplier<String, Exception> contactLabelUnsafeSupplier) {

		try {
			contactLabel = contactLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String contactLabel;

	@Schema
	public String getContactTelephone() {
		return contactTelephone;
	}

	public void setContactTelephone(String contactTelephone) {
		this.contactTelephone = contactTelephone;
	}

	@JsonIgnore
	public void setContactTelephone(
		UnsafeSupplier<String, Exception> contactTelephoneUnsafeSupplier) {

		try {
			contactTelephone = contactTelephoneUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String contactTelephone;

	@Schema
	public String getContactTelephoneIcon() {
		return contactTelephoneIcon;
	}

	public void setContactTelephoneIcon(String contactTelephoneIcon) {
		this.contactTelephoneIcon = contactTelephoneIcon;
	}

	@JsonIgnore
	public void setContactTelephoneIcon(
		UnsafeSupplier<String, Exception> contactTelephoneIconUnsafeSupplier) {

		try {
			contactTelephoneIcon = contactTelephoneIconUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String contactTelephoneIcon;

	@Schema
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonIgnore
	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String description;

	@Schema
	@Valid
	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	@JsonIgnore
	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected EventsSection eventsSection;

	@Schema
	public String[] getGalleryImagesList() {
		return galleryImagesList;
	}

	public void setGalleryImagesList(String[] galleryImagesList) {
		this.galleryImagesList = galleryImagesList;
	}

	@JsonIgnore
	public void setGalleryImagesList(
		UnsafeSupplier<String[], Exception> galleryImagesListUnsafeSupplier) {

		try {
			galleryImagesList = galleryImagesListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String[] galleryImagesList;

	@Schema
	public String getGalleryLabel() {
		return galleryLabel;
	}

	public void setGalleryLabel(String galleryLabel) {
		this.galleryLabel = galleryLabel;
	}

	@JsonIgnore
	public void setGalleryLabel(
		UnsafeSupplier<String, Exception> galleryLabelUnsafeSupplier) {

		try {
			galleryLabel = galleryLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String galleryLabel;

	@Schema
	public String getLocationLabel() {
		return locationLabel;
	}

	public void setLocationLabel(String locationLabel) {
		this.locationLabel = locationLabel;
	}

	@JsonIgnore
	public void setLocationLabel(
		UnsafeSupplier<String, Exception> locationLabelUnsafeSupplier) {

		try {
			locationLabel = locationLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String locationLabel;

	@Schema
	public String getLocationLatitude() {
		return locationLatitude;
	}

	public void setLocationLatitude(String locationLatitude) {
		this.locationLatitude = locationLatitude;
	}

	@JsonIgnore
	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> locationLatitudeUnsafeSupplier) {

		try {
			locationLatitude = locationLatitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String locationLatitude;

	@Schema
	public String getLocationLongitude() {
		return locationLongitude;
	}

	public void setLocationLongitude(String locationLongitude) {
		this.locationLongitude = locationLongitude;
	}

	@JsonIgnore
	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> locationLongitudeUnsafeSupplier) {

		try {
			locationLongitude = locationLongitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String locationLongitude;

	@Schema
	public String getWorkingDays() {
		return workingDays;
	}

	public void setWorkingDays(String workingDays) {
		this.workingDays = workingDays;
	}

	@JsonIgnore
	public void setWorkingDays(
		UnsafeSupplier<String, Exception> workingDaysUnsafeSupplier) {

		try {
			workingDays = workingDaysUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String workingDays;

	@Schema
	public String getWorkingDaysImage() {
		return workingDaysImage;
	}

	public void setWorkingDaysImage(String workingDaysImage) {
		this.workingDaysImage = workingDaysImage;
	}

	@JsonIgnore
	public void setWorkingDaysImage(
		UnsafeSupplier<String, Exception> workingDaysImageUnsafeSupplier) {

		try {
			workingDaysImage = workingDaysImageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String workingDaysImage;

	@Schema
	public String getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}

	@JsonIgnore
	public void setWorkingHours(
		UnsafeSupplier<String, Exception> workingHoursUnsafeSupplier) {

		try {
			workingHours = workingHoursUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String workingHours;

	@Schema
	public String getWorkingHoursImage() {
		return workingHoursImage;
	}

	public void setWorkingHoursImage(String workingHoursImage) {
		this.workingHoursImage = workingHoursImage;
	}

	@JsonIgnore
	public void setWorkingHoursImage(
		UnsafeSupplier<String, Exception> workingHoursImageUnsafeSupplier) {

		try {
			workingHoursImage = workingHoursImageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String workingHoursImage;

	@Schema
	public String getWorkingHoursLabel() {
		return workingHoursLabel;
	}

	public void setWorkingHoursLabel(String workingHoursLabel) {
		this.workingHoursLabel = workingHoursLabel;
	}

	@JsonIgnore
	public void setWorkingHoursLabel(
		UnsafeSupplier<String, Exception> workingHoursLabelUnsafeSupplier) {

		try {
			workingHoursLabel = workingHoursLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String workingHoursLabel;

	@Schema
	public String getWorkingHoursLabelColor() {
		return workingHoursLabelColor;
	}

	public void setWorkingHoursLabelColor(String workingHoursLabelColor) {
		this.workingHoursLabelColor = workingHoursLabelColor;
	}

	@JsonIgnore
	public void setWorkingHoursLabelColor(
		UnsafeSupplier<String, Exception>
			workingHoursLabelColorUnsafeSupplier) {

		try {
			workingHoursLabelColor = workingHoursLabelColorUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String workingHoursLabelColor;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Attraction)) {
			return false;
		}

		Attraction attraction = (Attraction)object;

		return Objects.equals(toString(), attraction.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (amenitiesLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"amenitiesLabel\": ");

			sb.append("\"");

			sb.append(_escape(amenitiesLabel));

			sb.append("\"");
		}

		if (amenitiesList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"amenitiesList\": ");

			sb.append("[");

			for (int i = 0; i < amenitiesList.length; i++) {
				sb.append(String.valueOf(amenitiesList[i]));

				if ((i + 1) < amenitiesList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (attractionBanner != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionBanner\": ");

			sb.append("\"");

			sb.append(_escape(attractionBanner));

			sb.append("\"");
		}

		if (attractionBannerIcon != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionBannerIcon\": ");

			sb.append("\"");

			sb.append(_escape(attractionBannerIcon));

			sb.append("\"");
		}

		if (attractionButtonLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionButtonLabel\": ");

			sb.append("\"");

			sb.append(_escape(attractionButtonLabel));

			sb.append("\"");
		}

		if (attractionId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionId\": ");

			sb.append("\"");

			sb.append(_escape(attractionId));

			sb.append("\"");
		}

		if (attractionTitle != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionTitle\": ");

			sb.append("\"");

			sb.append(_escape(attractionTitle));

			sb.append("\"");
		}

		if (bannerButtonColor != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bannerButtonColor\": ");

			sb.append("\"");

			sb.append(_escape(bannerButtonColor));

			sb.append("\"");
		}

		if (bookSpaceLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookSpaceLabel\": ");

			sb.append("\"");

			sb.append(_escape(bookSpaceLabel));

			sb.append("\"");
		}

		if (bookSpaceList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookSpaceList\": ");

			sb.append("[");

			for (int i = 0; i < bookSpaceList.length; i++) {
				sb.append(String.valueOf(bookSpaceList[i]));

				if ((i + 1) < bookSpaceList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (contactEmailAddress != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactEmailAddress\": ");

			sb.append("\"");

			sb.append(_escape(contactEmailAddress));

			sb.append("\"");
		}

		if (contactEmailAddressIcon != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactEmailAddressIcon\": ");

			sb.append("\"");

			sb.append(_escape(contactEmailAddressIcon));

			sb.append("\"");
		}

		if (contactLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactLabel\": ");

			sb.append("\"");

			sb.append(_escape(contactLabel));

			sb.append("\"");
		}

		if (contactTelephone != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactTelephone\": ");

			sb.append("\"");

			sb.append(_escape(contactTelephone));

			sb.append("\"");
		}

		if (contactTelephoneIcon != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactTelephoneIcon\": ");

			sb.append("\"");

			sb.append(_escape(contactTelephoneIcon));

			sb.append("\"");
		}

		if (description != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(description));

			sb.append("\"");
		}

		if (eventsSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(eventsSection));
		}

		if (galleryImagesList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryImagesList\": ");

			sb.append("[");

			for (int i = 0; i < galleryImagesList.length; i++) {
				sb.append("\"");

				sb.append(_escape(galleryImagesList[i]));

				sb.append("\"");

				if ((i + 1) < galleryImagesList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (galleryLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryLabel\": ");

			sb.append("\"");

			sb.append(_escape(galleryLabel));

			sb.append("\"");
		}

		if (locationLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLabel\": ");

			sb.append("\"");

			sb.append(_escape(locationLabel));

			sb.append("\"");
		}

		if (locationLatitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(locationLatitude));

			sb.append("\"");
		}

		if (locationLongitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(locationLongitude));

			sb.append("\"");
		}

		if (workingDays != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingDays\": ");

			sb.append("\"");

			sb.append(_escape(workingDays));

			sb.append("\"");
		}

		if (workingDaysImage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingDaysImage\": ");

			sb.append("\"");

			sb.append(_escape(workingDaysImage));

			sb.append("\"");
		}

		if (workingHours != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHours\": ");

			sb.append("\"");

			sb.append(_escape(workingHours));

			sb.append("\"");
		}

		if (workingHoursImage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHoursImage\": ");

			sb.append("\"");

			sb.append(_escape(workingHoursImage));

			sb.append("\"");
		}

		if (workingHoursLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHoursLabel\": ");

			sb.append("\"");

			sb.append(_escape(workingHoursLabel));

			sb.append("\"");
		}

		if (workingHoursLabelColor != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHoursLabelColor\": ");

			sb.append("\"");

			sb.append(_escape(workingHoursLabelColor));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.Attraction",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}