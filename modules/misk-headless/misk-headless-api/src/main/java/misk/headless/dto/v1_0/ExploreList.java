package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("ExploreList")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "ExploreList")
public class ExploreList implements Serializable {

	public static ExploreList toDTO(String json) {
		return ObjectMapperUtil.readValue(ExploreList.class, json);
	}

	@Schema
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@JsonIgnore
	public void setArea(UnsafeSupplier<String, Exception> areaUnsafeSupplier) {
		try {
			area = areaUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String area;

	@Schema
	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	@JsonIgnore
	public void setCapacity(
		UnsafeSupplier<String, Exception> capacityUnsafeSupplier) {

		try {
			capacity = capacityUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String capacity;

	@Schema
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@JsonIgnore
	public void setDuration(
		UnsafeSupplier<String, Exception> durationUnsafeSupplier) {

		try {
			duration = durationUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String duration;

	@Schema
	public String getExploreIcon() {
		return exploreIcon;
	}

	public void setExploreIcon(String exploreIcon) {
		this.exploreIcon = exploreIcon;
	}

	@JsonIgnore
	public void setExploreIcon(
		UnsafeSupplier<String, Exception> exploreIconUnsafeSupplier) {

		try {
			exploreIcon = exploreIconUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String exploreIcon;

	@Schema
	public String getExploreImages() {
		return exploreImages;
	}

	public void setExploreImages(String exploreImages) {
		this.exploreImages = exploreImages;
	}

	@JsonIgnore
	public void setExploreImages(
		UnsafeSupplier<String, Exception> exploreImagesUnsafeSupplier) {

		try {
			exploreImages = exploreImagesUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String exploreImages;

	@Schema
	public String getExploreTitle() {
		return exploreTitle;
	}

	public void setExploreTitle(String exploreTitle) {
		this.exploreTitle = exploreTitle;
	}

	@JsonIgnore
	public void setExploreTitle(
		UnsafeSupplier<String, Exception> exploreTitleUnsafeSupplier) {

		try {
			exploreTitle = exploreTitleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String exploreTitle;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExploreList)) {
			return false;
		}

		ExploreList exploreList = (ExploreList)object;

		return Objects.equals(toString(), exploreList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (area != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"area\": ");

			sb.append("\"");

			sb.append(_escape(area));

			sb.append("\"");
		}

		if (capacity != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"capacity\": ");

			sb.append("\"");

			sb.append(_escape(capacity));

			sb.append("\"");
		}

		if (duration != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"duration\": ");

			sb.append("\"");

			sb.append(_escape(duration));

			sb.append("\"");
		}

		if (exploreIcon != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreIcon\": ");

			sb.append("\"");

			sb.append(_escape(exploreIcon));

			sb.append("\"");
		}

		if (exploreImages != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreImages\": ");

			sb.append("\"");

			sb.append(_escape(exploreImages));

			sb.append("\"");
		}

		if (exploreTitle != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreTitle\": ");

			sb.append("\"");

			sb.append(_escape(exploreTitle));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.ExploreList",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}