package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("DeleteAccountPage")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "DeleteAccountPage")
public class DeleteAccountPage implements Serializable {

	public static DeleteAccountPage toDTO(String json) {
		return ObjectMapperUtil.readValue(DeleteAccountPage.class, json);
	}

	@Schema
	public String[] getAnswers() {
		return answers;
	}

	public void setAnswers(String[] answers) {
		this.answers = answers;
	}

	@JsonIgnore
	public void setAnswers(
		UnsafeSupplier<String[], Exception> answersUnsafeSupplier) {

		try {
			answers = answersUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String[] answers;

	@Schema
	public String getDeleteAccountLabel() {
		return deleteAccountLabel;
	}

	public void setDeleteAccountLabel(String deleteAccountLabel) {
		this.deleteAccountLabel = deleteAccountLabel;
	}

	@JsonIgnore
	public void setDeleteAccountLabel(
		UnsafeSupplier<String, Exception> deleteAccountLabelUnsafeSupplier) {

		try {
			deleteAccountLabel = deleteAccountLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String deleteAccountLabel;

	@Schema
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonIgnore
	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String description;

	@Schema
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@JsonIgnore
	public void setQuestion(
		UnsafeSupplier<String, Exception> questionUnsafeSupplier) {

		try {
			question = questionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String question;

	@Schema
	public String getQuestionExcerpt() {
		return questionExcerpt;
	}

	public void setQuestionExcerpt(String questionExcerpt) {
		this.questionExcerpt = questionExcerpt;
	}

	@JsonIgnore
	public void setQuestionExcerpt(
		UnsafeSupplier<String, Exception> questionExcerptUnsafeSupplier) {

		try {
			questionExcerpt = questionExcerptUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String questionExcerpt;

	@Schema
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@JsonIgnore
	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String title;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DeleteAccountPage)) {
			return false;
		}

		DeleteAccountPage deleteAccountPage = (DeleteAccountPage)object;

		return Objects.equals(toString(), deleteAccountPage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (answers != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"answers\": ");

			sb.append("[");

			for (int i = 0; i < answers.length; i++) {
				sb.append("\"");

				sb.append(_escape(answers[i]));

				sb.append("\"");

				if ((i + 1) < answers.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (deleteAccountLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deleteAccountLabel\": ");

			sb.append("\"");

			sb.append(_escape(deleteAccountLabel));

			sb.append("\"");
		}

		if (description != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(description));

			sb.append("\"");
		}

		if (question != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"question\": ");

			sb.append("\"");

			sb.append(_escape(question));

			sb.append("\"");
		}

		if (questionExcerpt != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"questionExcerpt\": ");

			sb.append("\"");

			sb.append(_escape(questionExcerpt));

			sb.append("\"");
		}

		if (title != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(title));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.DeleteAccountPage",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}