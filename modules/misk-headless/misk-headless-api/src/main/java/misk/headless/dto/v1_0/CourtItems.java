package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("CourtItems")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "CourtItems")
public class CourtItems implements Serializable {

	public static CourtItems toDTO(String json) {
		return ObjectMapperUtil.readValue(CourtItems.class, json);
	}

	@Schema
	@Valid
	public CourtCategory[] getCourtCategories() {
		return courtCategories;
	}

	public void setCourtCategories(CourtCategory[] courtCategories) {
		this.courtCategories = courtCategories;
	}

	@JsonIgnore
	public void setCourtCategories(
		UnsafeSupplier<CourtCategory[], Exception>
			courtCategoriesUnsafeSupplier) {

		try {
			courtCategories = courtCategoriesUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected CourtCategory[] courtCategories;

	@Schema
	@Valid
	public CourtLocation[] getCourtLocations() {
		return courtLocations;
	}

	public void setCourtLocations(CourtLocation[] courtLocations) {
		this.courtLocations = courtLocations;
	}

	@JsonIgnore
	public void setCourtLocations(
		UnsafeSupplier<CourtLocation[], Exception>
			courtLocationsUnsafeSupplier) {

		try {
			courtLocations = courtLocationsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected CourtLocation[] courtLocations;

	@Schema
	@Valid
	public Court[] getCourts() {
		return courts;
	}

	public void setCourts(Court[] courts) {
		this.courts = courts;
	}

	@JsonIgnore
	public void setCourts(
		UnsafeSupplier<Court[], Exception> courtsUnsafeSupplier) {

		try {
			courts = courtsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Court[] courts;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CourtItems)) {
			return false;
		}

		CourtItems courtItems = (CourtItems)object;

		return Objects.equals(toString(), courtItems.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (courtCategories != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtCategories\": ");

			sb.append("[");

			for (int i = 0; i < courtCategories.length; i++) {
				sb.append(String.valueOf(courtCategories[i]));

				if ((i + 1) < courtCategories.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (courtLocations != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtLocations\": ");

			sb.append("[");

			for (int i = 0; i < courtLocations.length; i++) {
				sb.append(String.valueOf(courtLocations[i]));

				if ((i + 1) < courtLocations.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (courts != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courts\": ");

			sb.append("[");

			for (int i = 0; i < courts.length; i++) {
				sb.append(String.valueOf(courts[i]));

				if ((i + 1) < courts.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.CourtItems",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}