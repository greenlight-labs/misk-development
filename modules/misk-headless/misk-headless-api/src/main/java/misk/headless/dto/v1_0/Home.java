package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("Home")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "Home")
public class Home implements Serializable {

	public static Home toDTO(String json) {
		return ObjectMapperUtil.readValue(Home.class, json);
	}

	@Schema
	@Valid
	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	@JsonIgnore
	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected EventsSection eventsSection;

	@Schema(
		description = "List of top 3 flip screens (Misk City, Experiencial Center & Wadi C)"
	)
	@Valid
	public FlipCard[] getFlipCards() {
		return flipCards;
	}

	public void setFlipCards(FlipCard[] flipCards) {
		this.flipCards = flipCards;
	}

	@JsonIgnore
	public void setFlipCards(
		UnsafeSupplier<FlipCard[], Exception> flipCardsUnsafeSupplier) {

		try {
			flipCards = flipCardsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(
		description = "List of top 3 flip screens (Misk City, Experiencial Center & Wadi C)"
	)
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected FlipCard[] flipCards;

	@Schema
	@Valid
	public HighlightsSection getHighlightsSection() {
		return highlightsSection;
	}

	public void setHighlightsSection(HighlightsSection highlightsSection) {
		this.highlightsSection = highlightsSection;
	}

	@JsonIgnore
	public void setHighlightsSection(
		UnsafeSupplier<HighlightsSection, Exception>
			highlightsSectionUnsafeSupplier) {

		try {
			highlightsSection = highlightsSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected HighlightsSection highlightsSection;

	@Schema
	@Valid
	public StoriesSection getStoriesSection() {
		return storiesSection;
	}

	public void setStoriesSection(StoriesSection storiesSection) {
		this.storiesSection = storiesSection;
	}

	@JsonIgnore
	public void setStoriesSection(
		UnsafeSupplier<StoriesSection, Exception>
			storiesSectionUnsafeSupplier) {

		try {
			storiesSection = storiesSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected StoriesSection storiesSection;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Home)) {
			return false;
		}

		Home home = (Home)object;

		return Objects.equals(toString(), home.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (eventsSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(eventsSection));
		}

		if (flipCards != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"flipCards\": ");

			sb.append("[");

			for (int i = 0; i < flipCards.length; i++) {
				sb.append(String.valueOf(flipCards[i]));

				if ((i + 1) < flipCards.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (highlightsSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlightsSection\": ");

			sb.append(String.valueOf(highlightsSection));
		}

		if (storiesSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"storiesSection\": ");

			sb.append(String.valueOf(storiesSection));
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.Home", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}