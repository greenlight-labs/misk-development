package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("FaclitiesList")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "FaclitiesList")
public class FaclitiesList implements Serializable {

	public static FaclitiesList toDTO(String json) {
		return ObjectMapperUtil.readValue(FaclitiesList.class, json);
	}

	@Schema
	public String getFaclitiesImages() {
		return faclitiesImages;
	}

	public void setFaclitiesImages(String faclitiesImages) {
		this.faclitiesImages = faclitiesImages;
	}

	@JsonIgnore
	public void setFaclitiesImages(
		UnsafeSupplier<String, Exception> faclitiesImagesUnsafeSupplier) {

		try {
			faclitiesImages = faclitiesImagesUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String faclitiesImages;

	@Schema
	public String getFaclitiesImagesLabel() {
		return faclitiesImagesLabel;
	}

	public void setFaclitiesImagesLabel(String faclitiesImagesLabel) {
		this.faclitiesImagesLabel = faclitiesImagesLabel;
	}

	@JsonIgnore
	public void setFaclitiesImagesLabel(
		UnsafeSupplier<String, Exception> faclitiesImagesLabelUnsafeSupplier) {

		try {
			faclitiesImagesLabel = faclitiesImagesLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String faclitiesImagesLabel;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FaclitiesList)) {
			return false;
		}

		FaclitiesList faclitiesList = (FaclitiesList)object;

		return Objects.equals(toString(), faclitiesList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (faclitiesImages != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesImages\": ");

			sb.append("\"");

			sb.append(_escape(faclitiesImages));

			sb.append("\"");
		}

		if (faclitiesImagesLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesImagesLabel\": ");

			sb.append("\"");

			sb.append(_escape(faclitiesImagesLabel));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.FaclitiesList",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}