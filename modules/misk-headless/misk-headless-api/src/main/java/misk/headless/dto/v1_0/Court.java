package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("Court")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "Court")
public class Court implements Serializable {

	public static Court toDTO(String json) {
		return ObjectMapperUtil.readValue(Court.class, json);
	}

	@Schema
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@JsonIgnore
	public void setCategoryId(
		UnsafeSupplier<Long, Exception> categoryIdUnsafeSupplier) {

		try {
			categoryId = categoryIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long categoryId;

	@Schema
	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	@JsonIgnore
	public void setContactEmail(
		UnsafeSupplier<String, Exception> contactEmailUnsafeSupplier) {

		try {
			contactEmail = contactEmailUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String contactEmail;

	@Schema
	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	@JsonIgnore
	public void setContactPhone(
		UnsafeSupplier<String, Exception> contactPhoneUnsafeSupplier) {

		try {
			contactPhone = contactPhoneUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String contactPhone;

	@Schema
	@Valid
	public CourtBooking getCourtBooking() {
		return courtBooking;
	}

	public void setCourtBooking(CourtBooking courtBooking) {
		this.courtBooking = courtBooking;
	}

	@JsonIgnore
	public void setCourtBooking(
		UnsafeSupplier<CourtBooking, Exception> courtBookingUnsafeSupplier) {

		try {
			courtBooking = courtBookingUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected CourtBooking courtBooking;

	@Schema
	@Valid
	public CourtCategory getCourtCategory() {
		return courtCategory;
	}

	public void setCourtCategory(CourtCategory courtCategory) {
		this.courtCategory = courtCategory;
	}

	@JsonIgnore
	public void setCourtCategory(
		UnsafeSupplier<CourtCategory, Exception> courtCategoryUnsafeSupplier) {

		try {
			courtCategory = courtCategoryUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected CourtCategory courtCategory;

	@Schema
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonIgnore
	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String description;

	@Schema
	public String[] getDetailImages() {
		return detailImages;
	}

	public void setDetailImages(String[] detailImages) {
		this.detailImages = detailImages;
	}

	@JsonIgnore
	public void setDetailImages(
		UnsafeSupplier<String[], Exception> detailImagesUnsafeSupplier) {

		try {
			detailImages = detailImagesUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String[] detailImages;

	@Schema
	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	@JsonIgnore
	public void setHeading(
		UnsafeSupplier<String, Exception> headingUnsafeSupplier) {

		try {
			heading = headingUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String heading;

	@Schema
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	public void setId(UnsafeSupplier<Long, Exception> idUnsafeSupplier) {
		try {
			id = idUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long id;

	@Schema
	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	@JsonIgnore
	public void setLat(UnsafeSupplier<String, Exception> latUnsafeSupplier) {
		try {
			lat = latUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String lat;

	@Schema
	public String getListingImage() {
		return listingImage;
	}

	public void setListingImage(String listingImage) {
		this.listingImage = listingImage;
	}

	@JsonIgnore
	public void setListingImage(
		UnsafeSupplier<String, Exception> listingImageUnsafeSupplier) {

		try {
			listingImage = listingImageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String listingImage;

	@Schema
	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	@JsonIgnore
	public void setLon(UnsafeSupplier<String, Exception> lonUnsafeSupplier) {
		try {
			lon = lonUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String lon;

	@Schema
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public void setName(UnsafeSupplier<String, Exception> nameUnsafeSupplier) {
		try {
			name = nameUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String name;

	@Schema
	public String getOpeningDays() {
		return openingDays;
	}

	public void setOpeningDays(String openingDays) {
		this.openingDays = openingDays;
	}

	@JsonIgnore
	public void setOpeningDays(
		UnsafeSupplier<String, Exception> openingDaysUnsafeSupplier) {

		try {
			openingDays = openingDaysUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String openingDays;

	@Schema
	public String getOpeningHours() {
		return openingHours;
	}

	public void setOpeningHours(String openingHours) {
		this.openingHours = openingHours;
	}

	@JsonIgnore
	public void setOpeningHours(
		UnsafeSupplier<String, Exception> openingHoursUnsafeSupplier) {

		try {
			openingHours = openingHoursUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String openingHours;

	@Schema
	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	@JsonIgnore
	public void setVenue(
		UnsafeSupplier<String, Exception> venueUnsafeSupplier) {

		try {
			venue = venueUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String venue;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Court)) {
			return false;
		}

		Court court = (Court)object;

		return Objects.equals(toString(), court.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (categoryId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"categoryId\": ");

			sb.append(categoryId);
		}

		if (contactEmail != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactEmail\": ");

			sb.append("\"");

			sb.append(_escape(contactEmail));

			sb.append("\"");
		}

		if (contactPhone != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactPhone\": ");

			sb.append("\"");

			sb.append(_escape(contactPhone));

			sb.append("\"");
		}

		if (courtBooking != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtBooking\": ");

			sb.append(String.valueOf(courtBooking));
		}

		if (courtCategory != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtCategory\": ");

			sb.append(String.valueOf(courtCategory));
		}

		if (description != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(description));

			sb.append("\"");
		}

		if (detailImages != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"detailImages\": ");

			sb.append("[");

			for (int i = 0; i < detailImages.length; i++) {
				sb.append("\"");

				sb.append(_escape(detailImages[i]));

				sb.append("\"");

				if ((i + 1) < detailImages.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (heading != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"heading\": ");

			sb.append("\"");

			sb.append(_escape(heading));

			sb.append("\"");
		}

		if (id != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append(id);
		}

		if (lat != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lat\": ");

			sb.append("\"");

			sb.append(_escape(lat));

			sb.append("\"");
		}

		if (listingImage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"listingImage\": ");

			sb.append("\"");

			sb.append(_escape(listingImage));

			sb.append("\"");
		}

		if (lon != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lon\": ");

			sb.append("\"");

			sb.append(_escape(lon));

			sb.append("\"");
		}

		if (name != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(name));

			sb.append("\"");
		}

		if (openingDays != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"openingDays\": ");

			sb.append("\"");

			sb.append(_escape(openingDays));

			sb.append("\"");
		}

		if (openingHours != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"openingHours\": ");

			sb.append("\"");

			sb.append(_escape(openingHours));

			sb.append("\"");
		}

		if (venue != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"venue\": ");

			sb.append("\"");

			sb.append(_escape(venue));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.Court", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}