package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("Contact")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "Contact")
public class Contact implements Serializable {

	public static Contact toDTO(String json) {
		return ObjectMapperUtil.readValue(Contact.class, json);
	}

	@Schema
	public String getAddress() {
		return Address;
	}

	public void setAddress(String Address) {
		this.Address = Address;
	}

	@JsonIgnore
	public void setAddress(
		UnsafeSupplier<String, Exception> AddressUnsafeSupplier) {

		try {
			Address = AddressUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String Address;

	@Schema
	public String getCallUsLabel() {
		return CallUsLabel;
	}

	public void setCallUsLabel(String CallUsLabel) {
		this.CallUsLabel = CallUsLabel;
	}

	@JsonIgnore
	public void setCallUsLabel(
		UnsafeSupplier<String, Exception> CallUsLabelUnsafeSupplier) {

		try {
			CallUsLabel = CallUsLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String CallUsLabel;

	@Schema
	public String getCallUsNumber() {
		return CallUsNumber;
	}

	public void setCallUsNumber(String CallUsNumber) {
		this.CallUsNumber = CallUsNumber;
	}

	@JsonIgnore
	public void setCallUsNumber(
		UnsafeSupplier<String, Exception> CallUsNumberUnsafeSupplier) {

		try {
			CallUsNumber = CallUsNumberUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String CallUsNumber;

	@Schema
	public String getContactLabel() {
		return ContactLabel;
	}

	public void setContactLabel(String ContactLabel) {
		this.ContactLabel = ContactLabel;
	}

	@JsonIgnore
	public void setContactLabel(
		UnsafeSupplier<String, Exception> ContactLabelUnsafeSupplier) {

		try {
			ContactLabel = ContactLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String ContactLabel;

	@Schema
	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	@JsonIgnore
	public void setDescription(
		UnsafeSupplier<String, Exception> DescriptionUnsafeSupplier) {

		try {
			Description = DescriptionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String Description;

	@Schema
	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String EmailAddress) {
		this.EmailAddress = EmailAddress;
	}

	@JsonIgnore
	public void setEmailAddress(
		UnsafeSupplier<String, Exception> EmailAddressUnsafeSupplier) {

		try {
			EmailAddress = EmailAddressUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String EmailAddress;

	@Schema
	public String getEmailLabel() {
		return EmailLabel;
	}

	public void setEmailLabel(String EmailLabel) {
		this.EmailLabel = EmailLabel;
	}

	@JsonIgnore
	public void setEmailLabel(
		UnsafeSupplier<String, Exception> EmailLabelUnsafeSupplier) {

		try {
			EmailLabel = EmailLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String EmailLabel;

	@Schema
	public String getLocationLabel() {
		return LocationLabel;
	}

	public void setLocationLabel(String LocationLabel) {
		this.LocationLabel = LocationLabel;
	}

	@JsonIgnore
	public void setLocationLabel(
		UnsafeSupplier<String, Exception> LocationLabelUnsafeSupplier) {

		try {
			LocationLabel = LocationLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String LocationLabel;

	@Schema
	public String getLocationLatitude() {
		return LocationLatitude;
	}

	public void setLocationLatitude(String LocationLatitude) {
		this.LocationLatitude = LocationLatitude;
	}

	@JsonIgnore
	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> LocationLatitudeUnsafeSupplier) {

		try {
			LocationLatitude = LocationLatitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String LocationLatitude;

	@Schema
	public String getLocationLongitude() {
		return LocationLongitude;
	}

	public void setLocationLongitude(String LocationLongitude) {
		this.LocationLongitude = LocationLongitude;
	}

	@JsonIgnore
	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> LocationLongitudeUnsafeSupplier) {

		try {
			LocationLongitude = LocationLongitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String LocationLongitude;

	@Schema
	public String getSocialMediaLabel() {
		return SocialMediaLabel;
	}

	public void setSocialMediaLabel(String SocialMediaLabel) {
		this.SocialMediaLabel = SocialMediaLabel;
	}

	@JsonIgnore
	public void setSocialMediaLabel(
		UnsafeSupplier<String, Exception> SocialMediaLabelUnsafeSupplier) {

		try {
			SocialMediaLabel = SocialMediaLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String SocialMediaLabel;

	@Schema
	@Valid
	public SocialMediaList[] getSocialMediaList() {
		return SocialMediaList;
	}

	public void setSocialMediaList(SocialMediaList[] SocialMediaList) {
		this.SocialMediaList = SocialMediaList;
	}

	@JsonIgnore
	public void setSocialMediaList(
		UnsafeSupplier<SocialMediaList[], Exception>
			SocialMediaListUnsafeSupplier) {

		try {
			SocialMediaList = SocialMediaListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected SocialMediaList[] SocialMediaList;

	@Schema
	public String getWebsiteAddress() {
		return WebsiteAddress;
	}

	public void setWebsiteAddress(String WebsiteAddress) {
		this.WebsiteAddress = WebsiteAddress;
	}

	@JsonIgnore
	public void setWebsiteAddress(
		UnsafeSupplier<String, Exception> WebsiteAddressUnsafeSupplier) {

		try {
			WebsiteAddress = WebsiteAddressUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WebsiteAddress;

	@Schema
	public String getWebsiteLabel() {
		return WebsiteLabel;
	}

	public void setWebsiteLabel(String WebsiteLabel) {
		this.WebsiteLabel = WebsiteLabel;
	}

	@JsonIgnore
	public void setWebsiteLabel(
		UnsafeSupplier<String, Exception> WebsiteLabelUnsafeSupplier) {

		try {
			WebsiteLabel = WebsiteLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WebsiteLabel;

	@Schema
	public String getWhatsAppLabel() {
		return WhatsAppLabel;
	}

	public void setWhatsAppLabel(String WhatsAppLabel) {
		this.WhatsAppLabel = WhatsAppLabel;
	}

	@JsonIgnore
	public void setWhatsAppLabel(
		UnsafeSupplier<String, Exception> WhatsAppLabelUnsafeSupplier) {

		try {
			WhatsAppLabel = WhatsAppLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WhatsAppLabel;

	@Schema
	public String getWhatsAppNumber() {
		return WhatsAppNumber;
	}

	public void setWhatsAppNumber(String WhatsAppNumber) {
		this.WhatsAppNumber = WhatsAppNumber;
	}

	@JsonIgnore
	public void setWhatsAppNumber(
		UnsafeSupplier<String, Exception> WhatsAppNumberUnsafeSupplier) {

		try {
			WhatsAppNumber = WhatsAppNumberUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WhatsAppNumber;

	@Schema
	public String getWorkingHours() {
		return WorkingHours;
	}

	public void setWorkingHours(String WorkingHours) {
		this.WorkingHours = WorkingHours;
	}

	@JsonIgnore
	public void setWorkingHours(
		UnsafeSupplier<String, Exception> WorkingHoursUnsafeSupplier) {

		try {
			WorkingHours = WorkingHoursUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WorkingHours;

	@Schema
	public String getWorkingHoursLabel() {
		return WorkingHoursLabel;
	}

	public void setWorkingHoursLabel(String WorkingHoursLabel) {
		this.WorkingHoursLabel = WorkingHoursLabel;
	}

	@JsonIgnore
	public void setWorkingHoursLabel(
		UnsafeSupplier<String, Exception> WorkingHoursLabelUnsafeSupplier) {

		try {
			WorkingHoursLabel = WorkingHoursLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WorkingHoursLabel;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Contact)) {
			return false;
		}

		Contact contact = (Contact)object;

		return Objects.equals(toString(), contact.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (Address != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Address\": ");

			sb.append("\"");

			sb.append(_escape(Address));

			sb.append("\"");
		}

		if (CallUsLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"CallUsLabel\": ");

			sb.append("\"");

			sb.append(_escape(CallUsLabel));

			sb.append("\"");
		}

		if (CallUsNumber != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"CallUsNumber\": ");

			sb.append("\"");

			sb.append(_escape(CallUsNumber));

			sb.append("\"");
		}

		if (ContactLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactLabel\": ");

			sb.append("\"");

			sb.append(_escape(ContactLabel));

			sb.append("\"");
		}

		if (Description != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Description\": ");

			sb.append("\"");

			sb.append(_escape(Description));

			sb.append("\"");
		}

		if (EmailAddress != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EmailAddress\": ");

			sb.append("\"");

			sb.append(_escape(EmailAddress));

			sb.append("\"");
		}

		if (EmailLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EmailLabel\": ");

			sb.append("\"");

			sb.append(_escape(EmailLabel));

			sb.append("\"");
		}

		if (LocationLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLabel\": ");

			sb.append("\"");

			sb.append(_escape(LocationLabel));

			sb.append("\"");
		}

		if (LocationLatitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(LocationLatitude));

			sb.append("\"");
		}

		if (LocationLongitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(LocationLongitude));

			sb.append("\"");
		}

		if (SocialMediaLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"SocialMediaLabel\": ");

			sb.append("\"");

			sb.append(_escape(SocialMediaLabel));

			sb.append("\"");
		}

		if (SocialMediaList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"SocialMediaList\": ");

			sb.append("[");

			for (int i = 0; i < SocialMediaList.length; i++) {
				sb.append(String.valueOf(SocialMediaList[i]));

				if ((i + 1) < SocialMediaList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (WebsiteAddress != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WebsiteAddress\": ");

			sb.append("\"");

			sb.append(_escape(WebsiteAddress));

			sb.append("\"");
		}

		if (WebsiteLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WebsiteLabel\": ");

			sb.append("\"");

			sb.append(_escape(WebsiteLabel));

			sb.append("\"");
		}

		if (WhatsAppLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WhatsAppLabel\": ");

			sb.append("\"");

			sb.append(_escape(WhatsAppLabel));

			sb.append("\"");
		}

		if (WhatsAppNumber != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WhatsAppNumber\": ");

			sb.append("\"");

			sb.append(_escape(WhatsAppNumber));

			sb.append("\"");
		}

		if (WorkingHours != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHours\": ");

			sb.append("\"");

			sb.append(_escape(WorkingHours));

			sb.append("\"");
		}

		if (WorkingHoursLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHoursLabel\": ");

			sb.append("\"");

			sb.append(_escape(WorkingHoursLabel));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.Contact", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}