package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("MiskForum")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "MiskForum")
public class MiskForum implements Serializable {

	public static MiskForum toDTO(String json) {
		return ObjectMapperUtil.readValue(MiskForum.class, json);
	}

	@Schema
	public String getAmenitiesLabel() {
		return AmenitiesLabel;
	}

	public void setAmenitiesLabel(String AmenitiesLabel) {
		this.AmenitiesLabel = AmenitiesLabel;
	}

	@JsonIgnore
	public void setAmenitiesLabel(
		UnsafeSupplier<String, Exception> AmenitiesLabelUnsafeSupplier) {

		try {
			AmenitiesLabel = AmenitiesLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String AmenitiesLabel;

	@Schema
	@Valid
	public AmenitiesList[] getAmenitiesList() {
		return AmenitiesList;
	}

	public void setAmenitiesList(AmenitiesList[] AmenitiesList) {
		this.AmenitiesList = AmenitiesList;
	}

	@JsonIgnore
	public void setAmenitiesList(
		UnsafeSupplier<AmenitiesList[], Exception>
			AmenitiesListUnsafeSupplier) {

		try {
			AmenitiesList = AmenitiesListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected AmenitiesList[] AmenitiesList;

	@Schema
	public String getBookSpaceLabel() {
		return BookSpaceLabel;
	}

	public void setBookSpaceLabel(String BookSpaceLabel) {
		this.BookSpaceLabel = BookSpaceLabel;
	}

	@JsonIgnore
	public void setBookSpaceLabel(
		UnsafeSupplier<String, Exception> BookSpaceLabelUnsafeSupplier) {

		try {
			BookSpaceLabel = BookSpaceLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String BookSpaceLabel;

	@Schema
	@Valid
	public BookSpaceList[] getBookSpaceList() {
		return BookSpaceList;
	}

	public void setBookSpaceList(BookSpaceList[] BookSpaceList) {
		this.BookSpaceList = BookSpaceList;
	}

	@JsonIgnore
	public void setBookSpaceList(
		UnsafeSupplier<BookSpaceList[], Exception>
			BookSpaceListUnsafeSupplier) {

		try {
			BookSpaceList = BookSpaceListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected BookSpaceList[] BookSpaceList;

	@Schema
	public String getContactEmailAddress() {
		return ContactEmailAddress;
	}

	public void setContactEmailAddress(String ContactEmailAddress) {
		this.ContactEmailAddress = ContactEmailAddress;
	}

	@JsonIgnore
	public void setContactEmailAddress(
		UnsafeSupplier<String, Exception> ContactEmailAddressUnsafeSupplier) {

		try {
			ContactEmailAddress = ContactEmailAddressUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String ContactEmailAddress;

	@Schema
	public String getContactLabel() {
		return ContactLabel;
	}

	public void setContactLabel(String ContactLabel) {
		this.ContactLabel = ContactLabel;
	}

	@JsonIgnore
	public void setContactLabel(
		UnsafeSupplier<String, Exception> ContactLabelUnsafeSupplier) {

		try {
			ContactLabel = ContactLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String ContactLabel;

	@Schema
	public String getContactTelephone() {
		return ContactTelephone;
	}

	public void setContactTelephone(String ContactTelephone) {
		this.ContactTelephone = ContactTelephone;
	}

	@JsonIgnore
	public void setContactTelephone(
		UnsafeSupplier<String, Exception> ContactTelephoneUnsafeSupplier) {

		try {
			ContactTelephone = ContactTelephoneUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String ContactTelephone;

	@Schema
	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	@JsonIgnore
	public void setDescription(
		UnsafeSupplier<String, Exception> DescriptionUnsafeSupplier) {

		try {
			Description = DescriptionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String Description;

	@Schema
	public String getEventLabel() {
		return EventLabel;
	}

	public void setEventLabel(String EventLabel) {
		this.EventLabel = EventLabel;
	}

	@JsonIgnore
	public void setEventLabel(
		UnsafeSupplier<String, Exception> EventLabelUnsafeSupplier) {

		try {
			EventLabel = EventLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String EventLabel;

	@Schema
	public String getEventName() {
		return EventName;
	}

	public void setEventName(String EventName) {
		this.EventName = EventName;
	}

	@JsonIgnore
	public void setEventName(
		UnsafeSupplier<String, Exception> EventNameUnsafeSupplier) {

		try {
			EventName = EventNameUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String EventName;

	@Schema
	public String[] getGalleryImagesList() {
		return GalleryImagesList;
	}

	public void setGalleryImagesList(String[] GalleryImagesList) {
		this.GalleryImagesList = GalleryImagesList;
	}

	@JsonIgnore
	public void setGalleryImagesList(
		UnsafeSupplier<String[], Exception> GalleryImagesListUnsafeSupplier) {

		try {
			GalleryImagesList = GalleryImagesListUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String[] GalleryImagesList;

	@Schema
	public String getGalleryLabel() {
		return GalleryLabel;
	}

	public void setGalleryLabel(String GalleryLabel) {
		this.GalleryLabel = GalleryLabel;
	}

	@JsonIgnore
	public void setGalleryLabel(
		UnsafeSupplier<String, Exception> GalleryLabelUnsafeSupplier) {

		try {
			GalleryLabel = GalleryLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String GalleryLabel;

	@Schema
	public String getLocationLabel() {
		return LocationLabel;
	}

	public void setLocationLabel(String LocationLabel) {
		this.LocationLabel = LocationLabel;
	}

	@JsonIgnore
	public void setLocationLabel(
		UnsafeSupplier<String, Exception> LocationLabelUnsafeSupplier) {

		try {
			LocationLabel = LocationLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String LocationLabel;

	@Schema
	public String getLocationLatitude() {
		return LocationLatitude;
	}

	public void setLocationLatitude(String LocationLatitude) {
		this.LocationLatitude = LocationLatitude;
	}

	@JsonIgnore
	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> LocationLatitudeUnsafeSupplier) {

		try {
			LocationLatitude = LocationLatitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String LocationLatitude;

	@Schema
	public String getLocationLongitude() {
		return LocationLongitude;
	}

	public void setLocationLongitude(String LocationLongitude) {
		this.LocationLongitude = LocationLongitude;
	}

	@JsonIgnore
	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> LocationLongitudeUnsafeSupplier) {

		try {
			LocationLongitude = LocationLongitudeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String LocationLongitude;

	@Schema
	public String getMiskForumBanner() {
		return MiskForumBanner;
	}

	public void setMiskForumBanner(String MiskForumBanner) {
		this.MiskForumBanner = MiskForumBanner;
	}

	@JsonIgnore
	public void setMiskForumBanner(
		UnsafeSupplier<String, Exception> MiskForumBannerUnsafeSupplier) {

		try {
			MiskForumBanner = MiskForumBannerUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String MiskForumBanner;

	@Schema
	public String getMiskForumLabel() {
		return MiskForumLabel;
	}

	public void setMiskForumLabel(String MiskForumLabel) {
		this.MiskForumLabel = MiskForumLabel;
	}

	@JsonIgnore
	public void setMiskForumLabel(
		UnsafeSupplier<String, Exception> MiskForumLabelUnsafeSupplier) {

		try {
			MiskForumLabel = MiskForumLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String MiskForumLabel;

	@Schema
	public String getWorkingDay() {
		return WorkingDay;
	}

	public void setWorkingDay(String WorkingDay) {
		this.WorkingDay = WorkingDay;
	}

	@JsonIgnore
	public void setWorkingDay(
		UnsafeSupplier<String, Exception> WorkingDayUnsafeSupplier) {

		try {
			WorkingDay = WorkingDayUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WorkingDay;

	@Schema
	public String getWorkingHours() {
		return WorkingHours;
	}

	public void setWorkingHours(String WorkingHours) {
		this.WorkingHours = WorkingHours;
	}

	@JsonIgnore
	public void setWorkingHours(
		UnsafeSupplier<String, Exception> WorkingHoursUnsafeSupplier) {

		try {
			WorkingHours = WorkingHoursUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WorkingHours;

	@Schema
	public String getWorkingHoursLabel() {
		return WorkingHoursLabel;
	}

	public void setWorkingHoursLabel(String WorkingHoursLabel) {
		this.WorkingHoursLabel = WorkingHoursLabel;
	}

	@JsonIgnore
	public void setWorkingHoursLabel(
		UnsafeSupplier<String, Exception> WorkingHoursLabelUnsafeSupplier) {

		try {
			WorkingHoursLabel = WorkingHoursLabelUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String WorkingHoursLabel;

	@Schema
	@Valid
	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	@JsonIgnore
	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected EventsSection eventsSection;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MiskForum)) {
			return false;
		}

		MiskForum miskForum = (MiskForum)object;

		return Objects.equals(toString(), miskForum.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (AmenitiesLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"AmenitiesLabel\": ");

			sb.append("\"");

			sb.append(_escape(AmenitiesLabel));

			sb.append("\"");
		}

		if (AmenitiesList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"AmenitiesList\": ");

			sb.append("[");

			for (int i = 0; i < AmenitiesList.length; i++) {
				sb.append(String.valueOf(AmenitiesList[i]));

				if ((i + 1) < AmenitiesList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (BookSpaceLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceLabel\": ");

			sb.append("\"");

			sb.append(_escape(BookSpaceLabel));

			sb.append("\"");
		}

		if (BookSpaceList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceList\": ");

			sb.append("[");

			for (int i = 0; i < BookSpaceList.length; i++) {
				sb.append(String.valueOf(BookSpaceList[i]));

				if ((i + 1) < BookSpaceList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (ContactEmailAddress != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactEmailAddress\": ");

			sb.append("\"");

			sb.append(_escape(ContactEmailAddress));

			sb.append("\"");
		}

		if (ContactLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactLabel\": ");

			sb.append("\"");

			sb.append(_escape(ContactLabel));

			sb.append("\"");
		}

		if (ContactTelephone != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactTelephone\": ");

			sb.append("\"");

			sb.append(_escape(ContactTelephone));

			sb.append("\"");
		}

		if (Description != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Description\": ");

			sb.append("\"");

			sb.append(_escape(Description));

			sb.append("\"");
		}

		if (EventLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EventLabel\": ");

			sb.append("\"");

			sb.append(_escape(EventLabel));

			sb.append("\"");
		}

		if (EventName != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EventName\": ");

			sb.append("\"");

			sb.append(_escape(EventName));

			sb.append("\"");
		}

		if (GalleryImagesList != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"GalleryImagesList\": ");

			sb.append("[");

			for (int i = 0; i < GalleryImagesList.length; i++) {
				sb.append("\"");

				sb.append(_escape(GalleryImagesList[i]));

				sb.append("\"");

				if ((i + 1) < GalleryImagesList.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (GalleryLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"GalleryLabel\": ");

			sb.append("\"");

			sb.append(_escape(GalleryLabel));

			sb.append("\"");
		}

		if (LocationLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLabel\": ");

			sb.append("\"");

			sb.append(_escape(LocationLabel));

			sb.append("\"");
		}

		if (LocationLatitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(LocationLatitude));

			sb.append("\"");
		}

		if (LocationLongitude != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(LocationLongitude));

			sb.append("\"");
		}

		if (MiskForumBanner != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"MiskForumBanner\": ");

			sb.append("\"");

			sb.append(_escape(MiskForumBanner));

			sb.append("\"");
		}

		if (MiskForumLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"MiskForumLabel\": ");

			sb.append("\"");

			sb.append(_escape(MiskForumLabel));

			sb.append("\"");
		}

		if (WorkingDay != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingDay\": ");

			sb.append("\"");

			sb.append(_escape(WorkingDay));

			sb.append("\"");
		}

		if (WorkingHours != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHours\": ");

			sb.append("\"");

			sb.append(_escape(WorkingHours));

			sb.append("\"");
		}

		if (WorkingHoursLabel != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHoursLabel\": ");

			sb.append("\"");

			sb.append(_escape(WorkingHoursLabel));

			sb.append("\"");
		}

		if (eventsSection != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(eventsSection));
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.MiskForum", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}