package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("MyFavorites")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "MyFavorites")
public class MyFavorites implements Serializable {

	public static MyFavorites toDTO(String json) {
		return ObjectMapperUtil.readValue(MyFavorites.class, json);
	}

	@Schema
	@Valid
	public EventList[] getEventLists() {
		return eventLists;
	}

	public void setEventLists(EventList[] eventLists) {
		this.eventLists = eventLists;
	}

	@JsonIgnore
	public void setEventLists(
		UnsafeSupplier<EventList[], Exception> eventListsUnsafeSupplier) {

		try {
			eventLists = eventListsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected EventList[] eventLists;

	@Schema
	@Valid
	public Highlight[] getHighlights() {
		return highlights;
	}

	public void setHighlights(Highlight[] highlights) {
		this.highlights = highlights;
	}

	@JsonIgnore
	public void setHighlights(
		UnsafeSupplier<Highlight[], Exception> highlightsUnsafeSupplier) {

		try {
			highlights = highlightsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Highlight[] highlights;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MyFavorites)) {
			return false;
		}

		MyFavorites myFavorites = (MyFavorites)object;

		return Objects.equals(toString(), myFavorites.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (eventLists != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventLists\": ");

			sb.append("[");

			for (int i = 0; i < eventLists.length; i++) {
				sb.append(String.valueOf(eventLists[i]));

				if ((i + 1) < eventLists.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (highlights != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlights\": ");

			sb.append("[");

			for (int i = 0; i < highlights.length; i++) {
				sb.append(String.valueOf(highlights[i]));

				if ((i + 1) < highlights.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.MyFavorites",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}