package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName(
	description = "Contains all of the data for notification settings.",
	value = "NotificationSettings"
)
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "NotificationSettings")
public class NotificationSettings implements Serializable {

	public static NotificationSettings toDTO(String json) {
		return ObjectMapperUtil.readValue(NotificationSettings.class, json);
	}

	@Schema(description = "User Id of the user.")
	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	@JsonIgnore
	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(description = "User Id of the user.")
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long appUserId;

	@Schema(description = "Send notification through SMS.")
	public Boolean getBroadcastMessage() {
		return broadcastMessage;
	}

	public void setBroadcastMessage(Boolean broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}

	@JsonIgnore
	public void setBroadcastMessage(
		UnsafeSupplier<Boolean, Exception> broadcastMessageUnsafeSupplier) {

		try {
			broadcastMessage = broadcastMessageUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(description = "Send notification through SMS.")
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean broadcastMessage;

	@Schema(description = "Company id of the application.")
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	@JsonIgnore
	public void setCompanyId(
		UnsafeSupplier<Integer, Exception> companyIdUnsafeSupplier) {

		try {
			companyId = companyIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(description = "Company id of the application.")
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Integer companyId;

	@Schema(
		description = "These notifications go to users that live in that location through simple targeting based on attributes like IP address or city."
	)
	public Boolean getEvents() {
		return events;
	}

	public void setEvents(Boolean events) {
		this.events = events;
	}

	@JsonIgnore
	public void setEvents(
		UnsafeSupplier<Boolean, Exception> eventsUnsafeSupplier) {

		try {
			events = eventsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(
		description = "These notifications go to users that live in that location through simple targeting based on attributes like IP address or city."
	)
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean events;

	@Schema(description = "Group id of the application.")
	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	@JsonIgnore
	public void setGroupId(
		UnsafeSupplier<Integer, Exception> groupIdUnsafeSupplier) {

		try {
			groupId = groupIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(description = "Group id of the application.")
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Integer groupId;

	@Schema(description = "Send notification through Email.")
	public Boolean getHighlight() {
		return highlight;
	}

	public void setHighlight(Boolean highlight) {
		this.highlight = highlight;
	}

	@JsonIgnore
	public void setHighlight(
		UnsafeSupplier<Boolean, Exception> highlightUnsafeSupplier) {

		try {
			highlight = highlightUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(description = "Send notification through Email.")
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean highlight;

	@Schema(
		description = "An automated notification sent by an application to a user when the application is not open."
	)
	public Boolean getNews() {
		return news;
	}

	public void setNews(Boolean news) {
		this.news = news;
	}

	@JsonIgnore
	public void setNews(UnsafeSupplier<Boolean, Exception> newsUnsafeSupplier) {
		try {
			news = newsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField(
		description = "An automated notification sent by an application to a user when the application is not open."
	)
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Boolean news;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof NotificationSettings)) {
			return false;
		}

		NotificationSettings notificationSettings =
			(NotificationSettings)object;

		return Objects.equals(toString(), notificationSettings.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (appUserId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(appUserId);
		}

		if (broadcastMessage != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"broadcastMessage\": ");

			sb.append(broadcastMessage);
		}

		if (companyId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"companyId\": ");

			sb.append(companyId);
		}

		if (events != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"events\": ");

			sb.append(events);
		}

		if (groupId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"groupId\": ");

			sb.append(groupId);
		}

		if (highlight != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlight\": ");

			sb.append(highlight);
		}

		if (news != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"news\": ");

			sb.append(news);
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.NotificationSettings",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}