package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("ExploreExperientialCenterPage")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "ExploreExperientialCenterPage")
public class ExploreExperientialCenterPage implements Serializable {

	public static ExploreExperientialCenterPage toDTO(String json) {
		return ObjectMapperUtil.readValue(
			ExploreExperientialCenterPage.class, json);
	}

	@Schema
	@Valid
	public ExploreExperientialCenterCategory[]
		getExploreExperientialCenterCategories() {

		return exploreExperientialCenterCategories;
	}

	public void setExploreExperientialCenterCategories(
		ExploreExperientialCenterCategory[]
			exploreExperientialCenterCategories) {

		this.exploreExperientialCenterCategories =
			exploreExperientialCenterCategories;
	}

	@JsonIgnore
	public void setExploreExperientialCenterCategories(
		UnsafeSupplier<ExploreExperientialCenterCategory[], Exception>
			exploreExperientialCenterCategoriesUnsafeSupplier) {

		try {
			exploreExperientialCenterCategories =
				exploreExperientialCenterCategoriesUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected ExploreExperientialCenterCategory[]
		exploreExperientialCenterCategories;

	@Schema
	@Valid
	public ExploreExperientialCenterPost[] getExploreExperientialCenterPosts() {
		return exploreExperientialCenterPosts;
	}

	public void setExploreExperientialCenterPosts(
		ExploreExperientialCenterPost[] exploreExperientialCenterPosts) {

		this.exploreExperientialCenterPosts = exploreExperientialCenterPosts;
	}

	@JsonIgnore
	public void setExploreExperientialCenterPosts(
		UnsafeSupplier<ExploreExperientialCenterPost[], Exception>
			exploreExperientialCenterPostsUnsafeSupplier) {

		try {
			exploreExperientialCenterPosts =
				exploreExperientialCenterPostsUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected ExploreExperientialCenterPost[] exploreExperientialCenterPosts;

	@Schema
	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	@JsonIgnore
	public void setPageTitle(
		UnsafeSupplier<String, Exception> pageTitleUnsafeSupplier) {

		try {
			pageTitle = pageTitleUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String pageTitle;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExploreExperientialCenterPage)) {
			return false;
		}

		ExploreExperientialCenterPage exploreExperientialCenterPage =
			(ExploreExperientialCenterPage)object;

		return Objects.equals(
			toString(), exploreExperientialCenterPage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (exploreExperientialCenterCategories != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreExperientialCenterCategories\": ");

			sb.append("[");

			for (int i = 0; i < exploreExperientialCenterCategories.length;
				 i++) {

				sb.append(
					String.valueOf(exploreExperientialCenterCategories[i]));

				if ((i + 1) < exploreExperientialCenterCategories.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (exploreExperientialCenterPosts != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreExperientialCenterPosts\": ");

			sb.append("[");

			for (int i = 0; i < exploreExperientialCenterPosts.length; i++) {
				sb.append(String.valueOf(exploreExperientialCenterPosts[i]));

				if ((i + 1) < exploreExperientialCenterPosts.length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (pageTitle != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"pageTitle\": ");

			sb.append("\"");

			sb.append(_escape(pageTitle));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.ExploreExperientialCenterPage",
		name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}