package misk.headless.dto.v1_0;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.liferay.petra.function.UnsafeSupplier;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.util.ObjectMapperUtil;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Generated;

import javax.validation.Valid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@GraphQLName("Booking")
@JsonFilter("Liferay.Vulcan")
@XmlRootElement(name = "Booking")
public class Booking implements Serializable {

	public static Booking toDTO(String json) {
		return ObjectMapperUtil.readValue(Booking.class, json);
	}

	@Schema
	public String getAppUserEmail() {
		return appUserEmail;
	}

	public void setAppUserEmail(String appUserEmail) {
		this.appUserEmail = appUserEmail;
	}

	@JsonIgnore
	public void setAppUserEmail(
		UnsafeSupplier<String, Exception> appUserEmailUnsafeSupplier) {

		try {
			appUserEmail = appUserEmailUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String appUserEmail;

	@Schema
	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	@JsonIgnore
	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long appUserId;

	@Schema
	public String getAppUserName() {
		return appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	@JsonIgnore
	public void setAppUserName(
		UnsafeSupplier<String, Exception> appUserNameUnsafeSupplier) {

		try {
			appUserName = appUserNameUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String appUserName;

	@Schema
	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	@JsonIgnore
	public void setBookingDate(
		UnsafeSupplier<String, Exception> bookingDateUnsafeSupplier) {

		try {
			bookingDate = bookingDateUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String bookingDate;

	@Schema
	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	@JsonIgnore
	public void setBookingId(
		UnsafeSupplier<Long, Exception> bookingIdUnsafeSupplier) {

		try {
			bookingId = bookingIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long bookingId;

	@Schema
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@JsonIgnore
	public void setCategory(
		UnsafeSupplier<String, Exception> categoryUnsafeSupplier) {

		try {
			category = categoryUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String category;

	@Schema
	@Valid
	public Court getCourt() {
		return court;
	}

	public void setCourt(Court court) {
		this.court = court;
	}

	@JsonIgnore
	public void setCourt(UnsafeSupplier<Court, Exception> courtUnsafeSupplier) {
		try {
			court = courtUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Court court;

	@Schema
	public Long getCourtId() {
		return courtId;
	}

	public void setCourtId(Long courtId) {
		this.courtId = courtId;
	}

	@JsonIgnore
	public void setCourtId(
		UnsafeSupplier<Long, Exception> courtIdUnsafeSupplier) {

		try {
			courtId = courtIdUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected Long courtId;

	@Schema
	public String getCourtLocation() {
		return courtLocation;
	}

	public void setCourtLocation(String courtLocation) {
		this.courtLocation = courtLocation;
	}

	@JsonIgnore
	public void setCourtLocation(
		UnsafeSupplier<String, Exception> courtLocationUnsafeSupplier) {

		try {
			courtLocation = courtLocationUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String courtLocation;

	@Schema
	public String getCourtName() {
		return courtName;
	}

	public void setCourtName(String courtName) {
		this.courtName = courtName;
	}

	@JsonIgnore
	public void setCourtName(
		UnsafeSupplier<String, Exception> courtNameUnsafeSupplier) {

		try {
			courtName = courtNameUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String courtName;

	@Schema
	@Valid
	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	@JsonIgnore
	public void setRequestStatus(
		UnsafeSupplier<RequestStatus, Exception> requestStatusUnsafeSupplier) {

		try {
			requestStatus = requestStatusUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected RequestStatus requestStatus;

	@Schema
	public String getSlotEndTime() {
		return slotEndTime;
	}

	public void setSlotEndTime(String slotEndTime) {
		this.slotEndTime = slotEndTime;
	}

	@JsonIgnore
	public void setSlotEndTime(
		UnsafeSupplier<String, Exception> slotEndTimeUnsafeSupplier) {

		try {
			slotEndTime = slotEndTimeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String slotEndTime;

	@Schema
	public String getSlotStartTime() {
		return slotStartTime;
	}

	public void setSlotStartTime(String slotStartTime) {
		this.slotStartTime = slotStartTime;
	}

	@JsonIgnore
	public void setSlotStartTime(
		UnsafeSupplier<String, Exception> slotStartTimeUnsafeSupplier) {

		try {
			slotStartTime = slotStartTimeUnsafeSupplier.get();
		}
		catch (RuntimeException re) {
			throw re;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@GraphQLField
	@JsonProperty(access = JsonProperty.Access.READ_WRITE)
	protected String slotStartTime;

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Booking)) {
			return false;
		}

		Booking booking = (Booking)object;

		return Objects.equals(toString(), booking.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		StringBundler sb = new StringBundler();

		sb.append("{");

		if (appUserEmail != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserEmail\": ");

			sb.append("\"");

			sb.append(_escape(appUserEmail));

			sb.append("\"");
		}

		if (appUserId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(appUserId);
		}

		if (appUserName != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserName\": ");

			sb.append("\"");

			sb.append(_escape(appUserName));

			sb.append("\"");
		}

		if (bookingDate != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookingDate\": ");

			sb.append("\"");

			sb.append(_escape(bookingDate));

			sb.append("\"");
		}

		if (bookingId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookingId\": ");

			sb.append(bookingId);
		}

		if (category != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"category\": ");

			sb.append("\"");

			sb.append(_escape(category));

			sb.append("\"");
		}

		if (court != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"court\": ");

			sb.append(String.valueOf(court));
		}

		if (courtId != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtId\": ");

			sb.append(courtId);
		}

		if (courtLocation != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtLocation\": ");

			sb.append("\"");

			sb.append(_escape(courtLocation));

			sb.append("\"");
		}

		if (courtName != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtName\": ");

			sb.append("\"");

			sb.append(_escape(courtName));

			sb.append("\"");
		}

		if (requestStatus != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"requestStatus\": ");

			sb.append(String.valueOf(requestStatus));
		}

		if (slotEndTime != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotEndTime\": ");

			sb.append("\"");

			sb.append(_escape(slotEndTime));

			sb.append("\"");
		}

		if (slotStartTime != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotStartTime\": ");

			sb.append("\"");

			sb.append(_escape(slotStartTime));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	@Schema(
		accessMode = Schema.AccessMode.READ_ONLY,
		defaultValue = "misk.headless.dto.v1_0.Booking", name = "x-class-name"
	)
	public String xClassName;

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		return string.replaceAll("\"", "\\\\\"");
	}

	private static boolean _isArray(Object value) {
		if (value == null) {
			return false;
		}

		Class<?> clazz = value.getClass();

		return clazz.isArray();
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			if (_isArray(value)) {
				sb.append("[");

				Object[] valueArray = (Object[])value;

				for (int i = 0; i < valueArray.length; i++) {
					if (valueArray[i] instanceof String) {
						sb.append("\"");
						sb.append(valueArray[i]);
						sb.append("\"");
					}
					else {
						sb.append(valueArray[i]);
					}

					if ((i + 1) < valueArray.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof Map) {
				sb.append(_toJSON((Map<String, ?>)value));
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(value);
				sb.append("\"");
			}
			else {
				sb.append(value);
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}