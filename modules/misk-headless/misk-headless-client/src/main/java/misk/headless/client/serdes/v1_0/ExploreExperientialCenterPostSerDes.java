package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ExploreExperientialCenterPost;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreExperientialCenterPostSerDes {

	public static ExploreExperientialCenterPost toDTO(String json) {
		ExploreExperientialCenterPostJSONParser
			exploreExperientialCenterPostJSONParser =
				new ExploreExperientialCenterPostJSONParser();

		return exploreExperientialCenterPostJSONParser.parseToDTO(json);
	}

	public static ExploreExperientialCenterPost[] toDTOs(String json) {
		ExploreExperientialCenterPostJSONParser
			exploreExperientialCenterPostJSONParser =
				new ExploreExperientialCenterPostJSONParser();

		return exploreExperientialCenterPostJSONParser.parseToDTOs(json);
	}

	public static String toJSON(
		ExploreExperientialCenterPost exploreExperientialCenterPost) {

		if (exploreExperientialCenterPost == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (exploreExperientialCenterPost.getAudioSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"audioSection\": ");

			sb.append(
				String.valueOf(
					exploreExperientialCenterPost.getAudioSection()));
		}

		if (exploreExperientialCenterPost.getButtonLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"buttonLabel\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterPost.getButtonLabel()));

			sb.append("\"");
		}

		if (exploreExperientialCenterPost.getCategoryId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"categoryId\": ");

			sb.append(exploreExperientialCenterPost.getCategoryId());
		}

		if (exploreExperientialCenterPost.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterPost.getDescription()));

			sb.append("\"");
		}

		if (exploreExperientialCenterPost.getEstimatedTourTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"estimatedTourTime\": ");

			sb.append("\"");

			sb.append(
				_escape(exploreExperientialCenterPost.getEstimatedTourTime()));

			sb.append("\"");
		}

		if (exploreExperientialCenterPost.getEstimatedTourTimeLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"estimatedTourTimeLabel\": ");

			sb.append("\"");

			sb.append(
				_escape(
					exploreExperientialCenterPost.getEstimatedTourTimeLabel()));

			sb.append("\"");
		}

		if (exploreExperientialCenterPost.getGalleryImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryImages\": ");

			sb.append("[");

			for (int i = 0;
				 i < exploreExperientialCenterPost.getGalleryImages().length;
				 i++) {

				sb.append("\"");

				sb.append(
					_escape(
						exploreExperientialCenterPost.getGalleryImages()[i]));

				sb.append("\"");

				if ((i + 1) <
						exploreExperientialCenterPost.
							getGalleryImages().length) {

					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (exploreExperientialCenterPost.getId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append(exploreExperientialCenterPost.getId());
		}

		if (exploreExperientialCenterPost.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterPost.getImage()));

			sb.append("\"");
		}

		if (exploreExperientialCenterPost.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterPost.getName()));

			sb.append("\"");
		}

		if (exploreExperientialCenterPost.getType() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"type\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterPost.getType()));

			sb.append("\"");
		}

		if (exploreExperientialCenterPost.getVideo() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"video\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterPost.getVideo()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ExploreExperientialCenterPostJSONParser
			exploreExperientialCenterPostJSONParser =
				new ExploreExperientialCenterPostJSONParser();

		return exploreExperientialCenterPostJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		ExploreExperientialCenterPost exploreExperientialCenterPost) {

		if (exploreExperientialCenterPost == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (exploreExperientialCenterPost.getAudioSection() == null) {
			map.put("audioSection", null);
		}
		else {
			map.put(
				"audioSection",
				String.valueOf(
					exploreExperientialCenterPost.getAudioSection()));
		}

		if (exploreExperientialCenterPost.getButtonLabel() == null) {
			map.put("buttonLabel", null);
		}
		else {
			map.put(
				"buttonLabel",
				String.valueOf(exploreExperientialCenterPost.getButtonLabel()));
		}

		if (exploreExperientialCenterPost.getCategoryId() == null) {
			map.put("categoryId", null);
		}
		else {
			map.put(
				"categoryId",
				String.valueOf(exploreExperientialCenterPost.getCategoryId()));
		}

		if (exploreExperientialCenterPost.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(exploreExperientialCenterPost.getDescription()));
		}

		if (exploreExperientialCenterPost.getEstimatedTourTime() == null) {
			map.put("estimatedTourTime", null);
		}
		else {
			map.put(
				"estimatedTourTime",
				String.valueOf(
					exploreExperientialCenterPost.getEstimatedTourTime()));
		}

		if (exploreExperientialCenterPost.getEstimatedTourTimeLabel() == null) {
			map.put("estimatedTourTimeLabel", null);
		}
		else {
			map.put(
				"estimatedTourTimeLabel",
				String.valueOf(
					exploreExperientialCenterPost.getEstimatedTourTimeLabel()));
		}

		if (exploreExperientialCenterPost.getGalleryImages() == null) {
			map.put("galleryImages", null);
		}
		else {
			map.put(
				"galleryImages",
				String.valueOf(
					exploreExperientialCenterPost.getGalleryImages()));
		}

		if (exploreExperientialCenterPost.getId() == null) {
			map.put("id", null);
		}
		else {
			map.put(
				"id", String.valueOf(exploreExperientialCenterPost.getId()));
		}

		if (exploreExperientialCenterPost.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put(
				"image",
				String.valueOf(exploreExperientialCenterPost.getImage()));
		}

		if (exploreExperientialCenterPost.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put(
				"name",
				String.valueOf(exploreExperientialCenterPost.getName()));
		}

		if (exploreExperientialCenterPost.getType() == null) {
			map.put("type", null);
		}
		else {
			map.put(
				"type",
				String.valueOf(exploreExperientialCenterPost.getType()));
		}

		if (exploreExperientialCenterPost.getVideo() == null) {
			map.put("video", null);
		}
		else {
			map.put(
				"video",
				String.valueOf(exploreExperientialCenterPost.getVideo()));
		}

		return map;
	}

	public static class ExploreExperientialCenterPostJSONParser
		extends BaseJSONParser<ExploreExperientialCenterPost> {

		@Override
		protected ExploreExperientialCenterPost createDTO() {
			return new ExploreExperientialCenterPost();
		}

		@Override
		protected ExploreExperientialCenterPost[] createDTOArray(int size) {
			return new ExploreExperientialCenterPost[size];
		}

		@Override
		protected void setField(
			ExploreExperientialCenterPost exploreExperientialCenterPost,
			String jsonParserFieldName, Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "audioSection")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setAudioSection(
						AudioSectionSerDes.toDTO((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "buttonLabel")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setButtonLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "categoryId")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setCategoryId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "estimatedTourTime")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setEstimatedTourTime(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "estimatedTourTimeLabel")) {

				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setEstimatedTourTimeLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "galleryImages")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setGalleryImages(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "id")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setImage(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setName(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "type")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setType(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "video")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPost.setVideo(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}