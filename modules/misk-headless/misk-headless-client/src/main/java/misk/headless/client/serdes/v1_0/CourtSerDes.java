package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Court;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CourtSerDes {

	public static Court toDTO(String json) {
		CourtJSONParser courtJSONParser = new CourtJSONParser();

		return courtJSONParser.parseToDTO(json);
	}

	public static Court[] toDTOs(String json) {
		CourtJSONParser courtJSONParser = new CourtJSONParser();

		return courtJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Court court) {
		if (court == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (court.getCategoryId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"categoryId\": ");

			sb.append(court.getCategoryId());
		}

		if (court.getContactEmail() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactEmail\": ");

			sb.append("\"");

			sb.append(_escape(court.getContactEmail()));

			sb.append("\"");
		}

		if (court.getContactPhone() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactPhone\": ");

			sb.append("\"");

			sb.append(_escape(court.getContactPhone()));

			sb.append("\"");
		}

		if (court.getCourtBooking() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtBooking\": ");

			sb.append(String.valueOf(court.getCourtBooking()));
		}

		if (court.getCourtCategory() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtCategory\": ");

			sb.append(String.valueOf(court.getCourtCategory()));
		}

		if (court.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(court.getDescription()));

			sb.append("\"");
		}

		if (court.getDetailImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"detailImages\": ");

			sb.append("[");

			for (int i = 0; i < court.getDetailImages().length; i++) {
				sb.append("\"");

				sb.append(_escape(court.getDetailImages()[i]));

				sb.append("\"");

				if ((i + 1) < court.getDetailImages().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (court.getHeading() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"heading\": ");

			sb.append("\"");

			sb.append(_escape(court.getHeading()));

			sb.append("\"");
		}

		if (court.getId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append(court.getId());
		}

		if (court.getLat() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lat\": ");

			sb.append("\"");

			sb.append(_escape(court.getLat()));

			sb.append("\"");
		}

		if (court.getListingImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"listingImage\": ");

			sb.append("\"");

			sb.append(_escape(court.getListingImage()));

			sb.append("\"");
		}

		if (court.getLon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lon\": ");

			sb.append("\"");

			sb.append(_escape(court.getLon()));

			sb.append("\"");
		}

		if (court.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(court.getName()));

			sb.append("\"");
		}

		if (court.getOpeningDays() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"openingDays\": ");

			sb.append("\"");

			sb.append(_escape(court.getOpeningDays()));

			sb.append("\"");
		}

		if (court.getOpeningHours() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"openingHours\": ");

			sb.append("\"");

			sb.append(_escape(court.getOpeningHours()));

			sb.append("\"");
		}

		if (court.getVenue() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"venue\": ");

			sb.append("\"");

			sb.append(_escape(court.getVenue()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		CourtJSONParser courtJSONParser = new CourtJSONParser();

		return courtJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Court court) {
		if (court == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (court.getCategoryId() == null) {
			map.put("categoryId", null);
		}
		else {
			map.put("categoryId", String.valueOf(court.getCategoryId()));
		}

		if (court.getContactEmail() == null) {
			map.put("contactEmail", null);
		}
		else {
			map.put("contactEmail", String.valueOf(court.getContactEmail()));
		}

		if (court.getContactPhone() == null) {
			map.put("contactPhone", null);
		}
		else {
			map.put("contactPhone", String.valueOf(court.getContactPhone()));
		}

		if (court.getCourtBooking() == null) {
			map.put("courtBooking", null);
		}
		else {
			map.put("courtBooking", String.valueOf(court.getCourtBooking()));
		}

		if (court.getCourtCategory() == null) {
			map.put("courtCategory", null);
		}
		else {
			map.put("courtCategory", String.valueOf(court.getCourtCategory()));
		}

		if (court.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(court.getDescription()));
		}

		if (court.getDetailImages() == null) {
			map.put("detailImages", null);
		}
		else {
			map.put("detailImages", String.valueOf(court.getDetailImages()));
		}

		if (court.getHeading() == null) {
			map.put("heading", null);
		}
		else {
			map.put("heading", String.valueOf(court.getHeading()));
		}

		if (court.getId() == null) {
			map.put("id", null);
		}
		else {
			map.put("id", String.valueOf(court.getId()));
		}

		if (court.getLat() == null) {
			map.put("lat", null);
		}
		else {
			map.put("lat", String.valueOf(court.getLat()));
		}

		if (court.getListingImage() == null) {
			map.put("listingImage", null);
		}
		else {
			map.put("listingImage", String.valueOf(court.getListingImage()));
		}

		if (court.getLon() == null) {
			map.put("lon", null);
		}
		else {
			map.put("lon", String.valueOf(court.getLon()));
		}

		if (court.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put("name", String.valueOf(court.getName()));
		}

		if (court.getOpeningDays() == null) {
			map.put("openingDays", null);
		}
		else {
			map.put("openingDays", String.valueOf(court.getOpeningDays()));
		}

		if (court.getOpeningHours() == null) {
			map.put("openingHours", null);
		}
		else {
			map.put("openingHours", String.valueOf(court.getOpeningHours()));
		}

		if (court.getVenue() == null) {
			map.put("venue", null);
		}
		else {
			map.put("venue", String.valueOf(court.getVenue()));
		}

		return map;
	}

	public static class CourtJSONParser extends BaseJSONParser<Court> {

		@Override
		protected Court createDTO() {
			return new Court();
		}

		@Override
		protected Court[] createDTOArray(int size) {
			return new Court[size];
		}

		@Override
		protected void setField(
			Court court, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "categoryId")) {
				if (jsonParserFieldValue != null) {
					court.setCategoryId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "contactEmail")) {
				if (jsonParserFieldValue != null) {
					court.setContactEmail((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "contactPhone")) {
				if (jsonParserFieldValue != null) {
					court.setContactPhone((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courtBooking")) {
				if (jsonParserFieldValue != null) {
					court.setCourtBooking(
						CourtBookingSerDes.toDTO((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courtCategory")) {
				if (jsonParserFieldValue != null) {
					court.setCourtCategory(
						CourtCategorySerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					court.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "detailImages")) {
				if (jsonParserFieldValue != null) {
					court.setDetailImages(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "heading")) {
				if (jsonParserFieldValue != null) {
					court.setHeading((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "id")) {
				if (jsonParserFieldValue != null) {
					court.setId(Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "lat")) {
				if (jsonParserFieldValue != null) {
					court.setLat((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "listingImage")) {
				if (jsonParserFieldValue != null) {
					court.setListingImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "lon")) {
				if (jsonParserFieldValue != null) {
					court.setLon((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					court.setName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "openingDays")) {
				if (jsonParserFieldValue != null) {
					court.setOpeningDays((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "openingHours")) {
				if (jsonParserFieldValue != null) {
					court.setOpeningHours((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "venue")) {
				if (jsonParserFieldValue != null) {
					court.setVenue((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}