package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.EventsSectionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class EventsSection implements Cloneable, Serializable {

	public static EventsSection toDTO(String json) {
		return EventsSectionSerDes.toDTO(json);
	}

	public EventList[] getItems() {
		return items;
	}

	public void setItems(EventList[] items) {
		this.items = items;
	}

	public void setItems(
		UnsafeSupplier<EventList[], Exception> itemsUnsafeSupplier) {

		try {
			items = itemsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventList[] items;

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public void setSectionTitle(
		UnsafeSupplier<String, Exception> sectionTitleUnsafeSupplier) {

		try {
			sectionTitle = sectionTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String sectionTitle;

	@Override
	public EventsSection clone() throws CloneNotSupportedException {
		return (EventsSection)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EventsSection)) {
			return false;
		}

		EventsSection eventsSection = (EventsSection)object;

		return Objects.equals(toString(), eventsSection.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return EventsSectionSerDes.toJSON(this);
	}

}