package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.MoreAboutCity;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MoreAboutCitySerDes {

	public static MoreAboutCity toDTO(String json) {
		MoreAboutCityJSONParser moreAboutCityJSONParser =
			new MoreAboutCityJSONParser();

		return moreAboutCityJSONParser.parseToDTO(json);
	}

	public static MoreAboutCity[] toDTOs(String json) {
		MoreAboutCityJSONParser moreAboutCityJSONParser =
			new MoreAboutCityJSONParser();

		return moreAboutCityJSONParser.parseToDTOs(json);
	}

	public static String toJSON(MoreAboutCity moreAboutCity) {
		if (moreAboutCity == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (moreAboutCity.getButtonLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"buttonLabel\": ");

			sb.append("\"");

			sb.append(_escape(moreAboutCity.getButtonLabel()));

			sb.append("\"");
		}

		if (moreAboutCity.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(moreAboutCity.getDescription()));

			sb.append("\"");
		}

		if (moreAboutCity.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(moreAboutCity.getImage()));

			sb.append("\"");
		}

		if (moreAboutCity.getSubtitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"subtitle\": ");

			sb.append("\"");

			sb.append(_escape(moreAboutCity.getSubtitle()));

			sb.append("\"");
		}

		if (moreAboutCity.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(moreAboutCity.getTitle()));

			sb.append("\"");
		}

		if (moreAboutCity.getTopHeading() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"topHeading\": ");

			sb.append("\"");

			sb.append(_escape(moreAboutCity.getTopHeading()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		MoreAboutCityJSONParser moreAboutCityJSONParser =
			new MoreAboutCityJSONParser();

		return moreAboutCityJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(MoreAboutCity moreAboutCity) {
		if (moreAboutCity == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (moreAboutCity.getButtonLabel() == null) {
			map.put("buttonLabel", null);
		}
		else {
			map.put(
				"buttonLabel", String.valueOf(moreAboutCity.getButtonLabel()));
		}

		if (moreAboutCity.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description", String.valueOf(moreAboutCity.getDescription()));
		}

		if (moreAboutCity.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(moreAboutCity.getImage()));
		}

		if (moreAboutCity.getSubtitle() == null) {
			map.put("subtitle", null);
		}
		else {
			map.put("subtitle", String.valueOf(moreAboutCity.getSubtitle()));
		}

		if (moreAboutCity.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(moreAboutCity.getTitle()));
		}

		if (moreAboutCity.getTopHeading() == null) {
			map.put("topHeading", null);
		}
		else {
			map.put(
				"topHeading", String.valueOf(moreAboutCity.getTopHeading()));
		}

		return map;
	}

	public static class MoreAboutCityJSONParser
		extends BaseJSONParser<MoreAboutCity> {

		@Override
		protected MoreAboutCity createDTO() {
			return new MoreAboutCity();
		}

		@Override
		protected MoreAboutCity[] createDTOArray(int size) {
			return new MoreAboutCity[size];
		}

		@Override
		protected void setField(
			MoreAboutCity moreAboutCity, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "buttonLabel")) {
				if (jsonParserFieldValue != null) {
					moreAboutCity.setButtonLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					moreAboutCity.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					moreAboutCity.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "subtitle")) {
				if (jsonParserFieldValue != null) {
					moreAboutCity.setSubtitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					moreAboutCity.setTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "topHeading")) {
				if (jsonParserFieldValue != null) {
					moreAboutCity.setTopHeading((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}