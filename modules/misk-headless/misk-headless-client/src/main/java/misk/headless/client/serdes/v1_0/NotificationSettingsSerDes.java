package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.NotificationSettings;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class NotificationSettingsSerDes {

	public static NotificationSettings toDTO(String json) {
		NotificationSettingsJSONParser notificationSettingsJSONParser =
			new NotificationSettingsJSONParser();

		return notificationSettingsJSONParser.parseToDTO(json);
	}

	public static NotificationSettings[] toDTOs(String json) {
		NotificationSettingsJSONParser notificationSettingsJSONParser =
			new NotificationSettingsJSONParser();

		return notificationSettingsJSONParser.parseToDTOs(json);
	}

	public static String toJSON(NotificationSettings notificationSettings) {
		if (notificationSettings == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (notificationSettings.getAppUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(notificationSettings.getAppUserId());
		}

		if (notificationSettings.getBroadcastMessage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"broadcastMessage\": ");

			sb.append(notificationSettings.getBroadcastMessage());
		}

		if (notificationSettings.getCompanyId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"companyId\": ");

			sb.append(notificationSettings.getCompanyId());
		}

		if (notificationSettings.getEvents() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"events\": ");

			sb.append(notificationSettings.getEvents());
		}

		if (notificationSettings.getGroupId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"groupId\": ");

			sb.append(notificationSettings.getGroupId());
		}

		if (notificationSettings.getHighlight() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlight\": ");

			sb.append(notificationSettings.getHighlight());
		}

		if (notificationSettings.getNews() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"news\": ");

			sb.append(notificationSettings.getNews());
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		NotificationSettingsJSONParser notificationSettingsJSONParser =
			new NotificationSettingsJSONParser();

		return notificationSettingsJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		NotificationSettings notificationSettings) {

		if (notificationSettings == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (notificationSettings.getAppUserId() == null) {
			map.put("appUserId", null);
		}
		else {
			map.put(
				"appUserId",
				String.valueOf(notificationSettings.getAppUserId()));
		}

		if (notificationSettings.getBroadcastMessage() == null) {
			map.put("broadcastMessage", null);
		}
		else {
			map.put(
				"broadcastMessage",
				String.valueOf(notificationSettings.getBroadcastMessage()));
		}

		if (notificationSettings.getCompanyId() == null) {
			map.put("companyId", null);
		}
		else {
			map.put(
				"companyId",
				String.valueOf(notificationSettings.getCompanyId()));
		}

		if (notificationSettings.getEvents() == null) {
			map.put("events", null);
		}
		else {
			map.put("events", String.valueOf(notificationSettings.getEvents()));
		}

		if (notificationSettings.getGroupId() == null) {
			map.put("groupId", null);
		}
		else {
			map.put(
				"groupId", String.valueOf(notificationSettings.getGroupId()));
		}

		if (notificationSettings.getHighlight() == null) {
			map.put("highlight", null);
		}
		else {
			map.put(
				"highlight",
				String.valueOf(notificationSettings.getHighlight()));
		}

		if (notificationSettings.getNews() == null) {
			map.put("news", null);
		}
		else {
			map.put("news", String.valueOf(notificationSettings.getNews()));
		}

		return map;
	}

	public static class NotificationSettingsJSONParser
		extends BaseJSONParser<NotificationSettings> {

		@Override
		protected NotificationSettings createDTO() {
			return new NotificationSettings();
		}

		@Override
		protected NotificationSettings[] createDTOArray(int size) {
			return new NotificationSettings[size];
		}

		@Override
		protected void setField(
			NotificationSettings notificationSettings,
			String jsonParserFieldName, Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "appUserId")) {
				if (jsonParserFieldValue != null) {
					notificationSettings.setAppUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "broadcastMessage")) {
				if (jsonParserFieldValue != null) {
					notificationSettings.setBroadcastMessage(
						(Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "companyId")) {
				if (jsonParserFieldValue != null) {
					notificationSettings.setCompanyId(
						Integer.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "events")) {
				if (jsonParserFieldValue != null) {
					notificationSettings.setEvents(
						(Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "groupId")) {
				if (jsonParserFieldValue != null) {
					notificationSettings.setGroupId(
						Integer.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "highlight")) {
				if (jsonParserFieldValue != null) {
					notificationSettings.setHighlight(
						(Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "news")) {
				if (jsonParserFieldValue != null) {
					notificationSettings.setNews((Boolean)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}