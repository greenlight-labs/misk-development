package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.TermsnConditions;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class TermsnConditionsSerDes {

	public static TermsnConditions toDTO(String json) {
		TermsnConditionsJSONParser termsnConditionsJSONParser =
			new TermsnConditionsJSONParser();

		return termsnConditionsJSONParser.parseToDTO(json);
	}

	public static TermsnConditions[] toDTOs(String json) {
		TermsnConditionsJSONParser termsnConditionsJSONParser =
			new TermsnConditionsJSONParser();

		return termsnConditionsJSONParser.parseToDTOs(json);
	}

	public static String toJSON(TermsnConditions termsnConditions) {
		if (termsnConditions == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (termsnConditions.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(termsnConditions.getDescription()));

			sb.append("\"");
		}

		if (termsnConditions.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(termsnConditions.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		TermsnConditionsJSONParser termsnConditionsJSONParser =
			new TermsnConditionsJSONParser();

		return termsnConditionsJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(TermsnConditions termsnConditions) {
		if (termsnConditions == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (termsnConditions.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(termsnConditions.getDescription()));
		}

		if (termsnConditions.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(termsnConditions.getTitle()));
		}

		return map;
	}

	public static class TermsnConditionsJSONParser
		extends BaseJSONParser<TermsnConditions> {

		@Override
		protected TermsnConditions createDTO() {
			return new TermsnConditions();
		}

		@Override
		protected TermsnConditions[] createDTOArray(int size) {
			return new TermsnConditions[size];
		}

		@Override
		protected void setField(
			TermsnConditions termsnConditions, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					termsnConditions.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					termsnConditions.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}