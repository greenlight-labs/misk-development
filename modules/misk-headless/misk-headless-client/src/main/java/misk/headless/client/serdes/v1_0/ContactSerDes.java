package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Contact;
import misk.headless.client.dto.v1_0.SocialMediaList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ContactSerDes {

	public static Contact toDTO(String json) {
		ContactJSONParser contactJSONParser = new ContactJSONParser();

		return contactJSONParser.parseToDTO(json);
	}

	public static Contact[] toDTOs(String json) {
		ContactJSONParser contactJSONParser = new ContactJSONParser();

		return contactJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Contact contact) {
		if (contact == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (contact.getAddress() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Address\": ");

			sb.append("\"");

			sb.append(_escape(contact.getAddress()));

			sb.append("\"");
		}

		if (contact.getCallUsLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"CallUsLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getCallUsLabel()));

			sb.append("\"");
		}

		if (contact.getCallUsNumber() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"CallUsNumber\": ");

			sb.append("\"");

			sb.append(_escape(contact.getCallUsNumber()));

			sb.append("\"");
		}

		if (contact.getContactLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getContactLabel()));

			sb.append("\"");
		}

		if (contact.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Description\": ");

			sb.append("\"");

			sb.append(_escape(contact.getDescription()));

			sb.append("\"");
		}

		if (contact.getEmailAddress() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EmailAddress\": ");

			sb.append("\"");

			sb.append(_escape(contact.getEmailAddress()));

			sb.append("\"");
		}

		if (contact.getEmailLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EmailLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getEmailLabel()));

			sb.append("\"");
		}

		if (contact.getLocationLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getLocationLabel()));

			sb.append("\"");
		}

		if (contact.getLocationLatitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(contact.getLocationLatitude()));

			sb.append("\"");
		}

		if (contact.getLocationLongitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(contact.getLocationLongitude()));

			sb.append("\"");
		}

		if (contact.getSocialMediaLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"SocialMediaLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getSocialMediaLabel()));

			sb.append("\"");
		}

		if (contact.getSocialMediaList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"SocialMediaList\": ");

			sb.append("[");

			for (int i = 0; i < contact.getSocialMediaList().length; i++) {
				sb.append(String.valueOf(contact.getSocialMediaList()[i]));

				if ((i + 1) < contact.getSocialMediaList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (contact.getWebsiteAddress() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WebsiteAddress\": ");

			sb.append("\"");

			sb.append(_escape(contact.getWebsiteAddress()));

			sb.append("\"");
		}

		if (contact.getWebsiteLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WebsiteLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getWebsiteLabel()));

			sb.append("\"");
		}

		if (contact.getWhatsAppLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WhatsAppLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getWhatsAppLabel()));

			sb.append("\"");
		}

		if (contact.getWhatsAppNumber() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WhatsAppNumber\": ");

			sb.append("\"");

			sb.append(_escape(contact.getWhatsAppNumber()));

			sb.append("\"");
		}

		if (contact.getWorkingHours() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHours\": ");

			sb.append("\"");

			sb.append(_escape(contact.getWorkingHours()));

			sb.append("\"");
		}

		if (contact.getWorkingHoursLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHoursLabel\": ");

			sb.append("\"");

			sb.append(_escape(contact.getWorkingHoursLabel()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ContactJSONParser contactJSONParser = new ContactJSONParser();

		return contactJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Contact contact) {
		if (contact == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (contact.getAddress() == null) {
			map.put("Address", null);
		}
		else {
			map.put("Address", String.valueOf(contact.getAddress()));
		}

		if (contact.getCallUsLabel() == null) {
			map.put("CallUsLabel", null);
		}
		else {
			map.put("CallUsLabel", String.valueOf(contact.getCallUsLabel()));
		}

		if (contact.getCallUsNumber() == null) {
			map.put("CallUsNumber", null);
		}
		else {
			map.put("CallUsNumber", String.valueOf(contact.getCallUsNumber()));
		}

		if (contact.getContactLabel() == null) {
			map.put("ContactLabel", null);
		}
		else {
			map.put("ContactLabel", String.valueOf(contact.getContactLabel()));
		}

		if (contact.getDescription() == null) {
			map.put("Description", null);
		}
		else {
			map.put("Description", String.valueOf(contact.getDescription()));
		}

		if (contact.getEmailAddress() == null) {
			map.put("EmailAddress", null);
		}
		else {
			map.put("EmailAddress", String.valueOf(contact.getEmailAddress()));
		}

		if (contact.getEmailLabel() == null) {
			map.put("EmailLabel", null);
		}
		else {
			map.put("EmailLabel", String.valueOf(contact.getEmailLabel()));
		}

		if (contact.getLocationLabel() == null) {
			map.put("LocationLabel", null);
		}
		else {
			map.put(
				"LocationLabel", String.valueOf(contact.getLocationLabel()));
		}

		if (contact.getLocationLatitude() == null) {
			map.put("LocationLatitude", null);
		}
		else {
			map.put(
				"LocationLatitude",
				String.valueOf(contact.getLocationLatitude()));
		}

		if (contact.getLocationLongitude() == null) {
			map.put("LocationLongitude", null);
		}
		else {
			map.put(
				"LocationLongitude",
				String.valueOf(contact.getLocationLongitude()));
		}

		if (contact.getSocialMediaLabel() == null) {
			map.put("SocialMediaLabel", null);
		}
		else {
			map.put(
				"SocialMediaLabel",
				String.valueOf(contact.getSocialMediaLabel()));
		}

		if (contact.getSocialMediaList() == null) {
			map.put("SocialMediaList", null);
		}
		else {
			map.put(
				"SocialMediaList",
				String.valueOf(contact.getSocialMediaList()));
		}

		if (contact.getWebsiteAddress() == null) {
			map.put("WebsiteAddress", null);
		}
		else {
			map.put(
				"WebsiteAddress", String.valueOf(contact.getWebsiteAddress()));
		}

		if (contact.getWebsiteLabel() == null) {
			map.put("WebsiteLabel", null);
		}
		else {
			map.put("WebsiteLabel", String.valueOf(contact.getWebsiteLabel()));
		}

		if (contact.getWhatsAppLabel() == null) {
			map.put("WhatsAppLabel", null);
		}
		else {
			map.put(
				"WhatsAppLabel", String.valueOf(contact.getWhatsAppLabel()));
		}

		if (contact.getWhatsAppNumber() == null) {
			map.put("WhatsAppNumber", null);
		}
		else {
			map.put(
				"WhatsAppNumber", String.valueOf(contact.getWhatsAppNumber()));
		}

		if (contact.getWorkingHours() == null) {
			map.put("WorkingHours", null);
		}
		else {
			map.put("WorkingHours", String.valueOf(contact.getWorkingHours()));
		}

		if (contact.getWorkingHoursLabel() == null) {
			map.put("WorkingHoursLabel", null);
		}
		else {
			map.put(
				"WorkingHoursLabel",
				String.valueOf(contact.getWorkingHoursLabel()));
		}

		return map;
	}

	public static class ContactJSONParser extends BaseJSONParser<Contact> {

		@Override
		protected Contact createDTO() {
			return new Contact();
		}

		@Override
		protected Contact[] createDTOArray(int size) {
			return new Contact[size];
		}

		@Override
		protected void setField(
			Contact contact, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "Address")) {
				if (jsonParserFieldValue != null) {
					contact.setAddress((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "CallUsLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setCallUsLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "CallUsNumber")) {
				if (jsonParserFieldValue != null) {
					contact.setCallUsNumber((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "ContactLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setContactLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "Description")) {
				if (jsonParserFieldValue != null) {
					contact.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "EmailAddress")) {
				if (jsonParserFieldValue != null) {
					contact.setEmailAddress((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "EmailLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setEmailLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "LocationLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setLocationLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "LocationLatitude")) {
				if (jsonParserFieldValue != null) {
					contact.setLocationLatitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "LocationLongitude")) {
				if (jsonParserFieldValue != null) {
					contact.setLocationLongitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "SocialMediaLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setSocialMediaLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "SocialMediaList")) {
				if (jsonParserFieldValue != null) {
					contact.setSocialMediaList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> SocialMediaListSerDes.toDTO(
								(String)object)
						).toArray(
							size -> new SocialMediaList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WebsiteAddress")) {
				if (jsonParserFieldValue != null) {
					contact.setWebsiteAddress((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WebsiteLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setWebsiteLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WhatsAppLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setWhatsAppLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WhatsAppNumber")) {
				if (jsonParserFieldValue != null) {
					contact.setWhatsAppNumber((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WorkingHours")) {
				if (jsonParserFieldValue != null) {
					contact.setWorkingHours((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WorkingHoursLabel")) {
				if (jsonParserFieldValue != null) {
					contact.setWorkingHoursLabel((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}