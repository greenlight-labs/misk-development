package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.SocialMediaList;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ContactSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Contact implements Cloneable, Serializable {

	public static Contact toDTO(String json) {
		return ContactSerDes.toDTO(json);
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String Address) {
		this.Address = Address;
	}

	public void setAddress(
		UnsafeSupplier<String, Exception> AddressUnsafeSupplier) {

		try {
			Address = AddressUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String Address;

	public String getCallUsLabel() {
		return CallUsLabel;
	}

	public void setCallUsLabel(String CallUsLabel) {
		this.CallUsLabel = CallUsLabel;
	}

	public void setCallUsLabel(
		UnsafeSupplier<String, Exception> CallUsLabelUnsafeSupplier) {

		try {
			CallUsLabel = CallUsLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String CallUsLabel;

	public String getCallUsNumber() {
		return CallUsNumber;
	}

	public void setCallUsNumber(String CallUsNumber) {
		this.CallUsNumber = CallUsNumber;
	}

	public void setCallUsNumber(
		UnsafeSupplier<String, Exception> CallUsNumberUnsafeSupplier) {

		try {
			CallUsNumber = CallUsNumberUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String CallUsNumber;

	public String getContactLabel() {
		return ContactLabel;
	}

	public void setContactLabel(String ContactLabel) {
		this.ContactLabel = ContactLabel;
	}

	public void setContactLabel(
		UnsafeSupplier<String, Exception> ContactLabelUnsafeSupplier) {

		try {
			ContactLabel = ContactLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String ContactLabel;

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> DescriptionUnsafeSupplier) {

		try {
			Description = DescriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String Description;

	public String getEmailAddress() {
		return EmailAddress;
	}

	public void setEmailAddress(String EmailAddress) {
		this.EmailAddress = EmailAddress;
	}

	public void setEmailAddress(
		UnsafeSupplier<String, Exception> EmailAddressUnsafeSupplier) {

		try {
			EmailAddress = EmailAddressUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String EmailAddress;

	public String getEmailLabel() {
		return EmailLabel;
	}

	public void setEmailLabel(String EmailLabel) {
		this.EmailLabel = EmailLabel;
	}

	public void setEmailLabel(
		UnsafeSupplier<String, Exception> EmailLabelUnsafeSupplier) {

		try {
			EmailLabel = EmailLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String EmailLabel;

	public String getLocationLabel() {
		return LocationLabel;
	}

	public void setLocationLabel(String LocationLabel) {
		this.LocationLabel = LocationLabel;
	}

	public void setLocationLabel(
		UnsafeSupplier<String, Exception> LocationLabelUnsafeSupplier) {

		try {
			LocationLabel = LocationLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String LocationLabel;

	public String getLocationLatitude() {
		return LocationLatitude;
	}

	public void setLocationLatitude(String LocationLatitude) {
		this.LocationLatitude = LocationLatitude;
	}

	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> LocationLatitudeUnsafeSupplier) {

		try {
			LocationLatitude = LocationLatitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String LocationLatitude;

	public String getLocationLongitude() {
		return LocationLongitude;
	}

	public void setLocationLongitude(String LocationLongitude) {
		this.LocationLongitude = LocationLongitude;
	}

	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> LocationLongitudeUnsafeSupplier) {

		try {
			LocationLongitude = LocationLongitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String LocationLongitude;

	public String getSocialMediaLabel() {
		return SocialMediaLabel;
	}

	public void setSocialMediaLabel(String SocialMediaLabel) {
		this.SocialMediaLabel = SocialMediaLabel;
	}

	public void setSocialMediaLabel(
		UnsafeSupplier<String, Exception> SocialMediaLabelUnsafeSupplier) {

		try {
			SocialMediaLabel = SocialMediaLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String SocialMediaLabel;

	public SocialMediaList[] getSocialMediaList() {
		return SocialMediaList;
	}

	public void setSocialMediaList(SocialMediaList[] SocialMediaList) {
		this.SocialMediaList = SocialMediaList;
	}

	public void setSocialMediaList(
		UnsafeSupplier<SocialMediaList[], Exception>
			SocialMediaListUnsafeSupplier) {

		try {
			SocialMediaList = SocialMediaListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected SocialMediaList[] SocialMediaList;

	public String getWebsiteAddress() {
		return WebsiteAddress;
	}

	public void setWebsiteAddress(String WebsiteAddress) {
		this.WebsiteAddress = WebsiteAddress;
	}

	public void setWebsiteAddress(
		UnsafeSupplier<String, Exception> WebsiteAddressUnsafeSupplier) {

		try {
			WebsiteAddress = WebsiteAddressUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WebsiteAddress;

	public String getWebsiteLabel() {
		return WebsiteLabel;
	}

	public void setWebsiteLabel(String WebsiteLabel) {
		this.WebsiteLabel = WebsiteLabel;
	}

	public void setWebsiteLabel(
		UnsafeSupplier<String, Exception> WebsiteLabelUnsafeSupplier) {

		try {
			WebsiteLabel = WebsiteLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WebsiteLabel;

	public String getWhatsAppLabel() {
		return WhatsAppLabel;
	}

	public void setWhatsAppLabel(String WhatsAppLabel) {
		this.WhatsAppLabel = WhatsAppLabel;
	}

	public void setWhatsAppLabel(
		UnsafeSupplier<String, Exception> WhatsAppLabelUnsafeSupplier) {

		try {
			WhatsAppLabel = WhatsAppLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WhatsAppLabel;

	public String getWhatsAppNumber() {
		return WhatsAppNumber;
	}

	public void setWhatsAppNumber(String WhatsAppNumber) {
		this.WhatsAppNumber = WhatsAppNumber;
	}

	public void setWhatsAppNumber(
		UnsafeSupplier<String, Exception> WhatsAppNumberUnsafeSupplier) {

		try {
			WhatsAppNumber = WhatsAppNumberUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WhatsAppNumber;

	public String getWorkingHours() {
		return WorkingHours;
	}

	public void setWorkingHours(String WorkingHours) {
		this.WorkingHours = WorkingHours;
	}

	public void setWorkingHours(
		UnsafeSupplier<String, Exception> WorkingHoursUnsafeSupplier) {

		try {
			WorkingHours = WorkingHoursUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WorkingHours;

	public String getWorkingHoursLabel() {
		return WorkingHoursLabel;
	}

	public void setWorkingHoursLabel(String WorkingHoursLabel) {
		this.WorkingHoursLabel = WorkingHoursLabel;
	}

	public void setWorkingHoursLabel(
		UnsafeSupplier<String, Exception> WorkingHoursLabelUnsafeSupplier) {

		try {
			WorkingHoursLabel = WorkingHoursLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WorkingHoursLabel;

	@Override
	public Contact clone() throws CloneNotSupportedException {
		return (Contact)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Contact)) {
			return false;
		}

		Contact contact = (Contact)object;

		return Objects.equals(toString(), contact.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ContactSerDes.toJSON(this);
	}

}