package misk.headless.client.function;

import javax.annotation.Generated;

/**
 * @author Tayyab Zafar
 * @generated
 */
@FunctionalInterface
@Generated("")
public interface UnsafeSupplier<T, E extends Throwable> {

	public T get() throws E;

}