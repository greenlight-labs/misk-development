package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ExploreExperientialCenterCategory;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreExperientialCenterCategorySerDes {

	public static ExploreExperientialCenterCategory toDTO(String json) {
		ExploreExperientialCenterCategoryJSONParser
			exploreExperientialCenterCategoryJSONParser =
				new ExploreExperientialCenterCategoryJSONParser();

		return exploreExperientialCenterCategoryJSONParser.parseToDTO(json);
	}

	public static ExploreExperientialCenterCategory[] toDTOs(String json) {
		ExploreExperientialCenterCategoryJSONParser
			exploreExperientialCenterCategoryJSONParser =
				new ExploreExperientialCenterCategoryJSONParser();

		return exploreExperientialCenterCategoryJSONParser.parseToDTOs(json);
	}

	public static String toJSON(
		ExploreExperientialCenterCategory exploreExperientialCenterCategory) {

		if (exploreExperientialCenterCategory == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (exploreExperientialCenterCategory.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(
				_escape(exploreExperientialCenterCategory.getDescription()));

			sb.append("\"");
		}

		if (exploreExperientialCenterCategory.getEstimatedTourTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"estimatedTourTime\": ");

			sb.append("\"");

			sb.append(
				_escape(
					exploreExperientialCenterCategory.getEstimatedTourTime()));

			sb.append("\"");
		}

		if (exploreExperientialCenterCategory.getEstimatedTourTimeLabel() !=
				null) {

			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"estimatedTourTimeLabel\": ");

			sb.append("\"");

			sb.append(
				_escape(
					exploreExperientialCenterCategory.
						getEstimatedTourTimeLabel()));

			sb.append("\"");
		}

		if (exploreExperientialCenterCategory.getHeading() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"heading\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterCategory.getHeading()));

			sb.append("\"");
		}

		if (exploreExperientialCenterCategory.getId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append(exploreExperientialCenterCategory.getId());
		}

		if (exploreExperientialCenterCategory.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterCategory.getImage()));

			sb.append("\"");
		}

		if (exploreExperientialCenterCategory.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterCategory.getName()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ExploreExperientialCenterCategoryJSONParser
			exploreExperientialCenterCategoryJSONParser =
				new ExploreExperientialCenterCategoryJSONParser();

		return exploreExperientialCenterCategoryJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		ExploreExperientialCenterCategory exploreExperientialCenterCategory) {

		if (exploreExperientialCenterCategory == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (exploreExperientialCenterCategory.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(
					exploreExperientialCenterCategory.getDescription()));
		}

		if (exploreExperientialCenterCategory.getEstimatedTourTime() == null) {
			map.put("estimatedTourTime", null);
		}
		else {
			map.put(
				"estimatedTourTime",
				String.valueOf(
					exploreExperientialCenterCategory.getEstimatedTourTime()));
		}

		if (exploreExperientialCenterCategory.getEstimatedTourTimeLabel() ==
				null) {

			map.put("estimatedTourTimeLabel", null);
		}
		else {
			map.put(
				"estimatedTourTimeLabel",
				String.valueOf(
					exploreExperientialCenterCategory.
						getEstimatedTourTimeLabel()));
		}

		if (exploreExperientialCenterCategory.getHeading() == null) {
			map.put("heading", null);
		}
		else {
			map.put(
				"heading",
				String.valueOf(exploreExperientialCenterCategory.getHeading()));
		}

		if (exploreExperientialCenterCategory.getId() == null) {
			map.put("id", null);
		}
		else {
			map.put(
				"id",
				String.valueOf(exploreExperientialCenterCategory.getId()));
		}

		if (exploreExperientialCenterCategory.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put(
				"image",
				String.valueOf(exploreExperientialCenterCategory.getImage()));
		}

		if (exploreExperientialCenterCategory.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put(
				"name",
				String.valueOf(exploreExperientialCenterCategory.getName()));
		}

		return map;
	}

	public static class ExploreExperientialCenterCategoryJSONParser
		extends BaseJSONParser<ExploreExperientialCenterCategory> {

		@Override
		protected ExploreExperientialCenterCategory createDTO() {
			return new ExploreExperientialCenterCategory();
		}

		@Override
		protected ExploreExperientialCenterCategory[] createDTOArray(int size) {
			return new ExploreExperientialCenterCategory[size];
		}

		@Override
		protected void setField(
			ExploreExperientialCenterCategory exploreExperientialCenterCategory,
			String jsonParserFieldName, Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterCategory.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "estimatedTourTime")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterCategory.setEstimatedTourTime(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "estimatedTourTimeLabel")) {

				if (jsonParserFieldValue != null) {
					exploreExperientialCenterCategory.setEstimatedTourTimeLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "heading")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterCategory.setHeading(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "id")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterCategory.setId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterCategory.setImage(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterCategory.setName(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}