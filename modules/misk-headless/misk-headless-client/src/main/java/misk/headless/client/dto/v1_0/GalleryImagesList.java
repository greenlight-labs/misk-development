package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.GalleryImagesListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class GalleryImagesList implements Cloneable, Serializable {

	public static GalleryImagesList toDTO(String json) {
		return GalleryImagesListSerDes.toDTO(json);
	}

	public String getImagesURL() {
		return imagesURL;
	}

	public void setImagesURL(String imagesURL) {
		this.imagesURL = imagesURL;
	}

	public void setImagesURL(
		UnsafeSupplier<String, Exception> imagesURLUnsafeSupplier) {

		try {
			imagesURL = imagesURLUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String imagesURL;

	@Override
	public GalleryImagesList clone() throws CloneNotSupportedException {
		return (GalleryImagesList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GalleryImagesList)) {
			return false;
		}

		GalleryImagesList galleryImagesList = (GalleryImagesList)object;

		return Objects.equals(toString(), galleryImagesList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return GalleryImagesListSerDes.toJSON(this);
	}

}