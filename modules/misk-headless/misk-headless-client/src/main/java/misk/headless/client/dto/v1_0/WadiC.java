package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AttractionSection;
import misk.headless.client.dto.v1_0.BannerSection;
import misk.headless.client.dto.v1_0.BookCourtSection;
import misk.headless.client.dto.v1_0.EventsSection;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.WadiCSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class WadiC implements Cloneable, Serializable {

	public static WadiC toDTO(String json) {
		return WadiCSerDes.toDTO(json);
	}

	public AttractionSection getAttractionSection() {
		return attractionSection;
	}

	public void setAttractionSection(AttractionSection attractionSection) {
		this.attractionSection = attractionSection;
	}

	public void setAttractionSection(
		UnsafeSupplier<AttractionSection, Exception>
			attractionSectionUnsafeSupplier) {

		try {
			attractionSection = attractionSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected AttractionSection attractionSection;

	public BannerSection getBannerSection() {
		return bannerSection;
	}

	public void setBannerSection(BannerSection bannerSection) {
		this.bannerSection = bannerSection;
	}

	public void setBannerSection(
		UnsafeSupplier<BannerSection, Exception> bannerSectionUnsafeSupplier) {

		try {
			bannerSection = bannerSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected BannerSection bannerSection;

	public BookCourtSection getBookCourtSection() {
		return bookCourtSection;
	}

	public void setBookCourtSection(BookCourtSection bookCourtSection) {
		this.bookCourtSection = bookCourtSection;
	}

	public void setBookCourtSection(
		UnsafeSupplier<BookCourtSection, Exception>
			bookCourtSectionUnsafeSupplier) {

		try {
			bookCourtSection = bookCourtSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected BookCourtSection bookCourtSection;

	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventsSection eventsSection;

	@Override
	public WadiC clone() throws CloneNotSupportedException {
		return (WadiC)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof WadiC)) {
			return false;
		}

		WadiC wadiC = (WadiC)object;

		return Objects.equals(toString(), wadiC.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return WadiCSerDes.toJSON(this);
	}

}