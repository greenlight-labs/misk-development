package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.CourtBookingSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CourtBooking implements Cloneable, Serializable {

	public static CourtBooking toDTO(String json) {
		return CourtBookingSerDes.toDTO(json);
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public void setBookingDate(
		UnsafeSupplier<String, Exception> bookingDateUnsafeSupplier) {

		try {
			bookingDate = bookingDateUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String bookingDate;

	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	public void setBookingId(
		UnsafeSupplier<Long, Exception> bookingIdUnsafeSupplier) {

		try {
			bookingId = bookingIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long bookingId;

	public String getSlotEndTime() {
		return slotEndTime;
	}

	public void setSlotEndTime(String slotEndTime) {
		this.slotEndTime = slotEndTime;
	}

	public void setSlotEndTime(
		UnsafeSupplier<String, Exception> slotEndTimeUnsafeSupplier) {

		try {
			slotEndTime = slotEndTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String slotEndTime;

	public String getSlotStartTime() {
		return slotStartTime;
	}

	public void setSlotStartTime(String slotStartTime) {
		this.slotStartTime = slotStartTime;
	}

	public void setSlotStartTime(
		UnsafeSupplier<String, Exception> slotStartTimeUnsafeSupplier) {

		try {
			slotStartTime = slotStartTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String slotStartTime;

	@Override
	public CourtBooking clone() throws CloneNotSupportedException {
		return (CourtBooking)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CourtBooking)) {
			return false;
		}

		CourtBooking courtBooking = (CourtBooking)object;

		return Objects.equals(toString(), courtBooking.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return CourtBookingSerDes.toJSON(this);
	}

}