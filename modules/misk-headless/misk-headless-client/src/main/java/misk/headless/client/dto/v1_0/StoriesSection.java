package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Story;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.StoriesSectionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class StoriesSection implements Cloneable, Serializable {

	public static StoriesSection toDTO(String json) {
		return StoriesSectionSerDes.toDTO(json);
	}

	public Story[] getItems() {
		return items;
	}

	public void setItems(Story[] items) {
		this.items = items;
	}

	public void setItems(
		UnsafeSupplier<Story[], Exception> itemsUnsafeSupplier) {

		try {
			items = itemsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Story[] items;

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public void setSectionTitle(
		UnsafeSupplier<String, Exception> sectionTitleUnsafeSupplier) {

		try {
			sectionTitle = sectionTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String sectionTitle;

	@Override
	public StoriesSection clone() throws CloneNotSupportedException {
		return (StoriesSection)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof StoriesSection)) {
			return false;
		}

		StoriesSection storiesSection = (StoriesSection)object;

		return Objects.equals(toString(), storiesSection.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return StoriesSectionSerDes.toJSON(this);
	}

}