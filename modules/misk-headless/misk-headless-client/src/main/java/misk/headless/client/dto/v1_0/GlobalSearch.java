package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Album;
import misk.headless.client.dto.v1_0.Court;
import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.dto.v1_0.Highlight;
import misk.headless.client.dto.v1_0.Story;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.GlobalSearchSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class GlobalSearch implements Cloneable, Serializable {

	public static GlobalSearch toDTO(String json) {
		return GlobalSearchSerDes.toDTO(json);
	}

	public Album[] getAlbums() {
		return albums;
	}

	public void setAlbums(Album[] albums) {
		this.albums = albums;
	}

	public void setAlbums(
		UnsafeSupplier<Album[], Exception> albumsUnsafeSupplier) {

		try {
			albums = albumsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Album[] albums;

	public Court[] getCourts() {
		return courts;
	}

	public void setCourts(Court[] courts) {
		this.courts = courts;
	}

	public void setCourts(
		UnsafeSupplier<Court[], Exception> courtsUnsafeSupplier) {

		try {
			courts = courtsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Court[] courts;

	public EventList[] getEventLists() {
		return eventLists;
	}

	public void setEventLists(EventList[] eventLists) {
		this.eventLists = eventLists;
	}

	public void setEventLists(
		UnsafeSupplier<EventList[], Exception> eventListsUnsafeSupplier) {

		try {
			eventLists = eventListsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventList[] eventLists;

	public Highlight[] getHighlights() {
		return highlights;
	}

	public void setHighlights(Highlight[] highlights) {
		this.highlights = highlights;
	}

	public void setHighlights(
		UnsafeSupplier<Highlight[], Exception> highlightsUnsafeSupplier) {

		try {
			highlights = highlightsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Highlight[] highlights;

	public Story[] getStories() {
		return stories;
	}

	public void setStories(Story[] stories) {
		this.stories = stories;
	}

	public void setStories(
		UnsafeSupplier<Story[], Exception> storiesUnsafeSupplier) {

		try {
			stories = storiesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Story[] stories;

	@Override
	public GlobalSearch clone() throws CloneNotSupportedException {
		return (GlobalSearch)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GlobalSearch)) {
			return false;
		}

		GlobalSearch globalSearch = (GlobalSearch)object;

		return Objects.equals(toString(), globalSearch.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return GlobalSearchSerDes.toJSON(this);
	}

}