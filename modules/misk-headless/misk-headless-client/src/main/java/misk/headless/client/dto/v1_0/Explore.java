package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.EventsSection;
import misk.headless.client.dto.v1_0.ExploreList;
import misk.headless.client.dto.v1_0.FaclitiesList;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ExploreSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Explore implements Cloneable, Serializable {

	public static Explore toDTO(String json) {
		return ExploreSerDes.toDTO(json);
	}

	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventsSection eventsSection;

	public String getExploreLabel() {
		return exploreLabel;
	}

	public void setExploreLabel(String exploreLabel) {
		this.exploreLabel = exploreLabel;
	}

	public void setExploreLabel(
		UnsafeSupplier<String, Exception> exploreLabelUnsafeSupplier) {

		try {
			exploreLabel = exploreLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String exploreLabel;

	public ExploreList[] getExploreList() {
		return exploreList;
	}

	public void setExploreList(ExploreList[] exploreList) {
		this.exploreList = exploreList;
	}

	public void setExploreList(
		UnsafeSupplier<ExploreList[], Exception> exploreListUnsafeSupplier) {

		try {
			exploreList = exploreListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected ExploreList[] exploreList;

	public String getExpolerId() {
		return expolerId;
	}

	public void setExpolerId(String expolerId) {
		this.expolerId = expolerId;
	}

	public void setExpolerId(
		UnsafeSupplier<String, Exception> expolerIdUnsafeSupplier) {

		try {
			expolerId = expolerIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String expolerId;

	public String getFaclitiesLabel() {
		return faclitiesLabel;
	}

	public void setFaclitiesLabel(String faclitiesLabel) {
		this.faclitiesLabel = faclitiesLabel;
	}

	public void setFaclitiesLabel(
		UnsafeSupplier<String, Exception> faclitiesLabelUnsafeSupplier) {

		try {
			faclitiesLabel = faclitiesLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String faclitiesLabel;

	public FaclitiesList[] getFaclitiesList() {
		return faclitiesList;
	}

	public void setFaclitiesList(FaclitiesList[] faclitiesList) {
		this.faclitiesList = faclitiesList;
	}

	public void setFaclitiesList(
		UnsafeSupplier<FaclitiesList[], Exception>
			faclitiesListUnsafeSupplier) {

		try {
			faclitiesList = faclitiesListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected FaclitiesList[] faclitiesList;

	public String[] getGalleryImagesList() {
		return galleryImagesList;
	}

	public void setGalleryImagesList(String[] galleryImagesList) {
		this.galleryImagesList = galleryImagesList;
	}

	public void setGalleryImagesList(
		UnsafeSupplier<String[], Exception> galleryImagesListUnsafeSupplier) {

		try {
			galleryImagesList = galleryImagesListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] galleryImagesList;

	public String getGalleryLabel() {
		return galleryLabel;
	}

	public void setGalleryLabel(String galleryLabel) {
		this.galleryLabel = galleryLabel;
	}

	public void setGalleryLabel(
		UnsafeSupplier<String, Exception> galleryLabelUnsafeSupplier) {

		try {
			galleryLabel = galleryLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String galleryLabel;

	public String getHeaderBanner() {
		return headerBanner;
	}

	public void setHeaderBanner(String headerBanner) {
		this.headerBanner = headerBanner;
	}

	public void setHeaderBanner(
		UnsafeSupplier<String, Exception> headerBannerUnsafeSupplier) {

		try {
			headerBanner = headerBannerUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String headerBanner;

	public String getHeaderDesc() {
		return headerDesc;
	}

	public void setHeaderDesc(String headerDesc) {
		this.headerDesc = headerDesc;
	}

	public void setHeaderDesc(
		UnsafeSupplier<String, Exception> headerDescUnsafeSupplier) {

		try {
			headerDesc = headerDescUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String headerDesc;

	public String getHeaderTitle() {
		return headerTitle;
	}

	public void setHeaderTitle(String headerTitle) {
		this.headerTitle = headerTitle;
	}

	public void setHeaderTitle(
		UnsafeSupplier<String, Exception> headerTitleUnsafeSupplier) {

		try {
			headerTitle = headerTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String headerTitle;

	public String getLocationLabel() {
		return locationLabel;
	}

	public void setLocationLabel(String locationLabel) {
		this.locationLabel = locationLabel;
	}

	public void setLocationLabel(
		UnsafeSupplier<String, Exception> locationLabelUnsafeSupplier) {

		try {
			locationLabel = locationLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String locationLabel;

	public String getLocationLatitude() {
		return locationLatitude;
	}

	public void setLocationLatitude(String locationLatitude) {
		this.locationLatitude = locationLatitude;
	}

	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> locationLatitudeUnsafeSupplier) {

		try {
			locationLatitude = locationLatitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String locationLatitude;

	public String getLocationLongitude() {
		return locationLongitude;
	}

	public void setLocationLongitude(String locationLongitude) {
		this.locationLongitude = locationLongitude;
	}

	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> locationLongitudeUnsafeSupplier) {

		try {
			locationLongitude = locationLongitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String locationLongitude;

	@Override
	public Explore clone() throws CloneNotSupportedException {
		return (Explore)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Explore)) {
			return false;
		}

		Explore explore = (Explore)object;

		return Objects.equals(toString(), explore.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ExploreSerDes.toJSON(this);
	}

}