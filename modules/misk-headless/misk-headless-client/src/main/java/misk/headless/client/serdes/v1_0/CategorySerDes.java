package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Category;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CategorySerDes {

	public static Category toDTO(String json) {
		CategoryJSONParser categoryJSONParser = new CategoryJSONParser();

		return categoryJSONParser.parseToDTO(json);
	}

	public static Category[] toDTOs(String json) {
		CategoryJSONParser categoryJSONParser = new CategoryJSONParser();

		return categoryJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Category category) {
		if (category == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (category.getItemCategory() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemCategory\": ");

			sb.append("\"");

			sb.append(_escape(category.getItemCategory()));

			sb.append("\"");
		}

		if (category.getItemImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemImage\": ");

			sb.append("\"");

			sb.append(_escape(category.getItemImage()));

			sb.append("\"");
		}

		if (category.getItemPrice() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemPrice\": ");

			sb.append("\"");

			sb.append(_escape(category.getItemPrice()));

			sb.append("\"");
		}

		if (category.getItemSummary() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemSummary\": ");

			sb.append("\"");

			sb.append(_escape(category.getItemSummary()));

			sb.append("\"");
		}

		if (category.getItemTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"itemTitle\": ");

			sb.append("\"");

			sb.append(_escape(category.getItemTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		CategoryJSONParser categoryJSONParser = new CategoryJSONParser();

		return categoryJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Category category) {
		if (category == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (category.getItemCategory() == null) {
			map.put("itemCategory", null);
		}
		else {
			map.put("itemCategory", String.valueOf(category.getItemCategory()));
		}

		if (category.getItemImage() == null) {
			map.put("itemImage", null);
		}
		else {
			map.put("itemImage", String.valueOf(category.getItemImage()));
		}

		if (category.getItemPrice() == null) {
			map.put("itemPrice", null);
		}
		else {
			map.put("itemPrice", String.valueOf(category.getItemPrice()));
		}

		if (category.getItemSummary() == null) {
			map.put("itemSummary", null);
		}
		else {
			map.put("itemSummary", String.valueOf(category.getItemSummary()));
		}

		if (category.getItemTitle() == null) {
			map.put("itemTitle", null);
		}
		else {
			map.put("itemTitle", String.valueOf(category.getItemTitle()));
		}

		return map;
	}

	public static class CategoryJSONParser extends BaseJSONParser<Category> {

		@Override
		protected Category createDTO() {
			return new Category();
		}

		@Override
		protected Category[] createDTOArray(int size) {
			return new Category[size];
		}

		@Override
		protected void setField(
			Category category, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "itemCategory")) {
				if (jsonParserFieldValue != null) {
					category.setItemCategory((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "itemImage")) {
				if (jsonParserFieldValue != null) {
					category.setItemImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "itemPrice")) {
				if (jsonParserFieldValue != null) {
					category.setItemPrice((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "itemSummary")) {
				if (jsonParserFieldValue != null) {
					category.setItemSummary((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "itemTitle")) {
				if (jsonParserFieldValue != null) {
					category.setItemTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}