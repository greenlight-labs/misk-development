package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AppUser;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AppUserSerDes {

	public static AppUser toDTO(String json) {
		AppUserJSONParser appUserJSONParser = new AppUserJSONParser();

		return appUserJSONParser.parseToDTO(json);
	}

	public static AppUser[] toDTOs(String json) {
		AppUserJSONParser appUserJSONParser = new AppUserJSONParser();

		return appUserJSONParser.parseToDTOs(json);
	}

	public static String toJSON(AppUser appUser) {
		if (appUser == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (appUser.getAndroidDeviceToken() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"androidDeviceToken\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getAndroidDeviceToken()));

			sb.append("\"");
		}

		if (appUser.getAppUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(appUser.getAppUserId());
		}

		if (appUser.getAppleUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appleUserId\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getAppleUserId()));

			sb.append("\"");
		}

		if (appUser.getAutoLogin() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"autoLogin\": ");

			sb.append(appUser.getAutoLogin());
		}

		if (appUser.getConfirmPassword() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"confirmPassword\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getConfirmPassword()));

			sb.append("\"");
		}

		if (appUser.getDeactivateReason() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deactivateReason\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getDeactivateReason()));

			sb.append("\"");
		}

		if (appUser.getDeleteReason() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deleteReason\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getDeleteReason()));

			sb.append("\"");
		}

		if (appUser.getEmailAddress() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"emailAddress\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getEmailAddress()));

			sb.append("\"");
		}

		if (appUser.getEmailAddressVerified() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"emailAddressVerified\": ");

			sb.append(appUser.getEmailAddressVerified());
		}

		if (appUser.getFacebookId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"facebookId\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getFacebookId()));

			sb.append("\"");
		}

		if (appUser.getFullName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"fullName\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getFullName()));

			sb.append("\"");
		}

		if (appUser.getGcpToken() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"gcpToken\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getGcpToken()));

			sb.append("\"");
		}

		if (appUser.getGoogleUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"googleUserId\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getGoogleUserId()));

			sb.append("\"");
		}

		if (appUser.getIosDeviceToken() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"iosDeviceToken\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getIosDeviceToken()));

			sb.append("\"");
		}

		if (appUser.getIsVerified() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"isVerified\": ");

			sb.append(appUser.getIsVerified());
		}

		if (appUser.getLanguageId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"languageId\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getLanguageId()));

			sb.append("\"");
		}

		if (appUser.getNewPassword() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"newPassword\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getNewPassword()));

			sb.append("\"");
		}

		if (appUser.getPasscode() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"passcode\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getPasscode()));

			sb.append("\"");
		}

		if (appUser.getPassword() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"password\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getPassword()));

			sb.append("\"");
		}

		if (appUser.getPasswordResetToken() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"passwordResetToken\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getPasswordResetToken()));

			sb.append("\"");
		}

		if (appUser.getPhoneNumber() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"phoneNumber\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getPhoneNumber()));

			sb.append("\"");
		}

		if (appUser.getPhoneNumberVerified() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"phoneNumberVerified\": ");

			sb.append(appUser.getPhoneNumberVerified());
		}

		if (appUser.getProfileBannerImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"profileBannerImage\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getProfileBannerImage()));

			sb.append("\"");
		}

		if (appUser.getProfileImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"profileImage\": ");

			sb.append("\"");

			sb.append(_escape(appUser.getProfileImage()));

			sb.append("\"");
		}

		if (appUser.getRequestStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"requestStatus\": ");

			sb.append(String.valueOf(appUser.getRequestStatus()));
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		AppUserJSONParser appUserJSONParser = new AppUserJSONParser();

		return appUserJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(AppUser appUser) {
		if (appUser == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (appUser.getAndroidDeviceToken() == null) {
			map.put("androidDeviceToken", null);
		}
		else {
			map.put(
				"androidDeviceToken",
				String.valueOf(appUser.getAndroidDeviceToken()));
		}

		if (appUser.getAppUserId() == null) {
			map.put("appUserId", null);
		}
		else {
			map.put("appUserId", String.valueOf(appUser.getAppUserId()));
		}

		if (appUser.getAppleUserId() == null) {
			map.put("appleUserId", null);
		}
		else {
			map.put("appleUserId", String.valueOf(appUser.getAppleUserId()));
		}

		if (appUser.getAutoLogin() == null) {
			map.put("autoLogin", null);
		}
		else {
			map.put("autoLogin", String.valueOf(appUser.getAutoLogin()));
		}

		if (appUser.getConfirmPassword() == null) {
			map.put("confirmPassword", null);
		}
		else {
			map.put(
				"confirmPassword",
				String.valueOf(appUser.getConfirmPassword()));
		}

		if (appUser.getDeactivateReason() == null) {
			map.put("deactivateReason", null);
		}
		else {
			map.put(
				"deactivateReason",
				String.valueOf(appUser.getDeactivateReason()));
		}

		if (appUser.getDeleteReason() == null) {
			map.put("deleteReason", null);
		}
		else {
			map.put("deleteReason", String.valueOf(appUser.getDeleteReason()));
		}

		if (appUser.getEmailAddress() == null) {
			map.put("emailAddress", null);
		}
		else {
			map.put("emailAddress", String.valueOf(appUser.getEmailAddress()));
		}

		if (appUser.getEmailAddressVerified() == null) {
			map.put("emailAddressVerified", null);
		}
		else {
			map.put(
				"emailAddressVerified",
				String.valueOf(appUser.getEmailAddressVerified()));
		}

		if (appUser.getFacebookId() == null) {
			map.put("facebookId", null);
		}
		else {
			map.put("facebookId", String.valueOf(appUser.getFacebookId()));
		}

		if (appUser.getFullName() == null) {
			map.put("fullName", null);
		}
		else {
			map.put("fullName", String.valueOf(appUser.getFullName()));
		}

		if (appUser.getGcpToken() == null) {
			map.put("gcpToken", null);
		}
		else {
			map.put("gcpToken", String.valueOf(appUser.getGcpToken()));
		}

		if (appUser.getGoogleUserId() == null) {
			map.put("googleUserId", null);
		}
		else {
			map.put("googleUserId", String.valueOf(appUser.getGoogleUserId()));
		}

		if (appUser.getIosDeviceToken() == null) {
			map.put("iosDeviceToken", null);
		}
		else {
			map.put(
				"iosDeviceToken", String.valueOf(appUser.getIosDeviceToken()));
		}

		if (appUser.getIsVerified() == null) {
			map.put("isVerified", null);
		}
		else {
			map.put("isVerified", String.valueOf(appUser.getIsVerified()));
		}

		if (appUser.getLanguageId() == null) {
			map.put("languageId", null);
		}
		else {
			map.put("languageId", String.valueOf(appUser.getLanguageId()));
		}

		if (appUser.getNewPassword() == null) {
			map.put("newPassword", null);
		}
		else {
			map.put("newPassword", String.valueOf(appUser.getNewPassword()));
		}

		if (appUser.getPasscode() == null) {
			map.put("passcode", null);
		}
		else {
			map.put("passcode", String.valueOf(appUser.getPasscode()));
		}

		if (appUser.getPassword() == null) {
			map.put("password", null);
		}
		else {
			map.put("password", String.valueOf(appUser.getPassword()));
		}

		if (appUser.getPasswordResetToken() == null) {
			map.put("passwordResetToken", null);
		}
		else {
			map.put(
				"passwordResetToken",
				String.valueOf(appUser.getPasswordResetToken()));
		}

		if (appUser.getPhoneNumber() == null) {
			map.put("phoneNumber", null);
		}
		else {
			map.put("phoneNumber", String.valueOf(appUser.getPhoneNumber()));
		}

		if (appUser.getPhoneNumberVerified() == null) {
			map.put("phoneNumberVerified", null);
		}
		else {
			map.put(
				"phoneNumberVerified",
				String.valueOf(appUser.getPhoneNumberVerified()));
		}

		if (appUser.getProfileBannerImage() == null) {
			map.put("profileBannerImage", null);
		}
		else {
			map.put(
				"profileBannerImage",
				String.valueOf(appUser.getProfileBannerImage()));
		}

		if (appUser.getProfileImage() == null) {
			map.put("profileImage", null);
		}
		else {
			map.put("profileImage", String.valueOf(appUser.getProfileImage()));
		}

		if (appUser.getRequestStatus() == null) {
			map.put("requestStatus", null);
		}
		else {
			map.put(
				"requestStatus", String.valueOf(appUser.getRequestStatus()));
		}

		return map;
	}

	public static class AppUserJSONParser extends BaseJSONParser<AppUser> {

		@Override
		protected AppUser createDTO() {
			return new AppUser();
		}

		@Override
		protected AppUser[] createDTOArray(int size) {
			return new AppUser[size];
		}

		@Override
		protected void setField(
			AppUser appUser, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "androidDeviceToken")) {
				if (jsonParserFieldValue != null) {
					appUser.setAndroidDeviceToken((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "appUserId")) {
				if (jsonParserFieldValue != null) {
					appUser.setAppUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "appleUserId")) {
				if (jsonParserFieldValue != null) {
					appUser.setAppleUserId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "autoLogin")) {
				if (jsonParserFieldValue != null) {
					appUser.setAutoLogin((Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "confirmPassword")) {
				if (jsonParserFieldValue != null) {
					appUser.setConfirmPassword((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "deactivateReason")) {
				if (jsonParserFieldValue != null) {
					appUser.setDeactivateReason((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "deleteReason")) {
				if (jsonParserFieldValue != null) {
					appUser.setDeleteReason((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "emailAddress")) {
				if (jsonParserFieldValue != null) {
					appUser.setEmailAddress((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "emailAddressVerified")) {

				if (jsonParserFieldValue != null) {
					appUser.setEmailAddressVerified(
						(Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "facebookId")) {
				if (jsonParserFieldValue != null) {
					appUser.setFacebookId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "fullName")) {
				if (jsonParserFieldValue != null) {
					appUser.setFullName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "gcpToken")) {
				if (jsonParserFieldValue != null) {
					appUser.setGcpToken((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "googleUserId")) {
				if (jsonParserFieldValue != null) {
					appUser.setGoogleUserId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "iosDeviceToken")) {
				if (jsonParserFieldValue != null) {
					appUser.setIosDeviceToken((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "isVerified")) {
				if (jsonParserFieldValue != null) {
					appUser.setIsVerified((Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "languageId")) {
				if (jsonParserFieldValue != null) {
					appUser.setLanguageId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "newPassword")) {
				if (jsonParserFieldValue != null) {
					appUser.setNewPassword((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "passcode")) {
				if (jsonParserFieldValue != null) {
					appUser.setPasscode((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "password")) {
				if (jsonParserFieldValue != null) {
					appUser.setPassword((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "passwordResetToken")) {

				if (jsonParserFieldValue != null) {
					appUser.setPasswordResetToken((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "phoneNumber")) {
				if (jsonParserFieldValue != null) {
					appUser.setPhoneNumber((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "phoneNumberVerified")) {

				if (jsonParserFieldValue != null) {
					appUser.setPhoneNumberVerified(
						(Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "profileBannerImage")) {

				if (jsonParserFieldValue != null) {
					appUser.setProfileBannerImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "profileImage")) {
				if (jsonParserFieldValue != null) {
					appUser.setProfileImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "requestStatus")) {
				if (jsonParserFieldValue != null) {
					appUser.setRequestStatus(
						RequestStatusSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}