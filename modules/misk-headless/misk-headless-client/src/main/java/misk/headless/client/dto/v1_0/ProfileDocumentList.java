package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ProfileDocumentListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ProfileDocumentList implements Cloneable, Serializable {

	public static ProfileDocumentList toDTO(String json) {
		return ProfileDocumentListSerDes.toDTO(json);
	}

	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long appUserId;

	public String[] getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(String[] uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public void setUploadedFile(
		UnsafeSupplier<String[], Exception> uploadedFileUnsafeSupplier) {

		try {
			uploadedFile = uploadedFileUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] uploadedFile;

	@Override
	public ProfileDocumentList clone() throws CloneNotSupportedException {
		return (ProfileDocumentList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ProfileDocumentList)) {
			return false;
		}

		ProfileDocumentList profileDocumentList = (ProfileDocumentList)object;

		return Objects.equals(toString(), profileDocumentList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ProfileDocumentListSerDes.toJSON(this);
	}

}