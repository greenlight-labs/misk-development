package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.UnreadNotificationCountSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class UnreadNotificationCount implements Cloneable, Serializable {

	public static UnreadNotificationCount toDTO(String json) {
		return UnreadNotificationCountSerDes.toDTO(json);
	}

	public Integer getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(Integer unreadCount) {
		this.unreadCount = unreadCount;
	}

	public void setUnreadCount(
		UnsafeSupplier<Integer, Exception> unreadCountUnsafeSupplier) {

		try {
			unreadCount = unreadCountUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Integer unreadCount;

	@Override
	public UnreadNotificationCount clone() throws CloneNotSupportedException {
		return (UnreadNotificationCount)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof UnreadNotificationCount)) {
			return false;
		}

		UnreadNotificationCount unreadNotificationCount =
			(UnreadNotificationCount)object;

		return Objects.equals(toString(), unreadNotificationCount.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return UnreadNotificationCountSerDes.toJSON(this);
	}

}