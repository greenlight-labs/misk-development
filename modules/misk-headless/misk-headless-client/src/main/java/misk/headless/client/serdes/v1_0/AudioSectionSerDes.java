package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AudioSection;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AudioSectionSerDes {

	public static AudioSection toDTO(String json) {
		AudioSectionJSONParser audioSectionJSONParser =
			new AudioSectionJSONParser();

		return audioSectionJSONParser.parseToDTO(json);
	}

	public static AudioSection[] toDTOs(String json) {
		AudioSectionJSONParser audioSectionJSONParser =
			new AudioSectionJSONParser();

		return audioSectionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(AudioSection audioSection) {
		if (audioSection == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (audioSection.getAudio() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"audio\": ");

			sb.append("\"");

			sb.append(_escape(audioSection.getAudio()));

			sb.append("\"");
		}

		if (audioSection.getCatColor() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catColor\": ");

			sb.append("\"");

			sb.append(_escape(audioSection.getCatColor()));

			sb.append("\"");
		}

		if (audioSection.getCatName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catName\": ");

			sb.append("\"");

			sb.append(_escape(audioSection.getCatName()));

			sb.append("\"");
		}

		if (audioSection.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(audioSection.getImage()));

			sb.append("\"");
		}

		if (audioSection.getLocation() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"location\": ");

			sb.append("\"");

			sb.append(_escape(audioSection.getLocation()));

			sb.append("\"");
		}

		if (audioSection.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(audioSection.getName()));

			sb.append("\"");
		}

		if (audioSection.getProfession() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"profession\": ");

			sb.append("\"");

			sb.append(_escape(audioSection.getProfession()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		AudioSectionJSONParser audioSectionJSONParser =
			new AudioSectionJSONParser();

		return audioSectionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(AudioSection audioSection) {
		if (audioSection == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (audioSection.getAudio() == null) {
			map.put("audio", null);
		}
		else {
			map.put("audio", String.valueOf(audioSection.getAudio()));
		}

		if (audioSection.getCatColor() == null) {
			map.put("catColor", null);
		}
		else {
			map.put("catColor", String.valueOf(audioSection.getCatColor()));
		}

		if (audioSection.getCatName() == null) {
			map.put("catName", null);
		}
		else {
			map.put("catName", String.valueOf(audioSection.getCatName()));
		}

		if (audioSection.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(audioSection.getImage()));
		}

		if (audioSection.getLocation() == null) {
			map.put("location", null);
		}
		else {
			map.put("location", String.valueOf(audioSection.getLocation()));
		}

		if (audioSection.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put("name", String.valueOf(audioSection.getName()));
		}

		if (audioSection.getProfession() == null) {
			map.put("profession", null);
		}
		else {
			map.put("profession", String.valueOf(audioSection.getProfession()));
		}

		return map;
	}

	public static class AudioSectionJSONParser
		extends BaseJSONParser<AudioSection> {

		@Override
		protected AudioSection createDTO() {
			return new AudioSection();
		}

		@Override
		protected AudioSection[] createDTOArray(int size) {
			return new AudioSection[size];
		}

		@Override
		protected void setField(
			AudioSection audioSection, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "audio")) {
				if (jsonParserFieldValue != null) {
					audioSection.setAudio((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "catColor")) {
				if (jsonParserFieldValue != null) {
					audioSection.setCatColor((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "catName")) {
				if (jsonParserFieldValue != null) {
					audioSection.setCatName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					audioSection.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "location")) {
				if (jsonParserFieldValue != null) {
					audioSection.setLocation((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					audioSection.setName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "profession")) {
				if (jsonParserFieldValue != null) {
					audioSection.setProfession((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}