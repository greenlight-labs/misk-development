package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Court;
import misk.headless.client.dto.v1_0.RequestStatus;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.BookingSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Booking implements Cloneable, Serializable {

	public static Booking toDTO(String json) {
		return BookingSerDes.toDTO(json);
	}

	public String getAppUserEmail() {
		return appUserEmail;
	}

	public void setAppUserEmail(String appUserEmail) {
		this.appUserEmail = appUserEmail;
	}

	public void setAppUserEmail(
		UnsafeSupplier<String, Exception> appUserEmailUnsafeSupplier) {

		try {
			appUserEmail = appUserEmailUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String appUserEmail;

	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long appUserId;

	public String getAppUserName() {
		return appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public void setAppUserName(
		UnsafeSupplier<String, Exception> appUserNameUnsafeSupplier) {

		try {
			appUserName = appUserNameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String appUserName;

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public void setBookingDate(
		UnsafeSupplier<String, Exception> bookingDateUnsafeSupplier) {

		try {
			bookingDate = bookingDateUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String bookingDate;

	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	public void setBookingId(
		UnsafeSupplier<Long, Exception> bookingIdUnsafeSupplier) {

		try {
			bookingId = bookingIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long bookingId;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setCategory(
		UnsafeSupplier<String, Exception> categoryUnsafeSupplier) {

		try {
			category = categoryUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String category;

	public Court getCourt() {
		return court;
	}

	public void setCourt(Court court) {
		this.court = court;
	}

	public void setCourt(UnsafeSupplier<Court, Exception> courtUnsafeSupplier) {
		try {
			court = courtUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Court court;

	public Long getCourtId() {
		return courtId;
	}

	public void setCourtId(Long courtId) {
		this.courtId = courtId;
	}

	public void setCourtId(
		UnsafeSupplier<Long, Exception> courtIdUnsafeSupplier) {

		try {
			courtId = courtIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long courtId;

	public String getCourtLocation() {
		return courtLocation;
	}

	public void setCourtLocation(String courtLocation) {
		this.courtLocation = courtLocation;
	}

	public void setCourtLocation(
		UnsafeSupplier<String, Exception> courtLocationUnsafeSupplier) {

		try {
			courtLocation = courtLocationUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String courtLocation;

	public String getCourtName() {
		return courtName;
	}

	public void setCourtName(String courtName) {
		this.courtName = courtName;
	}

	public void setCourtName(
		UnsafeSupplier<String, Exception> courtNameUnsafeSupplier) {

		try {
			courtName = courtNameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String courtName;

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public void setRequestStatus(
		UnsafeSupplier<RequestStatus, Exception> requestStatusUnsafeSupplier) {

		try {
			requestStatus = requestStatusUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected RequestStatus requestStatus;

	public String getSlotEndTime() {
		return slotEndTime;
	}

	public void setSlotEndTime(String slotEndTime) {
		this.slotEndTime = slotEndTime;
	}

	public void setSlotEndTime(
		UnsafeSupplier<String, Exception> slotEndTimeUnsafeSupplier) {

		try {
			slotEndTime = slotEndTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String slotEndTime;

	public String getSlotStartTime() {
		return slotStartTime;
	}

	public void setSlotStartTime(String slotStartTime) {
		this.slotStartTime = slotStartTime;
	}

	public void setSlotStartTime(
		UnsafeSupplier<String, Exception> slotStartTimeUnsafeSupplier) {

		try {
			slotStartTime = slotStartTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String slotStartTime;

	@Override
	public Booking clone() throws CloneNotSupportedException {
		return (Booking)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Booking)) {
			return false;
		}

		Booking booking = (Booking)object;

		return Objects.equals(toString(), booking.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return BookingSerDes.toJSON(this);
	}

}