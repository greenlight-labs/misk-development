package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.BannerSection;
import misk.headless.client.dto.v1_0.DiscoverSection;
import misk.headless.client.dto.v1_0.EventsSection;
import misk.headless.client.dto.v1_0.HenaCafeSection;
import misk.headless.client.dto.v1_0.HighlightsSection;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ExperientialCenterSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExperientialCenter implements Cloneable, Serializable {

	public static ExperientialCenter toDTO(String json) {
		return ExperientialCenterSerDes.toDTO(json);
	}

	public BannerSection getBannerSection() {
		return bannerSection;
	}

	public void setBannerSection(BannerSection bannerSection) {
		this.bannerSection = bannerSection;
	}

	public void setBannerSection(
		UnsafeSupplier<BannerSection, Exception> bannerSectionUnsafeSupplier) {

		try {
			bannerSection = bannerSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected BannerSection bannerSection;

	public DiscoverSection getDiscoverSection() {
		return discoverSection;
	}

	public void setDiscoverSection(DiscoverSection discoverSection) {
		this.discoverSection = discoverSection;
	}

	public void setDiscoverSection(
		UnsafeSupplier<DiscoverSection, Exception>
			discoverSectionUnsafeSupplier) {

		try {
			discoverSection = discoverSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected DiscoverSection discoverSection;

	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventsSection eventsSection;

	public HenaCafeSection getHenaCafeSection() {
		return henaCafeSection;
	}

	public void setHenaCafeSection(HenaCafeSection henaCafeSection) {
		this.henaCafeSection = henaCafeSection;
	}

	public void setHenaCafeSection(
		UnsafeSupplier<HenaCafeSection, Exception>
			henaCafeSectionUnsafeSupplier) {

		try {
			henaCafeSection = henaCafeSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected HenaCafeSection henaCafeSection;

	public HighlightsSection getHighlightsSection() {
		return highlightsSection;
	}

	public void setHighlightsSection(HighlightsSection highlightsSection) {
		this.highlightsSection = highlightsSection;
	}

	public void setHighlightsSection(
		UnsafeSupplier<HighlightsSection, Exception>
			highlightsSectionUnsafeSupplier) {

		try {
			highlightsSection = highlightsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected HighlightsSection highlightsSection;

	@Override
	public ExperientialCenter clone() throws CloneNotSupportedException {
		return (ExperientialCenter)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExperientialCenter)) {
			return false;
		}

		ExperientialCenter experientialCenter = (ExperientialCenter)object;

		return Objects.equals(toString(), experientialCenter.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ExperientialCenterSerDes.toJSON(this);
	}

}