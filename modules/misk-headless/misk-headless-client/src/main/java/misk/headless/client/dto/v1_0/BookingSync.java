package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.BookingSyncSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookingSync implements Cloneable, Serializable {

	public static BookingSync toDTO(String json) {
		return BookingSyncSerDes.toDTO(json);
	}

	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	public void setBookingId(
		UnsafeSupplier<Long, Exception> bookingIdUnsafeSupplier) {

		try {
			bookingId = bookingIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long bookingId;

	public Boolean getSynced() {
		return synced;
	}

	public void setSynced(Boolean synced) {
		this.synced = synced;
	}

	public void setSynced(
		UnsafeSupplier<Boolean, Exception> syncedUnsafeSupplier) {

		try {
			synced = syncedUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean synced;

	@Override
	public BookingSync clone() throws CloneNotSupportedException {
		return (BookingSync)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BookingSync)) {
			return false;
		}

		BookingSync bookingSync = (BookingSync)object;

		return Objects.equals(toString(), bookingSync.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return BookingSyncSerDes.toJSON(this);
	}

}