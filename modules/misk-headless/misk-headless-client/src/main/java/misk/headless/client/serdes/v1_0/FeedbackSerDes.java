package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Feedback;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FeedbackSerDes {

	public static Feedback toDTO(String json) {
		FeedbackJSONParser feedbackJSONParser = new FeedbackJSONParser();

		return feedbackJSONParser.parseToDTO(json);
	}

	public static Feedback[] toDTOs(String json) {
		FeedbackJSONParser feedbackJSONParser = new FeedbackJSONParser();

		return feedbackJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Feedback feedback) {
		if (feedback == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (feedback.getEmailAddress() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"emailAddress\": ");

			sb.append("\"");

			sb.append(_escape(feedback.getEmailAddress()));

			sb.append("\"");
		}

		if (feedback.getFeedbackType() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"feedbackType\": ");

			sb.append("\"");

			sb.append(_escape(feedback.getFeedbackType()));

			sb.append("\"");
		}

		if (feedback.getFullName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"fullName\": ");

			sb.append("\"");

			sb.append(_escape(feedback.getFullName()));

			sb.append("\"");
		}

		if (feedback.getLanguageId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"languageId\": ");

			sb.append("\"");

			sb.append(_escape(feedback.getLanguageId()));

			sb.append("\"");
		}

		if (feedback.getMessage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"message\": ");

			sb.append("\"");

			sb.append(_escape(feedback.getMessage()));

			sb.append("\"");
		}

		if (feedback.getRequestStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"requestStatus\": ");

			sb.append(String.valueOf(feedback.getRequestStatus()));
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		FeedbackJSONParser feedbackJSONParser = new FeedbackJSONParser();

		return feedbackJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Feedback feedback) {
		if (feedback == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (feedback.getEmailAddress() == null) {
			map.put("emailAddress", null);
		}
		else {
			map.put("emailAddress", String.valueOf(feedback.getEmailAddress()));
		}

		if (feedback.getFeedbackType() == null) {
			map.put("feedbackType", null);
		}
		else {
			map.put("feedbackType", String.valueOf(feedback.getFeedbackType()));
		}

		if (feedback.getFullName() == null) {
			map.put("fullName", null);
		}
		else {
			map.put("fullName", String.valueOf(feedback.getFullName()));
		}

		if (feedback.getLanguageId() == null) {
			map.put("languageId", null);
		}
		else {
			map.put("languageId", String.valueOf(feedback.getLanguageId()));
		}

		if (feedback.getMessage() == null) {
			map.put("message", null);
		}
		else {
			map.put("message", String.valueOf(feedback.getMessage()));
		}

		if (feedback.getRequestStatus() == null) {
			map.put("requestStatus", null);
		}
		else {
			map.put(
				"requestStatus", String.valueOf(feedback.getRequestStatus()));
		}

		return map;
	}

	public static class FeedbackJSONParser extends BaseJSONParser<Feedback> {

		@Override
		protected Feedback createDTO() {
			return new Feedback();
		}

		@Override
		protected Feedback[] createDTOArray(int size) {
			return new Feedback[size];
		}

		@Override
		protected void setField(
			Feedback feedback, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "emailAddress")) {
				if (jsonParserFieldValue != null) {
					feedback.setEmailAddress((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "feedbackType")) {
				if (jsonParserFieldValue != null) {
					feedback.setFeedbackType((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "fullName")) {
				if (jsonParserFieldValue != null) {
					feedback.setFullName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "languageId")) {
				if (jsonParserFieldValue != null) {
					feedback.setLanguageId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "message")) {
				if (jsonParserFieldValue != null) {
					feedback.setMessage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "requestStatus")) {
				if (jsonParserFieldValue != null) {
					feedback.setRequestStatus(
						RequestStatusSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}