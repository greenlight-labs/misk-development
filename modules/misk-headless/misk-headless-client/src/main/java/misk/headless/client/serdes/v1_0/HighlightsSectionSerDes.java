package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Highlight;
import misk.headless.client.dto.v1_0.HighlightsSection;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HighlightsSectionSerDes {

	public static HighlightsSection toDTO(String json) {
		HighlightsSectionJSONParser highlightsSectionJSONParser =
			new HighlightsSectionJSONParser();

		return highlightsSectionJSONParser.parseToDTO(json);
	}

	public static HighlightsSection[] toDTOs(String json) {
		HighlightsSectionJSONParser highlightsSectionJSONParser =
			new HighlightsSectionJSONParser();

		return highlightsSectionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(HighlightsSection highlightsSection) {
		if (highlightsSection == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (highlightsSection.getItems() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"items\": ");

			sb.append("[");

			for (int i = 0; i < highlightsSection.getItems().length; i++) {
				sb.append(String.valueOf(highlightsSection.getItems()[i]));

				if ((i + 1) < highlightsSection.getItems().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (highlightsSection.getSectionTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"sectionTitle\": ");

			sb.append("\"");

			sb.append(_escape(highlightsSection.getSectionTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		HighlightsSectionJSONParser highlightsSectionJSONParser =
			new HighlightsSectionJSONParser();

		return highlightsSectionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		HighlightsSection highlightsSection) {

		if (highlightsSection == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (highlightsSection.getItems() == null) {
			map.put("items", null);
		}
		else {
			map.put("items", String.valueOf(highlightsSection.getItems()));
		}

		if (highlightsSection.getSectionTitle() == null) {
			map.put("sectionTitle", null);
		}
		else {
			map.put(
				"sectionTitle",
				String.valueOf(highlightsSection.getSectionTitle()));
		}

		return map;
	}

	public static class HighlightsSectionJSONParser
		extends BaseJSONParser<HighlightsSection> {

		@Override
		protected HighlightsSection createDTO() {
			return new HighlightsSection();
		}

		@Override
		protected HighlightsSection[] createDTOArray(int size) {
			return new HighlightsSection[size];
		}

		@Override
		protected void setField(
			HighlightsSection highlightsSection, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "items")) {
				if (jsonParserFieldValue != null) {
					highlightsSection.setItems(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> HighlightSerDes.toDTO((String)object)
						).toArray(
							size -> new Highlight[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "sectionTitle")) {
				if (jsonParserFieldValue != null) {
					highlightsSection.setSectionTitle(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}