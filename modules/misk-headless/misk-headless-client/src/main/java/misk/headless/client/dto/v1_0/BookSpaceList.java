package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.BookSpaceListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookSpaceList implements Cloneable, Serializable {

	public static BookSpaceList toDTO(String json) {
		return BookSpaceListSerDes.toDTO(json);
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String Area) {
		this.Area = Area;
	}

	public void setArea(UnsafeSupplier<String, Exception> AreaUnsafeSupplier) {
		try {
			Area = AreaUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String Area;

	public String getBookSpaceIcon() {
		return BookSpaceIcon;
	}

	public void setBookSpaceIcon(String BookSpaceIcon) {
		this.BookSpaceIcon = BookSpaceIcon;
	}

	public void setBookSpaceIcon(
		UnsafeSupplier<String, Exception> BookSpaceIconUnsafeSupplier) {

		try {
			BookSpaceIcon = BookSpaceIconUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String BookSpaceIcon;

	public String getBookSpaceImages() {
		return BookSpaceImages;
	}

	public void setBookSpaceImages(String BookSpaceImages) {
		this.BookSpaceImages = BookSpaceImages;
	}

	public void setBookSpaceImages(
		UnsafeSupplier<String, Exception> BookSpaceImagesUnsafeSupplier) {

		try {
			BookSpaceImages = BookSpaceImagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String BookSpaceImages;

	public String getBookSpaceTitle() {
		return BookSpaceTitle;
	}

	public void setBookSpaceTitle(String BookSpaceTitle) {
		this.BookSpaceTitle = BookSpaceTitle;
	}

	public void setBookSpaceTitle(
		UnsafeSupplier<String, Exception> BookSpaceTitleUnsafeSupplier) {

		try {
			BookSpaceTitle = BookSpaceTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String BookSpaceTitle;

	public String getCapacity() {
		return Capacity;
	}

	public void setCapacity(String Capacity) {
		this.Capacity = Capacity;
	}

	public void setCapacity(
		UnsafeSupplier<String, Exception> CapacityUnsafeSupplier) {

		try {
			Capacity = CapacityUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String Capacity;

	public String getDuration() {
		return Duration;
	}

	public void setDuration(String Duration) {
		this.Duration = Duration;
	}

	public void setDuration(
		UnsafeSupplier<String, Exception> DurationUnsafeSupplier) {

		try {
			Duration = DurationUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String Duration;

	@Override
	public BookSpaceList clone() throws CloneNotSupportedException {
		return (BookSpaceList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BookSpaceList)) {
			return false;
		}

		BookSpaceList bookSpaceList = (BookSpaceList)object;

		return Objects.equals(toString(), bookSpaceList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return BookSpaceListSerDes.toJSON(this);
	}

}