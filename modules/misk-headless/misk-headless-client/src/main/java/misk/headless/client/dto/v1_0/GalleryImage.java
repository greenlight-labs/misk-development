package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.GalleryImageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class GalleryImage implements Cloneable, Serializable {

	public static GalleryImage toDTO(String json) {
		return GalleryImageSerDes.toDTO(json);
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setImage(
		UnsafeSupplier<String, Exception> imageUnsafeSupplier) {

		try {
			image = imageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String image;

	@Override
	public GalleryImage clone() throws CloneNotSupportedException {
		return (GalleryImage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof GalleryImage)) {
			return false;
		}

		GalleryImage galleryImage = (GalleryImage)object;

		return Objects.equals(toString(), galleryImage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return GalleryImageSerDes.toJSON(this);
	}

}