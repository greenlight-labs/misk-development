package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Court;
import misk.headless.client.dto.v1_0.CourtCategory;
import misk.headless.client.dto.v1_0.CourtItems;
import misk.headless.client.dto.v1_0.CourtLocation;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CourtItemsSerDes {

	public static CourtItems toDTO(String json) {
		CourtItemsJSONParser courtItemsJSONParser = new CourtItemsJSONParser();

		return courtItemsJSONParser.parseToDTO(json);
	}

	public static CourtItems[] toDTOs(String json) {
		CourtItemsJSONParser courtItemsJSONParser = new CourtItemsJSONParser();

		return courtItemsJSONParser.parseToDTOs(json);
	}

	public static String toJSON(CourtItems courtItems) {
		if (courtItems == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (courtItems.getCourtCategories() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtCategories\": ");

			sb.append("[");

			for (int i = 0; i < courtItems.getCourtCategories().length; i++) {
				sb.append(String.valueOf(courtItems.getCourtCategories()[i]));

				if ((i + 1) < courtItems.getCourtCategories().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (courtItems.getCourtLocations() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtLocations\": ");

			sb.append("[");

			for (int i = 0; i < courtItems.getCourtLocations().length; i++) {
				sb.append(String.valueOf(courtItems.getCourtLocations()[i]));

				if ((i + 1) < courtItems.getCourtLocations().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (courtItems.getCourts() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courts\": ");

			sb.append("[");

			for (int i = 0; i < courtItems.getCourts().length; i++) {
				sb.append(String.valueOf(courtItems.getCourts()[i]));

				if ((i + 1) < courtItems.getCourts().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		CourtItemsJSONParser courtItemsJSONParser = new CourtItemsJSONParser();

		return courtItemsJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(CourtItems courtItems) {
		if (courtItems == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (courtItems.getCourtCategories() == null) {
			map.put("courtCategories", null);
		}
		else {
			map.put(
				"courtCategories",
				String.valueOf(courtItems.getCourtCategories()));
		}

		if (courtItems.getCourtLocations() == null) {
			map.put("courtLocations", null);
		}
		else {
			map.put(
				"courtLocations",
				String.valueOf(courtItems.getCourtLocations()));
		}

		if (courtItems.getCourts() == null) {
			map.put("courts", null);
		}
		else {
			map.put("courts", String.valueOf(courtItems.getCourts()));
		}

		return map;
	}

	public static class CourtItemsJSONParser
		extends BaseJSONParser<CourtItems> {

		@Override
		protected CourtItems createDTO() {
			return new CourtItems();
		}

		@Override
		protected CourtItems[] createDTOArray(int size) {
			return new CourtItems[size];
		}

		@Override
		protected void setField(
			CourtItems courtItems, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "courtCategories")) {
				if (jsonParserFieldValue != null) {
					courtItems.setCourtCategories(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> CourtCategorySerDes.toDTO((String)object)
						).toArray(
							size -> new CourtCategory[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courtLocations")) {
				if (jsonParserFieldValue != null) {
					courtItems.setCourtLocations(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> CourtLocationSerDes.toDTO((String)object)
						).toArray(
							size -> new CourtLocation[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courts")) {
				if (jsonParserFieldValue != null) {
					courtItems.setCourts(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> CourtSerDes.toDTO((String)object)
						).toArray(
							size -> new Court[size]
						));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}