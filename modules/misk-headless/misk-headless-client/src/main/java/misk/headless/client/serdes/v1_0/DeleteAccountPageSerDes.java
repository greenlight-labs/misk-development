package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.DeleteAccountPage;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DeleteAccountPageSerDes {

	public static DeleteAccountPage toDTO(String json) {
		DeleteAccountPageJSONParser deleteAccountPageJSONParser =
			new DeleteAccountPageJSONParser();

		return deleteAccountPageJSONParser.parseToDTO(json);
	}

	public static DeleteAccountPage[] toDTOs(String json) {
		DeleteAccountPageJSONParser deleteAccountPageJSONParser =
			new DeleteAccountPageJSONParser();

		return deleteAccountPageJSONParser.parseToDTOs(json);
	}

	public static String toJSON(DeleteAccountPage deleteAccountPage) {
		if (deleteAccountPage == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (deleteAccountPage.getAnswers() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"answers\": ");

			sb.append("[");

			for (int i = 0; i < deleteAccountPage.getAnswers().length; i++) {
				sb.append("\"");

				sb.append(_escape(deleteAccountPage.getAnswers()[i]));

				sb.append("\"");

				if ((i + 1) < deleteAccountPage.getAnswers().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (deleteAccountPage.getDeleteAccountLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deleteAccountLabel\": ");

			sb.append("\"");

			sb.append(_escape(deleteAccountPage.getDeleteAccountLabel()));

			sb.append("\"");
		}

		if (deleteAccountPage.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(deleteAccountPage.getDescription()));

			sb.append("\"");
		}

		if (deleteAccountPage.getQuestion() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"question\": ");

			sb.append("\"");

			sb.append(_escape(deleteAccountPage.getQuestion()));

			sb.append("\"");
		}

		if (deleteAccountPage.getQuestionExcerpt() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"questionExcerpt\": ");

			sb.append("\"");

			sb.append(_escape(deleteAccountPage.getQuestionExcerpt()));

			sb.append("\"");
		}

		if (deleteAccountPage.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(deleteAccountPage.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		DeleteAccountPageJSONParser deleteAccountPageJSONParser =
			new DeleteAccountPageJSONParser();

		return deleteAccountPageJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		DeleteAccountPage deleteAccountPage) {

		if (deleteAccountPage == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (deleteAccountPage.getAnswers() == null) {
			map.put("answers", null);
		}
		else {
			map.put("answers", String.valueOf(deleteAccountPage.getAnswers()));
		}

		if (deleteAccountPage.getDeleteAccountLabel() == null) {
			map.put("deleteAccountLabel", null);
		}
		else {
			map.put(
				"deleteAccountLabel",
				String.valueOf(deleteAccountPage.getDeleteAccountLabel()));
		}

		if (deleteAccountPage.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(deleteAccountPage.getDescription()));
		}

		if (deleteAccountPage.getQuestion() == null) {
			map.put("question", null);
		}
		else {
			map.put(
				"question", String.valueOf(deleteAccountPage.getQuestion()));
		}

		if (deleteAccountPage.getQuestionExcerpt() == null) {
			map.put("questionExcerpt", null);
		}
		else {
			map.put(
				"questionExcerpt",
				String.valueOf(deleteAccountPage.getQuestionExcerpt()));
		}

		if (deleteAccountPage.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(deleteAccountPage.getTitle()));
		}

		return map;
	}

	public static class DeleteAccountPageJSONParser
		extends BaseJSONParser<DeleteAccountPage> {

		@Override
		protected DeleteAccountPage createDTO() {
			return new DeleteAccountPage();
		}

		@Override
		protected DeleteAccountPage[] createDTOArray(int size) {
			return new DeleteAccountPage[size];
		}

		@Override
		protected void setField(
			DeleteAccountPage deleteAccountPage, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "answers")) {
				if (jsonParserFieldValue != null) {
					deleteAccountPage.setAnswers(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "deleteAccountLabel")) {

				if (jsonParserFieldValue != null) {
					deleteAccountPage.setDeleteAccountLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					deleteAccountPage.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "question")) {
				if (jsonParserFieldValue != null) {
					deleteAccountPage.setQuestion((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "questionExcerpt")) {
				if (jsonParserFieldValue != null) {
					deleteAccountPage.setQuestionExcerpt(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					deleteAccountPage.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}