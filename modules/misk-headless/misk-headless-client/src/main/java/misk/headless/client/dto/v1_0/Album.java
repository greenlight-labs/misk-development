package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Gallery;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.AlbumSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Album implements Cloneable, Serializable {

	public static Album toDTO(String json) {
		return AlbumSerDes.toDTO(json);
	}

	public Long getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Long albumId) {
		this.albumId = albumId;
	}

	public void setAlbumId(
		UnsafeSupplier<Long, Exception> albumIdUnsafeSupplier) {

		try {
			albumId = albumIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long albumId;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setCategoryName(
		UnsafeSupplier<String, Exception> categoryNameUnsafeSupplier) {

		try {
			categoryName = categoryNameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String categoryName;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public Gallery[] getGalleries() {
		return galleries;
	}

	public void setGalleries(Gallery[] galleries) {
		this.galleries = galleries;
	}

	public void setGalleries(
		UnsafeSupplier<Gallery[], Exception> galleriesUnsafeSupplier) {

		try {
			galleries = galleriesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Gallery[] galleries;

	public String getListingImage() {
		return listingImage;
	}

	public void setListingImage(String listingImage) {
		this.listingImage = listingImage;
	}

	public void setListingImage(
		UnsafeSupplier<String, Exception> listingImageUnsafeSupplier) {

		try {
			listingImage = listingImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String listingImage;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	@Override
	public Album clone() throws CloneNotSupportedException {
		return (Album)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Album)) {
			return false;
		}

		Album album = (Album)object;

		return Objects.equals(toString(), album.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return AlbumSerDes.toJSON(this);
	}

}