package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.DeactivateAccountPageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DeactivateAccountPage implements Cloneable, Serializable {

	public static DeactivateAccountPage toDTO(String json) {
		return DeactivateAccountPageSerDes.toDTO(json);
	}

	public String[] getAnswers() {
		return answers;
	}

	public void setAnswers(String[] answers) {
		this.answers = answers;
	}

	public void setAnswers(
		UnsafeSupplier<String[], Exception> answersUnsafeSupplier) {

		try {
			answers = answersUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] answers;

	public String getDeactivateAccountLabel() {
		return deactivateAccountLabel;
	}

	public void setDeactivateAccountLabel(String deactivateAccountLabel) {
		this.deactivateAccountLabel = deactivateAccountLabel;
	}

	public void setDeactivateAccountLabel(
		UnsafeSupplier<String, Exception>
			deactivateAccountLabelUnsafeSupplier) {

		try {
			deactivateAccountLabel = deactivateAccountLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String deactivateAccountLabel;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public void setQuestion(
		UnsafeSupplier<String, Exception> questionUnsafeSupplier) {

		try {
			question = questionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String question;

	public String getQuestionExcerpt() {
		return questionExcerpt;
	}

	public void setQuestionExcerpt(String questionExcerpt) {
		this.questionExcerpt = questionExcerpt;
	}

	public void setQuestionExcerpt(
		UnsafeSupplier<String, Exception> questionExcerptUnsafeSupplier) {

		try {
			questionExcerpt = questionExcerptUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String questionExcerpt;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	@Override
	public DeactivateAccountPage clone() throws CloneNotSupportedException {
		return (DeactivateAccountPage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DeactivateAccountPage)) {
			return false;
		}

		DeactivateAccountPage deactivateAccountPage =
			(DeactivateAccountPage)object;

		return Objects.equals(toString(), deactivateAccountPage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return DeactivateAccountPageSerDes.toJSON(this);
	}

}