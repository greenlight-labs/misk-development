package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Album;
import misk.headless.client.dto.v1_0.Court;
import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.dto.v1_0.GlobalSearch;
import misk.headless.client.dto.v1_0.Highlight;
import misk.headless.client.dto.v1_0.Story;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class GlobalSearchSerDes {

	public static GlobalSearch toDTO(String json) {
		GlobalSearchJSONParser globalSearchJSONParser =
			new GlobalSearchJSONParser();

		return globalSearchJSONParser.parseToDTO(json);
	}

	public static GlobalSearch[] toDTOs(String json) {
		GlobalSearchJSONParser globalSearchJSONParser =
			new GlobalSearchJSONParser();

		return globalSearchJSONParser.parseToDTOs(json);
	}

	public static String toJSON(GlobalSearch globalSearch) {
		if (globalSearch == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (globalSearch.getAlbums() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"albums\": ");

			sb.append("[");

			for (int i = 0; i < globalSearch.getAlbums().length; i++) {
				sb.append(String.valueOf(globalSearch.getAlbums()[i]));

				if ((i + 1) < globalSearch.getAlbums().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (globalSearch.getCourts() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courts\": ");

			sb.append("[");

			for (int i = 0; i < globalSearch.getCourts().length; i++) {
				sb.append(String.valueOf(globalSearch.getCourts()[i]));

				if ((i + 1) < globalSearch.getCourts().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (globalSearch.getEventLists() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventLists\": ");

			sb.append("[");

			for (int i = 0; i < globalSearch.getEventLists().length; i++) {
				sb.append(String.valueOf(globalSearch.getEventLists()[i]));

				if ((i + 1) < globalSearch.getEventLists().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (globalSearch.getHighlights() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlights\": ");

			sb.append("[");

			for (int i = 0; i < globalSearch.getHighlights().length; i++) {
				sb.append(String.valueOf(globalSearch.getHighlights()[i]));

				if ((i + 1) < globalSearch.getHighlights().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (globalSearch.getStories() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"stories\": ");

			sb.append("[");

			for (int i = 0; i < globalSearch.getStories().length; i++) {
				sb.append(String.valueOf(globalSearch.getStories()[i]));

				if ((i + 1) < globalSearch.getStories().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		GlobalSearchJSONParser globalSearchJSONParser =
			new GlobalSearchJSONParser();

		return globalSearchJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(GlobalSearch globalSearch) {
		if (globalSearch == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (globalSearch.getAlbums() == null) {
			map.put("albums", null);
		}
		else {
			map.put("albums", String.valueOf(globalSearch.getAlbums()));
		}

		if (globalSearch.getCourts() == null) {
			map.put("courts", null);
		}
		else {
			map.put("courts", String.valueOf(globalSearch.getCourts()));
		}

		if (globalSearch.getEventLists() == null) {
			map.put("eventLists", null);
		}
		else {
			map.put("eventLists", String.valueOf(globalSearch.getEventLists()));
		}

		if (globalSearch.getHighlights() == null) {
			map.put("highlights", null);
		}
		else {
			map.put("highlights", String.valueOf(globalSearch.getHighlights()));
		}

		if (globalSearch.getStories() == null) {
			map.put("stories", null);
		}
		else {
			map.put("stories", String.valueOf(globalSearch.getStories()));
		}

		return map;
	}

	public static class GlobalSearchJSONParser
		extends BaseJSONParser<GlobalSearch> {

		@Override
		protected GlobalSearch createDTO() {
			return new GlobalSearch();
		}

		@Override
		protected GlobalSearch[] createDTOArray(int size) {
			return new GlobalSearch[size];
		}

		@Override
		protected void setField(
			GlobalSearch globalSearch, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "albums")) {
				if (jsonParserFieldValue != null) {
					globalSearch.setAlbums(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> AlbumSerDes.toDTO((String)object)
						).toArray(
							size -> new Album[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courts")) {
				if (jsonParserFieldValue != null) {
					globalSearch.setCourts(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> CourtSerDes.toDTO((String)object)
						).toArray(
							size -> new Court[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventLists")) {
				if (jsonParserFieldValue != null) {
					globalSearch.setEventLists(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> EventListSerDes.toDTO((String)object)
						).toArray(
							size -> new EventList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "highlights")) {
				if (jsonParserFieldValue != null) {
					globalSearch.setHighlights(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> HighlightSerDes.toDTO((String)object)
						).toArray(
							size -> new Highlight[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "stories")) {
				if (jsonParserFieldValue != null) {
					globalSearch.setStories(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> StorySerDes.toDTO((String)object)
						).toArray(
							size -> new Story[size]
						));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}