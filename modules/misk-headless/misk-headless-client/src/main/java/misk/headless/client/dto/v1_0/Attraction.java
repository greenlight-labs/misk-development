package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AmenitiesList;
import misk.headless.client.dto.v1_0.BookSpaceList;
import misk.headless.client.dto.v1_0.EventsSection;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.AttractionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Attraction implements Cloneable, Serializable {

	public static Attraction toDTO(String json) {
		return AttractionSerDes.toDTO(json);
	}

	public String getAmenitiesLabel() {
		return amenitiesLabel;
	}

	public void setAmenitiesLabel(String amenitiesLabel) {
		this.amenitiesLabel = amenitiesLabel;
	}

	public void setAmenitiesLabel(
		UnsafeSupplier<String, Exception> amenitiesLabelUnsafeSupplier) {

		try {
			amenitiesLabel = amenitiesLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String amenitiesLabel;

	public AmenitiesList[] getAmenitiesList() {
		return amenitiesList;
	}

	public void setAmenitiesList(AmenitiesList[] amenitiesList) {
		this.amenitiesList = amenitiesList;
	}

	public void setAmenitiesList(
		UnsafeSupplier<AmenitiesList[], Exception>
			amenitiesListUnsafeSupplier) {

		try {
			amenitiesList = amenitiesListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected AmenitiesList[] amenitiesList;

	public String getAttractionBanner() {
		return attractionBanner;
	}

	public void setAttractionBanner(String attractionBanner) {
		this.attractionBanner = attractionBanner;
	}

	public void setAttractionBanner(
		UnsafeSupplier<String, Exception> attractionBannerUnsafeSupplier) {

		try {
			attractionBanner = attractionBannerUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String attractionBanner;

	public String getAttractionBannerIcon() {
		return attractionBannerIcon;
	}

	public void setAttractionBannerIcon(String attractionBannerIcon) {
		this.attractionBannerIcon = attractionBannerIcon;
	}

	public void setAttractionBannerIcon(
		UnsafeSupplier<String, Exception> attractionBannerIconUnsafeSupplier) {

		try {
			attractionBannerIcon = attractionBannerIconUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String attractionBannerIcon;

	public String getAttractionButtonLabel() {
		return attractionButtonLabel;
	}

	public void setAttractionButtonLabel(String attractionButtonLabel) {
		this.attractionButtonLabel = attractionButtonLabel;
	}

	public void setAttractionButtonLabel(
		UnsafeSupplier<String, Exception> attractionButtonLabelUnsafeSupplier) {

		try {
			attractionButtonLabel = attractionButtonLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String attractionButtonLabel;

	public String getAttractionId() {
		return attractionId;
	}

	public void setAttractionId(String attractionId) {
		this.attractionId = attractionId;
	}

	public void setAttractionId(
		UnsafeSupplier<String, Exception> attractionIdUnsafeSupplier) {

		try {
			attractionId = attractionIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String attractionId;

	public String getAttractionTitle() {
		return attractionTitle;
	}

	public void setAttractionTitle(String attractionTitle) {
		this.attractionTitle = attractionTitle;
	}

	public void setAttractionTitle(
		UnsafeSupplier<String, Exception> attractionTitleUnsafeSupplier) {

		try {
			attractionTitle = attractionTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String attractionTitle;

	public String getBannerButtonColor() {
		return bannerButtonColor;
	}

	public void setBannerButtonColor(String bannerButtonColor) {
		this.bannerButtonColor = bannerButtonColor;
	}

	public void setBannerButtonColor(
		UnsafeSupplier<String, Exception> bannerButtonColorUnsafeSupplier) {

		try {
			bannerButtonColor = bannerButtonColorUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String bannerButtonColor;

	public String getBookSpaceLabel() {
		return bookSpaceLabel;
	}

	public void setBookSpaceLabel(String bookSpaceLabel) {
		this.bookSpaceLabel = bookSpaceLabel;
	}

	public void setBookSpaceLabel(
		UnsafeSupplier<String, Exception> bookSpaceLabelUnsafeSupplier) {

		try {
			bookSpaceLabel = bookSpaceLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String bookSpaceLabel;

	public BookSpaceList[] getBookSpaceList() {
		return bookSpaceList;
	}

	public void setBookSpaceList(BookSpaceList[] bookSpaceList) {
		this.bookSpaceList = bookSpaceList;
	}

	public void setBookSpaceList(
		UnsafeSupplier<BookSpaceList[], Exception>
			bookSpaceListUnsafeSupplier) {

		try {
			bookSpaceList = bookSpaceListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected BookSpaceList[] bookSpaceList;

	public String getContactEmailAddress() {
		return contactEmailAddress;
	}

	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}

	public void setContactEmailAddress(
		UnsafeSupplier<String, Exception> contactEmailAddressUnsafeSupplier) {

		try {
			contactEmailAddress = contactEmailAddressUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String contactEmailAddress;

	public String getContactEmailAddressIcon() {
		return contactEmailAddressIcon;
	}

	public void setContactEmailAddressIcon(String contactEmailAddressIcon) {
		this.contactEmailAddressIcon = contactEmailAddressIcon;
	}

	public void setContactEmailAddressIcon(
		UnsafeSupplier<String, Exception>
			contactEmailAddressIconUnsafeSupplier) {

		try {
			contactEmailAddressIcon =
				contactEmailAddressIconUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String contactEmailAddressIcon;

	public String getContactLabel() {
		return contactLabel;
	}

	public void setContactLabel(String contactLabel) {
		this.contactLabel = contactLabel;
	}

	public void setContactLabel(
		UnsafeSupplier<String, Exception> contactLabelUnsafeSupplier) {

		try {
			contactLabel = contactLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String contactLabel;

	public String getContactTelephone() {
		return contactTelephone;
	}

	public void setContactTelephone(String contactTelephone) {
		this.contactTelephone = contactTelephone;
	}

	public void setContactTelephone(
		UnsafeSupplier<String, Exception> contactTelephoneUnsafeSupplier) {

		try {
			contactTelephone = contactTelephoneUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String contactTelephone;

	public String getContactTelephoneIcon() {
		return contactTelephoneIcon;
	}

	public void setContactTelephoneIcon(String contactTelephoneIcon) {
		this.contactTelephoneIcon = contactTelephoneIcon;
	}

	public void setContactTelephoneIcon(
		UnsafeSupplier<String, Exception> contactTelephoneIconUnsafeSupplier) {

		try {
			contactTelephoneIcon = contactTelephoneIconUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String contactTelephoneIcon;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventsSection eventsSection;

	public String[] getGalleryImagesList() {
		return galleryImagesList;
	}

	public void setGalleryImagesList(String[] galleryImagesList) {
		this.galleryImagesList = galleryImagesList;
	}

	public void setGalleryImagesList(
		UnsafeSupplier<String[], Exception> galleryImagesListUnsafeSupplier) {

		try {
			galleryImagesList = galleryImagesListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] galleryImagesList;

	public String getGalleryLabel() {
		return galleryLabel;
	}

	public void setGalleryLabel(String galleryLabel) {
		this.galleryLabel = galleryLabel;
	}

	public void setGalleryLabel(
		UnsafeSupplier<String, Exception> galleryLabelUnsafeSupplier) {

		try {
			galleryLabel = galleryLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String galleryLabel;

	public String getLocationLabel() {
		return locationLabel;
	}

	public void setLocationLabel(String locationLabel) {
		this.locationLabel = locationLabel;
	}

	public void setLocationLabel(
		UnsafeSupplier<String, Exception> locationLabelUnsafeSupplier) {

		try {
			locationLabel = locationLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String locationLabel;

	public String getLocationLatitude() {
		return locationLatitude;
	}

	public void setLocationLatitude(String locationLatitude) {
		this.locationLatitude = locationLatitude;
	}

	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> locationLatitudeUnsafeSupplier) {

		try {
			locationLatitude = locationLatitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String locationLatitude;

	public String getLocationLongitude() {
		return locationLongitude;
	}

	public void setLocationLongitude(String locationLongitude) {
		this.locationLongitude = locationLongitude;
	}

	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> locationLongitudeUnsafeSupplier) {

		try {
			locationLongitude = locationLongitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String locationLongitude;

	public String getWorkingDays() {
		return workingDays;
	}

	public void setWorkingDays(String workingDays) {
		this.workingDays = workingDays;
	}

	public void setWorkingDays(
		UnsafeSupplier<String, Exception> workingDaysUnsafeSupplier) {

		try {
			workingDays = workingDaysUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingDays;

	public String getWorkingDaysImage() {
		return workingDaysImage;
	}

	public void setWorkingDaysImage(String workingDaysImage) {
		this.workingDaysImage = workingDaysImage;
	}

	public void setWorkingDaysImage(
		UnsafeSupplier<String, Exception> workingDaysImageUnsafeSupplier) {

		try {
			workingDaysImage = workingDaysImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingDaysImage;

	public String getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}

	public void setWorkingHours(
		UnsafeSupplier<String, Exception> workingHoursUnsafeSupplier) {

		try {
			workingHours = workingHoursUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingHours;

	public String getWorkingHoursImage() {
		return workingHoursImage;
	}

	public void setWorkingHoursImage(String workingHoursImage) {
		this.workingHoursImage = workingHoursImage;
	}

	public void setWorkingHoursImage(
		UnsafeSupplier<String, Exception> workingHoursImageUnsafeSupplier) {

		try {
			workingHoursImage = workingHoursImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingHoursImage;

	public String getWorkingHoursLabel() {
		return workingHoursLabel;
	}

	public void setWorkingHoursLabel(String workingHoursLabel) {
		this.workingHoursLabel = workingHoursLabel;
	}

	public void setWorkingHoursLabel(
		UnsafeSupplier<String, Exception> workingHoursLabelUnsafeSupplier) {

		try {
			workingHoursLabel = workingHoursLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingHoursLabel;

	public String getWorkingHoursLabelColor() {
		return workingHoursLabelColor;
	}

	public void setWorkingHoursLabelColor(String workingHoursLabelColor) {
		this.workingHoursLabelColor = workingHoursLabelColor;
	}

	public void setWorkingHoursLabelColor(
		UnsafeSupplier<String, Exception>
			workingHoursLabelColorUnsafeSupplier) {

		try {
			workingHoursLabelColor = workingHoursLabelColorUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingHoursLabelColor;

	@Override
	public Attraction clone() throws CloneNotSupportedException {
		return (Attraction)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Attraction)) {
			return false;
		}

		Attraction attraction = (Attraction)object;

		return Objects.equals(toString(), attraction.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return AttractionSerDes.toJSON(this);
	}

}