package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AppUser;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.AddVisitorSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AddVisitor implements Cloneable, Serializable {

	public static AddVisitor toDTO(String json) {
		return AddVisitorSerDes.toDTO(json);
	}

	public AppUser[] getAppUsers() {
		return appUsers;
	}

	public void setAppUsers(AppUser[] appUsers) {
		this.appUsers = appUsers;
	}

	public void setAppUsers(
		UnsafeSupplier<AppUser[], Exception> appUsersUnsafeSupplier) {

		try {
			appUsers = appUsersUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected AppUser[] appUsers;

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public void setCount(UnsafeSupplier<Long, Exception> countUnsafeSupplier) {
		try {
			count = countUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long count;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public void setEventId(
		UnsafeSupplier<Long, Exception> eventIdUnsafeSupplier) {

		try {
			eventId = eventIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long eventId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setUserId(
		UnsafeSupplier<Long, Exception> userIdUnsafeSupplier) {

		try {
			userId = userIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long userId;

	@Override
	public AddVisitor clone() throws CloneNotSupportedException {
		return (AddVisitor)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AddVisitor)) {
			return false;
		}

		AddVisitor addVisitor = (AddVisitor)object;

		return Objects.equals(toString(), addVisitor.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return AddVisitorSerDes.toJSON(this);
	}

}