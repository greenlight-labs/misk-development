package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.DeleteAccountPageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DeleteAccountPage implements Cloneable, Serializable {

	public static DeleteAccountPage toDTO(String json) {
		return DeleteAccountPageSerDes.toDTO(json);
	}

	public String[] getAnswers() {
		return answers;
	}

	public void setAnswers(String[] answers) {
		this.answers = answers;
	}

	public void setAnswers(
		UnsafeSupplier<String[], Exception> answersUnsafeSupplier) {

		try {
			answers = answersUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] answers;

	public String getDeleteAccountLabel() {
		return deleteAccountLabel;
	}

	public void setDeleteAccountLabel(String deleteAccountLabel) {
		this.deleteAccountLabel = deleteAccountLabel;
	}

	public void setDeleteAccountLabel(
		UnsafeSupplier<String, Exception> deleteAccountLabelUnsafeSupplier) {

		try {
			deleteAccountLabel = deleteAccountLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String deleteAccountLabel;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public void setQuestion(
		UnsafeSupplier<String, Exception> questionUnsafeSupplier) {

		try {
			question = questionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String question;

	public String getQuestionExcerpt() {
		return questionExcerpt;
	}

	public void setQuestionExcerpt(String questionExcerpt) {
		this.questionExcerpt = questionExcerpt;
	}

	public void setQuestionExcerpt(
		UnsafeSupplier<String, Exception> questionExcerptUnsafeSupplier) {

		try {
			questionExcerpt = questionExcerptUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String questionExcerpt;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	@Override
	public DeleteAccountPage clone() throws CloneNotSupportedException {
		return (DeleteAccountPage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DeleteAccountPage)) {
			return false;
		}

		DeleteAccountPage deleteAccountPage = (DeleteAccountPage)object;

		return Objects.equals(toString(), deleteAccountPage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return DeleteAccountPageSerDes.toJSON(this);
	}

}