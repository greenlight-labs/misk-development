package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AmenitiesList;
import misk.headless.client.dto.v1_0.Attraction;
import misk.headless.client.dto.v1_0.BookSpaceList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AttractionSerDes {

	public static Attraction toDTO(String json) {
		AttractionJSONParser attractionJSONParser = new AttractionJSONParser();

		return attractionJSONParser.parseToDTO(json);
	}

	public static Attraction[] toDTOs(String json) {
		AttractionJSONParser attractionJSONParser = new AttractionJSONParser();

		return attractionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Attraction attraction) {
		if (attraction == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (attraction.getAmenitiesLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"amenitiesLabel\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getAmenitiesLabel()));

			sb.append("\"");
		}

		if (attraction.getAmenitiesList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"amenitiesList\": ");

			sb.append("[");

			for (int i = 0; i < attraction.getAmenitiesList().length; i++) {
				sb.append(String.valueOf(attraction.getAmenitiesList()[i]));

				if ((i + 1) < attraction.getAmenitiesList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (attraction.getAttractionBanner() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionBanner\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getAttractionBanner()));

			sb.append("\"");
		}

		if (attraction.getAttractionBannerIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionBannerIcon\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getAttractionBannerIcon()));

			sb.append("\"");
		}

		if (attraction.getAttractionButtonLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionButtonLabel\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getAttractionButtonLabel()));

			sb.append("\"");
		}

		if (attraction.getAttractionId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionId\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getAttractionId()));

			sb.append("\"");
		}

		if (attraction.getAttractionTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionTitle\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getAttractionTitle()));

			sb.append("\"");
		}

		if (attraction.getBannerButtonColor() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bannerButtonColor\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getBannerButtonColor()));

			sb.append("\"");
		}

		if (attraction.getBookSpaceLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookSpaceLabel\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getBookSpaceLabel()));

			sb.append("\"");
		}

		if (attraction.getBookSpaceList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookSpaceList\": ");

			sb.append("[");

			for (int i = 0; i < attraction.getBookSpaceList().length; i++) {
				sb.append(String.valueOf(attraction.getBookSpaceList()[i]));

				if ((i + 1) < attraction.getBookSpaceList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (attraction.getContactEmailAddress() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactEmailAddress\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getContactEmailAddress()));

			sb.append("\"");
		}

		if (attraction.getContactEmailAddressIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactEmailAddressIcon\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getContactEmailAddressIcon()));

			sb.append("\"");
		}

		if (attraction.getContactLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactLabel\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getContactLabel()));

			sb.append("\"");
		}

		if (attraction.getContactTelephone() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactTelephone\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getContactTelephone()));

			sb.append("\"");
		}

		if (attraction.getContactTelephoneIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"contactTelephoneIcon\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getContactTelephoneIcon()));

			sb.append("\"");
		}

		if (attraction.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getDescription()));

			sb.append("\"");
		}

		if (attraction.getEventsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(attraction.getEventsSection()));
		}

		if (attraction.getGalleryImagesList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryImagesList\": ");

			sb.append("[");

			for (int i = 0; i < attraction.getGalleryImagesList().length; i++) {
				sb.append("\"");

				sb.append(_escape(attraction.getGalleryImagesList()[i]));

				sb.append("\"");

				if ((i + 1) < attraction.getGalleryImagesList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (attraction.getGalleryLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryLabel\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getGalleryLabel()));

			sb.append("\"");
		}

		if (attraction.getLocationLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLabel\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getLocationLabel()));

			sb.append("\"");
		}

		if (attraction.getLocationLatitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getLocationLatitude()));

			sb.append("\"");
		}

		if (attraction.getLocationLongitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getLocationLongitude()));

			sb.append("\"");
		}

		if (attraction.getWorkingDays() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingDays\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getWorkingDays()));

			sb.append("\"");
		}

		if (attraction.getWorkingDaysImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingDaysImage\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getWorkingDaysImage()));

			sb.append("\"");
		}

		if (attraction.getWorkingHours() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHours\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getWorkingHours()));

			sb.append("\"");
		}

		if (attraction.getWorkingHoursImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHoursImage\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getWorkingHoursImage()));

			sb.append("\"");
		}

		if (attraction.getWorkingHoursLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHoursLabel\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getWorkingHoursLabel()));

			sb.append("\"");
		}

		if (attraction.getWorkingHoursLabelColor() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingHoursLabelColor\": ");

			sb.append("\"");

			sb.append(_escape(attraction.getWorkingHoursLabelColor()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		AttractionJSONParser attractionJSONParser = new AttractionJSONParser();

		return attractionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Attraction attraction) {
		if (attraction == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (attraction.getAmenitiesLabel() == null) {
			map.put("amenitiesLabel", null);
		}
		else {
			map.put(
				"amenitiesLabel",
				String.valueOf(attraction.getAmenitiesLabel()));
		}

		if (attraction.getAmenitiesList() == null) {
			map.put("amenitiesList", null);
		}
		else {
			map.put(
				"amenitiesList", String.valueOf(attraction.getAmenitiesList()));
		}

		if (attraction.getAttractionBanner() == null) {
			map.put("attractionBanner", null);
		}
		else {
			map.put(
				"attractionBanner",
				String.valueOf(attraction.getAttractionBanner()));
		}

		if (attraction.getAttractionBannerIcon() == null) {
			map.put("attractionBannerIcon", null);
		}
		else {
			map.put(
				"attractionBannerIcon",
				String.valueOf(attraction.getAttractionBannerIcon()));
		}

		if (attraction.getAttractionButtonLabel() == null) {
			map.put("attractionButtonLabel", null);
		}
		else {
			map.put(
				"attractionButtonLabel",
				String.valueOf(attraction.getAttractionButtonLabel()));
		}

		if (attraction.getAttractionId() == null) {
			map.put("attractionId", null);
		}
		else {
			map.put(
				"attractionId", String.valueOf(attraction.getAttractionId()));
		}

		if (attraction.getAttractionTitle() == null) {
			map.put("attractionTitle", null);
		}
		else {
			map.put(
				"attractionTitle",
				String.valueOf(attraction.getAttractionTitle()));
		}

		if (attraction.getBannerButtonColor() == null) {
			map.put("bannerButtonColor", null);
		}
		else {
			map.put(
				"bannerButtonColor",
				String.valueOf(attraction.getBannerButtonColor()));
		}

		if (attraction.getBookSpaceLabel() == null) {
			map.put("bookSpaceLabel", null);
		}
		else {
			map.put(
				"bookSpaceLabel",
				String.valueOf(attraction.getBookSpaceLabel()));
		}

		if (attraction.getBookSpaceList() == null) {
			map.put("bookSpaceList", null);
		}
		else {
			map.put(
				"bookSpaceList", String.valueOf(attraction.getBookSpaceList()));
		}

		if (attraction.getContactEmailAddress() == null) {
			map.put("contactEmailAddress", null);
		}
		else {
			map.put(
				"contactEmailAddress",
				String.valueOf(attraction.getContactEmailAddress()));
		}

		if (attraction.getContactEmailAddressIcon() == null) {
			map.put("contactEmailAddressIcon", null);
		}
		else {
			map.put(
				"contactEmailAddressIcon",
				String.valueOf(attraction.getContactEmailAddressIcon()));
		}

		if (attraction.getContactLabel() == null) {
			map.put("contactLabel", null);
		}
		else {
			map.put(
				"contactLabel", String.valueOf(attraction.getContactLabel()));
		}

		if (attraction.getContactTelephone() == null) {
			map.put("contactTelephone", null);
		}
		else {
			map.put(
				"contactTelephone",
				String.valueOf(attraction.getContactTelephone()));
		}

		if (attraction.getContactTelephoneIcon() == null) {
			map.put("contactTelephoneIcon", null);
		}
		else {
			map.put(
				"contactTelephoneIcon",
				String.valueOf(attraction.getContactTelephoneIcon()));
		}

		if (attraction.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(attraction.getDescription()));
		}

		if (attraction.getEventsSection() == null) {
			map.put("eventsSection", null);
		}
		else {
			map.put(
				"eventsSection", String.valueOf(attraction.getEventsSection()));
		}

		if (attraction.getGalleryImagesList() == null) {
			map.put("galleryImagesList", null);
		}
		else {
			map.put(
				"galleryImagesList",
				String.valueOf(attraction.getGalleryImagesList()));
		}

		if (attraction.getGalleryLabel() == null) {
			map.put("galleryLabel", null);
		}
		else {
			map.put(
				"galleryLabel", String.valueOf(attraction.getGalleryLabel()));
		}

		if (attraction.getLocationLabel() == null) {
			map.put("locationLabel", null);
		}
		else {
			map.put(
				"locationLabel", String.valueOf(attraction.getLocationLabel()));
		}

		if (attraction.getLocationLatitude() == null) {
			map.put("locationLatitude", null);
		}
		else {
			map.put(
				"locationLatitude",
				String.valueOf(attraction.getLocationLatitude()));
		}

		if (attraction.getLocationLongitude() == null) {
			map.put("locationLongitude", null);
		}
		else {
			map.put(
				"locationLongitude",
				String.valueOf(attraction.getLocationLongitude()));
		}

		if (attraction.getWorkingDays() == null) {
			map.put("workingDays", null);
		}
		else {
			map.put("workingDays", String.valueOf(attraction.getWorkingDays()));
		}

		if (attraction.getWorkingDaysImage() == null) {
			map.put("workingDaysImage", null);
		}
		else {
			map.put(
				"workingDaysImage",
				String.valueOf(attraction.getWorkingDaysImage()));
		}

		if (attraction.getWorkingHours() == null) {
			map.put("workingHours", null);
		}
		else {
			map.put(
				"workingHours", String.valueOf(attraction.getWorkingHours()));
		}

		if (attraction.getWorkingHoursImage() == null) {
			map.put("workingHoursImage", null);
		}
		else {
			map.put(
				"workingHoursImage",
				String.valueOf(attraction.getWorkingHoursImage()));
		}

		if (attraction.getWorkingHoursLabel() == null) {
			map.put("workingHoursLabel", null);
		}
		else {
			map.put(
				"workingHoursLabel",
				String.valueOf(attraction.getWorkingHoursLabel()));
		}

		if (attraction.getWorkingHoursLabelColor() == null) {
			map.put("workingHoursLabelColor", null);
		}
		else {
			map.put(
				"workingHoursLabelColor",
				String.valueOf(attraction.getWorkingHoursLabelColor()));
		}

		return map;
	}

	public static class AttractionJSONParser
		extends BaseJSONParser<Attraction> {

		@Override
		protected Attraction createDTO() {
			return new Attraction();
		}

		@Override
		protected Attraction[] createDTOArray(int size) {
			return new Attraction[size];
		}

		@Override
		protected void setField(
			Attraction attraction, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "amenitiesLabel")) {
				if (jsonParserFieldValue != null) {
					attraction.setAmenitiesLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "amenitiesList")) {
				if (jsonParserFieldValue != null) {
					attraction.setAmenitiesList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> AmenitiesListSerDes.toDTO((String)object)
						).toArray(
							size -> new AmenitiesList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "attractionBanner")) {
				if (jsonParserFieldValue != null) {
					attraction.setAttractionBanner(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "attractionBannerIcon")) {

				if (jsonParserFieldValue != null) {
					attraction.setAttractionBannerIcon(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "attractionButtonLabel")) {

				if (jsonParserFieldValue != null) {
					attraction.setAttractionButtonLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "attractionId")) {
				if (jsonParserFieldValue != null) {
					attraction.setAttractionId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "attractionTitle")) {
				if (jsonParserFieldValue != null) {
					attraction.setAttractionTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bannerButtonColor")) {
				if (jsonParserFieldValue != null) {
					attraction.setBannerButtonColor(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bookSpaceLabel")) {
				if (jsonParserFieldValue != null) {
					attraction.setBookSpaceLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bookSpaceList")) {
				if (jsonParserFieldValue != null) {
					attraction.setBookSpaceList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> BookSpaceListSerDes.toDTO((String)object)
						).toArray(
							size -> new BookSpaceList[size]
						));
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "contactEmailAddress")) {

				if (jsonParserFieldValue != null) {
					attraction.setContactEmailAddress(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "contactEmailAddressIcon")) {

				if (jsonParserFieldValue != null) {
					attraction.setContactEmailAddressIcon(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "contactLabel")) {
				if (jsonParserFieldValue != null) {
					attraction.setContactLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "contactTelephone")) {
				if (jsonParserFieldValue != null) {
					attraction.setContactTelephone(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "contactTelephoneIcon")) {

				if (jsonParserFieldValue != null) {
					attraction.setContactTelephoneIcon(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					attraction.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventsSection")) {
				if (jsonParserFieldValue != null) {
					attraction.setEventsSection(
						EventsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "galleryImagesList")) {
				if (jsonParserFieldValue != null) {
					attraction.setGalleryImagesList(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "galleryLabel")) {
				if (jsonParserFieldValue != null) {
					attraction.setGalleryLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationLabel")) {
				if (jsonParserFieldValue != null) {
					attraction.setLocationLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationLatitude")) {
				if (jsonParserFieldValue != null) {
					attraction.setLocationLatitude(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationLongitude")) {
				if (jsonParserFieldValue != null) {
					attraction.setLocationLongitude(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workingDays")) {
				if (jsonParserFieldValue != null) {
					attraction.setWorkingDays((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workingDaysImage")) {
				if (jsonParserFieldValue != null) {
					attraction.setWorkingDaysImage(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workingHours")) {
				if (jsonParserFieldValue != null) {
					attraction.setWorkingHours((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workingHoursImage")) {
				if (jsonParserFieldValue != null) {
					attraction.setWorkingHoursImage(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workingHoursLabel")) {
				if (jsonParserFieldValue != null) {
					attraction.setWorkingHoursLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "workingHoursLabelColor")) {

				if (jsonParserFieldValue != null) {
					attraction.setWorkingHoursLabelColor(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}