package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ManageAccountPageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ManageAccountPage implements Cloneable, Serializable {

	public static ManageAccountPage toDTO(String json) {
		return ManageAccountPageSerDes.toDTO(json);
	}

	public String getDeactivateAccountLabel() {
		return deactivateAccountLabel;
	}

	public void setDeactivateAccountLabel(String deactivateAccountLabel) {
		this.deactivateAccountLabel = deactivateAccountLabel;
	}

	public void setDeactivateAccountLabel(
		UnsafeSupplier<String, Exception>
			deactivateAccountLabelUnsafeSupplier) {

		try {
			deactivateAccountLabel = deactivateAccountLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String deactivateAccountLabel;

	public String getDeleteAccountLabel() {
		return deleteAccountLabel;
	}

	public void setDeleteAccountLabel(String deleteAccountLabel) {
		this.deleteAccountLabel = deleteAccountLabel;
	}

	public void setDeleteAccountLabel(
		UnsafeSupplier<String, Exception> deleteAccountLabelUnsafeSupplier) {

		try {
			deleteAccountLabel = deleteAccountLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String deleteAccountLabel;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public void setSubtitle(
		UnsafeSupplier<String, Exception> subtitleUnsafeSupplier) {

		try {
			subtitle = subtitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String subtitle;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	@Override
	public ManageAccountPage clone() throws CloneNotSupportedException {
		return (ManageAccountPage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ManageAccountPage)) {
			return false;
		}

		ManageAccountPage manageAccountPage = (ManageAccountPage)object;

		return Objects.equals(toString(), manageAccountPage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ManageAccountPageSerDes.toJSON(this);
	}

}