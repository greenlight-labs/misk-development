package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.CommunityWallSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CommunityWall implements Cloneable, Serializable {

	public static CommunityWall toDTO(String json) {
		return CommunityWallSerDes.toDTO(json);
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setData(UnsafeSupplier<String, Exception> dataUnsafeSupplier) {
		try {
			data = dataUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String data;

	@Override
	public CommunityWall clone() throws CloneNotSupportedException {
		return (CommunityWall)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CommunityWall)) {
			return false;
		}

		CommunityWall communityWall = (CommunityWall)object;

		return Objects.equals(toString(), communityWall.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return CommunityWallSerDes.toJSON(this);
	}

}