package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ExploreListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreList implements Cloneable, Serializable {

	public static ExploreList toDTO(String json) {
		return ExploreListSerDes.toDTO(json);
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public void setArea(UnsafeSupplier<String, Exception> areaUnsafeSupplier) {
		try {
			area = areaUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String area;

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public void setCapacity(
		UnsafeSupplier<String, Exception> capacityUnsafeSupplier) {

		try {
			capacity = capacityUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String capacity;

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setDuration(
		UnsafeSupplier<String, Exception> durationUnsafeSupplier) {

		try {
			duration = durationUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String duration;

	public String getExploreIcon() {
		return exploreIcon;
	}

	public void setExploreIcon(String exploreIcon) {
		this.exploreIcon = exploreIcon;
	}

	public void setExploreIcon(
		UnsafeSupplier<String, Exception> exploreIconUnsafeSupplier) {

		try {
			exploreIcon = exploreIconUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String exploreIcon;

	public String getExploreImages() {
		return exploreImages;
	}

	public void setExploreImages(String exploreImages) {
		this.exploreImages = exploreImages;
	}

	public void setExploreImages(
		UnsafeSupplier<String, Exception> exploreImagesUnsafeSupplier) {

		try {
			exploreImages = exploreImagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String exploreImages;

	public String getExploreTitle() {
		return exploreTitle;
	}

	public void setExploreTitle(String exploreTitle) {
		this.exploreTitle = exploreTitle;
	}

	public void setExploreTitle(
		UnsafeSupplier<String, Exception> exploreTitleUnsafeSupplier) {

		try {
			exploreTitle = exploreTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String exploreTitle;

	@Override
	public ExploreList clone() throws CloneNotSupportedException {
		return (ExploreList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExploreList)) {
			return false;
		}

		ExploreList exploreList = (ExploreList)object;

		return Objects.equals(toString(), exploreList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ExploreListSerDes.toJSON(this);
	}

}