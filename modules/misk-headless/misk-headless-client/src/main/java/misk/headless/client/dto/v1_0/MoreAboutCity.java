package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.MoreAboutCitySerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MoreAboutCity implements Cloneable, Serializable {

	public static MoreAboutCity toDTO(String json) {
		return MoreAboutCitySerDes.toDTO(json);
	}

	public String getButtonLabel() {
		return buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		this.buttonLabel = buttonLabel;
	}

	public void setButtonLabel(
		UnsafeSupplier<String, Exception> buttonLabelUnsafeSupplier) {

		try {
			buttonLabel = buttonLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String buttonLabel;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setImage(
		UnsafeSupplier<String, Exception> imageUnsafeSupplier) {

		try {
			image = imageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String image;

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public void setSubtitle(
		UnsafeSupplier<String, Exception> subtitleUnsafeSupplier) {

		try {
			subtitle = subtitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String subtitle;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	public String getTopHeading() {
		return topHeading;
	}

	public void setTopHeading(String topHeading) {
		this.topHeading = topHeading;
	}

	public void setTopHeading(
		UnsafeSupplier<String, Exception> topHeadingUnsafeSupplier) {

		try {
			topHeading = topHeadingUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String topHeading;

	@Override
	public MoreAboutCity clone() throws CloneNotSupportedException {
		return (MoreAboutCity)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MoreAboutCity)) {
			return false;
		}

		MoreAboutCity moreAboutCity = (MoreAboutCity)object;

		return Objects.equals(toString(), moreAboutCity.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return MoreAboutCitySerDes.toJSON(this);
	}

}