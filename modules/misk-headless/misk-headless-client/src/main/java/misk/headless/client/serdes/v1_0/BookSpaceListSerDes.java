package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.BookSpaceList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookSpaceListSerDes {

	public static BookSpaceList toDTO(String json) {
		BookSpaceListJSONParser bookSpaceListJSONParser =
			new BookSpaceListJSONParser();

		return bookSpaceListJSONParser.parseToDTO(json);
	}

	public static BookSpaceList[] toDTOs(String json) {
		BookSpaceListJSONParser bookSpaceListJSONParser =
			new BookSpaceListJSONParser();

		return bookSpaceListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(BookSpaceList bookSpaceList) {
		if (bookSpaceList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (bookSpaceList.getArea() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Area\": ");

			sb.append("\"");

			sb.append(_escape(bookSpaceList.getArea()));

			sb.append("\"");
		}

		if (bookSpaceList.getBookSpaceIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceIcon\": ");

			sb.append("\"");

			sb.append(_escape(bookSpaceList.getBookSpaceIcon()));

			sb.append("\"");
		}

		if (bookSpaceList.getBookSpaceImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceImages\": ");

			sb.append("\"");

			sb.append(_escape(bookSpaceList.getBookSpaceImages()));

			sb.append("\"");
		}

		if (bookSpaceList.getBookSpaceTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceTitle\": ");

			sb.append("\"");

			sb.append(_escape(bookSpaceList.getBookSpaceTitle()));

			sb.append("\"");
		}

		if (bookSpaceList.getCapacity() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Capacity\": ");

			sb.append("\"");

			sb.append(_escape(bookSpaceList.getCapacity()));

			sb.append("\"");
		}

		if (bookSpaceList.getDuration() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Duration\": ");

			sb.append("\"");

			sb.append(_escape(bookSpaceList.getDuration()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		BookSpaceListJSONParser bookSpaceListJSONParser =
			new BookSpaceListJSONParser();

		return bookSpaceListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(BookSpaceList bookSpaceList) {
		if (bookSpaceList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (bookSpaceList.getArea() == null) {
			map.put("Area", null);
		}
		else {
			map.put("Area", String.valueOf(bookSpaceList.getArea()));
		}

		if (bookSpaceList.getBookSpaceIcon() == null) {
			map.put("BookSpaceIcon", null);
		}
		else {
			map.put(
				"BookSpaceIcon",
				String.valueOf(bookSpaceList.getBookSpaceIcon()));
		}

		if (bookSpaceList.getBookSpaceImages() == null) {
			map.put("BookSpaceImages", null);
		}
		else {
			map.put(
				"BookSpaceImages",
				String.valueOf(bookSpaceList.getBookSpaceImages()));
		}

		if (bookSpaceList.getBookSpaceTitle() == null) {
			map.put("BookSpaceTitle", null);
		}
		else {
			map.put(
				"BookSpaceTitle",
				String.valueOf(bookSpaceList.getBookSpaceTitle()));
		}

		if (bookSpaceList.getCapacity() == null) {
			map.put("Capacity", null);
		}
		else {
			map.put("Capacity", String.valueOf(bookSpaceList.getCapacity()));
		}

		if (bookSpaceList.getDuration() == null) {
			map.put("Duration", null);
		}
		else {
			map.put("Duration", String.valueOf(bookSpaceList.getDuration()));
		}

		return map;
	}

	public static class BookSpaceListJSONParser
		extends BaseJSONParser<BookSpaceList> {

		@Override
		protected BookSpaceList createDTO() {
			return new BookSpaceList();
		}

		@Override
		protected BookSpaceList[] createDTOArray(int size) {
			return new BookSpaceList[size];
		}

		@Override
		protected void setField(
			BookSpaceList bookSpaceList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "Area")) {
				if (jsonParserFieldValue != null) {
					bookSpaceList.setArea((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "BookSpaceIcon")) {
				if (jsonParserFieldValue != null) {
					bookSpaceList.setBookSpaceIcon(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "BookSpaceImages")) {
				if (jsonParserFieldValue != null) {
					bookSpaceList.setBookSpaceImages(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "BookSpaceTitle")) {
				if (jsonParserFieldValue != null) {
					bookSpaceList.setBookSpaceTitle(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "Capacity")) {
				if (jsonParserFieldValue != null) {
					bookSpaceList.setCapacity((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "Duration")) {
				if (jsonParserFieldValue != null) {
					bookSpaceList.setDuration((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}