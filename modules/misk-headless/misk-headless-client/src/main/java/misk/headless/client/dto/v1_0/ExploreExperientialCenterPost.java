package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AudioSection;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ExploreExperientialCenterPostSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreExperientialCenterPost implements Cloneable, Serializable {

	public static ExploreExperientialCenterPost toDTO(String json) {
		return ExploreExperientialCenterPostSerDes.toDTO(json);
	}

	public AudioSection getAudioSection() {
		return audioSection;
	}

	public void setAudioSection(AudioSection audioSection) {
		this.audioSection = audioSection;
	}

	public void setAudioSection(
		UnsafeSupplier<AudioSection, Exception> audioSectionUnsafeSupplier) {

		try {
			audioSection = audioSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected AudioSection audioSection;

	public String getButtonLabel() {
		return buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		this.buttonLabel = buttonLabel;
	}

	public void setButtonLabel(
		UnsafeSupplier<String, Exception> buttonLabelUnsafeSupplier) {

		try {
			buttonLabel = buttonLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String buttonLabel;

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public void setCategoryId(
		UnsafeSupplier<Long, Exception> categoryIdUnsafeSupplier) {

		try {
			categoryId = categoryIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long categoryId;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getEstimatedTourTime() {
		return estimatedTourTime;
	}

	public void setEstimatedTourTime(String estimatedTourTime) {
		this.estimatedTourTime = estimatedTourTime;
	}

	public void setEstimatedTourTime(
		UnsafeSupplier<String, Exception> estimatedTourTimeUnsafeSupplier) {

		try {
			estimatedTourTime = estimatedTourTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String estimatedTourTime;

	public String getEstimatedTourTimeLabel() {
		return estimatedTourTimeLabel;
	}

	public void setEstimatedTourTimeLabel(String estimatedTourTimeLabel) {
		this.estimatedTourTimeLabel = estimatedTourTimeLabel;
	}

	public void setEstimatedTourTimeLabel(
		UnsafeSupplier<String, Exception>
			estimatedTourTimeLabelUnsafeSupplier) {

		try {
			estimatedTourTimeLabel = estimatedTourTimeLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String estimatedTourTimeLabel;

	public String[] getGalleryImages() {
		return galleryImages;
	}

	public void setGalleryImages(String[] galleryImages) {
		this.galleryImages = galleryImages;
	}

	public void setGalleryImages(
		UnsafeSupplier<String[], Exception> galleryImagesUnsafeSupplier) {

		try {
			galleryImages = galleryImagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] galleryImages;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId(UnsafeSupplier<Long, Exception> idUnsafeSupplier) {
		try {
			id = idUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long id;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setImage(
		UnsafeSupplier<String, Exception> imageUnsafeSupplier) {

		try {
			image = imageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String image;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setName(UnsafeSupplier<String, Exception> nameUnsafeSupplier) {
		try {
			name = nameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String name;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setType(UnsafeSupplier<String, Exception> typeUnsafeSupplier) {
		try {
			type = typeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String type;

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public void setVideo(
		UnsafeSupplier<String, Exception> videoUnsafeSupplier) {

		try {
			video = videoUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String video;

	@Override
	public ExploreExperientialCenterPost clone()
		throws CloneNotSupportedException {

		return (ExploreExperientialCenterPost)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExploreExperientialCenterPost)) {
			return false;
		}

		ExploreExperientialCenterPost exploreExperientialCenterPost =
			(ExploreExperientialCenterPost)object;

		return Objects.equals(
			toString(), exploreExperientialCenterPost.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ExploreExperientialCenterPostSerDes.toJSON(this);
	}

}