package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ExperientialCenter;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExperientialCenterSerDes {

	public static ExperientialCenter toDTO(String json) {
		ExperientialCenterJSONParser experientialCenterJSONParser =
			new ExperientialCenterJSONParser();

		return experientialCenterJSONParser.parseToDTO(json);
	}

	public static ExperientialCenter[] toDTOs(String json) {
		ExperientialCenterJSONParser experientialCenterJSONParser =
			new ExperientialCenterJSONParser();

		return experientialCenterJSONParser.parseToDTOs(json);
	}

	public static String toJSON(ExperientialCenter experientialCenter) {
		if (experientialCenter == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (experientialCenter.getBannerSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bannerSection\": ");

			sb.append(String.valueOf(experientialCenter.getBannerSection()));
		}

		if (experientialCenter.getDiscoverSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"discoverSection\": ");

			sb.append(String.valueOf(experientialCenter.getDiscoverSection()));
		}

		if (experientialCenter.getEventsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(experientialCenter.getEventsSection()));
		}

		if (experientialCenter.getHenaCafeSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"henaCafeSection\": ");

			sb.append(String.valueOf(experientialCenter.getHenaCafeSection()));
		}

		if (experientialCenter.getHighlightsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlightsSection\": ");

			sb.append(
				String.valueOf(experientialCenter.getHighlightsSection()));
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ExperientialCenterJSONParser experientialCenterJSONParser =
			new ExperientialCenterJSONParser();

		return experientialCenterJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		ExperientialCenter experientialCenter) {

		if (experientialCenter == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (experientialCenter.getBannerSection() == null) {
			map.put("bannerSection", null);
		}
		else {
			map.put(
				"bannerSection",
				String.valueOf(experientialCenter.getBannerSection()));
		}

		if (experientialCenter.getDiscoverSection() == null) {
			map.put("discoverSection", null);
		}
		else {
			map.put(
				"discoverSection",
				String.valueOf(experientialCenter.getDiscoverSection()));
		}

		if (experientialCenter.getEventsSection() == null) {
			map.put("eventsSection", null);
		}
		else {
			map.put(
				"eventsSection",
				String.valueOf(experientialCenter.getEventsSection()));
		}

		if (experientialCenter.getHenaCafeSection() == null) {
			map.put("henaCafeSection", null);
		}
		else {
			map.put(
				"henaCafeSection",
				String.valueOf(experientialCenter.getHenaCafeSection()));
		}

		if (experientialCenter.getHighlightsSection() == null) {
			map.put("highlightsSection", null);
		}
		else {
			map.put(
				"highlightsSection",
				String.valueOf(experientialCenter.getHighlightsSection()));
		}

		return map;
	}

	public static class ExperientialCenterJSONParser
		extends BaseJSONParser<ExperientialCenter> {

		@Override
		protected ExperientialCenter createDTO() {
			return new ExperientialCenter();
		}

		@Override
		protected ExperientialCenter[] createDTOArray(int size) {
			return new ExperientialCenter[size];
		}

		@Override
		protected void setField(
			ExperientialCenter experientialCenter, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "bannerSection")) {
				if (jsonParserFieldValue != null) {
					experientialCenter.setBannerSection(
						BannerSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "discoverSection")) {
				if (jsonParserFieldValue != null) {
					experientialCenter.setDiscoverSection(
						DiscoverSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventsSection")) {
				if (jsonParserFieldValue != null) {
					experientialCenter.setEventsSection(
						EventsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "henaCafeSection")) {
				if (jsonParserFieldValue != null) {
					experientialCenter.setHenaCafeSection(
						HenaCafeSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "highlightsSection")) {
				if (jsonParserFieldValue != null) {
					experientialCenter.setHighlightsSection(
						HighlightsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}