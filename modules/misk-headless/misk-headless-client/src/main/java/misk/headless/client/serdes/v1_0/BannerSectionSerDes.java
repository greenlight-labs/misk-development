package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.BannerSection;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BannerSectionSerDes {

	public static BannerSection toDTO(String json) {
		BannerSectionJSONParser bannerSectionJSONParser =
			new BannerSectionJSONParser();

		return bannerSectionJSONParser.parseToDTO(json);
	}

	public static BannerSection[] toDTOs(String json) {
		BannerSectionJSONParser bannerSectionJSONParser =
			new BannerSectionJSONParser();

		return bannerSectionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(BannerSection bannerSection) {
		if (bannerSection == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (bannerSection.getButtonLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"buttonLabel\": ");

			sb.append("\"");

			sb.append(_escape(bannerSection.getButtonLabel()));

			sb.append("\"");
		}

		if (bannerSection.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(bannerSection.getDescription()));

			sb.append("\"");
		}

		if (bannerSection.getHeading() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"heading\": ");

			sb.append("\"");

			sb.append(_escape(bannerSection.getHeading()));

			sb.append("\"");
		}

		if (bannerSection.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(bannerSection.getImage()));

			sb.append("\"");
		}

		if (bannerSection.getTitleField1() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"titleField1\": ");

			sb.append("\"");

			sb.append(_escape(bannerSection.getTitleField1()));

			sb.append("\"");
		}

		if (bannerSection.getTitleField2() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"titleField2\": ");

			sb.append("\"");

			sb.append(_escape(bannerSection.getTitleField2()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		BannerSectionJSONParser bannerSectionJSONParser =
			new BannerSectionJSONParser();

		return bannerSectionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(BannerSection bannerSection) {
		if (bannerSection == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (bannerSection.getButtonLabel() == null) {
			map.put("buttonLabel", null);
		}
		else {
			map.put(
				"buttonLabel", String.valueOf(bannerSection.getButtonLabel()));
		}

		if (bannerSection.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description", String.valueOf(bannerSection.getDescription()));
		}

		if (bannerSection.getHeading() == null) {
			map.put("heading", null);
		}
		else {
			map.put("heading", String.valueOf(bannerSection.getHeading()));
		}

		if (bannerSection.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(bannerSection.getImage()));
		}

		if (bannerSection.getTitleField1() == null) {
			map.put("titleField1", null);
		}
		else {
			map.put(
				"titleField1", String.valueOf(bannerSection.getTitleField1()));
		}

		if (bannerSection.getTitleField2() == null) {
			map.put("titleField2", null);
		}
		else {
			map.put(
				"titleField2", String.valueOf(bannerSection.getTitleField2()));
		}

		return map;
	}

	public static class BannerSectionJSONParser
		extends BaseJSONParser<BannerSection> {

		@Override
		protected BannerSection createDTO() {
			return new BannerSection();
		}

		@Override
		protected BannerSection[] createDTOArray(int size) {
			return new BannerSection[size];
		}

		@Override
		protected void setField(
			BannerSection bannerSection, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "buttonLabel")) {
				if (jsonParserFieldValue != null) {
					bannerSection.setButtonLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					bannerSection.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "heading")) {
				if (jsonParserFieldValue != null) {
					bannerSection.setHeading((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					bannerSection.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "titleField1")) {
				if (jsonParserFieldValue != null) {
					bannerSection.setTitleField1((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "titleField2")) {
				if (jsonParserFieldValue != null) {
					bannerSection.setTitleField2((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}