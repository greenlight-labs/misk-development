package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.dto.v1_0.Highlight;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.MyFavoritesSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MyFavorites implements Cloneable, Serializable {

	public static MyFavorites toDTO(String json) {
		return MyFavoritesSerDes.toDTO(json);
	}

	public EventList[] getEventLists() {
		return eventLists;
	}

	public void setEventLists(EventList[] eventLists) {
		this.eventLists = eventLists;
	}

	public void setEventLists(
		UnsafeSupplier<EventList[], Exception> eventListsUnsafeSupplier) {

		try {
			eventLists = eventListsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventList[] eventLists;

	public Highlight[] getHighlights() {
		return highlights;
	}

	public void setHighlights(Highlight[] highlights) {
		this.highlights = highlights;
	}

	public void setHighlights(
		UnsafeSupplier<Highlight[], Exception> highlightsUnsafeSupplier) {

		try {
			highlights = highlightsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Highlight[] highlights;

	@Override
	public MyFavorites clone() throws CloneNotSupportedException {
		return (MyFavorites)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MyFavorites)) {
			return false;
		}

		MyFavorites myFavorites = (MyFavorites)object;

		return Objects.equals(toString(), myFavorites.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return MyFavoritesSerDes.toJSON(this);
	}

}