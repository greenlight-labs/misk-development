package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.CategoryListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CategoryList implements Cloneable, Serializable {

	public static CategoryList toDTO(String json) {
		return CategoryListSerDes.toDTO(json);
	}

	public String getCatId() {
		return catId;
	}

	public void setCatId(String catId) {
		this.catId = catId;
	}

	public void setCatId(
		UnsafeSupplier<String, Exception> catIdUnsafeSupplier) {

		try {
			catId = catIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String catId;

	public String getCatTitle() {
		return catTitle;
	}

	public void setCatTitle(String catTitle) {
		this.catTitle = catTitle;
	}

	public void setCatTitle(
		UnsafeSupplier<String, Exception> catTitleUnsafeSupplier) {

		try {
			catTitle = catTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String catTitle;

	@Override
	public CategoryList clone() throws CloneNotSupportedException {
		return (CategoryList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CategoryList)) {
			return false;
		}

		CategoryList categoryList = (CategoryList)object;

		return Objects.equals(toString(), categoryList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return CategoryListSerDes.toJSON(this);
	}

}