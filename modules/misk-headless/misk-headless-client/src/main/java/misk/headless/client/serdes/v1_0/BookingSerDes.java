package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Booking;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookingSerDes {

	public static Booking toDTO(String json) {
		BookingJSONParser bookingJSONParser = new BookingJSONParser();

		return bookingJSONParser.parseToDTO(json);
	}

	public static Booking[] toDTOs(String json) {
		BookingJSONParser bookingJSONParser = new BookingJSONParser();

		return bookingJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Booking booking) {
		if (booking == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (booking.getAppUserEmail() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserEmail\": ");

			sb.append("\"");

			sb.append(_escape(booking.getAppUserEmail()));

			sb.append("\"");
		}

		if (booking.getAppUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(booking.getAppUserId());
		}

		if (booking.getAppUserName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserName\": ");

			sb.append("\"");

			sb.append(_escape(booking.getAppUserName()));

			sb.append("\"");
		}

		if (booking.getBookingDate() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookingDate\": ");

			sb.append("\"");

			sb.append(_escape(booking.getBookingDate()));

			sb.append("\"");
		}

		if (booking.getBookingId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookingId\": ");

			sb.append(booking.getBookingId());
		}

		if (booking.getCategory() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"category\": ");

			sb.append("\"");

			sb.append(_escape(booking.getCategory()));

			sb.append("\"");
		}

		if (booking.getCourt() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"court\": ");

			sb.append(String.valueOf(booking.getCourt()));
		}

		if (booking.getCourtId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtId\": ");

			sb.append(booking.getCourtId());
		}

		if (booking.getCourtLocation() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtLocation\": ");

			sb.append("\"");

			sb.append(_escape(booking.getCourtLocation()));

			sb.append("\"");
		}

		if (booking.getCourtName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtName\": ");

			sb.append("\"");

			sb.append(_escape(booking.getCourtName()));

			sb.append("\"");
		}

		if (booking.getRequestStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"requestStatus\": ");

			sb.append(String.valueOf(booking.getRequestStatus()));
		}

		if (booking.getSlotEndTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotEndTime\": ");

			sb.append("\"");

			sb.append(_escape(booking.getSlotEndTime()));

			sb.append("\"");
		}

		if (booking.getSlotStartTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotStartTime\": ");

			sb.append("\"");

			sb.append(_escape(booking.getSlotStartTime()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		BookingJSONParser bookingJSONParser = new BookingJSONParser();

		return bookingJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Booking booking) {
		if (booking == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (booking.getAppUserEmail() == null) {
			map.put("appUserEmail", null);
		}
		else {
			map.put("appUserEmail", String.valueOf(booking.getAppUserEmail()));
		}

		if (booking.getAppUserId() == null) {
			map.put("appUserId", null);
		}
		else {
			map.put("appUserId", String.valueOf(booking.getAppUserId()));
		}

		if (booking.getAppUserName() == null) {
			map.put("appUserName", null);
		}
		else {
			map.put("appUserName", String.valueOf(booking.getAppUserName()));
		}

		if (booking.getBookingDate() == null) {
			map.put("bookingDate", null);
		}
		else {
			map.put("bookingDate", String.valueOf(booking.getBookingDate()));
		}

		if (booking.getBookingId() == null) {
			map.put("bookingId", null);
		}
		else {
			map.put("bookingId", String.valueOf(booking.getBookingId()));
		}

		if (booking.getCategory() == null) {
			map.put("category", null);
		}
		else {
			map.put("category", String.valueOf(booking.getCategory()));
		}

		if (booking.getCourt() == null) {
			map.put("court", null);
		}
		else {
			map.put("court", String.valueOf(booking.getCourt()));
		}

		if (booking.getCourtId() == null) {
			map.put("courtId", null);
		}
		else {
			map.put("courtId", String.valueOf(booking.getCourtId()));
		}

		if (booking.getCourtLocation() == null) {
			map.put("courtLocation", null);
		}
		else {
			map.put(
				"courtLocation", String.valueOf(booking.getCourtLocation()));
		}

		if (booking.getCourtName() == null) {
			map.put("courtName", null);
		}
		else {
			map.put("courtName", String.valueOf(booking.getCourtName()));
		}

		if (booking.getRequestStatus() == null) {
			map.put("requestStatus", null);
		}
		else {
			map.put(
				"requestStatus", String.valueOf(booking.getRequestStatus()));
		}

		if (booking.getSlotEndTime() == null) {
			map.put("slotEndTime", null);
		}
		else {
			map.put("slotEndTime", String.valueOf(booking.getSlotEndTime()));
		}

		if (booking.getSlotStartTime() == null) {
			map.put("slotStartTime", null);
		}
		else {
			map.put(
				"slotStartTime", String.valueOf(booking.getSlotStartTime()));
		}

		return map;
	}

	public static class BookingJSONParser extends BaseJSONParser<Booking> {

		@Override
		protected Booking createDTO() {
			return new Booking();
		}

		@Override
		protected Booking[] createDTOArray(int size) {
			return new Booking[size];
		}

		@Override
		protected void setField(
			Booking booking, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "appUserEmail")) {
				if (jsonParserFieldValue != null) {
					booking.setAppUserEmail((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "appUserId")) {
				if (jsonParserFieldValue != null) {
					booking.setAppUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "appUserName")) {
				if (jsonParserFieldValue != null) {
					booking.setAppUserName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bookingDate")) {
				if (jsonParserFieldValue != null) {
					booking.setBookingDate((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bookingId")) {
				if (jsonParserFieldValue != null) {
					booking.setBookingId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "category")) {
				if (jsonParserFieldValue != null) {
					booking.setCategory((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "court")) {
				if (jsonParserFieldValue != null) {
					booking.setCourt(
						CourtSerDes.toDTO((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courtId")) {
				if (jsonParserFieldValue != null) {
					booking.setCourtId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courtLocation")) {
				if (jsonParserFieldValue != null) {
					booking.setCourtLocation((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "courtName")) {
				if (jsonParserFieldValue != null) {
					booking.setCourtName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "requestStatus")) {
				if (jsonParserFieldValue != null) {
					booking.setRequestStatus(
						RequestStatusSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "slotEndTime")) {
				if (jsonParserFieldValue != null) {
					booking.setSlotEndTime((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "slotStartTime")) {
				if (jsonParserFieldValue != null) {
					booking.setSlotStartTime((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}