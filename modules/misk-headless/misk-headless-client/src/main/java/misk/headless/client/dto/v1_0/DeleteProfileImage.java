package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.DeleteProfileImageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DeleteProfileImage implements Cloneable, Serializable {

	public static DeleteProfileImage toDTO(String json) {
		return DeleteProfileImageSerDes.toDTO(json);
	}

	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long appUserId;

	@Override
	public DeleteProfileImage clone() throws CloneNotSupportedException {
		return (DeleteProfileImage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DeleteProfileImage)) {
			return false;
		}

		DeleteProfileImage deleteProfileImage = (DeleteProfileImage)object;

		return Objects.equals(toString(), deleteProfileImage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return DeleteProfileImageSerDes.toJSON(this);
	}

}