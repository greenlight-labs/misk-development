package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AddVisitor;
import misk.headless.client.dto.v1_0.AppUser;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AddVisitorSerDes {

	public static AddVisitor toDTO(String json) {
		AddVisitorJSONParser addVisitorJSONParser = new AddVisitorJSONParser();

		return addVisitorJSONParser.parseToDTO(json);
	}

	public static AddVisitor[] toDTOs(String json) {
		AddVisitorJSONParser addVisitorJSONParser = new AddVisitorJSONParser();

		return addVisitorJSONParser.parseToDTOs(json);
	}

	public static String toJSON(AddVisitor addVisitor) {
		if (addVisitor == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (addVisitor.getAppUsers() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUsers\": ");

			sb.append("[");

			for (int i = 0; i < addVisitor.getAppUsers().length; i++) {
				sb.append(String.valueOf(addVisitor.getAppUsers()[i]));

				if ((i + 1) < addVisitor.getAppUsers().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (addVisitor.getCount() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"count\": ");

			sb.append(addVisitor.getCount());
		}

		if (addVisitor.getEventId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventId\": ");

			sb.append(addVisitor.getEventId());
		}

		if (addVisitor.getUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"userId\": ");

			sb.append(addVisitor.getUserId());
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		AddVisitorJSONParser addVisitorJSONParser = new AddVisitorJSONParser();

		return addVisitorJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(AddVisitor addVisitor) {
		if (addVisitor == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (addVisitor.getAppUsers() == null) {
			map.put("appUsers", null);
		}
		else {
			map.put("appUsers", String.valueOf(addVisitor.getAppUsers()));
		}

		if (addVisitor.getCount() == null) {
			map.put("count", null);
		}
		else {
			map.put("count", String.valueOf(addVisitor.getCount()));
		}

		if (addVisitor.getEventId() == null) {
			map.put("eventId", null);
		}
		else {
			map.put("eventId", String.valueOf(addVisitor.getEventId()));
		}

		if (addVisitor.getUserId() == null) {
			map.put("userId", null);
		}
		else {
			map.put("userId", String.valueOf(addVisitor.getUserId()));
		}

		return map;
	}

	public static class AddVisitorJSONParser
		extends BaseJSONParser<AddVisitor> {

		@Override
		protected AddVisitor createDTO() {
			return new AddVisitor();
		}

		@Override
		protected AddVisitor[] createDTOArray(int size) {
			return new AddVisitor[size];
		}

		@Override
		protected void setField(
			AddVisitor addVisitor, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "appUsers")) {
				if (jsonParserFieldValue != null) {
					addVisitor.setAppUsers(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> AppUserSerDes.toDTO((String)object)
						).toArray(
							size -> new AppUser[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "count")) {
				if (jsonParserFieldValue != null) {
					addVisitor.setCount(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventId")) {
				if (jsonParserFieldValue != null) {
					addVisitor.setEventId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "userId")) {
				if (jsonParserFieldValue != null) {
					addVisitor.setUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}