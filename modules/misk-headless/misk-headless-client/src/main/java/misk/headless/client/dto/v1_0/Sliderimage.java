package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.SliderimageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Sliderimage implements Cloneable, Serializable {

	public static Sliderimage toDTO(String json) {
		return SliderimageSerDes.toDTO(json);
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public void setImages(
		UnsafeSupplier<String, Exception> imagesUnsafeSupplier) {

		try {
			images = imagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String images;

	@Override
	public Sliderimage clone() throws CloneNotSupportedException {
		return (Sliderimage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Sliderimage)) {
			return false;
		}

		Sliderimage sliderimage = (Sliderimage)object;

		return Objects.equals(toString(), sliderimage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return SliderimageSerDes.toJSON(this);
	}

}