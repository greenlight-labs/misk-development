package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ExploreWadiC;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreWadiCSerDes {

	public static ExploreWadiC toDTO(String json) {
		ExploreWadiCJSONParser exploreWadiCJSONParser =
			new ExploreWadiCJSONParser();

		return exploreWadiCJSONParser.parseToDTO(json);
	}

	public static ExploreWadiC[] toDTOs(String json) {
		ExploreWadiCJSONParser exploreWadiCJSONParser =
			new ExploreWadiCJSONParser();

		return exploreWadiCJSONParser.parseToDTOs(json);
	}

	public static String toJSON(ExploreWadiC exploreWadiC) {
		if (exploreWadiC == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (exploreWadiC.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(exploreWadiC.getDescription()));

			sb.append("\"");
		}

		if (exploreWadiC.getEstimatedTourTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"estimatedTourTime\": ");

			sb.append("\"");

			sb.append(_escape(exploreWadiC.getEstimatedTourTime()));

			sb.append("\"");
		}

		if (exploreWadiC.getEstimatedTourTimeLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"estimatedTourTimeLabel\": ");

			sb.append("\"");

			sb.append(_escape(exploreWadiC.getEstimatedTourTimeLabel()));

			sb.append("\"");
		}

		if (exploreWadiC.getGalleryImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryImages\": ");

			sb.append("[");

			for (int i = 0; i < exploreWadiC.getGalleryImages().length; i++) {
				sb.append("\"");

				sb.append(_escape(exploreWadiC.getGalleryImages()[i]));

				sb.append("\"");

				if ((i + 1) < exploreWadiC.getGalleryImages().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (exploreWadiC.getId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append(exploreWadiC.getId());
		}

		if (exploreWadiC.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(exploreWadiC.getImage()));

			sb.append("\"");
		}

		if (exploreWadiC.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(exploreWadiC.getName()));

			sb.append("\"");
		}

		if (exploreWadiC.getType() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"type\": ");

			sb.append("\"");

			sb.append(_escape(exploreWadiC.getType()));

			sb.append("\"");
		}

		if (exploreWadiC.getVideo() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"video\": ");

			sb.append("\"");

			sb.append(_escape(exploreWadiC.getVideo()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ExploreWadiCJSONParser exploreWadiCJSONParser =
			new ExploreWadiCJSONParser();

		return exploreWadiCJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(ExploreWadiC exploreWadiC) {
		if (exploreWadiC == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (exploreWadiC.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description", String.valueOf(exploreWadiC.getDescription()));
		}

		if (exploreWadiC.getEstimatedTourTime() == null) {
			map.put("estimatedTourTime", null);
		}
		else {
			map.put(
				"estimatedTourTime",
				String.valueOf(exploreWadiC.getEstimatedTourTime()));
		}

		if (exploreWadiC.getEstimatedTourTimeLabel() == null) {
			map.put("estimatedTourTimeLabel", null);
		}
		else {
			map.put(
				"estimatedTourTimeLabel",
				String.valueOf(exploreWadiC.getEstimatedTourTimeLabel()));
		}

		if (exploreWadiC.getGalleryImages() == null) {
			map.put("galleryImages", null);
		}
		else {
			map.put(
				"galleryImages",
				String.valueOf(exploreWadiC.getGalleryImages()));
		}

		if (exploreWadiC.getId() == null) {
			map.put("id", null);
		}
		else {
			map.put("id", String.valueOf(exploreWadiC.getId()));
		}

		if (exploreWadiC.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(exploreWadiC.getImage()));
		}

		if (exploreWadiC.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put("name", String.valueOf(exploreWadiC.getName()));
		}

		if (exploreWadiC.getType() == null) {
			map.put("type", null);
		}
		else {
			map.put("type", String.valueOf(exploreWadiC.getType()));
		}

		if (exploreWadiC.getVideo() == null) {
			map.put("video", null);
		}
		else {
			map.put("video", String.valueOf(exploreWadiC.getVideo()));
		}

		return map;
	}

	public static class ExploreWadiCJSONParser
		extends BaseJSONParser<ExploreWadiC> {

		@Override
		protected ExploreWadiC createDTO() {
			return new ExploreWadiC();
		}

		@Override
		protected ExploreWadiC[] createDTOArray(int size) {
			return new ExploreWadiC[size];
		}

		@Override
		protected void setField(
			ExploreWadiC exploreWadiC, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "estimatedTourTime")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setEstimatedTourTime(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "estimatedTourTimeLabel")) {

				if (jsonParserFieldValue != null) {
					exploreWadiC.setEstimatedTourTimeLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "galleryImages")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setGalleryImages(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "id")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "type")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setType((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "video")) {
				if (jsonParserFieldValue != null) {
					exploreWadiC.setVideo((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}