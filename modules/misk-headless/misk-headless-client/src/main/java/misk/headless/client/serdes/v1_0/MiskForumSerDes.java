package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AmenitiesList;
import misk.headless.client.dto.v1_0.BookSpaceList;
import misk.headless.client.dto.v1_0.MiskForum;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MiskForumSerDes {

	public static MiskForum toDTO(String json) {
		MiskForumJSONParser miskForumJSONParser = new MiskForumJSONParser();

		return miskForumJSONParser.parseToDTO(json);
	}

	public static MiskForum[] toDTOs(String json) {
		MiskForumJSONParser miskForumJSONParser = new MiskForumJSONParser();

		return miskForumJSONParser.parseToDTOs(json);
	}

	public static String toJSON(MiskForum miskForum) {
		if (miskForum == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (miskForum.getAmenitiesLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"AmenitiesLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getAmenitiesLabel()));

			sb.append("\"");
		}

		if (miskForum.getAmenitiesList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"AmenitiesList\": ");

			sb.append("[");

			for (int i = 0; i < miskForum.getAmenitiesList().length; i++) {
				sb.append(String.valueOf(miskForum.getAmenitiesList()[i]));

				if ((i + 1) < miskForum.getAmenitiesList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (miskForum.getBookSpaceLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getBookSpaceLabel()));

			sb.append("\"");
		}

		if (miskForum.getBookSpaceList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"BookSpaceList\": ");

			sb.append("[");

			for (int i = 0; i < miskForum.getBookSpaceList().length; i++) {
				sb.append(String.valueOf(miskForum.getBookSpaceList()[i]));

				if ((i + 1) < miskForum.getBookSpaceList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (miskForum.getContactEmailAddress() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactEmailAddress\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getContactEmailAddress()));

			sb.append("\"");
		}

		if (miskForum.getContactLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getContactLabel()));

			sb.append("\"");
		}

		if (miskForum.getContactTelephone() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"ContactTelephone\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getContactTelephone()));

			sb.append("\"");
		}

		if (miskForum.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Description\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getDescription()));

			sb.append("\"");
		}

		if (miskForum.getEventLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EventLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getEventLabel()));

			sb.append("\"");
		}

		if (miskForum.getEventName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"EventName\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getEventName()));

			sb.append("\"");
		}

		if (miskForum.getGalleryImagesList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"GalleryImagesList\": ");

			sb.append("[");

			for (int i = 0; i < miskForum.getGalleryImagesList().length; i++) {
				sb.append("\"");

				sb.append(_escape(miskForum.getGalleryImagesList()[i]));

				sb.append("\"");

				if ((i + 1) < miskForum.getGalleryImagesList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (miskForum.getGalleryLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"GalleryLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getGalleryLabel()));

			sb.append("\"");
		}

		if (miskForum.getLocationLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getLocationLabel()));

			sb.append("\"");
		}

		if (miskForum.getLocationLatitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getLocationLatitude()));

			sb.append("\"");
		}

		if (miskForum.getLocationLongitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"LocationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getLocationLongitude()));

			sb.append("\"");
		}

		if (miskForum.getMiskForumBanner() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"MiskForumBanner\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getMiskForumBanner()));

			sb.append("\"");
		}

		if (miskForum.getMiskForumLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"MiskForumLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getMiskForumLabel()));

			sb.append("\"");
		}

		if (miskForum.getWorkingDay() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingDay\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getWorkingDay()));

			sb.append("\"");
		}

		if (miskForum.getWorkingHours() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHours\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getWorkingHours()));

			sb.append("\"");
		}

		if (miskForum.getWorkingHoursLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"WorkingHoursLabel\": ");

			sb.append("\"");

			sb.append(_escape(miskForum.getWorkingHoursLabel()));

			sb.append("\"");
		}

		if (miskForum.getEventsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(miskForum.getEventsSection()));
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		MiskForumJSONParser miskForumJSONParser = new MiskForumJSONParser();

		return miskForumJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(MiskForum miskForum) {
		if (miskForum == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (miskForum.getAmenitiesLabel() == null) {
			map.put("AmenitiesLabel", null);
		}
		else {
			map.put(
				"AmenitiesLabel",
				String.valueOf(miskForum.getAmenitiesLabel()));
		}

		if (miskForum.getAmenitiesList() == null) {
			map.put("AmenitiesList", null);
		}
		else {
			map.put(
				"AmenitiesList", String.valueOf(miskForum.getAmenitiesList()));
		}

		if (miskForum.getBookSpaceLabel() == null) {
			map.put("BookSpaceLabel", null);
		}
		else {
			map.put(
				"BookSpaceLabel",
				String.valueOf(miskForum.getBookSpaceLabel()));
		}

		if (miskForum.getBookSpaceList() == null) {
			map.put("BookSpaceList", null);
		}
		else {
			map.put(
				"BookSpaceList", String.valueOf(miskForum.getBookSpaceList()));
		}

		if (miskForum.getContactEmailAddress() == null) {
			map.put("ContactEmailAddress", null);
		}
		else {
			map.put(
				"ContactEmailAddress",
				String.valueOf(miskForum.getContactEmailAddress()));
		}

		if (miskForum.getContactLabel() == null) {
			map.put("ContactLabel", null);
		}
		else {
			map.put(
				"ContactLabel", String.valueOf(miskForum.getContactLabel()));
		}

		if (miskForum.getContactTelephone() == null) {
			map.put("ContactTelephone", null);
		}
		else {
			map.put(
				"ContactTelephone",
				String.valueOf(miskForum.getContactTelephone()));
		}

		if (miskForum.getDescription() == null) {
			map.put("Description", null);
		}
		else {
			map.put("Description", String.valueOf(miskForum.getDescription()));
		}

		if (miskForum.getEventLabel() == null) {
			map.put("EventLabel", null);
		}
		else {
			map.put("EventLabel", String.valueOf(miskForum.getEventLabel()));
		}

		if (miskForum.getEventName() == null) {
			map.put("EventName", null);
		}
		else {
			map.put("EventName", String.valueOf(miskForum.getEventName()));
		}

		if (miskForum.getGalleryImagesList() == null) {
			map.put("GalleryImagesList", null);
		}
		else {
			map.put(
				"GalleryImagesList",
				String.valueOf(miskForum.getGalleryImagesList()));
		}

		if (miskForum.getGalleryLabel() == null) {
			map.put("GalleryLabel", null);
		}
		else {
			map.put(
				"GalleryLabel", String.valueOf(miskForum.getGalleryLabel()));
		}

		if (miskForum.getLocationLabel() == null) {
			map.put("LocationLabel", null);
		}
		else {
			map.put(
				"LocationLabel", String.valueOf(miskForum.getLocationLabel()));
		}

		if (miskForum.getLocationLatitude() == null) {
			map.put("LocationLatitude", null);
		}
		else {
			map.put(
				"LocationLatitude",
				String.valueOf(miskForum.getLocationLatitude()));
		}

		if (miskForum.getLocationLongitude() == null) {
			map.put("LocationLongitude", null);
		}
		else {
			map.put(
				"LocationLongitude",
				String.valueOf(miskForum.getLocationLongitude()));
		}

		if (miskForum.getMiskForumBanner() == null) {
			map.put("MiskForumBanner", null);
		}
		else {
			map.put(
				"MiskForumBanner",
				String.valueOf(miskForum.getMiskForumBanner()));
		}

		if (miskForum.getMiskForumLabel() == null) {
			map.put("MiskForumLabel", null);
		}
		else {
			map.put(
				"MiskForumLabel",
				String.valueOf(miskForum.getMiskForumLabel()));
		}

		if (miskForum.getWorkingDay() == null) {
			map.put("WorkingDay", null);
		}
		else {
			map.put("WorkingDay", String.valueOf(miskForum.getWorkingDay()));
		}

		if (miskForum.getWorkingHours() == null) {
			map.put("WorkingHours", null);
		}
		else {
			map.put(
				"WorkingHours", String.valueOf(miskForum.getWorkingHours()));
		}

		if (miskForum.getWorkingHoursLabel() == null) {
			map.put("WorkingHoursLabel", null);
		}
		else {
			map.put(
				"WorkingHoursLabel",
				String.valueOf(miskForum.getWorkingHoursLabel()));
		}

		if (miskForum.getEventsSection() == null) {
			map.put("eventsSection", null);
		}
		else {
			map.put(
				"eventsSection", String.valueOf(miskForum.getEventsSection()));
		}

		return map;
	}

	public static class MiskForumJSONParser extends BaseJSONParser<MiskForum> {

		@Override
		protected MiskForum createDTO() {
			return new MiskForum();
		}

		@Override
		protected MiskForum[] createDTOArray(int size) {
			return new MiskForum[size];
		}

		@Override
		protected void setField(
			MiskForum miskForum, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "AmenitiesLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setAmenitiesLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "AmenitiesList")) {
				if (jsonParserFieldValue != null) {
					miskForum.setAmenitiesList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> AmenitiesListSerDes.toDTO((String)object)
						).toArray(
							size -> new AmenitiesList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "BookSpaceLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setBookSpaceLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "BookSpaceList")) {
				if (jsonParserFieldValue != null) {
					miskForum.setBookSpaceList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> BookSpaceListSerDes.toDTO((String)object)
						).toArray(
							size -> new BookSpaceList[size]
						));
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "ContactEmailAddress")) {

				if (jsonParserFieldValue != null) {
					miskForum.setContactEmailAddress(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "ContactLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setContactLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "ContactTelephone")) {
				if (jsonParserFieldValue != null) {
					miskForum.setContactTelephone((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "Description")) {
				if (jsonParserFieldValue != null) {
					miskForum.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "EventLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setEventLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "EventName")) {
				if (jsonParserFieldValue != null) {
					miskForum.setEventName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "GalleryImagesList")) {
				if (jsonParserFieldValue != null) {
					miskForum.setGalleryImagesList(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "GalleryLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setGalleryLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "LocationLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setLocationLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "LocationLatitude")) {
				if (jsonParserFieldValue != null) {
					miskForum.setLocationLatitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "LocationLongitude")) {
				if (jsonParserFieldValue != null) {
					miskForum.setLocationLongitude(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "MiskForumBanner")) {
				if (jsonParserFieldValue != null) {
					miskForum.setMiskForumBanner((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "MiskForumLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setMiskForumLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WorkingDay")) {
				if (jsonParserFieldValue != null) {
					miskForum.setWorkingDay((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WorkingHours")) {
				if (jsonParserFieldValue != null) {
					miskForum.setWorkingHours((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "WorkingHoursLabel")) {
				if (jsonParserFieldValue != null) {
					miskForum.setWorkingHoursLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventsSection")) {
				if (jsonParserFieldValue != null) {
					miskForum.setEventsSection(
						EventsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}