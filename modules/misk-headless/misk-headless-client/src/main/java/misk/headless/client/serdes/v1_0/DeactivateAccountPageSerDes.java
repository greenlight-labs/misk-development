package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.DeactivateAccountPage;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DeactivateAccountPageSerDes {

	public static DeactivateAccountPage toDTO(String json) {
		DeactivateAccountPageJSONParser deactivateAccountPageJSONParser =
			new DeactivateAccountPageJSONParser();

		return deactivateAccountPageJSONParser.parseToDTO(json);
	}

	public static DeactivateAccountPage[] toDTOs(String json) {
		DeactivateAccountPageJSONParser deactivateAccountPageJSONParser =
			new DeactivateAccountPageJSONParser();

		return deactivateAccountPageJSONParser.parseToDTOs(json);
	}

	public static String toJSON(DeactivateAccountPage deactivateAccountPage) {
		if (deactivateAccountPage == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (deactivateAccountPage.getAnswers() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"answers\": ");

			sb.append("[");

			for (int i = 0; i < deactivateAccountPage.getAnswers().length;
				 i++) {

				sb.append("\"");

				sb.append(_escape(deactivateAccountPage.getAnswers()[i]));

				sb.append("\"");

				if ((i + 1) < deactivateAccountPage.getAnswers().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (deactivateAccountPage.getDeactivateAccountLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deactivateAccountLabel\": ");

			sb.append("\"");

			sb.append(
				_escape(deactivateAccountPage.getDeactivateAccountLabel()));

			sb.append("\"");
		}

		if (deactivateAccountPage.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(deactivateAccountPage.getDescription()));

			sb.append("\"");
		}

		if (deactivateAccountPage.getQuestion() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"question\": ");

			sb.append("\"");

			sb.append(_escape(deactivateAccountPage.getQuestion()));

			sb.append("\"");
		}

		if (deactivateAccountPage.getQuestionExcerpt() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"questionExcerpt\": ");

			sb.append("\"");

			sb.append(_escape(deactivateAccountPage.getQuestionExcerpt()));

			sb.append("\"");
		}

		if (deactivateAccountPage.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(deactivateAccountPage.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		DeactivateAccountPageJSONParser deactivateAccountPageJSONParser =
			new DeactivateAccountPageJSONParser();

		return deactivateAccountPageJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		DeactivateAccountPage deactivateAccountPage) {

		if (deactivateAccountPage == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (deactivateAccountPage.getAnswers() == null) {
			map.put("answers", null);
		}
		else {
			map.put(
				"answers", String.valueOf(deactivateAccountPage.getAnswers()));
		}

		if (deactivateAccountPage.getDeactivateAccountLabel() == null) {
			map.put("deactivateAccountLabel", null);
		}
		else {
			map.put(
				"deactivateAccountLabel",
				String.valueOf(
					deactivateAccountPage.getDeactivateAccountLabel()));
		}

		if (deactivateAccountPage.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(deactivateAccountPage.getDescription()));
		}

		if (deactivateAccountPage.getQuestion() == null) {
			map.put("question", null);
		}
		else {
			map.put(
				"question",
				String.valueOf(deactivateAccountPage.getQuestion()));
		}

		if (deactivateAccountPage.getQuestionExcerpt() == null) {
			map.put("questionExcerpt", null);
		}
		else {
			map.put(
				"questionExcerpt",
				String.valueOf(deactivateAccountPage.getQuestionExcerpt()));
		}

		if (deactivateAccountPage.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(deactivateAccountPage.getTitle()));
		}

		return map;
	}

	public static class DeactivateAccountPageJSONParser
		extends BaseJSONParser<DeactivateAccountPage> {

		@Override
		protected DeactivateAccountPage createDTO() {
			return new DeactivateAccountPage();
		}

		@Override
		protected DeactivateAccountPage[] createDTOArray(int size) {
			return new DeactivateAccountPage[size];
		}

		@Override
		protected void setField(
			DeactivateAccountPage deactivateAccountPage,
			String jsonParserFieldName, Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "answers")) {
				if (jsonParserFieldValue != null) {
					deactivateAccountPage.setAnswers(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "deactivateAccountLabel")) {

				if (jsonParserFieldValue != null) {
					deactivateAccountPage.setDeactivateAccountLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					deactivateAccountPage.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "question")) {
				if (jsonParserFieldValue != null) {
					deactivateAccountPage.setQuestion(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "questionExcerpt")) {
				if (jsonParserFieldValue != null) {
					deactivateAccountPage.setQuestionExcerpt(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					deactivateAccountPage.setTitle(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}