package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.CourtBooking;
import misk.headless.client.dto.v1_0.CourtCategory;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.CourtSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Court implements Cloneable, Serializable {

	public static Court toDTO(String json) {
		return CourtSerDes.toDTO(json);
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public void setCategoryId(
		UnsafeSupplier<Long, Exception> categoryIdUnsafeSupplier) {

		try {
			categoryId = categoryIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long categoryId;

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public void setContactEmail(
		UnsafeSupplier<String, Exception> contactEmailUnsafeSupplier) {

		try {
			contactEmail = contactEmailUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String contactEmail;

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public void setContactPhone(
		UnsafeSupplier<String, Exception> contactPhoneUnsafeSupplier) {

		try {
			contactPhone = contactPhoneUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String contactPhone;

	public CourtBooking getCourtBooking() {
		return courtBooking;
	}

	public void setCourtBooking(CourtBooking courtBooking) {
		this.courtBooking = courtBooking;
	}

	public void setCourtBooking(
		UnsafeSupplier<CourtBooking, Exception> courtBookingUnsafeSupplier) {

		try {
			courtBooking = courtBookingUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected CourtBooking courtBooking;

	public CourtCategory getCourtCategory() {
		return courtCategory;
	}

	public void setCourtCategory(CourtCategory courtCategory) {
		this.courtCategory = courtCategory;
	}

	public void setCourtCategory(
		UnsafeSupplier<CourtCategory, Exception> courtCategoryUnsafeSupplier) {

		try {
			courtCategory = courtCategoryUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected CourtCategory courtCategory;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String[] getDetailImages() {
		return detailImages;
	}

	public void setDetailImages(String[] detailImages) {
		this.detailImages = detailImages;
	}

	public void setDetailImages(
		UnsafeSupplier<String[], Exception> detailImagesUnsafeSupplier) {

		try {
			detailImages = detailImagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] detailImages;

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public void setHeading(
		UnsafeSupplier<String, Exception> headingUnsafeSupplier) {

		try {
			heading = headingUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String heading;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setId(UnsafeSupplier<Long, Exception> idUnsafeSupplier) {
		try {
			id = idUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long id;

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public void setLat(UnsafeSupplier<String, Exception> latUnsafeSupplier) {
		try {
			lat = latUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String lat;

	public String getListingImage() {
		return listingImage;
	}

	public void setListingImage(String listingImage) {
		this.listingImage = listingImage;
	}

	public void setListingImage(
		UnsafeSupplier<String, Exception> listingImageUnsafeSupplier) {

		try {
			listingImage = listingImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String listingImage;

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public void setLon(UnsafeSupplier<String, Exception> lonUnsafeSupplier) {
		try {
			lon = lonUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String lon;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setName(UnsafeSupplier<String, Exception> nameUnsafeSupplier) {
		try {
			name = nameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String name;

	public String getOpeningDays() {
		return openingDays;
	}

	public void setOpeningDays(String openingDays) {
		this.openingDays = openingDays;
	}

	public void setOpeningDays(
		UnsafeSupplier<String, Exception> openingDaysUnsafeSupplier) {

		try {
			openingDays = openingDaysUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String openingDays;

	public String getOpeningHours() {
		return openingHours;
	}

	public void setOpeningHours(String openingHours) {
		this.openingHours = openingHours;
	}

	public void setOpeningHours(
		UnsafeSupplier<String, Exception> openingHoursUnsafeSupplier) {

		try {
			openingHours = openingHoursUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String openingHours;

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public void setVenue(
		UnsafeSupplier<String, Exception> venueUnsafeSupplier) {

		try {
			venue = venueUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String venue;

	@Override
	public Court clone() throws CloneNotSupportedException {
		return (Court)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Court)) {
			return false;
		}

		Court court = (Court)object;

		return Objects.equals(toString(), court.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return CourtSerDes.toJSON(this);
	}

}