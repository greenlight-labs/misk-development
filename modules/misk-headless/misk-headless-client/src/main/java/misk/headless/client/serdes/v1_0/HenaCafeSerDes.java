package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Category;
import misk.headless.client.dto.v1_0.HenaCafe;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HenaCafeSerDes {

	public static HenaCafe toDTO(String json) {
		HenaCafeJSONParser henaCafeJSONParser = new HenaCafeJSONParser();

		return henaCafeJSONParser.parseToDTO(json);
	}

	public static HenaCafe[] toDTOs(String json) {
		HenaCafeJSONParser henaCafeJSONParser = new HenaCafeJSONParser();

		return henaCafeJSONParser.parseToDTOs(json);
	}

	public static String toJSON(HenaCafe henaCafe) {
		if (henaCafe == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (henaCafe.getCategories() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"categories\": ");

			sb.append("[");

			for (int i = 0; i < henaCafe.getCategories().length; i++) {
				sb.append(String.valueOf(henaCafe.getCategories()[i]));

				if ((i + 1) < henaCafe.getCategories().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (henaCafe.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getDescription()));

			sb.append("\"");
		}

		if (henaCafe.getLatitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"latitude\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getLatitude()));

			sb.append("\"");
		}

		if (henaCafe.getLocation() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"location\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getLocation()));

			sb.append("\"");
		}

		if (henaCafe.getLongitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"longitude\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getLongitude()));

			sb.append("\"");
		}

		if (henaCafe.getSliderimage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"sliderimage\": ");

			sb.append("[");

			for (int i = 0; i < henaCafe.getSliderimage().length; i++) {
				sb.append("\"");

				sb.append(_escape(henaCafe.getSliderimage()[i]));

				sb.append("\"");

				if ((i + 1) < henaCafe.getSliderimage().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (henaCafe.getSubtitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"subtitle\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getSubtitle()));

			sb.append("\"");
		}

		if (henaCafe.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getTitle()));

			sb.append("\"");
		}

		if (henaCafe.getWorkingdays() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingdays\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getWorkingdays()));

			sb.append("\"");
		}

		if (henaCafe.getWorkinghours() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workinghours\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getWorkinghours()));

			sb.append("\"");
		}

		if (henaCafe.getWorkingsummary() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"workingsummary\": ");

			sb.append("\"");

			sb.append(_escape(henaCafe.getWorkingsummary()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		HenaCafeJSONParser henaCafeJSONParser = new HenaCafeJSONParser();

		return henaCafeJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(HenaCafe henaCafe) {
		if (henaCafe == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (henaCafe.getCategories() == null) {
			map.put("categories", null);
		}
		else {
			map.put("categories", String.valueOf(henaCafe.getCategories()));
		}

		if (henaCafe.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(henaCafe.getDescription()));
		}

		if (henaCafe.getLatitude() == null) {
			map.put("latitude", null);
		}
		else {
			map.put("latitude", String.valueOf(henaCafe.getLatitude()));
		}

		if (henaCafe.getLocation() == null) {
			map.put("location", null);
		}
		else {
			map.put("location", String.valueOf(henaCafe.getLocation()));
		}

		if (henaCafe.getLongitude() == null) {
			map.put("longitude", null);
		}
		else {
			map.put("longitude", String.valueOf(henaCafe.getLongitude()));
		}

		if (henaCafe.getSliderimage() == null) {
			map.put("sliderimage", null);
		}
		else {
			map.put("sliderimage", String.valueOf(henaCafe.getSliderimage()));
		}

		if (henaCafe.getSubtitle() == null) {
			map.put("subtitle", null);
		}
		else {
			map.put("subtitle", String.valueOf(henaCafe.getSubtitle()));
		}

		if (henaCafe.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(henaCafe.getTitle()));
		}

		if (henaCafe.getWorkingdays() == null) {
			map.put("workingdays", null);
		}
		else {
			map.put("workingdays", String.valueOf(henaCafe.getWorkingdays()));
		}

		if (henaCafe.getWorkinghours() == null) {
			map.put("workinghours", null);
		}
		else {
			map.put("workinghours", String.valueOf(henaCafe.getWorkinghours()));
		}

		if (henaCafe.getWorkingsummary() == null) {
			map.put("workingsummary", null);
		}
		else {
			map.put(
				"workingsummary", String.valueOf(henaCafe.getWorkingsummary()));
		}

		return map;
	}

	public static class HenaCafeJSONParser extends BaseJSONParser<HenaCafe> {

		@Override
		protected HenaCafe createDTO() {
			return new HenaCafe();
		}

		@Override
		protected HenaCafe[] createDTOArray(int size) {
			return new HenaCafe[size];
		}

		@Override
		protected void setField(
			HenaCafe henaCafe, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "categories")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setCategories(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> CategorySerDes.toDTO((String)object)
						).toArray(
							size -> new Category[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "latitude")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setLatitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "location")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setLocation((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "longitude")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setLongitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "sliderimage")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setSliderimage(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "subtitle")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setSubtitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workingdays")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setWorkingdays((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workinghours")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setWorkinghours((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "workingsummary")) {
				if (jsonParserFieldValue != null) {
					henaCafe.setWorkingsummary((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}