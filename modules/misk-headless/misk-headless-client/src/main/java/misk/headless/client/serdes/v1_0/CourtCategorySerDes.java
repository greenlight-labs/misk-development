package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.CourtCategory;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CourtCategorySerDes {

	public static CourtCategory toDTO(String json) {
		CourtCategoryJSONParser courtCategoryJSONParser =
			new CourtCategoryJSONParser();

		return courtCategoryJSONParser.parseToDTO(json);
	}

	public static CourtCategory[] toDTOs(String json) {
		CourtCategoryJSONParser courtCategoryJSONParser =
			new CourtCategoryJSONParser();

		return courtCategoryJSONParser.parseToDTOs(json);
	}

	public static String toJSON(CourtCategory courtCategory) {
		if (courtCategory == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (courtCategory.getBlackIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"blackIcon\": ");

			sb.append("\"");

			sb.append(_escape(courtCategory.getBlackIcon()));

			sb.append("\"");
		}

		if (courtCategory.getIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"icon\": ");

			sb.append("\"");

			sb.append(_escape(courtCategory.getIcon()));

			sb.append("\"");
		}

		if (courtCategory.getId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append(courtCategory.getId());
		}

		if (courtCategory.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(courtCategory.getImage()));

			sb.append("\"");
		}

		if (courtCategory.getLocationId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationId\": ");

			sb.append(courtCategory.getLocationId());
		}

		if (courtCategory.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(courtCategory.getName()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		CourtCategoryJSONParser courtCategoryJSONParser =
			new CourtCategoryJSONParser();

		return courtCategoryJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(CourtCategory courtCategory) {
		if (courtCategory == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (courtCategory.getBlackIcon() == null) {
			map.put("blackIcon", null);
		}
		else {
			map.put("blackIcon", String.valueOf(courtCategory.getBlackIcon()));
		}

		if (courtCategory.getIcon() == null) {
			map.put("icon", null);
		}
		else {
			map.put("icon", String.valueOf(courtCategory.getIcon()));
		}

		if (courtCategory.getId() == null) {
			map.put("id", null);
		}
		else {
			map.put("id", String.valueOf(courtCategory.getId()));
		}

		if (courtCategory.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(courtCategory.getImage()));
		}

		if (courtCategory.getLocationId() == null) {
			map.put("locationId", null);
		}
		else {
			map.put(
				"locationId", String.valueOf(courtCategory.getLocationId()));
		}

		if (courtCategory.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put("name", String.valueOf(courtCategory.getName()));
		}

		return map;
	}

	public static class CourtCategoryJSONParser
		extends BaseJSONParser<CourtCategory> {

		@Override
		protected CourtCategory createDTO() {
			return new CourtCategory();
		}

		@Override
		protected CourtCategory[] createDTOArray(int size) {
			return new CourtCategory[size];
		}

		@Override
		protected void setField(
			CourtCategory courtCategory, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "blackIcon")) {
				if (jsonParserFieldValue != null) {
					courtCategory.setBlackIcon((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "icon")) {
				if (jsonParserFieldValue != null) {
					courtCategory.setIcon((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "id")) {
				if (jsonParserFieldValue != null) {
					courtCategory.setId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					courtCategory.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationId")) {
				if (jsonParserFieldValue != null) {
					courtCategory.setLocationId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					courtCategory.setName((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}