package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.BookingTermsConditionsSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookingTermsConditions implements Cloneable, Serializable {

	public static BookingTermsConditions toDTO(String json) {
		return BookingTermsConditionsSerDes.toDTO(json);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getExcerpt() {
		return excerpt;
	}

	public void setExcerpt(String excerpt) {
		this.excerpt = excerpt;
	}

	public void setExcerpt(
		UnsafeSupplier<String, Exception> excerptUnsafeSupplier) {

		try {
			excerpt = excerptUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String excerpt;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	@Override
	public BookingTermsConditions clone() throws CloneNotSupportedException {
		return (BookingTermsConditions)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BookingTermsConditions)) {
			return false;
		}

		BookingTermsConditions bookingTermsConditions =
			(BookingTermsConditions)object;

		return Objects.equals(toString(), bookingTermsConditions.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return BookingTermsConditionsSerDes.toJSON(this);
	}

}