package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ProfileBannerImage;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ProfileBannerImageSerDes {

	public static ProfileBannerImage toDTO(String json) {
		ProfileBannerImageJSONParser profileBannerImageJSONParser =
			new ProfileBannerImageJSONParser();

		return profileBannerImageJSONParser.parseToDTO(json);
	}

	public static ProfileBannerImage[] toDTOs(String json) {
		ProfileBannerImageJSONParser profileBannerImageJSONParser =
			new ProfileBannerImageJSONParser();

		return profileBannerImageJSONParser.parseToDTOs(json);
	}

	public static String toJSON(ProfileBannerImage profileBannerImage) {
		if (profileBannerImage == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (profileBannerImage.getAppUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(profileBannerImage.getAppUserId());
		}

		if (profileBannerImage.getProfileBannerImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"profileBannerImage\": ");

			sb.append("\"");

			sb.append(_escape(profileBannerImage.getProfileBannerImage()));

			sb.append("\"");
		}

		if (profileBannerImage.getRequestStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"requestStatus\": ");

			sb.append(String.valueOf(profileBannerImage.getRequestStatus()));
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ProfileBannerImageJSONParser profileBannerImageJSONParser =
			new ProfileBannerImageJSONParser();

		return profileBannerImageJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		ProfileBannerImage profileBannerImage) {

		if (profileBannerImage == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (profileBannerImage.getAppUserId() == null) {
			map.put("appUserId", null);
		}
		else {
			map.put(
				"appUserId", String.valueOf(profileBannerImage.getAppUserId()));
		}

		if (profileBannerImage.getProfileBannerImage() == null) {
			map.put("profileBannerImage", null);
		}
		else {
			map.put(
				"profileBannerImage",
				String.valueOf(profileBannerImage.getProfileBannerImage()));
		}

		if (profileBannerImage.getRequestStatus() == null) {
			map.put("requestStatus", null);
		}
		else {
			map.put(
				"requestStatus",
				String.valueOf(profileBannerImage.getRequestStatus()));
		}

		return map;
	}

	public static class ProfileBannerImageJSONParser
		extends BaseJSONParser<ProfileBannerImage> {

		@Override
		protected ProfileBannerImage createDTO() {
			return new ProfileBannerImage();
		}

		@Override
		protected ProfileBannerImage[] createDTOArray(int size) {
			return new ProfileBannerImage[size];
		}

		@Override
		protected void setField(
			ProfileBannerImage profileBannerImage, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "appUserId")) {
				if (jsonParserFieldValue != null) {
					profileBannerImage.setAppUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "profileBannerImage")) {

				if (jsonParserFieldValue != null) {
					profileBannerImage.setProfileBannerImage(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "requestStatus")) {
				if (jsonParserFieldValue != null) {
					profileBannerImage.setRequestStatus(
						RequestStatusSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}