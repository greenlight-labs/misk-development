package misk.headless.client.resource.v1_0;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AddVisitor;
import misk.headless.client.dto.v1_0.CategoryList;
import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.pagination.Pagination;
import misk.headless.client.problem.Problem;
import misk.headless.client.serdes.v1_0.CategoryListSerDes;
import misk.headless.client.serdes.v1_0.EventListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public interface EventListResource {

	public static Builder builder() {
		return new Builder();
	}

	public Page<EventList> getEventsListing(
			String languageId, String typeevent, Integer payType,
			String priceFrom, String priceTo, Long categoryId, String dateFrom,
			String dateTo, Pagination pagination)
		throws Exception;

	public HttpInvoker.HttpResponse getEventsListingHttpResponse(
			String languageId, String typeevent, Integer payType,
			String priceFrom, String priceTo, Long categoryId, String dateFrom,
			String dateTo, Pagination pagination)
		throws Exception;

	public EventList getEventsDetails(Long eventid, String languageId)
		throws Exception;

	public HttpInvoker.HttpResponse getEventsDetailsHttpResponse(
			Long eventid, String languageId)
		throws Exception;

	public AddVisitor getAttendeesList(Long eventid, String languageId)
		throws Exception;

	public HttpInvoker.HttpResponse getAttendeesListHttpResponse(
			Long eventid, String languageId)
		throws Exception;

	public AddVisitor AddEventsVisitor(AddVisitor addVisitor) throws Exception;

	public HttpInvoker.HttpResponse AddEventsVisitorHttpResponse(
			AddVisitor addVisitor)
		throws Exception;

	public Page<CategoryList> getEventCategories(String languageId)
		throws Exception;

	public HttpInvoker.HttpResponse getEventCategoriesHttpResponse(
			String languageId)
		throws Exception;

	public static class Builder {

		public Builder authentication(String login, String password) {
			_login = login;
			_password = password;

			return this;
		}

		public EventListResource build() {
			return new EventListResourceImpl(this);
		}

		public Builder endpoint(String host, int port, String scheme) {
			_host = host;
			_port = port;
			_scheme = scheme;

			return this;
		}

		public Builder header(String key, String value) {
			_headers.put(key, value);

			return this;
		}

		public Builder locale(Locale locale) {
			_locale = locale;

			return this;
		}

		public Builder parameter(String key, String value) {
			_parameters.put(key, value);

			return this;
		}

		public Builder parameters(String... parameters) {
			if ((parameters.length % 2) != 0) {
				throw new IllegalArgumentException(
					"Parameters length is not an even number");
			}

			for (int i = 0; i < parameters.length; i += 2) {
				String parameterName = String.valueOf(parameters[i]);
				String parameterValue = String.valueOf(parameters[i + 1]);

				_parameters.put(parameterName, parameterValue);
			}

			return this;
		}

		private Builder() {
		}

		private Map<String, String> _headers = new LinkedHashMap<>();
		private String _host = "localhost";
		private Locale _locale;
		private String _login = "";
		private String _password = "";
		private Map<String, String> _parameters = new LinkedHashMap<>();
		private int _port = 8080;
		private String _scheme = "http";

	}

	public static class EventListResourceImpl implements EventListResource {

		public Page<EventList> getEventsListing(
				String languageId, String typeevent, Integer payType,
				String priceFrom, String priceTo, Long categoryId,
				String dateFrom, String dateTo, Pagination pagination)
			throws Exception {

			HttpInvoker.HttpResponse httpResponse =
				getEventsListingHttpResponse(
					languageId, typeevent, payType, priceFrom, priceTo,
					categoryId, dateFrom, dateTo, pagination);

			String content = httpResponse.getContent();

			if ((httpResponse.getStatusCode() / 100) != 2) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response content: " + content);
				_logger.log(
					Level.WARNING,
					"HTTP response message: " + httpResponse.getMessage());
				_logger.log(
					Level.WARNING,
					"HTTP response status code: " +
						httpResponse.getStatusCode());

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
			else {
				_logger.fine("HTTP response content: " + content);
				_logger.fine(
					"HTTP response message: " + httpResponse.getMessage());
				_logger.fine(
					"HTTP response status code: " +
						httpResponse.getStatusCode());
			}

			try {
				return Page.of(content, EventListSerDes::toDTO);
			}
			catch (Exception e) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response: " + content, e);

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
		}

		public HttpInvoker.HttpResponse getEventsListingHttpResponse(
				String languageId, String typeevent, Integer payType,
				String priceFrom, String priceTo, Long categoryId,
				String dateFrom, String dateTo, Pagination pagination)
			throws Exception {

			HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

			if (_builder._locale != null) {
				httpInvoker.header(
					"Accept-Language", _builder._locale.toLanguageTag());
			}

			for (Map.Entry<String, String> entry :
					_builder._headers.entrySet()) {

				httpInvoker.header(entry.getKey(), entry.getValue());
			}

			for (Map.Entry<String, String> entry :
					_builder._parameters.entrySet()) {

				httpInvoker.parameter(entry.getKey(), entry.getValue());
			}

			httpInvoker.httpMethod(HttpInvoker.HttpMethod.GET);

			if (languageId != null) {
				httpInvoker.parameter("languageId", String.valueOf(languageId));
			}

			if (typeevent != null) {
				httpInvoker.parameter("typeevent", String.valueOf(typeevent));
			}

			if (payType != null) {
				httpInvoker.parameter("payType", String.valueOf(payType));
			}

			if (priceFrom != null) {
				httpInvoker.parameter("priceFrom", String.valueOf(priceFrom));
			}

			if (priceTo != null) {
				httpInvoker.parameter("priceTo", String.valueOf(priceTo));
			}

			if (categoryId != null) {
				httpInvoker.parameter("categoryId", String.valueOf(categoryId));
			}

			if (dateFrom != null) {
				httpInvoker.parameter("dateFrom", String.valueOf(dateFrom));
			}

			if (dateTo != null) {
				httpInvoker.parameter("dateTo", String.valueOf(dateTo));
			}

			if (pagination != null) {
				httpInvoker.parameter(
					"page", String.valueOf(pagination.getPage()));
				httpInvoker.parameter(
					"pageSize", String.valueOf(pagination.getPageSize()));
			}

			httpInvoker.path(
				_builder._scheme + "://" + _builder._host + ":" +
					_builder._port + "/o/misk-headless/v1.0/EventsListing");

			httpInvoker.userNameAndPassword(
				_builder._login + ":" + _builder._password);

			return httpInvoker.invoke();
		}

		public EventList getEventsDetails(Long eventid, String languageId)
			throws Exception {

			HttpInvoker.HttpResponse httpResponse =
				getEventsDetailsHttpResponse(eventid, languageId);

			String content = httpResponse.getContent();

			if ((httpResponse.getStatusCode() / 100) != 2) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response content: " + content);
				_logger.log(
					Level.WARNING,
					"HTTP response message: " + httpResponse.getMessage());
				_logger.log(
					Level.WARNING,
					"HTTP response status code: " +
						httpResponse.getStatusCode());

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
			else {
				_logger.fine("HTTP response content: " + content);
				_logger.fine(
					"HTTP response message: " + httpResponse.getMessage());
				_logger.fine(
					"HTTP response status code: " +
						httpResponse.getStatusCode());
			}

			try {
				return EventListSerDes.toDTO(content);
			}
			catch (Exception e) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response: " + content, e);

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
		}

		public HttpInvoker.HttpResponse getEventsDetailsHttpResponse(
				Long eventid, String languageId)
			throws Exception {

			HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

			if (_builder._locale != null) {
				httpInvoker.header(
					"Accept-Language", _builder._locale.toLanguageTag());
			}

			for (Map.Entry<String, String> entry :
					_builder._headers.entrySet()) {

				httpInvoker.header(entry.getKey(), entry.getValue());
			}

			for (Map.Entry<String, String> entry :
					_builder._parameters.entrySet()) {

				httpInvoker.parameter(entry.getKey(), entry.getValue());
			}

			httpInvoker.httpMethod(HttpInvoker.HttpMethod.GET);

			if (languageId != null) {
				httpInvoker.parameter("languageId", String.valueOf(languageId));
			}

			httpInvoker.path(
				_builder._scheme + "://" + _builder._host + ":" +
					_builder._port +
						"/o/misk-headless/v1.0/EventsDetails/{eventid}");

			httpInvoker.path("eventid", eventid);

			httpInvoker.userNameAndPassword(
				_builder._login + ":" + _builder._password);

			return httpInvoker.invoke();
		}

		public AddVisitor getAttendeesList(Long eventid, String languageId)
			throws Exception {

			HttpInvoker.HttpResponse httpResponse =
				getAttendeesListHttpResponse(eventid, languageId);

			String content = httpResponse.getContent();

			if ((httpResponse.getStatusCode() / 100) != 2) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response content: " + content);
				_logger.log(
					Level.WARNING,
					"HTTP response message: " + httpResponse.getMessage());
				_logger.log(
					Level.WARNING,
					"HTTP response status code: " +
						httpResponse.getStatusCode());

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
			else {
				_logger.fine("HTTP response content: " + content);
				_logger.fine(
					"HTTP response message: " + httpResponse.getMessage());
				_logger.fine(
					"HTTP response status code: " +
						httpResponse.getStatusCode());
			}

			try {
				return misk.headless.client.serdes.v1_0.AddVisitorSerDes.toDTO(
					content);
			}
			catch (Exception e) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response: " + content, e);

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
		}

		public HttpInvoker.HttpResponse getAttendeesListHttpResponse(
				Long eventid, String languageId)
			throws Exception {

			HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

			if (_builder._locale != null) {
				httpInvoker.header(
					"Accept-Language", _builder._locale.toLanguageTag());
			}

			for (Map.Entry<String, String> entry :
					_builder._headers.entrySet()) {

				httpInvoker.header(entry.getKey(), entry.getValue());
			}

			for (Map.Entry<String, String> entry :
					_builder._parameters.entrySet()) {

				httpInvoker.parameter(entry.getKey(), entry.getValue());
			}

			httpInvoker.httpMethod(HttpInvoker.HttpMethod.GET);

			if (languageId != null) {
				httpInvoker.parameter("languageId", String.valueOf(languageId));
			}

			httpInvoker.path(
				_builder._scheme + "://" + _builder._host + ":" +
					_builder._port +
						"/o/misk-headless/v1.0/AttendesList/{eventid}");

			httpInvoker.path("eventid", eventid);

			httpInvoker.userNameAndPassword(
				_builder._login + ":" + _builder._password);

			return httpInvoker.invoke();
		}

		public AddVisitor AddEventsVisitor(AddVisitor addVisitor)
			throws Exception {

			HttpInvoker.HttpResponse httpResponse =
				AddEventsVisitorHttpResponse(addVisitor);

			String content = httpResponse.getContent();

			if ((httpResponse.getStatusCode() / 100) != 2) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response content: " + content);
				_logger.log(
					Level.WARNING,
					"HTTP response message: " + httpResponse.getMessage());
				_logger.log(
					Level.WARNING,
					"HTTP response status code: " +
						httpResponse.getStatusCode());

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
			else {
				_logger.fine("HTTP response content: " + content);
				_logger.fine(
					"HTTP response message: " + httpResponse.getMessage());
				_logger.fine(
					"HTTP response status code: " +
						httpResponse.getStatusCode());
			}

			try {
				return misk.headless.client.serdes.v1_0.AddVisitorSerDes.toDTO(
					content);
			}
			catch (Exception e) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response: " + content, e);

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
		}

		public HttpInvoker.HttpResponse AddEventsVisitorHttpResponse(
				AddVisitor addVisitor)
			throws Exception {

			HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

			httpInvoker.body(addVisitor.toString(), "application/json");

			if (_builder._locale != null) {
				httpInvoker.header(
					"Accept-Language", _builder._locale.toLanguageTag());
			}

			for (Map.Entry<String, String> entry :
					_builder._headers.entrySet()) {

				httpInvoker.header(entry.getKey(), entry.getValue());
			}

			for (Map.Entry<String, String> entry :
					_builder._parameters.entrySet()) {

				httpInvoker.parameter(entry.getKey(), entry.getValue());
			}

			httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);

			httpInvoker.path(
				_builder._scheme + "://" + _builder._host + ":" +
					_builder._port + "/o/misk-headless/v1.0/addeventvisitor");

			httpInvoker.userNameAndPassword(
				_builder._login + ":" + _builder._password);

			return httpInvoker.invoke();
		}

		public Page<CategoryList> getEventCategories(String languageId)
			throws Exception {

			HttpInvoker.HttpResponse httpResponse =
				getEventCategoriesHttpResponse(languageId);

			String content = httpResponse.getContent();

			if ((httpResponse.getStatusCode() / 100) != 2) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response content: " + content);
				_logger.log(
					Level.WARNING,
					"HTTP response message: " + httpResponse.getMessage());
				_logger.log(
					Level.WARNING,
					"HTTP response status code: " +
						httpResponse.getStatusCode());

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
			else {
				_logger.fine("HTTP response content: " + content);
				_logger.fine(
					"HTTP response message: " + httpResponse.getMessage());
				_logger.fine(
					"HTTP response status code: " +
						httpResponse.getStatusCode());
			}

			try {
				return Page.of(content, CategoryListSerDes::toDTO);
			}
			catch (Exception e) {
				_logger.log(
					Level.WARNING,
					"Unable to process HTTP response: " + content, e);

				throw new Problem.ProblemException(Problem.toDTO(content));
			}
		}

		public HttpInvoker.HttpResponse getEventCategoriesHttpResponse(
				String languageId)
			throws Exception {

			HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

			if (_builder._locale != null) {
				httpInvoker.header(
					"Accept-Language", _builder._locale.toLanguageTag());
			}

			for (Map.Entry<String, String> entry :
					_builder._headers.entrySet()) {

				httpInvoker.header(entry.getKey(), entry.getValue());
			}

			for (Map.Entry<String, String> entry :
					_builder._parameters.entrySet()) {

				httpInvoker.parameter(entry.getKey(), entry.getValue());
			}

			httpInvoker.httpMethod(HttpInvoker.HttpMethod.GET);

			if (languageId != null) {
				httpInvoker.parameter("languageId", String.valueOf(languageId));
			}

			httpInvoker.path(
				_builder._scheme + "://" + _builder._host + ":" +
					_builder._port + "/o/misk-headless/v1.0/event/categories");

			httpInvoker.userNameAndPassword(
				_builder._login + ":" + _builder._password);

			return httpInvoker.invoke();
		}

		private EventListResourceImpl(Builder builder) {
			_builder = builder;
		}

		private static final Logger _logger = Logger.getLogger(
			EventListResource.class.getName());

		private Builder _builder;

	}

}