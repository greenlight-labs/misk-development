package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Gallery;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class GallerySerDes {

	public static Gallery toDTO(String json) {
		GalleryJSONParser galleryJSONParser = new GalleryJSONParser();

		return galleryJSONParser.parseToDTO(json);
	}

	public static Gallery[] toDTOs(String json) {
		GalleryJSONParser galleryJSONParser = new GalleryJSONParser();

		return galleryJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Gallery gallery) {
		if (gallery == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (gallery.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(gallery.getImage()));

			sb.append("\"");
		}

		if (gallery.getThumbnail() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"thumbnail\": ");

			sb.append("\"");

			sb.append(_escape(gallery.getThumbnail()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		GalleryJSONParser galleryJSONParser = new GalleryJSONParser();

		return galleryJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Gallery gallery) {
		if (gallery == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (gallery.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(gallery.getImage()));
		}

		if (gallery.getThumbnail() == null) {
			map.put("thumbnail", null);
		}
		else {
			map.put("thumbnail", String.valueOf(gallery.getThumbnail()));
		}

		return map;
	}

	public static class GalleryJSONParser extends BaseJSONParser<Gallery> {

		@Override
		protected Gallery createDTO() {
			return new Gallery();
		}

		@Override
		protected Gallery[] createDTOArray(int size) {
			return new Gallery[size];
		}

		@Override
		protected void setField(
			Gallery gallery, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					gallery.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "thumbnail")) {
				if (jsonParserFieldValue != null) {
					gallery.setThumbnail((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}