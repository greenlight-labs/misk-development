package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.BannerImages;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BannerImagesSerDes {

	public static BannerImages toDTO(String json) {
		BannerImagesJSONParser bannerImagesJSONParser =
			new BannerImagesJSONParser();

		return bannerImagesJSONParser.parseToDTO(json);
	}

	public static BannerImages[] toDTOs(String json) {
		BannerImagesJSONParser bannerImagesJSONParser =
			new BannerImagesJSONParser();

		return bannerImagesJSONParser.parseToDTOs(json);
	}

	public static String toJSON(BannerImages bannerImages) {
		if (bannerImages == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (bannerImages.getFavoritesBannerImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"favoritesBannerImage\": ");

			sb.append("\"");

			sb.append(_escape(bannerImages.getFavoritesBannerImage()));

			sb.append("\"");
		}

		if (bannerImages.getSouvenirsImageBanner() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"souvenirsImageBanner\": ");

			sb.append("\"");

			sb.append(_escape(bannerImages.getSouvenirsImageBanner()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		BannerImagesJSONParser bannerImagesJSONParser =
			new BannerImagesJSONParser();

		return bannerImagesJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(BannerImages bannerImages) {
		if (bannerImages == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (bannerImages.getFavoritesBannerImage() == null) {
			map.put("favoritesBannerImage", null);
		}
		else {
			map.put(
				"favoritesBannerImage",
				String.valueOf(bannerImages.getFavoritesBannerImage()));
		}

		if (bannerImages.getSouvenirsImageBanner() == null) {
			map.put("souvenirsImageBanner", null);
		}
		else {
			map.put(
				"souvenirsImageBanner",
				String.valueOf(bannerImages.getSouvenirsImageBanner()));
		}

		return map;
	}

	public static class BannerImagesJSONParser
		extends BaseJSONParser<BannerImages> {

		@Override
		protected BannerImages createDTO() {
			return new BannerImages();
		}

		@Override
		protected BannerImages[] createDTOArray(int size) {
			return new BannerImages[size];
		}

		@Override
		protected void setField(
			BannerImages bannerImages, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "favoritesBannerImage")) {
				if (jsonParserFieldValue != null) {
					bannerImages.setFavoritesBannerImage(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "souvenirsImageBanner")) {

				if (jsonParserFieldValue != null) {
					bannerImages.setSouvenirsImageBanner(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}