package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.CategoryList;
import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.dto.v1_0.Schedule;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class EventListSerDes {

	public static EventList toDTO(String json) {
		EventListJSONParser eventListJSONParser = new EventListJSONParser();

		return eventListJSONParser.parseToDTO(json);
	}

	public static EventList[] toDTOs(String json) {
		EventListJSONParser eventListJSONParser = new EventListJSONParser();

		return eventListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(EventList eventList) {
		if (eventList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (eventList.getAttendesList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attendesList\": ");

			sb.append(String.valueOf(eventList.getAttendesList()));
		}

		if (eventList.getBuyTicketLink() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"buyTicketLink\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getBuyTicketLink()));

			sb.append("\"");
		}

		if (eventList.getCatColor() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catColor\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getCatColor()));

			sb.append("\"");
		}

		if (eventList.getCategoryLists() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"categoryLists\": ");

			sb.append("[");

			for (int i = 0; i < eventList.getCategoryLists().length; i++) {
				sb.append(String.valueOf(eventList.getCategoryLists()[i]));

				if ((i + 1) < eventList.getCategoryLists().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (eventList.getCurrency() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"currency\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getCurrency()));

			sb.append("\"");
		}

		if (eventList.getDisplaydate() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"displaydate\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getDisplaydate()));

			sb.append("\"");
		}

		if (eventList.getEmail() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"email\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEmail()));

			sb.append("\"");
		}

		if (eventList.getEventDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventDescription\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventDescription()));

			sb.append("\"");
		}

		if (eventList.getEventEndDateTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventEndDateTime\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventEndDateTime()));

			sb.append("\"");
		}

		if (eventList.getEventFeaturedImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventFeaturedImage\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventFeaturedImage()));

			sb.append("\"");
		}

		if (eventList.getEventId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventId\": ");

			sb.append(eventList.getEventId());
		}

		if (eventList.getEventLists() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventLists\": ");

			sb.append("[");

			for (int i = 0; i < eventList.getEventLists().length; i++) {
				sb.append(String.valueOf(eventList.getEventLists()[i]));

				if ((i + 1) < eventList.getEventLists().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (eventList.getEventLocation() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventLocation\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventLocation()));

			sb.append("\"");
		}

		if (eventList.getEventMonth() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventMonth\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventMonth()));

			sb.append("\"");
		}

		if (eventList.getEventPrice() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventPrice\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventPrice()));

			sb.append("\"");
		}

		if (eventList.getEventStartDateTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventStartDateTime\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventStartDateTime()));

			sb.append("\"");
		}

		if (eventList.getEventType() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventType\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventType()));

			sb.append("\"");
		}

		if (eventList.getEventcategory() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventcategory\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getEventcategory()));

			sb.append("\"");
		}

		if (eventList.getIsFavourite() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"isFavourite\": ");

			sb.append(eventList.getIsFavourite());
		}

		if (eventList.getLan() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lan\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getLan()));

			sb.append("\"");
		}

		if (eventList.getLat() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"lat\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getLat()));

			sb.append("\"");
		}

		if (eventList.getLink() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"link\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getLink()));

			sb.append("\"");
		}

		if (eventList.getLocationLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLabel\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getLocationLabel()));

			sb.append("\"");
		}

		if (eventList.getPayType() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"payType\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getPayType()));

			sb.append("\"");
		}

		if (eventList.getPhone() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"phone\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getPhone()));

			sb.append("\"");
		}

		if (eventList.getSchedules() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"schedules\": ");

			sb.append("[");

			for (int i = 0; i < eventList.getSchedules().length; i++) {
				sb.append(String.valueOf(eventList.getSchedules()[i]));

				if ((i + 1) < eventList.getSchedules().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (eventList.getSliderimage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"sliderimage\": ");

			sb.append("[");

			for (int i = 0; i < eventList.getSliderimage().length; i++) {
				sb.append("\"");

				sb.append(_escape(eventList.getSliderimage()[i]));

				sb.append("\"");

				if ((i + 1) < eventList.getSliderimage().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (eventList.getTagSlug() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"tagSlug\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getTagSlug()));

			sb.append("\"");
		}

		if (eventList.getTagTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"tagTitle\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getTagTitle()));

			sb.append("\"");
		}

		if (eventList.getTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"time\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getTime()));

			sb.append("\"");
		}

		if (eventList.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getTitle()));

			sb.append("\"");
		}

		if (eventList.getVenue() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"venue\": ");

			sb.append("\"");

			sb.append(_escape(eventList.getVenue()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		EventListJSONParser eventListJSONParser = new EventListJSONParser();

		return eventListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(EventList eventList) {
		if (eventList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (eventList.getAttendesList() == null) {
			map.put("attendesList", null);
		}
		else {
			map.put(
				"attendesList", String.valueOf(eventList.getAttendesList()));
		}

		if (eventList.getBuyTicketLink() == null) {
			map.put("buyTicketLink", null);
		}
		else {
			map.put(
				"buyTicketLink", String.valueOf(eventList.getBuyTicketLink()));
		}

		if (eventList.getCatColor() == null) {
			map.put("catColor", null);
		}
		else {
			map.put("catColor", String.valueOf(eventList.getCatColor()));
		}

		if (eventList.getCategoryLists() == null) {
			map.put("categoryLists", null);
		}
		else {
			map.put(
				"categoryLists", String.valueOf(eventList.getCategoryLists()));
		}

		if (eventList.getCurrency() == null) {
			map.put("currency", null);
		}
		else {
			map.put("currency", String.valueOf(eventList.getCurrency()));
		}

		if (eventList.getDisplaydate() == null) {
			map.put("displaydate", null);
		}
		else {
			map.put("displaydate", String.valueOf(eventList.getDisplaydate()));
		}

		if (eventList.getEmail() == null) {
			map.put("email", null);
		}
		else {
			map.put("email", String.valueOf(eventList.getEmail()));
		}

		if (eventList.getEventDescription() == null) {
			map.put("eventDescription", null);
		}
		else {
			map.put(
				"eventDescription",
				String.valueOf(eventList.getEventDescription()));
		}

		if (eventList.getEventEndDateTime() == null) {
			map.put("eventEndDateTime", null);
		}
		else {
			map.put(
				"eventEndDateTime",
				String.valueOf(eventList.getEventEndDateTime()));
		}

		if (eventList.getEventFeaturedImage() == null) {
			map.put("eventFeaturedImage", null);
		}
		else {
			map.put(
				"eventFeaturedImage",
				String.valueOf(eventList.getEventFeaturedImage()));
		}

		if (eventList.getEventId() == null) {
			map.put("eventId", null);
		}
		else {
			map.put("eventId", String.valueOf(eventList.getEventId()));
		}

		if (eventList.getEventLists() == null) {
			map.put("eventLists", null);
		}
		else {
			map.put("eventLists", String.valueOf(eventList.getEventLists()));
		}

		if (eventList.getEventLocation() == null) {
			map.put("eventLocation", null);
		}
		else {
			map.put(
				"eventLocation", String.valueOf(eventList.getEventLocation()));
		}

		if (eventList.getEventMonth() == null) {
			map.put("eventMonth", null);
		}
		else {
			map.put("eventMonth", String.valueOf(eventList.getEventMonth()));
		}

		if (eventList.getEventPrice() == null) {
			map.put("eventPrice", null);
		}
		else {
			map.put("eventPrice", String.valueOf(eventList.getEventPrice()));
		}

		if (eventList.getEventStartDateTime() == null) {
			map.put("eventStartDateTime", null);
		}
		else {
			map.put(
				"eventStartDateTime",
				String.valueOf(eventList.getEventStartDateTime()));
		}

		if (eventList.getEventType() == null) {
			map.put("eventType", null);
		}
		else {
			map.put("eventType", String.valueOf(eventList.getEventType()));
		}

		if (eventList.getEventcategory() == null) {
			map.put("eventcategory", null);
		}
		else {
			map.put(
				"eventcategory", String.valueOf(eventList.getEventcategory()));
		}

		if (eventList.getIsFavourite() == null) {
			map.put("isFavourite", null);
		}
		else {
			map.put("isFavourite", String.valueOf(eventList.getIsFavourite()));
		}

		if (eventList.getLan() == null) {
			map.put("lan", null);
		}
		else {
			map.put("lan", String.valueOf(eventList.getLan()));
		}

		if (eventList.getLat() == null) {
			map.put("lat", null);
		}
		else {
			map.put("lat", String.valueOf(eventList.getLat()));
		}

		if (eventList.getLink() == null) {
			map.put("link", null);
		}
		else {
			map.put("link", String.valueOf(eventList.getLink()));
		}

		if (eventList.getLocationLabel() == null) {
			map.put("locationLabel", null);
		}
		else {
			map.put(
				"locationLabel", String.valueOf(eventList.getLocationLabel()));
		}

		if (eventList.getPayType() == null) {
			map.put("payType", null);
		}
		else {
			map.put("payType", String.valueOf(eventList.getPayType()));
		}

		if (eventList.getPhone() == null) {
			map.put("phone", null);
		}
		else {
			map.put("phone", String.valueOf(eventList.getPhone()));
		}

		if (eventList.getSchedules() == null) {
			map.put("schedules", null);
		}
		else {
			map.put("schedules", String.valueOf(eventList.getSchedules()));
		}

		if (eventList.getSliderimage() == null) {
			map.put("sliderimage", null);
		}
		else {
			map.put("sliderimage", String.valueOf(eventList.getSliderimage()));
		}

		if (eventList.getTagSlug() == null) {
			map.put("tagSlug", null);
		}
		else {
			map.put("tagSlug", String.valueOf(eventList.getTagSlug()));
		}

		if (eventList.getTagTitle() == null) {
			map.put("tagTitle", null);
		}
		else {
			map.put("tagTitle", String.valueOf(eventList.getTagTitle()));
		}

		if (eventList.getTime() == null) {
			map.put("time", null);
		}
		else {
			map.put("time", String.valueOf(eventList.getTime()));
		}

		if (eventList.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(eventList.getTitle()));
		}

		if (eventList.getVenue() == null) {
			map.put("venue", null);
		}
		else {
			map.put("venue", String.valueOf(eventList.getVenue()));
		}

		return map;
	}

	public static class EventListJSONParser extends BaseJSONParser<EventList> {

		@Override
		protected EventList createDTO() {
			return new EventList();
		}

		@Override
		protected EventList[] createDTOArray(int size) {
			return new EventList[size];
		}

		@Override
		protected void setField(
			EventList eventList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "attendesList")) {
				if (jsonParserFieldValue != null) {
					eventList.setAttendesList(
						AddVisitorSerDes.toDTO((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "buyTicketLink")) {
				if (jsonParserFieldValue != null) {
					eventList.setBuyTicketLink((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "catColor")) {
				if (jsonParserFieldValue != null) {
					eventList.setCatColor((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "categoryLists")) {
				if (jsonParserFieldValue != null) {
					eventList.setCategoryLists(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> CategoryListSerDes.toDTO((String)object)
						).toArray(
							size -> new CategoryList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "currency")) {
				if (jsonParserFieldValue != null) {
					eventList.setCurrency((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "displaydate")) {
				if (jsonParserFieldValue != null) {
					eventList.setDisplaydate((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "email")) {
				if (jsonParserFieldValue != null) {
					eventList.setEmail((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventDescription")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventEndDateTime")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventEndDateTime((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "eventFeaturedImage")) {

				if (jsonParserFieldValue != null) {
					eventList.setEventFeaturedImage(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventId")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventLists")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventLists(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> EventListSerDes.toDTO((String)object)
						).toArray(
							size -> new EventList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventLocation")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventLocation((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventMonth")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventMonth((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventPrice")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventPrice((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "eventStartDateTime")) {

				if (jsonParserFieldValue != null) {
					eventList.setEventStartDateTime(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventType")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventType((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventcategory")) {
				if (jsonParserFieldValue != null) {
					eventList.setEventcategory((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "isFavourite")) {
				if (jsonParserFieldValue != null) {
					eventList.setIsFavourite((Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "lan")) {
				if (jsonParserFieldValue != null) {
					eventList.setLan((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "lat")) {
				if (jsonParserFieldValue != null) {
					eventList.setLat((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "link")) {
				if (jsonParserFieldValue != null) {
					eventList.setLink((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationLabel")) {
				if (jsonParserFieldValue != null) {
					eventList.setLocationLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "payType")) {
				if (jsonParserFieldValue != null) {
					eventList.setPayType((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "phone")) {
				if (jsonParserFieldValue != null) {
					eventList.setPhone((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "schedules")) {
				if (jsonParserFieldValue != null) {
					eventList.setSchedules(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> ScheduleSerDes.toDTO((String)object)
						).toArray(
							size -> new Schedule[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "sliderimage")) {
				if (jsonParserFieldValue != null) {
					eventList.setSliderimage(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "tagSlug")) {
				if (jsonParserFieldValue != null) {
					eventList.setTagSlug((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "tagTitle")) {
				if (jsonParserFieldValue != null) {
					eventList.setTagTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "time")) {
				if (jsonParserFieldValue != null) {
					eventList.setTime((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					eventList.setTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "venue")) {
				if (jsonParserFieldValue != null) {
					eventList.setVenue((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}