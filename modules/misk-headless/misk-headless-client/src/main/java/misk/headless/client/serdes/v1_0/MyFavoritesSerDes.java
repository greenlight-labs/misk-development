package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.dto.v1_0.Highlight;
import misk.headless.client.dto.v1_0.MyFavorites;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MyFavoritesSerDes {

	public static MyFavorites toDTO(String json) {
		MyFavoritesJSONParser myFavoritesJSONParser =
			new MyFavoritesJSONParser();

		return myFavoritesJSONParser.parseToDTO(json);
	}

	public static MyFavorites[] toDTOs(String json) {
		MyFavoritesJSONParser myFavoritesJSONParser =
			new MyFavoritesJSONParser();

		return myFavoritesJSONParser.parseToDTOs(json);
	}

	public static String toJSON(MyFavorites myFavorites) {
		if (myFavorites == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (myFavorites.getEventLists() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventLists\": ");

			sb.append("[");

			for (int i = 0; i < myFavorites.getEventLists().length; i++) {
				sb.append(String.valueOf(myFavorites.getEventLists()[i]));

				if ((i + 1) < myFavorites.getEventLists().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (myFavorites.getHighlights() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlights\": ");

			sb.append("[");

			for (int i = 0; i < myFavorites.getHighlights().length; i++) {
				sb.append(String.valueOf(myFavorites.getHighlights()[i]));

				if ((i + 1) < myFavorites.getHighlights().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		MyFavoritesJSONParser myFavoritesJSONParser =
			new MyFavoritesJSONParser();

		return myFavoritesJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(MyFavorites myFavorites) {
		if (myFavorites == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (myFavorites.getEventLists() == null) {
			map.put("eventLists", null);
		}
		else {
			map.put("eventLists", String.valueOf(myFavorites.getEventLists()));
		}

		if (myFavorites.getHighlights() == null) {
			map.put("highlights", null);
		}
		else {
			map.put("highlights", String.valueOf(myFavorites.getHighlights()));
		}

		return map;
	}

	public static class MyFavoritesJSONParser
		extends BaseJSONParser<MyFavorites> {

		@Override
		protected MyFavorites createDTO() {
			return new MyFavorites();
		}

		@Override
		protected MyFavorites[] createDTOArray(int size) {
			return new MyFavorites[size];
		}

		@Override
		protected void setField(
			MyFavorites myFavorites, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "eventLists")) {
				if (jsonParserFieldValue != null) {
					myFavorites.setEventLists(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> EventListSerDes.toDTO((String)object)
						).toArray(
							size -> new EventList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "highlights")) {
				if (jsonParserFieldValue != null) {
					myFavorites.setHighlights(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> HighlightSerDes.toDTO((String)object)
						).toArray(
							size -> new Highlight[size]
						));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}