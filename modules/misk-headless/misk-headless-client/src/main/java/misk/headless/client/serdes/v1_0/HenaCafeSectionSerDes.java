package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.HenaCafeSection;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HenaCafeSectionSerDes {

	public static HenaCafeSection toDTO(String json) {
		HenaCafeSectionJSONParser henaCafeSectionJSONParser =
			new HenaCafeSectionJSONParser();

		return henaCafeSectionJSONParser.parseToDTO(json);
	}

	public static HenaCafeSection[] toDTOs(String json) {
		HenaCafeSectionJSONParser henaCafeSectionJSONParser =
			new HenaCafeSectionJSONParser();

		return henaCafeSectionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(HenaCafeSection henaCafeSection) {
		if (henaCafeSection == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (henaCafeSection.getIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"icon\": ");

			sb.append("\"");

			sb.append(_escape(henaCafeSection.getIcon()));

			sb.append("\"");
		}

		if (henaCafeSection.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(henaCafeSection.getImage()));

			sb.append("\"");
		}

		if (henaCafeSection.getSectionTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"sectionTitle\": ");

			sb.append("\"");

			sb.append(_escape(henaCafeSection.getSectionTitle()));

			sb.append("\"");
		}

		if (henaCafeSection.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(henaCafeSection.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		HenaCafeSectionJSONParser henaCafeSectionJSONParser =
			new HenaCafeSectionJSONParser();

		return henaCafeSectionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(HenaCafeSection henaCafeSection) {
		if (henaCafeSection == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (henaCafeSection.getIcon() == null) {
			map.put("icon", null);
		}
		else {
			map.put("icon", String.valueOf(henaCafeSection.getIcon()));
		}

		if (henaCafeSection.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(henaCafeSection.getImage()));
		}

		if (henaCafeSection.getSectionTitle() == null) {
			map.put("sectionTitle", null);
		}
		else {
			map.put(
				"sectionTitle",
				String.valueOf(henaCafeSection.getSectionTitle()));
		}

		if (henaCafeSection.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(henaCafeSection.getTitle()));
		}

		return map;
	}

	public static class HenaCafeSectionJSONParser
		extends BaseJSONParser<HenaCafeSection> {

		@Override
		protected HenaCafeSection createDTO() {
			return new HenaCafeSection();
		}

		@Override
		protected HenaCafeSection[] createDTOArray(int size) {
			return new HenaCafeSection[size];
		}

		@Override
		protected void setField(
			HenaCafeSection henaCafeSection, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "icon")) {
				if (jsonParserFieldValue != null) {
					henaCafeSection.setIcon((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					henaCafeSection.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "sectionTitle")) {
				if (jsonParserFieldValue != null) {
					henaCafeSection.setSectionTitle(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					henaCafeSection.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}