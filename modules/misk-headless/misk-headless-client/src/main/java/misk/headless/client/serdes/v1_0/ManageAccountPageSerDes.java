package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ManageAccountPage;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ManageAccountPageSerDes {

	public static ManageAccountPage toDTO(String json) {
		ManageAccountPageJSONParser manageAccountPageJSONParser =
			new ManageAccountPageJSONParser();

		return manageAccountPageJSONParser.parseToDTO(json);
	}

	public static ManageAccountPage[] toDTOs(String json) {
		ManageAccountPageJSONParser manageAccountPageJSONParser =
			new ManageAccountPageJSONParser();

		return manageAccountPageJSONParser.parseToDTOs(json);
	}

	public static String toJSON(ManageAccountPage manageAccountPage) {
		if (manageAccountPage == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (manageAccountPage.getDeactivateAccountLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deactivateAccountLabel\": ");

			sb.append("\"");

			sb.append(_escape(manageAccountPage.getDeactivateAccountLabel()));

			sb.append("\"");
		}

		if (manageAccountPage.getDeleteAccountLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"deleteAccountLabel\": ");

			sb.append("\"");

			sb.append(_escape(manageAccountPage.getDeleteAccountLabel()));

			sb.append("\"");
		}

		if (manageAccountPage.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(manageAccountPage.getDescription()));

			sb.append("\"");
		}

		if (manageAccountPage.getSubtitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"subtitle\": ");

			sb.append("\"");

			sb.append(_escape(manageAccountPage.getSubtitle()));

			sb.append("\"");
		}

		if (manageAccountPage.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(manageAccountPage.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ManageAccountPageJSONParser manageAccountPageJSONParser =
			new ManageAccountPageJSONParser();

		return manageAccountPageJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		ManageAccountPage manageAccountPage) {

		if (manageAccountPage == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (manageAccountPage.getDeactivateAccountLabel() == null) {
			map.put("deactivateAccountLabel", null);
		}
		else {
			map.put(
				"deactivateAccountLabel",
				String.valueOf(manageAccountPage.getDeactivateAccountLabel()));
		}

		if (manageAccountPage.getDeleteAccountLabel() == null) {
			map.put("deleteAccountLabel", null);
		}
		else {
			map.put(
				"deleteAccountLabel",
				String.valueOf(manageAccountPage.getDeleteAccountLabel()));
		}

		if (manageAccountPage.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(manageAccountPage.getDescription()));
		}

		if (manageAccountPage.getSubtitle() == null) {
			map.put("subtitle", null);
		}
		else {
			map.put(
				"subtitle", String.valueOf(manageAccountPage.getSubtitle()));
		}

		if (manageAccountPage.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(manageAccountPage.getTitle()));
		}

		return map;
	}

	public static class ManageAccountPageJSONParser
		extends BaseJSONParser<ManageAccountPage> {

		@Override
		protected ManageAccountPage createDTO() {
			return new ManageAccountPage();
		}

		@Override
		protected ManageAccountPage[] createDTOArray(int size) {
			return new ManageAccountPage[size];
		}

		@Override
		protected void setField(
			ManageAccountPage manageAccountPage, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "deactivateAccountLabel")) {
				if (jsonParserFieldValue != null) {
					manageAccountPage.setDeactivateAccountLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "deleteAccountLabel")) {

				if (jsonParserFieldValue != null) {
					manageAccountPage.setDeleteAccountLabel(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					manageAccountPage.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "subtitle")) {
				if (jsonParserFieldValue != null) {
					manageAccountPage.setSubtitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					manageAccountPage.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}