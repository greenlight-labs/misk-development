package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.UploadedFileSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class UploadedFile implements Cloneable, Serializable {

	public static UploadedFile toDTO(String json) {
		return UploadedFileSerDes.toDTO(json);
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public void setImages(
		UnsafeSupplier<String, Exception> imagesUnsafeSupplier) {

		try {
			images = imagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String images;

	@Override
	public UploadedFile clone() throws CloneNotSupportedException {
		return (UploadedFile)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof UploadedFile)) {
			return false;
		}

		UploadedFile uploadedFile = (UploadedFile)object;

		return Objects.equals(toString(), uploadedFile.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return UploadedFileSerDes.toJSON(this);
	}

}