package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Date;
import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Media;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.SouvenirsSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Souvenirs implements Cloneable, Serializable {

	public static Souvenirs toDTO(String json) {
		return SouvenirsSerDes.toDTO(json);
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String Date) {
		this.Date = Date;
	}

	public void setDate(UnsafeSupplier<String, Exception> DateUnsafeSupplier) {
		try {
			Date = DateUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String Date;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmail(
		UnsafeSupplier<String, Exception> emailUnsafeSupplier) {

		try {
			email = emailUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String email;

	public Media[] getMedia() {
		return media;
	}

	public void setMedia(Media[] media) {
		this.media = media;
	}

	public void setMedia(
		UnsafeSupplier<Media[], Exception> mediaUnsafeSupplier) {

		try {
			media = mediaUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Media[] media;

	@Override
	public Souvenirs clone() throws CloneNotSupportedException {
		return (Souvenirs)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Souvenirs)) {
			return false;
		}

		Souvenirs souvenirs = (Souvenirs)object;

		return Objects.equals(toString(), souvenirs.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return SouvenirsSerDes.toJSON(this);
	}

}