package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.GalleryImagesList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class GalleryImagesListSerDes {

	public static GalleryImagesList toDTO(String json) {
		GalleryImagesListJSONParser galleryImagesListJSONParser =
			new GalleryImagesListJSONParser();

		return galleryImagesListJSONParser.parseToDTO(json);
	}

	public static GalleryImagesList[] toDTOs(String json) {
		GalleryImagesListJSONParser galleryImagesListJSONParser =
			new GalleryImagesListJSONParser();

		return galleryImagesListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(GalleryImagesList galleryImagesList) {
		if (galleryImagesList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (galleryImagesList.getImagesURL() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"imagesURL\": ");

			sb.append("\"");

			sb.append(_escape(galleryImagesList.getImagesURL()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		GalleryImagesListJSONParser galleryImagesListJSONParser =
			new GalleryImagesListJSONParser();

		return galleryImagesListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		GalleryImagesList galleryImagesList) {

		if (galleryImagesList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (galleryImagesList.getImagesURL() == null) {
			map.put("imagesURL", null);
		}
		else {
			map.put(
				"imagesURL", String.valueOf(galleryImagesList.getImagesURL()));
		}

		return map;
	}

	public static class GalleryImagesListJSONParser
		extends BaseJSONParser<GalleryImagesList> {

		@Override
		protected GalleryImagesList createDTO() {
			return new GalleryImagesList();
		}

		@Override
		protected GalleryImagesList[] createDTOArray(int size) {
			return new GalleryImagesList[size];
		}

		@Override
		protected void setField(
			GalleryImagesList galleryImagesList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "imagesURL")) {
				if (jsonParserFieldValue != null) {
					galleryImagesList.setImagesURL(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}