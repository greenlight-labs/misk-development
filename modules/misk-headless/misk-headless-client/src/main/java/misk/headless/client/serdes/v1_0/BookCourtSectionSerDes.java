package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.BookCourtSection;
import misk.headless.client.dto.v1_0.CourtCategory;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookCourtSectionSerDes {

	public static BookCourtSection toDTO(String json) {
		BookCourtSectionJSONParser bookCourtSectionJSONParser =
			new BookCourtSectionJSONParser();

		return bookCourtSectionJSONParser.parseToDTO(json);
	}

	public static BookCourtSection[] toDTOs(String json) {
		BookCourtSectionJSONParser bookCourtSectionJSONParser =
			new BookCourtSectionJSONParser();

		return bookCourtSectionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(BookCourtSection bookCourtSection) {
		if (bookCourtSection == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (bookCourtSection.getCourtCategories() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"courtCategories\": ");

			sb.append("[");

			for (int i = 0; i < bookCourtSection.getCourtCategories().length;
				 i++) {

				sb.append(
					String.valueOf(bookCourtSection.getCourtCategories()[i]));

				if ((i + 1) < bookCourtSection.getCourtCategories().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (bookCourtSection.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(bookCourtSection.getDescription()));

			sb.append("\"");
		}

		if (bookCourtSection.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(bookCourtSection.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		BookCourtSectionJSONParser bookCourtSectionJSONParser =
			new BookCourtSectionJSONParser();

		return bookCourtSectionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(BookCourtSection bookCourtSection) {
		if (bookCourtSection == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (bookCourtSection.getCourtCategories() == null) {
			map.put("courtCategories", null);
		}
		else {
			map.put(
				"courtCategories",
				String.valueOf(bookCourtSection.getCourtCategories()));
		}

		if (bookCourtSection.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(bookCourtSection.getDescription()));
		}

		if (bookCourtSection.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(bookCourtSection.getTitle()));
		}

		return map;
	}

	public static class BookCourtSectionJSONParser
		extends BaseJSONParser<BookCourtSection> {

		@Override
		protected BookCourtSection createDTO() {
			return new BookCourtSection();
		}

		@Override
		protected BookCourtSection[] createDTOArray(int size) {
			return new BookCourtSection[size];
		}

		@Override
		protected void setField(
			BookCourtSection bookCourtSection, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "courtCategories")) {
				if (jsonParserFieldValue != null) {
					bookCourtSection.setCourtCategories(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> CourtCategorySerDes.toDTO((String)object)
						).toArray(
							size -> new CourtCategory[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					bookCourtSection.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					bookCourtSection.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}