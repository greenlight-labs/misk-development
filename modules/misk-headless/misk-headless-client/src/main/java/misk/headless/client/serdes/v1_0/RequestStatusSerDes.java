package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.RequestStatus;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class RequestStatusSerDes {

	public static RequestStatus toDTO(String json) {
		RequestStatusJSONParser requestStatusJSONParser =
			new RequestStatusJSONParser();

		return requestStatusJSONParser.parseToDTO(json);
	}

	public static RequestStatus[] toDTOs(String json) {
		RequestStatusJSONParser requestStatusJSONParser =
			new RequestStatusJSONParser();

		return requestStatusJSONParser.parseToDTOs(json);
	}

	public static String toJSON(RequestStatus requestStatus) {
		if (requestStatus == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (requestStatus.getCode() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"code\": ");

			sb.append(requestStatus.getCode());
		}

		if (requestStatus.getMessage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"message\": ");

			sb.append("\"");

			sb.append(_escape(requestStatus.getMessage()));

			sb.append("\"");
		}

		if (requestStatus.getStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"status\": ");

			sb.append(requestStatus.getStatus());
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		RequestStatusJSONParser requestStatusJSONParser =
			new RequestStatusJSONParser();

		return requestStatusJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(RequestStatus requestStatus) {
		if (requestStatus == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (requestStatus.getCode() == null) {
			map.put("code", null);
		}
		else {
			map.put("code", String.valueOf(requestStatus.getCode()));
		}

		if (requestStatus.getMessage() == null) {
			map.put("message", null);
		}
		else {
			map.put("message", String.valueOf(requestStatus.getMessage()));
		}

		if (requestStatus.getStatus() == null) {
			map.put("status", null);
		}
		else {
			map.put("status", String.valueOf(requestStatus.getStatus()));
		}

		return map;
	}

	public static class RequestStatusJSONParser
		extends BaseJSONParser<RequestStatus> {

		@Override
		protected RequestStatus createDTO() {
			return new RequestStatus();
		}

		@Override
		protected RequestStatus[] createDTOArray(int size) {
			return new RequestStatus[size];
		}

		@Override
		protected void setField(
			RequestStatus requestStatus, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "code")) {
				if (jsonParserFieldValue != null) {
					requestStatus.setCode(
						Integer.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "message")) {
				if (jsonParserFieldValue != null) {
					requestStatus.setMessage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "status")) {
				if (jsonParserFieldValue != null) {
					requestStatus.setStatus((Boolean)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}