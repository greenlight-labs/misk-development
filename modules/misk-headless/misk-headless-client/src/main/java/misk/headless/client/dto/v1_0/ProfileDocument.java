package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ProfileDocumentSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ProfileDocument implements Cloneable, Serializable {

	public static ProfileDocument toDTO(String json) {
		return ProfileDocumentSerDes.toDTO(json);
	}

	public String getFormImage() {
		return formImage;
	}

	public void setFormImage(String formImage) {
		this.formImage = formImage;
	}

	public void setFormImage(
		UnsafeSupplier<String, Exception> formImageUnsafeSupplier) {

		try {
			formImage = formImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String formImage;

	public String getFormPdf() {
		return formPdf;
	}

	public void setFormPdf(String formPdf) {
		this.formPdf = formPdf;
	}

	public void setFormPdf(
		UnsafeSupplier<String, Exception> formPdfUnsafeSupplier) {

		try {
			formPdf = formPdfUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String formPdf;

	@Override
	public ProfileDocument clone() throws CloneNotSupportedException {
		return (ProfileDocument)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ProfileDocument)) {
			return false;
		}

		ProfileDocument profileDocument = (ProfileDocument)object;

		return Objects.equals(toString(), profileDocument.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ProfileDocumentSerDes.toJSON(this);
	}

}