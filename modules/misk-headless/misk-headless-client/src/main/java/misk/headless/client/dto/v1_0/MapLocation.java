package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.MapLocationSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MapLocation implements Cloneable, Serializable {

	public static MapLocation toDTO(String json) {
		return MapLocationSerDes.toDTO(json);
	}

	public String getCallUs() {
		return callUs;
	}

	public void setCallUs(String callUs) {
		this.callUs = callUs;
	}

	public void setCallUs(
		UnsafeSupplier<String, Exception> callUsUnsafeSupplier) {

		try {
			callUs = callUsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String callUs;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmail(
		UnsafeSupplier<String, Exception> emailUnsafeSupplier) {

		try {
			email = emailUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String email;

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLatitude(
		UnsafeSupplier<String, Exception> latitudeUnsafeSupplier) {

		try {
			latitude = latitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String latitude;

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setLongitude(
		UnsafeSupplier<String, Exception> longitudeUnsafeSupplier) {

		try {
			longitude = longitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String longitude;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public void setWhatsapp(
		UnsafeSupplier<String, Exception> whatsappUnsafeSupplier) {

		try {
			whatsapp = whatsappUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String whatsapp;

	@Override
	public MapLocation clone() throws CloneNotSupportedException {
		return (MapLocation)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MapLocation)) {
			return false;
		}

		MapLocation mapLocation = (MapLocation)object;

		return Objects.equals(toString(), mapLocation.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return MapLocationSerDes.toJSON(this);
	}

}