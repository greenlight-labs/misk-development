package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ExploreExperientialCenterCategory;
import misk.headless.client.dto.v1_0.ExploreExperientialCenterPost;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ExploreExperientialCenterPageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreExperientialCenterPage implements Cloneable, Serializable {

	public static ExploreExperientialCenterPage toDTO(String json) {
		return ExploreExperientialCenterPageSerDes.toDTO(json);
	}

	public ExploreExperientialCenterCategory[]
		getExploreExperientialCenterCategories() {

		return exploreExperientialCenterCategories;
	}

	public void setExploreExperientialCenterCategories(
		ExploreExperientialCenterCategory[]
			exploreExperientialCenterCategories) {

		this.exploreExperientialCenterCategories =
			exploreExperientialCenterCategories;
	}

	public void setExploreExperientialCenterCategories(
		UnsafeSupplier<ExploreExperientialCenterCategory[], Exception>
			exploreExperientialCenterCategoriesUnsafeSupplier) {

		try {
			exploreExperientialCenterCategories =
				exploreExperientialCenterCategoriesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected ExploreExperientialCenterCategory[]
		exploreExperientialCenterCategories;

	public ExploreExperientialCenterPost[] getExploreExperientialCenterPosts() {
		return exploreExperientialCenterPosts;
	}

	public void setExploreExperientialCenterPosts(
		ExploreExperientialCenterPost[] exploreExperientialCenterPosts) {

		this.exploreExperientialCenterPosts = exploreExperientialCenterPosts;
	}

	public void setExploreExperientialCenterPosts(
		UnsafeSupplier<ExploreExperientialCenterPost[], Exception>
			exploreExperientialCenterPostsUnsafeSupplier) {

		try {
			exploreExperientialCenterPosts =
				exploreExperientialCenterPostsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected ExploreExperientialCenterPost[] exploreExperientialCenterPosts;

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public void setPageTitle(
		UnsafeSupplier<String, Exception> pageTitleUnsafeSupplier) {

		try {
			pageTitle = pageTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String pageTitle;

	@Override
	public ExploreExperientialCenterPage clone()
		throws CloneNotSupportedException {

		return (ExploreExperientialCenterPage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ExploreExperientialCenterPage)) {
			return false;
		}

		ExploreExperientialCenterPage exploreExperientialCenterPage =
			(ExploreExperientialCenterPage)object;

		return Objects.equals(
			toString(), exploreExperientialCenterPage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ExploreExperientialCenterPageSerDes.toJSON(this);
	}

}