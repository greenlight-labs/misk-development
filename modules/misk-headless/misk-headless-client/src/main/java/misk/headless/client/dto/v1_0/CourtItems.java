package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Court;
import misk.headless.client.dto.v1_0.CourtCategory;
import misk.headless.client.dto.v1_0.CourtLocation;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.CourtItemsSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CourtItems implements Cloneable, Serializable {

	public static CourtItems toDTO(String json) {
		return CourtItemsSerDes.toDTO(json);
	}

	public CourtCategory[] getCourtCategories() {
		return courtCategories;
	}

	public void setCourtCategories(CourtCategory[] courtCategories) {
		this.courtCategories = courtCategories;
	}

	public void setCourtCategories(
		UnsafeSupplier<CourtCategory[], Exception>
			courtCategoriesUnsafeSupplier) {

		try {
			courtCategories = courtCategoriesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected CourtCategory[] courtCategories;

	public CourtLocation[] getCourtLocations() {
		return courtLocations;
	}

	public void setCourtLocations(CourtLocation[] courtLocations) {
		this.courtLocations = courtLocations;
	}

	public void setCourtLocations(
		UnsafeSupplier<CourtLocation[], Exception>
			courtLocationsUnsafeSupplier) {

		try {
			courtLocations = courtLocationsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected CourtLocation[] courtLocations;

	public Court[] getCourts() {
		return courts;
	}

	public void setCourts(Court[] courts) {
		this.courts = courts;
	}

	public void setCourts(
		UnsafeSupplier<Court[], Exception> courtsUnsafeSupplier) {

		try {
			courts = courtsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Court[] courts;

	@Override
	public CourtItems clone() throws CloneNotSupportedException {
		return (CourtItems)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof CourtItems)) {
			return false;
		}

		CourtItems courtItems = (CourtItems)object;

		return Objects.equals(toString(), courtItems.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return CourtItemsSerDes.toJSON(this);
	}

}