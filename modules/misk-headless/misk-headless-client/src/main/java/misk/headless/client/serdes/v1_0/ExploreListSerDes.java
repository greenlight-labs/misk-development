package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ExploreList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreListSerDes {

	public static ExploreList toDTO(String json) {
		ExploreListJSONParser exploreListJSONParser =
			new ExploreListJSONParser();

		return exploreListJSONParser.parseToDTO(json);
	}

	public static ExploreList[] toDTOs(String json) {
		ExploreListJSONParser exploreListJSONParser =
			new ExploreListJSONParser();

		return exploreListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(ExploreList exploreList) {
		if (exploreList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (exploreList.getArea() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"area\": ");

			sb.append("\"");

			sb.append(_escape(exploreList.getArea()));

			sb.append("\"");
		}

		if (exploreList.getCapacity() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"capacity\": ");

			sb.append("\"");

			sb.append(_escape(exploreList.getCapacity()));

			sb.append("\"");
		}

		if (exploreList.getDuration() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"duration\": ");

			sb.append("\"");

			sb.append(_escape(exploreList.getDuration()));

			sb.append("\"");
		}

		if (exploreList.getExploreIcon() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreIcon\": ");

			sb.append("\"");

			sb.append(_escape(exploreList.getExploreIcon()));

			sb.append("\"");
		}

		if (exploreList.getExploreImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreImages\": ");

			sb.append("\"");

			sb.append(_escape(exploreList.getExploreImages()));

			sb.append("\"");
		}

		if (exploreList.getExploreTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreTitle\": ");

			sb.append("\"");

			sb.append(_escape(exploreList.getExploreTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ExploreListJSONParser exploreListJSONParser =
			new ExploreListJSONParser();

		return exploreListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(ExploreList exploreList) {
		if (exploreList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (exploreList.getArea() == null) {
			map.put("area", null);
		}
		else {
			map.put("area", String.valueOf(exploreList.getArea()));
		}

		if (exploreList.getCapacity() == null) {
			map.put("capacity", null);
		}
		else {
			map.put("capacity", String.valueOf(exploreList.getCapacity()));
		}

		if (exploreList.getDuration() == null) {
			map.put("duration", null);
		}
		else {
			map.put("duration", String.valueOf(exploreList.getDuration()));
		}

		if (exploreList.getExploreIcon() == null) {
			map.put("exploreIcon", null);
		}
		else {
			map.put(
				"exploreIcon", String.valueOf(exploreList.getExploreIcon()));
		}

		if (exploreList.getExploreImages() == null) {
			map.put("exploreImages", null);
		}
		else {
			map.put(
				"exploreImages",
				String.valueOf(exploreList.getExploreImages()));
		}

		if (exploreList.getExploreTitle() == null) {
			map.put("exploreTitle", null);
		}
		else {
			map.put(
				"exploreTitle", String.valueOf(exploreList.getExploreTitle()));
		}

		return map;
	}

	public static class ExploreListJSONParser
		extends BaseJSONParser<ExploreList> {

		@Override
		protected ExploreList createDTO() {
			return new ExploreList();
		}

		@Override
		protected ExploreList[] createDTOArray(int size) {
			return new ExploreList[size];
		}

		@Override
		protected void setField(
			ExploreList exploreList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "area")) {
				if (jsonParserFieldValue != null) {
					exploreList.setArea((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "capacity")) {
				if (jsonParserFieldValue != null) {
					exploreList.setCapacity((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "duration")) {
				if (jsonParserFieldValue != null) {
					exploreList.setDuration((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "exploreIcon")) {
				if (jsonParserFieldValue != null) {
					exploreList.setExploreIcon((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "exploreImages")) {
				if (jsonParserFieldValue != null) {
					exploreList.setExploreImages((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "exploreTitle")) {
				if (jsonParserFieldValue != null) {
					exploreList.setExploreTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}