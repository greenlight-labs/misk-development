package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.PushNotification;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class PushNotificationSerDes {

	public static PushNotification toDTO(String json) {
		PushNotificationJSONParser pushNotificationJSONParser =
			new PushNotificationJSONParser();

		return pushNotificationJSONParser.parseToDTO(json);
	}

	public static PushNotification[] toDTOs(String json) {
		PushNotificationJSONParser pushNotificationJSONParser =
			new PushNotificationJSONParser();

		return pushNotificationJSONParser.parseToDTOs(json);
	}

	public static String toJSON(PushNotification pushNotification) {
		if (pushNotification == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (pushNotification.getAppUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(pushNotification.getAppUserId());
		}

		if (pushNotification.getBody() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"body\": ");

			sb.append("\"");

			sb.append(_escape(pushNotification.getBody()));

			sb.append("\"");
		}

		if (pushNotification.getHighlight() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlight\": ");

			sb.append(String.valueOf(pushNotification.getHighlight()));
		}

		if (pushNotification.getNotificationId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"notificationId\": ");

			sb.append(pushNotification.getNotificationId());
		}

		if (pushNotification.getReadStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"readStatus\": ");

			sb.append(pushNotification.getReadStatus());
		}

		if (pushNotification.getRequestStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"requestStatus\": ");

			sb.append(String.valueOf(pushNotification.getRequestStatus()));
		}

		if (pushNotification.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(pushNotification.getTitle()));

			sb.append("\"");
		}

		if (pushNotification.getType() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"type\": ");

			sb.append("\"");

			sb.append(_escape(pushNotification.getType()));

			sb.append("\"");
		}

		if (pushNotification.getTypeId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"typeId\": ");

			sb.append(pushNotification.getTypeId());
		}

		if (pushNotification.getTypeLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"typeLabel\": ");

			sb.append("\"");

			sb.append(_escape(pushNotification.getTypeLabel()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		PushNotificationJSONParser pushNotificationJSONParser =
			new PushNotificationJSONParser();

		return pushNotificationJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(PushNotification pushNotification) {
		if (pushNotification == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (pushNotification.getAppUserId() == null) {
			map.put("appUserId", null);
		}
		else {
			map.put(
				"appUserId", String.valueOf(pushNotification.getAppUserId()));
		}

		if (pushNotification.getBody() == null) {
			map.put("body", null);
		}
		else {
			map.put("body", String.valueOf(pushNotification.getBody()));
		}

		if (pushNotification.getHighlight() == null) {
			map.put("highlight", null);
		}
		else {
			map.put(
				"highlight", String.valueOf(pushNotification.getHighlight()));
		}

		if (pushNotification.getNotificationId() == null) {
			map.put("notificationId", null);
		}
		else {
			map.put(
				"notificationId",
				String.valueOf(pushNotification.getNotificationId()));
		}

		if (pushNotification.getReadStatus() == null) {
			map.put("readStatus", null);
		}
		else {
			map.put(
				"readStatus", String.valueOf(pushNotification.getReadStatus()));
		}

		if (pushNotification.getRequestStatus() == null) {
			map.put("requestStatus", null);
		}
		else {
			map.put(
				"requestStatus",
				String.valueOf(pushNotification.getRequestStatus()));
		}

		if (pushNotification.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(pushNotification.getTitle()));
		}

		if (pushNotification.getType() == null) {
			map.put("type", null);
		}
		else {
			map.put("type", String.valueOf(pushNotification.getType()));
		}

		if (pushNotification.getTypeId() == null) {
			map.put("typeId", null);
		}
		else {
			map.put("typeId", String.valueOf(pushNotification.getTypeId()));
		}

		if (pushNotification.getTypeLabel() == null) {
			map.put("typeLabel", null);
		}
		else {
			map.put(
				"typeLabel", String.valueOf(pushNotification.getTypeLabel()));
		}

		return map;
	}

	public static class PushNotificationJSONParser
		extends BaseJSONParser<PushNotification> {

		@Override
		protected PushNotification createDTO() {
			return new PushNotification();
		}

		@Override
		protected PushNotification[] createDTOArray(int size) {
			return new PushNotification[size];
		}

		@Override
		protected void setField(
			PushNotification pushNotification, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "appUserId")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setAppUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "body")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setBody((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "highlight")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setHighlight(
						HighlightSerDes.toDTO((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "notificationId")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setNotificationId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "readStatus")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setReadStatus(
						(Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "requestStatus")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setRequestStatus(
						RequestStatusSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "type")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setType((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "typeId")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setTypeId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "typeLabel")) {
				if (jsonParserFieldValue != null) {
					pushNotification.setTypeLabel((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}