package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.AnswerSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Answer implements Cloneable, Serializable {

	public static Answer toDTO(String json) {
		return AnswerSerDes.toDTO(json);
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setReason(
		UnsafeSupplier<String, Exception> reasonUnsafeSupplier) {

		try {
			reason = reasonUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String reason;

	@Override
	public Answer clone() throws CloneNotSupportedException {
		return (Answer)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Answer)) {
			return false;
		}

		Answer answer = (Answer)object;

		return Objects.equals(toString(), answer.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return AnswerSerDes.toJSON(this);
	}

}