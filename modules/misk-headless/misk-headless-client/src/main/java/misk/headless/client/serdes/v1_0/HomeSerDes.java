package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.FlipCard;
import misk.headless.client.dto.v1_0.Home;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HomeSerDes {

	public static Home toDTO(String json) {
		HomeJSONParser homeJSONParser = new HomeJSONParser();

		return homeJSONParser.parseToDTO(json);
	}

	public static Home[] toDTOs(String json) {
		HomeJSONParser homeJSONParser = new HomeJSONParser();

		return homeJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Home home) {
		if (home == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (home.getEventsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(home.getEventsSection()));
		}

		if (home.getFlipCards() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"flipCards\": ");

			sb.append("[");

			for (int i = 0; i < home.getFlipCards().length; i++) {
				sb.append(String.valueOf(home.getFlipCards()[i]));

				if ((i + 1) < home.getFlipCards().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (home.getHighlightsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlightsSection\": ");

			sb.append(String.valueOf(home.getHighlightsSection()));
		}

		if (home.getStoriesSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"storiesSection\": ");

			sb.append(String.valueOf(home.getStoriesSection()));
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		HomeJSONParser homeJSONParser = new HomeJSONParser();

		return homeJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Home home) {
		if (home == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (home.getEventsSection() == null) {
			map.put("eventsSection", null);
		}
		else {
			map.put("eventsSection", String.valueOf(home.getEventsSection()));
		}

		if (home.getFlipCards() == null) {
			map.put("flipCards", null);
		}
		else {
			map.put("flipCards", String.valueOf(home.getFlipCards()));
		}

		if (home.getHighlightsSection() == null) {
			map.put("highlightsSection", null);
		}
		else {
			map.put(
				"highlightsSection",
				String.valueOf(home.getHighlightsSection()));
		}

		if (home.getStoriesSection() == null) {
			map.put("storiesSection", null);
		}
		else {
			map.put("storiesSection", String.valueOf(home.getStoriesSection()));
		}

		return map;
	}

	public static class HomeJSONParser extends BaseJSONParser<Home> {

		@Override
		protected Home createDTO() {
			return new Home();
		}

		@Override
		protected Home[] createDTOArray(int size) {
			return new Home[size];
		}

		@Override
		protected void setField(
			Home home, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "eventsSection")) {
				if (jsonParserFieldValue != null) {
					home.setEventsSection(
						EventsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "flipCards")) {
				if (jsonParserFieldValue != null) {
					home.setFlipCards(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> FlipCardSerDes.toDTO((String)object)
						).toArray(
							size -> new FlipCard[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "highlightsSection")) {
				if (jsonParserFieldValue != null) {
					home.setHighlightsSection(
						HighlightsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "storiesSection")) {
				if (jsonParserFieldValue != null) {
					home.setStoriesSection(
						StoriesSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}