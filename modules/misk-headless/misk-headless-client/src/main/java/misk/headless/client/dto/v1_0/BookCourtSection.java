package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.CourtCategory;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.BookCourtSectionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookCourtSection implements Cloneable, Serializable {

	public static BookCourtSection toDTO(String json) {
		return BookCourtSectionSerDes.toDTO(json);
	}

	public CourtCategory[] getCourtCategories() {
		return courtCategories;
	}

	public void setCourtCategories(CourtCategory[] courtCategories) {
		this.courtCategories = courtCategories;
	}

	public void setCourtCategories(
		UnsafeSupplier<CourtCategory[], Exception>
			courtCategoriesUnsafeSupplier) {

		try {
			courtCategories = courtCategoriesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected CourtCategory[] courtCategories;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	@Override
	public BookCourtSection clone() throws CloneNotSupportedException {
		return (BookCourtSection)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BookCourtSection)) {
			return false;
		}

		BookCourtSection bookCourtSection = (BookCourtSection)object;

		return Objects.equals(toString(), bookCourtSection.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return BookCourtSectionSerDes.toJSON(this);
	}

}