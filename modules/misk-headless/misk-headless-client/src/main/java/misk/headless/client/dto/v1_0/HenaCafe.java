package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Category;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.HenaCafeSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HenaCafe implements Cloneable, Serializable {

	public static HenaCafe toDTO(String json) {
		return HenaCafeSerDes.toDTO(json);
	}

	public Category[] getCategories() {
		return categories;
	}

	public void setCategories(Category[] categories) {
		this.categories = categories;
	}

	public void setCategories(
		UnsafeSupplier<Category[], Exception> categoriesUnsafeSupplier) {

		try {
			categories = categoriesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Category[] categories;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> descriptionUnsafeSupplier) {

		try {
			description = descriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String description;

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLatitude(
		UnsafeSupplier<String, Exception> latitudeUnsafeSupplier) {

		try {
			latitude = latitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String latitude;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setLocation(
		UnsafeSupplier<String, Exception> locationUnsafeSupplier) {

		try {
			location = locationUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String location;

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setLongitude(
		UnsafeSupplier<String, Exception> longitudeUnsafeSupplier) {

		try {
			longitude = longitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String longitude;

	public String[] getSliderimage() {
		return sliderimage;
	}

	public void setSliderimage(String[] sliderimage) {
		this.sliderimage = sliderimage;
	}

	public void setSliderimage(
		UnsafeSupplier<String[], Exception> sliderimageUnsafeSupplier) {

		try {
			sliderimage = sliderimageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] sliderimage;

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public void setSubtitle(
		UnsafeSupplier<String, Exception> subtitleUnsafeSupplier) {

		try {
			subtitle = subtitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String subtitle;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	public String getWorkingdays() {
		return workingdays;
	}

	public void setWorkingdays(String workingdays) {
		this.workingdays = workingdays;
	}

	public void setWorkingdays(
		UnsafeSupplier<String, Exception> workingdaysUnsafeSupplier) {

		try {
			workingdays = workingdaysUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingdays;

	public String getWorkinghours() {
		return workinghours;
	}

	public void setWorkinghours(String workinghours) {
		this.workinghours = workinghours;
	}

	public void setWorkinghours(
		UnsafeSupplier<String, Exception> workinghoursUnsafeSupplier) {

		try {
			workinghours = workinghoursUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workinghours;

	public String getWorkingsummary() {
		return workingsummary;
	}

	public void setWorkingsummary(String workingsummary) {
		this.workingsummary = workingsummary;
	}

	public void setWorkingsummary(
		UnsafeSupplier<String, Exception> workingsummaryUnsafeSupplier) {

		try {
			workingsummary = workingsummaryUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String workingsummary;

	@Override
	public HenaCafe clone() throws CloneNotSupportedException {
		return (HenaCafe)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof HenaCafe)) {
			return false;
		}

		HenaCafe henaCafe = (HenaCafe)object;

		return Objects.equals(toString(), henaCafe.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return HenaCafeSerDes.toJSON(this);
	}

}