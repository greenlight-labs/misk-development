package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.RequestStatus;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.ProfileBannerImageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ProfileBannerImage implements Cloneable, Serializable {

	public static ProfileBannerImage toDTO(String json) {
		return ProfileBannerImageSerDes.toDTO(json);
	}

	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long appUserId;

	public String getProfileBannerImage() {
		return profileBannerImage;
	}

	public void setProfileBannerImage(String profileBannerImage) {
		this.profileBannerImage = profileBannerImage;
	}

	public void setProfileBannerImage(
		UnsafeSupplier<String, Exception> profileBannerImageUnsafeSupplier) {

		try {
			profileBannerImage = profileBannerImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String profileBannerImage;

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public void setRequestStatus(
		UnsafeSupplier<RequestStatus, Exception> requestStatusUnsafeSupplier) {

		try {
			requestStatus = requestStatusUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected RequestStatus requestStatus;

	@Override
	public ProfileBannerImage clone() throws CloneNotSupportedException {
		return (ProfileBannerImage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof ProfileBannerImage)) {
			return false;
		}

		ProfileBannerImage profileBannerImage = (ProfileBannerImage)object;

		return Objects.equals(toString(), profileBannerImage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return ProfileBannerImageSerDes.toJSON(this);
	}

}