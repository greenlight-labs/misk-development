package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Highlight;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.HighlightsSectionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HighlightsSection implements Cloneable, Serializable {

	public static HighlightsSection toDTO(String json) {
		return HighlightsSectionSerDes.toDTO(json);
	}

	public Highlight[] getItems() {
		return items;
	}

	public void setItems(Highlight[] items) {
		this.items = items;
	}

	public void setItems(
		UnsafeSupplier<Highlight[], Exception> itemsUnsafeSupplier) {

		try {
			items = itemsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Highlight[] items;

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public void setSectionTitle(
		UnsafeSupplier<String, Exception> sectionTitleUnsafeSupplier) {

		try {
			sectionTitle = sectionTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String sectionTitle;

	@Override
	public HighlightsSection clone() throws CloneNotSupportedException {
		return (HighlightsSection)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof HighlightsSection)) {
			return false;
		}

		HighlightsSection highlightsSection = (HighlightsSection)object;

		return Objects.equals(toString(), highlightsSection.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return HighlightsSectionSerDes.toJSON(this);
	}

}