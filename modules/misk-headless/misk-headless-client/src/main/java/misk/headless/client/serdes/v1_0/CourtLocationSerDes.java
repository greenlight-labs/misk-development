package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.CourtLocation;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CourtLocationSerDes {

	public static CourtLocation toDTO(String json) {
		CourtLocationJSONParser courtLocationJSONParser =
			new CourtLocationJSONParser();

		return courtLocationJSONParser.parseToDTO(json);
	}

	public static CourtLocation[] toDTOs(String json) {
		CourtLocationJSONParser courtLocationJSONParser =
			new CourtLocationJSONParser();

		return courtLocationJSONParser.parseToDTOs(json);
	}

	public static String toJSON(CourtLocation courtLocation) {
		if (courtLocation == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (courtLocation.getId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append(courtLocation.getId());
		}

		if (courtLocation.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(courtLocation.getName()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		CourtLocationJSONParser courtLocationJSONParser =
			new CourtLocationJSONParser();

		return courtLocationJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(CourtLocation courtLocation) {
		if (courtLocation == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (courtLocation.getId() == null) {
			map.put("id", null);
		}
		else {
			map.put("id", String.valueOf(courtLocation.getId()));
		}

		if (courtLocation.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put("name", String.valueOf(courtLocation.getName()));
		}

		return map;
	}

	public static class CourtLocationJSONParser
		extends BaseJSONParser<CourtLocation> {

		@Override
		protected CourtLocation createDTO() {
			return new CourtLocation();
		}

		@Override
		protected CourtLocation[] createDTOArray(int size) {
			return new CourtLocation[size];
		}

		@Override
		protected void setField(
			CourtLocation courtLocation, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "id")) {
				if (jsonParserFieldValue != null) {
					courtLocation.setId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					courtLocation.setName((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}