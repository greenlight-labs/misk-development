package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.StoriesSection;
import misk.headless.client.dto.v1_0.Story;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class StoriesSectionSerDes {

	public static StoriesSection toDTO(String json) {
		StoriesSectionJSONParser storiesSectionJSONParser =
			new StoriesSectionJSONParser();

		return storiesSectionJSONParser.parseToDTO(json);
	}

	public static StoriesSection[] toDTOs(String json) {
		StoriesSectionJSONParser storiesSectionJSONParser =
			new StoriesSectionJSONParser();

		return storiesSectionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(StoriesSection storiesSection) {
		if (storiesSection == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (storiesSection.getItems() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"items\": ");

			sb.append("[");

			for (int i = 0; i < storiesSection.getItems().length; i++) {
				sb.append(String.valueOf(storiesSection.getItems()[i]));

				if ((i + 1) < storiesSection.getItems().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (storiesSection.getSectionTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"sectionTitle\": ");

			sb.append("\"");

			sb.append(_escape(storiesSection.getSectionTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		StoriesSectionJSONParser storiesSectionJSONParser =
			new StoriesSectionJSONParser();

		return storiesSectionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(StoriesSection storiesSection) {
		if (storiesSection == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (storiesSection.getItems() == null) {
			map.put("items", null);
		}
		else {
			map.put("items", String.valueOf(storiesSection.getItems()));
		}

		if (storiesSection.getSectionTitle() == null) {
			map.put("sectionTitle", null);
		}
		else {
			map.put(
				"sectionTitle",
				String.valueOf(storiesSection.getSectionTitle()));
		}

		return map;
	}

	public static class StoriesSectionJSONParser
		extends BaseJSONParser<StoriesSection> {

		@Override
		protected StoriesSection createDTO() {
			return new StoriesSection();
		}

		@Override
		protected StoriesSection[] createDTOArray(int size) {
			return new StoriesSection[size];
		}

		@Override
		protected void setField(
			StoriesSection storiesSection, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "items")) {
				if (jsonParserFieldValue != null) {
					storiesSection.setItems(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> StorySerDes.toDTO((String)object)
						).toArray(
							size -> new Story[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "sectionTitle")) {
				if (jsonParserFieldValue != null) {
					storiesSection.setSectionTitle(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}