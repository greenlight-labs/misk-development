package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.FaclitiesList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FaclitiesListSerDes {

	public static FaclitiesList toDTO(String json) {
		FaclitiesListJSONParser faclitiesListJSONParser =
			new FaclitiesListJSONParser();

		return faclitiesListJSONParser.parseToDTO(json);
	}

	public static FaclitiesList[] toDTOs(String json) {
		FaclitiesListJSONParser faclitiesListJSONParser =
			new FaclitiesListJSONParser();

		return faclitiesListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(FaclitiesList faclitiesList) {
		if (faclitiesList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (faclitiesList.getFaclitiesImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesImages\": ");

			sb.append("\"");

			sb.append(_escape(faclitiesList.getFaclitiesImages()));

			sb.append("\"");
		}

		if (faclitiesList.getFaclitiesImagesLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesImagesLabel\": ");

			sb.append("\"");

			sb.append(_escape(faclitiesList.getFaclitiesImagesLabel()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		FaclitiesListJSONParser faclitiesListJSONParser =
			new FaclitiesListJSONParser();

		return faclitiesListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(FaclitiesList faclitiesList) {
		if (faclitiesList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (faclitiesList.getFaclitiesImages() == null) {
			map.put("faclitiesImages", null);
		}
		else {
			map.put(
				"faclitiesImages",
				String.valueOf(faclitiesList.getFaclitiesImages()));
		}

		if (faclitiesList.getFaclitiesImagesLabel() == null) {
			map.put("faclitiesImagesLabel", null);
		}
		else {
			map.put(
				"faclitiesImagesLabel",
				String.valueOf(faclitiesList.getFaclitiesImagesLabel()));
		}

		return map;
	}

	public static class FaclitiesListJSONParser
		extends BaseJSONParser<FaclitiesList> {

		@Override
		protected FaclitiesList createDTO() {
			return new FaclitiesList();
		}

		@Override
		protected FaclitiesList[] createDTOArray(int size) {
			return new FaclitiesList[size];
		}

		@Override
		protected void setField(
			FaclitiesList faclitiesList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "faclitiesImages")) {
				if (jsonParserFieldValue != null) {
					faclitiesList.setFaclitiesImages(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "faclitiesImagesLabel")) {

				if (jsonParserFieldValue != null) {
					faclitiesList.setFaclitiesImagesLabel(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}