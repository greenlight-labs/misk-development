package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.SocialMediaListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class SocialMediaList implements Cloneable, Serializable {

	public static SocialMediaList toDTO(String json) {
		return SocialMediaListSerDes.toDTO(json);
	}

	public String getSocialMediaName() {
		return socialMediaName;
	}

	public void setSocialMediaName(String socialMediaName) {
		this.socialMediaName = socialMediaName;
	}

	public void setSocialMediaName(
		UnsafeSupplier<String, Exception> socialMediaNameUnsafeSupplier) {

		try {
			socialMediaName = socialMediaNameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String socialMediaName;

	public String getSocialMediaURL() {
		return socialMediaURL;
	}

	public void setSocialMediaURL(String socialMediaURL) {
		this.socialMediaURL = socialMediaURL;
	}

	public void setSocialMediaURL(
		UnsafeSupplier<String, Exception> socialMediaURLUnsafeSupplier) {

		try {
			socialMediaURL = socialMediaURLUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String socialMediaURL;

	@Override
	public SocialMediaList clone() throws CloneNotSupportedException {
		return (SocialMediaList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof SocialMediaList)) {
			return false;
		}

		SocialMediaList socialMediaList = (SocialMediaList)object;

		return Objects.equals(toString(), socialMediaList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return SocialMediaListSerDes.toJSON(this);
	}

}