package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Media;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MediaSerDes {

	public static Media toDTO(String json) {
		MediaJSONParser mediaJSONParser = new MediaJSONParser();

		return mediaJSONParser.parseToDTO(json);
	}

	public static Media[] toDTOs(String json) {
		MediaJSONParser mediaJSONParser = new MediaJSONParser();

		return mediaJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Media media) {
		if (media == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (media.getId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"id\": ");

			sb.append("\"");

			sb.append(_escape(media.getId()));

			sb.append("\"");
		}

		if (media.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(media.getTitle()));

			sb.append("\"");
		}

		if (media.getVideoURL() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"videoURL\": ");

			sb.append("\"");

			sb.append(_escape(media.getVideoURL()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		MediaJSONParser mediaJSONParser = new MediaJSONParser();

		return mediaJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Media media) {
		if (media == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (media.getId() == null) {
			map.put("id", null);
		}
		else {
			map.put("id", String.valueOf(media.getId()));
		}

		if (media.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(media.getTitle()));
		}

		if (media.getVideoURL() == null) {
			map.put("videoURL", null);
		}
		else {
			map.put("videoURL", String.valueOf(media.getVideoURL()));
		}

		return map;
	}

	public static class MediaJSONParser extends BaseJSONParser<Media> {

		@Override
		protected Media createDTO() {
			return new Media();
		}

		@Override
		protected Media[] createDTOArray(int size) {
			return new Media[size];
		}

		@Override
		protected void setField(
			Media media, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "id")) {
				if (jsonParserFieldValue != null) {
					media.setId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					media.setTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "videoURL")) {
				if (jsonParserFieldValue != null) {
					media.setVideoURL((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}