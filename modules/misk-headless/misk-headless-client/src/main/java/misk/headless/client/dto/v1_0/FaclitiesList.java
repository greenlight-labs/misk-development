package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.FaclitiesListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FaclitiesList implements Cloneable, Serializable {

	public static FaclitiesList toDTO(String json) {
		return FaclitiesListSerDes.toDTO(json);
	}

	public String getFaclitiesImages() {
		return faclitiesImages;
	}

	public void setFaclitiesImages(String faclitiesImages) {
		this.faclitiesImages = faclitiesImages;
	}

	public void setFaclitiesImages(
		UnsafeSupplier<String, Exception> faclitiesImagesUnsafeSupplier) {

		try {
			faclitiesImages = faclitiesImagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String faclitiesImages;

	public String getFaclitiesImagesLabel() {
		return faclitiesImagesLabel;
	}

	public void setFaclitiesImagesLabel(String faclitiesImagesLabel) {
		this.faclitiesImagesLabel = faclitiesImagesLabel;
	}

	public void setFaclitiesImagesLabel(
		UnsafeSupplier<String, Exception> faclitiesImagesLabelUnsafeSupplier) {

		try {
			faclitiesImagesLabel = faclitiesImagesLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String faclitiesImagesLabel;

	@Override
	public FaclitiesList clone() throws CloneNotSupportedException {
		return (FaclitiesList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FaclitiesList)) {
			return false;
		}

		FaclitiesList faclitiesList = (FaclitiesList)object;

		return Objects.equals(toString(), faclitiesList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return FaclitiesListSerDes.toJSON(this);
	}

}