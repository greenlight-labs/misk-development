package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.BookingSync;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookingSyncSerDes {

	public static BookingSync toDTO(String json) {
		BookingSyncJSONParser bookingSyncJSONParser =
			new BookingSyncJSONParser();

		return bookingSyncJSONParser.parseToDTO(json);
	}

	public static BookingSync[] toDTOs(String json) {
		BookingSyncJSONParser bookingSyncJSONParser =
			new BookingSyncJSONParser();

		return bookingSyncJSONParser.parseToDTOs(json);
	}

	public static String toJSON(BookingSync bookingSync) {
		if (bookingSync == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (bookingSync.getBookingId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookingId\": ");

			sb.append(bookingSync.getBookingId());
		}

		if (bookingSync.getSynced() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"synced\": ");

			sb.append(bookingSync.getSynced());
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		BookingSyncJSONParser bookingSyncJSONParser =
			new BookingSyncJSONParser();

		return bookingSyncJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(BookingSync bookingSync) {
		if (bookingSync == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (bookingSync.getBookingId() == null) {
			map.put("bookingId", null);
		}
		else {
			map.put("bookingId", String.valueOf(bookingSync.getBookingId()));
		}

		if (bookingSync.getSynced() == null) {
			map.put("synced", null);
		}
		else {
			map.put("synced", String.valueOf(bookingSync.getSynced()));
		}

		return map;
	}

	public static class BookingSyncJSONParser
		extends BaseJSONParser<BookingSync> {

		@Override
		protected BookingSync createDTO() {
			return new BookingSync();
		}

		@Override
		protected BookingSync[] createDTOArray(int size) {
			return new BookingSync[size];
		}

		@Override
		protected void setField(
			BookingSync bookingSync, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "bookingId")) {
				if (jsonParserFieldValue != null) {
					bookingSync.setBookingId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "synced")) {
				if (jsonParserFieldValue != null) {
					bookingSync.setSynced((Boolean)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}