package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.EventsSection;
import misk.headless.client.dto.v1_0.FlipCard;
import misk.headless.client.dto.v1_0.HighlightsSection;
import misk.headless.client.dto.v1_0.StoriesSection;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.HomeSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Home implements Cloneable, Serializable {

	public static Home toDTO(String json) {
		return HomeSerDes.toDTO(json);
	}

	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventsSection eventsSection;

	public FlipCard[] getFlipCards() {
		return flipCards;
	}

	public void setFlipCards(FlipCard[] flipCards) {
		this.flipCards = flipCards;
	}

	public void setFlipCards(
		UnsafeSupplier<FlipCard[], Exception> flipCardsUnsafeSupplier) {

		try {
			flipCards = flipCardsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected FlipCard[] flipCards;

	public HighlightsSection getHighlightsSection() {
		return highlightsSection;
	}

	public void setHighlightsSection(HighlightsSection highlightsSection) {
		this.highlightsSection = highlightsSection;
	}

	public void setHighlightsSection(
		UnsafeSupplier<HighlightsSection, Exception>
			highlightsSectionUnsafeSupplier) {

		try {
			highlightsSection = highlightsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected HighlightsSection highlightsSection;

	public StoriesSection getStoriesSection() {
		return storiesSection;
	}

	public void setStoriesSection(StoriesSection storiesSection) {
		this.storiesSection = storiesSection;
	}

	public void setStoriesSection(
		UnsafeSupplier<StoriesSection, Exception>
			storiesSectionUnsafeSupplier) {

		try {
			storiesSection = storiesSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected StoriesSection storiesSection;

	@Override
	public Home clone() throws CloneNotSupportedException {
		return (Home)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Home)) {
			return false;
		}

		Home home = (Home)object;

		return Objects.equals(toString(), home.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return HomeSerDes.toJSON(this);
	}

}