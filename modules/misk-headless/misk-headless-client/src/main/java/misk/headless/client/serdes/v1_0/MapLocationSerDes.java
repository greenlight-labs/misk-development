package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.MapLocation;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MapLocationSerDes {

	public static MapLocation toDTO(String json) {
		MapLocationJSONParser mapLocationJSONParser =
			new MapLocationJSONParser();

		return mapLocationJSONParser.parseToDTO(json);
	}

	public static MapLocation[] toDTOs(String json) {
		MapLocationJSONParser mapLocationJSONParser =
			new MapLocationJSONParser();

		return mapLocationJSONParser.parseToDTOs(json);
	}

	public static String toJSON(MapLocation mapLocation) {
		if (mapLocation == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (mapLocation.getCallUs() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"callUs\": ");

			sb.append("\"");

			sb.append(_escape(mapLocation.getCallUs()));

			sb.append("\"");
		}

		if (mapLocation.getEmail() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"email\": ");

			sb.append("\"");

			sb.append(_escape(mapLocation.getEmail()));

			sb.append("\"");
		}

		if (mapLocation.getLatitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"latitude\": ");

			sb.append("\"");

			sb.append(_escape(mapLocation.getLatitude()));

			sb.append("\"");
		}

		if (mapLocation.getLongitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"longitude\": ");

			sb.append("\"");

			sb.append(_escape(mapLocation.getLongitude()));

			sb.append("\"");
		}

		if (mapLocation.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(mapLocation.getTitle()));

			sb.append("\"");
		}

		if (mapLocation.getWhatsapp() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"whatsapp\": ");

			sb.append("\"");

			sb.append(_escape(mapLocation.getWhatsapp()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		MapLocationJSONParser mapLocationJSONParser =
			new MapLocationJSONParser();

		return mapLocationJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(MapLocation mapLocation) {
		if (mapLocation == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (mapLocation.getCallUs() == null) {
			map.put("callUs", null);
		}
		else {
			map.put("callUs", String.valueOf(mapLocation.getCallUs()));
		}

		if (mapLocation.getEmail() == null) {
			map.put("email", null);
		}
		else {
			map.put("email", String.valueOf(mapLocation.getEmail()));
		}

		if (mapLocation.getLatitude() == null) {
			map.put("latitude", null);
		}
		else {
			map.put("latitude", String.valueOf(mapLocation.getLatitude()));
		}

		if (mapLocation.getLongitude() == null) {
			map.put("longitude", null);
		}
		else {
			map.put("longitude", String.valueOf(mapLocation.getLongitude()));
		}

		if (mapLocation.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(mapLocation.getTitle()));
		}

		if (mapLocation.getWhatsapp() == null) {
			map.put("whatsapp", null);
		}
		else {
			map.put("whatsapp", String.valueOf(mapLocation.getWhatsapp()));
		}

		return map;
	}

	public static class MapLocationJSONParser
		extends BaseJSONParser<MapLocation> {

		@Override
		protected MapLocation createDTO() {
			return new MapLocation();
		}

		@Override
		protected MapLocation[] createDTOArray(int size) {
			return new MapLocation[size];
		}

		@Override
		protected void setField(
			MapLocation mapLocation, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "callUs")) {
				if (jsonParserFieldValue != null) {
					mapLocation.setCallUs((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "email")) {
				if (jsonParserFieldValue != null) {
					mapLocation.setEmail((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "latitude")) {
				if (jsonParserFieldValue != null) {
					mapLocation.setLatitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "longitude")) {
				if (jsonParserFieldValue != null) {
					mapLocation.setLongitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					mapLocation.setTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "whatsapp")) {
				if (jsonParserFieldValue != null) {
					mapLocation.setWhatsapp((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}