package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ExploreExperientialCenterCategory;
import misk.headless.client.dto.v1_0.ExploreExperientialCenterPage;
import misk.headless.client.dto.v1_0.ExploreExperientialCenterPost;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreExperientialCenterPageSerDes {

	public static ExploreExperientialCenterPage toDTO(String json) {
		ExploreExperientialCenterPageJSONParser
			exploreExperientialCenterPageJSONParser =
				new ExploreExperientialCenterPageJSONParser();

		return exploreExperientialCenterPageJSONParser.parseToDTO(json);
	}

	public static ExploreExperientialCenterPage[] toDTOs(String json) {
		ExploreExperientialCenterPageJSONParser
			exploreExperientialCenterPageJSONParser =
				new ExploreExperientialCenterPageJSONParser();

		return exploreExperientialCenterPageJSONParser.parseToDTOs(json);
	}

	public static String toJSON(
		ExploreExperientialCenterPage exploreExperientialCenterPage) {

		if (exploreExperientialCenterPage == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (exploreExperientialCenterPage.
				getExploreExperientialCenterCategories() != null) {

			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreExperientialCenterCategories\": ");

			sb.append("[");

			for (int i = 0;
				 i < exploreExperientialCenterPage.
					 getExploreExperientialCenterCategories().length;
				 i++) {

				sb.append(
					String.valueOf(
						exploreExperientialCenterPage.
							getExploreExperientialCenterCategories()[i]));

				if ((i + 1) < exploreExperientialCenterPage.
						getExploreExperientialCenterCategories().length) {

					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (exploreExperientialCenterPage.getExploreExperientialCenterPosts() !=
				null) {

			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreExperientialCenterPosts\": ");

			sb.append("[");

			for (int i = 0;
				 i < exploreExperientialCenterPage.
					 getExploreExperientialCenterPosts().length;
				 i++) {

				sb.append(
					String.valueOf(
						exploreExperientialCenterPage.
							getExploreExperientialCenterPosts()[i]));

				if ((i + 1) < exploreExperientialCenterPage.
						getExploreExperientialCenterPosts().length) {

					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (exploreExperientialCenterPage.getPageTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"pageTitle\": ");

			sb.append("\"");

			sb.append(_escape(exploreExperientialCenterPage.getPageTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ExploreExperientialCenterPageJSONParser
			exploreExperientialCenterPageJSONParser =
				new ExploreExperientialCenterPageJSONParser();

		return exploreExperientialCenterPageJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		ExploreExperientialCenterPage exploreExperientialCenterPage) {

		if (exploreExperientialCenterPage == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (exploreExperientialCenterPage.
				getExploreExperientialCenterCategories() == null) {

			map.put("exploreExperientialCenterCategories", null);
		}
		else {
			map.put(
				"exploreExperientialCenterCategories",
				String.valueOf(
					exploreExperientialCenterPage.
						getExploreExperientialCenterCategories()));
		}

		if (exploreExperientialCenterPage.getExploreExperientialCenterPosts() ==
				null) {

			map.put("exploreExperientialCenterPosts", null);
		}
		else {
			map.put(
				"exploreExperientialCenterPosts",
				String.valueOf(
					exploreExperientialCenterPage.
						getExploreExperientialCenterPosts()));
		}

		if (exploreExperientialCenterPage.getPageTitle() == null) {
			map.put("pageTitle", null);
		}
		else {
			map.put(
				"pageTitle",
				String.valueOf(exploreExperientialCenterPage.getPageTitle()));
		}

		return map;
	}

	public static class ExploreExperientialCenterPageJSONParser
		extends BaseJSONParser<ExploreExperientialCenterPage> {

		@Override
		protected ExploreExperientialCenterPage createDTO() {
			return new ExploreExperientialCenterPage();
		}

		@Override
		protected ExploreExperientialCenterPage[] createDTOArray(int size) {
			return new ExploreExperientialCenterPage[size];
		}

		@Override
		protected void setField(
			ExploreExperientialCenterPage exploreExperientialCenterPage,
			String jsonParserFieldName, Object jsonParserFieldValue) {

			if (Objects.equals(
					jsonParserFieldName,
					"exploreExperientialCenterCategories")) {

				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPage.
						setExploreExperientialCenterCategories(
							Stream.of(
								toStrings((Object[])jsonParserFieldValue)
							).map(
								object ->
									ExploreExperientialCenterCategorySerDes.
										toDTO((String)object)
							).toArray(
								size ->
									new ExploreExperientialCenterCategory[size]
							));
				}
			}
			else if (Objects.equals(
						jsonParserFieldName,
						"exploreExperientialCenterPosts")) {

				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPage.
						setExploreExperientialCenterPosts(
							Stream.of(
								toStrings((Object[])jsonParserFieldValue)
							).map(
								object ->
									ExploreExperientialCenterPostSerDes.toDTO(
										(String)object)
							).toArray(
								size -> new ExploreExperientialCenterPost[size]
							));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "pageTitle")) {
				if (jsonParserFieldValue != null) {
					exploreExperientialCenterPage.setPageTitle(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}