package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.DeleteProfileImage;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DeleteProfileImageSerDes {

	public static DeleteProfileImage toDTO(String json) {
		DeleteProfileImageJSONParser deleteProfileImageJSONParser =
			new DeleteProfileImageJSONParser();

		return deleteProfileImageJSONParser.parseToDTO(json);
	}

	public static DeleteProfileImage[] toDTOs(String json) {
		DeleteProfileImageJSONParser deleteProfileImageJSONParser =
			new DeleteProfileImageJSONParser();

		return deleteProfileImageJSONParser.parseToDTOs(json);
	}

	public static String toJSON(DeleteProfileImage deleteProfileImage) {
		if (deleteProfileImage == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (deleteProfileImage.getAppUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(deleteProfileImage.getAppUserId());
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		DeleteProfileImageJSONParser deleteProfileImageJSONParser =
			new DeleteProfileImageJSONParser();

		return deleteProfileImageJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		DeleteProfileImage deleteProfileImage) {

		if (deleteProfileImage == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (deleteProfileImage.getAppUserId() == null) {
			map.put("appUserId", null);
		}
		else {
			map.put(
				"appUserId", String.valueOf(deleteProfileImage.getAppUserId()));
		}

		return map;
	}

	public static class DeleteProfileImageJSONParser
		extends BaseJSONParser<DeleteProfileImage> {

		@Override
		protected DeleteProfileImage createDTO() {
			return new DeleteProfileImage();
		}

		@Override
		protected DeleteProfileImage[] createDTOArray(int size) {
			return new DeleteProfileImage[size];
		}

		@Override
		protected void setField(
			DeleteProfileImage deleteProfileImage, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "appUserId")) {
				if (jsonParserFieldValue != null) {
					deleteProfileImage.setAppUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}