package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Schedule;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ScheduleSerDes {

	public static Schedule toDTO(String json) {
		ScheduleJSONParser scheduleJSONParser = new ScheduleJSONParser();

		return scheduleJSONParser.parseToDTO(json);
	}

	public static Schedule[] toDTOs(String json) {
		ScheduleJSONParser scheduleJSONParser = new ScheduleJSONParser();

		return scheduleJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Schedule schedule) {
		if (schedule == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (schedule.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(schedule.getDescription()));

			sb.append("\"");
		}

		if (schedule.getDuration() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"duration\": ");

			sb.append("\"");

			sb.append(_escape(schedule.getDuration()));

			sb.append("\"");
		}

		if (schedule.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(schedule.getImage()));

			sb.append("\"");
		}

		if (schedule.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(schedule.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ScheduleJSONParser scheduleJSONParser = new ScheduleJSONParser();

		return scheduleJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Schedule schedule) {
		if (schedule == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (schedule.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(schedule.getDescription()));
		}

		if (schedule.getDuration() == null) {
			map.put("duration", null);
		}
		else {
			map.put("duration", String.valueOf(schedule.getDuration()));
		}

		if (schedule.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(schedule.getImage()));
		}

		if (schedule.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(schedule.getTitle()));
		}

		return map;
	}

	public static class ScheduleJSONParser extends BaseJSONParser<Schedule> {

		@Override
		protected Schedule createDTO() {
			return new Schedule();
		}

		@Override
		protected Schedule[] createDTOArray(int size) {
			return new Schedule[size];
		}

		@Override
		protected void setField(
			Schedule schedule, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					schedule.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "duration")) {
				if (jsonParserFieldValue != null) {
					schedule.setDuration((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					schedule.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					schedule.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}