package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Attraction;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.AttractionSectionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AttractionSection implements Cloneable, Serializable {

	public static AttractionSection toDTO(String json) {
		return AttractionSectionSerDes.toDTO(json);
	}

	public Attraction[] getItems() {
		return items;
	}

	public void setItems(Attraction[] items) {
		this.items = items;
	}

	public void setItems(
		UnsafeSupplier<Attraction[], Exception> itemsUnsafeSupplier) {

		try {
			items = itemsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Attraction[] items;

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public void setSectionTitle(
		UnsafeSupplier<String, Exception> sectionTitleUnsafeSupplier) {

		try {
			sectionTitle = sectionTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String sectionTitle;

	@Override
	public AttractionSection clone() throws CloneNotSupportedException {
		return (AttractionSection)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AttractionSection)) {
			return false;
		}

		AttractionSection attractionSection = (AttractionSection)object;

		return Objects.equals(toString(), attractionSection.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return AttractionSectionSerDes.toJSON(this);
	}

}