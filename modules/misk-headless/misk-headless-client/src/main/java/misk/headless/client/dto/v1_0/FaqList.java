package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.FaqListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FaqList implements Cloneable, Serializable {

	public static FaqList toDTO(String json) {
		return FaqListSerDes.toDTO(json);
	}

	public String getFaqAnswer() {
		return faqAnswer;
	}

	public void setFaqAnswer(String faqAnswer) {
		this.faqAnswer = faqAnswer;
	}

	public void setFaqAnswer(
		UnsafeSupplier<String, Exception> faqAnswerUnsafeSupplier) {

		try {
			faqAnswer = faqAnswerUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String faqAnswer;

	public String getFaqQuestion() {
		return faqQuestion;
	}

	public void setFaqQuestion(String faqQuestion) {
		this.faqQuestion = faqQuestion;
	}

	public void setFaqQuestion(
		UnsafeSupplier<String, Exception> faqQuestionUnsafeSupplier) {

		try {
			faqQuestion = faqQuestionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String faqQuestion;

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public void setStatus(
		UnsafeSupplier<Boolean, Exception> statusUnsafeSupplier) {

		try {
			status = statusUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean status;

	@Override
	public FaqList clone() throws CloneNotSupportedException {
		return (FaqList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FaqList)) {
			return false;
		}

		FaqList faqList = (FaqList)object;

		return Objects.equals(toString(), faqList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return FaqListSerDes.toJSON(this);
	}

}