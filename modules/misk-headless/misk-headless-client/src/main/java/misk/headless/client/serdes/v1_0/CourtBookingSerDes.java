package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.CourtBooking;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CourtBookingSerDes {

	public static CourtBooking toDTO(String json) {
		CourtBookingJSONParser courtBookingJSONParser =
			new CourtBookingJSONParser();

		return courtBookingJSONParser.parseToDTO(json);
	}

	public static CourtBooking[] toDTOs(String json) {
		CourtBookingJSONParser courtBookingJSONParser =
			new CourtBookingJSONParser();

		return courtBookingJSONParser.parseToDTOs(json);
	}

	public static String toJSON(CourtBooking courtBooking) {
		if (courtBooking == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (courtBooking.getBookingDate() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookingDate\": ");

			sb.append("\"");

			sb.append(_escape(courtBooking.getBookingDate()));

			sb.append("\"");
		}

		if (courtBooking.getBookingId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookingId\": ");

			sb.append(courtBooking.getBookingId());
		}

		if (courtBooking.getSlotEndTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotEndTime\": ");

			sb.append("\"");

			sb.append(_escape(courtBooking.getSlotEndTime()));

			sb.append("\"");
		}

		if (courtBooking.getSlotStartTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotStartTime\": ");

			sb.append("\"");

			sb.append(_escape(courtBooking.getSlotStartTime()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		CourtBookingJSONParser courtBookingJSONParser =
			new CourtBookingJSONParser();

		return courtBookingJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(CourtBooking courtBooking) {
		if (courtBooking == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (courtBooking.getBookingDate() == null) {
			map.put("bookingDate", null);
		}
		else {
			map.put(
				"bookingDate", String.valueOf(courtBooking.getBookingDate()));
		}

		if (courtBooking.getBookingId() == null) {
			map.put("bookingId", null);
		}
		else {
			map.put("bookingId", String.valueOf(courtBooking.getBookingId()));
		}

		if (courtBooking.getSlotEndTime() == null) {
			map.put("slotEndTime", null);
		}
		else {
			map.put(
				"slotEndTime", String.valueOf(courtBooking.getSlotEndTime()));
		}

		if (courtBooking.getSlotStartTime() == null) {
			map.put("slotStartTime", null);
		}
		else {
			map.put(
				"slotStartTime",
				String.valueOf(courtBooking.getSlotStartTime()));
		}

		return map;
	}

	public static class CourtBookingJSONParser
		extends BaseJSONParser<CourtBooking> {

		@Override
		protected CourtBooking createDTO() {
			return new CourtBooking();
		}

		@Override
		protected CourtBooking[] createDTOArray(int size) {
			return new CourtBooking[size];
		}

		@Override
		protected void setField(
			CourtBooking courtBooking, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "bookingDate")) {
				if (jsonParserFieldValue != null) {
					courtBooking.setBookingDate((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bookingId")) {
				if (jsonParserFieldValue != null) {
					courtBooking.setBookingId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "slotEndTime")) {
				if (jsonParserFieldValue != null) {
					courtBooking.setSlotEndTime((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "slotStartTime")) {
				if (jsonParserFieldValue != null) {
					courtBooking.setSlotStartTime((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}