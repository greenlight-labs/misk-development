package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AmenitiesList;
import misk.headless.client.dto.v1_0.BookSpaceList;
import misk.headless.client.dto.v1_0.EventsSection;
import misk.headless.client.dto.v1_0.GalleryImagesList;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.MiskForumSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class MiskForum implements Cloneable, Serializable {

	public static MiskForum toDTO(String json) {
		return MiskForumSerDes.toDTO(json);
	}

	public String getAmenitiesLabel() {
		return AmenitiesLabel;
	}

	public void setAmenitiesLabel(String AmenitiesLabel) {
		this.AmenitiesLabel = AmenitiesLabel;
	}

	public void setAmenitiesLabel(
		UnsafeSupplier<String, Exception> AmenitiesLabelUnsafeSupplier) {

		try {
			AmenitiesLabel = AmenitiesLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String AmenitiesLabel;

	public AmenitiesList[] getAmenitiesList() {
		return AmenitiesList;
	}

	public void setAmenitiesList(AmenitiesList[] AmenitiesList) {
		this.AmenitiesList = AmenitiesList;
	}

	public void setAmenitiesList(
		UnsafeSupplier<AmenitiesList[], Exception>
			AmenitiesListUnsafeSupplier) {

		try {
			AmenitiesList = AmenitiesListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected AmenitiesList[] AmenitiesList;

	public String getBookSpaceLabel() {
		return BookSpaceLabel;
	}

	public void setBookSpaceLabel(String BookSpaceLabel) {
		this.BookSpaceLabel = BookSpaceLabel;
	}

	public void setBookSpaceLabel(
		UnsafeSupplier<String, Exception> BookSpaceLabelUnsafeSupplier) {

		try {
			BookSpaceLabel = BookSpaceLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String BookSpaceLabel;

	public BookSpaceList[] getBookSpaceList() {
		return BookSpaceList;
	}

	public void setBookSpaceList(BookSpaceList[] BookSpaceList) {
		this.BookSpaceList = BookSpaceList;
	}

	public void setBookSpaceList(
		UnsafeSupplier<BookSpaceList[], Exception>
			BookSpaceListUnsafeSupplier) {

		try {
			BookSpaceList = BookSpaceListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected BookSpaceList[] BookSpaceList;

	public String getContactEmailAddress() {
		return ContactEmailAddress;
	}

	public void setContactEmailAddress(String ContactEmailAddress) {
		this.ContactEmailAddress = ContactEmailAddress;
	}

	public void setContactEmailAddress(
		UnsafeSupplier<String, Exception> ContactEmailAddressUnsafeSupplier) {

		try {
			ContactEmailAddress = ContactEmailAddressUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String ContactEmailAddress;

	public String getContactLabel() {
		return ContactLabel;
	}

	public void setContactLabel(String ContactLabel) {
		this.ContactLabel = ContactLabel;
	}

	public void setContactLabel(
		UnsafeSupplier<String, Exception> ContactLabelUnsafeSupplier) {

		try {
			ContactLabel = ContactLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String ContactLabel;

	public String getContactTelephone() {
		return ContactTelephone;
	}

	public void setContactTelephone(String ContactTelephone) {
		this.ContactTelephone = ContactTelephone;
	}

	public void setContactTelephone(
		UnsafeSupplier<String, Exception> ContactTelephoneUnsafeSupplier) {

		try {
			ContactTelephone = ContactTelephoneUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String ContactTelephone;

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	public void setDescription(
		UnsafeSupplier<String, Exception> DescriptionUnsafeSupplier) {

		try {
			Description = DescriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String Description;

	public String getEventLabel() {
		return EventLabel;
	}

	public void setEventLabel(String EventLabel) {
		this.EventLabel = EventLabel;
	}

	public void setEventLabel(
		UnsafeSupplier<String, Exception> EventLabelUnsafeSupplier) {

		try {
			EventLabel = EventLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String EventLabel;

	public String getEventName() {
		return EventName;
	}

	public void setEventName(String EventName) {
		this.EventName = EventName;
	}

	public void setEventName(
		UnsafeSupplier<String, Exception> EventNameUnsafeSupplier) {

		try {
			EventName = EventNameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String EventName;

	public String[] getGalleryImagesList() {
		return GalleryImagesList;
	}

	public void setGalleryImagesList(String[] GalleryImagesList) {
		this.GalleryImagesList = GalleryImagesList;
	}

	public void setGalleryImagesList(
		UnsafeSupplier<String[], Exception> GalleryImagesListUnsafeSupplier) {

		try {
			GalleryImagesList = GalleryImagesListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] GalleryImagesList;

	public String getGalleryLabel() {
		return GalleryLabel;
	}

	public void setGalleryLabel(String GalleryLabel) {
		this.GalleryLabel = GalleryLabel;
	}

	public void setGalleryLabel(
		UnsafeSupplier<String, Exception> GalleryLabelUnsafeSupplier) {

		try {
			GalleryLabel = GalleryLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String GalleryLabel;

	public String getLocationLabel() {
		return LocationLabel;
	}

	public void setLocationLabel(String LocationLabel) {
		this.LocationLabel = LocationLabel;
	}

	public void setLocationLabel(
		UnsafeSupplier<String, Exception> LocationLabelUnsafeSupplier) {

		try {
			LocationLabel = LocationLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String LocationLabel;

	public String getLocationLatitude() {
		return LocationLatitude;
	}

	public void setLocationLatitude(String LocationLatitude) {
		this.LocationLatitude = LocationLatitude;
	}

	public void setLocationLatitude(
		UnsafeSupplier<String, Exception> LocationLatitudeUnsafeSupplier) {

		try {
			LocationLatitude = LocationLatitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String LocationLatitude;

	public String getLocationLongitude() {
		return LocationLongitude;
	}

	public void setLocationLongitude(String LocationLongitude) {
		this.LocationLongitude = LocationLongitude;
	}

	public void setLocationLongitude(
		UnsafeSupplier<String, Exception> LocationLongitudeUnsafeSupplier) {

		try {
			LocationLongitude = LocationLongitudeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String LocationLongitude;

	public String getMiskForumBanner() {
		return MiskForumBanner;
	}

	public void setMiskForumBanner(String MiskForumBanner) {
		this.MiskForumBanner = MiskForumBanner;
	}

	public void setMiskForumBanner(
		UnsafeSupplier<String, Exception> MiskForumBannerUnsafeSupplier) {

		try {
			MiskForumBanner = MiskForumBannerUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String MiskForumBanner;

	public String getMiskForumLabel() {
		return MiskForumLabel;
	}

	public void setMiskForumLabel(String MiskForumLabel) {
		this.MiskForumLabel = MiskForumLabel;
	}

	public void setMiskForumLabel(
		UnsafeSupplier<String, Exception> MiskForumLabelUnsafeSupplier) {

		try {
			MiskForumLabel = MiskForumLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String MiskForumLabel;

	public String getWorkingDay() {
		return WorkingDay;
	}

	public void setWorkingDay(String WorkingDay) {
		this.WorkingDay = WorkingDay;
	}

	public void setWorkingDay(
		UnsafeSupplier<String, Exception> WorkingDayUnsafeSupplier) {

		try {
			WorkingDay = WorkingDayUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WorkingDay;

	public String getWorkingHours() {
		return WorkingHours;
	}

	public void setWorkingHours(String WorkingHours) {
		this.WorkingHours = WorkingHours;
	}

	public void setWorkingHours(
		UnsafeSupplier<String, Exception> WorkingHoursUnsafeSupplier) {

		try {
			WorkingHours = WorkingHoursUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WorkingHours;

	public String getWorkingHoursLabel() {
		return WorkingHoursLabel;
	}

	public void setWorkingHoursLabel(String WorkingHoursLabel) {
		this.WorkingHoursLabel = WorkingHoursLabel;
	}

	public void setWorkingHoursLabel(
		UnsafeSupplier<String, Exception> WorkingHoursLabelUnsafeSupplier) {

		try {
			WorkingHoursLabel = WorkingHoursLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String WorkingHoursLabel;

	public EventsSection getEventsSection() {
		return eventsSection;
	}

	public void setEventsSection(EventsSection eventsSection) {
		this.eventsSection = eventsSection;
	}

	public void setEventsSection(
		UnsafeSupplier<EventsSection, Exception> eventsSectionUnsafeSupplier) {

		try {
			eventsSection = eventsSectionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventsSection eventsSection;

	@Override
	public MiskForum clone() throws CloneNotSupportedException {
		return (MiskForum)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof MiskForum)) {
			return false;
		}

		MiskForum miskForum = (MiskForum)object;

		return Objects.equals(toString(), miskForum.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return MiskForumSerDes.toJSON(this);
	}

}