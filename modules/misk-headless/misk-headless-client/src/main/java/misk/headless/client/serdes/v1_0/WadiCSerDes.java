package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.WadiC;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class WadiCSerDes {

	public static WadiC toDTO(String json) {
		WadiCJSONParser wadiCJSONParser = new WadiCJSONParser();

		return wadiCJSONParser.parseToDTO(json);
	}

	public static WadiC[] toDTOs(String json) {
		WadiCJSONParser wadiCJSONParser = new WadiCJSONParser();

		return wadiCJSONParser.parseToDTOs(json);
	}

	public static String toJSON(WadiC wadiC) {
		if (wadiC == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (wadiC.getAttractionSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"attractionSection\": ");

			sb.append(String.valueOf(wadiC.getAttractionSection()));
		}

		if (wadiC.getBannerSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bannerSection\": ");

			sb.append(String.valueOf(wadiC.getBannerSection()));
		}

		if (wadiC.getBookCourtSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"bookCourtSection\": ");

			sb.append(String.valueOf(wadiC.getBookCourtSection()));
		}

		if (wadiC.getEventsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(wadiC.getEventsSection()));
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		WadiCJSONParser wadiCJSONParser = new WadiCJSONParser();

		return wadiCJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(WadiC wadiC) {
		if (wadiC == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (wadiC.getAttractionSection() == null) {
			map.put("attractionSection", null);
		}
		else {
			map.put(
				"attractionSection",
				String.valueOf(wadiC.getAttractionSection()));
		}

		if (wadiC.getBannerSection() == null) {
			map.put("bannerSection", null);
		}
		else {
			map.put("bannerSection", String.valueOf(wadiC.getBannerSection()));
		}

		if (wadiC.getBookCourtSection() == null) {
			map.put("bookCourtSection", null);
		}
		else {
			map.put(
				"bookCourtSection",
				String.valueOf(wadiC.getBookCourtSection()));
		}

		if (wadiC.getEventsSection() == null) {
			map.put("eventsSection", null);
		}
		else {
			map.put("eventsSection", String.valueOf(wadiC.getEventsSection()));
		}

		return map;
	}

	public static class WadiCJSONParser extends BaseJSONParser<WadiC> {

		@Override
		protected WadiC createDTO() {
			return new WadiC();
		}

		@Override
		protected WadiC[] createDTOArray(int size) {
			return new WadiC[size];
		}

		@Override
		protected void setField(
			WadiC wadiC, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "attractionSection")) {
				if (jsonParserFieldValue != null) {
					wadiC.setAttractionSection(
						AttractionSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bannerSection")) {
				if (jsonParserFieldValue != null) {
					wadiC.setBannerSection(
						BannerSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "bookCourtSection")) {
				if (jsonParserFieldValue != null) {
					wadiC.setBookCourtSection(
						BookCourtSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "eventsSection")) {
				if (jsonParserFieldValue != null) {
					wadiC.setEventsSection(
						EventsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}