package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Welcome;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class WelcomeSerDes {

	public static Welcome toDTO(String json) {
		WelcomeJSONParser welcomeJSONParser = new WelcomeJSONParser();

		return welcomeJSONParser.parseToDTO(json);
	}

	public static Welcome[] toDTOs(String json) {
		WelcomeJSONParser welcomeJSONParser = new WelcomeJSONParser();

		return welcomeJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Welcome welcome) {
		if (welcome == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (welcome.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(welcome.getDescription()));

			sb.append("\"");
		}

		if (welcome.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(welcome.getImage()));

			sb.append("\"");
		}

		if (welcome.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(welcome.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		WelcomeJSONParser welcomeJSONParser = new WelcomeJSONParser();

		return welcomeJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Welcome welcome) {
		if (welcome == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (welcome.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(welcome.getDescription()));
		}

		if (welcome.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(welcome.getImage()));
		}

		if (welcome.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(welcome.getTitle()));
		}

		return map;
	}

	public static class WelcomeJSONParser extends BaseJSONParser<Welcome> {

		@Override
		protected Welcome createDTO() {
			return new Welcome();
		}

		@Override
		protected Welcome[] createDTOArray(int size) {
			return new Welcome[size];
		}

		@Override
		protected void setField(
			Welcome welcome, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					welcome.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					welcome.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					welcome.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}