package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AddVisitor;
import misk.headless.client.dto.v1_0.CategoryList;
import misk.headless.client.dto.v1_0.Schedule;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.EventListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class EventList implements Cloneable, Serializable {

	public static EventList toDTO(String json) {
		return EventListSerDes.toDTO(json);
	}

	public AddVisitor getAttendesList() {
		return attendesList;
	}

	public void setAttendesList(AddVisitor attendesList) {
		this.attendesList = attendesList;
	}

	public void setAttendesList(
		UnsafeSupplier<AddVisitor, Exception> attendesListUnsafeSupplier) {

		try {
			attendesList = attendesListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected AddVisitor attendesList;

	public String getBuyTicketLink() {
		return buyTicketLink;
	}

	public void setBuyTicketLink(String buyTicketLink) {
		this.buyTicketLink = buyTicketLink;
	}

	public void setBuyTicketLink(
		UnsafeSupplier<String, Exception> buyTicketLinkUnsafeSupplier) {

		try {
			buyTicketLink = buyTicketLinkUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String buyTicketLink;

	public String getCatColor() {
		return catColor;
	}

	public void setCatColor(String catColor) {
		this.catColor = catColor;
	}

	public void setCatColor(
		UnsafeSupplier<String, Exception> catColorUnsafeSupplier) {

		try {
			catColor = catColorUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String catColor;

	public CategoryList[] getCategoryLists() {
		return categoryLists;
	}

	public void setCategoryLists(CategoryList[] categoryLists) {
		this.categoryLists = categoryLists;
	}

	public void setCategoryLists(
		UnsafeSupplier<CategoryList[], Exception> categoryListsUnsafeSupplier) {

		try {
			categoryLists = categoryListsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected CategoryList[] categoryLists;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setCurrency(
		UnsafeSupplier<String, Exception> currencyUnsafeSupplier) {

		try {
			currency = currencyUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String currency;

	public String getDisplaydate() {
		return displaydate;
	}

	public void setDisplaydate(String displaydate) {
		this.displaydate = displaydate;
	}

	public void setDisplaydate(
		UnsafeSupplier<String, Exception> displaydateUnsafeSupplier) {

		try {
			displaydate = displaydateUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String displaydate;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEmail(
		UnsafeSupplier<String, Exception> emailUnsafeSupplier) {

		try {
			email = emailUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String email;

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public void setEventDescription(
		UnsafeSupplier<String, Exception> eventDescriptionUnsafeSupplier) {

		try {
			eventDescription = eventDescriptionUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventDescription;

	public String getEventEndDateTime() {
		return eventEndDateTime;
	}

	public void setEventEndDateTime(String eventEndDateTime) {
		this.eventEndDateTime = eventEndDateTime;
	}

	public void setEventEndDateTime(
		UnsafeSupplier<String, Exception> eventEndDateTimeUnsafeSupplier) {

		try {
			eventEndDateTime = eventEndDateTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventEndDateTime;

	public String getEventFeaturedImage() {
		return eventFeaturedImage;
	}

	public void setEventFeaturedImage(String eventFeaturedImage) {
		this.eventFeaturedImage = eventFeaturedImage;
	}

	public void setEventFeaturedImage(
		UnsafeSupplier<String, Exception> eventFeaturedImageUnsafeSupplier) {

		try {
			eventFeaturedImage = eventFeaturedImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventFeaturedImage;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public void setEventId(
		UnsafeSupplier<Long, Exception> eventIdUnsafeSupplier) {

		try {
			eventId = eventIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long eventId;

	public EventList[] getEventLists() {
		return eventLists;
	}

	public void setEventLists(EventList[] eventLists) {
		this.eventLists = eventLists;
	}

	public void setEventLists(
		UnsafeSupplier<EventList[], Exception> eventListsUnsafeSupplier) {

		try {
			eventLists = eventListsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected EventList[] eventLists;

	public String getEventLocation() {
		return eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	public void setEventLocation(
		UnsafeSupplier<String, Exception> eventLocationUnsafeSupplier) {

		try {
			eventLocation = eventLocationUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventLocation;

	public String getEventMonth() {
		return eventMonth;
	}

	public void setEventMonth(String eventMonth) {
		this.eventMonth = eventMonth;
	}

	public void setEventMonth(
		UnsafeSupplier<String, Exception> eventMonthUnsafeSupplier) {

		try {
			eventMonth = eventMonthUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventMonth;

	public String getEventPrice() {
		return eventPrice;
	}

	public void setEventPrice(String eventPrice) {
		this.eventPrice = eventPrice;
	}

	public void setEventPrice(
		UnsafeSupplier<String, Exception> eventPriceUnsafeSupplier) {

		try {
			eventPrice = eventPriceUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventPrice;

	public String getEventStartDateTime() {
		return eventStartDateTime;
	}

	public void setEventStartDateTime(String eventStartDateTime) {
		this.eventStartDateTime = eventStartDateTime;
	}

	public void setEventStartDateTime(
		UnsafeSupplier<String, Exception> eventStartDateTimeUnsafeSupplier) {

		try {
			eventStartDateTime = eventStartDateTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventStartDateTime;

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public void setEventType(
		UnsafeSupplier<String, Exception> eventTypeUnsafeSupplier) {

		try {
			eventType = eventTypeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventType;

	public String getEventcategory() {
		return eventcategory;
	}

	public void setEventcategory(String eventcategory) {
		this.eventcategory = eventcategory;
	}

	public void setEventcategory(
		UnsafeSupplier<String, Exception> eventcategoryUnsafeSupplier) {

		try {
			eventcategory = eventcategoryUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String eventcategory;

	public Boolean getIsFavourite() {
		return isFavourite;
	}

	public void setIsFavourite(Boolean isFavourite) {
		this.isFavourite = isFavourite;
	}

	public void setIsFavourite(
		UnsafeSupplier<Boolean, Exception> isFavouriteUnsafeSupplier) {

		try {
			isFavourite = isFavouriteUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean isFavourite;

	public String getLan() {
		return lan;
	}

	public void setLan(String lan) {
		this.lan = lan;
	}

	public void setLan(UnsafeSupplier<String, Exception> lanUnsafeSupplier) {
		try {
			lan = lanUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String lan;

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public void setLat(UnsafeSupplier<String, Exception> latUnsafeSupplier) {
		try {
			lat = latUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String lat;

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setLink(UnsafeSupplier<String, Exception> linkUnsafeSupplier) {
		try {
			link = linkUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String link;

	public String getLocationLabel() {
		return locationLabel;
	}

	public void setLocationLabel(String locationLabel) {
		this.locationLabel = locationLabel;
	}

	public void setLocationLabel(
		UnsafeSupplier<String, Exception> locationLabelUnsafeSupplier) {

		try {
			locationLabel = locationLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String locationLabel;

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public void setPayType(
		UnsafeSupplier<String, Exception> payTypeUnsafeSupplier) {

		try {
			payType = payTypeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String payType;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPhone(
		UnsafeSupplier<String, Exception> phoneUnsafeSupplier) {

		try {
			phone = phoneUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String phone;

	public Schedule[] getSchedules() {
		return schedules;
	}

	public void setSchedules(Schedule[] schedules) {
		this.schedules = schedules;
	}

	public void setSchedules(
		UnsafeSupplier<Schedule[], Exception> schedulesUnsafeSupplier) {

		try {
			schedules = schedulesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Schedule[] schedules;

	public String[] getSliderimage() {
		return sliderimage;
	}

	public void setSliderimage(String[] sliderimage) {
		this.sliderimage = sliderimage;
	}

	public void setSliderimage(
		UnsafeSupplier<String[], Exception> sliderimageUnsafeSupplier) {

		try {
			sliderimage = sliderimageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String[] sliderimage;

	public String getTagSlug() {
		return tagSlug;
	}

	public void setTagSlug(String tagSlug) {
		this.tagSlug = tagSlug;
	}

	public void setTagSlug(
		UnsafeSupplier<String, Exception> tagSlugUnsafeSupplier) {

		try {
			tagSlug = tagSlugUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String tagSlug;

	public String getTagTitle() {
		return tagTitle;
	}

	public void setTagTitle(String tagTitle) {
		this.tagTitle = tagTitle;
	}

	public void setTagTitle(
		UnsafeSupplier<String, Exception> tagTitleUnsafeSupplier) {

		try {
			tagTitle = tagTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String tagTitle;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setTime(UnsafeSupplier<String, Exception> timeUnsafeSupplier) {
		try {
			time = timeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String time;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public void setVenue(
		UnsafeSupplier<String, Exception> venueUnsafeSupplier) {

		try {
			venue = venueUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String venue;

	@Override
	public EventList clone() throws CloneNotSupportedException {
		return (EventList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof EventList)) {
			return false;
		}

		EventList eventList = (EventList)object;

		return Objects.equals(toString(), eventList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return EventListSerDes.toJSON(this);
	}

}