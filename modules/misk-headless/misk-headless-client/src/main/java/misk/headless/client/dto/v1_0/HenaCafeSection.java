package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.HenaCafeSectionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HenaCafeSection implements Cloneable, Serializable {

	public static HenaCafeSection toDTO(String json) {
		return HenaCafeSectionSerDes.toDTO(json);
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setIcon(UnsafeSupplier<String, Exception> iconUnsafeSupplier) {
		try {
			icon = iconUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String icon;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setImage(
		UnsafeSupplier<String, Exception> imageUnsafeSupplier) {

		try {
			image = imageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String image;

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public void setSectionTitle(
		UnsafeSupplier<String, Exception> sectionTitleUnsafeSupplier) {

		try {
			sectionTitle = sectionTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String sectionTitle;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle(
		UnsafeSupplier<String, Exception> titleUnsafeSupplier) {

		try {
			title = titleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String title;

	@Override
	public HenaCafeSection clone() throws CloneNotSupportedException {
		return (HenaCafeSection)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof HenaCafeSection)) {
			return false;
		}

		HenaCafeSection henaCafeSection = (HenaCafeSection)object;

		return Objects.equals(toString(), henaCafeSection.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return HenaCafeSectionSerDes.toJSON(this);
	}

}