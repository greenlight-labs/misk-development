package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.dto.v1_0.EventsSection;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class EventsSectionSerDes {

	public static EventsSection toDTO(String json) {
		EventsSectionJSONParser eventsSectionJSONParser =
			new EventsSectionJSONParser();

		return eventsSectionJSONParser.parseToDTO(json);
	}

	public static EventsSection[] toDTOs(String json) {
		EventsSectionJSONParser eventsSectionJSONParser =
			new EventsSectionJSONParser();

		return eventsSectionJSONParser.parseToDTOs(json);
	}

	public static String toJSON(EventsSection eventsSection) {
		if (eventsSection == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (eventsSection.getItems() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"items\": ");

			sb.append("[");

			for (int i = 0; i < eventsSection.getItems().length; i++) {
				sb.append(String.valueOf(eventsSection.getItems()[i]));

				if ((i + 1) < eventsSection.getItems().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (eventsSection.getSectionTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"sectionTitle\": ");

			sb.append("\"");

			sb.append(_escape(eventsSection.getSectionTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		EventsSectionJSONParser eventsSectionJSONParser =
			new EventsSectionJSONParser();

		return eventsSectionJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(EventsSection eventsSection) {
		if (eventsSection == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (eventsSection.getItems() == null) {
			map.put("items", null);
		}
		else {
			map.put("items", String.valueOf(eventsSection.getItems()));
		}

		if (eventsSection.getSectionTitle() == null) {
			map.put("sectionTitle", null);
		}
		else {
			map.put(
				"sectionTitle",
				String.valueOf(eventsSection.getSectionTitle()));
		}

		return map;
	}

	public static class EventsSectionJSONParser
		extends BaseJSONParser<EventsSection> {

		@Override
		protected EventsSection createDTO() {
			return new EventsSection();
		}

		@Override
		protected EventsSection[] createDTOArray(int size) {
			return new EventsSection[size];
		}

		@Override
		protected void setField(
			EventsSection eventsSection, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "items")) {
				if (jsonParserFieldValue != null) {
					eventsSection.setItems(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> EventListSerDes.toDTO((String)object)
						).toArray(
							size -> new EventList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "sectionTitle")) {
				if (jsonParserFieldValue != null) {
					eventsSection.setSectionTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}