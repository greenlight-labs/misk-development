package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Explore;
import misk.headless.client.dto.v1_0.ExploreList;
import misk.headless.client.dto.v1_0.FaclitiesList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ExploreSerDes {

	public static Explore toDTO(String json) {
		ExploreJSONParser exploreJSONParser = new ExploreJSONParser();

		return exploreJSONParser.parseToDTO(json);
	}

	public static Explore[] toDTOs(String json) {
		ExploreJSONParser exploreJSONParser = new ExploreJSONParser();

		return exploreJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Explore explore) {
		if (explore == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (explore.getEventsSection() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"eventsSection\": ");

			sb.append(String.valueOf(explore.getEventsSection()));
		}

		if (explore.getExploreLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreLabel\": ");

			sb.append("\"");

			sb.append(_escape(explore.getExploreLabel()));

			sb.append("\"");
		}

		if (explore.getExploreList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"exploreList\": ");

			sb.append("[");

			for (int i = 0; i < explore.getExploreList().length; i++) {
				sb.append(String.valueOf(explore.getExploreList()[i]));

				if ((i + 1) < explore.getExploreList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (explore.getExpolerId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"expolerId\": ");

			sb.append("\"");

			sb.append(_escape(explore.getExpolerId()));

			sb.append("\"");
		}

		if (explore.getFaclitiesLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesLabel\": ");

			sb.append("\"");

			sb.append(_escape(explore.getFaclitiesLabel()));

			sb.append("\"");
		}

		if (explore.getFaclitiesList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faclitiesList\": ");

			sb.append("[");

			for (int i = 0; i < explore.getFaclitiesList().length; i++) {
				sb.append(String.valueOf(explore.getFaclitiesList()[i]));

				if ((i + 1) < explore.getFaclitiesList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (explore.getGalleryImagesList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryImagesList\": ");

			sb.append("[");

			for (int i = 0; i < explore.getGalleryImagesList().length; i++) {
				sb.append("\"");

				sb.append(_escape(explore.getGalleryImagesList()[i]));

				sb.append("\"");

				if ((i + 1) < explore.getGalleryImagesList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (explore.getGalleryLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleryLabel\": ");

			sb.append("\"");

			sb.append(_escape(explore.getGalleryLabel()));

			sb.append("\"");
		}

		if (explore.getHeaderBanner() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"headerBanner\": ");

			sb.append("\"");

			sb.append(_escape(explore.getHeaderBanner()));

			sb.append("\"");
		}

		if (explore.getHeaderDesc() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"headerDesc\": ");

			sb.append("\"");

			sb.append(_escape(explore.getHeaderDesc()));

			sb.append("\"");
		}

		if (explore.getHeaderTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"headerTitle\": ");

			sb.append("\"");

			sb.append(_escape(explore.getHeaderTitle()));

			sb.append("\"");
		}

		if (explore.getLocationLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLabel\": ");

			sb.append("\"");

			sb.append(_escape(explore.getLocationLabel()));

			sb.append("\"");
		}

		if (explore.getLocationLatitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLatitude\": ");

			sb.append("\"");

			sb.append(_escape(explore.getLocationLatitude()));

			sb.append("\"");
		}

		if (explore.getLocationLongitude() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"locationLongitude\": ");

			sb.append("\"");

			sb.append(_escape(explore.getLocationLongitude()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ExploreJSONParser exploreJSONParser = new ExploreJSONParser();

		return exploreJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Explore explore) {
		if (explore == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (explore.getEventsSection() == null) {
			map.put("eventsSection", null);
		}
		else {
			map.put(
				"eventsSection", String.valueOf(explore.getEventsSection()));
		}

		if (explore.getExploreLabel() == null) {
			map.put("exploreLabel", null);
		}
		else {
			map.put("exploreLabel", String.valueOf(explore.getExploreLabel()));
		}

		if (explore.getExploreList() == null) {
			map.put("exploreList", null);
		}
		else {
			map.put("exploreList", String.valueOf(explore.getExploreList()));
		}

		if (explore.getExpolerId() == null) {
			map.put("expolerId", null);
		}
		else {
			map.put("expolerId", String.valueOf(explore.getExpolerId()));
		}

		if (explore.getFaclitiesLabel() == null) {
			map.put("faclitiesLabel", null);
		}
		else {
			map.put(
				"faclitiesLabel", String.valueOf(explore.getFaclitiesLabel()));
		}

		if (explore.getFaclitiesList() == null) {
			map.put("faclitiesList", null);
		}
		else {
			map.put(
				"faclitiesList", String.valueOf(explore.getFaclitiesList()));
		}

		if (explore.getGalleryImagesList() == null) {
			map.put("galleryImagesList", null);
		}
		else {
			map.put(
				"galleryImagesList",
				String.valueOf(explore.getGalleryImagesList()));
		}

		if (explore.getGalleryLabel() == null) {
			map.put("galleryLabel", null);
		}
		else {
			map.put("galleryLabel", String.valueOf(explore.getGalleryLabel()));
		}

		if (explore.getHeaderBanner() == null) {
			map.put("headerBanner", null);
		}
		else {
			map.put("headerBanner", String.valueOf(explore.getHeaderBanner()));
		}

		if (explore.getHeaderDesc() == null) {
			map.put("headerDesc", null);
		}
		else {
			map.put("headerDesc", String.valueOf(explore.getHeaderDesc()));
		}

		if (explore.getHeaderTitle() == null) {
			map.put("headerTitle", null);
		}
		else {
			map.put("headerTitle", String.valueOf(explore.getHeaderTitle()));
		}

		if (explore.getLocationLabel() == null) {
			map.put("locationLabel", null);
		}
		else {
			map.put(
				"locationLabel", String.valueOf(explore.getLocationLabel()));
		}

		if (explore.getLocationLatitude() == null) {
			map.put("locationLatitude", null);
		}
		else {
			map.put(
				"locationLatitude",
				String.valueOf(explore.getLocationLatitude()));
		}

		if (explore.getLocationLongitude() == null) {
			map.put("locationLongitude", null);
		}
		else {
			map.put(
				"locationLongitude",
				String.valueOf(explore.getLocationLongitude()));
		}

		return map;
	}

	public static class ExploreJSONParser extends BaseJSONParser<Explore> {

		@Override
		protected Explore createDTO() {
			return new Explore();
		}

		@Override
		protected Explore[] createDTOArray(int size) {
			return new Explore[size];
		}

		@Override
		protected void setField(
			Explore explore, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "eventsSection")) {
				if (jsonParserFieldValue != null) {
					explore.setEventsSection(
						EventsSectionSerDes.toDTO(
							(String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "exploreLabel")) {
				if (jsonParserFieldValue != null) {
					explore.setExploreLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "exploreList")) {
				if (jsonParserFieldValue != null) {
					explore.setExploreList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> ExploreListSerDes.toDTO((String)object)
						).toArray(
							size -> new ExploreList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "expolerId")) {
				if (jsonParserFieldValue != null) {
					explore.setExpolerId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "faclitiesLabel")) {
				if (jsonParserFieldValue != null) {
					explore.setFaclitiesLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "faclitiesList")) {
				if (jsonParserFieldValue != null) {
					explore.setFaclitiesList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> FaclitiesListSerDes.toDTO((String)object)
						).toArray(
							size -> new FaclitiesList[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "galleryImagesList")) {
				if (jsonParserFieldValue != null) {
					explore.setGalleryImagesList(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "galleryLabel")) {
				if (jsonParserFieldValue != null) {
					explore.setGalleryLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "headerBanner")) {
				if (jsonParserFieldValue != null) {
					explore.setHeaderBanner((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "headerDesc")) {
				if (jsonParserFieldValue != null) {
					explore.setHeaderDesc((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "headerTitle")) {
				if (jsonParserFieldValue != null) {
					explore.setHeaderTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationLabel")) {
				if (jsonParserFieldValue != null) {
					explore.setLocationLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationLatitude")) {
				if (jsonParserFieldValue != null) {
					explore.setLocationLatitude((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "locationLongitude")) {
				if (jsonParserFieldValue != null) {
					explore.setLocationLongitude((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}