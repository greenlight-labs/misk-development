package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.BookingTermsConditions;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BookingTermsConditionsSerDes {

	public static BookingTermsConditions toDTO(String json) {
		BookingTermsConditionsJSONParser bookingTermsConditionsJSONParser =
			new BookingTermsConditionsJSONParser();

		return bookingTermsConditionsJSONParser.parseToDTO(json);
	}

	public static BookingTermsConditions[] toDTOs(String json) {
		BookingTermsConditionsJSONParser bookingTermsConditionsJSONParser =
			new BookingTermsConditionsJSONParser();

		return bookingTermsConditionsJSONParser.parseToDTOs(json);
	}

	public static String toJSON(BookingTermsConditions bookingTermsConditions) {
		if (bookingTermsConditions == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (bookingTermsConditions.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(bookingTermsConditions.getDescription()));

			sb.append("\"");
		}

		if (bookingTermsConditions.getExcerpt() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"excerpt\": ");

			sb.append("\"");

			sb.append(_escape(bookingTermsConditions.getExcerpt()));

			sb.append("\"");
		}

		if (bookingTermsConditions.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(bookingTermsConditions.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		BookingTermsConditionsJSONParser bookingTermsConditionsJSONParser =
			new BookingTermsConditionsJSONParser();

		return bookingTermsConditionsJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		BookingTermsConditions bookingTermsConditions) {

		if (bookingTermsConditions == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (bookingTermsConditions.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put(
				"description",
				String.valueOf(bookingTermsConditions.getDescription()));
		}

		if (bookingTermsConditions.getExcerpt() == null) {
			map.put("excerpt", null);
		}
		else {
			map.put(
				"excerpt", String.valueOf(bookingTermsConditions.getExcerpt()));
		}

		if (bookingTermsConditions.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(bookingTermsConditions.getTitle()));
		}

		return map;
	}

	public static class BookingTermsConditionsJSONParser
		extends BaseJSONParser<BookingTermsConditions> {

		@Override
		protected BookingTermsConditions createDTO() {
			return new BookingTermsConditions();
		}

		@Override
		protected BookingTermsConditions[] createDTOArray(int size) {
			return new BookingTermsConditions[size];
		}

		@Override
		protected void setField(
			BookingTermsConditions bookingTermsConditions,
			String jsonParserFieldName, Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					bookingTermsConditions.setDescription(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "excerpt")) {
				if (jsonParserFieldValue != null) {
					bookingTermsConditions.setExcerpt(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					bookingTermsConditions.setTitle(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}