package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.DetailImageSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DetailImage implements Cloneable, Serializable {

	public static DetailImage toDTO(String json) {
		return DetailImageSerDes.toDTO(json);
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public void setImages(
		UnsafeSupplier<String, Exception> imagesUnsafeSupplier) {

		try {
			images = imagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String images;

	@Override
	public DetailImage clone() throws CloneNotSupportedException {
		return (DetailImage)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DetailImage)) {
			return false;
		}

		DetailImage detailImage = (DetailImage)object;

		return Objects.equals(toString(), detailImage.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return DetailImageSerDes.toJSON(this);
	}

}