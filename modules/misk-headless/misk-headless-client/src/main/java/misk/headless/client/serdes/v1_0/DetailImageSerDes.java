package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.DetailImage;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DetailImageSerDes {

	public static DetailImage toDTO(String json) {
		DetailImageJSONParser detailImageJSONParser =
			new DetailImageJSONParser();

		return detailImageJSONParser.parseToDTO(json);
	}

	public static DetailImage[] toDTOs(String json) {
		DetailImageJSONParser detailImageJSONParser =
			new DetailImageJSONParser();

		return detailImageJSONParser.parseToDTOs(json);
	}

	public static String toJSON(DetailImage detailImage) {
		if (detailImage == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (detailImage.getImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"images\": ");

			sb.append("\"");

			sb.append(_escape(detailImage.getImages()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		DetailImageJSONParser detailImageJSONParser =
			new DetailImageJSONParser();

		return detailImageJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(DetailImage detailImage) {
		if (detailImage == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (detailImage.getImages() == null) {
			map.put("images", null);
		}
		else {
			map.put("images", String.valueOf(detailImage.getImages()));
		}

		return map;
	}

	public static class DetailImageJSONParser
		extends BaseJSONParser<DetailImage> {

		@Override
		protected DetailImage createDTO() {
			return new DetailImage();
		}

		@Override
		protected DetailImage[] createDTOArray(int size) {
			return new DetailImage[size];
		}

		@Override
		protected void setField(
			DetailImage detailImage, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "images")) {
				if (jsonParserFieldValue != null) {
					detailImage.setImages((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}