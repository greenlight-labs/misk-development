package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.TimeSlotSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class TimeSlot implements Cloneable, Serializable {

	public static TimeSlot toDTO(String json) {
		return TimeSlotSerDes.toDTO(json);
	}

	public String getSlotEndTime() {
		return slotEndTime;
	}

	public void setSlotEndTime(String slotEndTime) {
		this.slotEndTime = slotEndTime;
	}

	public void setSlotEndTime(
		UnsafeSupplier<String, Exception> slotEndTimeUnsafeSupplier) {

		try {
			slotEndTime = slotEndTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String slotEndTime;

	public String getSlotStartTime() {
		return slotStartTime;
	}

	public void setSlotStartTime(String slotStartTime) {
		this.slotStartTime = slotStartTime;
	}

	public void setSlotStartTime(
		UnsafeSupplier<String, Exception> slotStartTimeUnsafeSupplier) {

		try {
			slotStartTime = slotStartTimeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String slotStartTime;

	@Override
	public TimeSlot clone() throws CloneNotSupportedException {
		return (TimeSlot)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof TimeSlot)) {
			return false;
		}

		TimeSlot timeSlot = (TimeSlot)object;

		return Objects.equals(toString(), timeSlot.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return TimeSlotSerDes.toJSON(this);
	}

}