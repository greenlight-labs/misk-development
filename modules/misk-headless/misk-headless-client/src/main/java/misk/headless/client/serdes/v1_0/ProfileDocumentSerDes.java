package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ProfileDocument;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ProfileDocumentSerDes {

	public static ProfileDocument toDTO(String json) {
		ProfileDocumentJSONParser profileDocumentJSONParser =
			new ProfileDocumentJSONParser();

		return profileDocumentJSONParser.parseToDTO(json);
	}

	public static ProfileDocument[] toDTOs(String json) {
		ProfileDocumentJSONParser profileDocumentJSONParser =
			new ProfileDocumentJSONParser();

		return profileDocumentJSONParser.parseToDTOs(json);
	}

	public static String toJSON(ProfileDocument profileDocument) {
		if (profileDocument == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (profileDocument.getFormImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"formImage\": ");

			sb.append("\"");

			sb.append(_escape(profileDocument.getFormImage()));

			sb.append("\"");
		}

		if (profileDocument.getFormPdf() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"formPdf\": ");

			sb.append("\"");

			sb.append(_escape(profileDocument.getFormPdf()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ProfileDocumentJSONParser profileDocumentJSONParser =
			new ProfileDocumentJSONParser();

		return profileDocumentJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(ProfileDocument profileDocument) {
		if (profileDocument == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (profileDocument.getFormImage() == null) {
			map.put("formImage", null);
		}
		else {
			map.put(
				"formImage", String.valueOf(profileDocument.getFormImage()));
		}

		if (profileDocument.getFormPdf() == null) {
			map.put("formPdf", null);
		}
		else {
			map.put("formPdf", String.valueOf(profileDocument.getFormPdf()));
		}

		return map;
	}

	public static class ProfileDocumentJSONParser
		extends BaseJSONParser<ProfileDocument> {

		@Override
		protected ProfileDocument createDTO() {
			return new ProfileDocument();
		}

		@Override
		protected ProfileDocument[] createDTOArray(int size) {
			return new ProfileDocument[size];
		}

		@Override
		protected void setField(
			ProfileDocument profileDocument, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "formImage")) {
				if (jsonParserFieldValue != null) {
					profileDocument.setFormImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "formPdf")) {
				if (jsonParserFieldValue != null) {
					profileDocument.setFormPdf((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}