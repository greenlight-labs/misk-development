package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.ProfileDocumentList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class ProfileDocumentListSerDes {

	public static ProfileDocumentList toDTO(String json) {
		ProfileDocumentListJSONParser profileDocumentListJSONParser =
			new ProfileDocumentListJSONParser();

		return profileDocumentListJSONParser.parseToDTO(json);
	}

	public static ProfileDocumentList[] toDTOs(String json) {
		ProfileDocumentListJSONParser profileDocumentListJSONParser =
			new ProfileDocumentListJSONParser();

		return profileDocumentListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(ProfileDocumentList profileDocumentList) {
		if (profileDocumentList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (profileDocumentList.getAppUserId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"appUserId\": ");

			sb.append(profileDocumentList.getAppUserId());
		}

		if (profileDocumentList.getUploadedFile() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"uploadedFile\": ");

			sb.append("[");

			for (int i = 0; i < profileDocumentList.getUploadedFile().length;
				 i++) {

				sb.append("\"");

				sb.append(_escape(profileDocumentList.getUploadedFile()[i]));

				sb.append("\"");

				if ((i + 1) < profileDocumentList.getUploadedFile().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		ProfileDocumentListJSONParser profileDocumentListJSONParser =
			new ProfileDocumentListJSONParser();

		return profileDocumentListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(
		ProfileDocumentList profileDocumentList) {

		if (profileDocumentList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (profileDocumentList.getAppUserId() == null) {
			map.put("appUserId", null);
		}
		else {
			map.put(
				"appUserId",
				String.valueOf(profileDocumentList.getAppUserId()));
		}

		if (profileDocumentList.getUploadedFile() == null) {
			map.put("uploadedFile", null);
		}
		else {
			map.put(
				"uploadedFile",
				String.valueOf(profileDocumentList.getUploadedFile()));
		}

		return map;
	}

	public static class ProfileDocumentListJSONParser
		extends BaseJSONParser<ProfileDocumentList> {

		@Override
		protected ProfileDocumentList createDTO() {
			return new ProfileDocumentList();
		}

		@Override
		protected ProfileDocumentList[] createDTOArray(int size) {
			return new ProfileDocumentList[size];
		}

		@Override
		protected void setField(
			ProfileDocumentList profileDocumentList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "appUserId")) {
				if (jsonParserFieldValue != null) {
					profileDocumentList.setAppUserId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "uploadedFile")) {
				if (jsonParserFieldValue != null) {
					profileDocumentList.setUploadedFile(
						toStrings((Object[])jsonParserFieldValue));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}