package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Media;
import misk.headless.client.dto.v1_0.Souvenirs;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class SouvenirsSerDes {

	public static Souvenirs toDTO(String json) {
		SouvenirsJSONParser souvenirsJSONParser = new SouvenirsJSONParser();

		return souvenirsJSONParser.parseToDTO(json);
	}

	public static Souvenirs[] toDTOs(String json) {
		SouvenirsJSONParser souvenirsJSONParser = new SouvenirsJSONParser();

		return souvenirsJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Souvenirs souvenirs) {
		if (souvenirs == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (souvenirs.getDate() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"Date\": ");

			sb.append("\"");

			sb.append(_escape(souvenirs.getDate()));

			sb.append("\"");
		}

		if (souvenirs.getEmail() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"email\": ");

			sb.append("\"");

			sb.append(_escape(souvenirs.getEmail()));

			sb.append("\"");
		}

		if (souvenirs.getMedia() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"media\": ");

			sb.append("[");

			for (int i = 0; i < souvenirs.getMedia().length; i++) {
				sb.append(String.valueOf(souvenirs.getMedia()[i]));

				if ((i + 1) < souvenirs.getMedia().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		SouvenirsJSONParser souvenirsJSONParser = new SouvenirsJSONParser();

		return souvenirsJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Souvenirs souvenirs) {
		if (souvenirs == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (souvenirs.getDate() == null) {
			map.put("Date", null);
		}
		else {
			map.put("Date", String.valueOf(souvenirs.getDate()));
		}

		if (souvenirs.getEmail() == null) {
			map.put("email", null);
		}
		else {
			map.put("email", String.valueOf(souvenirs.getEmail()));
		}

		if (souvenirs.getMedia() == null) {
			map.put("media", null);
		}
		else {
			map.put("media", String.valueOf(souvenirs.getMedia()));
		}

		return map;
	}

	public static class SouvenirsJSONParser extends BaseJSONParser<Souvenirs> {

		@Override
		protected Souvenirs createDTO() {
			return new Souvenirs();
		}

		@Override
		protected Souvenirs[] createDTOArray(int size) {
			return new Souvenirs[size];
		}

		@Override
		protected void setField(
			Souvenirs souvenirs, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "Date")) {
				if (jsonParserFieldValue != null) {
					souvenirs.setDate((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "email")) {
				if (jsonParserFieldValue != null) {
					souvenirs.setEmail((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "media")) {
				if (jsonParserFieldValue != null) {
					souvenirs.setMedia(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> MediaSerDes.toDTO((String)object)
						).toArray(
							size -> new Media[size]
						));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}