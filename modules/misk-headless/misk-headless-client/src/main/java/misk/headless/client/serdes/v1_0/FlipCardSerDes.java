package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.FlipCard;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FlipCardSerDes {

	public static FlipCard toDTO(String json) {
		FlipCardJSONParser flipCardJSONParser = new FlipCardJSONParser();

		return flipCardJSONParser.parseToDTO(json);
	}

	public static FlipCard[] toDTOs(String json) {
		FlipCardJSONParser flipCardJSONParser = new FlipCardJSONParser();

		return flipCardJSONParser.parseToDTOs(json);
	}

	public static String toJSON(FlipCard flipCard) {
		if (flipCard == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (flipCard.getButtonLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"buttonLabel\": ");

			sb.append("\"");

			sb.append(_escape(flipCard.getButtonLabel()));

			sb.append("\"");
		}

		if (flipCard.getHeading() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"heading\": ");

			sb.append("\"");

			sb.append(_escape(flipCard.getHeading()));

			sb.append("\"");
		}

		if (flipCard.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(flipCard.getImage()));

			sb.append("\"");
		}

		if (flipCard.getTitleField1() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"titleField1\": ");

			sb.append("\"");

			sb.append(_escape(flipCard.getTitleField1()));

			sb.append("\"");
		}

		if (flipCard.getTitleField2() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"titleField2\": ");

			sb.append("\"");

			sb.append(_escape(flipCard.getTitleField2()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		FlipCardJSONParser flipCardJSONParser = new FlipCardJSONParser();

		return flipCardJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(FlipCard flipCard) {
		if (flipCard == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (flipCard.getButtonLabel() == null) {
			map.put("buttonLabel", null);
		}
		else {
			map.put("buttonLabel", String.valueOf(flipCard.getButtonLabel()));
		}

		if (flipCard.getHeading() == null) {
			map.put("heading", null);
		}
		else {
			map.put("heading", String.valueOf(flipCard.getHeading()));
		}

		if (flipCard.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(flipCard.getImage()));
		}

		if (flipCard.getTitleField1() == null) {
			map.put("titleField1", null);
		}
		else {
			map.put("titleField1", String.valueOf(flipCard.getTitleField1()));
		}

		if (flipCard.getTitleField2() == null) {
			map.put("titleField2", null);
		}
		else {
			map.put("titleField2", String.valueOf(flipCard.getTitleField2()));
		}

		return map;
	}

	public static class FlipCardJSONParser extends BaseJSONParser<FlipCard> {

		@Override
		protected FlipCard createDTO() {
			return new FlipCard();
		}

		@Override
		protected FlipCard[] createDTOArray(int size) {
			return new FlipCard[size];
		}

		@Override
		protected void setField(
			FlipCard flipCard, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "buttonLabel")) {
				if (jsonParserFieldValue != null) {
					flipCard.setButtonLabel((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "heading")) {
				if (jsonParserFieldValue != null) {
					flipCard.setHeading((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					flipCard.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "titleField1")) {
				if (jsonParserFieldValue != null) {
					flipCard.setTitleField1((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "titleField2")) {
				if (jsonParserFieldValue != null) {
					flipCard.setTitleField2((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}