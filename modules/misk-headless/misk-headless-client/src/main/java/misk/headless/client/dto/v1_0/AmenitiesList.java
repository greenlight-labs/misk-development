package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.AmenitiesListSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AmenitiesList implements Cloneable, Serializable {

	public static AmenitiesList toDTO(String json) {
		return AmenitiesListSerDes.toDTO(json);
	}

	public String getAmenitiesImages() {
		return amenitiesImages;
	}

	public void setAmenitiesImages(String amenitiesImages) {
		this.amenitiesImages = amenitiesImages;
	}

	public void setAmenitiesImages(
		UnsafeSupplier<String, Exception> amenitiesImagesUnsafeSupplier) {

		try {
			amenitiesImages = amenitiesImagesUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String amenitiesImages;

	public String getAmenitiesImagesLabel() {
		return amenitiesImagesLabel;
	}

	public void setAmenitiesImagesLabel(String amenitiesImagesLabel) {
		this.amenitiesImagesLabel = amenitiesImagesLabel;
	}

	public void setAmenitiesImagesLabel(
		UnsafeSupplier<String, Exception> amenitiesImagesLabelUnsafeSupplier) {

		try {
			amenitiesImagesLabel = amenitiesImagesLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String amenitiesImagesLabel;

	@Override
	public AmenitiesList clone() throws CloneNotSupportedException {
		return (AmenitiesList)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AmenitiesList)) {
			return false;
		}

		AmenitiesList amenitiesList = (AmenitiesList)object;

		return Objects.equals(toString(), amenitiesList.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return AmenitiesListSerDes.toJSON(this);
	}

}