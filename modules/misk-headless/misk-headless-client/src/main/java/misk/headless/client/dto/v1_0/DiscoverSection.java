package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Album;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.DiscoverSectionSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class DiscoverSection implements Cloneable, Serializable {

	public static DiscoverSection toDTO(String json) {
		return DiscoverSectionSerDes.toDTO(json);
	}

	public Album[] getItems() {
		return items;
	}

	public void setItems(Album[] items) {
		this.items = items;
	}

	public void setItems(
		UnsafeSupplier<Album[], Exception> itemsUnsafeSupplier) {

		try {
			items = itemsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Album[] items;

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	public void setSectionTitle(
		UnsafeSupplier<String, Exception> sectionTitleUnsafeSupplier) {

		try {
			sectionTitle = sectionTitleUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String sectionTitle;

	@Override
	public DiscoverSection clone() throws CloneNotSupportedException {
		return (DiscoverSection)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DiscoverSection)) {
			return false;
		}

		DiscoverSection discoverSection = (DiscoverSection)object;

		return Objects.equals(toString(), discoverSection.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return DiscoverSectionSerDes.toJSON(this);
	}

}