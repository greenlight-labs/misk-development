package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.SocialMediaList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class SocialMediaListSerDes {

	public static SocialMediaList toDTO(String json) {
		SocialMediaListJSONParser socialMediaListJSONParser =
			new SocialMediaListJSONParser();

		return socialMediaListJSONParser.parseToDTO(json);
	}

	public static SocialMediaList[] toDTOs(String json) {
		SocialMediaListJSONParser socialMediaListJSONParser =
			new SocialMediaListJSONParser();

		return socialMediaListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(SocialMediaList socialMediaList) {
		if (socialMediaList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (socialMediaList.getSocialMediaName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"socialMediaName\": ");

			sb.append("\"");

			sb.append(_escape(socialMediaList.getSocialMediaName()));

			sb.append("\"");
		}

		if (socialMediaList.getSocialMediaURL() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"socialMediaURL\": ");

			sb.append("\"");

			sb.append(_escape(socialMediaList.getSocialMediaURL()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		SocialMediaListJSONParser socialMediaListJSONParser =
			new SocialMediaListJSONParser();

		return socialMediaListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(SocialMediaList socialMediaList) {
		if (socialMediaList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (socialMediaList.getSocialMediaName() == null) {
			map.put("socialMediaName", null);
		}
		else {
			map.put(
				"socialMediaName",
				String.valueOf(socialMediaList.getSocialMediaName()));
		}

		if (socialMediaList.getSocialMediaURL() == null) {
			map.put("socialMediaURL", null);
		}
		else {
			map.put(
				"socialMediaURL",
				String.valueOf(socialMediaList.getSocialMediaURL()));
		}

		return map;
	}

	public static class SocialMediaListJSONParser
		extends BaseJSONParser<SocialMediaList> {

		@Override
		protected SocialMediaList createDTO() {
			return new SocialMediaList();
		}

		@Override
		protected SocialMediaList[] createDTOArray(int size) {
			return new SocialMediaList[size];
		}

		@Override
		protected void setField(
			SocialMediaList socialMediaList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "socialMediaName")) {
				if (jsonParserFieldValue != null) {
					socialMediaList.setSocialMediaName(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "socialMediaURL")) {
				if (jsonParserFieldValue != null) {
					socialMediaList.setSocialMediaURL(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}