package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.TimeSlot;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class TimeSlotSerDes {

	public static TimeSlot toDTO(String json) {
		TimeSlotJSONParser timeSlotJSONParser = new TimeSlotJSONParser();

		return timeSlotJSONParser.parseToDTO(json);
	}

	public static TimeSlot[] toDTOs(String json) {
		TimeSlotJSONParser timeSlotJSONParser = new TimeSlotJSONParser();

		return timeSlotJSONParser.parseToDTOs(json);
	}

	public static String toJSON(TimeSlot timeSlot) {
		if (timeSlot == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (timeSlot.getSlotEndTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotEndTime\": ");

			sb.append("\"");

			sb.append(_escape(timeSlot.getSlotEndTime()));

			sb.append("\"");
		}

		if (timeSlot.getSlotStartTime() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"slotStartTime\": ");

			sb.append("\"");

			sb.append(_escape(timeSlot.getSlotStartTime()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		TimeSlotJSONParser timeSlotJSONParser = new TimeSlotJSONParser();

		return timeSlotJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(TimeSlot timeSlot) {
		if (timeSlot == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (timeSlot.getSlotEndTime() == null) {
			map.put("slotEndTime", null);
		}
		else {
			map.put("slotEndTime", String.valueOf(timeSlot.getSlotEndTime()));
		}

		if (timeSlot.getSlotStartTime() == null) {
			map.put("slotStartTime", null);
		}
		else {
			map.put(
				"slotStartTime", String.valueOf(timeSlot.getSlotStartTime()));
		}

		return map;
	}

	public static class TimeSlotJSONParser extends BaseJSONParser<TimeSlot> {

		@Override
		protected TimeSlot createDTO() {
			return new TimeSlot();
		}

		@Override
		protected TimeSlot[] createDTOArray(int size) {
			return new TimeSlot[size];
		}

		@Override
		protected void setField(
			TimeSlot timeSlot, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "slotEndTime")) {
				if (jsonParserFieldValue != null) {
					timeSlot.setSlotEndTime((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "slotStartTime")) {
				if (jsonParserFieldValue != null) {
					timeSlot.setSlotStartTime((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}