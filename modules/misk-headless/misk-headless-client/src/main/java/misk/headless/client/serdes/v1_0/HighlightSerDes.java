package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Highlight;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class HighlightSerDes {

	public static Highlight toDTO(String json) {
		HighlightJSONParser highlightJSONParser = new HighlightJSONParser();

		return highlightJSONParser.parseToDTO(json);
	}

	public static Highlight[] toDTOs(String json) {
		HighlightJSONParser highlightJSONParser = new HighlightJSONParser();

		return highlightJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Highlight highlight) {
		if (highlight == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (highlight.getAudio() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"audio\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getAudio()));

			sb.append("\"");
		}

		if (highlight.getCatColor() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catColor\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getCatColor()));

			sb.append("\"");
		}

		if (highlight.getCatName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catName\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getCatName()));

			sb.append("\"");
		}

		if (highlight.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getDescription()));

			sb.append("\"");
		}

		if (highlight.getFacebook() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"facebook\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getFacebook()));

			sb.append("\"");
		}

		if (highlight.getHighlightId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"highlightId\": ");

			sb.append(highlight.getHighlightId());
		}

		if (highlight.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getImage()));

			sb.append("\"");
		}

		if (highlight.getIsFavourite() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"isFavourite\": ");

			sb.append(highlight.getIsFavourite());
		}

		if (highlight.getLinkedin() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"linkedin\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getLinkedin()));

			sb.append("\"");
		}

		if (highlight.getLocation() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"location\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getLocation()));

			sb.append("\"");
		}

		if (highlight.getName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"name\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getName()));

			sb.append("\"");
		}

		if (highlight.getProfession() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"profession\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getProfession()));

			sb.append("\"");
		}

		if (highlight.getTagDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"tagDescription\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getTagDescription()));

			sb.append("\"");
		}

		if (highlight.getTagSlug() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"tagSlug\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getTagSlug()));

			sb.append("\"");
		}

		if (highlight.getTagTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"tagTitle\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getTagTitle()));

			sb.append("\"");
		}

		if (highlight.getTwitter() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"twitter\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getTwitter()));

			sb.append("\"");
		}

		if (highlight.getType() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"type\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getType()));

			sb.append("\"");
		}

		if (highlight.getVideo() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"video\": ");

			sb.append("\"");

			sb.append(_escape(highlight.getVideo()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		HighlightJSONParser highlightJSONParser = new HighlightJSONParser();

		return highlightJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Highlight highlight) {
		if (highlight == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (highlight.getAudio() == null) {
			map.put("audio", null);
		}
		else {
			map.put("audio", String.valueOf(highlight.getAudio()));
		}

		if (highlight.getCatColor() == null) {
			map.put("catColor", null);
		}
		else {
			map.put("catColor", String.valueOf(highlight.getCatColor()));
		}

		if (highlight.getCatName() == null) {
			map.put("catName", null);
		}
		else {
			map.put("catName", String.valueOf(highlight.getCatName()));
		}

		if (highlight.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(highlight.getDescription()));
		}

		if (highlight.getFacebook() == null) {
			map.put("facebook", null);
		}
		else {
			map.put("facebook", String.valueOf(highlight.getFacebook()));
		}

		if (highlight.getHighlightId() == null) {
			map.put("highlightId", null);
		}
		else {
			map.put("highlightId", String.valueOf(highlight.getHighlightId()));
		}

		if (highlight.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(highlight.getImage()));
		}

		if (highlight.getIsFavourite() == null) {
			map.put("isFavourite", null);
		}
		else {
			map.put("isFavourite", String.valueOf(highlight.getIsFavourite()));
		}

		if (highlight.getLinkedin() == null) {
			map.put("linkedin", null);
		}
		else {
			map.put("linkedin", String.valueOf(highlight.getLinkedin()));
		}

		if (highlight.getLocation() == null) {
			map.put("location", null);
		}
		else {
			map.put("location", String.valueOf(highlight.getLocation()));
		}

		if (highlight.getName() == null) {
			map.put("name", null);
		}
		else {
			map.put("name", String.valueOf(highlight.getName()));
		}

		if (highlight.getProfession() == null) {
			map.put("profession", null);
		}
		else {
			map.put("profession", String.valueOf(highlight.getProfession()));
		}

		if (highlight.getTagDescription() == null) {
			map.put("tagDescription", null);
		}
		else {
			map.put(
				"tagDescription",
				String.valueOf(highlight.getTagDescription()));
		}

		if (highlight.getTagSlug() == null) {
			map.put("tagSlug", null);
		}
		else {
			map.put("tagSlug", String.valueOf(highlight.getTagSlug()));
		}

		if (highlight.getTagTitle() == null) {
			map.put("tagTitle", null);
		}
		else {
			map.put("tagTitle", String.valueOf(highlight.getTagTitle()));
		}

		if (highlight.getTwitter() == null) {
			map.put("twitter", null);
		}
		else {
			map.put("twitter", String.valueOf(highlight.getTwitter()));
		}

		if (highlight.getType() == null) {
			map.put("type", null);
		}
		else {
			map.put("type", String.valueOf(highlight.getType()));
		}

		if (highlight.getVideo() == null) {
			map.put("video", null);
		}
		else {
			map.put("video", String.valueOf(highlight.getVideo()));
		}

		return map;
	}

	public static class HighlightJSONParser extends BaseJSONParser<Highlight> {

		@Override
		protected Highlight createDTO() {
			return new Highlight();
		}

		@Override
		protected Highlight[] createDTOArray(int size) {
			return new Highlight[size];
		}

		@Override
		protected void setField(
			Highlight highlight, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "audio")) {
				if (jsonParserFieldValue != null) {
					highlight.setAudio((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "catColor")) {
				if (jsonParserFieldValue != null) {
					highlight.setCatColor((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "catName")) {
				if (jsonParserFieldValue != null) {
					highlight.setCatName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					highlight.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "facebook")) {
				if (jsonParserFieldValue != null) {
					highlight.setFacebook((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "highlightId")) {
				if (jsonParserFieldValue != null) {
					highlight.setHighlightId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					highlight.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "isFavourite")) {
				if (jsonParserFieldValue != null) {
					highlight.setIsFavourite((Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "linkedin")) {
				if (jsonParserFieldValue != null) {
					highlight.setLinkedin((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "location")) {
				if (jsonParserFieldValue != null) {
					highlight.setLocation((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "name")) {
				if (jsonParserFieldValue != null) {
					highlight.setName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "profession")) {
				if (jsonParserFieldValue != null) {
					highlight.setProfession((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "tagDescription")) {
				if (jsonParserFieldValue != null) {
					highlight.setTagDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "tagSlug")) {
				if (jsonParserFieldValue != null) {
					highlight.setTagSlug((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "tagTitle")) {
				if (jsonParserFieldValue != null) {
					highlight.setTagTitle((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "twitter")) {
				if (jsonParserFieldValue != null) {
					highlight.setTwitter((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "type")) {
				if (jsonParserFieldValue != null) {
					highlight.setType((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "video")) {
				if (jsonParserFieldValue != null) {
					highlight.setVideo((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}