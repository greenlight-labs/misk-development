package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.RequestStatus;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.AppUserSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AppUser implements Cloneable, Serializable {

	public static AppUser toDTO(String json) {
		return AppUserSerDes.toDTO(json);
	}

	public String getAndroidDeviceToken() {
		return androidDeviceToken;
	}

	public void setAndroidDeviceToken(String androidDeviceToken) {
		this.androidDeviceToken = androidDeviceToken;
	}

	public void setAndroidDeviceToken(
		UnsafeSupplier<String, Exception> androidDeviceTokenUnsafeSupplier) {

		try {
			androidDeviceToken = androidDeviceTokenUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String androidDeviceToken;

	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long appUserId;

	public String getAppleUserId() {
		return appleUserId;
	}

	public void setAppleUserId(String appleUserId) {
		this.appleUserId = appleUserId;
	}

	public void setAppleUserId(
		UnsafeSupplier<String, Exception> appleUserIdUnsafeSupplier) {

		try {
			appleUserId = appleUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String appleUserId;

	public Boolean getAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(Boolean autoLogin) {
		this.autoLogin = autoLogin;
	}

	public void setAutoLogin(
		UnsafeSupplier<Boolean, Exception> autoLoginUnsafeSupplier) {

		try {
			autoLogin = autoLoginUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean autoLogin;

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setConfirmPassword(
		UnsafeSupplier<String, Exception> confirmPasswordUnsafeSupplier) {

		try {
			confirmPassword = confirmPasswordUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String confirmPassword;

	public String getDeactivateReason() {
		return deactivateReason;
	}

	public void setDeactivateReason(String deactivateReason) {
		this.deactivateReason = deactivateReason;
	}

	public void setDeactivateReason(
		UnsafeSupplier<String, Exception> deactivateReasonUnsafeSupplier) {

		try {
			deactivateReason = deactivateReasonUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String deactivateReason;

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public void setDeleteReason(
		UnsafeSupplier<String, Exception> deleteReasonUnsafeSupplier) {

		try {
			deleteReason = deleteReasonUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String deleteReason;

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public void setEmailAddress(
		UnsafeSupplier<String, Exception> emailAddressUnsafeSupplier) {

		try {
			emailAddress = emailAddressUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String emailAddress;

	public Boolean getEmailAddressVerified() {
		return emailAddressVerified;
	}

	public void setEmailAddressVerified(Boolean emailAddressVerified) {
		this.emailAddressVerified = emailAddressVerified;
	}

	public void setEmailAddressVerified(
		UnsafeSupplier<Boolean, Exception> emailAddressVerifiedUnsafeSupplier) {

		try {
			emailAddressVerified = emailAddressVerifiedUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean emailAddressVerified;

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public void setFacebookId(
		UnsafeSupplier<String, Exception> facebookIdUnsafeSupplier) {

		try {
			facebookId = facebookIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String facebookId;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setFullName(
		UnsafeSupplier<String, Exception> fullNameUnsafeSupplier) {

		try {
			fullName = fullNameUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String fullName;

	public String getGcpToken() {
		return gcpToken;
	}

	public void setGcpToken(String gcpToken) {
		this.gcpToken = gcpToken;
	}

	public void setGcpToken(
		UnsafeSupplier<String, Exception> gcpTokenUnsafeSupplier) {

		try {
			gcpToken = gcpTokenUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String gcpToken;

	public String getGoogleUserId() {
		return googleUserId;
	}

	public void setGoogleUserId(String googleUserId) {
		this.googleUserId = googleUserId;
	}

	public void setGoogleUserId(
		UnsafeSupplier<String, Exception> googleUserIdUnsafeSupplier) {

		try {
			googleUserId = googleUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String googleUserId;

	public String getIosDeviceToken() {
		return iosDeviceToken;
	}

	public void setIosDeviceToken(String iosDeviceToken) {
		this.iosDeviceToken = iosDeviceToken;
	}

	public void setIosDeviceToken(
		UnsafeSupplier<String, Exception> iosDeviceTokenUnsafeSupplier) {

		try {
			iosDeviceToken = iosDeviceTokenUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String iosDeviceToken;

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public void setIsVerified(
		UnsafeSupplier<Boolean, Exception> isVerifiedUnsafeSupplier) {

		try {
			isVerified = isVerifiedUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean isVerified;

	public String getLanguageId() {
		return languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public void setLanguageId(
		UnsafeSupplier<String, Exception> languageIdUnsafeSupplier) {

		try {
			languageId = languageIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String languageId;

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public void setNewPassword(
		UnsafeSupplier<String, Exception> newPasswordUnsafeSupplier) {

		try {
			newPassword = newPasswordUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String newPassword;

	public String getPasscode() {
		return passcode;
	}

	public void setPasscode(String passcode) {
		this.passcode = passcode;
	}

	public void setPasscode(
		UnsafeSupplier<String, Exception> passcodeUnsafeSupplier) {

		try {
			passcode = passcodeUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String passcode;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPassword(
		UnsafeSupplier<String, Exception> passwordUnsafeSupplier) {

		try {
			password = passwordUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String password;

	public String getPasswordResetToken() {
		return passwordResetToken;
	}

	public void setPasswordResetToken(String passwordResetToken) {
		this.passwordResetToken = passwordResetToken;
	}

	public void setPasswordResetToken(
		UnsafeSupplier<String, Exception> passwordResetTokenUnsafeSupplier) {

		try {
			passwordResetToken = passwordResetTokenUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String passwordResetToken;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setPhoneNumber(
		UnsafeSupplier<String, Exception> phoneNumberUnsafeSupplier) {

		try {
			phoneNumber = phoneNumberUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String phoneNumber;

	public Boolean getPhoneNumberVerified() {
		return phoneNumberVerified;
	}

	public void setPhoneNumberVerified(Boolean phoneNumberVerified) {
		this.phoneNumberVerified = phoneNumberVerified;
	}

	public void setPhoneNumberVerified(
		UnsafeSupplier<Boolean, Exception> phoneNumberVerifiedUnsafeSupplier) {

		try {
			phoneNumberVerified = phoneNumberVerifiedUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean phoneNumberVerified;

	public String getProfileBannerImage() {
		return profileBannerImage;
	}

	public void setProfileBannerImage(String profileBannerImage) {
		this.profileBannerImage = profileBannerImage;
	}

	public void setProfileBannerImage(
		UnsafeSupplier<String, Exception> profileBannerImageUnsafeSupplier) {

		try {
			profileBannerImage = profileBannerImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String profileBannerImage;

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public void setProfileImage(
		UnsafeSupplier<String, Exception> profileImageUnsafeSupplier) {

		try {
			profileImage = profileImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String profileImage;

	public RequestStatus getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus) {
		this.requestStatus = requestStatus;
	}

	public void setRequestStatus(
		UnsafeSupplier<RequestStatus, Exception> requestStatusUnsafeSupplier) {

		try {
			requestStatus = requestStatusUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected RequestStatus requestStatus;

	@Override
	public AppUser clone() throws CloneNotSupportedException {
		return (AppUser)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof AppUser)) {
			return false;
		}

		AppUser appUser = (AppUser)object;

		return Objects.equals(toString(), appUser.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return AppUserSerDes.toJSON(this);
	}

}