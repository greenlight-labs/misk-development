package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.FaqList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FaqListSerDes {

	public static FaqList toDTO(String json) {
		FaqListJSONParser faqListJSONParser = new FaqListJSONParser();

		return faqListJSONParser.parseToDTO(json);
	}

	public static FaqList[] toDTOs(String json) {
		FaqListJSONParser faqListJSONParser = new FaqListJSONParser();

		return faqListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(FaqList faqList) {
		if (faqList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (faqList.getFaqAnswer() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faqAnswer\": ");

			sb.append("\"");

			sb.append(_escape(faqList.getFaqAnswer()));

			sb.append("\"");
		}

		if (faqList.getFaqQuestion() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faqQuestion\": ");

			sb.append("\"");

			sb.append(_escape(faqList.getFaqQuestion()));

			sb.append("\"");
		}

		if (faqList.getStatus() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"status\": ");

			sb.append(faqList.getStatus());
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		FaqListJSONParser faqListJSONParser = new FaqListJSONParser();

		return faqListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(FaqList faqList) {
		if (faqList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (faqList.getFaqAnswer() == null) {
			map.put("faqAnswer", null);
		}
		else {
			map.put("faqAnswer", String.valueOf(faqList.getFaqAnswer()));
		}

		if (faqList.getFaqQuestion() == null) {
			map.put("faqQuestion", null);
		}
		else {
			map.put("faqQuestion", String.valueOf(faqList.getFaqQuestion()));
		}

		if (faqList.getStatus() == null) {
			map.put("status", null);
		}
		else {
			map.put("status", String.valueOf(faqList.getStatus()));
		}

		return map;
	}

	public static class FaqListJSONParser extends BaseJSONParser<FaqList> {

		@Override
		protected FaqList createDTO() {
			return new FaqList();
		}

		@Override
		protected FaqList[] createDTOArray(int size) {
			return new FaqList[size];
		}

		@Override
		protected void setField(
			FaqList faqList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "faqAnswer")) {
				if (jsonParserFieldValue != null) {
					faqList.setFaqAnswer((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "faqQuestion")) {
				if (jsonParserFieldValue != null) {
					faqList.setFaqQuestion((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "status")) {
				if (jsonParserFieldValue != null) {
					faqList.setStatus((Boolean)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}