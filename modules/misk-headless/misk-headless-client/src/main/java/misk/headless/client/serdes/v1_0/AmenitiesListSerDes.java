package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.AmenitiesList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AmenitiesListSerDes {

	public static AmenitiesList toDTO(String json) {
		AmenitiesListJSONParser amenitiesListJSONParser =
			new AmenitiesListJSONParser();

		return amenitiesListJSONParser.parseToDTO(json);
	}

	public static AmenitiesList[] toDTOs(String json) {
		AmenitiesListJSONParser amenitiesListJSONParser =
			new AmenitiesListJSONParser();

		return amenitiesListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(AmenitiesList amenitiesList) {
		if (amenitiesList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (amenitiesList.getAmenitiesImages() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"amenitiesImages\": ");

			sb.append("\"");

			sb.append(_escape(amenitiesList.getAmenitiesImages()));

			sb.append("\"");
		}

		if (amenitiesList.getAmenitiesImagesLabel() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"amenitiesImagesLabel\": ");

			sb.append("\"");

			sb.append(_escape(amenitiesList.getAmenitiesImagesLabel()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		AmenitiesListJSONParser amenitiesListJSONParser =
			new AmenitiesListJSONParser();

		return amenitiesListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(AmenitiesList amenitiesList) {
		if (amenitiesList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (amenitiesList.getAmenitiesImages() == null) {
			map.put("amenitiesImages", null);
		}
		else {
			map.put(
				"amenitiesImages",
				String.valueOf(amenitiesList.getAmenitiesImages()));
		}

		if (amenitiesList.getAmenitiesImagesLabel() == null) {
			map.put("amenitiesImagesLabel", null);
		}
		else {
			map.put(
				"amenitiesImagesLabel",
				String.valueOf(amenitiesList.getAmenitiesImagesLabel()));
		}

		return map;
	}

	public static class AmenitiesListJSONParser
		extends BaseJSONParser<AmenitiesList> {

		@Override
		protected AmenitiesList createDTO() {
			return new AmenitiesList();
		}

		@Override
		protected AmenitiesList[] createDTOArray(int size) {
			return new AmenitiesList[size];
		}

		@Override
		protected void setField(
			AmenitiesList amenitiesList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "amenitiesImages")) {
				if (jsonParserFieldValue != null) {
					amenitiesList.setAmenitiesImages(
						(String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(
						jsonParserFieldName, "amenitiesImagesLabel")) {

				if (jsonParserFieldValue != null) {
					amenitiesList.setAmenitiesImagesLabel(
						(String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}