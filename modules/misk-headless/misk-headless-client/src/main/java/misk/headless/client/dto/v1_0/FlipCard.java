package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.FlipCardSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FlipCard implements Cloneable, Serializable {

	public static FlipCard toDTO(String json) {
		return FlipCardSerDes.toDTO(json);
	}

	public String getButtonLabel() {
		return buttonLabel;
	}

	public void setButtonLabel(String buttonLabel) {
		this.buttonLabel = buttonLabel;
	}

	public void setButtonLabel(
		UnsafeSupplier<String, Exception> buttonLabelUnsafeSupplier) {

		try {
			buttonLabel = buttonLabelUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String buttonLabel;

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public void setHeading(
		UnsafeSupplier<String, Exception> headingUnsafeSupplier) {

		try {
			heading = headingUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String heading;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setImage(
		UnsafeSupplier<String, Exception> imageUnsafeSupplier) {

		try {
			image = imageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String image;

	public String getTitleField1() {
		return titleField1;
	}

	public void setTitleField1(String titleField1) {
		this.titleField1 = titleField1;
	}

	public void setTitleField1(
		UnsafeSupplier<String, Exception> titleField1UnsafeSupplier) {

		try {
			titleField1 = titleField1UnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String titleField1;

	public String getTitleField2() {
		return titleField2;
	}

	public void setTitleField2(String titleField2) {
		this.titleField2 = titleField2;
	}

	public void setTitleField2(
		UnsafeSupplier<String, Exception> titleField2UnsafeSupplier) {

		try {
			titleField2 = titleField2UnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String titleField2;

	@Override
	public FlipCard clone() throws CloneNotSupportedException {
		return (FlipCard)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof FlipCard)) {
			return false;
		}

		FlipCard flipCard = (FlipCard)object;

		return Objects.equals(toString(), flipCard.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return FlipCardSerDes.toJSON(this);
	}

}