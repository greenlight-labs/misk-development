package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Story;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class StorySerDes {

	public static Story toDTO(String json) {
		StoryJSONParser storyJSONParser = new StoryJSONParser();

		return storyJSONParser.parseToDTO(json);
	}

	public static Story[] toDTOs(String json) {
		StoryJSONParser storyJSONParser = new StoryJSONParser();

		return storyJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Story story) {
		if (story == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (story.getDate() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"date\": ");

			sb.append("\"");

			sb.append(_escape(story.getDate()));

			sb.append("\"");
		}

		if (story.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(story.getDescription()));

			sb.append("\"");
		}

		if (story.getImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"image\": ");

			sb.append("\"");

			sb.append(_escape(story.getImage()));

			sb.append("\"");
		}

		if (story.getIsFavourite() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"isFavourite\": ");

			sb.append(story.getIsFavourite());
		}

		if (story.getLink() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"link\": ");

			sb.append("\"");

			sb.append(_escape(story.getLink()));

			sb.append("\"");
		}

		if (story.getStoryId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"storyId\": ");

			sb.append(story.getStoryId());
		}

		if (story.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(story.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		StoryJSONParser storyJSONParser = new StoryJSONParser();

		return storyJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Story story) {
		if (story == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (story.getDate() == null) {
			map.put("date", null);
		}
		else {
			map.put("date", String.valueOf(story.getDate()));
		}

		if (story.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(story.getDescription()));
		}

		if (story.getImage() == null) {
			map.put("image", null);
		}
		else {
			map.put("image", String.valueOf(story.getImage()));
		}

		if (story.getIsFavourite() == null) {
			map.put("isFavourite", null);
		}
		else {
			map.put("isFavourite", String.valueOf(story.getIsFavourite()));
		}

		if (story.getLink() == null) {
			map.put("link", null);
		}
		else {
			map.put("link", String.valueOf(story.getLink()));
		}

		if (story.getStoryId() == null) {
			map.put("storyId", null);
		}
		else {
			map.put("storyId", String.valueOf(story.getStoryId()));
		}

		if (story.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(story.getTitle()));
		}

		return map;
	}

	public static class StoryJSONParser extends BaseJSONParser<Story> {

		@Override
		protected Story createDTO() {
			return new Story();
		}

		@Override
		protected Story[] createDTOArray(int size) {
			return new Story[size];
		}

		@Override
		protected void setField(
			Story story, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "date")) {
				if (jsonParserFieldValue != null) {
					story.setDate((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					story.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "image")) {
				if (jsonParserFieldValue != null) {
					story.setImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "isFavourite")) {
				if (jsonParserFieldValue != null) {
					story.setIsFavourite((Boolean)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "link")) {
				if (jsonParserFieldValue != null) {
					story.setLink((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "storyId")) {
				if (jsonParserFieldValue != null) {
					story.setStoryId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					story.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}