package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Album;
import misk.headless.client.dto.v1_0.Gallery;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AlbumSerDes {

	public static Album toDTO(String json) {
		AlbumJSONParser albumJSONParser = new AlbumJSONParser();

		return albumJSONParser.parseToDTO(json);
	}

	public static Album[] toDTOs(String json) {
		AlbumJSONParser albumJSONParser = new AlbumJSONParser();

		return albumJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Album album) {
		if (album == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (album.getAlbumId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"albumId\": ");

			sb.append(album.getAlbumId());
		}

		if (album.getCategoryName() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"categoryName\": ");

			sb.append("\"");

			sb.append(_escape(album.getCategoryName()));

			sb.append("\"");
		}

		if (album.getDescription() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"description\": ");

			sb.append("\"");

			sb.append(_escape(album.getDescription()));

			sb.append("\"");
		}

		if (album.getGalleries() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"galleries\": ");

			sb.append("[");

			for (int i = 0; i < album.getGalleries().length; i++) {
				sb.append(String.valueOf(album.getGalleries()[i]));

				if ((i + 1) < album.getGalleries().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		if (album.getListingImage() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"listingImage\": ");

			sb.append("\"");

			sb.append(_escape(album.getListingImage()));

			sb.append("\"");
		}

		if (album.getTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"title\": ");

			sb.append("\"");

			sb.append(_escape(album.getTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		AlbumJSONParser albumJSONParser = new AlbumJSONParser();

		return albumJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Album album) {
		if (album == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (album.getAlbumId() == null) {
			map.put("albumId", null);
		}
		else {
			map.put("albumId", String.valueOf(album.getAlbumId()));
		}

		if (album.getCategoryName() == null) {
			map.put("categoryName", null);
		}
		else {
			map.put("categoryName", String.valueOf(album.getCategoryName()));
		}

		if (album.getDescription() == null) {
			map.put("description", null);
		}
		else {
			map.put("description", String.valueOf(album.getDescription()));
		}

		if (album.getGalleries() == null) {
			map.put("galleries", null);
		}
		else {
			map.put("galleries", String.valueOf(album.getGalleries()));
		}

		if (album.getListingImage() == null) {
			map.put("listingImage", null);
		}
		else {
			map.put("listingImage", String.valueOf(album.getListingImage()));
		}

		if (album.getTitle() == null) {
			map.put("title", null);
		}
		else {
			map.put("title", String.valueOf(album.getTitle()));
		}

		return map;
	}

	public static class AlbumJSONParser extends BaseJSONParser<Album> {

		@Override
		protected Album createDTO() {
			return new Album();
		}

		@Override
		protected Album[] createDTOArray(int size) {
			return new Album[size];
		}

		@Override
		protected void setField(
			Album album, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "albumId")) {
				if (jsonParserFieldValue != null) {
					album.setAlbumId(
						Long.valueOf((String)jsonParserFieldValue));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "categoryName")) {
				if (jsonParserFieldValue != null) {
					album.setCategoryName((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "description")) {
				if (jsonParserFieldValue != null) {
					album.setDescription((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "galleries")) {
				if (jsonParserFieldValue != null) {
					album.setGalleries(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> GallerySerDes.toDTO((String)object)
						).toArray(
							size -> new Gallery[size]
						));
				}
			}
			else if (Objects.equals(jsonParserFieldName, "listingImage")) {
				if (jsonParserFieldValue != null) {
					album.setListingImage((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "title")) {
				if (jsonParserFieldValue != null) {
					album.setTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}