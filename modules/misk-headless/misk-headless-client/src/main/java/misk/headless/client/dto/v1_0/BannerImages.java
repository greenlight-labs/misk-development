package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.BannerImagesSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class BannerImages implements Cloneable, Serializable {

	public static BannerImages toDTO(String json) {
		return BannerImagesSerDes.toDTO(json);
	}

	public String getFavoritesBannerImage() {
		return favoritesBannerImage;
	}

	public void setFavoritesBannerImage(String favoritesBannerImage) {
		this.favoritesBannerImage = favoritesBannerImage;
	}

	public void setFavoritesBannerImage(
		UnsafeSupplier<String, Exception> favoritesBannerImageUnsafeSupplier) {

		try {
			favoritesBannerImage = favoritesBannerImageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String favoritesBannerImage;

	public String getSouvenirsImageBanner() {
		return souvenirsImageBanner;
	}

	public void setSouvenirsImageBanner(String souvenirsImageBanner) {
		this.souvenirsImageBanner = souvenirsImageBanner;
	}

	public void setSouvenirsImageBanner(
		UnsafeSupplier<String, Exception> souvenirsImageBannerUnsafeSupplier) {

		try {
			souvenirsImageBanner = souvenirsImageBannerUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected String souvenirsImageBanner;

	@Override
	public BannerImages clone() throws CloneNotSupportedException {
		return (BannerImages)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof BannerImages)) {
			return false;
		}

		BannerImages bannerImages = (BannerImages)object;

		return Objects.equals(toString(), bannerImages.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return BannerImagesSerDes.toJSON(this);
	}

}