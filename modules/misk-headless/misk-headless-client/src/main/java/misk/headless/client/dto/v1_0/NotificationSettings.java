package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.NotificationSettingsSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class NotificationSettings implements Cloneable, Serializable {

	public static NotificationSettings toDTO(String json) {
		return NotificationSettingsSerDes.toDTO(json);
	}

	public Long getAppUserId() {
		return appUserId;
	}

	public void setAppUserId(Long appUserId) {
		this.appUserId = appUserId;
	}

	public void setAppUserId(
		UnsafeSupplier<Long, Exception> appUserIdUnsafeSupplier) {

		try {
			appUserId = appUserIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Long appUserId;

	public Boolean getBroadcastMessage() {
		return broadcastMessage;
	}

	public void setBroadcastMessage(Boolean broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}

	public void setBroadcastMessage(
		UnsafeSupplier<Boolean, Exception> broadcastMessageUnsafeSupplier) {

		try {
			broadcastMessage = broadcastMessageUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean broadcastMessage;

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public void setCompanyId(
		UnsafeSupplier<Integer, Exception> companyIdUnsafeSupplier) {

		try {
			companyId = companyIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Integer companyId;

	public Boolean getEvents() {
		return events;
	}

	public void setEvents(Boolean events) {
		this.events = events;
	}

	public void setEvents(
		UnsafeSupplier<Boolean, Exception> eventsUnsafeSupplier) {

		try {
			events = eventsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean events;

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public void setGroupId(
		UnsafeSupplier<Integer, Exception> groupIdUnsafeSupplier) {

		try {
			groupId = groupIdUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Integer groupId;

	public Boolean getHighlight() {
		return highlight;
	}

	public void setHighlight(Boolean highlight) {
		this.highlight = highlight;
	}

	public void setHighlight(
		UnsafeSupplier<Boolean, Exception> highlightUnsafeSupplier) {

		try {
			highlight = highlightUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean highlight;

	public Boolean getNews() {
		return news;
	}

	public void setNews(Boolean news) {
		this.news = news;
	}

	public void setNews(UnsafeSupplier<Boolean, Exception> newsUnsafeSupplier) {
		try {
			news = newsUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected Boolean news;

	@Override
	public NotificationSettings clone() throws CloneNotSupportedException {
		return (NotificationSettings)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof NotificationSettings)) {
			return false;
		}

		NotificationSettings notificationSettings =
			(NotificationSettings)object;

		return Objects.equals(toString(), notificationSettings.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return NotificationSettingsSerDes.toJSON(this);
	}

}