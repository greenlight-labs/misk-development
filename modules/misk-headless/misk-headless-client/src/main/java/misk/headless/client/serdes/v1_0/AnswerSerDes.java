package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Answer;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class AnswerSerDes {

	public static Answer toDTO(String json) {
		AnswerJSONParser answerJSONParser = new AnswerJSONParser();

		return answerJSONParser.parseToDTO(json);
	}

	public static Answer[] toDTOs(String json) {
		AnswerJSONParser answerJSONParser = new AnswerJSONParser();

		return answerJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Answer answer) {
		if (answer == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (answer.getReason() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"reason\": ");

			sb.append("\"");

			sb.append(_escape(answer.getReason()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		AnswerJSONParser answerJSONParser = new AnswerJSONParser();

		return answerJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Answer answer) {
		if (answer == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (answer.getReason() == null) {
			map.put("reason", null);
		}
		else {
			map.put("reason", String.valueOf(answer.getReason()));
		}

		return map;
	}

	public static class AnswerJSONParser extends BaseJSONParser<Answer> {

		@Override
		protected Answer createDTO() {
			return new Answer();
		}

		@Override
		protected Answer[] createDTOArray(int size) {
			return new Answer[size];
		}

		@Override
		protected void setField(
			Answer answer, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "reason")) {
				if (jsonParserFieldValue != null) {
					answer.setReason((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}