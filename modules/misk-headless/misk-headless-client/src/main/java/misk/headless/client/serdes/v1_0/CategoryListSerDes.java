package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.CategoryList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class CategoryListSerDes {

	public static CategoryList toDTO(String json) {
		CategoryListJSONParser categoryListJSONParser =
			new CategoryListJSONParser();

		return categoryListJSONParser.parseToDTO(json);
	}

	public static CategoryList[] toDTOs(String json) {
		CategoryListJSONParser categoryListJSONParser =
			new CategoryListJSONParser();

		return categoryListJSONParser.parseToDTOs(json);
	}

	public static String toJSON(CategoryList categoryList) {
		if (categoryList == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (categoryList.getCatId() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catId\": ");

			sb.append("\"");

			sb.append(_escape(categoryList.getCatId()));

			sb.append("\"");
		}

		if (categoryList.getCatTitle() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"catTitle\": ");

			sb.append("\"");

			sb.append(_escape(categoryList.getCatTitle()));

			sb.append("\"");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		CategoryListJSONParser categoryListJSONParser =
			new CategoryListJSONParser();

		return categoryListJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(CategoryList categoryList) {
		if (categoryList == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (categoryList.getCatId() == null) {
			map.put("catId", null);
		}
		else {
			map.put("catId", String.valueOf(categoryList.getCatId()));
		}

		if (categoryList.getCatTitle() == null) {
			map.put("catTitle", null);
		}
		else {
			map.put("catTitle", String.valueOf(categoryList.getCatTitle()));
		}

		return map;
	}

	public static class CategoryListJSONParser
		extends BaseJSONParser<CategoryList> {

		@Override
		protected CategoryList createDTO() {
			return new CategoryList();
		}

		@Override
		protected CategoryList[] createDTOArray(int size) {
			return new CategoryList[size];
		}

		@Override
		protected void setField(
			CategoryList categoryList, String jsonParserFieldName,
			Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "catId")) {
				if (jsonParserFieldValue != null) {
					categoryList.setCatId((String)jsonParserFieldValue);
				}
			}
			else if (Objects.equals(jsonParserFieldName, "catTitle")) {
				if (jsonParserFieldValue != null) {
					categoryList.setCatTitle((String)jsonParserFieldValue);
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}