package misk.headless.client.dto.v1_0;

import java.io.Serializable;

import java.util.Objects;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.FaqList;
import misk.headless.client.function.UnsafeSupplier;
import misk.headless.client.serdes.v1_0.FaqSerDes;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Faq implements Cloneable, Serializable {

	public static Faq toDTO(String json) {
		return FaqSerDes.toDTO(json);
	}

	public FaqList[] getFaqList() {
		return faqList;
	}

	public void setFaqList(FaqList[] faqList) {
		this.faqList = faqList;
	}

	public void setFaqList(
		UnsafeSupplier<FaqList[], Exception> faqListUnsafeSupplier) {

		try {
			faqList = faqListUnsafeSupplier.get();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected FaqList[] faqList;

	@Override
	public Faq clone() throws CloneNotSupportedException {
		return (Faq)super.clone();
	}

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof Faq)) {
			return false;
		}

		Faq faq = (Faq)object;

		return Objects.equals(toString(), faq.toString());
	}

	@Override
	public int hashCode() {
		String string = toString();

		return string.hashCode();
	}

	public String toString() {
		return FaqSerDes.toJSON(this);
	}

}