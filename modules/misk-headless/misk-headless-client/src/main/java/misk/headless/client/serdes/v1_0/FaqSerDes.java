package misk.headless.client.serdes.v1_0;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import javax.annotation.Generated;

import misk.headless.client.dto.v1_0.Faq;
import misk.headless.client.dto.v1_0.FaqList;
import misk.headless.client.json.BaseJSONParser;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class FaqSerDes {

	public static Faq toDTO(String json) {
		FaqJSONParser faqJSONParser = new FaqJSONParser();

		return faqJSONParser.parseToDTO(json);
	}

	public static Faq[] toDTOs(String json) {
		FaqJSONParser faqJSONParser = new FaqJSONParser();

		return faqJSONParser.parseToDTOs(json);
	}

	public static String toJSON(Faq faq) {
		if (faq == null) {
			return "null";
		}

		StringBuilder sb = new StringBuilder();

		sb.append("{");

		if (faq.getFaqList() != null) {
			if (sb.length() > 1) {
				sb.append(", ");
			}

			sb.append("\"faqList\": ");

			sb.append("[");

			for (int i = 0; i < faq.getFaqList().length; i++) {
				sb.append(String.valueOf(faq.getFaqList()[i]));

				if ((i + 1) < faq.getFaqList().length) {
					sb.append(", ");
				}
			}

			sb.append("]");
		}

		sb.append("}");

		return sb.toString();
	}

	public static Map<String, Object> toMap(String json) {
		FaqJSONParser faqJSONParser = new FaqJSONParser();

		return faqJSONParser.parseToMap(json);
	}

	public static Map<String, String> toMap(Faq faq) {
		if (faq == null) {
			return null;
		}

		Map<String, String> map = new TreeMap<>();

		if (faq.getFaqList() == null) {
			map.put("faqList", null);
		}
		else {
			map.put("faqList", String.valueOf(faq.getFaqList()));
		}

		return map;
	}

	public static class FaqJSONParser extends BaseJSONParser<Faq> {

		@Override
		protected Faq createDTO() {
			return new Faq();
		}

		@Override
		protected Faq[] createDTOArray(int size) {
			return new Faq[size];
		}

		@Override
		protected void setField(
			Faq faq, String jsonParserFieldName, Object jsonParserFieldValue) {

			if (Objects.equals(jsonParserFieldName, "faqList")) {
				if (jsonParserFieldValue != null) {
					faq.setFaqList(
						Stream.of(
							toStrings((Object[])jsonParserFieldValue)
						).map(
							object -> FaqListSerDes.toDTO((String)object)
						).toArray(
							size -> new FaqList[size]
						));
				}
			}
		}

	}

	private static String _escape(Object object) {
		String string = String.valueOf(object);

		for (String[] strings : BaseJSONParser.JSON_ESCAPE_STRINGS) {
			string = string.replace(strings[0], strings[1]);
		}

		return string;
	}

	private static String _toJSON(Map<String, ?> map) {
		StringBuilder sb = new StringBuilder("{");

		@SuppressWarnings("unchecked")
		Set set = map.entrySet();

		@SuppressWarnings("unchecked")
		Iterator<Map.Entry<String, ?>> iterator = set.iterator();

		while (iterator.hasNext()) {
			Map.Entry<String, ?> entry = iterator.next();

			sb.append("\"");
			sb.append(entry.getKey());
			sb.append("\": ");

			Object value = entry.getValue();

			Class<?> valueClass = value.getClass();

			if (value instanceof Map) {
				sb.append(_toJSON((Map)value));
			}
			else if (valueClass.isArray()) {
				Object[] values = (Object[])value;

				sb.append("[");

				for (int i = 0; i < values.length; i++) {
					sb.append("\"");
					sb.append(_escape(values[i]));
					sb.append("\"");

					if ((i + 1) < values.length) {
						sb.append(", ");
					}
				}

				sb.append("]");
			}
			else if (value instanceof String) {
				sb.append("\"");
				sb.append(_escape(entry.getValue()));
				sb.append("\"");
			}
			else {
				sb.append(String.valueOf(entry.getValue()));
			}

			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}

		sb.append("}");

		return sb.toString();
	}

}