<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Email Template</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/style.css"/>
</head>
<body>
<table align="center" width="100%">
    <tr>
        <td align="center">
            <table cellpadding="0" cellspacing="0" width="550" align="center" style="border: 1px solid rgba(117,119,123,0.3)">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" height="80" width="100%" align="center" style="border-bottom: 1px solid rgba(117,119,123,0.3)">
                            <tr>
                                <td align="center" style="text-align:center;" width="100%" height="100px" bgcolor="#ffffff">
                                    <#--<img width="200px" align="middle" src="${portletContextPath}/img/logo.png" alt=""/>-->
                                </td>
                            </tr>
                        </table>

                        <table cellpadding="0" cellspacing="0" width="100%" align="center">
                            <tr height="50"></tr>
                            <tr>
                                <td align="center" style="font-size:32px;font-weight:900;text-align:center;font-family:Montserrat,sans-serif;color: #DA1884" width="100%" bgcolor="#ffffff">
                                    Account Deleted Notification!
                                </td>
                            </tr>
                        </table>

                        <table cellpadding="0" cellspacing="0" width="100%" align="center" style="padding-left: 30px">
                            <tr height="52"></tr>
                            <tr>
                                <td align="left" style="font-size:20px;font-weight:700;text-align:left;font-family:Arial, Helvetica, sans-serif;color:#75787B"  bgcolor="#ffffff" height="30">
                                    Hi,
                                </td>
                            </tr>
                            <tr>
                                <td height="50" style="font-size:20px;font-weight:700;text-align:left;font-family:Arial, Helvetica, sans-serif;color:#7578;">
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size:19px;text-align:left;font-family:Arial, Helvetica, sans-serif;color:#75787B" width="100%" height="30">
                                    This user has been deleted from the system.
                                    <br>
                                    <strong>Full Name:</strong> ${name} <br>
                                    <strong>Email Address:</strong> ${email} <br>
                                    <strong>Phone Number:</strong> ${phone} <br>
                                    <strong>Created Date:</strong> ${createdDate} <br>
                                    <strong>Delete Reason:</strong> ${deleteReason} <br>
                                </td>
                            </tr>
                            <tr>
                                <td height="50" style="font-size:20px;font-weight:700;text-align:left;font-family:Arial, Helvetica, sans-serif;color:#7578;">
                                </td>
                            </tr>
                        </table>

                        <table cellpadding="0" cellspacing="0" height="80" width="100%" align="center" style="border-top: 1px solid rgba(117,119,123,0.3)">
                            <tr>
                                <td align="center" style="font-size:12px;font-weight:400;text-align:center;font-family:Arial, Helvetica, sans-serif;color:#75787B;"  bgcolor="#ffffff" height="40">
                                    © ${the_year} MiSK City. All rights Reserved
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
