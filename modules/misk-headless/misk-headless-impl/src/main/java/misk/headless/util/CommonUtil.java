package misk.headless.util;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.language.UTF8Control;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import misk.headless.constants.ApiKeys;
import org.apache.commons.lang3.LocaleUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriInfo;
import java.text.Format;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component(immediate = true, service = CommonUtil.class)
public class CommonUtil {
    private static final Log _log = LogFactoryUtil.getLog(CommonUtil.class);

    public static String generateResetToken() {
        return UUID.randomUUID().toString();
    }

    // generate 4 digit passcode
    public static String generatePasscode() {
        Random random = new Random();
        return String.format("%04d", random.nextInt(10000));
    }

    public static Date convertStringToDate(String field){
        // "January 2, 2010"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d, yyyy", Locale.ENGLISH);
        LocalDate ld = LocalDate.parse(field, formatter);
        return Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    /* https://stackoverflow.com/questions/4216745/java-string-to-date-conversion */
    public static Date convertStringToDateTime(String field){
        // "2022-03-29 12:00"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        LocalDateTime ldt = LocalDateTime.parse(field, formatter);
        return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static String convertDateTimeToStringDate(Date field){
        Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        return dateTimeFormat.format(field);
    }

    public static String convertDateTimeToStringTime(Date field){
        Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("HH:mm", Locale.ENGLISH);
        return dateTimeFormat.format(field);
    }

    public static String convertDateTimeToStringDateTime(Date field){
        Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return dateTimeFormat.format(field);
    }

    public static String getMediaUrl(String mediaUrl, UriInfo contextUriInfo){
        // check mediaUrl is not empty and return it with base url
        if(StringUtils.isNotEmpty(mediaUrl)){
            return ApiKeys.getBaseURL(contextUriInfo) + mediaUrl;
        }
        return StringPool.BLANK;
    }

    /**
     * get language translation from language key
    * */
    public static String getLanguageTranslation(HttpServletRequest httpServletRequest, String key){
        // get X-Language-Id from header and set it to locale
        String languageId = httpServletRequest.getHeader(ApiKeys.X_LANGUAGE_ID);
        // get default language if languageId is empty
        if(StringUtils.isEmpty(languageId)){
            languageId = Locale.getDefault().getLanguage();
        }
        Locale locale = LocaleUtils.toLocale(languageId);
        // get resource bundle
        ResourceBundle resourceBundle = ResourceBundle.getBundle("content.Language", locale, UTF8Control.INSTANCE);
        return LanguageUtil.get(resourceBundle, key);
    }

    /**
     * get language id from header
    * */
    public static String getHeaderLanguage(HttpServletRequest httpServletRequest){
        // get X-Language-Id from header and set it to locale
        String languageId = httpServletRequest.getHeader(ApiKeys.X_LANGUAGE_ID);
        // get default language if languageId is empty
        if(StringUtils.isEmpty(languageId)){
            languageId = Locale.getDefault().getLanguage();
        }
        return languageId;
    }

}
