package misk.headless.util;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import favorite.model.Favorite;
import favorite.service.FavoriteLocalService;
import favorite.service.FavoriteLocalServiceUtil;
import misk.headless.dto.v1_0.EventList;
import misk.headless.dto.v1_0.Highlight;
import misk.headless.dto.v1_0.Story;
import org.jsoup.helper.StringUtil;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceArray;

public class FavouriteUtil {

    private static final long EVENT_TYPE_ID = 1;
    private static final long HIGHLIGHT_TYPE_ID = 2;
    private static final long ATTRACTION_TYPE_ID = 3;
    private static final long DISCOVER_TYPE_ID = 4;
    private static final long STORY_TYPE_ID = 5;

    public static EventList[] updateEventsFavouriteStatus(EventList[] items, HttpServletRequest request){

        String appUserIdParam = request.getParameter("appUserId");
        if(appUserIdParam == null){
            return items;
        }

        long appUserId = Long.parseLong(appUserIdParam);

        Long[] favIds = getFavouriteIds(EVENT_TYPE_ID, appUserId);
        for (EventList item : items) {
            boolean isFavourite = ArrayUtil.contains(favIds, item.getEventId());
            if(isFavourite){
                item.setIsFavourite(true);
            }
        }
        return items;
    }

    public static List<EventList> updateEventsFavouriteStatus(List<EventList> items, HttpServletRequest request){

        String appUserIdParam = request.getParameter("appUserId");
        if(appUserIdParam == null){
            return items;
        }

        long appUserId = Long.parseLong(appUserIdParam);

        Long[] favIds = getFavouriteIds(EVENT_TYPE_ID, appUserId);
        for (EventList item : items) {
            boolean isFavourite = ArrayUtil.contains(favIds, item.getEventId());
            if(isFavourite){
                item.setIsFavourite(true);
            }
        }
        return items;
    }

    public static EventList updateEventFavouriteStatus(EventList item, HttpServletRequest request){

        String appUserIdParam = request.getParameter("appUserId");
        if(appUserIdParam == null){
            return item;
        }

        long appUserId = Long.parseLong(appUserIdParam);

        Long[] favIds = getFavouriteIds(EVENT_TYPE_ID, appUserId);
        boolean isFavourite = ArrayUtil.contains(favIds, item.getEventId());
        if(isFavourite){
            item.setIsFavourite(true);
        }
        return item;
    }

    public static Highlight[] updateHighlightsFavouriteStatus(Highlight[] items, HttpServletRequest request){

        String appUserIdParam = request.getParameter("appUserId");
        if(appUserIdParam == null){
            return items;
        }

        long appUserId = Long.parseLong(appUserIdParam);

        Long[] favIds = getFavouriteIds(HIGHLIGHT_TYPE_ID, appUserId);
        for (Highlight item : items) {
            boolean isFavourite = ArrayUtil.contains(favIds, item.getHighlightId());
            if(isFavourite){
                item.setIsFavourite(true);
            }
        }
        return items;
    }

    public static Story[] updateStoriesFavouriteStatus(Story[] items, HttpServletRequest request){

        String appUserIdParam = request.getParameter("appUserId");
        if(appUserIdParam == null){
            return items;
        }

        long appUserId = Long.parseLong(appUserIdParam);

        Long[] favIds = getFavouriteIds(HIGHLIGHT_TYPE_ID, appUserId);
        for (Story item : items) {
            boolean isFavourite = ArrayUtil.contains(favIds, item.getStoryId());
            if(isFavourite){
                item.setIsFavourite(true);
            }
        }
        return items;
    }

    public static List<Story> updateStoriesFavouriteStatus(List<Story> items, HttpServletRequest request){

        String appUserIdParam = request.getParameter("appUserId");
        if(appUserIdParam == null){
            return items;
        }

        long appUserId = Long.parseLong(appUserIdParam);

        Long[] favIds = getFavouriteIds(HIGHLIGHT_TYPE_ID, appUserId);
        for (Story item : items) {
            boolean isFavourite = ArrayUtil.contains(favIds, item.getStoryId());
            if(isFavourite){
                item.setIsFavourite(true);
            }
        }
        return items;
    }

    private static long getAppUserId(HttpServletRequest request) {
        HttpSession session = request.getSession();
        boolean isAuthenticated = (boolean) session.getAttribute("isAuthenticated");
        if(! isAuthenticated){
            return 0;
        }

        return (long) session.getAttribute("appUserId");

    }

    private static Long[] getFavouriteIds(long typeId, long userId){
        List<Long> favIds = new ArrayList<>();

        List<Favorite> items = getFavourites(typeId, userId);

        if(items != null){
            for (Favorite item: items) {
                favIds.add(item.getItemId());
            }
        }

        return favIds.toArray(new Long[0]);
    }

    private static List<Favorite> getFavourites(long typeId, long userId) {
        try{
            return favoriteLocalServiceUtil.getFavorites(typeId, userId);

        }catch(PortalException e){
            return null;
        }
    }

    Log _log = LogFactoryUtil.getLog(FavouriteUtil.class);

    @Reference
    protected static FavoriteLocalServiceUtil favoriteLocalServiceUtil;
}
