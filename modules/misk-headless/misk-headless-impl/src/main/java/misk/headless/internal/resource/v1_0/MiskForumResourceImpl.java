package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import java.text.Format;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import events.model.Category;
import events.model.EventsGallery;
import events.service.CategoryLocalServiceUtil;
import events.service.EventService;
import events.service.EventsGalleryLocalServiceUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.AmenitiesList;
import misk.headless.dto.v1_0.BookSpaceList;
import misk.headless.dto.v1_0.EventList;
import misk.headless.dto.v1_0.EventsSection;
import misk.headless.dto.v1_0.MiskForum;
import misk.headless.resource.v1_0.MiskForumResource;
import misk.headless.util.DocumentMediaUtil;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/misk-forum.properties", scope = ServiceScope.PROTOTYPE, service = MiskForumResource.class)
public class MiskForumResourceImpl extends BaseMiskForumResourceImpl {

	Log _log = LogFactoryUtil.getLog(ContactResourceImpl.class);
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	protected String baseURL = "https://webserver-miskcity-dev.lfr.cloud";

	@Override
	public MiskForum getMiskForum(String languageId) throws Exception {
		try {
			System.out.println(" --- " + languageId);
			return this.getMiskForumContent(languageId);
		} catch (Exception ex) {
			_log.error("Error retrieving getMiskForum page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new MiskForum();
	}

	public MiskForum getMiskForumContent(String languageId) throws DocumentException, PortalException {
		MiskForum MiskForum = new MiskForum();

		long groupId = appGroupId;
		String articleId = "MISK_FORUM";

		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId,
				WorkflowConstants.STATUS_APPROVED);
		String articleContent = article.getContentByLocale(languageId);
		Document document = SAXReaderUtil.read(articleContent);
		System.out.println(" --- " + articleId + " ---- " + groupId);
		Node miskForumBanner = document
				.selectSingleNode("/root/dynamic-element[@name='MiskForumBanner']/dynamic-content");
		DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
		MiskForum.setMiskForumBanner(this.baseURL + _documentMediaUtil.getImageURL(miskForumBanner.getText()));

		Node miskForumLabel = document
				.selectSingleNode("/root/dynamic-element[@name='MiskForumLabel']/dynamic-content");
		MiskForum.setMiskForumLabel(miskForumLabel.getText());

		Node workingHoursLabel = document
				.selectSingleNode("/root/dynamic-element[@name='WorkingHoursLabel']/dynamic-content");
		MiskForum.setWorkingHoursLabel(workingHoursLabel.getText());

		Node workingHours = document.selectSingleNode("/root/dynamic-element[@name='WorkingHours']/dynamic-content");
		MiskForum.setWorkingHours(workingHours.getText());

		Node workingDay = document.selectSingleNode("/root/dynamic-element[@name='WorkingDay']/dynamic-content");
		MiskForum.setWorkingDay(workingDay.getText());

		Node description = document.selectSingleNode("/root/dynamic-element[@name='Description']/dynamic-content");
		MiskForum.setDescription(description.getText());

		Node locationLabel = document.selectSingleNode("/root/dynamic-element[@name='LocationLabel']/dynamic-content");
		MiskForum.setLocationLabel(locationLabel.getText());

		Node locationLatitude = document
				.selectSingleNode("/root/dynamic-element[@name='LocationLatitude']/dynamic-content");
		MiskForum.setLocationLatitude(locationLatitude.getText());

		Node locationLongitude = document
				.selectSingleNode("/root/dynamic-element[@name='LocationLongitude']/dynamic-content");
		MiskForum.setLocationLongitude(locationLongitude.getText());

		Node contactLabel = document.selectSingleNode("/root/dynamic-element[@name='ContactLabel']/dynamic-content");
		MiskForum.setContactLabel(contactLabel.getText());

		Node contactEmailAddress = document
				.selectSingleNode("/root/dynamic-element[@name='ContactEmailAddress']/dynamic-content");
		MiskForum.setContactEmailAddress(contactEmailAddress.getText());

		Node contactTelephone = document
				.selectSingleNode("/root/dynamic-element[@name='ContactTelephone']/dynamic-content");
		MiskForum.setContactTelephone(contactTelephone.getText());

		Node eventLabel = document.selectSingleNode("/root/dynamic-element[@name='EventLabel']/dynamic-content");
		MiskForum.setEventLabel(eventLabel.getText());

		Node eventTypeNode = document.selectSingleNode("/root/dynamic-element[@name='EventName']/dynamic-content");
		String eventType = eventTypeNode.getText();
		Node galleryLabel = document.selectSingleNode("/root/dynamic-element[@name='GalleryLabel']/dynamic-content");
		MiskForum.setGalleryLabel(galleryLabel.getText());

		List<Node> galleryLlist = document
				.selectNodes("/root/dynamic-element[@name='GalleryLabel']/dynamic-element[@name='Imagehcp2']");

		String[] galleryImageList = new String[galleryLlist.size()];
		for (int i = 0; i < galleryLlist.size(); i++) {
			Node galleryNode = galleryLlist.get(i);
			String galleryURL = galleryNode.selectNodes(
					"/root/dynamic-element[@name='GalleryLabel']/dynamic-element[@name='Imagehcp2']/dynamic-content")
					.get(i).getText();
			galleryImageList[i] = this.baseURL + _documentMediaUtil.getImageURL(galleryURL);
		}
		MiskForum.setGalleryImagesList(galleryImageList);

		Node amenitiesLabel = document
				.selectSingleNode("/root/dynamic-element[@name='AmenitiesLabel']/dynamic-content");
		MiskForum.setAmenitiesLabel(amenitiesLabel.getText());

		List<Node> amenitiesList = document
				.selectNodes("/root/dynamic-element[@name='AmenitiesLabel']/dynamic-element[@name='AmenitiesList']");

		AmenitiesList[] amenitiesImagesList = new AmenitiesList[amenitiesList.size()];
		for (int i = 0; i < amenitiesList.size(); i++) {
			AmenitiesList amenitiesListObj = new AmenitiesList();
			Node amenitiesNode = amenitiesList.get(i);
			String amenitiesURL = amenitiesNode.selectNodes(
					"/root/dynamic-element[@name='AmenitiesLabel']/dynamic-element[@name='AmenitiesList']/dynamic-content")
					.get(i).getText();
			amenitiesListObj.setAmenitiesImages(this.baseURL + _documentMediaUtil.getImageURL(amenitiesURL));
			amenitiesListObj.setAmenitiesImagesLabel(_documentMediaUtil.getImageDescription(amenitiesURL));
			amenitiesImagesList[i] = amenitiesListObj;
		}
		MiskForum.setAmenitiesList(amenitiesImagesList);

		Node bookSpaceLabel = document
				.selectSingleNode("/root/dynamic-element[@name='BookSpaceLabel']/dynamic-content");
		MiskForum.setBookSpaceLabel(bookSpaceLabel.getText());

		List<Node> bookSpaceList = document.selectNodes(
				"/root/dynamic-element[@name='BookSpaceLabel']/dynamic-element[@name='BookSpaceSeparator']");

		BookSpaceList[] bookSList = new BookSpaceList[bookSpaceList.size()];
		for (int i = 0; i < bookSpaceList.size(); i++) {

			BookSpaceList bookSpaceListObj = new BookSpaceList();
			Node amenitiesNode = amenitiesList.get(i);

			String bookSpaceImages = amenitiesNode.selectNodes(
					"/root/dynamic-element[@name='BookSpaceLabel']/dynamic-element[@name='BookSpaceSeparator']/dynamic-element[@name='BookSpaceImages']/dynamic-content")
					.get(i).getText();

			bookSpaceListObj.setBookSpaceImages(this.baseURL + _documentMediaUtil.getImageURL(bookSpaceImages));

			String bookSpaceIcon = amenitiesNode.selectNodes(
					"/root/dynamic-element[@name='BookSpaceLabel']/dynamic-element[@name='BookSpaceSeparator']/dynamic-element[@name='BookSpaceIcon']/dynamic-content")
					.get(i).getText();

			bookSpaceListObj.setBookSpaceIcon(this.baseURL + _documentMediaUtil.getImageURL(bookSpaceIcon));

			String bookSpaceTitle = amenitiesNode.selectNodes(
					"/root/dynamic-element[@name='BookSpaceLabel']/dynamic-element[@name='BookSpaceSeparator']/dynamic-element[@name='BookSpaceTitle']/dynamic-content")
					.get(i).getText();
			bookSpaceListObj.setBookSpaceTitle(bookSpaceTitle);

			String capacity = amenitiesNode.selectNodes(
					"/root/dynamic-element[@name='BookSpaceLabel']/dynamic-element[@name='BookSpaceSeparator']/dynamic-element[@name='Capacity']/dynamic-content")
					.get(i).getText();
			bookSpaceListObj.setCapacity(capacity);

			String area = amenitiesNode.selectNodes(
					"/root/dynamic-element[@name='BookSpaceLabel']/dynamic-element[@name='BookSpaceSeparator']/dynamic-element[@name='Area']/dynamic-content")
					.get(i).getText();
			bookSpaceListObj.setArea(area);

			String duration = amenitiesNode.selectNodes(
					"/root/dynamic-element[@name='BookSpaceLabel']/dynamic-element[@name='BookSpaceSeparator']/dynamic-element[@name='Duration']/dynamic-content")
					.get(i).getText();
			bookSpaceListObj.setDuration(duration);

			bookSList[i] = bookSpaceListObj;
		}
		MiskForum.setBookSpaceList(bookSList);
		try {
			MiskForum.setEventsSection(getEventSection(languageId, eventType));
		} catch (Exception e) {
			_log.error("error while getting event list");
		}
		return MiskForum;

	}

	private EventList[] getEvents(String languageId, String eventType) {
		int typeEvent = 1;// default
		if (Validator.isNotNull(eventType) && Validator.isNumber(eventType)) {
			typeEvent = Integer.parseInt(eventType);
		}
		List<events.model.Event> records = _eventService.getEvents(this.webGroupId, 0, 9);

		EventList[] results = new EventList[records.size()];
		int i = 0;

		for (events.model.Event curRecord : records) {
			EventList result = new EventList();

			List<EventsGallery> eventg = EventsGalleryLocalServiceUtil.findAllInDiscover(curRecord.getEventId());

			String imagearray[] = new String[eventg.size()];
			for (int j = 0; j < eventg.size(); j++) {
				imagearray[j] = ApiKeys.getBaseURL(this.contextUriInfo) + eventg.get(j).getImage(languageId);
			}

			result.setEventId(curRecord.getEventId());
			result.setTitle(curRecord.getTitle(languageId));
			String paytype = "Free";
			if (curRecord.getPaytype() == 2) {
				paytype = "Paid";
			}
			result.setPayType(paytype);
			result.setEventDescription(curRecord.getEventdescription(languageId));
			result.setTime(curRecord.getTiming(languageId));
			result.setVenue(curRecord.getVenue(languageId));
			Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("dd MMMM");

			result.setDisplaydate(String.valueOf(curRecord.getDisplayDate()));
			result.setEventMonth(dateTimeFormat.format(curRecord.getDisplayDate()));
			result.setIsFavourite(Boolean.TRUE);
			result.setEventFeaturedImage(
					ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getEventfeaturedImage(languageId));
			Category cat = CategoryLocalServiceUtil.fetchCategory(curRecord.getCategoryId());
			result.setCatColor(cat.getCatColor());
			result.setEventcategory(cat.getName(languageId));
			result.setEventType(String.valueOf(curRecord.getType()));
			result.setSliderimage(imagearray);
			results[i] = result;
			i++;
		}
		return results;
	}

	private EventsSection getEventSection(String languageId, String eventType) throws Exception {
		EventsSection eventsection = new EventsSection();
		eventsection.setItems(this.getEvents(languageId, eventType));

		return eventsection;
	}

	@Reference
	private EventService _eventService;
}