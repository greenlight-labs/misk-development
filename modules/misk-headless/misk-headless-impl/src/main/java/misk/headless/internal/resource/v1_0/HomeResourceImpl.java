package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import misk.headless.components.EventComponent;
import misk.headless.components.HighlightComponent;
import misk.headless.components.StoryComponent;
import misk.headless.constants.ApiKeys;
import misk.headless.constants.EventKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.HomeResource;
import misk.headless.util.DocumentMediaUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.HashMap;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/home.properties",
	scope = ServiceScope.PROTOTYPE, service = HomeResource.class
)
public class HomeResourceImpl extends BaseHomeResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HomeResourceImpl.class);

	@Override
	public Home getHomePage(String languageId)
			throws Exception {
		try {
			Home home = new Home();
			home.setFlipCards(this.getFlipCards(languageId));
			home.setHighlightsSection(this.getHighlightsSection(languageId));
			home.setStoriesSection(this.getStoriesSection(languageId));
			home.setEventsSection(this.getEventSection(languageId));
			return home;
		} catch (Exception ex) {
			_log.error("Error retrieving home page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	public FlipCard[] getFlipCards(String languageId) throws Exception{
		FlipCard[] results = new FlipCard[3];

		long groupId = this.appGroupId;
		String articleId = "APP_HOME_TOP_FLIP_SCREENS";

		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0 );
		String articleContent = article.getContentByLocale(languageId);

		Document document = SAXReaderUtil.read(articleContent);
		List<Node> nodes = document.selectNodes("/root/dynamic-element[@name='Heading']");
		int i = 0;
		for (Node node: nodes) {
			Node headingNode = node.selectSingleNode("dynamic-content");
			Node titleField1Node = node.selectSingleNode("dynamic-element[@name='TitleField1']/dynamic-content");
			Node titleField2Node = node.selectSingleNode("dynamic-element[@name='TitleField2']/dynamic-content");
			Node buttonLabelNode = node.selectSingleNode("dynamic-element[@name='ButtonLabel']/dynamic-content");
			Node imageNode = node.selectSingleNode("dynamic-element[@name='Image']/dynamic-content");

			FlipCard result = new FlipCard();
			result.setHeading(headingNode.getText());
			result.setTitleField1(titleField1Node.getText());
			result.setTitleField2(titleField2Node.getText());
			result.setButtonLabel(buttonLabelNode.getText());

			DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
			result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(imageNode.getText()));

			results[i] = result;
			i++;
		}

		return results;
	}

	private HighlightsSection getHighlightsSection(String languageId) throws Exception{
		HighlightsSection section = new HighlightsSection();
		String sectionTitle = this.getSectionTitle(languageId, "highlightsSectionTitle");
		section.setSectionTitle(sectionTitle);
		section.setItems(HighlightComponent.getHighlights(languageId, HighlightComponent.TAG_HIGHLIGHT, this.contextHttpServletRequest, this.contextUriInfo));

		return section;
	}

	private StoriesSection getStoriesSection(String languageId) throws Exception{
		StoriesSection storiesSection = new StoriesSection();
		String sectionTitle = this.getSectionTitle(languageId, "storiesSectionTitle");
		storiesSection.setSectionTitle(sectionTitle);
		storiesSection.setItems(StoryComponent.getStories(languageId, this.contextHttpServletRequest, this.contextUriInfo));

		return storiesSection;
	}

    private EventsSection getEventSection(String languageId) throws Exception{
		EventsSection eventsection = new EventsSection();
		String sectionTitle = this.getSectionTitle(languageId, "eventsSectionTitle");
		eventsection.setSectionTitle(sectionTitle);
		eventsection.setItems(EventComponent.getEventsByPageSlug(languageId, EventKeys.PAGE_HOME, this.contextHttpServletRequest, this.contextUriInfo));

		return eventsection;
	}

	public HashMap<String, String> getHomeSectionsTitle(String languageId) throws Exception{
		long groupId = this.appGroupId;
		String articleId = "APP_HOME_SECTIONS_TITLE";

		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0 );
		String articleContent = article.getContentByLocale(languageId);
		Document document = SAXReaderUtil.read(articleContent);
		Node root = document.selectSingleNode("/root");

		Node highlightsSectionTitleNode = root.selectSingleNode("dynamic-element[@name='HighlightsSectionTitle']/dynamic-content");
		Node storiesSectionTitleNode = root.selectSingleNode("dynamic-element[@name='StoriesSectionTitle']/dynamic-content");
		Node eventsSectionTitleNode = root.selectSingleNode("dynamic-element[@name='EventsSectionTitle']/dynamic-content");
		Node communityWallSectionTitleNode = root.selectSingleNode("dynamic-element[@name='CommunityWallSectionTitle']/dynamic-content");

		HashMap<String, String> sectionTitles = new HashMap<String, String>();
		sectionTitles.put("highlightsSectionTitle", highlightsSectionTitleNode.getText());
		sectionTitles.put("storiesSectionTitle", storiesSectionTitleNode.getText());
		sectionTitles.put("eventsSectionTitle", eventsSectionTitleNode.getText());
		sectionTitles.put("communityWallSectionTitle", communityWallSectionTitleNode.getText());

		return sectionTitles;
	}

	private String getSectionTitle(String languageId, String sectionTitleKey) throws Exception {
		HashMap<String, String> sectionTitles = this.getHomeSectionsTitle(languageId);
		return sectionTitles.get(sectionTitleKey);
	}

}