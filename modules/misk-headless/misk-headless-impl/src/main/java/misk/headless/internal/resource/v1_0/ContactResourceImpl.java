package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.Contact;
import misk.headless.dto.v1_0.FaqList;
import misk.headless.dto.v1_0.SocialMediaList;
import misk.headless.resource.v1_0.ContactResource;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/contact.properties", scope = ServiceScope.PROTOTYPE, service = ContactResource.class)
public class ContactResourceImpl extends BaseContactResourceImpl {

	Log _log = LogFactoryUtil.getLog(ContactResourceImpl.class);
	long groupId;
	long groupIdMainWebsite;
	protected String baseURL = "https://webserver-miskcity-dev.lfr.cloud";

	public ContactResourceImpl() {
		ApiKeys apiKeys = new ApiKeys();
		this.groupIdMainWebsite = apiKeys.webGroupId;
		this.groupId = apiKeys.appGroupId;
	}

	@Override
	public Contact getContact(String languageId) throws Exception {
		try {

			return this.getContactContent(languageId);
		} catch (Exception ex) {
			_log.error("Error retrieving ContactResourceImpl page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new Contact();
	}

	public Contact getContactContent(String languageId) throws DocumentException, PortalException {
		Contact contactInfo = new Contact();

		long groupId = this.groupId;
		String articleId = "CONTACT_INFO";
		System.out.println(" articleId " + articleId + " groupId " + groupId);
		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId,
				WorkflowConstants.STATUS_APPROVED);
		String articleContent = article.getContentByLocale(languageId);

		Document document = SAXReaderUtil.read(articleContent);

		Node contactLabel = document.selectSingleNode("/root/dynamic-element[@name='ContactLabel']/dynamic-content");
		contactInfo.setContactLabel(contactLabel.getText());

		Node description = document.selectSingleNode("/root/dynamic-element[@name='Description']/dynamic-content");
		contactInfo.setDescription(description.getText());

		Node locationLabel = document.selectSingleNode("/root/dynamic-element[@name='LocationLabel']/dynamic-content");
		contactInfo.setLocationLabel(locationLabel.getText());

		Node locationLatitude = document
				.selectSingleNode("/root/dynamic-element[@name='LocationLatitude']/dynamic-content");
		contactInfo.setLocationLatitude(locationLatitude.getText());

		Node locationLongitude = document
				.selectSingleNode("/root/dynamic-element[@name='LocationLongitude']/dynamic-content");
		contactInfo.setLocationLongitude(locationLongitude.getText());

		Node address = document.selectSingleNode("/root/dynamic-element[@name='Address']/dynamic-content");
		contactInfo.setAddress(address.getText());

		Node callUsLabel = document.selectSingleNode("/root/dynamic-element[@name='CallUsLabel']/dynamic-content");
		contactInfo.setCallUsLabel(callUsLabel.getText());

		Node callUsNumber = document.selectSingleNode("/root/dynamic-element[@name='CallUsNumber']/dynamic-content");
		contactInfo.setCallUsNumber(callUsNumber.getText());

		Node whatsAppLabel = document.selectSingleNode("/root/dynamic-element[@name='WhatsAppLabel']/dynamic-content");
		contactInfo.setWhatsAppLabel(whatsAppLabel.getText());

		Node whatsAppNumber = document
				.selectSingleNode("/root/dynamic-element[@name='WhatsAppNumber']/dynamic-content");
		contactInfo.setWhatsAppNumber(whatsAppNumber.getText());

		Node websiteLabel = document.selectSingleNode("/root/dynamic-element[@name='WebsiteLabel']/dynamic-content");
		contactInfo.setWebsiteLabel(websiteLabel.getText());

		Node websiteAddress = document
				.selectSingleNode("/root/dynamic-element[@name='WebsiteAddress']/dynamic-content");
		contactInfo.setWebsiteAddress(websiteAddress.getText());

		Node emailLabel = document.selectSingleNode("/root/dynamic-element[@name='EmailLabel']/dynamic-content");
		contactInfo.setEmailLabel(emailLabel.getText());

		Node emailAddress = document.selectSingleNode("/root/dynamic-element[@name='EmailAddress']/dynamic-content");
		contactInfo.setEmailAddress(emailAddress.getText());

		Node workingHoursLabel = document
				.selectSingleNode("/root/dynamic-element[@name='WorkingHoursLabel']/dynamic-content");
		contactInfo.setWorkingHoursLabel(workingHoursLabel.getText());

		Node workingHours = document.selectSingleNode("/root/dynamic-element[@name='WorkingHours']/dynamic-content");
		contactInfo.setWorkingHours(workingHours.getText());

		Node socialMediaLabel = document
				.selectSingleNode("/root/dynamic-element[@name='SocialMediaLabel']/dynamic-content");
		contactInfo.setSocialMediaLabel(socialMediaLabel.getText());

		List<Node> socialMediaList = document.selectNodes(
				"/root/dynamic-element[@name='SocialMediaLabel']/dynamic-element[@name='SocialMediaList']");
		SocialMediaList[] socialList = new SocialMediaList[socialMediaList.size()];
		for (int i = 0; i < socialMediaList.size(); i++) {
			Node languageNode1 = socialMediaList.get(i);
			String socialMediaName = languageNode1.selectNodes(
					"/root/dynamic-element[@name='SocialMediaLabel']/dynamic-element[@name='SocialMediaList']/dynamic-element[@name='SocialMediaName']/dynamic-content")
					.get(i).getText();
			String socialMediaURL = languageNode1.selectNodes(
					"/root/dynamic-element[@name='SocialMediaLabel']/dynamic-element[@name='SocialMediaList']/dynamic-element[@name='SocialMediaURL']/dynamic-content")
					.get(i).getText();
			SocialMediaList socalMediaListObj = new SocialMediaList();
			socalMediaListObj.setSocialMediaName(socialMediaName);
			socalMediaListObj.setSocialMediaURL(socialMediaURL);
			socialList[i] = socalMediaListObj;
		}
		contactInfo.setSocialMediaList(socialList);
		return contactInfo;

	}
}