package misk.headless.constants;

/**
 * @author tz
 */
public class HomeKeys {

	public static final String HOMEEVENTSADMINWEB =
		"misk_headless_web_EventsAdminWebPortlet";

	public static final String HOMECATEGORIESADMINWEB =
			"misk_headless_events_admin_web_CategoriesAdminWebPortlet";

}