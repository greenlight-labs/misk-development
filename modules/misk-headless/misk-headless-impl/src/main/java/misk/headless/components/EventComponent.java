package misk.headless.components;

import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.TimeZoneUtil;
import com.liferay.portal.kernel.util.Validator;
import events.model.*;
import events.model.Category;
import events.service.*;
import misk.app.users.service.AppUserLocalServiceUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.constants.EventKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.dto.v1_0.Schedule;
import misk.headless.util.CommonUtil;
import misk.headless.util.FavouriteUtil;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriInfo;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

public class EventComponent {

    public static long webGroupId = ApiKeys.webGroupId;
    public static long appGroupId = ApiKeys.appGroupId;

    public static EventList[] getEvents(String languageId, HttpServletRequest contextHttpServletRequest, UriInfo contextUriInfo) throws Exception {
        List<events.model.Event> records = EventLocalServiceUtil.getEvents(webGroupId);

        //limit to 9 records
        if(records.size() > 9) {
            records = records.subList(0, 9);
        }

        EventList[] results = new EventList[records.size()];
        int i = 0;

        for (events.model.Event curRecord : records) {
            EventList result = getDTO(languageId, curRecord, contextUriInfo);
            // get event attendees list
            result.setAttendesList(getAttendeesList(curRecord.getEventId(), languageId, contextUriInfo));

            results[i] = result;
            i++;
        }
        // update events favourite status
        FavouriteUtil.updateEventsFavouriteStatus(results, contextHttpServletRequest);

        return results;
    }
    public static EventList[] getEventsByPageSlug(String languageId, String pageSlug, HttpServletRequest contextHttpServletRequest, UriInfo contextUriInfo) throws Exception {

        PageTag pageTag = PageTagLocalServiceUtil.fetchByPageSlug(pageSlug);;
        // if pageTag not found return empty array
        if(Validator.isNull(pageTag)) {
            return new EventList[0];
        }
        // if tagId is zero then return all events
        if (pageTag.getTagId() == 0) {
            return getEvents(languageId, contextHttpServletRequest, contextUriInfo);
        }

        List<events.model.Event> records = EventLocalServiceUtil.findByTagId(pageTag.getTagId());

        //limit to 9 records
        if(records.size() > 9) {
            records = records.subList(0, 9);
        }

        EventList[] results = new EventList[records.size()];
        int i = 0;

        for (events.model.Event curRecord : records) {
            EventList result = getDTO(languageId, curRecord, contextUriInfo);
            // get event attendees list
            result.setAttendesList(getAttendeesList(curRecord.getEventId(), languageId, contextUriInfo));

            results[i] = result;
            i++;
        }
        // update events favourite status
        FavouriteUtil.updateEventsFavouriteStatus(results, contextHttpServletRequest);

        return results;
    }

    public static EventList[] getRelatedEvents(String languageId, events.model.Event event, HttpServletRequest contextHttpServletRequest, UriInfo contextUriInfo) throws Exception {

        List<events.model.Event> records = EventLocalServiceUtil.getEventsByCategory(event.getCategoryId());

        List<EventList> results = new ArrayList<>();

        for (events.model.Event curRecord : records) {
            if(curRecord.getEventId() == event.getEventId()){
                continue;
            }
            EventList result = getDTO(languageId, curRecord, contextUriInfo);
            // get event attendees list
            result.setAttendesList(getAttendeesList(event.getEventId(), languageId, contextUriInfo));

            results.add(result);
        }

        // update events favourite status
        FavouriteUtil.updateEventsFavouriteStatus(results, contextHttpServletRequest);

        return results.toArray(new EventList[0]);
    }

    public static CategoryList[] getEventCategories(String languageId) {
        List<Category> categories = CategoryLocalServiceUtil.getCategories(webGroupId);
        CategoryList[] catList = new CategoryList[categories.size()];

        for (int k = 0; k < categories.size(); k++) {
            CategoryList list = new CategoryList();
            list.setCatId(String.valueOf(categories.get(k).getCategoryId()));
            list.setCatTitle(categories.get(k).getName(languageId));
            catList[k] = list;
        }
        return catList;
    }

    public static Schedule[] getSchedules(long eventId, String languageId, UriInfo contextUriInfo) {
        List<events.model.Schedule> records = ScheduleLocalServiceUtil.findAllInEvent(eventId);
        Schedule[] results = new Schedule[records.size()];
        int i = 0;

        for (events.model.Schedule curRecord : records) {
            Schedule result = new Schedule();
            result.setTitle(curRecord.getTitle(languageId));
            result.setDescription(curRecord.getDescription(languageId));
            result.setDuration(curRecord.getDuration(languageId));
            result.setImage(ApiKeys.getBaseURL(contextUriInfo) + curRecord.getImage());
            results[i] = result;
            i++;
        }
        return results;
    }

    public static AddVisitor getAttendeesList(Long eventId, String languageId, UriInfo contextUriInfo)
            throws Exception {
        AddVisitor visitors = new AddVisitor();
        List<EventVisitors> eventVisitors = EventVisitorsLocalServiceUtil.findEventByvisitor(eventId);
        List<AppUser> appUsers = new ArrayList<>();
        for (EventVisitors eventVisitor : eventVisitors) {
            AppUser appUser = new AppUser();
            misk.app.users.model.AppUser appUserDb = AppUserLocalServiceUtil.fetchAppUser(eventVisitor.getUserId());
            if (Validator.isNotNull(appUserDb)) {
                if (Validator.isNotNull(appUserDb.getProfileImage())) {
                    appUser.setProfileImage(ApiKeys.getBaseURL(contextUriInfo) + "/" + appUserDb.getProfileImage());
                } else {
                    appUser.setProfileImage(ApiKeys.getBaseURL(contextUriInfo) + "/documents/91702/0/user-profile-avatar.png");
                }
                appUser.setAppUserId(appUserDb.getAppUserId());
                appUsers.add(appUser);
            }
        }
        visitors.setCount(Long.valueOf(appUsers.size()));
        visitors.setAppUsers(appUsers.toArray(new AppUser[0]));
        return visitors;
    }

    public static EventList getDTO(String languageId, events.model.Event curRecord, UriInfo contextUriInfo) {

        Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("dd MMMM");

        List<EventsGallery> eventGallery = EventsGalleryLocalServiceUtil.findAllInDiscover(curRecord.getEventId());

        String[] galleryImages = new String[eventGallery.size()];
        for (int j = 0; j < eventGallery.size(); j++) {
            galleryImages[j] = ApiKeys.getBaseURL(contextUriInfo) + eventGallery.get(j).getImage(languageId);
        }

        EventList result = new EventList();
        result.setEventId(curRecord.getEventId());
        result.setTitle(curRecord.getTitle(languageId));
        String paytype = "Free";
        if (curRecord.getPaytype() == 2) {
            paytype = "Paid";
        }
        result.setPayType(paytype);
        // get EventStartDateTime in Asia/Riyadh timezone
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("Asia/Riyadh"));
        result.setEventStartDateTime(df.format(curRecord.getEventStartDateTime()));
        // get EventEndDateTime in Asia/Riyadh timezone
        result.setEventEndDateTime(df.format(curRecord.getEventEndDateTime()));
        /*result.setEventStartDateTime(CommonUtil.convertDateTimeToStringDateTime(curRecord.getEventStartDateTime()));
        result.setEventEndDateTime(CommonUtil.convertDateTimeToStringDateTime(curRecord.getEventEndDateTime()));*/
        result.setEventDescription(curRecord.getEventdescription(languageId));
        result.setTime(curRecord.getTiming(languageId));
        result.setVenue(curRecord.getVenue(languageId));
        result.setDisplaydate(String.valueOf(curRecord.getDisplayDate()));
        result.setEventMonth(dateTimeFormat.format(curRecord.getDisplayDate()));
        result.setIsFavourite(Boolean.FALSE);
        result.setEventPrice(curRecord.getEventPrice());
        result.setCurrency(curRecord.getCurrency());
        result.setEventFeaturedImage(ApiKeys.getBaseURL(contextUriInfo) + curRecord.getEventfeaturedImage(languageId));
        result.setEventType(String.valueOf(curRecord.getType()));
        result.setSliderimage(galleryImages);
        result.setLocationLabel("Location");
        if (languageId.equals("ar_SA")) {
            result.setLocationLabel("الموقع الجغرافي");
        }
        result.setLat(curRecord.getLat());
        result.setLan(curRecord.getLan());
        result.setEmail(curRecord.getEmail());
        result.setPhone(curRecord.getPhone());
        result.setBuyTicketLink(curRecord.getBuyTicketLink(languageId));
        //@MY_TODO: need to remove category query form loop, add it outer loop, fetch and store in array
        Category category = CategoryLocalServiceUtil.fetchCategory(curRecord.getCategoryId());
        result.setEventcategory("");
        result.setCatColor("");
        if(Validator.isNotNull(category)){
            result.setEventcategory(category.getName(languageId));
            result.setCatColor(category.getCatColor());
        }
        //@MY_TODO: need to remove tag query form loop, add it outer loop, fetch and store in array
        Tag tag = TagLocalServiceUtil.fetchTag(curRecord.getTagId());
        result.setTagSlug("");
        result.setTagTitle("");
        if(Validator.isNotNull(tag)){
            result.setTagSlug(tag.getSlug());
            result.setTagTitle(tag.getName(languageId));
        }

        // generate story detail link
        String language = "en";
        if(Objects.equals(languageId, "ar_SA")){
            language = "ar";
        }
        result.setLink(ApiKeys.getBaseURL(contextUriInfo) +"/"+ language +"/events/-/events/detail/" + curRecord.getEventId());

        return result;
    }

    public static EventList updateEventFavouriteStatus(EventList item, HttpServletRequest request){

        return FavouriteUtil.updateEventFavouriteStatus(item, request);
    }

    public static List<EventList> updateEventsFavouriteStatus(List<EventList> items, HttpServletRequest request){

        return FavouriteUtil.updateEventsFavouriteStatus(items, request);
    }
}
