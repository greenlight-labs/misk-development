package misk.headless.internal.graphql.query.v1_0;

import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeFunction;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.vulcan.accept.language.AcceptLanguage;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLTypeExtension;
import com.liferay.portal.vulcan.pagination.Page;
import com.liferay.portal.vulcan.pagination.Pagination;

import java.util.Date;
import java.util.Map;
import java.util.function.BiFunction;

import javax.annotation.Generated;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.UriInfo;

import misk.headless.dto.v1_0.AddVisitor;
import misk.headless.dto.v1_0.AppUser;
import misk.headless.dto.v1_0.Attraction;
import misk.headless.dto.v1_0.BannerImages;
import misk.headless.dto.v1_0.Booking;
import misk.headless.dto.v1_0.BookingTermsConditions;
import misk.headless.dto.v1_0.CommunityWall;
import misk.headless.dto.v1_0.Contact;
import misk.headless.dto.v1_0.Court;
import misk.headless.dto.v1_0.CourtItems;
import misk.headless.dto.v1_0.EventList;
import misk.headless.dto.v1_0.ExperientialCenter;
import misk.headless.dto.v1_0.Explore;
import misk.headless.dto.v1_0.ExploreWadiC;
import misk.headless.dto.v1_0.Faq;
import misk.headless.dto.v1_0.GlobalSearch;
import misk.headless.dto.v1_0.HenaCafe;
import misk.headless.dto.v1_0.Home;
import misk.headless.dto.v1_0.MapLocation;
import misk.headless.dto.v1_0.MiskForum;
import misk.headless.dto.v1_0.MyFavorites;
import misk.headless.dto.v1_0.NotificationSettings;
import misk.headless.dto.v1_0.ProfileDocumentList;
import misk.headless.dto.v1_0.PushNotification;
import misk.headless.dto.v1_0.Story;
import misk.headless.dto.v1_0.TermsnConditions;
import misk.headless.dto.v1_0.UnreadNotificationCount;
import misk.headless.dto.v1_0.WadiC;
import misk.headless.dto.v1_0.Welcome;
import misk.headless.resource.v1_0.AppUserResource;
import misk.headless.resource.v1_0.AttractionResource;
import misk.headless.resource.v1_0.BannerImagesResource;
import misk.headless.resource.v1_0.BookingResource;
import misk.headless.resource.v1_0.CommunityWallResource;
import misk.headless.resource.v1_0.ContactResource;
import misk.headless.resource.v1_0.CourtResource;
import misk.headless.resource.v1_0.EventListResource;
import misk.headless.resource.v1_0.ExperientialCenterResource;
import misk.headless.resource.v1_0.ExploreResource;
import misk.headless.resource.v1_0.ExploreWadiCResource;
import misk.headless.resource.v1_0.FaqResource;
import misk.headless.resource.v1_0.GlobalSearchResource;
import misk.headless.resource.v1_0.HenaCafeResource;
import misk.headless.resource.v1_0.HomeResource;
import misk.headless.resource.v1_0.MapLocationResource;
import misk.headless.resource.v1_0.MiskForumResource;
import misk.headless.resource.v1_0.MyFavoritesResource;
import misk.headless.resource.v1_0.NotificationSettingsResource;
import misk.headless.resource.v1_0.ProfileDocumentListResource;
import misk.headless.resource.v1_0.PushNotificationResource;
import misk.headless.resource.v1_0.StoryResource;
import misk.headless.resource.v1_0.TermsnConditionsResource;
import misk.headless.resource.v1_0.WadiCResource;
import misk.headless.resource.v1_0.WelcomeResource;

import org.osgi.service.component.ComponentServiceObjects;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Query {

	public static void setAppUserResourceComponentServiceObjects(
		ComponentServiceObjects<AppUserResource>
			appUserResourceComponentServiceObjects) {

		_appUserResourceComponentServiceObjects =
			appUserResourceComponentServiceObjects;
	}

	public static void setAttractionResourceComponentServiceObjects(
		ComponentServiceObjects<AttractionResource>
			attractionResourceComponentServiceObjects) {

		_attractionResourceComponentServiceObjects =
			attractionResourceComponentServiceObjects;
	}

	public static void setBannerImagesResourceComponentServiceObjects(
		ComponentServiceObjects<BannerImagesResource>
			bannerImagesResourceComponentServiceObjects) {

		_bannerImagesResourceComponentServiceObjects =
			bannerImagesResourceComponentServiceObjects;
	}

	public static void setBookingResourceComponentServiceObjects(
		ComponentServiceObjects<BookingResource>
			bookingResourceComponentServiceObjects) {

		_bookingResourceComponentServiceObjects =
			bookingResourceComponentServiceObjects;
	}

	public static void setCommunityWallResourceComponentServiceObjects(
		ComponentServiceObjects<CommunityWallResource>
			communityWallResourceComponentServiceObjects) {

		_communityWallResourceComponentServiceObjects =
			communityWallResourceComponentServiceObjects;
	}

	public static void setContactResourceComponentServiceObjects(
		ComponentServiceObjects<ContactResource>
			contactResourceComponentServiceObjects) {

		_contactResourceComponentServiceObjects =
			contactResourceComponentServiceObjects;
	}

	public static void setCourtResourceComponentServiceObjects(
		ComponentServiceObjects<CourtResource>
			courtResourceComponentServiceObjects) {

		_courtResourceComponentServiceObjects =
			courtResourceComponentServiceObjects;
	}

	public static void setEventListResourceComponentServiceObjects(
		ComponentServiceObjects<EventListResource>
			eventListResourceComponentServiceObjects) {

		_eventListResourceComponentServiceObjects =
			eventListResourceComponentServiceObjects;
	}

	public static void setExperientialCenterResourceComponentServiceObjects(
		ComponentServiceObjects<ExperientialCenterResource>
			experientialCenterResourceComponentServiceObjects) {

		_experientialCenterResourceComponentServiceObjects =
			experientialCenterResourceComponentServiceObjects;
	}

	public static void setExploreResourceComponentServiceObjects(
		ComponentServiceObjects<ExploreResource>
			exploreResourceComponentServiceObjects) {

		_exploreResourceComponentServiceObjects =
			exploreResourceComponentServiceObjects;
	}

	public static void setExploreWadiCResourceComponentServiceObjects(
		ComponentServiceObjects<ExploreWadiCResource>
			exploreWadiCResourceComponentServiceObjects) {

		_exploreWadiCResourceComponentServiceObjects =
			exploreWadiCResourceComponentServiceObjects;
	}

	public static void setFaqResourceComponentServiceObjects(
		ComponentServiceObjects<FaqResource>
			faqResourceComponentServiceObjects) {

		_faqResourceComponentServiceObjects =
			faqResourceComponentServiceObjects;
	}

	public static void setGlobalSearchResourceComponentServiceObjects(
		ComponentServiceObjects<GlobalSearchResource>
			globalSearchResourceComponentServiceObjects) {

		_globalSearchResourceComponentServiceObjects =
			globalSearchResourceComponentServiceObjects;
	}

	public static void setHenaCafeResourceComponentServiceObjects(
		ComponentServiceObjects<HenaCafeResource>
			henaCafeResourceComponentServiceObjects) {

		_henaCafeResourceComponentServiceObjects =
			henaCafeResourceComponentServiceObjects;
	}

	public static void setHomeResourceComponentServiceObjects(
		ComponentServiceObjects<HomeResource>
			homeResourceComponentServiceObjects) {

		_homeResourceComponentServiceObjects =
			homeResourceComponentServiceObjects;
	}

	public static void setMapLocationResourceComponentServiceObjects(
		ComponentServiceObjects<MapLocationResource>
			mapLocationResourceComponentServiceObjects) {

		_mapLocationResourceComponentServiceObjects =
			mapLocationResourceComponentServiceObjects;
	}

	public static void setMiskForumResourceComponentServiceObjects(
		ComponentServiceObjects<MiskForumResource>
			miskForumResourceComponentServiceObjects) {

		_miskForumResourceComponentServiceObjects =
			miskForumResourceComponentServiceObjects;
	}

	public static void setMyFavoritesResourceComponentServiceObjects(
		ComponentServiceObjects<MyFavoritesResource>
			myFavoritesResourceComponentServiceObjects) {

		_myFavoritesResourceComponentServiceObjects =
			myFavoritesResourceComponentServiceObjects;
	}

	public static void setNotificationSettingsResourceComponentServiceObjects(
		ComponentServiceObjects<NotificationSettingsResource>
			notificationSettingsResourceComponentServiceObjects) {

		_notificationSettingsResourceComponentServiceObjects =
			notificationSettingsResourceComponentServiceObjects;
	}

	public static void setProfileDocumentListResourceComponentServiceObjects(
		ComponentServiceObjects<ProfileDocumentListResource>
			profileDocumentListResourceComponentServiceObjects) {

		_profileDocumentListResourceComponentServiceObjects =
			profileDocumentListResourceComponentServiceObjects;
	}

	public static void setPushNotificationResourceComponentServiceObjects(
		ComponentServiceObjects<PushNotificationResource>
			pushNotificationResourceComponentServiceObjects) {

		_pushNotificationResourceComponentServiceObjects =
			pushNotificationResourceComponentServiceObjects;
	}

	public static void setStoryResourceComponentServiceObjects(
		ComponentServiceObjects<StoryResource>
			storyResourceComponentServiceObjects) {

		_storyResourceComponentServiceObjects =
			storyResourceComponentServiceObjects;
	}

	public static void setTermsnConditionsResourceComponentServiceObjects(
		ComponentServiceObjects<TermsnConditionsResource>
			termsnConditionsResourceComponentServiceObjects) {

		_termsnConditionsResourceComponentServiceObjects =
			termsnConditionsResourceComponentServiceObjects;
	}

	public static void setWadiCResourceComponentServiceObjects(
		ComponentServiceObjects<WadiCResource>
			wadiCResourceComponentServiceObjects) {

		_wadiCResourceComponentServiceObjects =
			wadiCResourceComponentServiceObjects;
	}

	public static void setWelcomeResourceComponentServiceObjects(
		ComponentServiceObjects<WelcomeResource>
			welcomeResourceComponentServiceObjects) {

		_welcomeResourceComponentServiceObjects =
			welcomeResourceComponentServiceObjects;
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {profileDetails(languageId: ___, profileid: ___){appUserId, fullName, emailAddress, phoneNumber, languageId, password, facebookId, googleUserId, appleUserId, autoLogin, passcode, passwordResetToken, newPassword, confirmPassword, profileImage, profileBannerImage, isVerified, emailAddressVerified, phoneNumberVerified, gcpToken, androidDeviceToken, iosDeviceToken, deactivateReason, deleteReason, requestStatus}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Profile Details.")
	public AppUser profileDetails(
			@GraphQLName("profileid") Long profileid,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.getProfileDetails(
				profileid, languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {attraction(attractionid: ___, languageId: ___){attractionId, attractionBanner, attractionTitle, attractionButtonLabel, attractionBannerIcon, bannerButtonColor, workingHoursLabel, workingHours, workingHoursLabelColor, workingHoursImage, workingDaysImage, workingDays, description, locationLabel, locationLatitude, locationLongitude, contactLabel, contactEmailAddress, contactTelephone, contactEmailAddressIcon, contactTelephoneIcon, galleryLabel, galleryImagesList, amenitiesLabel, amenitiesList, bookSpaceLabel, bookSpaceList, eventsSection}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Attraction Information.")
	public Attraction attraction(
			@GraphQLName("attractionid") Long attractionid,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_attractionResourceComponentServiceObjects,
			this::_populateResourceContext,
			attractionResource -> attractionResource.getAttraction(
				attractionid, languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {bannerImages{souvenirsImageBanner, favoritesBannerImage}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Get banner images links.")
	public BannerImages bannerImages() throws Exception {
		return _applyComponentServiceObjects(
			_bannerImagesResourceComponentServiceObjects,
			this::_populateResourceContext,
			bannerImagesResource -> bannerImagesResource.getBannerImages());
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {courtBookingTimeSlots(courtId: ___, date: ___, languageId: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(
		description = "Retrieves the court booking available time slots."
	)
	public BookingPage courtBookingTimeSlots(
			@GraphQLName("courtId") Long courtId,
			@GraphQLName("languageId") String languageId,
			@GraphQLName("date") Date date)
		throws Exception {

		return _applyComponentServiceObjects(
			_bookingResourceComponentServiceObjects,
			this::_populateResourceContext,
			bookingResource -> new BookingPage(
				bookingResource.getCourtBookingTimeSlots(
					courtId, languageId, date)));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {courtBookingTermsConditions(languageId: ___){title, excerpt, description}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(
		description = "Retrieves the court booking terms and conditions."
	)
	public BookingTermsConditions courtBookingTermsConditions(
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_bookingResourceComponentServiceObjects,
			this::_populateResourceContext,
			bookingResource -> bookingResource.getCourtBookingTermsConditions(
				languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {bookingDetails(bookingId: ___, languageId: ___){}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the court detail page.")
	public Object bookingDetails(
			@GraphQLName("bookingId") Long bookingId,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_bookingResourceComponentServiceObjects,
			this::_populateResourceContext,
			bookingResource -> bookingResource.getBookingDetails(
				bookingId, languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {communityWall{data}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Get Community Wall Json.")
	public CommunityWall communityWall() throws Exception {
		return _applyComponentServiceObjects(
			_communityWallResourceComponentServiceObjects,
			this::_populateResourceContext,
			communityWallResource -> communityWallResource.getCommunityWall());
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {contact(languageId: ___){ContactLabel, Description, LocationLabel, LocationLatitude, LocationLongitude, Address, CallUsLabel, CallUsNumber, WhatsAppLabel, WhatsAppNumber, WebsiteLabel, WebsiteAddress, EmailLabel, EmailAddress, WorkingHoursLabel, WorkingHours, SocialMediaLabel, SocialMediaList}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Contact Information.")
	public Contact contact(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_contactResourceComponentServiceObjects,
			this::_populateResourceContext,
			contactResource -> contactResource.getContact(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {courtItems(languageId: ___){courtLocations, courtCategories, courts}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the court page.")
	public CourtItems courtItems(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_courtResourceComponentServiceObjects,
			this::_populateResourceContext,
			courtResource -> courtResource.getCourtItems(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {courtDetails(appUserId: ___, courtId: ___, languageId: ___){id, categoryId, heading, name, listingImage, detailImages, venue, openingHours, openingDays, description, lat, lon, contactEmail, contactPhone, courtBooking, courtCategory}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the court detail page.")
	public Court courtDetails(
			@GraphQLName("courtId") Long courtId,
			@GraphQLName("languageId") String languageId,
			@GraphQLName("appUserId") Long appUserId)
		throws Exception {

		return _applyComponentServiceObjects(
			_courtResourceComponentServiceObjects,
			this::_populateResourceContext,
			courtResource -> courtResource.getCourtDetails(
				courtId, languageId, appUserId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {relatedCourts(categoryId: ___, courtId: ___, languageId: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the related court section details.")
	public CourtPage relatedCourts(
			@GraphQLName("categoryId") Long categoryId,
			@GraphQLName("courtId") Long courtId,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_courtResourceComponentServiceObjects,
			this::_populateResourceContext,
			courtResource -> new CourtPage(
				courtResource.getRelatedCourts(
					categoryId, courtId, languageId)));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {eventsListing(categoryId: ___, dateFrom: ___, dateTo: ___, languageId: ___, page: ___, pageSize: ___, payType: ___, priceFrom: ___, priceTo: ___, typeevent: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Event Listing sections.")
	public EventListPage eventsListing(
			@GraphQLName("languageId") String languageId,
			@GraphQLName("typeevent") String typeevent,
			@GraphQLName("payType") Integer payType,
			@GraphQLName("priceFrom") String priceFrom,
			@GraphQLName("priceTo") String priceTo,
			@GraphQLName("categoryId") Long categoryId,
			@GraphQLName("dateFrom") String dateFrom,
			@GraphQLName("dateTo") String dateTo,
			@GraphQLName("pageSize") int pageSize,
			@GraphQLName("page") int page)
		throws Exception {

		return _applyComponentServiceObjects(
			_eventListResourceComponentServiceObjects,
			this::_populateResourceContext,
			eventListResource -> new EventListPage(
				eventListResource.getEventsListing(
					languageId, typeevent, payType, priceFrom, priceTo,
					categoryId, dateFrom, dateTo,
					Pagination.of(page, pageSize))));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {eventsDetails(eventid: ___, languageId: ___){eventId, title, displaydate, eventStartDateTime, eventEndDateTime, venue, time, payType, eventDescription, eventLocation, eventType, eventcategory, catColor, eventFeaturedImage, isFavourite, eventMonth, locationLabel, lat, lan, email, phone, eventPrice, buyTicketLink, currency, link, sliderimage, eventLists, categoryLists, attendesList, schedules, tagSlug, tagTitle}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Events details sections.")
	public EventList eventsDetails(
			@GraphQLName("eventid") Long eventid,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_eventListResourceComponentServiceObjects,
			this::_populateResourceContext,
			eventListResource -> eventListResource.getEventsDetails(
				eventid, languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {attendeesList(eventid: ___, languageId: ___){eventId, userId, count, appUsers}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Events Attendees sections.")
	public AddVisitor attendeesList(
			@GraphQLName("eventid") Long eventid,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_eventListResourceComponentServiceObjects,
			this::_populateResourceContext,
			eventListResource -> eventListResource.getAttendeesList(
				eventid, languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {eventCategories(languageId: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Event Categories.")
	public EventListPage eventCategories(
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_eventListResourceComponentServiceObjects,
			this::_populateResourceContext,
			eventListResource -> new EventListPage(
				eventListResource.getEventCategories(languageId)));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {experientialCenterPage(languageId: ___){bannerSection, highlightsSection, henaCafeSection, discoverSection, eventsSection}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(
		description = "Retrieves the experiential center page sections."
	)
	public ExperientialCenter experientialCenterPage(
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_experientialCenterResourceComponentServiceObjects,
			this::_populateResourceContext,
			experientialCenterResource ->
				experientialCenterResource.getExperientialCenterPage(
					languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {explore(languageId: ___){expolerId, headerBanner, headerTitle, headerDesc, locationLabel, locationLatitude, locationLongitude, galleryLabel, galleryImagesList, faclitiesLabel, faclitiesList, exploreLabel, exploreList, eventsSection}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Explore Information.")
	public Explore explore(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_exploreResourceComponentServiceObjects,
			this::_populateResourceContext,
			exploreResource -> exploreResource.getExplore(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {exploreWadiC(languageId: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Explore Wadi C Information.")
	public ExploreWadiCPage exploreWadiC(
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_exploreWadiCResourceComponentServiceObjects,
			this::_populateResourceContext,
			exploreWadiCResource -> new ExploreWadiCPage(
				exploreWadiCResource.getExploreWadiC(languageId)));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {faqList(languageId: ___){faqList}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the FaqList.")
	public Faq faqList(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_faqResourceComponentServiceObjects, this::_populateResourceContext,
			faqResource -> faqResource.getFaqList(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {globalSearch(languageId: ___, searchTerm: ___){}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the court detail page.")
	public Object globalSearch(
			@GraphQLName("searchTerm") String searchTerm,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_globalSearchResourceComponentServiceObjects,
			this::_populateResourceContext,
			globalSearchResource -> globalSearchResource.getGlobalSearch(
				searchTerm, languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {henaCafe(languageId: ___){title, description, sliderimage, subtitle, location, workinghours, workingdays, workingsummary, categories, latitude, longitude}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Hena Cafe sections.")
	public HenaCafe henaCafe(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_henaCafeResourceComponentServiceObjects,
			this::_populateResourceContext,
			henaCafeResource -> henaCafeResource.getHenaCafe(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {homePage(languageId: ___){flipCards, highlightsSection, storiesSection, eventsSection}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the home page sections.")
	public Home homePage(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_homeResourceComponentServiceObjects,
			this::_populateResourceContext,
			homeResource -> homeResource.getHomePage(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {mapLocations(languageId: ___, page: ___, pageSize: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves map locations.")
	public MapLocationPage mapLocations(
			@GraphQLName("languageId") String languageId,
			@GraphQLName("pageSize") int pageSize,
			@GraphQLName("page") int page)
		throws Exception {

		return _applyComponentServiceObjects(
			_mapLocationResourceComponentServiceObjects,
			this::_populateResourceContext,
			mapLocationResource -> new MapLocationPage(
				mapLocationResource.getMapLocations(
					languageId, Pagination.of(page, pageSize))));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {miskForum(languageId: ___){MiskForumBanner, MiskForumLabel, WorkingHoursLabel, WorkingHours, WorkingDay, Description, LocationLabel, LocationLatitude, LocationLongitude, ContactLabel, ContactEmailAddress, ContactTelephone, EventLabel, EventName, GalleryLabel, GalleryImagesList, AmenitiesLabel, AmenitiesList, BookSpaceLabel, BookSpaceList, eventsSection}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Misk Forum Information.")
	public MiskForum miskForum(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_miskForumResourceComponentServiceObjects,
			this::_populateResourceContext,
			miskForumResource -> miskForumResource.getMiskForum(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {myFavorites(languageId: ___, typeId: ___, userId: ___){}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves user favorites.")
	public Object myFavorites(
			@GraphQLName("languageId") String languageId,
			@GraphQLName("typeId") Long typeId,
			@GraphQLName("userId") Long userId)
		throws Exception {

		return _applyComponentServiceObjects(
			_myFavoritesResourceComponentServiceObjects,
			this::_populateResourceContext,
			myFavoritesResource -> myFavoritesResource.getMyFavorites(
				languageId, typeId, userId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {notificationSettingsPage(appUserId: ___){news, events, highlight, broadcastMessage, appUserId, companyId, groupId}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(
		description = "Retrieves the list of vitamins and minerals. Results can be paginated, filtered, searched, and sorted."
	)
	public NotificationSettings notificationSettingsPage(
			@GraphQLName("appUserId") Long appUserId)
		throws Exception {

		return _applyComponentServiceObjects(
			_notificationSettingsResourceComponentServiceObjects,
			this::_populateResourceContext,
			notificationSettingsResource ->
				notificationSettingsResource.getNotificationSettingsPage(
					appUserId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {uploadedFile(appUserId: ___){appUserId, uploadedFile}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the list of uploaded files.")
	public ProfileDocumentList uploadedFile(
			@GraphQLName("appUserId") Long appUserId)
		throws Exception {

		return _applyComponentServiceObjects(
			_profileDocumentListResourceComponentServiceObjects,
			this::_populateResourceContext,
			profileDocumentListResource ->
				profileDocumentListResource.getUploadedFile(appUserId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {pushNotifications(appUserId: ___, page: ___, pageSize: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField
	public PushNotificationPage pushNotifications(
			@GraphQLName("appUserId") Long appUserId,
			@GraphQLName("pageSize") int pageSize,
			@GraphQLName("page") int page)
		throws Exception {

		return _applyComponentServiceObjects(
			_pushNotificationResourceComponentServiceObjects,
			this::_populateResourceContext,
			pushNotificationResource -> new PushNotificationPage(
				pushNotificationResource.getPushNotifications(
					appUserId, Pagination.of(page, pageSize))));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {unreadNotificationCount(appUserId: ___){unreadCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField
	public UnreadNotificationCount unreadNotificationCount(
			@GraphQLName("appUserId") Long appUserId)
		throws Exception {

		return _applyComponentServiceObjects(
			_pushNotificationResourceComponentServiceObjects,
			this::_populateResourceContext,
			pushNotificationResource ->
				pushNotificationResource.getUnreadNotificationCount(appUserId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {stories(languageId: ___, page: ___, pageSize: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the story listing section.")
	public StoryPage stories(
			@GraphQLName("languageId") String languageId,
			@GraphQLName("pageSize") int pageSize,
			@GraphQLName("page") int page)
		throws Exception {

		return _applyComponentServiceObjects(
			_storyResourceComponentServiceObjects,
			this::_populateResourceContext,
			storyResource -> new StoryPage(
				storyResource.getStories(
					languageId, Pagination.of(page, pageSize))));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {storyDetails(languageId: ___, storyId: ___){storyId, title, image, date, description, link, isFavourite}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the story detail page.")
	public Story storyDetails(
			@GraphQLName("storyId") Long storyId,
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_storyResourceComponentServiceObjects,
			this::_populateResourceContext,
			storyResource -> storyResource.getStoryDetails(
				storyId, languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {termsnConditions(languageId: ___){title, description}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the Hena Cafe sections.")
	public TermsnConditions termsnConditions(
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_termsnConditionsResourceComponentServiceObjects,
			this::_populateResourceContext,
			termsnConditionsResource ->
				termsnConditionsResource.getTermsnConditions(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {wadiC(languageId: ___){bannerSection, attractionSection, bookCourtSection, eventsSection}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(description = "Retrieves the WadiC page sections.")
	public WadiC wadiC(@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_wadiCResourceComponentServiceObjects,
			this::_populateResourceContext,
			wadiCResource -> wadiCResource.getWadiC(languageId));
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -H 'Content-Type: text/plain; charset=utf-8' -X 'POST' 'http://localhost:8080/o/graphql' -d $'{"query": "query {welcomeScreens(languageId: ___){items {__}, page, pageSize, totalCount}}"}' -u 'test@liferay.com:test'
	 */
	@GraphQLField(
		description = "Retrieves the content of all tutorial screens."
	)
	public WelcomePage welcomeScreens(
			@GraphQLName("languageId") String languageId)
		throws Exception {

		return _applyComponentServiceObjects(
			_welcomeResourceComponentServiceObjects,
			this::_populateResourceContext,
			welcomeResource -> new WelcomePage(
				welcomeResource.getWelcomeScreens(languageId)));
	}

	@GraphQLTypeExtension(NotificationSettings.class)
	public class GetUploadedFileTypeExtension {

		public GetUploadedFileTypeExtension(
			NotificationSettings notificationSettings) {

			_notificationSettings = notificationSettings;
		}

		@GraphQLField(description = "Retrieves the list of uploaded files.")
		public ProfileDocumentList uploadedFile() throws Exception {
			return _applyComponentServiceObjects(
				_profileDocumentListResourceComponentServiceObjects,
				Query.this::_populateResourceContext,
				profileDocumentListResource ->
					profileDocumentListResource.getUploadedFile(
						_notificationSettings.getAppUserId()));
		}

		private NotificationSettings _notificationSettings;

	}

	@GraphQLTypeExtension(ProfileDocumentList.class)
	public class GetNotificationSettingsPageTypeExtension {

		public GetNotificationSettingsPageTypeExtension(
			ProfileDocumentList profileDocumentList) {

			_profileDocumentList = profileDocumentList;
		}

		@GraphQLField(
			description = "Retrieves the list of vitamins and minerals. Results can be paginated, filtered, searched, and sorted."
		)
		public NotificationSettings notificationSettingsPage()
			throws Exception {

			return _applyComponentServiceObjects(
				_notificationSettingsResourceComponentServiceObjects,
				Query.this::_populateResourceContext,
				notificationSettingsResource ->
					notificationSettingsResource.getNotificationSettingsPage(
						_profileDocumentList.getAppUserId()));
		}

		private ProfileDocumentList _profileDocumentList;

	}

	@GraphQLTypeExtension(NotificationSettings.class)
	public class GetUnreadNotificationCountTypeExtension {

		public GetUnreadNotificationCountTypeExtension(
			NotificationSettings notificationSettings) {

			_notificationSettings = notificationSettings;
		}

		@GraphQLField
		public UnreadNotificationCount unreadNotificationCount()
			throws Exception {

			return _applyComponentServiceObjects(
				_pushNotificationResourceComponentServiceObjects,
				Query.this::_populateResourceContext,
				pushNotificationResource ->
					pushNotificationResource.getUnreadNotificationCount(
						_notificationSettings.getAppUserId()));
		}

		private NotificationSettings _notificationSettings;

	}

	@GraphQLTypeExtension(NotificationSettings.class)
	public class GetPushNotificationsTypeExtension {

		public GetPushNotificationsTypeExtension(
			NotificationSettings notificationSettings) {

			_notificationSettings = notificationSettings;
		}

		@GraphQLField
		public PushNotificationPage pushNotifications(
				@GraphQLName("pageSize") int pageSize,
				@GraphQLName("page") int page)
			throws Exception {

			return _applyComponentServiceObjects(
				_pushNotificationResourceComponentServiceObjects,
				Query.this::_populateResourceContext,
				pushNotificationResource -> new PushNotificationPage(
					pushNotificationResource.getPushNotifications(
						_notificationSettings.getAppUserId(),
						Pagination.of(page, pageSize))));
		}

		private NotificationSettings _notificationSettings;

	}

	@GraphQLTypeExtension(Booking.class)
	public class GetCourtDetailsTypeExtension {

		public GetCourtDetailsTypeExtension(Booking booking) {
			_booking = booking;
		}

		@GraphQLField(description = "Retrieves the court detail page.")
		public Court courtDetails(
				@GraphQLName("languageId") String languageId,
				@GraphQLName("appUserId") Long appUserId)
			throws Exception {

			return _applyComponentServiceObjects(
				_courtResourceComponentServiceObjects,
				Query.this::_populateResourceContext,
				courtResource -> courtResource.getCourtDetails(
					_booking.getCourtId(), languageId, appUserId));
		}

		private Booking _booking;

	}

	@GraphQLName("AppUserPage")
	public class AppUserPage {

		public AppUserPage(Page appUserPage) {
			actions = appUserPage.getActions();

			items = appUserPage.getItems();
			lastPage = appUserPage.getLastPage();
			page = appUserPage.getPage();
			pageSize = appUserPage.getPageSize();
			totalCount = appUserPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<AppUser> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("AttractionPage")
	public class AttractionPage {

		public AttractionPage(Page attractionPage) {
			actions = attractionPage.getActions();

			items = attractionPage.getItems();
			lastPage = attractionPage.getLastPage();
			page = attractionPage.getPage();
			pageSize = attractionPage.getPageSize();
			totalCount = attractionPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Attraction> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("BannerImagesPage")
	public class BannerImagesPage {

		public BannerImagesPage(Page bannerImagesPage) {
			actions = bannerImagesPage.getActions();

			items = bannerImagesPage.getItems();
			lastPage = bannerImagesPage.getLastPage();
			page = bannerImagesPage.getPage();
			pageSize = bannerImagesPage.getPageSize();
			totalCount = bannerImagesPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<BannerImages> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("BookingPage")
	public class BookingPage {

		public BookingPage(Page bookingPage) {
			actions = bookingPage.getActions();

			items = bookingPage.getItems();
			lastPage = bookingPage.getLastPage();
			page = bookingPage.getPage();
			pageSize = bookingPage.getPageSize();
			totalCount = bookingPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Booking> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("CommunityWallPage")
	public class CommunityWallPage {

		public CommunityWallPage(Page communityWallPage) {
			actions = communityWallPage.getActions();

			items = communityWallPage.getItems();
			lastPage = communityWallPage.getLastPage();
			page = communityWallPage.getPage();
			pageSize = communityWallPage.getPageSize();
			totalCount = communityWallPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<CommunityWall> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("ContactPage")
	public class ContactPage {

		public ContactPage(Page contactPage) {
			actions = contactPage.getActions();

			items = contactPage.getItems();
			lastPage = contactPage.getLastPage();
			page = contactPage.getPage();
			pageSize = contactPage.getPageSize();
			totalCount = contactPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Contact> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("CourtPage")
	public class CourtPage {

		public CourtPage(Page courtPage) {
			actions = courtPage.getActions();

			items = courtPage.getItems();
			lastPage = courtPage.getLastPage();
			page = courtPage.getPage();
			pageSize = courtPage.getPageSize();
			totalCount = courtPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Court> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("EventListPage")
	public class EventListPage {

		public EventListPage(Page eventListPage) {
			actions = eventListPage.getActions();

			items = eventListPage.getItems();
			lastPage = eventListPage.getLastPage();
			page = eventListPage.getPage();
			pageSize = eventListPage.getPageSize();
			totalCount = eventListPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<EventList> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("ExperientialCenterPage")
	public class ExperientialCenterPage {

		public ExperientialCenterPage(Page experientialCenterPage) {
			actions = experientialCenterPage.getActions();

			items = experientialCenterPage.getItems();
			lastPage = experientialCenterPage.getLastPage();
			page = experientialCenterPage.getPage();
			pageSize = experientialCenterPage.getPageSize();
			totalCount = experientialCenterPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<ExperientialCenter> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("ExplorePage")
	public class ExplorePage {

		public ExplorePage(Page explorePage) {
			actions = explorePage.getActions();

			items = explorePage.getItems();
			lastPage = explorePage.getLastPage();
			page = explorePage.getPage();
			pageSize = explorePage.getPageSize();
			totalCount = explorePage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Explore> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("ExploreWadiCPage")
	public class ExploreWadiCPage {

		public ExploreWadiCPage(Page exploreWadiCPage) {
			actions = exploreWadiCPage.getActions();

			items = exploreWadiCPage.getItems();
			lastPage = exploreWadiCPage.getLastPage();
			page = exploreWadiCPage.getPage();
			pageSize = exploreWadiCPage.getPageSize();
			totalCount = exploreWadiCPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<ExploreWadiC> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("FaqPage")
	public class FaqPage {

		public FaqPage(Page faqPage) {
			actions = faqPage.getActions();

			items = faqPage.getItems();
			lastPage = faqPage.getLastPage();
			page = faqPage.getPage();
			pageSize = faqPage.getPageSize();
			totalCount = faqPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Faq> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("GlobalSearchPage")
	public class GlobalSearchPage {

		public GlobalSearchPage(Page globalSearchPage) {
			actions = globalSearchPage.getActions();

			items = globalSearchPage.getItems();
			lastPage = globalSearchPage.getLastPage();
			page = globalSearchPage.getPage();
			pageSize = globalSearchPage.getPageSize();
			totalCount = globalSearchPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<GlobalSearch> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("HenaCafePage")
	public class HenaCafePage {

		public HenaCafePage(Page henaCafePage) {
			actions = henaCafePage.getActions();

			items = henaCafePage.getItems();
			lastPage = henaCafePage.getLastPage();
			page = henaCafePage.getPage();
			pageSize = henaCafePage.getPageSize();
			totalCount = henaCafePage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<HenaCafe> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("HomePage")
	public class HomePage {

		public HomePage(Page homePage) {
			actions = homePage.getActions();

			items = homePage.getItems();
			lastPage = homePage.getLastPage();
			page = homePage.getPage();
			pageSize = homePage.getPageSize();
			totalCount = homePage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Home> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("MapLocationPage")
	public class MapLocationPage {

		public MapLocationPage(Page mapLocationPage) {
			actions = mapLocationPage.getActions();

			items = mapLocationPage.getItems();
			lastPage = mapLocationPage.getLastPage();
			page = mapLocationPage.getPage();
			pageSize = mapLocationPage.getPageSize();
			totalCount = mapLocationPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<MapLocation> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("MiskForumPage")
	public class MiskForumPage {

		public MiskForumPage(Page miskForumPage) {
			actions = miskForumPage.getActions();

			items = miskForumPage.getItems();
			lastPage = miskForumPage.getLastPage();
			page = miskForumPage.getPage();
			pageSize = miskForumPage.getPageSize();
			totalCount = miskForumPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<MiskForum> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("MyFavoritesPage")
	public class MyFavoritesPage {

		public MyFavoritesPage(Page myFavoritesPage) {
			actions = myFavoritesPage.getActions();

			items = myFavoritesPage.getItems();
			lastPage = myFavoritesPage.getLastPage();
			page = myFavoritesPage.getPage();
			pageSize = myFavoritesPage.getPageSize();
			totalCount = myFavoritesPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<MyFavorites> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("NotificationSettingsPage")
	public class NotificationSettingsPage {

		public NotificationSettingsPage(Page notificationSettingsPage) {
			actions = notificationSettingsPage.getActions();

			items = notificationSettingsPage.getItems();
			lastPage = notificationSettingsPage.getLastPage();
			page = notificationSettingsPage.getPage();
			pageSize = notificationSettingsPage.getPageSize();
			totalCount = notificationSettingsPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<NotificationSettings> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("ProfileDocumentListPage")
	public class ProfileDocumentListPage {

		public ProfileDocumentListPage(Page profileDocumentListPage) {
			actions = profileDocumentListPage.getActions();

			items = profileDocumentListPage.getItems();
			lastPage = profileDocumentListPage.getLastPage();
			page = profileDocumentListPage.getPage();
			pageSize = profileDocumentListPage.getPageSize();
			totalCount = profileDocumentListPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<ProfileDocumentList> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("PushNotificationPage")
	public class PushNotificationPage {

		public PushNotificationPage(Page pushNotificationPage) {
			actions = pushNotificationPage.getActions();

			items = pushNotificationPage.getItems();
			lastPage = pushNotificationPage.getLastPage();
			page = pushNotificationPage.getPage();
			pageSize = pushNotificationPage.getPageSize();
			totalCount = pushNotificationPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<PushNotification> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("StoryPage")
	public class StoryPage {

		public StoryPage(Page storyPage) {
			actions = storyPage.getActions();

			items = storyPage.getItems();
			lastPage = storyPage.getLastPage();
			page = storyPage.getPage();
			pageSize = storyPage.getPageSize();
			totalCount = storyPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Story> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("TermsnConditionsPage")
	public class TermsnConditionsPage {

		public TermsnConditionsPage(Page termsnConditionsPage) {
			actions = termsnConditionsPage.getActions();

			items = termsnConditionsPage.getItems();
			lastPage = termsnConditionsPage.getLastPage();
			page = termsnConditionsPage.getPage();
			pageSize = termsnConditionsPage.getPageSize();
			totalCount = termsnConditionsPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<TermsnConditions> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("WadiCPage")
	public class WadiCPage {

		public WadiCPage(Page wadiCPage) {
			actions = wadiCPage.getActions();

			items = wadiCPage.getItems();
			lastPage = wadiCPage.getLastPage();
			page = wadiCPage.getPage();
			pageSize = wadiCPage.getPageSize();
			totalCount = wadiCPage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<WadiC> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	@GraphQLName("WelcomePage")
	public class WelcomePage {

		public WelcomePage(Page welcomePage) {
			actions = welcomePage.getActions();

			items = welcomePage.getItems();
			lastPage = welcomePage.getLastPage();
			page = welcomePage.getPage();
			pageSize = welcomePage.getPageSize();
			totalCount = welcomePage.getTotalCount();
		}

		@GraphQLField
		protected Map<String, Map> actions;

		@GraphQLField
		protected java.util.Collection<Welcome> items;

		@GraphQLField
		protected long lastPage;

		@GraphQLField
		protected long page;

		@GraphQLField
		protected long pageSize;

		@GraphQLField
		protected long totalCount;

	}

	private <T, R, E1 extends Throwable, E2 extends Throwable> R
			_applyComponentServiceObjects(
				ComponentServiceObjects<T> componentServiceObjects,
				UnsafeConsumer<T, E1> unsafeConsumer,
				UnsafeFunction<T, R, E2> unsafeFunction)
		throws E1, E2 {

		T resource = componentServiceObjects.getService();

		try {
			unsafeConsumer.accept(resource);

			return unsafeFunction.apply(resource);
		}
		finally {
			componentServiceObjects.ungetService(resource);
		}
	}

	private void _populateResourceContext(AppUserResource appUserResource)
		throws Exception {

		appUserResource.setContextAcceptLanguage(_acceptLanguage);
		appUserResource.setContextCompany(_company);
		appUserResource.setContextHttpServletRequest(_httpServletRequest);
		appUserResource.setContextHttpServletResponse(_httpServletResponse);
		appUserResource.setContextUriInfo(_uriInfo);
		appUserResource.setContextUser(_user);
		appUserResource.setGroupLocalService(_groupLocalService);
		appUserResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(AttractionResource attractionResource)
		throws Exception {

		attractionResource.setContextAcceptLanguage(_acceptLanguage);
		attractionResource.setContextCompany(_company);
		attractionResource.setContextHttpServletRequest(_httpServletRequest);
		attractionResource.setContextHttpServletResponse(_httpServletResponse);
		attractionResource.setContextUriInfo(_uriInfo);
		attractionResource.setContextUser(_user);
		attractionResource.setGroupLocalService(_groupLocalService);
		attractionResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			BannerImagesResource bannerImagesResource)
		throws Exception {

		bannerImagesResource.setContextAcceptLanguage(_acceptLanguage);
		bannerImagesResource.setContextCompany(_company);
		bannerImagesResource.setContextHttpServletRequest(_httpServletRequest);
		bannerImagesResource.setContextHttpServletResponse(
			_httpServletResponse);
		bannerImagesResource.setContextUriInfo(_uriInfo);
		bannerImagesResource.setContextUser(_user);
		bannerImagesResource.setGroupLocalService(_groupLocalService);
		bannerImagesResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(BookingResource bookingResource)
		throws Exception {

		bookingResource.setContextAcceptLanguage(_acceptLanguage);
		bookingResource.setContextCompany(_company);
		bookingResource.setContextHttpServletRequest(_httpServletRequest);
		bookingResource.setContextHttpServletResponse(_httpServletResponse);
		bookingResource.setContextUriInfo(_uriInfo);
		bookingResource.setContextUser(_user);
		bookingResource.setGroupLocalService(_groupLocalService);
		bookingResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			CommunityWallResource communityWallResource)
		throws Exception {

		communityWallResource.setContextAcceptLanguage(_acceptLanguage);
		communityWallResource.setContextCompany(_company);
		communityWallResource.setContextHttpServletRequest(_httpServletRequest);
		communityWallResource.setContextHttpServletResponse(
			_httpServletResponse);
		communityWallResource.setContextUriInfo(_uriInfo);
		communityWallResource.setContextUser(_user);
		communityWallResource.setGroupLocalService(_groupLocalService);
		communityWallResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(ContactResource contactResource)
		throws Exception {

		contactResource.setContextAcceptLanguage(_acceptLanguage);
		contactResource.setContextCompany(_company);
		contactResource.setContextHttpServletRequest(_httpServletRequest);
		contactResource.setContextHttpServletResponse(_httpServletResponse);
		contactResource.setContextUriInfo(_uriInfo);
		contactResource.setContextUser(_user);
		contactResource.setGroupLocalService(_groupLocalService);
		contactResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(CourtResource courtResource)
		throws Exception {

		courtResource.setContextAcceptLanguage(_acceptLanguage);
		courtResource.setContextCompany(_company);
		courtResource.setContextHttpServletRequest(_httpServletRequest);
		courtResource.setContextHttpServletResponse(_httpServletResponse);
		courtResource.setContextUriInfo(_uriInfo);
		courtResource.setContextUser(_user);
		courtResource.setGroupLocalService(_groupLocalService);
		courtResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(EventListResource eventListResource)
		throws Exception {

		eventListResource.setContextAcceptLanguage(_acceptLanguage);
		eventListResource.setContextCompany(_company);
		eventListResource.setContextHttpServletRequest(_httpServletRequest);
		eventListResource.setContextHttpServletResponse(_httpServletResponse);
		eventListResource.setContextUriInfo(_uriInfo);
		eventListResource.setContextUser(_user);
		eventListResource.setGroupLocalService(_groupLocalService);
		eventListResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			ExperientialCenterResource experientialCenterResource)
		throws Exception {

		experientialCenterResource.setContextAcceptLanguage(_acceptLanguage);
		experientialCenterResource.setContextCompany(_company);
		experientialCenterResource.setContextHttpServletRequest(
			_httpServletRequest);
		experientialCenterResource.setContextHttpServletResponse(
			_httpServletResponse);
		experientialCenterResource.setContextUriInfo(_uriInfo);
		experientialCenterResource.setContextUser(_user);
		experientialCenterResource.setGroupLocalService(_groupLocalService);
		experientialCenterResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(ExploreResource exploreResource)
		throws Exception {

		exploreResource.setContextAcceptLanguage(_acceptLanguage);
		exploreResource.setContextCompany(_company);
		exploreResource.setContextHttpServletRequest(_httpServletRequest);
		exploreResource.setContextHttpServletResponse(_httpServletResponse);
		exploreResource.setContextUriInfo(_uriInfo);
		exploreResource.setContextUser(_user);
		exploreResource.setGroupLocalService(_groupLocalService);
		exploreResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			ExploreWadiCResource exploreWadiCResource)
		throws Exception {

		exploreWadiCResource.setContextAcceptLanguage(_acceptLanguage);
		exploreWadiCResource.setContextCompany(_company);
		exploreWadiCResource.setContextHttpServletRequest(_httpServletRequest);
		exploreWadiCResource.setContextHttpServletResponse(
			_httpServletResponse);
		exploreWadiCResource.setContextUriInfo(_uriInfo);
		exploreWadiCResource.setContextUser(_user);
		exploreWadiCResource.setGroupLocalService(_groupLocalService);
		exploreWadiCResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(FaqResource faqResource)
		throws Exception {

		faqResource.setContextAcceptLanguage(_acceptLanguage);
		faqResource.setContextCompany(_company);
		faqResource.setContextHttpServletRequest(_httpServletRequest);
		faqResource.setContextHttpServletResponse(_httpServletResponse);
		faqResource.setContextUriInfo(_uriInfo);
		faqResource.setContextUser(_user);
		faqResource.setGroupLocalService(_groupLocalService);
		faqResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			GlobalSearchResource globalSearchResource)
		throws Exception {

		globalSearchResource.setContextAcceptLanguage(_acceptLanguage);
		globalSearchResource.setContextCompany(_company);
		globalSearchResource.setContextHttpServletRequest(_httpServletRequest);
		globalSearchResource.setContextHttpServletResponse(
			_httpServletResponse);
		globalSearchResource.setContextUriInfo(_uriInfo);
		globalSearchResource.setContextUser(_user);
		globalSearchResource.setGroupLocalService(_groupLocalService);
		globalSearchResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(HenaCafeResource henaCafeResource)
		throws Exception {

		henaCafeResource.setContextAcceptLanguage(_acceptLanguage);
		henaCafeResource.setContextCompany(_company);
		henaCafeResource.setContextHttpServletRequest(_httpServletRequest);
		henaCafeResource.setContextHttpServletResponse(_httpServletResponse);
		henaCafeResource.setContextUriInfo(_uriInfo);
		henaCafeResource.setContextUser(_user);
		henaCafeResource.setGroupLocalService(_groupLocalService);
		henaCafeResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(HomeResource homeResource)
		throws Exception {

		homeResource.setContextAcceptLanguage(_acceptLanguage);
		homeResource.setContextCompany(_company);
		homeResource.setContextHttpServletRequest(_httpServletRequest);
		homeResource.setContextHttpServletResponse(_httpServletResponse);
		homeResource.setContextUriInfo(_uriInfo);
		homeResource.setContextUser(_user);
		homeResource.setGroupLocalService(_groupLocalService);
		homeResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			MapLocationResource mapLocationResource)
		throws Exception {

		mapLocationResource.setContextAcceptLanguage(_acceptLanguage);
		mapLocationResource.setContextCompany(_company);
		mapLocationResource.setContextHttpServletRequest(_httpServletRequest);
		mapLocationResource.setContextHttpServletResponse(_httpServletResponse);
		mapLocationResource.setContextUriInfo(_uriInfo);
		mapLocationResource.setContextUser(_user);
		mapLocationResource.setGroupLocalService(_groupLocalService);
		mapLocationResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(MiskForumResource miskForumResource)
		throws Exception {

		miskForumResource.setContextAcceptLanguage(_acceptLanguage);
		miskForumResource.setContextCompany(_company);
		miskForumResource.setContextHttpServletRequest(_httpServletRequest);
		miskForumResource.setContextHttpServletResponse(_httpServletResponse);
		miskForumResource.setContextUriInfo(_uriInfo);
		miskForumResource.setContextUser(_user);
		miskForumResource.setGroupLocalService(_groupLocalService);
		miskForumResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			MyFavoritesResource myFavoritesResource)
		throws Exception {

		myFavoritesResource.setContextAcceptLanguage(_acceptLanguage);
		myFavoritesResource.setContextCompany(_company);
		myFavoritesResource.setContextHttpServletRequest(_httpServletRequest);
		myFavoritesResource.setContextHttpServletResponse(_httpServletResponse);
		myFavoritesResource.setContextUriInfo(_uriInfo);
		myFavoritesResource.setContextUser(_user);
		myFavoritesResource.setGroupLocalService(_groupLocalService);
		myFavoritesResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			NotificationSettingsResource notificationSettingsResource)
		throws Exception {

		notificationSettingsResource.setContextAcceptLanguage(_acceptLanguage);
		notificationSettingsResource.setContextCompany(_company);
		notificationSettingsResource.setContextHttpServletRequest(
			_httpServletRequest);
		notificationSettingsResource.setContextHttpServletResponse(
			_httpServletResponse);
		notificationSettingsResource.setContextUriInfo(_uriInfo);
		notificationSettingsResource.setContextUser(_user);
		notificationSettingsResource.setGroupLocalService(_groupLocalService);
		notificationSettingsResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			ProfileDocumentListResource profileDocumentListResource)
		throws Exception {

		profileDocumentListResource.setContextAcceptLanguage(_acceptLanguage);
		profileDocumentListResource.setContextCompany(_company);
		profileDocumentListResource.setContextHttpServletRequest(
			_httpServletRequest);
		profileDocumentListResource.setContextHttpServletResponse(
			_httpServletResponse);
		profileDocumentListResource.setContextUriInfo(_uriInfo);
		profileDocumentListResource.setContextUser(_user);
		profileDocumentListResource.setGroupLocalService(_groupLocalService);
		profileDocumentListResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			PushNotificationResource pushNotificationResource)
		throws Exception {

		pushNotificationResource.setContextAcceptLanguage(_acceptLanguage);
		pushNotificationResource.setContextCompany(_company);
		pushNotificationResource.setContextHttpServletRequest(
			_httpServletRequest);
		pushNotificationResource.setContextHttpServletResponse(
			_httpServletResponse);
		pushNotificationResource.setContextUriInfo(_uriInfo);
		pushNotificationResource.setContextUser(_user);
		pushNotificationResource.setGroupLocalService(_groupLocalService);
		pushNotificationResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(StoryResource storyResource)
		throws Exception {

		storyResource.setContextAcceptLanguage(_acceptLanguage);
		storyResource.setContextCompany(_company);
		storyResource.setContextHttpServletRequest(_httpServletRequest);
		storyResource.setContextHttpServletResponse(_httpServletResponse);
		storyResource.setContextUriInfo(_uriInfo);
		storyResource.setContextUser(_user);
		storyResource.setGroupLocalService(_groupLocalService);
		storyResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			TermsnConditionsResource termsnConditionsResource)
		throws Exception {

		termsnConditionsResource.setContextAcceptLanguage(_acceptLanguage);
		termsnConditionsResource.setContextCompany(_company);
		termsnConditionsResource.setContextHttpServletRequest(
			_httpServletRequest);
		termsnConditionsResource.setContextHttpServletResponse(
			_httpServletResponse);
		termsnConditionsResource.setContextUriInfo(_uriInfo);
		termsnConditionsResource.setContextUser(_user);
		termsnConditionsResource.setGroupLocalService(_groupLocalService);
		termsnConditionsResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(WadiCResource wadiCResource)
		throws Exception {

		wadiCResource.setContextAcceptLanguage(_acceptLanguage);
		wadiCResource.setContextCompany(_company);
		wadiCResource.setContextHttpServletRequest(_httpServletRequest);
		wadiCResource.setContextHttpServletResponse(_httpServletResponse);
		wadiCResource.setContextUriInfo(_uriInfo);
		wadiCResource.setContextUser(_user);
		wadiCResource.setGroupLocalService(_groupLocalService);
		wadiCResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(WelcomeResource welcomeResource)
		throws Exception {

		welcomeResource.setContextAcceptLanguage(_acceptLanguage);
		welcomeResource.setContextCompany(_company);
		welcomeResource.setContextHttpServletRequest(_httpServletRequest);
		welcomeResource.setContextHttpServletResponse(_httpServletResponse);
		welcomeResource.setContextUriInfo(_uriInfo);
		welcomeResource.setContextUser(_user);
		welcomeResource.setGroupLocalService(_groupLocalService);
		welcomeResource.setRoleLocalService(_roleLocalService);
	}

	private static ComponentServiceObjects<AppUserResource>
		_appUserResourceComponentServiceObjects;
	private static ComponentServiceObjects<AttractionResource>
		_attractionResourceComponentServiceObjects;
	private static ComponentServiceObjects<BannerImagesResource>
		_bannerImagesResourceComponentServiceObjects;
	private static ComponentServiceObjects<BookingResource>
		_bookingResourceComponentServiceObjects;
	private static ComponentServiceObjects<CommunityWallResource>
		_communityWallResourceComponentServiceObjects;
	private static ComponentServiceObjects<ContactResource>
		_contactResourceComponentServiceObjects;
	private static ComponentServiceObjects<CourtResource>
		_courtResourceComponentServiceObjects;
	private static ComponentServiceObjects<EventListResource>
		_eventListResourceComponentServiceObjects;
	private static ComponentServiceObjects<ExperientialCenterResource>
		_experientialCenterResourceComponentServiceObjects;
	private static ComponentServiceObjects<ExploreResource>
		_exploreResourceComponentServiceObjects;
	private static ComponentServiceObjects<ExploreWadiCResource>
		_exploreWadiCResourceComponentServiceObjects;
	private static ComponentServiceObjects<FaqResource>
		_faqResourceComponentServiceObjects;
	private static ComponentServiceObjects<GlobalSearchResource>
		_globalSearchResourceComponentServiceObjects;
	private static ComponentServiceObjects<HenaCafeResource>
		_henaCafeResourceComponentServiceObjects;
	private static ComponentServiceObjects<HomeResource>
		_homeResourceComponentServiceObjects;
	private static ComponentServiceObjects<MapLocationResource>
		_mapLocationResourceComponentServiceObjects;
	private static ComponentServiceObjects<MiskForumResource>
		_miskForumResourceComponentServiceObjects;
	private static ComponentServiceObjects<MyFavoritesResource>
		_myFavoritesResourceComponentServiceObjects;
	private static ComponentServiceObjects<NotificationSettingsResource>
		_notificationSettingsResourceComponentServiceObjects;
	private static ComponentServiceObjects<ProfileDocumentListResource>
		_profileDocumentListResourceComponentServiceObjects;
	private static ComponentServiceObjects<PushNotificationResource>
		_pushNotificationResourceComponentServiceObjects;
	private static ComponentServiceObjects<StoryResource>
		_storyResourceComponentServiceObjects;
	private static ComponentServiceObjects<TermsnConditionsResource>
		_termsnConditionsResourceComponentServiceObjects;
	private static ComponentServiceObjects<WadiCResource>
		_wadiCResourceComponentServiceObjects;
	private static ComponentServiceObjects<WelcomeResource>
		_welcomeResourceComponentServiceObjects;

	private AcceptLanguage _acceptLanguage;
	private com.liferay.portal.kernel.model.Company _company;
	private BiFunction<Object, String, Filter> _filterBiFunction;
	private GroupLocalService _groupLocalService;
	private HttpServletRequest _httpServletRequest;
	private HttpServletResponse _httpServletResponse;
	private RoleLocalService _roleLocalService;
	private BiFunction<Object, String, Sort[]> _sortsBiFunction;
	private UriInfo _uriInfo;
	private com.liferay.portal.kernel.model.User _user;

}