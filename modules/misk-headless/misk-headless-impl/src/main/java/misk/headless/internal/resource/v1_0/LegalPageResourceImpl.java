package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.PrivacyPolicy;
import misk.headless.resource.v1_0.LegalPageResource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/legal-page.properties",
	scope = ServiceScope.PROTOTYPE, service = LegalPageResource.class
)
public class LegalPageResourceImpl extends BaseLegalPageResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HenaCafeResourceImpl.class);

	@Override
	public PrivacyPolicy getPrivacyPolicy(String languageId)
			throws Exception {
		try {
			PrivacyPolicy results = new PrivacyPolicy();
			String articleid = "APP_PAGE_PRIVACY_POLICY";
			JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(appGroupId, articleid, 0 );
			String articleContent = article.getContentByLocale(languageId);
			Document document = SAXReaderUtil.read(articleContent);

			Node title = document.selectSingleNode("/root/dynamic-element[@name='Title']/dynamic-content");
			results.setTitle(title.getText());
			Node Descriptions = document.selectSingleNode("/root/dynamic-element[@name='Descriptions']/dynamic-content");
			results.setDescription(Descriptions.getText());

			return results;
		} catch (Exception ex) {
			_log.error("Error retrieving privacy policy page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

}