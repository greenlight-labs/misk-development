package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.vulcan.pagination.Page;
import com.liferay.portal.vulcan.pagination.Pagination;
import misk.headless.components.StoryComponent;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.Story;
import misk.headless.resource.v1_0.StoryResource;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;
import stories.service.StoryLocalService;

import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/story.properties",
	scope = ServiceScope.PROTOTYPE, service = StoryResource.class
)
public class StoryResourceImpl extends BaseStoryResourceImpl {
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HomeResourceImpl.class);

	@Override
	public Page<Story> getStories(String languageId, Pagination pagination) throws Exception {

		try {
			int totalCount = StoryComponent.getStoriesCount();

			List<Story> results = StoryComponent.getStories(languageId, pagination, this.contextHttpServletRequest, this.contextUriInfo);

			return Page.of(results, pagination, totalCount);
		} catch (Exception ex) {
			_log.error("Error retrieving stories data: " + ex.getMessage(), ex);
			throw ex;
		}
	}

	@Override
	public Story getStoryDetails(Long storyId, String languageId) throws Exception {
		return StoryComponent.getStory(storyId, languageId, this.contextUriInfo);
	}

	@Reference
	StoryLocalService _storyLocalService;
}