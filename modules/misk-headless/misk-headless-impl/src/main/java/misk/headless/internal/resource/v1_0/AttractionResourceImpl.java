package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.DocumentException;

import java.text.Format;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import attractions.model.Amenities;
import attractions.model.Attractions;
import attractions.model.BookSpace;
import attractions.model.Gallary;
import attractions.service.AmenitiesLocalService;
import attractions.service.AttractionsLocalService;
import attractions.service.BookSpaceLocalService;
import attractions.service.GallaryLocalService;
import events.model.Category;
import events.model.EventsGallery;
import events.service.CategoryLocalServiceUtil;
import events.service.EventService;
import events.service.EventsGalleryLocalServiceUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.AmenitiesList;
import misk.headless.dto.v1_0.Attraction;
import misk.headless.dto.v1_0.BookSpaceList;
import misk.headless.dto.v1_0.EventList;
import misk.headless.dto.v1_0.EventsSection;
import misk.headless.resource.v1_0.AttractionResource;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/attraction.properties", scope = ServiceScope.PROTOTYPE, service = AttractionResource.class)
public class AttractionResourceImpl extends BaseAttractionResourceImpl {
	Log _log = LogFactoryUtil.getLog(ContactResourceImpl.class);
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	// protected String baseURL = "https://webserver-miskcity-dev.lfr.cloud";

	@Override
	public Attraction getAttraction(Long attractionid, String languageId) throws Exception {
		try {
			_log.info(" ---attractionid " + attractionid + " languageId " + languageId);

			return this.getAttractionContent(attractionid, languageId);
		} catch (Exception ex) {
			_log.error("Error retrieving getMiskForum page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new Attraction();
	}

	public Attraction getAttractionContent(Long attractionid, String languageId)
			throws DocumentException, PortalException {
		Attraction attractionWS = new Attraction();
		Attractions attractionDB = _attractionsLocalService.getAttractions(attractionid);
		if (Validator.isNotNull(attractionDB)) {
			attractionWS.setAttractionId(String.valueOf(attractionDB.getAttractionsId()));
			attractionWS.setAttractionBanner(
					ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getForumBannerImage(languageId));
			attractionWS.setAttractionTitle(attractionDB.getForumBannerTitle(languageId));
			attractionWS.setAttractionButtonLabel(attractionDB.getForumButtonLabel(languageId));
			attractionWS.setAttractionBannerIcon(
					ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getForumBannerIcon(languageId));
			attractionWS.setBannerButtonColor(attractionDB.getForumButtonColor(languageId));
			attractionWS.setWorkingHoursLabel(attractionDB.getWorkingHoursLabel(languageId));
			attractionWS.setWorkingHours(attractionDB.getWorkingHours(languageId));
			attractionWS.setWorkingDays(attractionDB.getWorkingDays(languageId));

			attractionWS.setWorkingHoursLabelColor(attractionDB.getWorkingHoursLabelColor(languageId));
			attractionWS.setWorkingHoursImage(
					ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getWorkingHoursImage(languageId));
			attractionWS.setWorkingDaysImage(
					ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getWorkingDaysImage(languageId));
			attractionWS.setContactTelephoneIcon(
					ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getContactTelephoneIcon(languageId));
			attractionWS.setContactEmailAddressIcon(
					ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getContactEmailAddressIcon(languageId));

			attractionWS.setDescription(attractionDB.getDescription(languageId));
			attractionWS.setLocationLabel(attractionDB.getLocationLabel(languageId));
			attractionWS.setLocationLatitude(attractionDB.getLocationLatitude(languageId));
			attractionWS.setLocationLongitude(attractionDB.getLocationLongitude(languageId));
			attractionWS.setContactLabel(attractionDB.getContactLabel(languageId));
			attractionWS.setContactEmailAddress(attractionDB.getContactEmailAddress(languageId));
			attractionWS.setContactTelephone(attractionDB.getContactTelephone(languageId));
			attractionWS.setGalleryLabel(attractionDB.getGallaryLabel(languageId));
			attractionWS.setAmenitiesLabel(attractionDB.getAmenitiesLabel(languageId));
			attractionWS.setBookSpaceLabel(attractionDB.getBookSpaceLabel(languageId));
		}

		// Gallary
		List<Gallary> gallaryList = _gallaryLocalService.findAllInAttractions(attractionid);
		if (Validator.isNotNull(gallaryList) && gallaryList.size() > 0) {
			String[] galleryImageList = new String[gallaryList.size()];
			int i = 0;
			for (Gallary gg : gallaryList) {
				galleryImageList[i] = ApiKeys.getBaseURL(this.contextUriInfo) + gg.getImage(languageId);
				i++;
			}
			attractionWS.setGalleryImagesList(galleryImageList);
		}

		// Amenities

		List<Amenities> amenitiesList = _amenitiesLocalService.findAllInAttractions(attractionid);
		AmenitiesList[] amenitiesImagesList = new AmenitiesList[amenitiesList.size()];

		if (Validator.isNotNull(amenitiesList) && amenitiesList.size() > 0) {
			int i = 0;
			for (Amenities ament : amenitiesList) {
				AmenitiesList amenitiesListObj = new AmenitiesList();
				amenitiesListObj
						.setAmenitiesImages(ApiKeys.getBaseURL(this.contextUriInfo) + ament.getImage(languageId));
				amenitiesListObj.setAmenitiesImagesLabel(ament.getTitle(languageId));
				amenitiesImagesList[i] = amenitiesListObj;
				i++;
			}
			attractionWS.setAmenitiesList(amenitiesImagesList);
		}

		List<BookSpace> bookSpaceList = _bookSpaceLocalService.findAllInAttractions(attractionid);
		BookSpaceList[] bookSList = new BookSpaceList[bookSpaceList.size()];

		if (Validator.isNotNull(bookSpaceList) && bookSpaceList.size() > 0) {

			int i = 0;
			for (BookSpace bkspace : bookSpaceList) {
				BookSpaceList bookSpaceListObj = new BookSpaceList();
				bookSpaceListObj.setBookSpaceImages(
						ApiKeys.getBaseURL(this.contextUriInfo) + bkspace.getBookSpaceImage(languageId));
				bookSpaceListObj
						.setBookSpaceIcon(ApiKeys.getBaseURL(this.contextUriInfo) + bkspace.getIcon(languageId));
				bookSpaceListObj.setBookSpaceTitle(bkspace.getBookSpaceTitle(languageId));
				bookSpaceListObj.setCapacity(bkspace.getCapacity(languageId));
				bookSpaceListObj.setArea(bkspace.getArea(languageId));
				bookSpaceListObj.setDuration(bkspace.getDuration(languageId));
				bookSList[i] = bookSpaceListObj;
				i++;
			}
			attractionWS.setBookSpaceList(bookSList);
		}

		try {
			attractionWS.setEventsSection(getEventSection(languageId, attractionDB.getEventId()));
		} catch (Exception e) {
			_log.error("error while getting event list");
		}
		return attractionWS;
	}

	private EventList[] getEvents(String languageId, String eventType) {
		int typeEvent = 1;// default
		if (Validator.isNotNull(eventType) && Validator.isNumber(eventType)) {
			typeEvent = Integer.parseInt(eventType);
		}
		List<events.model.Event> records = _eventService.getEvents(this.webGroupId, 0, 9);

		EventList[] results = new EventList[records.size()];
		int i = 0;

		for (events.model.Event curRecord : records) {
			EventList result = new EventList();

			List<EventsGallery> eventg = EventsGalleryLocalServiceUtil.findAllInDiscover(curRecord.getEventId());

			String imagearray[] = new String[eventg.size()];
			for (int j = 0; j < eventg.size(); j++) {
				imagearray[j] = ApiKeys.getBaseURL(this.contextUriInfo) + eventg.get(j).getImage(languageId);
			}

			result.setEventId(curRecord.getEventId());
			result.setTitle(curRecord.getTitle(languageId));
			String paytype = "Free";
			if (curRecord.getPaytype() == 2) {
				paytype = "Paid";
			}
			result.setPayType(paytype);
			result.setEventDescription(curRecord.getEventdescription(languageId));
			result.setTime(curRecord.getTiming(languageId));
			result.setVenue(curRecord.getVenue(languageId));
			Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("dd MMMM");

			result.setDisplaydate(String.valueOf(curRecord.getDisplayDate()));
			result.setEventMonth(dateTimeFormat.format(curRecord.getDisplayDate()));
			result.setIsFavourite(Boolean.TRUE);
			result.setEventFeaturedImage(
					ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getEventfeaturedImage(languageId));
			Category cat = CategoryLocalServiceUtil.fetchCategory(curRecord.getCategoryId());
			result.setCatColor(cat.getCatColor());
			result.setEventcategory(cat.getName(languageId));
			result.setEventType(String.valueOf(curRecord.getType()));
			result.setSliderimage(imagearray);
			results[i] = result;
			i++;
		}
		return results;
	}

	private EventsSection getEventSection(String languageId, String eventType) throws Exception {
		EventsSection eventsection = new EventsSection();
		eventsection.setItems(this.getEvents(languageId, eventType));

		return eventsection;
	}

	@Reference
	private EventService _eventService;

	@Reference
	private AttractionsLocalService _attractionsLocalService;

	@Reference
	GallaryLocalService _gallaryLocalService;

	@Reference
	AmenitiesLocalService _amenitiesLocalService;

	@Reference
	BookSpaceLocalService _bookSpaceLocalService;
}