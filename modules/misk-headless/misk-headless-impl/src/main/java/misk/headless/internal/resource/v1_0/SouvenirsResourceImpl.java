package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import java.util.ArrayList;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.dto.v1_0.Souvenirs;
import misk.headless.resource.v1_0.SouvenirsResource;
import misk.headless.util.SouvenirsUtil;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/souvenirs.properties", scope = ServiceScope.PROTOTYPE, service = SouvenirsResource.class)
public class SouvenirsResourceImpl extends BaseSouvenirsResourceImpl {
	Log _log = LogFactoryUtil.getLog(SouvenirsResourceImpl.class);

	@Override
	public Response getSouvenirs(Souvenirs souvenirs) throws Exception {
		if (Validator.isNull(souvenirs) || souvenirs.getEmail().isEmpty()
				|| !Validator.isEmailAddress(souvenirs.getEmail())) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-valid"));
		}

		ArrayList<Object> souvenirsList = new ArrayList<>();

		_log.info("getSouvenirs " + souvenirs.toString());
		Souvenirs[] souvenirList = SouvenirsUtil.getSouvenirs(souvenirs.getEmail(), souvenirs);
		if (Validator.isNotNull(souvenirList) && souvenirList.length > 0) {
			for (Souvenirs slist : souvenirList) {
				souvenirsList.add(slist);
			}
		}

		return Response.ok(souvenirsList).build();
	}
}