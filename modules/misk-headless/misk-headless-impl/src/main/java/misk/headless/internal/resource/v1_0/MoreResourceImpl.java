package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.MoreAboutCity;
import misk.headless.resource.v1_0.MoreResource;

import misk.headless.util.CommonUtil;
import misk.headless.util.DocumentMediaUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/more.properties",
	scope = ServiceScope.PROTOTYPE, service = MoreResource.class
)
public class MoreResourceImpl extends BaseMoreResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HenaCafeResourceImpl.class);

	@Override
	public MoreAboutCity getAboutCity(String languageId) throws Exception {
		try {
			String articleId = "APP_PAGE_MORE_MENU_BANNER_SECTION";
			long groupId = this.appGroupId;
			MoreAboutCity result = new MoreAboutCity();
			JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0 );
			String articleContent = article.getContentByLocale(languageId);
			Document document = SAXReaderUtil.read(articleContent);

			Node topHeading = document.selectSingleNode("/root/dynamic-element[@name='TopHeading']/dynamic-content");
			result.setTopHeading(topHeading.getText());
			Node title = document.selectSingleNode("/root/dynamic-element[@name='Title']/dynamic-content");
			result.setTitle(title.getText());
			Node subtitle = document.selectSingleNode("/root/dynamic-element[@name='Subtitle']/dynamic-content");
			result.setSubtitle(subtitle.getText());
			Node description = document.selectSingleNode("/root/dynamic-element[@name='Description']/dynamic-content");
			result.setDescription(description.getText());
			Node buttonLabel = document.selectSingleNode("/root/dynamic-element[@name='ButtonLabel']/dynamic-content");
			result.setButtonLabel(buttonLabel.getText());
			Node image = document.selectSingleNode("/root/dynamic-element[@name='Image']/dynamic-content");
			DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
			result.setImage(CommonUtil.getMediaUrl(_documentMediaUtil.getImageURL(image.getText()), this.contextUriInfo));

			return result;
		} catch (Exception ex) {
			_log.error("Error retrieving more menu banner section data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}
}