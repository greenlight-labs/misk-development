package misk.headless.social.login;

public class AppleKeyDTO {
	public String kty;
    public String kid;
    public String sig;
    public String alg;
    public String n;
    public String e;

    public String getKty() {
        return kty;
    }
    public void setKty(String kty) {
        this.kty = kty;
    }

    public String getKid() {
        return kid;
    }
    public void setKid(String kid) {
        this.kid = kid;
    }

    public String getSig() {
        return sig;
    }
    public void setSig(String sig) {
        this.sig = sig;
    }

    public String getAlg() {
        return alg;
    }
    public void setAlg(String alg) {
        this.alg = alg;
    }

    public String getN() {
        return n;
    }
    public void setN(String n) {
        this.n = n;
    }

    public String getE() {
        return e;
    }
    public void setE(String e) {
        this.e = e;
    }

}





