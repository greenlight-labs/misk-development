package misk.headless.internal.resource.v1_0;

import com.fasterxml.jackson.core.JsonParser;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.HenaCafeResource;

import misk.headless.util.DocumentMediaUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/hena-cafe.properties",
	scope = ServiceScope.PROTOTYPE, service = HenaCafeResource.class
)
public class HenaCafeResourceImpl extends BaseHenaCafeResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HenaCafeResourceImpl.class);

	@Override
	public HenaCafe getHenaCafe(String languageId)
			throws Exception {
		try {

			return this.getHenacontent(languageId);
		} catch (Exception ex) {
			_log.error("Error retrieving Hencafe page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	public HenaCafe getHenacontent(String languageId) throws Exception{

		HenaCafe results = new HenaCafe();
		long groupId = this.appGroupId;
		String articleId = "92125";
		int i;
		int j=0;
		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0 );
		String articleContent = article.getContentByLocale(languageId);

		Document document = SAXReaderUtil.read(articleContent);
		Node titlec= document.selectSingleNode("/root/dynamic-element[@name='Textmc2a']/dynamic-content");
		results.setTitle(titlec.getText());
		Node subtitle= document.selectSingleNode("/root/dynamic-element[@name='Subtitle']/dynamic-content");
		results.setSubtitle(subtitle.getText());
		Node location= document.selectSingleNode("/root/dynamic-element[@name='Location']/dynamic-content");
		results.setLocation(location.getText());
		Node WorkingHours= document.selectSingleNode("/root/dynamic-element[@name='WorkingHours']/dynamic-content");
		results.setWorkinghours(WorkingHours.getText());
		Node WorkingDays= document.selectSingleNode("/root/dynamic-element[@name='WorkingDays']/dynamic-content");
		results.setWorkingdays(WorkingDays.getText());
		Node WorkingSummary= document.selectSingleNode("/root/dynamic-element[@name='WorkingSummary']/dynamic-content");
		results.setWorkingsummary(WorkingSummary.getText());
		List<Node> nodes  = document.selectNodes("/root/dynamic-element[@name='SliderImage']/dynamic-content");
		DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
		String imagearray[] = new String[nodes.size()];
		for (i = 0; i < nodes.size(); i++){
			imagearray[i] = ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(nodes.get(i).getText());
		}
		results.setSliderimage(imagearray);

		Node latitude = document.selectSingleNode("/root/dynamic-element[@name='Latitude']/dynamic-content");
		results.setLatitude(latitude.getText());
		Node longitude = document.selectSingleNode("/root/dynamic-element[@name='Longitude']/dynamic-content");
		results.setLongitude(longitude.getText());

		List<Node> categoryNodes = document.selectNodes("/root/dynamic-element[@name='ItemTitle']");
		Category catresults[] = new Category[categoryNodes.size()];

		for (Node node: categoryNodes) {
			Node titleNode = node.selectSingleNode("dynamic-content");
			Node category = node.selectSingleNode("dynamic-element[@name='Category']/dynamic-content");
			Node ItemSummary = node.selectSingleNode("dynamic-element[@name='ItemSummary']/dynamic-content");
			Node ItemFrom = node.selectSingleNode("dynamic-element[@name='ItemFrom']/dynamic-content");
			Node Itemimage = node.selectSingleNode("dynamic-element[@name='Image1g6q']/dynamic-content");
			//Node imageNode = node.selectSingleNode("dynamic-element[@name='Image1cxa']/dynamic-content");

			Category catresult = new Category();
			catresult.setItemTitle(titleNode.getText());
			catresult.setItemCategory(category.getText());
			catresult.setItemSummary(ItemSummary.getText());
			catresult.setItemPrice(ItemFrom.getText());
			catresult.setItemImage(ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(Itemimage.getText()));
			catresults[j]=catresult;
			j++;
		}
		results.setCategories(catresults);

		return results;
	}


}