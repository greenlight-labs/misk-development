package misk.headless.internal.resource.v1_0;

import bookings.service.BookingLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.batch.engine.resource.VulcanBatchEngineImportTaskResource;
import com.liferay.portal.vulcan.pagination.Page;
import com.liferay.portal.vulcan.pagination.Pagination;
import courts.model.Court;
import courts.service.CategoryLocalService;
import courts.service.CourtLocalService;
import courts.service.LocationLocalService;
import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.Booking;
import misk.headless.dto.v1_0.BookingSync;
import misk.headless.dto.v1_0.RequestStatus;
import misk.headless.resource.v1_0.InvixiumResource;

import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/invixium.properties",
	scope = ServiceScope.PROTOTYPE, service = InvixiumResource.class
)
public class InvixiumResourceImpl extends BaseInvixiumResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HomeResourceImpl.class);

	@Override
	public Page<Booking> getCourtBookings(Boolean allRecords, Boolean sync, Pagination pagination) throws Exception {

		try {
			//English language is fixed for Invixium booking APIs
			String languageId = "en_US";

			/*if(Validator.isNull(allRecords) || Validator.isNull(sync)){
				throw new BadRequestException("allRecords and sync parameters are required.");
			}*/

			int totalCount = 0;
			List<bookings.model.Booking> records;
			if(allRecords){
				totalCount = _bookingLocalService.getBookingsCount();
				records = _bookingLocalService.getBookings(pagination.getStartPosition(), pagination.getEndPosition());
			}else if(sync) {
				totalCount = _bookingLocalService.countBySync(true);
				records = _bookingLocalService.findBySync(true, pagination.getStartPosition(), pagination.getEndPosition());
			}else{
				totalCount = _bookingLocalService.countBySync(false);
				records = _bookingLocalService.findBySync(false, pagination.getStartPosition(), pagination.getEndPosition());
			}
			List<Booking> results = new ArrayList<>();
			for (bookings.model.Booking curRecord : records) {
				//@MY_TODO: fix multiple sql queries in loop, create custom-sql finder method
				courts.model.Court court = _courtLocalService.fetchCourt(curRecord.getCourtId());
				courts.model.Category category = _categoryLocalService.fetchCategory(court.getCategoryId());
				courts.model.Location location = _locationLocalService.fetchLocation(category.getLocationId());
				AppUser appUser = _appUserLocalService.fetchAppUser(curRecord.getAppUserId());

				Booking result = new Booking();
				result.setBookingId(curRecord.getBookingId());
				result.setCourtId(curRecord.getCourtId());
				result.setCourtName(court.getName(languageId));
				result.setCourtLocation(location.getName(languageId));
				result.setAppUserId(curRecord.getAppUserId());
				result.setAppUserName(appUser.getFullName());
				result.setAppUserEmail(appUser.getEmailAddress());
				result.setSlotStartTime(CommonUtil.convertDateTimeToStringTime(curRecord.getSlotStartTime()));
				result.setSlotEndTime(CommonUtil.convertDateTimeToStringTime(curRecord.getSlotEndTime()));
				result.setBookingDate(CommonUtil.convertDateTimeToStringDate(curRecord.getSlotStartTime()));
				result.setCourtLocation(location.getName(languageId));
				results.add(result);
			}

			return Page.of(results, pagination, totalCount);
		} catch (Exception ex) {
			_log.error("Error retrieving booking details data: " + ex.getMessage(), ex);
			//_log.error(ex.getMessage());
			//throw ex;
			throw new BadRequestException("Unable to fetch the booking records, Please try again later.");
		}
	}

	@Override
	public RequestStatus updateBookingSyncStatus(BookingSync[] bookingSyncs)
			throws Exception {

		try{
			for (BookingSync curBooking: bookingSyncs) {

				bookings.model.Booking booking = _bookingLocalService.fetchBooking(curBooking.getBookingId());
				booking.setSync(true);
				_bookingLocalService.updateBooking(booking);
			}

			RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatus(true);
			requestStatus.setMessage("Records synced status have been updated successfully.");
			return requestStatus;

		} catch (Exception ex) {
			_log.error("Error updating sync status for bookings: " + ex.getMessage(), ex);
			//throw ex;
			RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatus(false);
			requestStatus.setMessage("Unable to update the sync status for court bookings, Records will be synced on next call.");
			return requestStatus;
		}
	}

	@Reference
	protected BookingLocalService _bookingLocalService;

	@Reference
	protected CourtLocalService _courtLocalService;

	@Reference
	private CategoryLocalService _categoryLocalService;

	@Reference
	private LocationLocalService _locationLocalService;

	@Reference
	private AppUserLocalService _appUserLocalService;
}
