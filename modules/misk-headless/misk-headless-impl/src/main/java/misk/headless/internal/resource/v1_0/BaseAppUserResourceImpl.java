package misk.headless.internal.resource.v1_0;

import com.liferay.petra.function.UnsafeFunction;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.GroupedModel;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.filter.Filter;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.ResourceActionLocalService;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.odata.filter.ExpressionConvert;
import com.liferay.portal.odata.filter.FilterParser;
import com.liferay.portal.odata.filter.FilterParserProvider;
import com.liferay.portal.vulcan.accept.language.AcceptLanguage;
import com.liferay.portal.vulcan.batch.engine.VulcanBatchEngineTaskItemDelegate;
import com.liferay.portal.vulcan.batch.engine.resource.VulcanBatchEngineImportTaskResource;
import com.liferay.portal.vulcan.multipart.MultipartBody;
import com.liferay.portal.vulcan.pagination.Page;
import com.liferay.portal.vulcan.pagination.Pagination;
import com.liferay.portal.vulcan.resource.EntityModelResource;
import com.liferay.portal.vulcan.util.ActionUtil;
import com.liferay.portal.vulcan.util.TransformUtil;

import java.io.Serializable;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Generated;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import misk.headless.dto.v1_0.AppUser;
import misk.headless.dto.v1_0.DeleteProfileImage;
import misk.headless.resource.v1_0.AppUserResource;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
@javax.ws.rs.Path("/v1.0")
public abstract class BaseAppUserResourceImpl
	implements AppUserResource, EntityModelResource,
			   VulcanBatchEngineTaskItemDelegate<AppUser> {

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'GET' 'http://localhost:8080/o/misk-headless/v1.0/ProfileDetails/{profileid}'  -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.Operation(
		description = "Retrieves the Profile Details."
	)
	@io.swagger.v3.oas.annotations.Parameters(
		value = {
			@io.swagger.v3.oas.annotations.Parameter(
				in = io.swagger.v3.oas.annotations.enums.ParameterIn.PATH,
				name = "profileid"
			),
			@io.swagger.v3.oas.annotations.Parameter(
				in = io.swagger.v3.oas.annotations.enums.ParameterIn.QUERY,
				name = "languageId"
			)
		}
	)
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.GET
	@javax.ws.rs.Path("/ProfileDetails/{profileid}")
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser getProfileDetails(
			@io.swagger.v3.oas.annotations.Parameter(hidden = true)
			@javax.validation.constraints.NotNull
			@javax.ws.rs.PathParam("profileid")
			Long profileid,
			@io.swagger.v3.oas.annotations.Parameter(hidden = true)
			@javax.validation.constraints.NotNull
			@javax.ws.rs.QueryParam("languageId")
			String languageId)
		throws Exception {

		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/register' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/register")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser postRegister(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/register/update' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/register/update")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser updateRegister(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/register/verify-otp' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/register/verify-otp")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser verifyRegistrationOTP(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/register/activate/verified-otp' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/register/activate/verified-otp")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser activateVerifiedMobileOTPUser(AppUser appUser)
		throws Exception {

		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/editProfile'  -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes("multipart/form-data")
	@javax.ws.rs.Path("/editProfile")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser editUserProfile(MultipartBody multipartBody)
		throws Exception {

		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/updateProfileBannerImage'  -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes("multipart/form-data")
	@javax.ws.rs.Path("/updateProfileBannerImage")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser updateUserProfileBannerImage(MultipartBody multipartBody)
		throws Exception {

		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/delete-profile-image' -d $'{"appUserId": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/delete-profile-image")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser deleteUserProfileImage(DeleteProfileImage deleteProfileImage)
		throws Exception {

		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/login' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/login")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser postLogin(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/social-login' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/social-login")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser postSocialLogin(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/reset-password' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/reset-password")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser resetPassword(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/change-password' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/change-password")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser changePassword(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/check-password' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/check-password")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser checkPassword(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/forgot-password' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/forgot-password")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser forgotPassword(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/forgot-password-phone' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/forgot-password-phone")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser forgotPasswordPhone(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/set-email-address-verified' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/set-email-address-verified")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser setEmailAddressVerified(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/set-phone-number-verified' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/set-phone-number-verified")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser setPhoneNumberVerified(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/account-details-update' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/account-details-update")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser updateUserAccountDetails(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/resend-otp' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/resend-otp")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser resendOTP(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/verify-otp' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/verify-otp")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser verifyOTP(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'POST' 'http://localhost:8080/o/misk-headless/v1.0/user/add-device' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.Path("/user/add-device")
	@javax.ws.rs.POST
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser addDevice(AppUser appUser) throws Exception {
		return new AppUser();
	}

	/**
	 * Invoke this method with the command line:
	 *
	 * curl -X 'DELETE' 'http://localhost:8080/o/misk-headless/v1.0/delete-account' -d $'{"androidDeviceToken": ___, "appUserId": ___, "appleUserId": ___, "autoLogin": ___, "confirmPassword": ___, "deactivateReason": ___, "deleteReason": ___, "emailAddress": ___, "emailAddressVerified": ___, "facebookId": ___, "fullName": ___, "gcpToken": ___, "googleUserId": ___, "iosDeviceToken": ___, "isVerified": ___, "languageId": ___, "newPassword": ___, "passcode": ___, "password": ___, "passwordResetToken": ___, "phoneNumber": ___, "phoneNumberVerified": ___, "profileBannerImage": ___, "profileImage": ___, "requestStatus": ___}' --header 'Content-Type: application/json' -u 'test@liferay.com:test'
	 */
	@io.swagger.v3.oas.annotations.tags.Tags(
		value = {@io.swagger.v3.oas.annotations.tags.Tag(name = "AppUser")}
	)
	@javax.ws.rs.Consumes({"application/json", "application/xml"})
	@javax.ws.rs.DELETE
	@javax.ws.rs.Path("/delete-account")
	@javax.ws.rs.Produces({"application/json", "application/xml"})
	@Override
	public AppUser deleteAccount(AppUser appUser) throws Exception {
		return new AppUser();
	}

	@Override
	@SuppressWarnings("PMD.UnusedLocalVariable")
	public void create(
			java.util.Collection<AppUser> appUsers,
			Map<String, Serializable> parameters)
		throws Exception {
	}

	@Override
	public void delete(
			java.util.Collection<AppUser> appUsers,
			Map<String, Serializable> parameters)
		throws Exception {
	}

	@Override
	public EntityModel getEntityModel(Map<String, List<String>> multivaluedMap)
		throws Exception {

		return getEntityModel(
			new MultivaluedHashMap<String, Object>(multivaluedMap));
	}

	@Override
	public EntityModel getEntityModel(MultivaluedMap multivaluedMap)
		throws Exception {

		return null;
	}

	@Override
	public Page<AppUser> read(
			Filter filter, Pagination pagination, Sort[] sorts,
			Map<String, Serializable> parameters, String search)
		throws Exception {

		return null;
	}

	@Override
	public void setLanguageId(String languageId) {
		this.contextAcceptLanguage = new AcceptLanguage() {

			@Override
			public List<Locale> getLocales() {
				return null;
			}

			@Override
			public String getPreferredLanguageId() {
				return languageId;
			}

			@Override
			public Locale getPreferredLocale() {
				return LocaleUtil.fromLanguageId(languageId);
			}

		};
	}

	@Override
	public void update(
			java.util.Collection<AppUser> appUsers,
			Map<String, Serializable> parameters)
		throws Exception {
	}

	public void setContextAcceptLanguage(AcceptLanguage contextAcceptLanguage) {
		this.contextAcceptLanguage = contextAcceptLanguage;
	}

	public void setContextCompany(
		com.liferay.portal.kernel.model.Company contextCompany) {

		this.contextCompany = contextCompany;
	}

	public void setContextHttpServletRequest(
		HttpServletRequest contextHttpServletRequest) {

		this.contextHttpServletRequest = contextHttpServletRequest;
	}

	public void setContextHttpServletResponse(
		HttpServletResponse contextHttpServletResponse) {

		this.contextHttpServletResponse = contextHttpServletResponse;
	}

	public void setContextUriInfo(UriInfo contextUriInfo) {
		this.contextUriInfo = contextUriInfo;
	}

	public void setContextUser(
		com.liferay.portal.kernel.model.User contextUser) {

		this.contextUser = contextUser;
	}

	public void setExpressionConvert(
		ExpressionConvert<Filter> expressionConvert) {

		this.expressionConvert = expressionConvert;
	}

	public void setFilterParserProvider(
		FilterParserProvider filterParserProvider) {

		this.filterParserProvider = filterParserProvider;
	}

	public void setGroupLocalService(GroupLocalService groupLocalService) {
		this.groupLocalService = groupLocalService;
	}

	public void setResourceActionLocalService(
		ResourceActionLocalService resourceActionLocalService) {

		this.resourceActionLocalService = resourceActionLocalService;
	}

	public void setResourcePermissionLocalService(
		ResourcePermissionLocalService resourcePermissionLocalService) {

		this.resourcePermissionLocalService = resourcePermissionLocalService;
	}

	public void setRoleLocalService(RoleLocalService roleLocalService) {
		this.roleLocalService = roleLocalService;
	}

	@Override
	public Filter toFilter(
		String filterString, Map<String, List<String>> multivaluedMap) {

		try {
			EntityModel entityModel = getEntityModel(multivaluedMap);

			FilterParser filterParser = filterParserProvider.provide(
				entityModel);

			com.liferay.portal.odata.filter.Filter oDataFilter =
				new com.liferay.portal.odata.filter.Filter(
					filterParser.parse(filterString));

			return expressionConvert.convert(
				oDataFilter.getExpression(),
				contextAcceptLanguage.getPreferredLocale(), entityModel);
		}
		catch (Exception exception) {
			if (_log.isDebugEnabled()) {
				_log.debug("Invalid filter " + filterString, exception);
			}
		}

		return null;
	}

	protected Map<String, String> addAction(
		String actionName, GroupedModel groupedModel, String methodName) {

		return ActionUtil.addAction(
			actionName, getClass(), groupedModel, methodName,
			contextScopeChecker, contextUriInfo);
	}

	protected Map<String, String> addAction(
		String actionName, Long id, String methodName, Long ownerId,
		String permissionName, Long siteId) {

		return ActionUtil.addAction(
			actionName, getClass(), id, methodName, contextScopeChecker,
			ownerId, permissionName, siteId, contextUriInfo);
	}

	protected Map<String, String> addAction(
		String actionName, Long id, String methodName,
		ModelResourcePermission modelResourcePermission) {

		return ActionUtil.addAction(
			actionName, getClass(), id, methodName, contextScopeChecker,
			modelResourcePermission, contextUriInfo);
	}

	protected Map<String, String> addAction(
		String actionName, String methodName, String permissionName,
		Long siteId) {

		return addAction(
			actionName, siteId, methodName, null, permissionName, siteId);
	}

	protected <T, R> List<R> transform(
		java.util.Collection<T> collection,
		UnsafeFunction<T, R, Exception> unsafeFunction) {

		return TransformUtil.transform(collection, unsafeFunction);
	}

	protected <T, R> R[] transform(
		T[] array, UnsafeFunction<T, R, Exception> unsafeFunction,
		Class<?> clazz) {

		return TransformUtil.transform(array, unsafeFunction, clazz);
	}

	protected <T, R> R[] transformToArray(
		java.util.Collection<T> collection,
		UnsafeFunction<T, R, Exception> unsafeFunction, Class<?> clazz) {

		return TransformUtil.transformToArray(
			collection, unsafeFunction, clazz);
	}

	protected <T, R> List<R> transformToList(
		T[] array, UnsafeFunction<T, R, Exception> unsafeFunction) {

		return TransformUtil.transformToList(array, unsafeFunction);
	}

	protected AcceptLanguage contextAcceptLanguage;
	protected com.liferay.portal.kernel.model.Company contextCompany;
	protected HttpServletRequest contextHttpServletRequest;
	protected HttpServletResponse contextHttpServletResponse;
	protected Object contextScopeChecker;
	protected UriInfo contextUriInfo;
	protected com.liferay.portal.kernel.model.User contextUser;
	protected ExpressionConvert<Filter> expressionConvert;
	protected FilterParserProvider filterParserProvider;
	protected GroupLocalService groupLocalService;
	protected ResourceActionLocalService resourceActionLocalService;
	protected ResourcePermissionLocalService resourcePermissionLocalService;
	protected RoleLocalService roleLocalService;
	protected VulcanBatchEngineImportTaskResource
		vulcanBatchEngineImportTaskResource;

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseAppUserResourceImpl.class);

}