package misk.headless.internal.resource.v1_0;

import bookings.service.BookingLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.pagination.Page;
import courts.service.CategoryLocalService;
import courts.service.CourtLocalService;
import courts.service.LocationLocalService;
import misk.app.users.service.AppUserLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.AppUser;
import misk.headless.dto.v1_0.Booking;
import misk.headless.dto.v1_0.Court;
import misk.headless.dto.v1_0.RequestStatus;
import misk.headless.resource.v1_0.ProfileResource;

import misk.headless.util.AccountUtil;
import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/profile.properties",
	scope = ServiceScope.PROTOTYPE, service = ProfileResource.class
)
public class ProfileResourceImpl extends BaseProfileResourceImpl {

	Log _log = LogFactoryUtil.getLog(AppUserResourceImpl.class);

	@Override
	public Page<Booking> getMyBookings(String languageId, Long appUserId)
			throws Exception {

		try {
			List<bookings.model.Booking> records = _bookingLocalService.findAllInAppUser(appUserId);
			List<Booking> results = new ArrayList<>();

			for (bookings.model.Booking curRecord : records) {
				//@MY_TODO: fix multiple sql queries in loop, create custom-sql finder method
				courts.model.Court court = _courtLocalService.fetchCourt(curRecord.getCourtId());
				Court bookingCourt = getBookingCourt(languageId, court);

				Booking result = new Booking();
				result.setBookingId(curRecord.getBookingId());
				result.setCourtId(curRecord.getCourtId());
				result.setCourt(bookingCourt);
				result.setAppUserId(curRecord.getAppUserId());
				result.setSlotStartTime(CommonUtil.convertDateTimeToStringTime(curRecord.getSlotStartTime()));
				result.setSlotEndTime(CommonUtil.convertDateTimeToStringTime(curRecord.getSlotEndTime()));
				result.setBookingDate(CommonUtil.convertDateTimeToStringDate(curRecord.getSlotStartTime()));
				results.add(result);
			}

			return Page.of(results);
		} catch (Exception ex) {
			_log.error("Error retrieving related courts data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	protected Court getBookingCourt(String languageId, courts.model.Court curRecord){

		//@MY_TODO: create dynamic slider at backend and fetch images from there
		String[] images = {
				ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage(),
				ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage(),
				ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage()};

		Court court = new Court();
		court.setId(curRecord.getCourtId());
		court.setCategoryId(curRecord.getCategoryId());
		court.setName(curRecord.getName(languageId));
		court.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getListingImage());
		court.setDetailImages(images);
		court.setVenue(curRecord.getVenue(languageId));
		court.setOpeningHours(curRecord.getOpeningHours(languageId));
		court.setOpeningDays(curRecord.getOpeningDays(languageId));
		court.setDescription(curRecord.getDescription(languageId));
		court.setLat(curRecord.getLat());
		court.setLon(curRecord.getLon());
		court.setContactEmail(curRecord.getContactEmail());
		court.setContactPhone(curRecord.getContactPhone());
		return court;
	}

	@Override
	public AppUser checkValidChangePhoneRequest(AppUser appUser) throws Exception {

		if (Validator.isNull(appUser.getAppUserId()) || Validator.isNull(appUser.getPhoneNumber())) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "change-phone-request-fields-required"));
		}

		RequestStatus requestStatus = new RequestStatus();
		misk.app.users.model.AppUser userEntry = _appUserLocalService.fetchAppUser(appUser.getAppUserId());

		if (Validator.isNotNull(userEntry)) {

			// check phone has not been linked with another user
			AccountUtil.isValidChangePhoneRequest(userEntry, appUser.getPhoneNumber(), contextHttpServletRequest);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "change-phone-request-success"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
		}

		return toAppUser(userEntry, requestStatus);
	}

	private static @GraphQLField AppUser toAppUser(misk.app.users.model.AppUser appUser, RequestStatus requestStatus) {
		RequestStatus newRequestStatus = requestStatus;
		return new AppUser() {
			{
				appUserId = appUser.getAppUserId();
				fullName = appUser.getFullName();
				emailAddress = appUser.getEmailAddress();
				phoneNumber = appUser.getPhoneNumber();
				languageId = appUser.getLanguageId();
				requestStatus = newRequestStatus;
				isVerified = appUser.getStatus();
				emailAddressVerified = appUser.getEmailAddressVerified();
				phoneNumberVerified = appUser.getPhoneNumberVerified();
			}
		};
	}

	@Reference
	protected LocationLocalService _locationLocalService;

	@Reference
	protected CategoryLocalService _categoryLocalService;

	@Reference
	protected CourtLocalService _courtLocalService;

	@Reference
	protected BookingLocalService _bookingLocalService;

	@Reference
	private AppUserLocalService _appUserLocalService;
}