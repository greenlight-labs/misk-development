package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.vulcan.pagination.Page;
import courts.model.Court;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.ExploreWadiC;
import misk.headless.resource.v1_0.ExploreWadiCResource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/explore-wadi-c.properties",
	scope = ServiceScope.PROTOTYPE, service = ExploreWadiCResource.class
)
public class ExploreWadiCResourceImpl extends BaseExploreWadiCResourceImpl {



	@Override
	public Page<ExploreWadiC> getExploreWadiC(String languageId) throws Exception {

		//@MY_TODO: create admin module, fetch dynamic items, fix pagination
		try {
			List<misk.headless.dto.v1_0.ExploreWadiC> results = new ArrayList<>();

			String[] galleryImages = {
					ApiKeys.getBaseURL(this.contextUriInfo) + "/documents/91702/97402/wadi-c-banner.png",
					ApiKeys.getBaseURL(this.contextUriInfo) + "/documents/91702/97402/wadi-c-banner.png",
					ApiKeys.getBaseURL(this.contextUriInfo) + "/documents/91702/97402/wadi-c-banner.png"
			};

			misk.headless.dto.v1_0.ExploreWadiC result1 = new misk.headless.dto.v1_0.ExploreWadiC();
			result1.setId(1L);
			result1.setName("Explore WADI C");
			result1.setDescription("Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.");
			result1.setEstimatedTourTimeLabel("ESTIMATED TOUR TIME:");
			result1.setEstimatedTourTime("2 HRS");
			result1.setType("image");
			result1.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + "/documents/91702/97402/wadi-c-banner.png");
			results.add(result1);

			misk.headless.dto.v1_0.ExploreWadiC result2 = new misk.headless.dto.v1_0.ExploreWadiC();
			result2.setId(2L);
			result2.setName("Welcome to WADI C");
			result2.setDescription("Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.");
			result2.setEstimatedTourTimeLabel("ESTIMATED TOUR TIME:");
			result2.setEstimatedTourTime("30 MINS");
			result2.setType("gallery");
			result2.setGalleryImages(galleryImages);
			results.add(result2);

			misk.headless.dto.v1_0.ExploreWadiC result3 = new misk.headless.dto.v1_0.ExploreWadiC();
			result3.setId(3L);
			result3.setName("Night At WADI C");
			result3.setDescription("Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.");
			result3.setEstimatedTourTimeLabel("ESTIMATED TOUR TIME:");
			result3.setEstimatedTourTime("30 MINS");
			result3.setType("video");
			result3.setVideo(ApiKeys.getBaseURL(this.contextUriInfo) + "/documents/91702/0/dubai-palm-island.mp4");
			results.add(result3);

			return Page.of(results);
		} catch (Exception ex) {
			_log.error("Error retrieving related courts data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	Log _log = LogFactoryUtil.getLog(ExploreWadiCResourceImpl.class);
}