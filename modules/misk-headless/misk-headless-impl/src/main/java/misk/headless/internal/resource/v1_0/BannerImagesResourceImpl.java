package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.BannerImages;
import misk.headless.resource.v1_0.BannerImagesResource;
import misk.headless.util.DocumentMediaUtil;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/banner-images.properties", scope = ServiceScope.PROTOTYPE, service = BannerImagesResource.class)
public class BannerImagesResourceImpl extends BaseBannerImagesResourceImpl {
	Log _log = LogFactoryUtil.getLog(BannerImagesResourceImpl.class);
	long groupId;
	long groupIdMainWebsite;

	public BannerImagesResourceImpl() {
		ApiKeys apiKeys = new ApiKeys();
		this.groupIdMainWebsite = apiKeys.webGroupId;
		this.groupId = apiKeys.appGroupId;
	}

	public BannerImages getBannerImages() throws Exception {

		return getBannerImagesList();
	}

	public BannerImages getBannerImagesList() throws Exception {

		BannerImages bannerImagesObj = new BannerImages();
		long groupId = this.groupId;

		String articleId = "127438";
		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0);
		String articleContent = article.getContent();
		Document document = SAXReaderUtil.read(articleContent);
		Node root = document.selectSingleNode("/root");
		Node souvenirsImageBanner = root
				.selectSingleNode("dynamic-element[@name='souvenirsImageBanner']/dynamic-content");

		Node favoritesBannerImage = root
				.selectSingleNode("dynamic-element[@name='favoritesBannerImage']/dynamic-content");
		DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
		bannerImagesObj.setFavoritesBannerImage(ApiKeys.getBaseURL(this.contextUriInfo)
				+ _documentMediaUtil.getImageURL(favoritesBannerImage.getText()));
		bannerImagesObj.setSouvenirsImageBanner(ApiKeys.getBaseURL(this.contextUriInfo)
				+ _documentMediaUtil.getImageURL(souvenirsImageBanner.getText()));
		return bannerImagesObj;

	}
}
