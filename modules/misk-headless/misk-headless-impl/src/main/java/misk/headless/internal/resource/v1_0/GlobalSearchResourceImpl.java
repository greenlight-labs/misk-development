package misk.headless.internal.resource.v1_0;

import attractions.model.Attractions;
import attractions.service.AttractionsLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.search.SearchContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import com.liferay.portal.kernel.util.Validator;
import discover.service.GalleryLocalService;
import highlights.service.CategoryLocalService;
import misk.headless.components.EventComponent;
import misk.headless.components.HighlightComponent;
import misk.headless.components.StoryComponent;
import misk.headless.dto.v1_0.Gallery;
import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import courts.model.Court;
import courts.service.CourtLocalService;
import discover.model.Discover;
import discover.service.DiscoverLocalService;
import events.model.Event;
import events.model.EventsGallery;
import events.service.EventLocalService;
import events.service.EventsGalleryLocalServiceUtil;
import highlights.model.Highlight;
import highlights.service.HighlightLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.EventList;

import misk.headless.resource.v1_0.GlobalSearchResource;
import stories.model.Story;
import stories.service.StoryLocalService;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/global-search.properties", scope = ServiceScope.PROTOTYPE, service = GlobalSearchResource.class)
public class GlobalSearchResourceImpl extends BaseGlobalSearchResourceImpl {

	Log log = LogFactoryUtil.getLog(getClass());

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	@Override
	public Response getGlobalSearch(@NotNull String searchTerm, @NotNull String languageId) throws Exception {

		ArrayList<Object> searchResult = new ArrayList<>();

		SearchContext searchContext = new SearchContext();

		List<misk.headless.dto.v1_0.Album> miskDiscover = null;
		/* List<misk.headless.dto.v1_0.Court> miskCourts = null; */
		List<misk.headless.dto.v1_0.Highlight> miskHighlights = null;
		List<misk.headless.dto.v1_0.EventList> miskEvents = null;
		List<misk.headless.dto.v1_0.Story> miskStories = null;
		//List<misk.headless.dto.v1_0.Attraction> miskAttractions = null;
		if (Validator.isNotNull(searchTerm)) {
			searchTerm = searchTerm.replaceAll("\\$", "").replaceAll("&", "").replaceAll("\\*", "").replaceAll("@", "")
					.replaceAll("#", "").replaceAll("\\^", "").replaceAll("-", "").replaceAll("\\)", "")
					.replaceAll("\\(", "");
		}
		if(Validator.isNull(searchTerm)||searchTerm=="") {
			return Response.ok(searchResult).build();
		}
		searchContext.setCompanyId(contextCompany.getCompanyId());
		System.out.println(contextCompany.getCompanyId());
		searchContext.setGroupIds(new long[] { webGroupId, appGroupId });
		searchContext.setKeywords(searchTerm);
		searchContext.setAttribute("groupId", appGroupId);
		searchContext.setLocale(contextAcceptLanguage.getPreferredLocale());
		// @MY_TODO: pagination need to add
		miskDiscover = this.getMiskDiscover(searchContext, languageId);
		if (miskDiscover.size() > 0) {
			HashMap<String, Object> discover = new HashMap<>();
			discover.put("title", CommonUtil.getLanguageTranslation(contextHttpServletRequest, "global-search-type-discover"));
			discover.put("type", "Discover");
			discover.put("data", miskDiscover);
			searchResult.add(discover);
		}

		/*
		 * miskCourts = this.getMiskCourts(searchContext, languageId);
		 * if(miskCourts.size() > 0) { HashMap<String, Object> court = new HashMap<>();
		 * court.put("title", "Courts"); court.put("data", miskCourts);
		 * searchResult.add(court); }
		 */

		miskHighlights = this.getMiskHighlights(searchContext, languageId);
		if (miskHighlights.size() > 0) {
			HashMap<String, Object> highlight = new HashMap<>();
			highlight.put("title", CommonUtil.getLanguageTranslation(contextHttpServletRequest, "global-search-type-highlight"));
			highlight.put("type", "Highlights");
			highlight.put("data", miskHighlights);
			searchResult.add(highlight);
		}

		miskEvents = this.getMiskEvents(searchContext, languageId);
		if (miskEvents.size() > 0) {
			HashMap<String, Object> event = new HashMap<>();
			event.put("title", CommonUtil.getLanguageTranslation(contextHttpServletRequest, "global-search-type-event"));
			event.put("type", "Events");
			event.put("data", miskEvents);
			searchResult.add(event);
		}

		miskStories = this.getMiskStories(searchContext, languageId);
		if (miskStories.size() > 0) {
			HashMap<String, Object> story = new HashMap<>();
			story.put("title", CommonUtil.getLanguageTranslation(contextHttpServletRequest, "global-search-type-news"));
			story.put("type", "News");
			story.put("data", miskStories);
			searchResult.add(story);
		}

		/*miskAttractions = this.getMiskAttractions(searchContext, languageId);
		if (miskAttractions.size() > 0) {
			HashMap<String, Object> attraction = new HashMap<>();
			attraction.put("title", "Attractions");
			attraction.put("data", miskAttractions);
			searchResult.add(attraction);
		}*/

		return Response.ok(searchResult).build();
	}

	private List<misk.headless.dto.v1_0.Attraction> getMiskAttractions(SearchContext searchContext, String languageId) {
		List<Attractions> attractions = attractionsLocalService.searchAttractions(searchContext);
		List<misk.headless.dto.v1_0.Attraction> miskAttractions = new ArrayList<>();

		for (Attractions attraction : attractions) {
			misk.headless.dto.v1_0.Attraction miskAttraction = new misk.headless.dto.v1_0.Attraction();

			miskAttraction.setAttractionId(String.valueOf(attraction.getAttractionsId()));
			miskAttraction.setAttractionBanner(
					ApiKeys.getBaseURL(this.contextUriInfo) + attraction.getForumBannerImage(languageId));
			miskAttraction.setAttractionTitle(attraction.getForumBannerTitle(languageId));
			miskAttraction.setWorkingHoursLabel(attraction.getWorkingHoursLabel(languageId));
			miskAttraction.setWorkingHoursLabelColor(attraction.getWorkingHoursLabelColor(languageId));
			miskAttraction.setWorkingHoursImage(
					ApiKeys.getBaseURL(this.contextUriInfo) + attraction.getWorkingHoursImage(languageId));
			miskAttraction.setWorkingHours(attraction.getWorkingHours(languageId));
			miskAttraction.setWorkingDaysImage(
					ApiKeys.getBaseURL(this.contextUriInfo) + attraction.getWorkingDaysImage(languageId));
			miskAttraction.setWorkingDays(attraction.getWorkingDays(languageId));
			miskAttraction.setDescription(attraction.getDescription(languageId));

			miskAttractions.add(miskAttraction);
		}

		return miskAttractions;
	}

	private List<misk.headless.dto.v1_0.Story> getMiskStories(SearchContext searchContext, String languageId) {
		List<Story> stories = storyLocalService.searchStories(searchContext);
		List<misk.headless.dto.v1_0.Story> miskStories = new ArrayList<>();

		for (Story story : stories) {
			misk.headless.dto.v1_0.Story result = StoryComponent.getDTO(languageId, this.contextUriInfo, story);

			miskStories.add(result);
		}

		return miskStories;
	}

	private List<EventList> getMiskEvents(SearchContext searchContext, String languageId) {
		List<Event> events = eventLocalService.searchEvents(searchContext);

		List<misk.headless.dto.v1_0.EventList> miskEvents = new ArrayList<>();

		for (Event event : events) {
			EventList result = EventComponent.getDTO(languageId, event, this.contextUriInfo);

			miskEvents.add(result);
		}

		return miskEvents;
	}

	private List<misk.headless.dto.v1_0.Highlight> getMiskHighlights(SearchContext searchContext, String languageId) {

		return HighlightComponent.searchHighlights(searchContext, languageId, this.contextHttpServletRequest, this.contextUriInfo);
	}

	private List<misk.headless.dto.v1_0.Court> getMiskCourts(SearchContext searchContext, String languageId) {
		List<Court> courts = courtLocalService.searchCourts(searchContext);
		List<misk.headless.dto.v1_0.Court> miskCourts = new ArrayList<>();
		for (Court court : courts) {
			misk.headless.dto.v1_0.Court miskCourt = new misk.headless.dto.v1_0.Court();

			miskCourt.setId(court.getCourtId());
			miskCourt.setName(court.getName(languageId));
			miskCourt.setDescription(court.getDescription(languageId));
			miskCourt.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + court.getListingImage());
			miskCourt.setLat(court.getLat());
			miskCourt.setVenue(court.getVenue(languageId));
			miskCourt.setContactEmail(court.getContactEmail());
			miskCourt.setContactPhone(court.getContactPhone());
			miskCourts.add(miskCourt);
		}
		return miskCourts;
	}

	private List<misk.headless.dto.v1_0.Album> getMiskDiscover(SearchContext searchContext, String languageId) {

		List<Discover> discovers = discoverLocalService.searchDiscovers(searchContext);
		List<misk.headless.dto.v1_0.Album> miskDiscovers = new ArrayList<>();
		for (Discover discover : discovers) {
			misk.headless.dto.v1_0.Album misDiscover = new misk.headless.dto.v1_0.Album();

			misDiscover.setAlbumId(discover.getDiscoverId());
			misDiscover.setTitle(discover.getTitle(languageId));
			misDiscover.setDescription(discover.getDescription(languageId));
			misDiscover.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + discover.getListingImage(languageId));

			Gallery[] gallery = getAlbumGallery(discover.getDiscoverId(), languageId);
			misDiscover.setGalleries(gallery);

			miskDiscovers.add(misDiscover);
		}
		return miskDiscovers;
	}

	private Gallery[] getAlbumGallery(long discoverId, String languageId){
		List<discover.model.Gallery> records = _galleryLocalService.findAllInDiscover(discoverId);
		Gallery[] results = new Gallery[records.size()];
		int i = 0;

		for (discover.model.Gallery curRecord : records) {
			Gallery result = new Gallery();
			result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getImage(languageId));
			result.setThumbnail(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getThumbnail(languageId));
			results[i] = result;
			i++;
		}
		return results;
	}

	@Reference
	EventLocalService eventLocalService;

	@Reference
	DiscoverLocalService discoverLocalService;

	@Reference
	CourtLocalService courtLocalService;

	@Reference
	StoryLocalService storyLocalService;

	@Reference
	HighlightLocalService highlightLocalService;

	@Reference
	private CategoryLocalService _categoryLocalService;

	@Reference
	AttractionsLocalService attractionsLocalService;

	@Reference
	private GalleryLocalService _galleryLocalService;

}