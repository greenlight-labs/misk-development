package misk.headless.constants;

public class AccountKeys {
	public static final Integer EMAIL_NOT_VERIFIED_CODE = 1001;
	public static final Integer PHONE_NOT_VERIFIED_CODE = 1002;
	public static final Integer NOT_VALID_FORGOT_PASSWORD_REQUEST_CODE = 1003;
	public static final Integer NOT_VALID_EDIT_PROFILE_REQUEST_CODE = 1004;
	public static final Integer NOT_VALID_CHANGE_PHONE_REQUEST_CODE = 1005;
	public static final Integer ACCOUNT_IS_DEACTIVATED_CODE = 1006;
}
