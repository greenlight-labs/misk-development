package misk.headless.components;

import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.util.Validator;
import highlights.model.Category;
import highlights.model.Highlight;
import highlights.model.Tag;
import highlights.service.CategoryLocalServiceUtil;
import highlights.service.HighlightLocalServiceUtil;
import highlights.service.TagLocalServiceUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.util.FavouriteUtil;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

public class HighlightComponent {

    public static final String TAG_HIGHLIGHT = "highlight";
    public static final String TAG_PORTRAIT_MANIFESTO = "portrait-manifesto";
    public static long webGroupId = ApiKeys.webGroupId;
    public static long appGroupId = ApiKeys.appGroupId;

    public static misk.headless.dto.v1_0.Highlight[] getHighlights(String languageId, String tagSlug, HttpServletRequest contextHttpServletRequest, UriInfo contextUriInfo) {

        long tagId = getTagId(tagSlug);
        if(Validator.isNull(tagId)) {
            return new misk.headless.dto.v1_0.Highlight[0];
        }

        List<highlights.model.Highlight> records = HighlightLocalServiceUtil.findByTagId(tagId);
        misk.headless.dto.v1_0.Highlight[] results = new misk.headless.dto.v1_0.Highlight[records.size()];
        int i = 0;

        for (highlights.model.Highlight curRecord : records) {

            //@MY_TODO: need to remove category query form loop, add it outer loop, fetch and store in array
            Category category = CategoryLocalServiceUtil.fetchCategory(curRecord.getCategoryId());

            //@MY_TODO: need to remove tag query form loop, add it outer loop, fetch and store in array
            Tag tag = TagLocalServiceUtil.fetchTag(curRecord.getTagId());

            results[i] = getDTO(languageId, contextUriInfo, curRecord, category, tag);
            i++;
        }

        // update events favourite status
        FavouriteUtil.updateHighlightsFavouriteStatus(results, contextHttpServletRequest);

        return results;
    }

    public static List<misk.headless.dto.v1_0.Highlight> searchHighlights(SearchContext searchContext, String languageId, HttpServletRequest contextHttpServletRequest, UriInfo contextUriInfo) {

        List<Highlight> records = HighlightLocalServiceUtil.searchHighlights(searchContext);
        List<misk.headless.dto.v1_0.Highlight> results = new ArrayList<>();

        for (highlights.model.Highlight curRecord : records) {

            //@MY_TODO: need to remove category query form loop, add it outer loop, fetch and store in array
            Category category = CategoryLocalServiceUtil.fetchCategory(curRecord.getCategoryId());

            //@MY_TODO: need to remove tag query form loop, add it outer loop, fetch and store in array
            Tag tag = TagLocalServiceUtil.fetchTag(curRecord.getTagId());

            results.add(getDTO(languageId, contextUriInfo, curRecord, category, tag));
        }

        return results;
    }

    public static misk.headless.dto.v1_0.Highlight getDTO(String languageId, UriInfo contextUriInfo, Highlight curRecord, Category category, Tag tag) {
        misk.headless.dto.v1_0.Highlight result = new misk.headless.dto.v1_0.Highlight();

        result.setHighlightId(curRecord.getHighlightId());
        result.setName(curRecord.getName(languageId));
        result.setProfession(curRecord.getProfession(languageId));
        result.setLocation(curRecord.getLocation(languageId));
        result.setImage(ApiKeys.getBaseURL(contextUriInfo) + curRecord.getImage(languageId));
        result.setDescription(curRecord.getDescription(languageId));
        result.setFacebook(curRecord.getFacebook(languageId));
        result.setTwitter(curRecord.getTwitter(languageId));
        result.setLinkedin(curRecord.getLinkedin(languageId));
        result.setIsFavourite(Boolean.FALSE);
        result.setType("image");
        if(Validator.isNotNull(curRecord.getVideo())){
            result.setType("video");
            result.setVideo(ApiKeys.getBaseURL(contextUriInfo) + curRecord.getVideo(languageId));
        } else if(Validator.isNotNull(curRecord.getAudio())){
            result.setType("audio");
            result.setAudio(ApiKeys.getBaseURL(contextUriInfo) + curRecord.getAudio(languageId));
        }
        // set category and tag fields empty initially and then set them if they are not null
        result.setCatName("");
        result.setCatColor("");
        result.setTagSlug("");
        result.setTagTitle("");
        result.setTagDescription("");
        if (Validator.isNotNull(category)) {
            result.setCatName(category.getName(languageId));
            result.setCatColor(category.getColor());
        }
        if (Validator.isNotNull(tag)) {
            result.setTagSlug(tag.getSlug());
            result.setTagTitle(tag.getName(languageId));
            result.setTagDescription(tag.getDescription(languageId));
        }
        return result;
    }

    public static long getTagId(String tagSlug){
        Tag tag = TagLocalServiceUtil.fetchBySlug(tagSlug);
        if(Validator.isNotNull(tag)){
            return tag.getTagId();
        }
        return 0;
    }
}
