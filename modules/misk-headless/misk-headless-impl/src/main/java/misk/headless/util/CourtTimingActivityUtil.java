package misk.headless.util;

import bookings.model.Booking;
import bookings.model.CourtTiming;
import bookings.service.BookingLocalService;
import bookings.service.CourtTimingLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import misk.headless.dto.v1_0.TimeSlot;
import org.apache.commons.lang3.ArrayUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.ws.rs.BadRequestException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component(immediate = true, service = CourtTimingActivityUtil.class)
public class CourtTimingActivityUtil {
    private static final Log _log = LogFactoryUtil.getLog(CourtTimingActivityUtil.class);

    public List<TimeSlot> getAvailableSlots(Long courtId, Date date) {

        List<TimeSlot> timeSlots = new ArrayList<>();

        try{
            // check if selected date is past date or not
            /*boolean isPastDate = isPastDate(date);
            if(isPastDate){
                throw new BadRequestException("selected date is past date.");
            }*/

            // get Day (1-7/Mon-Sun) from date
            int day = getDayFromDate(date);

            // check if selected date is not a closed day of court
            boolean isCloseDay = isClosedDay(courtId, day);
            if(isCloseDay){
                throw new BadRequestException("Court is closed on selected day.");
            }

            // check if the court is closed all day for maintenance on selected date
            /*boolean isMaintenanceDay = isMaintenanceDay(courtId, date);
            if(isMaintenanceDay){
                throw new BadRequestException("Court is closed on selected date for maintenance.");
            }*/

            // calculate all slots using slotDuration, openTime & closeTime
            List<TimeSlot> courtTimeSlots = getCourtTimeSlots(courtId, day);

            // remove slots which appeared between partially maintenance time
            //courtTimeSlots = removePartiallyMaintenanceTime(courtTimeSlots);

            // remove slots which are already booked
            courtTimeSlots = removeBookedSlots(courtTimeSlots, courtId, date);

            return courtTimeSlots;
        }catch (Exception ex){
            throw ex;
        }
        //return timeSlots;
    }

    private List<TimeSlot> removeBookedSlots(List<TimeSlot> courtTimeSlots, Long courtId, Date date) {
        List<Booking> bookings = _bookingLocalService.getBookedSlots(courtId, date);
        SimpleDateFormat slotTime = new SimpleDateFormat("HH:mm");
        //int[] idx = new int[0];
        List<TimeSlot> removeTimeSlots = new ArrayList<>();
        for (Booking curBooking : bookings) {
            String bookedStartTime = slotTime.format(curBooking.getSlotStartTime());
            String bookedEndTime = slotTime.format(curBooking.getSlotEndTime());
            LocalTime bookedStart = LocalTime.parse(bookedStartTime);
            LocalTime bookedEnd = LocalTime.parse(bookedEndTime);
            for (TimeSlot timeSlot: courtTimeSlots) {
                LocalTime slotStartTime = LocalTime.parse(timeSlot.getSlotStartTime());
                LocalTime slotEndTime = LocalTime.parse(timeSlot.getSlotEndTime());
                //if(bookedStart >= slotEndTime || bookedEnd <= slotStartTime){
                if(!(bookedStart.compareTo(slotEndTime) >= 0 || bookedEnd.compareTo(slotStartTime) <= 0 )){
                    //idx = ArrayUtils.add(idx, courtTimeSlots.indexOf(timeSlot));
                    removeTimeSlots.add(timeSlot);
                }
            }
        }

        for (TimeSlot removeSlot : removeTimeSlots) {
            courtTimeSlots.remove(removeSlot);
        }

        return courtTimeSlots;
    }

    public List<TimeSlot> getCourtTimeSlots(Long courtId, int day) {
        CourtTiming courtTiming = _courtTimingLocalService.fetchByCourtDay(courtId, day);
        if(Validator.isNotNull(courtTiming)){
            int slotDuration = courtTiming.getSlotDuration() + courtTiming.getBreakTime();
            return createTimeSlots(slotDuration, courtTiming.getOpenTime(), courtTiming.getCloseTime());
        }else{
            throw new BadRequestException("Court timings are not set in the system for this selected day, Please contact administrator.");
        }
    }

    public List<TimeSlot> createTimeSlots(int slotDuration, String openTime, String closeTime) {

        SimpleDateFormat sdf= new SimpleDateFormat("HH:mm");
        List<TimeSlot> timeSlots = new ArrayList<>();

        try {

            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(sdf.parse(openTime));

            /*if (startCalendar.get(Calendar.MINUTE) < slotDuration) {
                startCalendar.set(Calendar.MINUTE, slotDuration);
            } else {
                startCalendar.add(Calendar.MINUTE, slotDuration); // overstep hour and clear minutes
                startCalendar.clear(Calendar.MINUTE);
            }*/

            Calendar endCalendar = Calendar.getInstance();
            //endCalendar.setTime(startCalendar.getTime());
            endCalendar.setTime(sdf.parse(closeTime));

            // if you want dates for whole next day, uncomment next line
            endCalendar.add(Calendar.DAY_OF_YEAR, 0);
            //endCalendar.add(Calendar.HOUR_OF_DAY, 24 - startCalendar.get(Calendar.HOUR_OF_DAY));
            //endCalendar.add(Calendar.HOUR_OF_DAY, startCalendar.get(Calendar.HOUR_OF_DAY));

            endCalendar.clear(Calendar.MINUTE);
            endCalendar.clear(Calendar.SECOND);
            endCalendar.clear(Calendar.MILLISECOND);

            SimpleDateFormat slotTime = new SimpleDateFormat("HH:mm");
            while (endCalendar.after(startCalendar)) {
                String slotStartTime = slotTime.format(startCalendar.getTime());

                startCalendar.add(Calendar.MINUTE, slotDuration);
                String slotEndTime = slotTime.format(startCalendar.getTime());

                LocalTime slotEndTime1 = LocalTime.parse(slotEndTime);
                LocalTime closeTime2 = LocalTime.parse(closeTime);

                if(!slotEndTime1.isAfter(closeTime2)){
                    TimeSlot timeSlot = new TimeSlot();
                    timeSlot.setSlotStartTime(slotStartTime);
                    timeSlot.setSlotEndTime(slotEndTime);
                    timeSlots.add(timeSlot);
                }
            }

            return timeSlots;
        } catch (ParseException e) {
            // date in wrong format
            //throw e;
            return timeSlots;
        }
    }

    public boolean isMaintenanceDay(Long courtId, Date date) {
        return false;
    }

    public boolean isClosedDay(Long courtId, int day) {
        CourtTiming courtTiming = _courtTimingLocalService.fetchByCourtDay(courtId, day);
        return Validator.isNotNull(courtTiming) && courtTiming.getClosedDay();
    }

    /*public static int getDayFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }*/

    public static int getDayFromDate(Date date) {
        LocalDate localDate = date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        DayOfWeek day = localDate.getDayOfWeek();
        return day.getValue();
    }

    @Reference
    private BookingLocalService _bookingLocalService;

    @Reference
    private CourtTimingLocalService _courtTimingLocalService;

}
