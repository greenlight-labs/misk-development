package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;

import javax.ws.rs.BadRequestException;

import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import forms.service.FeedbackLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.Feedback;
import misk.headless.dto.v1_0.RequestStatus;
import misk.headless.resource.v1_0.FeedbackResource;
import misk.headless.util.SendEmailUtil;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/feedback.properties", scope = ServiceScope.PROTOTYPE, service = FeedbackResource.class)
public class FeedbackResourceImpl extends BaseFeedbackResourceImpl {
	Log _log = LogFactoryUtil.getLog(FeedbackResourceImpl.class);
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	@Override
	public Feedback addFeedback(Feedback feedback) throws Exception {

		try {
			if (Validator.isNull(feedback.getFullName()) || Validator.isNull(feedback.getEmailAddress())
					|| !Validator.isEmailAddress(feedback.getEmailAddress())
					|| Validator.isNull(feedback.getFeedbackType()) || Validator.isNull(feedback.getMessage())) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "feedback-fields-required"));
			}

			ServiceContext serviceContext = ServiceContextFactory.getInstance(forms.model.Feedback.class.getName(),
					this.contextHttpServletRequest);
			serviceContext.setCompanyId(this.contextCompany.getCompanyId());
			serviceContext.setScopeGroupId(ApiKeys.appGroupId);
			forms.model.Feedback varFeedback = _feedbackLocalService.addFeedback(feedback.getFullName(),
					feedback.getEmailAddress(), feedback.getFeedbackType(), feedback.getMessage(), serviceContext);

			RequestStatus requestStatus = new RequestStatus();
			String languageId = "ar_SA";

			if (Validator.isNotNull(feedback.getLanguageId())) {
				languageId = feedback.getLanguageId();
			}

			SendEmailUtil.sendMailUsingTemplateForFeedback(feedback.getFullName(), feedback.getEmailAddress(),
					feedback.getFeedbackType(), feedback.getMessage(), languageId);
			
			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "feedback-added-successful"));

			return toFeedbackStatus(varFeedback, requestStatus);

		} catch (Exception ex) {
			_log.error("Error creating feedback form entry: " + ex.getMessage(), ex);
			throw ex;
		}

	}

	private static @GraphQLField Feedback toFeedbackStatus(forms.model.Feedback varFeedback,
			RequestStatus requestStatus) {
		RequestStatus newRequestStatus = requestStatus;
		return new Feedback() {
			{
				requestStatus = newRequestStatus;
			}
		};
	}

	@Reference
	private FeedbackLocalService _feedbackLocalService;

}
