package misk.headless.internal.graphql.mutation.v1_0;

import com.liferay.petra.function.UnsafeConsumer;
import com.liferay.petra.function.UnsafeFunction;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.vulcan.accept.language.AcceptLanguage;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLName;
import com.liferay.portal.vulcan.multipart.MultipartBody;

import java.util.function.BiFunction;

import javax.annotation.Generated;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import misk.headless.dto.v1_0.AddVisitor;
import misk.headless.dto.v1_0.AppUser;
import misk.headless.dto.v1_0.Booking;
import misk.headless.dto.v1_0.DeleteProfileImage;
import misk.headless.dto.v1_0.Feedback;
import misk.headless.dto.v1_0.NotificationSettings;
import misk.headless.dto.v1_0.ProfileDocument;
import misk.headless.dto.v1_0.PushNotification;
import misk.headless.dto.v1_0.Souvenirs;
import misk.headless.resource.v1_0.AppUserResource;
import misk.headless.resource.v1_0.BookingResource;
import misk.headless.resource.v1_0.EventListResource;
import misk.headless.resource.v1_0.FeedbackResource;
import misk.headless.resource.v1_0.MyFavoritesResource;
import misk.headless.resource.v1_0.NotificationSettingsResource;
import misk.headless.resource.v1_0.ProfileDocumentResource;
import misk.headless.resource.v1_0.PushNotificationResource;
import misk.headless.resource.v1_0.SouvenirsResource;

import org.osgi.service.component.ComponentServiceObjects;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public class Mutation {

	public static void setAppUserResourceComponentServiceObjects(
		ComponentServiceObjects<AppUserResource>
			appUserResourceComponentServiceObjects) {

		_appUserResourceComponentServiceObjects =
			appUserResourceComponentServiceObjects;
	}

	public static void setBookingResourceComponentServiceObjects(
		ComponentServiceObjects<BookingResource>
			bookingResourceComponentServiceObjects) {

		_bookingResourceComponentServiceObjects =
			bookingResourceComponentServiceObjects;
	}

	public static void setEventListResourceComponentServiceObjects(
		ComponentServiceObjects<EventListResource>
			eventListResourceComponentServiceObjects) {

		_eventListResourceComponentServiceObjects =
			eventListResourceComponentServiceObjects;
	}

	public static void setFeedbackResourceComponentServiceObjects(
		ComponentServiceObjects<FeedbackResource>
			feedbackResourceComponentServiceObjects) {

		_feedbackResourceComponentServiceObjects =
			feedbackResourceComponentServiceObjects;
	}

	public static void setMyFavoritesResourceComponentServiceObjects(
		ComponentServiceObjects<MyFavoritesResource>
			myFavoritesResourceComponentServiceObjects) {

		_myFavoritesResourceComponentServiceObjects =
			myFavoritesResourceComponentServiceObjects;
	}

	public static void setNotificationSettingsResourceComponentServiceObjects(
		ComponentServiceObjects<NotificationSettingsResource>
			notificationSettingsResourceComponentServiceObjects) {

		_notificationSettingsResourceComponentServiceObjects =
			notificationSettingsResourceComponentServiceObjects;
	}

	public static void setProfileDocumentResourceComponentServiceObjects(
		ComponentServiceObjects<ProfileDocumentResource>
			profileDocumentResourceComponentServiceObjects) {

		_profileDocumentResourceComponentServiceObjects =
			profileDocumentResourceComponentServiceObjects;
	}

	public static void setPushNotificationResourceComponentServiceObjects(
		ComponentServiceObjects<PushNotificationResource>
			pushNotificationResourceComponentServiceObjects) {

		_pushNotificationResourceComponentServiceObjects =
			pushNotificationResourceComponentServiceObjects;
	}

	public static void setSouvenirsResourceComponentServiceObjects(
		ComponentServiceObjects<SouvenirsResource>
			souvenirsResourceComponentServiceObjects) {

		_souvenirsResourceComponentServiceObjects =
			souvenirsResourceComponentServiceObjects;
	}

	@GraphQLField
	public AppUser createRegister(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.postRegister(appUser));
	}

	@GraphQLField
	public AppUser updateRegister(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.updateRegister(appUser));
	}

	@GraphQLField
	public AppUser verifyRegistrationOTP(
			@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.verifyRegistrationOTP(appUser));
	}

	@GraphQLField
	public AppUser activateVerifiedMobileOTPUser(
			@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.activateVerifiedMobileOTPUser(
				appUser));
	}

	@GraphQLField
	@GraphQLName(description = "null", value = "editUserProfileMultipartBody")
	public AppUser editUserProfile(
			@GraphQLName("multipartBody") MultipartBody multipartBody)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.editUserProfile(multipartBody));
	}

	@GraphQLField
	@GraphQLName(
		description = "null",
		value = "updateUserProfileBannerImageMultipartBody"
	)
	public AppUser updateUserProfileBannerImage(
			@GraphQLName("multipartBody") MultipartBody multipartBody)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.updateUserProfileBannerImage(
				multipartBody));
	}

	@GraphQLField
	public AppUser deleteUserProfileImage(
			@GraphQLName("deleteProfileImage") DeleteProfileImage
				deleteProfileImage)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.deleteUserProfileImage(
				deleteProfileImage));
	}

	@GraphQLField
	public AppUser createLogin(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.postLogin(appUser));
	}

	@GraphQLField
	public AppUser createSocialLogin(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.postSocialLogin(appUser));
	}

	@GraphQLField
	public AppUser resetPassword(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.resetPassword(appUser));
	}

	@GraphQLField
	public AppUser changePassword(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.changePassword(appUser));
	}

	@GraphQLField
	public AppUser checkPassword(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.checkPassword(appUser));
	}

	@GraphQLField
	public AppUser forgotPassword(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.forgotPassword(appUser));
	}

	@GraphQLField
	public AppUser forgotPasswordPhone(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.forgotPasswordPhone(appUser));
	}

	@GraphQLField
	public AppUser setEmailAddressVerified(
			@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.setEmailAddressVerified(
				appUser));
	}

	@GraphQLField
	public AppUser setPhoneNumberVerified(
			@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.setPhoneNumberVerified(appUser));
	}

	@GraphQLField
	public AppUser updateUserAccountDetails(
			@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.updateUserAccountDetails(
				appUser));
	}

	@GraphQLField
	public AppUser resendOTP(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.resendOTP(appUser));
	}

	@GraphQLField
	public AppUser verifyOTP(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.verifyOTP(appUser));
	}

	@GraphQLField
	public AppUser addDevice(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.addDevice(appUser));
	}

	@GraphQLField
	public AppUser deleteAccount(@GraphQLName("appUser") AppUser appUser)
		throws Exception {

		return _applyComponentServiceObjects(
			_appUserResourceComponentServiceObjects,
			this::_populateResourceContext,
			appUserResource -> appUserResource.deleteAccount(appUser));
	}

	@GraphQLField
	public Booking createBooking(
			@GraphQLName("languageId") String languageId,
			@GraphQLName("booking") Booking booking)
		throws Exception {

		return _applyComponentServiceObjects(
			_bookingResourceComponentServiceObjects,
			this::_populateResourceContext,
			bookingResource -> bookingResource.createBooking(
				languageId, booking));
	}

	@GraphQLField
	public Booking updateBooking(
			@GraphQLName("languageId") String languageId,
			@GraphQLName("booking") Booking booking)
		throws Exception {

		return _applyComponentServiceObjects(
			_bookingResourceComponentServiceObjects,
			this::_populateResourceContext,
			bookingResource -> bookingResource.updateBooking(
				languageId, booking));
	}

	@GraphQLField
	public Booking deleteBooking(@GraphQLName("bookingId") Long bookingId)
		throws Exception {

		return _applyComponentServiceObjects(
			_bookingResourceComponentServiceObjects,
			this::_populateResourceContext,
			bookingResource -> bookingResource.deleteBooking(bookingId));
	}

	@GraphQLField
	public Response deleteBookingBatch(
			@GraphQLName("callbackURL") String callbackURL,
			@GraphQLName("object") Object object)
		throws Exception {

		return _applyComponentServiceObjects(
			_bookingResourceComponentServiceObjects,
			this::_populateResourceContext,
			bookingResource -> bookingResource.deleteBookingBatch(
				callbackURL, object));
	}

	@GraphQLField
	public AddVisitor AddEventsVisitor(
			@GraphQLName("addVisitor") AddVisitor addVisitor)
		throws Exception {

		return _applyComponentServiceObjects(
			_eventListResourceComponentServiceObjects,
			this::_populateResourceContext,
			eventListResource -> eventListResource.AddEventsVisitor(
				addVisitor));
	}

	@GraphQLField
	public Feedback addFeedback(@GraphQLName("feedback") Feedback feedback)
		throws Exception {

		return _applyComponentServiceObjects(
			_feedbackResourceComponentServiceObjects,
			this::_populateResourceContext,
			feedbackResource -> feedbackResource.addFeedback(feedback));
	}

	@GraphQLField
	public Response createMyFavorite(
			@GraphQLName("itemId") Long itemId,
			@GraphQLName("typeId") Long typeId,
			@GraphQLName("userId") Long userId)
		throws Exception {

		return _applyComponentServiceObjects(
			_myFavoritesResourceComponentServiceObjects,
			this::_populateResourceContext,
			myFavoritesResource -> myFavoritesResource.postMyFavorite(
				itemId, typeId, userId));
	}

	@GraphQLField
	public Response deleteFavorite(
			@GraphQLName("itemId") Long itemId,
			@GraphQLName("typeId") Long typeId,
			@GraphQLName("userId") Long userId)
		throws Exception {

		return _applyComponentServiceObjects(
			_myFavoritesResourceComponentServiceObjects,
			this::_populateResourceContext,
			myFavoritesResource -> myFavoritesResource.deleteFavorite(
				itemId, typeId, userId));
	}

	@GraphQLField(description = "Create a new vitamin/mineral.")
	public NotificationSettings createNotificationSettings(
			@GraphQLName("notificationSettings") NotificationSettings
				notificationSettings)
		throws Exception {

		return _applyComponentServiceObjects(
			_notificationSettingsResourceComponentServiceObjects,
			this::_populateResourceContext,
			notificationSettingsResource ->
				notificationSettingsResource.postNotificationSettings(
					notificationSettings));
	}

	@GraphQLField
	public Response createNotificationSettingsBatch(
			@GraphQLName("callbackURL") String callbackURL,
			@GraphQLName("object") Object object)
		throws Exception {

		return _applyComponentServiceObjects(
			_notificationSettingsResourceComponentServiceObjects,
			this::_populateResourceContext,
			notificationSettingsResource ->
				notificationSettingsResource.postNotificationSettingsBatch(
					callbackURL, object));
	}

	@GraphQLField
	@GraphQLName(
		description = "null", value = "uploadProfileDocumentsMultipartBody"
	)
	public ProfileDocument uploadProfileDocuments(
			@GraphQLName("multipartBody") MultipartBody multipartBody)
		throws Exception {

		return _applyComponentServiceObjects(
			_profileDocumentResourceComponentServiceObjects,
			this::_populateResourceContext,
			profileDocumentResource ->
				profileDocumentResource.uploadProfileDocuments(multipartBody));
	}

	@GraphQLField
	public PushNotification updateReadStatus(
			@GraphQLName("pushNotification") PushNotification pushNotification)
		throws Exception {

		return _applyComponentServiceObjects(
			_pushNotificationResourceComponentServiceObjects,
			this::_populateResourceContext,
			pushNotificationResource ->
				pushNotificationResource.updateReadStatus(pushNotification));
	}

	@GraphQLField
	public Response getSouvenirs(@GraphQLName("souvenirs") Souvenirs souvenirs)
		throws Exception {

		return _applyComponentServiceObjects(
			_souvenirsResourceComponentServiceObjects,
			this::_populateResourceContext,
			souvenirsResource -> souvenirsResource.getSouvenirs(souvenirs));
	}

	private <T, R, E1 extends Throwable, E2 extends Throwable> R
			_applyComponentServiceObjects(
				ComponentServiceObjects<T> componentServiceObjects,
				UnsafeConsumer<T, E1> unsafeConsumer,
				UnsafeFunction<T, R, E2> unsafeFunction)
		throws E1, E2 {

		T resource = componentServiceObjects.getService();

		try {
			unsafeConsumer.accept(resource);

			return unsafeFunction.apply(resource);
		}
		finally {
			componentServiceObjects.ungetService(resource);
		}
	}

	private <T, E1 extends Throwable, E2 extends Throwable> void
			_applyVoidComponentServiceObjects(
				ComponentServiceObjects<T> componentServiceObjects,
				UnsafeConsumer<T, E1> unsafeConsumer,
				UnsafeConsumer<T, E2> unsafeFunction)
		throws E1, E2 {

		T resource = componentServiceObjects.getService();

		try {
			unsafeConsumer.accept(resource);

			unsafeFunction.accept(resource);
		}
		finally {
			componentServiceObjects.ungetService(resource);
		}
	}

	private void _populateResourceContext(AppUserResource appUserResource)
		throws Exception {

		appUserResource.setContextAcceptLanguage(_acceptLanguage);
		appUserResource.setContextCompany(_company);
		appUserResource.setContextHttpServletRequest(_httpServletRequest);
		appUserResource.setContextHttpServletResponse(_httpServletResponse);
		appUserResource.setContextUriInfo(_uriInfo);
		appUserResource.setContextUser(_user);
		appUserResource.setGroupLocalService(_groupLocalService);
		appUserResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(BookingResource bookingResource)
		throws Exception {

		bookingResource.setContextAcceptLanguage(_acceptLanguage);
		bookingResource.setContextCompany(_company);
		bookingResource.setContextHttpServletRequest(_httpServletRequest);
		bookingResource.setContextHttpServletResponse(_httpServletResponse);
		bookingResource.setContextUriInfo(_uriInfo);
		bookingResource.setContextUser(_user);
		bookingResource.setGroupLocalService(_groupLocalService);
		bookingResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(EventListResource eventListResource)
		throws Exception {

		eventListResource.setContextAcceptLanguage(_acceptLanguage);
		eventListResource.setContextCompany(_company);
		eventListResource.setContextHttpServletRequest(_httpServletRequest);
		eventListResource.setContextHttpServletResponse(_httpServletResponse);
		eventListResource.setContextUriInfo(_uriInfo);
		eventListResource.setContextUser(_user);
		eventListResource.setGroupLocalService(_groupLocalService);
		eventListResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(FeedbackResource feedbackResource)
		throws Exception {

		feedbackResource.setContextAcceptLanguage(_acceptLanguage);
		feedbackResource.setContextCompany(_company);
		feedbackResource.setContextHttpServletRequest(_httpServletRequest);
		feedbackResource.setContextHttpServletResponse(_httpServletResponse);
		feedbackResource.setContextUriInfo(_uriInfo);
		feedbackResource.setContextUser(_user);
		feedbackResource.setGroupLocalService(_groupLocalService);
		feedbackResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			MyFavoritesResource myFavoritesResource)
		throws Exception {

		myFavoritesResource.setContextAcceptLanguage(_acceptLanguage);
		myFavoritesResource.setContextCompany(_company);
		myFavoritesResource.setContextHttpServletRequest(_httpServletRequest);
		myFavoritesResource.setContextHttpServletResponse(_httpServletResponse);
		myFavoritesResource.setContextUriInfo(_uriInfo);
		myFavoritesResource.setContextUser(_user);
		myFavoritesResource.setGroupLocalService(_groupLocalService);
		myFavoritesResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			NotificationSettingsResource notificationSettingsResource)
		throws Exception {

		notificationSettingsResource.setContextAcceptLanguage(_acceptLanguage);
		notificationSettingsResource.setContextCompany(_company);
		notificationSettingsResource.setContextHttpServletRequest(
			_httpServletRequest);
		notificationSettingsResource.setContextHttpServletResponse(
			_httpServletResponse);
		notificationSettingsResource.setContextUriInfo(_uriInfo);
		notificationSettingsResource.setContextUser(_user);
		notificationSettingsResource.setGroupLocalService(_groupLocalService);
		notificationSettingsResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			ProfileDocumentResource profileDocumentResource)
		throws Exception {

		profileDocumentResource.setContextAcceptLanguage(_acceptLanguage);
		profileDocumentResource.setContextCompany(_company);
		profileDocumentResource.setContextHttpServletRequest(
			_httpServletRequest);
		profileDocumentResource.setContextHttpServletResponse(
			_httpServletResponse);
		profileDocumentResource.setContextUriInfo(_uriInfo);
		profileDocumentResource.setContextUser(_user);
		profileDocumentResource.setGroupLocalService(_groupLocalService);
		profileDocumentResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(
			PushNotificationResource pushNotificationResource)
		throws Exception {

		pushNotificationResource.setContextAcceptLanguage(_acceptLanguage);
		pushNotificationResource.setContextCompany(_company);
		pushNotificationResource.setContextHttpServletRequest(
			_httpServletRequest);
		pushNotificationResource.setContextHttpServletResponse(
			_httpServletResponse);
		pushNotificationResource.setContextUriInfo(_uriInfo);
		pushNotificationResource.setContextUser(_user);
		pushNotificationResource.setGroupLocalService(_groupLocalService);
		pushNotificationResource.setRoleLocalService(_roleLocalService);
	}

	private void _populateResourceContext(SouvenirsResource souvenirsResource)
		throws Exception {

		souvenirsResource.setContextAcceptLanguage(_acceptLanguage);
		souvenirsResource.setContextCompany(_company);
		souvenirsResource.setContextHttpServletRequest(_httpServletRequest);
		souvenirsResource.setContextHttpServletResponse(_httpServletResponse);
		souvenirsResource.setContextUriInfo(_uriInfo);
		souvenirsResource.setContextUser(_user);
		souvenirsResource.setGroupLocalService(_groupLocalService);
		souvenirsResource.setRoleLocalService(_roleLocalService);
	}

	private static ComponentServiceObjects<AppUserResource>
		_appUserResourceComponentServiceObjects;
	private static ComponentServiceObjects<BookingResource>
		_bookingResourceComponentServiceObjects;
	private static ComponentServiceObjects<EventListResource>
		_eventListResourceComponentServiceObjects;
	private static ComponentServiceObjects<FeedbackResource>
		_feedbackResourceComponentServiceObjects;
	private static ComponentServiceObjects<MyFavoritesResource>
		_myFavoritesResourceComponentServiceObjects;
	private static ComponentServiceObjects<NotificationSettingsResource>
		_notificationSettingsResourceComponentServiceObjects;
	private static ComponentServiceObjects<ProfileDocumentResource>
		_profileDocumentResourceComponentServiceObjects;
	private static ComponentServiceObjects<PushNotificationResource>
		_pushNotificationResourceComponentServiceObjects;
	private static ComponentServiceObjects<SouvenirsResource>
		_souvenirsResourceComponentServiceObjects;

	private AcceptLanguage _acceptLanguage;
	private com.liferay.portal.kernel.model.Company _company;
	private GroupLocalService _groupLocalService;
	private HttpServletRequest _httpServletRequest;
	private HttpServletResponse _httpServletResponse;
	private RoleLocalService _roleLocalService;
	private BiFunction<Object, String, Sort[]> _sortsBiFunction;
	private UriInfo _uriInfo;
	private com.liferay.portal.kernel.model.User _user;

}