package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import discover.model.Discover;
import discover.service.DiscoverLocalService;
import discover.service.GalleryLocalService;
import misk.headless.components.EventComponent;
import misk.headless.components.HighlightComponent;
import misk.headless.constants.ApiKeys;
import misk.headless.constants.EventKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.ExperientialCenterResource;
import misk.headless.util.DocumentMediaUtil;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.HashMap;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
        properties = "OSGI-INF/liferay/rest/v1_0/experiential-center.properties",
        scope = ServiceScope.PROTOTYPE, service = ExperientialCenterResource.class
)
public class ExperientialCenterResourceImpl
        extends BaseExperientialCenterResourceImpl {

    public long webGroupId = ApiKeys.webGroupId;
    public long appGroupId = ApiKeys.appGroupId;

    Log _log = LogFactoryUtil.getLog(ExperientialCenterResourceImpl.class);

    @Override
    public ExperientialCenter getExperientialCenterPage(String languageId)
            throws Exception {
        try {
            ExperientialCenter experientialCenter = new ExperientialCenter();
            experientialCenter.setBannerSection(this.getBannerSection(languageId));
            experientialCenter.setHighlightsSection(this.getHighlightsSection(languageId));
            experientialCenter.setHenaCafeSection(this.getHenaCafeSection(languageId));
            experientialCenter.setDiscoverSection(this.getDiscoverSection(languageId));
            experientialCenter.setEventsSection(this.getEventSection(languageId));
            return experientialCenter;
        } catch (Exception ex) {
            _log.error("Error retrieving experiential center page data: " + ex.getMessage(), ex);

            _log.error(ex.getMessage());
        }
        return null;
    }

    public BannerSection getBannerSection(String languageId) throws Exception {

        long groupId = this.appGroupId;
        String articleId = "APP_EC_PAGE_BANNER_SECTION";

        JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0);
        String articleContent = article.getContentByLocale(languageId);

        Document document = SAXReaderUtil.read(articleContent);
        Node root = document.selectSingleNode("/root");

        Node headingNode = root.selectSingleNode("dynamic-element[@name='Heading']/dynamic-content");
        Node titleField1Node = root.selectSingleNode("dynamic-element[@name='TitleField1']/dynamic-content");
        Node titleField2Node = root.selectSingleNode("dynamic-element[@name='TitleField2']/dynamic-content");
        Node descriptionNode = root.selectSingleNode("dynamic-element[@name='Description']/dynamic-content");
        Node buttonLabelNode = root.selectSingleNode("dynamic-element[@name='ButtonLabel']/dynamic-content");
        Node imageNode = root.selectSingleNode("dynamic-element[@name='Image']/dynamic-content");

        BannerSection result = new BannerSection();
        result.setHeading(headingNode.getText());
        result.setTitleField1(titleField1Node.getText());
        result.setTitleField2(titleField2Node.getText());
        result.setDescription(descriptionNode.getText());
        result.setButtonLabel(buttonLabelNode.getText());

        DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
        result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(imageNode.getText()));

        return result;
    }

    private HighlightsSection getHighlightsSection(String languageId) throws Exception{
        HighlightsSection section = new HighlightsSection();
        String sectionTitle = this.getSectionTitle(languageId, "highlightsSectionTitle");
        section.setSectionTitle(sectionTitle);
        section.setItems(HighlightComponent.getHighlights(languageId, HighlightComponent.TAG_PORTRAIT_MANIFESTO, this.contextHttpServletRequest, this.contextUriInfo));

        return section;
    }

    public HenaCafeSection getHenaCafeSection(String languageId) throws Exception {

        long groupId = this.appGroupId;
        String articleId = "APP_EC_PAGE_HENA_CAFE_SECTION";

        JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0);
        String articleContent = article.getContentByLocale(languageId);

        Document document = SAXReaderUtil.read(articleContent);
        Node root = document.selectSingleNode("/root");

        Node sectionTitleNode = root.selectSingleNode("dynamic-element[@name='SectionTitle']/dynamic-content");
        Node titleNode = root.selectSingleNode("dynamic-element[@name='Title']/dynamic-content");
        Node iconNode = root.selectSingleNode("dynamic-element[@name='Icon']/dynamic-content");
        Node imageNode = root.selectSingleNode("dynamic-element[@name='Image']/dynamic-content");

        HenaCafeSection result = new HenaCafeSection();
        result.setSectionTitle(sectionTitleNode.getText());
        result.setTitle(titleNode.getText());

        DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
        String iconImageUrl = "";
        String imageUrl = "";
        if(StringUtils.isNotEmpty(iconNode.getText())){
            iconImageUrl = ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(iconNode.getText());
        }
        if(StringUtils.isNotEmpty(imageNode.getText())){
            imageUrl = ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(imageNode.getText());
        }
        result.setIcon(iconImageUrl);
        result.setImage(imageUrl);

        return result;
    }

    private DiscoverSection getDiscoverSection(String languageId) throws Exception{
        DiscoverSection section = new DiscoverSection();
        String sectionTitle = this.getSectionTitle(languageId, "discoverSectionTitle");
        section.setSectionTitle(sectionTitle);
        section.setItems(this.getAlbums(languageId));

        return section;
    }

    private EventsSection getEventSection(String languageId) throws Exception{
        EventsSection eventsection = new EventsSection();
        String sectionTitle = this.getSectionTitle(languageId, "eventsSectionTitle");
        eventsection.setSectionTitle(sectionTitle);
        eventsection.setItems(EventComponent.getEventsByPageSlug(languageId, EventKeys.PAGE_EXPERIENCE_CENTER, this.contextHttpServletRequest, this.contextUriInfo));

        return eventsection;
    }

    public HashMap<String, String> getPageSectionsTitle(String languageId) throws Exception{
        long groupId = this.appGroupId;
        String articleId = "APP_EC_SECTIONS_TITLE";

        JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0 );
        String articleContent = article.getContentByLocale(languageId);
        Document document = SAXReaderUtil.read(articleContent);
        Node root = document.selectSingleNode("/root");

        Node highlightsSectionTitleNode = root.selectSingleNode("dynamic-element[@name='HighlightsSectionTitle']/dynamic-content");
        Node discoverSectionTitleNode = root.selectSingleNode("dynamic-element[@name='DiscoverSectionTitle']/dynamic-content");
        Node eventsSectionTitleNode = root.selectSingleNode("dynamic-element[@name='EventsSectionTitle']/dynamic-content");

        HashMap<String, String> sectionTitles = new HashMap<String, String>();
        sectionTitles.put("highlightsSectionTitle", highlightsSectionTitleNode.getText());
        sectionTitles.put("discoverSectionTitle", discoverSectionTitleNode.getText());
        sectionTitles.put("eventsSectionTitle", eventsSectionTitleNode.getText());

        return sectionTitles;
    }

    private String getSectionTitle(String languageId, String sectionTitleKey) throws Exception {
        HashMap<String, String> sectionTitles = this.getPageSectionsTitle(languageId);
        return sectionTitles.get(sectionTitleKey);
    }

    private Album[] getAlbums(String languageId){
        List<Discover> records = _discoverLocalService.findAllInGroup(this.appGroupId, 0, 9);
        Album[] results = new Album[records.size()];
        int i = 0;

        for (Discover curRecord : records) {
            Album result = new Album();
            result.setAlbumId(curRecord.getDiscoverId());
            result.setTitle(curRecord.getTitle(languageId));
            result.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getListingImage(languageId));
            result.setDescription(curRecord.getDescription(languageId));

            Gallery[] gallery = this.getAlbumGallery(curRecord.getDiscoverId(), languageId);
            result.setGalleries(gallery);

            results[i] = result;
            i++;
        }
        return results;
    }

    private Gallery[] getAlbumGallery(long discoverId, String languageId){
        List<discover.model.Gallery> records = _galleryLocalService.findAllInDiscover(discoverId);
        Gallery[] results = new Gallery[records.size()];
        int i = 0;

        for (discover.model.Gallery curRecord : records) {
            Gallery result = new Gallery();
            result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getImage(languageId));
            result.setThumbnail(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getThumbnail(languageId));
            results[i] = result;
            i++;
        }
        return results;
    }

    @Reference
    private DiscoverLocalService _discoverLocalService;

    @Reference
    private GalleryLocalService _galleryLocalService;
}