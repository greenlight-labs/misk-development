package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import faq.service.FaqLocalServiceUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.Faq;
import misk.headless.dto.v1_0.FaqList;
import misk.headless.resource.v1_0.FaqResource;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/faq.properties", scope = ServiceScope.PROTOTYPE, service = FaqResource.class)
public class FaqResourceImpl extends BaseFaqResourceImpl {
	Log _log = LogFactoryUtil.getLog(FaqResourceImpl.class);
	long groupId;
	long groupIdMainWebsite;
	protected String baseURL = "https://webserver-miskcity-dev.lfr.cloud";

	public FaqResourceImpl() {
		ApiKeys apiKeys = new ApiKeys();
		this.groupIdMainWebsite = apiKeys.webGroupId;
		this.groupId = apiKeys.appGroupId;
	}

	public Faq getHenaCafe(String languageId) throws Exception {
		try {

			return this.getFaqList(languageId);
		} catch (Exception ex) {
			_log.error("Error retrieving FaqResourceImpl page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new Faq();
	}

	public misk.headless.dto.v1_0.Faq getFaqList(String languageId) throws Exception {
		Faq faqObj = new Faq();
		long groupId = this.groupId;

		List<faq.model.Faq> faqListDB = FaqLocalServiceUtil.getFaqs(-1, -1);
		FaqList[] faqList = new FaqList[faqListDB.size()];
		if (Validator.isNotNull(faqListDB)) {
			for (int k = 0; k < faqListDB.size(); k++) {
				FaqList faqListObj = new FaqList();
				faqListObj.setFaqQuestion(faqListDB.get(k).getQuestion(languageId));
				faqListObj.setFaqAnswer(faqListDB.get(k).getAnswer(languageId));
				faqListObj.setStatus(faqListDB.get(k).getActive());
				faqList[k] = faqListObj;
			}

		}
		faqObj.setFaqList(faqList);
		return faqObj;
	}
}