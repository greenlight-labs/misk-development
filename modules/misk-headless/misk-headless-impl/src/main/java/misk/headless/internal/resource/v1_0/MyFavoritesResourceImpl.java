package misk.headless.internal.resource.v1_0;

import attractions.service.AttractionsLocalService;
import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

import com.liferay.portal.kernel.util.Validator;
import highlights.model.Category;
import highlights.model.Tag;
import highlights.service.CategoryLocalServiceUtil;
import highlights.service.TagLocalServiceUtil;
import misk.headless.components.EventComponent;
import misk.headless.components.HighlightComponent;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.Attraction;
import misk.headless.dto.v1_0.Highlight;
import misk.headless.dto.v1_0.RequestStatus;
import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import events.model.Event;
import events.service.EventLocalService;
import favorite.model.Favorite;
import favorite.model.Type;
import favorite.service.FavoriteLocalService;
import favorite.service.TypeLocalService;
import highlights.service.HighlightLocalService;
import misk.headless.dto.v1_0.EventList;
import misk.headless.resource.v1_0.MyFavoritesResource;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/my-favorites.properties", scope = ServiceScope.PROTOTYPE, service = MyFavoritesResource.class)
public class MyFavoritesResourceImpl extends BaseMyFavoritesResourceImpl {
	private static final String EVENTLIST = "Event";
	private static final String HIGHLIGHT = "Highlight";
	private static final String ATTRACTION = "Attraction";
	private String[] types;

	@Override
	public Map<String, Object> getMyFavorites(@NotNull String languageId, Long typeId, @NotNull Long userId)
			throws Exception {

		if (typeId != null) {
			return this.getFavItems(languageId, typeId, userId);
		} else {
			return this.getFavItems(languageId, userId);
		}

	}

	@Override
	public Response postMyFavorite(@NotNull Long itemId, @NotNull Long typeId, @NotNull Long userId) throws Exception {

		if(Validator.isNull(itemId) || Validator.isNull(userId) || Validator.isNull(typeId)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "favourite-fields-required"));
		}

		Favorite favoriteExist = favoriteLocalService.fetchByU_T_I(userId, typeId, itemId);

		if(Validator.isNotNull(favoriteExist)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "favourite-already-exist"));
		}
		Favorite favorite = favoriteLocalService.createFavorite(counterLocalService.increment());

		favorite.setItemId(itemId);
		favorite.setUserId(userId);
		favorite.setTypeId(typeId);

		favorite = favoriteLocalService.addFavorite(favorite);

		RequestStatus requestStatus = new RequestStatus();
		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "favourite-added-successful"));
		Response.ResponseBuilder responseBuilder = Response.ok();
		responseBuilder.entity(requestStatus);
		return responseBuilder.build();
	}

	@Override
	public Response deleteFavorite(Long itemId, Long typeId, Long userId) throws Exception {

		if(Validator.isNull(itemId) || Validator.isNull(userId) || Validator.isNull(typeId)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "favourite-fields-required"));
		}

		Favorite favorite = favoriteLocalService.fetchByU_T_I(userId, typeId, itemId);
		if(Validator.isNull(favorite)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "no-record-found"));
		}

		favorite = favoriteLocalService.deleteFavorite(favorite);

		if(Validator.isNull(favorite)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "favourite-delete-error"));
		}

		RequestStatus requestStatus = new RequestStatus();
		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "favourite-delete-successful"));

		Response.ResponseBuilder responseBuilder = Response.ok();
		responseBuilder.entity(requestStatus);
		return responseBuilder.build();
	}

	private Map getFavItems(@NotNull String languageId, @NotNull Long userId) throws PortalException {

		Map<String, List> fevItemsMap = new HashMap<>();

		List<EventList> eventListObject = new ArrayList<>();
		List<Highlight> highlightListObject = new ArrayList<>();
		List<Attraction> attractionListObject = new ArrayList<>();

		List<Favorite> favoriteEvents = favoriteLocalService.getUserfavorites(userId);

		favoriteEvents.forEach(favoriteEvent -> {
			Type type = typeLocalService.fetchType(favoriteEvent.getTypeId());

			if(Validator.isNotNull(type)){
				addFavouriteItem(languageId, eventListObject, highlightListObject, attractionListObject, favoriteEvent, type);
			}
		});
		fevItemsMap.put("Events", eventListObject);
		fevItemsMap.put("Highlights", highlightListObject);
		//fevItemsMap.put("Attractions", attractionListObject);
		return fevItemsMap;
	}

	private Map getFavItems(@NotNull String languageId, Long typeId, @NotNull Long userId) throws PortalException {

		Map<String, List> fevItemsMap = new HashMap<>();

		List<EventList> eventListObject = new ArrayList<>();
		List<Highlight> highlightListObject = new ArrayList<>();
		List<Attraction> attractionListObject = new ArrayList<>();

		List<Favorite> favoriteEvents = favoriteLocalService.getFavorites(typeId, userId);
		_log.debug(favoriteEvents);

		Type type = typeLocalService.fetchType(typeId);

		if(Validator.isNull(type)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "favourite-type-not-found"));
		}

		favoriteEvents.forEach(favoriteEvent -> {
			addFavouriteItem(languageId, eventListObject, highlightListObject, attractionListObject, favoriteEvent, type);
		});
		fevItemsMap.put("Events", eventListObject);
		fevItemsMap.put("Highlights", highlightListObject);
		//fevItemsMap.put("Attractions", attractionListObject);
		return fevItemsMap;
	}

	private void addFavouriteItem(String languageId, List<EventList> eventListObject, List<Highlight> highlightListObject, List<Attraction> attractionListObject, Favorite favoriteEvent, Type type) {
		if (type.getName().equalsIgnoreCase(EVENTLIST)) {
			EventList favItem = getFavouriteEvent(languageId, favoriteEvent);
			if(Validator.isNotNull(favItem)){
				eventListObject.add(favItem);
			}
		} else if (type.getName().equalsIgnoreCase(HIGHLIGHT)) {
			Highlight favItem = getFavouriteHighlight(languageId, favoriteEvent);
			if(Validator.isNotNull(favItem)){
				highlightListObject.add(favItem);
			}
		} else if (type.getName().equalsIgnoreCase(ATTRACTION)) {
			Attraction favItem = getFavouriteAttraction(languageId, favoriteEvent);
			if(Validator.isNotNull(favItem)){
				attractionListObject.add(favItem);
			}
		}
	}

	protected void getTypes() {
		/*
		String[] types = new String[0];
		int count = 0;
		List<Type> records = typeLocalService.getTypes(0, 50);
		for(Type record : records){
			types[] = record.getName();
			count++;
		}
		return types;*/
		this.types = new String[]{"Event", "Highlight", "Attraction"};
	}

	protected EventList getFavouriteEvent(String languageId, Favorite favoriteEvent){

		try {
			Event favItem = eventLocalService.getEvent(favoriteEvent.getItemId());

			return EventComponent.getDTO(languageId, favItem, this.contextUriInfo);
		} catch (PortalException e) {
			return null;
		}
	}

	protected Highlight getFavouriteHighlight(String languageId, Favorite favoriteEvent){

		try {
			highlights.model.Highlight favItem = highlightLocalService.getHighlight(favoriteEvent.getItemId());

			Category category = CategoryLocalServiceUtil.fetchCategory(favItem.getCategoryId());
			Tag tag = TagLocalServiceUtil.fetchTag(favItem.getTagId());

			return HighlightComponent.getDTO(languageId, this.contextUriInfo, favItem, category, tag);
		} catch (PortalException e) {
			return null;
		}
	}

	protected Attraction getFavouriteAttraction(String languageId, Favorite favoriteEvent){

		try {
			attractions.model.Attractions favItem = attractionsLocalService.getAttractions(favoriteEvent.getItemId());

			Attraction item = new Attraction();
			item.setAttractionId(String.valueOf(favItem.getAttractionsId()));
			item.setAttractionBanner(ApiKeys.getBaseURL(this.contextUriInfo) + favItem.getForumBannerImage(languageId));
			item.setAttractionTitle(favItem.getForumBannerTitle(languageId));
			item.setAttractionButtonLabel(favItem.getForumButtonLabel(languageId));
			item.setAttractionBannerIcon(ApiKeys.getBaseURL(this.contextUriInfo) + favItem.getForumBannerIcon(languageId));
			item.setBannerButtonColor(favItem.getForumButtonColor(languageId));
			item.setWorkingHoursLabel(favItem.getWorkingHoursLabel(languageId));
			item.setWorkingHours(favItem.getWorkingHours(languageId));
			item.setWorkingDays(favItem.getWorkingDays(languageId));
			item.setDescription(favItem.getDescription(languageId));
			item.setWorkingHoursLabelColor(favItem.getWorkingHoursLabelColor(languageId));
			item.setWorkingHoursImage(ApiKeys.getBaseURL(this.contextUriInfo) + favItem.getWorkingHoursImage(languageId));
			item.setWorkingDaysImage(ApiKeys.getBaseURL(this.contextUriInfo) + favItem.getWorkingDaysImage(languageId));

			return item;
		} catch (PortalException e) {
			return null;
		}
	}

	Log _log = LogFactoryUtil.getLog(MyFavoritesResourceImpl.class);
	@Reference
	FavoriteLocalService favoriteLocalService;
	@Reference
	TypeLocalService typeLocalService;
	@Reference
	EventLocalService eventLocalService;
	@Reference
	HighlightLocalService highlightLocalService;
	@Reference
	AttractionsLocalService attractionsLocalService;
	@Reference
	CounterLocalService counterLocalService;

}