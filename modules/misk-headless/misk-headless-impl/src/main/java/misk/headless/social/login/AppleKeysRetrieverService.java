package misk.headless.social.login;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

public class AppleKeysRetrieverService {

	public AppleKeysResponse sendRetriveRequest(String retriveAppleKeysUrl) {
		Client client = ClientBuilder.newClient();
		WebTarget myResource = client.target(retriveAppleKeysUrl);
		String appleKeysResponse = myResource.request(MediaType.APPLICATION_JSON).get(String.class);
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		AppleKeysResponse res = null;
		try {
			res = objectMapper.readValue(appleKeysResponse, AppleKeysResponse.class);

			return res;
		} catch (Exception e) {

			return null;
		}
	}

}
