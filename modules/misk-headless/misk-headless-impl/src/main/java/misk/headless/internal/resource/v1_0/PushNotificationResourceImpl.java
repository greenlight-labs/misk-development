package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.pagination.Page;
import com.liferay.portal.vulcan.pagination.Pagination;
import com.push.notification.constants.PushNotificationKeys;
import com.push.notification.service.PushNotificationLocalService;
import com.push.notification.service.PushNotificationReadStatusLocalService;
import com.push.notification.service.persistence.PushNotificationFinder;
import com.push.notification.service.persistence.impl.PushNotificationFinderImpl;
import highlights.model.Category;
import highlights.model.Tag;
import highlights.model.impl.HighlightImpl;
import highlights.service.CategoryLocalServiceUtil;
import highlights.service.HighlightLocalServiceUtil;
import highlights.service.TagLocalServiceUtil;
import misk.app.users.service.AppUserLocalService;
import misk.headless.components.HighlightComponent;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.PushNotificationResource;

import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import javax.validation.Valid;
import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/push-notification.properties",
	scope = ServiceScope.PROTOTYPE, service = PushNotificationResource.class
)
public class PushNotificationResourceImpl
	extends BasePushNotificationResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HomeResourceImpl.class);

	@Override
	public Page<PushNotification> getPushNotifications(Long appUserId, Pagination pagination)
			throws Exception {

		try {
			int totalCount = _pushNotificationReadStatusLocalService.countByAppUserId(appUserId);
			List<com.push.notification.model.PushNotificationReadStatus> appUserRecords = _pushNotificationReadStatusLocalService.findByAppUserId(appUserId);
			// get notification ids array from appUserRecords
			long[] notificationIds = new long[appUserRecords.size()];
			for (com.push.notification.model.PushNotificationReadStatus appUserRecord : appUserRecords) {
				notificationIds[appUserRecords.indexOf(appUserRecord)] = appUserRecord.getNotificationId();
			}
			// get notifications from notificationIds
			List<com.push.notification.model.PushNotification> records = _pushNotificationLocalService.findByNotificationIds(notificationIds, pagination.getStartPosition(), pagination.getEndPosition());
			//List<com.push.notification.model.PushNotification> records = _pushNotificationLocalService.getPushNotifications(pagination.getStartPosition(), pagination.getEndPosition());
			List<PushNotification> results = new ArrayList<>();
			//get language id from header
			String languageId = CommonUtil.getHeaderLanguage(contextHttpServletRequest);

			for (com.push.notification.model.PushNotification curRecord : records) {
				PushNotification result = new PushNotification();
				result.setNotificationId(curRecord.getNotificationId());
				result.setType(curRecord.getType());
				//get push notification type label
				result.setTypeLabel(getNotificationTypeLabel(curRecord.getType()));
				result.setTypeId(curRecord.getTypeId());
				result.setTitle(curRecord.getTitle(languageId));
				result.setBody(curRecord.getBody(languageId));
				// get readStatus from appUserRecords by comparing curRecord.getNotificationId()
				for (com.push.notification.model.PushNotificationReadStatus appUserRecord : appUserRecords) {
					if (appUserRecord.getNotificationId() == curRecord.getNotificationId()) {
						result.setReadStatus(appUserRecord.getReadStatus());
						break;
					}
				}
				if (curRecord.getType().equals(PushNotificationKeys.NOTIFICATION_TYPE_HIGHLIGHT)) {
					highlights.model.Highlight highlight = HighlightLocalServiceUtil.fetchHighlight(curRecord.getTypeId());
					if(Validator.isNotNull(highlight)) {
						//@MY_TODO: need to remove category query form loop, add it outer loop, fetch and store in array
						Category category = CategoryLocalServiceUtil.fetchCategory(highlight.getCategoryId());
						//@MY_TODO: need to remove tag query form loop, add it outer loop, fetch and store in array
						Tag tag = TagLocalServiceUtil.fetchTag(highlight.getTagId());
						result.setHighlight(HighlightComponent.getDTO("en_US", this.contextUriInfo, highlight, category, tag));
					}
				}
				results.add(result);
			}

			return Page.of(results, pagination, totalCount);
		} catch (Exception ex) {
			_log.error("Error retrieving push notification data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}

		return Page.of(Collections.emptyList());
	}

	private String getNotificationTypeLabel(String type) {
		String typeLabel = "";
		switch (type) {
			case PushNotificationKeys.NOTIFICATION_TYPE_NEWS:
				typeLabel = CommonUtil.getLanguageTranslation(contextHttpServletRequest, "notification-type-label-news");
				break;
			case PushNotificationKeys.NOTIFICATION_TYPE_EVENT:
				typeLabel = CommonUtil.getLanguageTranslation(contextHttpServletRequest, "notification-type-label-event");
				break;
			case PushNotificationKeys.NOTIFICATION_TYPE_EVENT_UPDATE:
				typeLabel = CommonUtil.getLanguageTranslation(contextHttpServletRequest, "notification-type-label-event-update");
				break;
			case PushNotificationKeys.NOTIFICATION_TYPE_HIGHLIGHT:
				typeLabel = CommonUtil.getLanguageTranslation(contextHttpServletRequest, "notification-type-label-highlight");
				break;
			case PushNotificationKeys.NOTIFICATION_TYPE_BROADCAST_MESSAGE:
				typeLabel = CommonUtil.getLanguageTranslation(contextHttpServletRequest, "notification-type-label-broadcast-message");
				break;
		}
		return typeLabel;
	}

	@Override
	public PushNotification updateReadStatus(PushNotification pushNotification)
			throws Exception {

		if(Validator.isNull(pushNotification.getNotificationId()) ||
				Validator.isNull(pushNotification.getAppUserId()) ||
				Validator.isNull(pushNotification.getReadStatus())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "update-read-status-fields-required"));
		}

		com.push.notification.model.PushNotificationReadStatus entry = _pushNotificationReadStatusLocalService.fetchByAppUserNotification(pushNotification.getAppUserId(), pushNotification.getNotificationId());

		if(Validator.isNull(entry)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "update-read-status-record-not-found"));
		}

		entry.setReadStatus(pushNotification.getReadStatus());
		com.push.notification.model.PushNotificationReadStatus updatedEntry = _pushNotificationReadStatusLocalService.updatePushNotificationReadStatus(entry);

		if(Validator.isNull(updatedEntry)){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "update-read-status-failed"));
		}

		RequestStatus requestStatus = new RequestStatus();
		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "read-status-updated-successful"));

		PushNotification pushNotification1 = new PushNotification();
		pushNotification1.setRequestStatus(requestStatus);
		return pushNotification1;
	}

	@Override
	public UnreadNotificationCount getUnreadNotificationCount(Long appUserId) throws Exception {
		UnreadNotificationCount unreadNotificationCount = new UnreadNotificationCount();
		unreadNotificationCount.setUnreadCount(0);
		try {
			int unreadCount = _pushNotificationReadStatusLocalService.countByAppUserReadStatus(appUserId, false);
			unreadNotificationCount.setUnreadCount(unreadCount);
			return unreadNotificationCount;
		} catch (Exception ex) {
			_log.error("Error retrieving push notification unread count data: " + ex.getMessage(), ex);
			_log.error(ex.getMessage());
		}

		return unreadNotificationCount;
	}

	@Reference
	protected AppUserLocalService _appUserLocalService;

	@Reference
	protected PushNotificationLocalService _pushNotificationLocalService;

	@Reference
	protected PushNotificationReadStatusLocalService _pushNotificationReadStatusLocalService;

	@Reference
	protected PushNotificationFinder _pushNotificationFinder;
}