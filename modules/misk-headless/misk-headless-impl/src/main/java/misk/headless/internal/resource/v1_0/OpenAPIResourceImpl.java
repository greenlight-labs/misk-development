package misk.headless.internal.resource.v1_0;

import com.liferay.portal.vulcan.resource.OpenAPIResource;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Generated;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/openapi.properties",
	service = OpenAPIResourceImpl.class
)
@Generated("")
@OpenAPIDefinition(
	info = @Info(description = "Misk Headless APIs for Mobile Applications (Android, iOS)", license = @License(name = "Apache 2.0", url = "http://www.apache.org/licenses/LICENSE-2.0.html"), title = "Misk Headless", version = "v1.0")
)
@Path("/v1.0")
public class OpenAPIResourceImpl {

	@GET
	@Path("/openapi.{type:json|yaml}")
	@Produces({MediaType.APPLICATION_JSON, "application/yaml"})
	public Response getOpenAPI(@PathParam("type") String type)
		throws Exception {

		try {
			Class<? extends OpenAPIResource> clazz =
				_openAPIResource.getClass();

			clazz.getMethod(
				"getOpenAPI", Set.class, String.class, UriInfo.class);
		}
		catch (NoSuchMethodException noSuchMethodException) {
			return _openAPIResource.getOpenAPI(_resourceClasses, type);
		}

		return _openAPIResource.getOpenAPI(_resourceClasses, type, _uriInfo);
	}

	@Reference
	private OpenAPIResource _openAPIResource;

	@Context
	private UriInfo _uriInfo;

	private final Set<Class<?>> _resourceClasses = new HashSet<Class<?>>() {
		{
			add(AccountResourceImpl.class);

			add(AppUserResourceImpl.class);

			add(AttractionResourceImpl.class);

			add(BannerImagesResourceImpl.class);

			add(BookingResourceImpl.class);

			add(CommunityWallResourceImpl.class);

			add(ContactResourceImpl.class);

			add(CourtResourceImpl.class);

			add(EventListResourceImpl.class);

			add(ExperientialCenterResourceImpl.class);

			add(ExploreResourceImpl.class);

			add(ExploreExperientialCenterResourceImpl.class);

			add(ExploreWadiCResourceImpl.class);

			add(FaqResourceImpl.class);

			add(FeedbackResourceImpl.class);

			add(GlobalSearchResourceImpl.class);

			add(HenaCafeResourceImpl.class);

			add(HomeResourceImpl.class);

			add(InvixiumResourceImpl.class);

			add(LegalPageResourceImpl.class);

			add(MapLocationResourceImpl.class);

			add(MiskForumResourceImpl.class);

			add(MoreResourceImpl.class);

			add(MyFavoritesResourceImpl.class);

			add(NotificationSettingsResourceImpl.class);

			add(ProfileResourceImpl.class);

			add(ProfileDocumentResourceImpl.class);

			add(ProfileDocumentListResourceImpl.class);

			add(PushNotificationResourceImpl.class);

			add(SouvenirsResourceImpl.class);

			add(StoryResourceImpl.class);

			add(TermsnConditionsResourceImpl.class);

			add(WadiCResourceImpl.class);

			add(WelcomeResourceImpl.class);

			add(OpenAPIResourceImpl.class);
		}
	};

}