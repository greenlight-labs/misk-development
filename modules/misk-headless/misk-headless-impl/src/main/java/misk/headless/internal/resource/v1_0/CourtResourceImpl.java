package misk.headless.internal.resource.v1_0;

import bookings.model.impl.BookingModelImpl;
import bookings.service.BookingLocalService;
import bookings.service.persistence.BookingPersistence;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.pagination.Page;
import courts.service.CategoryLocalService;
import courts.service.CourtLocalService;
import courts.service.LocationLocalService;
import courts.service.persistence.CourtPersistence;
import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.CourtResource;
import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/court.properties",
	scope = ServiceScope.PROTOTYPE, service = CourtResource.class
)
public class CourtResourceImpl extends BaseCourtResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HomeResourceImpl.class);

	@Override
	public CourtItems getCourtItems(String languageId) throws Exception {
		CourtItems courtItems = new CourtItems();
		courtItems.setCourtLocations(this.getCourtLocations(languageId));
		courtItems.setCourtCategories(this.getCourtCategories(languageId));
		courtItems.setCourts(this.getCourts(languageId));
		return courtItems;
	}

	@Override
	public Court getCourtDetails(Long courtId, String languageId, Long appUserId) throws Exception {

		courts.model.Court record = _courtLocalService.fetchCourt(courtId);

		if (Validator.isNull(record)) {
			throw new BadRequestException("No record found with this id.");
		}

		AppUser appUser = _appUserLocalService.fetchAppUser(appUserId);

		if (Validator.isNull(appUser)) {
			throw new BadRequestException("No app user exist with this id.");
		}

		List<bookings.model.Booking> bookings = _bookingLocalService.findByCourt_AppUser(courtId, appUserId);
		CourtBooking courtBooking = new CourtBooking();
		if(bookings.size() > 0){
			bookings.model.Booking booking = bookings.get(0);

			courtBooking.setBookingId(booking.getBookingId());
			courtBooking.setBookingDate(CommonUtil.convertDateTimeToStringDate(booking.getSlotStartTime()));
			courtBooking.setSlotStartTime(CommonUtil.convertDateTimeToStringTime(booking.getSlotStartTime()));
			courtBooking.setSlotEndTime(CommonUtil.convertDateTimeToStringTime(booking.getSlotEndTime()));
		}

		courts.model.Category category = _categoryLocalService.fetchCategory(record.getCategoryId());
		CourtCategory courtCategory = new CourtCategory();
		courtCategory.setIcon(ApiKeys.getBaseURL(this.contextUriInfo) + category.getIcon());
		courtCategory.setBlackIcon(ApiKeys.getBaseURL(this.contextUriInfo) + category.getBlackIcon());

		String[] images = {
				ApiKeys.getBaseURL(this.contextUriInfo) + record.getDetailImage(),
				ApiKeys.getBaseURL(this.contextUriInfo) + record.getDetailImage(),
				ApiKeys.getBaseURL(this.contextUriInfo) + record.getDetailImage()};

		Court court = new Court();
		court.setId(record.getCourtId());
		court.setCategoryId(record.getCategoryId());
		court.setHeading(record.getHeading(languageId));
		court.setName(record.getName(languageId));
		court.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + record.getListingImage());
		court.setDetailImages(images);
		court.setVenue(record.getVenue(languageId));
		court.setOpeningHours(record.getOpeningHours(languageId));
		court.setOpeningDays(record.getOpeningDays(languageId));
		court.setDescription(record.getDescription(languageId));
		court.setLat(record.getLat());
		court.setLon(record.getLon());
		court.setContactEmail(record.getContactEmail());
		court.setContactPhone(record.getContactPhone());
		court.setCourtBooking(courtBooking);
		court.setCourtCategory(courtCategory);
		return court;
	}

	/* **************************************************************************** */

	private CourtLocation[] getCourtLocations(String languageId){
		List<courts.model.Location> records = _locationLocalService.findAllInGroup(this.appGroupId);
		CourtLocation[] results = new CourtLocation[records.size()];
		int i = 0;

		for (courts.model.Location curRecord : records) {
			CourtLocation result = new CourtLocation();
			result.setId(curRecord.getLocationId());
			result.setName(curRecord.getName(languageId));
			results[i] = result;
			i++;
		}
		return results;
	}

	private CourtCategory[] getCourtCategories(String languageId) {
		List<courts.model.Category> records = _categoryLocalService.findAllInGroup(this.appGroupId);
		CourtCategory[] results = new CourtCategory[records.size()];
		int i = 0;

		for (courts.model.Category curRecord : records) {
			CourtCategory result = new CourtCategory();
			result.setId(curRecord.getCategoryId());
			result.setLocationId(curRecord.getLocationId());
			result.setName(curRecord.getName(languageId));
			result.setIcon(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getIcon());
			result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getImage());
			results[i] = result;
			i++;
		}
		return results;
	}

	private Court[] getCourts(String languageId) {
		List<courts.model.Court> records = _courtLocalService.findAllInGroup(this.appGroupId);
		Court[] results = new Court[records.size()];
		int i = 0;

		for (courts.model.Court curRecord : records) {

			//@MY_TODO: create dynamic slider at backend and fetch images from there
			String[] images = {
					ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage(),
					ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage(),
					ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage()};

			Court result = new Court();
			result.setId(curRecord.getCourtId());
			result.setCategoryId(curRecord.getCategoryId());
			result.setName(curRecord.getName(languageId));
			result.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getListingImage());
			result.setDetailImages(images);
			result.setVenue(curRecord.getVenue(languageId));
			result.setOpeningHours(curRecord.getOpeningHours(languageId));
			result.setOpeningDays(curRecord.getOpeningDays(languageId));
			result.setDescription(curRecord.getDescription(languageId));
			result.setLat(curRecord.getLat());
			result.setLon(curRecord.getLon());
			result.setContactEmail(curRecord.getContactEmail());
			result.setContactPhone(curRecord.getContactPhone());
			results[i] = result;
			i++;
		}
		return results;
	}

	public Page<Court> getRelatedCourts(Long categoryId, Long courtId, String languageId) throws Exception {


		try {
			List<courts.model.Court> records = _courtLocalService.findAllInCategory(categoryId);
			List<Court> results = new ArrayList<>();

			for (courts.model.Court curRecord : records) {
				if(curRecord.getCourtId() == courtId){
					continue;
				}

				//@MY_TODO: create dynamic slider at backend and fetch images from there
				String[] images = {
						ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage(),
						ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage(),
						ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getDetailImage()};

				Court result = new Court();
				result.setId(curRecord.getCourtId());
				result.setCategoryId(curRecord.getCategoryId());
				result.setName(curRecord.getName(languageId));
				result.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getListingImage());
				result.setDetailImages(images);
				result.setVenue(curRecord.getVenue(languageId));
				result.setOpeningHours(curRecord.getOpeningHours(languageId));
				result.setOpeningDays(curRecord.getOpeningDays(languageId));
				result.setDescription(curRecord.getDescription(languageId));
				result.setLat(curRecord.getLat());
				result.setLon(curRecord.getLon());
				result.setContactEmail(curRecord.getContactEmail());
				result.setContactPhone(curRecord.getContactPhone());
				results.add(result);
			}

			return Page.of(results);
		} catch (Exception ex) {
			_log.error("Error retrieving related courts data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	@Reference
	protected LocationLocalService _locationLocalService;

	@Reference
	protected CategoryLocalService _categoryLocalService;

	@Reference
	protected CourtLocalService _courtLocalService;

	@Reference
	protected AppUserLocalService _appUserLocalService;

	@Reference
	protected CourtPersistence _courtPersistence;

	@Reference
	protected BookingLocalService _bookingLocalService;
}