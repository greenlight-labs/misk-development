package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.vulcan.pagination.Page;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.Welcome;
import misk.headless.resource.v1_0.WelcomeResource;

import misk.headless.util.DocumentMediaUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/welcome.properties",
	scope = ServiceScope.PROTOTYPE, service = WelcomeResource.class
)
public class WelcomeResourceImpl extends BaseWelcomeResourceImpl {
	Log _log = LogFactoryUtil.getLog(WelcomeResourceImpl.class);
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	@Override
	public Page<Welcome> getWelcomeScreens(String languageId)
			throws Exception {
		try {
			return Page.of(this.getScreens(languageId));
		} catch (Exception ex) {
			_log.error("Error listing onboarding screen data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	public List<Welcome> getScreens(String languageId) throws Exception{
		List<Welcome> results = new ArrayList<>();

		String articleId = "APP_ONBOARDING";

		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(this.appGroupId, articleId, 0 );
		String articleContent = article.getContentByLocale(languageId);

		Document document = SAXReaderUtil.read(articleContent);
		List<Node> nodes = document.selectNodes("/root/dynamic-element[@name='Title']");
		for (Node node: nodes) {
			Node titleNode = node.selectSingleNode("dynamic-content");
			Node descriptionNode = node.selectSingleNode("dynamic-element[@name='Description']/dynamic-content");
			Node imageNode = node.selectSingleNode("dynamic-element[@name='Image1cxa']/dynamic-content");

			Welcome result = new Welcome();
			result.setTitle(titleNode.getText());
			result.setDescription(descriptionNode.getText());

			DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
			result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(imageNode.getText()));

			results.add(result);

			/*String title = StringPool.BLANK;
			if (Validator.isNotNull(node) && node.getText().length() > 0) {
				title = titleNode.getText();
			}*/
		}

		return results;
	}
}