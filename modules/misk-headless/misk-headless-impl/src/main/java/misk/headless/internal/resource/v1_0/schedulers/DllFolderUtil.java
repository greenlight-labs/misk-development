package misk.headless.internal.resource.v1_0.schedulers;

import com.liferay.document.library.kernel.exception.NoSuchFileEntryException;
import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.model.DLVersionNumberIncrease;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import misk.headless.constants.ApiKeys;

public class DllFolderUtil {

	static Log log = LogFactoryUtil.getLog(DllFolderUtil.class);

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	public static final int DEFAULT_BUFFER_SIZE = 8192;
	private static final String UTF = "UTF-8";

	public static void createDLFolder(String folderName, String description, String eventCardsJsonString,
			ServiceContext serviceContext) throws PortalException, IOException {
		long userId = 20130;
		long groupId = 20124;
		long repositoryId = 20124;
		boolean mountPoint = false;
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		boolean hidden = false;

		try {
			DLFolder dlFolder = DLFolderLocalServiceUtil.getFolder(groupId, 0, folderName);
			fileUploadByDL(folderName, eventCardsJsonString, dlFolder, serviceContext);
		} catch (NoSuchFolderException nsfe) {
			DLFolder dlFolder = DLFolderLocalServiceUtil.addFolder(userId, groupId, repositoryId, mountPoint,
					parentFolderId, folderName, description, hidden, serviceContext);
			fileUploadByDL(folderName, eventCardsJsonString, dlFolder, serviceContext);
		}

	}

	public static void fileUploadByDL(String folderName, String content, DLFolder dlFolder,
			ServiceContext serviceContext) throws IOException {

		InputStream stream = new ByteArrayInputStream(content.getBytes(Charset.forName(UTF)));
		File file = FileUtil.createTempFile("Test", "json");
		FileUtil.write(file, stream);

		String mimeType = MimeTypesUtil.getContentType(file);
		if (log.isInfoEnabled()) {
			log.info(file.getTotalSpace());
			log.info("Exist=>" + file.exists());
			log.info(mimeType);
		}
		String fileName = "Event Cards.json";
		String title = "Event Cards";
		String description = "This file is added via programatically";
		String createdChangeLog = "Created Log";
		String updatedChangeLog = "updated Log";
		try {

			InputStream is = stream;
			try {
				DLFileEntry dlList = DLFileEntryLocalServiceUtil.getFileEntry(serviceContext.getScopeGroupId(),
						dlFolder.getFolderId(), title);
				if (log.isInfoEnabled())
					log.info(" update few");
				DLFileEntryLocalServiceUtil.updateFileEntry(serviceContext.getUserId(), dlList.getFileEntryId(),
						fileName, mimeType, title, description, updatedChangeLog, DLVersionNumberIncrease.AUTOMATIC,
						dlList.getFileEntryTypeId(), null, file, is, (file.getTotalSpace() / 1024) / 1024,
						serviceContext);
			} catch (NoSuchFileEntryException e) {
				if (log.isInfoEnabled())
					log.info("No File - create new");
				long fileEntryTypeId = dlFolder.getDefaultFileEntryTypeId();

				DLFileEntryLocalServiceUtil.addFileEntry(serviceContext.getUserId(), serviceContext.getScopeGroupId(),
						serviceContext.getScopeGroupId(), dlFolder.getFolderId(), fileName, mimeType, title,
						description, createdChangeLog, fileEntryTypeId, null, file, is,
						(file.getTotalSpace() / 1024) / 1024, serviceContext);
			} finally {
				is.close();
				FileUtil.delete(file);
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}

	}

	public static String convertInputStreamToString(InputStream is) throws IOException {

		ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
		int length;
		while ((length = is.read(buffer)) != -1) {
			result.write(buffer, 0, length);
		}

		return result.toString(UTF);
	}

}
