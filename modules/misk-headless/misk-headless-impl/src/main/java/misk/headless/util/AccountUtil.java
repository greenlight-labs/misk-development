package misk.headless.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalService;
import misk.app.users.service.AppUserLocalServiceUtil;
import misk.headless.constants.AccountKeys;
import misk.headless.dto.v1_0.RequestStatus;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.UUID;


@Component(immediate = true, service = AccountUtil.class)
public class AccountUtil {
    private static final Log _log = LogFactoryUtil.getLog(AccountUtil.class);

    public static String generateResetToken() {
        return UUID.randomUUID().toString();
    }

    // generate 4 digit passcode
    public static String generatePasscode() {
        Random random = new Random();
        return String.format("%04d", random.nextInt(10000));
    }

    public static void isEmailAddressVerified(AppUser appUser, HttpServletRequest contextHttpServletRequest) {
        RequestStatus requestStatus = new RequestStatus();
        if(! appUser.getEmailAddressVerified()){
            requestStatus.setStatus(false);
            requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-not-verified"));
            requestStatus.setCode(AccountKeys.EMAIL_NOT_VERIFIED_CODE);

            throw new BadRequestException(
                    Response.status(Response.Status.BAD_REQUEST)
                            .entity(requestStatus)
                            .build()
            );
        }
    }

    public static void isPhoneNumberVerified(AppUser appUser, HttpServletRequest contextHttpServletRequest) {
        RequestStatus requestStatus = new RequestStatus();
        if(! appUser.getPhoneNumberVerified()){
            requestStatus.setStatus(false);
            requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-not-verified"));
            requestStatus.setCode(AccountKeys.PHONE_NOT_VERIFIED_CODE);

            throw new BadRequestException(
                    Response.status(Response.Status.BAD_REQUEST)
                            .entity(requestStatus)
                            .build()
            );
        }
    }

    public static void sendOTPEmail(misk.app.users.model.AppUser appUser) {
        // send otp to email
        SendEmailUtil.resendOTP(appUser.getFullName(), appUser.getEmailAddress(), appUser.getPasscode());
    }

    public static Boolean sendOTPPhone(misk.app.users.model.AppUser appUser) {

        return false;
    }

    public static void isValidForgotPasswordRequest(AppUser appUser, HttpServletRequest contextHttpServletRequest) {

        Instant then = appUser.getPasswordModifiedDate().toInstant();
        Instant now = Instant.now();
        Instant twentyFourHoursEarlier = now.minus( 24 , ChronoUnit.HOURS );
        // Is that moment (a) not before 24 hours ago, AND (b) before now (not in the future)?
        boolean within24Hours = ( ! then.isBefore( twentyFourHoursEarlier ) ) &&  then.isBefore( now ) ;

        RequestStatus requestStatus = new RequestStatus();
        if(within24Hours){
            requestStatus.setStatus(false);
            requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "not-valid-forgot-password-request"));
            requestStatus.setCode(AccountKeys.NOT_VALID_FORGOT_PASSWORD_REQUEST_CODE);

            //@MY_TODO: remove comment to throw 24 hours exception
            /*throw new BadRequestException(
                    Response.status(Response.Status.BAD_REQUEST)
                            .entity(requestStatus)
                            .build()
            );*/
        }
    }

    public static void isValidEditProfileRequest(AppUser appUser, HttpServletRequest contextHttpServletRequest) {

        Instant then = appUser.getModifiedDate().toInstant();
        Instant now = Instant.now();
        Instant twentyFourHoursEarlier = now.minus( 24 , ChronoUnit.HOURS );
        // Is that moment (a) not before 24 hours ago, AND (b) before now (not in the future)?
        boolean within24Hours = ( ! then.isBefore( twentyFourHoursEarlier ) ) &&  then.isBefore( now ) ;

        RequestStatus requestStatus = new RequestStatus();
        if(within24Hours){
            requestStatus.setStatus(false);
            requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "not-valid-edit-profile-request"));
            requestStatus.setCode(AccountKeys.NOT_VALID_EDIT_PROFILE_REQUEST_CODE);

            //@MY_TODO: remove comment to throw 24 hours exception
            /*throw new BadRequestException(
                    Response.status(Response.Status.BAD_REQUEST)
                            .entity(requestStatus)
                            .build()
            );*/
        }
    }

    public static void isValidChangePhoneRequest(AppUser appUser, String phoneNumber, HttpServletRequest contextHttpServletRequest) {

        misk.app.users.model.AppUser phoneUser = AppUserLocalServiceUtil.fetchByPhoneNumber(phoneNumber);

        RequestStatus requestStatus = new RequestStatus();
        if(Validator.isNotNull(phoneUser) && phoneUser.getAppUserId() != appUser.getAppUserId()){
            requestStatus.setStatus(false);
            requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "not-valid-change-phone-request"));
            requestStatus.setCode(AccountKeys.NOT_VALID_CHANGE_PHONE_REQUEST_CODE);

            throw new BadRequestException(
                    Response.status(Response.Status.BAD_REQUEST)
                            .entity(requestStatus)
                            .build()
            );
        }
    }

    public static void isAccountDeactivated(AppUser appUser, HttpServletRequest contextHttpServletRequest) {
        RequestStatus requestStatus = new RequestStatus();
        misk.headless.dto.v1_0.AppUser appUser1 = new misk.headless.dto.v1_0.AppUser();
        if(! appUser.getStatus() && ! appUser.getPhoneNumberVerified() && StringUtils.isNotEmpty(appUser.getDeactivateReason())){
            requestStatus.setStatus(false);
            requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-is-deactivated"));
            requestStatus.setCode(AccountKeys.ACCOUNT_IS_DEACTIVATED_CODE);
            appUser1.setRequestStatus(requestStatus);
            appUser1.setAppUserId(appUser.getAppUserId());
            appUser1.setPhoneNumber(appUser.getPhoneNumber());

            throw new BadRequestException(
                    Response.status(Response.Status.BAD_REQUEST)
                            .entity(appUser1)
                            .build()
            );
        }
    }

    public static Boolean updatePhoneNumber(misk.app.users.model.AppUser appUser) {

        return false;
    }

    public static Boolean updateEmailAddress(misk.app.users.model.AppUser appUser) {

        return false;
    }

    @Reference
    private AppUserLocalService _appUserLocalService;

}
