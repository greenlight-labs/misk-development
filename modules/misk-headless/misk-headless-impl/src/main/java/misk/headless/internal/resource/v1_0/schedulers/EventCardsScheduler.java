package misk.headless.internal.resource.v1_0.schedulers;

import com.liferay.document.library.kernel.exception.DuplicateFolderNameException;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.scheduler.SchedulerEngineHelperUtil;
import com.liferay.portal.kernel.scheduler.SchedulerEntryImpl;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.Trigger;
import com.liferay.portal.kernel.scheduler.TriggerFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.Map;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;

import misk.headless.constants.ApiKeys;

@Component(immediate = true, property = { "cron.expression= 0 0 * ? * *" // scheduler runs every 1 Hour.
}, service = EventCardsScheduler.class)
public class EventCardsScheduler extends BaseMessageListener {

	Log log = LogFactoryUtil.getLog(getClass());
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	public long userId = 20130;
	String EVENT_END_POINT = "https://app.the-wallrus.com/events/25781/cards.json?approved=true&source=all";
	String folderName = "Event Cards";
	String folderDescription = "Event Cards";

	@Override
	protected void doReceive(Message message) throws Exception {
		if (log.isInfoEnabled())
			log.info("Scheduled task executed...");

		HttpURLConnection connection = null;
		InputStream inputStream = null;

		String JsonCards = StringPool.BLANK;

		try {
			connection = (HttpURLConnection) new URL(EVENT_END_POINT).openConnection();
			inputStream = connection.getInputStream();
			try {
				JsonCards = DllFolderUtil.convertInputStreamToString(inputStream);
			} catch (IOException e) {
				log.error("unable to convert api response to String " + e.getMessage());
			}
		} catch (Exception e1) {
			log.error("http Connection issue in external endpoint call" + e1);
		}

		try {

			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setUserId(userId);
			serviceContext.setScopeGroupId(webGroupId);
			serviceContext.setAddGroupPermissions(true);
			serviceContext.setAddGuestPermissions(true);
			serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);

			DllFolderUtil.createDLFolder(folderName, folderDescription, JsonCards, serviceContext);

		} catch (DuplicateFolderNameException dne) {
			log.error(dne.getMessage());

		}

	}

	@Activate
	@Modified
	protected void activate(Map<String, Object> properties) throws SchedulerException {
		try {
			String cronExpression = GetterUtil.getString(properties.get("cron.expression"), "cronExpression");
			log.info(" cronExpression: " + cronExpression);

			String listenerClass = getClass().getName();
			Trigger jobTrigger = TriggerFactoryUtil.createTrigger(listenerClass, listenerClass, new Date(), null,
					cronExpression);

			SchedulerEntryImpl schedulerEntryImpl = new SchedulerEntryImpl(getClass().getName(), jobTrigger);

			SchedulerEngineHelperUtil.register(this, schedulerEntryImpl, DestinationNames.SCHEDULER_DISPATCH);

		} catch (Exception e) {
			log.error(e);
		}
	}

	@Deactivate
	protected void deactive() {
		SchedulerEngineHelperUtil.unregister(this);
	}

}