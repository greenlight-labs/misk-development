package misk.headless.internal.resource.v1_0;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.multipart.BinaryFile;
import com.liferay.portal.vulcan.multipart.MultipartBody;
import com.notification.settings.service.NotificationSettingsLocalService;

import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;
import javax.ws.rs.BadRequestException;

import misk.headless.dto.v1_0.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import misk.app.users.model.impl.AppUserImpl;
import misk.app.users.service.AppUserLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.resource.v1_0.AppUserResource;
import misk.headless.social.login.VerifyAppleToken;
import misk.headless.util.AccountUtil;
import misk.headless.util.CommonConstant;
import misk.headless.util.CommonUtil;
import misk.headless.util.SendEmailUtil;

/**
 * @author Tayyab Zafar
 */
@Component(
		properties = "OSGI-INF/liferay/rest/v1_0/app-user.properties",
		scope = ServiceScope.PROTOTYPE, service = AppUserResource.class
)
public class AppUserResourceImpl extends BaseAppUserResourceImpl {

	Log _log = LogFactoryUtil.getLog(AppUserResourceImpl.class);
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	//private static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#~|\\$%\\^&\\*\\?\\[\\]\\{\\}\\(\\)\"\\';:_\\-<>\\.,=\\+\\/\\\\]).{8,}$";
	private static final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#~|$%^&*?\\[\\]{}()\"';:_\\-<>.,=+/\\\\]).{8,}$";
	private static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
	public String miskProfileDocment = CommonConstant.PROFILE_DOC_FOLDER_NAME;

	@Override
	public AppUser postRegister(AppUser appUser) throws Exception {
		try {

			if (Validator.isNull(appUser)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-object-needed"));
			}

			if (Validator.isNull(appUser.getFullName()) ||
					Validator.isNull(appUser.getEmailAddress()) ||
					Validator.isNull(appUser.getPhoneNumber()) ||
					Validator.isNull(appUser.getPassword())) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-object-fields-needed"));
			}

			if(appUser.getFullName().length() > 50 ){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "full-name-length"));
			}

			if(!Validator.isEmailAddress(appUser.getEmailAddress())){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-valid"));
			}

			if(!isValid(appUser.getPassword())){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-not-valid"));
			}

			misk.app.users.model.AppUser alreadyExist = _appUserLocalService.fetchByEmailAddress(appUser.getEmailAddress());

			if (Validator.isNotNull(alreadyExist)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-already-exist"));
			}
			misk.app.users.model.AppUser alreadyExistphone = _appUserLocalService.fetchByPhoneNumber(appUser.getPhoneNumber());
			if (Validator.isNotNull(alreadyExistphone)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-already-exist"));
			}

			// 4 digit passcode
			String passcode = CommonUtil.generatePasscode();

			misk.app.users.model.AppUser entry = new AppUserImpl();
			entry.setFullName(appUser.getFullName());
			entry.setEmailAddress(appUser.getEmailAddress());
			entry.setPhoneNumber(appUser.getPhoneNumber());
			entry.setLanguageId("en_US");
			entry.setPassword(appUser.getPassword());
			entry.setPasswordReset(false);
			entry.setPasscode(passcode);
			entry.setAgreedToTermsOfUse(true);
			entry.setEmailAddressVerified(false);
			entry.setStatus(false);

			entry.setCompanyId(this.contextCompany.getCompanyId());
			entry.setGroupId(this.appGroupId);

			// Validation
			/*AppUserValidator appUserValidator = new AppUserValidator();
			appUserValidator.validate(entry);*/

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// Add entry
			misk.app.users.model.AppUser addedEntry = _appUserLocalService.addEntry(entry, serviceContext);

			// Add user notification settings
			notificationSettingsLocalService.setDefaultNotificationSettingsForUser((int) addedEntry.getAppUserId(), this.contextCompany.getCompanyId(), this.appGroupId);

			RequestStatus requestStatus = new RequestStatus();

			// send registration otp email
			SendEmailUtil.sendRegistrationOtp(addedEntry.getFullName(), addedEntry.getEmailAddress(), passcode);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "registration-successful"));

			return toAppUser(addedEntry, requestStatus);
		} catch (Exception ex) {
			_log.error("Error creating app user: " + ex.getMessage(), ex);
			throw ex;
			/*RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatus(false);
			requestStatus.setMessage(ex.getMessage());
			return toRequestStatus(requestStatus);*/
		}
	}

	@Override
	public AppUser updateRegister(AppUser appUser) throws Exception {
		try {

			if (Validator.isNull(appUser)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-object-needed"));
			}

			if (Validator.isNull(appUser.getAppUserId()) ||
					Validator.isNull(appUser.getFullName()) ||
					Validator.isNull(appUser.getEmailAddress()) ||
					Validator.isNull(appUser.getPhoneNumber()) ||
					Validator.isNull(appUser.getPassword())) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-object-fields-needed-with-user-id"));
			}

			if(appUser.getFullName().length() > 50 ){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "full-name-length"));
			}

			if(!Validator.isEmailAddress(appUser.getEmailAddress())){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-valid"));
			}

			if(!isValid(appUser.getPassword())){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-not-valid"));
			}

			misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchAppUser(appUser.getAppUserId());

			if(Validator.isNull(updateEntry)){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
			}

			boolean isEmailUsedByAnotherUser = _appUserLocalService.isEmailUsedByAnotherUser(appUser.getEmailAddress(), appUser.getAppUserId());
			if (isEmailUsedByAnotherUser) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-already-exist"));
			}

			boolean isPhoneUsedByAnotherUser = _appUserLocalService.isPhoneUsedByAnotherUser(appUser.getPhoneNumber(), appUser.getAppUserId());
			if (isPhoneUsedByAnotherUser) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-already-exist"));
			}

			// 4 digit passcode
			String passcode = CommonUtil.generatePasscode();

			if(!Objects.equals(appUser.getEmailAddress(), updateEntry.getEmailAddress())){
				updateEntry.setPasscode(passcode);
			}

			updateEntry.setFullName(appUser.getFullName());
			updateEntry.setEmailAddress(appUser.getEmailAddress());
			updateEntry.setPhoneNumber(appUser.getPhoneNumber());
			updateEntry.setPassword(appUser.getPassword());

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// Add entry
			misk.app.users.model.AppUser updatedEntry = _appUserLocalService.updateEntry(updateEntry, serviceContext);

			// Add user notification settings
			notificationSettingsLocalService.setDefaultNotificationSettingsForUser((int) updatedEntry.getAppUserId(), this.contextCompany.getCompanyId(), this.appGroupId);

			RequestStatus requestStatus = new RequestStatus();

			if(!Objects.equals(appUser.getEmailAddress(), updateEntry.getEmailAddress())) {
				// send registration otp email
				SendEmailUtil.sendRegistrationOtp(updatedEntry.getFullName(), updatedEntry.getEmailAddress(), passcode);
			}

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "registration-successful"));

			return toAppUser(updatedEntry, requestStatus);
		} catch (Exception ex) {
			_log.error("Error creating app user: " + ex.getMessage(), ex);
			throw ex;
			/*RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatus(false);
			requestStatus.setMessage(ex.getMessage());
			return toRequestStatus(requestStatus);*/
		}
	}

	@Override
	public AppUser verifyRegistrationOTP(AppUser appUser)
			throws Exception {

		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.getAppUser(appUser.getAppUserId());

		String passwordResetToken = null;
		if ( updateEntry.getEmailAddressVerified() && updateEntry.getPhoneNumberVerified() ) {

			// account activated
			updateEntry.setStatus(true);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-activated"));
		} else {
			requestStatus.setStatus(false);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "verify-email-and-phone"));
		}

		return toVerifyOTP(updateEntry, requestStatus, passwordResetToken);
	}

	@Override
	public AppUser activateVerifiedMobileOTPUser(AppUser appUser)
			throws Exception {

		if (Validator.isNull(appUser.getPhoneNumber()) ||
				Validator.isNull(appUser.getGcpToken())) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-and-gcp-token-required"));
		}

		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchByPhoneNumber(appUser.getPhoneNumber());

		if ( Validator.isNull(updateEntry) ) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-not-found"));
		}

		// account activated
		updateEntry.setGcpToken(appUser.getGcpToken());
		updateEntry.setStatus(true);

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

		// update entry
		_appUserLocalService.updateEntry(updateEntry, serviceContext);

		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-activated"));

		return toAppUser(updateEntry, requestStatus);
	}

	@Override
	public AppUser postLogin(AppUser appUser) throws Exception {

		if(Validator.isNull(appUser.getEmailAddress()) && Validator.isNull(appUser.getPhoneNumber())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-or-phone-number-required"));
		}
		if(Validator.isNull(appUser.getPassword())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-required"));
		}
		misk.app.users.model.AppUser userEntryEmail = _appUserLocalService.fetchByEmailAddress(appUser.getEmailAddress());
		misk.app.users.model.AppUser userEntryPhone = null;
		if (Validator.isNotNull(appUser.getPhoneNumber())) {
			userEntryPhone = _appUserLocalService.fetchByPhoneNumber(appUser.getPhoneNumber());
		}
		if (Validator.isNull(userEntryEmail) && Validator.isNull(userEntryPhone)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-exist-with-email-address-phone-number"));
		}else if(Validator.isNull(userEntryEmail) && Validator.isNotNull(userEntryPhone)){
			userEntryEmail = userEntryPhone;
		}

		if (Objects.equals(userEntryEmail.getPassword(), appUser.getPassword())) {
			// check account deactivate status, emailAddress and PhoneNumber are verified or not
			AccountUtil.isAccountDeactivated(userEntryEmail, contextHttpServletRequest);
			AccountUtil.isEmailAddressVerified(userEntryEmail, contextHttpServletRequest);
			AccountUtil.isPhoneNumberVerified(userEntryEmail, contextHttpServletRequest);

			if(!userEntryEmail.getStatus()){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-not-activated"));
			}
			HttpSession session = this.contextHttpServletRequest.getSession();
			session.setAttribute("appUserId", appUser.getAppUserId());
			session.setAttribute("isAuthenticated", true);
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "invalid-email-password"));
		}

		return toAppUser(userEntryEmail);
	}

	@Override
	public AppUser postSocialLogin(AppUser appUser) throws Exception {

		misk.app.users.model.AppUser userEntry = null;

		if(Validator.isNotNull(appUser.getFacebookId())){
			userEntry = _appUserLocalService.fetchByFacebookId(appUser.getFacebookId());
		}else if(Validator.isNotNull(appUser.getGoogleUserId())){
			userEntry = _appUserLocalService.fetchByGoogleUserId(appUser.getGoogleUserId());
		}else if(Validator.isNotNull(appUser.getAppleUserId())){
			String appleUserEmailAddress = VerifyAppleToken.verify(appUser.getAppleUserId());
			if (Validator.isNotNull(appleUserEmailAddress) && !appleUserEmailAddress.isEmpty()) {
				userEntry = _appUserLocalService.fetchByEmailAddress(appleUserEmailAddress);
			} else {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "invalid-apple-user-token"));
			}
		}

		if (Validator.isNull(userEntry)) {
			return this.registerSocialUser(appUser);
		} else if (Validator.isNotNull(appUser.getFacebookId()) ||
				Validator.isNotNull(appUser.getGoogleUserId()) ||
				Validator.isNotNull(appUser.getAppleUserId())
		) {
			// check account deactivate status
			AccountUtil.isAccountDeactivated(userEntry, contextHttpServletRequest);
			doSocialLogin(appUser, userEntry);
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "invalid-request-required-parameters-missing"));
		}

		return toAppUser(userEntry);
	}

	public AppUser registerSocialUser(AppUser appUser) throws Exception {
		try {

			if (Validator.isNull(appUser)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-object-needed"));
			}

			if (Validator.isNull(appUser.getFullName()) ||
					Validator.isNull(appUser.getEmailAddress()) ||
					(Validator.isNull(appUser.getFacebookId()) && Validator.isNull(appUser.getGoogleUserId()) && Validator.isNull(appUser.getAppleUserId()))
			) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "social-account-required-parameters-missing"));
			}
			if(!Validator.isEmailAddress(appUser.getEmailAddress())){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-valid"));
			}

			misk.app.users.model.AppUser entry = _appUserLocalService.fetchByEmailAddress(appUser.getEmailAddress());
			boolean newEntryFlag = false;

			if(Validator.isNull(entry)){
				entry = new AppUserImpl();
				newEntryFlag = true;
			}
			entry.setFullName(appUser.getFullName());
			entry.setEmailAddress(appUser.getEmailAddress());
			entry.setLanguageId("en_US");
			entry.setFacebookId(appUser.getFacebookId());
			entry.setGoogleUserId(appUser.getGoogleUserId());
			entry.setAppleUserId(appUser.getAppleUserId());
			entry.setPassword(null);
			entry.setPasswordReset(false);
			entry.setAgreedToTermsOfUse(true);
			entry.setEmailAddressVerified(true);
			entry.setStatus(true);

			entry.setCompanyId(this.contextCompany.getCompanyId());
			entry.setGroupId(this.appGroupId);

			// Validation
			/*AppUserValidator appUserValidator = new AppUserValidator();
			appUserValidator.validate(entry);*/

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			misk.app.users.model.AppUser socialEntry;
			if(newEntryFlag){
				// Add entry
				socialEntry = _appUserLocalService.addEntry(entry, serviceContext);
				// Add user notification settings
				notificationSettingsLocalService.setDefaultNotificationSettingsForUser((int) socialEntry.getAppUserId(), this.contextCompany.getCompanyId(), this.appGroupId);
			}else{
				// Update entry
				socialEntry = _appUserLocalService.updateEntry(entry, serviceContext);
			}

			RequestStatus requestStatus = new RequestStatus();

			// send registration otp email
			//SendEmailUtil.sendRegistrationOtp(addedEntry.getFullName(), addedEntry.getEmailAddress(), passcode);

			// create login session
			doSocialLogin(appUser, socialEntry);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "social-account-registration-successful"));

			return toAppUser(socialEntry, requestStatus);
		} catch (Exception ex) {
			_log.error("Error creating app user: " + ex.getMessage(), ex);
			throw ex;
		}
	}

	protected void doSocialLogin(AppUser appUser, misk.app.users.model.AppUser socialEntry) {
		if(Objects.equals(appUser.getFacebookId(), socialEntry.getFacebookId()) ||
				Objects.equals(appUser.getGoogleUserId(), socialEntry.getGoogleUserId()) ||
				Objects.equals(appUser.getAppleUserId(), socialEntry.getAppleUserId())
		){
			HttpSession session = this.contextHttpServletRequest.getSession();
			session.setAttribute("appUserId", socialEntry.getAppUserId());
			session.setAttribute("isAuthenticated", true);
		}
	}

	@Override
	public AppUser resetPassword(AppUser appUser) throws Exception {

		if (Validator.isNull(appUser.getNewPassword())) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "new-password-required"));
		}
		if(!isValid(appUser.getNewPassword())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-not-valid"));
		}
		if(!Objects.equals(appUser.getNewPassword(), appUser.getConfirmPassword())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-not-matched"));
		}
		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.getAppUser(appUser.getAppUserId());

		if (updateEntry.getPasswordReset() && Objects.equals(updateEntry.getPasswordResetToken(), appUser.getPasswordResetToken())) {
			// account activated
			Date now = new Date();
			updateEntry.setPasswordReset(false);
			updateEntry.setPasswordResetToken(null);
			updateEntry.setPasswordModifiedDate(now);
			updateEntry.setPassword(appUser.getNewPassword());
			updateEntry.setStatus(true);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			misk.app.users.model.AppUser updatedEntry = _appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-reset-successful"));
		} else {
			requestStatus.setStatus(false);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-reset-failed"));
		}

		return toAppUser(updateEntry, requestStatus);
	}

	@Override
	public AppUser forgotPassword(AppUser appUser) throws Exception {
		RequestStatus requestStatus = new RequestStatus();

		if(!Validator.isEmailAddress(appUser.getEmailAddress())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-valid"));
		}

		misk.app.users.model.AppUser updateEntry = null;
		try {
			updateEntry=_appUserLocalService.findByEmailAddress(appUser.getEmailAddress());
		} catch (Exception e) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-found"));
		}
		if (Validator.isNotNull(updateEntry)) {
			AccountUtil.isValidForgotPasswordRequest(updateEntry, contextHttpServletRequest);

			// set new 4 digit passcode
			String passcode = CommonUtil.generatePasscode();

			updateEntry.setPasscode(passcode);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			//serviceContext.setSignedIn(true);

			// update entry
			_appUserLocalService.updateAppUser(updateEntry);

			// send otp to email
			SendEmailUtil.resendOTP(updateEntry.getFullName(), updateEntry.getEmailAddress(), passcode);

			if(! updateEntry.getPhoneNumberVerified()) {
				requestStatus.setStatus(true);
				requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "verify-email-address-phone-number-otp"));
			}else {
				requestStatus.setStatus(true);
				requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "verify-email-address-otp"));
			}
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-found"));
		}

		return toAppUser(updateEntry, requestStatus);
	}
	@Override
	public AppUser forgotPasswordPhone(AppUser appUser) throws Exception {
		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchByPhoneNumber(appUser.getPhoneNumber());

		String passwordResetToken;
		if (Validator.isNotNull(updateEntry)) {

			AccountUtil.isValidForgotPasswordRequest(updateEntry, contextHttpServletRequest);

			// reset password token
			passwordResetToken = CommonUtil.generateResetToken();

			updateEntry.setPasswordResetToken(passwordResetToken);
			updateEntry.setPasswordReset(true);

			if(! updateEntry.getEmailAddressVerified()) {
				// set new 4 digit passcode
				String passcode = CommonUtil.generatePasscode();
				updateEntry.setPasscode(passcode);
				// send otp to email
				SendEmailUtil.resendOTP(updateEntry.getFullName(), updateEntry.getEmailAddress(), passcode);
				requestStatus.setStatus(true);
				requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "verify-email-address-phone-number-otp"));
			}else {
				requestStatus.setStatus(true);
				requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "verify-phone-number-otp"));
			}

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-not-found"));
		}

		return toVerifyOTP(updateEntry, requestStatus, passwordResetToken);
	}

	@Override
	public AppUser changePassword(AppUser appUser) throws Exception {

		RequestStatus requestStatus = new RequestStatus();

		if (Validator.isNull(appUser.getNewPassword())) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "new-password-required"));
		}
		if(!isValid(appUser.getNewPassword())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-not-valid"));
		}
		if(!Objects.equals(appUser.getNewPassword(), appUser.getConfirmPassword())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-not-matched"));
		}

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchByEmailAddress(appUser.getEmailAddress());

		if (Validator.isNull(updateEntry)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-exist-with-email-address"));
		}

		if(!Objects.equals(appUser.getPassword(), updateEntry.getPassword())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "old-password-not-matched"));
		}

		if (Validator.isNotNull(appUser.getNewPassword())) {

			AccountUtil.isValidForgotPasswordRequest(updateEntry, contextHttpServletRequest);

			// account activated
			Date now = new Date();
			updateEntry.setPasswordModifiedDate(now);
			updateEntry.setPassword(appUser.getNewPassword());

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			misk.app.users.model.AppUser updatedEntry = _appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-reset-successful"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "new-password-required"));
		}

		return toAppUser(updateEntry, requestStatus);
	}

	@Override
	public AppUser checkPassword(AppUser appUser) throws Exception {

		RequestStatus requestStatus = new RequestStatus();
		misk.app.users.model.AppUser updateEntry = null;
		try {
			updateEntry = _appUserLocalService.fetchByEmailAddress(appUser.getEmailAddress());
		} catch (Exception e) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-exist-with-email-address"));
		}
		if (Validator.isNull(updateEntry)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-exist-with-email-address"));
		}
		if (!Objects.equals(appUser.getNewPassword(), appUser.getConfirmPassword())) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "password-not-matched"));
		}
		if(!Objects.equals(appUser.getPassword(), updateEntry.getPassword())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "old-password-not-matched"));
		}

		return toAppUser(updateEntry, requestStatus);
	}
	
	@Override
	public AppUser resendOTP(AppUser appUser) throws Exception {
		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.getAppUser(appUser.getAppUserId());

		if (Validator.isNotNull(updateEntry)) {
			// set new 4 digit passcode
			String passcode = CommonUtil.generatePasscode();

			updateEntry.setPasscode(passcode);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			// send otp to email
			SendEmailUtil.resendOTP(updateEntry.getFullName(), updateEntry.getEmailAddress(), passcode);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-otp-sent"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "request-not-being-processed"));
		}

		return toAppUser(updateEntry, requestStatus);
	}

	@Override
	public AppUser verifyOTP(AppUser appUser)
			throws Exception {

		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.getAppUser(appUser.getAppUserId());

		String passwordResetToken = null;
		if (Objects.equals(updateEntry.getPasscode(), appUser.getPasscode())) {

			// reset password token
			passwordResetToken = CommonUtil.generateResetToken();

			updateEntry.setPasscode(null);
			updateEntry.setPasswordResetToken(passwordResetToken);
			updateEntry.setPasswordReset(true);
			updateEntry.setEmailAddressVerified(true);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "otp-verified-successful"));
		} else {
			requestStatus.setStatus(false);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "otp-not-verified"));
		}

		return toVerifyOTP(updateEntry, requestStatus, passwordResetToken);
	}

	@Override
	public AppUser setEmailAddressVerified(AppUser appUser)
			throws Exception {

		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchByEmailAddress(appUser.getEmailAddress());

		if (Validator.isNull(updateEntry)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-found"));
		}

		if (Objects.equals(updateEntry.getPasscode(), appUser.getPasscode())) {
			updateEntry.setEmailAddressVerified(true);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-verified-successful"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "otp-not-verified"));
		}

		return toAppUser(updateEntry, requestStatus);
	}

	@Override
	public AppUser setPhoneNumberVerified(AppUser appUser)
			throws Exception {

		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchByPhoneNumber(appUser.getPhoneNumber());

		if (Validator.isNotNull(updateEntry)) {
			updateEntry.setPhoneNumberVerified(true);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-verified-successful"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-not-found"));
		}

		return toAppUser(updateEntry, requestStatus);
	}

	@Override
	public AppUser editUserProfile(MultipartBody multipartBody)
			throws Exception {
		BinaryFile binaryImgFile = multipartBody.getBinaryFile("profileImage");
		String appUserId = multipartBody.getValueAsString("appUserId");
		String fullName = multipartBody.getValueAsString("fullName");
		String phoneNumber = multipartBody.getValueAsString("phoneNumber");

		if (Validator.isNull(appUserId)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-id-required"));
		}

		RequestStatus requestStatus = new RequestStatus();
		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchAppUser(Long.parseLong(appUserId));

		if (Validator.isNotNull(updateEntry)) {

			// check update request initiated within 24 hours
			AccountUtil.isValidEditProfileRequest(updateEntry, contextHttpServletRequest);

			// check phone has not been linked with another user
			if(Validator.isNotNull(phoneNumber)){
				AccountUtil.isValidChangePhoneRequest(updateEntry, phoneNumber, contextHttpServletRequest);
				updateEntry.setPhoneNumber(phoneNumber);
			}

			// create parent folder, user folder and upload profile image
			if (Validator.isNotNull(binaryImgFile)&& ! binaryImgFile.getFileName().isEmpty()) {
				String userProfileImage = _addDocument(appGroupId, appGroupId, multipartBody,"profileImage");
				updateEntry.setProfileImage(userProfileImage);
			}
			
			if(Validator.isNotNull(fullName)){
				updateEntry.setFullName(fullName);
			}

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "profile-updated-successful"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
		}

		return toAppuserEditPost(updateEntry,requestStatus,ApiKeys.getBaseURL(this.contextUriInfo));
	}

	@Override
	public AppUser updateUserProfileBannerImage(MultipartBody multipartBody) throws Exception {

		String appUserId = multipartBody.getValueAsString("appUserId");
		BinaryFile binaryProfileBannerImgFile = multipartBody.getBinaryFile("profileBannerImage");

		if (Validator.isNull(appUserId) && Validator.isNotNull(binaryProfileBannerImgFile) && ! binaryProfileBannerImgFile.getFileName().isEmpty()) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-profile-banner-fields-required"));
		}

		RequestStatus requestStatus = new RequestStatus();
		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchAppUser(Long.parseLong(appUserId));

		if (Validator.isNotNull(updateEntry)) {

			// Profile Banner Image
			String userBannerProfileImage = _addDocument(appGroupId, appGroupId, multipartBody,"profileBannerImage");
			updateEntry.setProfileBannerImage(userBannerProfileImage);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "profile-banner-updated-successful"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
		}

		return toAppuserEditPost(updateEntry,requestStatus,ApiKeys.getBaseURL(this.contextUriInfo));
	}

	@Override
	public AppUser deleteUserProfileImage(DeleteProfileImage deleteProfileImage) throws Exception {
		long appUserId = deleteProfileImage.getAppUserId();

		if (Validator.isNull(appUserId)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-id-required"));
		}

		RequestStatus requestStatus = new RequestStatus();
		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchAppUser(appUserId);

		if (Validator.isNotNull(updateEntry)) {

			// Delete profile image
			updateEntry.setProfileImage("");
			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// update entry
			_appUserLocalService.updateEntry(updateEntry, serviceContext);

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "profile-image-deleted-successful"));
		} else {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
		}

		return toAppuserEditPost(updateEntry,requestStatus,ApiKeys.getBaseURL(this.contextUriInfo));
	}

	private String _addDocument(Long groupId, long repositoryId, MultipartBody multipartBody, String imageType)
			throws Exception {
		ProfileDocument document = new ProfileDocument();
		BinaryFile binaryImgFile = multipartBody.getBinaryFile(imageType);
		String appUserId = multipartBody.getValueAsString("appUserId");
		FileEntry results = null;
		if(imageType.equalsIgnoreCase("profileBannerImage")) {
			miskProfileDocment=CommonConstant.PROFILE_BANNER_FOLDER_NAME;
		}
		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),
					this.contextHttpServletRequest);
			Company company = CompanyLocalServiceUtil.getCompany(serviceContext.getCompanyId());
			User user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(),
					CommonConstant.ADMIN_SCREEN_NAME);
			PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.create(user);
			PermissionThreadLocal.setPermissionChecker(permissionChecker);
			PrincipalThreadLocal.setName(user.getUserId());
			// check parent folder exists ,In case not create it
			Folder miskProfileDocmentFolder = null;
			Folder userDocmentFolder = null;
			if (!isFolderExists(repositoryId, 0, miskProfileDocment)) {
				miskProfileDocmentFolder = createFolder(repositoryId, 0, miskProfileDocment, miskProfileDocment,
						serviceContext);
			} else {
				miskProfileDocmentFolder = getFolderId(repositoryId, 0, miskProfileDocment);
			}
			if (Validator.isNull(miskProfileDocmentFolder)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "parent-folder-not-found"));
			}
			// check userId folder exists ,if Exists create new folder with userid otherwise
			// delete folder for update new images
			if (!isFolderExists(repositoryId, miskProfileDocmentFolder.getFolderId(), String.valueOf(appUserId))) {
				userDocmentFolder = createFolder(repositoryId, miskProfileDocmentFolder.getFolderId(),
						String.valueOf(appUserId), String.valueOf(appUserId), serviceContext);
			} else {
				Folder deleteFolder = getFolderId(repositoryId, miskProfileDocmentFolder.getFolderId(),
						String.valueOf(appUserId));
				if (Validator.isNotNull(deleteFolder)) {
					deleteFolder(deleteFolder.getFolderId());
					userDocmentFolder = createFolder(repositoryId, miskProfileDocmentFolder.getFolderId(),
							String.valueOf(appUserId), String.valueOf(appUserId), serviceContext);
				}
			}
			if (Validator.isNull(userDocmentFolder)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-folder-not-created"));
			}

			// set guest view permission on parent and user folder
			setFolderPermissions(miskProfileDocmentFolder, serviceContext);
			setFolderPermissions(userDocmentFolder, serviceContext);

			String extension = FilenameUtils.getExtension(binaryImgFile.getFileName());
			String fileName =StringPool.BLANK;
					if(imageType.equalsIgnoreCase("profileImage")) {
						fileName="profile-avatar-"+appUserId+"."+extension;
					}else {
						fileName="profile-background-banner-"+appUserId+"."+extension;
					}
			results = _dlAppService.addFileEntry(repositoryId, userDocmentFolder.getFolderId(), fileName,
					binaryImgFile.getContentType(), fileName, "", "",
					binaryImgFile.getInputStream(), binaryImgFile.getSize(), serviceContext);
		} catch (Exception e) {
			_log.error("error in adding files ",e);
		}
		return "documents/"+results.getRepositoryId()+"/"+results.getFolderId()+"/"+results.getFileName()+"/"+results.getUuid();
	}
	public void deleteFolder(long folderId) {
		try {
			_dlAppService.deleteFolder(folderId);
		} catch (PortalException e) {
			_log.error("error in deleteFolder ",e);
		}
	}
	public void setFolderPermissions(Folder folder, ServiceContext serviceContext) {
		try {
			Role guestRole = roleLocalService.getRole(serviceContext.getCompanyId(), RoleConstants.GUEST);
			resourcePermissionLocalService.setResourcePermissions(serviceContext.getCompanyId(), DLFolder.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(folder.getFolderId()), guestRole.getRoleId(), new String[]{"VIEW"});
		} catch (PortalException e) {
			_log.error("error assigning guest permissions ",e);
		}
	}

	public Folder createFolder(long repositoryId, long parentFolderId, String name, String description,
							   ServiceContext serviceContext) {
		Folder folder = null;
		try {
			folder = _dlAppService.addFolder(repositoryId, parentFolderId, name, description, serviceContext);

		} catch (PortalException e) {
			_log.error("error in createFolder ",e);
		}
		return folder;
	}

	public boolean isFolderExists(long repositoryId, long parentFolderId, String name) {
		try {
			Folder folder = _dlAppService.getFolder(repositoryId, parentFolderId, name);
			if (Validator.isNotNull(folder)) {
				return true;
			}
		} catch (PortalException e) {
			_log.error("error in  isFolderExists ",e);
		}
		return false;
	}

	public Folder getFolderId(long repositoryId, long parentFolderId, String name) {
		Folder folder = null;
		try {
			folder = _dlAppService.getFolder(repositoryId, parentFolderId, name);
			if (Validator.isNotNull(folder)) {
				return folder;
			}
		} catch (PortalException e) {
			_log.error("error in  getFolderId ",e);
		}
		return folder;
	}

	@Override
	public AppUser getProfileDetails(Long profileid, String languageId) throws Exception{

		misk.app.users.model.AppUser getProfile = _appUserLocalService.getAppUser(profileid);

		return toAppuserEdit(getProfile,ApiKeys.getBaseURL(this.contextUriInfo));

	}

	@Override
	public AppUser addDevice(AppUser appUser) throws Exception {
		if (Validator.isNull(appUser.getAppUserId()) &&
				Validator.isNull(appUser.getEmailAddress()) &&
				(Validator.isNull(appUser.getAndroidDeviceToken()) ||
						Validator.isNull(appUser.getIosDeviceToken()))) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "add-device-fields-required"));
		}

		RequestStatus requestStatus = new RequestStatus();

		misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchByEmailAddress(appUser.getEmailAddress());

		if ( Validator.isNull(updateEntry) || updateEntry.getAppUserId() != appUser.getAppUserId() ) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
		}

		// account activated
		if(Validator.isNotNull(appUser.getAndroidDeviceToken())){
			updateEntry.setAndroidDeviceToken(appUser.getAndroidDeviceToken());
		}
		if(Validator.isNotNull(appUser.getIosDeviceToken())){
			updateEntry.setIosDeviceToken(appUser.getIosDeviceToken());
		}

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

		// update entry
		_appUserLocalService.updateEntry(updateEntry, serviceContext);

		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "device-added-successfully"));

		return toAppUser(updateEntry, requestStatus);
	}

	@Override
	public AppUser updateUserAccountDetails(AppUser appUser) throws Exception {
		try {

			if (Validator.isNull(appUser)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-object-needed"));
			}

			if (Validator.isNull(appUser.getEmailAddress()) &&
					Validator.isNull(appUser.getPhoneNumber())) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-or-phone-number-required"));
			}

			if(Validator.isNotNull(appUser.getFullName()) && appUser.getFullName().length() > 50 ){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "full-name-length"));
			}

			if(Validator.isNotNull(appUser.getEmailAddress()) && !Validator.isEmailAddress(appUser.getEmailAddress())){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-not-valid"));
			}

			misk.app.users.model.AppUser updateEntry = _appUserLocalService.fetchAppUser(appUser.getAppUserId());

			if(Validator.isNull(updateEntry)){
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
			}

			boolean isEmailUsedByAnotherUser = _appUserLocalService.isEmailUsedByAnotherUser(appUser.getEmailAddress(), appUser.getAppUserId());
			if (isEmailUsedByAnotherUser) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "email-address-already-exist"));
			}

			boolean isPhoneUsedByAnotherUser = _appUserLocalService.isPhoneUsedByAnotherUser(appUser.getPhoneNumber(), appUser.getAppUserId());
			if (isPhoneUsedByAnotherUser) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "phone-number-already-exist"));
			}

			if( Validator.isNotNull(appUser.getFullName()) ) {
				updateEntry.setFullName(appUser.getFullName());
			}

			String passcode = null;
			if( Validator.isNotNull(appUser.getEmailAddress()) ) {
				updateEntry.setEmailAddress(appUser.getEmailAddress());
				// 4 digit passcode
				passcode = CommonUtil.generatePasscode();
				if(!Objects.equals(appUser.getEmailAddress(), updateEntry.getEmailAddress())){
					updateEntry.setPasscode(passcode);
				}
			}

			if( Validator.isNotNull(appUser.getPhoneNumber()) ) {
				updateEntry.setPhoneNumber(appUser.getPhoneNumber());
			}

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			// Add entry
			misk.app.users.model.AppUser updatedEntry = _appUserLocalService.updateEntry(updateEntry, serviceContext);

			RequestStatus requestStatus = new RequestStatus();

			if(Validator.isNotNull(appUser.getEmailAddress()) && !Objects.equals(appUser.getEmailAddress(), updateEntry.getEmailAddress())) {
				// send registration otp email
				SendEmailUtil.sendRegistrationOtp(updatedEntry.getFullName(), updatedEntry.getEmailAddress(), passcode);
			}

			requestStatus.setStatus(true);
			requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-account-updated-successful"));

			return toAppUser(updatedEntry, requestStatus);
		} catch (Exception ex) {
			_log.error("Error creating app user: " + ex.getMessage(), ex);
			throw ex;
			/*RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatus(false);
			requestStatus.setMessage(ex.getMessage());
			return toRequestStatus(requestStatus);*/
		}
	}

	@Override
	public AppUser deleteAccount(AppUser appUser) throws Exception {
		//@MY_TODO: need to delete this method, only for development purpose
		if(Validator.isNull(appUser.getPhoneNumber()) && Validator.isNull(appUser.getAppUserId())){
			throw new BadRequestException("Phone number or app user id field is required.");
		}

		misk.app.users.model.AppUser deletedEntry;

		if(Validator.isNotNull(appUser.getPhoneNumber())){

			misk.app.users.model.AppUser entryPhone = _appUserLocalService.fetchByPhoneNumber(appUser.getPhoneNumber());
			deletedEntry = _appUserLocalService.deleteAppUser(entryPhone);
		}else{
			deletedEntry = _appUserLocalService.deleteAppUser(appUser.getAppUserId());
		}

		if(Validator.isNull(deletedEntry)){
			throw new BadRequestException("Account does not exist with provided phone number or user id.");
		}

		RequestStatus requestStatus = new RequestStatus();
		requestStatus.setMessage("Account has been deleted successfully.");

		return toAppUser(deletedEntry, requestStatus);
	}

	private static @GraphQLField AppUser toAppUser(misk.app.users.model.AppUser appUser) {
		return new AppUser() {
			{
				appUserId = appUser.getAppUserId();
				fullName = appUser.getFullName();
				emailAddress = appUser.getEmailAddress();
				phoneNumber = appUser.getPhoneNumber();
				languageId = appUser.getLanguageId();
				isVerified = appUser.getStatus();
				emailAddressVerified = appUser.getEmailAddressVerified();
				phoneNumberVerified = appUser.getPhoneNumberVerified();
			}
		};
	}

	private static @GraphQLField AppUser toAppUser(misk.app.users.model.AppUser appUser, RequestStatus requestStatus) {
		RequestStatus newRequestStatus = requestStatus;
		return new AppUser() {
			{
				appUserId = appUser.getAppUserId();
				fullName = appUser.getFullName();
				emailAddress = appUser.getEmailAddress();
				phoneNumber = appUser.getPhoneNumber();
				languageId = appUser.getLanguageId();
				requestStatus = newRequestStatus;
				isVerified = appUser.getStatus();
				emailAddressVerified = appUser.getEmailAddressVerified();
				phoneNumberVerified = appUser.getPhoneNumberVerified();
			}
		};
	}
	private static @GraphQLField AppUser toAppuserEdit(misk.app.users.model.AppUser appUser,String baseurl) {

		return new AppUser() {
			{
				String profileImage1 = "";
				if(StringUtils.isNotEmpty(appUser.getProfileImage())){
					profileImage1 = baseurl + "/" + appUser.getProfileImage();
				}
				String profileBannerImage1 = "";
				if(StringUtils.isNotEmpty(appUser.getProfileBannerImage())){
					profileBannerImage1 = baseurl + "/" + appUser.getProfileBannerImage();
				}
				appUserId = appUser.getAppUserId();
				fullName = appUser.getFullName();
				emailAddress = appUser.getEmailAddress();
				phoneNumber = appUser.getPhoneNumber();
				languageId = appUser.getLanguageId();
				isVerified = appUser.getStatus();
				emailAddressVerified = appUser.getEmailAddressVerified();
				phoneNumberVerified = appUser.getPhoneNumberVerified();
				profileImage = profileImage1;
				profileBannerImage = profileBannerImage1;
			}
		};
	}

	private static @GraphQLField AppUser toAppuserEditPost(misk.app.users.model.AppUser appUser, RequestStatus requestStatus, String baseurl) {

		RequestStatus newRequestStatus = requestStatus;

		return new AppUser() {
			{
				String profileImage1 = "";
				if(StringUtils.isNotEmpty(appUser.getProfileImage())){
					profileImage1 = baseurl + "/" + appUser.getProfileImage();
				}
				String profileBannerImage1 = "";
				if(StringUtils.isNotEmpty(appUser.getProfileBannerImage())){
					profileBannerImage1 = baseurl + "/" + appUser.getProfileBannerImage();
				}
				appUserId = appUser.getAppUserId();
				fullName = appUser.getFullName();
				emailAddress = appUser.getEmailAddress();
				phoneNumber = appUser.getPhoneNumber();
				languageId = appUser.getLanguageId();
				isVerified = appUser.getStatus();
				emailAddressVerified = appUser.getEmailAddressVerified();
				phoneNumberVerified = appUser.getPhoneNumberVerified();
				profileImage = profileImage1;
				profileBannerImage = profileBannerImage1;
				requestStatus = newRequestStatus;
			}
		};
	}

	private static @GraphQLField AppUser toVerifyOTP(misk.app.users.model.AppUser appUser, RequestStatus requestStatus, String passwordResetToken) {
		RequestStatus newRequestStatus = requestStatus;
		String newPasswordResetToken = passwordResetToken;
		return new AppUser() {
			{
				appUserId = appUser.getAppUserId();
				fullName = appUser.getFullName();
				emailAddress = appUser.getEmailAddress();
				phoneNumber = appUser.getPhoneNumber();
				languageId = appUser.getLanguageId();
				passwordResetToken = newPasswordResetToken;
				requestStatus = newRequestStatus;
				isVerified = appUser.getStatus();
				emailAddressVerified = appUser.getEmailAddressVerified();
				phoneNumberVerified = appUser.getPhoneNumberVerified();
			}
		};
	}

	private static @GraphQLField AppUser toRequestStatus(RequestStatus requestStatus) {
		RequestStatus newRequestStatus = requestStatus;
		return new AppUser() {
			{
				requestStatus = newRequestStatus;
			}
		};
	}

	public static boolean isValid(final String password) {
		Matcher matcher = pattern.matcher(password);
		return matcher.matches();
	}

	@Reference
	private AppUserLocalService _appUserLocalService;

	@Reference
	public NotificationSettingsLocalService notificationSettingsLocalService;

	@Reference
	private DLAppService _dlAppService;
}