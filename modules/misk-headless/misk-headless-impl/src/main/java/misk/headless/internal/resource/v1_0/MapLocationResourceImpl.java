package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.vulcan.pagination.Page;
import com.liferay.portal.vulcan.pagination.Pagination;
import map.locations.service.MapLocationLocalServiceUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.MapLocation;
import misk.headless.resource.v1_0.MapLocationResource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/map-location.properties",
	scope = ServiceScope.PROTOTYPE, service = MapLocationResource.class
)
public class MapLocationResourceImpl extends BaseMapLocationResourceImpl {

	Log _log = LogFactoryUtil.getLog(MapLocationResourceImpl.class);

	@Override
	public Page<MapLocation> getMapLocations(String languageId, Pagination pagination)
			throws Exception {
		try {
			// get total count of map locations
			int totalCount = MapLocationLocalServiceUtil.countByGroupId(ApiKeys.appGroupId);
			return Page.of(this.getMapPage(languageId, pagination.getStartPosition(), pagination.getEndPosition()), pagination, totalCount);
		} catch (Exception ex) {
			_log.error("Error retrieving map page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	protected List<MapLocation> getMapPage(String languageId, int startPosition, int endPosition) throws Exception {
		// fetch all records from database using map location service and return them
		// as a list of MapLocation objects
		List<MapLocation> mapLocations = new ArrayList<>();
		List<map.locations.model.MapLocation> records = MapLocationLocalServiceUtil.findByGroupId(ApiKeys.appGroupId, startPosition, endPosition);
		// convert each record to MapLocation object
		for (map.locations.model.MapLocation record : records) {
			MapLocation mapLocation = new MapLocation();
			mapLocation.setTitle(record.getTitle(languageId));
			mapLocation.setLatitude(record.getLatitude());
			mapLocation.setLongitude(record.getLongitude());
			mapLocation.setCallUs(record.getPhone());
			mapLocation.setWhatsapp(record.getWhatsapp());
			mapLocation.setEmail(record.getEmail());
			mapLocations.add(mapLocation);
		}

		return mapLocations;
	}
}