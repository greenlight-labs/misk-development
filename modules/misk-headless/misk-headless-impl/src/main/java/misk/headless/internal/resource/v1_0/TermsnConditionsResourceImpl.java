package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.HenaCafe;
import misk.headless.dto.v1_0.TermsnConditions;
import misk.headless.resource.v1_0.TermsnConditionsResource;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/termsn-conditions.properties",
	scope = ServiceScope.PROTOTYPE, service = TermsnConditionsResource.class
)
public class TermsnConditionsResourceImpl
	extends BaseTermsnConditionsResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(HenaCafeResourceImpl.class);

	@Override
	public TermsnConditions getTermsnConditions(String languageId)
			throws Exception {
		try {
			return this.getTermsnconditions(languageId);
		} catch (Exception ex) {
			_log.error("Error retrieving Hencafe page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}
	public TermsnConditions getTermsnconditions(String languageId) throws Exception{

		String articleid = "92166";
		long groupId = this.appGroupId;
		TermsnConditions results = new TermsnConditions();
		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleid, 0 );
		String articleContent = article.getContentByLocale(languageId);
		Document document = SAXReaderUtil.read(articleContent);

		Node title = document.selectSingleNode("/root/dynamic-element[@name='Title']/dynamic-content");
		results.setTitle(title.getText());
		Node Descriptions = document.selectSingleNode("/root/dynamic-element[@name='Descriptions']/dynamic-content");
		results.setDescription(Descriptions.getText());

		System.out.print(articleContent);
		return results;

		//Node titlec= document.selectSingleNode("/root/dynamic-element[@name='Textmc2a']/dynamic-content");
	}
}