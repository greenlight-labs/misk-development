package misk.headless.internal.resource.v1_0;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.notification.settings.service.NotificationSettingsLocalService;

import java.util.Optional;

import javax.ws.rs.BadRequestException;

import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.dto.v1_0.NotificationSettings;
import misk.headless.resource.v1_0.NotificationSettingsResource;

/**
 * @author Tayyab Zafar
 * 
 *         <p>This class is to handle all kind of notification Settings.</p>
 * 
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/notification-settings.properties", scope = ServiceScope.PROTOTYPE, service = NotificationSettingsResource.class)
public class NotificationSettingsResourceImpl extends BaseNotificationSettingsResourceImpl {

	Log _log = LogFactoryUtil.getLog(getClass());

	@Override
	public NotificationSettings getNotificationSettingsPage(Long appUserId) throws Exception {
		com.notification.settings.model.NotificationSettings dbNotificationSettings = null;
		NotificationSettings notificationSettings = new NotificationSettings();

		if (Validator.isNull(appUserId)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-id-required"));
		}

		try {

			dbNotificationSettings = notificationSettingsLocalService.fetchByAppUserId(appUserId.intValue());
			notificationSettings.setAppUserId((long) dbNotificationSettings.getAppUserId());
			notificationSettings.setNews(dbNotificationSettings.getNews());
			notificationSettings.setEvents(dbNotificationSettings.getEvents());
			notificationSettings.setHighlight(dbNotificationSettings.getHighlights());
			notificationSettings.setBroadcastMessage(dbNotificationSettings.getBroadcastMessage());
			notificationSettings.setCompanyId(notificationSettings.getCompanyId());
			return notificationSettings;
		} catch (NullPointerException e) {
			_log.error(e.getMessage() + e.getCause());

			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-not-found"));
		}

	}

	@Override
	public NotificationSettings postNotificationSettings(NotificationSettings notificationSettings) {

		com.notification.settings.model.NotificationSettings updatedNotificationSettings = null;
		if (Validator.isNull(notificationSettings.getAppUserId())) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-id-required"));
		}
		if (Validator.isNull(notificationSettings)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "missing-request-body"));
		}
		com.notification.settings.model.NotificationSettings dbNotificationSettings = null;
		try {
			dbNotificationSettings = notificationSettingsLocalService
					.fetchByAppUserId(notificationSettings.getAppUserId().intValue());
		} catch (NullPointerException e) {
			_log.error(e.getMessage());

		}

		if (Validator.isNull(dbNotificationSettings)) {
			dbNotificationSettings = notificationSettingsLocalService
					.createNotificationSettings(CounterLocalServiceUtil.increment());

			dbNotificationSettings.setAppUserId(notificationSettings.getAppUserId().intValue());

			dbNotificationSettings = setNotificationSettings(notificationSettings, dbNotificationSettings);
			try {
				updatedNotificationSettings = notificationSettingsLocalService
						.addNotificationSettings(dbNotificationSettings);

			} catch (Exception e) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "internal-server-error"));
			}
		} else {

			dbNotificationSettings = setNotificationSettings(notificationSettings, dbNotificationSettings);
			try {
				updatedNotificationSettings = notificationSettingsLocalService
						.updateNotificationSettings(dbNotificationSettings);

			} catch (Exception e) {

				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "internal-server-error"));
			}
		}

		return toNotificationSettings(updatedNotificationSettings);

	}

	private com.notification.settings.model.NotificationSettings setNotificationSettings(
			NotificationSettings notificationSettings,
			com.notification.settings.model.NotificationSettings dbNotificationSettings) {
		dbNotificationSettings.setNews(Optional.ofNullable(notificationSettings.getNews()).orElse(Boolean.FALSE));
		dbNotificationSettings.setEvents(Optional.ofNullable(notificationSettings.getEvents()).orElse(Boolean.FALSE));
		dbNotificationSettings.setHighlights(Optional.ofNullable(notificationSettings.getHighlight()).orElse(Boolean.FALSE));
		dbNotificationSettings.setBroadcastMessage(Optional.ofNullable(notificationSettings.getBroadcastMessage()).orElse(Boolean.FALSE));
		return dbNotificationSettings;
	}

	private static @GraphQLField NotificationSettings toNotificationSettings(
			com.notification.settings.model.NotificationSettings dbNotificationSettings) {

		return new NotificationSettings() {
			private static final long serialVersionUID = 1L;
			{
				appUserId = (long) dbNotificationSettings.getAppUserId();
				news = dbNotificationSettings.getNews();
				events = dbNotificationSettings.getEvents();
				highlight = dbNotificationSettings.getHighlights();
				broadcastMessage = dbNotificationSettings.getBroadcastMessage();

			}
		};

	}

	@Reference
	public NotificationSettingsLocalService notificationSettingsLocalService;

}