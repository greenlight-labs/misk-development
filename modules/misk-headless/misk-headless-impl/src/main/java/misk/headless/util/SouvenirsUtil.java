package misk.headless.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import misk.headless.dto.v1_0.Souvenirs;

public class SouvenirsUtil {

	public static Souvenirs[] getSouvenirs(String emailAddress, Souvenirs souvenirs) {
		Client client = ClientBuilder.newClient();

		Response resource = client.target(CommonConstant.SOUVENIRS_API_URL).request(MediaType.APPLICATION_JSON)
				.header("x-api-key", CommonConstant.SOUVENIRS_API_KEY)
				.post(Entity.entity("{\"email\": \"" + emailAddress + "\"}", MediaType.APPLICATION_JSON));
		try {
			Souvenirs[] recordFound = new ObjectMapper().readValue(resource.readEntity(String.class),
					Souvenirs[].class);
			return recordFound;
		} catch (Exception e) {
			e.printStackTrace();

		}
		Souvenirs[] noRecordFound = new Souvenirs[0];

		return noRecordFound;
	}

}
