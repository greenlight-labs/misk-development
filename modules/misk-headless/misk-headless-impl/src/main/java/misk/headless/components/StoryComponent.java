package misk.headless.components;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.pagination.Pagination;
import misk.headless.constants.ApiKeys;
import misk.headless.util.CommonUtil;
import misk.headless.util.FavouriteUtil;
import stories.service.StoryLocalServiceUtil;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StoryComponent {

    public static long webGroupId = ApiKeys.webGroupId;
    public static long appGroupId = ApiKeys.appGroupId;

    public static int getStoriesCount() {
        return StoryLocalServiceUtil.getStoriesCount(webGroupId);
    }
    public static misk.headless.dto.v1_0.Story[] getStories(String languageId, HttpServletRequest contextHttpServletRequest, UriInfo contextUriInfo) {
        List<stories.model.Story> records = StoryLocalServiceUtil.getStories(webGroupId, 0, 9);
        misk.headless.dto.v1_0.Story[] results = new misk.headless.dto.v1_0.Story[records.size()];
        int i = 0;

        for (stories.model.Story curRecord : records) {
            misk.headless.dto.v1_0.Story result = getDTO(languageId, contextUriInfo, curRecord);
            results[i] = result;
            i++;
        }

        // update events favourite status
        FavouriteUtil.updateStoriesFavouriteStatus(results, contextHttpServletRequest);

        return results;
    }

    public static List<misk.headless.dto.v1_0.Story> getStories(String languageId, Pagination pagination, HttpServletRequest contextHttpServletRequest, UriInfo contextUriInfo) {
        List<stories.model.Story> records = StoryLocalServiceUtil.getStories(webGroupId, pagination.getStartPosition(), pagination.getEndPosition());
        List<misk.headless.dto.v1_0.Story> results = new ArrayList<>();

        for (stories.model.Story curRecord : records) {
            misk.headless.dto.v1_0.Story result = getDTO(languageId, contextUriInfo, curRecord);
            results.add(result);
        }

        // update events favourite status
        FavouriteUtil.updateStoriesFavouriteStatus(results, contextHttpServletRequest);

        return results;
    }

    public static misk.headless.dto.v1_0.Story getStory(Long storyId, String languageId, UriInfo contextUriInfo) {
        stories.model.Story record = StoryLocalServiceUtil.fetchStory(storyId);

        if (Validator.isNull(record)) {
            throw new BadRequestException("No record found with this id.");
        }

        return getDTO(languageId, contextUriInfo, record);
    }

    public static misk.headless.dto.v1_0.Story getDTO(String languageId, UriInfo contextUriInfo, stories.model.Story record) {
        misk.headless.dto.v1_0.Story story = new misk.headless.dto.v1_0.Story();
        story.setStoryId(record.getStoryId());
        story.setTitle(record.getTitle(languageId));
        story.setDate(CommonUtil.convertDateTimeToStringDateTime(record.getDisplayDate()));
        // this field is for listing image - small image size
        //story.setImage(ApiKeys.getBaseURL(contextUriInfo) + curRecord.getSmallImage(languageId));
        story.setImage(ApiKeys.getBaseURL(contextUriInfo) + record.getBigImage(languageId));
        story.setDescription(record.getDescriptionField1(languageId));
        story.setIsFavourite(Boolean.FALSE);
        // generate story detail link
        String language = "en";
        if(Objects.equals(languageId, "ar_SA")){
            language = "ar";
        }
        story.setLink(ApiKeys.getBaseURL(contextUriInfo) +"/"+ language +"/news/-/stories/detail/" + record.getStoryId());
        return story;
    }
}
