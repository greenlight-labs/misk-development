package misk.headless.internal.resource.v1_0;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.DocumentException;

import java.util.List;

import javax.ws.rs.BadRequestException;

import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.ProfileDocumentList;
import misk.headless.resource.v1_0.ProfileDocumentListResource;
import misk.headless.util.CommonConstant;
import misk.headless.util.DocumentMediaUtil;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/profile-document-list.properties",
	scope = ServiceScope.PROTOTYPE, service = ProfileDocumentListResource.class
)
public class ProfileDocumentListResourceImpl
	extends BaseProfileDocumentListResourceImpl {
	
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	protected String baseURL = CommonConstant.BASE_URL;
	public String miskProfileDocment = CommonConstant.PROFILE_DOC_FOLDER_NAME;
	
	public ProfileDocumentList getUploadedFile(Integer appUserId)
		throws Exception {
		
		String appUserIdStr=String.valueOf(appUserId);
		
		if (Validator.isNull(appUserIdStr)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-id-required"));
		}
		

		return this.getUploadedFileList(appUserIdStr);
	}
	
	public ProfileDocumentList getUploadedFileList(String appUserIdStr) throws DocumentException, PortalException {
		ProfileDocumentList profileDocumentList=new ProfileDocumentList();
		DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
		Folder miskProfileDocmentFolder = null;
		Folder userDocmentFolder = null;
		ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),
				this.contextHttpServletRequest);
		Company company = CompanyLocalServiceUtil.getCompany(serviceContext.getCompanyId());
		User user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(),
				CommonConstant.ADMIN_SCREEN_NAME);
		PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.create(user);
		PermissionThreadLocal.setPermissionChecker(permissionChecker);
		PrincipalThreadLocal.setName(user.getUserId());
		String[] profileDocList = {"",""};
		
		miskProfileDocmentFolder = getFolderId(appGroupId, 0, miskProfileDocment);
		if(Validator.isNotNull(miskProfileDocmentFolder)) {
			userDocmentFolder=getFolderId(appGroupId, miskProfileDocmentFolder.getFolderId(), appUserIdStr);
			if(Validator.isNotNull(userDocmentFolder)) {
				List<FileEntry> fileEntries=_dlAppService.getFileEntries(appGroupId, userDocmentFolder.getFolderId());
				if(Validator.isNotNull(fileEntries)) {
					for (int i = 0; i < fileEntries.size(); i++) {
						profileDocList[i] = this.baseURL + _documentMediaUtil.getImageURL(fileEntries.get(i).getFileEntryId(),appGroupId,fileEntries.get(i).getUuid());
						
					}
					profileDocumentList.setUploadedFile(profileDocList);
				}
			}
		}
		profileDocumentList.setUploadedFile(profileDocList);
		return profileDocumentList;
	}
	
	public Folder getFolderId(long repositoryId, long parentFolderId, String name) {
		Folder folder = null;
		try {
			folder = _dlAppService.getFolder(repositoryId, parentFolderId, name);
			if (Validator.isNotNull(folder)) {
				return folder;
			}
		} catch (PortalException e) {
			_log.error("error in  getFolderId ",e);
		}
		return folder;
	}
	
	@Reference
	private DLAppService _dlAppService;
	
	Log _log = LogFactoryUtil.getLog(ProfileDocumentListResourceImpl.class);
}