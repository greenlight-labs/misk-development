package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import java.util.HashMap;
import java.util.List;

import misk.headless.components.EventComponent;
import misk.headless.constants.EventKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import events.service.EventService;
import explore.model.Amenities;
import explore.model.Attractions;
import explore.model.BookSpace;
import explore.model.Gallary;
import explore.service.AmenitiesLocalService;
import explore.service.AttractionsLocalService;
import explore.service.BookSpaceLocalService;
import explore.service.GallaryLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.EventsSection;
import misk.headless.dto.v1_0.Explore;
import misk.headless.dto.v1_0.ExploreList;
import misk.headless.dto.v1_0.FaclitiesList;
import misk.headless.resource.v1_0.ExploreResource;
import misk.headless.util.DocumentMediaUtil;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/explore.properties", scope = ServiceScope.PROTOTYPE, service = ExploreResource.class)
public class ExploreResourceImpl extends BaseExploreResourceImpl {

	Log _log = LogFactoryUtil.getLog(ContactResourceImpl.class);
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	// protected String baseURL = "https://webserver-miskcity-dev.lfr.cloud";

	@Override
	public Explore getExplore(String languageId) throws Exception {
		try {
			return this.getAttractionContent(languageId);
		} catch (Exception ex) {
			_log.error("Error retrieving getMiskForum page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new Explore();
	}

	public Explore getAttractionContent(String languageId) throws DocumentException, PortalException {
		Explore attractionWS = new Explore();
		List<Attractions> attractionList = _attractionsLocalService.getAttractionses(-1, -1);
		Attractions attractionDB = null;
		long attractionid = 0;
		if (Validator.isNotNull(attractionList) && attractionList.size() > 0) {
			attractionDB = attractionList.get(0);
			attractionid = attractionDB.getAttractionsId();
		} else {
			return new Explore();
		}
		if (Validator.isNotNull(attractionDB)) {
				System.out.println("attractionDB.getHeaderArticleId()---"+attractionDB.getHeaderArticleId());
			getAboutUsDescription(languageId, attractionWS, attractionDB.getHeaderArticleId());
			attractionWS.setExpolerId(String.valueOf(attractionDB.getAttractionsId()));

			attractionWS.setLocationLabel(attractionDB.getLocationLabel(languageId));
			attractionWS.setLocationLatitude(attractionDB.getLocationLatitude(languageId));
			attractionWS.setLocationLongitude(attractionDB.getLocationLongitude(languageId));
			attractionWS.setGalleryLabel(attractionDB.getGallaryLabel(languageId));
			attractionWS.setFaclitiesLabel(attractionDB.getAmenitiesLabel(languageId));
			attractionWS.setExploreLabel(attractionDB.getBookSpaceLabel(languageId));
		}

		// Gallary
		List<Gallary> gallaryList = _gallaryLocalService.findAllInAttractions(attractionid);
		if (Validator.isNotNull(gallaryList) && gallaryList.size() > 0) {
			String[] galleryImageList = new String[gallaryList.size()];
			int i = 0;
			for (Gallary gg : gallaryList) {
				galleryImageList[i] = ApiKeys.getBaseURL(this.contextUriInfo) + gg.getImage(languageId);
				i++;
			}
			attractionWS.setGalleryImagesList(galleryImageList);
		}

		// Amenities

		List<Amenities> amenitiesList = _amenitiesLocalService.findAllInAttractions(attractionid);
		FaclitiesList[] amenitiesImagesList = new FaclitiesList[amenitiesList.size()];

		if (Validator.isNotNull(amenitiesList) && amenitiesList.size() > 0) {
			int i = 0;
			for (Amenities ament : amenitiesList) {
				FaclitiesList amenitiesListObj = new FaclitiesList();
				amenitiesListObj
						.setFaclitiesImages(ApiKeys.getBaseURL(this.contextUriInfo) + ament.getImage(languageId));
				amenitiesListObj.setFaclitiesImagesLabel(ament.getTitle(languageId));
				amenitiesImagesList[i] = amenitiesListObj;
				i++;
			}
			attractionWS.setFaclitiesList(amenitiesImagesList);
		}

		List<BookSpace> bookSpaceList = _bookSpaceLocalService.findAllInAttractions(attractionid);
		ExploreList[] bookSList = new ExploreList[bookSpaceList.size()];

		if (Validator.isNotNull(bookSpaceList) && bookSpaceList.size() > 0) {

			int i = 0;
			for (BookSpace bkspace : bookSpaceList) {
				ExploreList bookSpaceListObj = new ExploreList();
				bookSpaceListObj.setExploreImages(
						ApiKeys.getBaseURL(this.contextUriInfo) + bkspace.getBookSpaceImage(languageId));
				bookSpaceListObj.setExploreIcon(ApiKeys.getBaseURL(this.contextUriInfo) + bkspace.getIcon(languageId));
				bookSpaceListObj.setExploreTitle(bkspace.getBookSpaceTitle(languageId));
				bookSpaceListObj.setCapacity(bkspace.getCapacity(languageId));
				bookSpaceListObj.setArea(bkspace.getArea(languageId));
				bookSpaceListObj.setDuration(bkspace.getDuration(languageId));
				bookSList[i] = bookSpaceListObj;
				i++;
			}
			attractionWS.setExploreList(bookSList);
		}

		try {
			attractionWS.setEventsSection(getEventSection(languageId, attractionDB.getEventId()));
		} catch (Exception e) {
			_log.error("error while getting event list");
		}
		return attractionWS;
	}

	private EventsSection getEventSection(String languageId, String eventType) throws Exception {
		EventsSection eventsection = new EventsSection();
		String sectionTitle = this.getSectionTitle(languageId, "eventsSectionTitle");
		eventsection.setSectionTitle(sectionTitle);
		eventsection.setItems(EventComponent.getEventsByPageSlug(languageId, EventKeys.PAGE_ABOUT_CITY, this.contextHttpServletRequest, this.contextUriInfo));

		return eventsection;
	}

	private void getAboutUsDescription(String languageId, Explore attractionWS, String articleId)
			throws DocumentException, PortalException {
		long groupId = appGroupId;
		JournalArticle article = null;
		if (Validator.isNull(articleId) || articleId.isEmpty()) {
			articleId = "MISK_ABOUT_DISC";
		}
		try {
		article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId,
				WorkflowConstants.STATUS_APPROVED);
		String articleContent = article.getContentByLocale(languageId);
		Document document = SAXReaderUtil.read(articleContent);
		
		Node aboutUsBanner = document.selectSingleNode("/root/dynamic-element[@name='AboutUsBanner']/dynamic-content");
		DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
		attractionWS.setHeaderBanner(
				ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(aboutUsBanner.getText()));

		Node aboutUsLabel = document.selectSingleNode("/root/dynamic-element[@name='AboutUsLabel']/dynamic-content");
		attractionWS.setHeaderTitle(aboutUsLabel.getText());

		Node aboutUsDesc = document.selectSingleNode("/root/dynamic-element[@name='AboutUsDesc']/dynamic-content");
		attractionWS.setHeaderDesc(aboutUsDesc.getText());

		}catch(Exception e) {
			 e.printStackTrace();
		}
	}

	public HashMap<String, String> getPageSectionsTitle(String languageId) throws Exception{
		String articleId = "APP_ABOUT_MISK_CITY_SECTIONS_TITLE";

		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(ApiKeys.appGroupId, articleId, 0 );
		String articleContent = article.getContentByLocale(languageId);
		Document document = SAXReaderUtil.read(articleContent);
		Node root = document.selectSingleNode("/root");

		Node eventsSectionTitleNode = root.selectSingleNode("dynamic-element[@name='EventSectionTitle']/dynamic-content");

		HashMap<String, String> sectionTitles = new HashMap<String, String>();
		sectionTitles.put("eventsSectionTitle", eventsSectionTitleNode.getText());

		return sectionTitles;
	}

	private String getSectionTitle(String languageId, String sectionTitleKey) throws Exception {
		HashMap<String, String> sectionTitles = this.getPageSectionsTitle(languageId);
		return sectionTitles.get(sectionTitleKey);
	}

	@Reference
	private EventService _eventService;

	@Reference
	private AttractionsLocalService _attractionsLocalService;

	@Reference
	GallaryLocalService _gallaryLocalService;

	@Reference
	AmenitiesLocalService _amenitiesLocalService;

	@Reference
	BookSpaceLocalService _bookSpaceLocalService;
}