package misk.headless.internal.resource.v1_0;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.CommunityWall;
import misk.headless.internal.resource.v1_0.schedulers.DllFolderUtil;
import misk.headless.resource.v1_0.CommunityWallResource;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/community-wall.properties", scope = ServiceScope.PROTOTYPE, service = CommunityWallResource.class)
public class CommunityWallResourceImpl extends BaseCommunityWallResourceImpl {

	static final Log logger = LogFactoryUtil.getLog(CommunityWallResourceImpl.class);

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	private String folderName = "Event Cards";

	@Override
	public CommunityWall getCommunityWall() throws Exception {
		String JsonCards = StringPool.BLANK;
		CommunityWall communityWall = new CommunityWall();
		try {
			DLFolder dlFolder = DLFolderLocalServiceUtil.getFolder(webGroupId, 0, folderName);
			DLFileEntry dlList = DLFileEntryLocalServiceUtil.getFileEntry(webGroupId, dlFolder.getFolderId(),
					folderName);
			InputStream is = dlList.getContentStream();
			JsonCards = DllFolderUtil.convertInputStreamToString(is);
			if (logger.isInfoEnabled())
				logger.info(JsonCards);
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		communityWall.setData(JsonCards);
		return communityWall;
	}

}