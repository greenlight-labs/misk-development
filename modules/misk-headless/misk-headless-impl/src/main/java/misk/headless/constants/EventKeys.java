package misk.headless.constants;

public class EventKeys {
	public static final String PAGE_HOME = "home";
	public static final String PAGE_EXPERIENCE_CENTER = "experience-center";
	public static final String PAGE_ABOUT_CITY = "about-city";
	public static final String PAGE_WADI_C = "wadi-c";
}
