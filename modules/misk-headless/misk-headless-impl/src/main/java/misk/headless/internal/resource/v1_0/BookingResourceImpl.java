package misk.headless.internal.resource.v1_0;

import bookings.model.impl.BookingImpl;
import bookings.service.BookingLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.*;

import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;

import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import com.liferay.portal.vulcan.pagination.Page;
import courts.model.Court;
import courts.model.Location;
import courts.model.Category;
import courts.service.CategoryLocalService;
import courts.service.CourtLocalService;
import courts.service.LocationLocalService;
import misk.app.users.model.AppUser;
import misk.app.users.service.AppUserLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.util.CommonUtil;
import misk.headless.util.CourtTimingActivityUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.resource.v1_0.BookingResource;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/booking.properties", scope = ServiceScope.PROTOTYPE, service = BookingResource.class)
public class BookingResourceImpl extends BaseBookingResourceImpl {

	Log _log = LogFactoryUtil.getLog(getClass());
	public static String QR_API_DATA_URL = new String("https://api.qrserver.com/v1/create-qr-code/?size=350x350&data=");

	@Override
	public Map<String, String> getBookingDetails(@NotNull Long bookingId, @NotNull String languageId) throws Exception {

		bookings.model.Booking entry = _bookingLocalService.fetchBooking(bookingId);
		if (Validator.isNull(entry)) {
			throw new BadRequestException("No record exist with this booking id.");
		}

		//@MY_TODO: fix multiple sql queries in loop, create custom-sql finder method
		courts.model.Court court = _courtLocalService.fetchCourt(entry.getCourtId());
		courts.model.Category category = _categoryLocalService.fetchCategory(court.getCategoryId());
		courts.model.Location location = _locationLocalService.fetchLocation(category.getLocationId());
		AppUser appUser = _appUserLocalService.fetchAppUser(entry.getAppUserId());

		Booking booking = new Booking();
		Map<String, String> response = new HashMap<>();

		booking.setBookingId(bookingId);
		booking.setCourtId(entry.getCourtId());
		booking.setCourtName(court.getName(languageId));
		booking.setCourtLocation(location.getName(languageId));
		booking.setAppUserId(entry.getAppUserId());
		booking.setAppUserName(appUser.getFullName());
		booking.setAppUserEmail(appUser.getEmailAddress());
		booking.setBookingDate(CommonUtil.convertDateTimeToStringDate(entry.getSlotStartTime()));
		booking.setSlotStartTime(CommonUtil.convertDateTimeToStringTime(entry.getSlotStartTime()));
		booking.setSlotEndTime(CommonUtil.convertDateTimeToStringTime(entry.getSlotEndTime()));

		response.put("qrCodeURL", QR_API_DATA_URL + booking.toString());
		return response;
	}

	@Override
	public BookingTermsConditions getCourtBookingTermsConditions(String languageId)
			throws Exception {

		BookingTermsConditions bookingTermsConditions = new BookingTermsConditions();
		bookingTermsConditions.setTitle("Terms & Conditions");
		bookingTermsConditions.setExcerpt("To continue booking our courts, please review and accept our terms and conditions");
		bookingTermsConditions.setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos omnis placeat quod rerum voluptatem. Animi architecto aut eos in inventore laboriosam nisi officiis, quia recusandae soluta velit voluptatibus! A beatae consequatur cumque, debitis doloremque doloribus, ea excepturi ipsa magnam minima, mollitia nulla soluta sunt ullam voluptate voluptatem voluptatibus? Esse, numquam!");
		return bookingTermsConditions;
	}

	@Override
	public Page<TimeSlot> getCourtBookingTimeSlots(Long courtId, String languageId, Date date)
			throws Exception {
		//@MY_TODO: validate against CourtTiming & CourtActivity
		//*********** courtActivity ********************
		// Boolean isFullAvailable = _courtActivityUtil.isCourtAvailableToday();
		// Boolean isPartiallyAvailable = _courtActivityUtil.isCourtPartiallyAvailableToday();
		//*********** courtTiming ********************
		List<TimeSlot> courtTimeSlots = _courtTimingActivityUtil.getAvailableSlots(courtId, date);

		return Page.of(courtTimeSlots);

		/*String[] slotStartTime = {"09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00"};
		String[] slotEndTime = {"10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00"};
		List<TimeSlot> timeSlots = new ArrayList<>();
		for (int i = 0; i < slotStartTime.length; i++) {
			TimeSlot timeSlot = new TimeSlot();
			timeSlot.setSlotStartTime(slotStartTime[i]);
			timeSlot.setSlotEndTime(slotEndTime[i]);
			timeSlots.add(timeSlot);
		}
		return Page.of(timeSlots);*/
	}

	@Override
	public Booking createBooking(String languageId, Booking booking)
			throws Exception {

		try{
			if (Validator.isNull(booking.getCourtId()) ||
					Validator.isNull(booking.getAppUserId()) ||
					Validator.isNull(booking.getBookingDate()) ||
					Validator.isNull(booking.getSlotStartTime()) ||
					Validator.isNull(booking.getSlotEndTime())) {
				throw new BadRequestException("Court, Customer, Booking Date, Start Time and End Time fields are required.");
			}

			Date slotStartTime = CommonUtil.convertStringToDateTime(booking.getBookingDate()+" "+booking.getSlotStartTime());
			Date slotEndTime = CommonUtil.convertStringToDateTime(booking.getBookingDate()+" "+booking.getSlotEndTime());

			boolean isSlotBooked = isSlotBooked(booking.getCourtId(), slotStartTime, slotEndTime);

			if(isSlotBooked){
				throw new BadRequestException("Current Slot already booked.");
			}

			bookings.model.Booking entry = new BookingImpl();
			entry.setCourtId(booking.getCourtId());
			entry.setAppUserId(booking.getAppUserId());
			entry.setSlotStartTime(slotStartTime);
			entry.setSlotEndTime(slotEndTime);
			entry.setStatus(true);
			entry.setSync(false);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					bookings.model.Booking.class.getName(), this.contextHttpServletRequest);
			serviceContext.setCompanyId(this.contextCompany.getCompanyId());
			serviceContext.setScopeGroupId(ApiKeys.appGroupId);

			// fetch court & appUser
			Court court = _courtLocalService.fetchCourt(booking.getCourtId());
			Category courtCategory = _categoryLocalService.fetchCategory(court.getCategoryId());
			Location courtLocation = _locationLocalService.fetchLocation(courtCategory.getLocationId());
			AppUser appUser = _appUserLocalService.fetchAppUser(booking.getAppUserId());

			// Add entry
			bookings.model.Booking addedEntry = _bookingLocalService.addEntry(entry, serviceContext);

			RequestStatus requestStatus = new RequestStatus();

			// send booking email
			//SendEmailUtil.sendRegistrationOtp(addedEntry.getAppUserId(), addedEntry.getEmailAddress(), passcode);

			requestStatus.setStatus(true);
			requestStatus.setMessage("Booking has been completed successfully.");

			return toBookingQRCode(languageId, addedEntry, requestStatus, court, appUser, courtLocation);
		} catch (Exception ex) {
			_log.error("Error creating booking: " + ex.getMessage(), ex);
			throw ex;
		}
	}

	private boolean isSlotBooked(long courtId, Date slotStartTime, Date slotEndTime) {
		int count = _bookingLocalService.isSlotBooked(courtId, slotStartTime, slotEndTime);
		return count > 0;
	}

	@Override
	public Booking updateBooking(String languageId, Booking booking)
			throws Exception {

		try{
			if (Validator.isNull(booking.getCourtId()) ||
					Validator.isNull(booking.getAppUserId()) ||
					Validator.isNull(booking.getSlotStartTime()) ||
					Validator.isNull(booking.getSlotEndTime())) {
				throw new BadRequestException("Court, Customer, Start Time and End TIme fields are required.");
			}

			bookings.model.Booking entry = _bookingLocalService.fetchBooking(booking.getBookingId());
			if (Validator.isNull(entry)) {
				throw new BadRequestException("No record exist with this booking id.");
			}

			String slotStartTime = booking.getBookingDate()+" "+booking.getSlotStartTime();
			String slotEndTime = booking.getBookingDate()+" "+booking.getSlotEndTime();

			entry.setCourtId(booking.getCourtId());
			entry.setAppUserId(booking.getAppUserId());
			entry.setSlotStartTime(CommonUtil.convertStringToDateTime(slotStartTime));
			entry.setSlotEndTime(CommonUtil.convertStringToDateTime(slotEndTime));
			entry.setStatus(true);
			entry.setSync(false);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					bookings.model.Booking.class.getName(), this.contextHttpServletRequest);
			serviceContext.setCompanyId(this.contextCompany.getCompanyId());
			serviceContext.setScopeGroupId(ApiKeys.appGroupId);

			// fetch court & appUser
			Court court = _courtLocalService.fetchCourt(booking.getCourtId());
			Category courtCategory = _categoryLocalService.fetchCategory(court.getCategoryId());
			Location courtLocation = _locationLocalService.fetchLocation(courtCategory.getLocationId());
			AppUser appUser = _appUserLocalService.fetchAppUser(booking.getAppUserId());

			// Add entry
			bookings.model.Booking updatedEntry = _bookingLocalService.updateEntry(entry, serviceContext);

			RequestStatus requestStatus = new RequestStatus();

			// send booking email
			//SendEmailUtil.sendRegistrationOtp(addedEntry.getAppUserId(), addedEntry.getEmailAddress(), passcode);

			requestStatus.setStatus(true);
			requestStatus.setMessage("Booking has been updated successfully.");

			return toBookingQRCode(languageId, updatedEntry, requestStatus, court, appUser, courtLocation);
		} catch (Exception ex) {
			_log.error("Error creating booking: " + ex.getMessage(), ex);
			throw ex;
		}
	}

	@Override
	public Booking deleteBooking(Long bookingId)
			throws Exception {

		try{
			_bookingLocalService.deleteBooking(bookingId);

			RequestStatus requestStatus = new RequestStatus();
			requestStatus.setStatus(true);
			requestStatus.setMessage("Booking has been deleted successfully.");

			return toBookingStatus(requestStatus);
		} catch (Exception ex) {
			_log.error("Error creating booking: " + ex.getMessage(), ex);
			throw ex;
		}
	}

	private static @GraphQLField Booking toBookingQRCode(String languageId, bookings.model.Booking booking, RequestStatus requestStatus, Court court, AppUser appUser, Location location) {
		RequestStatus newRequestStatus = requestStatus;
		Court newCourt = court;
		return new Booking() {
			{
				bookingId = booking.getBookingId();
				courtId = newCourt.getCourtId();
				courtName = newCourt.getName(languageId);
				courtLocation = location.getName(languageId);
				appUserId = booking.getAppUserId();
				appUserName = appUser.getFullName();
				appUserEmail = appUser.getEmailAddress();
				bookingDate = CommonUtil.convertDateTimeToStringDate(booking.getSlotStartTime());
				slotStartTime = CommonUtil.convertDateTimeToStringTime(booking.getSlotStartTime());
				slotEndTime = CommonUtil.convertDateTimeToStringTime(booking.getSlotEndTime());
				requestStatus = newRequestStatus;
			}
		};
	}

	private static @GraphQLField Booking toBookingStatus(RequestStatus requestStatus) {
		RequestStatus newRequestStatus = requestStatus;
		return new Booking() {
			{
				requestStatus = newRequestStatus;
			}
		};
	}

	@Reference
	private BookingLocalService _bookingLocalService;

	@Reference
	private CourtLocalService _courtLocalService;

	@Reference
	private CategoryLocalService _categoryLocalService;

	@Reference
	private LocationLocalService _locationLocalService;

	@Reference
	private AppUserLocalService _appUserLocalService;

	@Reference
	private CourtTimingActivityUtil _courtTimingActivityUtil;

}