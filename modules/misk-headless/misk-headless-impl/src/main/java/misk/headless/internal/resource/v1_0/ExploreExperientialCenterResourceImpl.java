package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import explore.experiential.center.model.Category;
import explore.experiential.center.model.Gallery;
import explore.experiential.center.model.Post;
import explore.experiential.center.service.CategoryLocalServiceUtil;
import explore.experiential.center.service.GalleryLocalServiceUtil;
import explore.experiential.center.service.PostLocalServiceUtil;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.ExploreExperientialCenterResource;

import misk.headless.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/explore-experiential-center.properties",
	scope = ServiceScope.PROTOTYPE,
	service = ExploreExperientialCenterResource.class
)
public class ExploreExperientialCenterResourceImpl
	extends BaseExploreExperientialCenterResourceImpl {

	@Override
	public ExploreExperientialCenterPage getExploreExperientialCenter(String languageId) throws Exception {

		//@MY_TODO: add pagination here
		try {
			ExploreExperientialCenterPage page = new ExploreExperientialCenterPage();
			page.setPageTitle(this.getPageTitle(languageId));
			page.setExploreExperientialCenterCategories(this.getCategories(languageId));
			page.setExploreExperientialCenterPosts(this.getPosts(languageId));

			return page;
		} catch (Exception ex) {
			_log.error("Error retrieving related courts data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	private String getPageTitle(String languageId) throws Exception{
		String articleId = "APP_PAGE_EXPLORE_EXPERIENTIAL_CENTER";
		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(ApiKeys.appGroupId, articleId, 0);
		String articleContent = article.getContentByLocale(languageId);
		Document document = SAXReaderUtil.read(articleContent);

		Node pageTitle = document.selectSingleNode("/root/dynamic-element[@name='PageTitle']/dynamic-content");
		return pageTitle.getText();
	}

	private ExploreExperientialCenterCategory[] getCategories(String languageId) {
		List<ExploreExperientialCenterCategory> results = new ArrayList<>();
		List<Category> records = CategoryLocalServiceUtil.findByGroupId(ApiKeys.appGroupId);

		for (Category curRecord : records) {
			ExploreExperientialCenterCategory result = new ExploreExperientialCenterCategory();
			result.setId(curRecord.getCategoryId());
			result.setName(curRecord.getName(languageId));
			result.setImage(CommonUtil.getMediaUrl(curRecord.getImage(languageId), this.contextUriInfo));
			result.setHeading(curRecord.getHeading(languageId));
			result.setDescription(curRecord.getDescription(languageId));
			result.setEstimatedTourTimeLabel(curRecord.getEstimatedTourTimeLabel(languageId));
			result.setEstimatedTourTime(curRecord.getEstimatedTourTime(languageId));
			results.add(result);
		}

		return results.toArray(new ExploreExperientialCenterCategory[0]);
	}

	private ExploreExperientialCenterPost[] getPosts(String languageId) {
		List<ExploreExperientialCenterPost> results = new ArrayList<>();
		List<Post> records = PostLocalServiceUtil.findByGroupId(ApiKeys.appGroupId);

		for (Post curRecord : records) {
			ExploreExperientialCenterPost result = new ExploreExperientialCenterPost();
			result.setId(curRecord.getPostId());
			result.setCategoryId(curRecord.getCategoryId());
			result.setName(curRecord.getName(languageId));
			result.setDescription(curRecord.getDescription(languageId));
			result.setEstimatedTourTimeLabel(curRecord.getEstimatedTourTimeLabel(languageId));
			result.setEstimatedTourTime(curRecord.getEstimatedTourTime(languageId));
			result.setType(curRecord.getType());
			if(Objects.equals(curRecord.getType(), "image")){
				result.setImage(CommonUtil.getMediaUrl(curRecord.getImage(), this.contextUriInfo));
			}else if(Objects.equals(curRecord.getType(), "video")) {
				result.setImage(CommonUtil.getMediaUrl(curRecord.getImage(), this.contextUriInfo));
				result.setVideo(CommonUtil.getMediaUrl(curRecord.getVideo(), this.contextUriInfo));
			}else if(Objects.equals(curRecord.getType(), "souvenir")) {
				//@MY_TODO: Souvenir matching is capitalize in Mobile App
				result.setType(StringUtils.capitalize(curRecord.getType()));
				result.setImage(CommonUtil.getMediaUrl(curRecord.getImage(), this.contextUriInfo));
				result.setButtonLabel(curRecord.getButtonLabel(languageId));
			}else if(Objects.equals(curRecord.getType(), "gallery")) {
				result.setGalleryImages(this.getGalleryImages(curRecord.getPostId()));
			}else if(Objects.equals(curRecord.getType(), "audio")) {
				result.setAudioSection(this.getAudioSection(languageId, curRecord));
			}
			results.add(result);
		}

		return results.toArray(new ExploreExperientialCenterPost[0]);
	}

	private String[] getGalleryImages(long postId) {
		List<String> results = new ArrayList<>();
		List<Gallery> records = GalleryLocalServiceUtil.findByPostId(postId);
		for (Gallery curRecord : records) {
			results.add(CommonUtil.getMediaUrl(curRecord.getImage(), this.contextUriInfo));
		}

		return results.toArray(new String[0]);
	}

	private AudioSection getAudioSection(String languageId, Post curRecord) {
		AudioSection section = new AudioSection();
		section.setCatColor(curRecord.getAudioCatColor());
		section.setCatName(curRecord.getAudioCatName(languageId));
		section.setName(curRecord.getAudioName(languageId));
		section.setProfession(curRecord.getAudioProfession(languageId));
		section.setLocation(curRecord.getAudioLocation(languageId));
		section.setImage(CommonUtil.getMediaUrl(curRecord.getImage(), this.contextUriInfo));
		section.setAudio(CommonUtil.getMediaUrl(curRecord.getAudio(), this.contextUriInfo));
		return section;
	}

	Log _log = LogFactoryUtil.getLog(ExploreExperientialCenterResourceImpl.class);
}