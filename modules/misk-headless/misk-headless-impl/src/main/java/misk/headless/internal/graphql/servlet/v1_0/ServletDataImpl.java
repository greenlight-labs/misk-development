package misk.headless.internal.graphql.servlet.v1_0;

import com.liferay.portal.vulcan.graphql.servlet.ServletData;

import javax.annotation.Generated;

import misk.headless.internal.graphql.mutation.v1_0.Mutation;
import misk.headless.internal.graphql.query.v1_0.Query;
import misk.headless.resource.v1_0.AppUserResource;
import misk.headless.resource.v1_0.AttractionResource;
import misk.headless.resource.v1_0.BannerImagesResource;
import misk.headless.resource.v1_0.BookingResource;
import misk.headless.resource.v1_0.CommunityWallResource;
import misk.headless.resource.v1_0.ContactResource;
import misk.headless.resource.v1_0.CourtResource;
import misk.headless.resource.v1_0.EventListResource;
import misk.headless.resource.v1_0.ExperientialCenterResource;
import misk.headless.resource.v1_0.ExploreResource;
import misk.headless.resource.v1_0.ExploreWadiCResource;
import misk.headless.resource.v1_0.FaqResource;
import misk.headless.resource.v1_0.FeedbackResource;
import misk.headless.resource.v1_0.GlobalSearchResource;
import misk.headless.resource.v1_0.HenaCafeResource;
import misk.headless.resource.v1_0.HomeResource;
import misk.headless.resource.v1_0.MapLocationResource;
import misk.headless.resource.v1_0.MiskForumResource;
import misk.headless.resource.v1_0.MyFavoritesResource;
import misk.headless.resource.v1_0.NotificationSettingsResource;
import misk.headless.resource.v1_0.ProfileDocumentListResource;
import misk.headless.resource.v1_0.ProfileDocumentResource;
import misk.headless.resource.v1_0.PushNotificationResource;
import misk.headless.resource.v1_0.SouvenirsResource;
import misk.headless.resource.v1_0.StoryResource;
import misk.headless.resource.v1_0.TermsnConditionsResource;
import misk.headless.resource.v1_0.WadiCResource;
import misk.headless.resource.v1_0.WelcomeResource;

import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentServiceObjects;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceScope;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Component(immediate = true, service = ServletData.class)
@Generated("")
public class ServletDataImpl implements ServletData {

	@Activate
	public void activate(BundleContext bundleContext) {
		Mutation.setAppUserResourceComponentServiceObjects(
			_appUserResourceComponentServiceObjects);
		Mutation.setBookingResourceComponentServiceObjects(
			_bookingResourceComponentServiceObjects);
		Mutation.setEventListResourceComponentServiceObjects(
			_eventListResourceComponentServiceObjects);
		Mutation.setFeedbackResourceComponentServiceObjects(
			_feedbackResourceComponentServiceObjects);
		Mutation.setMyFavoritesResourceComponentServiceObjects(
			_myFavoritesResourceComponentServiceObjects);
		Mutation.setNotificationSettingsResourceComponentServiceObjects(
			_notificationSettingsResourceComponentServiceObjects);
		Mutation.setProfileDocumentResourceComponentServiceObjects(
			_profileDocumentResourceComponentServiceObjects);
		Mutation.setPushNotificationResourceComponentServiceObjects(
			_pushNotificationResourceComponentServiceObjects);
		Mutation.setSouvenirsResourceComponentServiceObjects(
			_souvenirsResourceComponentServiceObjects);

		Query.setAppUserResourceComponentServiceObjects(
			_appUserResourceComponentServiceObjects);
		Query.setAttractionResourceComponentServiceObjects(
			_attractionResourceComponentServiceObjects);
		Query.setBannerImagesResourceComponentServiceObjects(
			_bannerImagesResourceComponentServiceObjects);
		Query.setBookingResourceComponentServiceObjects(
			_bookingResourceComponentServiceObjects);
		Query.setCommunityWallResourceComponentServiceObjects(
			_communityWallResourceComponentServiceObjects);
		Query.setContactResourceComponentServiceObjects(
			_contactResourceComponentServiceObjects);
		Query.setCourtResourceComponentServiceObjects(
			_courtResourceComponentServiceObjects);
		Query.setEventListResourceComponentServiceObjects(
			_eventListResourceComponentServiceObjects);
		Query.setExperientialCenterResourceComponentServiceObjects(
			_experientialCenterResourceComponentServiceObjects);
		Query.setExploreResourceComponentServiceObjects(
			_exploreResourceComponentServiceObjects);
		Query.setExploreWadiCResourceComponentServiceObjects(
			_exploreWadiCResourceComponentServiceObjects);
		Query.setFaqResourceComponentServiceObjects(
			_faqResourceComponentServiceObjects);
		Query.setGlobalSearchResourceComponentServiceObjects(
			_globalSearchResourceComponentServiceObjects);
		Query.setHenaCafeResourceComponentServiceObjects(
			_henaCafeResourceComponentServiceObjects);
		Query.setHomeResourceComponentServiceObjects(
			_homeResourceComponentServiceObjects);
		Query.setMapLocationResourceComponentServiceObjects(
			_mapLocationResourceComponentServiceObjects);
		Query.setMiskForumResourceComponentServiceObjects(
			_miskForumResourceComponentServiceObjects);
		Query.setMyFavoritesResourceComponentServiceObjects(
			_myFavoritesResourceComponentServiceObjects);
		Query.setNotificationSettingsResourceComponentServiceObjects(
			_notificationSettingsResourceComponentServiceObjects);
		Query.setProfileDocumentListResourceComponentServiceObjects(
			_profileDocumentListResourceComponentServiceObjects);
		Query.setPushNotificationResourceComponentServiceObjects(
			_pushNotificationResourceComponentServiceObjects);
		Query.setStoryResourceComponentServiceObjects(
			_storyResourceComponentServiceObjects);
		Query.setTermsnConditionsResourceComponentServiceObjects(
			_termsnConditionsResourceComponentServiceObjects);
		Query.setWadiCResourceComponentServiceObjects(
			_wadiCResourceComponentServiceObjects);
		Query.setWelcomeResourceComponentServiceObjects(
			_welcomeResourceComponentServiceObjects);
	}

	@Override
	public Mutation getMutation() {
		return new Mutation();
	}

	@Override
	public String getPath() {
		return "/misk-headless-graphql/v1_0";
	}

	@Override
	public Query getQuery() {
		return new Query();
	}

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<AppUserResource>
		_appUserResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<BookingResource>
		_bookingResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<EventListResource>
		_eventListResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<FeedbackResource>
		_feedbackResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<MyFavoritesResource>
		_myFavoritesResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<NotificationSettingsResource>
		_notificationSettingsResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<ProfileDocumentResource>
		_profileDocumentResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<PushNotificationResource>
		_pushNotificationResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<SouvenirsResource>
		_souvenirsResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<AttractionResource>
		_attractionResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<BannerImagesResource>
		_bannerImagesResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<CommunityWallResource>
		_communityWallResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<ContactResource>
		_contactResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<CourtResource>
		_courtResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<ExperientialCenterResource>
		_experientialCenterResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<ExploreResource>
		_exploreResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<ExploreWadiCResource>
		_exploreWadiCResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<FaqResource>
		_faqResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<GlobalSearchResource>
		_globalSearchResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<HenaCafeResource>
		_henaCafeResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<HomeResource>
		_homeResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<MapLocationResource>
		_mapLocationResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<MiskForumResource>
		_miskForumResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<ProfileDocumentListResource>
		_profileDocumentListResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<StoryResource>
		_storyResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<TermsnConditionsResource>
		_termsnConditionsResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<WadiCResource>
		_wadiCResourceComponentServiceObjects;

	@Reference(scope = ReferenceScope.PROTOTYPE_REQUIRED)
	private ComponentServiceObjects<WelcomeResource>
		_welcomeResourceComponentServiceObjects;

}