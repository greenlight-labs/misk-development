package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.vulcan.graphql.annotation.GraphQLField;
import misk.app.users.service.AppUserLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.AccountResource;

import misk.headless.util.CommonUtil;
import misk.headless.util.DocumentMediaUtil;
import misk.headless.util.SendEmailUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
	properties = "OSGI-INF/liferay/rest/v1_0/account.properties",
	scope = ServiceScope.PROTOTYPE, service = AccountResource.class
)
public class AccountResourceImpl extends BaseAccountResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;

	Log _log = LogFactoryUtil.getLog(AccountResourceImpl.class);

	@Override
	public ManageAccountPage getManageAccountPage(String languageId) throws Exception {

		try {
			String articleid = "APP_PAGE_MANAGE_ACCOUNT";
			long groupId = this.appGroupId;
			ManageAccountPage page = new ManageAccountPage();
			JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleid, 0 );
			String articleContent = article.getContentByLocale(languageId);
			Document document = SAXReaderUtil.read(articleContent);

			Node title = document.selectSingleNode("/root/dynamic-element[@name='Title']/dynamic-content");
			page.setTitle(title.getText());
			Node subtitle = document.selectSingleNode("/root/dynamic-element[@name='Subtitle']/dynamic-content");
			page.setSubtitle(subtitle.getText());
			Node description = document.selectSingleNode("/root/dynamic-element[@name='Description']/dynamic-content");
			page.setDescription(description.getText());
			Node deactivateAccountLabel = document.selectSingleNode("/root/dynamic-element[@name='DeactivateAccountLabel']/dynamic-content");
			page.setDeactivateAccountLabel(deactivateAccountLabel.getText());
			Node deleteAccountLabel = document.selectSingleNode("/root/dynamic-element[@name='DeleteAccountLabel']/dynamic-content");
			page.setDeleteAccountLabel(deleteAccountLabel.getText());

			return page;
		} catch (Exception ex) {
			_log.error("Error retrieving manage account page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new ManageAccountPage();
	}

	@Override
	public DeactivateAccountPage getDeactivateAccountPage(String languageId) throws Exception {

		try {
			String articleid = "APP_PAGE_DEACTIVATE_ACCOUNT";
			long groupId = this.appGroupId;
			DeactivateAccountPage page = new DeactivateAccountPage();
			JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleid, 0 );
			String articleContent = article.getContentByLocale(languageId);
			Document document = SAXReaderUtil.read(articleContent);

			Node title = document.selectSingleNode("/root/dynamic-element[@name='Title']/dynamic-content");
			page.setTitle(title.getText());
			Node description = document.selectSingleNode("/root/dynamic-element[@name='Description']/dynamic-content");
			page.setDescription(description.getText());
			Node question = document.selectSingleNode("/root/dynamic-element[@name='Question']/dynamic-content");
			page.setQuestion(question.getText());
			Node questionExcerpt = document.selectSingleNode("/root/dynamic-element[@name='QuestionExcerpt']/dynamic-content");
			page.setQuestionExcerpt(questionExcerpt.getText());
			Node deactivateAccountLabel = document.selectSingleNode("/root/dynamic-element[@name='DeactivateAccountLabel']/dynamic-content");
			page.setDeactivateAccountLabel(deactivateAccountLabel.getText());

			List<String> results = new ArrayList<>();
			List<Node> answers = document.selectNodes("/root/dynamic-element[@name='Answers']");
			for(Node answer : answers){
				Node reason = answer.selectSingleNode("dynamic-content");
				Answer result = new Answer();
				result.setReason(reason.getText());
				results.add(result.getReason());
			}

			page.setAnswers(results.toArray(new String[0]));

			return page;
		} catch (Exception ex) {
			_log.error("Error retrieving deactivate account page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new DeactivateAccountPage();
	}

	@Override
	public DeleteAccountPage getDeleteAccountPage(String languageId) throws Exception {

		try {
			String articleid = "APP_PAGE_DELETE_ACCOUNT";
			long groupId = this.appGroupId;
			DeleteAccountPage page = new DeleteAccountPage();
			JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleid, 0 );
			String articleContent = article.getContentByLocale(languageId);
			Document document = SAXReaderUtil.read(articleContent);

			Node title = document.selectSingleNode("/root/dynamic-element[@name='Title']/dynamic-content");
			page.setTitle(title.getText());
			Node description = document.selectSingleNode("/root/dynamic-element[@name='Description']/dynamic-content");
			page.setDescription(description.getText());
			Node question = document.selectSingleNode("/root/dynamic-element[@name='Question']/dynamic-content");
			page.setQuestion(question.getText());
			Node questionExcerpt = document.selectSingleNode("/root/dynamic-element[@name='QuestionExcerpt']/dynamic-content");
			page.setQuestionExcerpt(questionExcerpt.getText());
			Node deleteAccountLabel = document.selectSingleNode("/root/dynamic-element[@name='DeleteAccountLabel']/dynamic-content");
			page.setDeleteAccountLabel(deleteAccountLabel.getText());

			List<String> results = new ArrayList<>();
			List<Node> answers = document.selectNodes("/root/dynamic-element[@name='Answers']");
			for(Node answer : answers){
				Node reason = answer.selectSingleNode("dynamic-content");
				Answer result = new Answer();
				result.setReason(reason.getText());
				results.add(result.getReason());
			}

			page.setAnswers(results.toArray(new String[0]));

			return page;
		} catch (Exception ex) {
			_log.error("Error retrieving delete account page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return new DeleteAccountPage();
	}

	@Override
	public AppUser deactivateAccount(AppUser appUser) throws Exception {
		if(Validator.isNull(appUser.getAppUserId()) || Validator.isNull(appUser.getDeactivateReason())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "deactivate-account-fields-required"));
		}

		misk.app.users.model.AppUser userEntry = _appUserLocalService.fetchAppUser(appUser.getAppUserId());

		if(Validator.isNotNull(userEntry)){
			userEntry.setStatus(false);
			userEntry.setPhoneNumberVerified(false);
			userEntry.setDeactivateReason(appUser.getDeactivateReason());

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			userEntry = _appUserLocalService.updateEntry(userEntry, serviceContext);
		}else{
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-not-found"));
		}

		RequestStatus requestStatus = new RequestStatus();
		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-deactivated-successful"));

		return toAppUser(userEntry, requestStatus);
	}

	@Override
	public AppUser reactivateAccount(AppUser appUser) throws Exception {
		if(Validator.isNull(appUser.getAppUserId())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-id-required"));
		}

		misk.app.users.model.AppUser userEntry = _appUserLocalService.fetchAppUser(appUser.getAppUserId());

		if(Validator.isNotNull(userEntry)){
			userEntry.setStatus(true);
			userEntry.setPhoneNumberVerified(true);
			userEntry.setDeactivateReason(null);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);

			userEntry = _appUserLocalService.updateEntry(userEntry, serviceContext);
		}else{
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-not-found"));
		}

		RequestStatus requestStatus = new RequestStatus();
		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-reactivated-successful"));

		return toAppUser(userEntry, requestStatus);
	}

	@Override
	public AppUser deleteAccount(AppUser appUser) throws Exception {
		//@MY_TODO: add validation for delete reason field
		if(Validator.isNull(appUser.getAppUserId())){
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "app-user-id-required"));
		}

		misk.app.users.model.AppUser userEntry = _appUserLocalService.fetchAppUser(appUser.getAppUserId());

		if(Validator.isNotNull(userEntry)){

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					misk.app.users.model.AppUser.class.getName(), this.contextHttpServletRequest);
			//@MY_TODO: need to delete user's data from other tables as well (check todo.md)
			userEntry = _appUserLocalService.deleteAppUser(userEntry);
			// send delete account email
			SendEmailUtil.sendDeleteAccountEmail(userEntry, appUser.getDeleteReason());
		}else{
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-not-found"));
		}

		RequestStatus requestStatus = new RequestStatus();
		requestStatus.setStatus(true);
		requestStatus.setMessage(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "account-deleted-successful"));

		return toAppUser(userEntry, requestStatus);
	}

	private static @GraphQLField AppUser toAppUser(misk.app.users.model.AppUser appUser, RequestStatus requestStatus) {
		RequestStatus newRequestStatus = requestStatus;
		return new AppUser() {
			{
				appUserId = appUser.getAppUserId();
				fullName = appUser.getFullName();
				emailAddress = appUser.getEmailAddress();
				phoneNumber = appUser.getPhoneNumber();
				languageId = appUser.getLanguageId();
				requestStatus = newRequestStatus;
				isVerified = appUser.getStatus();
				emailAddressVerified = appUser.getEmailAddressVerified();
				phoneNumberVerified = appUser.getPhoneNumberVerified();
			}
		};
	}

	@Reference
	private AppUserLocalService _appUserLocalService;
}