package misk.headless.social.login;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class JwtUtils {

	private static JsonMapper jsonMapper = JsonMapper.builder()
			.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
			// .addModule(new JavaTimeModule())
			.build();

	private JwtUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static JwtClaims getClaims(String accessToken) {

		if (accessToken == null) {
			throw JwtException.invalidAccessToken("Null access token");
		}
		String[] tokens = accessToken.split("\\.");
		try {
			byte[] claimsBytesDecoded = Base64.getDecoder().decode(tokens[1].getBytes(StandardCharsets.UTF_8));
			return jsonMapper.readValue(claimsBytesDecoded, JwtClaimsImpl.class);
		} catch (IllegalArgumentException | IOException e) {
			throw JwtException.invalidClaims("Failed to parse access_token claims", e);
		}
	}

	public static interface JwtClaims {
		public String getAud();

		public String getSub();

		public String getC_hash();

		public String getEmail_verified();

		public String getIss();

		public String getEmail();

	}

	public static class JwtClaimsImpl implements JwtClaims {

		private String aud;
		private String sub;
		private String c_hash;
		private String email_verified;
		private String iss;
		private String email;

		public String getAud() {
			return aud;
		}

		public void setAud(String aud) {
			this.aud = aud;
		}

		public String getSub() {
			return sub;
		}

		public void setSub(String sub) {
			this.sub = sub;
		}

		public String getC_hash() {
			return c_hash;
		}

		public void setC_hash(String c_hash) {
			this.c_hash = c_hash;
		}

		public String getEmail_verified() {
			return email_verified;
		}

		public void setEmail_verified(String email_verified) {
			this.email_verified = email_verified;
		}

		public String getIss() {
			return iss;
		}

		public void setIss(String iss) {
			this.iss = iss;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		@Override
		public String toString() {
			return "JwtClaimsImpl [aud=" + aud + ", sub=" + sub + ", c_hash=" + c_hash + ", email_verified="
					+ email_verified + ", iss=" + iss + ", email=" + email + "]";
		}

	}

}
