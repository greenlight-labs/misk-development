package misk.headless.internal.resource.v1_0;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryTypeLocalService;
import com.liferay.dynamic.data.mapping.service.DDMStructureService;
import com.liferay.dynamic.data.mapping.util.DDMBeanTranslator;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.multipart.BinaryFile;
import com.liferay.portal.vulcan.multipart.MultipartBody;

import javax.ws.rs.BadRequestException;

import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.ProfileDocument;
import misk.headless.resource.v1_0.ProfileDocumentResource;
import misk.headless.util.CommonConstant;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/profile-document.properties", scope = ServiceScope.PROTOTYPE, service = ProfileDocumentResource.class)
public class ProfileDocumentResourceImpl extends BaseProfileDocumentResourceImpl {

	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	protected String baseURL = CommonConstant.BASE_URL;
	public String miskProfileDocment = CommonConstant.PROFILE_DOC_FOLDER_NAME;

	public ProfileDocument uploadProfileDocuments(MultipartBody multipartBody) throws Exception {

		BinaryFile binaryImgFile = multipartBody.getBinaryFile("formImage");
		BinaryFile binaryPdfFile = multipartBody.getBinaryFile("formPdf");
		String appUserId = multipartBody.getValueAsString("appUserId");
		if (Validator.isNull(binaryImgFile.getFileName()) && Validator.isNull(binaryPdfFile.getFileName())
				|| Validator.isNull(appUserId)) {
			throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "profile-no-file-found"));
		}
		return _addDocument(appGroupId, appGroupId, multipartBody);

	}

	private ProfileDocument _addDocument(Long groupId, long repositoryId, MultipartBody multipartBody)
			throws Exception {
		ProfileDocument document = new ProfileDocument();
		BinaryFile binaryImgFile = multipartBody.getBinaryFile("formImage");
		BinaryFile binaryPdfFile = multipartBody.getBinaryFile("formPdf");
		String appUserId = multipartBody.getValueAsString("appUserId");
		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),
					this.contextHttpServletRequest);
			Company company = CompanyLocalServiceUtil.getCompany(serviceContext.getCompanyId());
			User user = UserLocalServiceUtil.getUserByScreenName(company.getCompanyId(),
					CommonConstant.ADMIN_SCREEN_NAME);
			PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.create(user);
			PermissionThreadLocal.setPermissionChecker(permissionChecker);
			PrincipalThreadLocal.setName(user.getUserId());
			// check parent folder exists ,In case not create it
			Folder miskProfileDocmentFolder = null;
			Folder userDocmentFolder = null;
			if (!isFolderExists(repositoryId, 0, miskProfileDocment)) {
				miskProfileDocmentFolder = createFolder(repositoryId, 0, miskProfileDocment, miskProfileDocment,
						serviceContext);
			} else {
				miskProfileDocmentFolder = getFolderId(repositoryId, 0, miskProfileDocment);
			}
			if (Validator.isNull(miskProfileDocmentFolder)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "parent-folder-not-found"));
			}
			// check userId folder exists ,if Exists create new folder with userid otherwise
			// delete folder for update new images
			if (!isFolderExists(repositoryId, miskProfileDocmentFolder.getFolderId(), String.valueOf(appUserId))) {
				userDocmentFolder = createFolder(repositoryId, miskProfileDocmentFolder.getFolderId(),
						String.valueOf(appUserId), String.valueOf(appUserId), serviceContext);
			} else {
				Folder deleteFolder = getFolderId(repositoryId, miskProfileDocmentFolder.getFolderId(),
						String.valueOf(appUserId));
				if (Validator.isNotNull(deleteFolder)) {
					deleteFolder(deleteFolder.getFolderId());
					userDocmentFolder = createFolder(repositoryId, miskProfileDocmentFolder.getFolderId(),
							String.valueOf(appUserId), String.valueOf(appUserId), serviceContext);
				}
			}
			if (Validator.isNull(userDocmentFolder)) {
				throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-folder-not-created"));
			}

			if (Validator.isNotNull(binaryPdfFile.getFileName())) {
				_dlAppService.addFileEntry(repositoryId, userDocmentFolder.getFolderId(), binaryImgFile.getFileName(),
						binaryImgFile.getContentType(), binaryImgFile.getFileName(), "", "",
						binaryImgFile.getInputStream(), binaryImgFile.getSize(), serviceContext);
			}
			if (Validator.isNotNull(binaryPdfFile.getFileName())) {
				_dlAppService.addFileEntry(repositoryId, userDocmentFolder.getFolderId(), binaryPdfFile.getFileName(),
						binaryPdfFile.getContentType(), binaryPdfFile.getFileName(), "", "",
						binaryPdfFile.getInputStream(), binaryPdfFile.getSize(), serviceContext);
			}
		} catch (Exception e) {
			_log.error("error in adding files ",e);
		}
		return document;
	}

	public void deleteFolder(long folderId) {
		try {
			_dlAppService.deleteFolder(folderId);
		} catch (PortalException e) {
			_log.error("error in deleteFolder ",e);
		}
	}

	public Folder createFolder(long repositoryId, long parentFolderId, String name, String description,
			ServiceContext serviceContext) {
		Folder folder = null;
		try {
			folder = _dlAppService.addFolder(repositoryId, parentFolderId, name, description, serviceContext);

		} catch (PortalException e) {
			_log.error("error in createFolder ",e);
		}
		return folder;
	}

	public boolean isFolderExists(long repositoryId, long parentFolderId, String name) {
		try {
			Folder folder = _dlAppService.getFolder(repositoryId, parentFolderId, name);
			if (Validator.isNotNull(folder)) {
				return true;
			}
		} catch (PortalException e) {
			_log.error("error in  isFolderExists ",e);
		}
		return false;
	}

	public Folder getFolderId(long repositoryId, long parentFolderId, String name) {
		Folder folder = null;
		try {
			folder = _dlAppService.getFolder(repositoryId, parentFolderId, name);
			if (Validator.isNotNull(folder)) {
				return folder;
			}
		} catch (PortalException e) {
			_log.error("error in  getFolderId ",e);
		}
		return folder;
	}

	@Reference
	private DLAppService _dlAppService;
	
	Log _log = LogFactoryUtil.getLog(ProfileDocumentResourceImpl.class);
}
