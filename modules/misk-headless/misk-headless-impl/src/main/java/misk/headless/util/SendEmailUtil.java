package misk.headless.util;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.template.*;
import misk.app.users.model.AppUser;
import org.osgi.service.component.annotations.Component;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;

@Component(immediate = true, service = SendEmailUtil.class)
public class SendEmailUtil {
	private static final Log _log = LogFactoryUtil.getLog(SendEmailUtil.class);

	public static void sendEmail(String from, String to, String subject, String body) {
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;

		/*
		 * String body = ContentUtil.get(this.getClass().getClassLoader().getResource(
		 * "META-INF/resources/templates/admin-email.ftl"), true); body =
		 * StringUtil.replace(body, new String[] {
		 * "[$NAME$]","[$RESULT$]","[$PERCENTAGE$]","[$EXAM$]" }, new String[] { "Ravi",
		 * "CONGRATULATION" ,"80%" , "CCLP"}); /*
		 ********************************************************/

		try {
			fromAddress = new InternetAddress("misk-city@bcw-global.com");
			toAddress = new InternetAddress(to);
			MailMessage clientMailMessage = new MailMessage();
			clientMailMessage.setTo(toAddress);
			clientMailMessage.setFrom(fromAddress);
			clientMailMessage.setSubject(subject);
			clientMailMessage.setBody(body);
			clientMailMessage.setHTMLFormat(true);
			MailServiceUtil.sendEmail(clientMailMessage);
		} catch (AddressException e1) {
			e1.printStackTrace();
		}
	}

	public static void sendEmail(String from, String to, ArrayList<String> toCCAddresses, String subject, String body) {
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		try {
			fromAddress = new InternetAddress("misk-city@bcw-global.com");
			InternetAddress[] toCCInternetAddresses = new InternetAddress[toCCAddresses.size()];
			for (int i = 0; i < toCCAddresses.size(); i++) {
				toCCInternetAddresses[i] = new InternetAddress(toCCAddresses.get(i));
			}
			toAddress = new InternetAddress(to);
			MailMessage clientMailMessage = new MailMessage();
			clientMailMessage.setFrom(fromAddress);
			clientMailMessage.setTo(toAddress);
			clientMailMessage.setCC(toCCInternetAddresses);
			clientMailMessage.setSubject(subject);
			clientMailMessage.setBody(body);
			clientMailMessage.setHTMLFormat(true);
			MailServiceUtil.sendEmail(clientMailMessage);
		} catch (AddressException e1) {
			e1.printStackTrace();
		}
	}

	public static void sendRegistrationOtp(String name, String email, String passcode) {
		try {
			int the_year = LocalDate.now().getYear();
			String templateFile = "META-INF/resources/templates/registration-otp-email.ftl";

			TemplateResource templateResource = new URLTemplateResource("0",
					SendEmailUtil.class.getClassLoader().getResource(templateFile));
			Template template = TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, templateResource,
					false);
			// Add the data-models
			template.put("name", name);
			template.put("passcode", passcode);
			/*
			 * template.put("portalURL", configs.get("portalURL"));
			 * template.put("portletContextPath", configs.get("portletContextPath"));
			 */
			template.put("the_year", the_year);
			StringWriter out = new StringWriter();
			template.processTemplate(out);

			String body = out.toString();
			String subject = "[Misk Foundation] - Registration OTP";

			sendEmail(null, email, subject, body);

		} catch (TemplateException ex) {
			ex.printStackTrace();
		}
	}

	public static void resendOTP(String name, String email, String passcode) {
		try {
			int the_year = LocalDate.now().getYear();
			String templateFile = "META-INF/resources/templates/resend-otp-email.ftl";

			TemplateResource templateResource = new URLTemplateResource("0",
					SendEmailUtil.class.getClassLoader().getResource(templateFile));
			Template template = TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, templateResource,
					false);
			// Add the data-models
			template.put("name", name);
			template.put("passcode", passcode);
			/*
			 * template.put("portalURL", configs.get("portalURL"));
			 * template.put("portletContextPath", configs.get("portletContextPath"));
			 */
			template.put("the_year", the_year);
			StringWriter out = new StringWriter();
			template.processTemplate(out);

			String body = out.toString();
			String subject = "[Misk Foundation] - Validate OTP (One Time Passcode)";

			sendEmail(null, email, subject, body);

		} catch (TemplateException ex) {
			ex.printStackTrace();
		}
	}

	public static void sendMailUsingTemplateForFeedback(String fullName, String email, String type, String message, String languageId) {
		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		String adminBody = "";
		String clientBody = "";

		/*
		 * String body = ContentUtil.get(this.getClass().getClassLoader().getResource(
		 * "META-INF/resources/templates/admin-email.ftl"), true); body =
		 * StringUtil.replace(body, new String[] {
		 * "[$NAME$]","[$RESULT$]","[$PERCENTAGE$]","[$EXAM$]" }, new String[] { "Ravi",
		 * "CONGRATULATION" ,"80%" , "CCLP"}); /*
		 ********************************************************/

		try {
			/* admin email */
			String baseURL = CommonConstant.BASE_URL;
			int the_year = LocalDate.now().getYear();
			String templateFile = "META-INF/resources/templates/feedback/admin-email.ftl";

			TemplateResource templateResource = new URLTemplateResource("0",
					SendEmailUtil.class.getClassLoader().getResource(templateFile));
			Template template = TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, templateResource,
					false);
			// Add the data-models
			template.put("portalURL", baseURL);
			template.put("portletContextPath", baseURL+"/o/forms.web");
			template.put("name", fullName);
			template.put("email", email);
			template.put("phone", type); 
			template.put("question", message);
			template.put("the_year", the_year);
			StringWriter out = new StringWriter();
			template.processTemplate(out);
			adminBody = out.toString();
			/* client email */
			String clientTemplateFile = "META-INF/resources/templates/feedback/client-email-en.ftl";
			if (languageId.equals("ar_SA")) {
				clientTemplateFile = "META-INF/resources/templates/feedback/client-email-ar.ftl";
			}
			TemplateResource clientTemplateResource = new URLTemplateResource("0",
					SendEmailUtil.class.getClassLoader().getResource(clientTemplateFile));
			Template clientTemplate = TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL,
					clientTemplateResource, false);
			// Add the data-models
			clientTemplate.put("portalURL", baseURL);
			clientTemplate.put("portletContextPath", baseURL+"/o/forms.web");
			clientTemplate.put("name", fullName);
			clientTemplate.put("the_year", the_year);
			StringWriter clientOut = new StringWriter();
			clientTemplate.processTemplate(clientOut);
			clientBody = clientOut.toString();
		} catch (TemplateException e1) {
			e1.printStackTrace();
		}

		try {
			/* admin email */
			fromAddress = new InternetAddress("misk-city@bcw-global.com");
			toAddress = new InternetAddress("qatraffictest@gmail.com");
			MailMessage mailMessage = new MailMessage();
			mailMessage.setTo(toAddress);
			mailMessage.setFrom(fromAddress);
			mailMessage.setSubject("[Misk Foundation] - Contact Us Form");
			mailMessage.setBody(adminBody);
			mailMessage.setHTMLFormat(true);
			MailServiceUtil.sendEmail(mailMessage);
			/* client email */
			fromAddress = new InternetAddress("misk-city@bcw-global.com");
			toAddress = new InternetAddress(email);
			MailMessage clientMailMessage = new MailMessage();
			clientMailMessage.setTo(toAddress);
			clientMailMessage.setFrom(fromAddress);
			clientMailMessage.setSubject("[Misk Foundation] - Contact Us Form");
			clientMailMessage.setBody(clientBody);
			clientMailMessage.setHTMLFormat(true);
			MailServiceUtil.sendEmail(clientMailMessage);
		} catch (AddressException e) {
			e.printStackTrace();
		}
	}

	public static void sendDeleteAccountEmail(AppUser appUser, String deleteReason) {
		try {
			int the_year = LocalDate.now().getYear();
			String templateFile = "META-INF/resources/templates/delete-account-email.ftl";

			TemplateResource templateResource = new URLTemplateResource("0",
					SendEmailUtil.class.getClassLoader().getResource(templateFile));
			Template template = TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, templateResource,
					false);
			// Add the data-models
			template.put("name", appUser.getFullName());
			template.put("email", appUser.getEmailAddress());
			template.put("phone", appUser.getPhoneNumber());
			template.put("createdDate", appUser.getCreateDate());
			template.put("deleteReason", deleteReason);
			/*
			 * template.put("portalURL", configs.get("portalURL"));
			 * template.put("portletContextPath", configs.get("portletContextPath"));
			 */
			template.put("the_year", the_year);
			StringWriter out = new StringWriter();
			template.processTemplate(out);

			String body = out.toString();
			String subject = "[Misk Foundation] - Delete Account Notification";
			// multiple toCC addresses
			ArrayList<String> toCCAddresses = new ArrayList<>();
			//toCCAddresses.add("yshehadeh@appswave.io");
			toCCAddresses.add("haneen@appswave.io");
			toCCAddresses.add("taha@greenlightlabs.tech");
			toCCAddresses.add("naeem@greenlightlabs.tech");

			sendEmail(null, "yshehadeh@appswave.io", toCCAddresses, subject, body);

		} catch (TemplateException ex) {
			ex.printStackTrace();
		}
	}
}
