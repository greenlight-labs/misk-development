package misk.headless.internal.resource.v1_0;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import attractions.model.Amenities;
import attractions.model.Attractions;
import attractions.model.BookSpace;
import attractions.model.Gallary;
import attractions.service.AmenitiesLocalService;
import attractions.service.AttractionsLocalService;
import attractions.service.BookSpaceLocalService;
import attractions.service.GallaryLocalService;
import discover.model.Category;
import discover.model.Discover;
import discover.service.CategoryLocalService;
import discover.service.DiscoverLocalService;
import discover.service.GalleryLocalService;
import events.model.EventVisitors;
import events.model.EventsGallery;
import events.service.CategoryLocalServiceUtil;
import events.service.EventService;
import events.service.EventVisitorsLocalService;
import events.service.EventsGalleryLocalServiceUtil;
import misk.app.users.service.AppUserLocalService;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.AddVisitor;
import misk.headless.dto.v1_0.Album;
import misk.headless.dto.v1_0.AmenitiesList;
import misk.headless.dto.v1_0.AppUser;
import misk.headless.dto.v1_0.Attraction;
import misk.headless.dto.v1_0.AttractionSection;
import misk.headless.dto.v1_0.BannerSection;
import misk.headless.dto.v1_0.BookCourtSection;
import misk.headless.dto.v1_0.BookSpaceList;
import misk.headless.dto.v1_0.CourtCategory;
import misk.headless.dto.v1_0.DiscoverSection;
import misk.headless.dto.v1_0.EventList;
import misk.headless.dto.v1_0.EventsSection;
import misk.headless.dto.v1_0.Gallery;
import misk.headless.dto.v1_0.WadiC;
import misk.headless.resource.v1_0.WadiCResource;
import misk.headless.util.DocumentMediaUtil;

/**
 * @author Tayyab Zafar
 */
@Component(properties = "OSGI-INF/liferay/rest/v1_0/wadi-c.properties", scope = ServiceScope.PROTOTYPE, service = WadiCResource.class)
public class WadiCResourceImpl extends BaseWadiCResourceImpl {
	public long webGroupId = ApiKeys.webGroupId;
	public long appGroupId = ApiKeys.appGroupId;
	// protected String baseURL = "https://webserver-miskcity-dev.lfr.cloud";
	Log _log = LogFactoryUtil.getLog(WadiCResourceImpl.class);

	@Override
	public WadiC getWadiC(String languageId) throws Exception {

		try {
			WadiC wadiC = new WadiC();
			wadiC.setBannerSection(this.getBannerSection(languageId));
			// wadiC.setDiscoverSection(this.getDiscoverSection(languageId));
			wadiC.setAttractionSection(this.getAttractionSection(languageId));
			wadiC.setBookCourtSection(this.getBookCourtSection(languageId));
			wadiC.setEventsSection(this.getEventSection(languageId));

			return wadiC;
		} catch (Exception ex) {
			_log.error("Error retrieving experiential center page data: " + ex.getMessage(), ex);

			_log.error(ex.getMessage());
		}
		return null;
	}

	public BannerSection getBannerSection(String languageId) throws Exception {

		long groupId = this.appGroupId;
		String articleId = "APP_WADI_C_HEADER";
		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0);
		String articleContent = article.getContentByLocale(languageId);

		Document document = SAXReaderUtil.read(articleContent);
		Node root = document.selectSingleNode("/root");

		Node headingNode = root.selectSingleNode("dynamic-element[@name='heading']/dynamic-content");
		Node titleField1Node = root.selectSingleNode("dynamic-element[@name='titleField1']/dynamic-content");
		Node titleField2Node = root.selectSingleNode("dynamic-element[@name='titleField2']/dynamic-content");
		Node descriptionNode = root.selectSingleNode("dynamic-element[@name='description']/dynamic-content");
		Node buttonLabelNode = root.selectSingleNode("dynamic-element[@name='buttonLabel']/dynamic-content");
		Node imageNode = root.selectSingleNode("dynamic-element[@name='image']/dynamic-content");

		BannerSection result = new BannerSection();
		result.setHeading(headingNode.getText());
		result.setTitleField1(titleField1Node.getText());
		result.setTitleField2(titleField2Node.getText());
		result.setDescription(descriptionNode.getText());
		result.setButtonLabel(buttonLabelNode.getText());

		DocumentMediaUtil _documentMediaUtil = new DocumentMediaUtil();
		result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + _documentMediaUtil.getImageURL(imageNode.getText()));

		return result;
	}

	private AttractionSection getAttractionSection(String languageId) throws Exception {
		AttractionSection section = new AttractionSection();
		section.setItems(this.getAttractionContent(languageId));

		return section;
	}

	private DiscoverSection getDiscoverSection(String languageId) throws Exception {
		DiscoverSection section = new DiscoverSection();
		String sectionTitle = this.getSectionTitle(languageId, "discoverSectionTitle");
		section.setSectionTitle(sectionTitle);

		// section.setCategoryName(this.getCategory(languageId,));
		section.setItems(this.getAlbums(languageId));

		return section;
	}

	private String getSectionTitle(String languageId, String sectionTitleKey) throws Exception {
		HashMap<String, String> sectionTitles = this.getPageSectionsTitle(languageId);
		return sectionTitles.get(sectionTitleKey);
	}

	public HashMap<String, String> getPageSectionsTitle(String languageId) throws Exception {
		long groupId = this.appGroupId;
		String articleId = "APP_WADI_C_TITLES";

		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0);

		String articleContent = article.getContentByLocale(languageId);
		Document document = SAXReaderUtil.read(articleContent);
		Node root = document.selectSingleNode("/root");

		Node discoverSectionTitleNode = root
				.selectSingleNode("dynamic-element[@name='DiscoverSectionTitle']/dynamic-content");

		HashMap<String, String> sectionTitles = new HashMap<String, String>();
		// sectionTitles.put("highlightsSectionTitle",
		// highlightsSectionTitleNode.getText());
		sectionTitles.put("discoverSectionTitle", discoverSectionTitleNode.getText());

		// sectionTitles.put("eventsSectionTitle", eventsSectionTitleNode.getText());

		return sectionTitles;
	}

	private Album[] getAlbums(String languageId) {
		List<Discover> records = _discoverLocalService.findAllInGroup(this.webGroupId, 0, 9);
		Album[] results = new Album[records.size()];
		int i = 0;

		for (Discover curRecord : records) {
			Album result = new Album();
			result.setTitle(curRecord.getTitle(languageId));
			result.setListingImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getListingImage(languageId));
			result.setDescription(curRecord.getDescription(languageId));
			Category category = _categoryLocalService.fetchCategory(curRecord.getCategoryId());
			result.setCategoryName(category.getName(languageId));
			Gallery[] gallery = this.getAlbumGallery(curRecord.getDiscoverId(), languageId);
			result.setGalleries(gallery);

			results[i] = result;
			i++;
		}
		return results;
	}

	private Gallery[] getAlbumGallery(long discoverId, String languageId) {
		List<discover.model.Gallery> records = _galleryLocalService.findAllInDiscover(discoverId);
		Gallery[] results = new Gallery[records.size()];
		int i = 0;

		for (discover.model.Gallery curRecord : records) {
			Gallery result = new Gallery();
			result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getImage(languageId));
			result.setThumbnail(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getThumbnail(languageId));
			results[i] = result;
			i++;
		}
		return results;
	}

	private BookCourtSection getBookCourtSection(String languageId) throws Exception {
		long groupId = this.appGroupId;
		String articleId = "APP_WADI_C_BOOK_COURT_SECTION";
		JournalArticle article = JournalArticleLocalServiceUtil.fetchLatestArticle(groupId, articleId, 0);
		String articleContent = article.getContentByLocale(languageId);

		Document document = SAXReaderUtil.read(articleContent);
		Node root = document.selectSingleNode("/root");

		Node titleNode = root.selectSingleNode("dynamic-element[@name='Title']/dynamic-content");
		Node descriptionNode = root.selectSingleNode("dynamic-element[@name='Description']/dynamic-content");

		BookCourtSection section = new BookCourtSection();
		section.setTitle(titleNode.getText());
		section.setDescription(descriptionNode.getText());
		section.setCourtCategories(this.getCourtCategories(languageId));

		return section;
	}

	private CourtCategory[] getCourtCategories(String languageId) {
		List<courts.model.Category> records = _courtCategoryLocalService.findAllInGroup(this.appGroupId, 0, 9);
		CourtCategory[] results = new CourtCategory[records.size()];
		int i = 0;

		for (courts.model.Category curRecord : records) {
			CourtCategory result = new CourtCategory();
			result.setId(curRecord.getCategoryId());
			result.setLocationId(curRecord.getLocationId());
			result.setName(curRecord.getName(languageId));
			result.setIcon(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getIcon());
			result.setImage(ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getImage());
			results[i] = result;
			i++;
		}
		return results;
	}

	private EventsSection getEventSection(String languageId) throws Exception {
		EventsSection eventsection = new EventsSection();
		String sectionTitle = this.getSectionTitle(languageId, "eventsSectionTitle");
		eventsection.setSectionTitle(sectionTitle);
		eventsection.setItems(this.getEvents(languageId));

		return eventsection;
	}

	private EventList[] getEvents(String languageId) throws Exception {
		List<events.model.Event> records = _eventService.getEventsByType(3, 0, 9);

		EventList[] results = new EventList[records.size()];
		int i = 0;

		for (events.model.Event curRecord : records) {
			EventList result = new EventList();

			List<EventsGallery> eventg = EventsGalleryLocalServiceUtil.findAllInDiscover(curRecord.getEventId());

			String imagearray[] = new String[eventg.size()];
			for (int j = 0; j < eventg.size(); j++) {
				imagearray[j] = ApiKeys.getBaseURL(this.contextUriInfo) + eventg.get(j).getImage(languageId);
			}

			result.setEventId(curRecord.getEventId());
			result.setTitle(curRecord.getTitle(languageId));
			String paytype = "Free";
			if (curRecord.getPaytype() == 2) {
				paytype = "Paid";
			}
			result.setPayType(paytype);
			result.setEventDescription(curRecord.getEventdescription(languageId));
			result.setTime(curRecord.getTiming(languageId));
			result.setVenue(curRecord.getVenue(languageId));
			Format dateTimeFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("dd MMMM");

			result.setDisplaydate(String.valueOf(curRecord.getDisplayDate()));
			result.setEventMonth(dateTimeFormat.format(curRecord.getDisplayDate()));
			result.setIsFavourite(Boolean.TRUE);
			result.setEventPrice(curRecord.getEventPrice());
			result.setCurrency(curRecord.getCurrency());
			result.setEventFeaturedImage(
					ApiKeys.getBaseURL(this.contextUriInfo) + curRecord.getEventfeaturedImage(languageId));
			events.model.Category cat = CategoryLocalServiceUtil.fetchCategory(curRecord.getCategoryId());
			result.setEventcategory(cat.getName(languageId));
			result.setCatColor(cat.getCatColor());
			result.setEventType(String.valueOf(curRecord.getType()));
			result.setSliderimage(imagearray);

			// get event attendees list
			result.setAttendesList(getAttendeesList(curRecord.getEventId(), languageId));

			results[i] = result;
			i++;
		}
		return results;
	}

	public AddVisitor getAttendeesList(Long eventid, String languageId) throws Exception {
		AddVisitor visitors = new AddVisitor();
		List<EventVisitors> eventVisitors = _eventvisitorService.findEventByvisitor(eventid);
		AppUser[] realtedEventsList = new AppUser[eventVisitors.size()];
		for (int i = 0; i < eventVisitors.size(); i++) {
			AppUser appuseri = new AppUser();
			misk.app.users.model.AppUser userDetails = _appUserLocalService.fetchAppUser(eventVisitors.get(i).getUserId());

			if(Validator.isNotNull(userDetails)){
				if (Validator.isNotNull(userDetails.getProfileImage())) {
					appuseri.setProfileImage(ApiKeys.getBaseURL(this.contextUriInfo) + "/" + userDetails.getProfileImage());
				} else {
					appuseri.setProfileImage(
							ApiKeys.getBaseURL(this.contextUriInfo) + "/documents/91702/0/user-profile-avatar.png");
				}
				appuseri.setAppUserId(userDetails.getAppUserId());
				realtedEventsList[i] = appuseri;
			}
		}
		visitors.setCount(Long.valueOf(eventVisitors.size()));
		visitors.setAppUsers(realtedEventsList);
		return visitors;
	}

	public Attraction[] getAttractionContent(String languageId) throws DocumentException, PortalException {

		List<Attractions> attractionList = _attractionsLocalService.getAttractionses(-1, -1);
		Attraction[] results = new Attraction[attractionList.size()];
		int counter = 0;
		if (Validator.isNotNull(attractionList) && attractionList.size() > 0) {
			for (Attractions attractionDB : attractionList) {
				Attraction attractionWS = new Attraction();
				attractionWS.setAttractionId(String.valueOf(attractionDB.getAttractionsId()));
				attractionWS.setAttractionBanner(
						ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getForumBannerImage(languageId));
				attractionWS.setAttractionTitle(attractionDB.getForumBannerTitle(languageId));
				attractionWS.setAttractionButtonLabel(attractionDB.getForumButtonLabel(languageId));
				attractionWS.setAttractionBannerIcon(
						ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getForumBannerIcon(languageId));
				attractionWS.setBannerButtonColor(attractionDB.getForumButtonColor(languageId));
				attractionWS.setWorkingHoursLabel(attractionDB.getWorkingHoursLabel(languageId));
				attractionWS.setWorkingHours(attractionDB.getWorkingHours(languageId));
				attractionWS.setWorkingDays(attractionDB.getWorkingDays(languageId));
				attractionWS.setDescription(attractionDB.getDescription(languageId));
				attractionWS.setLocationLabel(attractionDB.getLocationLabel(languageId));
				attractionWS.setLocationLatitude(attractionDB.getLocationLatitude(languageId));
				attractionWS.setLocationLongitude(attractionDB.getLocationLongitude(languageId));
				attractionWS.setContactLabel(attractionDB.getContactLabel(languageId));
				attractionWS.setContactEmailAddress(attractionDB.getContactEmailAddress(languageId));
				attractionWS.setContactTelephone(attractionDB.getContactTelephone(languageId));
				attractionWS.setGalleryLabel(attractionDB.getGallaryLabel(languageId));
				attractionWS.setAmenitiesLabel(attractionDB.getAmenitiesLabel(languageId));
				attractionWS.setBookSpaceLabel(attractionDB.getBookSpaceLabel(languageId));

				attractionWS.setWorkingHoursLabelColor(attractionDB.getWorkingHoursLabelColor(languageId));
				attractionWS.setWorkingHoursImage(
						ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getWorkingHoursImage(languageId));
				attractionWS.setWorkingDaysImage(
						ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getWorkingDaysImage(languageId));
				attractionWS.setContactTelephoneIcon(
						ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getContactTelephoneIcon(languageId));
				attractionWS.setContactEmailAddressIcon(
						ApiKeys.getBaseURL(this.contextUriInfo) + attractionDB.getContactEmailAddressIcon(languageId));

				// Gallary
				List<Gallary> gallaryList = _gallaryLocalService.findAllInAttractions(attractionDB.getAttractionsId());
				if (Validator.isNotNull(gallaryList) && gallaryList.size() > 0) {
					String[] galleryImageList = new String[gallaryList.size()];
					int i = 0;
					for (Gallary gg : gallaryList) {
						galleryImageList[i] = ApiKeys.getBaseURL(this.contextUriInfo) + gg.getImage(languageId);
						i++;
					}
					attractionWS.setGalleryImagesList(galleryImageList);
				}

				// Amenities

				List<Amenities> amenitiesList = _amenitiesLocalService
						.findAllInAttractions(attractionDB.getAttractionsId());
				AmenitiesList[] amenitiesImagesList = new AmenitiesList[amenitiesList.size()];

				if (Validator.isNotNull(amenitiesList) && amenitiesList.size() > 0) {
					int i = 0;
					for (Amenities ament : amenitiesList) {
						AmenitiesList amenitiesListObj = new AmenitiesList();
						amenitiesListObj.setAmenitiesImages(
								ApiKeys.getBaseURL(this.contextUriInfo) + ament.getImage(languageId));
						amenitiesListObj.setAmenitiesImagesLabel(ament.getTitle(languageId));
						amenitiesImagesList[i] = amenitiesListObj;
						i++;
					}
					attractionWS.setAmenitiesList(amenitiesImagesList);
				}

				List<BookSpace> bookSpaceList = _bookSpaceLocalService
						.findAllInAttractions(attractionDB.getAttractionsId());
				BookSpaceList[] bookSList = new BookSpaceList[bookSpaceList.size()];

				if (Validator.isNotNull(bookSpaceList) && bookSpaceList.size() > 0) {

					int i = 0;
					for (BookSpace bkspace : bookSpaceList) {
						BookSpaceList bookSpaceListObj = new BookSpaceList();
						bookSpaceListObj.setBookSpaceImages(
								ApiKeys.getBaseURL(this.contextUriInfo) + bkspace.getBookSpaceImage(languageId));
						bookSpaceListObj.setBookSpaceIcon(
								ApiKeys.getBaseURL(this.contextUriInfo) + bkspace.getIcon(languageId));
						bookSpaceListObj.setBookSpaceTitle(bkspace.getBookSpaceTitle(languageId));
						bookSpaceListObj.setCapacity(bkspace.getCapacity(languageId));
						bookSpaceListObj.setArea(bkspace.getArea(languageId));
						bookSpaceListObj.setDuration(bkspace.getDuration(languageId));
						bookSList[i] = bookSpaceListObj;
						i++;
					}
					attractionWS.setBookSpaceList(bookSList);
				}

				results[counter] = attractionWS;
				counter++;
			}

		}
		return results;
	}

	@Reference
	private AttractionsLocalService _attractionsLocalService;

	@Reference
	GallaryLocalService _gallaryLocalService;

	@Reference
	AmenitiesLocalService _amenitiesLocalService;

	@Reference
	BookSpaceLocalService _bookSpaceLocalService;
	/*
	 * @Reference private HighlightLocalService _highlightLocalService;
	 */

	@Reference
	private DiscoverLocalService _discoverLocalService;

	@Reference
	private GalleryLocalService _galleryLocalService;

	@Reference
	private CategoryLocalService _categoryLocalService;

	@Reference
	private courts.service.CategoryLocalService _courtCategoryLocalService;

	@Reference
	private EventService _eventService;

	@Reference
	private EventVisitorsLocalService _eventvisitorService;

	@Reference
	private AppUserLocalService _appUserLocalService;
}