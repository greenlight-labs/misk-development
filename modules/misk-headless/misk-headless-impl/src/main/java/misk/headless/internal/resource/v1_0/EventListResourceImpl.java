package misk.headless.internal.resource.v1_0;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.vulcan.pagination.Page;
import com.liferay.portal.vulcan.pagination.Pagination;
import events.model.Category;
import events.model.Event;
import events.model.EventVisitors;
import events.model.impl.EventVisitorsImpl;
import events.service.*;
import misk.headless.components.EventComponent;
import misk.headless.constants.ApiKeys;
import misk.headless.dto.v1_0.*;
import misk.headless.resource.v1_0.EventListResource;
import misk.headless.util.CommonUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Tayyab Zafar
 */
@Component(
        properties = "OSGI-INF/liferay/rest/v1_0/event-list.properties",
        scope = ServiceScope.PROTOTYPE, service = EventListResource.class
)
public class EventListResourceImpl extends BaseEventListResourceImpl {

    public long webGroupId = ApiKeys.webGroupId;
    public long appGroupId = ApiKeys.appGroupId;

    Log _log = LogFactoryUtil.getLog(EventListResourceImpl.class);

    @Override
    public Page<EventList> getEventsListing(String languageId, String typeevent,
                                            Integer payType, String priceFrom, String priceTo, Long categoryId, String dateFrom, String dateTo,
                                            Pagination pagination)
            throws Exception {
        try {
            HashMap<String, Object> filterObj = validateFilterFields(payType, priceFrom, priceTo, categoryId, dateFrom, dateTo);

            Boolean isFilterSet = isFilterSet(filterObj);

            int count = 0;

            if (isFilterSet) {
                payType = (Integer) filterObj.get("payType");
                priceFrom = (String) filterObj.get("priceFrom");
                priceTo = (String) filterObj.get("priceTo");
                categoryId = (Long) filterObj.get("categoryId");
                dateFrom = (String) filterObj.get("dateFrom");
                dateTo = (String) filterObj.get("dateTo");
                count = _eventLocalService.countByFilter(payType, priceFrom, priceTo, categoryId, dateFrom, dateTo);
            } else {
                count = _eventService.getEventsCount(this.webGroupId);
            }

            return Page.of(this.getEventList(languageId, pagination, filterObj), pagination, count);
        } catch (Exception ex) {
            _log.error("Error retrieving Event Listing page data: " + ex.getMessage(), ex);

            _log.error(ex.getMessage());
        }
        return null;
    }

    protected HashMap<String, Object> validateFilterFields(Integer payType, String priceFrom, String priceTo, Long categoryId, String dateFrom, String dateTo) {

        if (Validator.isNull(payType)) payType = 0;
        if (Validator.isNull(priceFrom)) priceFrom = "";
        if (Validator.isNull(priceTo)) priceTo = "";
        if (Validator.isNull(categoryId)) categoryId = 0L;
        if (Validator.isNull(dateFrom)) dateFrom = "";
        if (Validator.isNull(dateTo)) dateTo = "";

        HashMap<String, Object> filterObj = new HashMap<String, Object>();
        filterObj.put("payType", payType);
        filterObj.put("priceFrom", priceFrom);
        filterObj.put("priceTo", priceTo);
        filterObj.put("categoryId", categoryId);
        filterObj.put("dateFrom", dateFrom);
        filterObj.put("dateTo", dateTo);
        return filterObj;
    }

    @Override
    public AddVisitor AddEventsVisitor(AddVisitor addVisitor) throws PortalException {

        List<EventVisitors> eventVisitors = _eventvisitorService.findEventByUser(addVisitor.getUserId());
        boolean noneMatch = eventVisitors.stream().noneMatch(o -> addVisitor.getEventId().equals(o.getEventvisitorId()));
        if (noneMatch) {
            EventVisitors event = new EventVisitorsImpl();
            event.setUserId(addVisitor.getUserId());
            event.setEventvisitorId(addVisitor.getEventId());
            event.setCompanyId(this.contextCompany.getCompanyId());
            event.setGroupId(this.appGroupId);
            _eventvisitorService.addEventVisitors(event);
        } else {
            throw new BadRequestException(CommonUtil.getLanguageTranslation(contextHttpServletRequest, "user-already-subscribed-to-this-event"));
        }
        return addVisitor;

    }

    @Override
    public AddVisitor getAttendeesList(Long eventId, String languageId)
            throws Exception {

        return EventComponent.getAttendeesList(eventId, languageId, this.contextUriInfo);
    }

    @Override
    public EventList getEventsDetails(Long eventid, String languageId)
            throws Exception {
        try {

            Event event = EventLocalServiceUtil.getEvent(eventid);

            EventList result = EventComponent.getDTO(languageId, event, this.contextUriInfo);
            // get event schedule details
            result.setSchedules(EventComponent.getSchedules(eventid, languageId, this.contextUriInfo));
            // get related events by category
            EventList[] relatedEventsList = EventComponent.getRelatedEvents(languageId, event, this.contextHttpServletRequest, this.contextUriInfo);
            result.setEventLists(relatedEventsList);
            // get categories list
            CategoryList[] catList = EventComponent.getEventCategories(languageId);
            result.setCategoryLists(catList);
            // get event attendees list
            result.setAttendesList(EventComponent.getAttendeesList(event.getEventId(), languageId, this.contextUriInfo));
            // update event favourite status
            EventComponent.updateEventFavouriteStatus(result, this.contextHttpServletRequest);
            return result;
        } catch (Exception ex) {
            _log.error("Error retrieving Event Listing page data: " + ex.getMessage(), ex);

            _log.error(ex.getMessage());
        }
        return null;
    }

    public List<EventList> getEventList(String languageId, Pagination pagination, HashMap<String, Object> filterObj) throws Exception {

        List<Event> records = null;

        Boolean isFilterSet = isFilterSet(filterObj);

        if (isFilterSet) {
            records = _eventLocalService.findByFilter(
                    Integer.parseInt(filterObj.get("payType").toString()),
                    filterObj.get("priceFrom").toString(),
                    filterObj.get("priceTo").toString(),
                    Long.parseLong(filterObj.get("categoryId").toString()),
                    filterObj.get("dateFrom").toString(),
                    filterObj.get("dateTo").toString(),
                    pagination.getStartPosition(),
                    pagination.getEndPosition());
        } else {
            records = _eventService.getEvents(this.webGroupId, pagination.getStartPosition(), pagination.getEndPosition());
        }

        List<EventList> results = new ArrayList<>();

        for (events.model.Event curRecord : records) {
            EventList result = EventComponent.getDTO(languageId, curRecord, this.contextUriInfo);
            // get event attendees list
            result.setAttendesList(EventComponent.getAttendeesList(curRecord.getEventId(), languageId, this.contextUriInfo));
            results.add(result);
        }

        // update events favourite status
        EventComponent.updateEventsFavouriteStatus(results, contextHttpServletRequest);

        return results;
    }

    protected Boolean isFilterSet(HashMap<String, Object> filterObj) {
        return Integer.parseInt(filterObj.get("payType").toString()) > 0 ||
                (Validator.isNotNull(filterObj.get("priceFrom")) && Validator.isNotNull(filterObj.get("priceTo"))) ||
                Long.parseLong(filterObj.get("categoryId").toString()) > 0 ||
                (Validator.isNotNull(filterObj.get("dateFrom")) && Validator.isNotNull(filterObj.get("dateTo")));
    }

    @Override
    public Page<CategoryList> getEventCategories(String languageId)
            throws Exception {
        try{
            List<Category> categories = CategoryLocalServiceUtil.getCategories(this.webGroupId);
            List<CategoryList> categoryLists = new ArrayList<>();

            for(Category curCategory : categories){
                CategoryList categoryList = new CategoryList();
                categoryList.setCatId(String.valueOf(curCategory.getCategoryId()));
                categoryList.setCatTitle(curCategory.getName(languageId));
                categoryLists.add(categoryList);
            }
            return Page.of(categoryLists);
        } catch (Exception ex) {
            _log.error("Error retrieving Event categories data: " + ex.getMessage(), ex);
            _log.error(ex.getMessage());
        }
        return null;
    }

    @Reference
    private EventService _eventService;

    @Reference
    private EventLocalService _eventLocalService;

    @Reference
    private EventVisitorsLocalService _eventvisitorService;
}
