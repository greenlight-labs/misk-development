package misk.headless.social.login;

import java.util.List;

public class AppleKeysResponse {

    private List<AppleKeyDTO> keys;

    public List<AppleKeyDTO> getKeys() {
        return keys;
    }

    public void setKeys(List<AppleKeyDTO> keys) {
        this.keys = keys;
    }

}