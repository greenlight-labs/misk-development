package misk.headless.constants;

import javax.ws.rs.core.UriInfo;
import java.net.InetAddress;
import java.net.URI;
import java.util.Arrays;

/**
 * @author tz
 */
public class ApiKeys {

	public static final String X_LANGUAGE_ID = "X-LANGUAGE-ID";
	public static long webGroupId = 20124;
	public static long appGroupId = 91702;

	public static String getBaseURL(UriInfo contextUriInfo){
		URI baseUri = contextUriInfo.getBaseUri();
		String scheme = contextUriInfo.getBaseUri().getScheme();
		String authority = baseUri.getAuthority();
		int port = baseUri.getPort();
		String host = baseUri.getHost();
		return scheme+"://"+authority;
	}

}