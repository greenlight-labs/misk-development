package misk.headless.social.login;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import misk.headless.social.login.JwtUtils.JwtClaims;


public class VerifyAppleToken {


public static String verify(String jwtAppleToken) throws NoSuchAlgorithmException, InvalidKeySpecException {
	
        AppleKeysRetrieverService retriver = new AppleKeysRetrieverService();       
        AppleKeysResponse res = retriver.sendRetriveRequest("https://appleid.apple.com/auth/keys");
        List<AppleKeyDTO> appleKeys = res.getKeys();
        return  getEmailAddressFromToken(jwtAppleToken); 
    }

	/*
	 * public static boolean verify(String jwtToken, String publicKey) { try {
	 * 
	 * JwtHelper.decodeAndVerify(jwtToken, new
	 * RsaVerifier(getRSAPublicKey(publicKey))); } catch (Exception e) { return
	 * false; }
	 * 
	 * return true; }
	 */


     private static RSAPublicKey getRSAPublicKey(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
            KeyFactory keyFactory = java.security.KeyFactory.getInstance("RSA");
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(java.util.Base64.getDecoder().decode(publicKey));
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
     }


    private static String getPemPublicKeyFromBase64UrlEncodedXMLRSAKey(String urlBase64Modulus, String urlBase64Exp) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] e = Base64.getUrlDecoder().decode(urlBase64Exp);
        byte[] n = Base64.getUrlDecoder().decode(urlBase64Modulus);

        BigInteger exponent = new BigInteger(1, e);
        BigInteger modulus = new BigInteger(1, n);

        return getPemPublicKey(modulus, exponent);
    }

    private static String getPemPublicKey(BigInteger modulus, BigInteger exponent) throws NoSuchAlgorithmException, InvalidKeySpecException {
        RSAPublicKeySpec publicKeySpec = new java.security.spec.RSAPublicKeySpec(modulus, exponent);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey myPublicKey = keyFactory.generatePublic(publicKeySpec);

        byte[] park = Base64.getEncoder().encode(myPublicKey.getEncoded());


        return new String(park);
    }
   
    
	/*
	 * private static String getEmailAddressFromToken(String payloadBody) { String
	 * emailAddress=StringUtils.EMPTY; try { JSONObject jsonObject =
	 * JSONFactoryUtil.createJSONObject(payloadBody);
	 * emailAddress=(jsonObject.get("email") != null ? jsonObject.get("email") :
	 * StringUtils.EMPTY).toString(); } catch (Exception e) { throw new
	 * IllegalStateException("Cannot read token claims", e); } return emailAddress;
	 * }
	 */
    
    private static String getEmailAddressFromToken(String payloadBody) {
    	String emailAddress=StringUtils.EMPTY;
        try {
            JwtClaims jwt=JwtUtils.getClaims(payloadBody);
        	emailAddress=(jwt != null ? jwt.getEmail() : StringUtils.EMPTY).toString(); 
        	System.out.println("---------emailAddress-----------------------"+emailAddress);
        } catch (Exception e) {
          throw new IllegalStateException("Cannot read token claims", e);
        }
        return emailAddress;
      }
    

}