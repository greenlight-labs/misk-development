package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.AddVisitor;
import misk.headless.client.dto.v1_0.EventList;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.pagination.Pagination;
import misk.headless.client.resource.v1_0.EventListResource;
import misk.headless.client.serdes.v1_0.EventListSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseEventListResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_eventListResource.setContextCompany(testCompany);

		EventListResource.Builder builder = EventListResource.builder();

		eventListResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		EventList eventList1 = randomEventList();

		String json = objectMapper.writeValueAsString(eventList1);

		EventList eventList2 = EventListSerDes.toDTO(json);

		Assert.assertTrue(equals(eventList1, eventList2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		EventList eventList = randomEventList();

		String json1 = objectMapper.writeValueAsString(eventList);
		String json2 = EventListSerDes.toJSON(eventList);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		EventList eventList = randomEventList();

		eventList.setBuyTicketLink(regex);
		eventList.setCatColor(regex);
		eventList.setCurrency(regex);
		eventList.setDisplaydate(regex);
		eventList.setEmail(regex);
		eventList.setEventDescription(regex);
		eventList.setEventEndDateTime(regex);
		eventList.setEventFeaturedImage(regex);
		eventList.setEventLocation(regex);
		eventList.setEventMonth(regex);
		eventList.setEventPrice(regex);
		eventList.setEventStartDateTime(regex);
		eventList.setEventType(regex);
		eventList.setEventcategory(regex);
		eventList.setLan(regex);
		eventList.setLat(regex);
		eventList.setLink(regex);
		eventList.setLocationLabel(regex);
		eventList.setPayType(regex);
		eventList.setPhone(regex);
		eventList.setTagSlug(regex);
		eventList.setTagTitle(regex);
		eventList.setTime(regex);
		eventList.setTitle(regex);
		eventList.setVenue(regex);

		String json = EventListSerDes.toJSON(eventList);

		Assert.assertFalse(json.contains(regex));

		eventList = EventListSerDes.toDTO(json);

		Assert.assertEquals(regex, eventList.getBuyTicketLink());
		Assert.assertEquals(regex, eventList.getCatColor());
		Assert.assertEquals(regex, eventList.getCurrency());
		Assert.assertEquals(regex, eventList.getDisplaydate());
		Assert.assertEquals(regex, eventList.getEmail());
		Assert.assertEquals(regex, eventList.getEventDescription());
		Assert.assertEquals(regex, eventList.getEventEndDateTime());
		Assert.assertEquals(regex, eventList.getEventFeaturedImage());
		Assert.assertEquals(regex, eventList.getEventLocation());
		Assert.assertEquals(regex, eventList.getEventMonth());
		Assert.assertEquals(regex, eventList.getEventPrice());
		Assert.assertEquals(regex, eventList.getEventStartDateTime());
		Assert.assertEquals(regex, eventList.getEventType());
		Assert.assertEquals(regex, eventList.getEventcategory());
		Assert.assertEquals(regex, eventList.getLan());
		Assert.assertEquals(regex, eventList.getLat());
		Assert.assertEquals(regex, eventList.getLink());
		Assert.assertEquals(regex, eventList.getLocationLabel());
		Assert.assertEquals(regex, eventList.getPayType());
		Assert.assertEquals(regex, eventList.getPhone());
		Assert.assertEquals(regex, eventList.getTagSlug());
		Assert.assertEquals(regex, eventList.getTagTitle());
		Assert.assertEquals(regex, eventList.getTime());
		Assert.assertEquals(regex, eventList.getTitle());
		Assert.assertEquals(regex, eventList.getVenue());
	}

	@Test
	public void testGetEventsListing() throws Exception {
		Page<EventList> page = eventListResource.getEventsListing(
			RandomTestUtil.randomString(), RandomTestUtil.randomString(), null,
			RandomTestUtil.randomString(), RandomTestUtil.randomString(), null,
			RandomTestUtil.randomString(), RandomTestUtil.randomString(),
			Pagination.of(1, 10));

		long totalCount = page.getTotalCount();

		EventList eventList1 = testGetEventsListing_addEventList(
			randomEventList());

		EventList eventList2 = testGetEventsListing_addEventList(
			randomEventList());

		page = eventListResource.getEventsListing(
			null, null, null, null, null, null, null, null,
			Pagination.of(1, 10));

		Assert.assertEquals(totalCount + 2, page.getTotalCount());

		assertContains(eventList1, (List<EventList>)page.getItems());
		assertContains(eventList2, (List<EventList>)page.getItems());
		assertValid(page);
	}

	@Test
	public void testGetEventsListingWithPagination() throws Exception {
		Page<EventList> totalPage = eventListResource.getEventsListing(
			null, null, null, null, null, null, null, null, null);

		int totalCount = GetterUtil.getInteger(totalPage.getTotalCount());

		EventList eventList1 = testGetEventsListing_addEventList(
			randomEventList());

		EventList eventList2 = testGetEventsListing_addEventList(
			randomEventList());

		EventList eventList3 = testGetEventsListing_addEventList(
			randomEventList());

		Page<EventList> page1 = eventListResource.getEventsListing(
			null, null, null, null, null, null, null, null,
			Pagination.of(1, totalCount + 2));

		List<EventList> eventLists1 = (List<EventList>)page1.getItems();

		Assert.assertEquals(
			eventLists1.toString(), totalCount + 2, eventLists1.size());

		Page<EventList> page2 = eventListResource.getEventsListing(
			null, null, null, null, null, null, null, null,
			Pagination.of(2, totalCount + 2));

		Assert.assertEquals(totalCount + 3, page2.getTotalCount());

		List<EventList> eventLists2 = (List<EventList>)page2.getItems();

		Assert.assertEquals(eventLists2.toString(), 1, eventLists2.size());

		Page<EventList> page3 = eventListResource.getEventsListing(
			null, null, null, null, null, null, null, null,
			Pagination.of(1, totalCount + 3));

		assertContains(eventList1, (List<EventList>)page3.getItems());
		assertContains(eventList2, (List<EventList>)page3.getItems());
		assertContains(eventList3, (List<EventList>)page3.getItems());
	}

	protected EventList testGetEventsListing_addEventList(EventList eventList)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGetEventsDetails() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetEventsDetails() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetEventsDetailsNotFound() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGetEventCategories() throws Exception {
		Page<EventList> page = eventListResource.getEventCategories(
			RandomTestUtil.randomString());

		long totalCount = page.getTotalCount();

		EventList eventList1 = testGetEventCategories_addEventList(
			randomEventList());

		EventList eventList2 = testGetEventCategories_addEventList(
			randomEventList());

		page = eventListResource.getEventCategories(null);

		Assert.assertEquals(totalCount + 2, page.getTotalCount());

		assertContains(eventList1, (List<EventList>)page.getItems());
		assertContains(eventList2, (List<EventList>)page.getItems());
		assertValid(page);
	}

	protected EventList testGetEventCategories_addEventList(EventList eventList)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGetAttendeesList() throws Exception {
		EventList postEventList = testGetEventList_addEventList();

		AddVisitor postAddVisitor = testGetAttendeesList_addAddVisitor(
			postEventList.getId(), randomAddVisitor());

		AddVisitor getAddVisitor = eventListResource.getAttendeesList(
			postEventList.getId());

		assertEquals(postAddVisitor, getAddVisitor);
		assertValid(getAddVisitor);
	}

	protected AddVisitor testGetAttendeesList_addAddVisitor(
			long eventListId, AddVisitor addVisitor)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testAddEventsVisitor() throws Exception {
		Assert.assertTrue(true);
	}

	protected void assertContains(
		EventList eventList, List<EventList> eventLists) {

		boolean contains = false;

		for (EventList item : eventLists) {
			if (equals(eventList, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			eventLists + " does not contain " + eventList, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(EventList eventList1, EventList eventList2) {
		Assert.assertTrue(
			eventList1 + " does not equal " + eventList2,
			equals(eventList1, eventList2));
	}

	protected void assertEquals(
		List<EventList> eventLists1, List<EventList> eventLists2) {

		Assert.assertEquals(eventLists1.size(), eventLists2.size());

		for (int i = 0; i < eventLists1.size(); i++) {
			EventList eventList1 = eventLists1.get(i);
			EventList eventList2 = eventLists2.get(i);

			assertEquals(eventList1, eventList2);
		}
	}

	protected void assertEquals(
		AddVisitor addVisitor1, AddVisitor addVisitor2) {

		Assert.assertTrue(
			addVisitor1 + " does not equal " + addVisitor2,
			equals(addVisitor1, addVisitor2));
	}

	protected void assertEqualsIgnoringOrder(
		List<EventList> eventLists1, List<EventList> eventLists2) {

		Assert.assertEquals(eventLists1.size(), eventLists2.size());

		for (EventList eventList1 : eventLists1) {
			boolean contains = false;

			for (EventList eventList2 : eventLists2) {
				if (equals(eventList1, eventList2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				eventLists2 + " does not contain " + eventList1, contains);
		}
	}

	protected void assertValid(EventList eventList) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("attendesList", additionalAssertFieldName)) {
				if (eventList.getAttendesList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("buyTicketLink", additionalAssertFieldName)) {
				if (eventList.getBuyTicketLink() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("catColor", additionalAssertFieldName)) {
				if (eventList.getCatColor() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("categoryLists", additionalAssertFieldName)) {
				if (eventList.getCategoryLists() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("currency", additionalAssertFieldName)) {
				if (eventList.getCurrency() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("displaydate", additionalAssertFieldName)) {
				if (eventList.getDisplaydate() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("email", additionalAssertFieldName)) {
				if (eventList.getEmail() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventDescription", additionalAssertFieldName)) {
				if (eventList.getEventDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventEndDateTime", additionalAssertFieldName)) {
				if (eventList.getEventEndDateTime() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"eventFeaturedImage", additionalAssertFieldName)) {

				if (eventList.getEventFeaturedImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventId", additionalAssertFieldName)) {
				if (eventList.getEventId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventLists", additionalAssertFieldName)) {
				if (eventList.getEventLists() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventLocation", additionalAssertFieldName)) {
				if (eventList.getEventLocation() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventMonth", additionalAssertFieldName)) {
				if (eventList.getEventMonth() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventPrice", additionalAssertFieldName)) {
				if (eventList.getEventPrice() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"eventStartDateTime", additionalAssertFieldName)) {

				if (eventList.getEventStartDateTime() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventType", additionalAssertFieldName)) {
				if (eventList.getEventType() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventcategory", additionalAssertFieldName)) {
				if (eventList.getEventcategory() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("isFavourite", additionalAssertFieldName)) {
				if (eventList.getIsFavourite() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("lan", additionalAssertFieldName)) {
				if (eventList.getLan() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("lat", additionalAssertFieldName)) {
				if (eventList.getLat() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("link", additionalAssertFieldName)) {
				if (eventList.getLink() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("locationLabel", additionalAssertFieldName)) {
				if (eventList.getLocationLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("payType", additionalAssertFieldName)) {
				if (eventList.getPayType() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("phone", additionalAssertFieldName)) {
				if (eventList.getPhone() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("schedules", additionalAssertFieldName)) {
				if (eventList.getSchedules() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("sliderimage", additionalAssertFieldName)) {
				if (eventList.getSliderimage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("tagSlug", additionalAssertFieldName)) {
				if (eventList.getTagSlug() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("tagTitle", additionalAssertFieldName)) {
				if (eventList.getTagTitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("time", additionalAssertFieldName)) {
				if (eventList.getTime() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (eventList.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("venue", additionalAssertFieldName)) {
				if (eventList.getVenue() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<EventList> page) {
		boolean valid = false;

		java.util.Collection<EventList> eventLists = page.getItems();

		int size = eventLists.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(AddVisitor addVisitor) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAddVisitorAssertFieldNames()) {

			if (Objects.equals("appUsers", additionalAssertFieldName)) {
				if (addVisitor.getAppUsers() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("count", additionalAssertFieldName)) {
				if (addVisitor.getCount() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventId", additionalAssertFieldName)) {
				if (addVisitor.getEventId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("userId", additionalAssertFieldName)) {
				if (addVisitor.getUserId() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalAddVisitorAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.EventList.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(EventList eventList1, EventList eventList2) {
		if (eventList1 == eventList2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("attendesList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getAttendesList(),
						eventList2.getAttendesList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("buyTicketLink", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getBuyTicketLink(),
						eventList2.getBuyTicketLink())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("catColor", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getCatColor(), eventList2.getCatColor())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("categoryLists", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getCategoryLists(),
						eventList2.getCategoryLists())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("currency", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getCurrency(), eventList2.getCurrency())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("displaydate", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getDisplaydate(),
						eventList2.getDisplaydate())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("email", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEmail(), eventList2.getEmail())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventDescription", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventDescription(),
						eventList2.getEventDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventEndDateTime", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventEndDateTime(),
						eventList2.getEventEndDateTime())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"eventFeaturedImage", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						eventList1.getEventFeaturedImage(),
						eventList2.getEventFeaturedImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventId(), eventList2.getEventId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventLists", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventLists(),
						eventList2.getEventLists())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventLocation", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventLocation(),
						eventList2.getEventLocation())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventMonth", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventMonth(),
						eventList2.getEventMonth())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventPrice", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventPrice(),
						eventList2.getEventPrice())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"eventStartDateTime", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						eventList1.getEventStartDateTime(),
						eventList2.getEventStartDateTime())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventType", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventType(), eventList2.getEventType())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventcategory", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getEventcategory(),
						eventList2.getEventcategory())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("isFavourite", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getIsFavourite(),
						eventList2.getIsFavourite())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("lan", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getLan(), eventList2.getLan())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("lat", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getLat(), eventList2.getLat())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("link", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getLink(), eventList2.getLink())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("locationLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getLocationLabel(),
						eventList2.getLocationLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("payType", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getPayType(), eventList2.getPayType())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("phone", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getPhone(), eventList2.getPhone())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("schedules", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getSchedules(), eventList2.getSchedules())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("sliderimage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getSliderimage(),
						eventList2.getSliderimage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("tagSlug", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getTagSlug(), eventList2.getTagSlug())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("tagTitle", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getTagTitle(), eventList2.getTagTitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("time", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getTime(), eventList2.getTime())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getTitle(), eventList2.getTitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("venue", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						eventList1.getVenue(), eventList2.getVenue())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected boolean equals(AddVisitor addVisitor1, AddVisitor addVisitor2) {
		if (addVisitor1 == addVisitor2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAddVisitorAssertFieldNames()) {

			if (Objects.equals("appUsers", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						addVisitor1.getAppUsers(), addVisitor2.getAppUsers())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("count", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						addVisitor1.getCount(), addVisitor2.getCount())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						addVisitor1.getEventId(), addVisitor2.getEventId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("userId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						addVisitor1.getUserId(), addVisitor2.getUserId())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_eventListResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_eventListResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, EventList eventList) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("attendesList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("buyTicketLink")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getBuyTicketLink()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("catColor")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getCatColor()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("categoryLists")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("currency")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getCurrency()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("displaydate")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getDisplaydate()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("email")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEmail()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventDescription")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventDescription()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventEndDateTime")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventEndDateTime()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventFeaturedImage")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventFeaturedImage()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("eventLists")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("eventLocation")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventLocation()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventMonth")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventMonth()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventPrice")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventPrice()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventStartDateTime")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventStartDateTime()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventType")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventType()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventcategory")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getEventcategory()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("isFavourite")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("lan")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getLan()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("lat")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getLat()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("link")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getLink()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("locationLabel")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getLocationLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("payType")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getPayType()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("phone")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getPhone()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("schedules")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("sliderimage")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("tagSlug")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getTagSlug()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("tagTitle")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getTagTitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("time")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getTime()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("title")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getTitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("venue")) {
			sb.append("'");
			sb.append(String.valueOf(eventList.getVenue()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected EventList randomEventList() throws Exception {
		return new EventList() {
			{
				buyTicketLink = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				catColor = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				currency = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				displaydate = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				email =
					StringUtil.toLowerCase(RandomTestUtil.randomString()) +
						"@liferay.com";
				eventDescription = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventEndDateTime = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventFeaturedImage = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventId = RandomTestUtil.randomLong();
				eventLocation = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventMonth = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventPrice = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventStartDateTime = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventType = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				eventcategory = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				isFavourite = RandomTestUtil.randomBoolean();
				lan = StringUtil.toLowerCase(RandomTestUtil.randomString());
				lat = StringUtil.toLowerCase(RandomTestUtil.randomString());
				link = StringUtil.toLowerCase(RandomTestUtil.randomString());
				locationLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				payType = StringUtil.toLowerCase(RandomTestUtil.randomString());
				phone = StringUtil.toLowerCase(RandomTestUtil.randomString());
				tagSlug = StringUtil.toLowerCase(RandomTestUtil.randomString());
				tagTitle = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				time = StringUtil.toLowerCase(RandomTestUtil.randomString());
				title = StringUtil.toLowerCase(RandomTestUtil.randomString());
				venue = StringUtil.toLowerCase(RandomTestUtil.randomString());
			}
		};
	}

	protected EventList randomIrrelevantEventList() throws Exception {
		EventList randomIrrelevantEventList = randomEventList();

		return randomIrrelevantEventList;
	}

	protected EventList randomPatchEventList() throws Exception {
		return randomEventList();
	}

	protected AddVisitor randomAddVisitor() throws Exception {
		return new AddVisitor() {
			{
				count = RandomTestUtil.randomLong();
				eventId = RandomTestUtil.randomLong();
				userId = RandomTestUtil.randomLong();
			}
		};
	}

	protected EventListResource eventListResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseEventListResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.EventListResource _eventListResource;

}