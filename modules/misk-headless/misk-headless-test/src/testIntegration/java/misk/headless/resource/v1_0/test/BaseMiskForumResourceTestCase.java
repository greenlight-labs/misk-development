package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.MiskForum;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.MiskForumResource;
import misk.headless.client.serdes.v1_0.MiskForumSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseMiskForumResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_miskForumResource.setContextCompany(testCompany);

		MiskForumResource.Builder builder = MiskForumResource.builder();

		miskForumResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		MiskForum miskForum1 = randomMiskForum();

		String json = objectMapper.writeValueAsString(miskForum1);

		MiskForum miskForum2 = MiskForumSerDes.toDTO(json);

		Assert.assertTrue(equals(miskForum1, miskForum2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		MiskForum miskForum = randomMiskForum();

		String json1 = objectMapper.writeValueAsString(miskForum);
		String json2 = MiskForumSerDes.toJSON(miskForum);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		MiskForum miskForum = randomMiskForum();

		miskForum.setAmenitiesLabel(regex);
		miskForum.setBookSpaceLabel(regex);
		miskForum.setContactEmailAddress(regex);
		miskForum.setContactLabel(regex);
		miskForum.setContactTelephone(regex);
		miskForum.setDescription(regex);
		miskForum.setEventLabel(regex);
		miskForum.setEventName(regex);
		miskForum.setGalleryLabel(regex);
		miskForum.setLocationLabel(regex);
		miskForum.setLocationLatitude(regex);
		miskForum.setLocationLongitude(regex);
		miskForum.setMiskForumBanner(regex);
		miskForum.setMiskForumLabel(regex);
		miskForum.setWorkingDay(regex);
		miskForum.setWorkingHours(regex);
		miskForum.setWorkingHoursLabel(regex);

		String json = MiskForumSerDes.toJSON(miskForum);

		Assert.assertFalse(json.contains(regex));

		miskForum = MiskForumSerDes.toDTO(json);

		Assert.assertEquals(regex, miskForum.getAmenitiesLabel());
		Assert.assertEquals(regex, miskForum.getBookSpaceLabel());
		Assert.assertEquals(regex, miskForum.getContactEmailAddress());
		Assert.assertEquals(regex, miskForum.getContactLabel());
		Assert.assertEquals(regex, miskForum.getContactTelephone());
		Assert.assertEquals(regex, miskForum.getDescription());
		Assert.assertEquals(regex, miskForum.getEventLabel());
		Assert.assertEquals(regex, miskForum.getEventName());
		Assert.assertEquals(regex, miskForum.getGalleryLabel());
		Assert.assertEquals(regex, miskForum.getLocationLabel());
		Assert.assertEquals(regex, miskForum.getLocationLatitude());
		Assert.assertEquals(regex, miskForum.getLocationLongitude());
		Assert.assertEquals(regex, miskForum.getMiskForumBanner());
		Assert.assertEquals(regex, miskForum.getMiskForumLabel());
		Assert.assertEquals(regex, miskForum.getWorkingDay());
		Assert.assertEquals(regex, miskForum.getWorkingHours());
		Assert.assertEquals(regex, miskForum.getWorkingHoursLabel());
	}

	@Test
	public void testGetMiskForum() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetMiskForum() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetMiskForumNotFound() throws Exception {
		Assert.assertTrue(true);
	}

	protected void assertContains(
		MiskForum miskForum, List<MiskForum> miskForums) {

		boolean contains = false;

		for (MiskForum item : miskForums) {
			if (equals(miskForum, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			miskForums + " does not contain " + miskForum, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(MiskForum miskForum1, MiskForum miskForum2) {
		Assert.assertTrue(
			miskForum1 + " does not equal " + miskForum2,
			equals(miskForum1, miskForum2));
	}

	protected void assertEquals(
		List<MiskForum> miskForums1, List<MiskForum> miskForums2) {

		Assert.assertEquals(miskForums1.size(), miskForums2.size());

		for (int i = 0; i < miskForums1.size(); i++) {
			MiskForum miskForum1 = miskForums1.get(i);
			MiskForum miskForum2 = miskForums2.get(i);

			assertEquals(miskForum1, miskForum2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<MiskForum> miskForums1, List<MiskForum> miskForums2) {

		Assert.assertEquals(miskForums1.size(), miskForums2.size());

		for (MiskForum miskForum1 : miskForums1) {
			boolean contains = false;

			for (MiskForum miskForum2 : miskForums2) {
				if (equals(miskForum1, miskForum2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				miskForums2 + " does not contain " + miskForum1, contains);
		}
	}

	protected void assertValid(MiskForum miskForum) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("AmenitiesLabel", additionalAssertFieldName)) {
				if (miskForum.getAmenitiesLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("AmenitiesList", additionalAssertFieldName)) {
				if (miskForum.getAmenitiesList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("BookSpaceLabel", additionalAssertFieldName)) {
				if (miskForum.getBookSpaceLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("BookSpaceList", additionalAssertFieldName)) {
				if (miskForum.getBookSpaceList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"ContactEmailAddress", additionalAssertFieldName)) {

				if (miskForum.getContactEmailAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("ContactLabel", additionalAssertFieldName)) {
				if (miskForum.getContactLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("ContactTelephone", additionalAssertFieldName)) {
				if (miskForum.getContactTelephone() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("Description", additionalAssertFieldName)) {
				if (miskForum.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("EventLabel", additionalAssertFieldName)) {
				if (miskForum.getEventLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("EventName", additionalAssertFieldName)) {
				if (miskForum.getEventName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"GalleryImagesList", additionalAssertFieldName)) {

				if (miskForum.getGalleryImagesList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("GalleryLabel", additionalAssertFieldName)) {
				if (miskForum.getGalleryLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("LocationLabel", additionalAssertFieldName)) {
				if (miskForum.getLocationLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("LocationLatitude", additionalAssertFieldName)) {
				if (miskForum.getLocationLatitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"LocationLongitude", additionalAssertFieldName)) {

				if (miskForum.getLocationLongitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("MiskForumBanner", additionalAssertFieldName)) {
				if (miskForum.getMiskForumBanner() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("MiskForumLabel", additionalAssertFieldName)) {
				if (miskForum.getMiskForumLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("WorkingDay", additionalAssertFieldName)) {
				if (miskForum.getWorkingDay() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("WorkingHours", additionalAssertFieldName)) {
				if (miskForum.getWorkingHours() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"WorkingHoursLabel", additionalAssertFieldName)) {

				if (miskForum.getWorkingHoursLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (miskForum.getEventsSection() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<MiskForum> page) {
		boolean valid = false;

		java.util.Collection<MiskForum> miskForums = page.getItems();

		int size = miskForums.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.MiskForum.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(MiskForum miskForum1, MiskForum miskForum2) {
		if (miskForum1 == miskForum2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("AmenitiesLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getAmenitiesLabel(),
						miskForum2.getAmenitiesLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("AmenitiesList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getAmenitiesList(),
						miskForum2.getAmenitiesList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("BookSpaceLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getBookSpaceLabel(),
						miskForum2.getBookSpaceLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("BookSpaceList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getBookSpaceList(),
						miskForum2.getBookSpaceList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"ContactEmailAddress", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						miskForum1.getContactEmailAddress(),
						miskForum2.getContactEmailAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("ContactLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getContactLabel(),
						miskForum2.getContactLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("ContactTelephone", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getContactTelephone(),
						miskForum2.getContactTelephone())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("Description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getDescription(),
						miskForum2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("EventLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getEventLabel(),
						miskForum2.getEventLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("EventName", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getEventName(), miskForum2.getEventName())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"GalleryImagesList", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						miskForum1.getGalleryImagesList(),
						miskForum2.getGalleryImagesList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("GalleryLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getGalleryLabel(),
						miskForum2.getGalleryLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("LocationLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getLocationLabel(),
						miskForum2.getLocationLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("LocationLatitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getLocationLatitude(),
						miskForum2.getLocationLatitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"LocationLongitude", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						miskForum1.getLocationLongitude(),
						miskForum2.getLocationLongitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("MiskForumBanner", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getMiskForumBanner(),
						miskForum2.getMiskForumBanner())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("MiskForumLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getMiskForumLabel(),
						miskForum2.getMiskForumLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("WorkingDay", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getWorkingDay(),
						miskForum2.getWorkingDay())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("WorkingHours", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getWorkingHours(),
						miskForum2.getWorkingHours())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"WorkingHoursLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						miskForum1.getWorkingHoursLabel(),
						miskForum2.getWorkingHoursLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						miskForum1.getEventsSection(),
						miskForum2.getEventsSection())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_miskForumResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_miskForumResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, MiskForum miskForum) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("AmenitiesLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getAmenitiesLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("AmenitiesList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("BookSpaceLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getBookSpaceLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("BookSpaceList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("ContactEmailAddress")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getContactEmailAddress()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("ContactLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getContactLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("ContactTelephone")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getContactTelephone()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("Description")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getDescription()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("EventLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getEventLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("EventName")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getEventName()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("GalleryImagesList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("GalleryLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getGalleryLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("LocationLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getLocationLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("LocationLatitude")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getLocationLatitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("LocationLongitude")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getLocationLongitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("MiskForumBanner")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getMiskForumBanner()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("MiskForumLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getMiskForumLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WorkingDay")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getWorkingDay()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WorkingHours")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getWorkingHours()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WorkingHoursLabel")) {
			sb.append("'");
			sb.append(String.valueOf(miskForum.getWorkingHoursLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventsSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected MiskForum randomMiskForum() throws Exception {
		return new MiskForum() {
			{
				AmenitiesLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				BookSpaceLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				ContactEmailAddress = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				ContactLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				ContactTelephone = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				Description = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				EventLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				EventName = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				GalleryLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				LocationLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				LocationLatitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				LocationLongitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				MiskForumBanner = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				MiskForumLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WorkingDay = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WorkingHours = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WorkingHoursLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected MiskForum randomIrrelevantMiskForum() throws Exception {
		MiskForum randomIrrelevantMiskForum = randomMiskForum();

		return randomIrrelevantMiskForum;
	}

	protected MiskForum randomPatchMiskForum() throws Exception {
		return randomMiskForum();
	}

	protected MiskForumResource miskForumResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseMiskForumResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.MiskForumResource _miskForumResource;

}