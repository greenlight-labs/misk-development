package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.Explore;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.ExploreResource;
import misk.headless.client.serdes.v1_0.ExploreSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseExploreResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_exploreResource.setContextCompany(testCompany);

		ExploreResource.Builder builder = ExploreResource.builder();

		exploreResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Explore explore1 = randomExplore();

		String json = objectMapper.writeValueAsString(explore1);

		Explore explore2 = ExploreSerDes.toDTO(json);

		Assert.assertTrue(equals(explore1, explore2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Explore explore = randomExplore();

		String json1 = objectMapper.writeValueAsString(explore);
		String json2 = ExploreSerDes.toJSON(explore);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		Explore explore = randomExplore();

		explore.setExploreLabel(regex);
		explore.setExpolerId(regex);
		explore.setFaclitiesLabel(regex);
		explore.setGalleryLabel(regex);
		explore.setHeaderBanner(regex);
		explore.setHeaderDesc(regex);
		explore.setHeaderTitle(regex);
		explore.setLocationLabel(regex);
		explore.setLocationLatitude(regex);
		explore.setLocationLongitude(regex);

		String json = ExploreSerDes.toJSON(explore);

		Assert.assertFalse(json.contains(regex));

		explore = ExploreSerDes.toDTO(json);

		Assert.assertEquals(regex, explore.getExploreLabel());
		Assert.assertEquals(regex, explore.getExpolerId());
		Assert.assertEquals(regex, explore.getFaclitiesLabel());
		Assert.assertEquals(regex, explore.getGalleryLabel());
		Assert.assertEquals(regex, explore.getHeaderBanner());
		Assert.assertEquals(regex, explore.getHeaderDesc());
		Assert.assertEquals(regex, explore.getHeaderTitle());
		Assert.assertEquals(regex, explore.getLocationLabel());
		Assert.assertEquals(regex, explore.getLocationLatitude());
		Assert.assertEquals(regex, explore.getLocationLongitude());
	}

	@Test
	public void testGetExplore() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetExplore() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetExploreNotFound() throws Exception {
		Assert.assertTrue(true);
	}

	protected void assertContains(Explore explore, List<Explore> explores) {
		boolean contains = false;

		for (Explore item : explores) {
			if (equals(explore, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(explores + " does not contain " + explore, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(Explore explore1, Explore explore2) {
		Assert.assertTrue(
			explore1 + " does not equal " + explore2,
			equals(explore1, explore2));
	}

	protected void assertEquals(
		List<Explore> explores1, List<Explore> explores2) {

		Assert.assertEquals(explores1.size(), explores2.size());

		for (int i = 0; i < explores1.size(); i++) {
			Explore explore1 = explores1.get(i);
			Explore explore2 = explores2.get(i);

			assertEquals(explore1, explore2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<Explore> explores1, List<Explore> explores2) {

		Assert.assertEquals(explores1.size(), explores2.size());

		for (Explore explore1 : explores1) {
			boolean contains = false;

			for (Explore explore2 : explores2) {
				if (equals(explore1, explore2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				explores2 + " does not contain " + explore1, contains);
		}
	}

	protected void assertValid(Explore explore) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (explore.getEventsSection() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("exploreLabel", additionalAssertFieldName)) {
				if (explore.getExploreLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("exploreList", additionalAssertFieldName)) {
				if (explore.getExploreList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("expolerId", additionalAssertFieldName)) {
				if (explore.getExpolerId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("faclitiesLabel", additionalAssertFieldName)) {
				if (explore.getFaclitiesLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("faclitiesList", additionalAssertFieldName)) {
				if (explore.getFaclitiesList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"galleryImagesList", additionalAssertFieldName)) {

				if (explore.getGalleryImagesList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("galleryLabel", additionalAssertFieldName)) {
				if (explore.getGalleryLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("headerBanner", additionalAssertFieldName)) {
				if (explore.getHeaderBanner() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("headerDesc", additionalAssertFieldName)) {
				if (explore.getHeaderDesc() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("headerTitle", additionalAssertFieldName)) {
				if (explore.getHeaderTitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("locationLabel", additionalAssertFieldName)) {
				if (explore.getLocationLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("locationLatitude", additionalAssertFieldName)) {
				if (explore.getLocationLatitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"locationLongitude", additionalAssertFieldName)) {

				if (explore.getLocationLongitude() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Explore> page) {
		boolean valid = false;

		java.util.Collection<Explore> explores = page.getItems();

		int size = explores.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.Explore.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(Explore explore1, Explore explore2) {
		if (explore1 == explore2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getEventsSection(),
						explore2.getEventsSection())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("exploreLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getExploreLabel(),
						explore2.getExploreLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("exploreList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getExploreList(), explore2.getExploreList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("expolerId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getExpolerId(), explore2.getExpolerId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("faclitiesLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getFaclitiesLabel(),
						explore2.getFaclitiesLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("faclitiesList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getFaclitiesList(),
						explore2.getFaclitiesList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"galleryImagesList", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						explore1.getGalleryImagesList(),
						explore2.getGalleryImagesList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("galleryLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getGalleryLabel(),
						explore2.getGalleryLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("headerBanner", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getHeaderBanner(),
						explore2.getHeaderBanner())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("headerDesc", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getHeaderDesc(), explore2.getHeaderDesc())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("headerTitle", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getHeaderTitle(), explore2.getHeaderTitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("locationLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getLocationLabel(),
						explore2.getLocationLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("locationLatitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						explore1.getLocationLatitude(),
						explore2.getLocationLatitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"locationLongitude", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						explore1.getLocationLongitude(),
						explore2.getLocationLongitude())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_exploreResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_exploreResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, Explore explore) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("eventsSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("exploreLabel")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getExploreLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("exploreList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("expolerId")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getExpolerId()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("faclitiesLabel")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getFaclitiesLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("faclitiesList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("galleryImagesList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("galleryLabel")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getGalleryLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("headerBanner")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getHeaderBanner()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("headerDesc")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getHeaderDesc()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("headerTitle")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getHeaderTitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("locationLabel")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getLocationLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("locationLatitude")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getLocationLatitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("locationLongitude")) {
			sb.append("'");
			sb.append(String.valueOf(explore.getLocationLongitude()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected Explore randomExplore() throws Exception {
		return new Explore() {
			{
				exploreLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				expolerId = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				faclitiesLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				galleryLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				headerBanner = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				headerDesc = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				headerTitle = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				locationLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				locationLatitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				locationLongitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected Explore randomIrrelevantExplore() throws Exception {
		Explore randomIrrelevantExplore = randomExplore();

		return randomIrrelevantExplore;
	}

	protected Explore randomPatchExplore() throws Exception {
		return randomExplore();
	}

	protected ExploreResource exploreResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseExploreResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.ExploreResource _exploreResource;

}