package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.ExploreWadiC;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.ExploreWadiCResource;
import misk.headless.client.serdes.v1_0.ExploreWadiCSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseExploreWadiCResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_exploreWadiCResource.setContextCompany(testCompany);

		ExploreWadiCResource.Builder builder = ExploreWadiCResource.builder();

		exploreWadiCResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		ExploreWadiC exploreWadiC1 = randomExploreWadiC();

		String json = objectMapper.writeValueAsString(exploreWadiC1);

		ExploreWadiC exploreWadiC2 = ExploreWadiCSerDes.toDTO(json);

		Assert.assertTrue(equals(exploreWadiC1, exploreWadiC2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		ExploreWadiC exploreWadiC = randomExploreWadiC();

		String json1 = objectMapper.writeValueAsString(exploreWadiC);
		String json2 = ExploreWadiCSerDes.toJSON(exploreWadiC);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		ExploreWadiC exploreWadiC = randomExploreWadiC();

		exploreWadiC.setDescription(regex);
		exploreWadiC.setEstimatedTourTime(regex);
		exploreWadiC.setEstimatedTourTimeLabel(regex);
		exploreWadiC.setImage(regex);
		exploreWadiC.setName(regex);
		exploreWadiC.setType(regex);
		exploreWadiC.setVideo(regex);

		String json = ExploreWadiCSerDes.toJSON(exploreWadiC);

		Assert.assertFalse(json.contains(regex));

		exploreWadiC = ExploreWadiCSerDes.toDTO(json);

		Assert.assertEquals(regex, exploreWadiC.getDescription());
		Assert.assertEquals(regex, exploreWadiC.getEstimatedTourTime());
		Assert.assertEquals(regex, exploreWadiC.getEstimatedTourTimeLabel());
		Assert.assertEquals(regex, exploreWadiC.getImage());
		Assert.assertEquals(regex, exploreWadiC.getName());
		Assert.assertEquals(regex, exploreWadiC.getType());
		Assert.assertEquals(regex, exploreWadiC.getVideo());
	}

	@Test
	public void testGetExploreWadiC() throws Exception {
		Page<ExploreWadiC> page = exploreWadiCResource.getExploreWadiC(
			RandomTestUtil.randomString());

		long totalCount = page.getTotalCount();

		ExploreWadiC exploreWadiC1 = testGetExploreWadiC_addExploreWadiC(
			randomExploreWadiC());

		ExploreWadiC exploreWadiC2 = testGetExploreWadiC_addExploreWadiC(
			randomExploreWadiC());

		page = exploreWadiCResource.getExploreWadiC(null);

		Assert.assertEquals(totalCount + 2, page.getTotalCount());

		assertContains(exploreWadiC1, (List<ExploreWadiC>)page.getItems());
		assertContains(exploreWadiC2, (List<ExploreWadiC>)page.getItems());
		assertValid(page);
	}

	protected ExploreWadiC testGetExploreWadiC_addExploreWadiC(
			ExploreWadiC exploreWadiC)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected ExploreWadiC testGraphQLExploreWadiC_addExploreWadiC()
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertContains(
		ExploreWadiC exploreWadiC, List<ExploreWadiC> exploreWadiCs) {

		boolean contains = false;

		for (ExploreWadiC item : exploreWadiCs) {
			if (equals(exploreWadiC, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			exploreWadiCs + " does not contain " + exploreWadiC, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(
		ExploreWadiC exploreWadiC1, ExploreWadiC exploreWadiC2) {

		Assert.assertTrue(
			exploreWadiC1 + " does not equal " + exploreWadiC2,
			equals(exploreWadiC1, exploreWadiC2));
	}

	protected void assertEquals(
		List<ExploreWadiC> exploreWadiCs1, List<ExploreWadiC> exploreWadiCs2) {

		Assert.assertEquals(exploreWadiCs1.size(), exploreWadiCs2.size());

		for (int i = 0; i < exploreWadiCs1.size(); i++) {
			ExploreWadiC exploreWadiC1 = exploreWadiCs1.get(i);
			ExploreWadiC exploreWadiC2 = exploreWadiCs2.get(i);

			assertEquals(exploreWadiC1, exploreWadiC2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<ExploreWadiC> exploreWadiCs1, List<ExploreWadiC> exploreWadiCs2) {

		Assert.assertEquals(exploreWadiCs1.size(), exploreWadiCs2.size());

		for (ExploreWadiC exploreWadiC1 : exploreWadiCs1) {
			boolean contains = false;

			for (ExploreWadiC exploreWadiC2 : exploreWadiCs2) {
				if (equals(exploreWadiC1, exploreWadiC2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				exploreWadiCs2 + " does not contain " + exploreWadiC1,
				contains);
		}
	}

	protected void assertValid(ExploreWadiC exploreWadiC) throws Exception {
		boolean valid = true;

		if (exploreWadiC.getId() == null) {
			valid = false;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (exploreWadiC.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"estimatedTourTime", additionalAssertFieldName)) {

				if (exploreWadiC.getEstimatedTourTime() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"estimatedTourTimeLabel", additionalAssertFieldName)) {

				if (exploreWadiC.getEstimatedTourTimeLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("galleryImages", additionalAssertFieldName)) {
				if (exploreWadiC.getGalleryImages() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("image", additionalAssertFieldName)) {
				if (exploreWadiC.getImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("name", additionalAssertFieldName)) {
				if (exploreWadiC.getName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("type", additionalAssertFieldName)) {
				if (exploreWadiC.getType() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("video", additionalAssertFieldName)) {
				if (exploreWadiC.getVideo() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<ExploreWadiC> page) {
		boolean valid = false;

		java.util.Collection<ExploreWadiC> exploreWadiCs = page.getItems();

		int size = exploreWadiCs.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.ExploreWadiC.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(
		ExploreWadiC exploreWadiC1, ExploreWadiC exploreWadiC2) {

		if (exploreWadiC1 == exploreWadiC2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreWadiC1.getDescription(),
						exploreWadiC2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"estimatedTourTime", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						exploreWadiC1.getEstimatedTourTime(),
						exploreWadiC2.getEstimatedTourTime())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"estimatedTourTimeLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						exploreWadiC1.getEstimatedTourTimeLabel(),
						exploreWadiC2.getEstimatedTourTimeLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("galleryImages", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreWadiC1.getGalleryImages(),
						exploreWadiC2.getGalleryImages())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("id", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreWadiC1.getId(), exploreWadiC2.getId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("image", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreWadiC1.getImage(), exploreWadiC2.getImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("name", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreWadiC1.getName(), exploreWadiC2.getName())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("type", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreWadiC1.getType(), exploreWadiC2.getType())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("video", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreWadiC1.getVideo(), exploreWadiC2.getVideo())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_exploreWadiCResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_exploreWadiCResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, ExploreWadiC exploreWadiC) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("description")) {
			sb.append("'");
			sb.append(String.valueOf(exploreWadiC.getDescription()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("estimatedTourTime")) {
			sb.append("'");
			sb.append(String.valueOf(exploreWadiC.getEstimatedTourTime()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("estimatedTourTimeLabel")) {
			sb.append("'");
			sb.append(String.valueOf(exploreWadiC.getEstimatedTourTimeLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("galleryImages")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("id")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("image")) {
			sb.append("'");
			sb.append(String.valueOf(exploreWadiC.getImage()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("name")) {
			sb.append("'");
			sb.append(String.valueOf(exploreWadiC.getName()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("type")) {
			sb.append("'");
			sb.append(String.valueOf(exploreWadiC.getType()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("video")) {
			sb.append("'");
			sb.append(String.valueOf(exploreWadiC.getVideo()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected ExploreWadiC randomExploreWadiC() throws Exception {
		return new ExploreWadiC() {
			{
				description = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				estimatedTourTime = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				estimatedTourTimeLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				id = RandomTestUtil.randomLong();
				image = StringUtil.toLowerCase(RandomTestUtil.randomString());
				name = StringUtil.toLowerCase(RandomTestUtil.randomString());
				type = StringUtil.toLowerCase(RandomTestUtil.randomString());
				video = StringUtil.toLowerCase(RandomTestUtil.randomString());
			}
		};
	}

	protected ExploreWadiC randomIrrelevantExploreWadiC() throws Exception {
		ExploreWadiC randomIrrelevantExploreWadiC = randomExploreWadiC();

		return randomIrrelevantExploreWadiC;
	}

	protected ExploreWadiC randomPatchExploreWadiC() throws Exception {
		return randomExploreWadiC();
	}

	protected ExploreWadiCResource exploreWadiCResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseExploreWadiCResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.ExploreWadiCResource
		_exploreWadiCResource;

}