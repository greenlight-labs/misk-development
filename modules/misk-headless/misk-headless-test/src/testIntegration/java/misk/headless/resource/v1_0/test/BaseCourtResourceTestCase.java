package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.Court;
import misk.headless.client.dto.v1_0.CourtItems;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.CourtResource;
import misk.headless.client.serdes.v1_0.CourtSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseCourtResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_courtResource.setContextCompany(testCompany);

		CourtResource.Builder builder = CourtResource.builder();

		courtResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Court court1 = randomCourt();

		String json = objectMapper.writeValueAsString(court1);

		Court court2 = CourtSerDes.toDTO(json);

		Assert.assertTrue(equals(court1, court2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Court court = randomCourt();

		String json1 = objectMapper.writeValueAsString(court);
		String json2 = CourtSerDes.toJSON(court);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		Court court = randomCourt();

		court.setContactEmail(regex);
		court.setContactPhone(regex);
		court.setDescription(regex);
		court.setHeading(regex);
		court.setLat(regex);
		court.setListingImage(regex);
		court.setLon(regex);
		court.setName(regex);
		court.setOpeningDays(regex);
		court.setOpeningHours(regex);
		court.setVenue(regex);

		String json = CourtSerDes.toJSON(court);

		Assert.assertFalse(json.contains(regex));

		court = CourtSerDes.toDTO(json);

		Assert.assertEquals(regex, court.getContactEmail());
		Assert.assertEquals(regex, court.getContactPhone());
		Assert.assertEquals(regex, court.getDescription());
		Assert.assertEquals(regex, court.getHeading());
		Assert.assertEquals(regex, court.getLat());
		Assert.assertEquals(regex, court.getListingImage());
		Assert.assertEquals(regex, court.getLon());
		Assert.assertEquals(regex, court.getName());
		Assert.assertEquals(regex, court.getOpeningDays());
		Assert.assertEquals(regex, court.getOpeningHours());
		Assert.assertEquals(regex, court.getVenue());
	}

	@Test
	public void testGetCourtDetails() throws Exception {
		Court postCourt = testGetCourtDetails_addCourt();

		Court getCourt = courtResource.getCourtDetails(
			postCourt.getId(), null, null);

		assertEquals(postCourt, getCourt);
		assertValid(getCourt);
	}

	protected Court testGetCourtDetails_addCourt() throws Exception {
		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGraphQLGetCourtDetails() throws Exception {
		Court court = testGraphQLCourt_addCourt();

		Assert.assertTrue(
			equals(
				court,
				CourtSerDes.toDTO(
					JSONUtil.getValueAsString(
						invokeGraphQLQuery(
							new GraphQLField(
								"courtDetails",
								new HashMap<String, Object>() {
									{
										put("courtId", court.getId());
									}
								},
								getGraphQLFields())),
						"JSONObject/data", "Object/courtDetails"))));
	}

	@Test
	public void testGraphQLGetCourtDetailsNotFound() throws Exception {
		Long irrelevantCourtId = RandomTestUtil.randomLong();

		Assert.assertEquals(
			"Not Found",
			JSONUtil.getValueAsString(
				invokeGraphQLQuery(
					new GraphQLField(
						"courtDetails",
						new HashMap<String, Object>() {
							{
								put("courtId", irrelevantCourtId);
							}
						},
						getGraphQLFields())),
				"JSONArray/errors", "Object/0", "JSONObject/extensions",
				"Object/code"));
	}

	@Test
	public void testGetRelatedCourts() throws Exception {
		Long categoryId = testGetRelatedCourts_getCategoryId();
		Long irrelevantCategoryId =
			testGetRelatedCourts_getIrrelevantCategoryId();

		Page<Court> page = courtResource.getRelatedCourts(
			categoryId, null, RandomTestUtil.randomString());

		Assert.assertEquals(0, page.getTotalCount());

		if (irrelevantCategoryId != null) {
			Court irrelevantCourt = testGetRelatedCourts_addCourt(
				irrelevantCategoryId, randomIrrelevantCourt());

			page = courtResource.getRelatedCourts(
				irrelevantCategoryId, null, null);

			Assert.assertEquals(1, page.getTotalCount());

			assertEquals(
				Arrays.asList(irrelevantCourt), (List<Court>)page.getItems());
			assertValid(page);
		}

		Court court1 = testGetRelatedCourts_addCourt(categoryId, randomCourt());

		Court court2 = testGetRelatedCourts_addCourt(categoryId, randomCourt());

		page = courtResource.getRelatedCourts(categoryId, null, null);

		Assert.assertEquals(2, page.getTotalCount());

		assertEqualsIgnoringOrder(
			Arrays.asList(court1, court2), (List<Court>)page.getItems());
		assertValid(page);
	}

	protected Court testGetRelatedCourts_addCourt(Long categoryId, Court court)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected Long testGetRelatedCourts_getCategoryId() throws Exception {
		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected Long testGetRelatedCourts_getIrrelevantCategoryId()
		throws Exception {

		return null;
	}

	@Test
	public void testGetCourtItems() throws Exception {
		Court postCourt = testGetCourt_addCourt();

		CourtItems postCourtItems = testGetCourtItems_addCourtItems(
			postCourt.getId(), randomCourtItems());

		CourtItems getCourtItems = courtResource.getCourtItems(
			postCourt.getId());

		assertEquals(postCourtItems, getCourtItems);
		assertValid(getCourtItems);
	}

	protected CourtItems testGetCourtItems_addCourtItems(
			long courtId, CourtItems courtItems)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected Court testGraphQLCourt_addCourt() throws Exception {
		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertContains(Court court, List<Court> courts) {
		boolean contains = false;

		for (Court item : courts) {
			if (equals(court, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(courts + " does not contain " + court, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(Court court1, Court court2) {
		Assert.assertTrue(
			court1 + " does not equal " + court2, equals(court1, court2));
	}

	protected void assertEquals(List<Court> courts1, List<Court> courts2) {
		Assert.assertEquals(courts1.size(), courts2.size());

		for (int i = 0; i < courts1.size(); i++) {
			Court court1 = courts1.get(i);
			Court court2 = courts2.get(i);

			assertEquals(court1, court2);
		}
	}

	protected void assertEquals(
		CourtItems courtItems1, CourtItems courtItems2) {

		Assert.assertTrue(
			courtItems1 + " does not equal " + courtItems2,
			equals(courtItems1, courtItems2));
	}

	protected void assertEqualsIgnoringOrder(
		List<Court> courts1, List<Court> courts2) {

		Assert.assertEquals(courts1.size(), courts2.size());

		for (Court court1 : courts1) {
			boolean contains = false;

			for (Court court2 : courts2) {
				if (equals(court1, court2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				courts2 + " does not contain " + court1, contains);
		}
	}

	protected void assertValid(Court court) throws Exception {
		boolean valid = true;

		if (court.getId() == null) {
			valid = false;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("categoryId", additionalAssertFieldName)) {
				if (court.getCategoryId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("contactEmail", additionalAssertFieldName)) {
				if (court.getContactEmail() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("contactPhone", additionalAssertFieldName)) {
				if (court.getContactPhone() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("courtBooking", additionalAssertFieldName)) {
				if (court.getCourtBooking() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("courtCategory", additionalAssertFieldName)) {
				if (court.getCourtCategory() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (court.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("detailImages", additionalAssertFieldName)) {
				if (court.getDetailImages() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("heading", additionalAssertFieldName)) {
				if (court.getHeading() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("lat", additionalAssertFieldName)) {
				if (court.getLat() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("listingImage", additionalAssertFieldName)) {
				if (court.getListingImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("lon", additionalAssertFieldName)) {
				if (court.getLon() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("name", additionalAssertFieldName)) {
				if (court.getName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("openingDays", additionalAssertFieldName)) {
				if (court.getOpeningDays() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("openingHours", additionalAssertFieldName)) {
				if (court.getOpeningHours() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("venue", additionalAssertFieldName)) {
				if (court.getVenue() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Court> page) {
		boolean valid = false;

		java.util.Collection<Court> courts = page.getItems();

		int size = courts.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(CourtItems courtItems) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalCourtItemsAssertFieldNames()) {

			if (Objects.equals("courtCategories", additionalAssertFieldName)) {
				if (courtItems.getCourtCategories() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("courtLocations", additionalAssertFieldName)) {
				if (courtItems.getCourtLocations() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("courts", additionalAssertFieldName)) {
				if (courtItems.getCourts() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalCourtItemsAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.Court.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(Court court1, Court court2) {
		if (court1 == court2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("categoryId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getCategoryId(), court2.getCategoryId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("contactEmail", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getContactEmail(), court2.getContactEmail())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("contactPhone", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getContactPhone(), court2.getContactPhone())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("courtBooking", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getCourtBooking(), court2.getCourtBooking())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("courtCategory", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getCourtCategory(), court2.getCourtCategory())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getDescription(), court2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("detailImages", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getDetailImages(), court2.getDetailImages())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("heading", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getHeading(), court2.getHeading())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("id", additionalAssertFieldName)) {
				if (!Objects.deepEquals(court1.getId(), court2.getId())) {
					return false;
				}

				continue;
			}

			if (Objects.equals("lat", additionalAssertFieldName)) {
				if (!Objects.deepEquals(court1.getLat(), court2.getLat())) {
					return false;
				}

				continue;
			}

			if (Objects.equals("listingImage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getListingImage(), court2.getListingImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("lon", additionalAssertFieldName)) {
				if (!Objects.deepEquals(court1.getLon(), court2.getLon())) {
					return false;
				}

				continue;
			}

			if (Objects.equals("name", additionalAssertFieldName)) {
				if (!Objects.deepEquals(court1.getName(), court2.getName())) {
					return false;
				}

				continue;
			}

			if (Objects.equals("openingDays", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getOpeningDays(), court2.getOpeningDays())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("openingHours", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						court1.getOpeningHours(), court2.getOpeningHours())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("venue", additionalAssertFieldName)) {
				if (!Objects.deepEquals(court1.getVenue(), court2.getVenue())) {
					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected boolean equals(CourtItems courtItems1, CourtItems courtItems2) {
		if (courtItems1 == courtItems2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalCourtItemsAssertFieldNames()) {

			if (Objects.equals("courtCategories", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						courtItems1.getCourtCategories(),
						courtItems2.getCourtCategories())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("courtLocations", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						courtItems1.getCourtLocations(),
						courtItems2.getCourtLocations())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("courts", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						courtItems1.getCourts(), courtItems2.getCourts())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_courtResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_courtResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, Court court) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("categoryId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("contactEmail")) {
			sb.append("'");
			sb.append(String.valueOf(court.getContactEmail()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("contactPhone")) {
			sb.append("'");
			sb.append(String.valueOf(court.getContactPhone()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("courtBooking")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("courtCategory")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("description")) {
			sb.append("'");
			sb.append(String.valueOf(court.getDescription()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("detailImages")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("heading")) {
			sb.append("'");
			sb.append(String.valueOf(court.getHeading()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("id")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("lat")) {
			sb.append("'");
			sb.append(String.valueOf(court.getLat()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("listingImage")) {
			sb.append("'");
			sb.append(String.valueOf(court.getListingImage()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("lon")) {
			sb.append("'");
			sb.append(String.valueOf(court.getLon()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("name")) {
			sb.append("'");
			sb.append(String.valueOf(court.getName()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("openingDays")) {
			sb.append("'");
			sb.append(String.valueOf(court.getOpeningDays()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("openingHours")) {
			sb.append("'");
			sb.append(String.valueOf(court.getOpeningHours()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("venue")) {
			sb.append("'");
			sb.append(String.valueOf(court.getVenue()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected Court randomCourt() throws Exception {
		return new Court() {
			{
				categoryId = RandomTestUtil.randomLong();
				contactEmail = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				contactPhone = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				description = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				heading = StringUtil.toLowerCase(RandomTestUtil.randomString());
				id = RandomTestUtil.randomLong();
				lat = StringUtil.toLowerCase(RandomTestUtil.randomString());
				listingImage = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				lon = StringUtil.toLowerCase(RandomTestUtil.randomString());
				name = StringUtil.toLowerCase(RandomTestUtil.randomString());
				openingDays = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				openingHours = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				venue = StringUtil.toLowerCase(RandomTestUtil.randomString());
			}
		};
	}

	protected Court randomIrrelevantCourt() throws Exception {
		Court randomIrrelevantCourt = randomCourt();

		return randomIrrelevantCourt;
	}

	protected Court randomPatchCourt() throws Exception {
		return randomCourt();
	}

	protected CourtItems randomCourtItems() throws Exception {
		return new CourtItems() {
			{
			}
		};
	}

	protected CourtResource courtResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseCourtResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.CourtResource _courtResource;

}