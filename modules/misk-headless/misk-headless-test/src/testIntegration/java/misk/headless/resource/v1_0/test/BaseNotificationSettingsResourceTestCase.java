package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.NotificationSettings;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.NotificationSettingsResource;
import misk.headless.client.serdes.v1_0.NotificationSettingsSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseNotificationSettingsResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_notificationSettingsResource.setContextCompany(testCompany);

		NotificationSettingsResource.Builder builder =
			NotificationSettingsResource.builder();

		notificationSettingsResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		NotificationSettings notificationSettings1 =
			randomNotificationSettings();

		String json = objectMapper.writeValueAsString(notificationSettings1);

		NotificationSettings notificationSettings2 =
			NotificationSettingsSerDes.toDTO(json);

		Assert.assertTrue(equals(notificationSettings1, notificationSettings2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		NotificationSettings notificationSettings =
			randomNotificationSettings();

		String json1 = objectMapper.writeValueAsString(notificationSettings);
		String json2 = NotificationSettingsSerDes.toJSON(notificationSettings);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		NotificationSettings notificationSettings =
			randomNotificationSettings();

		String json = NotificationSettingsSerDes.toJSON(notificationSettings);

		Assert.assertFalse(json.contains(regex));

		notificationSettings = NotificationSettingsSerDes.toDTO(json);
	}

	@Test
	public void testGetNotificationSettingsPage() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetNotificationSettingsPage() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetNotificationSettingsPageNotFound()
		throws Exception {

		Assert.assertTrue(true);
	}

	@Test
	public void testPostNotificationSettings() throws Exception {
		NotificationSettings randomNotificationSettings =
			randomNotificationSettings();

		NotificationSettings postNotificationSettings =
			testPostNotificationSettings_addNotificationSettings(
				randomNotificationSettings);

		assertEquals(randomNotificationSettings, postNotificationSettings);
		assertValid(postNotificationSettings);
	}

	protected NotificationSettings
			testPostNotificationSettings_addNotificationSettings(
				NotificationSettings notificationSettings)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertContains(
		NotificationSettings notificationSettings,
		List<NotificationSettings> notificationSettingses) {

		boolean contains = false;

		for (NotificationSettings item : notificationSettingses) {
			if (equals(notificationSettings, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			notificationSettingses + " does not contain " +
				notificationSettings,
			contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(
		NotificationSettings notificationSettings1,
		NotificationSettings notificationSettings2) {

		Assert.assertTrue(
			notificationSettings1 + " does not equal " + notificationSettings2,
			equals(notificationSettings1, notificationSettings2));
	}

	protected void assertEquals(
		List<NotificationSettings> notificationSettingses1,
		List<NotificationSettings> notificationSettingses2) {

		Assert.assertEquals(
			notificationSettingses1.size(), notificationSettingses2.size());

		for (int i = 0; i < notificationSettingses1.size(); i++) {
			NotificationSettings notificationSettings1 =
				notificationSettingses1.get(i);
			NotificationSettings notificationSettings2 =
				notificationSettingses2.get(i);

			assertEquals(notificationSettings1, notificationSettings2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<NotificationSettings> notificationSettingses1,
		List<NotificationSettings> notificationSettingses2) {

		Assert.assertEquals(
			notificationSettingses1.size(), notificationSettingses2.size());

		for (NotificationSettings notificationSettings1 :
				notificationSettingses1) {

			boolean contains = false;

			for (NotificationSettings notificationSettings2 :
					notificationSettingses2) {

				if (equals(notificationSettings1, notificationSettings2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				notificationSettingses2 + " does not contain " +
					notificationSettings1,
				contains);
		}
	}

	protected void assertValid(NotificationSettings notificationSettings)
		throws Exception {

		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (notificationSettings.getAppUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("broadcastMessage", additionalAssertFieldName)) {
				if (notificationSettings.getBroadcastMessage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("companyId", additionalAssertFieldName)) {
				if (notificationSettings.getCompanyId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("events", additionalAssertFieldName)) {
				if (notificationSettings.getEvents() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("groupId", additionalAssertFieldName)) {
				if (notificationSettings.getGroupId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("highlight", additionalAssertFieldName)) {
				if (notificationSettings.getHighlight() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("news", additionalAssertFieldName)) {
				if (notificationSettings.getNews() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<NotificationSettings> page) {
		boolean valid = false;

		java.util.Collection<NotificationSettings> notificationSettingses =
			page.getItems();

		int size = notificationSettingses.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(
					misk.headless.dto.v1_0.NotificationSettings.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(
		NotificationSettings notificationSettings1,
		NotificationSettings notificationSettings2) {

		if (notificationSettings1 == notificationSettings2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						notificationSettings1.getAppUserId(),
						notificationSettings2.getAppUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("broadcastMessage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						notificationSettings1.getBroadcastMessage(),
						notificationSettings2.getBroadcastMessage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("companyId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						notificationSettings1.getCompanyId(),
						notificationSettings2.getCompanyId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("events", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						notificationSettings1.getEvents(),
						notificationSettings2.getEvents())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("groupId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						notificationSettings1.getGroupId(),
						notificationSettings2.getGroupId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("highlight", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						notificationSettings1.getHighlight(),
						notificationSettings2.getHighlight())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("news", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						notificationSettings1.getNews(),
						notificationSettings2.getNews())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_notificationSettingsResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_notificationSettingsResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator,
		NotificationSettings notificationSettings) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("appUserId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("broadcastMessage")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("companyId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("events")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("groupId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("highlight")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("news")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected NotificationSettings randomNotificationSettings()
		throws Exception {

		return new NotificationSettings() {
			{
				appUserId = RandomTestUtil.randomLong();
				broadcastMessage = RandomTestUtil.randomBoolean();
				companyId = RandomTestUtil.randomInt();
				events = RandomTestUtil.randomBoolean();
				groupId = RandomTestUtil.randomInt();
				highlight = RandomTestUtil.randomBoolean();
				news = RandomTestUtil.randomBoolean();
			}
		};
	}

	protected NotificationSettings randomIrrelevantNotificationSettings()
		throws Exception {

		NotificationSettings randomIrrelevantNotificationSettings =
			randomNotificationSettings();

		return randomIrrelevantNotificationSettings;
	}

	protected NotificationSettings randomPatchNotificationSettings()
		throws Exception {

		return randomNotificationSettings();
	}

	protected NotificationSettingsResource notificationSettingsResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseNotificationSettingsResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.NotificationSettingsResource
		_notificationSettingsResource;

}