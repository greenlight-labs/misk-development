package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.PushNotification;
import misk.headless.client.dto.v1_0.UnreadNotificationCount;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.pagination.Pagination;
import misk.headless.client.resource.v1_0.PushNotificationResource;
import misk.headless.client.serdes.v1_0.PushNotificationSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BasePushNotificationResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_pushNotificationResource.setContextCompany(testCompany);

		PushNotificationResource.Builder builder =
			PushNotificationResource.builder();

		pushNotificationResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		PushNotification pushNotification1 = randomPushNotification();

		String json = objectMapper.writeValueAsString(pushNotification1);

		PushNotification pushNotification2 = PushNotificationSerDes.toDTO(json);

		Assert.assertTrue(equals(pushNotification1, pushNotification2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		PushNotification pushNotification = randomPushNotification();

		String json1 = objectMapper.writeValueAsString(pushNotification);
		String json2 = PushNotificationSerDes.toJSON(pushNotification);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		PushNotification pushNotification = randomPushNotification();

		pushNotification.setBody(regex);
		pushNotification.setTitle(regex);
		pushNotification.setType(regex);
		pushNotification.setTypeLabel(regex);

		String json = PushNotificationSerDes.toJSON(pushNotification);

		Assert.assertFalse(json.contains(regex));

		pushNotification = PushNotificationSerDes.toDTO(json);

		Assert.assertEquals(regex, pushNotification.getBody());
		Assert.assertEquals(regex, pushNotification.getTitle());
		Assert.assertEquals(regex, pushNotification.getType());
		Assert.assertEquals(regex, pushNotification.getTypeLabel());
	}

	@Test
	public void testGetPushNotifications() throws Exception {
		Page<PushNotification> page =
			pushNotificationResource.getPushNotifications(
				null, Pagination.of(1, 10));

		long totalCount = page.getTotalCount();

		PushNotification pushNotification1 =
			testGetPushNotifications_addPushNotification(
				randomPushNotification());

		PushNotification pushNotification2 =
			testGetPushNotifications_addPushNotification(
				randomPushNotification());

		page = pushNotificationResource.getPushNotifications(
			null, Pagination.of(1, 10));

		Assert.assertEquals(totalCount + 2, page.getTotalCount());

		assertContains(
			pushNotification1, (List<PushNotification>)page.getItems());
		assertContains(
			pushNotification2, (List<PushNotification>)page.getItems());
		assertValid(page);
	}

	@Test
	public void testGetPushNotificationsWithPagination() throws Exception {
		Page<PushNotification> totalPage =
			pushNotificationResource.getPushNotifications(null, null);

		int totalCount = GetterUtil.getInteger(totalPage.getTotalCount());

		PushNotification pushNotification1 =
			testGetPushNotifications_addPushNotification(
				randomPushNotification());

		PushNotification pushNotification2 =
			testGetPushNotifications_addPushNotification(
				randomPushNotification());

		PushNotification pushNotification3 =
			testGetPushNotifications_addPushNotification(
				randomPushNotification());

		Page<PushNotification> page1 =
			pushNotificationResource.getPushNotifications(
				null, Pagination.of(1, totalCount + 2));

		List<PushNotification> pushNotifications1 =
			(List<PushNotification>)page1.getItems();

		Assert.assertEquals(
			pushNotifications1.toString(), totalCount + 2,
			pushNotifications1.size());

		Page<PushNotification> page2 =
			pushNotificationResource.getPushNotifications(
				null, Pagination.of(2, totalCount + 2));

		Assert.assertEquals(totalCount + 3, page2.getTotalCount());

		List<PushNotification> pushNotifications2 =
			(List<PushNotification>)page2.getItems();

		Assert.assertEquals(
			pushNotifications2.toString(), 1, pushNotifications2.size());

		Page<PushNotification> page3 =
			pushNotificationResource.getPushNotifications(
				null, Pagination.of(1, totalCount + 3));

		assertContains(
			pushNotification1, (List<PushNotification>)page3.getItems());
		assertContains(
			pushNotification2, (List<PushNotification>)page3.getItems());
		assertContains(
			pushNotification3, (List<PushNotification>)page3.getItems());
	}

	protected PushNotification testGetPushNotifications_addPushNotification(
			PushNotification pushNotification)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGraphQLGetPushNotifications() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testUpdateReadStatus() throws Exception {
		PushNotification randomPushNotification = randomPushNotification();

		PushNotification postPushNotification =
			testUpdateReadStatus_addPushNotification(randomPushNotification);

		assertEquals(randomPushNotification, postPushNotification);
		assertValid(postPushNotification);
	}

	protected PushNotification testUpdateReadStatus_addPushNotification(
			PushNotification pushNotification)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGetUnreadNotificationCount() throws Exception {
		PushNotification postPushNotification =
			testGetPushNotification_addPushNotification();

		UnreadNotificationCount postUnreadNotificationCount =
			testGetUnreadNotificationCount_addUnreadNotificationCount(
				postPushNotification.getId(), randomUnreadNotificationCount());

		UnreadNotificationCount getUnreadNotificationCount =
			pushNotificationResource.getUnreadNotificationCount(
				postPushNotification.getId());

		assertEquals(postUnreadNotificationCount, getUnreadNotificationCount);
		assertValid(getUnreadNotificationCount);
	}

	protected UnreadNotificationCount
			testGetUnreadNotificationCount_addUnreadNotificationCount(
				long pushNotificationId,
				UnreadNotificationCount unreadNotificationCount)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertContains(
		PushNotification pushNotification,
		List<PushNotification> pushNotifications) {

		boolean contains = false;

		for (PushNotification item : pushNotifications) {
			if (equals(pushNotification, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			pushNotifications + " does not contain " + pushNotification,
			contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(
		PushNotification pushNotification1,
		PushNotification pushNotification2) {

		Assert.assertTrue(
			pushNotification1 + " does not equal " + pushNotification2,
			equals(pushNotification1, pushNotification2));
	}

	protected void assertEquals(
		List<PushNotification> pushNotifications1,
		List<PushNotification> pushNotifications2) {

		Assert.assertEquals(
			pushNotifications1.size(), pushNotifications2.size());

		for (int i = 0; i < pushNotifications1.size(); i++) {
			PushNotification pushNotification1 = pushNotifications1.get(i);
			PushNotification pushNotification2 = pushNotifications2.get(i);

			assertEquals(pushNotification1, pushNotification2);
		}
	}

	protected void assertEquals(
		UnreadNotificationCount unreadNotificationCount1,
		UnreadNotificationCount unreadNotificationCount2) {

		Assert.assertTrue(
			unreadNotificationCount1 + " does not equal " +
				unreadNotificationCount2,
			equals(unreadNotificationCount1, unreadNotificationCount2));
	}

	protected void assertEqualsIgnoringOrder(
		List<PushNotification> pushNotifications1,
		List<PushNotification> pushNotifications2) {

		Assert.assertEquals(
			pushNotifications1.size(), pushNotifications2.size());

		for (PushNotification pushNotification1 : pushNotifications1) {
			boolean contains = false;

			for (PushNotification pushNotification2 : pushNotifications2) {
				if (equals(pushNotification1, pushNotification2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				pushNotifications2 + " does not contain " + pushNotification1,
				contains);
		}
	}

	protected void assertValid(PushNotification pushNotification)
		throws Exception {

		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (pushNotification.getAppUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("body", additionalAssertFieldName)) {
				if (pushNotification.getBody() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("highlight", additionalAssertFieldName)) {
				if (pushNotification.getHighlight() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("notificationId", additionalAssertFieldName)) {
				if (pushNotification.getNotificationId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("readStatus", additionalAssertFieldName)) {
				if (pushNotification.getReadStatus() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (pushNotification.getRequestStatus() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (pushNotification.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("type", additionalAssertFieldName)) {
				if (pushNotification.getType() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("typeId", additionalAssertFieldName)) {
				if (pushNotification.getTypeId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("typeLabel", additionalAssertFieldName)) {
				if (pushNotification.getTypeLabel() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<PushNotification> page) {
		boolean valid = false;

		java.util.Collection<PushNotification> pushNotifications =
			page.getItems();

		int size = pushNotifications.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(
		UnreadNotificationCount unreadNotificationCount) {

		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalUnreadNotificationCountAssertFieldNames()) {

			if (Objects.equals("unreadCount", additionalAssertFieldName)) {
				if (unreadNotificationCount.getUnreadCount() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalUnreadNotificationCountAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(
					misk.headless.dto.v1_0.PushNotification.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(
		PushNotification pushNotification1,
		PushNotification pushNotification2) {

		if (pushNotification1 == pushNotification2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getAppUserId(),
						pushNotification2.getAppUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("body", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getBody(),
						pushNotification2.getBody())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("highlight", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getHighlight(),
						pushNotification2.getHighlight())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("notificationId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getNotificationId(),
						pushNotification2.getNotificationId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("readStatus", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getReadStatus(),
						pushNotification2.getReadStatus())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getRequestStatus(),
						pushNotification2.getRequestStatus())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getTitle(),
						pushNotification2.getTitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("type", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getType(),
						pushNotification2.getType())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("typeId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getTypeId(),
						pushNotification2.getTypeId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("typeLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						pushNotification1.getTypeLabel(),
						pushNotification2.getTypeLabel())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected boolean equals(
		UnreadNotificationCount unreadNotificationCount1,
		UnreadNotificationCount unreadNotificationCount2) {

		if (unreadNotificationCount1 == unreadNotificationCount2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalUnreadNotificationCountAssertFieldNames()) {

			if (Objects.equals("unreadCount", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						unreadNotificationCount1.getUnreadCount(),
						unreadNotificationCount2.getUnreadCount())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_pushNotificationResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_pushNotificationResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator,
		PushNotification pushNotification) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("appUserId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("body")) {
			sb.append("'");
			sb.append(String.valueOf(pushNotification.getBody()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("highlight")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("notificationId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("readStatus")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("requestStatus")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("title")) {
			sb.append("'");
			sb.append(String.valueOf(pushNotification.getTitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("type")) {
			sb.append("'");
			sb.append(String.valueOf(pushNotification.getType()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("typeId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("typeLabel")) {
			sb.append("'");
			sb.append(String.valueOf(pushNotification.getTypeLabel()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected PushNotification randomPushNotification() throws Exception {
		return new PushNotification() {
			{
				appUserId = RandomTestUtil.randomLong();
				body = StringUtil.toLowerCase(RandomTestUtil.randomString());
				notificationId = RandomTestUtil.randomLong();
				readStatus = RandomTestUtil.randomBoolean();
				title = StringUtil.toLowerCase(RandomTestUtil.randomString());
				type = StringUtil.toLowerCase(RandomTestUtil.randomString());
				typeId = RandomTestUtil.randomLong();
				typeLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected PushNotification randomIrrelevantPushNotification()
		throws Exception {

		PushNotification randomIrrelevantPushNotification =
			randomPushNotification();

		return randomIrrelevantPushNotification;
	}

	protected PushNotification randomPatchPushNotification() throws Exception {
		return randomPushNotification();
	}

	protected UnreadNotificationCount randomUnreadNotificationCount()
		throws Exception {

		return new UnreadNotificationCount() {
			{
				unreadCount = RandomTestUtil.randomInteger();
			}
		};
	}

	protected PushNotificationResource pushNotificationResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BasePushNotificationResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.PushNotificationResource
		_pushNotificationResource;

}