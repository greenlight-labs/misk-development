package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.Contact;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.ContactResource;
import misk.headless.client.serdes.v1_0.ContactSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseContactResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_contactResource.setContextCompany(testCompany);

		ContactResource.Builder builder = ContactResource.builder();

		contactResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Contact contact1 = randomContact();

		String json = objectMapper.writeValueAsString(contact1);

		Contact contact2 = ContactSerDes.toDTO(json);

		Assert.assertTrue(equals(contact1, contact2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Contact contact = randomContact();

		String json1 = objectMapper.writeValueAsString(contact);
		String json2 = ContactSerDes.toJSON(contact);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		Contact contact = randomContact();

		contact.setAddress(regex);
		contact.setCallUsLabel(regex);
		contact.setCallUsNumber(regex);
		contact.setContactLabel(regex);
		contact.setDescription(regex);
		contact.setEmailAddress(regex);
		contact.setEmailLabel(regex);
		contact.setLocationLabel(regex);
		contact.setLocationLatitude(regex);
		contact.setLocationLongitude(regex);
		contact.setSocialMediaLabel(regex);
		contact.setWebsiteAddress(regex);
		contact.setWebsiteLabel(regex);
		contact.setWhatsAppLabel(regex);
		contact.setWhatsAppNumber(regex);
		contact.setWorkingHours(regex);
		contact.setWorkingHoursLabel(regex);

		String json = ContactSerDes.toJSON(contact);

		Assert.assertFalse(json.contains(regex));

		contact = ContactSerDes.toDTO(json);

		Assert.assertEquals(regex, contact.getAddress());
		Assert.assertEquals(regex, contact.getCallUsLabel());
		Assert.assertEquals(regex, contact.getCallUsNumber());
		Assert.assertEquals(regex, contact.getContactLabel());
		Assert.assertEquals(regex, contact.getDescription());
		Assert.assertEquals(regex, contact.getEmailAddress());
		Assert.assertEquals(regex, contact.getEmailLabel());
		Assert.assertEquals(regex, contact.getLocationLabel());
		Assert.assertEquals(regex, contact.getLocationLatitude());
		Assert.assertEquals(regex, contact.getLocationLongitude());
		Assert.assertEquals(regex, contact.getSocialMediaLabel());
		Assert.assertEquals(regex, contact.getWebsiteAddress());
		Assert.assertEquals(regex, contact.getWebsiteLabel());
		Assert.assertEquals(regex, contact.getWhatsAppLabel());
		Assert.assertEquals(regex, contact.getWhatsAppNumber());
		Assert.assertEquals(regex, contact.getWorkingHours());
		Assert.assertEquals(regex, contact.getWorkingHoursLabel());
	}

	@Test
	public void testGetContact() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetContact() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetContactNotFound() throws Exception {
		Assert.assertTrue(true);
	}

	protected void assertContains(Contact contact, List<Contact> contacts) {
		boolean contains = false;

		for (Contact item : contacts) {
			if (equals(contact, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(contacts + " does not contain " + contact, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(Contact contact1, Contact contact2) {
		Assert.assertTrue(
			contact1 + " does not equal " + contact2,
			equals(contact1, contact2));
	}

	protected void assertEquals(
		List<Contact> contacts1, List<Contact> contacts2) {

		Assert.assertEquals(contacts1.size(), contacts2.size());

		for (int i = 0; i < contacts1.size(); i++) {
			Contact contact1 = contacts1.get(i);
			Contact contact2 = contacts2.get(i);

			assertEquals(contact1, contact2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<Contact> contacts1, List<Contact> contacts2) {

		Assert.assertEquals(contacts1.size(), contacts2.size());

		for (Contact contact1 : contacts1) {
			boolean contains = false;

			for (Contact contact2 : contacts2) {
				if (equals(contact1, contact2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				contacts2 + " does not contain " + contact1, contains);
		}
	}

	protected void assertValid(Contact contact) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("Address", additionalAssertFieldName)) {
				if (contact.getAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("CallUsLabel", additionalAssertFieldName)) {
				if (contact.getCallUsLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("CallUsNumber", additionalAssertFieldName)) {
				if (contact.getCallUsNumber() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("ContactLabel", additionalAssertFieldName)) {
				if (contact.getContactLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("Description", additionalAssertFieldName)) {
				if (contact.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("EmailAddress", additionalAssertFieldName)) {
				if (contact.getEmailAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("EmailLabel", additionalAssertFieldName)) {
				if (contact.getEmailLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("LocationLabel", additionalAssertFieldName)) {
				if (contact.getLocationLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("LocationLatitude", additionalAssertFieldName)) {
				if (contact.getLocationLatitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"LocationLongitude", additionalAssertFieldName)) {

				if (contact.getLocationLongitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("SocialMediaLabel", additionalAssertFieldName)) {
				if (contact.getSocialMediaLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("SocialMediaList", additionalAssertFieldName)) {
				if (contact.getSocialMediaList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("WebsiteAddress", additionalAssertFieldName)) {
				if (contact.getWebsiteAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("WebsiteLabel", additionalAssertFieldName)) {
				if (contact.getWebsiteLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("WhatsAppLabel", additionalAssertFieldName)) {
				if (contact.getWhatsAppLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("WhatsAppNumber", additionalAssertFieldName)) {
				if (contact.getWhatsAppNumber() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("WorkingHours", additionalAssertFieldName)) {
				if (contact.getWorkingHours() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"WorkingHoursLabel", additionalAssertFieldName)) {

				if (contact.getWorkingHoursLabel() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Contact> page) {
		boolean valid = false;

		java.util.Collection<Contact> contacts = page.getItems();

		int size = contacts.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.Contact.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(Contact contact1, Contact contact2) {
		if (contact1 == contact2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("Address", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getAddress(), contact2.getAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("CallUsLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getCallUsLabel(), contact2.getCallUsLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("CallUsNumber", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getCallUsNumber(),
						contact2.getCallUsNumber())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("ContactLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getContactLabel(),
						contact2.getContactLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("Description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getDescription(), contact2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("EmailAddress", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getEmailAddress(),
						contact2.getEmailAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("EmailLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getEmailLabel(), contact2.getEmailLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("LocationLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getLocationLabel(),
						contact2.getLocationLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("LocationLatitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getLocationLatitude(),
						contact2.getLocationLatitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"LocationLongitude", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						contact1.getLocationLongitude(),
						contact2.getLocationLongitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("SocialMediaLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getSocialMediaLabel(),
						contact2.getSocialMediaLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("SocialMediaList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getSocialMediaList(),
						contact2.getSocialMediaList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("WebsiteAddress", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getWebsiteAddress(),
						contact2.getWebsiteAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("WebsiteLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getWebsiteLabel(),
						contact2.getWebsiteLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("WhatsAppLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getWhatsAppLabel(),
						contact2.getWhatsAppLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("WhatsAppNumber", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getWhatsAppNumber(),
						contact2.getWhatsAppNumber())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("WorkingHours", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						contact1.getWorkingHours(),
						contact2.getWorkingHours())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"WorkingHoursLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						contact1.getWorkingHoursLabel(),
						contact2.getWorkingHoursLabel())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_contactResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_contactResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, Contact contact) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("Address")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getAddress()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("CallUsLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getCallUsLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("CallUsNumber")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getCallUsNumber()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("ContactLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getContactLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("Description")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getDescription()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("EmailAddress")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getEmailAddress()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("EmailLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getEmailLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("LocationLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getLocationLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("LocationLatitude")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getLocationLatitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("LocationLongitude")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getLocationLongitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("SocialMediaLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getSocialMediaLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("SocialMediaList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("WebsiteAddress")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getWebsiteAddress()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WebsiteLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getWebsiteLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WhatsAppLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getWhatsAppLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WhatsAppNumber")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getWhatsAppNumber()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WorkingHours")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getWorkingHours()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("WorkingHoursLabel")) {
			sb.append("'");
			sb.append(String.valueOf(contact.getWorkingHoursLabel()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected Contact randomContact() throws Exception {
		return new Contact() {
			{
				Address = StringUtil.toLowerCase(RandomTestUtil.randomString());
				CallUsLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				CallUsNumber = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				ContactLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				Description = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				EmailAddress = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				EmailLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				LocationLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				LocationLatitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				LocationLongitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				SocialMediaLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WebsiteAddress = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WebsiteLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WhatsAppLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WhatsAppNumber = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WorkingHours = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				WorkingHoursLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected Contact randomIrrelevantContact() throws Exception {
		Contact randomIrrelevantContact = randomContact();

		return randomIrrelevantContact;
	}

	protected Contact randomPatchContact() throws Exception {
		return randomContact();
	}

	protected ContactResource contactResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseContactResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.ContactResource _contactResource;

}