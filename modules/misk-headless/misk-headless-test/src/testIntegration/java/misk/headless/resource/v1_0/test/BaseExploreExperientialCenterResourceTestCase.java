package misk.headless.resource.v1_0.test;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.ExploreExperientialCenterPage;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.ExploreExperientialCenterResource;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseExploreExperientialCenterResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_exploreExperientialCenterResource.setContextCompany(testCompany);

		ExploreExperientialCenterResource.Builder builder =
			ExploreExperientialCenterResource.builder();

		exploreExperientialCenterResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testGetExploreExperientialCenter() throws Exception {
		ExploreExperientialCenter postExploreExperientialCenter =
			testGetExploreExperientialCenter_addExploreExperientialCenter();

		ExploreExperientialCenterPage postExploreExperientialCenterPage =
			testGetExploreExperientialCenter_addExploreExperientialCenterPage(
				postExploreExperientialCenter.getId(),
				randomExploreExperientialCenterPage());

		ExploreExperientialCenterPage getExploreExperientialCenterPage =
			exploreExperientialCenterResource.getExploreExperientialCenter(
				postExploreExperientialCenter.getId());

		assertEquals(
			postExploreExperientialCenterPage,
			getExploreExperientialCenterPage);
		assertValid(getExploreExperientialCenterPage);
	}

	protected ExploreExperientialCenterPage
			testGetExploreExperientialCenter_addExploreExperientialCenterPage(
				long exploreExperientialCenterId,
				ExploreExperientialCenterPage exploreExperientialCenterPage)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertContains(
		Object exploreExperientialCenter,
		List<Object> exploreExperientialCenters) {

		boolean contains = false;

		for (Object item : exploreExperientialCenters) {
			if (equals(exploreExperientialCenter, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			exploreExperientialCenters + " does not contain " +
				exploreExperientialCenter,
			contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(
		Object exploreExperientialCenter1, Object exploreExperientialCenter2) {

		Assert.assertTrue(
			exploreExperientialCenter1 + " does not equal " +
				exploreExperientialCenter2,
			equals(exploreExperientialCenter1, exploreExperientialCenter2));
	}

	protected void assertEquals(
		List<Object> exploreExperientialCenters1,
		List<Object> exploreExperientialCenters2) {

		Assert.assertEquals(
			exploreExperientialCenters1.size(),
			exploreExperientialCenters2.size());

		for (int i = 0; i < exploreExperientialCenters1.size(); i++) {
			Object exploreExperientialCenter1 = exploreExperientialCenters1.get(
				i);
			Object exploreExperientialCenter2 = exploreExperientialCenters2.get(
				i);

			assertEquals(
				exploreExperientialCenter1, exploreExperientialCenter2);
		}
	}

	protected void assertEquals(
		ExploreExperientialCenterPage exploreExperientialCenterPage1,
		ExploreExperientialCenterPage exploreExperientialCenterPage2) {

		Assert.assertTrue(
			exploreExperientialCenterPage1 + " does not equal " +
				exploreExperientialCenterPage2,
			equals(
				exploreExperientialCenterPage1,
				exploreExperientialCenterPage2));
	}

	protected void assertEqualsIgnoringOrder(
		List<Object> exploreExperientialCenters1,
		List<Object> exploreExperientialCenters2) {

		Assert.assertEquals(
			exploreExperientialCenters1.size(),
			exploreExperientialCenters2.size());

		for (Object exploreExperientialCenter1 : exploreExperientialCenters1) {
			boolean contains = false;

			for (Object exploreExperientialCenter2 :
					exploreExperientialCenters2) {

				if (equals(
						exploreExperientialCenter1,
						exploreExperientialCenter2)) {

					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				exploreExperientialCenters2 + " does not contain " +
					exploreExperientialCenter1,
				contains);
		}
	}

	protected void assertValid(Object exploreExperientialCenter)
		throws Exception {

		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Object> page) {
		boolean valid = false;

		java.util.Collection<Object> exploreExperientialCenters =
			page.getItems();

		int size = exploreExperientialCenters.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(
		ExploreExperientialCenterPage exploreExperientialCenterPage) {

		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalExploreExperientialCenterPageAssertFieldNames()) {

			if (Objects.equals(
					"exploreExperientialCenterCategories",
					additionalAssertFieldName)) {

				if (exploreExperientialCenterPage.
						getExploreExperientialCenterCategories() == null) {

					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"exploreExperientialCenterPosts",
					additionalAssertFieldName)) {

				if (exploreExperientialCenterPage.
						getExploreExperientialCenterPosts() == null) {

					valid = false;
				}

				continue;
			}

			if (Objects.equals("pageTitle", additionalAssertFieldName)) {
				if (exploreExperientialCenterPage.getPageTitle() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected String[]
		getAdditionalExploreExperientialCenterPageAssertFieldNames() {

		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(
		Object exploreExperientialCenter1, Object exploreExperientialCenter2) {

		if (exploreExperientialCenter1 == exploreExperientialCenter2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected boolean equals(
		ExploreExperientialCenterPage exploreExperientialCenterPage1,
		ExploreExperientialCenterPage exploreExperientialCenterPage2) {

		if (exploreExperientialCenterPage1 == exploreExperientialCenterPage2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalExploreExperientialCenterPageAssertFieldNames()) {

			if (Objects.equals(
					"exploreExperientialCenterCategories",
					additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						exploreExperientialCenterPage1.
							getExploreExperientialCenterCategories(),
						exploreExperientialCenterPage2.
							getExploreExperientialCenterCategories())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"exploreExperientialCenterPosts",
					additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						exploreExperientialCenterPage1.
							getExploreExperientialCenterPosts(),
						exploreExperientialCenterPage2.
							getExploreExperientialCenterPosts())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("pageTitle", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						exploreExperientialCenterPage1.getPageTitle(),
						exploreExperientialCenterPage2.getPageTitle())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_exploreExperientialCenterResource instanceof
				EntityModelResource)) {

			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_exploreExperientialCenterResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator,
		Object exploreExperientialCenter) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected ExploreExperientialCenterPage
			randomExploreExperientialCenterPage()
		throws Exception {

		return new ExploreExperientialCenterPage() {
			{
				pageTitle = RandomTestUtil.randomString();
			}
		};
	}

	protected ExploreExperientialCenterResource
		exploreExperientialCenterResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(
			BaseExploreExperientialCenterResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.ExploreExperientialCenterResource
		_exploreExperientialCenterResource;

}