package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.Attraction;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.AttractionResource;
import misk.headless.client.serdes.v1_0.AttractionSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseAttractionResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_attractionResource.setContextCompany(testCompany);

		AttractionResource.Builder builder = AttractionResource.builder();

		attractionResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Attraction attraction1 = randomAttraction();

		String json = objectMapper.writeValueAsString(attraction1);

		Attraction attraction2 = AttractionSerDes.toDTO(json);

		Assert.assertTrue(equals(attraction1, attraction2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Attraction attraction = randomAttraction();

		String json1 = objectMapper.writeValueAsString(attraction);
		String json2 = AttractionSerDes.toJSON(attraction);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		Attraction attraction = randomAttraction();

		attraction.setAmenitiesLabel(regex);
		attraction.setAttractionBanner(regex);
		attraction.setAttractionBannerIcon(regex);
		attraction.setAttractionButtonLabel(regex);
		attraction.setAttractionId(regex);
		attraction.setAttractionTitle(regex);
		attraction.setBannerButtonColor(regex);
		attraction.setBookSpaceLabel(regex);
		attraction.setContactEmailAddress(regex);
		attraction.setContactEmailAddressIcon(regex);
		attraction.setContactLabel(regex);
		attraction.setContactTelephone(regex);
		attraction.setContactTelephoneIcon(regex);
		attraction.setDescription(regex);
		attraction.setGalleryLabel(regex);
		attraction.setLocationLabel(regex);
		attraction.setLocationLatitude(regex);
		attraction.setLocationLongitude(regex);
		attraction.setWorkingDays(regex);
		attraction.setWorkingDaysImage(regex);
		attraction.setWorkingHours(regex);
		attraction.setWorkingHoursImage(regex);
		attraction.setWorkingHoursLabel(regex);
		attraction.setWorkingHoursLabelColor(regex);

		String json = AttractionSerDes.toJSON(attraction);

		Assert.assertFalse(json.contains(regex));

		attraction = AttractionSerDes.toDTO(json);

		Assert.assertEquals(regex, attraction.getAmenitiesLabel());
		Assert.assertEquals(regex, attraction.getAttractionBanner());
		Assert.assertEquals(regex, attraction.getAttractionBannerIcon());
		Assert.assertEquals(regex, attraction.getAttractionButtonLabel());
		Assert.assertEquals(regex, attraction.getAttractionId());
		Assert.assertEquals(regex, attraction.getAttractionTitle());
		Assert.assertEquals(regex, attraction.getBannerButtonColor());
		Assert.assertEquals(regex, attraction.getBookSpaceLabel());
		Assert.assertEquals(regex, attraction.getContactEmailAddress());
		Assert.assertEquals(regex, attraction.getContactEmailAddressIcon());
		Assert.assertEquals(regex, attraction.getContactLabel());
		Assert.assertEquals(regex, attraction.getContactTelephone());
		Assert.assertEquals(regex, attraction.getContactTelephoneIcon());
		Assert.assertEquals(regex, attraction.getDescription());
		Assert.assertEquals(regex, attraction.getGalleryLabel());
		Assert.assertEquals(regex, attraction.getLocationLabel());
		Assert.assertEquals(regex, attraction.getLocationLatitude());
		Assert.assertEquals(regex, attraction.getLocationLongitude());
		Assert.assertEquals(regex, attraction.getWorkingDays());
		Assert.assertEquals(regex, attraction.getWorkingDaysImage());
		Assert.assertEquals(regex, attraction.getWorkingHours());
		Assert.assertEquals(regex, attraction.getWorkingHoursImage());
		Assert.assertEquals(regex, attraction.getWorkingHoursLabel());
		Assert.assertEquals(regex, attraction.getWorkingHoursLabelColor());
	}

	@Test
	public void testGetAttraction() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetAttraction() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetAttractionNotFound() throws Exception {
		Assert.assertTrue(true);
	}

	protected void assertContains(
		Attraction attraction, List<Attraction> attractions) {

		boolean contains = false;

		for (Attraction item : attractions) {
			if (equals(attraction, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			attractions + " does not contain " + attraction, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(
		Attraction attraction1, Attraction attraction2) {

		Assert.assertTrue(
			attraction1 + " does not equal " + attraction2,
			equals(attraction1, attraction2));
	}

	protected void assertEquals(
		List<Attraction> attractions1, List<Attraction> attractions2) {

		Assert.assertEquals(attractions1.size(), attractions2.size());

		for (int i = 0; i < attractions1.size(); i++) {
			Attraction attraction1 = attractions1.get(i);
			Attraction attraction2 = attractions2.get(i);

			assertEquals(attraction1, attraction2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<Attraction> attractions1, List<Attraction> attractions2) {

		Assert.assertEquals(attractions1.size(), attractions2.size());

		for (Attraction attraction1 : attractions1) {
			boolean contains = false;

			for (Attraction attraction2 : attractions2) {
				if (equals(attraction1, attraction2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				attractions2 + " does not contain " + attraction1, contains);
		}
	}

	protected void assertValid(Attraction attraction) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("amenitiesLabel", additionalAssertFieldName)) {
				if (attraction.getAmenitiesLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("amenitiesList", additionalAssertFieldName)) {
				if (attraction.getAmenitiesList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("attractionBanner", additionalAssertFieldName)) {
				if (attraction.getAttractionBanner() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"attractionBannerIcon", additionalAssertFieldName)) {

				if (attraction.getAttractionBannerIcon() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"attractionButtonLabel", additionalAssertFieldName)) {

				if (attraction.getAttractionButtonLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("attractionId", additionalAssertFieldName)) {
				if (attraction.getAttractionId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("attractionTitle", additionalAssertFieldName)) {
				if (attraction.getAttractionTitle() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"bannerButtonColor", additionalAssertFieldName)) {

				if (attraction.getBannerButtonColor() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("bookSpaceLabel", additionalAssertFieldName)) {
				if (attraction.getBookSpaceLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("bookSpaceList", additionalAssertFieldName)) {
				if (attraction.getBookSpaceList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"contactEmailAddress", additionalAssertFieldName)) {

				if (attraction.getContactEmailAddress() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"contactEmailAddressIcon", additionalAssertFieldName)) {

				if (attraction.getContactEmailAddressIcon() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("contactLabel", additionalAssertFieldName)) {
				if (attraction.getContactLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("contactTelephone", additionalAssertFieldName)) {
				if (attraction.getContactTelephone() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"contactTelephoneIcon", additionalAssertFieldName)) {

				if (attraction.getContactTelephoneIcon() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (attraction.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (attraction.getEventsSection() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"galleryImagesList", additionalAssertFieldName)) {

				if (attraction.getGalleryImagesList() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("galleryLabel", additionalAssertFieldName)) {
				if (attraction.getGalleryLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("locationLabel", additionalAssertFieldName)) {
				if (attraction.getLocationLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("locationLatitude", additionalAssertFieldName)) {
				if (attraction.getLocationLatitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"locationLongitude", additionalAssertFieldName)) {

				if (attraction.getLocationLongitude() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("workingDays", additionalAssertFieldName)) {
				if (attraction.getWorkingDays() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("workingDaysImage", additionalAssertFieldName)) {
				if (attraction.getWorkingDaysImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("workingHours", additionalAssertFieldName)) {
				if (attraction.getWorkingHours() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"workingHoursImage", additionalAssertFieldName)) {

				if (attraction.getWorkingHoursImage() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"workingHoursLabel", additionalAssertFieldName)) {

				if (attraction.getWorkingHoursLabel() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"workingHoursLabelColor", additionalAssertFieldName)) {

				if (attraction.getWorkingHoursLabelColor() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Attraction> page) {
		boolean valid = false;

		java.util.Collection<Attraction> attractions = page.getItems();

		int size = attractions.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.Attraction.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(Attraction attraction1, Attraction attraction2) {
		if (attraction1 == attraction2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("amenitiesLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getAmenitiesLabel(),
						attraction2.getAmenitiesLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("amenitiesList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getAmenitiesList(),
						attraction2.getAmenitiesList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("attractionBanner", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getAttractionBanner(),
						attraction2.getAttractionBanner())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"attractionBannerIcon", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getAttractionBannerIcon(),
						attraction2.getAttractionBannerIcon())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"attractionButtonLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getAttractionButtonLabel(),
						attraction2.getAttractionButtonLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("attractionId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getAttractionId(),
						attraction2.getAttractionId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("attractionTitle", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getAttractionTitle(),
						attraction2.getAttractionTitle())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"bannerButtonColor", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getBannerButtonColor(),
						attraction2.getBannerButtonColor())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("bookSpaceLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getBookSpaceLabel(),
						attraction2.getBookSpaceLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("bookSpaceList", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getBookSpaceList(),
						attraction2.getBookSpaceList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"contactEmailAddress", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getContactEmailAddress(),
						attraction2.getContactEmailAddress())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"contactEmailAddressIcon", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getContactEmailAddressIcon(),
						attraction2.getContactEmailAddressIcon())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("contactLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getContactLabel(),
						attraction2.getContactLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("contactTelephone", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getContactTelephone(),
						attraction2.getContactTelephone())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"contactTelephoneIcon", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getContactTelephoneIcon(),
						attraction2.getContactTelephoneIcon())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getDescription(),
						attraction2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getEventsSection(),
						attraction2.getEventsSection())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"galleryImagesList", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getGalleryImagesList(),
						attraction2.getGalleryImagesList())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("galleryLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getGalleryLabel(),
						attraction2.getGalleryLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("locationLabel", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getLocationLabel(),
						attraction2.getLocationLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("locationLatitude", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getLocationLatitude(),
						attraction2.getLocationLatitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"locationLongitude", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getLocationLongitude(),
						attraction2.getLocationLongitude())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("workingDays", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getWorkingDays(),
						attraction2.getWorkingDays())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("workingDaysImage", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getWorkingDaysImage(),
						attraction2.getWorkingDaysImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("workingHours", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						attraction1.getWorkingHours(),
						attraction2.getWorkingHours())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"workingHoursImage", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getWorkingHoursImage(),
						attraction2.getWorkingHoursImage())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"workingHoursLabel", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getWorkingHoursLabel(),
						attraction2.getWorkingHoursLabel())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"workingHoursLabelColor", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						attraction1.getWorkingHoursLabelColor(),
						attraction2.getWorkingHoursLabelColor())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_attractionResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_attractionResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, Attraction attraction) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("amenitiesLabel")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getAmenitiesLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("amenitiesList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("attractionBanner")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getAttractionBanner()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("attractionBannerIcon")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getAttractionBannerIcon()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("attractionButtonLabel")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getAttractionButtonLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("attractionId")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getAttractionId()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("attractionTitle")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getAttractionTitle()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("bannerButtonColor")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getBannerButtonColor()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("bookSpaceLabel")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getBookSpaceLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("bookSpaceList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("contactEmailAddress")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getContactEmailAddress()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("contactEmailAddressIcon")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getContactEmailAddressIcon()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("contactLabel")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getContactLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("contactTelephone")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getContactTelephone()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("contactTelephoneIcon")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getContactTelephoneIcon()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("description")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getDescription()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("eventsSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("galleryImagesList")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("galleryLabel")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getGalleryLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("locationLabel")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getLocationLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("locationLatitude")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getLocationLatitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("locationLongitude")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getLocationLongitude()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingDays")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getWorkingDays()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingDaysImage")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getWorkingDaysImage()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingHours")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getWorkingHours()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingHoursImage")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getWorkingHoursImage()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingHoursLabel")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getWorkingHoursLabel()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("workingHoursLabelColor")) {
			sb.append("'");
			sb.append(String.valueOf(attraction.getWorkingHoursLabelColor()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected Attraction randomAttraction() throws Exception {
		return new Attraction() {
			{
				amenitiesLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				attractionBanner = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				attractionBannerIcon = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				attractionButtonLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				attractionId = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				attractionTitle = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				bannerButtonColor = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				bookSpaceLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				contactEmailAddress = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				contactEmailAddressIcon = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				contactLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				contactTelephone = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				contactTelephoneIcon = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				description = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				galleryLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				locationLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				locationLatitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				locationLongitude = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workingDays = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workingDaysImage = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workingHours = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workingHoursImage = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workingHoursLabel = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				workingHoursLabelColor = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected Attraction randomIrrelevantAttraction() throws Exception {
		Attraction randomIrrelevantAttraction = randomAttraction();

		return randomIrrelevantAttraction;
	}

	protected Attraction randomPatchAttraction() throws Exception {
		return randomAttraction();
	}

	protected AttractionResource attractionResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseAttractionResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.AttractionResource _attractionResource;

}