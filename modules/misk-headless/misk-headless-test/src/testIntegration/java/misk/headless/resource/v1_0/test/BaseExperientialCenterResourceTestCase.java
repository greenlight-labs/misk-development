package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.ExperientialCenter;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.ExperientialCenterResource;
import misk.headless.client.serdes.v1_0.ExperientialCenterSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseExperientialCenterResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_experientialCenterResource.setContextCompany(testCompany);

		ExperientialCenterResource.Builder builder =
			ExperientialCenterResource.builder();

		experientialCenterResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		ExperientialCenter experientialCenter1 = randomExperientialCenter();

		String json = objectMapper.writeValueAsString(experientialCenter1);

		ExperientialCenter experientialCenter2 = ExperientialCenterSerDes.toDTO(
			json);

		Assert.assertTrue(equals(experientialCenter1, experientialCenter2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		ExperientialCenter experientialCenter = randomExperientialCenter();

		String json1 = objectMapper.writeValueAsString(experientialCenter);
		String json2 = ExperientialCenterSerDes.toJSON(experientialCenter);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		ExperientialCenter experientialCenter = randomExperientialCenter();

		String json = ExperientialCenterSerDes.toJSON(experientialCenter);

		Assert.assertFalse(json.contains(regex));

		experientialCenter = ExperientialCenterSerDes.toDTO(json);
	}

	@Test
	public void testGetExperientialCenterPage() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGraphQLGetExperientialCenterPage() throws Exception {
		Assert.assertTrue(true);
	}

	@Test
	public void testGraphQLGetExperientialCenterPageNotFound()
		throws Exception {

		Assert.assertTrue(true);
	}

	protected void assertContains(
		ExperientialCenter experientialCenter,
		List<ExperientialCenter> experientialCenters) {

		boolean contains = false;

		for (ExperientialCenter item : experientialCenters) {
			if (equals(experientialCenter, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(
			experientialCenters + " does not contain " + experientialCenter,
			contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(
		ExperientialCenter experientialCenter1,
		ExperientialCenter experientialCenter2) {

		Assert.assertTrue(
			experientialCenter1 + " does not equal " + experientialCenter2,
			equals(experientialCenter1, experientialCenter2));
	}

	protected void assertEquals(
		List<ExperientialCenter> experientialCenters1,
		List<ExperientialCenter> experientialCenters2) {

		Assert.assertEquals(
			experientialCenters1.size(), experientialCenters2.size());

		for (int i = 0; i < experientialCenters1.size(); i++) {
			ExperientialCenter experientialCenter1 = experientialCenters1.get(
				i);
			ExperientialCenter experientialCenter2 = experientialCenters2.get(
				i);

			assertEquals(experientialCenter1, experientialCenter2);
		}
	}

	protected void assertEqualsIgnoringOrder(
		List<ExperientialCenter> experientialCenters1,
		List<ExperientialCenter> experientialCenters2) {

		Assert.assertEquals(
			experientialCenters1.size(), experientialCenters2.size());

		for (ExperientialCenter experientialCenter1 : experientialCenters1) {
			boolean contains = false;

			for (ExperientialCenter experientialCenter2 :
					experientialCenters2) {

				if (equals(experientialCenter1, experientialCenter2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				experientialCenters2 + " does not contain " +
					experientialCenter1,
				contains);
		}
	}

	protected void assertValid(ExperientialCenter experientialCenter)
		throws Exception {

		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("bannerSection", additionalAssertFieldName)) {
				if (experientialCenter.getBannerSection() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("discoverSection", additionalAssertFieldName)) {
				if (experientialCenter.getDiscoverSection() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (experientialCenter.getEventsSection() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("henaCafeSection", additionalAssertFieldName)) {
				if (experientialCenter.getHenaCafeSection() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals(
					"highlightsSection", additionalAssertFieldName)) {

				if (experientialCenter.getHighlightsSection() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<ExperientialCenter> page) {
		boolean valid = false;

		java.util.Collection<ExperientialCenter> experientialCenters =
			page.getItems();

		int size = experientialCenters.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(
					misk.headless.dto.v1_0.ExperientialCenter.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(
		ExperientialCenter experientialCenter1,
		ExperientialCenter experientialCenter2) {

		if (experientialCenter1 == experientialCenter2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("bannerSection", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						experientialCenter1.getBannerSection(),
						experientialCenter2.getBannerSection())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("discoverSection", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						experientialCenter1.getDiscoverSection(),
						experientialCenter2.getDiscoverSection())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("eventsSection", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						experientialCenter1.getEventsSection(),
						experientialCenter2.getEventsSection())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("henaCafeSection", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						experientialCenter1.getHenaCafeSection(),
						experientialCenter2.getHenaCafeSection())) {

					return false;
				}

				continue;
			}

			if (Objects.equals(
					"highlightsSection", additionalAssertFieldName)) {

				if (!Objects.deepEquals(
						experientialCenter1.getHighlightsSection(),
						experientialCenter2.getHighlightsSection())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_experientialCenterResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_experientialCenterResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator,
		ExperientialCenter experientialCenter) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("bannerSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("discoverSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("eventsSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("henaCafeSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("highlightsSection")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected ExperientialCenter randomExperientialCenter() throws Exception {
		return new ExperientialCenter() {
			{
			}
		};
	}

	protected ExperientialCenter randomIrrelevantExperientialCenter()
		throws Exception {

		ExperientialCenter randomIrrelevantExperientialCenter =
			randomExperientialCenter();

		return randomIrrelevantExperientialCenter;
	}

	protected ExperientialCenter randomPatchExperientialCenter()
		throws Exception {

		return randomExperientialCenter();
	}

	protected ExperientialCenterResource experientialCenterResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseExperientialCenterResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.ExperientialCenterResource
		_experientialCenterResource;

}