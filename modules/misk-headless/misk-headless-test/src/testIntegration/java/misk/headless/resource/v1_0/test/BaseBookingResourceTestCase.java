package misk.headless.resource.v1_0.test;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import com.liferay.petra.reflect.ReflectionUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.test.util.GroupTestUtil;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.odata.entity.EntityField;
import com.liferay.portal.odata.entity.EntityModel;
import com.liferay.portal.test.rule.Inject;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.vulcan.resource.EntityModelResource;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import java.text.DateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Generated;

import javax.ws.rs.core.MultivaluedHashMap;

import misk.headless.client.dto.v1_0.Booking;
import misk.headless.client.dto.v1_0.BookingTermsConditions;
import misk.headless.client.http.HttpInvoker;
import misk.headless.client.pagination.Page;
import misk.headless.client.resource.v1_0.BookingResource;
import misk.headless.client.serdes.v1_0.BookingSerDes;

import org.apache.commons.beanutils.BeanUtilsBean;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Tayyab Zafar
 * @generated
 */
@Generated("")
public abstract class BaseBookingResourceTestCase {

	@ClassRule
	@Rule
	public static final LiferayIntegrationTestRule liferayIntegrationTestRule =
		new LiferayIntegrationTestRule();

	@BeforeClass
	public static void setUpClass() throws Exception {
		_dateFormat = DateFormatFactoryUtil.getSimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	}

	@Before
	public void setUp() throws Exception {
		irrelevantGroup = GroupTestUtil.addGroup();
		testGroup = GroupTestUtil.addGroup();

		testCompany = CompanyLocalServiceUtil.getCompany(
			testGroup.getCompanyId());

		_bookingResource.setContextCompany(testCompany);

		BookingResource.Builder builder = BookingResource.builder();

		bookingResource = builder.authentication(
			"test@liferay.com", "test"
		).locale(
			LocaleUtil.getDefault()
		).build();
	}

	@After
	public void tearDown() throws Exception {
		GroupTestUtil.deleteGroup(irrelevantGroup);
		GroupTestUtil.deleteGroup(testGroup);
	}

	@Test
	public void testClientSerDesToDTO() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				enable(SerializationFeature.INDENT_OUTPUT);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Booking booking1 = randomBooking();

		String json = objectMapper.writeValueAsString(booking1);

		Booking booking2 = BookingSerDes.toDTO(json);

		Assert.assertTrue(equals(booking1, booking2));
	}

	@Test
	public void testClientSerDesToJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper() {
			{
				configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
				configure(
					SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
				setDateFormat(new ISO8601DateFormat());
				setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
				setSerializationInclusion(JsonInclude.Include.NON_NULL);
				setVisibility(
					PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
				setVisibility(
					PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE);
			}
		};

		Booking booking = randomBooking();

		String json1 = objectMapper.writeValueAsString(booking);
		String json2 = BookingSerDes.toJSON(booking);

		Assert.assertEquals(
			objectMapper.readTree(json1), objectMapper.readTree(json2));
	}

	@Test
	public void testEscapeRegexInStringFields() throws Exception {
		String regex = "^[0-9]+(\\.[0-9]{1,2})\"?";

		Booking booking = randomBooking();

		booking.setAppUserEmail(regex);
		booking.setAppUserName(regex);
		booking.setBookingDate(regex);
		booking.setCategory(regex);
		booking.setCourtLocation(regex);
		booking.setCourtName(regex);
		booking.setSlotEndTime(regex);
		booking.setSlotStartTime(regex);

		String json = BookingSerDes.toJSON(booking);

		Assert.assertFalse(json.contains(regex));

		booking = BookingSerDes.toDTO(json);

		Assert.assertEquals(regex, booking.getAppUserEmail());
		Assert.assertEquals(regex, booking.getAppUserName());
		Assert.assertEquals(regex, booking.getBookingDate());
		Assert.assertEquals(regex, booking.getCategory());
		Assert.assertEquals(regex, booking.getCourtLocation());
		Assert.assertEquals(regex, booking.getCourtName());
		Assert.assertEquals(regex, booking.getSlotEndTime());
		Assert.assertEquals(regex, booking.getSlotStartTime());
	}

	@Test
	public void testGetCourtBookingTimeSlots() throws Exception {
		Long courtId = testGetCourtBookingTimeSlots_getCourtId();
		Long irrelevantCourtId =
			testGetCourtBookingTimeSlots_getIrrelevantCourtId();

		Page<Booking> page = bookingResource.getCourtBookingTimeSlots(
			courtId, RandomTestUtil.randomString(), RandomTestUtil.nextDate());

		Assert.assertEquals(0, page.getTotalCount());

		if (irrelevantCourtId != null) {
			Booking irrelevantBooking = testGetCourtBookingTimeSlots_addBooking(
				irrelevantCourtId, randomIrrelevantBooking());

			page = bookingResource.getCourtBookingTimeSlots(
				irrelevantCourtId, null, null);

			Assert.assertEquals(1, page.getTotalCount());

			assertEquals(
				Arrays.asList(irrelevantBooking),
				(List<Booking>)page.getItems());
			assertValid(page);
		}

		Booking booking1 = testGetCourtBookingTimeSlots_addBooking(
			courtId, randomBooking());

		Booking booking2 = testGetCourtBookingTimeSlots_addBooking(
			courtId, randomBooking());

		page = bookingResource.getCourtBookingTimeSlots(courtId, null, null);

		Assert.assertEquals(2, page.getTotalCount());

		assertEqualsIgnoringOrder(
			Arrays.asList(booking1, booking2), (List<Booking>)page.getItems());
		assertValid(page);
	}

	protected Booking testGetCourtBookingTimeSlots_addBooking(
			Long courtId, Booking booking)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected Long testGetCourtBookingTimeSlots_getCourtId() throws Exception {
		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected Long testGetCourtBookingTimeSlots_getIrrelevantCourtId()
		throws Exception {

		return null;
	}

	@Test
	public void testCreateBooking() throws Exception {
		Booking randomBooking = randomBooking();

		Booking postBooking = testCreateBooking_addBooking(randomBooking);

		assertEquals(randomBooking, postBooking);
		assertValid(postBooking);
	}

	protected Booking testCreateBooking_addBooking(Booking booking)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testUpdateBooking() throws Exception {
		Booking randomBooking = randomBooking();

		Booking postBooking = testUpdateBooking_addBooking(randomBooking);

		assertEquals(randomBooking, postBooking);
		assertValid(postBooking);
	}

	protected Booking testUpdateBooking_addBooking(Booking booking)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testDeleteBooking() throws Exception {
		Booking randomBooking = randomBooking();

		Booking postBooking = testDeleteBooking_addBooking(randomBooking);

		assertEquals(randomBooking, postBooking);
		assertValid(postBooking);
	}

	protected Booking testDeleteBooking_addBooking(Booking booking)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	@Test
	public void testGetBookingDetails() throws Exception {
		Assert.assertTrue(false);
	}

	@Test
	public void testGetCourtBookingTermsConditions() throws Exception {
		Booking postBooking = testGetBooking_addBooking();

		BookingTermsConditions postBookingTermsConditions =
			testGetCourtBookingTermsConditions_addBookingTermsConditions(
				postBooking.getId(), randomBookingTermsConditions());

		BookingTermsConditions getBookingTermsConditions =
			bookingResource.getCourtBookingTermsConditions(postBooking.getId());

		assertEquals(postBookingTermsConditions, getBookingTermsConditions);
		assertValid(getBookingTermsConditions);
	}

	protected BookingTermsConditions
			testGetCourtBookingTermsConditions_addBookingTermsConditions(
				long bookingId, BookingTermsConditions bookingTermsConditions)
		throws Exception {

		throw new UnsupportedOperationException(
			"This method needs to be implemented");
	}

	protected void assertContains(Booking booking, List<Booking> bookings) {
		boolean contains = false;

		for (Booking item : bookings) {
			if (equals(booking, item)) {
				contains = true;

				break;
			}
		}

		Assert.assertTrue(bookings + " does not contain " + booking, contains);
	}

	protected void assertHttpResponseStatusCode(
		int expectedHttpResponseStatusCode,
		HttpInvoker.HttpResponse actualHttpResponse) {

		Assert.assertEquals(
			expectedHttpResponseStatusCode, actualHttpResponse.getStatusCode());
	}

	protected void assertEquals(Booking booking1, Booking booking2) {
		Assert.assertTrue(
			booking1 + " does not equal " + booking2,
			equals(booking1, booking2));
	}

	protected void assertEquals(
		List<Booking> bookings1, List<Booking> bookings2) {

		Assert.assertEquals(bookings1.size(), bookings2.size());

		for (int i = 0; i < bookings1.size(); i++) {
			Booking booking1 = bookings1.get(i);
			Booking booking2 = bookings2.get(i);

			assertEquals(booking1, booking2);
		}
	}

	protected void assertEquals(
		BookingTermsConditions bookingTermsConditions1,
		BookingTermsConditions bookingTermsConditions2) {

		Assert.assertTrue(
			bookingTermsConditions1 + " does not equal " +
				bookingTermsConditions2,
			equals(bookingTermsConditions1, bookingTermsConditions2));
	}

	protected void assertEqualsIgnoringOrder(
		List<Booking> bookings1, List<Booking> bookings2) {

		Assert.assertEquals(bookings1.size(), bookings2.size());

		for (Booking booking1 : bookings1) {
			boolean contains = false;

			for (Booking booking2 : bookings2) {
				if (equals(booking1, booking2)) {
					contains = true;

					break;
				}
			}

			Assert.assertTrue(
				bookings2 + " does not contain " + booking1, contains);
		}
	}

	protected void assertValid(Booking booking) throws Exception {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("appUserEmail", additionalAssertFieldName)) {
				if (booking.getAppUserEmail() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (booking.getAppUserId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("appUserName", additionalAssertFieldName)) {
				if (booking.getAppUserName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("bookingDate", additionalAssertFieldName)) {
				if (booking.getBookingDate() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("bookingId", additionalAssertFieldName)) {
				if (booking.getBookingId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("category", additionalAssertFieldName)) {
				if (booking.getCategory() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("court", additionalAssertFieldName)) {
				if (booking.getCourt() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("courtId", additionalAssertFieldName)) {
				if (booking.getCourtId() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("courtLocation", additionalAssertFieldName)) {
				if (booking.getCourtLocation() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("courtName", additionalAssertFieldName)) {
				if (booking.getCourtName() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (booking.getRequestStatus() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("slotEndTime", additionalAssertFieldName)) {
				if (booking.getSlotEndTime() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("slotStartTime", additionalAssertFieldName)) {
				if (booking.getSlotStartTime() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(Page<Booking> page) {
		boolean valid = false;

		java.util.Collection<Booking> bookings = page.getItems();

		int size = bookings.size();

		if ((page.getLastPage() > 0) && (page.getPage() > 0) &&
			(page.getPageSize() > 0) && (page.getTotalCount() > 0) &&
			(size > 0)) {

			valid = true;
		}

		Assert.assertTrue(valid);
	}

	protected void assertValid(BookingTermsConditions bookingTermsConditions) {
		boolean valid = true;

		for (String additionalAssertFieldName :
				getAdditionalBookingTermsConditionsAssertFieldNames()) {

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (bookingTermsConditions.getDescription() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("excerpt", additionalAssertFieldName)) {
				if (bookingTermsConditions.getExcerpt() == null) {
					valid = false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (bookingTermsConditions.getTitle() == null) {
					valid = false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		Assert.assertTrue(valid);
	}

	protected String[] getAdditionalAssertFieldNames() {
		return new String[0];
	}

	protected String[] getAdditionalBookingTermsConditionsAssertFieldNames() {
		return new String[0];
	}

	protected List<GraphQLField> getGraphQLFields() throws Exception {
		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field :
				getDeclaredFields(misk.headless.dto.v1_0.Booking.class)) {

			if (!ArrayUtil.contains(
					getAdditionalAssertFieldNames(), field.getName())) {

				continue;
			}

			graphQLFields.addAll(getGraphQLFields(field));
		}

		return graphQLFields;
	}

	protected List<GraphQLField> getGraphQLFields(Field... fields)
		throws Exception {

		List<GraphQLField> graphQLFields = new ArrayList<>();

		for (Field field : fields) {
			com.liferay.portal.vulcan.graphql.annotation.GraphQLField
				vulcanGraphQLField = field.getAnnotation(
					com.liferay.portal.vulcan.graphql.annotation.GraphQLField.
						class);

			if (vulcanGraphQLField != null) {
				Class<?> clazz = field.getType();

				if (clazz.isArray()) {
					clazz = clazz.getComponentType();
				}

				List<GraphQLField> childrenGraphQLFields = getGraphQLFields(
					getDeclaredFields(clazz));

				graphQLFields.add(
					new GraphQLField(field.getName(), childrenGraphQLFields));
			}
		}

		return graphQLFields;
	}

	protected String[] getIgnoredEntityFieldNames() {
		return new String[0];
	}

	protected boolean equals(Booking booking1, Booking booking2) {
		if (booking1 == booking2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalAssertFieldNames()) {

			if (Objects.equals("appUserEmail", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getAppUserEmail(),
						booking2.getAppUserEmail())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appUserId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getAppUserId(), booking2.getAppUserId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("appUserName", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getAppUserName(), booking2.getAppUserName())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("bookingDate", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getBookingDate(), booking2.getBookingDate())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("bookingId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getBookingId(), booking2.getBookingId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("category", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getCategory(), booking2.getCategory())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("court", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getCourt(), booking2.getCourt())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("courtId", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getCourtId(), booking2.getCourtId())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("courtLocation", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getCourtLocation(),
						booking2.getCourtLocation())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("courtName", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getCourtName(), booking2.getCourtName())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("requestStatus", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getRequestStatus(),
						booking2.getRequestStatus())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("slotEndTime", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getSlotEndTime(), booking2.getSlotEndTime())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("slotStartTime", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						booking1.getSlotStartTime(),
						booking2.getSlotStartTime())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected boolean equals(
		Map<String, Object> map1, Map<String, Object> map2) {

		if (Objects.equals(map1.keySet(), map2.keySet())) {
			for (Map.Entry<String, Object> entry : map1.entrySet()) {
				if (entry.getValue() instanceof Map) {
					if (!equals(
							(Map)entry.getValue(),
							(Map)map2.get(entry.getKey()))) {

						return false;
					}
				}
				else if (!Objects.deepEquals(
							entry.getValue(), map2.get(entry.getKey()))) {

					return false;
				}
			}

			return true;
		}

		return false;
	}

	protected boolean equals(
		BookingTermsConditions bookingTermsConditions1,
		BookingTermsConditions bookingTermsConditions2) {

		if (bookingTermsConditions1 == bookingTermsConditions2) {
			return true;
		}

		for (String additionalAssertFieldName :
				getAdditionalBookingTermsConditionsAssertFieldNames()) {

			if (Objects.equals("description", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						bookingTermsConditions1.getDescription(),
						bookingTermsConditions2.getDescription())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("excerpt", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						bookingTermsConditions1.getExcerpt(),
						bookingTermsConditions2.getExcerpt())) {

					return false;
				}

				continue;
			}

			if (Objects.equals("title", additionalAssertFieldName)) {
				if (!Objects.deepEquals(
						bookingTermsConditions1.getTitle(),
						bookingTermsConditions2.getTitle())) {

					return false;
				}

				continue;
			}

			throw new IllegalArgumentException(
				"Invalid additional assert field name " +
					additionalAssertFieldName);
		}

		return true;
	}

	protected Field[] getDeclaredFields(Class clazz) throws Exception {
		Stream<Field> stream = Stream.of(
			ReflectionUtil.getDeclaredFields(clazz));

		return stream.filter(
			field -> !field.isSynthetic()
		).toArray(
			Field[]::new
		);
	}

	protected java.util.Collection<EntityField> getEntityFields()
		throws Exception {

		if (!(_bookingResource instanceof EntityModelResource)) {
			throw new UnsupportedOperationException(
				"Resource is not an instance of EntityModelResource");
		}

		EntityModelResource entityModelResource =
			(EntityModelResource)_bookingResource;

		EntityModel entityModel = entityModelResource.getEntityModel(
			new MultivaluedHashMap());

		Map<String, EntityField> entityFieldsMap =
			entityModel.getEntityFieldsMap();

		return entityFieldsMap.values();
	}

	protected List<EntityField> getEntityFields(EntityField.Type type)
		throws Exception {

		java.util.Collection<EntityField> entityFields = getEntityFields();

		Stream<EntityField> stream = entityFields.stream();

		return stream.filter(
			entityField ->
				Objects.equals(entityField.getType(), type) &&
				!ArrayUtil.contains(
					getIgnoredEntityFieldNames(), entityField.getName())
		).collect(
			Collectors.toList()
		);
	}

	protected String getFilterString(
		EntityField entityField, String operator, Booking booking) {

		StringBundler sb = new StringBundler();

		String entityFieldName = entityField.getName();

		sb.append(entityFieldName);

		sb.append(" ");
		sb.append(operator);
		sb.append(" ");

		if (entityFieldName.equals("appUserEmail")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getAppUserEmail()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("appUserId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("appUserName")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getAppUserName()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("bookingDate")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getBookingDate()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("bookingId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("category")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getCategory()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("court")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("courtId")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("courtLocation")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getCourtLocation()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("courtName")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getCourtName()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("requestStatus")) {
			throw new IllegalArgumentException(
				"Invalid entity field " + entityFieldName);
		}

		if (entityFieldName.equals("slotEndTime")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getSlotEndTime()));
			sb.append("'");

			return sb.toString();
		}

		if (entityFieldName.equals("slotStartTime")) {
			sb.append("'");
			sb.append(String.valueOf(booking.getSlotStartTime()));
			sb.append("'");

			return sb.toString();
		}

		throw new IllegalArgumentException(
			"Invalid entity field " + entityFieldName);
	}

	protected String invoke(String query) throws Exception {
		HttpInvoker httpInvoker = HttpInvoker.newHttpInvoker();

		httpInvoker.body(
			JSONUtil.put(
				"query", query
			).toString(),
			"application/json");
		httpInvoker.httpMethod(HttpInvoker.HttpMethod.POST);
		httpInvoker.path("http://localhost:8080/o/graphql");
		httpInvoker.userNameAndPassword("test@liferay.com:test");

		HttpInvoker.HttpResponse httpResponse = httpInvoker.invoke();

		return httpResponse.getContent();
	}

	protected JSONObject invokeGraphQLMutation(GraphQLField graphQLField)
		throws Exception {

		GraphQLField mutationGraphQLField = new GraphQLField(
			"mutation", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(mutationGraphQLField.toString()));
	}

	protected JSONObject invokeGraphQLQuery(GraphQLField graphQLField)
		throws Exception {

		GraphQLField queryGraphQLField = new GraphQLField(
			"query", graphQLField);

		return JSONFactoryUtil.createJSONObject(
			invoke(queryGraphQLField.toString()));
	}

	protected Booking randomBooking() throws Exception {
		return new Booking() {
			{
				appUserEmail = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				appUserId = RandomTestUtil.randomLong();
				appUserName = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				bookingDate = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				bookingId = RandomTestUtil.randomLong();
				category = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				courtId = RandomTestUtil.randomLong();
				courtLocation = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				courtName = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				slotEndTime = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
				slotStartTime = StringUtil.toLowerCase(
					RandomTestUtil.randomString());
			}
		};
	}

	protected Booking randomIrrelevantBooking() throws Exception {
		Booking randomIrrelevantBooking = randomBooking();

		return randomIrrelevantBooking;
	}

	protected Booking randomPatchBooking() throws Exception {
		return randomBooking();
	}

	protected BookingTermsConditions randomBookingTermsConditions()
		throws Exception {

		return new BookingTermsConditions() {
			{
				description = RandomTestUtil.randomString();
				excerpt = RandomTestUtil.randomString();
				title = RandomTestUtil.randomString();
			}
		};
	}

	protected BookingResource bookingResource;
	protected Group irrelevantGroup;
	protected Company testCompany;
	protected Group testGroup;

	protected class GraphQLField {

		public GraphQLField(String key, GraphQLField... graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(String key, List<GraphQLField> graphQLFields) {
			this(key, new HashMap<>(), graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			GraphQLField... graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = Arrays.asList(graphQLFields);
		}

		public GraphQLField(
			String key, Map<String, Object> parameterMap,
			List<GraphQLField> graphQLFields) {

			_key = key;
			_parameterMap = parameterMap;
			_graphQLFields = graphQLFields;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder(_key);

			if (!_parameterMap.isEmpty()) {
				sb.append("(");

				for (Map.Entry<String, Object> entry :
						_parameterMap.entrySet()) {

					sb.append(entry.getKey());
					sb.append(": ");
					sb.append(entry.getValue());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append(")");
			}

			if (!_graphQLFields.isEmpty()) {
				sb.append("{");

				for (GraphQLField graphQLField : _graphQLFields) {
					sb.append(graphQLField.toString());
					sb.append(", ");
				}

				sb.setLength(sb.length() - 2);

				sb.append("}");
			}

			return sb.toString();
		}

		private final List<GraphQLField> _graphQLFields;
		private final String _key;
		private final Map<String, Object> _parameterMap;

	}

	private static final com.liferay.portal.kernel.log.Log _log =
		LogFactoryUtil.getLog(BaseBookingResourceTestCase.class);

	private static BeanUtilsBean _beanUtilsBean = new BeanUtilsBean() {

		@Override
		public void copyProperty(Object bean, String name, Object value)
			throws IllegalAccessException, InvocationTargetException {

			if (value != null) {
				super.copyProperty(bean, name, value);
			}
		}

	};
	private static DateFormat _dateFormat;

	@Inject
	private misk.headless.resource.v1_0.BookingResource _bookingResource;

}